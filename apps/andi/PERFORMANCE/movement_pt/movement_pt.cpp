/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "fstream"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;


void MultipleSteps(const double simTime, int iXRightBorderArg) {
    int iXLeftBorder = 0;
    int iXRightBorder = iXRightBorderArg;
    int iYBottomBorder = 0;
    int iYTopBorder = iXRightBorderArg;
    int iZFrontBorder = 0;
    int iZBackBorder = iXRightBorderArg;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
            iYTopBorder, 0.3 * 1.0 / std::sqrt(3), 20., 40., 0.0000146072, 1.225, 0);

    converter.print();

    T deltaX = converter.getConversionFactorLength();

    T omega = 1.5;//converter.getLatticeRelaxationFrequency();
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);

//    T const vel[Lattice < T > ::d] = { -0.075, 0.0, 0.0 };
    T const vel[Lattice < T > ::d] = { 0.0, 0.0, 0.0 };

    ConstVelocityProviderF<T, Lattice> constProvider(vel);
    ExternalFieldALE<T, Lattice, ConstVelocityProviderF> constExternalField(constProvider);

    PointXd<T, Lattice> lattice_anchor {0.0, 0.0, 0.0};
    PoseXd<T, Lattice> initial_lattice_pose {{0.0, 0.0, 0.0}, {0, 0, 0}}; //inital position of the lattice inside the external field

    BlockLatticeALE3D<T, Lattice, ExternalFieldALE<T, Lattice, ConstVelocityProviderF>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1,
            lattice_anchor, initial_lattice_pose, &bulkDynamics, constExternalField);

    OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
            createInterpBoundaryCondition3D<T, Lattice,
                    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    // prepareLattice
    boundaryCondition->addImpedanceBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary1N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary1P(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary2N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZFrontBorder, iZFrontBorder, omega);
    boundaryCondition->addImpedanceBoundary2P(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZBackBorder, iZBackBorder, omega);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder,
                           iZFrontBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder,
                           iZBackBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder,
                           iZFrontBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder,
                           iZBackBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder,
                           iZFrontBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1,
                           iZBackBorder - 1, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1,
                           iZBackBorder - 1, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1,
                           &instances::getBounceBack<T, Lattice>());

    boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder, iYBottomBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder, iYTopBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder, iYBottomBorder, iZBackBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder, iYTopBorder, iZBackBorder, omega);
    //
    boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder, iYBottomBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder, iYTopBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder, iYBottomBorder, iZBackBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder, iYTopBorder, iZBackBorder, omega);


    lattice.initDataArrays();

    // setBoundaryValues

    for (int iX = 0; iX <= iXRightBorder; ++iX) {
        for (int iY = 0; iY <= iYTopBorder; ++iY) {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
                T
                vel[] = {0.0, 0.0, 0.0};

                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }

    for (int iX = static_cast<int>(0.25 * (iXRightBorder + 1));
         iX < static_cast<int>(0.75 * (iXRightBorder + 1)); ++iX) {
        for (int iY = static_cast<int>(0.25 * (iYTopBorder + 1));
             iY <= static_cast<int>(0.25 * (iYTopBorder + 1)); ++iY) {
            for (int iZ = static_cast<int>(0.25 * (iZBackBorder + 1));
                 iZ < static_cast<int>(0.75 * (iZBackBorder + 1)); ++iZ) {
                T force[Lattice < T > ::d] = {0., converter.getLatticeForce(320. * deltaX * deltaX / 1.225), 0.0};
                lattice.defineForce(iX, iX, iY, iY, iZ, iZ, force);
            }
        }
    }

    std::string name;
    name = "pt_movement-";
    name += std::to_string(iXRightBorder + 1);
    name += "-";
    name += std::to_string(simTime) + "s";

    BlockVTKwriter3D<T> vtkWriter(name);
    BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
//    BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);

    singleton::directories().setOutputDir("./tmp/");

    vtkWriter.addFunctor(densityFunctor);
//    vtkWriter.addFunctor(velocityFunctor);
    vtkWriter.addFunctor(physVelocityFunctor);
    vtkWriter.addFunctor(forceFunctor);
    vtkWriter.write(0);

    lattice.copyDataToGPU();

    Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();

    for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps) {
        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        if (iSteps % converter.getLatticeTime(0.5) == 0)
            timer.print(iSteps, 2);

        if (iSteps % converter.getLatticeTime(0.05) == 0) {
//            lattice.copyDataToCPU();
//            vtkWriter.write(iSteps);
        }

        T conversionVelocity = converter.getConversionFactorVelocity();

        lattice.moveMeshGPU({{20.0/conversionVelocity,0,0}, {0,0,0}});
    }

    timer.stop();

#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
#endif

    delete boundaryCondition;

}

int main(int argc, char **argv) {
    const double simTime = 10;
    MultipleSteps(simTime, 55);
    return 0;
}
