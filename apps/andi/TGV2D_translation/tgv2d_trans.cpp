/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2006 - 2012 Mathias J. Krause, Jonas Fietz,
 *  Jonas Latt, Jonas Kratzke
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/* cavity2d.cpp:
 * This example illustrates a flow in a cuboid, lid-driven cavity.
 * It also shows how to use the XML parameter files and has an
 * example description file for OpenGPI. This version is for sequential
 * use. A version for parallel use is also available.
 */

#define D2Q9LATTICE 1
typedef double T;

#include <chrono>

#include "olb2D.h"

#ifndef OLB_PRECOMPILED // Unless precompiled version is used,

#include "olb2D.hh"   // include full template code

#endif

#include <cmath>
#include <iostream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

#define DESCRIPTOR D2Q9Descriptor


template <typename T, template <typename U> class DESCRIPTOR>
class TGV2D : public AnalyticalF2D<T, T> {
protected:
    T u0;

public:
    TGV2D(UnitConverter<T, DESCRIPTOR> const& converter, T frac) : AnalyticalF2D<T, S>(2) {
        u0 = converter.getCharLatticeVelocity();
    }

    bool operator()(T output[], const S input[]) override {

        T x = input[0] * 2 * M_PI;
        T y = input[1] * 2 * M_PI;

        output[0] = u0 * sin(x) * cos(y);
        output[1] = -u0 * cos(x) * sin(y);

        return true;
    }
};

void prepareLattice(UnitConverter<T, DESCRIPTOR> const &converter,
                    BlockLattice2D<T, DESCRIPTOR> &lattice,
                    Dynamics<T, DESCRIPTOR> &bulkDynamics,
                    OnLatticeBoundaryCondition2D<T, DESCRIPTOR> &bc) {
    const int nx = lattice.getNx();
    const int ny = lattice.getNy();
    const T omega = converter.getLatticeRelaxationFrequency();

    // left boundary
    bc.addPeriodicBoundary0N(0, 0, 1, ny - 2, omega);
    // right boundary
    bc.addPeriodicBoundary0P(nx - 1, nx - 1, 1, ny - 2, omega);
    // bottom boundary
    bc.addPeriodicBoundary1N(1, nx - 2, 0, 0, omega);
    // top boundary
    bc.addPeriodicBoundary1P(1, nx - 2, ny - 1, ny - 1, omega);

    // corners
    bc.addExternalPeriodicCornerNN(0, 0, omega);
    bc.addExternalPeriodicCornerNP(0, ny - 1, omega);
    bc.addExternalPeriodicCornerPN(nx - 1, 0, omega);
    bc.addExternalPeriodicCornerPP(nx - 1, ny - 1, omega);
}

void setBoundaryValues(UnitConverter<T, DESCRIPTOR> const &converter,
                       BlockLattice2D<T, DESCRIPTOR> &lattice,
                       BlockGeometry2D<T>& blockGeometry, int iT) {
    if (iT == 0) {
        AnalyticalConst2D<T,T> rho(1.);
        TGV2D<T, DESCRIPTOR> u(converter, 1);

        lattice.defineRhoU(blockGeometry, 0, rho, u);
        lattice.iniEquilibrium(blockGeometry, 0, rho, u);
    }

}

void getResults(BlockLatticeStructure2D<T, DESCRIPTOR> &lattice,
                UnitConverter<T, DESCRIPTOR> const &converter, int iT, Timer<T> timer,
                const T logT, const T imSave, const T vtkSave,
                std::string filenameGif, std::string filenameVtk,
                const int timerPrintMode, const int timerTimeSteps, bool converged) {

    if (iT % timerTimeSteps == 0 || converged) {
        timer.print(iT, timerPrintMode);
    }

    BlockVTKwriter2D<T> vtkWriter(filenameVtk);
    BlockLatticePhysVelocity2D<T, DESCRIPTOR> velocity(lattice, converter);
    BlockLatticePhysPressure2D<T, DESCRIPTOR> pressure(lattice, converter);
    vtkWriter.addFunctor(velocity);
    vtkWriter.addFunctor(pressure);


    if ((iT % converter.getLatticeTime(vtkSave) == 0 && iT > 0) || converged) {
        vtkWriter.write(iT);
    }
}


int main(int argc, char *argv[]) {

    // === 1st Step: Initialization ===
    olbInit(&argc, &argv);
    OstreamManager clout(std::cout, "main");

    std::string olbdir = "../../";  //config["Application"]["OlbDir"].get<std::string>();
    std::string outputdir = "./tmp/"; //config["Output"]["OutputDir"].get<std::string>();
    singleton::directories().setOlbDir(olbdir);
    singleton::directories().setOutputDir(outputdir);

    UnitConverterFromResolutionAndLatticeVelocity<T, DESCRIPTOR> const converter(
            (int) 100, // resolution
            (T) 0.1, // charLatticeVelocity: should be smaller or equal than 0.1. Higher => less stable, smaller => more accurate,
            (T) 1, // charPhysLength: reference length of simulation geometry
            (T) 1, // charPhysVelocity: maximal/highest expected velocity during simulation in __m / s__
            (T) 0.001, // physViscosity: physical kinematic viscosity in __m^2 / s__
            (T) 1.0 // physDensity: physical density in __kg / m^3__
    );

    int N = converter.getLatticeLength(1) + 1; // number of voxels in x,y,z direction

    Vector<T, 2> origin;
    Vector<int, 2> extent(N, N);

    Cuboid2D<T> rect(origin, converter.getPhysDeltaX(), extent);
    BlockGeometry2D<T> blockGeometry(rect);

    // === 3rd Step: Prepare Lattice ===
    T logT = 0.1;
    T imSave = 1;
    T vtkSave = 0.01;
    T maxPhysT = 10;
    int timerPrintMode = 0;
    int timerTimeSteps = 100;

    Timer<T> timer(converter.getLatticeTime(maxPhysT), N*N);


    std::string filenameGif = "tgv2dimage";
    std::string filenameVtk = "tgv2dvtk_10s_translation";

    ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T, DESCRIPTOR> > bulkDynamics(
            converter.getLatticeRelaxationFrequency()
    );

    BlockLatticeALE2D<T, DESCRIPTOR> lattice(N, N, &bulkDynamics);

    OnLatticeBoundaryCondition2D<T, DESCRIPTOR> *boundaryCondition = createInterpBoundaryCondition2D<T, DESCRIPTOR, ConstRhoBGKdynamics>(lattice);

    prepareLattice(converter, lattice, bulkDynamics, *boundaryCondition);

    lattice.initDataArrays();

    // === 4th Step: Main Loop with Timer ===

    int interval = converter.getLatticeTime(1);
    T epsilon = 1e-3;
    util::ValueTracer<T> converge(interval, epsilon);
    std::cout << "Start time loop" << std::endl;

    timer.start();

    for (int iT = 0; iT <= converter.getLatticeTime(maxPhysT); ++iT) {

        setBoundaryValues(converter, lattice, blockGeometry, iT);

        lattice.collideAndStream<ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T, DESCRIPTOR>>>();

        getResults(lattice, converter, iT, timer, logT, imSave, vtkSave, filenameGif, filenameVtk, timerPrintMode,
                   timerTimeSteps, converge.hasConverged());

//        lattice.moveMesh({1, 0}, 0, {0, 0});
//        lattice.moveMesh({0, 1}, 0, {0, 0});
//        lattice.moveMesh({1, 1}, 0, {0, 0});
//        lattice.moveMesh({100, 100}, 0, {0, 0});
//        lattice.moveMesh({33, 79}, 0, {0, 0});

        // whups, negative translation doesnt work, probably streaming in something worng
//        lattice.moveMesh({-1, 0}, 0, {0, 0});

        /**
         * Translation with interpolation only works well with high resolution
         * 0.5 is the 'worst' one
         * 0.25 and 0.75 (and etc.) are identical
         */
//        lattice.moveMesh({0.5, 0}, 0, {0, 0});
//        lattice.moveMesh({0.25, 0}, 0, {0, 0});
//        lattice.moveMesh({0.75, 0}, 0, {0, 0});

    }
    timer.stop();
    timer.printSummary();


    delete boundaryCondition;
}
