/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "fstream"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;


void MultipleSteps(const double simTime, int iXRightBorderArg) {
    int iXLeftBorder = 0;
    int iXRightBorder = iXRightBorderArg;
    int iYBottomBorder = 0;
    int iYTopBorder = iXRightBorderArg;
    int iZFrontBorder = 0;
    int iZBackBorder = iXRightBorderArg;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
            iYTopBorder, 0.3 * 1.0 / std::sqrt(3), 20., 40., 0.0000146072, 1.225, 0);

    converter.print();

    T deltaX = converter.getConversionFactorLength();

    T omega = 1.5;//converter.getLatticeRelaxationFrequency();
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);


   int eX = 99;
   int eY = iXRightBorderArg+5;
   int eZ = iXRightBorderArg+5;
   int eSize = (eX+1)*(eY+1)*(eZ+1);
   T** field_data = new T *[Lattice<T>::d];
   for (int i = 0; i < Lattice<T>::d; ++i) {
       field_data[i] = new T[eSize];
   }

   for (int x = 0; x < 50; ++x) {
       for (int y = 0; y < eY + 1; ++y) {
           for (int z = 0; z < eZ + 1; ++z) {
               field_data[0][util::getCellIndex3D(x, y, z, eY, eZ)] = -0.075;
               field_data[1][util::getCellIndex3D(x, y, z, eY, eZ)] = 0.0;
               field_data[2][util::getCellIndex3D(x, y, z, eY, eZ)] = 0.0;
           }
       }
   }

   for (int x = 50; x < eX + 1; ++x) {
       for (int y = 0; y < eY + 1; ++y) {
           for (int z = 0; z < eZ + 1; ++z) {
               field_data[0][util::getCellIndex3D(x, y, z, eY, eZ)] = 0.0;
               field_data[1][util::getCellIndex3D(x, y, z, eY, eZ)] = -0.050;
               field_data[2][util::getCellIndex3D(x, y, z, eY, eZ)] = 0.0;
           }
       }
   }

   for (int x = 80; x < eX + 1; ++x) {
       for (int y = 0; y < eY + 1; ++y) {
           for (int z = 0; z < eZ + 1; ++z) {
               field_data[0][util::getCellIndex3D(x, y, z, eY, eZ)] = 0.1;
               field_data[1][util::getCellIndex3D(x, y, z, eY, eZ)] = -0.075;
               field_data[2][util::getCellIndex3D(x, y, z, eY, eZ)] = 0.0;
           }
       }
   }



    Vec3<T> anchor(((T)iXRightBorder)/2.0, ((T)iXRightBorder)/2.0, ((T)iXRightBorder)/2.0);
    const Vec3<T> velocity(0.1, 0,0);

   PredefinedExternalField<T, Lattice> predefinedExternalField(field_data, eX, eY, eZ);

//    ConstExternalField<T, Lattice> constExternalField(velocity);
    ZeroExternalField<T, Lattice> zeroExternalField;
    BlockLatticeALE3D<T, Lattice, PredefinedExternalField<T, Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1,
            anchor, &bulkDynamics, predefinedExternalField);

    OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
            createInterpBoundaryCondition3D<T, Lattice,
                    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    // prepareLattice
    boundaryCondition->addImpedanceBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary1N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary1P(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder,
                                              iZFrontBorder + 1, iZBackBorder - 1, omega);
    boundaryCondition->addImpedanceBoundary2N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZFrontBorder, iZFrontBorder, omega);
    boundaryCondition->addImpedanceBoundary2P(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1,
                                              iZBackBorder, iZBackBorder, omega);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder,
                           iZFrontBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder,
                           iZBackBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder,
                           iZFrontBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder,
                           iZBackBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder,
                           iZFrontBorder, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1,
                           iZBackBorder - 1, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1,
                           iZBackBorder - 1, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1,
                           &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1,
                           &instances::getBounceBack<T, Lattice>());

    boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder, iYBottomBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder, iYTopBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder, iYBottomBorder, iZBackBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder, iYTopBorder, iZBackBorder, omega);
    //
    boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder, iYBottomBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder, iYTopBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder, iYBottomBorder, iZBackBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder, iYTopBorder, iZBackBorder, omega);


    lattice.initDataArrays();

    // setBoundaryValues

    for (int iX = 0; iX <= iXRightBorder; ++iX) {
        for (int iY = 0; iY <= iYTopBorder; ++iY) {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
                T
                vel[] = {0.0, 0.0, 0.0};

                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }

    for (int iX = static_cast<int>(0.25 * (iXRightBorder + 1));
         iX < static_cast<int>(0.75 * (iXRightBorder + 1)); ++iX) {
        for (int iY = static_cast<int>(0.25 * (iYTopBorder + 1));
             iY <= static_cast<int>(0.25 * (iYTopBorder + 1)); ++iY) {
            for (int iZ = static_cast<int>(0.25 * (iZBackBorder + 1));
                 iZ < static_cast<int>(0.75 * (iZBackBorder + 1)); ++iZ) {
                T force[Lattice < T > ::d] = {0., converter.getLatticeForce(320. * deltaX * deltaX / 1.225), 0.0};
                lattice.defineForce(iX, iX, iY, iY, iZ, iZ, force);
            }
        }
    }

    std::string name;
    name = "rotorDisk_X";
    name += std::to_string(iXRightBorder + 1);
    name += "_ALEGPU_trans_e2";

    BlockVTKwriter3D<T> vtkWriter(name);
    BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
    BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);

    singleton::directories().setOutputDir("./tmp/");

    vtkWriter.addFunctor(densityFunctor);
    vtkWriter.addFunctor(velocityFunctor);
    vtkWriter.addFunctor(physVelocityFunctor);
    vtkWriter.addFunctor(forceFunctor);
    vtkWriter.write(0);

    lattice.copyDataToGPU();

    Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();

    Vec3<T> mov(0.1, 0, 0);
    Vec3<T> angles(0,0,0);

  Vec3<T> position(0, ((T)iXRightBorder)/2.0, ((T)iXRightBorder)/2.0);
  Vec3<T> orientation(0,0,0);


    for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps) { 
#ifdef ENABLE_CUDA
      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
#else
      lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
#endif


        if (iSteps % converter.getLatticeTime(0.1) == 0)
            timer.print(iSteps, 2);

        if (iSteps % converter.getLatticeTime(0.2) == 0) {
#ifdef ENABLE_CUDA
          lattice.copyDataToCPU();
#endif
          vtkWriter.write(iSteps);
        }

#ifdef ENABLE_CUDA
      lattice.moveMeshGPU(mov, angles, position, orientation);
#else
        lattice.moveMesh(mov, angles, position, orientation);
#endif
      position(0) += mov(0);

    }

    timer.stop();

    std::cout << "I ended" << std::endl;
#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
#endif

    delete boundaryCondition;

}

int main(int argc, char **argv) {
    const double simTime = 10;
    MultipleSteps(simTime, 25);
    return 0;
}
