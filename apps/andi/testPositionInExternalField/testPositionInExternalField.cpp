/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "fstream"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T>
bool approx_equal(T a, T b){
    const T eps = 1E-3;
    if (a == 0) return fabs(b) <= eps;
    if (b == 0) return fabs(a) <= eps;
    return fabs(a - b) / max(fabs(a), fabs(b)) <= eps;
}

void testWithRotationMovement1() {
    const int sizeXYZ = 2;
    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
            sizeXYZ, 0.3 * 1.0 / std::sqrt(3), 20., 40., 0.0000146072, 1.225, 0);
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(1.5);

    int eX = 8;
    int eY = 4;
    int eZ = 4;
    int eSize = (eX+1)*(eY+1)*(eZ+1);
    T** field_data = new T *[Lattice<T>::d];
    for (int i = 0; i < Lattice<T>::d; ++i) {
        field_data[i] = new T[eSize];
    }

    for (int i = 0; i < eSize; ++i) {
        field_data[0][i] = 0.0;
        field_data[1][i] = 0.0;
        field_data[2][i] = 0.0;
    }


  Vec3<T> translation(2,0,0);
  Vec3<T> angles(0,0,-30.0*M_PI/180.0);
  Vec3<T> absolute_position(0, 0, 1);
  Vec3<T> absolute_orientation(0, 0, 0);

  Vec3<T> anchor(0, 0, 1);

/**
 * We look at vertex (2, 0, 1) in lattice coordinates
 * after the given transform (rot -30 deg around Z-axis, translation of (2, 0, 0)) its coordinates are (~3.73, 1, 2)
 * With the anchor at (0, 0, 1) and absolute position at (0, 0, 1) the relevant surrounding points for interpolation are:
 *  v1 (3, 1, 1)
 *  v2 (4, 1, 1)
 * -> write identical specific velocity at those locations in the external field
 * sampling the vertex (2, 0, 1) in lattice coordinates after the mesh movement should yield exactly that velocity
 * (the velocity also has to be rotated -30 deg around Z-axis)
 */
  const int index1 = util::getCellIndex3D(3, 1, 1, eY+1, eZ+1);
  const int index2 = util::getCellIndex3D(4, 1, 1, eY+1, eZ+1);
  Vec3<T> expectedVel(1, 2, 3);

  for (int i = 0; i < 3; ++i) {
      field_data[i][index1] = expectedVel(i);
      field_data[i][index2] = expectedVel(i);
  }

  PredefinedExternalField<T, Lattice> externalField(field_data, eX+1, eY+1, eZ+1);
  BlockLatticeALE3D<T, Lattice, PredefinedExternalField<T, Lattice>> lattice(sizeXYZ + 1, sizeXYZ + 1, sizeXYZ + 1, anchor, &bulkDynamics, externalField);
  lattice.initDataArrays();

  for (int iX = 0; iX <= sizeXYZ; ++iX) {
    for (int iY = 0; iY <= sizeXYZ; ++iY) {
      for (int iZ = 0; iZ <= sizeXYZ; ++iZ) {
        T vel[] = {0.0, 0.0, 0.0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }
  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
  lattice.moveMesh(translation, angles, absolute_position, absolute_orientation);
  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();


  T result[3];
  T shit;
  lattice.get(2, 0, 1).computeRhoU(shit, result);

  Mat3<T> rot(angles, Rotation::ToBody{});
  Mat3<T> rot2(absolute_orientation, Rotation::ToBody{});
  Affine3<T> rotTrans(rot);
  rotTrans.applyToRight(rot2);
  transformInPlace(expectedVel, rotTrans);

  printf("RES: %f, %f, %f\n", result[0], result[1], result[2]);
  printf("EXP: %f, %f, %f\n", expectedVel(0), expectedVel(1), expectedVel(2));

  assert(approx_equal(result[0], expectedVel(0)));
  assert(approx_equal(result[1], expectedVel(1)));
  assert(approx_equal(result[2], expectedVel(2)));
}

void testWithRotationMovement2() {
  const int sizeXYZ = 2;
  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      sizeXYZ, 0.3 * 1.0 / std::sqrt(3), 20., 40., 0.0000146072, 1.225, 0);
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(1.5);

  int eX = 8;
  int eY = 4;
  int eZ = 4;
  int eSize = (eX+1)*(eY+1)*(eZ+1);
  T** field_data = new T *[Lattice<T>::d];
  for (int i = 0; i < Lattice<T>::d; ++i) {
    field_data[i] = new T[eSize];
  }

  for (int i = 0; i < eSize; ++i) {
    field_data[0][i] = 0.0;
    field_data[1][i] = 0.0;
    field_data[2][i] = 0.0;
  }

  /**
   * We look at vertex (2, 0, 1) in lattice coordinates
   * after the given transform (rot -45 deg around Z-axis, translation of (2, 1, 0)) its coordinates are (~4.414, 2, 1)
   * With the anchor at (1, 1, 1) and absolute position at (1, 1, 1) the relevant surrounding points for interpolation are:
   *  v1 (4, 2, 1)
   *  v2 (5, 2, 1)
   * -> write identical specific velocity at those locations in the external field
   * sampling the vertex (2, 0, 1) in lattice coordinates after the mesh movement should yield exactly that velocity
   * (the velocity also has to be rotated -30 deg around Z-axis)
   */


  Vec3<T> translation(2,1,0);
  Vec3<T> angles(0,0,-45.0*M_PI/180.0);
  Vec3<T> absolute_position(1, 1, 1);
  Vec3<T> absolute_orientation(0, 0, 0);

  Vec3<T> anchor(1, 1, 1);


  const int index1 = util::getCellIndex3D(4, 2, 1, eY+1, eZ+1);
  const int index2 = util::getCellIndex3D(5, 2, 1, eY+1, eZ+1);
  Vec3<T> expectedVel(1, 2, 3);

  for (int i = 0; i < 3; ++i) {
    field_data[i][index1] = expectedVel(i);
    field_data[i][index2] = expectedVel(i);
  }

  PredefinedExternalField<T, Lattice> externalField(field_data, eX+1, eY+1, eZ+1);
  BlockLatticeALE3D<T, Lattice, PredefinedExternalField<T, Lattice>> lattice(sizeXYZ + 1, sizeXYZ + 1, sizeXYZ + 1, anchor, &bulkDynamics, externalField);
  lattice.initDataArrays();

  for (int iX = 0; iX <= sizeXYZ; ++iX) {
    for (int iY = 0; iY <= sizeXYZ; ++iY) {
      for (int iZ = 0; iZ <= sizeXYZ; ++iZ) {
        T vel[] = {0.0, 0.0, 0.0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }
  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
  lattice.moveMesh(translation, angles, absolute_position, absolute_orientation);
  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();


  T result[3];
  T shit;
  lattice.get(2, 0, 1).computeRhoU(shit, result);

  Mat3<T> rot(angles, Rotation::ToBody{});
  Mat3<T> rot2(absolute_orientation, Rotation::ToSpace{});
  Affine3<T> velRotation(rot);
  velRotation.applyToRight(rot2);
  transformInPlace(expectedVel, velRotation);

  printf("RES: %f, %f, %f\n", result[0], result[1], result[2]);
  printf("EXP: %f, %f, %f\n", expectedVel(0), expectedVel(1), expectedVel(2));

  assert(approx_equal(result[0], expectedVel(0)));
  assert(approx_equal(result[1], expectedVel(1)));
  assert(approx_equal(result[2], expectedVel(2)));
}




void testWithRotatedInitialPose() {
  const int sizeXYZ = 2;
  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      sizeXYZ, 0.3 * 1.0 / std::sqrt(3), 20., 40., 0.0000146072, 1.225, 0);
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(1.5);

  int eX = 8;
  int eY = 4;
  int eZ = 4;
  int eSize = (eX+1)*(eY+1)*(eZ+1);
  T** field_data = new T *[Lattice<T>::d];
  for (int i = 0; i < Lattice<T>::d; ++i) {
    field_data[i] = new T[eSize];
  }

  for (int i = 0; i < eSize; ++i) {
    field_data[0][i] = 0.0;
    field_data[1][i] = 0.0;
    field_data[2][i] = 0.0;
  }

  /**
   * vertex of interest is v = (2,2,1) in lattice coordinates
   */

  Vec3<T> translation(3,2,0);
  Vec3<T> angles(0,0,M_PI_4);
  Vec3<T> absolute_position(-1, 3, 0);
  Vec3<T> absolute_orientation(0, 0, -M_PI_4);
  Vec3<T> anchor(1, 1, 1);

  /**
   * with this, v gets moved to v' = (5.414, 3, 1) and thus
   *    q11 = (5, 3, 1)
   *    q21 = (6, 3, 1)
   *    q12 = (5, 4, 1)
   *    q22 = (6, 4, 1)     (all lattice coordinates, the q variants for z coords are omitted)
   *
   * Those qs in global coordinates are then rounded to nearest integer to look up velocities in external field
   *     q11 = (3.24, 1.59, 0)  -> (3, 2, 0)
   *     q21 = (3.95, 0.87, 0)  -> (4, 1, 0)
   *     q12 = (3.95, 2.30, 0)  -> (4, 2, 0)
   *     q22 = (4.67, 1.59, 0)  -> (5, 2, 0)
   */


  const int index_q11 = util::getCellIndex3D(3, 2, 0, eY+1, eZ+1);
  const int index_q21 = util::getCellIndex3D(4, 1, 0, eY+1, eZ+1);
  const int index_q12 = util::getCellIndex3D(4, 2, 0, eY+1, eZ+1);
  const int index_q22 = util::getCellIndex3D(5, 2, 0, eY+1, eZ+1);
  Vec3<T> expectedVel(1, 2, 3);

  for (int i = 0; i < 3; ++i) {
    field_data[i][index_q11] = expectedVel(i);
    field_data[i][index_q21] = expectedVel(i);
    field_data[i][index_q12] = expectedVel(i);
    field_data[i][index_q22] = expectedVel(i);
  }

  PredefinedExternalField<T, Lattice> externalField(field_data, eX+1, eY+1, eZ+1);
  BlockLatticeALE3D<T, Lattice, PredefinedExternalField<T, Lattice>> lattice(sizeXYZ + 1, sizeXYZ + 1, sizeXYZ + 1, anchor, &bulkDynamics, externalField);
  lattice.initDataArrays();

  for (int iX = 0; iX <= sizeXYZ; ++iX) {
    for (int iY = 0; iY <= sizeXYZ; ++iY) {
      for (int iZ = 0; iZ <= sizeXYZ; ++iZ) {
        T vel[] = {0.0, 0.0, 0.0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }
  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
  lattice.moveMesh(translation, angles, absolute_position, absolute_orientation);
  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();


  T result[3];
  T shit;
  lattice.get(2, 2, 1).computeRhoU(shit, result);

  Mat3<T> rot(angles, Rotation::ToBody{});
  Mat3<T> rot2(absolute_orientation, Rotation::ToSpace{});
  Affine3<T> rotTrans(rot);
  rotTrans.applyToRight(rot2);
  transformInPlace(expectedVel, rotTrans);

  printf("RES: %f, %f, %f\n", result[0], result[1], result[2]);
  printf("EXP: %f, %f, %f\n", expectedVel(0), expectedVel(1), expectedVel(2));

  assert(approx_equal(result[0], expectedVel(0)));
  assert(approx_equal(result[1], expectedVel(1)));
  assert(approx_equal(result[2], expectedVel(2)));
}


void testTransforms() {
  Vec3<T> vertex(2, 1, 1);
  Vec3<T> anchor(1, 1, 1);


  const T factor = M_PI / 180.0;
//  Vec3<T> initial_position(1.3, -0.8, 02);
  Vec3<T> initial_position(0,0,0);
//  Vec3<T> initial_orientation(68. * factor, -15.0 * factor, 18.0 * factor);
  Vec3<T> initial_orientation(0,0,0);

  Vec3<T> translation(-0.38, -0.75, 1.5);
  Vec3<T> angles(-173 * factor, 52 * factor, 92 * factor);
//  Vec3<T> angles(0, 0, 0);

  Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
  Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

  transformInPlace(vertex, movementTransform);
  transformInPlace(vertex, externalTransform);

  std::cout << vertex << std::endl;

}

int main(int argc, char **argv) {
  testWithRotationMovement1();
  testWithRotationMovement2();
  testWithRotatedInitialPose();
  return 0;
}
