/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "fstream"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T>
bool approx_equal(T a, T b){
    const T eps = 1E-3;
    if (a == 0) return fabs(b) <= eps;
    if (b == 0) return fabs(a) <= eps;
    return fabs(a - b) / max(fabs(a), fabs(b)) <= eps;
}

void test() {

    const int sizeXY = 4;


    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
            sizeXY, 0.3 * 1.0 / std::sqrt(3), 20., 40., 0.0000146072, 1.225, 0);
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(1.5);

    /**
     * 0 velocity field
     */
    int eX = 8;
    int eY = 4;
    int eZ = 4;
    int eSize = (eX+1)*(eY+1)*(eZ+1);


    T** field_data = new T *[Lattice<T>::d];
    for (int i = 0; i < Lattice<T>::d; ++i) {
        field_data[i] = new T[eSize];
    }

    for (int i = 0; i < eSize; ++i) {
        field_data[0][i] = 8.0;
        field_data[1][i] = 8.0;
    }

    const int index1 = util::getCellIndex2D(4, 2,eY+1);
    const int index2 = util::getCellIndex2D(5, 2,eY+1);
    T expectedVel[2] = {1, 0};

    field_data[0][index1] = expectedVel[0];
    field_data[1][index1] = expectedVel[1];
    field_data[0][index2] = expectedVel[0];
    field_data[1][index2] = expectedVel[1];


    FieldVelocityProviderF<T, Lattice> fieldProvider(field_data, eX+1, eY+1);
    ExternalFieldALE<T, Lattice, FieldVelocityProviderF> fieldExternalField(fieldProvider);

    PointXd<T, Lattice> lattice_anchor {0, 0};

    BlockLatticeALE2D<T, Lattice, ExternalFieldALE<T, Lattice, FieldVelocityProviderF>> 
    lattice(sizeXY + 1, sizeXY + 1, lattice_anchor, &bulkDynamics, fieldExternalField);

    lattice.initDataArrays();

    // setBoundaryValues

    for (int iX = 0; iX <= sizeXY; ++iX) {
        for (int iY = 0; iY <= sizeXY; ++iY) {
            T vel[] = {0.0, 0.0};
            lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
            lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
        }
    }
    
//    PoseXd<T, Lattice> movement {{2,0}, {22.5 * M_PI/180.0}};
    PoseXd<T, Lattice> movement {{2,0}, {0 * M_PI/180.0}};
    PoseXd<T, Lattice> lattice_pose {{0, 0}, {30 * M_PI/180.0}};


    lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    lattice.moveMesh(movement, lattice_pose);
    lattice_pose += movement.position_;
    std::cout << lattice_pose << std::endl;
    lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();


    T result[2];
    T shit;
    lattice.get(4, 0).computeRhoU(shit, result);

    util::rotateToBodySystem2D(expectedVel, lattice_pose.orientation_);
    util::rotateToBodySystem2D(expectedVel, movement.orientation_);

    printf("RES: %f, %f\n", result[0], result[1]);
    printf("EXP: %f, %f\n", expectedVel[0], expectedVel[1]);

    assert(approx_equal(result[0], expectedVel[0]));
    assert(approx_equal(result[1], expectedVel[1]));

}



int main(int argc, char **argv) {
    test();
    return 0;
}
