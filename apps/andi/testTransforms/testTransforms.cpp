#include "olb3D.h"
#include "olb3D.hh"   // include full template code


#include <cmath>
#include <iostream>
typedef double T;

template <typename T> bool approx_equal(T a, T b) {
  const T eps = 1E-3;
  if (a == 0)
    return fabs(b) <= eps;
  if (b == 0)
    return fabs(a) <= eps;
  return fabs(a - b) / max(fabs(a), fabs(b)) <= eps;
}

template <typename T, size_t Dim>
bool is_equal(const Vec<T, Dim> &lhs, const Vec<T, Dim> &rhs) {
  bool result = true;
  for (int i = 0; i < Dim; ++i) {
    result = result && approx_equal(lhs(i), rhs(i));
  }
  return result;
}

void testTransforms() {
  const T rad = M_PI / 180.0;
  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0, 0, 0);
    Vec3<T> initial_orientation(0, 0, 0);

    Vec3<T> translation(-0.38, -0.75, 1.5);
    Vec3<T> angles(-173 * rad, 52 * rad, 92 * rad);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(-0.40149, 0.24529, 1.4055);
    assert(is_equal(vertex, expected));
  }

  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0,0,0);
    Vec3<T> initial_orientation(30.0*rad, 25.0*rad, -10.0*rad);

    Vec3<T> translation(0,0,0);
    Vec3<T> angles(0,0,0);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(0.89254, -0.15738, -0.42262);
    assert(is_equal(vertex, expected));
  }

  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0.3, 0.4, -0.9);
    Vec3<T> initial_orientation(30.0*rad, 25.0*rad, -10.0*rad);

    Vec3<T> translation(0,0,0);
    Vec3<T> angles(0,0,0);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(1.19254, 0.24262, -1.32262);
    assert(is_equal(vertex, expected));
  }

  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0.3, 0.4, -0.9);
    Vec3<T> initial_orientation(30.0*rad, 25.0*rad, -10.0*rad);

    Vec3<T> translation(1.16045,0,0);
    Vec3<T> angles(0,0,0);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(2.22828, 0.05999, -1.81304);
    assert(is_equal(vertex, expected));
  }

  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0.3, 0.4, -0.9);
    Vec3<T> initial_orientation(30.0*rad, 25.0*rad, -10.0*rad);

    Vec3<T> translation(1.16045,0,-0.40351);
    Vec3<T> angles(0,0,0);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(2.11788, 0.2843, -2.12975);
    assert(is_equal(vertex, expected));
  }

  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0.3, 0.4, -0.9);
    Vec3<T> initial_orientation(30.0*rad, 25.0*rad, -10.0*rad);

    Vec3<T> translation(1.16045,0,-1.35638);
    Vec3<T> angles(0,0,0);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(1.85716, 0.81408, -2.87765);
    assert(is_equal(vertex, expected));
  }

  {
    Vec3<T> vertex(2, 1, 1);
    Vec3<T> anchor(1, 1, 1);
    Vec3<T> initial_position(0.3, 0.4, -0.9);
    Vec3<T> initial_orientation(30.0*rad, 25.0*rad, -10.0*rad);

    Vec3<T> translation(1.16045,0.44795,-1.35638);
    Vec3<T> angles(0,0,0);

    Affine3<T> movementTransform = aleutil::movementTransform(translation, angles, anchor);
    Affine3<T> externalTransform = aleutil::positionInExternalFieldTransform(initial_position, initial_orientation, anchor);

    transformInPlace(vertex, movementTransform);
    transformInPlace(vertex, externalTransform);

    Vec3<T> expected(2.01774, 1.17969, -2.67466);
    assert(is_equal(vertex, expected));
  }
}


int main(int argc, char *argv[]) {
  testTransforms();
}