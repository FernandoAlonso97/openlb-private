#include <chrono>

#include "olb2D.h"
#include "olb3D.h"

#ifndef OLB_PRECOMPILED // Unless precompiled version is used,

#include "olb2D.hh"   // include full template code
#include "olb3D.hh"   // include full template code

#endif

#include <cmath>
#include <iostream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

#define DESCRIPTOR ForcedD2Q9Descriptor
#define DESCRIPTOR_3D ForcedD3Q19Descriptor
typedef double T;


template<typename T>
bool approx_equal(T a, T b){
    const T eps = 1E-3;
    if (a == 0) return fabs(b) <= eps;
    if (b == 0) return fabs(a) <= eps;
    return fabs(a - b) / max(fabs(a), fabs(b)) <= eps;
}

template<typename T, template<typename U> class Lattice>
void testInterpolation() {
    const int q = Lattice<T>::q;

    T result[q] = {0};
    T** target = new T *[q];
    for (int i = 0; i < q; ++i) {
        target[i] = new T[1];
    }

    T ones[q] = {1};
    T twos[q] = {2};
    T threes[q] = {3};
    T fours[q] = {4};


    T a = 0;
    T b = 0;
    T c = 0;


    /**
     * bilinear interpolation
     */

    std::cout << "testing bilinear interpolation...\t";

    a = 0;
    b = 0;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == ones[0]);

    a = 1;
    b = 0;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == twos[0]);

    a = 0;
    b = 1;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == threes[0]);

    a = 1;
    b = 1;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == fours[0]);

    a = 0.5;
    b = 0;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == 1.5);

    a = 0.5;
    b = 1;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == 3.5);

    a = 0;
    b = 0.5;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == 2.);

    a = 1;
    b = 0.5;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == 3.);

    a = 0.5;
    b = 0.5;
    util::bilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, a, b);
    assert(target[0][0] == 2.5);

    std::cout << "fine." << std::endl;

    /**
     * trilinear interpolation
     */

    std::cout << "testing trilinear interpolation...\t";

    a = 0;
    b = 0;
    c = 0;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == ones[0]);

    a = 1;
    b = 0;
    c = 0;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == twos[0]);

    a = 0;
    b = 1;
    c = 0;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == threes[0]);

    a = 1;
    b = 1;
    c = 0;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == fours[0]);


    a = 0;
    b = 0;
    c = 1;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == fours[0]);

    a = 1;
    b = 0;
    c = 1;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == threes[0]);

    a = 0;
    b = 1;
    c = 1;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == twos[0]);

    a = 1;
    b = 1;
    c = 1;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == ones[0]);


    a = 0.5;
    b = 0.5;
    c = 0.5;
    util::trilinearInterpolate<T, Lattice>(target, 0, ones, twos, threes, fours, fours, threes, twos, ones, a, b, c);
    assert(target[0][0] == 2.5);

    std::cout << "fine." << std::endl;
}


template<typename T, template<typename U> class Lattice>
void testTransformPoint2D() {
    std::cout << "testing 2D transform...\t";

    {
        PointXd<T, Lattice> v   {0.0, 1.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, 0.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        T eX = 0.0;
        T eY = 1.0;
        assert(v.getX() == eX && v.getY() == eY);
    }

    {
        T rotdeg = 90.0; //ccw

        PointXd<T, Lattice> v   {0.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        T eX = 1.0;
        T eY = 0.0;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        
        assert(v.getX() == eX && v.getY() == eY);
    }

    {
        T rotdeg = 180.0;

        PointXd<T, Lattice> v   {0.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        T eX = 1.0;
        T eY = 1.0;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(v.getX() == eX && v.getY() == eY);
    }

    {
        T rotdeg = 270.0;

        PointXd<T, Lattice> v   {0.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        T eX = 0.0;
        T eY = 1.0;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        // printf("\nExpect: %f, %f\nResult: %f, %f\n", eX, eY, v.getX(), v.getY());
        assert(approx_equal(v.getX(), eX) && v.getY() == eY);
    }

    {
        T rotdeg = 45.0;

        PointXd<T, Lattice> v   {0.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        T eX = 0.5;
        T eY = -0.207107;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX) && approx_equal(v.getY(), eY));
    }

    {
        T rotdeg = 45.0;

        PointXd<T, Lattice> v   {0.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.5, 1.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        T eX = 1.0;
        T eY = 0.792893;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX) && approx_equal(v.getY(), eY));
    }

    {
        T rotdeg = 45.0;

        PointXd<T, Lattice> v   {0.0, 0.0};
        PoseXd<T, Lattice>  mov {{-1.5, -1.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.5, 0.5};

        T eX = -1.0;
        T eY = -1.207107;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX) && approx_equal(v.getY(), eY));
    }

    {
        T rotdeg = 45.0;

        PointXd<T, Lattice> v   {1.0, 1.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.0, 0.0};

        T eX = 0.0;
        T eY = 1.414214;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX) && approx_equal(v.getY(), eY));
    }

    {
        T rotdeg = 360.0;

        PointXd<T, Lattice> v   {1.0, 1.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.0, 0.0};

        T eX = 1.0;
        T eY = 1.0;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX) && approx_equal(v.getY(), eY));
    }

    {
        T rotdeg = -45.0;

        PointXd<T, Lattice> v   {1.0, 1.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0}, rotdeg * M_PI / 180.0};
        PointXd<T, Lattice> anc {0.0, 0.0};

        T eX = 1.414214;
        T eY = 0.0;

        util::transformPoint2D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX) && approx_equal(v.getY(), eY));
    }


    std::cout << "fine." << std::endl;
}


template<typename T, template<typename U> class Lattice>
void testTransformPoint3D() {
    std::cout << "testing 3D transform...\t";

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {0.0, 0.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.0, 0.0, 0.0};

        T eX = 0.0;
        T eY = 0.0;
        T eZ = 0.0;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {0.0, 0.0, 0.0};
        PoseXd<T, Lattice>  mov {{1.5, 1.5, 1.5}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 1.5;
        T eY = 1.5;
        T eZ = 1.5;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {0.0, 0.0, 0.0};
        PoseXd<T, Lattice>  mov {{-1.5, -1.5, -1.5}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = -1.5;
        T eY = -1.5;
        T eZ = -1.5;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 45.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 1.0;
        T eY = 1.20711;
        T eZ = 0.5;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 45.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.5;
        T eY = 1.0;
        T eZ = -0.20711;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.5;
        T eY = 1.20711;
        T eZ = 0.0;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {4.0, 0.0, 2.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {2.0, 2.0, 2.0};

        T eX = 4.828;
        T eY = 2.0;
        T eZ = 2.0;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));


    }

    {
        T phi = 45.0 * M_PI / 180.0;    // x axis (roll)
        T the = 45.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.85355;
        T eY = 1.20711;
        T eZ = 0.14645;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 45.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.35355;
        T eY = 1.35355;
        T eZ = 0.5;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 45.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.14645;
        T eY = 0.85355;
        T eZ = -0.20711;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 45.0 * M_PI / 180.0;    // x axis (roll)
        T the = 45.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.25;
        T eY = 1.25;
        T eZ = 0.14645;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 45.0 * M_PI / 180.0;    // x axis (roll)
        T the = 45.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.0};
        PoseXd<T, Lattice>  mov {{-0.25, -1.25, -0.14645}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.0;
        T eY = 0.0;
        T eZ = 0.0;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 30.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.5};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.683013;
        T eY = 1.183013;
        T eZ = 0.5;

       
        util::transformPoint3D<T, Lattice>(v, mov, anc);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 15.0 * M_PI / 180.0;    // x axis (roll)
        T the = 30.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice>         v   {0.0, 0.0, 1.0};
        OrientationXd<T, Lattice>   rot {phi, the, psi};

        T eX = 0.0;
        T eY = 0.0;
        T eZ = 1.0;

        util::rotateToInertiaSystem3D<T, Lattice>(v, rot);
        util::rotateToBodySystem3D<T, Lattice>(v, rot);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 45.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice>         v   {2.0, -2.0, 0.0};
        OrientationXd<T, Lattice>   rot {phi, the, psi};

        T eX = 2.828;
        T eY = 0.0;
        T eZ = 0.0;

        util::rotateToInertiaSystem3D<T, Lattice>(v, rot);

        printf("\nExpect: %f, %f, %f\nResult: %f, %f, %f\n", eX, eY, eZ, v.getX(), v.getY(), v.getZ());
        // util::rotateToBodySystem3D<T, Lattice>(v, rot);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 380.0 * M_PI / 180.0;    // x axis (roll)
        T the = 123.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 987.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice>         v   {1.0, 1.0, 1.0};
        OrientationXd<T, Lattice>   rot {phi, the, psi};

        T eX = 1.0;
        T eY = 1.0;
        T eZ = 1.0;

        util::rotateToInertiaSystem3D<T, Lattice>(v, rot);
        util::rotateToBodySystem3D<T, Lattice>(v, rot);
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }


    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.03 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.5};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.683013;
        T eY = 1.183013;
        T eZ = 0.5;

        for (int i = 0; i < 1000; ++i)
        {
            util::transformPoint3D<T, Lattice>(v, mov, anc);
        }

        
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 15.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.5};
        PoseXd<T, Lattice>  mov {{1.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.853553 + 1;
        T eY = 1.112372;
        T eZ = 0.5;

        util::transformPoint3D<T, Lattice>(v, mov, anc);

        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));

        eX = 1.683013;
        eY = 1.183013;
        eZ = 0.5;
        util::transformPoint3D<T, Lattice>(v, {{0.0, 0.0, 0.0}, {0.0, 0.0, psi}}, {1.5,0.5,0.5});

        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 0.0 * M_PI / 180.0;    // x axis (roll)
        T the = 0.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 0.03 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.5};
        PoseXd<T, Lattice>  mov {{0.0513, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.683013 + 1000 * 0.0513;
        T eY = 1.183013;
        T eZ = 0.5;

        for (int i = 0; i < 1000; ++i)
        {
            util::transformPoint3D<T, Lattice>(v, mov, anc);
            anc += mov.position_;
        }
        // printf("\nExpect: %f, %f, %f\nResult: %f, %f, %f\n", eX, eY, eZ, v.getX(), v.getY(), v.getZ());
        
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    {
        T phi = 64.0 * M_PI / 180.0;    // x axis (roll)
        T the = 104.0 * M_PI / 180.0;    // y axis (pitch)
        T psi = 8.0 * M_PI / 180.0;    // z axis (yaw)

        PointXd<T, Lattice> v   {1.0, 1.0, 0.5};
        PoseXd<T, Lattice>  mov {{0.0, 0.0, 0.0}, {phi, the, psi}};
        PointXd<T, Lattice> anc {0.5, 0.5, 0.5};

        T eX = 0.781516;
        T eY = 0.760904;
        T eZ = -0.09387;

        util::transformPoint3D<T, Lattice>(v, mov, anc);
        // printf("\nExpect: %f, %f, %f\nResult: %f, %f, %f\n", eX, eY, eZ, v.getX(), v.getY(), v.getZ());
        
        assert(approx_equal(v.getX(), eX));
        assert(approx_equal(v.getY(), eY));
        assert(approx_equal(v.getZ(), eZ));
    }

    std::cout << "fine." << std::endl;
}

int main(int argc, char *argv[]) {
    testInterpolation<T, DESCRIPTOR>();
    testTransformPoint2D<T, DESCRIPTOR>();
    testTransformPoint3D<T, DESCRIPTOR_3D>();
}
