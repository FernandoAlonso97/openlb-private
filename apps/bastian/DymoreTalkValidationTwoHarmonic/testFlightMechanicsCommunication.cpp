/*  This file is part of the OpenLB library *
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, class RotorCellData>
__global__ void scaleThrust(T const factor, RotorCellData rotorCellData, T** cellData)
{
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= rotorCellData._numberRotorCells)
      return;

    cellData[Lattice<T>::forceIndex+2][rotorCellData._cellIndexGPU[threadIndex]] *= factor;
}

template<typename Data>
void zeroSendData(Data& sendData) {
	// sendData.noHarmonicsRadial  = 1;
	// sendData.noHarmonicsAzimuth = 1;

	 // std::memset(sendData.ccX,0,sizeof(float)*25*9);
	 // std::memset(sendData.csX,0,sizeof(float)*25*9);
	 // std::memset(sendData.scX,0,sizeof(float)*25*9);
	 // std::memset(sendData.ssX,0,sizeof(float)*25*9);

	 // std::memset(sendData.ccY,0,sizeof(float)*25*9);
	 // std::memset(sendData.csY,0,sizeof(float)*25*9);
	 // std::memset(sendData.scY,0,sizeof(float)*25*9);
	 // std::memset(sendData.ssY,0,sizeof(float)*25*9);

	 // std::memset(sendData.ccZ,0,sizeof(float)*25*9);
	 // std::memset(sendData.csZ,0,sizeof(float)*25*9);
	 // std::memset(sendData.scZ,0,sizeof(float)*25*9);
	 // std::memset(sendData.ssZ,0,sizeof(float)*25*9);

	 // sendData.scalingFactor = 1;

	         for(unsigned int iRad = 0; iRad < sendData.noHarmonicsRadial; ++iRad)
          for(unsigned int iAz = 0; iAz < sendData.noHarmonicsAzimuth; ++iAz) {
              const unsigned int index = sendData.getIndex(iRad,iAz);
       sendData.ccX[index] = 0;
       sendData.csX[index] = 0;
       sendData.scX[index] = 0;
       sendData.ssX[index] = 0;

       sendData.ccY[index]  = 0;
       sendData.csY[index]  = 0;
       sendData.scY[index]  = 0;
       sendData.ssY[index]  = 0;

       sendData.ccZ[index] = 0;
       sendData.csZ[index] = 0;
       sendData.scZ[index] = 0;
       sendData.ssZ[index] = 0;
          }
}



template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;


    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);


    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

void MultipleSteps(unsigned int resolution, double simTime, std::string filename, long int port)
{
  constexpr unsigned int nbHarmRadial = 25;
  constexpr unsigned int nbHarmAzi = 5;
  constexpr unsigned int nbHarm = 20;
  using CouplingType = Coupling<T,TwoHarmonicThrust<T,Lattice<T>>,TwoHarmonicVelocity<T,Lattice<T>,nbHarmRadial,nbHarmAzi>>;
//  using CouplingType = Coupling<T,TwoHarmonicThrust<T,Lattice<T>>,TwoHarmonicVelocity<T,Lattice<T>,nbHarmRadial,nbHarmAzi>>;
//  using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;
  using ReceiveDataGensim = CouplingType::ReceiveDataType;
  using SendDataGensim    = CouplingType::SendDataType;
//  using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
//  using SendDataGensim = VelocityAtRotorsLinear;


  int iXLeftBorder = 0;
  int iXRightBorder = (resolution+1)*1.5-1;
  int iYBottomBorder = 0;
  int iYTopBorder   = (resolution+1)*1.5-1;
  int iZFrontBorder = 0;
  int iZBackBorder  = (resolution+1)*1.5-1;

  T const rotorRadius = 2.32;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,6.*rotorRadius
          ,30.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();
  T cellSize = spacing*spacing;

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";


  ConstExternalField<T,Lattice> externalFieldClass({0.,0.,0.});

  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1,
          &bulkDynamics);

  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {static_cast<T>(iXRightBorder/2.),static_cast<T>(iYTopBorder/2.),static_cast<T>((iZBackBorder+1)/2.)};


  Rotor<T> rotor(rotorRadius,0.207,rotorPosition,spacing);
  RotorCellData<T> rotorCellData(rotor,lattice, converter);
  RelaxationData<T> relaxationData(0.7);

  Rotor<T> rotorInflow(rotorRadius*1.2,0.,rotorPosition,spacing);
  SquareCellData<T> squareCellDataInflow(rotorInflow,lattice,converter);
  RotorCellData<T> rotorCellDataInflow(rotorInflow,lattice, converter);
//  std::tuple<RotorCellData<T>,unsigned int, unsigned int,T> inflowCplCnstr(rotorCellDataInflow,nbHarmRadial,nbHarmAzi,1.2);
  std::tuple<Rotor<T>,SquareCellData<T>,unsigned int,unsigned int, T, T, unsigned int, unsigned int,bool> inflowCplCnstr(rotorInflow,
          squareCellDataInflow,nbHarmRadial,nbHarmAzi,1.2,spacing,200,720,false);

  std::cout << rotorCellData._numberFullRotorCells << "," << rotorCellDataInflow._numberFullRotorCells << std::endl;

  CouplingType::CouplingDataType couplingData(cellSize,inflowCplCnstr);
//
//  CouplingType::CouplingDataType couplingData(rotorCellData);

  const T rootCutL = rotor.getHubRadius()*rotor.getRadiusUndim();
  std::cout << "Root cut: " << rootCutL << std::endl;
  static NoDynamics<T,Lattice> noDynamics;
  static PostProcessingDynamics<T,Lattice,CurvedSlipBoundaryProcessor3D<T,Lattice>> slipBoundary;

  auto rotorCenter = rotor.getCenter();
  for(unsigned int iX = 1; iX < lattice.getNx()-1; ++iX) {
  //      for(unsigned int iY = static_cast<int>(std::ceil(rotorCenter[1])); iY < lattice.getNy(); ++iY) {
          for(unsigned int iY = 1; iY < lattice.getNy()-1; ++iY) {
              for(unsigned int iZ = 1; iZ < lattice.getNz()-1; ++iZ) {
                  size_t index = util::getCellIndex3D(iX,iY,iZ,lattice.getNy(),lattice.getNz());
                  T radius = std::sqrt(std::pow(iX-rotorCenter[0],2)+std::pow(iY-rotorCenter[1],2)+std::pow(iZ-rotorCenter[2],2));
                  if(radius <= rootCutL) {
                    lattice.defineDynamics(iX,iY,iZ,&noDynamics);
                  }
                  else {
                    const T p[3] = {iX,iY,iZ};
                    unsigned int nNeighbor = 0;
                    for(unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                        const T neighbor[3] = {p[0]+Lattice<T>::c(iPop,0),p[1]+Lattice<T>::c(iPop,1),p[2]+Lattice<T>::c(iPop,2)};
                        const T radNeigbor = std::sqrt(std::pow(neighbor[0]-rotorCenter[0],2)+
                                std::pow(neighbor[1]-rotorCenter[1],2)+std::pow(neighbor[2]-rotorCenter[2],2));
                        if(radNeigbor <= rootCutL) {
                            lattice.defineDynamics(iX,iY,iZ,&slipBoundary);
                            break;
                        }
                    }
                  }
              }
        }
    }

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;

    auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
    auto slipCellIds = slipDataHandler->getCellIDs();
    auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();

    for(size_t index : slipDataHandler->getCellIDs()) {
       size_t p[3];
       util::getCellIndices3D(index,lattice.getNy(),lattice.getNz(),p);

       size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

       size_t nNeighbor = 0;
       int normal[3] = {0,0,0};

       for(unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
           size_t pN[Lattice<T>::d] = {p[0]+Lattice<T>::c(iPop,0),
                   p[1]+Lattice<T>::c(iPop,1),
                   p[2]+Lattice<T>::c(iPop,2) };
           size_t nIndex = util::getCellIndex3D(pN[0],pN[1],pN[2],lattice.getNy(),lattice.getNz());


           if(lattice.getFluidMask()[nIndex] == false) {
               T dir[3] = {Lattice<T>::c(iPop,0),Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2)};
               T delta = 0.25;

               for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                   normal[iDim] += Lattice<T>::c(iPop,iDim);

               size_t const idxDir = CurvedSlipBoundaryProcessor3D<T,Lattice>::idxDir(nNeighbor);
               size_t const idxDelta = CurvedSlipBoundaryProcessor3D<T,Lattice>::idxDelta(nNeighbor);
               slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
               slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

               slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxTau()][momentaIndex] =
                                       converter.getLatticeRelaxationTime();
               ++nNeighbor;
           }
       }

       T normalAbs = 0;
       for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
           normalAbs += normal[iDim]*normal[iDim];
       normalAbs = std::sqrt(normalAbs);


       for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
           slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNormal()+iDim][momentaIndex] = -normal[iDim]/normalAbs;


       slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNumNeighbour()][momentaIndex] = (T) nNeighbor;
    }

//  for(unsigned int iCell = 0; iCell< rotorCellData._numberRotorCells; ++iCell)
//      if(rotorCellData._cellPositionRadialCPU[iCell] < rotor.getHubRadius())
//      {
//          size_t position[3];
//          util::getCellIndices3D(rotorCellData._cellIndexCPU[iCell],lattice.getNy(),lattice.getNz(),position);
//          lattice.defineDynamics(position[0],position[1],position[2],&instances::getBounceBack<T,Lattice>());
//      }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::cout << "Rotor Position: " << rotor.getCenter()[0] << "," << rotor.getCenter()[1] << "," << rotor.getCenter()[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotor.getXLimits()[0] << " to " << rotor.getXLimits()[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotor.getYLimits()[0] << " to " << rotor.getYLimits()[1] << std::endl;

  std::cout << "Number of theoretical rotor cells: " << rotor.getNumberRotorCells() << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::cout << "Undim: " << converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea() << std::endl;

  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., converter.getLatticeVelocity(0.)};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string name;
  std::string directory = "/home/bhorvat6/outputOpenLB/TwoHarmonicHover/";
  name = filename+"_X";
  name += std::to_string(iXRightBorder+1);

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> maskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriterTrim( name );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(maskFunctor);

  std::vector<size_t> writeLimits{static_cast<size_t>(0.25*iXRightBorder),static_cast<size_t>(0.75*iXRightBorder),static_cast<size_t>(0.25*iYTopBorder),
	                              static_cast<size_t>(0.75*iYTopBorder),static_cast<size_t>(0.25*iZBackBorder),static_cast<size_t>(0.9*iZBackBorder)};

  // vtkWriterTrim.write(0);

  std::cout << "Undim: " << rotorCellData._undimThrust << ", " << converter.getLatticePressure(1) << std::endl;
  ReceiveDataGensim thrustData;
  SendDataGensim dataSend;
  NetworkInterfaceTCP<ReceiveDataGensim,SendDataGensim> networkCommunicator(port,OUTPUTIP,port,NETWORKBUFFERSIZE,true);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  std::ofstream coeffs(filename+"_coeffsV.txt");
  coeffs << "step,cc,cs,sc,ss,iRad,iPsi" << std::endl;

  std::ofstream coeff(filename+"_coeffsT.txt");
  coeff << "step,cc,cs,sc,ss,iRad,iPsi" << std::endl;

  std::ofstream inflowInterp(filename+"_inflowInterp.txt");
  inflowInterp << "step,iR,iAz,v" << std::endl;
  
  zeroSendData(dataSend);

  for(unsigned int time = 1; time <= converter.getLatticeTime(simTime)+10; ++time) {
      networkCommunicator.recieve_data(thrustData);

      // std::cout << "Thrust: " << thrustData.AbsThrust[2] << std::endl;

	  CouplingType::writeReceiveData(thrustData,lattice.getDataGPU(),rotorCellData,couplingData.getWriteData());
      double totalThrust = thrust::reduce(thrust::device,&lattice.getDataGPU()[Lattice<T>::forceIndex+2][0],
              &lattice.getDataGPU()[Lattice<T>::forceIndex+2][lattice.getNCells()-1]);
      totalThrust = converter.getPhysForce(totalThrust);

      // std::cout << "Total thrust: " << totalThrust << std::endl;
	  
	  double factor = 1.;
	  if(time > 10) {
		  factor = -thrustData.AbsThrust[2]/totalThrust;
		  scaleThrust<T><<<rotorCellData._numberRotorCells/256+1,256>>>(factor,rotorCellData,lattice.getDataGPU());

	      totalThrust = thrust::reduce(thrust::device,&lattice.getDataGPU()[Lattice<T>::forceIndex+2][0],
              &lattice.getDataGPU()[Lattice<T>::forceIndex+2][lattice.getNCells()-1]);
    	  totalThrust = converter.getPhysForce(totalThrust);
	  }

	  lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
      HANDLE_ERROR(cudaGetLastError());

      if( (time%(converter.getLatticeTime(0.05)) == 0)) 
	  {
		   lattice.copyDataToCPU();
		  vtkWriterTrim.write(writeLimits,time);
		  // vtkWriterTrim.write(12345678);
	  }

//      CouplingType::readSendData(dataSend,lattice.getDataGPU(),rotorCellData,couplingData.getReadData());
	  CouplingType::readSendData(dataSend,lattice.getDataGPU(),squareCellDataInflow,couplingData.getReadData());

	  // dataSend.ccZ[0] = 7.;
	  // dataSend.scalingFactor = 1.2;

			  // for(unsigned int iRad = 0; iRad < dataSend.noHarmonicsRadial; ++iRad)
          // for(unsigned int iAz = 0; iAz < dataSend.noHarmonicsAzimuth; ++iAz) {
              // const unsigned int index = dataSend.getIndex(iRad,iAz);


			  // std::cout << dataSend.scalingFactor << "\n";
			  // std::cout << dataSend.ccX[index] << " ";
        // std::cout << dataSend.csX[index] << " ";
        // std::cout << dataSend.scX[index] << " ";
        // std::cout << dataSend.ssX[index] << "\n";

        // std::cout << dataSend.ccY[index] << " ";
        // std::cout << dataSend.csY[index] << " ";
        // std::cout << dataSend.scY[index] << " ";
        // std::cout << dataSend.ssY[index] << "\n";

        // std::cout << dataSend.ccZ[index] << " ";
        // std::cout << dataSend.csZ[index] << " ";
        // std::cout << dataSend.scZ[index] << " ";
        // std::cout << dataSend.ssZ[index] << "\n\n";
		  // }

      networkCommunicator.send_data(dataSend);
      if( (time%converter.getLatticeTime(.1)) == 0) {
	     std::cout << "Total thrust: " << totalThrust << "(" << factor << ")\n";
         std::cout << "Dymore thrust: " << thrustData.AbsThrust[2] << "\n";
         std::cout << "Simulation time: " << converter.getPhysTime(time) << "\n\n\n";
	  }

      if( (time%converter.getLatticeTime(.05)) == 0)
      {
         std::cout << "Simulation time: " << converter.getPhysTime(time) << std::endl;
		 for(unsigned int iRad = 0; iRad<thrustData.noHarmonicsRadial; ++iRad)
			 for(unsigned int iAz = 0; iAz<thrustData.noHarmonicsAzimuth; ++iAz)
			 {
				 unsigned int index = thrustData.getIndex(iRad,iAz);
				 coeff << time << "," << thrustData.cc[index] << "," << thrustData.cs[index] << "," << thrustData.sc[index] << "," << thrustData.ss[index] <<
					 "," << iRad << "," << iAz << std::endl;
			 }

		 for(unsigned int iRad = 0; iRad < dataSend.noHarmonicsRadial; ++iRad)
			 for(unsigned int iAz = 0; iAz < dataSend.noHarmonicsAzimuth; ++iAz) {
				 coeffs << time << "," << dataSend.ccZ[dataSend.getIndex(iRad,iAz)] << "," << dataSend.csZ[dataSend.getIndex(iRad,iAz)] << ","
					 << dataSend.scZ[dataSend.getIndex(iRad,iAz)] << "," << dataSend.ssZ[dataSend.getIndex(iRad,iAz)] << ","
					 << iRad << "," << iAz << std::endl;
		 	}

		  // for(unsigned int iR = 0; iR < 300; ++iR) {
			  // for(unsigned int iAz = 0; iAz < 720; ++iAz) {
				  // inflowInterp << time << "," << iR/(300.-1.) << "," << iAz*M_PI*2./(720.-1.) << "," << couplingData.getReadData().h_inflowInterpVector[util::getCellIndex2D(iR,iAz,720)] << std::endl;
				// }
			// }
	  }
  }


  std::cout << "Simulation finished\n";


}

int main(int argc, char** argv)
{
    double simTime = 60;
	int c = 0;
	std::string filename;
	unsigned int port = 0;
	unsigned int resolution = 0;
	while ((c = getopt(argc, argv, "f:p:t:r:")) != -1){
		switch(c) {
			case 'f':
				filename.assign(optarg);
				break;
			case 'p':
				port = strtol(optarg,NULL,10);
				break;
			case 't':
				simTime = strtof(optarg,NULL);
				break;
			case 'r':
				resolution = strtol(optarg,NULL,10);
				break;
		}
	}

	std::cout << "Simulation: " << filename << " at port " << port << " with runtime " << simTime << " seconds" << std::endl;
    MultipleSteps(resolution,simTime,filename,port);
    return 0;
}
