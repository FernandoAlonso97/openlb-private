/*  This file is part of the OpenLB library *
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;
#include <iostream>
#include "contrib/MemorySpace/memory_spaces.h"

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"

#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/mpiCommunication.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#include <cmath>

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

template<typename T, class RotorCellData>
__global__ void scaleThrust(T const factor, RotorCellData rotorCellData, T** cellData)
{
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= rotorCellData._numberRotorCells)
      return;

    cellData[Lattice<T>::forceIndex+2][rotorCellData._cellIndexGPU[threadIndex]] *= factor;
}

template<typename T, class RotorCellData>
__global__ void writeThrustLocal(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		RotorCellData rotorCellData, T* thrust)
{
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= rotorCellData._numberRotorCells)
      return;

    size_t cellIndex = rotorCellData._cellIndexGPU[threadIndex];
	cellData[Lattice<T>::forceIndex+2][cellIndex] = thrust[threadIndex]*rotorCellData._cellWeightGPU[threadIndex];
}

template<typename T>
__global__ void printKernel(T* inflowRead, size_t nCells)
{
	const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
	const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
						 + blockIndex * blockDim.x * blockDim.y * blockDim.z;

	if (threadIndex >= nCells)
	  return;

	printf("%lf,",inflowRead[threadIndex]);
}

template<typename T, typename Lattice>
class InitEquilibrium {
public:
	InitEquilibrium(){};
	OPENLB_HOST_DEVICE
	void operator()(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
			size_t index, T rho, T v0, T v1, T v2, T uSqr) {

		T velocity[3] = {v0,v1,v2};
		for(unsigned int iPop = 0; iPop < Lattice::q; ++iPop)
			cellData[iPop][index] = lbDynamicsHelpers<T,Lattice>::equilibrium(iPop, rho, velocity, uSqr);
	}

};

template<typename Lattice>
class ThrustTableLookup {
	ThrustTableLookup() = default;
	template<typename T>
	OPENLB_HOST_DEVICE
	void operator()(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
			size_t index, RotorCellData<T> rotorCellData, float omega, float const * const thrust) {

		size_t cellIndex = rotorCellData._cellIndexGPU[index];
		cellData[Lattice::forceIndex+2][cellIndex] = thrust[index];
	}
};

template<typename Data>
void zeroSendData(Data& sendData) {

	for(unsigned int iRad = 0; iRad < sendData.noHarmonicsRadial; ++iRad)
	  for(unsigned int iAz = 0; iAz < sendData.noHarmonicsAzimuth; ++iAz) {
	   const unsigned int index = sendData.getIndex(iRad,iAz);
       sendData.ccX[index] = 0;
       sendData.csX[index] = 0;
       sendData.scX[index] = 0;
       sendData.ssX[index] = 0;

       sendData.ccY[index]  = 0;
       sendData.csY[index]  = 0;
       sendData.scY[index]  = 0;
       sendData.ssY[index]  = 0;

       sendData.ccZ[index] = 0;
       sendData.csZ[index] = 0;
       sendData.scZ[index] = 0;
       sendData.ssZ[index] = 0;
          }
}



template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, const SubDomainInformation<T,Lattice<T>>& domainInfo, const SubDomainInformation<T,Lattice<T>>& refDomain)
{
    int iXLeftBorder   = refDomain.globalIndexStart[0];;
    int iXRightBorder  = refDomain.globalIndexEnd[0]-1;;
    int iYBottomBorder = refDomain.globalIndexStart[1];;
    int iYTopBorder    = refDomain.globalIndexEnd[1]-1;;
    int iZFrontBorder  = refDomain.globalIndexStart[2];;
    int iZBackBorder   = refDomain.globalIndexEnd[2]-1;;

    T omega = dynamics.getOmega();

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P(omega,0.7);

    for(unsigned int iY = iYBottomBorder+1; iY <= iYTopBorder-1; ++iY)
    	for(unsigned int iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; ++iZ) {
    	     Index3D localIndex;
    	     if (domainInfo.isLocal(iXLeftBorder,iY,iZ,localIndex))
    	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane0N);
    	     if (domainInfo.isLocal(iXRightBorder,iY,iZ,localIndex))
    	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane0P);
    	}

    for(unsigned int iX = iXLeftBorder+1; iX <= iXRightBorder-1; ++iX)
    	for(unsigned int iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; ++iZ) {
    	     Index3D localIndex;
    	     if (domainInfo.isLocal(iX,iYBottomBorder,iZ,localIndex))
    	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane1N);
    	     if (domainInfo.isLocal(iX,iYTopBorder,iZ,localIndex))
    	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane1P);
    	}

    for(unsigned int iX = iXLeftBorder+1; iX <= iXRightBorder-1; ++iX)
    	for(unsigned int iY = iYBottomBorder+1; iY <= iYTopBorder-1; ++iY) {
    	     Index3D localIndex;
    	     if (domainInfo.isLocal(iX,iY,iZFrontBorder,localIndex))
    	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane2N);
    	     if (domainInfo.isLocal(iX,iY,iZBackBorder,localIndex))
    	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane2P);
    	}

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;

    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP(omega,0.7);


    for(unsigned int iX = iXLeftBorder+1; iX <= iXRightBorder-1; ++iX) {
	     Index3D localIndex;
	     if (domainInfo.isLocal(iX,iYTopBorder,iZFrontBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0PN);
	     if (domainInfo.isLocal(iX,iYBottomBorder,iZFrontBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0NN);
	     if (domainInfo.isLocal(iX,iYTopBorder,iZBackBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0PP);
	     if (domainInfo.isLocal(iX,iYBottomBorder,iZBackBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0NP);
    }

	for(unsigned int iY = iYBottomBorder+1; iY <= iYTopBorder-1; ++iY) {
	     Index3D localIndex;
	     if (domainInfo.isLocal(iXLeftBorder,iY,iZBackBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1PN);
	     if (domainInfo.isLocal(iXLeftBorder,iY,iZFrontBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1NN);
	     if (domainInfo.isLocal(iXRightBorder,iY,iZBackBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1PP);
	     if (domainInfo.isLocal(iXRightBorder,iY,iZFrontBorder,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1NP);
	}

	for(unsigned int iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; ++iZ) {
	     Index3D localIndex;
	     if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZ,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2PN);
	     if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZ,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2NN);
	     if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZ,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2PP);
	     if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZ,localIndex))
	    	 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2NP);
	}

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP(omega,0.7);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP(omega,0.7);


    Index3D localIndex;
    if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZFrontBorder,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerNNN);

    if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZFrontBorder,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerPNN);

    if (domainInfo.isLocal(iXLeftBorder ,iYTopBorder   ,iZFrontBorder,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerNPN);

    if (domainInfo.isLocal(iXLeftBorder ,iYBottomBorder,iZBackBorder ,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerNNP);

    if (domainInfo.isLocal(iXRightBorder,iYTopBorder   ,iZFrontBorder,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerPPN);

    if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZBackBorder ,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerPNP);

    if (domainInfo.isLocal(iXLeftBorder ,iYTopBorder   ,iZBackBorder ,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerNPP);

    if (domainInfo.isLocal(iXRightBorder,iYTopBorder   ,iZBackBorder ,localIndex))
		lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2], &cornerPPP);
}

void MultipleSteps(int rank, unsigned int resolution, double simTime, std::string filename, long int port)
{

  constexpr unsigned int nbHarmRadial = 1;
  constexpr unsigned int nbHarmAzi = 1;
  constexpr unsigned int nbHarm = 1;
  using CouplingType = Coupling<T,BesselHarmonicThrust<T,Lattice<T>>,TwoHarmonicVelocity<T,Lattice<T>,nbHarmRadial,nbHarmAzi>>;
  using ReceiveDataGensim = CouplingType::ReceiveDataType;
  using SendDataGensim    = CouplingType::SendDataType;

  int iXLeftBorder = 0;
  int iXRightBorder = (resolution+1)*1.5-1;
  int iYBottomBorder = 0;
  int iYTopBorder   = (resolution+1)*1.5-1;
  int iZFrontBorder = 0;
  int iZBackBorder  = (resolution+1)*2-1;

  unsigned ghostLayer[] = {0,0,1};

  Index3D globalDomain{iXRightBorder+1,iYTopBorder+1,iZBackBorder+1,0};


  const SubDomainInformation<T,Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T,Lattice<T>>(globalDomain[0],globalDomain[1],globalDomain[2],0u,1u,ghostLayer);
  DomainInformation<T,Lattice<T>> domainInfo = decomposeDomainAlongZ(refSubDomain,rank,getNoRanks(),ghostLayer);
  std::cout << domainInfo.getLocalInfo();
  std::cout << "####" << std::endl;


  CommunicationDataHandler<T,Lattice<T>,MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(domainInfo);

  T const rotorRadius = 2.32;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,6.*rotorRadius
          ,100.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();
  T cellSize = spacing*spacing;
  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";
  BlockLattice3D<T, Lattice> lattice(domainInfo.getLocalInfo(), &bulkDynamics);
  std::cout << "Finished!" << std::endl;

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,domainInfo.getLocalInfo(),domainInfo.refSubDomain);

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<size_t> rotorPosition = {static_cast<size_t>(iXRightBorder/2.),static_cast<size_t>(iYTopBorder/2.),static_cast<size_t>((iZBackBorder+1)*0.57)};

  Index3D index;
  const bool iAmTheChosenOne = domainInfo.getLocalInfo().isLocal(static_cast<unsigned long int>(rotorPosition[0]),
		  static_cast<unsigned long int>(rotorPosition[1]),
		  static_cast<unsigned long int>(rotorPosition[2]),index);

  Index3D rotorPositionIndexLocal = domainInfo.getLocalIdx(rotorPosition[0], rotorPosition[1], rotorPosition[2]);

  std::vector<T> rotorPositionLocal = {static_cast<T>(rotorPositionIndexLocal[0]),static_cast<T>(rotorPositionIndexLocal[1]),static_cast<T>(rotorPositionIndexLocal[2])};
  if(not iAmTheChosenOne)
	  rotorPositionLocal[2] = domainInfo.getLocalInfo().localGridSize()[2]/2;

  if(iAmTheChosenOne)
	  std::cout << "I AM THE CHOSEN ONE " << rank << std::endl;

 Rotor<T> rotor(rotorRadius,0.207,rotorPositionLocal,spacing);
 RotorCellData<T> rotorCellData(rotor,lattice, converter);

 Rotor<T> rotorInflow(rotorRadius*1.2,0.,rotorPositionLocal,spacing);
 SquareCellData<T> squareCellDataInflow(rotorInflow,lattice,converter);
 std::tuple<Rotor<T>,SquareCellData<T>,unsigned int,unsigned int, T, T, unsigned int, unsigned int, bool> inflowCplCnstr(rotorInflow,
		 squareCellDataInflow,nbHarmRadial,nbHarmAzi,1.2,spacing,200,720,false);

 CouplingType::CouplingDataType couplingData(cellSize,inflowCplCnstr);

 const T rootCutL = rotor.getHubRadius()*rotor.getRadiusUndim();
 std::cout << "Root cut: " << rootCutL << std::endl;
  static NoDynamics<T,Lattice> noDynamics;
  static PostProcessingDynamics<T,Lattice,CurvedSlipBoundaryProcessor3D<T,Lattice>> slipBoundary;

 if(iAmTheChosenOne) {
	   auto rotorCenter = rotor.getCenter();
	   for(unsigned int iX = 1; iX < lattice.getNx()-1; ++iX) {
	   //      for(unsigned int iY = static_cast<int>(std::ceil(rotorCenter[1])); iY < lattice.getNy(); ++iY) {
		   for(unsigned int iY = 1; iY < lattice.getNy()-1; ++iY) {
			   for(unsigned int iZ = 1; iZ < lattice.getNz()-1; ++iZ) {
				   size_t index = util::getCellIndex3D(iX,iY,iZ,lattice.getNy(),lattice.getNz());
				   T radius = std::sqrt(std::pow(iX-rotorCenter[0],2)+std::pow(iY-rotorCenter[1],2)+std::pow(iZ-rotorCenter[2],2));
				   if(radius <= rootCutL) {
					 lattice.defineDynamics(iX,iY,iZ,&noDynamics);
				   }
				   else {
					 const T p[3] = {iX,iY,iZ};
					 unsigned int nNeighbor = 0;
					 for(unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
						 const T neighbor[3] = {p[0]+Lattice<T>::c(iPop,0),p[1]+Lattice<T>::c(iPop,1),p[2]+Lattice<T>::c(iPop,2)};
						 const T radNeigbor = std::sqrt(std::pow(neighbor[0]-rotorCenter[0],2)+
								 std::pow(neighbor[1]-rotorCenter[1],2)+std::pow(neighbor[2]-rotorCenter[2],2));
						 if(radNeigbor <= rootCutL) {
							 // lattice.defineDynamics(iX,iY,iZ,&instances::getBounceBack<T,Lattice>());
							 lattice.defineDynamics(iX,iY,iZ,&slipBoundary);
							 break;
						 }
					 }
				   }
			   }
		   }
	   }
 }

  std::cout << "Define ghostlayer to be bulkdynamics but fluidmask false ";
  GhostDynamics<T, Lattice> ghostLayerDynamics(1.0);
  for (unsigned iX=0;iX<domainInfo.getLocalInfo().localGridSize()[0];++iX)
	  for (unsigned iY=0;iY<domainInfo.getLocalInfo().localGridSize()[1];++iY)
		  for (unsigned iZ=0;iZ<domainInfo.getLocalInfo().localGridSize()[2];++iZ) {
			if (iX<domainInfo.getLocalInfo().ghostLayer[0] or iX>=domainInfo.getLocalInfo().localGridSize()[0]-domainInfo.getLocalInfo().ghostLayer[0])
			  lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
			if (iY<domainInfo.getLocalInfo().ghostLayer[1] or iY>=domainInfo.getLocalInfo().localGridSize()[1]-domainInfo.getLocalInfo().ghostLayer[1])
			  lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
			if (iZ<domainInfo.getLocalInfo().ghostLayer[2] or iZ>=domainInfo.getLocalInfo().localGridSize()[2]-domainInfo.getLocalInfo().ghostLayer[2])
			  lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
		  }

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  cudaDeviceSynchronize();
  std::cout << "Finished!" << std::endl;

 if(iAmTheChosenOne) {

  auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
  auto slipCellIds = slipDataHandler->getCellIDs();
  auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();

  for(size_t index : slipDataHandler->getCellIDs()) {
	   size_t p[3];
	   util::getCellIndices3D(index,lattice.getNy(),lattice.getNz(),p);

	   size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

	   size_t nNeighbor = 0;
	   int normal[3] = {0,0,0};

	   for(unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
		   size_t pN[Lattice<T>::d] = {p[0]+Lattice<T>::c(iPop,0),
				   p[1]+Lattice<T>::c(iPop,1),
				   p[2]+Lattice<T>::c(iPop,2) };
		   size_t nIndex = util::getCellIndex3D(pN[0],pN[1],pN[2],lattice.getNy(),lattice.getNz());

		   if(lattice.getFluidMask()[nIndex] == false) {
			   T dir[3] = {Lattice<T>::c(iPop,0),Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2)};
			   T delta = 0.25;

			   for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
				   normal[iDim] += Lattice<T>::c(iPop,iDim);

			   size_t const idxDir = CurvedSlipBoundaryProcessor3D<T,Lattice>::idxDir(nNeighbor);
			   size_t const idxDelta = CurvedSlipBoundaryProcessor3D<T,Lattice>::idxDelta(nNeighbor);
			   slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
			   slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

			   slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxTau()][momentaIndex] =
									   converter.getLatticeRelaxationTime();
			   ++nNeighbor;
		   }
	   }

	   T normalAbs = 0;
	   for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
		   normalAbs += normal[iDim]*normal[iDim];
	   normalAbs = std::sqrt(normalAbs);

	   for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
		   slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNormal()+iDim][momentaIndex] = -normal[iDim]/normalAbs;

	   slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNumNeighbour()][momentaIndex] = (T) nNeighbor;
  }
 }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  InitEquilibrium<T,Lattice<T>> initFunctor;

  std::cout << "Init equilibrium.... ";

  T uInit[Lattice<T>::d] = {0.,0.,0.};
  T uSqrInit = 0.;
  T rhoInit = 1.0;
  lattice.apply(initFunctor,lattice.getNCells(),rhoInit,uInit[0],uInit[1],uInit[2],uSqrInit);

  std::cout << "Finished!" << std::endl;

  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  std::string name;
  std::string directory = "./tmp/";
  name = filename+"_X";
  name += std::to_string(iXRightBorder+1) + "_";

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> maskFunctor(lattice);
  BlockLatticeIndex3D<T,Lattice> indexFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterZ( name+"Zplane_", domainInfo );
  vtkWriterZ.addFunctor(densityFunctor);
  vtkWriterZ.addFunctor(physVelocityFunctor);
  vtkWriterZ.addFunctor(forceFunctor);
  vtkWriterZ.addFunctor(maskFunctor);
  vtkWriterZ.addFunctor(indexFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterY( name+"Yplane_", domainInfo );
  vtkWriterY.addFunctor(densityFunctor);
  vtkWriterY.addFunctor(physVelocityFunctor);
  vtkWriterY.addFunctor(forceFunctor);
  vtkWriterY.addFunctor(maskFunctor);
  vtkWriterY.addFunctor(indexFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterX( name+"Xplane_", domainInfo );
  vtkWriterX.addFunctor(densityFunctor);
  vtkWriterX.addFunctor(physVelocityFunctor);
  vtkWriterX.addFunctor(forceFunctor);
  vtkWriterX.addFunctor(maskFunctor);
  vtkWriterX.addFunctor(indexFunctor);

  size_t originZ[3] = {0.25*domainInfo.refSubDomain.globalIndexEnd[0]  ,0.25*domainInfo.refSubDomain.globalIndexEnd[1]  ,rotorPosition[2]-1};
  size_t extendZ[3] = {0.75*domainInfo.refSubDomain.globalIndexEnd[0]  ,0.75*domainInfo.refSubDomain.globalIndexEnd[1]  ,rotorPosition[2]+2};

  size_t originY[3] = {0.2*domainInfo.refSubDomain.globalIndexEnd[0],rotorPosition[1]-1,domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendY[3] = {0.8*domainInfo.refSubDomain.globalIndexEnd[0]  ,rotorPosition[1]+2,domainInfo.refSubDomain.globalIndexEnd[2]  };

  size_t originX[3] = {rotorPosition[0]-1,0.2*domainInfo.refSubDomain.globalIndexEnd[1]  ,domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendX[3] = {rotorPosition[0]+2,0.8*domainInfo.refSubDomain.globalIndexEnd[1]  ,domainInfo.refSubDomain.globalIndexEnd[2]  };

  vtkWriterZ.write(0,originZ,extendZ,0.);
  vtkWriterY.write(0,originY,extendY,0.);
  vtkWriterX.write(0,originX,extendX,0.);

  std::cout << "Initializing MultiLattice ...";
#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice,commDataHandler);
  ipcCommunication<T,Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilatticeCPU(lattice,commDataHandler);
  mpiCommunication<T,Lattice<T>> communication{commDataHandler};
#endif
  std::cout << "finished" << std::endl;

  ReceiveDataGensim thrustData;
  SendDataGensim dataSend;
  NetworkInterfaceTCP<ReceiveDataGensim,SendDataGensim> networkCommunicator(port,OUTPUTIP,port,NETWORKBUFFERSIZE,true);

  if(iAmTheChosenOne)
	  networkCommunicator.socket_init();

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  std::ofstream coeffs;
  std::ofstream coeff;
  std::ofstream inflowInterp;
  std::ofstream thrustInterpStream;

  std::vector<float> rDymore;
  std::vector<float> tDymore;

  if(iAmTheChosenOne) {
  	coeffs = std::ofstream(filename+"_coeffsV.txt");
  	coeffs << "step,cc,cs,sc,ss,iRad,iPsi" << std::endl;

 	coeff = std::ofstream(filename+"_coeffsT.txt");
  	coeff << "step,c,siRad,iPsi" << std::endl;
  }
  
  zeroSendData(dataSend);

  int exchange = 0;
  double totalThrust = 0;
  double totalThrustExact = 0.;
  double factor = 1.;

  memory_space::CudaUnified<float> u(20);
  
  boost::math::cyl_bessel_j_zero(0.,1,20,&u[0]);
  float* bla = &u[0];

  for(unsigned int time = 1; time <= converter.getLatticeTime(simTime)+10; ++time) {
	  if(iAmTheChosenOne) {
		  ++exchange;
		  if(exchange == 1) {
			  networkCommunicator.recieve_data(thrustData);

			  CouplingType::writeReceiveData(thrustData,lattice.getDataGPU(),rotorCellData,couplingData.getWriteData());

			  totalThrust = thrust::reduce(thrust::device,&lattice.getDataGPU()[Lattice<T>::forceIndex+2][0],
					  &lattice.getDataGPU()[Lattice<T>::forceIndex+2][lattice.getNCells()-1]);
			  totalThrust = converter.getPhysForce(totalThrust);

			  totalThrustExact = thrustData.AbsThrust[2];

			  if(time > 10 and totalThrust != 0.0) {
				  factor = -totalThrustExact/totalThrust;
				  // scaleThrust<T><<<rotorCellData._numberRotorCells/256+1,256>>>(factor,rotorCellData,lattice.getDataGPU());
				  cudaDeviceSynchronize();

				  totalThrust = thrust::reduce(thrust::device,&lattice.getDataGPU()[Lattice<T>::forceIndex+2][0],
					  &lattice.getDataGPU()[Lattice<T>::forceIndex+2][lattice.getNCells()-1]);
				  totalThrust = converter.getPhysForce(totalThrust);
			  }
		  }
	  }

	  if( (time%(converter.getLatticeTime(0.0369)) == 0) ) {
		   lattice.copyDataToCPU();
		   double physTime = converter.getPhysTime(time);
		   vtkWriterZ.write(time,originZ,extendZ,physTime);
		   vtkWriterY.write(time,originY,extendY,physTime);
		   vtkWriterX.write(time,originX,extendX,physTime);
	  }

// #ifdef ENABLE_CUDA
      // cudaDeviceSynchronize();
      // collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
      // HANDLE_ERROR(cudaGetLastError());
// #else
      // collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
// #endif

      if(iAmTheChosenOne) {
		  if(exchange == 1) {
			  CouplingType::readSendData(dataSend,lattice.getDataGPU(),squareCellDataInflow,couplingData.getReadData());
			  dataSend.ccX[0] = 0;
			  dataSend.ccY[0] = 0;
			  dataSend.ccZ[0] = 0;
			  dataSend.csX[0] = 0;
			  dataSend.csY[0] = 0;
			  dataSend.csZ[0] = 0;
			  dataSend.scX[0] = 0;
			  dataSend.scY[0] = 0;
			  dataSend.scZ[0] = 0;
			  dataSend.ssX[0] = 0;
			  dataSend.ssY[0] = 0;
			  dataSend.ssZ[0] = 0;
			  networkCommunicator.send_data(dataSend);
			  exchange = 0;
		  }
      }

      if(iAmTheChosenOne) {
		  if( (time%converter.getLatticeTime(.025)) == 0) {
			 std::cout << "Total thrust: " << totalThrust << "(" << factor << ")\n";
			 std::cout << "Exact thrust: " << totalThrustExact << "\n";
			 std::cout << "Simulation time: " << converter.getPhysTime(time) << "\n\n\n";
		  }

		  if( (time%converter.getLatticeTime(.05)) == 0) {
			 std::cout << "Simulation time: " << converter.getPhysTime(time) << std::endl;
			 for(unsigned int iRad = 0; iRad<thrustData.noHarmonicsRadial; ++iRad)
				 for(unsigned int iAz = 0; iAz<thrustData.noHarmonicsAzimuth; ++iAz)
				 {
					 unsigned int index = thrustData.getIndex(iRad,iAz);
					 coeff << time << "," << thrustData.c[index] << "," << thrustData.s[index] <<
						 "," << iRad << "," << iAz << std::endl;
				 }

			 for(unsigned int iRad = 0; iRad < dataSend.noHarmonicsRadial; ++iRad)
				 for(unsigned int iAz = 0; iAz < dataSend.noHarmonicsAzimuth; ++iAz) {
					 coeffs << time << "," << dataSend.ccZ[dataSend.getIndex(iRad,iAz)] << "," << dataSend.csZ[dataSend.getIndex(iRad,iAz)] << ","
						 << dataSend.scZ[dataSend.getIndex(iRad,iAz)] << "," << dataSend.ssZ[dataSend.getIndex(iRad,iAz)] << ","
						 << iRad << "," << iAz << std::endl;
				}
		  }
      }
  }

  std::cout << "Simulation finished\n";


}

int main(int argc, char** argv)
{
#ifdef ENABLE_CUDA
    int rank = initIPC();
#else
    int rank = initMPI();
#endif

    double simTime = 60;
	int c = 0;
	std::string filename;
	unsigned int port = 0;
	unsigned int resolution = 0;
	while ((c = getopt(argc, argv, "f:p:t:r:")) != -1){
		switch(c) {
			case 'f':
				filename.assign(optarg);
				break;
			case 'p':
				port = strtol(optarg,NULL,10);
				break;
			case 't':
				simTime = strtof(optarg,NULL);
				break;
			case 'r':
				resolution = strtol(optarg,NULL,10);
				break;
		}
	}

	std::cout << "Simulation: " << filename << " at port " << port << " with runtime " << simTime << " seconds" << std::endl;
    MultipleSteps(rank,resolution,simTime,filename,port);

#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
    MPI_Finalize();
#else
    finalizeMPI();
#endif
    return 0;
}
