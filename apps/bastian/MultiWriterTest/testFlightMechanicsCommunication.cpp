/*  This file is part of the OpenLB library *
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"

#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/mpiCommunication.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#include <cmath>

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(int rank, unsigned int resolution, double simTime, std::string filename, long int port)
{
  int iXLeftBorder = 0;
  int iXRightBorder = (resolution+1)*1.5-1;
  int iYBottomBorder = 0;
  int iYTopBorder   = (resolution+1)*1.5-1;
  int iZFrontBorder = 0;
  int iZBackBorder  = (resolution+1)*2-1;

  unsigned ghostLayer[] = {0,0,2};

  std::cout << "Borders: " << iXRightBorder << "," << iYTopBorder << "," << iZBackBorder << "\n";
  Index3D globalDomain{iXRightBorder+1,iYTopBorder+1,iZBackBorder+1,0};

  const SubDomainInformation<T,Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T,Lattice<T>>(globalDomain[0],globalDomain[1],globalDomain[2],0u,1u,ghostLayer);
  DomainInformation<T,Lattice<T>> domainInfo = decomposeDomainAlongZ(refSubDomain,rank,getNoRanks(),ghostLayer);
  std::cout << domainInfo.getLocalInfo();
  std::cout << "####" << std::endl;


  CommunicationDataHandler<T,Lattice<T>,MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(domainInfo);

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,6.
          ,30.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  float spacing = converter.getConversionFactorLength();
  T cellSize = spacing*spacing;
  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";
  BlockLattice3D<T, Lattice> lattice(domainInfo.getLocalInfo(), &bulkDynamics);
  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;

  std::cout << "Init equilibrium.... ";
  for (unsigned iX=0;iX<domainInfo.getLocalInfo().localGridSize()[0];++iX)
	  for (unsigned iY=0;iY<domainInfo.getLocalInfo().localGridSize()[1];++iY)
		  for (unsigned iZ=0;iZ<domainInfo.getLocalInfo().localGridSize()[2];++iZ) {
            T vel[Lattice<T>::d] = { converter.getLatticeVelocity(iX),
            		converter.getLatticeVelocity(iY),
					converter.getLatticeVelocity(iZ)};
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  std::string name;
  std::string directory = "./tmp/";
  name = filename+"_X";
  name += std::to_string(iXRightBorder+1) + "_";

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> maskFunctor(lattice);
  BlockLatticeIndex3D<T,Lattice> indexFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterZ( name+"Zplane_", domainInfo, false );
  vtkWriterZ.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterY( name+"Yplane_", domainInfo, false );
  vtkWriterY.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterX( name+"Xplane_", domainInfo, false);
  vtkWriterX.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterZ2( name+"2_Zplane_", domainInfo, spacing, false );
  vtkWriterZ2.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterY2( name+"2_Yplane_", domainInfo, spacing, false );
  vtkWriterY2.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterX2( name+"2_Xplane_", domainInfo, spacing, false);
  vtkWriterX2.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterZ3( name+"3_Zplane_", domainInfo, spacing, false );
  vtkWriterZ3.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterY3( name+"3_Yplane_", domainInfo, spacing, false );
  vtkWriterY3.addFunctor(physVelocityFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterX3( name+"3_Xplane_", domainInfo, spacing, false);
  vtkWriterX3.addFunctor(physVelocityFunctor);

  size_t originZ[3] = {domainInfo.refSubDomain.globalIndexStart[0],domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexEnd[2]/2+10};
  size_t extendZ[3] = {domainInfo.refSubDomain.globalIndexEnd[0]  ,domainInfo.refSubDomain.globalIndexEnd[1]  ,domainInfo.refSubDomain.globalIndexEnd[2]/2+20};

  size_t originY[3] = {domainInfo.refSubDomain.globalIndexStart[0],domainInfo.refSubDomain.globalIndexEnd[1]/2-1,domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendY[3] = {domainInfo.refSubDomain.globalIndexEnd[0]  ,domainInfo.refSubDomain.globalIndexEnd[1]/2+1,domainInfo.refSubDomain.globalIndexEnd[2]  };

  size_t originX[3] = {domainInfo.refSubDomain.globalIndexEnd[0]/2-1,domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendX[3] = {domainInfo.refSubDomain.globalIndexEnd[0]/2+1,domainInfo.refSubDomain.globalIndexEnd[1]  ,domainInfo.refSubDomain.globalIndexEnd[2]  };

  for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
	  std::cout << "Bla: " << originZ[iDim] << ", " << extendZ[iDim] << "\n";

  vtkWriterZ.write(0,originZ,extendZ,0.);
  vtkWriterZ.write(1,originZ,extendZ,0.1);
  vtkWriterZ.write(2,originZ,extendZ,0.2);
  vtkWriterY.write(0,originY,extendY,0.);
  vtkWriterY.write(0,originY,extendY,0.15);
  vtkWriterX.write(0,originX,extendX,0.);
  vtkWriterX.write(0,originX,extendX,0.17);


  float position[3] = {0,0,0.5};
  vtkWriterZ2.write(0,originZ,extendZ,0.  ,position);
  vtkWriterY2.write(0,originY,extendY,0.  ,position);
  vtkWriterX2.write(0,originX,extendX,0.  ,position);

  position[2] = 1.0;
  vtkWriterZ2.write(1,originZ,extendZ,0.1 ,position);
  vtkWriterY2.write(1,originY,extendY,0.11,position);
  vtkWriterX2.write(1,originX,extendX,0.17,position);

  position[2] = 1.2;
  vtkWriterZ2.write(2,originZ,extendZ,0.3 ,position);
  position[2] = 1.5;
  vtkWriterZ2.write(3,originZ,extendZ,0.4 ,position);

  vtkWriterZ3.write(0,originZ,extendZ);
  vtkWriterY3.write(0,originY,extendY);
  vtkWriterX3.write(0,originX,extendX);


  std::cout << "Initializing MultiLattice ...";
#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice,commDataHandler);
  ipcCommunication<T,Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilatticeCPU(lattice,commDataHandler);
  mpiCommunication<T,Lattice<T>> communication{commDataHandler};
#endif
  std::cout << "finished" << std::endl;

}

int main(int argc, char** argv)
{
#ifdef ENABLE_CUDA
    int rank = initIPC();
#else
    int rank = initMPI();
#endif

    double simTime = 60;
	int c = 0;
	std::string filename;
	unsigned int port = 0;
	unsigned int resolution = 0;
	while ((c = getopt(argc, argv, "f:p:t:r:")) != -1){
		switch(c) {
			case 'f':
				filename.assign(optarg);
				break;
			case 'p':
				port = strtol(optarg,NULL,10);
				break;
			case 't':
				simTime = strtof(optarg,NULL);
				break;
			case 'r':
				resolution = strtol(optarg,NULL,10);
				break;
		}
	}

	std::cout << "Simulation: " << filename << " at port " << port << " with runtime " << simTime << " seconds" << std::endl;
    MultipleSteps(rank,resolution,simTime,filename,port);

#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
    MPI_Finalize();
#else
    finalizeMPI();
#endif
    return 0;
}
