/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define D3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/momentaOnBoundaries.h"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/momentaOnBoundaries3D.h"
#include "boundary/momentaOnBoundaries3D.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include <cmath>
#include <cassert>
#include <bitset>

#define Lattice D3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, typename Lattice, typename Momenta>
struct ComputeRhoU {
    OPENLB_HOST_DEVICE
    explicit ComputeRhoU(bool* fluidMask):
    fluidMask_(fluidMask) {};

    OPENLB_HOST_DEVICE
    ComputeRhoU() = delete;

    OPENLB_HOST_DEVICE
    ~ComputeRhoU() = default;

    OPENLB_HOST_DEVICE
    void operator()(T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,const size_t threadIndex) {
//        if(fluidMask_[threadIndex] == true) {
//            T rho;
//            T u[3];
//            Momenta::computeRhoU(cellData,threadIndex,nullptr,0,rho,u);
//            cellData[Lattice::rhoIndex][threadIndex] = rho;
//            for(unsigned int iDim = 0; iDim < Lattice::d; ++iDim)
//                cellData[Lattice::uIndex+iDim][threadIndex] = u[iDim];
//        }
//        if(threadIndex == 25791) {
//            T rho = 0;
//            T u[3];
//            Momenta::computeRhoU(cellData,index,nullptr,0,rho,u);
//            printf("Post streaming rho u")
//        }
    }

    bool* fluidMask_;
};

template<unsigned int RESOLUTION>
void MultipleSteps()
{
  int iXN = 0;
  int iXP = 68;
  int iYN = 0;
  int iYP   = 68;
  int iZN = 0;
  int iZP  = 68;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXP
          ,0.3*1.0/std::sqrt(3)
          ,1.
          ,5.0
          ,0.00721688/5.
          ,1.
          ,0);

  converter.print();
  std::cout << "Re: " << converter.getPhysVelocity(0.1)*1./converter.getPhysViscosity() << std::endl;
  std::cout << "Needed viscosity: " << converter.getPhysVelocity(0.1)*1./400. << std::endl;
  std::cout << "Phys velocity: " << converter.getPhysVelocity(0.1) << std::endl;
  T spacing = converter.getConversionFactorLength();
  T cellSize = spacing*spacing;

  T omega = converter.getLatticeRelaxationFrequency();

  LudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";


  BlockLattice3D<T, Lattice> lattice(iXP + 1, iYP + 1, iZP +1, &bulkDynamics);

  std::cout << "Finished!" << std::endl;

  static PostProcessingDynamics<T,Lattice,CurvedWallBoundaryProcessor3D<T,Lattice>> slipBoundary;
  static NoDynamics<T,Lattice> noDynamics;
  auto bb = instances::getBounceBack<T,Lattice>();

  lattice.defineDynamics(iXN+1,iXN+1,iYN+1,iYP-1,iZN+1,iZP-1,&slipBoundary);
  lattice.defineDynamics(iXP-1,iXP-1,iYN+1,iYP-1,iZN+1,iZP-1,&slipBoundary);
  lattice.defineDynamics(iXN+1,iXP-1,iYN+1,iYN+1,iZN+1,iZP-1,&slipBoundary);
  lattice.defineDynamics(iXN+1,iXP-1,iYP-1,iYP-1,iZN+1,iZP-1,&slipBoundary);
  lattice.defineDynamics(iXN+1,iXP-1,iYN+1,iYP-1,iZN+1,iZN+1,&slipBoundary);
  lattice.defineDynamics(iXN+1,iXP-1,iYN+1,iYP-1,iZP-1,iZP-1,&slipBoundary);

  lattice.defineDynamics(iXN,iXN,iYN,iYP,iZN,iZP,&noDynamics);
  lattice.defineDynamics(iXP,iXP,iYN,iYP,iZN,iZP,&noDynamics);
  lattice.defineDynamics(iXN,iXP,iYN,iYN,iZN,iZP,&noDynamics);
  lattice.defineDynamics(iXN,iXP,iYP,iYP,iZN,iZP,&noDynamics);
  lattice.defineDynamics(iXN,iXP,iYN,iYP,iZN,iZN,&noDynamics);
  lattice.defineDynamics(iXN,iXP,iYN,iYP,iZP,iZP,&noDynamics);

//  lattice.defineDynamics(iXLeftBorder +1, iXLeftBorder +1, iYBottomBorder+1, iYTopBorder   -1, iZFrontBorder+1, iZBackBorder -1,&bb);
//  lattice.defineDynamics(iXRightBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder   -1, iZFrontBorder+1, iZBackBorder -1,&bb);
//  lattice.defineDynamics(iXLeftBorder +1, iXRightBorder-1, iYBottomBorder+1, iYBottomBorder+1, iZFrontBorder+1, iZBackBorder -1,&bb);
//  lattice.defineDynamics(iXLeftBorder +1, iXRightBorder-1, iYTopBorder   -1, iYTopBorder   -1, iZFrontBorder+1, iZBackBorder -1,&bb);
//  lattice.defineDynamics(iXLeftBorder +1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder   -1, iZFrontBorder+1, iZFrontBorder+1,&bb);
//  lattice.defineDynamics(iXLeftBorder +1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder   -1, iZBackBorder -1, iZBackBorder -1,&bb);



  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished !" << std::endl;

  auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
  auto slipCellIds = slipDataHandler->getCellIDs();
  auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();

  for(size_t index : slipDataHandler->getCellIDs()) {
      size_t p[3];
      util::getCellIndices3D(index,lattice.getNy(),lattice.getNz(),p);

      size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

      T uw[3] = {0,0,0};
      if(p[1] == iYP-1 and p[0] != iXN+1 and p[0] != iXP-1 and p[2] != iZN+1 and p[2] != iZP-1)
          uw[0] = 0.1;

      size_t nNeighbor = 0;
      int normal[3] = {0,0,0};

      for(unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
          size_t pN[Lattice<T>::d] = {p[0]+Lattice<T>::c(iPop,0),
                  p[1]+Lattice<T>::c(iPop,1),
                  p[2]+Lattice<T>::c(iPop,2) };
          size_t nIndex = util::getCellIndex3D(pN[0],pN[1],pN[2],lattice.getNy(),lattice.getNz());


          if(lattice.getFluidMask()[nIndex] == false) {
              T dir[3] = {Lattice<T>::c(iPop,0),Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2)};
              T delta = 0.25;

              size_t const idxDir = CurvedWallBoundaryProcessor3D<T,Lattice>::idxDir(nNeighbor);
              size_t const idxDelta = CurvedWallBoundaryProcessor3D<T,Lattice>::idxDelta(nNeighbor);
              slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
              slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

              ++nNeighbor;
          }
      }

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
          slipBoundaryPostProcData[CurvedWallBoundaryProcessor3D<T,Lattice>::idxVel()+iDim][momentaIndex] =
              uw[iDim];

      slipBoundaryPostProcData[CurvedWallBoundaryProcessor3D<T,Lattice>::idxTau()][momentaIndex] =
                              converter.getLatticeRelaxationTime();

      slipBoundaryPostProcData[CurvedWallBoundaryProcessor3D<T,Lattice>::idxNumNeighbour()][momentaIndex] = (T) nNeighbor;
  }

  std::cout << "Init equilibrium.... ";
  std::cout.flush();
  for (int iX = iXN; iX <= iXP; ++iX)
      for (int iY = iYN; iY <= iYP; ++iY)
          for (int iZ = iZN; iZ <= iZP; ++iZ)
          {
            T vel[Lattice<T>::d] = { converter.getLatticeVelocity(0.), 0., converter.getLatticeVelocity(0.)};
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }

  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  std::string name;
  std::string directory = "/scratch/ga69kiq/outputOpenLB/testCavity3D/";
  name = "cavity3D";
  name += std::to_string(iXP+1);
  std::string nameTrim = name;

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeVelocity3D<T,Lattice> latticeVel(lattice);
  BlockLatticeFluidMask3D<T,Lattice> maskFunctor(lattice);
  BlockLatticeIndex3D<T,Lattice> indexFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriterTrim( nameTrim );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(latticeVel);
  vtkWriterTrim.addFunctor(maskFunctor);
  vtkWriterTrim.addFunctor(indexFunctor);

  vtkWriterTrim.write(0);

  for(unsigned int iStep = 1; iStep < converter.getLatticeTime(100.0); ++iStep) {

      lattice.fieldCollisionGPU<LudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

      lattice.boundaryCollisionGPU();

      cudaDeviceSynchronize();
    #ifdef OLB_DEBUG
      HANDLE_ERROR(cudaPeekAtLastError());
    #endif

      lattice.streamGPU();

      cudaDeviceSynchronize();
    #ifdef OLB_DEBUG
      HANDLE_ERROR(cudaPeekAtLastError());
    #endif

      lattice.postProcessGPU();

      cudaDeviceSynchronize();
    #ifdef OLB_DEBUG
      HANDLE_ERROR(cudaPeekAtLastError());
    #endif
      if(iStep%1000 == 0) {
          lattice.copyDataToCPU();
          vtkWriterTrim.write(iStep);
      }
      if(iStep%20000 == 0)
          break;
  }


}

int main(int argc, char** argv)
{
    MultipleSteps<32>();
    return 0;
}
