/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/momentaOnBoundaries.h"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/momentaOnBoundaries3D.h"
#include "boundary/momentaOnBoundaries3D.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include <cmath>
#include <cassert>
#include <bitset>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

void printAll(T** cellData, size_t Index) {
    std::cout << "Population: " << std::endl;
    for(unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop)
        std::cout << cellData[iPop][Index] << std::endl;

    std::cout << std::endl << "Density" << std::endl;
    for(unsigned int iPop = Lattice<T>::rhoIndex; iPop == Lattice<T>::rhoIndex; ++iPop)
            std::cout << cellData[iPop][Index] << std::endl;

    std::cout << std::endl << "Velocity" << std::endl;
    for(unsigned int iPop = Lattice<T>::uIndex; iPop < Lattice<T>::uIndex+Lattice<T>::d; ++iPop)
            std::cout << cellData[iPop][Index] << std::endl;
    std::cout << "=============" << std::endl;

    T u[3];
    T rho;
    lbDynamicsHelpers<T,Lattice<T>>::computeRhoU(cellData,Index,rho,u);

    std::cout << std::endl << "Rho compute" << std::endl;
    for(unsigned int iPop = Lattice<T>::rhoIndex; iPop == Lattice<T>::rhoIndex; ++iPop)
            std::cout << rho << std::endl;

    std::cout << std::endl << "Velocity Compute" << std::endl;
    for(unsigned int iPop = 0; iPop < Lattice<T>::d; ++iPop)
            std::cout << u[iPop] << std::endl;
    std::cout << "=============" << std::endl;

}

template<typename T, typename Lattice, typename Momenta>
struct ComputeRhoU {
    OPENLB_HOST_DEVICE
    explicit ComputeRhoU(bool* fluidMask):
    fluidMask_(fluidMask) {};

    OPENLB_HOST_DEVICE
    ComputeRhoU() = delete;

    OPENLB_HOST_DEVICE
    ~ComputeRhoU() = default;

    OPENLB_HOST_DEVICE
    void operator()(T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,const size_t threadIndex) {
//        if(fluidMask_[threadIndex] == true) {
//            T rho;
//            T u[3];
//            Momenta::computeRhoU(cellData,threadIndex,nullptr,0,rho,u);
//            cellData[Lattice::rhoIndex][threadIndex] = rho;
//            for(unsigned int iDim = 0; iDim < Lattice::d; ++iDim)
//                cellData[Lattice::uIndex+iDim][threadIndex] = u[iDim];
//        }
//        if(threadIndex == 25791) {
//            T rho = 0;
//            T u[3];
//            Momenta::computeRhoU(cellData,index,nullptr,0,rho,u);
//            printf("Post streaming rho u")
//        }
    }

    bool* fluidMask_;
};

struct Sphere {
    template<typename Container>
    Sphere(Container center, double radius):
    radius_(radius){
        std::copy(center.begin(),center.end(),center_.begin());
    }

    double operator()(double x, double y, double z) const {
//        double x2 = (x-center_[0])*(x-center_[0]);
//        double y2 = (y-center_[1])*(y-center_[1]);
//        double z2 = (z-center_[2])*(z-center_[2]);
//        std::cout << x2 << "," << y2 << "," << z2 << "," << radius_*radius_ << std::endl;
        return (x-center_[0])*(x-center_[0])+(y-center_[1])*(y-center_[1])+(z-center_[2])*(z-center_[2])
                -radius_*radius_;
    }

    template<typename A, typename B>
    void normal(A n, B point) {
        n[0] = point[0]-center_[0];
        n[1] = point[1]-center_[1];
        n[2] = point[2]-center_[2];
        double normalize = std::sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
        n[0] /= normalize;
        n[1] /= normalize;
        n[2] /= normalize;
    }

    double radius_ = 0;
    std::array<double,3> center_;
};

struct Cylinder {
    template<typename Container>
    Cylinder(Container center, double radius):
    radius_(radius){
        std::copy(center.begin(),center.end(),center_.begin());
    }

    double operator()(double x, double y, double z) const {
//        double x2 = (x-center_[0])*(x-center_[0]);
//        double y2 = (y-center_[1])*(y-center_[1]);
//        double z2 = (z-center_[2])*(z-center_[2]);
//        std::cout << x2 << "," << y2 << "," << z2 << "," << radius_*radius_ << std::endl;
        return (x-center_[0])*(x-center_[0])+(z-center_[2])*(z-center_[2])
                -radius_*radius_;
    }

    template<typename A, typename B>
    void normal(A n, B point) {
        n[0] = point[0]-center_[0];
        n[1] = 0;
        n[2] = point[2]-center_[2];
        double normalize = std::sqrt(n[0]*n[0]+n[1]*n[1]+n[2]*n[2]);
        n[0] /= normalize;
        n[1] /= normalize;
        n[2] /= normalize;
    }

    double radius_ = 0;
    std::array<double,3> center_;
};

template<typename Point, typename Direction, typename Surface>
double getIntersection(Point const p, Direction const n, Surface const s) {
    double k = 1.0;

    auto a = s(p[0]+k*n[0],p[1]+k*n[1],p[2]+k*n[2]);
    auto b = s(p[0]+1.001*k*n[0],p[1]+1.001*k*n[1],p[2]+1.001*k*n[2]);

//    std::cout << "a,b: " << a << "," << b << std::endl;

    double der = (b-a)/0.001;

//    std::cout << "Init" << " : " << k << " ," << s(p[0]+k*n[0],p[1]+k*n[1],p[2]+k*n[2]) << "," << der << std::endl;

    T resOld = 1000;

    for(unsigned int iter = 0; iter < 10; ++iter) {
        T res = s(p[0]+k*n[0],p[1]+k*n[1],p[2]+k*n[2]);
        k -= 0.9*res/der;

//        std::cout << "Iter " << iter << ": " << k << " ," << res << std::endl;
        if(k > 1 or k < 0) {
            k = std::pow(0.8,iter);
//            std::cout << "I go here" << std::endl;
        }
        if(std::abs(res) < 1e-6)
            break;
        a = s(p[0]+k*n[0],p[1]+k*n[1],p[2]+k*n[2]);
        b = s(p[0]+1.001*k*n[0],p[1]+1.001*k*n[1],p[2]+1.001*k*n[2]);
        der = (b-a)/0.001;
    }

//    std::cout << "Intersection: " << p[0]+k*n[0] << "," << p[1]+k*n[1] << "," << p[2]+k*n[2] << std::endl;
//    std::cout << "Residuum:     " << s(p[0]+k*n[0],p[1]+k*n[1],p[2]+k*n[2]) << std::endl;
    return k;
}

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

//    static LudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM,0,-1,0>,PlaneFdBoundaryProcessor3D<T,Lattice,0,-1>> plane0N(omega);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
//    static LudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,PressureBM,0,-1,0>,PlaneFdBoundaryProcessor3D<T,Lattice,0,-1>> plane0N(omega);
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryProcessor3D<T,Lattice, 0,-1>> plane0N;

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
//    static LudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,PressureBM,0,1,0>,PlaneFdBoundaryProcessor3D<T,Lattice,0,1>> plane0P(omega);
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryProcessor3D<T,Lattice, 0, 1>> plane0P;

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
//    static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,-1,0>> plane1N;
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryProcessor3D<T,Lattice, 1,-1>> plane1N;

    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryProcessor3D<T,Lattice, 1, 1>> plane1P;
//    static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,1,0>> plane1P;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;
//    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM,2,1,0>,PlaneFdBoundaryProcessor3D<T,Lattice,2,1>> plane2P(omega);

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);

    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
//    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
//    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

//    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;

//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static PostProcessingDynamics<T,Lattice,PeriodicBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;


//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);
//
//    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
//    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
//    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
//    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);
//
//
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);

    static BounceBack<T,Lattice>& bb = instances::getBounceBack<T,Lattice>();
//
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &bb);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &bb);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &bb);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &bb);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &bb);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &bb);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &bb);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &bb);


//    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &bb);
//    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &bb);
//    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &bb);
//    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &bb);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

//    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
//    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
//    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
//    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
//    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
//    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
//    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
//    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);


    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &bb);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &bb);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &bb);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &bb);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &bb);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &bb);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &bb);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &bb);
}

template<unsigned int RESOLUTION>
void MultipleSteps()
{
  int iXLeftBorder = 0;
  int iXRightBorder = 4*RESOLUTION-1;
  int iYBottomBorder = 0;
  int iYTopBorder   = RESOLUTION-1;
  int iZFrontBorder = 0;
  int iZBackBorder  = RESOLUTION-1;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iZBackBorder
          ,0.3*1.0/std::sqrt(3)
          ,1.
          ,5.
          ,0.00146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();
  T cellSize = spacing*spacing;

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.04);

  std::cout << "Create blockLattice.... ";


  ConstExternalField<T,Lattice> externalFieldClass({0.,0.,0.});
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1, &bulkDynamics);
  std::vector<bool> rootMask(lattice.getNCells(),false);

  iZBackBorder   -= 1;

  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);
  std::cout << "Finished!" << std::endl;


  static PostProcessingDynamics<T,Lattice,CurvedWallBoundaryProcessor3D<T,Lattice>> slipBoundary;
  static NoDynamics<T,Lattice> noDynamics;
  auto bb = instances::getBounceBack<T,Lattice>();

  std::array<double,3> cylinderCenter{0.5*lattice.getNx(),0.5*lattice.getNy(),iZBackBorder*1.2};
  double cylinderRadius = 0.3*lattice.getNz();
  Cylinder roundwall(cylinderCenter,cylinderRadius);

  std::array<T,6> limitsSearch = {cylinderCenter[0]-cylinderRadius-2,cylinderCenter[0]+cylinderRadius+2,
          1,lattice.getNy()-2,
          cylinderCenter[2]-cylinderRadius-2,iZBackBorder
  };

//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder,&bb);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZBackBorder+1, iZBackBorder+1,&noDynamics);

  for(unsigned int iX = 1; iX <= iXRightBorder-1; ++iX) {
      for(unsigned int iY = limitsSearch[2]; iY <= limitsSearch[3]; ++iY) {
          for(unsigned int iZ = limitsSearch[4]; iZ <= limitsSearch[5]; ++iZ) {
              if(roundwall(iX,iY,iZ) > 0) {
                  if(iZ == iZBackBorder)
                      lattice.defineDynamics(iX,iY,iZ,&slipBoundary);
                  else {
                      const T p[3] = {iX,iY,iZ};
                      unsigned int nNeighbor = 0;
                      for(unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                          const T neighbor[3] = {p[0]+Lattice<T>::c(iPop,0),p[1]+Lattice<T>::c(iPop,1),p[2]+Lattice<T>::c(iPop,2)};
                          const T s = roundwall(neighbor[0],neighbor[1],neighbor[2]);
                          if(s <= 0) {
                              lattice.defineDynamics(iX,iY,iZ,&slipBoundary);
                              break;
                          }
                      }
                  }
              }
              else {
                  lattice.defineDynamics(iX,iY,iZ,&noDynamics);
              }
          }
      }
  }

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished !" << std::endl;

  auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
  auto slipCellIds = slipDataHandler->getCellIDs();
  auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();

  for(size_t index : slipDataHandler->getCellIDs()) {
        size_t p[3];
        util::getCellIndices3D(index,lattice.getNy(),lattice.getNz(),p);

        size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

        T uw[3] = {0,0,0};

        size_t nNeighbor = 0;
        float normal[3] = {0,0,-1};
        float tangential[3] = {1,0,0};

        if(p[2] != iZBackBorder) {
            float tmp[3];
            tmp[0] = p[0]-cylinderCenter[0];
            tmp[2] = p[2]-cylinderCenter[2];
            float norm = std::sqrt(tmp[0]*tmp[0]+tmp[2]*tmp[2]);
            normal[0] = tmp[0]/norm;
            normal[2] = tmp[2]/norm;
            tangential[0] = std::abs(normal[2]);
            tangential[2] = normal[0];
        }

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
            uw[iDim] = 0.1*tangential[iDim];

        for(unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            size_t pN[Lattice<T>::d] = {p[0]+Lattice<T>::c(iPop,0),
                    p[1]+Lattice<T>::c(iPop,1),
                    p[2]+Lattice<T>::c(iPop,2) };
            size_t nIndex = util::getCellIndex3D(pN[0],pN[1],pN[2],lattice.getNy(),lattice.getNz());


            if(lattice.getFluidMask()[nIndex] == false) {
                T dir[3] = {Lattice<T>::c(iPop,0),Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2)};
                T delta = 0.1;

                size_t const idxDir = CurvedWallBoundaryProcessor3D<T,Lattice>::idxDir(nNeighbor);
                size_t const idxDelta = CurvedWallBoundaryProcessor3D<T,Lattice>::idxDelta(nNeighbor);
                slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
                slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;
                ++nNeighbor;
            }
        }

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
            slipBoundaryPostProcData[CurvedWallBoundaryProcessor3D<T,Lattice>::idxVel()+iDim][momentaIndex] =
                uw[iDim];

        slipBoundaryPostProcData[CurvedWallBoundaryProcessor3D<T,Lattice>::idxTau()][momentaIndex] =
                                converter.getLatticeRelaxationTime();

        slipBoundaryPostProcData[CurvedWallBoundaryProcessor3D<T,Lattice>::idxNumNeighbour()][momentaIndex] = (T) nNeighbor;
    }

  std::cout << "Init equilibrium.... ";
  std::cout.flush();
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { converter.getLatticeVelocity(0.), 0., converter.getLatticeVelocity(0.)};
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }

  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  std::string name;
  std::string directory = "/scratch/ga69kiq/outputOpenLB/testCurvedWall/";
  name = "testCurvedWall_";
  name += std::to_string(iXRightBorder+1);
  std::string nameTrim = name;

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeFi<T,Lattice> fiFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> maskFunctor(lattice);
  BlockLatticeIndex3D<T,Lattice> indexFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriterTrim( nameTrim );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(maskFunctor);
  vtkWriterTrim.addFunctor(indexFunctor);

  vtkWriterTrim.write(0);

  for(unsigned int iStep = 1; iStep < converter.getLatticeTime(100.0); ++iStep) {

      lattice.fieldCollisionGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

      lattice.boundaryCollisionGPU();

      cudaDeviceSynchronize();
    #ifdef OLB_DEBUG
      HANDLE_ERROR(cudaPeekAtLastError());
    #endif

      lattice.streamGPU();

      cudaDeviceSynchronize();
    #ifdef OLB_DEBUG
      HANDLE_ERROR(cudaPeekAtLastError());
    #endif

      lattice.postProcessGPU();

      cudaDeviceSynchronize();
    #ifdef OLB_DEBUG
      HANDLE_ERROR(cudaPeekAtLastError());
    #endif
      if(iStep%1000 == 0) {
          lattice.copyDataToCPU();
          vtkWriterTrim.write(iStep);
      }
      if(iStep == 50000)
          break;
  }


}

int main(int argc, char** argv)
{
    MultipleSteps<64>();
    return 0;
}
