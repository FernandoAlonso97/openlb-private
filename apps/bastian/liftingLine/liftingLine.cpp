/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include <iostream>
#include <cmath>
#include <core/cellBlockData.h>
#include <core/cellBlockData.hh>
#include <dynamics/latticeDescriptors.h>
#include <dynamics/latticeDescriptors.hh>

using namespace olb;

#define Lattice descriptors::ForcedD3Q19Descriptor

template<typename T>
struct LiftingLine {


  size_t numPoints{};
  T angle{0};
  std::vector<std::vector<T>> linePoints{3};
  std::vector<T> eps;

  LiftingLine(unsigned int const resolutionLine, unsigned int const resolutionField,
      T const length, std::vector<T> const & start):
    numPoints{length*resolutionLine}
  {
    for(auto& iDim : linePoints)
    {
      iDim.resize(numPoints);
    }

    T deltaX = 0.5*length*resolutionField/resolutionLine;

    for(unsigned int iPoint = 0; iPoint < numPoints; ++iPoint)
    {
      linePoints[0][iPoint] = start[0]+iPoint*deltaX*cos(angle);
      linePoints[1][iPoint] = start[1]+iPoint*deltaX*sin(angle);
      linePoints[2][iPoint] = start[2];

//      std::cout << linePoints[0][iPoint] << "," << linePoints[1][iPoint]
//                << "," << linePoints[2][iPoint] << std::endl;
    }
  }

  T getRadialPosition(T point)
  {
    return point/numPoints;
  }

  T weight(T** cellData)
  {
    for(unsigned int iPoint=0; iPoint < numPoints; ++iPoint)
    {
      T eps = 0.25*0.09*M_PI*18*(4/M_PI)*0.27/(1./24.)*std::sqrt(1.-pow((iPoint-numPoints/2.)/numPoints,2));
      for(unsigned int iX = 0; iX < 48; ++iX)
        for(unsigned int iY = 0; iY < 48; ++iY)
          for(unsigned int iZ = 10; iZ < 11; ++iZ)
          {
            T radius = sqrt(pow(iX-linePoints[0][iPoint],2)+pow(iY-linePoints[1][iPoint],2)
                +pow(iZ-linePoints[2][iPoint],2));
              cellData[24][util::getCellIndex3D(iX,iY,iZ,48,48)] +=
                  1/(pow(eps,3)*pow(M_PI,1.5))*exp(-pow(radius/eps,2));
          }
    }

  }

};



void MultipleSteps(const double simTime, const double sizeInPlane,
        const double distanceAbove, const double distanceBelow, const int resolution,
        double smagoConst, double outputTime, std::string printFirst)
{

  int iXLeftBorder = 0;
  int iXRightBorder = static_cast<int>(std::nearbyint( sizeInPlane*resolution * 0.5f ) * 2.0f)-1;
  int iYBottomBorder = 0;
  int iYTopBorder = static_cast<int>(std::nearbyint( sizeInPlane*resolution * 0.5f ) * 2.0f)-1;
  int iZFrontBorder = 0;
  int iZBackBorder = static_cast<int>(std::nearbyint( (distanceAbove+distanceBelow)*resolution * 0.5f ) * 2.0f)-1;

  CellBlockData<T,Lattice> cellData(iXRightBorder,iYTopBorder,iZBackBorder);

  const int center = (iXRightBorder+1)/2;
  std::vector<int> jetForceXLimit = {center-resolution/2-1, center+resolution/2};
  std::vector<int> jetForceYLimit = {center-resolution/2-1, center+resolution/2};
  std::vector<int> jetForceZPosition = {static_cast<int>(distanceAbove*resolution)};

  std::cout << "Field size (X,Y,Z): " << iXRightBorder+1 << "," << iYTopBorder+1 << "," << iZBackBorder+1 << std::endl;

  std::cout << "X limits: " << jetForceXLimit[0] << " to " << jetForceXLimit[1] << std::endl;
  std::cout << "Y limits: " << jetForceYLimit[0] << " to " << jetForceYLimit[1] << std::endl;
  std::cout << "Z position: " << jetForceZPosition[0] << std::endl;

  std::vector<T> centerLine{static_cast<T>(center),static_cast<T>(center)
    ,static_cast<T>(jetForceZPosition[0])};
  LiftingLine<T> line{1/1.5*resolution,resolution,1.,centerLine};

  T** cellDataTs = cellData.getCellData();

  for(unsigned int iX = 0; iX < 48; ++iX)
    for(unsigned int iY = 0; iY < 48; ++iY)
      for(unsigned int iZ = 10; iZ < 11; ++iZ)
        cellDataTs[24][util::getCellIndex3D(iX,iY,iZ,48,48)] = 0;

  line.weight(cellDataTs);


  for(unsigned int iX = jetForceXLimit[0]; iX <= jetForceYLimit[1]; ++iX)
  {
    for(unsigned int iY = jetForceYLimit[0]; iY <= jetForceYLimit[1]; ++iY)
      std::cout << std::setprecision(1) << std::fixed << cellDataTs[24][util::getCellIndex3D(iX,iY,10,48,48)]*10000 << ",";
    std::cout << std::endl;
  }



}

int main(int argc, char** argv)
{
    int resolution = std::stoi(argv[1],NULL);
    double sizeXY = std::stod(argv[2],NULL);
    double heightAbove = std::stod(argv[3],NULL);
    double heightBelow = std::stod(argv[4],NULL);
    double smagoConst = std::stod(argv[5],NULL);

    const double simTime = std::stod(argv[6],NULL);
    double outputTime = std::stod(argv[7],NULL);
	std::string printFirst = argv[8];

    std::cout << "Resolution: " << resolution << std::endl;
    std::cout << "sizeXY: " << sizeXY << std::endl;
    std::cout << "heightAbove: " << heightAbove << std::endl;
    std::cout << "heightBelow: " << heightBelow << std::endl;
    std::cout << "smagoConst: " << smagoConst << std::endl;
    std::cout << "simTime: " << simTime << std::endl;
    std::cout << "outputTime: " << outputTime << std::endl;

    MultipleSteps(simTime,sizeXY,heightAbove,heightBelow,resolution,smagoConst,outputTime,printFirst);
	return 0;
}
