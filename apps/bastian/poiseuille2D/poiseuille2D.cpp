/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

template <typename T>
class Vortex2D : public AnalyticalF2D<T,T> {
protected:
#define PI 3.14159265358979323846
  std::vector<T> _centerPoint;
  std::vector<T> _translatoricSpeed;
  T _maxVelocity;
  T _radius;

public:
  Vortex2D(std::vector<T> centerPoint, std::vector<T> translatoricSpeed,  T maxVelocity, T radius) : AnalyticalF2D<T,T>(2)
  , _centerPoint(centerPoint)
  , _translatoricSpeed(translatoricSpeed)
  , _maxVelocity(maxVelocity)
  , _radius(radius)
  {}
  bool operator()(T output[], const T input[]) override
  {
      T centerPoint[2];
      centerPoint[0] = _centerPoint[0] + input[2]*_translatoricSpeed[0];
      centerPoint[1] = _centerPoint[1] + input[2]*_translatoricSpeed[1];

      T position[] = {input[0]-centerPoint[0],input[1]-centerPoint[1]};

      T distance = sqrt(position[0]*position[0]+position[1]*position[1]);

      if(distance < _radius)
      {
          T amplitude = _maxVelocity*sin(distance/_radius*PI);
          output[0] = position[1]/distance * amplitude;
          output[1] = position[0]/distance * amplitude;
      }
      else
      {
          output[0] = 0;
          output[1] = 0;
      }

      output[0] += _translatoricSpeed[0];
      output[1] += _translatoricSpeed[1];

      return true;
  }
#undef PI
};

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

void MultipleSteps(const double simTime)
{
  int iXLeftBorder = 0;
  int iXRightBorder = 127;
  int iYBottomBorder = 0;
  int iYTopBorder = 63;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iYTopBorder
          ,0.1*1.0/std::sqrt(3)
          ,1.
          ,1.
          ,0.01
          ,1.225
          ,0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, 0.0);
  BlockLattice2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);

  OnLatticeBoundaryCondition2D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition2D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  // prepareLattice
//  boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );

  boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
  boundaryCondition->addImpedanceBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
//  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

//  boundaryCondition->addPeriodicBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addPeriodicBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
  boundaryCondition->addPeriodicBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
  boundaryCondition->addPeriodicBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , &instances::getBounceBack<T,Lattice>());


    boundaryCondition->addExternalImpedanceCornerPP(iXRightBorder ,iYTopBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPN(iXRightBorder,iYBottomBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNP(iXLeftBorder ,iYTopBorder   , omega);
    boundaryCondition->addExternalImpedanceCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//    boundaryCondition->addExternalPeriodicCornerPP(iXRightBorder ,iYTopBorder, omega);
//    boundaryCondition->addExternalPeriodicCornerPN(iXRightBorder,iYBottomBorder, omega);
//    boundaryCondition->addExternalPeriodicCornerNP(iXLeftBorder ,iYTopBorder   , omega);
//    boundaryCondition->addExternalPeriodicCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//  boundaryCondition->addExternalVelocityCornerPP(iXRightBorder,iYTopBorder   , omega);
//  boundaryCondition->addExternalVelocityCornerPN(iXRightBorder,iYBottomBorder, omega);
//  boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder ,iYTopBorder   , omega);
//  boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder ,iYTopBorder, &instances::getBounceBack<T,Lattice>());


  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
        T vel[2] = { 0,0};
        lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

//  std::vector<T> axisPoint(2,0);
//  axisPoint[1] = iYTopBorder/2.;
//  std::vector<T> axisDirection(2,0);
//  axisDirection[0] = 1;
//  T maxVelocity = converter.getLatticeVelocity(0.63);
//  T maxForce = converter.getLatticeDiskLoading(1.);
//  T radius = axisPoint[1];
//
//  Poiseuille2D<T> inflowProfile(axisPoint, axisDirection,  maxVelocity, radius);
//  Poiseuille2D<T> forceProfile(axisPoint, axisDirection,  maxForce, radius);
//
//  for (int iY = iYBottomBorder; iY <= iYTopBorder; ++iY)
//  {
//    for (int iX = 0; iX <= 0; ++iX)
//    {
//        T coords[Lattice<T>::d] = {iX,iY};
//
//        T u[Lattice<T>::d] = {0};
//        u[0] = maxVelocity;
////        inflowProfile(u,coords);
//
//        T force[Lattice<T>::d];
////        forceProfile(force,coords);
////        force[0] = maxForce/2;
//        lattice.defineRhoU(iX,iX,iY,iY,1.,u);
//        lattice.iniEquilibrium(iX, iX, iY, iY, 1., u);
////        lattice.defineForce(iX,iX,iY,iY, force);
//    }
//  }

  BlockVTKwriter2D<T> vtkWriter( "poiseuille2DForced" );
  BlockLatticeDensity2D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T,Lattice> physVelocityFunctor(lattice,converter);
  BlockLatticeForce2D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/poiseuille2D/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.write(0);

  lattice.copyDataToGPU();

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy());
  timer.start();


  std::vector<T> center{-11.,iYTopBorder/2.};
  std::vector<T> speed{converter.getLatticeDiskLoading(0.8),0};
  Vortex2D<T> vortex(center,speed,converter.getLatticeDiskLoading(0.5),10);
//  std::vector<T> speed{converter.getLatticeVelocity(1.5),0};
//  Vortex2D<T> vortex(center,speed,converter.getLatticeVelocity(1.),10);

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {

      lattice.copyDataToCPU();

      for (int iX = 0; iX <= 1; ++iX)
      {
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
            T coords[3] = {iX,iY,iSteps};
            T val[2] = {0,0};
            vortex(val,coords);
            T vel[2] = { val[0], val[1]};
            T force[2] = { val[0], val[1] };
//            lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
//            lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
            lattice.defineForce(iX,iX,iY,iY,force);
        }
      }

      lattice.copyDataToGPU();

	  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);


	  if(iSteps%converter.getLatticeTime(0.01) == 0)
	  {
	      lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }
  }

  timer.stop();

}

int main()
{
    const double simTime = 10;
	MultipleSteps(simTime);
	return 0;
}
