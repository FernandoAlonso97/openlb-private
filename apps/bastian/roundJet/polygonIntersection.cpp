#ifndef POLYGONINTERSECTION_CPP
#define POLYGONINTERSECTION_CPP

#include <cmath>
#include <algorithm>

namespace polyInter {

template<typename T,int Dim>
class Vector final{

  public:

  explicit Vector() = default;

  ~Vector() = default;

  T operator() (const size_t index) const
  {
    assert(index<Dim);
    return _coordinates[index];
  }

  T& operator() (const size_t index)
  {
    assert(index<Dim);
    return _coordinates[index];
  }

  private:
    T   _coordinates[Dim];
};

template<typename T, int Dim>
Vector<T,Dim> operator+ (const Vector<T,Dim>& a, const Vector<T,Dim>& b)
{
  Vector<T,Dim> tmp;
  for (unsigned int i=0;i<Dim;++i)
  {
    tmp(i)=a(i)+b(i);
  }
  return tmp;
}

template<typename T, int Dim>
Vector<T,Dim> operator- (const Vector<T,Dim>& a, const Vector<T,Dim>& b)
{
  Vector<T,Dim> tmp;
  for (unsigned int i=0;i<Dim;++i)
  {
    tmp(i)=a(i)-b(i);
  }
  return tmp;
}

template<typename T, int Dim>
bool operator== (const Vector<T,Dim>& a, const Vector<T,Dim>& b)
{
  bool tmp{true};
  for (unsigned int i=0;i<Dim;++i)
  {
    tmp = tmp & !(a(i)-b(i));
  }
  return tmp;
}

template<typename T>
class Vector<T,2> final{

  public:

  Vector(T x, T y):_coordinates{x,y} {}

  Vector() = default;

  ~Vector() = default;

  T operator() (const size_t index) const
  {
    assert(index<2);
    return _coordinates[index];
  }

  T& operator() (const size_t index)
  {
    assert(index<2);
    return _coordinates[index];
  }

  private:
    T   _coordinates[2];

};

template<typename T>
class CircularListIterator;

template<typename T,int MaxNoOfEdges>
class Polygon{

  public:

  static const int MaxNoEdges = MaxNoOfEdges;

  Polygon()
  {
    std::fill_n(_edges,MaxNoEdges,Vector<T,2>{std::nan(""),std::nan("")});
  }

  void setNoEdges (const size_t noEdges)
  {
    _currNoEdges = noEdges;
  }

  const size_t getNoEdges () const
  {
    return _currNoEdges;
  }

  const size_t getMaxNoEdges () const
  {
    return MaxNoEdges;
  }

  void autoReduceNoOfEdges()
  {
    _currNoEdges = 0;
    while (!(std::isnan(_edges[_currNoEdges](0)) && std::isnan(_edges[_currNoEdges](1))) && _currNoEdges<MaxNoEdges)
      ++_currNoEdges;

    // std::cout << _currNoEdges << std::endl;
  }

  Vector<T,2> operator() (unsigned int index) const 
  {
    assert(index<_currNoEdges && "Index exceeds number of Edges, have you called setNoEdges?");
    return _edges[index];
  }

  Vector<T,2>& operator() (unsigned int index)
  {
    assert(index<_currNoEdges && "Index exceeds number of Edges, have you called setNoEdges?");
    return _edges[index];
  }

  CircularListIterator<Vector<T,2>> begin() 
  {
    return CircularListIterator<Vector<T,2>>(_currNoEdges,_edges,0);
  }

  CircularListIterator<Vector<T,2>> end()
  {
    return CircularListIterator<Vector<T,2>>(_currNoEdges,_edges,_currNoEdges);
  }
  
  private:
    Vector<T,2>    _edges[MaxNoEdges]; // = {Vector<T,2>{std::nan(""),std::nan("")}};
    volatile size_t         _currNoEdges = 0;

};

template<typename T>
class CircularListIterator{

  public:

  CircularListIterator(size_t noItems, T* items, unsigned int startingIndex):_items{items},_currentPosition(startingIndex),_noItems(noItems)
  {}

  T& getCurrent()
  {
    return _items[internalModulo(_currentPosition)];
  }
 
  T& getPrevious()
  {
    return _items[internalModulo(_currentPosition-1)];
  }

  T& getNext()
  {
    return _items[internalModulo(_currentPosition+1)];
  }

  CircularListIterator<T>& operator++ ()
  {
    ++_currentPosition;
    return *this;
  }

  CircularListIterator<T>& operator-- ()
  {
    --_currentPosition;
    return *this;
  }

  CircularListIterator<T> operator++ (int)
  {
    ++_currentPosition;
    return *this;
  }

  CircularListIterator<T> operator-- (int)
  {
    --_currentPosition;
    return *this;
  }

  bool operator== (CircularListIterator<T> rhs)
  {
    return (rhs._items==this->_items && rhs._currentPosition==this->_currentPosition && rhs._noItems == this->_noItems) ? true : false ;
  }


  bool operator< (CircularListIterator<T> rhs)
  {
    return (_currentPosition < rhs._currentPosition) ? true : false ;
  }

  bool operator!= (CircularListIterator<T> rhs)
  {
    return !(this->operator==(rhs));
  }

  int operator- (CircularListIterator<T> rhs)
  {
    return _currentPosition - rhs._currentPosition;
  }

  private:

    T*                _items;
    int               _currentPosition = 0;
    int               _noItems = 0;


    int internalModulo (int newValue)
    {
      // std::cout << "new,base,res " << newValue << " " << _noItems << " " <<  (newValue % _noItems +_noItems) % _noItems << std::endl;
      return (newValue % _noItems +_noItems) % _noItems;
    }

};

template<typename T, int NoEdgesP,int NoEdgesQ>
bool calculateConvexPolygonIntersection(Polygon<T,NoEdgesP>& p, Polygon<T,NoEdgesQ>& q, Polygon<T,NoEdgesP+NoEdgesQ>& polygonIntersection )
{
  
  p.autoReduceNoOfEdges();
  q.autoReduceNoOfEdges();
  polygonIntersection.setNoEdges(NoEdgesP+NoEdgesQ);

  CircularListIterator<Vector<T,2>>                    pIterator = p.begin();
  CircularListIterator<Vector<T,2>>                    qIterator = q.begin();
  CircularListIterator<Vector<T,2>>                    polygonIntersectionIterator = polygonIntersection.begin();

  Vector<T,2> pDot,qDot;
  Vector<T,2> intersectionPoint;
  T           crossProduct;
  T           hp_p,hp_q;
  T           loopFirstIntersection{std::nan("")};
  T           insideP{std::nan("")};


  for (int loopcount=0;loopcount <= 2*(NoEdgesP+NoEdgesQ);++loopcount)
  {
    pDot = pIterator.getCurrent()-pIterator.getPrevious();
    qDot = qIterator.getCurrent()-qIterator.getPrevious();

    crossProduct = pDot(1) * qDot(0) - pDot(0) * qDot(1);

    hp_p = pDot(0) * (qIterator.getCurrent()(1) - pIterator.getPrevious()(1)) - 
           pDot(1) * (qIterator.getCurrent()(0) - pIterator.getPrevious()(0));

    hp_q = qDot(0) * (pIterator.getCurrent()(1) - qIterator.getPrevious()(1)) - 
           qDot(1) * (pIterator.getCurrent()(0) - qIterator.getPrevious()(0));

    // lines do cross! -> add intersection point to intersection polygon
    if (crossProduct != 0.0)
    {
      // intersection of two infinte lines formed by the edges
      T lenghtFactorP = 1.0/(-pDot(0)*qDot(1) + qDot(0)*pDot(1)) * 
                        (
                        -qDot(1) * (-(pIterator.getPrevious()(0)-qIterator.getPrevious()(0)))
                        +qDot(0) * (-(pIterator.getPrevious()(1)-qIterator.getPrevious()(1)))
                        );

      T lenghtFactorQ = 1.0/(-pDot(0)*qDot(1) + qDot(0)*pDot(1)) *  
                        (
                        -pDot(1) * (-(pIterator.getPrevious()(0)-qIterator.getPrevious()(0)))
                        +pDot(0) * (-(pIterator.getPrevious()(1)-qIterator.getPrevious()(1)))
                        );
  
      // check if intersection happens inside of the line segment that is formed by the edges of the polygon (as only a intersection of infinite lines was calulated previously)
      if (1.0>=lenghtFactorP && lenghtFactorP>=0.0 && 1.0>=lenghtFactorQ && lenghtFactorQ>=0.0)
      {
        // calculate intersection coordinates
        // Vector<T,2> intersection;
        intersectionPoint(0) = pIterator.getPrevious()(0) + lenghtFactorP * pDot(0);
        intersectionPoint(1) = pIterator.getPrevious()(1) + lenghtFactorP * pDot(1);

        // check if still unset (meaning nan) if yes, then set to the current loop of the first intersection finding
        if (loopFirstIntersection != loopFirstIntersection)
          loopFirstIntersection = loopcount;
      
        //if not the first iteration
        if (loopcount-1 != loopFirstIntersection)
        {
          if (intersectionPoint == polygonIntersection.begin().getCurrent())
            break;
        }
        polygonIntersectionIterator.getCurrent() = intersectionPoint;

        ++polygonIntersectionIterator;

        if (hp_q >= 0.0)
          insideP = 1;
        else 
          insideP = -1;
        
      }
    }

    // advance either p or q by ruleset 1 (generally a advance the outside one)
    if (crossProduct >= 0.0)
    {
      //advannce q
      if (hp_q >= 0.0)
      {

        // check if next point is inside q
        if (insideP == -1)
        {
          polygonIntersectionIterator.getCurrent() = qIterator.getCurrent();
          ++polygonIntersectionIterator;
        }

        ++qIterator;
      }
      
      // advance p
      else
      {

        // check if next point is inside p
        if (insideP == 1)
        {
          polygonIntersectionIterator.getCurrent() = pIterator.getCurrent();
          ++polygonIntersectionIterator;
        }

        ++pIterator;
      }
    
    }
    // advance eithe p or q by ruleset 2
    else 
    {
      //advance p
      if (hp_p >= 0.0)
      {
        //check if next point is inside p
        if (insideP == 1)
        {
          polygonIntersectionIterator.getCurrent() = pIterator.getCurrent();
          ++polygonIntersectionIterator;
        }

        ++pIterator;  
      }

      //advance q
      else
      {
        //check if next point is inside Q
        if (insideP == -1)
        {
          polygonIntersectionIterator.getCurrent() = qIterator.getCurrent();
          ++polygonIntersectionIterator;
        }

        ++qIterator;
      }
    
    }


  }

  // if no intersection was found -> polygons must be enclosed
  if (loopFirstIntersection != loopFirstIntersection){
    
    bool PinsideQ{true};
    bool QinsideP{true};

    // loop over all sides of the polygon q in order to find out if current point of p is inside q -> must be fully enclosed
    for (size_t loopcount = 0;loopcount<NoEdgesQ;++loopcount)
    {
      qDot = qIterator.getCurrent()-qIterator.getPrevious();
   
      hp_q = qDot(0) * (pIterator.getCurrent()(1) - qIterator.getPrevious()(1)) - 
             qDot(1) * (pIterator.getCurrent()(0) - qIterator.getPrevious()(0));

      if (hp_q >= 0.0)
        PinsideQ = (PinsideQ && true);

      else
        PinsideQ = (PinsideQ && false);

      qIterator++;
    }

    //set intersection to P if P is fully inside Q
    if (PinsideQ)
    {
      for (size_t edge=0;edge<NoEdgesP;++edge)
      {
        polygonIntersectionIterator.getCurrent() = pIterator.getCurrent();
        ++polygonIntersectionIterator;
        ++pIterator;
      }
    }

    // if P is not fully inside Q, check if Q is fully inside P
    else 
    {
      for (size_t loopcount = 0; loopcount<NoEdgesP;++loopcount)
      {
        pDot = pIterator.getCurrent() - pIterator.getPrevious();
  
        hp_p = pDot(0) * (qIterator.getCurrent()(1) - pIterator.getPrevious()(1)) - 
               pDot(1) * (qIterator.getCurrent()(0) - pIterator.getPrevious()(0));

        if (hp_p >= 0.0)
          QinsideP = (QinsideP && true);
        else
          QinsideP = (QinsideP && false);

        pIterator++;
      }

      if (QinsideP)
      {
        for (size_t edge=0;edge<NoEdgesQ;++edge)
        {
          polygonIntersectionIterator.getCurrent() = qIterator.getCurrent();
          ++polygonIntersectionIterator;
          ++qIterator;
        }
      }
      else
        // printf("No intersection polygon, neither PinsideQ nor QinsideP");
        polygonIntersection.autoReduceNoOfEdges();
        return false;
    }


  }

  polygonIntersection.autoReduceNoOfEdges();

  // for (auto polygonIterator = polygonIntersection.begin(); polygonIterator!=polygonIntersection.end();polygonIterator++)
  // {
    // std::cout << "point " <<  polygonIterator.getCurrent()(0) << " " << polygonIterator.getCurrent()(1) << std::endl;
  // }
  // std::cout << "endofFunction" << std::endl;


  return true;
}

template<typename T, int NoEdges>
T calculateAreaOfConvexPolygon(Polygon<T,NoEdges> poly)
{
	T area=0.0;

  assert(poly.getNoEdges() > 2 && "Convex polygon has 2 or less points -> no area that can be calculated" );
  for (auto polyIterator = poly.begin(); polyIterator!=poly.end();polyIterator++)
  {
    area += polyIterator.getCurrent()(0) * polyIterator.getNext()(1) 
           -polyIterator.getCurrent()(1) * polyIterator.getNext()(0);
  }
	area *= 0.5;
	return area;
}

}
#endif
