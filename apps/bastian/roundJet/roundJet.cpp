/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
// #include "boundary/boundaryCondition3D.h"
// #include "boundary/boundaryCondition3D.hh"
// #include "boundary/boundaryPostProcessors3D.h"
// #include "boundary/boundaryPostProcessors3D.hh"
// #include "dynamics/latticeDescriptors.h"
// #include "dynamics/smagorinskyBGKdynamics.h"
// #include "dynamics/dynamics.h"
// #include "core/blockLattice3D.h"
// #include "core/singleton.h"
// #include "core/unitConverter.h"
// #include "io/blockVtkWriter3D.h"
// #include "dynamics/latticeDescriptors.hh"
// #include "dynamics/smagorinskyBGKdynamics.hh"
// #include "dynamics/dynamics.hh"
// #include "core/blockLattice3D.hh"
// #include "core/unitConverter.hh"
// #include "io/blockVtkWriter3D.hh"
// #include "functors/lattice/blockLatticeLocalF3D.h"
// #include "functors/lattice/blockLatticeLocalF3D.hh"
// #include "utilities/timer.h"
// #include "utilities/timer.hh"
#include <fstream>
#include <cmath>
#include <ctime>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <random>
#include "polygonIntersection.cpp"
#include "communication/CPUGPUDataExchange.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/writeFunctorToGPU.h"

#define ROTORRES 360
#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template <typename T, unsigned int Direction1, unsigned int Direction2, unsigned int Direction3, class Descriptor>
__global__ void sampleKernel(T* deviceSample, size_t samplingSize, unsigned int sampleSize, size_t nx, size_t ny, size_t nz, size_t pos1, size_t pos2, size_t pos3,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData)
{
    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;
    if(threadIndex >= samplingSize)
        return;

    const T normalizer = sqrt(static_cast<T>(Direction1*Direction1+Direction2*Direction2+Direction3*Direction3));

    size_t index = util::getCellIndex3D(pos1,pos2,pos3,ny,nz);
    index += static_cast<size_t>(threadIndex*Direction1/normalizer*ny*nz)
            +static_cast<size_t>(threadIndex*Direction2/normalizer*nz)
            +static_cast<size_t>(threadIndex*Direction3/normalizer);

    for(unsigned int iDim = 0; iDim < Descriptor::d; ++iDim)
    {
        deviceSample[threadIndex+iDim*samplingSize/3] += (cellData[Descriptor::uIndex+iDim][index]-deviceSample[threadIndex+iDim*samplingSize/3])/sampleSize;
    }


}

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

void MultipleSteps(const double simTime, const double sizeInPlane,
        const double distanceAbove, const double distanceBelow, const int resolution,
        double smagoConst, double outputTime, std::string printFirst)
{

  int iXLeftBorder = 0;
  int iXRightBorder = static_cast<int>(std::nearbyint( sizeInPlane*resolution * 0.5f ) * 2.0f)-1;
  int iYBottomBorder = 0;
  int iYTopBorder = static_cast<int>(std::nearbyint( sizeInPlane*resolution * 0.5f ) * 2.0f)-1;
  int iZFrontBorder = 0;
  int iZBackBorder = static_cast<int>(std::nearbyint( (distanceAbove+distanceBelow)*resolution * 0.5f ) * 2.0f)-1;


  const int center = (iXRightBorder+1)/2;
  std::vector<int> jetForceXLimit = {center-resolution/2-1, center+resolution/2};
  std::vector<int> jetForceYLimit = {center-resolution/2-1, center+resolution/2};
  std::vector<int> jetForceZPosition = {static_cast<int>(distanceAbove*resolution)};

  std::cout << "Field size (X,Y,Z): " << iXRightBorder+1 << "," << iYTopBorder+1 << "," << iZBackBorder+1 << std::endl;

  std::cout << "X limits: " << jetForceXLimit[0] << " to " << jetForceXLimit[1] << std::endl;
  std::cout << "Y limits: " << jetForceYLimit[0] << " to " << jetForceYLimit[1] << std::endl;
  std::cout << "Z position: " << jetForceZPosition[0] << std::endl;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          resolution
          ,0.3*1.0/std::sqrt(3)
          ,1.
          ,15.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  T deltaX = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics);


  OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition3D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

 // boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
 // boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
 // boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
 // boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
 boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );
   // boundaryCondition->addPressureBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addPressureBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );
//
 // boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
 // boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
 // boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
 // boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

 // boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
 // boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
 // boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
 // boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

 // boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
 // boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
 // boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
 // boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

 // boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
 // boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
 // boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
 // boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

 // boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
 // boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
 // boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
 // boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

  lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1,     &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1,      &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1,      &instances::getBounceBack<T,Lattice>() );
//  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder ,      &instances::getBounceBack<T,Lattice>() );
//  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  ,      &instances::getBounceBack<T,Lattice>() );

  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>() );

  lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>() );

  lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>() );
  lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>() );

  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

  lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());


  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {

            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
    }
  }

  T radius = 0.5/deltaX; // lattice Coordinate

  T centerIntersect[] = {center-0.5,center-0.5}; // lattice Coordinate

  polyInter::Polygon<T,ROTORRES> rotor;
  rotor.setNoEdges(ROTORRES);
  for (auto polygonIterator = rotor.begin();polygonIterator!=rotor.end();polygonIterator++)
  {
    polygonIterator.getCurrent()(0) = center + cos(2*M_PI*(polygonIterator-rotor.begin())/(rotor.getNoEdges())) * radius; // x coordinate
    polygonIterator.getCurrent()(1) = center + sin(2*M_PI*(polygonIterator-rotor.begin())/(rotor.getNoEdges())) * radius; // y coordinate
  }

  WriteFunctorByRangeToGPU3D<T,Lattice,ForceFunctor> writer(lattice,jetForceXLimit[0],jetForceXLimit[1],jetForceYLimit[0],
          jetForceYLimit[1],jetForceZPosition[0],jetForceZPosition[0]);

  T areaTotal = 0;
  std::vector<T> weights;

 for (int iX = jetForceXLimit[0]; iX <= jetForceXLimit[1]; ++iX)
 {
     for (int iY = jetForceYLimit[0]; iY <= jetForceYLimit[1]; ++iY)
     {
         for (int iZ = jetForceZPosition[0]; iZ <= jetForceZPosition[0]; ++iZ)
         {

            polyInter::Polygon<T,4> cell;
            cell.setNoEdges(4);
            cell(0)(0) = iX - 0.5;
            cell(0)(1) = iY - 0.5;

            cell(1)(0) = iX + 0.5;
            cell(1)(1) = iY - 0.5;

            cell(2)(0) = iX + 0.5;
            cell(2)(1) = iY + 0.5;

            cell(3)(0) = iX - 0.5;
            cell(3)(1) = iY + 0.5;

            polyInter::Polygon<T,ROTORRES+4> intersection;
            intersection.setNoEdges(ROTORRES+4);

            T area = 0.0;

            if (polyInter::calculateConvexPolygonIntersection(cell,rotor,intersection))
            {

               area = polyInter::calculateAreaOfConvexPolygon(intersection);
               areaTotal += area;
               writer.setByLocalIndex(iX-jetForceXLimit[0],iY-jetForceYLimit[0],0)[0] = 0.0;
               writer.setByLocalIndex(iX-jetForceXLimit[0],iY-jetForceYLimit[0],0)[1] = 0.0;
               writer.setByLocalIndex(iX-jetForceXLimit[0],iY-jetForceYLimit[0],0)[2] = area*converter.getLatticeForce(50.*deltaX*deltaX/1.225);
            }
           weights.push_back(area);

         }
     }
 }

 std::cout << "Area cumulative: " << areaTotal << "(" << 0.5*0.5*M_PI/(deltaX*deltaX) << ")" << std::endl;


  std::string name;
  std::string sizeInPlaneStr = std::to_string(sizeInPlane);
  std::string distanceAboveStr = std::to_string(distanceAbove);
  std::string distanceBelowStr = std::to_string(distanceBelow);
  std::string smagoConstStr = std::to_string(smagoConst);

  std::replace(sizeInPlaneStr.begin(),sizeInPlaneStr.end(),'.','_');
  std::replace(distanceAboveStr.begin(),distanceAboveStr.end(),'.','_');
  std::replace(distanceBelowStr.begin(),distanceBelowStr.end(),'.','_');
  std::replace(smagoConstStr.begin(),smagoConstStr.end(),'.','_');

  name = "roundJet_";
  name += "R_" + std::to_string(resolution) + "_XY_" + sizeInPlaneStr + "_Z1_"
           + distanceAboveStr + "_Z2_" + distanceBelowStr + "_SC_" + smagoConstStr;

  BlockVTKwriter3D<T> vtkWriter( name );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);


  std::vector<T> downStreamPosition;
  const unsigned int numPos = 50;

  for(unsigned int iPos = 0; iPos <= numPos; ++iPos)
      downStreamPosition.push_back(0.1+(0.9-0.1)/numPos*iPos);

  std::vector<thrust::device_vector<T>> deviceSample(downStreamPosition.size(),thrust::device_vector<T>{(iXRightBorder+1)*3});
  std::vector<thrust::host_vector<T>> hostSample(deviceSample.size(),thrust::host_vector<T>{(iXRightBorder+1)*3});
  std::vector<T*> deviceArray;

  for(auto& iSample : deviceSample)
      deviceArray.push_back(thrust::raw_pointer_cast(&iSample[0]));

  size_t samplingSize = deviceSample[0].size();
  unsigned int sampleSize = 1;

  std::string outDirectory = "/scratch/ga69kiq/roundJet/";

  singleton::directories().setOutputDir(outDirectory);

  std::vector<std::ofstream> outFile;

  for(T iPos : downStreamPosition)
  {
      std::string downStreamPositionStr = std::to_string(iPos);
      std::replace(downStreamPositionStr.begin(),downStreamPositionStr.end(),'.','_');
      outFile.push_back(std::ofstream(outDirectory+"data/"+name+"_"
              +downStreamPositionStr+".txt"));
  }

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);

  lattice.copyDataToGPU();
  writer.transferAndWrite();
  lattice.copyDataToCPU();

  if(printFirst == "true")
	  vtkWriter.write(0);

  std::srand(std::time(nullptr));
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<T> dis(-0.1,0.1);

  std::cout << "Start simulation" << std::endl;

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {
	  lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);

	  if(iSteps%converter.getLatticeTime(outputTime) == 0)
	  {
		  lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }

	  if(iSteps > converter.getLatticeTime(20))
	  {
	      for(unsigned int iSample = 0; iSample < deviceArray.size(); ++iSample)
	          sampleKernel<T,1,0,0,Lattice<T>><<<1,iXRightBorder+1>>>(deviceArray[iSample], samplingSize,sampleSize, lattice.getNx(),lattice.getNy(),lattice.getNz(),
	                      0, lattice.getNy()/2, jetForceZPosition[0]+static_cast<size_t>(distanceBelow*resolution*downStreamPosition[iSample]),
	                      lattice.cellData->gpuGetFluidData());
	      ++sampleSize;
	  }

	  cudaDeviceSynchronize();

	  auto elements = weights.begin();
      for (int iX = jetForceXLimit[0]; iX <= jetForceXLimit[1]; ++iX)
          for (int iY = jetForceYLimit[0]; iY <= jetForceYLimit[1]; ++iY)
              for (int iZ = jetForceZPosition[0]; iZ <= jetForceZPosition[0]; ++iZ)
              {
                T disturbance = dis(gen);
                writer.setByLocalIndex(iX-jetForceXLimit[0],iY-jetForceYLimit[0],0)[0] =
                        converter.getLatticeForce(50.*deltaX*deltaX/1.225)*elements[0]*(disturbance);
                writer.setByLocalIndex(iX-jetForceXLimit[0],iY-jetForceYLimit[0],0)[1] =
                        converter.getLatticeForce(50.*deltaX*deltaX/1.225)*elements[0]*(disturbance);
                writer.setByLocalIndex(iX-jetForceXLimit[0],iY-jetForceYLimit[0],0)[2] =
                        converter.getLatticeForce(50.*deltaX*deltaX/1.225)*elements[0]*(1+disturbance);
                ++elements;
              }
      writer.transferAndWrite();

  }

  for(unsigned int iSample = 0; iSample < hostSample.size(); ++iSample)
  {
      hostSample[iSample] = deviceSample[iSample];
      unsigned int const positionOffset = hostSample[iSample].size()/3;
      for(unsigned int iSamplePoint = 0; iSamplePoint < positionOffset; ++iSamplePoint)
          outFile[iSample] << hostSample[iSample][iSamplePoint] << "," << hostSample[iSample][iSamplePoint+positionOffset]
                           << "," << hostSample[iSample][iSamplePoint+2*positionOffset] << std::endl;
  }


  timer.stop();

  delete boundaryCondition;

}

int main(int argc, char** argv)
{
    int resolution = std::stoi(argv[1],NULL);
    double sizeXY = std::stod(argv[2],NULL);
    double heightAbove = std::stod(argv[3],NULL);
    double heightBelow = std::stod(argv[4],NULL);
    double smagoConst = std::stod(argv[5],NULL);

    const double simTime = std::stod(argv[6],NULL);
    double outputTime = std::stod(argv[7],NULL);
	std::string printFirst = argv[8];

    std::cout << "Resolution: " << resolution << std::endl;
    std::cout << "sizeXY: " << sizeXY << std::endl;
    std::cout << "heightAbove: " << heightAbove << std::endl;
    std::cout << "heightBelow: " << heightBelow << std::endl;
    std::cout << "smagoConst: " << smagoConst << std::endl;
    std::cout << "simTime: " << simTime << std::endl;
    std::cout << "outputTime: " << outputTime << std::endl;

    MultipleSteps(simTime,sizeXY,heightAbove,heightBelow,resolution,smagoConst,outputTime,printFirst);
	return 0;
}
