/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "../../../src/contrib/communication/NetworkInterface.h"
#include "../../../src/contrib/communication/NetworkInterface.cpp"
#include "../../../src/contrib/communication/NetworkDataStructures.h"
#include "../testFlightmechanicsCommunication/HarmonicLinear/helicopterStructure.h"
#include "../../../src/communication/CPUGPUDataExchange.h"
#include "../../../src/communication/readFunctorFromGPU.h"
#include "../../../src/communication/writeFunctorToGPU.h"
#include <chrono>
#include <thread>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, class BlockLattice>
int addFuselageCells(int resolution, BlockLattice &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("/scratch/BHorvat/openlb/apps/bastian/testFlightmechanicsCommunication/Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
//            std::cout << position[0] << "," << position[1] << "," << position[2] << std::endl;
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 1;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

template<typename T, template <typename> class Lattice, class BlockLattice>
void defineBoundaries(BlockLattice &lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);


      boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
      boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

      boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

      boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

      boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

      boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

      boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

}

using ReceiveData = ThrustAtRotorsHarmonic<10>;
using SendData = VelocityAtRotorsLinear;

template<class NetworkCommunicator>
void MultipleSteps(const double simTime, int iXRightBorderArg, NetworkCommunicator& networkCommunicator)
//void MultipleSteps(const double simTime, int iXRightBorderArg)
{

  int iXLeftBorder = 0;
  int iXRightBorder = iXRightBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = iXRightBorderArg;
  int iZFrontBorder = 0;
  int iZBackBorder = iXRightBorderArg;
  T rotorRadius = HelicopterData<T>::rotorRadius;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,4.*rotorRadius
          ,50.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  T conversionVelocity = converter.getConversionFactorVelocity();

  T deltaX = converter.getConversionFactorLength();

  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";

  T velocity[3] = {0.0,0.0,0.0};

  ConstVelocityProviderF<T,Lattice> provider(velocity);
  ExternalFieldALE<T,Lattice,ConstVelocityProviderF> externalFieldALE(provider);
  BlockLatticeALE3D<T, Lattice, ExternalFieldALE<T,Lattice,ConstVelocityProviderF>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1,
          {0, 0, 0}, &bulkDynamics,externalFieldALE);
  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);


  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {iXRightBorder/2.,iYTopBorder/2.,static_cast<T>(iZBackBorder+1)};
  size_t numCells = addFuselageCells(iXRightBorder+1,lattice,rotorPosition[2]);

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::vector<int> rotorLimitsX = {static_cast<int>(std::ceil(rotorPosition[0]-rotorRadius/spacing))-1,
          static_cast<int>(std::floor(rotorPosition[0]+rotorRadius/spacing))+1};
  std::vector<int> rotorLimitsY = {static_cast<int>(std::ceil(rotorPosition[1]-rotorRadius/spacing))-1,
          static_cast<int>(std::floor(rotorPosition[1]+rotorRadius/spacing))+1};

  std::cout << "Rotor Position: " << rotorPosition[0] << "," << rotorPosition[1] << "," << rotorPosition[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotorLimitsX[0] << " to " << rotorLimitsX[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotorLimitsY[0] << " to " << rotorLimitsY[1] << std::endl;

  int numberRotorCells = (rotorLimitsX[1]-rotorLimitsX[0]+1)*(rotorLimitsY[1]-rotorLimitsY[0]+1);

  std::vector<std::vector<int>> rotorCellPositionsRelativ(numberRotorCells, std::vector<int>(4));

  std::vector<T> rotorCellPositionRadial(numberRotorCells);
  std::vector<T> rotorCellPositionAzimuth(numberRotorCells);
  std::vector<T> rotorCellWeight(numberRotorCells);

  std::cout << "Define rotor details... ";

  T fullRotorCells = HelicopterData<T>::template calculateCellPosition<Lattice>(rotorPosition,rotorCellPositionRadial,rotorCellPositionAzimuth,rotorCellWeight,rotorCellPositionsRelativ,
          rotorLimitsX, rotorLimitsY, spacing, lattice);

  std::cout << "Finished!" << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  lattice.updateAnchorPoint(PointXd<T,Lattice>{rotorPosition[0],rotorPosition[1],rotorPosition[2]});

  T u[3] = {0,0,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
  std::cout << "Finished!" << std::endl;

  WriteFunctorByRangeToGPU3D<T,Lattice,ForceFunctor> writer(lattice,rotorLimitsX[0],rotorLimitsX[1],rotorLimitsY[0],rotorLimitsY[1],rotorPosition[2],rotorPosition[2]);
  ReadFunctorByRangeFromGPU3D<T,Lattice,VelocityFunctor> reader(lattice,rotorLimitsX[0],rotorLimitsX[1],rotorLimitsY[0],rotorLimitsY[1],rotorPosition[2],rotorPosition[2]);

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  ReceiveData dataReceive;
  SendData dataSend;

  std::string name;
  std::string directory = "/scratch/BHorvat/speedTest/";
  name = "speedTest_X";
  name += std::to_string(iXRightBorderArg+1);

  std::ofstream fileWriter(directory + "result.txt");

  if(fileWriter)
      std::cout << "File open" << std::endl;

  std::cout << "Starting time simulation" << std::endl;
  std::cout << "Steps: " << converter.getLatticeTime(simTime) << std::endl;
  std::cout << "time step size: " << converter.getPhysDeltaT()*1e6 << std::endl;

  auto start = std::chrono::system_clock::now();
  auto startSend = std::chrono::system_clock::now();
  auto endReceive = std::chrono::system_clock::now();
//
//  for(unsigned int timeStep = 0; timeStep <= converter.getLatticeTime(simTime); ++timeStep)
  for(unsigned int timeStep = 0; timeStep <= 10; ++timeStep)
  {

      auto start = std::chrono::system_clock::now();
      auto end = start;

      for(auto position : rotorCellPositionsRelativ)
      {
          writer.setByLocalIndex(position[0],position[1],position[2])[0] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[1] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[2] = 0;
      }

      start = std::chrono::system_clock::now();
      networkCommunicator.recieve_data(dataReceive);
      endReceive = std::chrono::system_clock::now();
      end = std::chrono::system_clock::now();

      std::cout << "Gensim step took: " << std::chrono::duration_cast<std::chrono::microseconds>(endReceive - startSend).count() << " µs" << std::endl;
      std::cout << "Receive took: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

      PoseXd<T, Lattice> position {{0, 0, 0}, {0, 0, 0}};
      PoseXd<T, Lattice> movement {{0,0,0}, {0,0,0}};

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          movement.position_[iDim]    = dataReceive.velocities[iDim]/converter.getConversionFactorVelocity();
          movement.orientation_[iDim] = dataReceive.rotatorics[iDim]*converter.getConversionFactorTime();
          position.position_[iDim]    = dataReceive.positions[iDim]/converter.getConversionFactorLength();
          position.orientation_[iDim] = dataReceive.attitudes[iDim];
      }

      for(auto pos : rotorCellPositionsRelativ)
      {
          std::vector<T> thrustPerCell(3,0);
          int index = pos[3];
          for (unsigned int iHarmonics=0; iHarmonics<10; ++iHarmonics)
          {
              thrustPerCell[0] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosX[iHarmonics];
              thrustPerCell[0] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinX[iHarmonics];

              thrustPerCell[1] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosY[iHarmonics];
              thrustPerCell[1] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinY[iHarmonics];

              thrustPerCell[2] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosZ[iHarmonics];
              thrustPerCell[2] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinZ[iHarmonics];
          }

          thrustPerCell[0] += dataReceive.presMainMeanX;
          thrustPerCell[1] += dataReceive.presMainMeanY;
          thrustPerCell[2] += dataReceive.presMainMeanZ;

          thrustPerCell[0] *= rotorCellWeight[index];
          thrustPerCell[1] *= rotorCellWeight[index];
          thrustPerCell[2] *= rotorCellWeight[index];

          writer.setByLocalIndex(pos[0], pos[1], pos[2])[0] = converter.getLatticeForce(thrustPerCell[0]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[1] = converter.getLatticeForce(thrustPerCell[1]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[2] = converter.getLatticeForce(thrustPerCell[2]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
      }

//      writer.transferAndWrite();

      start = std::chrono::system_clock::now();
      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
      end = std::chrono::system_clock::now();
      std::cout << "Collide and stream took: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;

      start = std::chrono::system_clock::now();
      lattice.moveMeshGPU(movement, position);
      end = std::chrono::system_clock::now();
      std::cout << "Move mesh took: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;
//
//      reader.readAndTransfer();
//
//
      for(auto pos : rotorCellPositionsRelativ)
      {
          size_t index = pos[3];
          std::vector<T> velocity(3,0);

          if(rotorCellWeight[index] == 1)
          {
              for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
              {
                  velocity[iDim] = converter.getPhysVelocity(reader.getByLocalIndex(pos[0],pos[1],pos[2])[iDim]);
                  dataSend.velocityMainMean[iDim] += velocity[iDim]/fullRotorCells;
                  dataSend.velocityMainSin[iDim] += std::sin(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
                  dataSend.velocityMainCos[iDim] += std::cos(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
              }
          }
      }

      startSend = std::chrono::system_clock::now();

      networkCommunicator.send_data(dataSend);

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          dataSend.velocityMainMean[iDim] = 0;
          dataSend.velocityMainSin[iDim] = 0;
          dataSend.velocityMainCos[iDim] = 0;
      }

      std::cout << std::endl;

  }

  auto end = std::chrono::system_clock::now();

  std::cout << "Elapsed time: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;
  std::cout << "Simulated time: " << simTime*1000 << " µs" << std::endl;

  cudaDeviceSynchronize();

}

int main()
{
    NetworkInterfaceTCP<ReceiveData,SendData> networkCommunicator(INPUTPORT,OUTPUTIP,OUTPUTPORT,NETWORKBUFFERSIZE,true);

    const double simTime = 10.0;

    MultipleSteps(simTime,127,networkCommunicator);
//    MultipleSteps(simTime,31);



	return 0;
}
