#define D2Q9LATTICE 1
typedef double T;


#include "olb2D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb2D.hh"   // include full template code
#endif
#include <iostream>

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D2Q9Descriptor

#ifdef ENABLE_CUDA

    __device__ bool d_result;

	__global__ void checkCellIDs(size_t* cellIDs, size_t assertVal1, size_t assertVal2, size_t assertVal3,
			size_t numberValues)
	{
		for(size_t i=0; i<numberValues; ++i)
		{
//			printf("%llu\n", (long long unsigned) cellIDs[i]);
			if(cellIDs[i] != assertVal1 && cellIDs[i] != assertVal2 && cellIDs[i] != assertVal3 )
			{
				d_result=false;
				return;
			}
		}
		d_result=true;

	}
#endif

int main()
{
	const int omega = 0.6;

	  ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  bulkDynamics (
			  omega, instances::getBulkMomenta<T,DESCRIPTOR>()
	  );

	BlockLattice2D<T, DESCRIPTOR> lattice( 5, 5, &bulkDynamics );
	const int nx = lattice.getNx();
	const int ny = lattice.getNy();

#ifdef ENABLE_CUDA
	GPUHandler<T,DESCRIPTOR>::get();
#endif

	OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
	boundaryCondition = createInterpBoundaryCondition2D<T,DESCRIPTOR,ConstRhoBGKdynamics>( lattice );
	boundaryCondition->addVelocityBoundary0N(   0,   0,   1,ny-2, omega );
	boundaryCondition->addVelocityBoundary0P( nx-1,nx-1,   1,ny-2, omega );
	boundaryCondition->addVelocityBoundary1N(   1,nx-2,   0,   0, omega );
	boundaryCondition->addVelocityBoundary1P(   1,nx-2,ny-1,ny-1, omega );

	boundaryCondition->addExternalVelocityCornerNN(   0,   0, omega );
	boundaryCondition->addExternalVelocityCornerNP(   0,ny-1, omega );
	boundaryCondition->addExternalVelocityCornerPN( nx-1,   0, omega );
	boundaryCondition->addExternalVelocityCornerPP( nx-1,ny-1, omega );

	lattice.initDataArrays();

#ifdef ENABLE_CUDA
	lattice.copyDataToGPU();
#endif

	auto cellIds = lattice.getDataHandler(lattice.getDynamics(0,1))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==5 || i==10 || i==15) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(1,0))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==1 || i==2 || i==3) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(1,ny-1))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==21 || i==22 || i==23) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(nx-1,1))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==9 || i==14 || i== 19) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(nx-1,0))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==4) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(0,ny-1))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==20) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(0,0))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==0) );

	cellIds = lattice.getDataHandler(lattice.getDynamics(nx-1, ny-1))->getCellIDs();
	for(auto i : cellIds)
		assert( (i==24) );

#ifdef ENABLE_CUDA
	auto cellIdMap = GPUHandler<T,DESCRIPTOR>::get().getDynamicsCellIDsMap();

	bool result = false;
	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(0,1))->second, 5, 10,15,
			lattice.getDataHandler(lattice.getDynamics(0,1))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(1,0))->second, 1, 2,3,
			lattice.getDataHandler(lattice.getDynamics(1,0))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(1,ny-1))->second, 21, 22,23,
			lattice.getDataHandler(lattice.getDynamics(1,ny-1))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(nx-1,1))->second, 9, 14,19,
			lattice.getDataHandler(lattice.getDynamics(nx-1,1))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(nx-1,0))->second, 4,999,999,
			lattice.getDataHandler(lattice.getDynamics(nx-1,0))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(0,ny-1))->second, 20,999,999,
			lattice.getDataHandler(lattice.getDynamics(0,ny-1))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(0,0))->second, 0,999,999,
			lattice.getDataHandler(lattice.getDynamics(0,0))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	checkCellIDs<<<1,1>>>(cellIdMap.find(lattice.getDynamics(nx-1,ny-1))->second, 24,999,999,
			lattice.getDataHandler(lattice.getDynamics(nx-1,ny-1))->getCellIDs().size());
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

#endif

	return 0;
}
