using T = double;

#include "olb3D.h"
#include "olb3D.hh"


using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;

#define DESCRIPTOR ForcedD3Q19Descriptor
#define GridSize 3

int main()
{

    ForcedBGKdynamics<T,DESCRIPTOR> bulkDynamics( 0.7, instances::getBulkMomenta<T,DESCRIPTOR>() );
    BGKdynamics<T,DESCRIPTOR> boundaryDynamics( 0.7, instances::getBulkMomenta<T,DESCRIPTOR>() );

    BlockGeometry3D<T> blockGeometry( 0,0,0,1,GridSize,GridSize,GridSize );
    BlockLattice3D<T, DESCRIPTOR> lattice(GridSize, GridSize, GridSize, blockGeometry);

    for(unsigned int iX=0; iX<GridSize; ++iX)
        for(unsigned int iY=0; iY<GridSize; ++iY)
            for(unsigned int iZ=0; iZ<GridSize; ++iZ)
            {
                lattice.get(iX,iY,iZ).defineDynamics(&boundaryDynamics);
                T u[3] = {0};
                lattice.get(iX,iY,iZ).defineRhoU(1,u);
                lattice.get(iX,iY,iZ).iniEquilibrium(1,u);
            }


    lattice.get(1,1,1).defineDynamics(&bulkDynamics);

    T force[3] = {1,0,0};
    lattice.get(1,1,1).defineExternalField(DESCRIPTOR<T>::ExternalField::forceBeginsAt,
                  DESCRIPTOR<T>::ExternalField::sizeOfForce, force);

    for(unsigned int iStep = 0; iStep < 100; ++iStep)
    {
        lattice.collideAndStream();
        T force[3] = {0,0,0};
        lattice.get(1,1,1).defineExternalField(DESCRIPTOR<T>::ExternalField::forceBeginsAt,
                      DESCRIPTOR<T>::ExternalField::sizeOfForce, force);
    }

    return 0;
}
