#define D3Q19LATTICE 1
typedef double T;


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D3Q19Descriptor

int main()
{
    const int omega = 0.6;

    ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  bulkDynamics (
            omega, instances::getBulkMomenta<T,DESCRIPTOR>()
    );

    BlockLattice3D<T, DESCRIPTOR> lattice( 5, 5, 5, &bulkDynamics );

    return 0;
}
