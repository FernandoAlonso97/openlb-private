#define D2Q9LATTICE 1
typedef double T;


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif
#include <iostream>

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D3Q19Descriptor

// From master
template<typename T, template<typename U> class Lattice, int direction, int orientation>
T velocityBMRho( CellView<T,Lattice> const& cell, const T* u )
{
  std::vector<int> const& onWallIndices
    = util::subIndex<Lattice<T>, direction, 0>();

  std::vector<int> const& normalIndices
    = util::subIndex<Lattice<T>, direction, orientation>();

  T rhoOnWall = T();
  for (auto & e : onWallIndices) {
    rhoOnWall += cell[e];
  }

  T rhoNormal = T();
  for (auto & e : normalIndices) {
    rhoNormal += cell[e];
  }

  T rho =((T)2*rhoNormal+rhoOnWall+(T)1) /
         ((T)1+(T)orientation*u[direction]);

  return rho;
}


int main()
{
	CellDataArray<T,DESCRIPTOR> cellDataArray;

	ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  bulkDynamics (
	    0.7, instances::getBulkMomenta<T,DESCRIPTOR>());

	T** data = new T*[DESCRIPTOR<T>::q+DESCRIPTOR<T>::d+1];

	for(int i=0; i<DESCRIPTOR<T>::q; ++i)
	{
		data[i] = new T[1];
		data[i][0] = i+1;
		cellDataArray.data[i] = new T[0];
		cellDataArray.data[i][0] = data[i][0];
	}

	CellView<T,DESCRIPTOR> cellView(&bulkDynamics, cellDataArray);

	T u[DESCRIPTOR<T>::d];
	for(int i=0; i<DESCRIPTOR<T>::d; ++i)
	{
		u[i] = i+2;
		data[DESCRIPTOR<T>::uIndex+i] = new T[1];
		data[DESCRIPTOR<T>::uIndex+i][0] = u[i];
	}

	T** dummyMomentaData;

	assert( (velocityBMRho<T,DESCRIPTOR,1,1>(cellView,u) == VelocityBM<T,DESCRIPTOR,1,1,0>::computeRho(data,0,dummyMomentaData,0))   );
	assert( (velocityBMRho<T,DESCRIPTOR,1,-1>(cellView,u) == VelocityBM<T,DESCRIPTOR,1,-1,0>::computeRho(data,0,dummyMomentaData,0)) );
	assert( (velocityBMRho<T,DESCRIPTOR,1,0>(cellView,u) == VelocityBM<T,DESCRIPTOR,1,0,0>::computeRho(data,0,dummyMomentaData,0))   );
	assert( (velocityBMRho<T,DESCRIPTOR,0,-1>(cellView,u) == VelocityBM<T,DESCRIPTOR,0,-1,0>::computeRho(data,0,dummyMomentaData,0)) );
	assert( (velocityBMRho<T,DESCRIPTOR,0,1>(cellView,u) == VelocityBM<T,DESCRIPTOR,0,1,0>::computeRho(data,0,dummyMomentaData,0))   );
	assert( (velocityBMRho<T,DESCRIPTOR,0,0>(cellView,u) == VelocityBM<T,DESCRIPTOR,0,0,0>::computeRho(data,0,dummyMomentaData,0))   );

	return 0;
}
