/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define D3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice D3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

void MultipleSteps()
{
  int iXLeftBorder = 0;
  int iXRightBorder = 3;
  int iYBottomBorder = 0;
  int iYTopBorder = 3;
  int iZFrontBorder = 0;
  int iZBackBorder = 3;

  T omega = 0.7;
  ConstRhoBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,
      instances::getBulkMomenta<T, Lattice>());
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1, &bulkDynamics);

  OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition3D<T,Lattice,
    ConstRhoBGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addVelocityBoundary0N(iXLeftBorder   ,iXLeftBorder,  iYBottomBorder+1 ,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addVelocityBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addVelocityBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addVelocityBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addVelocityBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
  boundaryCondition->addVelocityBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

  boundaryCondition->addExternalVelocityEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalVelocityEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalVelocityEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
  boundaryCondition->addExternalVelocityEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

  boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder,iXLeftBorder,iYBottomBorder+1,iYTopBorder-1,iZBackBorder,iZBackBorder, omega );
  boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder+1,iYTopBorder-1,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalVelocityEdge1PP(iXRightBorder,iXRightBorder,iYBottomBorder+1,iYTopBorder-1,iZBackBorder ,iZBackBorder , omega );
  boundaryCondition->addExternalVelocityEdge1NP(iXRightBorder ,iXRightBorder ,iYBottomBorder+1,iYTopBorder-1,iZFrontBorder ,iZFrontBorder , omega );

  boundaryCondition->addExternalVelocityEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalVelocityEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalVelocityEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

	boundaryCondition->addExternalVelocityCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
	boundaryCondition->addExternalVelocityCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
	boundaryCondition->addExternalVelocityCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
	boundaryCondition->addExternalVelocityCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
	boundaryCondition->addExternalVelocityCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
	boundaryCondition->addExternalVelocityCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
	boundaryCondition->addExternalVelocityCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);
	boundaryCondition->addExternalVelocityCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

  lattice.initDataArrays();

  // setBoundaryValues
  for (int iX = 0; iX <= iXRightBorder; ++iX) {
    for (int iY = 0; iY <= iYTopBorder; ++iY) {
      for (int iZ = 0; iZ<=iZBackBorder; ++iZ){
        T vel[] = { 0.0, 0.0, 0.0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
      }
    }
  }

  T vel[] = { 1.0, 0.0, 0.0};
  for (int iX = iXLeftBorder + 1; iX < iXRightBorder; ++iX) {
    for (int iY = iYBottomBorder +1; iY < iYTopBorder; ++iY)
    {
      lattice.defineRhoU(iX, iX, iY, iY, iZBackBorder, iZBackBorder, 1.0, vel);
      lattice.iniEquilibrium(iX, iX, iY, iY, iZBackBorder, iZBackBorder, 1.0, vel);
    }
  }

  BlockVTKwriter3D<T> vtkWriter( "cavity3DMini" );
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);

  TempFunctional f;

  lattice.forAll(f);
  std::cout << "=============================" << std::endl;

  vtkWriter.addFunctor( velocityFunctor );
  vtkWriter.addFunctor( densityFunctor );
  vtkWriter.write(0);

  for(int i=1; i<=1; ++i)
  {
	  lattice.collideAndStream<ConstRhoBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();
	  if(i%1 == 0)
	  {
//		  vtkWriter.write(i);
		  lattice.forAll(f);
		  std::cout << "=============================" << std::endl;
	  }
  }

}

int main()
{
	MultipleSteps();
	return 0;
}
