/*
 * cellBlockDataSimpleStreaming.cpp
 *
 *  Created on: May 28, 2019
 *      Author: ga69kiq
 */

#define FORCEDD3Q19LATTICE
typedef double T;

#include "core/cellBlockDataSimpleStream.h"
#include "core/cellBlockDataSimpleStream.hh"
#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "dynamics/lbHelpers.h"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "dynamics/dynamicsDataHandlerNew.h"
#include "core/util.h"
#include "core/config.h"
#include "dynamics/dynamicsKernels.h"


#define LATTICE descriptors::ForcedD3Q19Descriptor

using namespace olb;

template<typename T, template<typename> class Lattice, class Dynamics>
void iniEquilibrium(CellBlockDataSimple<T,Lattice>& cellBlock, std::vector<size_t> startIndex, std::vector<size_t> endIndex,
        T const rho, T const u[Lattice<T>::d], std::vector<size_t> size, Dynamics& bulkDynamics)
{
    T** fluidData = cellBlock.getCellData();

    for(size_t iX = startIndex[0]; iX < endIndex[0]; ++iX)
        for(size_t iY = startIndex[1]; iY < endIndex[1]; ++iY)
            for(size_t iZ = startIndex[2]; iZ < endIndex[2]; ++iZ)
            {
                size_t cellIndex = util::getCellIndex3D(iX,iY,iZ,size[1],size[2]);
                bulkDynamics.iniEquilibrium(fluidData,cellIndex,rho,u);
            }

}

template<typename T, template<typename> class Lattice>
void addBoundary(DynamicsDataHandlerNew<T>& dataHandler, std::vector<size_t> startIndex, std::vector<size_t> endIndex, std::vector<size_t> const size,
        Dynamics<T,Lattice> * const dynamics, bool * const fluidMask)
{
    for(size_t iX = startIndex[0]; iX <= endIndex[0]; ++iX)
        for(size_t iY = startIndex[1]; iY <= endIndex[1]; ++iY)
            for(size_t iZ = startIndex[2]; iZ <= endIndex[2]; ++iZ)
            {
                size_t cellIndex = util::getCellIndex3D(iX,iY,iZ,size[1],size[2]);
                dataHandler.registerCell(cellIndex);
                fluidMask[cellIndex] = dynamics->isPostProcDynamics();
            }
}

template<typename T, class Descriptor>
__global__  void collisionKernelSimple(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataCurrentGPU,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataNew, const size_t gridSize, bool* fluidMask, size_t const ny, size_t const nz)
{
    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= gridSize)
        return;
    if( not(fluidMask[threadIndex]))
        return;

    T cellDataCache[Descriptor::q];

# pragma unroll
    for(unsigned int iPop = 0; iPop < Descriptor::q; ++iPop)
      cellDataCache[iPop] = cellDataCurrentGPU[iPop][threadIndex];

    T u[Descriptor::d];
    T rho;

#pragma unroll
    for (int iPop=0; iPop < Descriptor::q; ++iPop)
    {
      rho += cellDataCache[iPop];

      for (int iD=0; iD < Descriptor::d; ++iD)
      {
          u[iD] += cellDataCache[iPop]*Descriptor::c(iPop,iD);
      }
    }
    rho += (T)1;
    for (int iD=0; iD < Descriptor::d; ++iD)
    {
      u[iD] /= rho;
    }

    const T uSqr = u[0]*u[0]+u[1]*u[1]+u[2]*u[2];

#pragma unroll
    for (int iPop=0; iPop < Descriptor::q; ++iPop)
    {
      T c_u = T();
      for (int iD=0; iD < Descriptor::d; ++iD) {
        c_u += Descriptor::c(iPop,iD)*u[iD];
      }

      const T equ = rho * Descriptor::t(iPop) * ( (T)1 + Descriptor::invCs2() * c_u
                                                + Descriptor::invCs2() * Descriptor::invCs2() * (T)0.5 * c_u *c_u
                                                - Descriptor::invCs2() * (T)0.5 * uSqr )
                                                - Descriptor::t(iPop);

      cellDataCache[iPop] *=  (T)1-1.96;
      cellDataCache[iPop] += 1.96 * equ;
    }

# pragma unroll
    for(unsigned int iPop = 0; iPop < Descriptor::q; ++iPop)
    {
        size_t threadIndexNew = threadIndex + (Descriptor::c(iPop,0)*ny+ Descriptor::c(iPop,1))*nz + Descriptor::c(iPop,2);
        cellDataNew[iPop][threadIndex] = cellDataCache[iPop];
    }


}


template<template <typename> class Lattice>
void Simulation(T const simTime, size_t const xDimArg)
{
    size_t const xDim = xDimArg;
    size_t const yDim = xDim;
    size_t const zDim = xDim;


    CellBlockDataSimple<T,Lattice> cellBlock(xDim+1,yDim+1,zDim+1);
    const size_t gridSize = cellBlock.getNx()*cellBlock.getNy()*cellBlock.getNz();
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(1.96, 0.1);

    bool* fluidMask = new bool[xDim*yDim*zDim];
    bool* fluidMaskGPU;

    cudaMalloc(&fluidMaskGPU,sizeof(bool)*gridSize);

    std::vector<size_t> startIndex = {0,0,0};
    std::vector<size_t> endIndex = {xDim,yDim,zDim};
    std::vector<size_t> const size = {xDim+1,yDim+1,zDim+1};
    const T u[] = {0.,0.,0.};

    iniEquilibrium<T,Lattice>(cellBlock,startIndex,endIndex,1.0,u, size, bulkDynamics);

    BounceBack<T,Lattice> bounceBackDynamics;
    DynamicsDataHandlerNew<T> bounceBackHandler(bounceBackDynamics.getMomentaDataSize(), false);
    addBoundary<T,Lattice>(bounceBackHandler, std::vector<size_t>{0,0,0}   , std::vector<size_t>{xDim-1,yDim-1,0}   , size, &bounceBackDynamics, fluidMask);
    addBoundary<T,Lattice>(bounceBackHandler, std::vector<size_t>{0,0,1}   , std::vector<size_t>{0,yDim-1,zDim}     , size, &bounceBackDynamics, fluidMask);
    addBoundary<T,Lattice>(bounceBackHandler, std::vector<size_t>{0,yDim,0}, std::vector<size_t>{xDim-1,yDim,zDim-1}, size, &bounceBackDynamics, fluidMask);
    addBoundary<T,Lattice>(bounceBackHandler, std::vector<size_t>{1,0,1}   , std::vector<size_t>{xDim,0,zDim}       , size, &bounceBackDynamics, fluidMask);
    addBoundary<T,Lattice>(bounceBackHandler, std::vector<size_t>{xDim,1,0}, std::vector<size_t>{xDim,yDim,zDim-1}  , size, &bounceBackDynamics, fluidMask);
    addBoundary<T,Lattice>(bounceBackHandler, std::vector<size_t>{1,1,zDim}, std::vector<size_t>{xDim,yDim,zDim}    , size, &bounceBackDynamics, fluidMask);

    T** momentaData = nullptr;
    cellBlock.copyCellDataToGPU();

    for(unsigned int iStep = 0; iStep < 3; ++iStep)
        collisionKernelSimple<T,Lattice<T>><<<16,256>>>(cellBlock.gpuGetFluidData(), cellBlock.gpuGetFluidDataOld()
                , gridSize, fluidMaskGPU, size[1], size[2]);




    return;
}

int main()
{
    T simTime = 10.;
    Simulation<LATTICE>(simTime,16);
    return 0;
}
