#define D2Q9LATTICE 1
typedef double T;


#include "olb2D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb2D.hh"   // include full template code
#endif
#include <iostream>

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D2Q9Descriptor

const int dataSize = DESCRIPTOR<T>::q+DESCRIPTOR<T>::d+1;

#ifdef ENABLE_CUDA

    __device__ T d_result[dataSize];

	__global__ void checkCellValues(T** data, size_t cellID)
	{
		for(size_t i=0; i<dataSize; ++i)
		{
			d_result[i] = data[i][cellID];
		}

	}
#endif

int main()
{
	const int N = 5;
	const int omega = 0.6;

	ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  bulkDynamics (
			omega, instances::getBulkMomenta<T,DESCRIPTOR>()
	);

	BlockLattice2D<T, DESCRIPTOR> lattice( N, N, &bulkDynamics );
	const int nx = lattice.getNx();
	const int ny = lattice.getNy();

#ifdef ENABLE_CUDA
	GPUHandler<T,DESCRIPTOR>::get();
#endif

	OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
	boundaryCondition = createInterpBoundaryCondition2D<T,DESCRIPTOR,ConstRhoBGKdynamics>( lattice );

	boundaryCondition->addVelocityBoundary0N(   0,   0,   1,ny-2, omega );
	boundaryCondition->addVelocityBoundary0P( nx-1,nx-1,   1,ny-2, omega );
	boundaryCondition->addVelocityBoundary1N(   1,nx-2,   0,   0, omega );
	boundaryCondition->addVelocityBoundary1P(   1,nx-2,ny-1,ny-1, omega );

	boundaryCondition->addExternalVelocityCornerNN(   0,   0, omega );
	boundaryCondition->addExternalVelocityCornerNP(   0,ny-1, omega );
	boundaryCondition->addExternalVelocityCornerPN( nx-1,   0, omega );
	boundaryCondition->addExternalVelocityCornerPP( nx-1,ny-1, omega );


	lattice.initDataArrays();

	for(int xPosition=0; xPosition<nx; ++xPosition)
		for(int yPosition=0; yPosition<ny; ++yPosition)
		{
			T rho = xPosition+nx*yPosition;
			T u[2] = {xPosition+nx*yPosition+1,xPosition+nx*yPosition+2};
			lattice.defineRhoU(xPosition,xPosition,yPosition,yPosition,rho,u);
			lattice.iniEquilibrium(xPosition,xPosition,yPosition,yPosition,rho,u);
		}

#ifdef ENABLE_CUDA
	lattice.copyDataToGPU();
#endif


#ifdef ENABLE_CUDA
	T** cellData = GPUHandler<T,DESCRIPTOR>::get().getFluidData();

	T result[dataSize];
	for(int iY=0; iY<N; ++iY)
		for(int iX=0; iX<N; ++iX)
		{
			size_t cellID = util::getCellIndex2D(iX,iY,nx);
			checkCellValues<<<1,1>>>(cellData,cellID);
			cudaDeviceSynchronize();
			cudaMemcpyFromSymbol(&result, d_result, dataSize*sizeof(T),0, cudaMemcpyDeviceToHost);
			for(int i=0; i<dataSize; ++i)
			{
				assert( ( (lattice.get(iX,iY))[i].get() == result[i]) );
			}
		}

#endif

	return 0;
}
