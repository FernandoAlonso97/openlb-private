#define D2Q9LATTICE 1
typedef double T;


#include "olb2D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb2D.hh"   // include full template code
#endif
#include <iostream>

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D2Q9Descriptor

int main()
{
	const int omega = 0.6;

	ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  bulkDynamics (
			omega, instances::getBulkMomenta<T,DESCRIPTOR>()
	);

	BlockLattice2D<T, DESCRIPTOR> lattice( 5, 5, &bulkDynamics );
	const int nx = lattice.getNx();
	const int ny = lattice.getNy();

	OnLatticeBoundaryCondition2D<T,DESCRIPTOR>*
	boundaryCondition = createInterpBoundaryCondition2D<T,DESCRIPTOR,ConstRhoBGKdynamics>( lattice );
	boundaryCondition->addVelocityBoundary0N(   0,   0,   1,ny-2, omega );
	boundaryCondition->addVelocityBoundary0P( nx-1,nx-1,   1,ny-2, omega );
	boundaryCondition->addVelocityBoundary1N(   1,nx-2,   0,   0, omega );
	boundaryCondition->addVelocityBoundary1P(   1,nx-2,ny-1,ny-1, omega );

	boundaryCondition->addExternalVelocityCornerNN(   0,   0, omega );
	boundaryCondition->addExternalVelocityCornerNP(   0,ny-1, omega );
	boundaryCondition->addExternalVelocityCornerPN( nx-1,   0, omega );
	boundaryCondition->addExternalVelocityCornerPP( nx-1,ny-1, omega );

	lattice.initDataArrays();

	for(int xPosition=0; xPosition<nx; ++xPosition)
		for(int yPosition=0; yPosition<ny; ++yPosition)
		{
			T rho = xPosition+nx*yPosition;
			T u[2] = {xPosition+nx*yPosition+1,xPosition+nx*yPosition+2};
			lattice.defineRhoU(xPosition,xPosition,yPosition,yPosition,rho,u);
			lattice.iniEquilibrium(xPosition,xPosition,yPosition,yPosition,rho,u);
		}

	for(int xPosition=0; xPosition<nx; ++xPosition)
		for(int yPosition=0; yPosition<ny; ++yPosition)
		{
			T rho;
			T u[2];
			lattice.get(xPosition,yPosition).computeRhoU(rho,u);
			T check = xPosition+nx*yPosition;
			assert((rho == check && u[0] == check+1 && u[1] == check+2));
//			std::cout << "(" << xPosition << "," << yPosition << "): " << rho - check << "," << u[0] - (check+1) <<
//					"," << u[1]- (check+2) << std::endl;
		}





	return 0;
}
