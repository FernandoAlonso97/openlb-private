#define D2Q9LATTICE 1
typedef double T;


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif
#include <iostream>

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D3Q19Descriptor

int main()
{
	const int omega = 0.6;

	ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  bulkDynamics (
			omega, instances::getBulkMomenta<T,DESCRIPTOR>()
	);

	BlockLattice3D<T, DESCRIPTOR> lattice( 5, 5, 5, &bulkDynamics );
	const int nx = lattice.getNx();
	const int ny = lattice.getNy();
	const int nz = lattice.getNz();

	OnLatticeBoundaryCondition3D<T,DESCRIPTOR>*
	boundaryCondition = createInterpBoundaryCondition3D<T,DESCRIPTOR,ConstRhoBGKdynamics>( lattice );

	boundaryCondition->addVelocityBoundary0N(    0,    0,    1,  ny-2,   1, nz-2, omega );
	boundaryCondition->addVelocityBoundary0P( nx-1, nx-1,    1,  ny-2,   1, nz-2, omega );
	boundaryCondition->addVelocityBoundary1N(    1, nx-2,    0,    0,    1, nz-2, omega );
    boundaryCondition->addVelocityBoundary1P(    1, nx-2, ny-1, ny-1,    1, nz-2, omega );
    boundaryCondition->addVelocityBoundary2N(    1, nx-2,    1, ny-2,    0,    0, omega );
    boundaryCondition->addVelocityBoundary2P(    1, nx-2,    1, ny-2, nz-1, nz-1, omega );

    boundaryCondition->addExternalVelocityEdge0PN(    1, nx-2, ny-1, ny-1,    0,    0, omega );
    boundaryCondition->addExternalVelocityEdge0NN(    1, nx-2,    0,    0,    0,    0, omega );
    boundaryCondition->addExternalVelocityEdge0PP(    1, nx-2, ny-1, ny-1, nz-1, nz-1, omega );
    boundaryCondition->addExternalVelocityEdge0NP(    1, nx-2,    0,    0, nz-1, nz-1, omega );

    boundaryCondition->addExternalVelocityEdge1PN( nx-1, nx-1,    1, ny-2,    0,    0, omega );
    boundaryCondition->addExternalVelocityEdge1NN(    0,    0,    1, ny-2,    0,    0, omega );
    boundaryCondition->addExternalVelocityEdge1PP( nx-1, nx-1,    1, ny-2, nz-1, nz-1, omega );
    boundaryCondition->addExternalVelocityEdge1NP(    0,    0,    1, ny-2, nz-1, nz-1, omega );

    boundaryCondition->addExternalVelocityEdge2PN( nx-1, nx-1,    0,    0,    1, nz-2, omega );
    boundaryCondition->addExternalVelocityEdge2NN(    0,    0,    0,    0,    1, nz-2, omega );
    boundaryCondition->addExternalVelocityEdge2PP( nx-1, nx-1, ny-1, ny-1,    1, nz-2, omega );
    boundaryCondition->addExternalVelocityEdge2NP( nx-1, nx-1, ny-1, ny-1,    1, nz-2, omega );

	boundaryCondition->addExternalVelocityCornerNNN(   0,    0,    0, omega);
	boundaryCondition->addExternalVelocityCornerPNN(nx-1,    0,    0, omega);
	boundaryCondition->addExternalVelocityCornerNPN(   0, ny-1,    0, omega);
	boundaryCondition->addExternalVelocityCornerNNP(   0,    0, nz-1, omega);
	boundaryCondition->addExternalVelocityCornerPPN(nx-1, ny-1,    0, omega);
	boundaryCondition->addExternalVelocityCornerPNP(nx-1,    0, nz-1, omega);
	boundaryCondition->addExternalVelocityCornerNPP(   0, ny-1, nz-1, omega);
	boundaryCondition->addExternalVelocityCornerPPP(nx-1, ny-1, nz-1, omega);

	lattice.initDataArrays();

	for(int xPosition=0; xPosition<nx; ++xPosition)
		for(int yPosition=0; yPosition<ny; ++yPosition)
		    for(int zPosition=0; zPosition<nz; ++zPosition)
		    {
		        T rho = xPosition+nx*yPosition+nx*ny*zPosition;
			    T u[3] = {rho+1,rho+2,rho+3};
			    lattice.defineRhoU(xPosition,xPosition,yPosition,yPosition,zPosition,zPosition,rho,u);
			    lattice.iniEquilibrium(xPosition,xPosition,yPosition,yPosition,zPosition,zPosition,rho,u);
            }

	for(int xPosition=0; xPosition<nx; ++xPosition)
		for(int yPosition=0; yPosition<ny; ++yPosition)
		    for(int zPosition=0; zPosition<nz; ++zPosition)
		    {
		        T rho;
			    T u[3];
			    lattice.get(xPosition,yPosition,zPosition).computeRhoU(rho,u);
			    T check = xPosition+nx*yPosition+nx*ny*zPosition;
			    assert((rho == check && u[0] == check+1 && u[1] == check+2 && u[2] == check+3));
//			std::cout << "(" << xPosition << "," << yPosition << "," << zPosition << "): " << rho - check << "," << u[0] - (check+1) <<
//					"," << u[1]- (check+2) << "," <<  u[2] - (check+3) << std::endl;
		    }





	return 0;
}
