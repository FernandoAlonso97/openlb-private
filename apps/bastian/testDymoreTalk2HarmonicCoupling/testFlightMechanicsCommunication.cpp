/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T>
__global__ void readThrust(T* thrustData, RotorCellData<T> rotorCellData, T** cellData)
{
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= rotorCellData._numberRotorCells)
      return;

    thrustData[threadIndex] = cellData[Lattice<T>::forceIndex+2][rotorCellData._cellIndexGPU[threadIndex]];
    printf("%lf\n",cellData[Lattice<T>::forceIndex+2][rotorCellData._cellIndexGPU[threadIndex]]);
}


template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;


    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);


    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

template<unsigned int RESOLUTION>
void MultipleSteps(const double simTime)
{
  constexpr unsigned int nbHarmRadial = 18;
  constexpr unsigned int nbHarmAzi = 5;
  constexpr unsigned int nbHarm = 20;
  using CouplingType = Coupling<T,TwoHarmonicThrust<T,Lattice<T>>,TwoHarmonicVelocity<T,Lattice<T>,nbHarmRadial,nbHarmAzi>>;
//  using CouplingType = Coupling<T,TwoHarmonicThrust<T,Lattice<T>>,TwoHarmonicVelocity<T,Lattice<T>,nbHarmRadial,nbHarmAzi>>;
//  using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;
  using ReceiveDataGensim = CouplingType::ReceiveDataType;
  using SendDataGensim    = CouplingType::SendDataType;
//  using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
//  using SendDataGensim = VelocityAtRotorsLinear;


  int iXLeftBorder = 0;
  int iXRightBorder = (RESOLUTION+1)*1.5-1;
  int iYBottomBorder = 0;
  int iYTopBorder   = (RESOLUTION+1)*1.5-1;
  int iZFrontBorder = 0;
  int iZBackBorder  = (RESOLUTION+1)*1.5-1;

  T const rotorRadius = 2.32;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,6.*rotorRadius
          ,40.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();
  T cellSize = spacing*spacing;

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";


  ConstExternalField<T,Lattice> externalFieldClass({0.,0.,0.});

  BlockLatticeALE3D<T, Lattice, ConstExternalField<T,Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1,
          {0,0,0}, &bulkDynamics, externalFieldClass);

  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {static_cast<T>(iXRightBorder/2.),static_cast<T>(iYTopBorder/2.),static_cast<T>((iZBackBorder+1)/2.)};


  Rotor<T> rotor(rotorRadius,0.207,rotorPosition,spacing);
  RotorCellData<T> rotorCellData(rotor,lattice, converter);
  RelaxationData<T> relaxationData(0.7);

  Rotor<T> rotorInflow(rotorRadius*1.2,0.,rotorPosition,spacing);
  SquareCellData<T> squareCellDataInflow(rotorInflow,lattice,converter);
  RotorCellData<T> rotorCellDataInflow(rotorInflow,lattice, converter);
//  std::tuple<RotorCellData<T>,unsigned int, unsigned int,T> inflowCplCnstr(rotorCellDataInflow,nbHarmRadial,nbHarmAzi,1.2);
  std::tuple<Rotor<T>,SquareCellData<T>,unsigned int,unsigned int, T, T, unsigned int, unsigned int> inflowCplCnstr(rotorInflow,
          squareCellDataInflow,nbHarm,nbHarm,1.4,spacing,100,360);

  std::cout << rotorCellData._numberFullRotorCells << "," << rotorCellDataInflow._numberFullRotorCells << std::endl;

  CouplingType::CouplingDataType couplingData(cellSize,inflowCplCnstr);
//
//  CouplingType::CouplingDataType couplingData(rotorCellData);

  for(unsigned int iCell = 0; iCell< rotorCellData._numberRotorCells; ++iCell)
      if(rotorCellData._cellPositionRadialCPU[iCell] < rotor.getHubRadius())
      {
          size_t position[3];
          util::getCellIndices3D(rotorCellData._cellIndexCPU[iCell],lattice.getNy(),lattice.getNz(),position);
          lattice.defineDynamics(position[0],position[1],position[2],&instances::getBounceBack<T,Lattice>());
      }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::cout << "Rotor Position: " << rotor.getCenter()[0] << "," << rotor.getCenter()[1] << "," << rotor.getCenter()[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotor.getXLimits()[0] << " to " << rotor.getXLimits()[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotor.getYLimits()[0] << " to " << rotor.getYLimits()[1] << std::endl;

  std::cout << "Number of theoretical rotor cells: " << rotor.getNumberRotorCells() << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::cout << "Undim: " << converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea() << std::endl;

  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  T force[3] = {0.,0.,converter.getLatticeForce(std::pow(spacing,2)*20000./1.225)/76.046648};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., converter.getLatticeVelocity(0.)};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string name;
  std::string directory = "/scratch/BHorvat/outputTwoHarmonicCoupling/";
  name = "dymoreCouplingTest_X";
  name += std::to_string(iXRightBorder+1);
  std::string nameTrim = name + "_trim";

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> maskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriterTrim( nameTrim );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(maskFunctor);

  vtkWriterTrim.write(0);

  std::cout << "Undim: " << rotorCellData._undimThrust << ", " << converter.getLatticePressure(1) << std::endl;
  ReceiveDataGensim thrustData;
  SendDataGensim dataSend;
  NetworkInterfaceTCP<ReceiveDataGensim,SendDataGensim> networkCommunicator(8888,OUTPUTIP,8888,NETWORKBUFFERSIZE,true);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

//  double thrust[3] = {0,0,0};
//  lattice.copyDataToCPU();
//  for (int iX = 0; iX <= iXRightBorder; ++iX)
//    for (int iY = 0; iY <= iYTopBorder; ++iY)
//      for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
//      {
//          for(int iDim = 0; iDim < Lattice<T>::d; ++iDim)
//              thrust[iDim] += converter.getPhysForce(lattice.getData(iX,iY,iZ,Lattice<T>::forceIndex+iDim)[0]);
//      }

//  thrustData.cc[0] = 1000*9.81;
//  thrustData.cc[thrustData.getIndex(0,4)] = 200*9.81;
//  thrustData.noHarmonicsAzimuth = nbHarmAzi;
//  thrustData.noHarmonicsRadial = nbHarmRadial;

  unsigned int preStep = 1;

  while(true) {
      networkCommunicator.recieve_data(thrustData);

//      thrustData.cc[0] = 200*9.81/rotorArea;

//      std::cout << "Harmonics radial: " << thrustData.noHarmonicsRadial << ", Harmonics azimuth: " << thrustData.noHarmonicsAzimuth << std::endl;
//      std::ofstream coeff("coeffsT.txt");
//      coeff << "cc,cs,sc,ss,iRad,iAz" << std::endl;
//      for(unsigned int iRad = 0; iRad<thrustData.noHarmonicsRadial; ++iRad)
//          for(unsigned int iAz = 0; iAz<thrustData.noHarmonicsAzimuth; ++iAz)
//          {
//              unsigned int index = thrustData.getIndex(iRad,iAz);
//              coeff << thrustData.cc[index] << "," << thrustData.cs[index] << "," << thrustData.sc[index] << "," << thrustData.ss[index] <<
//                      "," << iRad << "," << iAz << std::endl;
//          }


      std::cout << "Thrust: " << thrustData.AbsThrust[2] << std::endl;

      CouplingType::writeReceiveData(thrustData,lattice.getData(),rotorCellData,couplingData.getWriteData());
      double totalThrust = thrust::reduce(thrust::device,&lattice.getData()[Lattice<T>::forceIndex+2][0],
              &lattice.getData()[Lattice<T>::forceIndex+2][lattice.getNCells()-1]);
      totalThrust = converter.getPhysForce(totalThrust);

      std::cout << "Total thrust: " << totalThrust << std::endl;

      unsigned int trimTime = converter.getLatticeTime(2.0);

      for(unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
      {
		  lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
          HANDLE_ERROR(cudaGetLastError());
          if( (trimStep%converter.getLatticeTime(.2)) == 0)
          {
              std::cout << "Trim step:       " << preStep << std::endl;
              std::cout << "Simulation time: " << converter.getPhysTime(trimStep) << std::endl;
          }
      }

	  lattice.copyDataToCPU();
	  vtkWriterTrim.write(preStep);
      ++preStep;

//      CouplingType::readSendData(dataSend,lattice.getData(),rotorCellData,couplingData.getReadData());
	  CouplingType::readSendData(dataSend,lattice.getData(),squareCellDataInflow,couplingData.getReadData());

//      std::cout << "vi: " << dataSend.cc[0] << std::endl;

//      dataSend.bounds[0] = 0.3/dataSend.scalingFactor;
//      dataSend.bounds[1] = 0.85/dataSend.scalingFactor;
//      dataSend.minVelocity = 5.0;
//      dataSend.maxVelocity = 12.5;

      networkCommunicator.send_data(dataSend);

      std::ofstream inflowInterp("inflowInterp.txt");
      inflowInterp << "iR,iAz,v" << std::endl;

      for(unsigned int iR = 0; iR < 100; ++iR) {
          for(unsigned int iAz = 0; iAz < 360; ++iAz) {
              inflowInterp << iR/(100.-1.) << "," << iAz*M_PI*2./(360.-1.) << "," << couplingData.getReadData().h_inflowInterpVector[util::getCellIndex2D(iR,iAz,360)] << std::endl;
          }
      }

//      std::ofstream coeffs("coeffsV.txt");
//
//      coeffs << "cc,cs,sc,ss,iRad,iPsi" << std::endl;
//      for(unsigned int iRad = 0; iRad < dataSend.noHarmonicsRadial; ++iRad)
//          for(unsigned int iAz = 0; iAz < dataSend.noHarmonicsAzimuth; ++iAz) {
////              std::cout << "Index: " << thrustData.getIndex(iX,iY) << ", iX: " << iX << ", iY: " << iY <<
////                      ", cc " << thrustData.cc[thrustData.getIndex(iX,iY)] << ", sc " << thrustData.sc[thrustData.getIndex(iX,iY)] <<
////                      ", cs " << thrustData.cs[thrustData.getIndex(iX,iY)] << ", ss " << thrustData.ss[thrustData.getIndex(iX,iY)] << std::endl;
//              coeffs << dataSend.cc[dataSend.getIndex(iRad,iAz)] << "," << dataSend.cs[dataSend.getIndex(iRad,iAz)] << ","
//                      << dataSend.sc[dataSend.getIndex(iRad,iAz)] << "," << dataSend.ss[dataSend.getIndex(iRad,iAz)] << ","
//                      << iRad << "," << iAz << std::endl;
//          }
  }


}

int main(int argc, char** argv)
{
    const double simTime = 60;
    MultipleSteps<95>(simTime);
    return 0;
}
