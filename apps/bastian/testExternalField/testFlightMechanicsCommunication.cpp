/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define FORCEDD3Q19LATTICE 1
typedef double T;
//
//#include "olb3D.h"
//#include "olb3D.hh"
#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, template <typename> class Lattice>
void readBinary(T** externalField, std::string fileName, UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const & converter,
        std::vector<int64_t> & extent)
{
    uint8_t floatType = 1;
    uint8_t doubleType = 2;

    std::cout << "Starting reading test!" << std::endl;

    std::fstream readFile(fileName, std::ios::in | std::ios::binary);
    if(!readFile.is_open())
       std::cout << "File not opened" << std::endl;

    uint16_t readSize = 0;

    readFile.read( (char*) &readSize,  sizeof(uint16_t));
    std::string title;
    title.resize(readSize);
    readFile.read( (char*) &title[0], readSize);

    std::cout << "Title: " << title << std::endl;

    int32_t numVarsRead = 0;
    readFile.read( (char*) &numVarsRead, sizeof(int32_t));
    std::vector<std::string> varNames(numVarsRead);

    for(int32_t iVar = 0; iVar < numVarsRead; ++iVar)
    {
       readFile.read((char*) &readSize,sizeof(uint16_t));
       varNames[iVar].resize(readSize);
       readFile.read( (char*) &(varNames[iVar][0]),readSize);
       std::cout << "Variable: " << varNames[iVar] << std::endl;
    }

    readFile.read( (char*) &extent[0], 3*sizeof(int64_t));
    for(uint8_t iDim = 0; iDim < 3; ++iDim)
    {
       std::cout << "Extent in direction " << static_cast<uint16_t>(iDim) << " :" << extent[iDim] << std::endl;
    }

    uint64_t dataSizeToRead = extent[0]*extent[1]*extent[2];

    uint8_t valueType;

    readFile.read( (char*) &valueType, sizeof(uint8_t));

    std::cout << "Value type: " << static_cast<uint16_t>(valueType) << std::endl;

    double lengthFactor;
    readFile.read( (char*) &lengthFactor, sizeof(double));
    std::cout << "Length factor: " << lengthFactor << std::endl;

    std::vector<float> offsetRead(3);
    readFile.read( (char*) &offsetRead[0], 3*sizeof(float));

    for(uint8_t iDim = 0; iDim < 3; ++iDim)
    {
       std::cout << "Offset in direction " << static_cast<uint16_t>(iDim) << " :" << offsetRead[iDim] << std::endl;
    }


    uint16_t chunkSizeRead;
    readFile.read( (char*) &chunkSizeRead, sizeof(uint16_t));
    std::cout << "Chunk size: " << chunkSizeRead << std::endl;

    for(uint16_t iDim = 0; iDim < Lattice<T>::d; ++iDim)
        cudaMallocHost(&externalField[iDim], dataSizeToRead*sizeof(T));//= new T [dataSizeToRead];

    float dataFloat[numVarsRead][chunkSizeRead];
    double dataDouble[numVarsRead][chunkSizeRead];

    uint64_t chunkRead = 0;
    uint64_t numberChunksToRead = dataSizeToRead/chunkSizeRead + 1;

    for(uint64_t index = 0; index < dataSizeToRead; index+=chunkSizeRead)
    {
       if(index+chunkSizeRead > dataSizeToRead)
           chunkSizeRead = dataSizeToRead - index;

       if(chunkRead%100 == 0)
           std::cout << chunkRead << "/" << numberChunksToRead << " chunks read" << std::endl;
       if(valueType == floatType)
       {
           readFile.read( (char*) &dataFloat[0], chunkSizeRead*sizeof(float));
           readFile.read( (char*) &dataFloat[1], chunkSizeRead*sizeof(float));
           readFile.read( (char*) &dataFloat[2], chunkSizeRead*sizeof(float));


       }
       else if(valueType == doubleType)
       {
           readFile.read( (char*) &dataDouble[0], chunkSizeRead*sizeof(double));
           readFile.read( (char*) &dataDouble[1], chunkSizeRead*sizeof(double));
           readFile.read( (char*) &dataDouble[2], chunkSizeRead*sizeof(double));
       }

       for(int32_t iVar = 3; iVar < 2*Lattice<T>::d+1; ++iVar)
       {
           uint64_t indexStart = chunkRead*chunkSizeRead;

           if(valueType == floatType)
           {
               readFile.read( (char*) &dataFloat[iVar], chunkSizeRead*sizeof(float));

               if(iVar > Lattice<T>::d)
                   for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
                   {
                       uint64_t iX = std::round((dataFloat[0][iChunk]+offsetRead[0])/lengthFactor);
                       uint64_t iY = std::round((dataFloat[1][iChunk]+offsetRead[1])/lengthFactor);
                       uint64_t iZ = std::round((dataFloat[2][iChunk]+offsetRead[2])/lengthFactor);

                       uint64_t index = iZ+extent[2]*(iY+extent[1]*iX);
                       externalField[iVar-Lattice<T>::d-1][index] = converter.getLatticeVelocity(dataFloat[iVar][iChunk]);
                   }
           }
           else if(valueType == doubleType)
           {

               readFile.read( (char*) &dataDouble[iVar], chunkSizeRead*sizeof(double));

               if(iVar > Lattice<T>::d)
                   for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
                   {
                       uint64_t iX = std::round((dataDouble[0][iChunk]+offsetRead[0])/lengthFactor);
                       uint64_t iY = std::round((dataDouble[1][iChunk]+offsetRead[1])/lengthFactor);
                       uint64_t iZ = std::round((dataDouble[2][iChunk]+offsetRead[2])/lengthFactor);

                       uint64_t index = iZ+extent[2]*(iY+extent[1]*iX);
                       externalField[iVar-Lattice<T>::d-1][index] = converter.getLatticeVelocity(dataDouble[iVar][iChunk]);

                   }
           }

       }
       ++chunkRead;
    }
}

template<typename T, template <typename> class Lattice, typename FieldPointer>
void readBinaryNoDensity(FieldPointer externalField, std::string fileName, UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const & converter,
        std::vector<int64_t> & extent)
{

    *externalField = new T*[Lattice<T>::d];

    uint8_t floatType = 1;
    uint8_t doubleType = 2;

    std::cout << "Starting reading test!" << std::endl;

    std::fstream readFile(fileName, std::ios::in | std::ios::binary);
    if(!readFile.is_open())
       std::cout << "File not opened" << std::endl;

    uint16_t readSize = 0;

    readFile.read( (char*) &readSize,  sizeof(uint16_t));
    std::string title;
    title.resize(readSize);
    readFile.read( (char*) &title[0], readSize);

    std::cout << "Title: " << title << std::endl;

    int32_t numVarsRead = 0;
    readFile.read( (char*) &numVarsRead, sizeof(int32_t));
    std::vector<std::string> varNames(numVarsRead);

    for(int32_t iVar = 0; iVar < numVarsRead; ++iVar)
    {
       readFile.read((char*) &readSize,sizeof(uint16_t));
       varNames[iVar].resize(readSize);
       readFile.read( (char*) &(varNames[iVar][0]),readSize);
       std::cout << "Variable: " << varNames[iVar] << std::endl;
    }

    readFile.read( (char*) &extent[0], 3*sizeof(int64_t));
    for(uint8_t iDim = 0; iDim < 3; ++iDim)
    {
       std::cout << "Extent in direction " << static_cast<uint16_t>(iDim) << " :" << extent[iDim] << std::endl;
    }

    uint64_t dataSizeToRead = extent[0]*extent[1]*extent[2];

    uint8_t valueType;

    readFile.read( (char*) &valueType, sizeof(uint8_t));

    std::cout << "Value type: " << static_cast<uint16_t>(valueType) << std::endl;

    double lengthFactor;
    readFile.read( (char*) &lengthFactor, sizeof(double));
    std::cout << "Length factor: " << lengthFactor << std::endl;

    std::vector<float> offsetRead(3);
    readFile.read( (char*) &offsetRead[0], 3*sizeof(float));

    for(uint8_t iDim = 0; iDim < 3; ++iDim)
    {
       std::cout << "Offset in direction " << static_cast<uint16_t>(iDim) << " :" << offsetRead[iDim] << std::endl;
    }


    uint16_t chunkSizeRead;
    readFile.read( (char*) &chunkSizeRead, sizeof(uint16_t));
    std::cout << "Chunk size: " << chunkSizeRead << std::endl;

    for(uint16_t iDim = 0; iDim < Lattice<T>::d; ++iDim)
        (*externalField)[iDim] = new T[dataSizeToRead];
//        cudaMallocHost(&externalField[iDim], dataSizeToRead*sizeof(T));//= new T [dataSizeToRead];

    float dataFloat[numVarsRead][chunkSizeRead];
    double dataDouble[numVarsRead][chunkSizeRead];

    uint64_t chunkRead = 0;
    uint64_t numberChunksToRead = dataSizeToRead/chunkSizeRead + 1;

    for(uint64_t index = 0; index < dataSizeToRead; index+=chunkSizeRead)
    {
       if(index+chunkSizeRead > dataSizeToRead)
           chunkSizeRead = dataSizeToRead - index;

       if(chunkRead%100 == 0)
           std::cout << chunkRead << "/" << numberChunksToRead << " chunks read" << std::endl;
       if(valueType == floatType)
       {
           readFile.read( (char*) &dataFloat[0], chunkSizeRead*sizeof(float));
           readFile.read( (char*) &dataFloat[1], chunkSizeRead*sizeof(float));
           readFile.read( (char*) &dataFloat[2], chunkSizeRead*sizeof(float));


       }
       else if(valueType == doubleType)
       {
           readFile.read( (char*) &dataDouble[0], chunkSizeRead*sizeof(double));
           readFile.read( (char*) &dataDouble[1], chunkSizeRead*sizeof(double));
           readFile.read( (char*) &dataDouble[2], chunkSizeRead*sizeof(double));
       }

       for(int32_t iVar = 3; iVar < 2*Lattice<T>::d; ++iVar)
       {
           uint64_t indexStart = chunkRead*chunkSizeRead;

           if(valueType == floatType)
           {
               readFile.read( (char*) &dataFloat[iVar], chunkSizeRead*sizeof(float));

               for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
               {
                   uint64_t iX = std::round((dataFloat[0][iChunk]+offsetRead[0])/lengthFactor);
                   uint64_t iY = std::round((dataFloat[1][iChunk]+offsetRead[1])/lengthFactor);
                   uint64_t iZ = std::round((dataFloat[2][iChunk]+offsetRead[2])/lengthFactor);

                   uint64_t index = iZ+extent[2]*(iY+extent[1]*iX);
                   (*externalField)[iVar-Lattice<T>::d][index] = converter.getLatticeVelocity(dataFloat[iVar][iChunk]);
               }
           }
           else if(valueType == doubleType)
           {

               readFile.read( (char*) &dataDouble[iVar], chunkSizeRead*sizeof(double));

               for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
               {
                   uint64_t iX = std::round((dataDouble[0][iChunk]+offsetRead[0])/lengthFactor);
                   uint64_t iY = std::round((dataDouble[1][iChunk]+offsetRead[1])/lengthFactor);
                   uint64_t iZ = std::round((dataDouble[2][iChunk]+offsetRead[2])/lengthFactor);

                   uint64_t index = iZ+extent[2]*(iY+extent[1]*iX);
                   (*externalField)[iVar-Lattice<T>::d][index] = converter.getLatticeVelocity(dataDouble[iVar][iChunk]);

               }
           }

       }
       ++chunkRead;
    }
}

template<typename T, unsigned int Dimension = 3>
T** createField(std::array<T,Dimension> velocity, std::array<size_t,Dimension> extent)
{
    T** field = new T*[Dimension];
    size_t gridSize = 1;
    for(size_t iExtent : extent)
        gridSize *= iExtent;

    for(unsigned int iDim = 0; iDim < Dimension; ++iDim)
    {
        field[iDim] = new T[gridSize];
        for(unsigned int index = 0; index < gridSize; ++index)
            field[iDim][index] = velocity[iDim];
    }

    return field;
}

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;


    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);


    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

template<unsigned int RESOLUTION>
void MultipleSteps(const double simTime, std::vector<std::string> fileNames)
{
  int iXLeftBorder = 0;
  int iXRightBorder = RESOLUTION;
  int iYBottomBorder = 0;
  int iYTopBorder = RESOLUTION;
  int iZFrontBorder = 0;
  int iZBackBorder = RESOLUTION;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,4.*4.92
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";

  std::vector<T**> externalFields(fileNames.size());
  std::vector<int64_t> extent(3);

  auto field = externalFields.begin();
  for(auto fileName = fileNames.begin(); fileName != fileNames.end(); ++fileName, ++field)
  {
      readBinaryNoDensity(field,*fileName,converter,extent);
  }
//  0.25/converter.getPhysDeltaT()
  Vec3<T> outsideVelocity{0.,converter.getLatticeVelocity(0),0.};
  PredefinedTimeVariantExternalField<T,Lattice> externalFieldClass(externalFields, outsideVelocity,
          2.0/converter.getPhysDeltaT(), extent[0], extent[1], extent[2], 1.0);
  BlockLatticeALE3D<T, Lattice, PredefinedTimeVariantExternalField<T,Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1,
          {0,0,0}, &bulkDynamics, externalFieldClass);


//  T* externalField[Lattice<T>::d];
//  std::vector<int64_t> extent(3);
//  readBinary(externalField,"Offwindtech_0_634.myFormat",converter,extent);
//  PredefinedExternalField<T,Lattice> externalFieldClass(externalField,extent[0],extent[1],extent[2]);
//  BlockLatticeALE3D<T, Lattice, PredefinedExternalField<T,Lattice> > lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1,
//           {0, 0, 0}, &bulkDynamics,externalFieldClass);

//  Vec3<T> externalVelocity(converter.getLatticeVelocity(0.0), converter.getLatticeVelocity(10.0), 0.0);
//  ConstExternalField<T,Lattice> externalFieldALE(externalVelocity);
//  BlockLatticeALE3D<T, Lattice, ConstExternalField<T,Lattice> > lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1,
//          {0, 0, 0}, &bulkDynamics,externalFieldALE);
  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);
  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  T force[3] = {0.,0.,converter.getLatticeForce(std::pow(spacing,2)*20000./1.225)/76.046648};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
//            if(sqrt(pow(iX-iXRightBorder/2.,2)+pow(iY-iYTopBorder/2.,2)) < 4.92/spacing and iZ == static_cast<int>(0.25*iZBackBorder))
//                lattice.defineForce(iX, iX, iY, iY, iZ, iZ, force);
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string nameTrim;
  std::string directory = "/scratch/BHorvat/externalFieldCoupling/";
  nameTrim = "externalFieldCoupling_X";
  nameTrim += std::to_string(iXRightBorder+1);

  BlockVTKwriter3D<T> vtkWriter( nameTrim );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);

  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  Vec3<T> position{10,175,50};
  Vec3<T> attitude{0,0,0};
  Vec3<T> translation{0,0,0};
  Vec3<T> rotation{0,0,0};

  unsigned int print = 1;

//  for(unsigned int timeStep = 0; timeStep < converter.getLatticeTime(simTime); ++timeStep)
  for(unsigned int timeStep = 0; timeStep < converter.getLatticeTime(30); ++timeStep)
  {
      translation(0) = 5.0;
      translation(1) = 0.0;
      rotation(2) = 0.;

//      if(timeStep >= converter.getLatticeTime(5.) and timeStep <= converter.getLatticeTime(35.))
//          rotation(2) = 0.314159/3.;

      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

      if(timeStep%converter.getLatticeTime(0.5) == 0)
      {
          timer.print(timeStep);
          std::cout << "Position: " << position(0)*converter.getPhysDeltaX() << ","
                  << position(1)*converter.getPhysDeltaX() << ","
                  << position(2)*converter.getPhysDeltaX() << std::endl;
          std::cout << "Attitude: " << attitude(0) << ","
                  << attitude(1) << ","
                  << attitude(2) << std::endl;
      }

      if(timeStep%converter.getLatticeTime(0.5) == 0)
      {
          lattice.copyDataToCPU();
          vtkWriter.write(timeStep);
          std::cout << "PrintStep: " << print << std::endl;
          ++print;
      }

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          translation(iDim)  = translation(iDim)/converter.getConversionFactorVelocity();
          rotation(iDim)     = rotation(iDim)*converter.getConversionFactorTime();
          position(iDim)    += translation(iDim);
          attitude(iDim)    += rotation(iDim);
      }

      lattice.moveMeshGPU(translation,rotation,position,attitude);
      HANDLE_ERROR(cudaGetLastError());

  }

  lattice.copyDataToCPU();
  vtkWriter.write(converter.getLatticeTime(simTime));

  std::cout << "finished" << std::endl;

}

int main(int argc, char** argv)
{
    const double simTime = 60;
    std::vector<std::string> externalFieldFiles;
    for(unsigned int iArg = 1; iArg < argc; ++iArg)
    {
        std::cout << argv[iArg] << std::endl;
        externalFieldFiles.push_back(argv[iArg]);
    }
    MultipleSteps<31>(simTime,externalFieldFiles);
	return 0;
}
