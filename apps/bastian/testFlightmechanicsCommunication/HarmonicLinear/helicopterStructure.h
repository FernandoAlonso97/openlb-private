/*
 * helicopterStructure.h
 *
 *  Created on: Jan 14, 2019
 *      Author: ga69kiq
 */

#include <cmath>
#include <vector>

constexpr double bladeElementPosition[8] =
{
        1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
};

template<typename T>
class HelicopterData {
public:
    HelicopterData();


    static constexpr unsigned int numBladeElements = 8;
    static constexpr unsigned int numBladeEleMemory = 15;


//    static void transferBladePosToLatticePos(unsigned int bladeElement, T azimuthAngle, std::vector<int> &position, std::vector<int> origin, T spacing)
//    {
//        position[0] = origin[0]+static_cast<int>(std::round(-std::cos(azimuthAngle/180*M_PI)*radialBladePosition(bladeElement)/spacing));
//        position[1] = origin[1]+static_cast<int>(std::round( std::sin(azimuthAngle/180*M_PI)*radialBladePosition(bladeElement)/spacing));
//        position[2] = origin[2];
//    }

    template<template <typename> class Lattice,class BlockLattice>
    static size_t calculateCellPosition(const std::vector<T> rotorPosition,
            std::vector<T> &cellPositionRadial, std::vector<T> &cellPositionAzimuth, std::vector<T> &cellWeight,
            std::vector<std::vector<int>> &rotorCellPositionsRelativ, const std::vector<int> xLimit, const std::vector<int> yLimit, const double spacing,
            BlockLattice& lattice)
    {
        std::vector<T> rotorPositionRelativ = {rotorPosition[0]-xLimit[0], rotorPosition[1]-yLimit[0]};

        T weightAbs{0};
        T fullRotorCells{0};

        const T rotorRadiusUndim = rotorRadius/spacing;

        for(int iX = 0; iX < xLimit[1]-xLimit[0]+1; ++iX)
            for(int iY = 0; iY < yLimit[1]-yLimit[0]+1; ++iY)
            {
                int index = iX*(yLimit[1]-yLimit[0]+1)+iY;

                rotorCellPositionsRelativ[index][0] = iX;
                rotorCellPositionsRelativ[index][1] = iY;
                rotorCellPositionsRelativ[index][2] = 0;
                rotorCellPositionsRelativ[index][3] = index;

                T radialPosition = std::sqrt(std::pow(iX-rotorPositionRelativ[0],2)+std::pow(iY-rotorPositionRelativ[1],2))/rotorRadiusUndim;
                cellPositionRadial[index] = radialPosition;

                if(radialPosition < 0.223)
                {
                    cellWeight[index] = 0;
                    lattice.defineDynamics(iX+xLimit[0], iX+xLimit[0], iY+yLimit[0], iY+yLimit[0], rotorPosition[2], rotorPosition[2], &olb::instances::getBounceBack<T,Lattice>());
                }
                else if(radialPosition <= 1-std::sqrt(0.5)/rotorRadiusUndim)
                {
                    cellWeight[index] = 1;
                    fullRotorCells += 1;
                }
                else if(radialPosition <= 1+std::sqrt(0.5)/rotorRadiusUndim)
                {
                    cellWeight[index] = std::max((1+std::sqrt(0.5)/rotorRadiusUndim-radialPosition)/(std::sqrt(0.5)/rotorRadiusUndim),0.)*0.5;
                }

                T distanceX = static_cast<double>(iX)-rotorPositionRelativ[0];
                T distanceY = static_cast<double>(iY)-rotorPositionRelativ[1];
                cellPositionAzimuth[index] = (std::atan2(-distanceY,distanceX)+M_PI);
                weightAbs += cellWeight[index];
            }

        std::cout << "Sum of weights: " << weightAbs << ", should be " << rotorRadius*rotorRadius*M_PI/(spacing*spacing) << std::endl;
        std::cout << "Number of full rotor cells: " << fullRotorCells << std::endl;


        for(int iX = 0; iX < xLimit[1]-xLimit[0]+1; ++iX)
        {
            for(int iY = 0; iY < yLimit[1]-yLimit[0]+1; ++iY)
            {
                int index = iX*(yLimit[1]-yLimit[0]+1)+iY;
                std::cout << std::setprecision(2) << std::fixed << cellPositionRadial[index] << ",";
            }
            std::cout << std::endl;
        }

        std::cout << "=========" << std::endl;

        for(int iX = 0; iX < xLimit[1]-xLimit[0]+1; ++iX)
        {
            for(int iY = 0; iY < yLimit[1]-yLimit[0]+1; ++iY)
            {
                int index = iX*(yLimit[1]-yLimit[0]+1)+iY;
                std::cout << std::setprecision(2) << std::fixed << cellWeight[index] << ",";
            }
            std::cout << std::endl;
        }

        std::cout << "=========" << std::endl;

        for(int iX = xLimit[1]-xLimit[0]; iX >= 0; --iX)
        {
            for(int iY = 0; iY < yLimit[1]-yLimit[0]+1; ++iY)
            {
                int index = iX*(yLimit[1]-yLimit[0]+1)+iY;
                std::cout << std::setprecision(2) << std::fixed << cellPositionAzimuth[index] << ",";
            }
            std::cout << std::endl;
        }

        return fullRotorCells;

    }

public:

    static constexpr T rotorRadius = 4.92;
    static constexpr T rotorArea = rotorRadius*rotorRadius*M_PI;

    static constexpr T radialBladePosition(unsigned int bladeElement)
    {
        return bladeElementPosition[bladeElement]*rotorRadius;
    }

};


