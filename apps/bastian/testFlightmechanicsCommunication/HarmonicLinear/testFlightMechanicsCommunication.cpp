/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "../../../../src/contrib/communication/NetworkInterface.h"
#include "../../../../src/contrib/communication/NetworkInterface.cpp"
#include "../../../../src/contrib/communication/NetworkDataStructures.h"
#include "helicopterStructure.h"
#include "../../../../src/communication/CPUGPUDataExchange.h"
#include "../../../../src/communication/readFunctorFromGPU.h"
#include "../../../../src/communication/writeFunctorToGPU.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    if(pos[2] == 6)
    {
        std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
        for (int iPop = Lattice<T>::forceIndex; iPop < Lattice<T>::forceIndex+Lattice<T>::d; ++iPop)
        {
          std::cout << cell[iPop].get() << ", ";
        }
        sumZ += cell[Lattice<T>::forceIndex+2];
        std::cout << std::endl;
    }
  }
};

template<typename T>
int addFuselageCells(int resolution, BlockLatticeALE3D<T,Lattice,ExternalFieldALE<T,Lattice,ConstVelocityProviderF>> &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("../Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
//            std::cout << position[0] << "," << position[1] << "," << position[2] << std::endl;
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 1;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

template<typename T, template <typename> class Lattice>
void defineBoundaries(BlockLatticeALE3D<T,Lattice,ExternalFieldALE<T,Lattice,ConstVelocityProviderF>> &lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);


      boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
      boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

//      lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

      boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

}

void MultipleSteps(const double simTime, int iXRightBorderArg)
{
  using ReceiveData = ThrustAtRotorsHarmonic<10>;
  using SendData = VelocityAtRotorsLinear;

  int iXLeftBorder = 0;
  int iXRightBorder = iXRightBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = iXRightBorderArg;
  int iZFrontBorder = 0;
  int iZBackBorder = iXRightBorderArg;
  T rotorRadius = HelicopterData<T>::rotorRadius;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,4.*rotorRadius
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";
  const T velocity[3] = {0,0,0};
  ConstVelocityProviderF<T,Lattice> provider(velocity);
  ExternalFieldALE<T,Lattice,ConstVelocityProviderF> externalFieldALE(provider);
  BlockLatticeALE3D<T, Lattice, ExternalFieldALE<T,Lattice,ConstVelocityProviderF>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1,
          {0, 0, 0}, &bulkDynamics,externalFieldALE);
  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);


  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {iXRightBorder/2.,iYTopBorder/2.,static_cast<T>(iZBackBorder+1)};
  size_t numCells = addFuselageCells(iXRightBorder+1,lattice,rotorPosition[2]);

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::vector<int> rotorLimitsX = {static_cast<int>(std::ceil(rotorPosition[0]-rotorRadius/spacing))-1,
          static_cast<int>(std::floor(rotorPosition[0]+rotorRadius/spacing))+1};
  std::vector<int> rotorLimitsY = {static_cast<int>(std::ceil(rotorPosition[1]-rotorRadius/spacing))-1,
          static_cast<int>(std::floor(rotorPosition[1]+rotorRadius/spacing))+1};

  std::cout << "Rotor Position: " << rotorPosition[0] << "," << rotorPosition[1] << "," << rotorPosition[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotorLimitsX[0] << " to " << rotorLimitsX[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotorLimitsY[0] << " to " << rotorLimitsY[1] << std::endl;

  int numberRotorCells = (rotorLimitsX[1]-rotorLimitsX[0]+1)*(rotorLimitsY[1]-rotorLimitsY[0]+1);

  std::vector<std::vector<int>> rotorCellPositionsRelativ(numberRotorCells, std::vector<int>(4));

  std::vector<T> rotorCellPositionRadial(numberRotorCells);
  std::vector<T> rotorCellPositionAzimuth(numberRotorCells);
  std::vector<T> rotorCellWeight(numberRotorCells);

  std::cout << "Define rotor details... ";

  T fullRotorCells = HelicopterData<T>::template calculateCellPosition<Lattice>(rotorPosition,rotorCellPositionRadial,rotorCellPositionAzimuth,rotorCellWeight,rotorCellPositionsRelativ,
          rotorLimitsX, rotorLimitsY, spacing, lattice);

  std::cout << "Finished!" << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  lattice.updateAnchorPoint(PointXd<T,Lattice>{rotorPosition[0],rotorPosition[1],rotorPosition[2]});

  T u[3] = {0,0,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
  std::cout << "Finished!" << std::endl;

  WriteFunctorByRangeToGPU3D<T,Lattice,ForceFunctor> writer(lattice,rotorLimitsX[0],rotorLimitsX[1],rotorLimitsY[0],rotorLimitsY[1],rotorPosition[2],rotorPosition[2]);
  ReadFunctorByRangeFromGPU3D<T,Lattice,VelocityFunctor> reader(lattice,rotorLimitsX[0],rotorLimitsX[1],rotorLimitsY[0],rotorLimitsY[1],rotorPosition[2],rotorPosition[2]);

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  ReceiveData dataReceive;
  SendData dataSend;
  NetworkInterfaceTCP<ReceiveData,SendData> networkCommunicator(INPUTPORT,OUTPUTIP,OUTPUTPORT,NETWORKBUFFERSIZE,true);

  std::string name;
  std::string directory = "/scratch/BHorvat/flightMechanicsCoupling/V4/";
  name = "testFlightmechanicsCommunicationHarmonicLinear_X";
  name += std::to_string(iXRightBorderArg+1);

  BlockVTKwriter3D<T> vtkWriter( name );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeCoordinates3D<T,Lattice> coordinateFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(coordinateFunctor);

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  dataReceive.simulationStatus = 1;

  std::ofstream fileWriter(directory + "thrustLBM.txt");

  if(fileWriter)
      std::cout << "File open" << std::endl;


  size_t preStep = 0;
  while(dataReceive.simulationStatus != 0)
  {
      for(auto position : rotorCellPositionsRelativ)
      {
          writer.setByLocalIndex(position[0],position[1],position[2])[0] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[1] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[2] = 0;
      }
	  std::cout << "Waiting for receive data" << std::endl;

      networkCommunicator.recieve_data(dataReceive);
	  
      std::cout << "Simulation status is: " << dataReceive.simulationStatus << std::endl;

      PoseXd<T, Lattice> position {{0, 0, 0}, {0, 0, 0}};
      PoseXd<T, Lattice> movement {{0,0,0}, {0,0,0}};

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          movement.position_[iDim]    = dataReceive.velocities[iDim]/converter.getConversionFactorVelocity();
          movement.orientation_[iDim] = dataReceive.rotatorics[iDim];
          position.position_[iDim]    = dataReceive.positions[iDim];
          position.orientation_[iDim] = dataReceive.attitudes[iDim];

          std::cout << "Axis " << iDim << " : " << movement.position_[iDim] << "," << movement.orientation_[iDim] << "," << position.position_[iDim] << "," << position.orientation_[iDim] << std::endl;
      }

      std::vector<T> thrustAbs(3,0);

      for(auto pos : rotorCellPositionsRelativ)
      {
          std::vector<T> thrustPerCell(3,0);
          int index = pos[3];
          for (unsigned int iHarmonics=0; iHarmonics<10; ++iHarmonics)
          {
              thrustPerCell[0] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosX[iHarmonics];
              thrustPerCell[0] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinX[iHarmonics];

              thrustPerCell[1] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosY[iHarmonics];
              thrustPerCell[1] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinY[iHarmonics];

              thrustPerCell[2] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosZ[iHarmonics];
              thrustPerCell[2] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinZ[iHarmonics];

          }

          thrustPerCell[0] += dataReceive.presMainMeanX;
          thrustPerCell[1] += dataReceive.presMainMeanY;

          if(dataReceive.presMainMeanZ < 1.)
          {
              std::cout << "Low thrust: " << preStep << std::endl;
              dataReceive.presMainMeanZ = 20000;
          }

          thrustPerCell[2] += dataReceive.presMainMeanZ;

          thrustPerCell[0] *= rotorCellWeight[index]*1.067;
          thrustPerCell[1] *= rotorCellWeight[index]*1.067;
          thrustPerCell[2] *= rotorCellWeight[index]*1.067;

          fileWriter << rotorCellPositionAzimuth[index] << "  " << rotorCellPositionRadial[index] << "  " <<
                  thrustPerCell[0] << "  " << thrustPerCell[1] << "  " << thrustPerCell[2] << " " << rotorCellWeight[index] << std::endl;

          thrustAbs[0] += thrustPerCell[0];
          thrustAbs[1] += thrustPerCell[1];
          thrustAbs[2] += thrustPerCell[2];

          writer.setByLocalIndex(pos[0], pos[1], pos[2])[0] = converter.getLatticeForce(thrustPerCell[0]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[1] = converter.getLatticeForce(thrustPerCell[1]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[2] = converter.getLatticeForce(thrustPerCell[2]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;

//          T force[3] = {converter.getLatticeForce(thrustPerCell[0]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea,
//                  converter.getLatticeForce(thrustPerCell[1]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea,
//                  converter.getLatticeForce(thrustPerCell[2]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea};
//          lattice.defineForce(pos[0]+rotorLimitsX[0],pos[0]+rotorLimitsX[0],pos[1]+rotorLimitsY[0],
//                  pos[1]+rotorLimitsY[0],
//                  pos[2]+rotorPosition[2],pos[2]+rotorPosition[2],force);
      }

      std::cout << "Thrust in X direction: " << thrustAbs[0]*std::pow(spacing,2)/(1.225*HelicopterData<T>::rotorArea)
              << " (" << dataReceive.presMainMeanX << ")" << std::endl;
      std::cout << "Thrust in Y direction: " << thrustAbs[1]*std::pow(spacing,2)/(1.225*HelicopterData<T>::rotorArea)
                      << " (" << dataReceive.presMainMeanY << ")" << std::endl;
      std::cout << "Thrust in Z direction: " << thrustAbs[2]*std::pow(spacing,2)/(1.225*HelicopterData<T>::rotorArea)
                      << " (" << dataReceive.presMainMeanZ << ")" << std::endl;

      unsigned int trimTime = converter.getLatticeTime(9);
      writer.transferAndWrite();

      if(preStep == 0)
	  {
          trimTime = converter.getLatticeTime(15);
	  }

      for(unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
      {
          lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

          for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
              movement.orientation_[iDim] = 0;

          lattice.moveMeshGPU(movement, position);

      }

      lattice.copyDataToCPU();
      vtkWriter.write(preStep);

      std::cout << "Trimstep " << preStep << " finished" << std::endl;

      reader.readAndTransfer();

      std::vector<T> velAbs(3,0);

      for(auto pos : rotorCellPositionsRelativ)
      {
          size_t index = pos[3];
          std::vector<T> velocity(3,0);

          if(rotorCellWeight[index] == 1)
          {
              for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
              {
                  velocity[iDim] = converter.getPhysVelocity(reader.getByLocalIndex(pos[0],pos[1],pos[2])[iDim]);
                  velAbs[iDim] += velocity[iDim]*spacing*spacing;
                  dataSend.velocityMainMean[iDim] += velocity[iDim]/fullRotorCells;
                  dataSend.velocityMainSin[iDim] += std::sin(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
                  dataSend.velocityMainCos[iDim] += std::cos(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
              }
          }
      }

      std::cout << "Mean induced velocity is " << dataSend.velocityMainMean[2] << " m/s" << std::endl;

      networkCommunicator.send_data(dataSend);

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          dataSend.velocityMainMean[iDim] = 0;
          dataSend.velocityMainSin[iDim] = 0;
          dataSend.velocityMainCos[iDim] = 0;
      }

      ++preStep;

	  if(dataReceive.simulationStatus == 0)
	  {
	      std::cout << "New loop" << std::endl;
		  dataReceive.simulationStatus = 2;
	  }
  }

  std::cout << "Finished trim in " << preStep << " steps" << std::endl;

  std::cout << "Starting time simulation" << std::endl;


  for(unsigned int timeStep = 0; timeStep < converter.getLatticeTime(10); ++timeStep)
  {
      for(auto position : rotorCellPositionsRelativ)
      {
          writer.setByLocalIndex(position[0],position[1],position[2])[0] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[1] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[2] = 0;
      }

      networkCommunicator.recieve_data(dataReceive);

      PoseXd<T, Lattice> position {{0, 0, 0}, {0, 0, 0}};
      PoseXd<T, Lattice> movement {{0,0,0}, {0,0,0}};

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          movement.position_[iDim]    = dataReceive.velocities[iDim]/converter.getConversionFactorVelocity();
          movement.orientation_[iDim] = dataReceive.rotatorics[iDim];
          position.position_[iDim]    = dataReceive.positions[iDim];
          position.orientation_[iDim] = dataReceive.attitudes[iDim];
      }

      std::vector<T> thrustAbs(3,0);

      for(auto pos : rotorCellPositionsRelativ)
      {
          std::vector<T> thrustPerCell(3,0);
          int index = pos[3];
          for (unsigned int iHarmonics=0; iHarmonics<10; ++iHarmonics)
          {
              thrustPerCell[0] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosX[iHarmonics];
              thrustPerCell[0] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinX[iHarmonics];

              thrustPerCell[1] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosY[iHarmonics];
              thrustPerCell[1] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinY[iHarmonics];

              thrustPerCell[2] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosZ[iHarmonics];
              thrustPerCell[2] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinZ[iHarmonics];
          }

          thrustPerCell[0] += dataReceive.presMainMeanX;
          thrustPerCell[1] += dataReceive.presMainMeanY;
          thrustPerCell[2] += dataReceive.presMainMeanZ;

          thrustPerCell[0] *= rotorCellWeight[index];
          thrustPerCell[1] *= rotorCellWeight[index];
          thrustPerCell[2] *= rotorCellWeight[index];

          thrustAbs[2] += thrustPerCell[2];

          writer.setByLocalIndex(pos[0], pos[1], pos[2])[0] = converter.getLatticeForce(thrustPerCell[0]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[1] = converter.getLatticeForce(thrustPerCell[1]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[2] = converter.getLatticeForce(thrustPerCell[2]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
      }

      writer.transferAndWrite();

      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

      // Print simulation status information
      if(timeStep%converter.getLatticeTime(0.5) == 0)
          timer.print(timeStep);

      if(timeStep%converter.getLatticeTime(5.0) == 0)
      {
          lattice.copyDataToCPU();
          vtkWriter.write(timeStep+preStep+1);
      }

      lattice.moveMeshGPU(movement, position);

      reader.readAndTransfer();

      std::vector<T> velAbs(3,0);

      for(auto pos : rotorCellPositionsRelativ)
      {
          size_t index = pos[3];
          std::vector<T> velocity(3,0);

          if(rotorCellWeight[index] == 1)
          {
              for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
              {
                  velocity[iDim] = converter.getPhysVelocity(reader.getByLocalIndex(pos[0],pos[1],pos[2])[iDim]);
                  velAbs[iDim] += velocity[iDim]*spacing*spacing;
                  dataSend.velocityMainMean[iDim] += velocity[iDim]/fullRotorCells;
                  dataSend.velocityMainSin[iDim] += std::sin(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
                  dataSend.velocityMainCos[iDim] += std::cos(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
              }
          }
      }

      networkCommunicator.send_data(dataSend);

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          dataSend.velocityMainMean[iDim] = 0;
          dataSend.velocityMainSin[iDim] = 0;
          dataSend.velocityMainCos[iDim] = 0;
      }

  }

  timer.stop();

}

int main()
{
    const double simTime = 1;
    MultipleSteps(simTime,31);
	return 0;
}
