/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "../../../src/contrib/communication/NetworkInterface.h"
#include "../../../src/contrib/communication/NetworkInterface.cpp"
#include "../../../src/contrib/communication/NetworkDataStructures.h"
#include "helicopterStructure.h"
#include "../../../src/communication/CPUGPUDataExchange.h"
#include "../../../src/communication/readFunctorFromGPU.h"
#include "../../../src/communication/writeFunctorToGPU.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    if(pos[2] == 6)
    {
        std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
        for (int iPop = Lattice<T>::forceIndex; iPop < Lattice<T>::forceIndex+Lattice<T>::d; ++iPop)
        {
          std::cout << cell[iPop].get() << ", ";
        }
        sumZ += cell[Lattice<T>::forceIndex+2];
        std::cout << std::endl;
    }
  }
};

void addFuselageCells(int resolution, BlockLattice3D<T,Lattice> &lattice)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("/media/ga69kiq/work/BHorvat/GitRepositories/openlb/apps/bastian/testFlightmechanicsCommunication/Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            std::cout << position[0] << "," << position[1] << "," << position[2] << std::endl;
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }


    std::cout << "Fuselage cells: " << fuselageCells << std::endl;
}

template<typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T,Lattice> &lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);


      boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
      boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

    //  lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());


//      boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
//      boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
//      boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
//      boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

//      boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
//      boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
//      boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
//      boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

//      boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
//      boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
//      boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
//      boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

      boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

    //  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

}

std::set<std::vector<int>> computeRotorCells(std::vector<std::vector<int>> &positionVector, std::vector<int> rotorPosition, T spacing)
{
    std::set<std::vector<int>> set;


    for(T iAzimuth = 0; iAzimuth < 72; ++iAzimuth)
    {
        for(unsigned int iBladeElement = 0; iBladeElement < HelicopterData<T>::numBladeElements; ++iBladeElement)
        {

            HelicopterData<T>::transferBladePosToLatticePos(iBladeElement,iAzimuth*5.,
                    positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement],rotorPosition,spacing);
            set.insert(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement]);
        }
    }

    return set;
}

void MultipleSteps(const double simTime, int iXRightBorderArg)
{
  using ReceiveData = ThrustAtRotorsHarmonic<10>;
  using SendData = VelocityAtRotorsLinear;

  int iXLeftBorder = 0;
  int iXRightBorder = iXRightBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = iXRightBorderArg;
  int iZFrontBorder = 0;
  int iZBackBorder = iXRightBorderArg;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  int rotorLimitsX[2] = {(iXRightBorder+1)/4, (iXRightBorder+1)*3/4-1};
  int rotorLimitsY[2] = {(iYTopBorder+1)/4, (iYTopBorder+1)*3/4-1};
  std::vector<int> rotorPosition = {(iXRightBorder+1)/2,(iYTopBorder+1)/2,(iZBackBorder+1)/4-2};

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.1*1.0/std::sqrt(3)
          ,20.
          ,40.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T deltaX = converter.getConversionFactorLength();

  T omega = 1.96;//converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1, &bulkDynamics);
  std::cout << "Finished!" << std::endl;

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);
  addFuselageCells(iXRightBorder+1,lattice);
  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {

            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }


//  for (int iX = iXLeftBorder+1; iX <= iXRightBorder-1; ++iX)
//      for (int iY = iYBottomBorder; iY <= iYBottomBorder; ++iY)
//          for (int iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; ++iZ)
//          {
//
//            T vel[Lattice<T>::d] = { 0., converter.getLatticeVelocity(6.), 0.};
//            T rho[1];
//            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
//          }

  std::cout << "Finished!" << std::endl;

  WriteFunctorByRangeToGPU3D<T,Lattice,ForceFunctor> writer(lattice,rotorLimitsX[0],rotorLimitsX[1],rotorLimitsY[0],rotorLimitsY[1],rotorPosition[2],rotorPosition[2]);
  ReadFunctorByRangeFromGPU3D<T,Lattice,VelocityFunctor> reader(lattice,rotorLimitsX[0],rotorLimitsX[1],rotorLimitsY[0],rotorLimitsY[1],rotorPosition[2],rotorPosition[2]);

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  std::vector<std::vector<int>> positionVector(72*HelicopterData<T>::numBladeElements,std::vector<int>(3));
  set<std::vector<int>> positionSet = computeRotorCells(positionVector,rotorPosition,deltaX);

  ReceiveData dataReceive;
  SendData dataSend;
  NetworkInterfaceTCP<ReceiveData,SendData> networkCommunicator(INPUTPORT,OUTPUTIP,OUTPUTPORT,NETWORKBUFFERSIZE,true);

  std::string name;
  name = "testFlightmechanicsCommunication_X";
  name += std::to_string(iXRightBorderArg+1);

  BlockVTKwriter3D<T> vtkWriter( name );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/testFlightmechanicsCommunication/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  dataReceive.simulationStatus = 2;

  size_t preStep = 0;
  while(dataReceive.simulationStatus != 0)
  {
      for(auto iPosition : positionSet)
      {
          writer.setByGlobalIndex(iPosition[0],iPosition[1],iPosition[2])[0] = 0;
          writer.setByGlobalIndex(iPosition[0],iPosition[1],iPosition[2])[1] = 0;
          writer.setByGlobalIndex(iPosition[0],iPosition[1],iPosition[2])[2] = 0;
      }

      networkCommunicator.recieve_data(dataReceive);

      dataReceive.AbsThrust[0] = -converter.getLatticeForce(dataReceive.AbsThrust[0]*deltaX*deltaX/1.225);
      dataReceive.AbsThrust[1] = -converter.getLatticeForce(dataReceive.AbsThrust[1]*deltaX*deltaX/1.225);
      dataReceive.AbsThrust[2] = converter.getLatticeForce(dataReceive.AbsThrust[2]*deltaX*deltaX/1.225);

      double thrustBladeElements[3] = {0,0,0};

      for(unsigned int iAzimuth = 0; iAzimuth < 72; ++iAzimuth)
          for(unsigned int iBladeElement = 0; iBladeElement < HelicopterData<T>::numBladeElements; ++iBladeElement)
          {
    //              dataReceive.presMainMeanX[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement] = converter.getLatticeForce(dataReceive.presMainMeanX[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement]*deltaX*deltaX/1.225);
    //              dataReceive.presMainMeanY[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement] = converter.getLatticeForce(dataReceive.presMainMeanY[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement]*deltaX*deltaX/1.225);
    //              dataReceive.presMainMeanZ[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement] = converter.getLatticeForce(dataReceive.presMainMeanZ[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement]*deltaX*deltaX/1.225);
              thrustBladeElements[0] += dataReceive.presMainMeanX[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement];
              thrustBladeElements[1] += dataReceive.presMainMeanY[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement];
              thrustBladeElements[2] += dataReceive.presMainMeanZ[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement];
          }

      for(T iAzimuth = 0; iAzimuth < 72; ++iAzimuth)
      {
          for(unsigned int iBladeElement = 0; iBladeElement < HelicopterData<T>::numBladeElements; ++iBladeElement)
          {
              writer.setByGlobalIndex(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][0]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][1]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][2])[0] +=
                          dataReceive.presMainMeanX[static_cast<int>(iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement)]
                          /thrustBladeElements[0]*dataReceive.AbsThrust[0];

              writer.setByGlobalIndex(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][0]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][1]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][2])[1] +=
                          dataReceive.presMainMeanY[static_cast<int>(iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement)]
                          /thrustBladeElements[1]*dataReceive.AbsThrust[1];

              writer.setByGlobalIndex(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][0]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][1]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][2])[2] +=
                          dataReceive.presMainMeanZ[static_cast<int>(iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement)]
                          /thrustBladeElements[2]*dataReceive.AbsThrust[2];
          }
      }

      writer.transferAndWrite();

      for(unsigned int trimStep = 0; trimStep < converter.getLatticeTime(3); ++trimStep)
          lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

      vtkWriter.write(preStep);

      std::cout << "Trimstep finished" << std::endl;

      reader.readAndTransfer();

      for(unsigned int iX = 0; iX < (iXRightBorder+1)/2; ++iX)
          for(unsigned int iY = 0; iX < (iYTopBorder+1)/2; ++iY)
          {
              dataSend.velocityMainU[iX][iY] = converter.getPhysVelocity(reader.getByGlobalIndex(iX,iY,rotorPosition[2])[0]);
              dataSend.velocityMainV[iX][iY] = converter.getPhysVelocity(reader.getByGlobalIndex(iX,iY,rotorPosition[2])[1]);
              dataSend.velocityMainW[iX][iY] = converter.getPhysVelocity(reader.getByGlobalIndex(iX,iY,rotorPosition[2])[2]);
          }

      lattice.copyDataToCPU();

      networkCommunicator.send_data(dataSend);

      ++preStep;
      if(dataReceive.simulationStatus == 0)
	  {
	      std::cout << "New loop" << std::endl;
		  dataReceive.simulationStatus = 2;
	  }
  }

  for(unsigned int timeStep = 1; timeStep < converter.getLatticeTime(simTime); ++timeStep)
  {
      for(auto iPosition : positionSet)
      {
          writer.setByGlobalIndex(iPosition[0],iPosition[1],iPosition[2])[0] = 0;
          writer.setByGlobalIndex(iPosition[0],iPosition[1],iPosition[2])[1] = 0;
          writer.setByGlobalIndex(iPosition[0],iPosition[1],iPosition[2])[2] = 0;
      }

      networkCommunicator.recieve_data(dataReceive);
      std::cout << "Received timestep: " << timeStep << std::endl;

      dataReceive.AbsThrust[0] = -converter.getLatticeForce(dataReceive.AbsThrust[0]*deltaX*deltaX/1.225);
      dataReceive.AbsThrust[1] = -converter.getLatticeForce(dataReceive.AbsThrust[1]*deltaX*deltaX/1.225);
      dataReceive.AbsThrust[2] = -converter.getLatticeForce(dataReceive.AbsThrust[2]*deltaX*deltaX/1.225);

      double thrustBladeElements[3] = {0,0,0};

      for(unsigned int iAzimuth = 0; iAzimuth < 72; ++iAzimuth)
          for(unsigned int iBladeElement = 0; iBladeElement < HelicopterData<T>::numBladeElements; ++iBladeElement)
          {
//              dataReceive.presMainMeanX[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement] = converter.getLatticeForce(dataReceive.presMainMeanX[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement]*deltaX*deltaX/1.225);
//              dataReceive.presMainMeanY[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement] = converter.getLatticeForce(dataReceive.presMainMeanY[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement]*deltaX*deltaX/1.225);
//              dataReceive.presMainMeanZ[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement] = converter.getLatticeForce(dataReceive.presMainMeanZ[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement]*deltaX*deltaX/1.225);
              thrustBladeElements[0] += dataReceive.presMainMeanX[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement];
              thrustBladeElements[1] += dataReceive.presMainMeanY[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement];
              thrustBladeElements[2] += dataReceive.presMainMeanZ[iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement];
          }

      for(T iAzimuth = 0; iAzimuth < 72; ++iAzimuth)
      {
          for(unsigned int iBladeElement = 0; iBladeElement < HelicopterData<T>::numBladeElements; ++iBladeElement)
          {
              writer.setByGlobalIndex(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][0]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][1]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][2])[0] +=
                          dataReceive.presMainMeanX[static_cast<int>(iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement)]
                          /thrustBladeElements[0]*dataReceive.AbsThrust[0];

              writer.setByGlobalIndex(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][0]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][1]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][2])[1] +=
                          dataReceive.presMainMeanY[static_cast<int>(iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement)]
                          /thrustBladeElements[1]*dataReceive.AbsThrust[1];

              writer.setByGlobalIndex(positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][0]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][1]
                                     ,positionVector[iAzimuth*HelicopterData<T>::numBladeElements+iBladeElement][2])[2] +=
                          dataReceive.presMainMeanZ[static_cast<int>(iAzimuth*HelicopterData<T>::numBladeEleMemory+iBladeElement)]
                          /thrustBladeElements[2]*dataReceive.AbsThrust[2];
          }
      }

      writer.transferAndWrite();

      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

      reader.readAndTransfer();

      for(unsigned int iX = 0; iX < (iXRightBorder+1)/2; ++iX)
          for(unsigned int iY = 0; iX < (iYTopBorder+1)/2; ++iY)
          {
              dataSend.velocityMainU[iX][iY] = converter.getPhysVelocity(reader.getByGlobalIndex(iX,iY,rotorPosition[2])[0]);
              dataSend.velocityMainV[iX][iY] = converter.getPhysVelocity(reader.getByGlobalIndex(iX,iY,rotorPosition[2])[1]);
              dataSend.velocityMainW[iX][iY] = converter.getPhysVelocity(reader.getByGlobalIndex(iX,iY,rotorPosition[2])[2]);
          }

      for(unsigned int iX = 0; iX < (iXRightBorder+1)/2; ++iX)
          for(unsigned int iY = 0; iX < (iYTopBorder+1)/2; ++iY)
          {
              if(IsInsideMain)
              {

                          dataSend.velocityMainMean[0] += reader.getByGlobalIndex(iX,iY,rotorPosition[2])[0];
                          dataSend.velocityMainMean[1] += reader.getByGlobalIndex(iX,iY,rotorPosition[2])[1];
                          dataSend.velocityMainMean[2] += reader.getByGlobalIndex(iX,iY,rotorPosition[2])[2];

                          atomicAdd(& pointersDevice.VelocitiesDeviceMainSin[0].x,sin(psiMR)*u*radialDistMR/geometricalRotorDataOnDevice->radiusMR);
                          atomicAdd(& pointersDevice.VelocitiesDeviceMainSin[0].y,sin(psiMR)*v*radialDistMR/geometricalRotorDataOnDevice->radiusMR);
                          atomicAdd(& pointersDevice.VelocitiesDeviceMainSin[0].z,sin(psiMR)*w*radialDistMR/geometricalRotorDataOnDevice->radiusMR);

                          atomicAdd(& pointersDevice.VelocitiesDeviceMainCos[0].x,cos(psiMR)*u*radialDistMR/geometricalRotorDataOnDevice->radiusMR);
                          atomicAdd(& pointersDevice.VelocitiesDeviceMainCos[0].y,cos(psiMR)*v*radialDistMR/geometricalRotorDataOnDevice->radiusMR);
                          atomicAdd(& pointersDevice.VelocitiesDeviceMainCos[0].z,cos(psiMR)*w*radialDistMR/geometricalRotorDataOnDevice->radiusMR);

                          //printf("radial Distance %f",radialDist/geometricalRotorDataOnDevice->radiusMR);
                          //printf("radial Distance %f radius %f \n",radialDistMR/geometricalRotorDataOnDevice->radiusMR, geometricalRotorDataOnDevice->radiusMR );

              }

      if(timeStep%converter.getLatticeTime(0.5) == 0)
            timer.print(timeStep,2);


      if(timeStep%converter.getLatticeTime(1.0) == 0)
      {
          lattice.copyDataToCPU();
          vtkWriter.write(timeStep+preStep);
      }

      networkCommunicator.send_data(dataSend);
  }

  timer.stop();

}

int main()
{
    const double simTime = 10;
    MultipleSteps(simTime,31);
	return 0;
}
