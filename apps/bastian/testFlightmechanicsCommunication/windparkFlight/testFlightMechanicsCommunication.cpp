/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/momentaOnBoundaries.h"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include "contrib/ALE/readExternalField.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, class Blocklattice>
int addFuselageCells(int resolution, Blocklattice &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("../Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;
    else
        std::cout << "File not open" << std::endl;

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 0;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 0,1,0>,PlaneFdBoundaryProcessor3D
        <T,Lattice, 1,1>> plane0P(omega,0.05);
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 1,1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 1,1>> plane1P(omega,0.05);
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 2,-1, 1>> edge2NP(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1,-1>> edge2PN(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;,
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1, 1>> edge2PP(omega,0.05);


    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

template<unsigned int RESOLUTION>
void MultipleSteps(const double simTime)
{
  using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
  using SendDataGensim = VelocityAtRotorsLinear;
  using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;

  int iXLeftBorder = 0;
  int iXRightBorder = RESOLUTION;
  int iYBottomBorder = 0;
  int iYTopBorder = RESOLUTION;
  int iZFrontBorder = 0;
  int iZBackBorder = RESOLUTION;

  T const rotorRadius = 4.92;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,4.*4.92
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";

  std::vector<int64_t> extent(3);
  T const velocityConversion = converter.getConversionFactorVelocity();
  T** externalField = readExternalField<T>("/media/administrator/HTMWTUM/Privat/BHorvat/HeliOW/tecIO/examples/readszl/IAG_TestData_C1_interpolate_32_Cells_partial.szplt"
		  ,extent,velocityConversion);

  Vec3<T> outsideVelocity{converter.getLatticeVelocity(0.),converter.getLatticeVelocity(-15.),0.};

//  PredefinedTimeVariantExternalField<T,Lattice> externalFieldClass(externalFields, outsideVelocity,
//          2.0/converter.getPhysDeltaT(), extent[0], extent[1], extent[2], 1.0);
   PredefinedExternalField<T,Lattice> externalFieldClass(externalField, outsideVelocity, extent[0], extent[1], extent[2]);
// ConstExternalField<T,Lattice> externalFieldClass(outsideVelocity);

  BlockLatticeALE3D<T, Lattice, decltype(externalFieldClass)> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1,
          {0,0,0}, &bulkDynamics, externalFieldClass);

  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {static_cast<T>(iXRightBorder/2.),static_cast<T>(iYTopBorder/2.),static_cast<T>(iZBackBorder+1)};
  size_t numCells = addFuselageCells(iXRightBorder+1,lattice,rotorPosition[2]);

  Rotor<T> rotor(rotorRadius,rotorPosition,spacing);
  RotorCellData<T> rotorCellData(rotor,lattice, converter);
  CouplingType::CouplingDataType couplingData(rotorCellData);

  for(unsigned int iCell = 0; iCell< rotorCellData._numberRotorCells; ++iCell)
      if(rotorCellData._cellPositionRadialCPU[iCell] < 0.223)
      {
          size_t position[3];
          util::getCellIndices3D(rotorCellData._cellIndexCPU[iCell],lattice.getNy(),lattice.getNz(),position);
          lattice.defineDynamics(position[0],position[1],position[2],&instances::getBounceBack<T,Lattice>());
      }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::cout << "Rotor Position: " << rotor.getCenter()[0] << "," << rotor.getCenter()[1] << "," << rotor.getCenter()[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotor.getXLimits()[0] << " to " << rotor.getXLimits()[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotor.getYLimits()[0] << " to " << rotor.getYLimits()[1] << std::endl;

  std::cout << "Number of theoretical rotor cells: " << rotor.getNumberRotorCells() << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::cout << "Undim: " << converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea() << std::endl;

  lattice.updateAnchorPoint(Vec3<T>{rotorPosition[0],rotorPosition[1],rotorPosition[2]});
  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  T force[3] = {0.,0.,converter.getLatticeForce(std::pow(spacing,2)*20000./1.225)/76.046648};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string name;
  std::string directory = "/media/administrator/DataStorage/windparkFlight/";
  name = "externalFieldCoupling_X";
  name += std::to_string(iXRightBorder+1);
  std::string nameTrim = name + "_trim";

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriterTrim( nameTrim );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(velocityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(fluidMaskFunctor);

  vtkWriterTrim.write(0);

  ReceiveDataGensim dataReceiveGensim;
  SendDataGensim dataSendGensim;

  NetworkInterfaceTCP<ReceiveDataGensim,SendDataGensim> networkCommunicatorGensim(8888,OUTPUTIP,8888,NETWORKBUFFERSIZE,true);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  Vec3<T> position{0,0,0};
  Vec3<T> attitude{0,0,0};
  Vec3<T> velocity{30,0,0};
  Vec3<T> rotation{0,0,0};
  Vec3<T> attitudeDelta{0,0,M_PI_4};

  Vec3<T> positionALEInit{-150,1,1};
  Vec3<T> positionALE{0,0,0};
  Vec3<T> velocityNondim{0,converter.getLatticeVelocity(velocity(1)),0};
  Vec3<T> rotationNondim{0,0,0};

  positionALE = positionALEInit;

  size_t preStep = 1;
  while(dataReceiveGensim.simulationStatus != 0)
  {
      networkCommunicatorGensim.recieve_data(dataReceiveGensim);
      dataReceiveGensim.positions[2] = 0;

      CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());

      std::cout << "Thrust: " << dataReceiveGensim.presMainMeanX << "," << dataReceiveGensim.presMainMeanY << "," << dataReceiveGensim.presMainMeanZ << std::endl;

      std::cout << "Simulation status is: " << dataReceiveGensim.simulationStatus << std::endl;

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
    	  velocityNondim(iDim) = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
          rotationNondim(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
          positionALE(iDim)       = positionALEInit(iDim) + dataReceiveGensim.positions[iDim]/spacing;
          attitude(iDim)          = dataReceiveGensim.attitudes[iDim]+attitudeDelta(iDim);

          std::cout << "Axis " << iDim << " : " << dataReceiveGensim.velocities[iDim] << ","
                  << dataReceiveGensim.rotatorics[iDim] << "," << dataReceiveGensim.positions[iDim] << ","
                  << dataReceiveGensim.attitudes[iDim] << std::endl;
      }

      unsigned int trimTime = converter.getLatticeTime(8);

      if(preStep == 1)
      {
          trimTime = converter.getLatticeTime(8);
      }

      for(unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
      {
          lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
          lattice.moveMeshGPU(velocityNondim,rotationNondim,positionALE,attitude);
          HANDLE_ERROR(cudaGetLastError());
      }

      lattice.copyDataToCPU();
      vtkWriterTrim.write(preStep);

      std::cout << "Trimstep " << preStep << " finished" << std::endl;

      CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

      std::cout << "vi: " << dataSendGensim.velocityMainMean[0] << "," << dataSendGensim.velocityMainMean[1]
                                                          << "," << dataSendGensim.velocityMainMean[2] << std::endl;

      networkCommunicatorGensim.send_data(dataSendGensim);

	  std::cout << "Finished sending inflow" << std::endl;
      ++preStep;

      if(dataReceiveGensim.simulationStatus == 0)
      {
          break;
      }

  }

  std::string nameSim = name + "_sim";
  BlockVTKwriter3D<T> vtkWriterSim( nameSim );
  vtkWriterSim.addFunctor(densityFunctor);
  vtkWriterSim.addFunctor(velocityFunctor);
  vtkWriterSim.addFunctor(physVelocityFunctor);
  vtkWriterSim.addFunctor(forceFunctor);
  vtkWriterSim.addFunctor(fluidMaskFunctor);

  vtkWriterSim.write(0);

  std::cout << "Starting time simulation" << std::endl;
  unsigned int print = 1;

  for(unsigned int timeStep = 1; timeStep < converter.getLatticeTime(simTime); ++timeStep)
  {
        networkCommunicatorGensim.recieve_data(dataReceiveGensim);
        dataReceiveGensim.positions[2] += 1999.42;

        CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
        	velocityNondim(iDim) = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
            rotationNondim(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
            positionALE(iDim)       = positionALEInit(iDim) + dataReceiveGensim.positions[iDim]/spacing;
            attitude(iDim)          = dataReceiveGensim.attitudes[iDim]+attitudeDelta(iDim);
        }

        if(timeStep%converter.getLatticeTime(0.1) == 0)
        {
            std::cout << "position = " << dataReceiveGensim.positions[0] << "," << dataReceiveGensim.positions[1] << "," << dataReceiveGensim.positions[2] << std::endl;
            std::cout << "positionALE = " << positionALE(0) << "," << positionALE(1) << "," << positionALE(2) << std::endl;
        }

        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

        if(timeStep%converter.getLatticeTime(0.1) == 0)
        {
            lattice.copyDataToCPU();
            vtkWriterSim.write(std::round(converter.getPhysTime(timeStep)*1000));
            ++print;
        }

        lattice.moveMeshGPU(velocityNondim,rotationNondim,positionALE,attitude);
        HANDLE_ERROR(cudaGetLastError());


        CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

        networkCommunicatorGensim.send_data(dataSendGensim);

  }

  lattice.copyDataToCPU();
  vtkWriterSim.write(converter.getLatticeTime(simTime));

  std::cout << "finished" << std::endl;

}

int main(int argc, char** argv)
{
    const double simTime = 60;
    MultipleSteps<31>(simTime);
	return 0;
}
