/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/momentaOnBoundaries.h"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include "contrib/ALE/readExternalField.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 0,1,0>,PlaneFdBoundaryProcessor3D
        <T,Lattice, 1,1>> plane0P(omega,0.05);
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 1,1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 1,1>> plane1P(omega,0.05);
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 2,-1, 1>> edge2NP(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1,-1>> edge2PN(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;,
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1, 1>> edge2PP(omega,0.05);


    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

template<unsigned int RESOLUTION>
void MultipleSteps(const double simTime)
{
  using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
  using SendDataGensim = VelocityAtRotorsLinear;
  using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;

  int iXLeftBorder = 0;
  int iXRightBorder = (RESOLUTION+1)*1.5-1;
  int iYBottomBorder = 0;
  int iYTopBorder = (RESOLUTION+1)*1.5-1;
  int iZFrontBorder = 0;
  int iZBackBorder = (RESOLUTION+1)*1.5-1;

  T const rotorRadius = 4.92;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          RESOLUTION
          ,0.3*1.0/std::sqrt(3)
          ,4.*4.92
          ,40.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";

  std::vector<int64_t> extent(3);
  T const velocityConversion = converter.getConversionFactorVelocity();
  T** externalField = readExternalField<T>("/media/administrator/HTMWTUM/Privat/BHorvat/HeliOW/tecIO/examples/readszl/IAG_TestData_C1_interpolate_partial.szplt"
		  ,extent,velocityConversion);

  Vec3<T> outsideVelocity{converter.getLatticeVelocity(0.),converter.getLatticeVelocity(-15.),0.};

//  PredefinedTimeVariantExternalField<T,Lattice> externalFieldClass(externalFields, outsideVelocity,
//          2.0/converter.getPhysDeltaT(), extent[0], extent[1], extent[2], 1.0);
   PredefinedExternalField<T,Lattice> externalFieldClass(externalField, outsideVelocity, extent[0], extent[1], extent[2]);
// ConstExternalField<T,Lattice> externalFieldClass(outsideVelocity);

  BlockLatticeALE3D<T, Lattice, decltype(externalFieldClass)> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1,
          {0,0,0}, &bulkDynamics, externalFieldClass);

  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);

  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  T force[3] = {0.,0.,converter.getLatticeForce(std::pow(spacing,2)*20000./1.225)/76.046648};
  T center[3] = {iXRightBorder/2.,iYTopBorder/2.,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            if(iZ == (iZBackBorder+1)/2)
            {
            	T radialDistance = sqrt(std::pow(iX-center[0],2)+std::pow(iY-center[1],2))*spacing;
            	if(radialDistance <= 4.92)
            		lattice.defineForce(iX,iX,iY,iY,iZ,iZ,force);
            }
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string name;
  std::string directory = "/media/administrator/DataStorage/windparkFlightLight/";
  name = "externalFieldCoupling_X";
  name += std::to_string(iXRightBorder+1);
  std::string nameTrim = name + "_trim";

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  Vec3<T> position{0,0,0};
  Vec3<T> attitude{0,0,0};
  Vec3<T> velocity{30,0,0};
  Vec3<T> rotation{0,0,0};

  Vec3<T> positionALEInit{-150,1,1};
  Vec3<T> positionALE{0,0,0};
  Vec3<T> velocityNondim{0,converter.getLatticeVelocity(velocity(1)),0};
  Vec3<T> rotationNondim{0,0,0};

  std::string nameSim = name + "_sim";
  BlockVTKwriter3D<T> vtkWriterSim( nameSim );
  vtkWriterSim.addFunctor(densityFunctor);
  vtkWriterSim.addFunctor(velocityFunctor);
  vtkWriterSim.addFunctor(physVelocityFunctor);
  vtkWriterSim.addFunctor(forceFunctor);
  vtkWriterSim.addFunctor(fluidMaskFunctor);

  vtkWriterSim.write(0);

  positionALE = positionALEInit;

  std::cout << "Starting time simulation" << std::endl;
  unsigned int print = 1;

//  for(unsigned int timeStep = 1; timeStep < 10; ++timeStep)
  for(unsigned int timeStep = 0; timeStep < converter.getLatticeTime(simTime); ++timeStep)
  {

        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
        lattice.moveMeshGPU(velocityNondim,rotationNondim,positionALE,attitude);

        if(timeStep%converter.getLatticeTime(0.2) == 0)
        {
        	T seconds = converter.getPhysTime(timeStep);
        	timer.update(timeStep);
        	T computeTime = timer.getTotalRealTime();
        	std::cout << "Finished " << seconds << " simulation time after " << computeTime << " seconds" << std::endl;
        	std::cout << "ALE position: " << positionALE(0) << "," << positionALE(1) << "," << positionALE(2) << std::endl;
            lattice.copyDataToCPU();
            vtkWriterSim.write(timeStep);
            ++print;
        }


        if(converter.getPhysTime(timeStep) > 5.)
        	velocityNondim(0) = converter.getLatticeVelocity(velocity(0));
        positionALE(0) += velocityNondim(0);
//		lattice.copyDataToCPU();
//        vtkWriterSim.write(timeStep);
//        HANDLE_ERROR(cudaGetLastError());
  }

//  lattice.copyDataToCPU();
//  vtkWriterSim.write(converter.getLatticeTime(simTime));

  std::cout << "finished" << std::endl;

}

int main(int argc, char** argv)
{
    const double simTime = 10;
    MultipleSteps<63>(simTime);
	return 0;
}
