/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "fstream"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

template <typename T>
class gaussSphere : public AnalyticalF2D<T,int> {

protected:
  int x_, y_;
  T valueMax_;
  T valueBase_;
  int radius_;

// initial solution of the TGV
public:
  gaussSphere(int center[], int radius, T valueMax, T valueBase) : AnalyticalF2D<T,int>(1)
    ,x_(center[0])
    ,y_(center[1])
    ,valueMax_(valueMax)
    ,valueBase_(valueBase)
    ,radius_(radius)
  {}
  bool operator()(T output[], const int input[]) override
  {
    int radius = std::sqrt(std::pow(input[0]-x_,2)+std::pow(input[1]-y_,2));
    output[0] = valueBase_+(valueMax_ - valueBase_)*std::exp( -std::pow(radius,2)/(2*std::pow(800*0.1/50,2) ) );

    return true;
  };
};

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

void MultipleSteps(const double simTime, int iXRightBorderArg, bool isReference = false, bool isAle = false)
{
  int iXLeftBorder = 0;
  int iXRightBorder = iXRightBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = 63;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.1*1.0/std::sqrt(3)
          ,20.*(iXRightBorder+1.)/64.
          ,40.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T deltaX = converter.getConversionFactorLength();

  T omega = 1.96;//converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, 0.0);
  BlockLatticeALE2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);

  OnLatticeBoundaryCondition2D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition2D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  // prepareLattice
  if(isReference)
      boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
  else
      boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );

  boundaryCondition->addImpedanceBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

//  boundaryCondition->addPeriodicBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addPeriodicBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addPeriodicBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
//  boundaryCondition->addPeriodicBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder   , iYTopBorder   , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder+1     ,iYTopBorder-1, &instances::getBounceBack<T,Lattice>());

    boundaryCondition->addExternalImpedanceCornerPP(iXRightBorder ,iYTopBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPN(iXRightBorder,iYBottomBorder, omega);
  if(isReference)
  {
    boundaryCondition->addExternalImpedanceCornerNP(iXLeftBorder ,iYTopBorder   , omega);
    boundaryCondition->addExternalImpedanceCornerNN(iXLeftBorder ,iYBottomBorder, omega);
  }

//    boundaryCondition->addExternalPeriodicCornerPP(iXRightBorder ,iYTopBorder, omega);
//    boundaryCondition->addExternalPeriodicCornerPN(iXRightBorder,iYBottomBorder, omega);
//    boundaryCondition->addExternalPeriodicCornerNP(iXLeftBorder ,iYTopBorder   , omega);
//    boundaryCondition->addExternalPeriodicCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//    boundaryCondition->addExternalVelocityCornerPP(iXRightBorder ,iYTopBorder, omega);
//    boundaryCondition->addExternalVelocityCornerPN(iXRightBorder,iYBottomBorder, omega);
  if(!isReference)
  {
    boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder ,iYTopBorder   , omega);
    boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder ,iYBottomBorder, omega);
  }

//  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder ,iYTopBorder, &instances::getBounceBack<T,Lattice>());


  lattice.initDataArrays();

  // setBoundaryValues
  int center[Lattice<T>::d] = {0.25*(iXRightBorder+1),0.5*(iYTopBorder+1)};
  gaussSphere<T> sphere(center, 0.05*(iXRightBorder+1), 2.0, 1.);

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
        T vel[] = { 0.0, 0.0};
        T rho[1];
        int coordinates[Lattice<T>::d] = {iX,iY};
        sphere(rho,coordinates);
        lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  for (int iX = 0; iX <= 0; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
        T vel[] = { 0.0, 0.0};
        T rho[1];
        lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  for (int iY = 16; iY < 48; ++iY)
  {
    for (int iX = iXRightBorder-48; iX <= iXRightBorder-48; ++iX)
    {
        T force[Lattice<T>::d] = {converter.getLatticeForce(320.*deltaX*deltaX/1.225), 0.};
        for(int iDim =0; iDim < Lattice<T>::d; ++iDim)
        {
            lattice.defineForce(iX,iX,iY,iY,force);
        }
    }
  }

  std::string name;
  if(isReference)
      name = "rotorDiskInflow_X";
  else
      name = "rotorDiskNoInflow_X";

  name += std::to_string(iXRightBorder+1);

  if(isAle)
      name += "_ALE";

  BlockVTKwriter2D<T> vtkWriter( name );
  BlockLatticeDensity2D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T,Lattice> physVelocityFunctor(lattice,converter);
  BlockLatticeForce2D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/testImpedance2D/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.write(0);

  lattice.copyDataToGPU();

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy());
  timer.start();

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {
	  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);

	  if(iSteps%converter.getLatticeTime(0.1) == 0)
	  {
	      lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }
	  if(isAle)
	  {
	      T u[Lattice<T>::d] = {0.,0./converter.getConversionFactorVelocity()*min((T)iSteps/10000.,1.)};
	      lattice.moveMesh({0.,10./converter.getConversionFactorVelocity()*min((T)iSteps/10000.,1.)},0.,{0.,0.},iSteps,u);
	  }
//	  if(iSteps == 5000)
//	      break;
  }

  T velocityRotorDisk[Lattice<T>::d];

  std::fstream outFile("/scratch/BHorvat/testImpedance2D/vtkData/data/" + name + ".txt", std::ios::out);

  outFile << " u, v" << std::endl;

  for(unsigned int iX = iXRightBorder-48; iX <= iXRightBorder-48; ++iX)
      for(unsigned int iY = iYBottomBorder; iY <= iYTopBorder; ++iY)
      {
          lattice.get(iX,iY).computeU(velocityRotorDisk);
          for(unsigned int iDim = 0; iDim<Lattice<T>::d; ++iDim)
              outFile << velocityRotorDisk[iDim] << ", ";
          outFile << std::endl;
      }

  outFile.close();

  timer.stop();

}

int main()
{
    const double simTime = 12;
//    MultipleSteps(simTime,63,true,false);
    MultipleSteps(simTime,63,true,true);
//    MultipleSteps(simTime,63);
//    MultipleSteps(simTime,79);
//    MultipleSteps(simTime,95);
//    MultipleSteps(simTime,127);
//    MultipleSteps(simTime,127,true);
	return 0;
}
