/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define D3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice D3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

int main()
{
    const unsigned int direction = 0;
    const int orientation = -1;

    T* cellData[Lattice<T>::dataSize];
    for(unsigned int iPop; iPop<Lattice<T>::dataSize; ++iPop)
        cellData[iPop] = new T[1]{0};

    size_t index = 0;
    size_t indexCalcHelper[Lattice<T>::d] = {1};
    T** momentaDataEmpty;
    T collisionData[1] = {0.1};

    T u[Lattice<T>::d] = {1,1,1};
    BGKdynamics<T,Lattice,BulkMomenta<T,Lattice>> dynamics(0.7);
    dynamics.iniEquilibrium(cellData,0,1.1,u);
    u[direction] = 1.1;
    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);

    for(unsigned int counter = 0; counter < 1; ++counter)
    {
        ImpedanceBoundaryProcessor3DControl<T,Lattice,direction,orientation>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
        cellData[Lattice<T>::uIndex+1][index] +=0.05;
    }

    std::cout << "===================================" << std::endl;

    u[direction] = {1.0};
    dynamics.iniEquilibrium(cellData,0,1.1,u);
    u[direction] = 1.1;
    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);

    for(unsigned int counter = 0; counter < 1; ++counter)
    {
        ImpedanceBoundaryProcessor3D<T,Lattice,direction,orientation>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
        cellData[Lattice<T>::uIndex+1][index] +=0.05;
    }

//    u[0] = {1.0};
//    dynamics.iniEquilibrium(cellData,0,1.1,u);
//    u[1] = 1.1;
//    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);
//
//    ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
//
//    u[1] = {1.0};
//    dynamics.iniEquilibrium(cellData,0,1.1,u);
//    u[1] = 1.1;
//    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);
//
//    ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
//
//    u[1] = {1.0};
//    dynamics.iniEquilibrium(cellData,0,1.1,u);
//    u[2] = 1.1;
//    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);
//
//    ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
//
//    u[2] = {1.0};
//    dynamics.iniEquilibrium(cellData,0,1.1,u);
//    u[2] = 1.1;
//    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);
//
//    ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);

    return 0;
}
