/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define D3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice D3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

// template<class PostProcType>
// __global__ void postProcKernel(T** cellData)
// {
    // T** momentaDataEmpty;
    // T collisionData[1] = {0.1};
    // size_t index = 0;
    // size_t indexCalcHelper[Lattice<T>::d] = {1};

    // PostProcType::template process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
// }

int main()
{
    constexpr unsigned int direction = 0;
    constexpr int normal1 = -1;
    constexpr int normal2 = -1;

    T* cellData[Lattice<T>::dataSize];
    for(unsigned int iPop; iPop<Lattice<T>::dataSize; ++iPop)
        cellData[iPop] = new T[1]{0};

    T** cellDataGPU;

    // cudaError_t error = cudaMallocManaged(&cellDataGPU, (Lattice<T>::dataSize) * sizeof(T*));
    // HANDLE_ERROR(error);

    // for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i)
    // {
      // error = cudaMalloc(&cellDataGPU[i], sizeof(T) * (1) );
      // HANDLE_ERROR(error);
    // }


    size_t index = 0;
    size_t indexCalcHelper[Lattice<T>::d] = {1};
    T** momentaDataEmpty;
    T collisionData[1] = {0.1};

    T u[Lattice<T>::d] = {1,1,1};
    BGKdynamics<T,Lattice,BulkMomenta<T,Lattice>> dynamics(0.7);
    dynamics.iniEquilibrium(cellData,0,1.1,u);
    u[direction] = 1.1;
    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);

    for(unsigned int iPop = 0; iPop<Lattice<T>::q; ++iPop)
        std::cout << cellData[iPop][index] << " ";
    std::cout << std::endl;

    for(unsigned int counter = 0; counter < 1; ++counter)
    {
        // for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i)
        // {
          // error = cudaMemcpy(cellDataGPU[i], cellData[i], sizeof(T) * 1, cudaMemcpyHostToDevice);
          // HANDLE_ERROR(error);
        // }

        // postProcKernel<ImpedanceBoundaryEdgeProcessor3D<T,Lattice,direction,normal1,normal2>><<<1,1>>>(cellDataGPU);
		ImpedanceBoundaryEdgeProcessor3D<T,Lattice,direction,normal1,normal2>::template process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);

        // cudaDeviceSynchronize();

        // for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i)
        // {
          // error = cudaMemcpy(cellData[i], cellDataGPU[i], sizeof(T) * 1, cudaMemcpyDeviceToHost);
          // HANDLE_ERROR(error);
        // }

        for(unsigned int iPop = 0; iPop<Lattice<T>::q; ++iPop)
        {
            cellData[iPop][0] += 0.05;
        }

    }

    for(unsigned int iPop = 0; iPop<Lattice<T>::q; ++iPop)
        std::cout << cellData[iPop][index] << " ";
    std::cout << std::endl;

//    std::cout << "===================================" << std::endl;
//
//    u[direction] = {1.0};
//    dynamics.iniEquilibrium(cellData,0,1.1,u);
//    u[direction] = 1.1;
//    BulkMomenta<T,Lattice>::defineRhoU(cellData,0,momentaDataEmpty,0,1.1,u);
//
//    for(unsigned int counter = 0; counter < 1; ++counter)
//    {
//        ImpedanceBoundaryEdgeProcessor3D<T,Lattice,direction,normal1,normal2>::process<NoDynamics<T,Lattice>>(cellData,momentaDataEmpty,collisionData,index,indexCalcHelper);
//        cellData[Lattice<T>::uIndex+1][index] +=0.05;
//    }

    return 0;
}
