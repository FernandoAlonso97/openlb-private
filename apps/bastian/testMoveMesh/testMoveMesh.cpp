/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "fstream"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;


void MultipleSteps(const double simTime, int iXRightBorderArg)
{
  int iXLeftBorder = 0;
  int iXRightBorder = iXRightBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = 47;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iYTopBorder
          ,0.3*1.0/std::sqrt(3)
          ,20.
          ,40.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  T deltaX = converter.getConversionFactorLength();

  T omega = 1.5;//converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);

  T const vel[Lattice<T>::d] = {0.0, 0.0};

  ConstVelocityProviderF<T, Lattice> constProvider(vel);
  ExternalFieldALE<T, Lattice, ConstVelocityProviderF> externalField(constProvider);

  PointXd<T, Lattice> lattice_anchor {0.0, 0.0};

  BlockLatticeALE2D<T, Lattice, ExternalFieldALE<T, Lattice, ConstVelocityProviderF>> lattice(iXRightBorder + 1, iYTopBorder + 1, lattice_anchor, &bulkDynamics, externalField);

  OnLatticeBoundaryCondition2D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition2D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 ,omega );
  boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1   ,omega );
  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder   ,omega );
  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder      ,omega );

  boundaryCondition->addExternalImpedanceCornerPN(iXRightBorder,iYBottomBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPP(iXRightBorder,iYTopBorder   , omega);
  boundaryCondition->addExternalImpedanceCornerNP(iXLeftBorder,iYTopBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNN(iXLeftBorder,iYBottomBorder   , omega);


  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {

            T vel[Lattice<T>::d] = { 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

 for (int iX = static_cast<int>(0.25*(iXRightBorder+1)); iX < static_cast<int>(0.75*(iXRightBorder+1)); ++iX)
 {
     for (int iY = static_cast<int>(0.25*(iYTopBorder+1)); iY < static_cast<int>(0.25*(iYTopBorder+1))+1; ++iY)
     {
             T force[Lattice<T>::d] = {0., converter.getLatticeForce(320.*deltaX*deltaX/1.225)};
             lattice.defineForce(iX,iX,iY,iY,force);
     }
 }

  std::string name;
  name = "rotorLine_X";
  name += std::to_string(iXRightBorder+1);
  name += "_ALE";

  BlockVTKwriter2D<T> vtkWriter( name );
  BlockLatticeDensity2D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T,Lattice> physVelocityFunctor(lattice,converter);
  BlockLatticeForce2D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/testMoveMesh/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.write(0);

  lattice.copyDataToGPU();

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy());
  timer.start();


  T conversionVelocity = converter.getConversionFactorVelocity();
  PoseXd<T, Lattice> movement {{5.0/conversionVelocity, 0.0}, 0.0};
  PoseXd<T, Lattice> absolute_lattice_pose{{0, 0}, 0};

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {
	  lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);

	  if(iSteps%converter.getLatticeTime(0.05) == 0)
	  {
	      lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }

      lattice.moveMeshGPU(movement, absolute_lattice_pose);
	  absolute_lattice_pose += movement.position_;
  }

  timer.stop();

  std::cout << "I ended" << std::endl;
  cudaDeviceSynchronize();

  delete boundaryCondition;

}

int main(int argc, char** argv)
{
    const double simTime = 5;
    MultipleSteps(simTime,47);
	return 0;
}
