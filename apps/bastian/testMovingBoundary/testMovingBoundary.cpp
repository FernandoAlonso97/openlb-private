/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

//#include "core/cellBlockDataSimpleStream.h"
//#include "core/cellBlockDataSimpleStream.hh"
//#include "core/blockLatticeALE3D.h"
//#include "core/blockLatticeALE3D.hh"
//#include "core/externalFieldALE.h"
//#include "core/externalFieldALE.hh"
//#include "boundary/momentaOnBoundaries.h"
//#include "boundary/momentaOnBoundaries.hh"
//#include "boundary/momentaOnBoundaries3D.h"
//#include "boundary/momentaOnBoundaries3D.hh"
//#include "boundary/boundaryPostProcessors3D.h"
//#include "boundary/boundaryPostProcessors3D.hh"
//#include "core/affineTransform.h"
//#include "io/blockVtkWriter3D.h"
//#include "io/blockVtkWriter3D.hh"
//#include "functors/lattice/blockLatticeLocalF3D.h"
//#include "functors/lattice/blockLatticeLocalF3D.hh"
//#include "boundary/boundaryInstantiator3D.h"
//#include "boundary/boundaryCondition3D.h"
//#include "boundary/boundaryCondition3D.hh"
//#include "dynamics/latticeDescriptors.h"
//#include "dynamics/latticeDescriptors.hh"
//#include "dynamics/lbHelpers.h"
//#include "dynamics/smagorinskyBGKdynamics.h"
//#include "dynamics/smagorinskyBGKdynamics.hh"
//#include "dynamics/dynamicsDataHandlerNew.h"
//#include "core/util.h"
//#include "core/config.h"
//#include "utilities/timer.h"
//#include "utilities/timer.hh"
//#include "dynamics/dynamicsKernels.h"
//#include "functors/analytical/analyticalF.h"
//#include "functors/analytical/analyticalF.hh"
//#include "functors/analytical/analyticalBaseF.h"
//#include "functors/analytical/analyticalBaseF.hh"
//#include "functors/genericF.h"
//#include "functors/genericF.hh"
//#include <cstdio>
//#include <fstream>
//#include <iostream>
//#include <vector>

#include "olb3D.h"
#include "olb3D.hh"
#include "fstream"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;


void setBlockPosition(BlockLattice3D<T,Lattice>* lattice, Vec3<T> position)
{
    size_t nx = lattice->getNx();
    size_t ny = lattice->getNy();
    size_t nz = lattice->getNz();

    Vector<T,3> origin(1.1*nx,0.5*ny,0.4*nz);
    Vector<T,3> extend(2*nx-origin[0],0.2*ny,0.2*nz);
//    IndicatorCuboid3D<T> obstacle(extend,origin);

    IndicatorSphere3D<T> obstacle(origin,0.1*ny);

    bool* __restrict__ fluidMask = lattice->getFluidMask();
    bool isInside[1] = {false};


    for(unsigned int iX = 1; iX < lattice->getNx()-1; ++iX)
        for(unsigned int iY = 1; iY < lattice->getNy()-1; ++iY)
            for(unsigned int iZ = 1; iZ < lattice->getNz()-1; ++iZ)
            {
                T location[3] = {iX+position(0),iY+position(1),iZ+position(2)};
                obstacle(isInside,location);
                if(isInside[0])
                {
//                    if(iX == 30 and iY == 8 and iZ == 25)
//                    {
//                        std::cout << "I have a match" << iX << "," << iY << "," << iZ << std::endl;
//                        std::cout << position(0) << "," << position(1) << "," << position(2) << std::endl;
//                        std::cout << "Fluidmask: " << fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] << std::endl;
//                    }


                    fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] = false;

//                    if(iX == 30 and iY == 8 and iZ == 25)
//                    {
//                        std::cout << "Fluidmask: " << fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] << std::endl;
//                    }
                }
                else
                    fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] = true;
            }

}


void MultipleSteps(const double simTime, int iXRightBorderArg) {
    int iXLeftBorder = 0;
    int iXRightBorder = iXRightBorderArg;
    int iYBottomBorder = 0;
    int iYTopBorder = iXRightBorderArg;
    int iZFrontBorder = 0;
    int iZBackBorder = iXRightBorderArg;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
            iYTopBorder,
            0.3 * 1.0 / std::sqrt(3),
            20.,
            40.,
            0.0000146072,
            1.225,
            0);

    converter.print();

    T deltaX = converter.getConversionFactorLength();

    T omega = converter.getLatticeRelaxationFrequency();
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega,0.05);

    Vec3<T> externalVelocity(converter.getLatticeVelocity(0.0), 0.0, 0.0);

    ConstExternalField<T,Lattice> constExternalField(externalVelocity);

    Vec3<T> lattice_anchor {((T)iXRightBorder)/2.0, ((T)iXRightBorder)/2.0, ((T)iXRightBorder)/2.0};

    BlockLatticeALE3D<T, Lattice, ConstExternalField<T, Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1,
                                                                                                lattice_anchor, &bulkDynamics, constExternalField);

    OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
            createInterpBoundaryCondition3D<T, Lattice,
                    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    // prepareLattice
        boundaryCondition->addImpedanceBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, omega);
        boundaryCondition->addImpedanceBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, omega);
        boundaryCondition->addImpedanceBoundary1N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);
        boundaryCondition->addImpedanceBoundary1P(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);
        boundaryCondition->addImpedanceBoundary2N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, omega);
        boundaryCondition->addImpedanceBoundary2P(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, omega);

//        boundaryCondition->addImpedanceFixedRefBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, omega);
//        boundaryCondition->addImpedanceFixedRefBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, omega);
//        boundaryCondition->addImpedanceFixedRefBoundary1N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);
//        boundaryCondition->addImpedanceFixedRefBoundary1P(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);
//        boundaryCondition->addImpedanceFixedRefBoundary2N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, omega);
//        boundaryCondition->addImpedanceFixedRefBoundary2P(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, omega);

    boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
    boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
    boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
    boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

    boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
    boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
    boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
    boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

    boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

//    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

    boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder, iYBottomBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder, iYTopBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder, iYBottomBorder, iZBackBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder, iYTopBorder, iZBackBorder, omega);
    //
    boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder, iYBottomBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder, iYTopBorder, iZFrontBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder, iYBottomBorder, iZBackBorder, omega);
    boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder, iYTopBorder, iZBackBorder, omega);


    lattice.initDataArrays();

    // setBoundaryValues

    for (int iX = 0; iX <= iXRightBorder; ++iX) {
        for (int iY = 0; iY <= iYTopBorder; ++iY) {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
                T
                vel[] = {0.0, 0.0, 0.0};

                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }

    for (int iX = static_cast<int>(0.25 * (iXRightBorder + 1)); iX < static_cast<int>(0.75 * (iXRightBorder + 1)); ++iX)
    {
        for (int iY = static_cast<int>(0.25 * (iYTopBorder + 1)); iY < static_cast<int>(0.75 * (iYTopBorder + 1)); ++iY)
        {
            for (int iZ = static_cast<int>(0.25 * (iZBackBorder + 1)); iZ <= static_cast<int>(0.25 * (iZBackBorder + 1)); ++iZ)
            {
                T force[Lattice < T > ::d] = {0., 0., converter.getLatticeForce(320. * deltaX * deltaX / 1.225)};
                lattice.defineForce(iX, iX, iY, iY, iZ, iZ, force);
            }
        }
    }

    std::string name;
    name = "rotorDisk_X";
    name += std::to_string(iXRightBorder + 1);
    name += "_ALEGPU";

    BlockVTKwriter3D<T> vtkWriter(name);
    BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
    BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticeFluidMask3D<T,Lattice> fluidFunctor(lattice);


    singleton::directories().setOutputDir("/scratch/BHorvat/testMovingBoundary/");

    vtkWriter.addFunctor(densityFunctor);
    vtkWriter.addFunctor(velocityFunctor);
    vtkWriter.addFunctor(physVelocityFunctor);
    vtkWriter.addFunctor(forceFunctor);
    vtkWriter.addFunctor(fluidFunctor);
    vtkWriter.write(0);

    lattice.copyDataToGPU();

    util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();

    T conversionVelocity = converter.getConversionFactorVelocity();
    Vec3<T> position{0,0,0};
    Vec3<T> orientation{0, 0, 0};
    Vec3<T> translation{0.0/conversionVelocity,0,0};
    Vec3<T> rotation{0.,0.,0./180.*3.142*converter.getConversionFactorTime()};

    for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps) {

        setBlockPosition(&lattice,position);
        lattice.copyFluidMaskToGPU();
        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        if (iSteps % converter.getLatticeTime(0.5) == 0)
            timer.print(iSteps, 2);

        if (iSteps % converter.getLatticeTime(0.05) == 0 or false) {
            lattice.copyDataToCPU();
            vtkWriter.write(iSteps);
        }

        if(iSteps == converter.getLatticeTime(3.))
            translation(0) = 10./conversionVelocity;

        lattice.moveMeshGPU(translation, rotation, position, orientation);
        position += translation;
        orientation += rotation;
    }

    timer.stop();

    std::cout << "I ended" << std::endl;
#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
#endif

    delete boundaryCondition;

}

int main(int argc, char **argv) {
    const double simTime = 8.;
    MultipleSteps(simTime, 63);
    return 0;
}
