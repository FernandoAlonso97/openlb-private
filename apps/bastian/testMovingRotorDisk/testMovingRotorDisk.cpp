/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "fstream"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

T ramp(T currentTime, T startTime, T period)
{
    if(currentTime > startTime and currentTime <= startTime+period)
        return (currentTime-startTime)/period;
    else if (currentTime > startTime+period and currentTime <= startTime+2*period)
        return (startTime+2*period-currentTime)/period;
    else if (currentTime > startTime+2*period and currentTime <= startTime+3*period)
        return (startTime+2*period-currentTime)/period;
    else if (currentTime > startTime+3*period and currentTime <= startTime+4*period)
        return -(startTime+4*period-currentTime)/period;
    else
        return 0;

}

void readBinary(T** externalField, std::string fileName, UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const & converter)
{
    const uint8_t floatType = 1;
    const uint8_t doubleType = 2;

    uint8_t dimension = 3;

    std::fstream readFile(fileName, std::ios::in | std::ios::binary);
    if(!readFile.is_open())
        std::cout << "File not opened" << std::endl;

    uint16_t readSize = 0;

    readFile.read( (char*) &readSize,  sizeof(uint16_t));
    std::string title;
    title.resize(readSize);
    readFile.read( (char*) &title[0], readSize);

    std::cout << "Title: " << title << std::endl;

    int32_t numVarsRead = 0;
    readFile.read( (char*) &numVarsRead, sizeof(int32_t));
    std::vector<std::string> varNames(numVarsRead);

    for(int32_t iVar = 0; iVar < numVarsRead; ++iVar)
    {
        readFile.read((char*) &readSize,sizeof(uint16_t));
        varNames[iVar].resize(readSize);
        readFile.read( (char*) &(varNames[iVar][0]),readSize);
        std::cout << "Variable: " << varNames[iVar] << std::endl;
    }

    int64_t extent[dimension];

    for(uint8_t iDim = 0; iDim < 3; ++iDim)
    {
        readFile.read( (char*) &extent[iDim], sizeof(int64_t));
        std::cout << "Extent in direction " << static_cast<uint16_t>(iDim) << " :" << extent[iDim] << std::endl;
    }

    uint64_t dataSizeToRead = extent[0]*extent[1]*extent[2];

    uint16_t chunkSizeRead;
    readFile.read( (char*) &chunkSizeRead, sizeof(uint16_t));
    std::cout << "Chunk size: " << chunkSizeRead << std::endl;

    uint8_t valueType;

    readFile.read( (char*) &valueType, sizeof(uint8_t));

    std::cout << "Value type: " << static_cast<uint16_t>(valueType) << std::endl;

    double lengthFactor;
    readFile.read( (char*) &lengthFactor, sizeof(double));
    std::cout << "Length factor: " << lengthFactor << std::endl;

    for(uint16_t iDim = 0; iDim < dimension; ++iDim)
        externalField[iDim] = new double [dataSizeToRead];

    float dataFloat[numVarsRead][chunkSizeRead];
    double dataDouble[numVarsRead][chunkSizeRead];

    uint64_t chunkRead = 0;
    uint64_t numberChunksToRead = dataSizeToRead/chunkSizeRead + 1;

    for(uint64_t index = 0; index < dataSizeToRead; index+=chunkSizeRead)
    {
        if(index+chunkSizeRead > dataSizeToRead)
            chunkSizeRead = dataSizeToRead - index;

        if(chunkRead%100 == 0)
            std::cout << chunkRead << "/" << numberChunksToRead << " chunks read" << std::endl;

        for(int32_t iVar = 0; iVar < 2*dimension+1; ++iVar)
        {
            uint64_t indexStart = chunkRead*chunkSizeRead;

            if(valueType == floatType)
            {
                readFile.read( (char*) &dataFloat[iVar], chunkSizeRead*sizeof(float));
                if(iVar > dimension)
                    for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
                    {
                        uint64_t iX = (indexStart+iChunk)%extent[0];
                        uint64_t iY = ((indexStart+iChunk-iX)/extent[0])%extent[1];
                        uint64_t iZ = (indexStart+iChunk-iX-iY*extent[0])/(extent[0]*extent[1]);

                        uint64_t index = (iX*extent[1]+iY)*extent[2]+iZ;
                        externalField[iVar-dimension-1][index] = converter.getLatticeVelocity(dataFloat[iVar][iChunk]);

                    }
            }
            else if(valueType == doubleType)
            {
                readFile.read( (char*) &dataDouble[iVar], chunkSizeRead*sizeof(double));

                if(iVar > dimension)
                    for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
                    {
                        uint64_t iX = (indexStart+iChunk)%extent[0];
                        uint64_t iY = ((indexStart+iChunk-iX)/extent[0])%extent[1];
                        uint64_t iZ = (indexStart+iChunk-iX-iY*extent[0])/(extent[0]*extent[1]);

                        uint64_t index = (iX*extent[1]+iY)*extent[2]+iZ;
                        externalField[iVar-dimension-1][index] = converter.getLatticeVelocity(dataDouble[iVar][iChunk]);

                    }
            }

        }
        ++chunkRead;
    }
}


void MultipleSteps(const double simTime, int iXRightBorderArg, std::string fileName)
{
  int iXLeftBorder = 0;
  int iXRightBorder = iXRightBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = 63;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iYTopBorder
          ,0.1*1.0/std::sqrt(3)
          ,20.*(iYTopBorder+1.)/64.
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  T deltaX = converter.getConversionFactorLength();

  T omega = 1.96;//converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, 0.0);
  BlockLatticeALE2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, 0, 0, &bulkDynamics);

  OnLatticeBoundaryCondition2D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition2D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  // prepareLattice
//  boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addVelocityBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
  boundaryCondition->addVelocityBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );

  boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
  boundaryCondition->addImpedanceBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

//  boundaryCondition->addPeriodicBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addPeriodicBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );

//  boundaryCondition->addPressureBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
//  boundaryCondition->addPressureBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );
//  boundaryCondition->addPressureBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );
//  boundaryCondition->addPeriodicBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
//  boundaryCondition->addPeriodicBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder   , iYTopBorder   , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder+1     ,iYTopBorder-1, &instances::getBounceBack<T,Lattice>());

    boundaryCondition->addExternalImpedanceCornerPP(iXRightBorder ,iYTopBorder, omega);
//    boundaryCondition->addExternalImpedanceCornerPN(iXRightBorder,iYBottomBorder, omega);
    boundaryCondition->addExternalImpedanceCornerNP(iXLeftBorder ,iYTopBorder   , omega);
//    boundaryCondition->addExternalImpedanceCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//  lattice.defineDynamics(iXRightBorder , iXRightBorder, iYTopBorder, iYTopBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder  , iXLeftBorder , iYTopBorder, iYTopBorder, &instances::getBounceBack<T,Lattice>());

//    boundaryCondition->addExternalPeriodicCornerPP(iXRightBorder ,iYTopBorder, omega);
//    boundaryCondition->addExternalPeriodicCornerPN(iXRightBorder,iYBottomBorder, omega);
//    boundaryCondition->addExternalPeriodicCornerNP(iXLeftBorder ,iYTopBorder   , omega);
//    boundaryCondition->addExternalPeriodicCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//    boundaryCondition->addExternalVelocityCornerPP(iXRightBorder ,iYTopBorder, omega);
    boundaryCondition->addExternalVelocityCornerPN(iXRightBorder,iYBottomBorder, omega);
//    boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder ,iYTopBorder   , omega);
    boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder ,iYBottomBorder, omega);

//  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYBottomBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder ,iYTopBorder, &instances::getBounceBack<T,Lattice>());


  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
        T vel[Lattice<T>::d] = { 0., 0.};
        T rho[1];
        lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  for (int iY = 16; iY < 48; ++iY)
  {
    for (int iX = iXRightBorder-48; iX <= iXRightBorder-48; ++iX)
    {
        T force[Lattice<T>::d] = {converter.getLatticeForce(320.*deltaX*deltaX/1.225), 0.};
        for(int iDim =0; iDim < Lattice<T>::d; ++iDim)
        {
//            lattice.defineForce(iX,iX,iY,iY,force);
        }
    }
  }

  T* externalField[Lattice<T>::d];
  size_t iXRightBorderExternal = 2*iXRightBorder;
  externalField[0] = new T[iXRightBorderExternal*4000];
  externalField[1] = new T[iXRightBorderExternal*4000];

//  readBinary(externalField, fileName, converter);

  T vortexCenter[Lattice<T>::d] = {static_cast<double>(iXRightBorder)/2.,4000.-(static_cast<double>(iYTopBorder))};
  T gamma = 1.0;

  for(size_t iX = 0; iX < iXRightBorderExternal; ++iX)
      for(size_t iY = 0; iY < 4000; ++iY)
      {
          T gamma = 0.01;
          T time = 0.00001;
          T xRel =  (iX-vortexCenter[0])/(static_cast<double>(iYTopBorder)*0.5);
          T yRel =  (iY-vortexCenter[1])/(static_cast<double>(iYTopBorder)*0.5);
          T radius = sqrt(pow(xRel,2)+pow(yRel,2));
          T factor = gamma/(4*3.14*sqrt(436.422*time));
          T q = radius/(2*sqrt(436.422*time));
          T velPhi = factor*(1-exp(-pow(q,2)))/q;

          externalField[0][util::getCellIndex2D(iX,iY,4000)] = -yRel/radius * velPhi;
          externalField[1][util::getCellIndex2D(iX,iY,4000)] =  xRel/radius * velPhi;
      }

  lattice.setExternalField(externalField, iXRightBorderExternal, 4000);
  lattice.setPosition(2,4000);

  std::string name;
  name = "rotorDiskInflow_X";
  name += std::to_string(iXRightBorder+1);
  name += "_ALE_10_0__1_0";

  BlockVTKwriter2D<T> vtkWriter( name );
  BlockLatticeDensity2D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T,Lattice> physVelocityFunctor(lattice,converter);
  BlockLatticeForce2D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/testMovingRotorDisk/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.write(0);

//  lattice.copyDataToGPU();

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy());
  timer.start();

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {
	  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);

	  if(iSteps%converter.getLatticeTime(0.25) == 0)
	  {
//	      lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }
      T u[Lattice<T>::d] = {0.,20./converter.getConversionFactorVelocity()};
      T meshVelocity[Lattice<T>::d] = {0.0/converter.getConversionFactorVelocity(),-5.0/converter.getConversionFactorVelocity()};
      if(iSteps < converter.getLatticeTime(1.0))
          meshVelocity[1] = -0.01/converter.getConversionFactorVelocity();

      lattice.moveMesh(meshVelocity,0.0/100.,u);
  }

  timer.stop();

}

int main(int argc, char** argv)
{
    const double simTime = 15;
    std::string fileName = argv[1];
//    MultipleSteps(simTime,63);
    MultipleSteps(simTime,79, fileName);
//    MultipleSteps(simTime,95);
//    MultipleSteps(simTime,127);
	return 0;
}
