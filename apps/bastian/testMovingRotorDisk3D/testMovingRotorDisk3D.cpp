/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "fstream"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

T ramp(T currentTime, T startTime, T period)
{
    if(currentTime > startTime and currentTime <= startTime+period)
        return (currentTime-startTime)/period;
    else if (currentTime > startTime+period and currentTime <= startTime+2*period)
        return (startTime+2*period-currentTime)/period;
    else if (currentTime > startTime+2*period and currentTime <= startTime+3*period)
        return (startTime+2*period-currentTime)/period;
    else if (currentTime > startTime+3*period and currentTime <= startTime+4*period)
        return -(startTime+4*period-currentTime)/period;
    else
        return 0;

}

void readBinary(T** externalField, std::string fileName, UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const & converter, int64_t extent[Lattice<T>::d])
{
    const uint8_t floatType = 1;
    const uint8_t doubleType = 2;

    uint8_t dimension = 3;

    std::fstream readFile(fileName, std::ios::in | std::ios::binary);
    if(!readFile.is_open())
        std::cout << "File not opened" << std::endl;

    uint16_t readSize = 0;

    readFile.read( (char*) &readSize,  sizeof(uint16_t));
    std::string title;
    title.resize(readSize);
    readFile.read( (char*) &title[0], readSize);

    std::cout << "Title: " << title << std::endl;

    int32_t numVarsRead = 0;
    readFile.read( (char*) &numVarsRead, sizeof(int32_t));
    std::vector<std::string> varNames(numVarsRead);

    for(int32_t iVar = 0; iVar < numVarsRead; ++iVar)
    {
        readFile.read((char*) &readSize,sizeof(uint16_t));
        varNames[iVar].resize(readSize);
        readFile.read( (char*) &(varNames[iVar][0]),readSize);
        std::cout << "Variable: " << varNames[iVar] << std::endl;
    }

    for(uint8_t iDim = 0; iDim < 3; ++iDim)
    {
        readFile.read( (char*) &extent[iDim], sizeof(int64_t));
        std::cout << "Extent in direction " << static_cast<uint16_t>(iDim) << " :" << extent[iDim] << std::endl;
    }

    uint64_t dataSizeToRead = extent[0]*extent[1]*extent[2];

    uint16_t chunkSizeRead;
    readFile.read( (char*) &chunkSizeRead, sizeof(uint16_t));
    std::cout << "Chunk size: " << chunkSizeRead << std::endl;

    uint8_t valueType;

    readFile.read( (char*) &valueType, sizeof(uint8_t));

    std::cout << "Value type: " << static_cast<uint16_t>(valueType) << std::endl;

    double lengthFactor;
    readFile.read( (char*) &lengthFactor, sizeof(double));
    std::cout << "Length factor: " << lengthFactor << std::endl;

    for(uint16_t iDim = 0; iDim < dimension; ++iDim)
        externalField[iDim] = new double [dataSizeToRead];

    float dataFloat[numVarsRead][chunkSizeRead];
    double dataDouble[numVarsRead][chunkSizeRead];

    uint64_t chunkRead = 0;
    uint64_t numberChunksToRead = dataSizeToRead/chunkSizeRead + 1;

    for(uint64_t index = 0; index < dataSizeToRead; index+=chunkSizeRead)
    {
        if(index+chunkSizeRead > dataSizeToRead)
            chunkSizeRead = dataSizeToRead - index;

        if(chunkRead%100 == 0)
            std::cout << chunkRead << "/" << numberChunksToRead << " chunks read" << std::endl;

        for(int32_t iVar = 0; iVar < 2*dimension+1; ++iVar)
        {
            uint64_t indexStart = chunkRead*chunkSizeRead;

            if(valueType == floatType)
            {
                readFile.read( (char*) &dataFloat[iVar], chunkSizeRead*sizeof(float));
                if(iVar > dimension)
                    for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
                    {
                        uint64_t iX = (indexStart+iChunk)%extent[0];
                        uint64_t iY = ((indexStart+iChunk-iX)/extent[0])%extent[1];
                        uint64_t iZ = (indexStart+iChunk-iX-iY*extent[0])/(extent[0]*extent[1]);

                        uint64_t index = (iX*extent[1]+iY)*extent[2]+iZ;
                        externalField[iVar-dimension-1][index] = converter.getLatticeVelocity(dataFloat[iVar][iChunk]);

                    }
            }
            else if(valueType == doubleType)
            {
                readFile.read( (char*) &dataDouble[iVar], chunkSizeRead*sizeof(double));

                if(iVar > dimension)
                    for(uint16_t iChunk = 0; iChunk < chunkSizeRead; ++iChunk)
                    {
                        uint64_t iX = (indexStart+iChunk)%extent[0];
                        uint64_t iY = ((indexStart+iChunk-iX)/extent[0])%extent[1];
                        uint64_t iZ = (indexStart+iChunk-iX-iY*extent[0])/(extent[0]*extent[1]);

                        uint64_t index = (iX*extent[1]+iY)*extent[2]+iZ;
                        externalField[iVar-dimension-1][index] = converter.getLatticeVelocity(dataDouble[iVar][iChunk]);

                    }
            }

        }
        ++chunkRead;
    }
}

template<typename T>
int addFuselageCells(int resolution, BlockLatticeALE3D<T,Lattice,ExternalFieldALE<T,Lattice,ConstVelocityProviderF>> &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("/media/ga69kiq/work/BHorvat/GitRepositories/openlb/apps/bastian/testFlightmechanicsCommunication/Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;
    else
    {
        std::cout << "Fuselage file not found" << std::endl;
        std::exit(1);
    }

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
//            std::cout << position[0] << "," << position[1] << "," << position[2] << std::endl;
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 2;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

void MultipleSteps(const double simTime, int iZBackBorderArg, std::string fileName)
{
  int iXLeftBorder = 0;
  int iXRightBorder = 31;
  int iYBottomBorder = 0;
  int iYTopBorder = 31;
  int iZFrontBorder = 0;
  int iZBackBorder = iZBackBorderArg;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iYTopBorder
          ,0.2*1.0/std::sqrt(3)
          ,20.
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  T deltaX = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  const T velocity[3] = {0,0,0};
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, 0.1);
  ConstVelocityProviderF<T,Lattice> provider(velocity);
  ExternalFieldALE<T,Lattice,ConstVelocityProviderF> externalFieldALE(0,0,0,0,0,provider);
  BlockLatticeALE3D<T, Lattice, ExternalFieldALE<T,Lattice,ConstVelocityProviderF>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1, 0, 0, 0, &bulkDynamics,externalFieldALE);


  T rotorPositionZ = iZBackBorder+1;
  addFuselageCells(iZBackBorder+1,lattice,rotorPositionZ);

  OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition3D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

//  boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
//  boundaryCondition->addVelocityBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );

  boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
  boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

//  lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());


//  boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
//  boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
//  boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
//  boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//  boundaryCondition->addExternalVelocityEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
//  boundaryCondition->addExternalVelocityEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

//  boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
//  boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
//  boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
//  boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//  boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
//  boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

  lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

//  boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
//  boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
//  boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
//  boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
//
//  boundaryCondition->addExternalVelocityEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
//  boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
//  boundaryCondition->addExternalVelocityEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );

  lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
  lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

  boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

  boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

//  boundaryCondition->addExternalVelocityCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
//  boundaryCondition->addExternalVelocityCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
//  boundaryCondition->addExternalVelocityCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
//  boundaryCondition->addExternalVelocityCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);


//  boundaryCondition->addExternalVelocityCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
//  boundaryCondition->addExternalVelocityCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
//  boundaryCondition->addExternalVelocityCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
//  boundaryCondition->addExternalVelocityCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

//  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//  lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());


  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {

            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
    }
  }

 for (int iX = static_cast<int>(0.25*(iXRightBorder+1)); iX < static_cast<int>(0.75*(iXRightBorder+1)); ++iX)
 {
     for (int iY = static_cast<int>(0.25*(iYTopBorder+1)); iY < static_cast<int>(0.75*(iYTopBorder+1)); ++iY)
     {
         for (int iZ = static_cast<int>(rotorPositionZ); iZ <= static_cast<int>(rotorPositionZ); ++iZ)
         {
             T force[Lattice<T>::d] = {0., 0., converter.getLatticeForce(320.*deltaX*deltaX/1.225)};
             T radius = sqrt(pow(static_cast<double>(iY)-0.5*(iYTopBorder+1),2)+pow(static_cast<double>(iX)-0.5*(iXRightBorder+1),2));
             if(radius < 0.25*iYTopBorder)
                 lattice.defineForce(iX,iX,iY,iY,iZ,iZ,force);
         }
     }
 }

//  T* externalField[Lattice<T>::d];
//  int64_t extent[Lattice<T>::d] = {64,500,64};
//  for(unsigned int iDim = 0; iDim<Lattice<T>::d; ++iDim)
//  {
//      externalField[iDim] = new T[64*500*64]();
//      if(iDim == 1)
//          for(size_t index = 0; index<64*500*64; ++index)
//              externalField[iDim][index] = converter.getLatticeVelocity(1);
//  }

//  readBinary(externalField, fileName, converter, extent);
//
//  size_t iXRightBorderExternal = iXRightBorder+1;
//  size_t iYTopBorderExternal = 4000;
//  size_t iZBackBorderExternal = iZBackBorder+1;
//
//  for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
//      externalField[iDim] = new T[iXRightBorderExternal*iYTopBorderExternal*iZBackBorderExternal];
//
//
//  T vortexCenter[Lattice<T>::d] = {0,iYTopBorderExternal-(static_cast<double>(iYTopBorder+1)),static_cast<double>(iZBackBorder+1)*0.5};
//  T gamma = 1.0;
//
//  for(size_t iX = 0; iX < iXRightBorderExternal; ++iX)
//  {
//      for(size_t iY = 0; iY < iYTopBorderExternal; ++iY)
//      {
//          for(size_t iZ = 0; iZ < 2*iZBackBorderExternal; ++iZ)
//          {
//              T gamma = 0.005;
//              T time = 0.00001;
//              T zRel =  (iZ-vortexCenter[2])/(static_cast<double>(iYTopBorder+1));
//              T yRel =  (iY-vortexCenter[1])/(static_cast<double>(iYTopBorder+1));
//              T radius = sqrt(pow(zRel,2)+pow(yRel,2));
//              T factor = gamma/(4*3.14*sqrt(600.*time));
//              T q = radius/(2*sqrt(800.*time));
//              T velPhi = factor*(1-exp(-pow(q,2)))/q;
//
//              externalField[0][util::getCellIndex3D(iX,iY,iZ,iYTopBorderExternal,iZBackBorderExternal)] = 0.0;
//              externalField[1][util::getCellIndex3D(iX,iY,iZ,iYTopBorderExternal,iZBackBorderExternal)] =  zRel/radius * velPhi;
//              externalField[2][util::getCellIndex3D(iX,iY,iZ,iYTopBorderExternal,iZBackBorderExternal)] =  -yRel/radius * velPhi;
//          }
//      }
//  }

//  lattice.setExternalField(externalField, extent[0], extent[1], extent[2]);
//  lattice.setExternalField(externalField, iXRightBorderExternal, iYTopBorderExternal, iZBackBorderExternal);
//  lattice.setPosition(0,3999,0);

  std::string name;
  name = "movingRotor_X";
  name += std::to_string(iZBackBorderArg+1);
  name += "";

  BlockVTKwriter3D<T> vtkWriter( name );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/testMovingRotorDisk3D/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.write(0);

  lattice.copyDataToGPU();

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {
	  lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);

	  if(iSteps%converter.getLatticeTime(0.05) == 0)
	  {
	      lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }

      T conversionVelocity = converter.getConversionFactorVelocity();

      T angles[Lattice<T>::d] = {0.0,0.0,0.0};
      T meshVelocity[Lattice<T>::d] = {20.0/conversionVelocity,0.0/conversionVelocity, 0.0/conversionVelocity};
      T position[Lattice<T>::d] = {0,0,0};
      T attitudes[Lattice<T>::d] = {0,0,0};

//      if(iSteps < converter.getLatticeTime(3.0))
//          meshVelocity[0] = -0.01/converter.getConversionFactorVelocity();
      lattice.moveMeshGPU(meshVelocity,angles,position,attitudes);
  }

  timer.stop();

  delete boundaryCondition;

}

int main(int argc, char** argv)
{
    const double simTime = 15;
    std::string fileName = argv[1];
//    MultipleSteps(simTime,63);
    MultipleSteps(simTime,31, fileName);
//    MultipleSteps(simTime,95);
//    MultipleSteps(simTime,127);
	return 0;
}
