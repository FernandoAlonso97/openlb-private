/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "../../../src/contrib/communication/NetworkInterface.h"
#include "../../../src/contrib/communication/NetworkInterface.cpp"
#include "../../../src/contrib/communication/NetworkDataStructures.h"
#include "contrib/coupling/couplingCore.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    if(pos[2] == 6)
    {
        std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
        for (int iPop = Lattice<T>::forceIndex; iPop < Lattice<T>::forceIndex+Lattice<T>::d; ++iPop)
        {
          std::cout << cell[iPop].get() << ", ";
        }
        sumZ += cell[Lattice<T>::forceIndex+2];
        std::cout << std::endl;
    }
  }
};

template<typename T, class Blocklattice>
int addFuselageCells(int resolution, Blocklattice &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("../Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;
    else
        std::cout << "File not open" << std::endl;

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 0;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);


      boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
      boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

//      lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
    //  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
      boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

      boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

      boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
      boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
      boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

}

template<unsigned int RESOLUTION>
void MultipleSteps(const double simTime)
{
  using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
  using SendDataGensim = VelocityAtRotorsLinear;
  using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;

  int iXLeftBorder = 0;
  int iXRightBorder = RESOLUTION;
  int iYBottomBorder = 0;
  int iYTopBorder = RESOLUTION;
  int iZFrontBorder = 0;
  int iZBackBorder = RESOLUTION;
  T const rotorRadius = 4.92;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder
          ,0.3*1.0/std::sqrt(3)
          ,4.*rotorRadius
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();
  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";
  Vec3<T> externalVelocity(converter.getLatticeVelocity(0.0), 0.0, 0.0);
  ConstExternalField<T,Lattice> externalFieldALE(externalVelocity);
  BlockLatticeALE3D<T, Lattice, ConstExternalField<T,Lattice> > lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1,
          {0, 0, 0}, &bulkDynamics,externalFieldALE);
  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);


  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {static_cast<T>(iXRightBorder/2.),static_cast<T>(iYTopBorder/2.),static_cast<T>(iZBackBorder+1)};
  size_t numCells = addFuselageCells(iXRightBorder+1,lattice,rotorPosition[2]);

  Rotor<T> rotor(rotorRadius,rotorPosition,spacing);
  RotorCellData<T> rotorCellData(rotor,lattice, converter);
  CouplingType::CouplingDataType couplingData(rotorCellData);

  for(unsigned int iCell = 0; iCell< rotorCellData._numberRotorCells; ++iCell)
      if(rotorCellData._cellPositionRadialCPU[iCell] < 0.223)
      {
          size_t position[3];
          util::getCellIndices3D(rotorCellData._cellIndexCPU[iCell],lattice.getNy(),lattice.getNz(),position);
          lattice.defineDynamics(position[0],position[1],position[2],&instances::getBounceBack<T,Lattice>());
      }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::cout << "Rotor Position: " << rotor.getCenter()[0] << "," << rotor.getCenter()[1] << "," << rotor.getCenter()[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotor.getXLimits()[0] << " to " << rotor.getXLimits()[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotor.getYLimits()[0] << " to " << rotor.getYLimits()[1] << std::endl;

  std::cout << "Number of theoretical rotor cells: " << rotor.getNumberRotorCells() << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::cout << "Undim: " << converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea() << std::endl;

  lattice.updateAnchorPoint(Vec3<T>{rotorPosition[0],rotorPosition[1],rotorPosition[2]});

  T u[3] = {0,0,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string nameTrim;
  std::string directory = "/scratch/BHorvat/flightMechanicsCoupling/V7/";
  nameTrim = "trim_testFlightmechanicsCommunicationHarmonicLinear_X";
  nameTrim += std::to_string(iXRightBorder+1);

  BlockVTKwriter3D<T> vtkWriterTrim( nameTrim );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(velocityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(fluidMaskFunctor);

  vtkWriterTrim.write(0);

  ReceiveDataGensim dataReceiveGensim;
  SendDataGensim dataSendGensim;

  NetworkInterfaceTCP<ReceiveDataGensim,SendDataGensim> networkCommunicatorGensim(8888,OUTPUTIP,8888,NETWORKBUFFERSIZE,true);

  dataReceiveGensim.simulationStatus = 1;

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  std::ofstream fileWriter(directory + "thrustLBM.txt");

  if(fileWriter)
      std::cout << "File open" << std::endl;

  T const forceFactor =  converter.getLatticeForce( (spacing*spacing)/(1.225*rotorArea) );

  Vec3<T> position{0,0,0};
  Vec3<T> attitude{0,0,0};
  Vec3<T> translation{0,0,0};
  Vec3<T> rotation{0,0,0};

  size_t preStep = 1;
  while(dataReceiveGensim.simulationStatus != 0)
  {

//	  std::cout << "Waiting for receive data" << std::endl;
	  networkCommunicatorGensim.recieve_data(dataReceiveGensim);

      CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());

      std::cout << "Thrust: " << dataReceiveGensim.presMainMeanX << "," << dataReceiveGensim.presMainMeanY << "," << dataReceiveGensim.presMainMeanZ << std::endl;

      std::cout << "Simulation status is: " << dataReceiveGensim.simulationStatus << std::endl;

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          translation(iDim) = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
          rotation(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
          position(iDim)    = dataReceiveGensim.positions[iDim]/spacing;
          attitude(iDim)    = dataReceiveGensim.attitudes[iDim];

          std::cout << "Axis " << iDim << " : " << translation(iDim) << ","
                  << rotation(iDim) << "," << position(iDim) << ","
                  << attitude(iDim) << std::endl;
      }

      unsigned int trimTime = converter.getLatticeTime(15);

      if(preStep == 1)
	  {
          trimTime = converter.getLatticeTime(15);
	  }

      for(unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
      {
          lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
          lattice.moveMeshGPU(translation,rotation,position,attitude);
      }

      lattice.copyDataToCPU();
      vtkWriterTrim.write(preStep);

      std::cout << "Trimstep " << preStep << " finished" << std::endl;

      CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

      std::cout << "vi: " << dataSendGensim.velocityMainMean[0] << "," << dataSendGensim.velocityMainMean[1]
                                                          << "," << dataSendGensim.velocityMainMean[2] << std::endl;

      networkCommunicatorGensim.send_data(dataSendGensim);

      ++preStep;

	  if(dataReceiveGensim.simulationStatus == 0)
	  {
	      break;
//	      std::cout << "New loop" << std::endl;
//		  dataReceive.simulationStatus = 2;
	  }
  }

  std::cout << "Finished trim in " << preStep << " steps" << std::endl;

  std::string nameSim;
  nameSim = "sim_testFlightmechanicsCommunicationHarmonicLinear_X";
  nameSim += std::to_string(iXRightBorder+1);

  BlockVTKwriter3D<T> vtkWriterSim( nameSim );

  vtkWriterSim.addFunctor(densityFunctor);
  vtkWriterSim.addFunctor(velocityFunctor);
  vtkWriterSim.addFunctor(physVelocityFunctor);
  vtkWriterSim.addFunctor(forceFunctor);
  vtkWriterSim.addFunctor(fluidMaskFunctor);

  std::cout << "Starting time simulation" << std::endl;

  std::cout << "Waiting for receive data" << std::endl;

  for(unsigned int timeStep = 0; timeStep < converter.getLatticeTime(simTime); ++timeStep)
  {
        networkCommunicatorGensim.recieve_data(dataReceiveGensim);

        CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            translation(iDim) = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
            rotation(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
            position(iDim)    = dataReceiveGensim.positions[iDim]/spacing;
            attitude(iDim)    = dataReceiveGensim.attitudes[iDim];
        }

        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

        if(timeStep%converter.getLatticeTime(0.5) == 0)
            timer.print(timeStep);

        if(timeStep%converter.getLatticeTime(5.0) == 0)
        {
            lattice.copyDataToCPU();
            vtkWriterSim.write(timeStep);
        }

        lattice.moveMeshGPU(translation,rotation,position,attitude);


        CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

        networkCommunicatorGensim.send_data(dataSendGensim);

  }
}

int main()
{
    const double simTime = 20;
    MultipleSteps<31>(simTime);
	return 0;
}
