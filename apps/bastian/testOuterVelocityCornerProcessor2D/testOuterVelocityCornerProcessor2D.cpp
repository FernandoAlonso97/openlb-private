/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2006 - 2012 Mathias J. Krause, Jonas Fietz,
 *  Jonas Latt, Jonas Kratzke
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#define D2Q9LATTICE 1
typedef double T;

#include <chrono>

#include "olb2D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb2D.hh"   // include full template code
#endif
#include <cmath>
#include <iostream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

#define DESCRIPTOR D2Q9Descriptor

const int xNormal = -1;
const int yNormal = 1;
using MomentaType = FixedVelocityBM<T,DESCRIPTOR,0>;
using PostProcessorType = OuterVelocityCornerProcessor2D<T,DESCRIPTOR,xNormal,yNormal>;
using FluidDynamics = ConstRhoBGKdynamics<T, DESCRIPTOR, MomentaType, PostProcessorType>;

OPENLB_HOST_DEVICE
void printField(T** fieldData, int N)
{
	for(int iPop=0; iPop<9; ++iPop)
	{
		for(int iY=0; iY<N; ++iY)
			for(int iX=0; iX<N; ++iX)
				printf("%.1lf ",fieldData[iPop][iX+N*iY]);
		printf("\n");
	}

	printf("===============\n");
}

#ifdef ENABLE_CUDA

__device__ T d_rho;
__device__ T d_u[2];
__device__ T d_f;

__global__ void printKernel(T** fieldData, int N)
{
	printField(fieldData,N);
}

__global__ void collisionKernel(T** fieldData, T omega, size_t index)
{
	T collisionData[1] = {omega};
	T** momentaData;
	FluidDynamics::collision(fieldData, index, momentaData, 0, collisionData);
}

__global__ void computeRhoUKernel(T** fieldData, size_t index)
{
	T** momentaData;
	BasicDynamics<T,DESCRIPTOR,MomentaType>::computeRhoU(fieldData,index, momentaData, 0, d_rho ,d_u);
}

__global__ void postProcCellKernel(T** fieldData, size_t index, T omega, size_t domainSize)
{
	T collisionData[1] = {omega};
	T** momentaData;
	PostProcessorType::process<FluidDynamics>(fieldData,momentaData,collisionData,index,domainSize);
}

__global__ void getKernel(T** fieldData, size_t index, int iPop)
{
	d_f = fieldData[iPop][index];
}

#endif

int main( int argc, char* argv[] ) {

  olbInit( &argc, &argv );

  int xOffset = 2;
  int yOffset = 2;

  int domainSize = 5;

  CellBlockData2D<T, DESCRIPTOR> cellBlock( domainSize, domainSize );
  T** fieldData = cellBlock.getCellData();

  MomentaType momenta;
  FluidDynamics* bulkDynamics = new FluidDynamics(0.6, momenta);
  T collisionData[1] = {0.6};
  T** momentaData = nullptr;

#ifdef ENABLE_CUDA
	GPUHandler<T,DESCRIPTOR>::get();
	GPUHandler<T,DESCRIPTOR>::get().allocateFluidField(domainSize,domainSize);
#endif

  BlockLattice2D<T,DESCRIPTOR> lattice(domainSize,domainSize,bulkDynamics);
  lattice.initDataArrays();

  for(int i=0; i<domainSize; ++i)
  {
	  for(int j=0; j<domainSize; ++j)
	  {
		  T rho = i+1;
		  T u[DESCRIPTOR<T>::d] = {i+j,i+j+domainSize};

		  bulkDynamics->defineRhoU(fieldData, util::getCellIndex2D(i,j,domainSize), momentaData, 0, rho, u);
		  lattice.defineRhoU(i,i,j,j,rho,u);
	  }
  }


#ifdef ENABLE_CUDA
	bool fluidMaskDummy[domainSize*domainSize];
	GPUHandler<T,DESCRIPTOR>::get().transferCellDataToGPU(fieldData,fluidMaskDummy,domainSize*domainSize);
	cudaDeviceSynchronize();
	T** fieldDataGPU = GPUHandler<T,DESCRIPTOR>::get().getFluidData();
	cudaDeviceSynchronize();
#endif

  for(int i=0; i<domainSize; ++i)
	  for(int j=0; j<domainSize; ++j)
	  {
		  FluidDynamics::collision(fieldData, util::getCellIndex2D(i,j,domainSize), momentaData, 0, collisionData);
#ifdef ENABLE_CUDA
		  collisionKernel<<<1,1>>>(fieldDataGPU,collisionData[0],util::getCellIndex2D(i,j,domainSize));
		  cudaDeviceSynchronize();
#endif
	  }

//  printField(fieldData,domainSize);
#ifdef ENABLE_CUDA
//  printKernel<<<1,1>>>(fieldDataGPU,domainSize);
  cudaDeviceSynchronize();
#endif
  lattice.fieldCollision<FluidDynamics>();


for(int k=0; k<DESCRIPTOR<T>::q; ++k)
{
  for(int i=0; i<domainSize; ++i)
  {
	  for(int j=0; j<domainSize; ++j)
	  {
		  T rho;
		  T u[DESCRIPTOR<T>::d];

		  T rhoCPU;
		  T uCPU[DESCRIPTOR<T>::d];

		  T rhoGPU;
		  T uGPU[DESCRIPTOR<T>::d];

  		  bulkDynamics->computeRhoU(fieldData, util::getCellIndex2D(i,j,domainSize), momentaData, 0, rho,u);
  		  lattice.get(i,j).computeRhoU(rhoCPU,uCPU);
#ifdef ENABLE_CUDA
  		  computeRhoUKernel<<<1,1>>>(fieldDataGPU, util::getCellIndex2D(i,j,domainSize));
  		  cudaMemcpyFromSymbol(&rhoGPU,d_rho,sizeof(T), 0, cudaMemcpyDeviceToHost);
  		  cudaMemcpyFromSymbol(&uGPU,d_u,2*sizeof(T), 0, cudaMemcpyDeviceToHost);
#endif

  		  if( rho!= rhoCPU || u[0] != uCPU[0] || u[1] != uCPU[1])
  		  {
  			  std::cout << "CPU::CPU not identical (" << i << "," << j <<") diffRho: " << rho-rhoCPU << ", diffU: " << u[0]-uCPU[0] << "," << u[1]-uCPU[1] << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rhoCPU << " (" << uCPU[0] << "," << uCPU[1] << ")" << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rho << " (" << u[0] << "," << u[1] << ")" << std::endl;
			  std::cout << std::endl;
  		  }


  		  if( std::abs(rhoCPU-rhoGPU) > 1e-9 || std::abs(uCPU[0]-uGPU[0]) > 1e-9 || std::abs(uCPU[1]-uGPU[1]) > 1e-9 )
  		  {
  			  std::cout << "GPU::CPU not identical (" << i << "," << j <<") diffRho: " << rhoCPU-rhoGPU << ", diffU: " << uCPU[0]-uGPU[0] << "," << uCPU[1]-uGPU[1] << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rhoCPU << " (" << uCPU[0] << "," << uCPU[1] << ")" << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rhoGPU << " (" << uGPU[0] << "," << uGPU[1] << ")" << std::endl;
			  std::cout << std::endl;
  		  }


		  T fref = lattice.get(i,j)[k].get();
		  T fCPU = fieldData[k][util::getCellIndex2D(i,j,domainSize)];
		  T fGPU;
#ifdef ENABLE_CUDA
		  getKernel<<<1,1>>>(fieldDataGPU,util::getCellIndex2D(i,j,domainSize),k);
		  cudaDeviceSynchronize();
		  cudaMemcpyFromSymbol(&fGPU,d_f,sizeof(T), 0, cudaMemcpyDeviceToHost);
#endif
  		  if( fref!= fCPU)
  		  {
  			  std::cout << "CPU::CPU not identical (" << i << "," << j <<")[" << k << "]diff(f): " << fref-fCPU << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fref << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fCPU << std::endl;
			  std::cout << std::endl;
  		  }


  		  if( std::abs(fref-fGPU) > 1e-9 )
  		  {
  			  std::cout << "CPU::GPU not identical (" << i << "," << j <<")[" << k << "]diff(f): " << fref-fGPU << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fref << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fGPU << std::endl;
			  std::cout << "====================" << std::endl;
  		  }

  	  }
    }
}

  PostProcessorType postProcessor(xOffset,yOffset);

  postProcessor.process(lattice);

  int i = xOffset;
  for(int j = yOffset; j<domainSize-2; ++j)
  {
	  PostProcessorType::process<FluidDynamics>(fieldData,momentaData,collisionData,util::getCellIndex2D(i,j,domainSize),domainSize);
#ifdef ENABLE_CUDA
	  postProcCellKernel<<<1,1>>>(fieldDataGPU,util::getCellIndex2D(i,j,domainSize),collisionData[0],domainSize);
	  cudaDeviceSynchronize();
#endif
  }

  for(int i=0; i<domainSize; ++i)
	  for(int j=0; j<domainSize; ++j)
	  {
		  FluidDynamics::collision(fieldData, util::getCellIndex2D(i,j,domainSize), momentaData, 0, collisionData);
#ifdef ENABLE_CUDA
		  collisionKernel<<<1,1>>>(fieldDataGPU,collisionData[0],util::getCellIndex2D(i,j,domainSize));
#endif
	  }

  lattice.fieldCollision<FluidDynamics>();


for(int k=0; k<DESCRIPTOR<T>::q; ++k)
{
  for(int i=0; i<domainSize; ++i)
  {
	  for(int j=0; j<domainSize; ++j)
	  {
		  T rho;
		  T u[DESCRIPTOR<T>::d];

		  T rhoCPU;
		  T uCPU[DESCRIPTOR<T>::d];

		  T rhoGPU;
		  T uGPU[DESCRIPTOR<T>::d];

  		  bulkDynamics->computeRhoU(fieldData, util::getCellIndex2D(i,j,domainSize), momentaData, 0, rho,u);
  		  lattice.get(i,j).computeRhoU(rhoCPU,uCPU);
#ifdef ENABLE_CUDA
  		  computeRhoUKernel<<<1,1>>>(fieldDataGPU, util::getCellIndex2D(i,j,domainSize));
  		cudaMemcpyFromSymbol(&rhoGPU,d_rho,sizeof(T), 0, cudaMemcpyDeviceToHost);
  		cudaMemcpyFromSymbol(&uGPU,d_u,2*sizeof(T), 0, cudaMemcpyDeviceToHost);
#endif

  		  if( rho!= rhoCPU || u[0] != uCPU[0] || u[1] != uCPU[1])
  		  {
  			  std::cout << "CPU::CPU not identical (" << i << "," << j <<") diffRho: " << rho-rhoCPU << ", diffU: " << u[0]-uCPU[0] << "," << u[1]-uCPU[1] << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rhoCPU << " (" << uCPU[0] << "," << uCPU[1] << ")" << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rho << " (" << u[0] << "," << u[1] << ")" << std::endl;
			  std::cout << std::endl;
  		  }


  		  if( std::abs(rhoCPU-rhoGPU) > 1e-9 || std::abs(uCPU[0]-uGPU[0]) > 1e-9 || std::abs(uCPU[1]-uGPU[1]) > 1e-9 )
  		  {
  			  std::cout << "GPU::CPU not identical (" << i << "," << j <<") diffRho: " << rhoCPU-rhoGPU << ", diffU: " << uCPU[0]-uGPU[0] << "," << uCPU[1]-uGPU[1] << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rhoCPU << " (" << uCPU[0] << "," << uCPU[1] << ")" << std::endl;
			  std::cout << "(" << i << "," << j << "): " << rhoGPU << " (" << uGPU[0] << "," << uGPU[1] << ")" << std::endl;
			  std::cout << std::endl;
  		  }


		  T fref = lattice.get(i,j)[k].get();
		  T fCPU = fieldData[k][util::getCellIndex2D(i,j,domainSize)];
		  T fGPU;
#ifdef ENABLE_CUDA
		  getKernel<<<1,1>>>(fieldDataGPU,util::getCellIndex2D(i,j,domainSize),k);
		  cudaDeviceSynchronize();
		  cudaMemcpyFromSymbol(&fGPU,d_f,sizeof(T), 0, cudaMemcpyDeviceToHost);
#endif
  		  if( fref!= fCPU)
  		  {
  			  std::cout << "CPU::CPU not identical (" << i << "," << j <<")[" << k << "]diff(f): " << fref-fCPU << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fref << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fCPU << std::endl;
			  std::cout << std::endl;
  		  }


  		  if( std::abs(fref-fGPU) > 1e-9 )
  		  {
  			  std::cout << "CPU::GPU not identical (" << i << "," << j <<")[" << k << "]diff(f): " << fref-fGPU << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fref << std::endl;
			  std::cout << "(" << i << "," << j << ")[" << k << "]: " << fGPU << std::endl;
			  std::cout << "====================" << std::endl;
  		  }

  	  }
    }
}

  return 0;

}
