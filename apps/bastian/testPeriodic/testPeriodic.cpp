/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define D3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice D3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

int main()
{
	size_t nx = 12;
	size_t ny = 9;
	size_t nz = 6;

    T** cellData = new T*[Lattice<T>::dataSize];
	for(unsigned int iData = 0; iData < Lattice<T>::dataSize; ++iData)
		cellData[iData] = new T[nx*ny*nz];

	T** momentaData;
	T* collisionData;
	size_t indexCalcHelper[Lattice<T>::d] = {nx,ny,nz};

	size_t index = util::getCellIndex3D(0,2,2,ny,nz);
	std::cout << "PostProc 0,-1 : " << index  << " Opposite: " << util::getCellIndex3D(nx-1,2,2,ny,nz) << std::endl;
	PeriodicBoundaryProcessor3D<T,Lattice,0,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

	index = util::getCellIndex3D(nx-1,3,3,ny,nz);
    std::cout << "PostProc 0,1 : " << index << " Opposite: " << util::getCellIndex3D(0,3,3,ny,nz) << std::endl;
	PeriodicBoundaryProcessor3D<T,Lattice,0,1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

	index = util::getCellIndex3D(3,0,3,ny,nz);
    std::cout << "PostProc 1,-1 : " << index << " Opposite: " << util::getCellIndex3D(3,ny-1,3,ny,nz) << std::endl;
	PeriodicBoundaryProcessor3D<T,Lattice,1,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

	index = util::getCellIndex3D(2,ny-1,3,ny,nz);
    std::cout << "PostProc 1,1 : " << index << " Opposite: " << util::getCellIndex3D(2,0,3,ny,nz) << std::endl;
	PeriodicBoundaryProcessor3D<T,Lattice,1,1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

	index = util::getCellIndex3D(2,3,0,ny,nz);
    std::cout << "PostProc 2,-1 : " << index << " Opposite: " << util::getCellIndex3D(2,3,nz-1,ny,nz) << std::endl;
	PeriodicBoundaryProcessor3D<T,Lattice,2,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

	index = util::getCellIndex3D(3,2,nz-1,ny,nz);
    std::cout << "PostProc 2,1 : " << index << " Opposite: " << util::getCellIndex3D(3,2,0,ny,nz) << std::endl;
	PeriodicBoundaryProcessor3D<T,Lattice,2,1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

	std::cout << std::endl;

    index = util::getCellIndex3D(2,0,0,ny,nz);
    std::cout << "PostProc 0,-1,-1 : " << index << " NormalOpposite1: " << util::getCellIndex3D(2,ny-1,0,ny,nz)
    << " NormalOpposite2: " << util::getCellIndex3D(2,0,nz-1,ny,nz) << " DiagonalOpposite: " << util::getCellIndex3D(2,ny-1,nz-1,ny,nz) << std::endl;
    PeriodicBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

    index = util::getCellIndex3D(nx-1,1,0,ny,nz);
    std::cout << "PostProc 1,-1,1 : " << index << " NormalOpposite1: " << util::getCellIndex3D(nx-1,1,nz-1,ny,nz)
    << " NormalOpposite2: " << util::getCellIndex3D(0,1,0,ny,nz) << " DiagonalOpposite: " << util::getCellIndex3D(0,1,nz-1,ny,nz) << std::endl;
    PeriodicBoundaryEdgeProcessor3D<T,Lattice,1,-1,1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

    std::cout << std::endl;

    index = util::getCellIndex3D(nx-1,0,0,ny,nz);
    std::cout << "PostProc 1,-1,-1 : " << index << std::endl;
    PeriodicBoundaryCornerProcessor3D<T,Lattice,1,-1,-1>::process<NoDynamics<T,Lattice>>(cellData,momentaData,collisionData,index,indexCalcHelper);

    std::cout << std::endl;
    std::cout << "       "<< util::getCellIndex3D(0,ny-1,nz-1,ny,nz) << " + + + + + + + + + + + " << util::getCellIndex3D(nx-1,ny-1,nz-1,ny,nz) << "   " << std::endl;
    std::cout << "         +\\                      +\\    " << std::endl;
    std::cout << "         + \\                     + \\   " << std::endl;
    std::cout << "         +  \\                    +  \\  " << std::endl;
    std::cout << "         +   \\                   +   \\ " << std::endl;
    std::cout << "         +    " << util::getCellIndex3D(0,0,nz-1,ny,nz) << " + + + + + + + + +++ + " << util::getCellIndex3D(nx-1,0,nz-1,ny,nz) << std::endl;
    std::cout << "         +    +                  +    +" << std::endl;
    std::cout << "         +    +                  +    +" << std::endl;
    std::cout << "         +    +                  +    +" << std::endl;
    std::cout << "         +    +                  +    +" << std::endl;
    std::cout << "         +    +                  +    +" << std::endl;
    std::cout << "         +    +                  +    +" << std::endl;
    std::cout << "        " << util::getCellIndex3D(0,ny-1,0,ny,nz) << " + +++ + + + + + + + + " << util::getCellIndex3D(nx-1,ny-1,0,ny,nz) << "  +" << std::endl;
    std::cout << "          \\   +                   \\   +" << std::endl;
    std::cout << "z          \\  +                    \\  +" << std::endl;
    std::cout << "^ y         \\ +                     \\ +" << std::endl;
    std::cout << "I/           \\+                      \\+" << std::endl;
    std::cout << " ->x        " << util::getCellIndex3D(0,0,0,ny,nz) << " + + + + + + + + + + + " << util::getCellIndex3D(nx-1,0,0,ny,nz) << std::endl;

	return 0;
}
