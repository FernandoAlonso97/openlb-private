/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

void MultipleSteps()
{
  int iXLeftBorder = 0;
  int iXRightBorder = 2;
  int iYBottomBorder = 0;
  int iYTopBorder = 2;
  int iZFrontBorder = 0;
  int iZBackBorder = 2;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorder+1
          ,0.3*1.0/std::sqrt(3)
          ,1.9
          ,80
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  const T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,
      instances::getBulkMomenta<T, Lattice>(), 0.1);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1, &bulkDynamics);

  OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition3D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addImpedanceBoundaryIncompressible0N(iXLeftBorder   ,iXLeftBorder,  iYBottomBorder+1 ,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundaryIncompressible0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundaryIncompressible1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundaryIncompressible1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundaryIncompressible2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
  boundaryCondition->addImpedanceBoundaryIncompressible2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

  boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
  boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

  boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder,iXLeftBorder,iYBottomBorder+1,iYTopBorder-1,iZBackBorder,iZBackBorder, omega );
  boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder+1,iYTopBorder-1,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder,iXRightBorder,iYBottomBorder+1,iYTopBorder-1,iZBackBorder ,iZBackBorder , omega );
  boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder ,iXRightBorder ,iYBottomBorder+1,iYTopBorder-1,iZFrontBorder ,iZFrontBorder , omega );

  boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

  boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

  lattice.initDataArrays();

  // setBoundaryValues
  for (int iX = 0; iX <= iXRightBorder; ++iX) {
    for (int iY = 0; iY <= iYTopBorder; ++iY) {
      for (int iZ = 0; iZ<=iZBackBorder; ++iZ){
        T vel[] = { 0.0, 0.0, 0.0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
      }
    }
  }

//  T vel[] = { 0.0, 0.0, 0.1};
//  T force[3] = {0.0, 0.0, converter.getLatticeDiskLoading(10)};
//  for (int iX = 2; iX <= 2; ++iX)
//  {
//    for (int iY = 2; iY <= 2; ++iY)
//    {
//        for(int iZ = 2; iZ<=2; ++iZ)
//        {
//            lattice.defineExternalField(iX,iX,iY,iY,iZ,iZ,force,Lattice<T>::forceIndex,Lattice<T>::d);
//        }
//    }
//  }

  BlockVTKwriter3D<T> vtkWriter( "periodic3DMini" );
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);

  TempFunctional f;

  vtkWriter.addFunctor( velocityFunctor );
  vtkWriter.addFunctor( densityFunctor );
  vtkWriter.addFunctor( forceFunctor );
  vtkWriter.write(0);

//  lattice.forAll(f);

  for(int i=1; i<=converter.getLatticeTime(10000); ++i)
  {
	  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
	  if(false || i%converter.getLatticeTime(200.0) == 0)
	  {
		  vtkWriter.write(i);
	  }
  }

}

int main()
{
	MultipleSteps();
	return 0;
}
