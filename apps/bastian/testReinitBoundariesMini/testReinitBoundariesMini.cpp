/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime, int iXRightBorderArg, bool isReference = false)
{
  int iXLeftBorder = 0;
  int iXRightBorder = 3;
  int iYBottomBorder = 0;
  int iYTopBorder = 3;

  T omega = 1.96;
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, 2.0);
  BlockLattice2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);

  OnLatticeBoundaryCondition2D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition2D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );

  boundaryCondition->addImpedanceBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );
  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

  boundaryCondition->addExternalImpedanceCornerPP(iXRightBorder ,iYTopBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPN(iXRightBorder,iYBottomBorder, omega);
  boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder ,iYTopBorder   , omega);
  boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder ,iYBottomBorder, omega);

  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
        T vel[] = { 0.0, 0.0};
        T rho[1];
        lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  for(unsigned int iSteps=0; iSteps<=2; ++iSteps)
  {

	  lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps == 1)
	  {
	      std::cout << "Reset boundaries" << std::endl;
	      lattice.resetBoundaries();

	      boundaryCondition->addVelocityBoundary0P(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , omega );

	      boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , omega );
	      boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, omega );
	      boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , omega );

	      boundaryCondition->addExternalImpedanceCornerPP(iXRightBorder ,iYTopBorder, omega);
	      boundaryCondition->addExternalImpedanceCornerPN(iXRightBorder,iYBottomBorder, omega);
	      boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder ,iYTopBorder   , omega);
	      boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder ,iYBottomBorder, omega);

	      lattice.initDataArrays();

	      for (int iX = 0; iX <= iXRightBorder; ++iX)
	      {
	        for (int iY = 0; iY <= iYTopBorder; ++iY)
	        {
	            T vel[] = { 0.0, 0.0};
	            T rho[1];
	            lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
	            lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
	        }
	      }
	  }


  }


}

int main()
{
    MultipleSteps(0,0);
	return 0;
}
