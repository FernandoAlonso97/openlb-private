/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;


int main(int argc, char** argv)
{
    T point[3] = {0,1,2};
    T rotationCenter[3] = {0,2,2};
    T rotationAngles[3] = {45.0,0.,0.};
    T translation[3] = {0,-3,2};

    std::cout << "P(" << point[0] << "," << point[1] << "," << point[2] << ")" << std::endl;

    util::transformPoint3D<T,descriptors::D3Q19Descriptor>(point,translation,rotationCenter,rotationAngles);

    std::cout << "P(" << point[0] << "," << point[1] << "," << point[2] << ")" << std::endl;

//    for(unsigned int iLoop = 0; iLoop < 1000; ++iLoop)
    {
        util::rotateToBodySystem3D<T,descriptors::D3Q19Descriptor>(point,rotationAngles);

        std::cout << "P(" << point[0] << "," << point[1] << "," << point[2] << ")" << std::endl;

        util::rotateToInertiaSystem3D<T,descriptors::D3Q19Descriptor>(point,rotationAngles);

        std::cout << "P(" << point[0] << "," << point[1] << "," << point[2] << ")" << std::endl;
    }

	return 0;
}
