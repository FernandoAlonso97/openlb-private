#define D2Q9LATTICE 1
typedef double T;


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif
#include <iostream>

using namespace olb;
using namespace olb::descriptors;

using T = double;
#define DESCRIPTOR D3Q19Descriptor

template<typename Data>
OPENLB_HOST_DEVICE
void mySwap(Data& a, Data& b)
{
#if defined (__CUDA_ARCH__)
	Data tmp = a;
	a = b;
	b = tmp;
#else
	std::swap(a,b);
#endif
}

#ifdef ENABLE_CUDA

    __device__ bool d_result;

	__global__ void swapKernel(T** cellData, size_t index, int iPop)
	{
		mySwap(cellData[iPop][index], cellData[DESCRIPTOR<T>::opposite(iPop)][index]);
	}

	__global__ void gpuCheck(T** cellData,int iPop,size_t index,T value)
	{
		if(cellData[iPop][index]==value)
			d_result = true;
		else
			d_result = false;
	}

	__global__ void printKernel(T** cellData, int N)
	{
		for(int iPop=0; iPop<9; ++iPop)
		{
			for(int iY=0; iY<N; ++iY)
				for(int iX=0; iX<N; ++iX)
					printf("%.0lf ",cellData[iPop][iX+N*iY]);
			printf("\n");
		}

		printf("===============\n");
	}
#endif

int main()
{
	int N = 3;
	int offsetData[] = {0, 9, 3, 1, 12, 6, 10, 8, 4, 2, -9, -3, -1, -12, -6, -10, -8, -4, -2, 0, 0, 0, 0};

	for (int iPop = 0; iPop < 23; ++ iPop)
	{
		int offset = util::getGridOffset3D<DESCRIPTOR<T>>(iPop, N, N);
		assert(offsetData[iPop] == offset);
	}

	N = 4;
	CellBlockData3D<T, DESCRIPTOR> cellBlock( N, N, N );
	T** cellData = cellBlock.getCellData();

#ifdef ENABLE_CUDA
	GPUHandler<T,DESCRIPTOR>::get();
	GPUHandler<T,DESCRIPTOR>::get().allocateFluidField(N,N);
#endif


	for(int iPop=0; iPop < DESCRIPTOR<T>::q; ++iPop)
	{
        for(int zPosition=0; zPosition<N; ++zPosition)
        {
            for(int yPosition=0; yPosition<N; ++yPosition)
            {
                for(int xPosition=0; xPosition<N; ++xPosition)
                {
                    size_t index = util::getCellIndex3D(xPosition,yPosition,zPosition,N,N);
                    cellData[iPop][index] = iPop*1000+index;
                }
            }
        }
	}

#ifdef ENABLE_CUDA
	bool fluidMaskDummy[N*N];
	GPUHandler<T,DESCRIPTOR>::get().transferCellDataToGPU(cellData,fluidMaskDummy,N*N);
	T** cellDataGPU = GPUHandler<T,DESCRIPTOR>::get().getFluidData();
//	printKernel<<<1,1>>>(cellDataGPU,N);
	cudaDeviceSynchronize();
#endif



	for(int iPop=1; iPop < DESCRIPTOR<T>::q/2; ++iPop)
	{
	    for(int zPosition=0; zPosition<N; ++zPosition)
            for(int yPosition=0; yPosition<N; ++yPosition)
            {
                for(int xPosition=0; xPosition<N; ++xPosition)
                {
                    size_t index = util::getCellIndex3D(xPosition,yPosition,zPosition,N,N);
                    mySwap(cellData[iPop][index],cellData[DESCRIPTOR<T>::opposite(iPop)][index]);
#ifdef ENABLE_CUDA
                    swapKernel<<<1,1>>>(cellDataGPU,index,iPop);
                    cudaDeviceSynchronize();
#endif
                }
            }
	}

	cellBlock.stream();
	cellData = cellBlock.getCellData();

#ifdef ENABLE_CUDA
	GPUHandler<T,DESCRIPTOR>::get().gpuStream();
	cellDataGPU = GPUHandler<T,DESCRIPTOR>::get().getFluidData();
//	printKernel<<<1,1>>>(cellDataGPU,N);
	cudaDeviceSynchronize();
#endif

    for(int iPop=1; iPop < DESCRIPTOR<T>::q/2; ++iPop)
    {
        for(int zPosition=0; zPosition<N; ++zPosition)
            for(int yPosition=0; yPosition<N; ++yPosition)
            {
                for(int xPosition=0; xPosition<N; ++xPosition)
                {
                    size_t index = util::getCellIndex3D(xPosition,yPosition,zPosition,N,N);
                    mySwap(cellData[iPop][index],cellData[DESCRIPTOR<T>::opposite(iPop)][index]);
#ifdef ENABLE_CUDA
                    swapKernel<<<1,1>>>(cellDataGPU,index,iPop);
                    cudaDeviceSynchronize();
#endif
                }
            }
    }

    cellBlock.stream();
    cellData = cellBlock.getCellData();

#ifdef ENABLE_CUDA
	GPUHandler<T,DESCRIPTOR>::get().gpuStream();
	cellDataGPU = GPUHandler<T,DESCRIPTOR>::get().getFluidData();
//	printKernel<<<1,1>>>(cellDataGPU,N);
	cudaDeviceSynchronize();
#endif

	assert(cellData[17][31]==17021);
	assert(cellData[13][61]==13021);

#ifdef ENABLE_CUDA
	bool result = false;

	gpuCheck<<<1,1>>>(cellDataGPU,1,9,103);
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	gpuCheck<<<1,1>>>(cellDataGPU,3,5,315);
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	gpuCheck<<<1,1>>>(cellDataGPU,5,6,512);
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

	gpuCheck<<<1,1>>>(cellDataGPU,7,10,700);
	cudaDeviceSynchronize();
	cudaMemcpyFromSymbol(&result, d_result, sizeof(bool),0, cudaMemcpyDeviceToHost);
	assert(result);

#endif




	return 0;
}
