/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <cmath>

typedef double T;

#include "io/fileName.cpp"
#include "io/ostreamManager.h"
#include "io/ostreamManager.cpp"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "core/ALEutil.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, unsigned int Dimension = 3>
T** createField(std::array<size_t,Dimension> extent)
{
    T** field = new T*[Dimension];
    size_t gridSize = 1;
    for(size_t iExtent : extent)
        gridSize *= iExtent;

    for(unsigned int iDim = 0; iDim < Dimension; ++iDim)
    {
        field[iDim] = new T[gridSize];
    }
    for(unsigned int iX = 0; iX < extent[0]; ++iX)
        for(unsigned int iY = 0; iY < extent[1]; ++iY)
            for(unsigned int iZ = 0; iZ < extent[2]; ++iZ)
            {
                size_t index = iX*extent[1]*extent[2]+iY*extent[2]+iZ;
                field[0][index] = iX;
                field[1][index] = iY;
                field[2][index] = iZ;
            }

    return field;
}

int main()
{

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          1
          ,0.3*1.0/std::sqrt(3)
          ,4.
          ,80.
          ,0.0000146072
          ,1.225
          ,0);

  T spacing = converter.getConversionFactorLength();

  T spacingExtField = sqrt(2)*spacing;

  std::array<size_t,3> extent{10,10,10};
  T** externalField = createField<T,3>(extent);


  Vec3<T> outsideVelocity{converter.getLatticeVelocity(0.),converter.getLatticeVelocity(0.),0.};
  PredefinedExternalField<T,Lattice> externalFieldClass(externalField, outsideVelocity,
          extent[0], extent[1], extent[2], spacing/spacingExtField);


  Vec3<T> anchor{0,0,0};

  Vec3<T> positionALE{-sqrt(2)/2,sqrt(2)*3/2,0};
  Vec3<T> translationNondim{0.0,0,0};
  Vec3<T> attitude{0,0,45./180.*M_PI};
  Vec3<T> rotationNondim{0,0,0};

  const Affine3<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(positionALE, attitude, anchor);
  const Affine3<T> velocityRotation = aleutil::velocityRotationTransform<T>(rotationNondim, attitude);

  externalFieldClass.preTimeStepCalculations(velocityRotation, positionInExternalField);
  Vec3<T> velocity = externalFieldClass.velocityAt(1,0,2);

  std::cout << "Velocity: " << velocity(0) << "," << velocity(1) << "," << velocity(2) << std::endl;
  std::cout << "Solution: " << 1.41421 << "," << 1.41421 << "," << 1.41421 << std::endl;

  positionALE(0) = 0;

  const Affine3<T> positionInExternalField2 = aleutil::positionInExternalFieldTransform<T>(positionALE, attitude, anchor);
  const Affine3<T> velocityRotation2 = aleutil::velocityRotationTransform<T>(rotationNondim, attitude);

  externalFieldClass.preTimeStepCalculations(velocityRotation2, positionInExternalField2);
  velocity = externalFieldClass.velocityAt(1,0,2);

  std::cout << "Velocity: " << velocity(0) << "," << velocity(1) << "," << velocity(2) << std::endl;
  std::cout << "Solution: " << 1.76777 << "," << 1.06066 << "," << 1.41421 << std::endl;

  return 0;

}
