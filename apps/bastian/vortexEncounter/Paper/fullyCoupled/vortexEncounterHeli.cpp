/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "core/externalFieldALE.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
#include <fstream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <thread>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    if(pos[2] == 6)
    {
        std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
        for (int iPop = Lattice<T>::forceIndex; iPop < Lattice<T>::forceIndex+Lattice<T>::d; ++iPop)
        {
          std::cout << cell[iPop].get() << ", ";
        }
        sumZ += cell[Lattice<T>::forceIndex+2];
        std::cout << std::endl;
    }
  }
};

template<typename T>
struct Vortex
{
    Vortex(T vorticity, T coreRadiusIni, T radiusWEA, T omegaWEA, T azimuth, T vortexPosition[2]):
        vorticity_(vorticity)
       ,coreRadiusIni_(coreRadiusIni)
       ,radiusWEA_(radiusWEA)
       ,omegaWEA_(omegaWEA)
       ,azimuth_(azimuth)
       ,coreRadius_(coreRadiusIni*std::sqrt(1+0.000005/( std::pow(coreRadiusIni/radiusWEA,2)*omegaWEA )*azimuth))
       {
        vortexPosition_[0] = vortexPosition[0];
        vortexPosition_[1] = vortexPosition[1];
       }

    T vorticity_;
    T coreRadiusIni_;
    T radiusWEA_;
    T omegaWEA_;
    T azimuth_;
    T coreRadius_;
    T vortexPosition_[2];
};

template<typename T, class BlockLattice>
int addFuselageCells(int resolution, BlockLattice &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
//    std::ifstream fileReader("/scratch/ga69kiq/openLB/apps/bastian/vortexEncounter/helicopter/Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");
    std::ifstream fileReader("/scratch/BHorvat/openlb/apps/bastian/testFlightmechanicsCommunication/Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
//            std::cout << position[0] << "," << position[1] << "," << position[2] << std::endl;
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 0;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;


    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);

    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);


    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

using ReceiveData = ThrustAtRotorsHarmonic<10>;
using SendData = VelocityAtRotorsLinear;

template<class NetworkCommunicator, unsigned int RESOLUTION>
void MultipleSteps(const double simTime, int iXRightBorderArg, NetworkCommunicator& networkCommunicator, Vortex<T> vortex)
{

    using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
    using SendDataGensim = VelocityAtRotorsLinear;
    using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;

    int iXLeftBorder = 0;
    int iXRightBorder = RESOLUTION;
    int iYBottomBorder = 0;
    int iYTopBorder = RESOLUTION;
    int iZFrontBorder = 0;
    int iZBackBorder = RESOLUTION;

    T const rotorRadius = 4.92;
    T const rotorArea = rotorRadius*rotorRadius*M_PI;

    UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
            iXRightBorder
            ,0.3*1.0/std::sqrt(3)
            ,4.*4.92
            ,80.
            ,0.0000146072
            ,1.225
            ,0);

    converter.print();
    const T deltaX = converter.getPhysDeltaX();
    const T conversionVelocity = converter.getConversionFactorVelocity();


    T spacing = converter.getConversionFactorLength();

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

    std::cout << "Create blockLattice.... ";


  T vortexData[4] = {vortex.vortexPosition_[0]/deltaX,vortex.vortexPosition_[1]/deltaX,vortex.coreRadius_/deltaX,vortex.vorticity_/(conversionVelocity*deltaX)};

  T velocity[3] = {0.0,0.0,0.0};

  Vec3<T> externalVelocity(converter.getLatticeVelocity(0.0), 0.0, 0.0);
  VortexHorizontalExternalField<T,Lattice> horizontalVortex(vortexData[0],vortexData[1],vortexData[2],vortexData[3]);

  Vec3<T> lattice_anchor {0., 0., 0.};
  BlockLatticeALE3D<T, Lattice, VortexHorizontalExternalField<T, Lattice>> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1,
                                                                                              lattice_anchor, &bulkDynamics, horizontalVortex);
  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {static_cast<T>(iXRightBorder/2.),static_cast<T>(iYTopBorder/2.),static_cast<T>(iZBackBorder+1)};
  size_t numCells = addFuselageCells(iXRightBorder+1,lattice,rotorPosition[2]);

  Rotor<T> rotor(rotorRadius,rotorPosition,spacing);
  RotorCellData<T> rotorCellData(rotor,lattice, converter);
  CouplingType::CouplingDataType couplingData(rotorCellData);

  for(unsigned int iCell = 0; iCell< rotorCellData._numberRotorCells; ++iCell)
      if(rotorCellData._cellPositionRadialCPU[iCell] < 0.223)
      {
          size_t position[3];
          util::getCellIndices3D(rotorCellData._cellIndexCPU[iCell],lattice.getNy(),lattice.getNz(),position);
          lattice.defineDynamics(position[0],position[1],position[2],&instances::getBounceBack<T,Lattice>());
      }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::cout << "Rotor Position: " << rotor.getCenter()[0] << "," << rotor.getCenter()[1] << "," << rotor.getCenter()[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotor.getXLimits()[0] << " to " << rotor.getXLimits()[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotor.getYLimits()[0] << " to " << rotor.getYLimits()[1] << std::endl;

  std::cout << "Number of theoretical rotor cells: " << rotor.getNumberRotorCells() << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::cout << "Undim: " << converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea() << std::endl;

  lattice.updateAnchorPoint(Vec3<T>{rotorPosition[0],rotorPosition[1],rotorPosition[2]});
  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

  ReceiveData dataReceive;
  SendData dataSend;

  std::string name;
  std::string directory = "/scratch/BHorvat/tmp/";
  name = "vortexEncounterHeli_X";
  name += std::to_string(iXRightBorderArg+1);
  name += "_R_" + std::to_string(vortex.coreRadius_) + "_V_" + std::to_string(vortex.vorticity_);
  std::string nameTrim = name + "_trim";

  std::cout << "Running " << name << std::endl;

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriterTrim( nameTrim );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(velocityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(fluidMaskFunctor);

  vtkWriterTrim.write(0);

  ReceiveDataGensim dataReceiveGensim;
  SendDataGensim dataSendGensim;

  NetworkInterfaceTCP<ReceiveDataGensim,SendDataGensim> networkCommunicatorGensim(8888,OUTPUTIP,8888,NETWORKBUFFERSIZE,true);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  Vec3<T> position{0,0,0};
  Vec3<T> attitude{0,0,0};
  Vec3<T> translation{0,0,0};
  Vec3<T> rotation{0,0,0};

  dataReceive.simulationStatus = 1;

  std::ofstream fileWriter(directory + "thrustLBM.txt");
  std::ofstream inflowFile(directory + name + "_inflow.txt");

  if(fileWriter)
      std::cout << "File open" << std::endl;


  size_t preStep = 1;
  while(dataReceiveGensim.simulationStatus != 0)
  {
      networkCommunicatorGensim.recieve_data(dataReceiveGensim);
      dataReceiveGensim.positions[2] = 0;

      CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());

      std::cout << "Thrust: " << dataReceiveGensim.presMainMeanX << "," << dataReceiveGensim.presMainMeanY << "," << dataReceiveGensim.presMainMeanZ << std::endl;

      std::cout << "Simulation status is: " << dataReceiveGensim.simulationStatus << std::endl;

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          translation(iDim) = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
          rotation(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
          position(iDim)    = dataReceiveGensim.positions[iDim]/spacing;
          attitude(iDim)    = dataReceiveGensim.attitudes[iDim];

          std::cout << "Axis " << iDim << " : " << dataReceiveGensim.velocities[iDim] << ","
                  << dataReceiveGensim.rotatorics[iDim] << "," << dataReceiveGensim.positions[iDim] << ","
                  << dataReceiveGensim.attitudes[iDim] << std::endl;
      }

      unsigned int trimTime = converter.getLatticeTime(8);

      if(preStep == 1)
      {
          trimTime = converter.getLatticeTime(8);
      }

      for(unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
      {
          lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
          lattice.moveMeshGPU(translationNondim,rotationNondim,positionALE,attitude);
          HANDLE_ERROR(cudaGetLastError());
      }

      lattice.copyDataToCPU();
      vtkWriterTrim.write(preStep);

      std::cout << "Trimstep " << preStep << " finished" << std::endl;

      CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

      std::cout << "vi: " << dataSendGensim.velocityMainMean[0] << "," << dataSendGensim.velocityMainMean[1]
                                                          << "," << dataSendGensim.velocityMainMean[2] << std::endl;

      networkCommunicatorGensim.send_data(dataSendGensim);
      ++preStep;

      if(dataReceiveGensim.simulationStatus == 0)
      {
          break;
      }

  }

  std::cout << "Finished trim in " << preStep << " steps" << std::endl;

  std::string nameSim = name + "_sim";
  BlockVTKwriter3D<T> vtkWriterSim( nameSim );
  vtkWriterSim.addFunctor(densityFunctor);
  vtkWriterSim.addFunctor(velocityFunctor);
  vtkWriterSim.addFunctor(physVelocityFunctor);
  vtkWriterSim.addFunctor(forceFunctor);
  vtkWriterSim.addFunctor(fluidMaskFunctor);

  vtkWriterSim.write(0);

  std::cout << "Starting time simulation" << std::endl;
  unsigned int print = 1;

  for(unsigned int timeStep = 0; timeStep <= converter.getLatticeTime(simTime); ++timeStep)
  {
      for(auto position : rotorCellPositionsRelativ)
      {
          writer.setByLocalIndex(position[0],position[1],position[2])[0] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[1] = 0;
          writer.setByLocalIndex(position[0],position[1],position[2])[2] = 0;
      }

      networkCommunicator.recieve_data(dataReceive);

      Vec3<T> position {0, 0, 0};
      Vec3<T> orientation {0, 0, 0};
      Vec3<T> movement{0,0,0};
      Vec3<T> rotation{0,0,0};

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          movement(iDim)    = dataReceive.velocities[iDim]/converter.getConversionFactorVelocity();
          rotation(iDim)    = dataReceive.rotatorics[iDim]*converter.getConversionFactorTime();
          position(iDim)    = dataReceive.positions[iDim]/deltaX;
          orientation(iDim) = dataReceive.attitudes[iDim];
      }

      std::vector<T> thrustAbs(3,0);

      for(auto pos : rotorCellPositionsRelativ)
      {
          std::vector<T> thrustPerCell(3,0);
          int index = pos[3];
          for (unsigned int iHarmonics=0; iHarmonics<10; ++iHarmonics)
          {
              thrustPerCell[0] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosX[iHarmonics];
              thrustPerCell[0] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinX[iHarmonics];

              thrustPerCell[1] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosY[iHarmonics];
              thrustPerCell[1] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinY[iHarmonics];

              thrustPerCell[2] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainCosZ[iHarmonics];
              thrustPerCell[2] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[index])*dataReceive.presMainSinZ[iHarmonics];
          }

          thrustPerCell[0] += dataReceive.presMainMeanX;
          thrustPerCell[1] += dataReceive.presMainMeanY;
          thrustPerCell[2] += dataReceive.presMainMeanZ;

          thrustPerCell[0] *= rotorCellWeight[index];
          thrustPerCell[1] *= rotorCellWeight[index];
          thrustPerCell[2] *= rotorCellWeight[index];

          thrustAbs[2] += thrustPerCell[2];

          writer.setByLocalIndex(pos[0], pos[1], pos[2])[0] = converter.getLatticeForce(thrustPerCell[0]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[1] = converter.getLatticeForce(thrustPerCell[1]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
          writer.setByLocalIndex(pos[0], pos[1], pos[2])[2] = converter.getLatticeForce(thrustPerCell[2]*std::pow(spacing,2)/1.225)/HelicopterData<T>::rotorArea;
      }

      writer.transferAndWrite();

      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

//      if(timeStep%converter.getLatticeTime(0.1) == 0)
//      {
//          lattice.copyDataToCPU();
//          vtkWriter.write(timeStep+preStep+1);
//      }

      lattice.moveMeshGPU(movement,rotation, position, orientation);

      reader.readAndTransfer();
      size_t const * cellIndicesGPU = reader.getTransferObject().getRawPointerToGPUCellIndices();
      size_t numReadCells = reader.getTransferObject().getNumberOfRegisteredCells();

      std::vector<T> velAbs(3,0);

      if(timeStep%converter.getLatticeTime(0.1) == 0)
          inflowFile << "# Time: " << converter.getPhysTime(timeStep) << std::endl;

      for(auto pos : rotorCellPositionsRelativ)
      {
          size_t index = pos[3];
          std::vector<T> velocity(3,0);

          inflowFile << pos[0] << "," << pos[1] << "," << pos[2];

          if(rotorCellWeight[index] == 1)
          {
              for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
              {
                  velocity[iDim] = converter.getPhysVelocity(reader.getByLocalIndex(pos[0],pos[1],pos[2])[iDim]);
                  velAbs[iDim] += velocity[iDim]*spacing*spacing;
                  dataSend.velocityMainMean[iDim] += velocity[iDim]/fullRotorCells;
                  dataSend.velocityMainSin[iDim] += std::sin(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;
                  dataSend.velocityMainCos[iDim] += std::cos(rotorCellPositionAzimuth[index])*velocity[iDim]*rotorCellPositionRadial[index]/4./fullRotorCells;

                  if(timeStep%converter.getLatticeTime(0.1) == 0)
                      inflowFile << "," << velocity[iDim];
              }
          }
          else
          {
              if(timeStep%converter.getLatticeTime(0.1) == 0)
                  inflowFile << ",0,0,0";
          }

          if(timeStep%converter.getLatticeTime(0.1) == 0)
              inflowFile << std::endl;


      }

      networkCommunicator.send_data(dataSend);

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
          dataSend.velocityMainMean[iDim] = 0;
          dataSend.velocityMainSin[iDim] = 0;
          dataSend.velocityMainCos[iDim] = 0;
      }

  }

  cudaDeviceSynchronize();

  timer.stop();

}

int main()
{
    NetworkInterfaceTCP<ReceiveData,SendData> networkCommunicator(INPUTPORT,OUTPUTIP,OUTPUTPORT,NETWORKBUFFERSIZE,true);

    const double simTime = 10.0;
    const size_t resolution = 31;

    std::vector<std::tuple<T, T>> vorticity = {
		    std::make_tuple(177.,126.)
		   ,std::make_tuple(-177.,126.)
		   ,std::make_tuple(139.9,99.6)
		   ,std::make_tuple(-139.9,99.6)
		   ,std::make_tuple(88.5,63.)
		   ,std::make_tuple(-88.5,63.)
    };
    std::vector<T> age = {180.,720};

   T vortexPosition[2] = {50,-1000.};
//   T vortexPosition[2] = {90,-1000.};
//   T vortexPosition[2] = {140.,-1000.};

    for(unsigned int iVortex = 0; iVortex < 2; ++iVortex)
    {
        for(auto iAge : age)
        {
            Vortex<T> vortex(std::get<0>(vorticity[iVortex])*0.5,0.2112,std::get<1>(vorticity[iVortex]),0.634,iAge,vortexPosition);
            MultipleSteps(simTime,resolution,networkCommunicator,vortex);
            networkCommunicator.reconnect();
        }
    }

    for(unsigned int iVortex = 2; iVortex < 4; ++iVortex)
    {
        for(auto iAge : age)
        {
            Vortex<T> vortex(std::get<0>(vorticity[iVortex])*0.5,0.1669,std::get<1>(vorticity[iVortex]),0.801,iAge,vortexPosition);
            MultipleSteps(simTime,resolution,networkCommunicator,vortex);
            networkCommunicator.reconnect();
        }
    }

    for(unsigned int iVortex = 4; iVortex < 6; ++iVortex)
    {
        for(auto iAge : age)
        {
            Vortex<T> vortex(std::get<0>(vorticity[iVortex])*0.5,0.1056,std::get<1>(vorticity[iVortex]),1.267,iAge,vortexPosition);
            MultipleSteps(simTime,resolution,networkCommunicator,vortex);
            networkCommunicator.reconnect();
        }
    }


	return 0;
}
