/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "fstream"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
    for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
  }
};

T ramp(T currentTime, T startTime, T period)
{
    if(currentTime > startTime and currentTime <= startTime+period)
        return (currentTime-startTime)/period;
    else if (currentTime > startTime+period and currentTime <= startTime+2*period)
        return (startTime+2*period-currentTime)/period;
    else if (currentTime > startTime+2*period and currentTime <= startTime+3*period)
        return (startTime+2*period-currentTime)/period;
    else if (currentTime > startTime+3*period and currentTime <= startTime+4*period)
        return -(startTime+4*period-currentTime)/period;
    else
        return 0;

}


template<typename T>
int addFuselageCells(int resolution, BlockLatticeALE3D<T,Lattice,ExternalFieldALE<T,Lattice,FunctionVelocityProviderF>> &lattice, T &rotorPositionZ)
{
    size_t cells = resolution*resolution*resolution;
    std::ifstream fileReader("/media/ga69kiq/work/BHorvat/GitRepositories/openlb/apps/bastian/testFlightmechanicsCommunication/Fuselage_LBM/fus_" + std::to_string(resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;
    else
    {
        std::cout << "Fuselage file not found" << std::endl;
        std::exit(1);
    }

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]);
//            std::cout << position[0] << "," << position[1] << "," << position[2] << std::endl;
            lattice.defineDynamics((resolution-position[0]-1), (resolution-position[0]-1), position[1], position[1], position[2], position[2], &instances::getBounceBack<T,Lattice>());
            ++fuselageCells;
        }

    }
    rotorPositionZ -= 1;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << std::endl;
    return fuselageCells;
}

void MultipleSteps(const double simTime, int iZBackBorderArg)
{
  int iXLeftBorder = 0;
  int iXRightBorder = iZBackBorderArg;
  int iYBottomBorder = 0;
  int iYTopBorder = iZBackBorderArg;
  int iZFrontBorder = 0;
  int iZBackBorder = iZBackBorderArg;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iYTopBorder
          ,0.3*1.0/std::sqrt(3)
          ,20.
          ,60.
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();


  T conversionVelocity = converter.getConversionFactorVelocity();

  T deltaX = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, 0.075);
  const T velocity[3] = {-5/conversionVelocity,0/conversionVelocity,0/conversionVelocity};

  T vorticity = 177.*0.5/(conversionVelocity*deltaX);
  T coreRadiusIni = 0.2112;
  T radiusWEA = 126.;
  T omegaWEA = 0.634;
  T azimuth = 30.;
  T coreRadius = coreRadiusIni*std::sqrt(1+0.000005/( std::pow(coreRadiusIni/radiusWEA,2)*omegaWEA )*azimuth)/deltaX;
  T vortexPosition[2] = {100.,(iZBackBorder+1)*0.25};

  T vortexData[4] = {vortexPosition[0],vortexPosition[1],coreRadius,vorticity};

  VortexVelocityProviderF<T,Lattice> provider(vortexData,4);
  ExternalFieldALE<T,Lattice,VortexVelocityProviderF> externalFieldALE(provider);
  BlockLatticeALE3D<T, Lattice, ExternalFieldALE<T,Lattice,VortexVelocityProviderF>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder+1,
          {0, 0, 0}, &bulkDynamics,externalFieldALE);

  T rotorPositionZ = iZBackBorder+1;
  addFuselageCells(iZBackBorder+1,lattice,rotorPositionZ);

  OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
    createInterpBoundaryCondition3D<T,Lattice,
    ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
  boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

  boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
  boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
  boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

  boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
  boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
  boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
  boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

  boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
  boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

  boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

  boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
  boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
  boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);

  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {

            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
    }
  }

 for (int iX = static_cast<int>(0.25*(iXRightBorder+1)); iX < static_cast<int>(0.75*(iXRightBorder+1)); ++iX)
 {
     for (int iY = static_cast<int>(0.25*(iYTopBorder+1)); iY < static_cast<int>(0.75*(iYTopBorder+1)); ++iY)
     {
         for (int iZ = static_cast<int>(rotorPositionZ); iZ <= static_cast<int>(rotorPositionZ); ++iZ)
         {
             T force[Lattice<T>::d] = {0., 0., 0*converter.getLatticeForce(320.*deltaX*deltaX/1.225)};
             T radius = sqrt(pow(static_cast<double>(iY)-0.5*(iYTopBorder+1),2)+pow(static_cast<double>(iX)-0.5*(iXRightBorder+1),2));
             if(radius < 0.25*iYTopBorder)
                 lattice.defineForce(iX,iX,iY,iY,iZ,iZ,force);
         }
     }
 }

  std::string name;
  name = "vortexEncounter_X";
  name += std::to_string(iZBackBorderArg+1);
  name += "";

  BlockVTKwriter3D<T> vtkWriter( name );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);

  singleton::directories().setOutputDir("/scratch/BHorvat/vortexEncounter/V1/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.write(0);

  lattice.copyDataToGPU();

  Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();
  unsigned int printTime = converter.getLatticeTime(0.05);

  PoseXd<T, Lattice> position {{-10/converter.getConversionFactorLength(), 0, 0}, {0, 0, 0}};
  PoseXd<T, Lattice> movement {{10./conversionVelocity,0./conversionVelocity,0}, {0,0,0}};

  for(unsigned int iSteps=1; iSteps<=converter.getLatticeTime(simTime); ++iSteps)
  {
	  lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();

	  if(iSteps%converter.getLatticeTime(0.5) == 0)
	      timer.print(iSteps,2);

	  if(iSteps%printTime == 0)
	  {
	      lattice.copyDataToCPU();
		  vtkWriter.write(iSteps);
	  }


      if(iSteps < converter.getLatticeTime(1))
      {
          movement.position_[0] = 0.01/conversionVelocity;
          movement.position_[1] = 0.0/conversionVelocity;
      }
      else
      {
          movement.position_[0] = 10./conversionVelocity;
          movement.position_[1] = 0./conversionVelocity;
      }

//      if(iSteps == converter.getLatticeTime(16.30))
//          printTime = 1;

      position.position_[0] += movement.position_[0];
//      if(iSteps == converter.getLatticeTime(13))
//      {
//          T velocity[Lattice<T>::d] = {-10./conversionVelocity,0./conversionVelocity,0.};
//          provider.setVelocity(velocity);
//      }
//      else if(iSteps == converter.getLatticeTime(17))
//      {
//          T velocity[Lattice<T>::d] = {-15./conversionVelocity,0./conversionVelocity,0.};
//          provider.setVelocity(velocity);
//      }

      lattice.moveMeshGPU(movement,position);
  }

  timer.stop();

  delete boundaryCondition;

}

int main(int argc, char** argv)
{
    const double simTime = 20;
//    MultipleSteps(simTime,63);
    MultipleSteps(simTime,31);
//    MultipleSteps(simTime,95);
//    MultipleSteps(simTime,127);
	return 0;
}
