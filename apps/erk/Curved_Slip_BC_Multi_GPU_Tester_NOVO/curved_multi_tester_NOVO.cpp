/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "../../../eigen-3.4.0/Eigen/Eigen"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"
#include "communication/writeFunctorToGPU.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/readWriteFunctorsGPU.h"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;
using namespace Eigen;

/* Simulation Parameters */
T smagoConstant = 0.05;
const T shipLength = 2.77368;
const double simTime = 10;
unsigned int resolution = 80;
unsigned int xExtraResolution = 4 * resolution;

const int xLength = resolution + xExtraResolution;
const int yLength = 1.5 * resolution;
const int zLength = resolution * 0.5;
const T gridSpacing = shipLength / resolution;
const T gridArea = pow(gridSpacing, 2);

const T physInducedVelocity = 40;
const T physVelocity = 15.0;
const T physDensity = 1.225;               // kg/m^3
const T physKinematicViscosity = 14.61e-6; // m^2/s

/* Output Parameters */
bool outputVTKData = true;

const T vtkWriteInterval = 0.01; // s

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    initGhostLayer(domainInfo, lattice);

    T omega = dynamics.getOmega();






    auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, -1>> periodicplane1N;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, 1>> periodicplane1P;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, -1>> periodicplane2N;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, 1>> periodicplane2P;

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> slipedge0PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> slipedge0NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> slipedge0PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> slipedge0NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipedge1PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipedge1NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipedge1PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipedge1NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, -1>> slipedge2NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, 1>> slipedge2NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, -1>> slipedge2PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, 1>> slipedge2PP;

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    static IniEquilibriumDynamics<T, Lattice> iniEquil{};

    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipcornerPNN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipcornerPPN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipcornerPNP;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipcornerPPP;

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
            {
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
            }
            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics0P);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane1P); //&slip1P);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane1N); //&slip1N);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane2N); //&slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane2P); //&slip2P);
        }
    }

    OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
        createInterpBoundaryCondition3D<T, Lattice,
                                        ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2PP); //&slipedge2PP);
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NP); //&slipedge2NP);
        }
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NN); //&slipedge2NN);
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2PN); //&slipedge2PN);
        }
    }

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PP); //&slipedge1NP);
        }
        if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NP); //&slipedge1PP);
        }
        if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PN); //&slipedge1PN);
        }
        if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NN); //&slipedge1NN);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PN); //&slipedge0PN);
        if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PP); //&slipedge0PP);
        if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NN); //&slipedge0NN);
        if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NP); //&slipedge0NP);
    }

    Index3D localIndex;
    if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    {
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPN);
    }
    if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    {
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPP);
    }
    if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    {
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNP);
    }
    if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    {
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNN);
    }
    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN);
    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP);
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP);
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN);
}

/*
double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}
*/

//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
bool check75(int iX, int iY, int iZ, Index3D &localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

T calculateDelta(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], T neighborCell[3], T dir[3], STLreader<double> &stlReader)
{
    typedef double T;
    T physFluidLocation[3] = {converter.getPhysLength(slipCell[0]), converter.getPhysLength(slipCell[1]), converter.getPhysLength(slipCell[2])};
    T fracSpacing = 100;
    T dirMag = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));
    T dotDel = (converter.getPhysDeltaX() * dirMag) / fracSpacing;
    T physXpoint = converter.getPhysLength(slipCell[0]); //physFluidLocation[0];
    T physYpoint = converter.getPhysLength(slipCell[1]); //physFluidLocation[1];
    T physZpoint = converter.getPhysLength(slipCell[2]); //physFluidLocation[2];
    T Xw;
    T Yw;
    T Zw;
    T physPoint[3];
    for (int i = 1; i <= fracSpacing; ++i)
    {
        physPoint[0] = physXpoint;
        physPoint[1] = physYpoint,
        physPoint[2] = physZpoint;
        // std::vector<T> tmpLoc(physPoint, physPoint+3);
        Xw = physXpoint;
        Yw = physYpoint;
        Zw = physZpoint;
        physXpoint += dir[0] * dotDel;
        physYpoint += dir[1] * dotDel;
        physZpoint += dir[2] * dotDel;
        bool isInside[1];
        T location[3] = {physXpoint, physYpoint, physZpoint};
        stlReader(isInside, location);
        if (isInside[0])
        {
            break;
        }
    }
    T physWallLocation[3] = {Xw, Yw, Zw};
    T physSolidLocation[3] = {converter.getPhysLength(neighborCell[0]), converter.getPhysLength(neighborCell[1]), converter.getPhysLength(neighborCell[2])};
    T numMagnitude = sqrt(pow((physFluidLocation[0] - physWallLocation[0]), 2) +
                          pow((physFluidLocation[1] - physWallLocation[1]), 2) +
                          pow((physFluidLocation[2] - physWallLocation[2]), 2));
    T denMagnitude = sqrt(pow((physFluidLocation[0] - physSolidLocation[0]), 2) +
                          pow((physFluidLocation[1] - physSolidLocation[1]), 2) +
                          pow((physFluidLocation[2] - physSolidLocation[2]), 2));

    float delta = numMagnitude / denMagnitude;
    // T delta = numMagnitude/denMagnitude;
    /*
    myfile << physFluidLocation[0] << ",";
    myfile << physFluidLocation[1] << ",";
    myfile << physFluidLocation[2] << ",";
    myfile << physSolidLocation[0] << ",";
    myfile << physSolidLocation[1] << ",";
    myfile << physSolidLocation[2] << ",";
    myfile << Xw << ",";
    myfile << Yw << ",";
    myfile << Zw << ",";
    myfile << delta << "\n";
*/
    float latticeWallLocation[3] = {Xw / converter.getPhysDeltaX(), Yw / converter.getPhysDeltaX(), Zw / converter.getPhysDeltaX()};
    // T latticeWallLocation[3] = {Xw/converter.getPhysDeltaX(), Yw/converter.getPhysDeltaX(), Zw/converter.getPhysDeltaX()};
    std::vector<float> output = {latticeWallLocation[0], latticeWallLocation[1], latticeWallLocation[2], delta};
    // std::vector<T> output = {latticeWallLocation[0],latticeWallLocation[1] ,latticeWallLocation[2], delta};
    // float delta = 0.25;
    return delta;
    // return output;
}

std::vector<double> checkDirection(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, STLreader<double> &stlReader)
{
    typedef double T;
    T spacing = 1.0 / 100.0; //100.0;
    T fracSpacing = 100;
    bool correctDirection = true;
    for (int i = 1; i <= fracSpacing; ++i)
    {
        vecX += spacing * normalEigenvector[0];
        vecY += spacing * normalEigenvector[1];
        vecZ += spacing * normalEigenvector[2];

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if (isInsideSTL[0])
        {
            correctDirection = false;
            break;
        }
    }
    if (correctDirection == false)
    {
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalEigenvector[iDim] = -normalEigenvector[iDim];
        }
    }
    return normalEigenvector;
}

std::vector<double> checkOrientation(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, std::vector<double> otherEigenvector, STLreader<double> &stlReader)
{
    typedef double T;
    T fracSpacing = 100;
    T dirMag = sqrt(pow(normalEigenvector[0], 2) + pow(normalEigenvector[1], 2) + pow(normalEigenvector[2], 2));
    T spacing = (2.0 * converter.getPhysDeltaX() * dirMag) / fracSpacing;

    bool correctOrientation = false;
    for (int i = 1; i <= fracSpacing; ++i)
    {
        vecX += -spacing * normalEigenvector[0];
        vecY += -spacing * normalEigenvector[1];
        vecZ += -spacing * normalEigenvector[2];

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if (isInsideSTL[0])
        {
            correctOrientation = true;
            break;
        }
    }

    if (correctOrientation == false)
    {
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalEigenvector[iDim] = otherEigenvector[iDim];
        }
    }

    return normalEigenvector;
}

std::vector<double> normalizeNormals(std::vector<double> normalEigenvector)
{
    T normalAbs = 0;
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
        normalAbs += normalEigenvector[iDim] * normalEigenvector[iDim];
    }
    normalAbs = std::sqrt(normalAbs);
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
        normalEigenvector[iDim] = normalEigenvector[iDim] / normalAbs;
    }
    return normalEigenvector;
}

std::vector<T> calculateNormal(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], STLreader<double> &stlReader)
{
    typedef double T;
    T fracSpacing = 100;
    T pointX = slipCell[0];
    T pointY = slipCell[1];
    T pointZ = slipCell[2];
    // myfile << pointX << "," << pointY << "," << pointZ << ",";
    T normal[3] = {0, 0, 0};
    int numPoints = 0;
    T maxX = 0;
    T maxY = 0;
    T maxZ = 0;
    int p = 0;
    T centroidX = 0;
    T centroidY = 0;
    T centroidZ = 0;
    std::vector<T> pointsX(0);
    std::vector<T> pointsY(0);
    std::vector<T> pointsZ(0);
    std::vector<std::vector<T>> points;
    T covarianceMatrix[3][3];

    for (int iPop = 1; iPop < Lattice<T>::q; ++iPop)
    {
        T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};
        T dirMag = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));
        T dotDel = (2.0 * converter.getPhysDeltaX() * dirMag) / fracSpacing; //(2.0*sqrt(2.0)*converter.getPhysDeltaX()*dirMag)/fracSpacing;
        T physXpoint = converter.getPhysLength(slipCell[0]);                 //physFluidLocation[0];
        T physYpoint = converter.getPhysLength(slipCell[1]);                 //physFluidLocation[1];
        T physZpoint = converter.getPhysLength(slipCell[2]);                 //physFluidLocation[2];
        T Xw = physXpoint;
        T Yw = physYpoint;
        T Zw = physZpoint;
        for (int i = 1; i <= fracSpacing; ++i)
        {
            Xw += dir[0] * dotDel;
            Yw += dir[1] * dotDel;
            Zw += dir[2] * dotDel;
            bool isInside[1];
            T location[3] = {Xw, Yw, Zw};
            stlReader(isInside, location);
            if (isInside[0])
            {
                ////////////eigenvalue method///////////////
                centroidX += Xw;
                centroidY += Yw;
                centroidZ += Zw;

                pointsX.push_back(Xw);
                pointsY.push_back(Yw);
                pointsZ.push_back(Zw);
                //////////////
                numPoints++;
                normal[0] += (Xw - physXpoint);
                normal[1] += (Yw - physYpoint);
                normal[2] += (Zw - physZpoint);

                // myfile << physXpoint << "," << physYpoint << "," << physZpoint << "," << Xw << "," << Yw << "," << Zw << "\n";
                break;
            }
        }
    }

    T centroid[3] = {centroidX / numPoints, centroidY / numPoints, centroidZ / numPoints};
    points.resize(numPoints, std::vector<T>(3));
    for (unsigned int i = 0; i < numPoints; ++i)
    {
        points[i][0] = pointsX[i];
        points[i][1] = pointsY[i];
        points[i][2] = pointsZ[i];
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            covarianceMatrix[i][j] = 0.0;
            for (int k = 0; k < points.size(); k++)
            {
                covarianceMatrix[i][j] += (centroid[i] - points[k][i]) * (centroid[j] - points[k][j]);
                covarianceMatrix[i][j] /= points.size(); // - 1;
            }
        }
    }
    Matrix3d COV;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            COV(i, j) = covarianceMatrix[i][j];
        }
    }

    EigenSolver<MatrixXd> solution(COV);
    VectorXd D = solution.eigenvalues().real();
    MatrixXd V = solution.eigenvectors().real();

    T minEig = D.minCoeff();
    T maxEig = D.maxCoeff();
    std::vector<double> eigenvalues;
    std::vector<std::vector<T>> eigenVectorMatrix;
    eigenvalues.resize(D.size());
    eigenVectorMatrix.resize(D.size(), std::vector<T>(3));
    int index;
    int maxIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        eigenvalues[i] = D(i);
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            eigenVectorMatrix[i][j] = V(i, j);
        }
    }
    bool found = false;

    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == minEig)
        {
            index = i;
            found = true;
            break;
        }
    }

    bool foundMax = false;

    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == maxEig)
        {
            maxIndex = i;
            foundMax = true;
            break;
        }
    }

    std::vector<T> normalEigenvector;
    normalEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        normalEigenvector[i] = eigenVectorMatrix[i][index];
    }
    int otherIndex1;
    int otherIndex2;
    if (index == 0)
    {
        otherIndex1 = 1;
        otherIndex2 = 2;
    }
    if (index == 1)
    {
        otherIndex1 = 0;
        otherIndex2 = 2;
    }
    if (index == 2)
    {
        otherIndex1 = 0;
        otherIndex2 = 1;
    }
    int midIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        if (i != index && i != maxIndex)
        {
            midIndex = i;
        }
    }

    std::vector<T> otherEigenvector0;
    otherEigenvector0.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector0[i] = eigenVectorMatrix[i][index];
    }

    std::vector<T> otherEigenvector1;
    otherEigenvector1.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector1[i] = eigenVectorMatrix[i][otherIndex1];
    }

    std::vector<T> otherEigenvector2;
    otherEigenvector2.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector2[i] = eigenVectorMatrix[i][otherIndex2];
    }

    std::vector<T> maxEigenvector;
    maxEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        maxEigenvector[i] = eigenVectorMatrix[i][maxIndex];
    }

    std::vector<T> midEigenvector;
    midEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        midEigenvector[i] = eigenVectorMatrix[i][midIndex];
    }

    normalEigenvector = normalizeNormals(normalEigenvector);
    otherEigenvector1 = normalizeNormals(otherEigenvector1);
    otherEigenvector2 = normalizeNormals(otherEigenvector2);
    maxEigenvector = normalizeNormals(maxEigenvector);
    midEigenvector = normalizeNormals(midEigenvector);
    ///////////////////////////////////////////////////////

    T vecX = converter.getPhysLength(slipCell[0]);
    T vecY = converter.getPhysLength(slipCell[1]);
    T vecZ = converter.getPhysLength(slipCell[2]);
    T spacing = 1.0 / 100.0;

    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, midEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, maxEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);

    // myfile << normalEigenvector[0] << "," << normalEigenvector[1] << "," << normalEigenvector[2] << ",";
    // myfile << otherEigenvector1[0] << "," << otherEigenvector1[1] << "," << otherEigenvector1[2] << ",";
    // myfile << otherEigenvector2[0] << "," << otherEigenvector2[1] << "," << otherEigenvector2[2] << "\n";

    float output0 = normalEigenvector[0];
    float output1 = normalEigenvector[1];
    float output2 = normalEigenvector[2];

    std::vector<double> output = {output0, output1, output2};
    // std::vector<T> output = {normalEigenvector[0],normalEigenvector[1],normalEigenvector[2]};
    return output;
}

std::vector<int> CountFluidCellsAroundBody(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder)
{

    std::vector<int> counter{0, 0, 0, 0, 0, 0, 0};

    for (int iX = 2; iX <= iXRightBorder - 2; ++iX)
    {
        for (int iY = 2; iY <= iYTopBorder - 2; ++iY)
        {
            for (int iZ = 2; iZ <= iZBackBorder - 2; ++iZ)
            {

                bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                if (isFluid == false)
                {

                    int cell0P[3] = {iX + 1, iY, iZ};
                    int cell0N[3] = {iX - 1, iY, iZ};
                    int cell1P[3] = {iX, iY + 1, iZ};
                    int cell1N[3] = {iX, iY - 1, iZ};
                    int cell2P[3] = {iX, iY, iZ + 1};
                    int cell2N[3] = {iX, iY, iZ - 1};

                    bool isCell0PFluid = lattice.getMaskEntry(cell0P[0], cell0P[1], cell0P[2]);
                    bool isCell0NFluid = lattice.getMaskEntry(cell0N[0], cell0N[1], cell0N[2]);
                    bool isCell1PFluid = lattice.getMaskEntry(cell1P[0], cell1P[1], cell1P[2]);
                    bool isCell1NFluid = lattice.getMaskEntry(cell1N[0], cell1N[1], cell1N[2]);
                    bool isCell2PFluid = lattice.getMaskEntry(cell2P[0], cell2P[1], cell2P[2]);
                    bool isCell2NFluid = lattice.getMaskEntry(cell2N[0], cell2N[1], cell2N[2]);

                    if (isCell0PFluid)
                    {
                        counter[0]++;
                    }
                    if (isCell0NFluid)
                    {
                        counter[1]++;
                    }
                    if (isCell1PFluid)
                    {
                        counter[2]++;
                    }
                    if (isCell1NFluid)
                    {
                        counter[3]++;
                    }
                    if (isCell2PFluid)
                    {
                        counter[4]++;
                    }
                    if (isCell2NFluid)
                    {
                        counter[5]++;
                    }
                }
            }
        }
    }
    counter[6] = counter[0] + counter[1] + counter[2] + counter[3] + counter[4] + counter[5];
    return counter;
}

//burada kaldim

template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), shipLength, physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::cout << "Define boundaries.... " << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain, converter);

    int numero = domainInfo.localSubDomain; //this is hard coded you might want to change it
    bool isInGPU;
    if (numero == 0) //this is case specific this needs to be modified when the stl configuration changes
    {
        isInGPU = true;
    }
    else
    {
        isInGPU = false;
    }
    std::cout << "bool isInGPU test has been done for GPU #" << std::to_string(domainInfo.localSubDomain) << ". It returned:" << isInGPU << std::endl;

    ///////////////////CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC/////////////////////////////////////////////

    ///used to be
    ///static PostProcessingDynamics<T, Lattice, CurvedSlipBoundaryProcessor3D<T, Lattice>> slipBoundary; //mei et al
    static PostProcessingDynamics<T, Lattice, BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>> slipBoundary; ///reference in isabel's thing slip assumption
    static NoDynamics<T, Lattice> noDynamics;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    STLreader<T> stlReader("1_50_scale_carderock_sfs2_no_out_surface_long_fluid_15_width_4totlength_1_ship_2.STL", converter.getConversionFactorLength(), 1);
    //stl name suggests 4totlength_1_ship_2: 4 ship length of fluid domain 1 ship length away from inlet, ship, 2 ship length away from the outlet
    stlReader.print();

    for (int iX = 0; iX <= iXRightBorder; ++iX)
    {
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T iXPhys = converter.getPhysLength(iX);
                T iYPhys = converter.getPhysLength(iY);
                T iZPhys = converter.getPhysLength(iZ);
                //T location[3] = {(T)iX, (T)iY, (T)iZ};
                T location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);
                if (isInside[0])
                {
                    if (check75(iX, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                }
            }
        }
    }

    for (unsigned iX = 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = 1; iY <= iYTopBorder - 1; iY++)
        {
            Index3D localIndex;
            T iXPhys = converter.getPhysLength(iX);
            T iYPhys = converter.getPhysLength(iY);
            T iZPhys = converter.getPhysLength(0);
            T location[3] = {iXPhys, iYPhys, iZPhys};
            bool isInside[1];
            stlReader(isInside, location);
            if (isInside[0])
            {
            }
            else
            {
                if (check75(iX, iY, 0, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            }
        }
    }

    ///new application start

    ///new application end

    std::ofstream oldMethod0("oldmethod0.csv");
    std::ofstream oldMethod1("oldmethod1.csv");
    cudaDeviceSynchronize();

    if (numero == 0)
    {
        oldMethod0 << "Index1, Index2, Index3\n";
    }
    if (numero == 1)
    {
        oldMethod1 << "Index1, Index2, Index3\n";
    }
    size_t localIndices[3];
    //  bool checker = false;
    if (isInGPU)
    {
        for (int iX = 1; iX <= iXRightBorder - 1; ++iX)
        {
            for (int iY = 1; iY <= iYTopBorder - 1; ++iY)
            {
                for (int iZ = 2; iZ <= iZBackBorder - 1; ++iZ)
                {

                    bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                    if (isFluid == false)
                    {
                        for (unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop)
                        {
                            const int neighbor[3] = {iX + Lattice<T>::c(iPop, 0), iY + Lattice<T>::c(iPop, 1), iZ + Lattice<T>::c(iPop, 2)};
                            bool isNfluid = lattice.getMaskEntry(neighbor[0], neighbor[1], neighbor[2]);
                            if (neighbor[2] != 0)
                            {
                                if (isNfluid == true)
                                {

                                    lattice.defineDynamics(neighbor[0], neighbor[1], neighbor[2], &slipBoundary);
                                    size_t index = util::getCellIndex3D(iX, iY, iZ, lattice.getNy(), lattice.getNz());
                                    util::getCellIndices3D(index, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], localIndices);
                                    Index3D globalIndices = refSubDomain.getGlobalIndex(Index3D{localIndices[0], localIndices[1], localIndices[2], refSubDomain.localSubDomain});

                                    if (numero == 0)
                                    {
                                        oldMethod0 << globalIndices[0] << " " << globalIndices[1] << " " << globalIndices[2] << "\n";
                                    }
                                    if (numero == 1)
                                    {
                                        oldMethod1 << globalIndices[0] << " " << globalIndices[1] << " " << globalIndices[2] << "\n";
                                    }
                                    //if (neighbor[0] * converter.getPhysDeltaX() < 3.5)
                                    //{
                                    //    cout << "Neighbor iX= " << neighbor[0] * converter.getPhysDeltaX() << "  iY=" << neighbor[1] * converter.getPhysDeltaX() << "  iZ=" << neighbor[2] * converter.getPhysDeltaX() << endl;
                                    //}
                                    //NslipBoundaries++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    cudaDeviceSynchronize;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    oldMethod0.close();
    oldMethod1.close();


    std::cout << "Define ghostlayer to be bulkdynamics but fluidmask false ";
    GhostDynamics<T, Lattice> ghostLayerDynamics(1.0);
    for (unsigned iX = 0; iX < domainInfo.getLocalInfo().localGridSize()[0]; ++iX)
    {
        for (unsigned iY = 0; iY < domainInfo.getLocalInfo().localGridSize()[1]; ++iY)
        {
            for (unsigned iZ = 0; iZ < domainInfo.getLocalInfo().localGridSize()[2]; ++iZ)
            {
                if (iX < domainInfo.getLocalInfo().ghostLayer[0] or iX >= domainInfo.getLocalInfo().localGridSize()[0] - domainInfo.getLocalInfo().ghostLayer[0])
                    lattice.defineDynamics(iX, iY, iZ, &ghostLayerDynamics);
                if (iY < domainInfo.getLocalInfo().ghostLayer[1] or iY >= domainInfo.getLocalInfo().localGridSize()[1] - domainInfo.getLocalInfo().ghostLayer[1])
                    lattice.defineDynamics(iX, iY, iZ, &ghostLayerDynamics);
                if (iZ < domainInfo.getLocalInfo().ghostLayer[2] or iZ >= domainInfo.getLocalInfo().localGridSize()[2] - domainInfo.getLocalInfo().ghostLayer[2])
                    lattice.defineDynamics(iX, iY, iZ, &ghostLayerDynamics);
            }
        }
    }

    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    cudaDeviceSynchronize();
    std::cout << "Finished!" << std::endl;

    if (isInGPU)
    {
        //std::ofstream myfile;
        //myfile.open("DeltaVals.csv");
        //myfile << "iXf, iYf, iZf, iXs, iYs, iZs, Xw, Yw, Zw, delta, \n";
        auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
        auto slipCellIds = slipDataHandler->getCellIDs();
        auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();

        //std::ofstream myfile5;
        //myfile5.open("AppliedNormals.csv");
        //myfile5 << "Xf, Yf, Zf, Xn, Yn, Zn, \n";

        for (size_t index : slipDataHandler->getCellIDs())
        {

            size_t p[3];
            util::getCellIndices3D(index, lattice.getNy(), lattice.getNz(), p);
            T latticeFluidLocation[3] = {p[0], p[1], p[2]};

            size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

            size_t nNeighbor = 0;
            int normal[3] = {0, 0, 0};

            for (unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop)
            {
                size_t pN[Lattice<T>::d] = {p[0] + Lattice<T>::c(iPop, 0),
                                            p[1] + Lattice<T>::c(iPop, 1),
                                            p[2] + Lattice<T>::c(iPop, 2)};
                size_t nIndex = util::getCellIndex3D(pN[0], pN[1], pN[2], lattice.getNy(), lattice.getNz());
                T latticeNeighborLocation[3] = {pN[0], pN[1], pN[2]};

                if (lattice.getFluidMask()[nIndex] == false)
                {
                    T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};
                    T delta = calculateDelta(lattice, converter, latticeFluidLocation, latticeNeighborLocation, dir, stlReader);

                    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                    {
                        normal[iDim] += Lattice<T>::c(iPop, iDim);
                    }

                    size_t const idxDir = BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxDir(nNeighbor);
                    size_t const idxDelta = BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxDelta(nNeighbor);
                    slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
                    slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

                    slipBoundaryPostProcData[BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxTau()][momentaIndex] =
                        converter.getLatticeRelaxationTime();
                    ++nNeighbor;
                }
            }

            std::vector<T> normalT = calculateNormal(lattice, converter, latticeFluidLocation, stlReader);
            T normalAbs = 0;
            for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
            {
                normalAbs += normal[iDim] * normal[iDim];
            }
            normalAbs = std::sqrt(normalAbs);
            for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
            {

                slipBoundaryPostProcData[BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxNormal() + iDim][momentaIndex] = -normal[iDim] / normalAbs;
            }

            slipBoundaryPostProcData[BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxNumNeighbour()][momentaIndex] = (T)nNeighbor;
        }
        //myfile5.close();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::cout << "Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX)
    {
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                //lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

#ifdef ENABLE_CUDA
    initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
    ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
    initalizeCommDataMultilattice(lattice, commDataHandler);
    NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("/home/ekurban3/Documents/whynotworkoct31/tmp/");
    BlockVTKwriter3D<T> writer("outputVTK_" + std::to_string(domainInfo.localSubDomain));
    //BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    //BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    BlockVTKwriterMultiLattice3D<T, Lattice<T>> vtkmultiwriter("outputMultiVTK", domainInfo);
    BlockLatticePhysVelocity3D<T, Lattice> multiphysVelocityFunctor(lattice, 0, converter);
    BlockLatticePhysPressure3D<T, Lattice> multiphysPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> multifluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    //writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidmaskfunctor);
    vtkmultiwriter.addFunctor(multiphysVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    vtkmultiwriter.addFunctor(multiphysPressureFunctor);
    vtkmultiwriter.addFunctor(multifluidmaskfunctor);

    //std::vector<size_t> limitsvec0={500,1000,550,950,0,500};
    //std::vector<size_t> limitsvec1={0,500,550,950,0,500};

    //std::vector<size_t> limitsvec0 = {0, 300, 100, 200, 0, 100};
    //std::vector<size_t> limitsvec1 = {0, 300, 0, 300, 0, 100};

    //int numero = domainInfo.localSubDomain;
    int writeixstart = 0;
    int writeixend = xLength - 1;
    int writeiystart = 0;
    int writeiyend = yLength - 1;
    int writeizstart = 0;
    int writeizend = zLength;

    size_t startWRITE[3] = {writeixstart, writeiystart, writeizstart};
    size_t endWRITE[3] = {writeixend, writeiyend, writeizend};

    if (outputVTKData)
    {
        vtkmultiwriter.write(0, 0, startWRITE, endWRITE);
    }
    /*if(numero==0){
                if(outputVTKData){
                writer.write(limitsvec0, 0);
        }
    }
    if(numero==1){
        if(outputVTKData){
                writer.write(limitsvec1, 0);
        }
    }*/

    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    unsigned int rampUpSteps = trimTime / 10;
    cout << "lattice velocity is HERE CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC :::::::::::::::::::; " << finalLeftVelocity << endl;
    //unsigned int rampUpSteps = trimTime / 2; //old progressive
    Timer<T> timer(trimTime, lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        //  Uniform inflow version
        //if ((trimStep % converter.getLatticeTime(0.01)) == 0 && trimStep <= rampUpSteps) //old progressive
        if ((trimStep % converter.getLatticeTime(0.0001)) == 0 && trimStep <= rampUpSteps)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity*((T)trimStep/rampUpSteps);
            std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
            int iX = 0;
            for (int iY = 1; iY <= iYTopBorder-1; iY++) {
                for (int iZ = 1; iZ <= iZBackBorder-1; iZ++) {
                    
                    T vel[3] = {uVel, 0, 0};
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
            }
        }
        
        lattice.copyDataToGPU();



        }
        //1/7th rule version
        /*
            if(trimStep==0){        
                lattice.copyDataToCPU();
                T uVel = finalLeftVelocity;
                T resodouble=resolution;
                int iX = 0;
                for (int iY = 0; iY <= iYTopBorder; iY++) {
                    for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
                    T iZo=iZ;
                    
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if(iZ>(44.0/91.0*resodouble) || iZ==44*resolution/91)//needs to be modified when height of the domain is changed -- height of ship*4=height of ABL
                    {
                    T vel[3] = {uVel, 0, 0};
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                    }

                    if(iZ<(44.0/91.0*resodouble))//needs to be modified when height of the domain is changed
                    {
                    T vel[3] = {uVel*pow((iZo/(44.0/91.0*resodouble)),(1.0/7.0)), 0, 0};//needs to be modified when height of the domain is changed -- height of ship*4=height of ABL
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    }

                
                }
            }
            
                lattice.copyDataToGPU();
            }
            */

#ifdef ENABLE_CUDA
        collideAndStreamPostStreamUpdateMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            cout << "step=" << trimStep << " t=" << converter.getPhysTime(trimStep) << endl;
            //lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            /*

            if(numero==0){
                    if(outputVTKData){
                    lattice.copyDataToCPU();
                    writer.write(limitsvec0, trimStep);
               }
            }
            if(numero==1){
                if(outputVTKData){
                    lattice.copyDataToCPU();
                    writer.write(limitsvec1, trimStep);
               }
            }
*/

            if (outputVTKData)
            {
                lattice.copyDataToCPU();
                //writer.write(trimStep);
                vtkmultiwriter.write(trimStep, float(trimStep), startWRITE, endWRITE);
            }
        }
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    int rank = initIPC();
    unsigned int ghostLayerX = 2;
    unsigned int ghostLayerY = 0;
    unsigned int ghostLayerZ = 0;
    unsigned ghostLayer[3] = {ghostLayerX, ghostLayerY, ghostLayerZ};
    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(xLength), (size_t)(yLength), (size_t)(zLength), 0u, 1u, ghostLayer); //cant be an odd number if you have even numbers of gpus
    const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks(), ghostLayer);
    if (rank == 0)
    {
        std::cout << "REF SUBDOMAIN INFO" << std::endl;
        std::cout << refSubDomain;
        std::cout << "Domain Info" << std::endl;
        std::cout << subDomainInfo;
        std::cout << "####" << std::endl;
    }

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl;
    std::cout << "####################################" << std::endl;
    MultipleSteps(commDataHandler, refSubDomain);
    cudaDeviceSynchronize();
    MPI_Finalize();
    return 0;
}
