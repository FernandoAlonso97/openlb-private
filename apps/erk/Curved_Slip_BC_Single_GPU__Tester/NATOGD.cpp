/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "communication/writeFunctorToGPU.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/readWriteFunctorsGPU.h"

#include "../../../eigen-3.4.0/Eigen/Eigen"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;
using namespace Eigen;
/* Simulation Parameters */
T smagoConstant = 0.05;
const T shipLength = 2.77368;
const double simTime = 1;
unsigned int resolution = 100;
//unsigned int xExtraResolution = 2 * resolution;

const T gridSpacing = shipLength / resolution;
const T gridArea = pow(gridSpacing, 2);

const T physInducedVelocity = 40;
const T physVelocity = 1.0;              
const T physDensity = 1.225;               // kg/m^3
const T physKinematicViscosity = 14.61e-6; // m^2/s

/* Output Parameters */
bool outputVTKData = true;

const T vtkWriteInterval = 0.001; // s

int iXStart = 0;
int iXEnd = 3 * resolution;
int iYStart = 0;
int iYEnd = 1.5 * resolution;
int iZStart = 0;
int iZEnd = 0.5 * resolution;

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, UnitConverter<T, Lattice> converter)
{

    T omega = dynamics.getOmega();
    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);

    // auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    // auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXStart, iXStart, iYStart + 1, iYEnd - 1, iZStart + 1, iZEnd - 1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXEnd, iXEnd, iYStart + 1, iYEnd - 1, iZStart + 1, iZEnd - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};

    //new******************************************************************************************************
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    //new******************************************************************************************************

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edge0PN{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edge0PP{};

    //new******************************************************************************************************
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edge0NP{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edge0NN{};
    //new******************************************************************************************************

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    //new******************************************************************************************************
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    //new******************************************************************************************************
    static IniEquilibriumDynamics<T, Lattice> iniEquil{};

    lattice.defineDynamics(iXEnd, iXEnd, iYStart + 1, iYEnd - 1, iZStart + 1, iZEnd - 1, &pressureBCDynamics0P);
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYEnd, iYEnd, iZStart + 1, iZEnd - 1, &slip1P);
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYEnd, iYEnd, iZStart + 1, iZEnd - 1, &slip1N);
    lattice.defineDynamics(iXStart, iXStart, iYStart, iYEnd, iZStart, iZEnd, &iniEquil);
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYStart + 1, iYEnd - 1, iZEnd, iZEnd, &slip2P);
    lattice.defineDynamics(iXEnd, iXEnd, iYEnd, iYEnd, iZStart + 1, iZEnd - 1, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXEnd, iXEnd, iYStart, iYStart, iZStart + 1, iZEnd - 1, &instances::getBounceBack<T, Lattice>());
    lattice.defineDynamics(iXEnd, iXEnd, iYStart + 1, iYEnd - 1, iZStart, iZStart, &instances::getBounceBack<T, Lattice>()); //1NP
    lattice.defineDynamics(iXEnd, iXEnd, iYStart + 1, iYEnd - 1, iZEnd, iZEnd, &instances::getBounceBack<T, Lattice>());     //1PP
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYEnd, iYEnd, iZStart, iZStart, &edge0PN);                                //0PN
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYEnd, iYEnd, iZEnd, iZEnd, &edge0PP);                                    //0PP
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYStart, iYStart, iZStart, iZStart, &edge0NN);                            //0NN
    lattice.defineDynamics(iXStart + 1, iXEnd - 1, iYStart, iYStart, iZEnd, iZEnd, &edge0NP);                                //0NP
    lattice.defineDynamics(iXEnd, iXEnd, iYEnd, iYEnd, iZStart, iZStart, &cornerPPN);                                        //PPN
    lattice.defineDynamics(iXEnd, iXEnd, iYEnd, iYEnd, iZEnd, iZEnd, &cornerPPP);                                            //PPP
    lattice.defineDynamics(iXEnd, iXEnd, iYStart, iYStart, iZStart, iZStart, &cornerPNN);                                    //PNN
    lattice.defineDynamics(iXEnd, iXEnd, iYStart, iYStart, iZEnd, iZEnd, &cornerPNP);                                        //PNP
}

std::vector<int> CountFluidCellsAroundBody(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter)
{

    std::vector<int> counter{0, 0, 0, 0, 0, 0, 0};

    for (int iX = 1; iX <= iXEnd - 1; ++iX)
    {
        for (int iY = 1; iY <= iYEnd - 1; ++iY)
        {
            for (int iZ = 1; iZ <= iZEnd - 1; ++iZ)
            {

                bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                if (isFluid == false)
                {

                    int cell0P[3] = {iX + 1, iY, iZ};
                    int cell0N[3] = {iX - 1, iY, iZ};
                    int cell1P[3] = {iX, iY + 1, iZ};
                    int cell1N[3] = {iX, iY - 1, iZ};
                    int cell2P[3] = {iX, iY, iZ + 1};
                    int cell2N[3] = {iX, iY, iZ - 1};

                    bool isCell0PFluid = lattice.getMaskEntry(cell0P[0], cell0P[1], cell0P[2]);
                    bool isCell0NFluid = lattice.getMaskEntry(cell0N[0], cell0N[1], cell0N[2]);
                    bool isCell1PFluid = lattice.getMaskEntry(cell1P[0], cell1P[1], cell1P[2]);
                    bool isCell1NFluid = lattice.getMaskEntry(cell1N[0], cell1N[1], cell1N[2]);
                    bool isCell2PFluid = lattice.getMaskEntry(cell2P[0], cell2P[1], cell2P[2]);
                    bool isCell2NFluid = lattice.getMaskEntry(cell2N[0], cell2N[1], cell2N[2]);

                    if (isCell0PFluid)
                    {
                        counter[0]++;
                    }
                    if (isCell0NFluid)
                    {
                        counter[1]++;
                    }
                    if (isCell1PFluid)
                    {
                        counter[2]++;
                    }
                    if (isCell1NFluid)
                    {
                        counter[3]++;
                    }
                    if (isCell2PFluid)
                    {
                        counter[4]++;
                    }
                    if (isCell2NFluid)
                    {
                        counter[5]++;
                    }
                }
            }
        }
    }
    counter[6] = counter[0] + counter[1] + counter[2] + counter[3] + counter[4] + counter[5];
    return counter;
}

std::vector<T> calculateDelta(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], T neighborCell[3], T dir[3], STLreader<double> &stlReader)
{ //std::ofstream &myfile,
    typedef double T;
    auto tree = stlReader.getTree();
    T physFluidLocation[3] = {converter.getPhysLength(slipCell[0]), converter.getPhysLength(slipCell[1]), converter.getPhysLength(slipCell[2])};
    T fracSpacing = 100;
    T dirMag = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));
    T dotDel = (converter.getPhysDeltaX() * dirMag) / fracSpacing;
    T physXpoint = converter.getPhysLength(slipCell[0]); //physFluidLocation[0];
    T physYpoint = converter.getPhysLength(slipCell[1]); //physFluidLocation[1];
    T physZpoint = converter.getPhysLength(slipCell[2]); //physFluidLocation[2];
    T Xw;
    T Yw;
    T Zw;
    T physPoint[3];
    for (int i = 1; i <= fracSpacing; ++i)
    {
        physPoint[0] = physXpoint;
        physPoint[1] = physYpoint,
        physPoint[2] = physZpoint;
        std::vector<T> tmpLoc(physPoint, physPoint + 3);
        Xw = physXpoint;
        Yw = physYpoint;
        Zw = physZpoint;
        physXpoint += dir[0] * dotDel;
        physYpoint += dir[1] * dotDel;
        physZpoint += dir[2] * dotDel;
        bool isInside[1];
        T location[3] = {physXpoint, physYpoint, physZpoint};
        stlReader(isInside, location);
        if (isInside[0])
        {
            break;
        }
    }
    T physWallLocation[3] = {Xw, Yw, Zw};
    T physSolidLocation[3] = {converter.getPhysLength(neighborCell[0]), converter.getPhysLength(neighborCell[1]), converter.getPhysLength(neighborCell[2])};
    T numMagnitude = sqrt(pow((physFluidLocation[0] - physWallLocation[0]), 2) +
                          pow((physFluidLocation[1] - physWallLocation[1]), 2) +
                          pow((physFluidLocation[2] - physWallLocation[2]), 2));
    T denMagnitude = sqrt(pow((physFluidLocation[0] - physSolidLocation[0]), 2) +
                          pow((physFluidLocation[1] - physSolidLocation[1]), 2) +
                          pow((physFluidLocation[2] - physSolidLocation[2]), 2));

    T delta = numMagnitude / denMagnitude;

    T latticeWallLocation[3] = {Xw / converter.getPhysDeltaX(), Yw / converter.getPhysDeltaX(), Zw / converter.getPhysDeltaX()};
    std::vector<T> output = {latticeWallLocation[0], latticeWallLocation[1], latticeWallLocation[2], delta};

    return output;
}

std::vector<double> checkDirection(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, STLreader<double> &stlReader)
{
    typedef double T;
    T spacing = 1.0 / 100.0; //100.0;
    T fracSpacing = 100;
    bool correctDirection = true;
    for (int i = 1; i <= fracSpacing; ++i)
    {
        vecX += spacing * normalEigenvector[0];
        vecY += spacing * normalEigenvector[1];
        vecZ += spacing * normalEigenvector[2];

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if (isInsideSTL[0])
        {
            correctDirection = false;
            break;
        }
    }
    if (correctDirection == false)
    {
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalEigenvector[iDim] = -normalEigenvector[iDim];
        }
    }
    return normalEigenvector;
}

std::vector<double> checkOrientation(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, std::vector<double> otherEigenvector, STLreader<double> &stlReader)
{
    typedef double T;
    T fracSpacing = 100;
    T dirMag = sqrt(pow(normalEigenvector[0], 2) + pow(normalEigenvector[1], 2) + pow(normalEigenvector[2], 2));
    T spacing = (2.0 * converter.getPhysDeltaX() * dirMag) / fracSpacing;

    bool correctOrientation = false;
    for (int i = 1; i <= fracSpacing; ++i)
    {
        vecX += -spacing * normalEigenvector[0];
        vecY += -spacing * normalEigenvector[1];
        vecZ += -spacing * normalEigenvector[2];

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if (isInsideSTL[0])
        {
            correctOrientation = true;
            break;
        }
    }

    if (correctOrientation == false)
    {
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalEigenvector[iDim] = otherEigenvector[iDim];
        }
    }

    return normalEigenvector;
}

std::vector<double> normalizeNormals(std::vector<double> normalEigenvector)
{
    T normalAbs = 0;
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
        normalAbs += normalEigenvector[iDim] * normalEigenvector[iDim];
    }
    normalAbs = std::sqrt(normalAbs);
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
        normalEigenvector[iDim] = normalEigenvector[iDim] / normalAbs;
    }
    return normalEigenvector;
}

std::vector<T> calculateNormalPCA(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], STLreader<double> &stlReader)
{
    typedef double T;
    T fracSpacing = 100;
    T pointX = slipCell[0];
    T pointY = slipCell[1];
    T pointZ = slipCell[2];
    T normal[3] = {0, 0, 0};
    int numPoints = 0;
    T maxX = 0;
    T maxY = 0;
    T maxZ = 0;
    int p = 0;
    T centroidX = 0;
    T centroidY = 0;
    T centroidZ = 0;
    std::vector<T> pointsX(0);
    std::vector<T> pointsY(0);
    std::vector<T> pointsZ(0);
    std::vector<std::vector<T>> points;
    T covarianceMatrix[3][3];

    for (int iPop = 1; iPop < Lattice<T>::q; ++iPop)
    {
        T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};
        T dirMag = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));
        T dotDel = (2.0 * converter.getPhysDeltaX() * dirMag) / fracSpacing; //(2.0*sqrt(2.0)*converter.getPhysDeltaX()*dirMag)/fracSpacing;
        T physXpoint = converter.getPhysLength(slipCell[0]);                 //physFluidLocation[0];
        T physYpoint = converter.getPhysLength(slipCell[1]);                 //physFluidLocation[1];
        T physZpoint = converter.getPhysLength(slipCell[2]);                 //physFluidLocation[2];
        T Xw = physXpoint;
        T Yw = physYpoint;
        T Zw = physZpoint;
        for (int i = 1; i <= fracSpacing; ++i)
        {
            Xw += dir[0] * dotDel;
            Yw += dir[1] * dotDel;
            Zw += dir[2] * dotDel;
            bool isInside[1];
            T location[3] = {Xw, Yw, Zw};
            stlReader(isInside, location);
            if (isInside[0])
            {
                ////////////eigenvalue method///////////////
                centroidX += Xw;
                centroidY += Yw;
                centroidZ += Zw;

                pointsX.push_back(Xw);
                pointsY.push_back(Yw);
                pointsZ.push_back(Zw);
                //////////////
                numPoints++;
                normal[0] += (Xw - physXpoint);
                normal[1] += (Yw - physYpoint);
                normal[2] += (Zw - physZpoint);

                break;
            }
        }
    }

    T centroid[3] = {centroidX / numPoints, centroidY / numPoints, centroidZ / numPoints};
    points.resize(numPoints, std::vector<T>(3));
    for (unsigned int i = 0; i < numPoints; ++i)
    {
        points[i][0] = pointsX[i];
        points[i][1] = pointsY[i];
        points[i][2] = pointsZ[i];
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            covarianceMatrix[i][j] = 0.0;
            for (int k = 0; k < points.size(); k++)
            {
                covarianceMatrix[i][j] += (centroid[i] - points[k][i]) * (centroid[j] - points[k][j]);
                covarianceMatrix[i][j] /= points.size(); // - 1;
            }
        }
    }
    Matrix3d COV;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            COV(i, j) = covarianceMatrix[i][j];
        }
    }

    EigenSolver<MatrixXd> solution(COV);
    VectorXd D = solution.eigenvalues().real();
    MatrixXd V = solution.eigenvectors().real();

    T minEig = D.minCoeff();
    T maxEig = D.maxCoeff();
    std::vector<double> eigenvalues;
    std::vector<std::vector<T>> eigenVectorMatrix;
    eigenvalues.resize(D.size());
    eigenVectorMatrix.resize(D.size(), std::vector<T>(3));
    int index;
    int maxIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        eigenvalues[i] = D(i);
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            eigenVectorMatrix[i][j] = V(i, j);
        }
    }
    bool found = false;

    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == minEig)
        {
            index = i;
            found = true;
            break;
        }
    }

    bool foundMax = false;

    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == maxEig)
        {
            maxIndex = i;
            foundMax = true;
            break;
        }
    }

    std::vector<T> normalEigenvector;
    normalEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        normalEigenvector[i] = eigenVectorMatrix[i][index];
    }
    int otherIndex1;
    int otherIndex2;
    if (index == 0)
    {
        otherIndex1 = 1;
        otherIndex2 = 2;
    }
    if (index == 1)
    {
        otherIndex1 = 0;
        otherIndex2 = 2;
    }
    if (index == 2)
    {
        otherIndex1 = 0;
        otherIndex2 = 1;
    }
    int midIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        if (i != index && i != maxIndex)
        {
            midIndex = i;
        }
    }

    std::vector<T> otherEigenvector0;
    otherEigenvector0.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector0[i] = eigenVectorMatrix[i][index];
    }

    std::vector<T> otherEigenvector1;
    otherEigenvector1.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector1[i] = eigenVectorMatrix[i][otherIndex1];
    }

    std::vector<T> otherEigenvector2;
    otherEigenvector2.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector2[i] = eigenVectorMatrix[i][otherIndex2];
    }

    std::vector<T> maxEigenvector;
    maxEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        maxEigenvector[i] = eigenVectorMatrix[i][maxIndex];
    }

    std::vector<T> midEigenvector;
    midEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        midEigenvector[i] = eigenVectorMatrix[i][midIndex];
    }

    //////////////normalize normals//////////////////////////
    normalEigenvector = normalizeNormals(normalEigenvector);
    otherEigenvector1 = normalizeNormals(otherEigenvector1);
    otherEigenvector2 = normalizeNormals(otherEigenvector2);
    maxEigenvector = normalizeNormals(maxEigenvector);
    midEigenvector = normalizeNormals(midEigenvector);
    ///////////////////////////////////////////////////////

    T vecX = converter.getPhysLength(slipCell[0]);
    T vecY = converter.getPhysLength(slipCell[1]);
    T vecZ = converter.getPhysLength(slipCell[2]);
    T spacing = 1.0 / 100.0;

    ///////Series of direction and orientation checks//////////////////////////
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, midEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, maxEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);

    // myfile << normalEigenvector[0] << "," << normalEigenvector[1] << "," << normalEigenvector[2] << ",";
    // myfile << otherEigenvector1[0] << "," << otherEigenvector1[1] << "," << otherEigenvector1[2] << ",";
    // myfile << otherEigenvector2[0] << "," << otherEigenvector2[1] << "," << otherEigenvector2[2] << "\n";

    T output0 = normalEigenvector[0];
    T output1 = normalEigenvector[1];
    T output2 = normalEigenvector[2];

    std::vector<T> output = {output0, output1, output2};
    return output;
}

/*
double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}
*/

//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
bool check75(int iX, int iY, int iZ, Index3D &localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

//template <template <typename> class Memory>
void MultipleSteps()
{

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), shipLength, physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(iXEnd + 1, iYEnd + 1, iZEnd + 1, &bulkDynamics);

    std::cout << "Define boundaries.... " << std::endl;
    defineBoundaries(lattice, bulkDynamics, converter);

    ////////////////////////////CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC/////////////////////////
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};
    static NoDynamics<T, Lattice> noDynamics;
/*used to be::::::::::    
    static PostProcessingDynamics<T, Lattice, CurvedSlipBoundaryProcessor3D<T, Lattice>> slipBoundary;
    BounceBackMovingBoundaryBoundaryProcessor3D
 */   
    static PostProcessingDynamics<T, Lattice, BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>> slipBoundary;

    STLreader<double> stlReader("1_50_scale_carderock_sfs2_no_out_surface_long_fluid_15_width_4totlength_1_ship_2.STL", converter.getConversionFactorLength() / 16, 1);
    stlReader.print();

    for (int iX = 0; iX <= iXEnd; ++iX)
    {
        for (int iY = 0; iY <= iYEnd; ++iY)
        {
            for (int iZ = 0; iZ <= iZEnd; ++iZ)
            {
                double iXPhys = converter.getPhysLength(iX);
                double iYPhys = converter.getPhysLength(iY);
                double iZPhys = converter.getPhysLength(iZ);
                //T location[3] = {(T)iX, (T)iY, (T)iZ};
                double location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);
                if (isInside[0])
                {
                    lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>());
                }
            }
        }
    }

    for (unsigned iX = iXStart + 1; iX <= iXEnd - 1; iX++)
    {
        for (unsigned iY = iYStart + 1; iY <= iYEnd - 1; iY++)
        {
            Index3D localIndex;
            double iXPhys = converter.getPhysLength(iX);
            double iYPhys = converter.getPhysLength(iY);
            double iZPhys = converter.getPhysLength(iZStart);
            double location[3] = {iXPhys, iYPhys, iZPhys};
            bool isInside[1];
            stlReader(isInside, location);
            if (isInside[0])
            {
            }
            else
            {
                lattice.defineDynamics(iX, iY, iZStart, &slip2N);
                //lattice.defineDynamics(iX, iY, iZStart, &instances::getBounceBack<T, Lattice>());
            }
        }
    }
    ////here
    std::ofstream locBoundary("locboundary.csv");
    locBoundary << "Index1, Index2, Index3\n";
    ////////////////////////////////Implement Slip BC///////////////////////////////////////////////////////////////
    //int NslipBoundaries = 0;
    for (int iX = 1; iX <= iXEnd - 1; ++iX)
    {
        for (int iY = 1; iY <= iYEnd - 1; ++iY)
        {
            for (int iZ = 3; iZ <= iZEnd - 1; ++iZ)
            {

                bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                if (isFluid == false)
                {
                    for (unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop)
                    {
                        const int neighbor[3] = {iX + Lattice<T>::c(iPop, 0), iY + Lattice<T>::c(iPop, 1), iZ + Lattice<T>::c(iPop, 2)};
                        bool isNfluid = lattice.getMaskEntry(neighbor[0], neighbor[1], neighbor[2]);
                        if (neighbor[2] != 0)
                        {
                            if (isNfluid == true)
                            {
                                lattice.defineDynamics(neighbor[0], neighbor[1], neighbor[2], &slipBoundary);
                                locBoundary<<neighbor[0]<<" "<<neighbor[1]<<" "<<neighbor[2]<<"\n";
                                //if (neighbor[0] * converter.getPhysDeltaX() < 3.5)
                                //{
                                //    cout << "Neighbor iX= " << neighbor[0] * converter.getPhysDeltaX() << "  iY=" << neighbor[1] * converter.getPhysDeltaX() << "  iZ=" << neighbor[2] * converter.getPhysDeltaX() << endl;
                                //}
                                //NslipBoundaries++;
                            }
                        }
                    }
                }
            }
        }
    }

    locBoundary.close();

    ///ends here

    ///////////////////////CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC///////////////////////////////
    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;
    ///here 2
    std::cout << "CCC TEST CCC  Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXEnd; ++iX)
    {
        for (int iY = 0; iY <= iYEnd; ++iY)
        {
            for (int iZ = 0; iZ <= iZEnd; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }
    ///////////////////////////CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC//////////////////////
///here 2

    auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
    auto slipCellIds = slipDataHandler->getCellIDs();
    auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();
    auto tree = stlReader.getTree();

    int NSlipLoops = 0;

    std::ofstream normalClassic("normalClassic.csv");
    normalClassic << "Index1, Index2, Index3, Normal1, Normal2, Normal3\n";

    for (size_t index : slipDataHandler->getCellIDs())
    {
        NSlipLoops++;
        size_t p[3];
        util::getCellIndices3D(index, lattice.getNy(), lattice.getNz(), p);
        T latticeFluidLocation[3] = {p[0], p[1], p[2]};
        //cout<<"Location 1 x="<<p[0]<<" y="<<p[1]<<" z="<<p[2]<<endl;

////buraaaa
        size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

        size_t nNeighbor = 0;
        T normal[3] = {0, 0, 0};

        for (unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop)
        {
            size_t pN[Lattice<T>::d] = {p[0] + Lattice<T>::c(iPop, 0),
                                        p[1] + Lattice<T>::c(iPop, 1),
                                        p[2] + Lattice<T>::c(iPop, 2)};
            size_t nIndex = util::getCellIndex3D(pN[0], pN[1], pN[2], lattice.getNy(), lattice.getNz());
            T latticeNeighborLocation[3] = {pN[0], pN[1], pN[2]};

            if (lattice.getFluidMask()[nIndex] == false)
            {
                T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};

                std::vector<T> deltaData = calculateDelta(lattice, converter, latticeFluidLocation, latticeNeighborLocation, dir, stlReader); //myfile,
                T delta = deltaData[3];
                T latticeWallLocation[3] = {deltaData[0], deltaData[1], deltaData[2]};

                for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                {
                    normal[iDim] += Lattice<T>::c(iPop, iDim);
                }
                
                //cout<<"Location 2 x="<<p[0]<<" y="<<p[1]<<" z="<<p[2]<<endl;
                //cout<<"Normal x="<< normal[0]<<" normal y="<< normal[1]<<" normal z="<< normal[2]<<endl;

                size_t const idxDir = BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxDir(nNeighbor);
                size_t const idxDelta = BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxDelta(nNeighbor);
                slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
                slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

                slipBoundaryPostProcData[BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxTau()][momentaIndex] =
                    converter.getLatticeRelaxationTime();
                ++nNeighbor;
            }
        }

        std::vector<T> normalPCA = calculateNormalPCA(lattice, converter, latticeFluidLocation, stlReader);
        T normalAbs = 0;

        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalAbs += normal[iDim] * normal[iDim];
        }
        normalAbs = std::sqrt(normalAbs);
         cout<<"Location  x="<<p[0]<<" y="<<p[1]<<" z="<<p[2]<<endl;
         cout<<"NormalPCAin x="<< normalPCA[0]<<" normal y="<< normalPCA[1]<<" normal z="<< normalPCA[2]<<endl;

         normalClassic<<p[0]<<" "<<p[1]<<" "<<p[2]<<" "<<normalPCA[0]<<" "<<normalPCA[1]<<" "<<normalPCA[2]<<"\n";

        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {

            //slipBoundaryPostProcData[BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxNormal() + iDim][momentaIndex] = -normal[iDim] / normalAbs;
            slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNormal()+iDim][momentaIndex] = normalPCA[iDim];

        }

        slipBoundaryPostProcData[BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::idxNumNeighbour()][momentaIndex] = (T)nNeighbor;
    }
    normalClassic.close();
   ///ends here





///TRIALS START HERE
    std::ofstream normalNeo("normalNeo.csv");
    normalNeo << "Index1, Index2, Index3, Normal1, Normal2, Normal3\n";
//int iXStart = 0;
int iXRightBorder = 3 * resolution;
//int iYStart = 0;
int iYTopBorder = 1.5 * resolution;
//int iZStart = 0;
int iZBackBorder = 0.5 * resolution;
stlT shipOffsetX=0;
stlT shipOffsetY=0;
stlT shipOffsetZ=0;

STLreader<stlT> stlReaderTrial("1_50_scale_carderock_sfs2_no_out_surface_long_fluid_15_width_4totlength_1_ship_2.STL", converter.getConversionFactorLength()*0.1, 1.0);
  stlReader.print();

  std::cout << "Loaded! Assigning bounceback cells.." << std::endl;




  std::set<size_t> outsideNeighborsSet;
  std::vector<std::vector<int>> outsideNeighborsReplacementPops;
  int outsideNeighborCounter=0;
  for (int iX = 0; iX <= iXRightBorder; iX++)
    for (int iY = 0; iY <= iYTopBorder; iY++)
      for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
        stlT iXPhys = converter.getPhysLength((stlT)iX-shipOffsetX);
        stlT iYPhys = converter.getPhysLength((stlT)iY-shipOffsetY);
        stlT iZPhys = converter.getPhysLength((stlT)iZ-shipOffsetZ);
        stlT location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];
        stlReaderTrial(isInside, location);
        if (isInside[0]) {
          lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>());

          bool allNeighborsInside = true;
          std::vector<int> neighborsNotInside;
          for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
            stlT iXNeighborPhys = converter.getPhysLength((stlT)iX-shipOffsetX+(stlT)Lattice<T>::c(iPop, 0));
            stlT iYNeighborPhys = converter.getPhysLength((stlT)iY-shipOffsetY+(stlT)Lattice<T>::c(iPop, 1));
            stlT iZNeighborPhys = converter.getPhysLength((stlT)iZ-shipOffsetZ+(stlT)Lattice<T>::c(iPop, 2));
            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
            bool neighborInside[1];
            stlReaderTrial(neighborInside, neighborLocation);
            allNeighborsInside = allNeighborsInside && neighborInside[0];
            if (!neighborInside[0])
              neighborsNotInside.push_back(iPop);
            // printf("iX = %d, iY = %d, iZ = %d, neighbor %d inside: %d\n", iX, iY, iZ, iPop, neighborInside[0]);
          }

          if (!allNeighborsInside) {


            for (int outsideNeighbor : neighborsNotInside) {

              size_t outsideNeighborCellIndex = util::getCellIndex3D(iX + Lattice<T>::c(outsideNeighbor, 0), iY + Lattice<T>::c(outsideNeighbor, 1), iZ + Lattice<T>::c(outsideNeighbor, 2), iYTopBorder+1, iZBackBorder+1);
              auto result = outsideNeighborsSet.insert(outsideNeighborCellIndex);
              if (result.second) {
                
                olb::Vector<stlT,3> neighborPoint(converter.getPhysLength((stlT)iX-shipOffsetX+(stlT)Lattice<T>::c(outsideNeighbor, 0)),
                                               converter.getPhysLength((stlT)iY-shipOffsetY+(stlT)Lattice<T>::c(outsideNeighbor, 1)),
                                               converter.getPhysLength((stlT)iZ-shipOffsetZ+(stlT)Lattice<T>::c(outsideNeighbor, 2)));
                olb::Vector<stlT,3U> normal = stlReaderTrial.evalSurfaceNormal(neighborPoint);
                outsideNeighborCounter=outsideNeighborCounter+1;
                normalNeo<<iX + Lattice<T>::c(outsideNeighbor, 0)<<" "<<iY + Lattice<T>::c(outsideNeighbor, 1)<<" "<<iZ + Lattice<T>::c(outsideNeighbor, 2)<<" "<<normal[0]<<" "<<normal[1]<<" "<<normal[2]<<"\n";

              }
            }
          }
          
        }

      }



  for (size_t outsideNeighbor : outsideNeighborsSet) {
    size_t indices[3];
    util::getCellIndices3D(outsideNeighbor, iYTopBorder+1, iZBackBorder+1, indices);
    std::vector<int> replacementPops;
    for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
      stlT iXNeighborPhys = converter.getPhysLength((stlT)indices[0]-shipOffsetX+(stlT)Lattice<T>::c(iPop,0));
      stlT iYNeighborPhys = converter.getPhysLength((stlT)indices[1]-shipOffsetY+(stlT)Lattice<T>::c(iPop,1));
      stlT iZNeighborPhys = converter.getPhysLength((stlT)indices[2]-shipOffsetZ+(stlT)Lattice<T>::c(iPop,2));
      stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
      bool neighborInside[1];
      stlReaderTrial(neighborInside, neighborLocation);
      if (neighborInside[0]) {
        replacementPops.push_back(iPop);
      }
    }
    outsideNeighborsReplacementPops.push_back(replacementPops);
  }


  for (int boundaryPt = 0; boundaryPt < outsideNeighborsSet.size(); boundaryPt++) {
    auto it = outsideNeighborsSet.begin();
    std::advance(it, boundaryPt);
    size_t outsideNeighbor = *it;
    size_t indices[3];
    util::getCellIndices3D(outsideNeighbor, iYTopBorder+1, iZBackBorder+1, indices);
    std::vector<int> replacementPops = outsideNeighborsReplacementPops.at(boundaryPt);
  }



    normalNeo.close();

////TRIALS END HERE


    ////////////////////////////////CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC////////////////////////////

    std::cout << "Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXEnd; ++iX)
    {
        for (int iY = 0; iY <= iYEnd; ++iY)
        {
            for (int iZ = 0; iZ <= iZEnd; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("/home/ekurban3/Documents/temporaryoct3/tmp/");
    BlockVTKwriter3D<T> writer("outputVTK_");
    //BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    //BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    //writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidmaskfunctor);

    if (outputVTKData)
    {
        writer.write(0);
    }

    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    cout << "lattice velocity is HERE CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC :::::::::::::::::::; " << finalLeftVelocity << endl;
    unsigned int rampUpSteps = trimTime / 8;
    Timer<T> timer(trimTime, lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        //  Uniform inflow version
        if ((trimStep % converter.getLatticeTime(0.01)) == 0 && trimStep <= rampUpSteps)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity * ((T)trimStep / rampUpSteps);

            int iX = 0;
            for (int iY = 0; iY <= iYEnd; iY++)
            {
                for (int iZ = 0; iZ <= iZEnd; iZ++)
                {

                    T vel[3] = {uVel, 0, 0}; //needs to be modified when height of the domain is changed
                    lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                    lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                }
            }

            lattice.copyDataToGPU();
            //Uniform inflow version
        }

        //1/7th rule version
        /*
            if(trimStep==0){        
                lattice.copyDataToCPU();
                T uVel = finalLeftVelocity;
                T resodouble=resolution;
                int iX = 0;
                for (int iY = 0; iY <= iYEnd; iY++) {
                    for (int iZ = 0; iZ <= iZEnd; iZ++) {
                    T iZo=iZ;
                    
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if(iZ>(44.0/91.0*resodouble) || iZ==44*resolution/91)//needs to be modified when height of the domain is changed -- height of ship*4=height of ABL
                    {
                    T vel[3] = {uVel, 0, 0};
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                    }

                    if(iZ<(44.0/91.0*resodouble))//needs to be modified when height of the domain is changed
                    {
                    T vel[3] = {uVel*pow((iZo/(44.0/91.0*resodouble)),(1.0/7.0)), 0, 0};//needs to be modified when height of the domain is changed -- height of ship*4=height of ABL
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    }

                
                }
            }
            
                lattice.copyDataToGPU();
            }
            */

        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            /*

            if(numero==0){
                    if(outputVTKData){
                    lattice.copyDataToCPU();
                    writer.write(limitsvec0, trimStep);
               }
            }
            if(numero==1){
                if(outputVTKData){
                    lattice.copyDataToCPU();
                    writer.write(limitsvec1, trimStep);
               }
            }
*/

            if (outputVTKData)
            {
                lattice.copyDataToCPU();
                writer.write(trimStep);
            }
        }
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    std::clock_t start;
    double duration;
    start = std::clock();
    MultipleSteps();
    duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
    std::cout << "Time: " << duration << '\n';
    return 0;
}
