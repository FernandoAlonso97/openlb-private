/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef float T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant =0.05;
const T shipLength = 150.02256;
const double simTime = 150;
unsigned int resolution = 720;

const T gridSpacing = shipLength / resolution;
const T gridArea = pow(gridSpacing, 2);

const T physInducedVelocity = 55; 
const T physVelocity =20.5777778; 
const T physDensity = 1.225;               // kg/m^3
const T physKinematicViscosity = 14.61e-6; // m^2/s

const int xoffset = 120/gridSpacing + 0.5 * shipLength / gridSpacing;
const int yoffset = (shipLength) / gridSpacing;
const int zoffset=9.28/gridSpacing;

const int xoutputoffset=90/gridSpacing+1;
const int youtputoffsetport=50/gridSpacing+1;
const int youtputoffsetstarboard=17/gridSpacing+1;
const int zoutputoffsetsky=24/gridSpacing+1;
const int zoutputoffsetsea=7/gridSpacing+1;

/* Output Parameters */
bool outputVTKData = true;
bool outputRotorData = false;
bool outputDebugData = false;

const T vtkWriteInterval = 0.1; // s

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    T omega = dynamics.getOmega();

    // auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    // auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    /*
    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);
    */

    auto *pressureMomenta1N = new BasicDirichletBM<T, Lattice, PressureBM, 1, -1, 0>;
    auto *pressurePostProcessor1N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 1, -1>(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 1, -1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor1N)>::type::PostProcessorType> pressureBCDynamics1N(omega, *pressureMomenta1N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);
    /*
    auto *pressureMomenta2P = new BasicDirichletBM<T, Lattice, PressureBM, 2, 1, 0>;
    auto *pressurePostProcessor2P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 2, 1>(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYBottomBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);
*/

    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, 0, 0>> slip0N{};
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 0>> slip0P{};

    //new******************************************************************************************************
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    //new******************************************************************************************************

    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edge0PN{};
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edge0PP{};

    //new******************************************************************************************************
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edge0NP{};
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edge0NN{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, 0, 1>> edge1PN{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, 0, -1>> edge1NN{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edge1PP{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edge1NP{};

    //new******************************************************************************************************

    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    //new******************************************************************************************************
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    //new******************************************************************************************************
    static IniEquilibriumDynamics<T, Lattice> iniEquil{};
    static IniEquilibriumDynamics<T, Lattice> iniEquil2{};

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;

            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics0P);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;

            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics1N);
        }
    }

    /*for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1P);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);

            //if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
            //  lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);
        }
    }*/

    for (unsigned iX = iXLeftBorder; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
        }
    }

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil2);
        }
    }

    /*AUG 8
    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip0N);
        }
    }
    */
    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            Index3D localIndex;
            //if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
            //  lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2P);
        }
    }

    //     OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
    // createInterpBoundaryCondition3D<T, Lattice,
    //                                 ForcedLudwigSmagorinskyBGKdynamics>(lattice);
    /*
    for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
    {
        Index3D localIndex;

        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        //  if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex))   //????????????????
        //     boundaryCondition->addExternalVelocityEdge2NP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //????????????????
        //  if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))   //????????????????
        //     boundaryCondition->addExternalVelocityEdge2NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //????????????????
    }
*/
    for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++)
    {
        Index3D localIndex;

        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
    }

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        Index3D localIndex;

        if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());

        if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        /* AUG 8-2
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NP); //1NP
        if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PP); //1PP
        AUG 8-2*/

        /*AUG 8
        if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NN); //1NN

        if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PN); //1PN
        */
        //AUG 8

        // if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
        //     boundaryCondition->addExternalVelocityEdge1PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);  //????????????????????/
        // if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
        //     boundaryCondition->addExternalVelocityEdge1NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //??????????????????????
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //0PN
        if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //0PP
        //if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
        //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NN); //0NN
        //if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
        //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NP); //0NP
    }

    Index3D localIndex;
    // if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNPN(localIndex[0], localIndex[1], localIndex[2], omega); //***
    // if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNPP(localIndex[0], localIndex[1], localIndex[2], omega);  //**

    // if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNNN(localIndex[0], localIndex[1], localIndex[2], omega); //***
    // if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNNP(localIndex[0], localIndex[1], localIndex[2], omega);  //**

    //if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN); //PPN
    //if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP); //PPP

    //**
    //if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN); //PNN
    //if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP); //PNP

    STLreader<T> stlReader("gd-nato_baseline_watertight_inches_2018_12_04_modified_no_below_water_parts_FULLSCALE_originatx2y2z2ORIGINCORR.stl", converter.getConversionFactorLength() / 10, 1, 1, true);
    //TEMPORARILY NOT USED
    //STLreader<T> stlReader("gd-nato_baseline_watertight_inches_2018_12_04_modified_no_below_water_parts_1over50_originatx2y2z2.stl", converter.getConversionFactorLength() / 10, 1);

    //stl name suggests 4totlength_1_ship_2: 4 ship length of fluid domain 1 ship length away from inlet, ship, 2 ship length away from the outlet
    stlReader.print();

    std::cout << "Location of output origin is iX=" << xoffset << " iY= " << yoffset << " iZ= " << zoffset << std::endl;

    for (int iX = 0; iX <= iXRightBorder; ++iX)
    {
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T iXPhys = converter.getPhysLength(iX - xoffset);
                T iYPhys = converter.getPhysLength(iY - yoffset);
                T iZPhys = converter.getPhysLength(iZ - zoffset);
                //T location[3] = {(T)iX, (T)iY, (T)iZ};
                T location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);
                if (isInside[0])
                {
                    if (domainInfo.isLocal(iX, iY, iZ, localIndex))
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                }
            }
        }
    }

    std::vector<int> startpoint = {xoffset, yoffset - youtputoffsetport, zoffset - zoutputoffsetsea};
    std::vector<int> endpoint = {xoffset + xoutputoffset, yoffset + youtputoffsetstarboard, zoffset + zoutputoffsetsky};

    T aftdisttoorig = (endpoint[0] - xoffset) * gridSpacing;
    T portdisttoorig = (yoffset - startpoint[1]) * gridSpacing;
    T stardisttoorig = (endpoint[1] - yoffset) * gridSpacing;
    T seadisttoorig = (zoffset - startpoint[2]) * gridSpacing;
    T skydisttoorig = (endpoint[2] - zoffset) * gridSpacing;

    std::cout << std::endl;
    std::cout << " xoutput offset aft " << xoutputoffset << " youtput offset port " << youtputoffsetport << std::endl
              << " youtput offset starboard  " << youtputoffsetstarboard << " zouput offset sea " << zoutputoffsetsea << std::endl
              << "zouput offset sky " << zoutputoffsetsky << std::endl;
    std::cout << std::endl;

    std::cout << "Aft distance from origin" << aftdisttoorig << std::endl;
    std::cout << "Port distance from origin" << portdisttoorig << std::endl;
    std::cout << "Starboard distance from origin" << stardisttoorig << std::endl;
    std::cout << "Sea distance from origin" << seadisttoorig << std::endl;
    std::cout << "Sky distance from origin" << skydisttoorig << std::endl;
    std::cout << "Flow domain x-length" << aftdisttoorig << std::endl;
    T ylenn = (endpoint[1] - startpoint[1]) * gridSpacing;
    T zlenn = (endpoint[2] - startpoint[2]) * gridSpacing;
    std::cout << "Flow domain y-length" << ylenn << std::endl;
    std::cout << "Flow domain z-length" << zlenn << std::endl;

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            Index3D localIndex;
            T iXPhys = converter.getPhysLength(iX);
            T iYPhys = converter.getPhysLength(iY);
            T iZPhys = converter.getPhysLength(iZFrontBorder);
            T location[3] = {iXPhys, iYPhys, iZPhys};
            bool isInside[1];
            stlReader(isInside, location);
            if (isInside[0])
            {
            }
            else
            {
                if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
                    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            }
        }
    }
}

/*
double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}
*/

//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
bool check75(int iX, int iY, int iZ, Index3D &localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), shipLength, physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::cout << "Define boundaries.... " << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain, converter);

    int numero = domainInfo.localSubDomain;
    //static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, smagoConstant * 5);

    //if(numero==1){
    //lattice.defineDynamics(iXRightBorder - 10, iXRightBorder - 1, 1, iYTopBorder - 1, 1, iZBackBorder - 1, &spongeDynamics);
    //}
    //lattice.defineDynamics(2, iXRightBorder-2, 2, 10, 1, iZBackBorder - 1, &spongeDynamics);

    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

#ifdef ENABLE_CUDA
    initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
    ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
    initalizeCommDataMultilattice(lattice, commDataHandler);
    NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    unsigned int trimTime = converter.getLatticeTime(simTime);
    //singleton::directories().setOutputDir("/home/ekurban3/Documents/tmpaug05/tmp/");
    singleton::directories().setOutputDir("/data/ae-jral/ekurban3/YAWED/tmp/");
    BlockVTKwriter3D<T> vtkwriter("outputVTK_" + std::to_string(domainInfo.localSubDomain));
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid

    BlockVTKwriterMultiLattice3D<T, Lattice<T>> vtkmultiwriter("outputMultiVTK", domainInfo);
    BlockLatticePhysVelocity3D<T, Lattice> multiphysVelocityFunctor(lattice, 0, converter);
    BlockLatticePhysPressure3D<T, Lattice> multiphysPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> multifluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid

    vtkwriter.addFunctor(physVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    vtkwriter.addFunctor(physPressureFunctor);
    vtkwriter.addFunctor(fluidmaskfunctor);
    vtkmultiwriter.addFunctor(multiphysVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    vtkmultiwriter.addFunctor(multiphysPressureFunctor);
    vtkmultiwriter.addFunctor(multifluidmaskfunctor);

    size_t obaiX = iXRightBorder;
    size_t obaiY = iYTopBorder;
    size_t obaiZ = iZBackBorder;

    //std::vector<size_t> limitsvec={0,obaiX,0,obaiY,0,obaiZ};
    size_t startx = xoffset;
    size_t starty = yoffset - youtputoffsetport;
    size_t startz = zoffset - zoutputoffsetsea;
    size_t endx = xoffset + xoutputoffset;
    size_t endy = yoffset + youtputoffsetstarboard;
    size_t endz = zoffset + zoutputoffsetsky;

    size_t startpoint[3] = {startx, starty, startz};
    size_t endpoint[3] = {endx, endy, endz};
    T aftdisttoorig = (endpoint[0] - xoffset) * gridSpacing;
    T portdisttoorig = (yoffset - startpoint[1]) * gridSpacing;
    T stardisttoorig = (endpoint[1] - yoffset) * gridSpacing;
    T seadisttoorig = (zoffset - startpoint[2]) * gridSpacing;
    T skydisttoorig = (endpoint[2] - zoffset) * gridSpacing;

    T ylenn = (endpoint[1] - startpoint[1] + 1) * gridSpacing;
    T zlenn = (endpoint[2] - startpoint[2] + 1) * gridSpacing;

    T xstart = (xoffset - startpoint[0] + 1) * gridSpacing;
    T ystart = (yoffset - startpoint[1] + 1) * gridSpacing;
    T zstart = (zoffset - startpoint[2] + 1) * gridSpacing;
    T xend = (endpoint[0] - xoffset + 1) * gridSpacing;
    T yend = (endpoint[1] - yoffset + 1) * gridSpacing;
    T zend = (endpoint[2] - zoffset + 1) * gridSpacing;

    int iXLeftBorderWRITE = 0;
    int iXRightBorderWRITE = 3 * resolution - 1;
    int iYBottomBorderWRITE = 0;
    int iYTopBorderWRITE = 2 * resolution - 1;
    int iZFrontBorderWRITE = 0;
    int iZBackBorderWRITE = resolution - 1;

    size_t startWRITE[3] = {iXLeftBorderWRITE, iYBottomBorderWRITE, iZFrontBorderWRITE};
    size_t endWRITE[3] = {iXRightBorderWRITE, iYTopBorderWRITE, iZBackBorderWRITE};

    if (outputVTKData)
    {
        //vtkmultiwriter.write(0, 0, startpoint, endpoint);
        vtkmultiwriter.write(0, 0, startWRITE, endWRITE); //temporary
    }

    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    unsigned int rampUpSteps = converter.getLatticeTime(70);
    Timer<T> timer(trimTime, lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        //  Uniform inflow version
    
        
            if (trimStep <= rampUpSteps)
            {
                lattice.copyDataToCPU();
                T uVel = finalLeftVelocity * pow(3, 0.5) * 0.5*((T)trimStep/(T)rampUpSteps);
                T vVel = -finalLeftVelocity * 0.5*((T)trimStep/(T)rampUpSteps);
                /*
            int iX = 0;
            for (int iY = 0; iY <= iYTopBorder; iY++)
            {
                for (int iZ = 0; iZ <= iZBackBorder; iZ++)
                {

                    T vel[3] = {uVel, vVel, 0}; //needs to be modified when height of the domain is changed
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                    {
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                }
            }
            */
                int iX2endd = 3 * resolution - 1; //CHANGE THIS WHEN DOMAIN SIZE CHANGE
                int iY2 = 2 * resolution - 1;     //CHANGE THIS WHEN DOMAIN SIZE CHANGE
                for (int iX2 = 0; iX2 <= iX2endd - 1; iX2++)
                {
                    for (int iZ2 = 0; iZ2 <= iZBackBorder; iZ2++)
                    {

                        T vel[3] = {uVel, vVel, 0};
                        Index3D localIndex;
                        if (check75(iX2, iY2, iZ2, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                        {
                            //T fizu = converter.getPhysVelocity(uVel);
                            //T fizv = converter.getPhysVelocity(vVel);
                            //std::cout << "Velocity define uVel= " << converter.getPhysVelocity(uVel) << "  vVel= " << converter.getPhysVelocity(vVel) << endl;

                            lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                            lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        }
                    }
                }
                //AUG8
                int iY22 = 2 * resolution - 2;
                for (int iY3 = 1; iY3 <= iY22; iY3++)
                {
                    for (int iZ3 = 0; iZ3 <= iZBackBorder; iZ3++)
                    {

                        T vel[3] = {uVel, vVel, 0};
                        Index3D localIndex;
                        if (check75(0, iY3, iZ3, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                        {
                            //T fizu = converter.getPhysVelocity(uVel);
                            //T fizv = converter.getPhysVelocity(vVel);
                            //std::cout << "Velocity define uVel= " << converter.getPhysVelocity(uVel) << "  vVel= " << converter.getPhysVelocity(vVel) << endl;
                            //std::cout << "Trial working ????" << endl;
                            lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                            lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        }
                    }
                }
                //AUG 8

                std::cout << "CCCCCCCC uvel=" << converter.getPhysVelocity(uVel) << std::endl;
                std::cout << "CCCCCCCC vvel=" << converter.getPhysVelocity(vVel) << std::endl;

                lattice.copyDataToGPU();
                //Uniform inflow version
            }
        

#ifdef ENABLE_CUDA
        collideAndStreamPostStreamUpdateMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));
            //if (converter.getPhysTime(trimStep) > 6)
            //{
            if (outputVTKData)
            {
                lattice.copyDataToCPU();
                //vtkwriter.write(trimStep);
                //vtkmultiwriter.write(trimStep, float(trimStep), startpoint, endpoint);
                vtkmultiwriter.write(trimStep, float(trimStep), startWRITE, endWRITE);
                std::cout << "Start point x: " << xstart << "  y:" << ystart << "  z:" << zstart << std::endl;
                std::cout << "End point x: " << xend << "  y:" << yend << "  z:" << zend << std::endl;
                std::cout << "Flow domain x-length" << aftdisttoorig << std::endl;
                std::cout << "Flow domain y-length" << ylenn << std::endl;
                std::cout << "Flow domain z-length" << zlenn << std::endl;
            }
            //}
        }
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    int rank = initIPC();
    unsigned int ghostLayerX = 2;
    unsigned int ghostLayerY = 0;
    unsigned int ghostLayerZ = 0;
    unsigned ghostLayer[3] = {ghostLayerX, ghostLayerY, ghostLayerZ};
    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(3 * resolution), (size_t)(2 * resolution), (size_t)(resolution), 0u, 1u, ghostLayer); //cant be an odd number if you have even numbers of gpus
    const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks(), ghostLayer);
    if (rank == 0)
    {
        std::cout << "REF SUBDOMAIN INFO" << std::endl;
        std::cout << refSubDomain;
        std::cout << "Domain Info" << std::endl;
        std::cout << subDomainInfo;
        std::cout << "####" << std::endl;
    }

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl;
    std::cout << "####################################" << std::endl;

    MultipleSteps(commDataHandler, refSubDomain);

    cudaDeviceSynchronize();
    MPI_Finalize();
    return 0;
}