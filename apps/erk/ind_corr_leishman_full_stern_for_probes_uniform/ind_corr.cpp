/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef float T;
typedef double boundaryT;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include <algorithm>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.025;
const T shipLength = 1.54;
const double simTime = 1;
unsigned int resolution = 390;

//new for adaptive grid
unsigned int fineLeftOffset = resolution * 2;
unsigned int fineBottomOffset = resolution / 4;
unsigned int fineFrontOffset = 1; 
unsigned int fineX = 1.5 * resolution;
unsigned int fineY = resolution;
unsigned int fineZ = 0.5 * resolution;
//new for adaptive grid

const T gridSpacing = shipLength / resolution;
const T gridArea = pow(gridSpacing, 2);

const T physInducedVelocity = 32;
const T physVelocity = 30.5;
const T physDensity = 1.225;               // kg/m^3
const T physKinematicViscosity = 15.15e-6; //was 1.4e-5; // m^2/s

/* Output Parameters */
bool outputVTKData = true;
bool outputRotorData = false;
bool outputDebugData = false;

const T vtkWriteInterval = 0.001; // s

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter, unsigned int fineLeftOffset, unsigned int fineBottomOffset, unsigned int fineFrontOffset, unsigned int fineX, unsigned int fineY, unsigned int fineZ)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    T omega = dynamics.getOmega();

    // auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    // auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 2, iZBackBorder - 1); //+2 for adaptive
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    //new******************************************************************************************************
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    //new******************************************************************************************************

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edge0PN{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edge0PP{};

    //new******************************************************************************************************
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edge0NP{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edge0NN{};
    //new******************************************************************************************************

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    //new******************************************************************************************************
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    //new******************************************************************************************************
    static IniEquilibriumDynamics<T, Lattice> iniEquil{};

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        for (unsigned iZ = iZFrontBorder + 2; iZ <= iZBackBorder - 1; iZ++)
        { //+2 for adaptive
            Index3D localIndex;

            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics0P);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iZ = iZFrontBorder + 2; iZ <= iZBackBorder - 1; iZ++)
        { //+2 for adaptive
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1P);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);

            //if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
            //  lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);
        }
    }

    for (unsigned iY = iYBottomBorder; iY <= iYTopBorder; iY++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            Index3D localIndex;
            //if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
            //  lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2P);
        }
    }

    //     OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
    // createInterpBoundaryCondition3D<T, Lattice,
    //                                 ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    for (unsigned iZ = iZFrontBorder + 2; iZ <= iZBackBorder - 1; iZ++)
    { //+2 for adaptive
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        //  if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex))   //????????????????
        //     boundaryCondition->addExternalVelocityEdge2NP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //????????????????
        //  if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))   //????????????????
        //     boundaryCondition->addExternalVelocityEdge2NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //????????????????
    }

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder + 1, localIndex))                                         //iZFrontBorder+1 for adaptive
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //1NP
        if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //1PP
        // if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
        //     boundaryCondition->addExternalVelocityEdge1PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);  //????????????????????/
        // if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
        //     boundaryCondition->addExternalVelocityEdge1NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //??????????????????????
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder + 1, localIndex))            //iZFrontBorder+1 for adaptive
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PN); //0PN
        if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PP); //0PP
        if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder + 1, localIndex))         //iZFrontBorder+1 for adaptive
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NN); //0NN
        if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NP); //0NP
    }

    Index3D localIndex;
    // if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNPN(localIndex[0], localIndex[1], localIndex[2], omega); //***
    // if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNPP(localIndex[0], localIndex[1], localIndex[2], omega);  //**

    // if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNNN(localIndex[0], localIndex[1], localIndex[2], omega); //***
    // if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    // boundaryCondition->addExternalVelocityCornerNNP(localIndex[0], localIndex[1], localIndex[2], omega);  //**

    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder + 1, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN); //PPN
    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP); //PPP

    //**
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN); //PNN
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP); //PNP

    //CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC-I End

    //////--------------------------------------------**************************Aurora update-end
    /*
    STLreader<T> stlReader("1_90_scale_leishman_sfs2_no_out_surface_long_fluid_15width_4totlength_2_ship_1.STL", converter.getConversionFactorLength(), 1); 
    stlReader.print();
*/


    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            
            
                if (domainInfo.isLocal(iX, iY, iZFrontBorder + 1, localIndex))
                    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            
        }
    }



    for (int iX = iXLeftBorder + fineLeftOffset + 1; iX <= iXLeftBorder + fineLeftOffset + fineX - 2; ++iX)
    {
        for (int iY = iYBottomBorder + fineBottomOffset + 1; iY <= iYBottomBorder + fineBottomOffset + fineY - 2; ++iY)
        {
            int iZ = 1; //only on iZ=1 for adaptive

            if (domainInfo.isLocal(iX, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        }
    }

    for (unsigned iX = iXLeftBorder; iX <= iXRightBorder; iX++)
    {
        for (unsigned iY = iYBottomBorder; iY <= iYTopBorder; iY++)
        {
            if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        }
    }
}

/*
double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}
*/

//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
template <typename T, template <typename> class Lattice>
bool check75(int iX, int iY, int iZ, Index3D &localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

//template <template <typename> class Memory> 
void MultipleSteps()
{
    int rank = initIPC();
    int noRanks = getNoRanks();

    int iXRightBorder = 4 * resolution - 1;
    int iYTopBorder = 1.5 * resolution - 1;
    int iZBackBorder = resolution - 1;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), shipLength, physInducedVelocity, physKinematicViscosity, physDensity, 0);
    UnitConverter<T, Lattice> fineConverter(RefinedGrid3D<T, Lattice>::getConverterForRefinementLevel(converter, 1));

    converter.print();
    fineConverter.print();

    T omega = converter.getLatticeRelaxationFrequency();

    std::cout << "omega: " << omega << ", child omega: " << RefinedGrid3D<T, Lattice>::getOmegaForRefinementLevel(omega, 1) << std::endl;

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> childBulkDynamics(RefinedGrid3D<T, Lattice>::getOmegaForRefinementLevel(omega, 1), smagoConstant);

    std::cout << "Create RefinedGrid.... " << std::endl;
    RefinedGrid3D<T, Lattice> parent(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics, omega);
    parent.addChild(&childBulkDynamics, fineX, fineY, fineZ, fineLeftOffset, fineBottomOffset, fineFrontOffset);
    parent.children[0].setAdjacentToBoundary(false, false, false, false, true, false);
    
    unsigned int ghostLayerX = 2;
    unsigned int ghostLayerY = 0;
    unsigned int ghostLayerZ = 0;
    unsigned ghostLayer[3] = {ghostLayerX, ghostLayerY, ghostLayerZ};
    parent.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

    std::cout << "Set up refinement..." << std::endl;
    parent.setupRefinementGPU();
    parent.setupMultilatticeGPU();

    std::cout << "Define boundaries.... " << std::endl;
    parent.applyMasks();
    defineBoundaries(*(parent.getLatticePointer()), bulkDynamics, parent._localSubDomain, parent._refSubDomain, converter, fineLeftOffset, fineBottomOffset, fineFrontOffset, fineX, fineY, fineZ);

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    int iXLeftBorderRefined = 0;
    int iXRightBorderRefined = refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2);
    int iYBottomBorderRefined = 0;
    int iYTopBorderRefined = refinementutil::getRefinedRegionYFineLength3D(fineX, fineY, fineZ, 2);
    int iZFrontBorderRefined = 0;
    int iZBackBorderRefined = refinementutil::getRefinedRegionZFineLength3D(fineX, fineY, fineZ, 2);

    std::cout << "Define boundaries for refined region... " << std::endl;
    Index3D localIndex;

    for (unsigned ixrefined = iXLeftBorderRefined; ixrefined <= iXRightBorderRefined; ixrefined++)
    {
        for (unsigned iyrefined = iYBottomBorderRefined; iyrefined <= iYTopBorderRefined; iyrefined++)
        {
            for (unsigned izrefined = 0; izrefined <= 1; izrefined++)
            {
                if (parent.children[0]._localSubDomain.isLocal(ixrefined, iyrefined, izrefined, localIndex))
                    parent.children[0].getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
            }
        }
    }

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    Index3D localIndexStart;
    Index3D localIndexEnd;
    if (parent.children[0]._localSubDomain.isRegionLocalToValidGhostLayer(0,0,2,iXRightBorderRefined, iYTopBorderRefined, 2, localIndexStart, localIndexEnd))
        parent.children[0].getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &slip2N);

    STLreader<T> stlReader("1_90_scale_leishman_sfs2_no_out_surface_long_fluid_15width_4totlength_1_ship_2.STL", fineConverter.getConversionFactorLength(), 1);
    stlReader.print();

    for (int ixrefined = 0; ixrefined <= iXRightBorderRefined; ++ixrefined)
    {
        for (int iyrefined = 0; iyrefined <= iYTopBorderRefined; ++iyrefined)
        {
            for (int izrefined = 0; izrefined <= iZBackBorderRefined; ++izrefined)
            {
                T returningarray[3];
                parent.children[0].getTopLevelParentIndices(ixrefined, iyrefined, izrefined, returningarray);

                // printf("ixrefined %d iyrefined %d izrefined %d returningarray0 %f retarray1 %f retarray2 %f\n", ixrefined, iyrefined, izrefined, returningarray[0], returningarray[1], returningarray[2]);
                //convert returnin array to phys and check is in
                T iXPhys = converter.getPhysDeltaX()*returningarray[0]; //converter.getPhysLength(returningarray[0]);
                T iYPhys = converter.getPhysDeltaX()*returningarray[1];
                T iZPhys = converter.getPhysDeltaX()*returningarray[2];
                T location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);
                if (isInside[0])
                {
                    if (parent.children[0]._localSubDomain.isLocal(ixrefined, iyrefined, izrefined, localIndex))
                        parent.children[0].getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2]+2, &instances::getBounceBack<T, Lattice>());
                }
            }
        }
    }

    



    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    std::cout << "Init GPU data.... " << std::endl;
    parent.initDataArrays();
    T vel[3] = {0., 0., 0.};

    //??? we previously had T rho[1]; in init equilibrium for loops dont we need it anymore?

    parent.iniEquilibrium(1.0, vel);
    std::cout << "Finished!" << std::endl;

    std::cout << "Total number of parent cells (including masked): " << parent.getLatticePointer()->getNx() * parent.getLatticePointer()->getNy() * parent.getLatticePointer()->getNz() << std::endl;
    std::cout << "Total number of child cells: " << parent.children[0].getLatticePointer()->getNx() * parent.children[0].getLatticePointer()->getNy() * parent.children[0].getLatticePointer()->getNz() << std::endl;
    std::cout << "Total workload per parent timestep: " << parent.calculateTotalLatticeUnits() << std::endl;

    parent.copyLatticesToGPU();

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("/home/ekurban3/Documents/tmp/");

    RefinedGridVTKManager3D<T, Lattice> writer("SFS2", parent); 
    writer.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
    writer.addFunctor<BlockLatticePhysPressure3D<T, Lattice>>(0, converter); 
    writer.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();

    if (outputVTKData)
        writer.write(0);

    MPI_Barrier(MPI_COMM_WORLD);
    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    Timer<T> timer(trimTime, parent.calculateTotalLatticeUnits());
    timer.start();

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        //  Uniform inflow version
        if (trimStep == 0)
        {
            parent.getLatticePointer()->copyDataToCPU();
            T uVel = finalLeftVelocity;

            int iX = 0;
            for (int iY = 0; iY <= iYTopBorder; iY++)
            {
                for (int iZ = 0; iZ <= iZBackBorder; iZ++)
                {

                    T vel[3] = {uVel, 0, 0}; //needs to be modified when height of the domain is changed
                    Index3D localIndex;      ///???????
                    if (check75(0, iY, iZ, localIndex, (*parent.getLatticePointer()), parent._localSubDomain, parent._refSubDomain))
                    {                                                                                                                                                   ////???????
                        parent.getLatticePointer()->defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);     /////????????
                        parent.getLatticePointer()->iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel); /////??????
                    }
                }
            }

            parent.getLatticePointer()->copyDataToGPU();
            //Uniform inflow version
        }

        //1/7th rule version
        /*
            if(trimStep==0){        
                parent.getLatticePointer()->copyLatticesToCPU();
                T uVel = finalLeftVelocity;
                T resodouble=resolution;
                int iX = 0;
                for (int iY = 0; iY <= iYTopBorder; iY++) {
                    for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
                    T iZo=iZ;
                    
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    if(iZ>(44.0/91.0*resodouble) || iZ==44*resolution/91)//needs to be modified when height of the domain is changed -- height of ship*4=height of ABL
                    {
                    T vel[3] = {uVel, 0, 0};
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    parent.getLatticePointer()->defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    parent.getLatticePointer()->iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                    }

                    if(iZ<(44.0/91.0*resodouble))//needs to be modified when height of the domain is changed
                    {
                    T vel[3] = {uVel*pow((iZo/(44.0/91.0*resodouble)),(1.0/7.0)), 0, 0};//needs to be modified when height of the domain is changed -- height of ship*4=height of ABL
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    parent.getLatticePointer()->defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    parent.getLatticePointer()->iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                    }

                
                }
            }
            
                parent.getLatticePointer()->copyDataToGPU();
            }
            */

        parent.collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            (*parent.getLatticePointer()).getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            if (outputVTKData)
            {
                parent.copyLatticesToCPU();
                writer.write(trimStep);
            }
        }

    }
        timer.stop();
        timer.printSummary();
        finalizeIPC();
}

int main(int argc, char **argv)
{
    MultipleSteps();
    return 0;
}
