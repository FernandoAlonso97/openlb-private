/*  This file is part of the OpenLB library
*
*  Copyright (C) 2022 Isabel Fernandez
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
// #define D3Q19LATTICE 1
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"
#include "communication/writeFunctorToGPU.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/readWriteFunctorsGPU.h"
#include "../../../eigen-3.4.0/Eigen/Eigen"

#define Lattice ForcedD3Q19Descriptor
// #define Lattice D3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

using namespace Eigen;

/* Simulation Parameters */
/* Simulation Parameters */
T smagoConstant = 0.05;
const T shipLength = 3.00064467;
const double simTime = 10;
unsigned int resolution = 100;
//unsigned int xExtraResolution = 4 * resolution;

const int xLength = 4 * resolution;
const int yLength = 1.5 * resolution;
const int zLength = resolution;
const T gridSpacing = shipLength / resolution;
const T gridArea = pow(gridSpacing, 2);

const T physInducedVelocity = 40;
const T physVelocity = 15.0;
const T physDensity = 1.225;               // kg/m^3
const T physKinematicViscosity = 14.61e-6; // m^2/s

/* Output Parameters */
bool outputVTKData = true;

const T vtkWriteInterval = 0.01; // s

const int xoffset = 2.21791442 / gridSpacing + 1.5 * shipLength / gridSpacing;
const int yoffset = (0.75 * shipLength) / gridSpacing;
const int zoffset = 0.18569959 / gridSpacing;

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    initGhostLayer(domainInfo, lattice);

    T omega = dynamics.getOmega();
    /*
    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);
    */
    static ForcedLudwigSmagorinskyBGKdynamicsNonPostStream<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedance0P(omega, smagoConstant * 300);
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> slipedge0PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> slipedge0NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> slipedge0PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> slipedge0NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipedge1PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipedge1NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipedge1PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipedge1NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, -1>> slipedge2NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, 1>> slipedge2NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, -1>> slipedge2PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, 1>> slipedge2PP;

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edge0PN{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edge0PP{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edge0NP{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edge0NN{};

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;

    static IniEquilibriumDynamics<T, Lattice> iniEquil{};
    static ForcedLudwigSmagorinskyBGKdynamicsNonPostStream<T, Lattice, BulkMomenta<T, Lattice>> spongeback(omega, smagoConstant * 200);
    static ForcedLudwigSmagorinskyBGKdynamicsNonPostStream<T, Lattice, BulkMomenta<T, Lattice>> spongefront(omega, smagoConstant * 50);

    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;
            /*if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex)){
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
            }*/
            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &impedance0P);
        }
    }

    for (unsigned iY = iYBottomBorder; iY <= iYTopBorder; iY++)
    {
        for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
            {
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
            }
        }
    }

    for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
        {
            // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
        }
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
        {
            // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
        }
    }

    for (unsigned iY = iYBottomBorder; iY <= iYTopBorder; iY++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
        {
            // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
        }
        if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
        {
            // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
        {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1P); //&periodicplane1P);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N); //&periodicplane1N);
        }
    }

    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            Index3D localIndex;
            //if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
            //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane2N);//&slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2P); //&periodicplane2P);
        }
    }

    OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
        createInterpBoundaryCondition3D<T, Lattice,
                                        ForcedLudwigSmagorinskyBGKdynamics>(lattice);
    /*
    for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //&edge2PP);//////&slipedge2PP);
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex)){   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NP);//&slipedge2NP);
            }
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NN);//&slipedge2NN);
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //&edge2PN);//&instances::getBounceBack<T, Lattice>());////&slipedge2PN);
        }
    }
*/
    /*
    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //&edge1PP);// &instances::getBounceBack<T, Lattice>());  //&slipedge1NP);
        }
        if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
        {
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //&edge1NP);//&instances::getBounceBack<T, Lattice>());  ////&slipedge1PP);
        }
        
            if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PN);//&slipedge1PN);
            }
            if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NN);//&slipedge1NN);
            }  
    }
*/
    for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
    {
        Index3D localIndex;
        if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PN); //&slipedge0PN);
        if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PP); //&slipedge0PP);
        if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NN); //&slipedge0NN);
        if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NP); //&slipedge0NP);
    }

    Index3D localIndex;
    /*if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPN);
        }
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPP);
        }
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNP);
        }
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNN); 
        }*/
    /*
    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN);
    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP);
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP);
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN);
*/
    for (unsigned iX = iXRightBorder - 10; iX <= iXRightBorder - 1; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
            {
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &spongeback);
            }
        }
    }

    for (unsigned iX = iXRightBorder - 30; iX <= iXRightBorder - 11; iX++)
    {
        for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
        {
            for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
            {
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &spongefront);
            }
        }
    }

    //old stuff
    /*
    auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);
    */
    //static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, -1>> periodicplane1N;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, 1>> periodicplane1P;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, -1>> periodicplane2N;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, 1>> periodicplane2P;
    /*
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
*/
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    /*
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;
    */

    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipcornerPNN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipcornerPPN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipcornerPNP;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipcornerPPP;
}

bool check75(int iX, int iY, int iZ, Index3D &localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

void GetPressureOnFluidCellsAroundBody(Index3D &localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder, std::ofstream &myfile, T physDensity, T physVelocity)
{

    for (int iX = 1; iX <= iXRightBorder; ++iX)
    {
        for (int iY = 1; iY <= iYTopBorder; ++iY)
        {
            for (int iZ = 1; iZ <= iZBackBorder; ++iZ)
            {
                Index3D localIndex1;
                Index3D localIndex2;
                if (domainInfo.isLocal(iX, iY, iZ, localIndex1))
                {
                    bool isFluid = lattice.getMaskEntry(localIndex1[0], localIndex1[1], localIndex1[2]);
                    if (isFluid == false)
                    {
                        for (int iPop = 1; iPop < Lattice<T>::q; ++iPop)
                        {
                            // Get direction
                            int cX = Lattice<T>::c(iPop, 0);
                            int cY = Lattice<T>::c(iPop, 1);
                            int cZ = Lattice<T>::c(iPop, 2);
                            if (domainInfo.isLocal(iX + cX, iY + cY, iZ + cZ, localIndex2))
                            {
                                bool isNFluid = lattice.getMaskEntry(localIndex2[0], localIndex2[1], localIndex2[2]);
                                if (isNFluid == true)
                                {
                                    T celllatticePressure = (lattice.get(localIndex2[0], localIndex2[1], localIndex2[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
                                    T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
                                    T cellPressureCoeff = cellPhysPressure / (0.5 * physDensity * pow(physVelocity, 2));
                                    myfile << iX + cX << "," << iY + cY << "," << iZ + cZ << "," << cellPhysPressure << "," << cellPressureCoeff << "\n";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

T calculateDelta(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], T neighborCell[3], T dir[3], STLreader<double> &stlReader) //, std::ofstream &myfile)
{
    typedef double T;
    T physFluidLocation[3] = {converter.getPhysLength(slipCell[0]), converter.getPhysLength(slipCell[1]), converter.getPhysLength(slipCell[2])};
    T fracSpacing = 100;
    T dirMag = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));
    T dotDel = (converter.getPhysDeltaX() * dirMag) / fracSpacing;
    T physXpoint = converter.getPhysLength(slipCell[0]); //physFluidLocation[0];
    T physYpoint = converter.getPhysLength(slipCell[1]); //physFluidLocation[1];
    T physZpoint = converter.getPhysLength(slipCell[2]); //physFluidLocation[2];
    T Xw;
    T Yw;
    T Zw;
    T physPoint[3];
    for (int i = 1; i <= fracSpacing; ++i)
    {
        physPoint[0] = physXpoint;
        physPoint[1] = physYpoint,
        physPoint[2] = physZpoint;
        // std::vector<T> tmpLoc(physPoint, physPoint+3);
        Xw = physXpoint;
        Yw = physYpoint;
        Zw = physZpoint;
        physXpoint += dir[0] * dotDel;
        physYpoint += dir[1] * dotDel;
        physZpoint += dir[2] * dotDel;
        bool isInside[1];
        T location[3] = {physXpoint, physYpoint, physZpoint};
        stlReader(isInside, location);
        if (isInside[0])
        {
            break;
        }
    }
    T physWallLocation[3] = {Xw, Yw, Zw};
    T physSolidLocation[3] = {converter.getPhysLength(neighborCell[0]), converter.getPhysLength(neighborCell[1]), converter.getPhysLength(neighborCell[2])};
    T numMagnitude = sqrt(pow((physFluidLocation[0] - physWallLocation[0]), 2) +
                          pow((physFluidLocation[1] - physWallLocation[1]), 2) +
                          pow((physFluidLocation[2] - physWallLocation[2]), 2));
    T denMagnitude = sqrt(pow((physFluidLocation[0] - physSolidLocation[0]), 2) +
                          pow((physFluidLocation[1] - physSolidLocation[1]), 2) +
                          pow((physFluidLocation[2] - physSolidLocation[2]), 2));

    float delta = numMagnitude / denMagnitude;
    // T delta = numMagnitude/denMagnitude;
    /*
    myfile << physFluidLocation[0] << ",";
    myfile << physFluidLocation[1] << ",";
    myfile << physFluidLocation[2] << ",";
    myfile << physSolidLocation[0] << ",";
    myfile << physSolidLocation[1] << ",";
    myfile << physSolidLocation[2] << ",";
    myfile << Xw << ",";
    myfile << Yw << ",";
    myfile << Zw << ",";
    myfile << delta << "\n";
*/
    float latticeWallLocation[3] = {Xw / converter.getPhysDeltaX(), Yw / converter.getPhysDeltaX(), Zw / converter.getPhysDeltaX()};
    // T latticeWallLocation[3] = {Xw/converter.getPhysDeltaX(), Yw/converter.getPhysDeltaX(), Zw/converter.getPhysDeltaX()};
    std::vector<float> output = {latticeWallLocation[0], latticeWallLocation[1], latticeWallLocation[2], delta};
    // std::vector<T> output = {latticeWallLocation[0],latticeWallLocation[1] ,latticeWallLocation[2], delta};
    // float delta = 0.25;
    return delta;
    // return output;
}

T returnVector(T slipCell[3], T neighborCell[3], T dir[3], STLreader<double> &stlReader)
{
    // std::vector<T> output = {vec[0], vec[1], vec[2]};
    // return output;
    T delta = 0.28;
    return delta;
}

std::vector<double> checkDirection(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, STLreader<double> &stlReader)
{
    typedef double T;
    T spacing = 1.0 / 100.0; //100.0;
    T fracSpacing = 100;
    bool correctDirection = true;
    for (int i = 1; i <= fracSpacing; ++i)
    {
        vecX += spacing * normalEigenvector[0];
        vecY += spacing * normalEigenvector[1];
        vecZ += spacing * normalEigenvector[2];

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if (isInsideSTL[0])
        {
            correctDirection = false;
            break;
        }
    }
    if (correctDirection == false)
    {
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalEigenvector[iDim] = -normalEigenvector[iDim];
        }
    }
    return normalEigenvector;
}

std::vector<double> checkOrientation(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, std::vector<double> otherEigenvector, STLreader<double> &stlReader)
{
    typedef double T;
    T fracSpacing = 100;
    T dirMag = sqrt(pow(normalEigenvector[0], 2) + pow(normalEigenvector[1], 2) + pow(normalEigenvector[2], 2));
    T spacing = (2.0 * converter.getPhysDeltaX() * dirMag) / fracSpacing;

    bool correctOrientation = false;
    for (int i = 1; i <= fracSpacing; ++i)
    {
        vecX += -spacing * normalEigenvector[0];
        vecY += -spacing * normalEigenvector[1];
        vecZ += -spacing * normalEigenvector[2];

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if (isInsideSTL[0])
        {
            correctOrientation = true;
            break;
        }
    }

    if (correctOrientation == false)
    {
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalEigenvector[iDim] = otherEigenvector[iDim];
        }
    }

    return normalEigenvector;
}

std::vector<double> normalizeNormals(std::vector<double> normalEigenvector)
{
    T normalAbs = 0;
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
        normalAbs += normalEigenvector[iDim] * normalEigenvector[iDim];
    }
    normalAbs = std::sqrt(normalAbs);
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
        normalEigenvector[iDim] = normalEigenvector[iDim] / normalAbs;
    }
    return normalEigenvector;
}

std::vector<T> calculateNormal(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], STLreader<double> &stlReader)
{
    typedef double T;
    T fracSpacing = 100;
    T pointX = slipCell[0];
    T pointY = slipCell[1];
    T pointZ = slipCell[2];
    // myfile << pointX << "," << pointY << "," << pointZ << ",";
    T normal[3] = {0, 0, 0};
    int numPoints = 0;
    T maxX = 0;
    T maxY = 0;
    T maxZ = 0;
    int p = 0;
    T centroidX = 0;
    T centroidY = 0;
    T centroidZ = 0;
    std::vector<T> pointsX(0);
    std::vector<T> pointsY(0);
    std::vector<T> pointsZ(0);
    std::vector<std::vector<T>> points;
    T covarianceMatrix[3][3];

    for (int iPop = 1; iPop < Lattice<T>::q; ++iPop)
    {
        T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};
        T dirMag = sqrt(pow(dir[0], 2) + pow(dir[1], 2) + pow(dir[2], 2));
        T dotDel = (2.0 * converter.getPhysDeltaX() * dirMag) / fracSpacing; //(2.0*sqrt(2.0)*converter.getPhysDeltaX()*dirMag)/fracSpacing;
        T physXpoint = converter.getPhysLength(slipCell[0]);                 //physFluidLocation[0];
        T physYpoint = converter.getPhysLength(slipCell[1]);                 //physFluidLocation[1];
        T physZpoint = converter.getPhysLength(slipCell[2]);                 //physFluidLocation[2];
        T Xw = physXpoint;
        T Yw = physYpoint;
        T Zw = physZpoint;
        for (int i = 1; i <= fracSpacing; ++i)
        {
            Xw += dir[0] * dotDel;
            Yw += dir[1] * dotDel;
            Zw += dir[2] * dotDel;
            bool isInside[1];
            T location[3] = {Xw, Yw, Zw};
            stlReader(isInside, location);
            if (isInside[0])
            {
                ////////////eigenvalue method///////////////
                centroidX += Xw;
                centroidY += Yw;
                centroidZ += Zw;

                pointsX.push_back(Xw);
                pointsY.push_back(Yw);
                pointsZ.push_back(Zw);
                //////////////
                numPoints++;
                normal[0] += (Xw - physXpoint);
                normal[1] += (Yw - physYpoint);
                normal[2] += (Zw - physZpoint);

                // myfile << physXpoint << "," << physYpoint << "," << physZpoint << "," << Xw << "," << Yw << "," << Zw << "\n";
                break;
            }
        }
    }

    T centroid[3] = {centroidX / numPoints, centroidY / numPoints, centroidZ / numPoints};
    points.resize(numPoints, std::vector<T>(3));
    for (unsigned int i = 0; i < numPoints; ++i)
    {
        points[i][0] = pointsX[i];
        points[i][1] = pointsY[i];
        points[i][2] = pointsZ[i];
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            covarianceMatrix[i][j] = 0.0;
            for (int k = 0; k < points.size(); k++)
            {
                covarianceMatrix[i][j] += (centroid[i] - points[k][i]) * (centroid[j] - points[k][j]);
                covarianceMatrix[i][j] /= points.size(); // - 1;
            }
        }
    }
    Matrix3d COV;
    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            COV(i, j) = covarianceMatrix[i][j];
        }
    }

    EigenSolver<MatrixXd> solution(COV);
    VectorXd D = solution.eigenvalues().real();
    MatrixXd V = solution.eigenvectors().real();

    T minEig = D.minCoeff();
    T maxEig = D.maxCoeff();
    std::vector<double> eigenvalues;
    std::vector<std::vector<T>> eigenVectorMatrix;
    eigenvalues.resize(D.size());
    eigenVectorMatrix.resize(D.size(), std::vector<T>(3));
    int index;
    int maxIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        eigenvalues[i] = D(i);
    }

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            eigenVectorMatrix[i][j] = V(i, j);
        }
    }
    bool found = false;

    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == minEig)
        {
            index = i;
            found = true;
            break;
        }
    }

    bool foundMax = false;

    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == maxEig)
        {
            maxIndex = i;
            foundMax = true;
            break;
        }
    }

    std::vector<T> normalEigenvector;
    normalEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        normalEigenvector[i] = eigenVectorMatrix[i][index];
    }
    int otherIndex1;
    int otherIndex2;
    if (index == 0)
    {
        otherIndex1 = 1;
        otherIndex2 = 2;
    }
    if (index == 1)
    {
        otherIndex1 = 0;
        otherIndex2 = 2;
    }
    if (index == 2)
    {
        otherIndex1 = 0;
        otherIndex2 = 1;
    }
    int midIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        if (i != index && i != maxIndex)
        {
            midIndex = i;
        }
    }

    std::vector<T> otherEigenvector0;
    otherEigenvector0.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector0[i] = eigenVectorMatrix[i][index];
    }

    std::vector<T> otherEigenvector1;
    otherEigenvector1.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector1[i] = eigenVectorMatrix[i][otherIndex1];
    }

    std::vector<T> otherEigenvector2;
    otherEigenvector2.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector2[i] = eigenVectorMatrix[i][otherIndex2];
    }

    std::vector<T> maxEigenvector;
    maxEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        maxEigenvector[i] = eigenVectorMatrix[i][maxIndex];
    }

    std::vector<T> midEigenvector;
    midEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        midEigenvector[i] = eigenVectorMatrix[i][midIndex];
    }

    normalEigenvector = normalizeNormals(normalEigenvector);
    otherEigenvector1 = normalizeNormals(otherEigenvector1);
    otherEigenvector2 = normalizeNormals(otherEigenvector2);
    maxEigenvector = normalizeNormals(maxEigenvector);
    midEigenvector = normalizeNormals(midEigenvector);
    ///////////////////////////////////////////////////////

    T vecX = converter.getPhysLength(slipCell[0]);
    T vecY = converter.getPhysLength(slipCell[1]);
    T vecZ = converter.getPhysLength(slipCell[2]);
    T spacing = 1.0 / 100.0;

    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, midEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, maxEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);

    // myfile << normalEigenvector[0] << "," << normalEigenvector[1] << "," << normalEigenvector[2] << ",";
    // myfile << otherEigenvector1[0] << "," << otherEigenvector1[1] << "," << otherEigenvector1[2] << ",";
    // myfile << otherEigenvector2[0] << "," << otherEigenvector2[1] << "," << otherEigenvector2[2] << "\n";

    float output0 = normalEigenvector[0];
    float output1 = normalEigenvector[1];
    float output2 = normalEigenvector[2];

    std::vector<double> output = {output0, output1, output2};
    // std::vector<T> output = {normalEigenvector[0],normalEigenvector[1],normalEigenvector[2]};
    return output;
}
std::vector<int> CountFluidCellsAroundBody(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder)
{

    std::vector<int> counter{0, 0, 0, 0, 0, 0, 0};

    for (int iX = 2; iX <= iXRightBorder - 2; ++iX)
    {
        for (int iY = 2; iY <= iYTopBorder - 2; ++iY)
        {
            for (int iZ = 2; iZ <= iZBackBorder - 2; ++iZ)
            {

                bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                if (isFluid == false)
                {

                    int cell0P[3] = {iX + 1, iY, iZ};
                    int cell0N[3] = {iX - 1, iY, iZ};
                    int cell1P[3] = {iX, iY + 1, iZ};
                    int cell1N[3] = {iX, iY - 1, iZ};
                    int cell2P[3] = {iX, iY, iZ + 1};
                    int cell2N[3] = {iX, iY, iZ - 1};

                    bool isCell0PFluid = lattice.getMaskEntry(cell0P[0], cell0P[1], cell0P[2]);
                    bool isCell0NFluid = lattice.getMaskEntry(cell0N[0], cell0N[1], cell0N[2]);
                    bool isCell1PFluid = lattice.getMaskEntry(cell1P[0], cell1P[1], cell1P[2]);
                    bool isCell1NFluid = lattice.getMaskEntry(cell1N[0], cell1N[1], cell1N[2]);
                    bool isCell2PFluid = lattice.getMaskEntry(cell2P[0], cell2P[1], cell2P[2]);
                    bool isCell2NFluid = lattice.getMaskEntry(cell2N[0], cell2N[1], cell2N[2]);

                    if (isCell0PFluid)
                    {
                        counter[0]++;
                    }
                    if (isCell0NFluid)
                    {
                        counter[1]++;
                    }
                    if (isCell1PFluid)
                    {
                        counter[2]++;
                    }
                    if (isCell1NFluid)
                    {
                        counter[3]++;
                    }
                    if (isCell2PFluid)
                    {
                        counter[4]++;
                    }
                    if (isCell2NFluid)
                    {
                        counter[5]++;
                    }
                }
            }
        }
    }
    counter[6] = counter[0] + counter[1] + counter[2] + counter[3] + counter[4] + counter[5];
    return counter;
}

std::vector<T> getForces(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder)
{
    std::vector<T> physForceOutput{0.0, 0.0, 0.0};
    T physXForce = 0.0;
    T physYForce = 0.0;
    T physZForce = 0.0;

    for (int iX = 2; iX <= iXRightBorder - 2; ++iX)
    {
        for (int iY = 2; iY <= iYTopBorder - 2; ++iY)
        {
            for (int iZ = 2; iZ <= iZBackBorder - 2; ++iZ)
            {

                T output[3] = {0, 0, 0};
                bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                if (isFluid == false)
                {
                    for (int iPop = 1; iPop < Lattice<T>::q; ++iPop)
                    {
                        int cX = Lattice<T>::c(iPop, 0);
                        int cY = Lattice<T>::c(iPop, 1);
                        int cZ = Lattice<T>::c(iPop, 2);

                        // Get next cell located in the current direction
                        // Check if the next cell is a fluid node
                        if (lattice.getMaskEntry(iX + cX, iY + cY, iZ + cZ) == true)
                        {
                            // Get f_q of next fluid cell where l = opposite(q)
                            T f = lattice.get(iX + cX, iY + cY, iZ + cZ)[iPop].get();
                            // Get f_l of the boundary cell
                            // Add f_q and f_opp
                            f += lattice.get(iX, iY, iZ)[util::opposite<Lattice<T>>(iPop)];
                            // Update force
                            output[0] -= cX * f;
                            output[1] -= cY * f;
                            output[2] -= cZ * f;
                        }
                    }

                    output[0] = converter.getPhysForce(output[0]);
                    output[1] = converter.getPhysForce(output[1]);
                    output[2] = converter.getPhysForce(output[2]);
                }
                else
                {
                    output[0] = 0;
                    output[1] = 0;
                    output[2] = 0;
                }
                physXForce += output[0];
                physYForce += output[1];
                physZForce += output[2];
            }
        }
    }
    physForceOutput[0] = physXForce;
    physForceOutput[1] = physYForce;
    physForceOutput[2] = physZForce;
    return physForceOutput;
}

template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), shipLength, physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;
    Index3D localIndex1;
    Index3D localIndex2;
    Index3D localIndex3;

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamicsNonPostStream<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::cout << "Define boundaries.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain, converter);
    std::cout << "Define boundaries Finished. " << std::to_string(domainInfo.localSubDomain) << std::endl;

    //bool isInGPU = domainInfo.getLocalInfo().isLocal(540*scaler, iYTopBorder/2, iZBackBorder/2, localIndex);
    //std::cout << "For GPU " << std::to_string(domainInfo.localSubDomain) << ", ROBIN in GPU: " << isInGPU << std::endl;

    int numero = domainInfo.localSubDomain; //this is hard coded you might want to change it
    /*bool isInGPU;
    if (numero == 0) //this is case specific this needs to be modified when the stl configuration changes
    {
        isInGPU = true;
    }
    else
    {
        isInGPU = false;
    }
    std::cout << "bool isInGPU test has been done for GPU #" << std::to_string(domainInfo.localSubDomain) << ". It returned:" << isInGPU << std::endl;
*/
    static PostProcessingDynamics<T, Lattice, CurvedSlipBoundaryProcessor3D<T, Lattice>> slipBoundary;
    static NoDynamics<T, Lattice> noDynamics;
    STLreader<double> stlReader("150originx2y2z2.stl", converter.getConversionFactorLength() / 20, 1.0); //,1, true); ///doesn't seem to go unstable

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    /*stlReader.print();//yedek
        for (int iX = 2; iX <= lattice.getNx() - 2; ++iX)
        { //temporary measure
            for (int iY = 0; iY <= lattice.getNy(); ++iY)
            {
                for (int iZ = 0; iZ <= lattice.getNz(); ++iZ)
                {

                    double iXPhys = converter.getPhysLength(iX - xoffset);
                    double iYPhys = converter.getPhysLength(iY - yoffset);
                    double iZPhys = converter.getPhysLength(iZ - zoffset);
                    double location[3] = {iXPhys, iYPhys, iZPhys};
                    bool isInside[1];
                    stlReader(isInside, location);

                    if (isInside[0])
                    {
                        //if (check75(iX, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                        lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>()); //&noDynamics);
                    }
                }
            }
        }*/

    stlReader.print();
    for (int iX = 2; iX <= xLength - 2; ++iX)
    { //temporary measure
        for (int iY = 0; iY <= yLength - 1; ++iY)
        {
            for (int iZ = 0; iZ <= zLength - 1; ++iZ) //no ship only ground//used to be 3
            {

                double iXPhys = converter.getPhysLength(iX - xoffset);
                double iYPhys = converter.getPhysLength(iY - yoffset);
                double iZPhys = converter.getPhysLength(iZ - zoffset);
                double location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);

                if (isInside[0])
                {
                    if (check75(iX, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); //&noDynamics);
                }
            }
        }
    }

    for (unsigned iX = 1; iX <= xLength - 1; iX++)
    {
        for (unsigned iY = 1; iY <= yLength - 1; iY++)
        {
            Index3D localIndex;
            T iXPhys = converter.getPhysLength(iX - xoffset);
            T iYPhys = converter.getPhysLength(iY - yoffset);
            T iZPhys = converter.getPhysLength(0);
            T location[3] = {iXPhys, iYPhys, iZPhys};
            bool isInside[1];
            stlReader(isInside, location);
            if (isInside[0])
            {
            }
            else
            {
                if (check75(iX, iY, 0, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            }
        }
    }


    /*///temporary----------------------HERE
    ///////TRIALS START
    std::ofstream normalNeo0("normalNeo0.csv");
    std::ofstream normalNeo1("normalNeo1.csv");
    std::ofstream normalLocalNeo0("normalLocalNeo0.csv");
    std::ofstream normalLocalNeo1("normalLocalNeo1.csv");
    cudaDeviceSynchronize();

    if (numero == 0)
    {
        normalNeo0 << "Index1, Index2, Index3, Normal1, Normal2, Normal3\n";
    }
    if (numero == 1)
    {
        normalNeo1 << "Index1, Index2, Index3, Normal1, Normal2, Normal3\n";
    }
    if (numero == 0)
    {
        normalLocalNeo0 << "Index1, Index2, Index3, Normal1, Normal2, Normal3\n";
    }
    if (numero == 1)
    {
        normalLocalNeo1 << "Index1, Index2, Index3, Normal1, Normal2, Normal3\n";
    }

    stlT shipOffsetX = xoffset;
    stlT shipOffsetY = yoffset;
    stlT shipOffsetZ = zoffset;

    STLreader<stlT> stlReaderTrial("150originx2y2z2.stl", converter.getConversionFactorLength() / 20, 1.0);
    stlReader.print();

    std::cout << "Loaded! Assigning bounceback cells.." << std::endl;

    std::set<size_t> outsideNeighborsSet;
    std::vector<std::vector<int>> outsideNeighborsReplacementPops;
    std::map<size_t, vector<stlT>> indexandnormal;
    int outsideNeighborCounter = 0;
    for (int iX = 0; iX <= xLength; iX++)
    {
        for (int iY = 0; iY <= yLength; iY++)
        {
            for (int iZ = 3; iZ <= zLength; iZ++)
            {
                stlT iXPhys = converter.getPhysLength((stlT)iX - shipOffsetX);
                stlT iYPhys = converter.getPhysLength((stlT)iY - shipOffsetY);
                stlT iZPhys = converter.getPhysLength((stlT)iZ - shipOffsetZ);
                stlT location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReaderTrial(isInside, location);
                if (isInside[0])
                {
                    if (check75(iX, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                    {

                        
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                        

                        bool allNeighborsInside = true;
                        std::vector<int> neighborsNotInside;
                        for (int iPop = 0; iPop < Lattice<T>::q; iPop++)
                        {
                            stlT iXNeighborPhys = converter.getPhysLength((stlT)iX - shipOffsetX + (stlT)Lattice<T>::c(iPop, 0));
                            stlT iYNeighborPhys = converter.getPhysLength((stlT)iY - shipOffsetY + (stlT)Lattice<T>::c(iPop, 1));
                            stlT iZNeighborPhys = converter.getPhysLength((stlT)iZ - shipOffsetZ + (stlT)Lattice<T>::c(iPop, 2));
                            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
                            bool neighborInside[1];
                            stlReaderTrial(neighborInside, neighborLocation);
                            allNeighborsInside = allNeighborsInside && neighborInside[0];
                            if (!neighborInside[0])
                                neighborsNotInside.push_back(iPop);
                            // printf("iX = %d, iY = %d, iZ = %d, neighbor %d inside: %d\n", iX, iY, iZ, iPop, neighborInside[0]);
                        }

                        if (!allNeighborsInside)
                        {

                            for (int outsideNeighbor : neighborsNotInside)
                            {

                                size_t outsideNeighborCellIndex = util::getCellIndex3D(localIndex[0] + Lattice<T>::c(outsideNeighbor, 0), localIndex[1] + Lattice<T>::c(outsideNeighbor, 1), localIndex[2] + Lattice<T>::c(outsideNeighbor, 2), lattice.getNy(), lattice.getNz()); //local index usage

                                auto result = outsideNeighborsSet.insert(outsideNeighborCellIndex);
                                if (result.second)
                                {

                                    olb::Vector<stlT, 3> neighborPoint(converter.getPhysLength((stlT)iX - shipOffsetX + (stlT)Lattice<T>::c(outsideNeighbor, 0)),
                                                                       converter.getPhysLength((stlT)iY - shipOffsetY + (stlT)Lattice<T>::c(outsideNeighbor, 1)),
                                                                       converter.getPhysLength((stlT)iZ - shipOffsetZ + (stlT)Lattice<T>::c(outsideNeighbor, 2)));
                                    olb::Vector<stlT, 3U> normal = stlReaderTrial.evalSurfaceNormal(neighborPoint);
                                    outsideNeighborCounter = outsideNeighborCounter + 1;

                                    if (numero == 0)
                                    {
                                        normalNeo0 << iX + Lattice<T>::c(outsideNeighbor, 0) << " " << iY + Lattice<T>::c(outsideNeighbor, 1) << " " << iZ + Lattice<T>::c(outsideNeighbor, 2) << " " << normal[0] << " " << normal[1] << " " << normal[2] << "\n";
                                    }
                                    if (numero == 1)
                                    {
                                        normalNeo1 << iX + Lattice<T>::c(outsideNeighbor, 0) << " " << iY + Lattice<T>::c(outsideNeighbor, 1) << " " << iZ + Lattice<T>::c(outsideNeighbor, 2) << " " << normal[0] << " " << normal[1] << " " << normal[2] << "\n";
                                    }
                                    if (numero == 0)
                                    {
                                        normalLocalNeo0 << localIndex[0] + Lattice<T>::c(outsideNeighbor, 0) << " " << localIndex[1] + Lattice<T>::c(outsideNeighbor, 1) << " " << localIndex[2] + Lattice<T>::c(outsideNeighbor, 2) << " " << normal[0] << " " << normal[1] << " " << normal[2] << "\n";
                                    }
                                    if (numero == 1)
                                    {
                                        normalLocalNeo1 << localIndex[0] + Lattice<T>::c(outsideNeighbor, 0) << " " << localIndex[1] + Lattice<T>::c(outsideNeighbor, 1) << " " << localIndex[2] + Lattice<T>::c(outsideNeighbor, 2) << " " << normal[0] << " " << normal[1] << " " << normal[2] << "\n";
                                    }

                                    lattice.defineDynamics(localIndex[0] + Lattice<T>::c(outsideNeighbor, 0), localIndex[1] + Lattice<T>::c(outsideNeighbor, 1), localIndex[2] + Lattice<T>::c(outsideNeighbor, 2), &slipBoundary); //local here only
                                    indexandnormal[outsideNeighborCellIndex] = {normal[0], normal[1], normal[2]};
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    for (size_t outsideNeighbor : outsideNeighborsSet)
    {
        size_t indices[3];
        util::getCellIndices3D(outsideNeighbor, iYTopBorder + 1, iZBackBorder + 1, indices);
        std::vector<int> replacementPops;
        for (int iPop = 1; iPop < Lattice<T>::q; iPop++)
        {
            stlT iXNeighborPhys = converter.getPhysLength((stlT)indices[0] - shipOffsetX + (stlT)Lattice<T>::c(iPop, 0));
            stlT iYNeighborPhys = converter.getPhysLength((stlT)indices[1] - shipOffsetY + (stlT)Lattice<T>::c(iPop, 1));
            stlT iZNeighborPhys = converter.getPhysLength((stlT)indices[2] - shipOffsetZ + (stlT)Lattice<T>::c(iPop, 2));
            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
            bool neighborInside[1];
            stlReaderTrial(neighborInside, neighborLocation);
            if (neighborInside[0])
            {
                replacementPops.push_back(iPop);
            }
        }
        outsideNeighborsReplacementPops.push_back(replacementPops);
    }

    for (int boundaryPt = 0; boundaryPt < outsideNeighborsSet.size(); boundaryPt++)
    {
        auto it = outsideNeighborsSet.begin();
        std::advance(it, boundaryPt);
        size_t outsideNeighbor = *it;
        size_t indices[3];
        util::getCellIndices3D(outsideNeighbor, iYTopBorder + 1, iZBackBorder + 1, indices);
        std::vector<int> replacementPops = outsideNeighborsReplacementPops.at(boundaryPt);
    }
    cudaDeviceSynchronize;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    cout << "Devices Synchronized csvs will be closed" << endl;
    normalNeo0.close();
    normalNeo1.close();

    ///////TRIALS END
*///---------------------------HERE
    //std::ofstream oldMethod0("oldmethod0.csv");
    //std::ofstream oldMethod1("oldmethod1.csv");
    //cudaDeviceSynchronize();
    /*
    if (numero == 0)
    {
        oldMethod0 << "Index1, Index2, Index3\n";
    }
    if (numero == 1)
    {
        oldMethod1 << "Index1, Index2, Index3\n";
    }
    */
    size_t localIndices[3];
    /*//old location of slip definition
    if (isInGPU)
    {
        for (int iX = 3; iX <= lattice.getNx() - 3; ++iX)
        { //tempoarary measure
            for (int iY = 2; iY <= lattice.getNy() - 2; ++iY)
            {
                for (int iZ = 2; iZ <= lattice.getNz() - 2; ++iZ)
                {
                    bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
                    if (isFluid == false)
                    {
                        for (unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop)
                        {
                            const int neighbor[3] = {iX + Lattice<T>::c(iPop, 0), iY + Lattice<T>::c(iPop, 1), iZ + Lattice<T>::c(iPop, 2)};
                            bool isNfluid = lattice.getMaskEntry(neighbor[0], neighbor[1], neighbor[2]);
                            if (isNfluid == true)
                            {
                                lattice.defineDynamics(neighbor[0], neighbor[1], neighbor[2], &slipBoundary);
                                size_t index = util::getCellIndex3D(iX, iY, iZ, lattice.getNy(), lattice.getNz());
                                util::getCellIndices3D(index, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], localIndices);
                                Index3D globalIndices = refSubDomain.getGlobalIndex(Index3D{localIndices[0], localIndices[1], localIndices[2], refSubDomain.localSubDomain});

                                if (numero == 0)
                                {
                                    oldMethod0 << globalIndices[0] << " " << globalIndices[1] << " " << globalIndices[2] << "\n";
                                }
                                if (numero == 1)
                                {
                                    oldMethod1 << globalIndices[0] << " " << globalIndices[1] << " " << globalIndices[2] << "\n";
                                }
                            }
                        }
                    }
                }
            }
        }
        std::cout << "Slip Bounds Defined " << std::to_string(domainInfo.localSubDomain) << std::endl;
    }*/

    /*std::cout << "Define ghostlayer to be bulkdynamics but fluidmask false ";
    GhostDynamics<T, Lattice> ghostLayerDynamics(1.0);
    for (unsigned iX=0;iX<domainInfo.getLocalInfo().localGridSize()[0];++iX){
	  for (unsigned iY=0;iY<domainInfo.getLocalInfo().localGridSize()[1];++iY){
		  for (unsigned iZ=0;iZ<domainInfo.getLocalInfo().localGridSize()[2];++iZ) {
			if (iX<domainInfo.getLocalInfo().ghostLayer[0] or iX>=domainInfo.getLocalInfo().localGridSize()[0]-domainInfo.getLocalInfo().ghostLayer[0])
			  lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
			if (iY<domainInfo.getLocalInfo().ghostLayer[1] or iY>=domainInfo.getLocalInfo().localGridSize()[1]-domainInfo.getLocalInfo().ghostLayer[1])
			  lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
			if (iZ<domainInfo.getLocalInfo().ghostLayer[2] or iZ>=domainInfo.getLocalInfo().localGridSize()[2]-domainInfo.getLocalInfo().ghostLayer[2])
			  lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
		  }
      }
    }
   */

    std::cout << "Init GPU data.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    lattice.initDataArrays();
    cudaDeviceSynchronize();
    std::cout << "Finished init GPU!" << std::to_string(domainInfo.localSubDomain) << std::endl;

    //if (isInGPU) ////cancelled if (isInGPU) start
    //{////cancelled if (isInGPU) mid

    //std::ofstream myfile;
    //myfile.open("DeltaVals.csv");
    //myfile << "iXf, iYf, iZf, iXs, iYs, iZs, Xw, Yw, Zw, delta, \n";
/*-----------------------here2
    auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
    auto slipCellIds = slipDataHandler->getCellIDs();
    auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();
    int zMidIndex = ceil((iZBackBorder + 1) / 2);

    //std::ofstream myfile5;
    //myfile5.open("AppliedNormals.csv");
    //myfile5 << "Xf, Yf, Zf, Xn, Yn, Zn, \n";

    for (size_t index : slipDataHandler->getCellIDs())
    {

        size_t p[3];
        util::getCellIndices3D(index, lattice.getNy(), lattice.getNz(), p);
        T latticeFluidLocation[3] = {p[0], p[1], p[2]};

        size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

        size_t nNeighbor = 0;
        int normal[3] = {0, 0, 0};

        for (unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop)
        {
            size_t pN[Lattice<T>::d] = {p[0] + Lattice<T>::c(iPop, 0),
                                        p[1] + Lattice<T>::c(iPop, 1),
                                        p[2] + Lattice<T>::c(iPop, 2)};
            size_t nIndex = util::getCellIndex3D(pN[0], pN[1], pN[2], lattice.getNy(), lattice.getNz());
            T latticeNeighborLocation[3] = {pN[0], pN[1], pN[2]};

            if (lattice.getFluidMask()[nIndex] == false)////I think I need bounceback ?????????????
            {
                T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};
                T delta = calculateDelta(lattice, converter, latticeFluidLocation, latticeNeighborLocation, dir, stlReader); //, myfile);

                for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                {
                    normal[iDim] += Lattice<T>::c(iPop, iDim);
                }

                size_t const idxDir = CurvedSlipBoundaryProcessor3D<T, Lattice>::idxDir(nNeighbor);
                size_t const idxDelta = CurvedSlipBoundaryProcessor3D<T, Lattice>::idxDelta(nNeighbor);
                slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
                slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

                slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T, Lattice>::idxTau()][momentaIndex] =
                    converter.getLatticeRelaxationTime();
                ++nNeighbor;
            }
        }

        std::vector<T> normalT = calculateNormal(lattice, converter, latticeFluidLocation, stlReader);
        T normalAbs = 0;
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            normalAbs += normal[iDim] * normal[iDim];
        }
        normalAbs = std::sqrt(normalAbs);
        for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {

            slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T, Lattice>::idxNormal() + iDim][momentaIndex] = indexandnormal[index][iDim];
        }

        slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T, Lattice>::idxNumNeighbour()][momentaIndex] = (T)nNeighbor;
    }
    //myfile5.close();
    //}////cancelled if (isInGPU) end
*///-------------------------here 2
    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    std::cout << "Init equilibrium.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX)
    {
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T uVel = finalLeftVelocity;//trial
                T vel[3] = {uVel, 0., 0.};
                T rho[1];
                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
            }
        }
    }
    lattice.copyDataToGPU();
    std::cout << "Finished Init equilibrium!" << std::to_string(domainInfo.localSubDomain) << std::endl;

#ifdef ENABLE_CUDA
    initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
    ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
    initalizeCommDataMultilattice(lattice, commDataHandler);
    NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("/hddscratch/ekurban3/FullBBShip/tmp/");
    BlockVTKwriter3D<T> writer("outputVTK_" + std::to_string(domainInfo.localSubDomain));
    //BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
    //BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    BlockVTKwriterMultiLattice3D<T, Lattice<T>> vtkmultiwriter("outputMultiVTK", domainInfo);
    BlockLatticePhysVelocity3D<T, Lattice> multiphysVelocityFunctor(lattice, 0, converter);
    BlockLatticePhysPressure3D<T, Lattice> multiphysPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> multifluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    //writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidmaskfunctor);
    vtkmultiwriter.addFunctor(multiphysVelocityFunctor);
    //writer.addFunctor(forceFunctor);
    vtkmultiwriter.addFunctor(multiphysPressureFunctor);
    vtkmultiwriter.addFunctor(multifluidmaskfunctor);

    int writeixstart = 0;
    int writeixend = xLength - 1;
    int writeiystart = 0;
    int writeiyend = yLength - 1;
    int writeizstart = 0;
    int writeizend = zLength;

    size_t startWRITE[3] = {writeixstart, writeiystart, writeizstart};
    size_t endWRITE[3] = {writeixend, writeiyend, writeizend};

    if (outputVTKData)
    {
        vtkmultiwriter.write(0, 0, startWRITE, endWRITE);
    }
    /*
    T XArea;
    if(isInGPU){
        std::vector<int> numCells = CountFluidCellsAroundBody(lattice, converter, iXRightBorder, iYTopBorder, iZBackBorder);
        T latticeL = converter.getConversionFactorLength();
        XArea = numCells[0]*latticeL*latticeL;
        std::cout << "XArea: " << XArea << std::endl;
    }
 
    */

    unsigned int rampUpSteps = trimTime / 10; //increased to 4 again due to crash-Oct 18 //was 4 decreased to 8 to have more smooth results and less effect of ramping up, if it will not crash it should be fine//18/08/2021
    Timer<T> timer(trimTime, lattice.getNx() * lattice.getNy() * lattice.getNz());
    timer.start();
    std::clock_t start;
    double duration;
    start = std::clock();

    // std::ofstream myfile;
    // std::string filename = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_6s.csv";
    // std::cout << filename << std::endl;
    // myfile.open(filename);
    // myfile << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // std::ofstream myfile2;
    // std::string filename2 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_7s.csv";
    // std::cout << filename2 << std::endl;
    // myfile2.open(filename2);
    // myfile2 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // std::ofstream myfile3;
    // std::string filename3 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_2s.csv";
    // std::cout << filename3 << std::endl;
    // myfile3.open(filename3);
    // myfile3 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // std::ofstream myfile4;
    // std::string filename4 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_9s.csv";
    // std::cout << filename4 << std::endl;
    // myfile4.open(filename4);
    // myfile4 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    //std::ofstream myfilehalfL;
    //std::string filename5 = "VelocityTimeSeries_halfL_NoSLIP_res1_4000.csv";
    //myfilehalfL.open(filename5);
    //myfilehalfL << "t, u, v, w \n";

    //std::ofstream myfile1L;
    //std::string filename6 = "VelocityTimeSeries_1L_NoSLIP_res1_4000.csv";
    //myfile1L.open(filename6);
    //myfile1L << "t, u, v, w \n";

    //std::ofstream myfile2L;
    //std::string filename7 = "VelocityTimeSeries_2L_NoSLIP_res1_4000.csv";
    //myfile2L.open(filename7);
    //myfile2L << "t, u, v, w \n";

    // std::ofstream mydragfile;
    // mydragfile.open("DragvsTimeSlip_res275_long.csv");
    // mydragfile << "SimTime, Trim Step, Drag Coeff, Lift Coeff \n";

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        /*///old ramp-up version start
        
        if ((trimStep % converter.getLatticeTime(0.05)) == 0 && trimStep <= rampUpSteps)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity*((T)trimStep/rampUpSteps);
            std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
            int iX = 0;
            for (int iY = 0; iY <= iYTopBorder; iY++) {
                for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
                    
                    T vel[3] = {uVel, 0, 0};
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
            }
        }
        
        lattice.copyDataToGPU();



        }

        */
        //old ramp-up version end

        if (trimStep == 0)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity;
            int iX = 0;
            for (int iY = 0; iY <= iYTopBorder; iY++)
            {
                for (int iZ = 0; iZ <= iZBackBorder; iZ++)
                {
                    T vel[3] = {uVel, 0, 0}; //needs to be modified when height of the domain is changed
                    Index3D localIndex;
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                    {
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                }
            }

            //start
            for (unsigned iZ = 0; iZ <= iZBackBorder; iZ++)
            {
                T vel[3] = {uVel, 0, 0};
                Index3D localIndex;
                if (check75(xLength - 1, 0, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                {
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                }
                if (check75(xLength - 1, iYTopBorder, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                {
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                }
            }

            for (unsigned iY = 0; iY <= iYTopBorder; iY++)
            {
                T vel[3] = {uVel, 0, 0};
                Index3D localIndex;
                if (check75(xLength - 1, iY, 0, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                {
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                }
                if (check75(xLength - 1, iY, iZBackBorder, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain))
                {
                    lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                }
            }

            ///end

            lattice.copyDataToGPU();
        }

#ifdef ENABLE_CUDA
        collideAndStreamPostStreamUpdateMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamicsNonPostStream<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamicsNonPostStream<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        // if ((trimStep % converter.getLatticeTime(0.01)) == 0)
        // {
        //     timer.update(trimStep);
        //     timer.printStep();
        //     lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

        //     // if (outputVTKData) {
        //     //     lattice.copyDataToCPU();
        //     //     // writer.write(0,iXRightBorder, ceil(0.2*iYTopBorder), ceil(0.8*iYTopBorder), ceil(iZBackBorder/4), ceil(0.75*iZBackBorder), trimStep);
        //     //     writer.write(trimStep, (float) trimStep);
        //     // }
        // }

        /*if (trimStep % 1000 ==0){
            timer.update(trimStep);
            timer.printStep();
            std::cout << "Simulation time " << std::to_string(domainInfo.localSubDomain) << ": " << converter.getPhysTime(trimStep) << std::endl;

            T u[3]; 
            if (check75(ceil(iXRightBorder/4), ceil(iYTopBorder/4), ceil(iZBackBorder/4), localIndex2, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                lattice.get(localIndex2[0], localIndex2[1], localIndex2[2]).computeU(u);
            }
            std::cout << "U for GPU "<< std::to_string(domainInfo.localSubDomain) << ": " << u[0] << std::endl;
        }*/

        // if ((trimStep == converter.getLatticeTime(7)) || (trimStep == converter.getLatticeTime(0)))
        // {
        //     lattice.copyDataToCPU();
        //     writer.write(trimStep, (float) trimStep);
        // }

        // if ((trimStep == converter.getLatticeTime(6)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile, physDensity, physVelocity);
        // }

        // if ((trimStep == converter.getLatticeTime(7)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile2, physDensity, physVelocity);
        // }

        // if ((trimStep == converter.getLatticeTime(2)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile3, physDensity, physVelocity);
        // }
        /*
        if ((trimStep % converter.getLatticeTime(0.00025)) == 0)
        {
            lattice.copyDataToCPU();
            T uhalf[3]; 
            T u1L[3];
            T u2L[3];
            T u3L[3];
            Index3D localuIndex;
            Index3D local1uIndex;
            Index3D local2uIndex;
            Index3D local3uIndex;
            if (domainInfo.getLocalInfo().isLocal(840*scaler, iYTopBorder/2, iZBackBorder/2, localuIndex)){
                lattice.get(localuIndex[0], localuIndex[1], localuIndex[2]).computeU(uhalf);
                myfilehalfL << converter.getPhysTime(trimStep) << "," << uhalf[0] << "," << uhalf[1] << "," << uhalf[2] << "\n";
            }
            if (domainInfo.getLocalInfo().isLocal(997*scaler, iYTopBorder/2, iZBackBorder/2, local1uIndex)){
                lattice.get(local1uIndex[0], local1uIndex[1], local1uIndex[2]).computeU(u1L);
                myfile1L << converter.getPhysTime(trimStep) << "," << u1L[0] << "," << u1L[1] << "," << u1L[2] << "\n";
            }
            if (domainInfo.getLocalInfo().isLocal(1311*scaler, iYTopBorder/2, iZBackBorder/2, local2uIndex)){
                lattice.get(local2uIndex[0], local2uIndex[1], local2uIndex[2]).computeU(u2L);
                myfile2L << converter.getPhysTime(trimStep) << "," << u2L[0] << "," << u2L[1] << "," << u2L[2] << "\n";
            }
           
            // if (isInGPU){
            //     std::vector<T> physForces = getForces(lattice, converter, iXRightBorder, iYTopBorder, iZBackBorder);
            //     T factor = 2./(physDensity*physVelocity*physVelocity);
            //     T Cd = factor*physForces[0]/(XArea);
            //     T Cl = factor*physForces[1]/(XArea);
            //     mydragfile << converter.getPhysTime(trimStep) << ",";
            //     mydragfile << trimStep << ",";
            //     mydragfile << Cd << ",";
            //     mydragfile << Cl << "\n";
            // }
        }
*/
        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            cout << "step=" << trimStep << " t=" << converter.getPhysTime(trimStep) << endl;
            //lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            /*

            if(numero==0){
                    if(outputVTKData){
                    lattice.copyDataToCPU();
                    writer.write(limitsvec0, trimStep);
               }
            }
            if(numero==1){
                if(outputVTKData){
                    lattice.copyDataToCPU();
                    writer.write(limitsvec1, trimStep);
               }
            }
*/

            if (outputVTKData)
            {
                lattice.copyDataToCPU();
                //writer.write(trimStep);
                vtkmultiwriter.write(trimStep, float(trimStep), startWRITE, endWRITE);
            }
        }
    }
    timer.stop();
    timer.printSummary();
    duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
    std::cout << "Total Simulation Time: " << duration << '\n';
}

int main(int argc, char **argv)
{
    int rank = initIPC();
    unsigned int ghostLayerX = 2;
    unsigned int ghostLayerY = 0;
    unsigned int ghostLayerZ = 0;
    unsigned ghostLayer[3] = {ghostLayerX, ghostLayerY, ghostLayerZ};
    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(xLength), (size_t)(yLength), (size_t)(zLength), 0u, 1u, ghostLayer); //cant be an odd number if you have even numbers of gpus
    const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks(), ghostLayer);
    if (rank == 0)
    {
        std::cout << "REF SUBDOMAIN INFO" << std::endl;
        std::cout << refSubDomain;
        std::cout << "Domain Info" << std::endl;
        std::cout << subDomainInfo;
        std::cout << "####" << std::endl;
    }

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl;
    std::cout << "####################################" << std::endl;
    MultipleSteps(commDataHandler, refSubDomain);
    cudaDeviceSynchronize();
    MPI_Finalize();
    return 0;
}
