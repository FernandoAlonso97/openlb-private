/*  This file is part of the OpenLB library
*
*  Copyright (C) 2022 Isabel Fernandez
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
//Have done some different studies using forced or not-forced Smagorinsky BGK scheme 
//#define FORCEDD3Q19LATTICE 1
#define D3Q19LATTICE 1
// typedef double T; //////////
typedef float T; ///////////Float runs faster 

#include "olb3D.h"
#include "olb3D.hh"

#include <cmath>

#include <iostream>
#include <fstream>

#include <cstdio>
#include <ctime>

#include "../../../eigen-3.4.0/Eigen/Eigen"

// #define Lattice ForcedD3Q19Descriptor
#define Lattice D3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;
//using namespace olb::instances;

using namespace Eigen;

//**Simulation Paramters

const double simTime = 8;

T scaler = 1;
///Domain Size Setup
int iXLeftBorder = 0;
int iXRightBorder = ceil(600*scaler);
int iYBottomBorder = 0;
int iYTopBorder = ceil(340*scaler);
int iZFrontBorder = 0;
int iZBackBorder = ceil(260*scaler);

///Phys. Parameters
T physDensity = 1.225; // kg/m^3
T physKinematicViscosity = 4.5e-5;/// m^2/s
T physVelocity = 34.3;//m/s
T maxphysVelocity = 45;//max velocity eexperienced in simulation
T CharLength =  1.0;//
int resolution = 100;//

T smagoConstant = 0.1;

//////////////////////////////Functions callled during Main code (MutlitpleSteps) - Skip to line ~720 for main code//////////////////////////
template <typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice &lattice, Dynamics<T, Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    
    // auto* velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    // auto* velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1> (iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1);
    // static LudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto* pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto* pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1> (iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1);
    static LudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static IniEquilibriumDynamics<T, Lattice> iniEquil{};

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    /////I Use periodic BC on the domain walls. Have also tried with slip  
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, -1>> periodicplane1N;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, 1>> periodicplane1P;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, -1>> periodicplane2N;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, 1>> periodicplane2P;


    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &iniEquil);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &pressureBCDynamics0P);
     lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &periodicplane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &periodicplane1P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &periodicplane2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &periodicplane2P);



    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> slipedge0PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> slipedge0NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> slipedge0PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> slipedge0NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipedge1PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipedge1NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipedge1PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipedge1NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, -1>> slipedge2NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, 1>> slipedge2NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, -1>> slipedge2PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, 1>> slipedge2PP;

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

     lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);


    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;


    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, -1, -1>> slipcornerNNN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, 1, -1>> slipcornerNPN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, -1, 1>> slipcornerNNP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, -1, 1, 1>> slipcornerNPP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipcornerPNN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipcornerPPN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipcornerPNP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipcornerPPP;


    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

}


void writeCellPressuretoFile(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, std::ofstream &myfile, int cell[3], int dir, T physDensity, T physVelocity){

  T celllatticePressure = (lattice.get(cell[0], cell[1], cell[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
  T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
  T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
  myfile << cell[0] << ",";
  myfile << cell[1] << ",";
  myfile << cell[2] << ",";
  myfile << cellPhysPressure << ","; 
  myfile << cellPressureCoeff << ",";
  myfile << dir << "\n";
}



std::vector<int> CountFluidCellsAroundBody(BlockLattice3D<T, Lattice> &lattice,UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder){

std::vector<int> counter {0, 0, 0, 0, 0, 0, 0};

    for (int iX = 1; iX <= iXRightBorder-1; ++iX){
        for (int iY = 1; iY <= iYTopBorder-1; ++iY){
            for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ){
            
            bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
            if (isFluid == false){
 


              int cell0P[3] = {iX+1, iY, iZ};
              int cell0N[3] = {iX-1, iY, iZ};
              int cell1P[3] = {iX, iY+1, iZ};
              int cell1N[3] = {iX, iY-1, iZ};
              int cell2P[3] = {iX, iY, iZ+1};
              int cell2N[3] = {iX, iY, iZ-1};
              
              bool isCell0PFluid = lattice.getMaskEntry(cell0P[0], cell0P[1], cell0P[2]);
              bool isCell0NFluid = lattice.getMaskEntry(cell0N[0], cell0N[1], cell0N[2]);
              bool isCell1PFluid = lattice.getMaskEntry(cell1P[0], cell1P[1], cell1P[2]);
              bool isCell1NFluid = lattice.getMaskEntry(cell1N[0], cell1N[1], cell1N[2]);
              bool isCell2PFluid = lattice.getMaskEntry(cell2P[0], cell2P[1], cell2P[2]);
              bool isCell2NFluid = lattice.getMaskEntry(cell2N[0], cell2N[1], cell2N[2]);

              if (isCell0PFluid){
                 counter[0]++; 
              }
              if (isCell0NFluid){
                 counter[1]++; 
              }
              if (isCell1PFluid){
                 counter[2]++;   
              }
              if (isCell1NFluid){
                 counter[3]++; 
              }
              if (isCell2PFluid){
                 counter[4]++; 
              }
              if (isCell2NFluid){
                 counter[5]++; 
              }

          }
        }
      }
    }
    counter[6] = counter[0]+counter[1]+counter[2]+counter[3]+counter[4]+counter[5];
    return counter;
}


std::vector<T> getForces(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder){
    std::vector<T> physForceOutput {0.0, 0.0, 0.0};
    T physXForce = 0.0;
    T physYForce = 0.0;
    T physZForce = 0.0;

    for (int iX = 2; iX <= iXRightBorder-2; ++iX){
        for (int iY = 2; iY <= iYTopBorder-2; ++iY){
            for (int iZ = 2; iZ <= iZBackBorder-2; ++iZ){
            
            T output[3] = {0, 0, 0};
            bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
            if (isFluid == false){
                for (int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                // Get direction
                // std::cout << "iPop: " << iPop << std::endl;
                int cX = Lattice<T>::c(iPop, 0);
                int cY = Lattice<T>::c(iPop, 1);
                int cZ = Lattice<T>::c(iPop, 2);
                // std::cout << "{cX, cY, cZ} = {" << cX << ", " << cY << ", " << cZ << "}" << std::endl; 

                // Get next cell located in the current direction
                // Check if the next cell is a fluid node
                if (lattice.getMaskEntry(iX + cX, iY + cY, iZ + cZ) == true) {
                    // Get f_q of next fluid cell where l = opposite(q)
                    T f = lattice.get(iX + cX, iY + cY, iZ + cZ)[iPop].get();
                    // T f = lattice.get(iX + cX, iY + cY, iZ + cZ)[util::opposite<Lattice<T>>(iPop)].get();
                    // Get f_l of the boundary cell
                    // Add f_q and f_opp
                    f += lattice.get(iX, iY, iZ)[util::opposite<Lattice<T>>(iPop)];
                    // f += lattice.get(iX, iY, iZ)[iPop];
                    // Update force
                    output[0] -= cX * f;
                    output[1] -= cY * f;
                    output[2] -= cZ * f;
                }
            }

            output[0] = converter.getPhysForce(output[0]);
            output[1] = converter.getPhysForce(output[1]);
            output[2] = converter.getPhysForce(output[2]);
            //std::cout << " iX = " << iX << " iY = " << iY << " iZ = " << iZ << std::endl;
          }
        else{
            output[0] = 0;
            output[1] = 0;
            output[2] = 0;
          }
          physXForce += output[0];
          physYForce += output[1];
          physZForce += output[2];
        }
      }
    }
    physForceOutput[0] = physXForce;
    physForceOutput[1] = physYForce;
    physForceOutput[2] = physZForce;
    return physForceOutput;
}


void GetPressureOnFluidCellsAroundBody(BlockLattice3D<T, Lattice> &lattice,UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder, std::ofstream &myfile, T physDensity, T physVelocity){


    for (int iX = 1; iX <= iXRightBorder-1; ++iX){
        for (int iY = 1; iY <= iYTopBorder-1; ++iY){
            for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ){
            bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
            if (isFluid == false){
            for (int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                // Get direction
                int cX = Lattice<T>::c(iPop, 0);
                int cY = Lattice<T>::c(iPop, 1);
                int cZ = Lattice<T>::c(iPop, 2);
            
                // Get next cell located in the current direction
                // Check if the next cell is a fluid node
                if (lattice.getMaskEntry(iX + cX, iY + cY, iZ + cZ) == true) {
                    int cellNext[3] = {iX + cX, iY + cY, iZ + cZ};
                    int cellS[3]={iX,iY,iZ};
                    int dir = cX + cY + cZ; 
                    T celllatticePressure = (lattice.get(iX, iY, iZ).computeRho() - 1.0) / Lattice<T>::invCs2();
                    T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
                    T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
                    writeCellPressuretoFile(lattice, converter, myfile, cellNext, dir, physDensity, physVelocity);  

                }
            }
            }
            }
        }
    }
}

std::vector<T> calculateDelta(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], T neighborCell[3], T dir[3],  STLreader<double> &stlReader){//std::ofstream &myfile,
    typedef double T;
    auto tree = stlReader.getTree();
    T physFluidLocation[3] = {converter.getPhysLength(slipCell[0]), converter.getPhysLength(slipCell[1]), converter.getPhysLength(slipCell[2])};
    T fracSpacing = 100;
    T dirMag = sqrt(pow(dir[0],2)+pow(dir[1],2)+pow(dir[2],2));
    T dotDel = (converter.getPhysDeltaX()*dirMag)/fracSpacing;
    T physXpoint = converter.getPhysLength(slipCell[0]);//physFluidLocation[0];
    T physYpoint = converter.getPhysLength(slipCell[1]);//physFluidLocation[1];
    T physZpoint = converter.getPhysLength(slipCell[2]);//physFluidLocation[2];
    T Xw;
    T Yw;
    T Zw;
    T physPoint[3];
    for(int i = 1; i<= fracSpacing; ++i ) {
        physPoint[0] = physXpoint; 
        physPoint[1] = physYpoint, 
        physPoint[2] = physZpoint;
        std::vector<T> tmpLoc(physPoint, physPoint+3);
        Xw = physXpoint;
        Yw = physYpoint;
        Zw = physZpoint;
        physXpoint += dir[0]*dotDel;
        physYpoint += dir[1]*dotDel;
        physZpoint += dir[2]*dotDel;
        bool isInside[1];
        T location[3] = {physXpoint, physYpoint, physZpoint};
        stlReader(isInside, location);
        if(isInside[0]){
                break;
        }

    }
    T physWallLocation[3] = {Xw, Yw, Zw};
    T physSolidLocation[3] = {converter.getPhysLength(neighborCell[0]), converter.getPhysLength(neighborCell[1]), converter.getPhysLength(neighborCell[2])};
    T numMagnitude = sqrt(pow((physFluidLocation[0]-physWallLocation[0]),2)+
                                        pow((physFluidLocation[1]-physWallLocation[1]),2)+
                                        pow((physFluidLocation[2]-physWallLocation[2]),2));
    T denMagnitude = sqrt(pow((physFluidLocation[0]-physSolidLocation[0]),2)+
                                        pow((physFluidLocation[1]-physSolidLocation[1]),2)+
                                        pow((physFluidLocation[2]-physSolidLocation[2]),2));

    float delta = numMagnitude/denMagnitude;


    float latticeWallLocation[3] = {Xw/converter.getPhysDeltaX(), Yw/converter.getPhysDeltaX(), Zw/converter.getPhysDeltaX()};
    std::vector<float> output = {latticeWallLocation[0],latticeWallLocation[1] ,latticeWallLocation[2], delta};


    return output;
}

std::vector<double> checkDirection(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, STLreader<double> &stlReader){
    typedef double T;
    T spacing = 1.0/100.0;//100.0;
    T fracSpacing = 100;
    bool correctDirection = true;
    for (int i = 1; i<= fracSpacing; ++i ) {
        vecX += spacing*normalEigenvector[0];
        vecY += spacing*normalEigenvector[1];
        vecZ += spacing*normalEigenvector[2];
        

        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if(isInsideSTL[0]){
            correctDirection = false;
            break;
        }


    }
    if (correctDirection == false){
         for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
            normalEigenvector[iDim] = -normalEigenvector[iDim];
        }
    }
    return normalEigenvector;
}


std::vector<double> checkOrientation(UnitConverter<T, Lattice> converter, T vecX, T vecY, T vecZ, std::vector<double> normalEigenvector, std::vector<double> otherEigenvector,  STLreader<double> &stlReader){
    typedef double T;
    T fracSpacing = 100;
    T dirMag = sqrt(pow(normalEigenvector[0],2)+pow(normalEigenvector[1],2)+pow(normalEigenvector[2],2));
    T spacing = (2.0*converter.getPhysDeltaX()*dirMag)/fracSpacing;

    bool correctOrientation = false;
    for (int i = 1; i<= fracSpacing; ++i ) {
        vecX += -spacing*normalEigenvector[0];
        vecY += -spacing*normalEigenvector[1];
        vecZ += -spacing*normalEigenvector[2];
        
        bool isInsideSTL[1];
        T vecLocation[3] = {vecX, vecY, vecZ};
        stlReader(isInsideSTL, vecLocation);
        if(isInsideSTL[0]){
            correctOrientation = true;
            break;
        }


    }

    if (correctOrientation == false){
         for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
            normalEigenvector[iDim] = otherEigenvector[iDim];
        }
    }
    
    return normalEigenvector;
}


std::vector<double> normalizeNormals(std::vector<double> normalEigenvector){
    T normalAbs = 0;
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
        normalAbs += normalEigenvector[iDim]*normalEigenvector[iDim];
    }
    normalAbs = std::sqrt(normalAbs);
    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
        normalEigenvector[iDim] = normalEigenvector[iDim] / normalAbs;
    }
    return normalEigenvector;
}

std::vector<T> calculateNormalPCA(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], STLreader<double> &stlReader){
    typedef double T;
    T fracSpacing = 100;
    T pointX = slipCell[0];
    T pointY = slipCell[1];
    T pointZ = slipCell[2];
    T normal[3] = {0, 0, 0};
    int numPoints = 0;
    T maxX = 0;
    T maxY = 0;
    T maxZ = 0;
    int p = 0;
    T centroidX = 0;
    T centroidY = 0;
    T centroidZ = 0;
    std::vector<T> pointsX(0);
    std::vector<T> pointsY(0);
    std::vector<T> pointsZ(0);
    std::vector<std::vector<T>> points;
    T covarianceMatrix[3][3];
        
    for (int iPop = 1; iPop < Lattice<T>::q; ++iPop){
        T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)}; 
        T dirMag = sqrt(pow(dir[0],2)+pow(dir[1],2)+pow(dir[2],2));
        T dotDel = (2.0*converter.getPhysDeltaX()*dirMag)/fracSpacing;//(2.0*sqrt(2.0)*converter.getPhysDeltaX()*dirMag)/fracSpacing;
        T physXpoint = converter.getPhysLength(slipCell[0]);//physFluidLocation[0];
        T physYpoint = converter.getPhysLength(slipCell[1]);//physFluidLocation[1];
        T physZpoint = converter.getPhysLength(slipCell[2]);//physFluidLocation[2];
        T Xw = physXpoint;
        T Yw = physYpoint;
        T Zw = physZpoint;
        for(int i = 1; i<= fracSpacing; ++i ) {
            Xw += dir[0]*dotDel;
            Yw += dir[1]*dotDel;
            Zw += dir[2]*dotDel;
            bool isInside[1];
            T location[3] = {Xw, Yw, Zw};
            stlReader(isInside, location);
            if(isInside[0]){
                    ////////////eigenvalue method///////////////
                    centroidX += Xw;
                    centroidY += Yw;
                    centroidZ += Zw;

                    pointsX.push_back(Xw);
                    pointsY.push_back(Yw);
                    pointsZ.push_back(Zw);
                    //////////////
                    numPoints++;
                    normal[0] += (Xw - physXpoint); 
                    normal[1] += (Yw - physYpoint);
                    normal[2] += (Zw - physZpoint);
                
                    break;
                    
            }
        }
             
    }


    T centroid[3] = {centroidX/numPoints, centroidY/numPoints, centroidZ/numPoints};
    points.resize(numPoints, std::vector<T>(3));
    for (unsigned int i = 0; i < numPoints; ++i)
    {
        points[i][0] = pointsX[i];
        points[i][1] = pointsY[i];
        points[i][2] = pointsZ[i];

    }

    for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++) {
			covarianceMatrix[i][j] = 0.0;
			for (int k = 0; k < points.size(); k++){
				covarianceMatrix[i][j] += (centroid[i] - points[k][i]) *(centroid[j] - points[k][j]);
			covarianceMatrix[i][j] /= points.size();// - 1;
            }
		}
    }
    Matrix3d COV; 
    for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++) {  
            COV(i,j) = covarianceMatrix[i][j]; 
        }
    }

    EigenSolver<MatrixXd> solution(COV); 
    VectorXd D = solution.eigenvalues().real(); 
    MatrixXd V = solution.eigenvectors().real();

    T minEig = D.minCoeff();
    T maxEig = D.maxCoeff();
    std::vector<double> eigenvalues;
    std::vector<std::vector<T>> eigenVectorMatrix;
    eigenvalues.resize(D.size());
    eigenVectorMatrix.resize(D.size(), std::vector<T>(3));
    int index;
    int maxIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        eigenvalues[i] = D(i);
    }

    for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++) {  
            eigenVectorMatrix[i][j] = V(i,j); 
        }
    }
    bool found = false;
 
    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == minEig)
        {
            index = i;
            found = true;
            break;
        }       
    
    
    }

    bool foundMax = false;
 
    for (int i = 0; i < eigenvalues.size(); i++)
    {
        if (eigenvalues[i] == maxEig)
        {
            maxIndex = i;
            foundMax = true;
            break;
        }       
    
    
    }
  
    std::vector<T> normalEigenvector;
    normalEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        normalEigenvector[i] = eigenVectorMatrix[i][index];
    }
    int otherIndex1;
    int otherIndex2;
    if (index == 0){
        otherIndex1 = 1;
        otherIndex2 = 2;
    }
    if (index == 1){
        otherIndex1 = 0;
        otherIndex2 = 2;
    }
    if (index == 2){
        otherIndex1 = 0;
        otherIndex2 = 1;
    }
    int midIndex;
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        if (i != index && i != maxIndex){
            midIndex = i;
        } 
    }

    std::vector<T> otherEigenvector0;
    otherEigenvector0.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector0[i] = eigenVectorMatrix[i][index];
    }

    std::vector<T> otherEigenvector1;
    otherEigenvector1.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector1[i] = eigenVectorMatrix[i][otherIndex1];
    }

    std::vector<T> otherEigenvector2;
    otherEigenvector2.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        otherEigenvector2[i] = eigenVectorMatrix[i][otherIndex2];
    }

    std::vector<T> maxEigenvector;
    maxEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        maxEigenvector[i] = eigenVectorMatrix[i][maxIndex];
    }

    std::vector<T> midEigenvector;
    midEigenvector.resize(D.size());
    for (unsigned int i = 0; i < D.size(); ++i)
    {
        midEigenvector[i] = eigenVectorMatrix[i][midIndex];
    }

    //////////////normalize normals//////////////////////////
    normalEigenvector = normalizeNormals(normalEigenvector);
    otherEigenvector1 = normalizeNormals(otherEigenvector1);
    otherEigenvector2 = normalizeNormals(otherEigenvector2);
    maxEigenvector = normalizeNormals(maxEigenvector);
    midEigenvector = normalizeNormals(midEigenvector);
    ///////////////////////////////////////////////////////


    T vecX = converter.getPhysLength(slipCell[0]);
    T vecY = converter.getPhysLength(slipCell[1]);
    T vecZ = converter.getPhysLength(slipCell[2]);
    T spacing = 1.0/100.0;

    ///////Series of direction and orientation checks//////////////////////////
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, midEigenvector, stlReader); 
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);
    normalEigenvector = checkOrientation(converter, vecX, vecY, vecZ, normalEigenvector, maxEigenvector, stlReader);
    normalEigenvector = checkDirection(converter, vecX, vecY, vecZ, normalEigenvector, stlReader);


    // myfile << normalEigenvector[0] << "," << normalEigenvector[1] << "," << normalEigenvector[2] << ",";
    // myfile << otherEigenvector1[0] << "," << otherEigenvector1[1] << "," << otherEigenvector1[2] << ",";
    // myfile << otherEigenvector2[0] << "," << otherEigenvector2[1] << "," << otherEigenvector2[2] << "\n";

    float output0 = normalEigenvector[0];
    float output1 = normalEigenvector[1];
    float output2 = normalEigenvector[2];

    std::vector<float> output = {output0, output1, output2};
    return output;               
}

void MultipleSteps()
{

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), CharLength, maxphysVelocity, physKinematicViscosity, physDensity, 0);

  
    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();
    LudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... ";
    BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

    std::cout << "Finished!" << std::endl;
    std::vector<int> limits = {iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZBackBorder};

    std::cout << "Define boundaries.... ";
    defineBoundaries(lattice, bulkDynamics, limits);

    static NoDynamics<T, Lattice> noDynamics;
    static BounceBack<T, Lattice> bb;

    /////The boundary conditions are implemented in the PostProcessingDynamics class - see below. The CurvedSlip is the MLS boundary. The others are implemented as well. 
    static PostProcessingDynamics<T,Lattice,CurvedSlipBoundaryProcessor3D<T,Lattice>> slipBoundary;
    // static PostProcessingDynamics<T,Lattice,CurvedBouzidiBoundaryProcessor3D<T,Lattice>> slipBoundary;
    // static PostProcessingDynamics<T,Lattice,CurvedBouzidiQuadraticBoundaryProcessor3D<T,Lattice>> slipBoundary;
    // static PostProcessingDynamics<T,Lattice,CurvedYuBoundaryProcessor3D<T,Lattice>> slipBoundary;
    // static PostProcessingDynamics<T,Lattice,BounceBackMovingBoundaryBoundaryProcessor3D<T,Lattice>> slipBoundary;

    //////////Implementing ROBIN body ///////////////////////
    //To calculate accurate delta value, voxel size needs to be small enough to capture wall location 
    STLreader<double> stlReader( "ROBIN_reoriented.STL", converter.getConversionFactorLength()/12, 0.001*scaler);
    stlReader.print();

      for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                double iXPhys = converter.getPhysLength(iX);
                double iYPhys = converter.getPhysLength(iY);
                double iZPhys = converter.getPhysLength(iZ);
                double location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);
                if (isInside[0]) {
                    lattice.defineDynamics(iX, iY, iZ, &bb);
                }
            }
        }
    }



////////////////////////////////Implement Slip BC///////////////////////////////////////////////////////////////
//int NslipBoundaries = 0;
    for (int iX = 1; iX <= iXRightBorder-1; ++iX){
        for (int iY = 1; iY <= iYTopBorder-1; ++iY){
            for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ){
            
           
            bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
            if (isFluid == false){
                for(unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop){
                    const int neighbor[3] = {iX+Lattice<T>::c(iPop,0), iY+Lattice<T>::c(iPop,1), iZ+Lattice<T>::c(iPop,2)};
                    bool isNfluid = lattice.getMaskEntry(neighbor[0], neighbor[1], neighbor[2]);
                    if(isNfluid == true){
                           lattice.defineDynamics(neighbor[0], neighbor[1], neighbor[2], &slipBoundary); 
                           //NslipBoundaries++;
                    }
                }
         }
        }
      }
    }
/////////////////////////////////////////////////////////////////////////////////////////////////////////



    std::cout << "Init GPU data.... ";
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

//////////Apply Slip BC Parameters////////////////////////////////////////////////////////


    auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
    auto slipCellIds = slipDataHandler->getCellIDs();
    auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();
    auto tree = stlReader.getTree();
    

     int NSlipLoops=0;

     int zMidIndex = ceil(iZBackBorder/2);
     std::cout << "Zmid: " << zMidIndex << std::endl;

    ofstream outputfilee;
    outputfilee.open("index_for_slip.txt");
    

    for(size_t index : slipDataHandler->getCellIDs()) {
        NSlipLoops++;
        cout<<index<<endl;
        outputfilee<<index<<endl;

        size_t p[3];
        util::getCellIndices3D(index,lattice.getNy(),lattice.getNz(), p);
        T latticeFluidLocation[3] = {p[0], p[1], p[2]};

        size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

        size_t nNeighbor = 0;
        T normal[3] = {0,0,0};


        for(unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            size_t pN[Lattice<T>::d] = {p[0]+Lattice<T>::c(iPop,0),
                    p[1]+Lattice<T>::c(iPop,1),
                    p[2]+Lattice<T>::c(iPop,2) };
            size_t nIndex = util::getCellIndex3D(pN[0],pN[1],pN[2],lattice.getNy(),lattice.getNz());
            T latticeNeighborLocation[3] = {pN[0], pN[1], pN[2]};


            if(lattice.getFluidMask()[nIndex] == false) {
                T dir[3] = {Lattice<T>::c(iPop,0),Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2)};

                
                std::vector<T> deltaData = calculateDelta(lattice, converter, latticeFluidLocation, latticeNeighborLocation, dir,  stlReader);//myfile,
                T delta = deltaData[3];
                T latticeWallLocation[3] = {deltaData[0],deltaData[1],deltaData[2]};


                for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
                    normal[iDim] += Lattice<T>::c(iPop,iDim);
                }

                size_t const idxDir = CurvedSlipBoundaryProcessor3D<T,Lattice>::idxDir(nNeighbor);
                size_t const idxDelta = CurvedSlipBoundaryProcessor3D<T,Lattice>::idxDelta(nNeighbor);
                slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
                slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

                slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxTau()][momentaIndex] =
                                        converter.getLatticeRelaxationTime();
                ++nNeighbor;
            }
        }

        std::vector<T> normalPCA = calculateNormalPCA(lattice, converter, latticeFluidLocation, stlReader);
        T normalAbs = 0;

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
            normalAbs += normal[iDim]*normal[iDim];
        }
        normalAbs = std::sqrt(normalAbs);

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){

                slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNormal()+iDim][momentaIndex] = -normal[iDim]/normalAbs;
                //slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNormal()+iDim][momentaIndex] = normalPCA[iDim];
                


        }


        
      slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T,Lattice>::idxNumNeighbour()][momentaIndex] = (T) nNeighbor;
        
    }
    
   
/////////////////////////////////////////////////////////////////////////////////////////////////////////////


    std::cout << "Init equilibrium.... ";
    for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
        }
    }



    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

    unsigned int trimTime = converter.getLatticeTime(simTime);

    std::cout << "Creating VTK functors..." << std::endl;

    singleton::directories().setOutputDir("/home/ekurban3/Documents/isabeltest/tmp/");
    BlockVTKwriter3D<T> writer("outputVTK");
    BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);
    BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);

    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidMaskFunctor);
    writer.addFunctor(densityFunctor);

    std::cout << "Finished!" << std::endl;

    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();

    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    unsigned int rampUpSteps = trimTime/8;

    //////////////////For the drag/force calculations, this gives an approximation for the cross sectional area by counting number of cells facing a certain direction
    std::vector<int> numCells = CountFluidCellsAroundBody(lattice, converter, iXRightBorder, iYTopBorder, iZBackBorder);
    T latticeL = converter.getConversionFactorLength();
    T XArea = numCells[0]*latticeL*latticeL;
    T YArea = (numCells[2]+numCells[3])*latticeL*latticeL;
    T ZArea = (numCells[4]+numCells[5])*latticeL*latticeL;
    std::cout << "XArea: " << XArea << std::endl;
    std::cout << "YArea: " << YArea << std::endl;
    std::cout << "ZArea: " << ZArea << std::endl;
    
    //////////////For outputting drag to file
    //std::ofstream mydragfile;
    //mydragfile.open("DragvsTime.csv"); 
    //mydragfile << "SimTime, Trim Step, Drag Coeff, Lift Coeff \n";
    
    ////////////////////////////////////////////////////////////////////////////
    T pressureCollectionTime1 = 10.0; //s
    T vtkOutputInterval = 2.0;  /// s
    T dragDataOutputInterval = 0.01; ///s

    ////////////////Starting simulation - velocity is rampsed up////////////////
    for (unsigned int trimStep = 0; trimStep <= trimTime; ++trimStep)
    {
        if ((trimStep % converter.getLatticeTime(0.05)) == 0 && trimStep <= rampUpSteps) {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity*((T)trimStep/rampUpSteps);
            std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
            int iX = 0;
            for (int iY = 1; iY <= iYTopBorder-1; ++iY) {
                for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ) {
                    
                    T vel[3] = {uVel, 0.0, 0.0};
                    lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                    lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                }
            }

            lattice.copyDataToGPU();
        }
        lattice.collideAndStreamGPU<LudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        
        //////////////Outputing Data////////////
        /*if (trimStep % converter.getLatticeTime(vtkOutputInterval) ==0){
            lattice.copyDataToCPU();
             writer.write(trimStep);
        }*/

        if (trimStep == converter.getLatticeTime(pressureCollectionTime1))
        {

            std::ofstream myfile;
            myfile.open("PressureOutputFile.csv"); 
            myfile << "iX, iY, iZ, physPressure, pressureCoeff, dir \n";
            GetPressureOnFluidCellsAroundBody(lattice, converter, iXRightBorder, iYTopBorder, iZBackBorder, myfile, physDensity, physVelocity);
            myfile.close();
        }

        if (trimStep % converter.getLatticeTime(dragDataOutputInterval) ==0){
            lattice.copyDataToCPU();
            std::vector<T> physForces = getForces(lattice, converter, iXRightBorder, iYTopBorder, iZBackBorder);
            T factor = 2./(physDensity*physVelocity*physVelocity);
            T Cd = factor*physForces[0]/(XArea);
            T Cl = factor*physForces[1]/(XArea);
            //mydragfile << converter.getPhysTime(trimStep) << ",";
            //mydragfile << trimStep << ",";
            //mydragfile << Cd << ",";
            //mydragfile << Cl << "\n";
        }

        ///////////////Time Step Output///////////////
        if (trimStep % 1000 ==0){
            
            timer.update(trimStep);
            timer.printStep();
            std::cout << "Simulation time: " << converter.getPhysTime(trimStep) << std::endl;

            ///////Debugging///////
            // lattice.copyDataToCPU();
            // T u[3]; 
            // lattice.get(ceil(iXRightBorder/4), ceil(iYTopBorder/4), ceil(iZBackBorder/4)).computeU(u);
            // std::cout << "U: " << u[0] << std::endl;

        }
        

    }

    timer.stop();
    timer.printSummary();
}



int main(int argc, char **argv)
{
    std::clock_t start;
    double duration;
    start = std::clock();
    MultipleSteps();
    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    std::cout<<"Time: "<< duration <<'\n';
    return 0;
}
