/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
// #define D3Q19LATTICE 1
typedef float T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"

#define Lattice ForcedD3Q19Descriptor
// #define Lattice D3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.1;
//const T domainWidth = 0.4064;
const double simTime = 9;
// unsigned int resolution = 448;
// unsigned int xExtraResolution = 2*resolution;
const int XlengthBase = 800;//880;//728;
const int YlengthBase = 340;//260;//580;//340;
const int ZlengthBase = 260;//105;//260;
const T scaler = 3;

// const T gridSpacing = domainWidth/resolution;
// const T gridArea = pow(gridSpacing,2);

// const T physInducedVelocity = 50.0; // m/s
const T physVelocity = 34.3;  
const T maxPhysVelocity = 45;           //m/s
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 4.5e-5; // m^2/s
T CharLength =  1.0;//2.1;//3.5;//0.717169;//
int resolution = 100*CharLength;//*scaler;//350;

/////
const int XboxLeft = 340;
const int XboxRight = 390;
const int YboxBottom = 40;
const int YboxTop = 60;
const int ZboxFront = 4;
const int ZboxBack = 129;
/////////////





/* Output Parameters */
bool outputVTKData = true;
// bool outputRotorData = false;
// bool outputDebugData = false;

const T vtkWriteInterval = 1; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    T omega = dynamics.getOmega();


    auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> slipedge0PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> slipedge0NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> slipedge0PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> slipedge0NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipedge1PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipedge1NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipedge1PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipedge1NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, -1>> slipedge2NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, 1>> slipedge2NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, -1>> slipedge2PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, 1>> slipedge2PP;

    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;

    static IniEquilibriumDynamics<T, Lattice> iniEquil{};


    // static PostProcessingDynamics<T,Lattice, CurvedSlipBoundaryProcessor3D<T, Lattice>> slipBoundary;

    // static iniEquilibriumDynamics<T, Lattice> ini{};



    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipcornerPNN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipcornerPPN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipcornerPNP;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipcornerPPP;


    
        for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder -1; iY++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex)){
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
            }
            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics0P);
            
            // if (domainInfo.isLocal(iXLeftBorder+1, iY, iZ, localIndex))
            //     lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &ini);
            
            // if (domainInfo.isLocal(iXLeftBorder+2, iY, iZ, localIndex))
            //    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &ini);
            }
        }

        // for (unsigned iY = iYBottomBorder; iY <= iYTopBorder; iY++) {
        //         for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++) {
        //             Index3D localIndex;
        //             if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
        //                 lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);

        //         }
        // }
    
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1P);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);
            }
        }

        
        // for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder - 1; iX++) {
        // for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder -1; iZ++) {
        //     Index3D localIndex;
        //     if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
        //         lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N); //+1 x left // +1 x right // +1 z front // +1 z back
        //     }
        // }
        
        
        
        
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
        for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder-1; iY++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2P);
            }
        }

        OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
        createInterpBoundaryCondition3D<T, Lattice,
                                        ForcedLudwigSmagorinskyBGKdynamics>(lattice);

        //?????????????????
        //boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, omega);//do it like others keep x1, x1, y1, y1, z1, z1 --skeptical it will work as it supposed to be
        //boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, omega);
        //boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);                                

        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge2PP);
                // boundaryCondition->addExternalVelocityEdge2PP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
            if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex))   
                boundaryCondition->addExternalVelocityEdge2NP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //????????????????
            if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge2NN); 
            if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))   
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge2PN); 
                boundaryCondition->addExternalVelocityEdge2PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
        }




       for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder-1; iY++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge1NP); //1NP
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); 
            if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge1PP);   //1PP
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
            if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
                boundaryCondition->addExternalVelocityEdge1PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);  //????????????????????/
            if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
                boundaryCondition->addExternalVelocityEdge1NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //??????????????????????
        } 
    

    
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0PN); //0PN
            if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0PP);   //0PP
            if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0NN);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0NP);
        } 
    
        
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNPN(localIndex[0], localIndex[1], localIndex[2], omega); //***
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNPP(localIndex[0], localIndex[1], localIndex[2], omega);  //**
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNNP(localIndex[0], localIndex[1], localIndex[2], omega); //***
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNNN(localIndex[0], localIndex[1], localIndex[2], omega);  //**
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); 
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); 
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); 
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); 


        // //////////////////////////////////////////////////
        // // static NoDynamics<T, Lattice> noDynamics;
        // for (unsigned iX = XboxLeft; iX <= XboxRight; iX++) {
        //     for (unsigned iY = YboxBottom; iY <= YboxTop; iY++) {
        //         for (unsigned iZ = ZboxFront; iZ <= ZboxBack; iZ++) {
        //             Index3D localIndex;
        //             if (domainInfo.isLocal(iX, iY, iZ, localIndex)){
        //                 lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        //                 // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &noDynamics);
        //             }
        //         }
        //     }
        // }
        // ///////////////////////////////////////////

                    

    STLreader<T> stlReader( "ROBIN_reoriented.STL", converter.getConversionFactorLength()/3, 0.001*scaler,0,true);
    stlReader.print();

    // static NoDynamics<T, Lattice> noDynamics;


    for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T iXPhys = converter.getPhysLength(iX);
                T iYPhys = converter.getPhysLength(iY);
                T iZPhys = converter.getPhysLength(iZ);
                T location[3] = {iXPhys, iYPhys, iZPhys};
                
                bool isInside[1];
                stlReader(isInside, location);
                // for (int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                //     // Get direction
                //     int cX = Lattice<T>::c(iPop, 0);
                //     int cY = Lattice<T>::c(iPop, 1);
                //     int cZ = Lattice<T>::c(iPop, 2);
                //     T iXNPhys = converter.getPhysLength(iX+cX);
                //     T iYNPhys = converter.getPhysLength(iY+cY);
                //     T iZNPhys = converter.getPhysLength(iZ+cZ);
                //     T locationN[3] = {iXNPhys, iYNPhys, iZNPhys};
                //     bool isNInside[1];
                //     stlReader(isNInside, locationN);
                //     if (isInside[0] && isNInside[0])
                // }
                if (isInside[0])
                {
                    // if (domainInfo.isLocal(iX+2*68, iY+2*64, iZ+2*19, localIndex)){
                    if (domainInfo.isLocal(iX, iY, iZ, localIndex)){
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                        // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &noDynamics);
                    }
                    if (iX == 0 || iX == iXRightBorder || iY == 0 || iY == iYTopBorder || iZ == 0 || iZ == iZBackBorder){
                        std::cout << "Bounce-Back assigned to slip border" << std::endl;
                        // return;
                    }
                }
            }
        }
    }






    
    
}



double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}


//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
bool check75(int iX, int iY, int iZ, Index3D& localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

// void writeCellPressuretoFile(Index3D& localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter, std::ofstream &myfile, int cell[3], int dir, T physDensity, T physVelocity){
// if (check75(cell[0], cell[1], cell[2], localIndex, lattice, domainInfo, refDomain)){
//   T celllatticePressure = (lattice.get(cell[0], cell[1], cell[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
//   T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
//   T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
//   myfile << cell[0] << ",";
//   myfile << cell[1] << ",";
//   myfile << cell[2] << ",";
//   myfile << cellPhysPressure << ","; 
//   myfile << cellPressureCoeff << ",";
//   myfile << dir << "\n";
// }
// }

void GetPressureOnFluidCellsAroundBody(Index3D& localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder, std::ofstream &myfile, T physDensity, T physVelocity){

    for (int iX = 1; iX <=iXRightBorder; ++iX){
        for (int iY = 1; iY <= iYTopBorder; ++iY){
            for (int iZ = 1; iZ <= iZBackBorder; ++iZ){
            Index3D localIndex1;
            Index3D localIndex2;
            if (domainInfo.isLocal(iX, iY, iZ, localIndex1)){
            bool isFluid = lattice.getMaskEntry(localIndex1[0], localIndex1[1], localIndex1[2]);
            if (isFluid == false){
                // myfile <<  iX << "," << iY << "," << iZ <<"\n" ;
                //  myfile <<  localIndex1[0] << "," << localIndex1[1] << "," << localIndex1[2] <<"\n" ;
                for (int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                // Get direction
                int cX = Lattice<T>::c(iPop, 0);
                int cY = Lattice<T>::c(iPop, 1);
                int cZ = Lattice<T>::c(iPop, 2);
                if (domainInfo.isLocal(iX+cX, iY+cY, iZ+cZ, localIndex2)){
                bool isNFluid = lattice.getMaskEntry(localIndex2[0], localIndex2[1], localIndex2[2]);
                if (isNFluid == true){
                    T celllatticePressure = (lattice.get(localIndex2[0], localIndex2[1], localIndex2[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
                    T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
                    T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
                    myfile <<  iX+cX << "," << iY+cY << "," << iZ+cZ << "," << cellPhysPressure << "," << cellPressureCoeff << "\n" ;
                }

                }

                }
            }
            }
        }
        }
        }

    // for (int iX = 1; iX <= iXRightBorder-1; ++iX){
    //     for (int iY = 1; iY <= iYTopBorder-1; ++iY){
    //         for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ){
    //         if (check75(iX, iY, iZ, localIndex, lattice, domainInfo, refDomain)){   
    //         bool isFluid = lattice.getMaskEntry(iX, iY, iZ);
    //         // bool isFluid = lattice.getMaskEntry(localIndex[0], localIndex[1], localIndex[2]);
            
    //         if (isFluid == false){
    //         for (int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
    //             // Get direction
    //             int cX = Lattice<T>::c(iPop, 0);
    //             int cY = Lattice<T>::c(iPop, 1);
    //             int cZ = Lattice<T>::c(iPop, 2);
            
    //             // Get next cell located in the current direction
    //             // Check if the next cell is a fluid node
    //             if (check75(iX+cX, iY+cY, iZ+cZ, localIndex, lattice, domainInfo, refDomain)){ 
    //             if (lattice.getMaskEntry(iX + cX, iY + cY, iZ + cZ) == true) {
    //             // if (lattice.getMaskEntry(localIndex[0], localIndex[1], localIndex[2]) == true) {
    //                 int cellNext[3] = {iX + cX, iY + cY, iZ + cZ};
    //                 int cellS[3]={iX,iY,iZ};
    //                 int dir = cX + cY + cZ; 
    //                 // T celllatticePressure = (lattice.get(iX, iY, iZ).computeRho() - 1.0) / Lattice<T>::invCs2();
    //                 // T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
    //                 // T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
    //                 //writeCellPressuretoFile(lattice, converter, myfile, cellNext, dir, physDensity, physVelocity);  
    //                 T celllatticePressure = (lattice.get(cellNext[0], cellNext[1], cellNext[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
    //                 // T celllatticePressure = (lattice.get(localIndex[0], localIndex[1], localIndex[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
    //                 T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
    //                 T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
    //                 myfile << localIndex[0] << ",";
    //                 myfile << localIndex[1] << ",";
    //                 myfile << localIndex[2] << ",";
    //                 myfile << cellPhysPressure << ","; 
    //                 myfile << cellPressureCoeff << ",";
    //                 myfile << dir << "\n";

    //             }
    //             }
    //         }
    //         }
    //         }
    //         }
    //     }
    // }
}




template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;


    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), CharLength, maxPhysVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;
    Index3D localIndex1;
    
    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::cout << "Define boundaries.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain, converter);
    std::cout << "Define boundaries Finished. " << std::to_string(domainInfo.localSubDomain) << std::endl;
    
    std::string directory = "/data/ae-jral/ifernandez31/scale3Test1"; 
    // std::string directory = "./tmp/";  
    singleton::directories().setOutputDir(directory);
    BlockVTKwriter3D<T> writer("outputVTK_"+std::to_string(domainInfo.localSubDomain));
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidmaskfunctor);

    // lattice.copyDataToCPU();
    // writer.write(0,iXRightBorder, 0, iYTopBorder, 0, iZBackBorder, 0);
    writer.write(0,iXRightBorder, ceil(0.2*iYTopBorder), ceil(0.8*iYTopBorder), ceil(iZBackBorder/4), ceil(0.75*iZBackBorder), 0);
    return;
    


    std::cout << "Init GPU data.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished Init GPU data!" << std::to_string(domainInfo.localSubDomain) << std::endl;

    std::cout << "Init equilibrium.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                // if (domainInfo.getLocalInfo().isLocal(iX, iY, iZ, localIndex1)){
                //     lattice.defineRhoU(localIndex1[0], localIndex1[0], localIndex1[1], localIndex1[1], localIndex1[2], localIndex1[2], 1.0, vel);
                //     lattice.iniEquilibrium(localIndex1[0], localIndex1[0], localIndex1[1], localIndex1[1], localIndex1[2], localIndex1[2], 1.0, vel);
                // }
            }
        }
    }
    lattice.copyDataToGPU();
    std::cout << "Finished init equilibrium!" << std::to_string(domainInfo.localSubDomain) << std::endl;

    // lattice.copyDataToCPU();
    // return;

#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
  ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice, commDataHandler);
  NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    std::cout << "Finished initalizaCommDataMultilatticeGPU" << std::to_string(domainInfo.localSubDomain) << std::endl;
    unsigned int trimTime = converter.getLatticeTime(simTime);

    // std::string directory = "/data/ae-jral/ifernandez31/TestScale"; 
    // // std::string directory = "./tmp/";  
    // singleton::directories().setOutputDir(directory);
    // BlockVTKwriter3D<T> writer("outputVTK_"+std::to_string(domainInfo.localSubDomain));
    // BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    // BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    // BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    // BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    // BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    // writer.addFunctor(velocityFunctor);
    // writer.addFunctor(physVelocityFunctor);
    // writer.addFunctor(forceFunctor);
    // writer.addFunctor(physPressureFunctor);
    // writer.addFunctor(fluidmaskfunctor);

    // lattice.copyDataToCPU();
    // return;
    
    // int ixmin = 0;
    // int ixmax = iXRightBorder;
    // int iymin = 0;
    // int iymax = iYTopBorder;
    // int izmin = iZBackBorder/2-4;
    // int izmax = iZBackBorder/2+4; 

    std::cout << "X: " << lattice.getNx() << std::endl;
    std::cout << "Y: " << lattice.getNy() << std::endl;
    std::cout << "Z: " << lattice.getNz() << std::endl;


      
      
    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    unsigned int rampUpSteps = trimTime / 8; //increased to 4 again due to crash-Oct 18 //was 4 decreased to 8 to have more smooth results and less effect of ramping up, if it will not crash it should be fine//18/08/2021
    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();

    // std::ofstream myfile;
    // std::string filename = "Indexing"+std::to_string(domainInfo.localSubDomain)+".csv"; 
    // std::cout << filename << std::endl;
    // myfile.open(filename); 
    // myfile << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // Index3D localIndex1;

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        if ((trimStep % converter.getLatticeTime(0.1)) == 0 && trimStep <= rampUpSteps)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity;//*((T)trimStep/rampUpSteps);
            // std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
            int iX = 0;
            // for (int iX = 0; iX <= 3; iX++){
            for (int iY = 1; iY <= iYTopBorder; iY++) {
                for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
                    
                    T vel[3] = {uVel, 0, 0};
                    // if (check75(iX, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                    if (domainInfo.getLocalInfo().isLocal(iX, iY, iZ, localIndex)){
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
                }
            }
        // }
        
            lattice.copyDataToGPU();



        }



#ifdef ENABLE_CUDA
        collideAndStreamPostStreamUpdateMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        // lattice.copyDataToCPU();
        // return;

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));
            

            if (outputVTKData) {
                lattice.copyDataToCPU();
                // writer.write(0,iXRightBorder, 0, iYTopBorder, ceil(iZBackBorder/4), ceil(0.75*iZBackBorder), trimStep);
                writer.write(0,iXRightBorder, 0, iYTopBorder, 0, iZBackBorder, trimStep);
            }
        }
        // if ((trimStep == converter.getLatticeTime(1)))
        // {
        //     timer.update(trimStep);
        //     timer.printStep();
        //     lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));
        //     // T Utest = lattice.get(iXRightBorder/2, iYTopBorder/2, iZBackBorder/2).computeU();
        //     // std::cout << Utest << std::endl;
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile, physDensity, physVelocity);

        // }


        
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    int rank = initIPC();
    cout<< "control 1********************************************************************************************************************************************************************************************************"<< endl;
    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(ceil(XlengthBase*scaler)), (size_t)(ceil(YlengthBase*scaler)), (size_t)(ceil(ZlengthBase*scaler)), 0u, 1u);//cant be an odd number if you have even numbers of gpus
    cout<< "control 2********************************************************************************************************************************************************************************************************"<< endl;
    const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks());
    cout<< "control 3********************************************************************************************************************************************************************************************************"<< endl;
    if (rank == 0) {
        std::cout << "REF SUBDOMAIN INFO" << std::endl;
        std::cout << refSubDomain;
        std::cout << "Domain Info" << std::endl;
        std::cout << subDomainInfo;
        std::cout << "####" << std::endl;
    }

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl;
    std::cout << "####################################" << std::endl;


    MultipleSteps(commDataHandler, refSubDomain);

    cudaDeviceSynchronize();
    MPI_Finalize();
    return 0;
}
