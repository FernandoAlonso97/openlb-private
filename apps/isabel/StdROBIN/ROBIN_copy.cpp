/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
// #define D3Q19LATTICE 1
typedef float T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#define Lattice ForcedD3Q19Descriptor
// #define Lattice D3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.1;
//const T domainWidth = 0.4064;
const double simTime = 0.05; //9s
// unsigned int resolution = 448;
// unsigned int xExtraResolution = 2*resolution;
const int XlengthBase = 600;//800 should be adequate;//880;//850;//728;
const int YlengthBase = 340;//520;//580//340;
const int ZlengthBase = 260;//210;
const T scaler = 1;//0.25;//2;
const int Xfactor = 0;//225*scaler;
const int Yfactor = 0;

// const T gridSpacing = domainWidth/resolution;
// const T gridArea = pow(gridSpacing,2);

// const T physInducedVelocity = 50.0; // m/s
const T physVelocity = 34.3;  
const T maxPhysVelocity = 45;           //m/s
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 4.5e-5; // m^2/s
T CharLength =  1.0;//2.1;//3.5;//0.717169;//
int resolution = 100*CharLength;//350;


std::string directory = "/data/ae-jral/ifernandez31/multitest/";


/* Output Parameters */
bool outputVTKData = true;//false;//
// bool outputRotorData = false;
// bool outputDebugData = false;

const T vtkWriteInterval = 0.01; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter, std::ofstream &myfile)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    initGhostLayer(domainInfo, lattice);

    T omega = dynamics.getOmega();


    auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, -1>> periodicplane1N;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, 1>> periodicplane1P;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, -1>> periodicplane2N;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, 1>> periodicplane2P;

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> slipedge0PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> slipedge0NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> slipedge0PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> slipedge0NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipedge1PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipedge1NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipedge1PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipedge1NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, -1>> slipedge2NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, 1>> slipedge2NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, -1>> slipedge2PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, 1>> slipedge2PP;

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    static IniEquilibriumDynamics<T, Lattice> iniEquil{};



    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipcornerPNN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipcornerPPN;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipcornerPNP;
    // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipcornerPPP;


    
        for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder -1; iY++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex)){
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &iniEquil);
            }
            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics0P);
            }
        }
    
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane1P);//&slip1P);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane1N);//&slip1N);
            }
        }

        
        // for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder - 1; iX++) {
        // for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder -1; iZ++) {
        //     Index3D localIndex;
        //     if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
        //         lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N); //+1 x left // +1 x right // +1 z front // +1 z back
        //     }
        // }
        
        
        
        
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
        for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder-1; iY++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane2N);//&slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &periodicplane2P);//&slip2P);
            }
        }

        OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
        createInterpBoundaryCondition3D<T, Lattice,
                                        ForcedLudwigSmagorinskyBGKdynamics>(lattice);

        //?????????????????
        //boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, omega);//do it like others keep x1, x1, y1, y1, z1, z1 --skeptical it will work as it supposed to be
        //boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, omega);
        //boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);                                

        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2PP);//&slipedge2PP);
                // boundaryCondition->addExternalVelocityEdge2PP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
            if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex)){   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NP);//&slipedge2NP);
                // boundaryCondition->addExternalVelocityEdge2NP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega); 
            }  //????????????????
            if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NN);//&slipedge2NN); 
            if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex)){   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2PN);//&slipedge2PN); 
                // boundaryCondition->addExternalVelocityEdge2PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
            }
        }




       for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder-1; iY++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PP);//&slipedge1NP); //1NP
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>()); 
            }
            if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NP);//&slipedge1PP);   //1PP
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
            }
            if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PN);//&slipedge1PN);
                // boundaryCondition->addExternalVelocityEdge1PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega); 
            }
            if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NN);//&slipedge1NN);
                // boundaryCondition->addExternalVelocityEdge1NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
            }   //??????????????????????
        } 
    

    
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PN);//&slipedge0PN); //0PN
            if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0PP);//&slipedge0PP);   //0PP
            if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NN);//&slipedge0NN);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge0NP);//&slipedge0NP);
        } 
    
        
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPN);//&instances::getBounceBack<T, Lattice>()); 
            // boundaryCondition->addExternalVelocityCornerNPN(localIndex[0], localIndex[1], localIndex[2], omega); //***
        }
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPP);//&instances::getBounceBack<T, Lattice>());   
            // boundaryCondition->addExternalVelocityCornerNPP(localIndex[0], localIndex[1], localIndex[2], omega);  //**
        }
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNP);//&instances::getBounceBack<T, Lattice>()); 
            // boundaryCondition->addExternalVelocityCornerNNP(localIndex[0], localIndex[1], localIndex[2], omega); //***
        }
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex)){
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNN);//&instances::getBounceBack<T, Lattice>()); 
            // boundaryCondition->addExternalVelocityCornerNNN(localIndex[0], localIndex[1], localIndex[2], omega);  //**
        }
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN);//&instances::getBounceBack<T, Lattice>()); 
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP);//&instances::getBounceBack<T, Lattice>()); 
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP);//&instances::getBounceBack<T, Lattice>()); 
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN);//&instances::getBounceBack<T, Lattice>()); 



    



   

    

    
    



    //CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC-I End

    //////--------------------------------------------**************************Aurora update-end
    static PostProcessingDynamics<T, Lattice, CurvedSlipBoundaryProcessor3D<T, Lattice>> slipBoundary;
    static NoDynamics<T, Lattice> noDynamics;
    STLreader<double> stlReader( "ROBIN_reoriented.STL", converter.getConversionFactorLength()/6, 0.001*scaler);//,1, true);
    // STLreader<T> stlReader( "ROBIN_scale3.STL", converter.getConversionFactorLength(), 0.001);
    stlReader.print();
    // std::ofstream myfile;
    // myfile.open("ROBINIndexes2.csv");
    // myfile << "iX, iY, iZ\n";
    for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                double iXPhys = converter.getPhysLength(iX);
                double iYPhys = converter.getPhysLength(iY);
                double iZPhys = converter.getPhysLength(iZ);
                double location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);

                if (isInside[0])
                {
                    // if (domainInfo.isLocal(iX+185*3, iY+150*3, iZ+150, localIndex)){ //370*3, 340*3, 100*3;400*2,400*2,125*2move to left and down by ~50
                    if (domainInfo.isLocal(iX, iY, iZ, localIndex)){
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                    }
                    
                   // myfile << iX << "," << iY << "," << iZ << "\n";
                    // std::cout << iX+450 << std::endl;
                    // return;

                }
            }
        }
    }
    Index3D localIndex1;
    Index3D localIndex2;
    // Index3D localIndex3;
    for (int iX = 1; iX <= iXRightBorder - 1; ++iX){
        for (int iY = 1; iY <= iYTopBorder - 1; ++iY){
            for (int iZ = 1; iZ <= iZBackBorder - 1; ++iZ){
                if (domainInfo.isLocal(iX, iY, iZ, localIndex1)){
                    bool isFluid = lattice.getMaskEntry(localIndex1[0], localIndex1[1], localIndex1[2]);
                    if (isFluid == false){
                    for (unsigned int iPop = 1; iPop < Lattice<T>::q; ++iPop){
                        if (domainInfo.isLocal(iX + Lattice<T>::c(iPop, 0), iY + Lattice<T>::c(iPop, 1), iZ + Lattice<T>::c(iPop, 2), localIndex2)){
                        // const int neighbor[3] = {iX + Lattice<T>::c(iPop, 0), iY + Lattice<T>::c(iPop, 1), iZ + Lattice<T>::c(iPop, 2)};
                        bool isNfluid = lattice.getMaskEntry(localIndex2[0], localIndex2[1], localIndex2[2]);
                        if (isNfluid == true){
                            // if (domainInfo.isLocal(neighbor[0], neighbor[1], neighbor[2], localIndex3)){  
                                myfile << localIndex2[0] << "," << localIndex2[1] << "," << localIndex2[2] << "\n"; 
                                lattice.defineDynamics(localIndex2[0], localIndex2[1], localIndex2[2], &slipBoundary);
                            // }
                        }
                        }
                    }
                    }
                }
            }
        }
    }





    
    
}



double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}


//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
bool check75(int iX, int iY, int iZ, Index3D& localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}

void GetPressureOnFluidCellsAroundBody(Index3D& localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter, int iXRightBorder, int iYTopBorder, int iZBackBorder, std::ofstream &myfile, T physDensity, T physVelocity){

    for (int iX = 1; iX <=iXRightBorder; ++iX){
        for (int iY = 1; iY <= iYTopBorder; ++iY){
            for (int iZ = 1; iZ <= iZBackBorder; ++iZ){
            Index3D localIndex1;
            Index3D localIndex2;
            if (domainInfo.isLocal(iX, iY, iZ, localIndex1)){
            bool isFluid = lattice.getMaskEntry(localIndex1[0], localIndex1[1], localIndex1[2]);
            if (isFluid == false){
                // myfile <<  iX << "," << iY << "," << iZ <<"\n" ;
                //  myfile <<  localIndex1[0] << "," << localIndex1[1] << "," << localIndex1[2] <<"\n" ;
                for (int iPop = 1; iPop < Lattice<T>::q; ++iPop) {
                // Get direction
                int cX = Lattice<T>::c(iPop, 0);
                int cY = Lattice<T>::c(iPop, 1);
                int cZ = Lattice<T>::c(iPop, 2);
                if (domainInfo.isLocal(iX+cX, iY+cY, iZ+cZ, localIndex2)){
                bool isNFluid = lattice.getMaskEntry(localIndex2[0], localIndex2[1], localIndex2[2]);
                if (isNFluid == true){
                    T celllatticePressure = (lattice.get(localIndex2[0], localIndex2[1], localIndex2[2]).computeRho() - 1.0) / Lattice<T>::invCs2();
                    T cellPhysPressure = converter.getPhysPressure(celllatticePressure);
                    T cellPressureCoeff = cellPhysPressure/(0.5*physDensity*pow(physVelocity,2));
                    myfile <<  iX+cX << "," << iY+cY << "," << iZ+cZ << "," << cellPhysPressure << "," << cellPressureCoeff << "\n" ;
                }

                }

                }
            }
            }
        }
        }
        }
}

// T calculateDelta(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, T slipCell[3], T neighborCell[3], T dir[3],  STLreader<double> &stlReader){
T calculateDelta( UnitConverter<T, Lattice> converter, T slipCell[3], T neighborCell[3], T dir[3],  STLreader<double> &stlReader){
    typedef double T;
    // auto tree = stlReader.getTree();
    T physFluidLocation[3] = {converter.getPhysLength(slipCell[0]), converter.getPhysLength(slipCell[1]), converter.getPhysLength(slipCell[2])};
    T fracSpacing = 100;
    T dirMag = sqrt(pow(dir[0],2)+pow(dir[1],2)+pow(dir[2],2));
    T dotDel = (converter.getPhysDeltaX()*dirMag)/fracSpacing;
    T physXpoint = converter.getPhysLength(slipCell[0]);//physFluidLocation[0];
    T physYpoint = converter.getPhysLength(slipCell[1]);//physFluidLocation[1];
    T physZpoint = converter.getPhysLength(slipCell[2]);//physFluidLocation[2];
    T Xw;
    T Yw;
    T Zw;
    T physPoint[3];
    for(int i = 1; i<= fracSpacing; ++i ) {
        physPoint[0] = physXpoint; 
        physPoint[1] = physYpoint, 
        physPoint[2] = physZpoint;
        // std::vector<T> tmpLoc(physPoint, physPoint+3);
        Xw = physXpoint;
        Yw = physYpoint;
        Zw = physZpoint;
        physXpoint += dir[0]*dotDel;
        physYpoint += dir[1]*dotDel;
        physZpoint += dir[2]*dotDel;
        bool isInside[1];
        T location[3] = {physXpoint, physYpoint, physZpoint};
        stlReader(isInside, location);
        if(isInside[0]){
            break;
        }

    }
    T physWallLocation[3] = {Xw, Yw, Zw};
    T physSolidLocation[3] = {converter.getPhysLength(neighborCell[0]), converter.getPhysLength(neighborCell[1]), converter.getPhysLength(neighborCell[2])};
    T numMagnitude = sqrt(pow((physFluidLocation[0]-physWallLocation[0]),2)+
                                        pow((physFluidLocation[1]-physWallLocation[1]),2)+
                                        pow((physFluidLocation[2]-physWallLocation[2]),2));
    T denMagnitude = sqrt(pow((physFluidLocation[0]-physSolidLocation[0]),2)+
                                        pow((physFluidLocation[1]-physSolidLocation[1]),2)+
                                        pow((physFluidLocation[2]-physSolidLocation[2]),2));

    float delta = numMagnitude/denMagnitude;
    // T delta = numMagnitude/denMagnitude;

    // myfile << physFluidLocation[0] << ",";
    // myfile << physFluidLocation[1] << ",";
    // myfile << physFluidLocation[2] << ",";
    // myfile << physSolidLocation[0] << ",";
    // myfile << physSolidLocation[1] << ",";
    // myfile << physSolidLocation[2] << ",";
    // myfile << Xw << ",";
    // myfile << Yw << ",";
    // myfile << Zw << ",";
    // myfile << delta << "\n";

    float latticeWallLocation[3] = {Xw/converter.getPhysDeltaX(), Yw/converter.getPhysDeltaX(), Zw/converter.getPhysDeltaX()};
    // T latticeWallLocation[3] = {Xw/converter.getPhysDeltaX(), Yw/converter.getPhysDeltaX(), Zw/converter.getPhysDeltaX()};
    std::vector<float> output = {latticeWallLocation[0],latticeWallLocation[1] ,latticeWallLocation[2], delta};
    // std::vector<T> output = {latticeWallLocation[0],latticeWallLocation[1] ,latticeWallLocation[2], delta};
    // float delta = 0.25;
    return delta;
    // return output;
  
}

T returnVector (T slipCell[3], T neighborCell[3], T dir[3], STLreader<double> &stlReader){
    // std::vector<T> output = {vec[0], vec[1], vec[2]};
    // return output;
    T delta = 0.28;
    return delta;
}






template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;


    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), CharLength, maxPhysVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;
    Index3D localIndex1;
    Index3D localIndex2;
    Index3D localIndex3;
    
    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::ofstream myfile;
    std::string filename = "Cells"+std::to_string(domainInfo.localSubDomain)+"_TEST.csv"; 
    std::cout << filename << std::endl;
    myfile.open(filename); 
    myfile << "fX, fY, fZ \n";

    std::cout << "Define boundaries.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain, converter, myfile);
    std::cout << "Define boundaries Finished. " << std::to_string(domainInfo.localSubDomain) << std::endl;


    static PostProcessingDynamics<T, Lattice, CurvedSlipBoundaryProcessor3D<T, Lattice>> slipBoundary;
    static NoDynamics<T, Lattice> noDynamics;
    STLreader<double> stlReader( "ROBIN_reoriented.STL", converter.getConversionFactorLength()/6, 0.001*scaler);//,1, true);

    std::cout << "Init GPU data.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished init GPU!" << std::to_string(domainInfo.localSubDomain) << std::endl;


    auto slipDataHandler = lattice.getDataHandler(&slipBoundary);
    // std::cout << slipDataHandler << std::endl;
    // auto slipCellIds = slipDataHandler->getDynamicsData();
    // std::vector<auto> slipCellIds = slipDataHandler->getCellIDs();
    // auto slipCellIds = slipDataHandler->getCellIDs();
    auto slipBoundaryPostProcData = slipDataHandler->getPostProcData();

    for (size_t index : slipDataHandler->getCellIDs())
    {
        size_t p[3];
        util::getCellIndices3D(index, lattice.getNy(), lattice.getNz(), p);
        T latticeFluidLocation[3] = {p[0], p[1], p[2]};

        // myfile << p[0] << "," << p[1] << "," << p[2] << "\n";

        size_t momentaIndex = slipDataHandler->getMomentaIndex(index);

        size_t nNeighbor = 0;
        T normal[3] = {0, 0, 0};
        T normal1[3] = {0,0,0};

        // for (unsigned int iPop = 0; iPop < Lattice<T>::q; ++iPop)
        // {
        //     size_t pN[Lattice<T>::d] = {p[0] + Lattice<T>::c(iPop, 0),
        //                                 p[1] + Lattice<T>::c(iPop, 1),
        //                                 p[2] + Lattice<T>::c(iPop, 2)};
        //     size_t nIndex = util::getCellIndex3D(pN[0], pN[1], pN[2], lattice.getNy(), lattice.getNz());
        //     T latticeNeighborLocation[3] = {pN[0], pN[1], pN[2]};
            
        //     // if (check75(pN[0], pN[1], pN[2], localIndex3, lattice, domainInfo.getLocalInfo(), refSubDomain)){
        //     if (lattice.getFluidMask()[nIndex] == false)
        //     {
        //         // myfile << pN[0] << "," << pN[1] << "," << pN[2] << "," << p[0] << "," << p[1] << "," << p[2] << "\n";
        //         T dir[3] = {Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)};
        //         // T delta = 0.25;
        //         T delta = calculateDelta( converter, latticeFluidLocation, latticeNeighborLocation, dir, stlReader);
        //         // T vec = returnVector(latticeFluidLocation, latticeNeighborLocation, dir, stlReader);
        //         // std::vector<T> deltaData = calculateDelta(lattice, converter, latticeFluidLocation, latticeNeighborLocation, dir, stlReader);//myfile,
        //         // T delta = deltaData[3];
        //         // T latticeWallLocation[3] = {deltaData[0],deltaData[1],deltaData[2]};

        //         for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
        //             normal[iDim] += Lattice<T>::c(iPop, iDim);
        //             // normal1[iDim] += -latticeWallLocation[iDim]+latticeNeighborLocation[iDim];
        //             // normal1[iDim] += latticeWallLocation[iDim]-latticeFluidLocation[iDim];
        //             // normal[iDim] += Lattice<T>::c(iPop, iDim)*(-delta);
        //         }

        //         size_t const idxDir = CurvedSlipBoundaryProcessor3D<T, Lattice>::idxDir(nNeighbor);
        //         size_t const idxDelta = CurvedSlipBoundaryProcessor3D<T, Lattice>::idxDelta(nNeighbor);
        //         slipBoundaryPostProcData[idxDir][momentaIndex] = iPop;
        //         slipBoundaryPostProcData[idxDelta][momentaIndex] = delta;

        //         slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T, Lattice>::idxTau()][momentaIndex] =
        //             converter.getLatticeRelaxationTime();
        //         ++nNeighbor;
        //     }
        //     // }
        // }

        // // std::vector<T> normalT = calculateNormal(lattice, converter, latticeFluidLocation, stlReader, myNormalTestFile);
        // T normalAbs = 0;
        // T normal1Abs = 0;
        // // T normalTAbs = 0;
        // for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        // {
        //     normalAbs += normal[iDim] * normal[iDim];
        //     normal1Abs += normal1[iDim]*normal1[iDim];
        //     // normalTAbs += normalT[iDim]*normalT[iDim];
        // }
        // normalAbs = std::sqrt(normalAbs);
        // normal1Abs = std::sqrt(normal1Abs);
        // // normalTAbs = std::sqrt(normalTAbs);
        // // myfile2 << p[0] <<"," << p[1] << "," << p[2] << ",";
        // for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        // {
        //     slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T, Lattice>::idxNormal() + iDim][momentaIndex] = -normal[iDim] / normalAbs;
        //     // myfile2 << normalT[iDim] << ",";/// normalTAbs 
        // }
        // // myfile2 << "\n";

        // slipBoundaryPostProcData[CurvedSlipBoundaryProcessor3D<T, Lattice>::idxNumNeighbour()][momentaIndex] = (T)nNeighbor;
    }

    std::cout << "Init equilibrium.... " << std::to_string(domainInfo.localSubDomain) << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
                //  if (check75(iX, iY, iZ, localIndex1, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                //     lattice.iniEquilibrium(localIndex1[0], localIndex1[0], localIndex1[1], localIndex1[1], localIndex1[2], localIndex1[2], 1., vel);
                // }
            }
        }
    }
    lattice.copyDataToGPU();
    std::cout << "Finished Init equilibrium!" << std::to_string(domainInfo.localSubDomain) << std::endl;

#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
  ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice, commDataHandler);
  NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    unsigned int trimTime = converter.getLatticeTime(simTime);

    // std::string directory = "/data/ae-jral/ifernandez31/ROBINDomainSize"; //"./tmp/"
    singleton::directories().setOutputDir(directory);
    // BlockVTKwriter3D<T> writer("outputVTK_"+std::to_string(domainInfo.localSubDomain));
    BlockVTKwriterMultiLattice3D<T, Lattice<T>> writer("outputMultiVTK", domainInfo);
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidmaskfunctor);
    
    // int ixmin = 0;
    // int ixmax = iXRightBorder;
    // int iymin = 0;
    // int iymax = iYTopBorder;
    // int izmin = iZBackBorder/2-4;
    // int izmax = iZBackBorder/2+4; 

      
      
    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    unsigned int rampUpSteps = trimTime / 8; //increased to 4 again due to crash-Oct 18 //was 4 decreased to 8 to have more smooth results and less effect of ramping up, if it will not crash it should be fine//18/08/2021
    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();

    // std::ofstream myfile;
    // std::string filename = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_6s.csv"; 
    // std::cout << filename << std::endl;
    // myfile.open(filename); 
    // myfile << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // std::ofstream myfile2;
    // std::string filename2 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_7s.csv"; 
    // std::cout << filename2 << std::endl;
    // myfile2.open(filename2); 
    // myfile2 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // std::ofstream myfile3;
    // std::string filename3 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_8s.csv"; 
    // std::cout << filename3 << std::endl;
    // myfile3.open(filename3); 
    // myfile3 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    std::ofstream myfile4;
    std::string filename4 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_9s.csv"; 
    std::cout << filename4 << std::endl;
    myfile4.open(filename4); 
    myfile4 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    std::ofstream myfile5;
    std::string filename5 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_10s.csv"; 
    std::cout << filename5 << std::endl;
    myfile5.open(filename5); 
    myfile5 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    std::ofstream myfile6;
    std::string filename6 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_11s.csv"; 
    std::cout << filename6 << std::endl;
    myfile6.open(filename6); 
    myfile6 << "iX, iY, iZ, physPressure, pressureCoeff \n";

    // std::ofstream myfile7;
    // std::string filename7 = "Pressure"+std::to_string(domainInfo.localSubDomain)+"_12s.csv"; 
    // std::cout << filename7 << std::endl;
    // myfile7.open(filename7); 
    // myfile7 << "iX, iY, iZ, physPressure, pressureCoeff \n";



    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        if ((trimStep % converter.getLatticeTime(0.001)) == 0 && trimStep <= rampUpSteps)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity*((T)trimStep/rampUpSteps);
            std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
            int iX = 0;
            for (int iY = 1; iY <= iYTopBorder-1; iY++) {
                for (int iZ = 1; iZ <= iZBackBorder-1; iZ++) {
                    
                    T vel[3] = {uVel, 0, 0};
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
            }
        }
        
        lattice.copyDataToGPU();



        }



#ifdef ENABLE_CUDA
        collideAndStreamPostStreamUpdateMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));
            

            if (outputVTKData) {
                lattice.copyDataToCPU();
                // writer.write(0,iXRightBorder, ceil(0.2*iYTopBorder), ceil(0.8*iYTopBorder), ceil(iZBackBorder/4), ceil(0.75*iZBackBorder), trimStep);
                writer.write(trimStep, (float) trimStep);
            }
        }
           if ((trimStep % converter.getLatticeTime(1)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));
            
        }

        // if ((trimStep == converter.getLatticeTime(10.0)) || (trimStep == converter.getLatticeTime(1.0))){
        //     lattice.copyDataToCPU();
        //     writer.write(trimStep);
        //  }
        if (trimStep % 1000 ==0){
            timer.update(trimStep);
            timer.printStep();
            std::cout << "Simulation time " << std::to_string(domainInfo.localSubDomain) << ": " << converter.getPhysTime(trimStep) << std::endl;

            T u[3]; 
            if (check75(ceil(iXRightBorder/4), ceil(iYTopBorder/4), ceil(iZBackBorder/4), localIndex2, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                lattice.get(localIndex2[0], localIndex2[1], localIndex2[2]).computeU(u);
            }
            std::cout << "U for GPU "<< std::to_string(domainInfo.localSubDomain) << ": " << u[0] << std::endl;
            // lattice.copyDataToCPU();
            // writer.write(0,iXRightBorder,ceil(0.45*iYTopBorder), ceil(0.55*iYTopBorder), ceil(0.49*iZBackBorder), ceil(0.51*iZBackBorder), trimStep);
            // if (isnan(u[0])){
            //     return;
            // }
        }


        if ((trimStep == converter.getLatticeTime(10))) //(trimStep == converter.getLatticeTime(1)) ||
        {
            lattice.copyDataToCPU();
            writer.write(trimStep, (float) trimStep);
        }

        // if ((trimStep == converter.getLatticeTime(6)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile, physDensity, physVelocity);
        // }

        // if ((trimStep == converter.getLatticeTime(7)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile2, physDensity, physVelocity);
        // }

        // if ((trimStep == converter.getLatticeTime(8)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile3, physDensity, physVelocity);
        // }

        if ((trimStep == converter.getLatticeTime(9)))
        {
            lattice.copyDataToCPU();
            GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile4, physDensity, physVelocity);
        }

        if ((trimStep == converter.getLatticeTime(10)))
        {
            lattice.copyDataToCPU();
            GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile5, physDensity, physVelocity);
        }

        if ((trimStep == converter.getLatticeTime(11)))
        {
            lattice.copyDataToCPU();
            GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile6, physDensity, physVelocity);
        }

        // if ((trimStep == converter.getLatticeTime(12)))
        // {
        //     lattice.copyDataToCPU();
        //     GetPressureOnFluidCellsAroundBody(localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain, converter, refSubDomain.globalIndexEnd[0] - 2, refSubDomain.globalIndexEnd[1] - 2, refSubDomain.globalIndexEnd[2] - 2, myfile7, physDensity, physVelocity);
        // }


        
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    int rank = initIPC();

    unsigned ghostLayer[] = {1,0,0};

    cout<< "control 1********************************************************************************************************************************************************************************************************"<< endl;
    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(ceil(XlengthBase*scaler)), (size_t)(ceil(YlengthBase*scaler)), (size_t)(ceil(ZlengthBase*scaler)), 0u, 1u, ghostLayer);//cant be an odd number if you have even numbers of gpus
    cout<< "control 2********************************************************************************************************************************************************************************************************"<< endl;
    const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks(), ghostLayer);
    cout<< "control 3********************************************************************************************************************************************************************************************************"<< endl;
    if (rank == 0) {
        std::cout << "REF SUBDOMAIN INFO" << std::endl;
        std::cout << refSubDomain;
        std::cout << "Domain Info" << std::endl;
        std::cout << subDomainInfo;
        std::cout << "####" << std::endl;
    }

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl;
    std::cout << "####################################" << std::endl;


    MultipleSteps(commDataHandler, refSubDomain);

    cudaDeviceSynchronize();
    MPI_Finalize();
    return 0;
}
