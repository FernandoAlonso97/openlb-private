/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
// #define D3Q19LATTICE 1
typedef float T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include <random>
#include <ctime>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"

#define Lattice ForcedD3Q19Descriptor
// #define Lattice D3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.075;
//const T domainWidth = 0.4064;
const double simTime = 16;
// unsigned int resolution = 448;
// unsigned int xExtraResolution = 2*resolution;
const int XlengthBase = 600;
const int YlengthBase = 340;
const int ZlengthBase = 260;
const int scaler = 3;//2;

// const T gridSpacing = domainWidth/resolution;
// const T gridArea = pow(gridSpacing,2);

// const T physInducedVelocity = 50.0; // m/s
const T physVelocity = 34.3;  
const T maxPhysVelocity = 45;           //m/s
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 4.5e-5; // m^2/s
T CharLength =  2.1;//3.5;//0.717169;//
int resolution = 100*CharLength;//350;





/* Output Parameters */
bool outputVTKData = true;
// bool outputRotorData = false;
// bool outputDebugData = false;

const T vtkWriteInterval = 2; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain, UnitConverter<T, Lattice> converter)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    T omega = dynamics.getOmega();


    auto *velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    auto *velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto *pressureMomenta0P = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
    auto *pressurePostProcessor0P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor0P)>::type::PostProcessorType> pressureBCDynamics0P(omega, *pressureMomenta0P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> slip1P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> slip1N{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> slip2P{};
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> slip2N{};

    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> slipedge0PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> slipedge0NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> slipedge0PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> slipedge0NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipedge1PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipedge1NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipedge1PP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipedge1NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, -1>> slipedge2NN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, -1, 1>> slipedge2NP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, -1>> slipedge2PN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 2, 1, 1>> slipedge2PP;



    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> slipcornerPNN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> slipcornerPPN;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> slipcornerPNP;
    static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> slipcornerPPP;


    
        for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder -1; iY++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics0N);
            if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics0P);
            }
        }
    
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1P);
            }
            // if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex)){
            //     lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);
            // }
        }
        }

        
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder-1; iX++) {
        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex)){
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip1N);
                } //+1 x left // +1 x right // +1 z front // +1 z back
            }
        }
        
        
        
        
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
        for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder-1; iY++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2N);
            if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slip2P);
            }
        }

        OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
        createInterpBoundaryCondition3D<T, Lattice,
                                        ForcedLudwigSmagorinskyBGKdynamics>(lattice);

        //?????????????????
        //boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, omega);//do it like others keep x1, x1, y1, y1, z1, z1 --skeptical it will work as it supposed to be
        //boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, omega);
        //boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);                                

        for (unsigned iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; iZ++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
                // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                boundaryCondition->addExternalVelocityEdge2PP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
            if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex))   
                boundaryCondition->addExternalVelocityEdge2NP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //????????????????
            if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge2NN); 
            if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))   
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge2PN); 
        }




       for (unsigned iY = iYBottomBorder+1; iY <= iYTopBorder-1; iY++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge1NP); //1NP
            if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge1PP);   //1PP
            if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
                boundaryCondition->addExternalVelocityEdge1PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);  //????????????????????/
            if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
                boundaryCondition->addExternalVelocityEdge1NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);   //??????????????????????
        } 
    

    
        for (unsigned iX = iXLeftBorder+1; iX <= iXRightBorder -1; iX++) {
            Index3D localIndex;
            if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0PN); //0PN
            if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0PP);   //0PP
            if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0NN);
            if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
                lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipedge0NP);
        } 
    
        
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNPN(localIndex[0], localIndex[1], localIndex[2], omega); //***
        if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNPP(localIndex[0], localIndex[1], localIndex[2], omega);  //**
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNNP(localIndex[0], localIndex[1], localIndex[2], omega); //***
        if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
            boundaryCondition->addExternalVelocityCornerNNN(localIndex[0], localIndex[1], localIndex[2], omega);  //**
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipcornerPPN); //PPN
        if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipcornerPPP); //PPP
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipcornerPNP); 
        if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &slipcornerPNN); //PPP



    



   

    

    
    



    //CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC-I End

    //////--------------------------------------------**************************Aurora update-end

    STLreader<T> stlReader( "ROBIN_reoriented.STL", converter.getConversionFactorLength(), 0.001*scaler);

    for (int iX = 0; iX <= iXRightBorder; ++iX){
        for (int iY = 0; iY <= iYTopBorder; ++iY){
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T iXPhys = converter.getPhysLength(iX);
                T iYPhys = converter.getPhysLength(iY);
                T iZPhys = converter.getPhysLength(iZ);
                T location[3] = {iXPhys, iYPhys, iZPhys};
                bool isInside[1];
                stlReader(isInside, location);
                if (isInside[0])
                {
                    if (domainInfo.isLocal(iX, iY, iZ, localIndex))
                    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
                }
            }
        }
    }





    
    
}



double generate(double min, double max)
{
    using namespace std;

    static default_random_engine generator(unsigned(time(nullptr)));
    uniform_real_distribution<double> distribution(min, max);

    return distribution(generator);
}


//defineBoundaries(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
bool check75(int iX, int iY, int iZ, Index3D& localIndex, BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

    return domainInfo.isLocal(iX, iY, iZ, localIndex);
}





template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;


    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), CharLength, maxPhysVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;
    
    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::cout << "Define boundaries.... " << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain, converter);

    static NoDynamics<T, Lattice> noDynamics;



    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
  ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice, commDataHandler);
  NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");
    BlockVTKwriter3D<T> writer("outputVTK_"+std::to_string(domainInfo.localSubDomain));
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidmaskfunctor(lattice); //to show empty space in paraview, everywhere where there is fluid or not fluid
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidmaskfunctor);
    
    // int ixmin = 0;
    // int ixmax = iXRightBorder;
    // int iymin = 0;
    // int iymax = iYTopBorder;
    // int izmin = iZBackBorder/2-4;
    // int izmax = iZBackBorder/2+4; 
    
    
      
   

    T finalLeftVelocity = converter.getLatticeVelocity(physVelocity);
    unsigned int rampUpSteps = trimTime / 8; //increased to 4 again due to crash-Oct 18 //was 4 decreased to 8 to have more smooth results and less effect of ramping up, if it will not crash it should be fine//18/08/2021
    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();


    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {

        if ((trimStep % converter.getLatticeTime(0.01)) == 0 && trimStep <= rampUpSteps)
        {
            lattice.copyDataToCPU();
            T uVel = finalLeftVelocity*((T)trimStep/rampUpSteps);
            //std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
            int iX = 0;
            for (int iY = 1; iY <= iYTopBorder; iY++) {
                for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
                    
                    T vel[3] = {uVel, 0, 0};
                    if (check75(0, iY, iZ, localIndex, lattice, domainInfo.getLocalInfo(), refSubDomain)){
                        lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                        lattice.iniEquilibrium(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
                    }
            }
        }
        
        lattice.copyDataToGPU();



        }



#ifdef ENABLE_CUDA
        collideAndStreamPostStreamUpdateMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            if (outputVTKData) {
                lattice.copyDataToCPU();
                writer.write(0, iXRightBorder, 0, iYTopBorder, 0, iZBackBorder, trimStep);
            }
        }


        
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    int rank = initIPC();
    cout<< "control 1********************************************************************************************************************************************************************************************************"<< endl;
    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(ceil(XlengthBase*scaler)), (size_t)(ceil(YlengthBase*scaler)), (size_t)(ceil(ZlengthBase*scaler)), 0u, 1u);//cant be an odd number if you have even numbers of gpus
    cout<< "control 2********************************************************************************************************************************************************************************************************"<< endl;
    const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks());
    cout<< "control 3********************************************************************************************************************************************************************************************************"<< endl;
    if (rank == 0) {
        std::cout << "REF SUBDOMAIN INFO" << std::endl;
        std::cout << refSubDomain;
        std::cout << "Domain Info" << std::endl;
        std::cout << subDomainInfo;
        std::cout << "####" << std::endl;
    }

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl;
    std::cout << "####################################" << std::endl;


    MultipleSteps(commDataHandler, refSubDomain);

    cudaDeviceSynchronize();
    MPI_Finalize();
    return 0;
}
