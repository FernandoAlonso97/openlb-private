#define D2Q9LATTICE 1
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "../../../src/communication/CPUGPUDataExchange.h"
#include "../../../src/communication/readFunctorFromGPU.h"
#include "../../../src/communication/writeFunctorToGPU.h"

#define DESCRIPTOR D2Q9Descriptor

using namespace olb;
// using namespace olb::descriptors;
// using namespace olb::graphics;
// using namespace olb::util;
// using namespace std;

int main(){

  int N{10};
  ConstRhoBGKdynamics<T, DESCRIPTOR, BulkMomenta<T,DESCRIPTOR > >  dynamics (0);
  IndicatorCuboid2D<T> cuboid{Vector<T,2>{1,1},Vector<T,2>{0,0}};
  CuboidGeometry2D<T> cGeometry{cuboid,1.0/N,1};
  HeuristicLoadBalancer<T> loadBalancer{cGeometry};
  SuperGeometry2D<T> sGeometry{cGeometry,loadBalancer,0};
  SuperLattice2D<T,DESCRIPTOR> sLattice{sGeometry,&dynamics};
  sLattice.initDataArrays();

  WriteFunctorByRangeToGPU2D<T,DESCRIPTOR,VelocityFunctor> writer(sLattice.getExtendedBlockLattice(0),5,7,5,7);
  ReadFunctorByRangeFromGPU2D<T,DESCRIPTOR,VelocityFunctor> reader(sLattice.getExtendedBlockLattice(0),5,7,5,7);

  for (int iX=0;iX<2;++iX)
    for (int iY=0;iY<2;++iY)
    {
      writer.setByLocalIndex(iX,iY)[0] = iX;
      writer.setByLocalIndex(iX,iY)[1] = iY;
    }

  writer.transferAndWrite();
  reader.readAndTransfer();

  for (int iX=0;iX<2;++iX)
    for (int iY=0;iY<2;++iY)
    {
      // if (reader.getByLocalIndex(iX,iY)[0] != 1.0 || reader.getByLocalIndex(iX,iY)[1] != 2.0)
#ifdef OLB_DEBUG
      std::cout << "Values " << reader.getByLocalIndex(iX,iY)[0] << " " << reader.getByLocalIndex(iX,iY)[1] << " at index " << iX << " " << iY << " " << std::endl;
#endif
      OLB_PRECONDITION(reader.getByLocalIndex(iX,iY)[0] == iX && reader.getByLocalIndex(iX,iY)[1] == iY);
    }

  std::cout << "Numerical Test of range successful" << std::endl;

 cudaDeviceSynchronize();

 // IndicatorCircle2D<T> circle{Vector<T,2>{0.5,0.5},0.25};
 // WriteFunctorByIndicatorToGPU2D<T,DESCRIPTOR,VelocityFunctor> writeByIndicator{sLattice.getExtendedBlockLattice(0),sGeometry.getExtendedBlockGeometry(0),circle};
 // ReadFunctorByIndicatorFromGPU2D<T,DESCRIPTOR,VelocityFunctor> readByIndicator{sLattice.getExtendedBlockLattice(0),sGeometry.getExtendedBlockGeometry(0),circle};

 cudaDeviceSynchronize();
 std::cout << "End of program, marker added because of possible deallocation errors" << std::endl;
 return 0;
 }
