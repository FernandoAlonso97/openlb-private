/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
// #include "boundary/boundaryCondition3D.h"
// #include "boundary/boundaryCondition3D.hh"
// #include "boundary/boundaryPostProcessors3D.h"
// #include "boundary/boundaryPostProcessors3D.hh"
// #include "boundary/momentaOnBoundaries.h"
// #include "boundary/momentaOnBoundaries.hh"
// #include "boundary/momentaOnBoundaries3D.h"
// #include "boundary/momentaOnBoundaries3D.hh"
// #include "dynamics/latticeDescriptors.h"
// #include "dynamics/smagorinskyBGKdynamics.h"
// #include "dynamics/smagorinskyBGKdynamics.hh"
// #include "dynamics/dynamics.h"
// #include "dynamics/dynamics.hh"
// #include "core/blockLattice3D.h"
// #include "core/blockLatticeALE3D.h"
// #include "core/singleton.h"
// #include "core/unitConverter.h"
// #include "io/blockVtkWriter3D.h"
// #include "dynamics/latticeDescriptors.hh"
// #include "core/blockLattice3D.hh"
// #include "core/blockLatticeALE3D.hh"
// #include "core/unitConverter.hh"
// #include "io/blockVtkWriter3D.hh"
// #include "functors/lattice/blockLatticeLocalF3D.h"
// #include "functors/lattice/blockLatticeLocalF3D.hh"
// #include "utilities/timer.h"
// #include "utilities/timer.hh"
#include <fstream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <thread>
#include <cstdlib>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,0,-1>> slipDynamics{};

template<typename T,template <typename U> class Lattice>
class HomogeniousForce{

  public:
    OPENLB_HOST_DEVICE
    explicit HomogeniousForce() = default;

    void setForce (T newForce[Lattice<T>::d])
    {
	for (int i=0;i<Lattice<T>::d;++i)
		force_[i] = newForce[i];
    }
    OPENLB_HOST_DEVICE
    void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
    {
	for (int i=0;i<Lattice<T>::d;++i)
		cellData[Lattice<T>::forceIndex+i][threadIndex] = force_[i];
    }

  private:
 
    T force_[Lattice<T>::d];

};

class TempFunctional : public WriteCellFunctional<T, Lattice>
{
public:

  virtual void apply(CellView<T,Lattice> cell, int pos[3]) const override
  {
    if(pos[2] == 6)
    {
        std::cout << pos[0] << ", " << pos[1] << ", " << pos[2] << std::endl;
        for (int iPop = Lattice<T>::forceIndex; iPop < Lattice<T>::forceIndex+Lattice<T>::d; ++iPop)
        {
          std::cout << cell[iPop].get() << ", ";
        }
        sumZ += cell[Lattice<T>::forceIndex+2];
        std::cout << std::endl;
    }
  }
};


template<typename T, template <typename> class Lattice, class BlockLattice>
void defineBoundaries(BlockLattice &lattice, Dynamics<T,Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);



    // boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );
    // boundaryCondition->addVelocityBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addPeriodicBoundary1N(iXLeftBorder+1,iXRightBorder,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addPeriodicBoundary1P(iXLeftBorder+1,iXRightBorder,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    boundaryCondition->addPeriodicBoundary1N(iXLeftBorder,iXRightBorder,iYBottomBorder  ,iYBottomBorder,iZFrontBorder,iZBackBorder, omega );
    boundaryCondition->addPeriodicBoundary1P(iXLeftBorder,iXRightBorder,iYTopBorder     ,iYTopBorder   ,iZFrontBorder,iZBackBorder, omega );
    boundaryCondition->addPeriodicBoundary0N(iXLeftBorder,iXLeftBorder,iYBottomBorder  ,iYTopBorder,iZFrontBorder,iZBackBorder, omega );
    boundaryCondition->addPeriodicBoundary0P(iXRightBorder,iXRightBorder,iYBottomBorder     ,iYTopBorder   ,iZFrontBorder,iZBackBorder, omega );
    // boundaryCondition->addVelocityBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addVelocityBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

//      lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
     // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
     lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &slipDynamics);
     lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());

	  // boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
	  // boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );
    // boundaryCondition->addExternalVelocityEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
    // boundaryCondition->addExternalVelocityEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
		// boundaryCondition->addExternalVelocityEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
		// boundaryCondition->addExternalVelocityEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

      // boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      // boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
	  // boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );
		// boundaryCondition->addExternalVelocityEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
		// boundaryCondition->addExternalVelocityEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

      // boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      // boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
	  // boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
	  // boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
		// boundaryCondition->addExternalVelocityEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
		// boundaryCondition->addExternalVelocityEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

      // boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      // boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      // boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      // boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

    // boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
    // boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
    // boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
    // boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);
    //
    // boundaryCondition->addExternalVelocityCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
    // boundaryCondition->addExternalVelocityCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
    // boundaryCondition->addExternalVelocityCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
    // boundaryCondition->addExternalVelocityCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);
     // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder , iZFrontBorder  , iZBackBorder , &slipDynamics);
     // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder , iZFrontBorder  , iZBackBorder , &slipDynamics);

//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

}

void MultipleSteps(const double simTime, int iXRightBorderArg)
{

  int iXLeftBorder = 0;
  int iXRightBorder = (iXRightBorderArg+1)*30-1;
  int iYBottomBorder = 0;
  int iYTopBorder = (iXRightBorderArg+1)*15-1;
  int iZFrontBorder = 0;
  int iZBackBorder = (iXRightBorderArg+1)*10-1;

  T viscosity = 0.000004426085106;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          iXRightBorderArg
          ,0.3*1.0/std::sqrt(3)
          ,0.5901446809// meters shiplength//4*4.92
          ,50
          ,viscosity// aus messung Leishman SFS2 at BLWT //0.0000146072
          ,1.225
          ,0);

  converter.print();

  T conversionVelocity = converter.getConversionFactorVelocity();

  T deltaX = converter.getConversionFactorLength();

  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";

  Vec3<T> velocity = {converter.getLatticeVelocity(-10.0),0.0,0.0};

  ConstExternalField<T,Lattice> externalFlow(velocity);

  Vec3<T> lattice_anchor {0., 0., 0.};
  // BlockLatticeALE3D<T, Lattice, ConstExternalField<T, Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1,
                                                                                              // lattice_anchor, &bulkDynamics, externalFlow);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

  std::cout << "Finished!" << std::endl;

  std::vector<int> limits = {iXLeftBorder,iXRightBorder,iYBottomBorder,iYTopBorder,iZFrontBorder,iZBackBorder};

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,limits);
  // setBlockPosition<BlockLattice3D<T, Lattice, ConstExternalField<T, Lattice>>,Lattice>(lattice,deltaX);
  // setBlockPosition<BlockLattice3D<T, Lattice>,Lattice>(lattice,deltaX);
  // std::cout << "total dimensions " << iXRightBorder << " " << iYTopBorder  << " " << iZBackBorder << std::endl;
  // Vector<T,3> shipTipPosition {iXRightBorder-1*6*(iXRightBorderArg+1),0.5*iYTopBorder,iZBackBorder};
  // std::cout << "shipTipPosition " <<  shipTipPosition << std::endl;
  // setShipBoundaries<BlockLattice3D<T,Lattice>,Lattice> (lattice,shipTipPosition, (iXRightBorderArg+1) / 9,deltaX);

  std::cout << "Finished!" << std::endl;
  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;

  T shipHeight = 11* (iXRightBorderArg+1)/9;
  T ablHeight = 4*shipHeight;
  T targetVelocity =15.0;
  T velocityGradientAtWall = targetVelocity*1.0/7.0*pow((1.0/ablHeight),-6.0/7.0);
  T pressureDiff = velocityGradientAtWall * viscosity * (iXRightBorder*iYTopBorder*deltaX*deltaX);
  T initialVolumeForce = 0.00000000001* pressureDiff*converter.getConversionFactorPressure()/(iXRightBorder*iYTopBorder*iZBackBorder*deltaX*deltaX*deltaX);
  T force[Lattice<T>::d] = {-initialVolumeForce,0.0,0.0};

  std::cout << "initalForcing: " << initialVolumeForce << std::endl;
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= iXRightBorder; ++iX)
      for (int iY = 0; iY <= iYTopBorder; ++iY)
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[Lattice<T>::d] = { 0., 0., 0.};
            T rho[1];
           
            T obst_z = iZBackBorder;
            T obst_r = iZBackBorder;

            T maxVelocity = converter.getLatticeVelocity(targetVelocity);//0.111;


            double a = -1., b = 1.;

      
            double nRandom1 = rand()/(double)RAND_MAX*(b-a) + a;
            double nRandom2 = rand()/(double)RAND_MAX*(b-a) + a;
            double nRandom3 = rand()/(double)RAND_MAX*(b-a) + a;

            // T u_calc = -maxVelocity*pow(((obst_r-abs(iZ - obst_z))/obst_r), 1./7.);
            T inverseZCoordinate = (iZBackBorder - iZ);
            T u_calc = -maxVelocity;
            if (inverseZCoordinate < ablHeight)
               u_calc = -maxVelocity*pow((inverseZCoordinate/ablHeight), 1./7.);

            vel[0] = u_calc + 0.15*nRandom1*maxVelocity;// nRandom1*(0.00486); //(T) u_calc*(1 - cos((T) (twoPi*x))*sin((T) (twoPi*y))*exp((T) (-(x*x + y*y)))); //u_calc*(1 + 0.01*nRandom1);
            vel[1] =0.0+ 0.15*nRandom2*maxVelocity;//-(cos((T) (twoPi*y))*sin((T) (twoPi*y))*exp((T) (-(x*x + y*y)))); //0.01*u0*nRandom2;
            vel[2] =0.0 + 0.15*maxVelocity*nRandom3; //0.05

			      lattice.defineRhoU(iX,iX,iY,iY,iZ,iZ, 1., vel);
            lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
          }
  std::cout << "Finished!" << std::endl;


  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;
  std::string name = "abl";
  std::string directory = "/local_scratch/ga69wij/abl/";

  std::cout << "Running " << name << std::endl;

  BlockVTKwriter3D<T> vtkWriter( name );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeCoordinates3D<T,Lattice> coordinateFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(coordinateFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  std::cout << "Starting time simulation" << std::endl;
  std::cout << "Steps: " << converter.getLatticeTime(simTime) << std::endl;

  Vec3<T> zeroVec = {0.,0.,0.};
  Vec3<T> position = {0.,0.,0.};
  Vec3<T> movement = {converter.getLatticeVelocity(10),0.,0.};


  // initial forcing
  HomogeniousForce<T,Lattice> homogeniousForce{};
  homogeniousForce.setForce(force);
  lattice.apply(homogeniousForce);

  lattice.copyDataToCPU();

  vtkWriter.write(0);


  std::cout << "Finished writing first step" << std::endl;

  
  for(unsigned int timeStep = 1; timeStep <= converter.getLatticeTime(simTime); ++timeStep)
  {

      lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
	  // lattice.moveMeshGPU(movement, zeroVec, position, zeroVec);
      
	  if(timeStep%converter.getLatticeTime(1.0) == 0)
		  std::cout << "Finished step: " << timeStep << std::endl;
  
      if(timeStep%converter.getLatticeTime(20.0) == 0)
      {
          lattice.copyDataToCPU();
          T averagePhysVelocityX{0.0};
      	  for (int iY = 0; iY <= iYTopBorder; ++iY)
	  {
          	for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
		{
	         T output[Lattice<T>::d] = {0.0,0.0,0.0};
		 int input[Lattice<T>::d] = {0.5*iXRightBorder,iY,iZ};
 		 physVelocityFunctor(output,input);
		 averagePhysVelocityX += output[0];
		}
	  }
 
	  averagePhysVelocityX /= (iYTopBorder*iZBackBorder);

          std::cout << "AverageVelocityX=" << averagePhysVelocityX << std::endl;
	  std::cout << "ForceCurrent=" << force[0] << " ForceIncrement=" << (1.0-(-averagePhysVelocityX/targetVelocity)) * force[0] << std::endl;
	  force[0] += (1.0-(-averagePhysVelocityX/targetVelocity)) * force[0];
	  homogeniousForce.setForce(force);

	  lattice.apply(homogeniousForce);
	  
          HANDLE_ERROR(cudaPeekAtLastError());
          vtkWriter.write(timeStep);
      }
  }
  vtkWriter.write(converter.getLatticeTime(simTime));

  cudaDeviceSynchronize();

  timer.stop();

}

int main()
{

    const double simTime = 400.0;
    const size_t resolution = 31;//31;

  	

	MultipleSteps(simTime,resolution);

	return 0;
}
