/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: b.horvat@tum.de
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "core/util.h"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/MemorySpace/memory_spaces.h"

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;


template<typename T, template<typename U> class Lattice>
bool operator== (CellView<T,Lattice> const & a, CellView<T,Lattice> const & b)
{
  for (unsigned i=0;i<Lattice<T>::q;++i)
  {
    if (a[i]!=b[i])
      return false;
  }
  return true;
}

template<typename T, template<typename U> class Lattice>
bool operator!= (CellView<T,Lattice> const & a, CellView<T,Lattice> const & b)
{
  return !(a==b);
}

template<typename T, template<typename U> class Lattice>
std::ostream& operator<< (std::ostream& stream, CellView<T,Lattice> const & a)
{
  stream << "Values of CellView ";
  for (unsigned i=0;i<Lattice<T>::q;++i)
  {
    stream << a[i] <<" ";
  }
  stream << std::endl;
  return stream;
}


int main(int argc, char** argv)
{
  {
    const int Resolution = 31;
    const int noSubdomains = 2;
    Index3D globalDomain{Resolution+1,Resolution+1,Resolution+1,0};

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(1.9,0.05);

    std::cout << "Allocate Refrence BlockLattice3D ... ";

    BlockLattice3D<T,Lattice> referenceLattice {Resolution+1,Resolution+1,Resolution+1,&bulkDynamics};

    std::cout << "Finished" << std::endl;

    std::cout << "Creating SubDomainInformations ... ";
    std::vector<SubDomainInformation<T,Lattice<T>>> subDomInfo{};
    
    for (unsigned i=0;i<noSubdomains;++i)
    {
      subDomInfo.emplace_back(decomposeDomainAlongLongestCoord<T,Lattice<T>>(Resolution+1,Resolution+1,Resolution+1,i,noSubdomains));
    }
    std::cout << "Finished" << std::endl;

    const SubDomainInformation<T,Lattice<T>> referenceSubDomainInfo = decomposeDomainAlongLongestCoord<T,Lattice<T>>((size_t)(Resolution+1),(size_t)(Resolution+1),(size_t)(Resolution+1),0u,1u);

    std::cout << "Reference Subdomain \n";
    std::cout << referenceSubDomainInfo << std::endl;
    std::cout << "Subdomains that are tested here: " << std::endl;

    for (auto it=subDomInfo.begin();it!=subDomInfo.end();++it)
    {
      std::cout << "Subdomain \n";
      std::cout << *it << std::endl;
    }

    std::cout << std::endl;

    std::cout << "creating CommunicationDataHandler ...";

    std::vector<CommunicationDataHandler<T,Lattice<T>,memory_space::HostHeap>> commDataHandler {};
    for (auto it=subDomInfo.begin();it!=subDomInfo.end();++it)
      commDataHandler.emplace_back(createCommunicationDataHandler<memory_space::HostHeap>(*it));

    std::cout <<"Finished" << std::endl;

    std::cout << "Creating arrays for subdomains ...";

    BlockLattice3D<T,Lattice> subDomains[2] = {{commDataHandler[0].domainInfo, &bulkDynamics},{commDataHandler[1].domainInfo, &bulkDynamics}};

    std::cout << "Finished" << std::endl;

    std::cout << "Init Lattices ... ";
        for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
          for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
             for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
          {
              auto referenceCellView = referenceLattice.get(iX,iY,iZ);
                for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                {
                  referenceCellView[iPop] = iPop + (util::getCellIndex3D(iX,iY,iZ,Resolution+1,Resolution+1)*100);
                }
              Index3D localIndex;
              for (unsigned i =0;i<noSubdomains;++i)
              if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
              {
                auto cellView = subDomains[i].get(localIndex.index[0],localIndex.index[1],localIndex.index[2]);
                for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                {
                  cellView[iPop] = iPop + (util::getCellIndex3D(iX,iY,iZ,Resolution+1,Resolution+1)*100);

                }  
                  
                // std::cout << "index " << iX << " " << iY << " " << iZ << " value should" << idx << " value is " << subDomains[i][util::getCellIndex3D(localIndex.index[0],localIndex.index[1],localIndex.index[2],commDataHandler[i].domainInfo.localGridSize().index[1],commDataHandler[i].domainInfo.localGridSize().index[2])]<< " at index" << localIndex.index[0] << " " << localIndex.index[1] << " " << localIndex.index[2] << " of subdomain " << i << "\n";
              }

          }

    std::cout << "Finished!" << std::endl;
    std::cout << "Testing values in Lattices pre comm... ";

        for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
          for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
             for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
          {
              auto referenceCellView = referenceLattice.get(iX,iY,iZ);
              Index3D localIndex;
              for (unsigned i =0;i<noSubdomains;++i)
              if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
              {
                auto cellView = subDomains[i].get(localIndex.index[0],localIndex.index[1],localIndex.index[2]);
                // std::cout << "Cell in interface " << iX << " " << iY << " " << iZ << " reference " << 
                    // referenceLattice.get(iX,iY,iZ)
                  // <<  " actual " << cellView << " in sublattice " << i << std::endl;
                if (referenceCellView != cellView)
                {
                  std::cout << "Mismatch found in " << iX << " " << iY << " " << iZ << " value should be " << 
                    referenceCellView
                  <<  " value is " << cellView << std::endl;
                  exit(99);
                }
              }
          }

    std::cout << "success" << std::endl;


    for (auto it=commDataHandler.begin();it!=commDataHandler.end();++it)
      std::cout << *it << std::endl;

  // for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  // {
      // for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      // {
    
       // if (&commDataHandler[0].getCommunicationDataOut()[neighbour].popArrays[iPop].popData[0] != nullptr)
         // for (unsigned iX = iXS; iX<=iXE;++iX)
          // for (unsigned iY = iYS; iY<=iYE;++iY)
            // for (unsigned iZ = iZS; iZ<=iZE;++iZ)
            // {
               // commDataHandler[0].getCommunicationDataOut()[neighbour].popArrays[iPop].popData[util::getCellIndex3D(0, 0, 0,
               // commDataHandler[0].getCommunicationDataOut()[neighbour].size[1],commDataHandler[0].getCommunicationDataOut()[neighbour].size[2])]
               // = 5;/[>getData(iX,iY,iZ,iPop);
            // }
      // }
  // }
    std::cout << "Performing initial write to commbuffers ...";
    // first write in order to avoid check for first timestep
    for (unsigned i=0;i<noSubdomains;++i)
      subDomains[i].writeToCommBuffer(commDataHandler[i]);      
    std::cout << "finished"<< std::endl;

    std::cout << "Testing values in communication buffers out ... \n";
    for (unsigned i=0;i<noSubdomains;++i)
      for (unsigned nId=0;nId<Lattice<T>::q;++nId)
      {
       if (commDataHandler[i].domainInfo.hasNeighbour[nId])
       {
        std::cout << "values  for subdomain " << i << " ";
        for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
        {
          if (commDataHandler[i].getCommunicationDataOut()[nId].popArrays[iPop].popData)
            std::cout << commDataHandler[i].getCommunicationDataOut()[nId].popArrays[iPop].popData[0] << " ";
        }

        std::cout << std::endl;
       }

      }

    std::cout << "finished" << std::endl;
    // std::this_thread::sleep_for (std::chrono::seconds(5));

    std::cout << "Communication Step ...\n";
    communicate(commDataHandler);
    std::cout << "finished" << std::endl;

    std::cout << "Testing values in communication buffers in ... \n";
    for (unsigned i=0;i<noSubdomains;++i)
      for (unsigned nId=0;nId<Lattice<T>::q;++nId)
      {
       if (commDataHandler[i].domainInfo.hasNeighbour[Lattice<T>::opposite(nId)])
       {
        std::cout << "values  for subdomain " << i << " ";
        for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
        {
          if (commDataHandler[i].getCommunicationDataIn()[nId].popArrays[iPop].popData)
            std::cout << commDataHandler[i].getCommunicationDataIn()[nId].popArrays[iPop].popData[0] << " ";
        }

        std::cout << std::endl;
       }

      }
    std::cout << "finished" << std::endl;

    std::cout << "Reading data from comm buffers ...";
    for (unsigned i=0;i<noSubdomains;++i)
      subDomains[i].readFromCommBuffer(commDataHandler[i]);
    std::cout << "finished" << std::endl;

    std::cout <<"Check for equality with refrence domain at the corresponding locations \n" << std::endl;

        for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
          for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
             for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
          {
              auto referenceCellView = referenceLattice.get(iX,iY,iZ);
              Index3D localIndex;
              for (unsigned i =0;i<noSubdomains;++i)
              {
              if (15 == iZ) // normal direction should be 0,0,1 -> pops that should come in are 3,6,8,16,18
              {
                if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
                {
                  //TODO check against reference
                  // std::cout << "Cell sublattice " << localIndex.index[0] << " " << localIndex.index[1] << " " << localIndex.index[2] << std::endl;
                  auto cellView = subDomains[i].get(localIndex.index[0],localIndex.index[1],localIndex.index[2]);
                  // std::cout << "Cell in interface " << iX << " " << iY << " " << iZ << " reference " << 
                      // referenceLattice.get(iX,iY,iZ+1)
                    // <<  " actual " << cellView << std::endl;
                  for (unsigned population=0;population<Lattice<T>::q;++population)
                    if (cellView[population].get() != referenceLattice.get(iX,iY,iZ)[population].get())
                    {
                      if (cellView[population].get() != referenceLattice.get(iX,iY,iZ+1)[population].get())
                      {
                        std::cout << "population "<<population<< " domain " << i << " of " << iX << " " << iY<< " " <<iZ << " equals " << cellView[population].get() << std::endl;
                            
                        exit(98);
                      }
                    }
                  // if (cellView != referenceLattice.get(iX,iY,iZ+1))
                  // {
                    // std::cout << "Mismatch found in " << iX << " " << iY << " " << iZ << " value should be " << 
                      // referenceLattice.get(iX,iY,iZ+1)
                    // <<  " value is " << cellView << " sublattice " << i <<  std::endl;
                    // // exit(98);
                  // }
                }
                //TODO check against cell in other subdomain
              }
              if (16 == iZ)// normal direction is 0,0,-1 -> incoming pops are 7,9,12,15,17
              {
                if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
                {
                  //TODO check against reference
                  // std::cout << "Cell sublattice " << localIndex.index[0] << " " << localIndex.index[1] << " " << localIndex.index[2] << std::endl;
                  auto cellView = subDomains[i].get(localIndex.index[0],localIndex.index[1],localIndex.index[2]);
                  // std::cout << "Cell in interface " << iX << " " << iY << " " << iZ << " reference " << 
                      // referenceLattice.get(iX,iY,iZ+1)
                    // <<  " actual " << cellView << std::endl;
                  for (unsigned population=0;population<Lattice<T>::q;++population)
                    if (cellView[population].get() != referenceLattice.get(iX,iY,iZ)[population].get())
                    {
                      if (cellView[population].get() != referenceLattice.get(iX,iY,iZ-1)[population].get())
                      {
                        std::cout << "population "<<population<< " domain " << i << " of " << iX << " " << iY<< " " <<iZ << " equals " << cellView[population].get() << std::endl;
                            
                        exit(98);
                      }
                    }
                  // if (cellView != referenceLattice.get(iX,iY,iZ-1))
                  // {
                    // std::cout << "Mismatch found in " << iX << " " << iY << " " << iZ << " value should be " << 
                      // referenceLattice.get(iX,iY,iZ-1)
                    // <<  " value is " << cellView << " sublattice " << i <<  std::endl;
                    // exit(98);
                  // }
                }
                //TODO check against cell in other subdomain
              }


              // else if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
              // {
                // auto cellView = subDomains[i].get(localIndex.index[0],localIndex.index[1],localIndex.index[2]);
                // if (referenceCellView != cellView)
                // {
                  // std::cout << "Mismatch found in " << iX << " " << iY << " " << iZ << " value should be " << 
                    // referenceCellView
                  // <<  " value is " << cellView << std::endl;
                  // exit(99);
                // }
              // }
              }
          }

    std::cout << "success" << std::endl;
}
    return 0;
}
