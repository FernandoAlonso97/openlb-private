/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: b.horvat@tum.de
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "core/util.h"
#include "boundary/momentaOnBoundaries.h"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics,const  SubDomainInformation<T,Lattice<T>> &domainInfo, const SubDomainInformation<T,Lattice<T>> &refDomain)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0]-1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1]-1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2]-1;

    T omega = dynamics.getOmega();

    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N(omega,0.2);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 0,1,0>,PlaneFdBoundaryProcessor3D
        <T,Lattice, 0,1>> plane0P(omega,0.05);
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 1,-1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 1,-1>> plane1N(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 1,1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 1,1>> plane1P(omega,0.05);
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P(omega,0.2);

    for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
      for (unsigned iZ = iZFrontBorder+1;iZ<= iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder,iY,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane0N);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
          // std::cout << "index " << iXLeftBorder << " " << iY << " " << iZ << " domain no " << domainInfo.localSubDomain << " local index " << localIndex << std::endl;
        }
      }
    // lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    
    for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
      for (unsigned iZ = iZFrontBorder+1;iZ<= iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder,iY,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane0P);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);

    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      for (unsigned iZ = iZFrontBorder+1;iZ<= iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iYBottomBorder,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane1N);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    
    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      for (unsigned iZ = iZFrontBorder+1;iZ<= iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iYTopBorder,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane1P);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    
    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      for (unsigned iY = iYBottomBorder+1;iY<= iYTopBorder-1;++iY)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iY,iZFrontBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane2N);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);

    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      for (unsigned iY = iYBottomBorder+1;iY<= iYTopBorder-1;++iY)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iY,iZBackBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane2P);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);

    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, -1>> edge0PN(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, -1>> edge0NN(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,  1>> edge0PP(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,  1>> edge0NP(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, -1>> edge1PN(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, -1>> edge1NN(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,  1>> edge1PP(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,  1>> edge1NP(omega,0.2);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, -1>> edge2NN(omega,0.2);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 2,-1, 1>> edge2NP(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1,-1>> edge2PN(omega,0.05);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;,
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1, 1>> edge2PP(omega,0.05);


    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iYTopBorder,iZFrontBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0PN);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    //
    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iYBottomBorder,iZFrontBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0NN);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder,&edge0NN);


    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iYTopBorder,iZBackBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0PP);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iX,iYBottomBorder,iZBackBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0NP);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);



    for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder,iY,iZBackBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1PN);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    //
    for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder,iY,iZFrontBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1NN);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    //
    for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder,iY,iZBackBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1PP);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    //
    for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder,iY,iZFrontBorder,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1NP);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);




    for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2PN);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    // "
    for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2NN);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    //
    for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2PP);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    //
    for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
      {
        Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZ,localIndex))
        {
          // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2NP);
          lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
        }
      }
    // lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    Index3D localIndex;
    if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZFrontBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNNN);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    //
    if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZFrontBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPNN);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);

    if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZFrontBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNPN);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    //
    if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZBackBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNNP);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    //
    if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZFrontBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPPN);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    //
    if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZBackBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPNP);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    //
    if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZBackBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNPP);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    //
    if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZBackBorder,localIndex))
    {
      // lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPPP);
      lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
    }
    // lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

using ExtFieldType = PredefinedTimeVariantExternalField<T,Lattice>;

template<template<typename> class Memory>
void MultipleSteps(const double simTime,UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> converter,CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler,Index3D globalDomain,const SubDomainInformation<T,Lattice<T>>& refSubDomain )
{

  // int iXLeftBorder = 0;
  // int iXRightBorder = (RESOLUTION+1)*1.5-1;
  // int iYBottomBorder = 0;
  // int iYTopBorder =  (RESOLUTION+1)*1.5-1;
  // int iZFrontBorder = 0;
  // int iZBackBorder = (RESOLUTION+1)*1.5-1;

  auto domainInfo = commDataHandler.domainInfo;
  std::ofstream logFile("./test_out.txt");

  converter.print();

  std::cout << "Calculation steps: " << converter.getLatticeTime(simTime) << std::endl;


  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.05);

  std::cout << "Create blockLattice.... ";


//  PredefinedTimeVariantExternalField<T,Lattice> externalFieldClass(externalFields, outsideVelocity,
//          2.0/converter.getPhysDeltaT(), extent[0], extent[1], extent[2], 1.0);
// ConstExternalField<T,Lattice> externalFieldClass(outsideVelocity);

  // BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder +1, &bulkDynamics);
  BlockLattice3D<T,Lattice> lattice = {commDataHandler.domainInfo, &bulkDynamics};
  BlockLattice3D<T,Lattice> refLattice {refSubDomain,&bulkDynamics};
  // BlockLattice3D<T,Lattice> lattice[1] = {{commDataHandler[0].domainInfo, &bulkDynamics}};
  // for (auto& domain : commDataHandler)
    // lattice.emplace_back(domain.domainInfo, &bulkDynamics);

  std::cout << "Finished!" << std::endl;

  std::cout << "Define boundaries.... "<< std::endl;
  for (unsigned i=0;i<domainInfo.noSubDomains;++i)
    defineBoundaries(lattice,bulkDynamics,commDataHandler.domainInfo,refSubDomain);

  defineBoundaries(refLattice,bulkDynamics,refSubDomain,refSubDomain);
  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  T force[3] = {0.,0.,0.};
  T coreVel[3] = {1.0,0,0};
  T coreRadius = 3;
  T center[3] = {16,16,16};

  std::cout << "Init equilibrium.... ";
      for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
        for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
           for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
        {

          T rad[Lattice<T>::d] = {iX-center[0],iY-center[1],iZ-center[2]};
          T dist = sqrt(rad[0]*rad[0]+rad[1]*rad[1]+rad[2]*rad[2]);
                for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                {
                  *(refLattice.getData(iX,iY,iZ,iPop)) = iPop + (util::getCellIndex3D(iX,iY,iZ,globalDomain.index[1],globalDomain.index[2])*100);
                  if (iX==15 && iY==0 && iZ==0)
                    std::cout << *(refLattice.getData(iX,iY,iZ,iPop)) << std::endl;

                }  

               // if (dist>coreRadius)
               // {
                  // T vel[Lattice<T>::d] = { (coreVel[0]/(dist-coreRadius/2))/converter.getConversionFactorVelocity(),
                                           // (coreVel[1]/(dist-coreRadius/2))/converter.getConversionFactorVelocity(),
                                           // (coreVel[2]/(dist-coreRadius/2))/converter.getConversionFactorVelocity()};
                  // refLattice.iniEquilibrium(iX,iX,iY,iY,iZ,iZ,1.,vel);
               // }
               // else
               // {
                  // T vel[Lattice<T>::d] = { (coreVel[0])/converter.getConversionFactorVelocity(),
                                           // (coreVel[1])/converter.getConversionFactorVelocity(),
                                           // (coreVel[2])/converter.getConversionFactorVelocity()};
                  // refLattice.iniEquilibrium(iX,iX,iY,iY,iZ,iZ,1.,vel);
               // }

          Index3D localIndex;
            // for (unsigned i =0;i<domainInfo.noSubDomains;++i)
            if (commDataHandler.domainInfo.isLocal(iX,iY,iZ,localIndex))
            {
                for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                {
                  *(lattice.getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) = iPop + (util::getCellIndex3D(iX,iY,iZ,globalDomain.index[1],globalDomain.index[2])*100);
                  if (iX==15 && iY==0 && iZ==0)
                    std::cout <<" index " << iX << " " << iY << " " << iZ << " " <<  *(lattice.getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) << std::endl;

                }  
               // if (dist>coreRadius)
               // {
                  // T vel[Lattice<T>::d] = { (coreVel[0]/(dist-coreRadius/2))/converter.getConversionFactorVelocity(),
                                           // (coreVel[1]/(dist-coreRadius/2))/converter.getConversionFactorVelocity(),
                                           // (coreVel[2]/(dist-coreRadius/2))/converter.getConversionFactorVelocity()};
               // lattice.iniEquilibrium(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2],1.,vel);
               // }
               // else
               // {
                  // T vel[Lattice<T>::d] = { (coreVel[0])/converter.getConversionFactorVelocity(),
                                           // (coreVel[1])/converter.getConversionFactorVelocity(),
                                           // (coreVel[2])/converter.getConversionFactorVelocity()};
               // lattice.iniEquilibrium(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2],1.,vel);
               // }

                for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                {
                  // *(lattice[i].getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) = iPop + (util::getCellIndex3D(iX,iY,iZ,globalDomain.index[1],globalDomain.index[2])*100);
                  if (iX==15  && iY==0 && iZ==0)
                    std::cout << " values assigned " << *(lattice.getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) << std::endl;

                }  

            }
        }

  std::cout << "Finished!" << std::endl;

  // for (unsigned i=0;i<domainInfo.noSubDomains;++i)
  // {
    std::cout << "Init GPU data.... ";
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;
    std::cout << "Copy GPU data to CPU.... ";
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;
  // }

  refLattice.initDataArrays();
  refLattice.copyDataToGPU();

  std::string name;
  std::string name0;
  std::string name1;
  std::string directory = "./";
  name = "test";
  name += "_";
  name0 = name + std::to_string(commDataHandler.domainInfo.globalIndexEnd) + "_" + std::to_string(commDataHandler.domainInfo.localSubDomain);
  // name1 = name + std::to_string(commDataHandler[1].domainInfo.globalIndexEnd[0]) + "_" + std::to_string(commDataHandler[1].domainInfo.localSubDomain);

  BlockLatticeDensity3D<T,Lattice> densityFunctor0(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor0(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor0(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor0(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor0(lattice);

  // BlockLatticeDensity3D<T,Lattice> densityFunctor1(lattice[1]);
  // BlockLatticeVelocity3D<T,Lattice> velocityFunctor1(lattice[1]);
  // BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor1(lattice[1],0,converter);
  // BlockLatticeForce3D<T,Lattice> forceFunctor1(lattice[1]);
  // BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor1(lattice[1]);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriter3D<T> vtkWriter0( name0 );
  vtkWriter0.addFunctor(densityFunctor0);
  vtkWriter0.addFunctor(velocityFunctor0);
  vtkWriter0.addFunctor(physVelocityFunctor0);
  vtkWriter0.addFunctor(forceFunctor0);
  vtkWriter0.addFunctor(fluidMaskFunctor0);

  // BlockVTKwriter3D<T> vtkWriter1( name1 );
  // vtkWriter1.addFunctor(densityFunctor1);
  // vtkWriter1.addFunctor(velocityFunctor1);
  // vtkWriter1.addFunctor(physVelocityFunctor1);
  // vtkWriter1.addFunctor(forceFunctor1);
  // vtkWriter1.addFunctor(fluidMaskFunctor1);

  vtkWriter0.write(0);
  // vtkWriter1.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz()*domainInfo.noSubDomains);
  timer.start();

  std::cout << "Starting time simulation" << std::setprecision(20) <<  std::endl;
  unsigned int print = 1;

  // first write in order to avoid check for first timestep
  // for (unsigned i=0;i<domainInfo.noSubDomains;++i)
    // lattice[i].writeToCommBuffer(commDataHandler[i]);
  // std::cout <<"after initial write" << std::endl;
  // communicate(commDataHandler);
  // std::cout <<"after initial communicate" << std::endl;
  // for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
  // std::cout << " values pre start collision " << *(lattice[0].getData(15,0,0,iPop)) << std::endl;
  //
  //
  
#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice,commDataHandler);
  ipcCommunication<T,Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice,commDataHandler);
  NumaSwapCommunication<T,Lattice<T>,MemSpace> communication{commDataHandler};
#endif

  for(unsigned int timeStep = 1; timeStep <4 /*converter.getLatticeTime(simTime)-1*/; ++timeStep)
  {
        // for (unsigned j=0;j<domainInfo.noSubDomains;++j)
          // lattice[j].copyDataToCPU();

      for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
        for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
           for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
        {
          Index3D localIndex;
            // for (unsigned i =0;i<domainInfo.noSubDomains;++i)
            if (commDataHandler.domainInfo.isLocal(iX,iY,iZ,localIndex))
            {
                for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                {
                  if (*(lattice.getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) != *(refLattice.getData(iX,iY,iZ,iPop)))
                  {
                    std::cout << "Mismatch in values of lattice " << commDataHandler.domainInfo.localSubDomain << " to reference lattice found at position " << iX << " " << iY << " " << iZ << " in pop " << iPop << " value " << *(lattice.getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) << " vs reference " << *(refLattice.getData(iX,iY,iZ,iPop)) << " fluidMask " << lattice.getMaskEntry(localIndex.index[0],localIndex.index[1],localIndex.index[2]) <<  std::endl; 
                  }

                }  
            }
        }

        std::cout << "Test in timestep " << timeStep << " completed" << std::endl;
        // lattice[j].copyDataToGPU();
        //
        // lattice[j].collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>(commDataHandler[j],communication);
#ifdef ENABLE_CUDA
      collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
#else
      collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
#endif

#ifdef ENABLE_CUDA
       // for (unsigned i =0;i<domainInfo.noSubDomains;++i)
            lattice.copyDataToCPU();
       cudaDeviceSynchronize();
#endif
        // lattice[j].copyDataToCPU();

        refLattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();
        // for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
        // std::cout << " values after collision " << *(lattice[0].getData(15,0,0,iPop)) << std::endl;
      // for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
        // for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
           // for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
        // {
          // Index3D localIndex;
            // for (unsigned i =0;i<domainInfo.noSubDomains;++i)
            // if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
            // {
                // for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
                // {
                  // *(lattice[i].getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) = -1.0* *(lattice[i].getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop));
                  // if (iX==15 && iY==0 && iZ==0)
                    // std::cout << *(lattice[i].getData(localIndex.index[0],localIndex.index[1],localIndex.index[2],iPop)) << std::endl;

                // }  
            // }
        // }

        // for (unsigned j=0;j<domainInfo.noSubDomains;++j)
        // {
        // lattice[j].writeToCommBuffer(commDataHandler[j]);
        // // std::cout << "after write \n" << commDataHandler[i] << std::endl;
        // }

        // communicate(commDataHandler);
        // std::cout << "after communicate \n" << std::endl;
        
        // for (unsigned j=0;j<domainInfo.noSubDomains;++j)
        // lattice[j].readFromCommBuffer(commDataHandler[j]);

        std::cout << "end of timestep " << timeStep << std::endl;
        if(true or timeStep%converter.getLatticeTime(0.05) == 0)
        {
            vtkWriter0.write(std::round(converter.getPhysTime(timeStep)*1000));
            // vtkWriter1.write(std::round(converter.getPhysTime(timeStep)*1000));
            ++print;
        }
  }

  // for (unsigned i=0;i<domainInfo.noSubDomains;++i)
  lattice.copyDataToCPU();

  vtkWriter0.write(converter.getLatticeTime(simTime));
  // vtkWriter1.write(converter.getLatticeTime(simTime));

  std::cout << "finished" << std::endl;
}

int main(int argc, char** argv)
{
    const int Resolution = 7;


    UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
    		Resolution
            ,0.3*1.0/std::sqrt(3)
            ,4.*5.1
            ,120.
            ,0.0000146072
            ,1.225
            ,0);

    converter.print();

    Index3D globalDomain{Resolution+1,Resolution+1,Resolution+1,0};

    int rank = initIPC();

    const SubDomainInformation<T,Lattice<T>> subDomainInfo = decomposeDomainAlongLongestCoord<T,Lattice<T>>((size_t)(Resolution+1),(size_t)(Resolution+1),(size_t)(Resolution+1),rank,getNoRanks());

    const SubDomainInformation<T,Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T,Lattice<T>>((size_t)(Resolution+1),(size_t)(Resolution+1),(size_t)(Resolution+1),0u,1u);

    std::cout << subDomainInfo;
    std::cout << "####" << std::endl;


    CommunicationDataHandler<T,Lattice<T>,MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    std::cout << commDataHandler << std::endl; 
    std::cout << "####################################" << std::endl;

    T const velocityConversion = converter.getConversionFactorVelocity();
    std::vector<int64_t> extent(3);

    const double simTime = 2;

    MultipleSteps(simTime,converter,commDataHandler,globalDomain,refSubDomain);
    std::cout << "success" << std::endl;
	    
    cudaDeviceSynchronize();
    MPI_Finalize();

    return 0;
}
