/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: b.horvat@tum.de
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/util.h"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/MemorySpace/memory_spaces.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;



int main(int argc, char** argv)
{
    const int Resolution = 31;
    const int noSubdomains = 2;
    Index3D globalDomain{Resolution+1,Resolution+1,Resolution+1,0};

    std::cout << "Allocate Array ... ";

    auto referenceArray = memory_space::HostHeap<T>{(Resolution+1)*(Resolution+1)*(Resolution+1)};

    std::cout << "Finished" << std::endl;

    std::cout << "Creating SubDomainInformations ... ";
    std::vector<SubDomainInformation<T,Lattice<T>>> subDomInfo{};
    
    for (unsigned i=0;i<noSubdomains;++i)
    {
      subDomInfo.emplace_back(decomposeDomainAlongLongestCoord<T,Lattice<T>>(Resolution+1,Resolution+1,Resolution+1,i,noSubdomains));
    }
    std::cout << "Finished" << std::endl;

    const SubDomainInformation<T,Lattice<T>> referenceSubDomainInfo = decomposeDomainAlongLongestCoord<T,Lattice<T>>((size_t)(Resolution+1),(size_t)(Resolution+1),(size_t)(Resolution+1),0u,1u);

    std::cout << "Reference Subdomain \n";
    std::cout << referenceSubDomainInfo << std::endl;
    std::cout << "Subdomains that are tested here: " << std::endl;

    for (auto it=subDomInfo.begin();it!=subDomInfo.end();++it)
    {
      std::cout << "Subdomain \n";
      std::cout << *it << std::endl;
    }

    std::cout << std::endl;
    std::cout << "Creating arrays for subdomains ...";

    memory_space::HostHeap<T> subDomains[noSubdomains];
    for (unsigned i=0;i<subDomInfo.size();++i)
      subDomains[i] = memory_space::HostHeap<T>{subDomInfo[i].localGridSize().index[0]*subDomInfo[i].localGridSize().index[1]*subDomInfo[i].localGridSize().index[2]};

    std::cout << "Finished" << std::endl;
    std::cout << "creating CommunicationDataHandler ...";

    std::vector<CommunicationDataHandler<T,Lattice<T>,memory_space::HostHeap>> commDataHandler {};
    for (auto it=subDomInfo.begin();it!=subDomInfo.end();++it)
      commDataHandler.emplace_back(createCommunicationDataHandler<memory_space::HostHeap>(*it));

    std::cout <<"Finished" << std::endl;

    unsigned idx = 0;
    std::cout << "Init Array ... ";
        for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
          for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
             for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
          {
              referenceArray[util::getCellIndex3D(iX,iY,iZ,Resolution+1,Resolution+1)] = idx;
              Index3D localIndex;
              for (unsigned i =0;i<noSubdomains;++i)
              if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
              {
                subDomains[i][util::getCellIndex3D(localIndex.index[0],localIndex.index[1],localIndex.index[2],commDataHandler[i].domainInfo.localGridSize().index[1],commDataHandler[i].domainInfo.localGridSize().index[2])] = idx;
                // std::cout << "index " << iX << " " << iY << " " << iZ << " value should" << idx << " value is " << subDomains[i][util::getCellIndex3D(localIndex.index[0],localIndex.index[1],localIndex.index[2],commDataHandler[i].domainInfo.localGridSize().index[1],commDataHandler[i].domainInfo.localGridSize().index[2])]<< " at index" << localIndex.index[0] << " " << localIndex.index[1] << " " << localIndex.index[2] << " of subdomain " << i << "\n";
              }

              ++idx;
          }

    std::cout << "Finished!" << std::endl;
    std::cout << "Testing values in arrays ... ";

        for (unsigned iX = 0;iX<globalDomain.index[0];++iX)
          for (unsigned iY = 0;iY<globalDomain.index[1];++iY)
             for (unsigned iZ = 0;iZ<globalDomain.index[2];++iZ)
          {
              T val = -1;
              Index3D localIndex;
              for (unsigned i =0;i<noSubdomains;++i)
              if (commDataHandler[i].domainInfo.isLocal(iX,iY,iZ,localIndex))
              {
                val = subDomains[i][util::getCellIndex3D(localIndex.index[0],localIndex.index[1],localIndex.index[2],commDataHandler[i].domainInfo.localGridSize().index[1],commDataHandler[i].domainInfo.localGridSize().index[2])];
              }

              if (referenceArray[util::getCellIndex3D(iX,iY,iZ,Resolution+1,Resolution+1)] != val)
              {
                std::cout << "Mismatch found in " << iX << " " << iY << " " << iZ << " value should be " << 
                  referenceArray[util::getCellIndex3D(iX,iY,iZ,Resolution+1,Resolution+1)]
                <<  " value is " << val << std::endl;
                exit(99);
              }
          }

    std::cout << "success" << std::endl;
	    
    return 0;
}
