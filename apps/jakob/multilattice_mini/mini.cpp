/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

// #include "core/cell.hh"
// #include "dynamics/latticeDescriptors.hh"
// #include "core/blockLattice3D.hh"
// #include "core/blockLatticeALE3D.hh"
// #include "core/externalFieldALE.hh"
// #include "boundary/boundaryCondition3D.hh"
// #include "boundary/boundaryPostProcessors3D.hh"
// #include "boundary/momentaOnBoundaries.hh"
// #include "boundary/momentaOnBoundaries3D.hh"
// #include "dynamics/smagorinskyBGKdynamics.hh"
// #include "dynamics/dynamics.hh"
// #include "core/singleton.h"
// #include "core/unitConverter.hh"
// #include "functors/lattice/blockLatticeLocalF3D.hh"
// #include "utilities/timer.hh"
// #include "io/gpuIOFunctor.h"
// #include <fstream>
// #include <cmath>
// #include <ctime>
// #include <chrono>
// #include <thread>
// #include "contrib/domainDecomposition/domainDecomposition.h"
// #include "contrib/domainDecomposition/communication.h"
// #include "contrib/domainDecomposition/cudaIPC.h"
// #include "contrib/domainDecomposition/mpiCommunication.h"
// #include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"


#include "contrib/domainDecomposition/communication.h"
#include "dynamics/latticeDescriptors.hh"
#include "boundary/boundaryPostProcessors3D.hh"
#include "core/blockLattice3D.hh"
#include "contrib/domainDecomposition/mpiCommunication.h"
#include "core/blockLatticeALE3D.hh"
#include "io/gpuIOFunctor.h"
#include "core/externalFieldALE.hh"
#include "boundary/boundaryCondition3D.hh"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/momentaOnBoundaries3D.hh"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/cell.hh"
#include "dynamics/dynamics.hh"
#include "core/singleton.h"
#include "core/unitConverter.hh"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.hh"
#include <fstream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <thread>
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,0,-1>> slipDynamics{};
static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,-1,0>> slipDynamicsYMinus{};
static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,1,0>> slipDynamicsYPlus{};
static GhostDynamics<T, Lattice> ghostLayerDynamics(1.0);

#include <fstream>
//#include <cuda_runtime.h>
//#include <device_launch_parameters>
#include <curand_kernel.h>
#include <ctime>

template <typename T,template <typename U> class DESCRIPTOR, class BlockLatticeInterface>
class BlockLatticePopulation3D: public BlockLatticeF3D<T,DESCRIPTOR>{

  public: 
  BlockLatticePopulation3D () = delete;
  explicit BlockLatticePopulation3D (BlockLatticeStructure3D<T,DESCRIPTOR>& bl): BlockLatticeF3D<T,DESCRIPTOR>(bl,DESCRIPTOR<T>::q),_bl(static_cast<BlockLatticeInterface &>(bl)) {this->getName() = "population";}

  bool operator() (T output[], const int input[]) override
  {
    for (unsigned iPop=0;iPop<DESCRIPTOR<T>::q;++iPop)
    {
      output[iPop] = _bl.getData()[iPop][_bl.getCellIndex(input[0],input[1],input[2])];
    }
    return true;
  }

  private:

  BlockLatticeInterface& _bl;

};


template<typename T,template <typename U> class Lattice>
class PowerLawBL{

public:
	PowerLawBL() = delete;

	PowerLawBL(SubDomainInformation<T,Lattice<T>> subDomainInfo,size_t iX0, size_t iX1, size_t iY0, size_t iY1, size_t iZ0, size_t iZ1, T turbulentInensity, T blHeight, T maxVel,int maxZ):
      subDomInfo_{subDomainInfo},
			startIndices{iX0,iY0,iZ0},
			endIndices{iX1,iY1,iZ1},
			size_((iX1-iX0)*(iY1-iY0)*(iZ1-iZ0)),
			turbulentIntensity_(turbulentInensity),
			blHeight_(blHeight),
			maxVel_(maxVel),
			cellCoordinate(maxZ)
	{}

	
    	__device__
    	void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
	{
	
		size_t localIndices[3];
		util::getCellIndices3D(threadIndex,subDomInfo_.localGridSize()[1],subDomInfo_.localGridSize()[2],localIndices);
    
    Index3D globalIndices = subDomInfo_.getGlobalIndex(Index3D{localIndices[0],localIndices[1],localIndices[2],subDomInfo_.localSubDomain});
	  
		if ((globalIndices[0]>=startIndices[0] && globalIndices[0]<endIndices[0]) && (globalIndices[1]>=startIndices[1] && globalIndices[1]<endIndices[1]) && (globalIndices[2]>=startIndices[2] && globalIndices[2]<endIndices[2]))
		{
            		T vel[Lattice<T>::d] = { 0., 0., 0.};
            		T rho=1.0;
           
		           	curandState state;
			          curand_init((unsigned long long)clock() + threadIndex,0,0,&state);

            		double nRandom1 =2*(curand_uniform_double(&state)-0.5);
            		double nRandom2 =2*(curand_uniform_double(&state)-0.5);
            		double nRandom3 =2*(curand_uniform_double(&state)-0.5);

            		T inverseZCoordinate = (cellCoordinate - static_cast<T>(globalIndices[2]));
            		T u_calc = -maxVel_;

            		if (inverseZCoordinate < blHeight_)
               			u_calc = -maxVel_*pow((inverseZCoordinate/blHeight_), 1./7.);

                // u_calc = inverseZCoordinate / cellCoordinate * -maxVel_;

                vel[0] = u_calc + turbulentIntensity_*nRandom1*maxVel_;
                vel[1] = 0.0 + turbulentIntensity_*nRandom2*maxVel_;
                vel[2] = 0.0 + turbulentIntensity_*nRandom3*maxVel_;
	
            		cellData[Lattice<T>::uIndex  ][threadIndex] =vel[0];
            		cellData[Lattice<T>::uIndex+1][threadIndex] =vel[1];
            		cellData[Lattice<T>::uIndex+2][threadIndex] =vel[2]; //0.05

        T uSqr = util::normSqr<T,Lattice<T>::d>(vel);
        for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
           cellData[iPop][threadIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, vel, uSqr);
        }

		}
		else
			return;
	}

	private:
  const SubDomainInformation<T,Lattice<T>> subDomInfo_;
	const size_t startIndices[Lattice<T>::d];
	const size_t endIndices[Lattice<T>::d];
	const size_t size_ = 0;

	const T turbulentIntensity_ = 0.0;
  const T blHeight_ = 0.0;
	const T maxVel_ = 0.0;

	const int cellCoordinate = 0;

};


template<typename T,template <typename U> class Lattice>
class ReadRhoUFunctor{

	public:
	ReadRhoUFunctor() = delete;

	OPENLB_HOST_DEVICE
	ReadRhoUFunctor(size_t iX0, size_t iX1, size_t iY0, size_t iY1, size_t iZ0, size_t iZ1, size_t ny, size_t nz):
			startIndices{iX0,iY0,iZ0},
			endIndices{iX1,iY1,iZ1},
			size_((iX1-iX0)*(iY1-iY0)*(iZ1-iZ0)),
			ny_(ny),
			nz_(nz)
	{
		for (int i =0;i<Lattice<T>::d+1;++i)
			HANDLE_ERROR(cudaMallocManaged(&data_[i],sizeof(T)*size_));

	}

    	OPENLB_HOST_DEVICE
    	void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
	{
	
		size_t indices[3];
		util::getCellIndices3D(threadIndex,ny_,nz_,indices);
	  
		if ((indices[0]>=startIndices[0] && indices[0]<endIndices[0]) && (indices[1]>=startIndices[1] && indices[1]<endIndices[1]) && (indices[2]>=startIndices[2] && indices[2]<endIndices[2]))
			{

		 		size_t localIndex = util::getCellIndex3D(indices[0]-startIndices[0],
									 indices[1]-startIndices[1],
									 indices[2]-startIndices[2],
									 endIndices[1]-startIndices[1],
									 endIndices[2]-startIndices[2]);
				
				cellData[Lattice<T>::rhoIndex][threadIndex] = data_[0][localIndex];		
				for (int i=0;i<Lattice<T>::d;++i)
					cellData[Lattice<T>::uIndex+i][threadIndex] = data_[1+i][localIndex];
			
			}
		else 
			return;

	}

	void readFromFile (std::string filename)
	{
    std::ifstream infile;
		infile.open (filename);
		std::string line;
  		 if (infile.is_open())
  		 {
    		 while ( getline (infile,line) )
    		 {
           std::istringstream ss(line);
			int index=0;
			ss >> index;

			if (index<size_)
			for (int i=0;i<Lattice<T>::d+1;++i)
			    ss >> data_[i][index];
			
    		 }
    		 infile.close();
  		}
	}

	private:
	size_t startIndices[Lattice<T>::d];
	size_t endIndices[Lattice<T>::d];
	size_t ny_ = 0;
	size_t nz_ = 0;
	size_t size_ = 0;

	T* data_[Lattice<T>::d+1];

};


template<class Lattice,template <typename> class bullShitFuckopenLB>
void setBlockPosition(Lattice& lattice, T const deltaX)
{
	size_t nx = lattice.getNx();
	size_t ny = lattice.getNy();
	size_t nz = lattice.getNz();

	Vector<T,3> origin1(104./deltaX,50./deltaX,80./deltaX);
	Vector<T,3> extend1(40./deltaX,20./deltaX,5./deltaX);
	Vector<T,3> origin2(124./deltaX,50./deltaX,60./deltaX);
	Vector<T,3> extend2(20./deltaX,20./deltaX,25./deltaX);
	IndicatorCuboid3D<T> obstacle1(extend1,origin1);
	IndicatorCuboid3D<T> obstacle2(extend2,origin2);

	bool* __restrict__ fluidMask = lattice.getFluidMask();

	for(size_t iX = 1; iX < nx-1; ++iX)
		for(size_t iY = 1; iY < ny-1; ++iY)
			for(size_t iZ = 1; iZ < nz-1; ++iZ)
			{
							T location[3] = {iX, iY, iZ};
							bool isInside1 = false;
							bool isInside2 = false;
							obstacle1(&isInside1,location);
							obstacle2(&isInside2,location);

							bool isInside = isInside1 or isInside2;

							if(isInside)
							{
								// std::cout << fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] << std::endl;
								lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,bullShitFuckopenLB>());
								// fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] = false;
								// std::cout << fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] << std::endl;
								// std::cout << iX << "," << iY << "," << iZ << std::endl;
							}
							// else
								// lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,bullShitFuckopenLB>());
								// fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] = true;
			}
	
	// lattice.copyFluidMaskToGPU();

}

template<typename T,typename S>
class IndicatorRightTriangleBlock3D { // TODO at the moment only valid for Z-Direction

  private:

    Vector<S,3> tip_;
    Vector<S,3> base1_; // 90deg angle has to be here!
    Vector<S,3> base2_;
    S           blockHeight_ = 0;
    double      deltaX_ = 0;


  public:
  IndicatorRightTriangleBlock3D() = delete;
  IndicatorRightTriangleBlock3D(Vector<S,3> tip,Vector<S,3> base1, Vector<S,3> base2, S blockHeight,double deltaX):
    tip_(tip),
    base1_(base1),
    base2_(base2),
    blockHeight_(blockHeight),
    deltaX_(deltaX)

  {
  }

  bool operator() (bool output[], const S input[]) 
  {
    int yMean {tip_[1]};
    int xGes {tip_[0]-base1_[0]};
    int xDist {tip_[0]-input[0]};
    int baseLength {base2_[1]-base1_[1]};

    if (input[0] < tip_[0] and input[0]> base1_[0] and input[0] > base2_[0]) 
    {
      if (abs(input[1] - (yMean)) < xDist/xGes * 0.5*baseLength) 
      {
        if (input[2] > tip_[2]-blockHeight_)
        {
          std::cout << "point inside tip " << input[0] << " " << input[1] << " " << input[2] << "\n";
          return true;
        }
      }
    }
    return false;
  }


};

// template<typename T, unsigned SIZE>
// std::ostream& operator << (std::ostream& os, const Vector<T,SIZE> vec)
// {
  // os << "[ ";
  // for (int i =0;i<SIZE;++i)
    // os << vec[i] << " ";
  // os << "] \n";

  // return os;

// }


template<class Lattice,template <typename> class whatever>
void setShipBoundaries (Lattice& lattice,const DomainInformation<T,whatever<T>>& domainInfo, Vector<T,3> shipTipPosition, int noCellsPerUnit,T deltaX)
{
	size_t nx = domainInfo.getLocalInfo().globalIndexEnd[0];
	size_t ny = domainInfo.getLocalInfo().globalIndexEnd[1];
	size_t nz = domainInfo.getLocalInfo().globalIndexEnd[2];

  Vector<T,3> shipBasePosition1 {shipTipPosition + Vector<T,3>{-54*noCellsPerUnit,-9*noCellsPerUnit,0}};
  Vector<T,3> shipBasePosition2 {shipTipPosition + Vector<T,3>{-54*noCellsPerUnit,9*noCellsPerUnit,0}};

  std::cout << "shipBasePosition2 " << shipBasePosition2 << std::endl;

  // IndicatorRightTriangleBlock3D<T,T>shipTip  {shipTipPosition,shipBasePosition1,shipBasePosition2,3*noCellsPerUnit,deltaX};

for(size_t iX = 1; iX < nx-1; ++iX)
	for(size_t iY = 1; iY < ny-1; ++iY)
		for(size_t iZ = 1; iZ < nz-1; ++iZ)
       {
        T yMean {shipTipPosition[1]};
        T xGes {shipTipPosition[0]-shipBasePosition1[0]};
        T xDist {shipTipPosition[0]-iX};
        T baseLength {shipBasePosition2[1]-shipBasePosition1[1]};

	//std::cout << iX << " " << iY << " " << iZ << std::endl;
        if (iX < shipTipPosition[0] and iX> shipBasePosition1[0]) 
        {
          if (abs(iY - (yMean)) < (double)xDist/(double)xGes * 0.5*baseLength) 
          {
            if (iZ > shipTipPosition[2]-6*noCellsPerUnit)
            {
              Index3D localIndex;
              if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIndex))
              {
                 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,whatever>());
              }
              //std::cout << "point inside tip " << iX << " " << iY << " " << iZ << "\n";
							// lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,whatever>());
            }
          }
        }
      }


  Vector<T,3> mainHullExtend {92*noCellsPerUnit,18*noCellsPerUnit,14*noCellsPerUnit};
  Vector<T,3> mainHullOrigin {shipBasePosition2 + Vector<T,3>{-92*noCellsPerUnit,-18*noCellsPerUnit,-14*noCellsPerUnit}};

  std::cout << "hullOrigin " << mainHullOrigin << std::endl;
  std::cout << "hullExtend " << mainHullExtend << std::endl;

  IndicatorCuboid3D<T> mainHull {mainHullExtend,mainHullOrigin};

  Vector<T,3> mainChimneyExtend {8*noCellsPerUnit,4*noCellsPerUnit,8*noCellsPerUnit};
  Vector<T,3> mainChimneyOrigin {shipBasePosition1 + Vector<T,3>{(-92+22)*noCellsPerUnit,(+9-2)*noCellsPerUnit,(-14-8)*noCellsPerUnit}};

  std::cout << "chimneyOrigin " << mainChimneyOrigin << std::endl;
  std::cout << "chimneyExtend " << mainChimneyExtend << std::endl;

  IndicatorCuboid3D<T> chimney {mainChimneyExtend,mainChimneyOrigin};

  Vector<T,3> flightDeckExtend {128*noCellsPerUnit,18*noCellsPerUnit,6*noCellsPerUnit};
  Vector<T,3> flightDeckOrigin {shipBasePosition2+Vector<T,3>{-128*noCellsPerUnit,-18*noCellsPerUnit,-6*noCellsPerUnit}};

  IndicatorCuboid3D<T> flightDeck {flightDeckExtend,flightDeckOrigin};


	bool* __restrict__ fluidMask = lattice.getFluidMask();

  bool isInside [3];
	for(size_t iX = 1; iX < nx-1; ++iX)
		for(size_t iY = 1; iY < ny-1; ++iY)
			for(size_t iZ = 1; iZ < nz-1; ++iZ)
			{
							T location[3] = {iX, iY, iZ};

              if(mainHull(isInside,location) or flightDeck(isInside,location) or chimney(isInside,location))
							{
                Index3D localIndex;
                if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIndex))
                {
                   lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,whatever>());
                }
                // std::cout << "add boundary at " << iX << " " << iY << " " << iZ << "\n";
                // lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,whatever>());
							}

      }


}



template<typename T, template <typename> class Lattice, class BlockLattice>
void defineBoundaries(BlockLattice &lattice, Dynamics<T,Lattice> &dynamics,const SubDomainInformation<T,Lattice<T>>& domainInfo, const SubDomainInformation<T,Lattice<T>>& refDomain )
{
    int iXLeftBorder =   refDomain.globalIndexStart[0];
    int iXRightBorder =  refDomain.globalIndexEnd[0]-1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder =    refDomain.globalIndexEnd[1]-1;
    int iZFrontBorder =  refDomain.globalIndexStart[2];
    int iZBackBorder =   refDomain.globalIndexEnd[2]-1;

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    // OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        // createInterpBoundaryCondition3D<T,Lattice,
        // ConstRhoBGKdynamics>(lattice);



    //boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
    //
    //now done by powerlaw functor
    // boundaryCondition->addVelocityBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );

    // boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );
    // boundaryCondition->addVelocityBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addPeriodicBoundary1N(iXLeftBorder+1,iXRightBorder,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addPeriodicBoundary1P(iXLeftBorder+1,iXRightBorder,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );



    // boundaryCondition->addPeriodicBoundary1N(iXLeftBorder,iXRightBorder,iYBottomBorder  ,iYBottomBorder,iZFrontBorder,iZBackBorder, omega );
    // boundaryCondition->addPeriodicBoundary1P(iXLeftBorder,iXRightBorder,iYTopBorder     ,iYTopBorder   ,iZFrontBorder,iZBackBorder, omega );
    
    // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomB  order, iYBottomBorder , iZFrontBorder  , iZBackBorder , &slipDynamicsYMinus);
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iZ = iZFrontBorder;iZ<=iZBackBorder;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYBottomBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&slipDynamicsYMinus);
          }
         }

     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder , iZFrontBorder  , iZBackBorder , &slipDynamicsYPlus);
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iZ = iZFrontBorder;iZ<=iZBackBorder;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYTopBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&slipDynamicsYPlus);
          }
         }



    // boundaryCondition->addVelocityBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addVelocityBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

//      lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
     // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
    // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &slipDynamics);
    //
     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
     
     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &slipDynamics);
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iY,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&slipDynamics);
          }
         }

     //lattice.defineDynamics(iXLeftBorder+1, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
     
     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iY,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
          }
         }

	  // boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
	  // boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );
    // boundaryCondition->addExternalVelocityEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
    // boundaryCondition->addExternalVelocityEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
		// boundaryCondition->addExternalVelocityEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
		// boundaryCondition->addExternalVelocityEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

      //boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      //boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

	 //  boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
	 //  boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

		// boundaryCondition->addExternalVelocityEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
		// boundaryCondition->addExternalVelocityEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

      //boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      //boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

	//   boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
	//   boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

		// boundaryCondition->addExternalVelocityEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
		// boundaryCondition->addExternalVelocityEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

      //boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      //boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      //boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      //boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

    // boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
    // boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
    // boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
    // boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);
    //
    // boundaryCondition->addExternalVelocityCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
    // boundaryCondition->addExternalVelocityCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
    // boundaryCondition->addExternalVelocityCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
    // boundaryCondition->addExternalVelocityCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);
     // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder , iZFrontBorder  , iZBackBorder , &slipDynamics);
     // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder , iZFrontBorder  , iZBackBorder , &slipDynamics);

//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
    //boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    
    // boundaryCondition->addPressureBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          T vel[3] = {0.0,0.0,0.0};
          if (domainInfo.isLocal(iXLeftBorder,iY,iZ,localIndex))
          {
             boundaryCondition->addPressureBoundary0N(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2],omega);
             // lattice.defineRhoU(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
             // lattice.iniEquilibrium(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
          }
         }

}

template<template<typename> class Memory>
void MultipleSteps(const int simTime, int Resolution,CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler) // numbers of cells per Shiplength
{

  T viscosity = 0.000004426085106;
  //T viscosity = 0.0000184433;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          Resolution
          ,0.3*1.0/std::sqrt(3)
          ,138.6857//0.5901446809// meters shiplength//4*4.92
          ,40
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  std::cout << commDataHandler << std::endl;

  auto domainInfo = commDataHandler.domainInfo;

  T conversionVelocity = converter.getConversionFactorVelocity();

  T deltaX = converter.getConversionFactorLength();

  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.025);
  // NoDynamics<T, Lattice> bulkDynamics(1.0);
  // NoDynamics<T, Lattice> ghostLayerDynamics(1.0);
  // GhostDynamics<T, Lattice> ghostLayerDynamics(1.0);
  // ConstRhoBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);

  std::cout << "Create blockLattice.... ";

  Vec3<T> velocity = {converter.getLatticeVelocity(7.0),0.0,0.0};

  ConstExternalField<T,Lattice> externalFlow(velocity);

  Index3D lattice_anchor_local = domainInfo.getLocalIdx(0,0,0);
  std::cout << "local Lattice anchor in lattice coords " << lattice_anchor_local[0] << " " << lattice_anchor_local[1] << " " << lattice_anchor_local[2] << " in subdomain " << lattice_anchor_local.subDomain << std::endl;
  Vec3<T> lattice_anchor_local_vec {lattice_anchor_local[0], lattice_anchor_local[1], lattice_anchor_local[2]};
  BlockLatticeALE3D<T, Lattice, ConstExternalField<T, Lattice>> lattice(domainInfo.getLocalInfo(), lattice_anchor_local_vec, &bulkDynamics, externalFlow);
  // BlockLattice3D<T, Lattice> lattice(domainInfo.getLocalInfo(), &bulkDynamics);

  std::cout << "Finished!" << std::endl;


  std::cout << "Define boundaries.... ";
  // defineBoundaries(lattice,bulkDynamics,domainInfo.getLocalInfo(),domainInfo.refSubDomain);
  // setBlockPosition<BlockLattice3D<T, Lattice, ConstExternalField<T, Lattice>>,Lattice>(lattice,deltaX);
  // setBlockPosition<BlockLattice3D<T, Lattice>,Lattice>(lattice,deltaX);
  std::cout << "total dimensions " << domainInfo.refSubDomain.globalIndexEnd[0] << " " << domainInfo.refSubDomain.globalIndexEnd[1]  << " " <<domainInfo.refSubDomain.globalIndexEnd[2] << std::endl;
  // Vector<T,3> shipTipPosition {domainInfo.refSubDomain.globalIndexEnd[0]-shipTipPositionDistanceToInflowBC,0.5*domainInfo.refSubDomain.globalIndexEnd[1],domainInfo.refSubDomain.globalIndexEnd[2]};
  // std::cout << "shipTipPosition " <<  shipTipPosition << std::endl;
  // setShipBoundaries<BlockLattice3D<T,Lattice>,Lattice> (lattice,domainInfo,shipTipPosition, minimalShipBuildingBlock,deltaX);

  std::cout << "Finished!" << std::endl;

  std::cout << "define one cell inside domain to be bounceback ..." << std::endl;
          Index3D localIndex;
          if (domainInfo.getLocalInfo().isLocal(1,1,1,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
          }
        lattice.setCurrentFluidMaskAsStationary();
  std::cout << "Finished!" << std::endl;

  std::cout << "Define ghostlayer to be bulkdynamics but fluidmask false ";
  for (unsigned iX=0;iX<domainInfo.getLocalInfo().localGridSize()[0];++iX)
  for (unsigned iY=0;iY<domainInfo.getLocalInfo().localGridSize()[1];++iY)
  for (unsigned iZ=0;iZ<domainInfo.getLocalInfo().localGridSize()[2];++iZ)
  {
    if (   iX<domainInfo.getLocalInfo().ghostLayer[0] or iX>=domainInfo.getLocalInfo().localGridSize()[0]-domainInfo.getLocalInfo().ghostLayer[0]
        or iY<domainInfo.getLocalInfo().ghostLayer[1] or iY>=domainInfo.getLocalInfo().localGridSize()[1]-domainInfo.getLocalInfo().ghostLayer[1]
        or iZ<domainInfo.getLocalInfo().ghostLayer[2] or iZ>=domainInfo.getLocalInfo().localGridSize()[2]-domainInfo.getLocalInfo().ghostLayer[2])
    {
      lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
      // lattice.defineDynamics(iX,iY,iZ,&instances::getBounceBack<T,Lattice>());
    std::cout << "defining for " << iX << " " << iY << " " << iZ << std::endl;
    }
  }

  std::cout << "Finished!" << std::endl;


  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;

  std::cout << "pointer in main of static dynamics " << &ghostLayerDynamics << std::endl;

  // int shipHeight = 22*minimalShipBuildingBlock;
  // int ablHeight = 0;//4*shipHeight;
  // std::cout << "shipHeigt and ablHeigt vs domainHeight " << shipHeight << " " << ablHeight << " " << domainInfo.refSubDomain.globalIndexEnd[2] << std::endl;
  T targetVelocity = 0.0;//30;//15.0;
  T vel[Lattice<T>::d] = {0 , 0., converter.getLatticeVelocity(targetVelocity)};

  std::cout << "Init equilibrium.... ";
  for (int iX = domainInfo.refSubDomain.globalIndexStart[0]; iX <= domainInfo.refSubDomain.globalIndexEnd[0]; ++iX)
      // for (int iY = domainInfo.refSubDomain.globalIndexStart[1]+1; iY <= domainInfo.refSubDomain.globalIndexEnd[1]-1; ++iY)
      for (int iY = domainInfo.refSubDomain.globalIndexStart[1]; iY <= domainInfo.refSubDomain.globalIndexEnd[1]; ++iY)
          for (int iZ = domainInfo.refSubDomain.globalIndexStart[2]; iZ <= domainInfo.refSubDomain.globalIndexEnd[2]; ++iZ)
          {

      Index3D localIndex;
      if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIndex))
      {
        lattice.defineRhoU(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
        lattice.iniEquilibrium(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
      }
  }
  std::cout << "Finished!" << std::endl;

  // for (int iX=0;iX<domainInfo.getLocalInfo().localGridSize()[0];++iX)
  // for (int iY=0;iY<domainInfo.getLocalInfo().localGridSize()[1];++iY)
  // for (int iZ=0;iZ<domainInfo.getLocalInfo().localGridSize()[2];++iZ)
        // for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
        // {
          // // lattice.getDataCPU()[iPop][lattice.getCellIndex(iX,iY,iZ)] = 10000000   *domainInfo.getLocalInfo().localSubDomain +
                                                                       // // 1000000    *iX+
                                                                       // // 10000      *iY+
                                                                       // // 100        *iZ+
                                                                       // // 1          *iPop;
          // T setVal(99);

          // if (iPop == 0)
            // setVal = domainInfo.getLocalInfo().localSubDomain;

          // if (iPop == 1)
            // setVal = iX;

          // if (iPop == 2)
            // setVal = iY;

          // if (iPop == 3)
            // setVal = iZ;
            
          // lattice.getDataCPU()[iPop][lattice.getCellIndex(iX,iY,iZ)] = setVal;// +
                                                                       // // 1000000    *iX+
                                                                       // // 10000      *iY+
                                                                       // // 100        *iZ+
                                                                       // // 1          *iPop;
          // // lattice.defineRhoU(iX,iX,iY,iY,iZ,iZ,1.,vel);
          // // lattice.getDataCPU()[Lattice<T>::rhoIndex][lattice.getCellIndex(iX,iY,iZ)] = 5.0;//100   *domainInfo.getLocalInfo().localSubDomain;// +
          // // std::cout << "indices " << iX << " " << iY << " " << iZ << " " << lattice.getDataCPU()[Lattice<T>::rhoIndex][lattice.getCellIndex(iX,iY,iZ)] << std::endl;
        // }

  std::cout << domainInfo.getLocalInfo() << std::endl;

  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  //lattice.copyDataToCPU();
  std::cout << "Finished!" << std::endl;
  std::string name = "multilattice_mini_rank_" + std::to_string(commDataHandler.domainInfo.localSubDomain) + "_";
  std::string directory = "/2nd_scratch/ga69wij/multilattice_mini_"+std::to_string(Resolution)+"/";

  std::cout << "Running " << name << std::endl;

  singleton::directories().setOutputDir(directory);

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeCoordinates3D<T,Lattice> coordinateFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);
  BlockLatticePopulation3D<T,Lattice,decltype(lattice)> popFunctor (lattice);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterFull( name+"fullDomain_", domainInfo,false,true );
  vtkWriterFull.addFunctor(densityFunctor);
  // vtkWriterFull.addFunctor(velocityFunctor);
  vtkWriterFull.addFunctor(physVelocityFunctor);
  // vtkWriterFull.addFunctor(forceFunctor);
  vtkWriterFull.addFunctor(coordinateFunctor);
  vtkWriterFull.addFunctor(fluidMaskFunctor);
  vtkWriterFull.addFunctor(popFunctor);


  util::Timer<T> timer(simTime,domainInfo.refSubDomain.globalIndexEnd[0]*domainInfo.refSubDomain.globalIndexEnd[1]*domainInfo.refSubDomain.globalIndexEnd[2]);
  timer.start();

  std::cout << "Starting time simulation" << std::endl;
  std::cout << "Steps: " << simTime << std::endl;

  Vec3<T> zeroVec = {0.,0.,0.};
  Vec3<T> position = {0.,0.,0.};
  Vec3<T> movement = {converter.getLatticeVelocity(3),0.,0.};

  lattice.copyDataToCPU();

  size_t originFull[3] = {domainInfo.refSubDomain.globalIndexStart[0],domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendFull[3] = {domainInfo.refSubDomain.globalIndexEnd[0],domainInfo.refSubDomain.globalIndexEnd[1],domainInfo.refSubDomain.globalIndexEnd[2]};

  vtkWriterFull.write(0,originFull,extendFull);
  std::cout << "Finished writing first step" << std::endl;
  //
  std::cout << "Initializing MultiLattice ...";
#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice,commDataHandler);
  ipcCommunication<T,Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilatticeCPU(lattice,commDataHandler);
  mpiCommunication<T,Lattice<T>> communication{commDataHandler};
#endif

  std::cout << "finished" << std::endl;
  //get random number in interval [a,b] to randomize the sampletime for averaging

  for(unsigned int timeStep = 1; timeStep <= simTime; ++timeStep)
  {

      for (unsigned dim=0;dim<Lattice<T>::d;++dim)
        position(dim) = position(dim) + movement(dim);

      std::cout << "lattice now at position " << position(0) << " " << position(1) << " "<< position(2) << std::endl;

#ifdef ENABLE_CUDA
      cudaDeviceSynchronize();	
      // collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
      collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
      HANDLE_ERROR(cudaPeekAtLastError());
      lattice.moveMeshGPU(movement, zeroVec, position, zeroVec,domainInfo.getLocalInfo());
#else
      collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
      // collideAndStreamMultilattice<NoDynamics<T,Lattice>>(lattice,commDataHandler,communication);
      lattice.moveMesh(movement, zeroVec, position, zeroVec,domainInfo.getLocalInfo());
#endif
      
#ifdef ENABLE_CUDA
      cudaDeviceSynchronize();
      lattice.copyDataToCPU();
      cudaDeviceSynchronize();
#endif
     vtkWriterFull.write(timeStep,originFull,extendFull);
     std::cout << "timestep " << timeStep << " finished" << std::endl;
  }
#ifdef ENABLE_CUDA
  cudaDeviceSynchronize();
#endif

  timer.stop();
}

int main()
{
#ifdef OLB_DEBUG
  std::cout << "this is a debug version" << std::endl;
#endif

#ifdef ENABLE_CUDA
    int rank = initIPC();
#else
    int rank = initMPI();
#endif

    const unsigned simSteps = 400;

    const size_t Resolution = 64; //multiple of 182;

      int iXLeftBorder = 0;
      int iXRightBorder = Resolution-1;
      int iYBottomBorder = 0;
      int iYTopBorder = Resolution-1;
      int iZFrontBorder = 0;
      int iZBackBorder = Resolution-1;

      unsigned ghostLayer[] = {2,2,2};
      // unsigned ghostLayer[] = {0,0,0};

      Index3D globalDomain{iXRightBorder+1,iYTopBorder+1,iZBackBorder+1,0};
      

      // std::this_thread::sleep_for(std::chrono::seconds(60));


      const SubDomainInformation<T,Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T,Lattice<T>>(globalDomain[0],globalDomain[1],globalDomain[2],0u,1u,ghostLayer);
      const DomainInformation<T,Lattice<T>> domainInfo = decomposeDomainAlongX(refSubDomain,rank,getNoRanks(),ghostLayer);
      std::cout << domainInfo.getLocalInfo();

      std::cout << "####" << std::endl;


      CommunicationDataHandler<T,Lattice<T>,MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(domainInfo);

      // std::cout << commDataHandler << std::endl; 
      // std::cout << "####################################" << std::endl;

      MultipleSteps(simSteps,Resolution,commDataHandler);

      // std::cout << "####################################" << std::endl;
      // std::cout << "####################################" << std::endl;
      // std::cout << "####################################" << std::endl;
      // std::cout << "calculation with res " << shipResolution  << " finished " << std::endl;

#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
    MPI_Finalize();
#else
    finalizeMPI();
#endif

	return 0;
}
