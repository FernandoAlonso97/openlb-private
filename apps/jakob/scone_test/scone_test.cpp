/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <fstream>
#include <iostream>
#include <cmath>

#include "contrib/domainDecomposition/readCSV.h"

int main()
{
////////////////////////// OLD TEST
    // std::string filename{"../scone/SCONE_D1H_1.dat"};
    // std::ifstream filestream (filename);

    // if (filestream.is_open())
    // {
    // for (unsigned i=0;i<5;++i)
    // {
      // std::string line;
      // std::getline(filestream,line);
    // }

    // while (filestream)
    // {
      // std::vector<std::string> retVal = olb::readNextLine(filestream,' ');
      // for (auto it=retVal.begin();it!=retVal.end();++it)
        // std::cout << std::stod(*it) << " ";

      // std::cout << std::endl;
    // }
    // }
    // else
      // throw (std::invalid_argument("Filename " + filename +" does not exist"));
///////////////////////// OLD TEST END
//
//
//


  unsigned itr=0;

  std::vector<double>  const conversionFactors{
    1, // time
    1/3.2808, //x -> ft to m
    1/3.2808, //y -> ft to m
    1/3.2808, //z -> ft to m
    M_PI/180.0, //phi -> deg to rad
    M_PI/180.0, //theta -> deg to rad
    M_PI/180.0, //psi -> deg to rad
    1/3.2808, //vx -> ft to m
    1/3.2808, //vy -> ft to m
    1/3.2808, //vz -> ft to m
    M_PI/180.0, //phi/s -> deg to rad
    M_PI/180.0, //theta/s -> deg to rad
    M_PI/180.0, //psi/s -> deg to rad
    1/3.2808, //ax -> ft to m
    1/3.2808, //ay -> ft to m
    1/3.2808, //az -> ft to m
    M_PI/180.0, //phi/s/s -> deg to rad
    M_PI/180.0, //theta/s/s -> deg to rad
    M_PI/180.0, //psi/s/s -> deg to rad
  };

  olb::ReadSconeData reader ("../scone/SCONE_D1H_1.dat",5);
  olb::ConvertStrToDouble<decltype(reader)> sconeValues (reader);
  while(true)
  {
    std::vector<double> retVal = olb::transformToSI (sconeValues(),conversionFactors);
    // std::vector<double> retVal = sconeValues();
    for (auto it=retVal.begin();it!=retVal.end();++it)
      std::cout << (*it) << " ";

    std::cout << std::endl;
    ++itr;
  }

	return 0;
}
