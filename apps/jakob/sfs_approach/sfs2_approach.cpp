/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"
#define OUTPUTIP "127.0.0.1"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
// #include "core/externalFieldALE.h"
// #include "core/externalFieldALE.hh"
#include "boundary/momentaOnBoundaries.h"
#include "boundary/momentaOnBoundaries.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "contrib/coupling/couplingCore.h"
#include "contrib/communication/NetworkInterface.h"
#include "contrib/communication/NetworkInterface.cpp"
#include "contrib/communication/NetworkDataStructures.h"
// #include "contrib/ALE/readExternalField.hh"
#include "core/externalFieldALE_cherrypick_basti.h"
#include "core/externalFieldALE_cherrypick_basti.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "contrib/domainDecomposition/readvti.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/mpiCommunication.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"


#include <iomanip>      // std::setprecision

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template<typename T, class Blocklattice,class SubDomInfo, int Resolution,class SubDomainInfo>
int addFuselageCells(Blocklattice &lattice, T &rotorPositionZ,const SubDomainInfo& refDomain, const SubDomainInfo& subDomInfo)
{
    size_t cells = Resolution*Resolution*Resolution;
    std::ifstream fileReader("../Fuselage_LBM/fus_" + std::to_string(Resolution) + ".dat");

    if(fileReader)
        std::cout << "File open" << std::endl;
    else
    {
        std::cout << "File not open" << std::endl;
        exit 99;
    }

    for(int i=0; i<2; ++i)
    {
        std::string line;
        std::getline(fileReader,line);
    }

    std::vector<bool> fluidMask(cells,false);
    size_t fuselageCells = 0;
    int midpoint[3]={refDomain.coreGridSize()[0]/2,refDomain.coreGridSize()[1]/2,refDomain.coreGridSize()[2]/2};
    int startpoint[3]={midpoint[0]-Resolution/2,midpoint[1]-Resolution/2,midpoint[2]-Resolution/4};

    for(unsigned int counter = 0; counter<cells; ++counter)
    {
        std::string line;
        std::getline(fileReader,line);
        std::string delim = " ";
        auto start = 0U;
        auto end = line.find(delim);
        std::vector<int> position(3);
        for(unsigned int pos = 0; pos < 3; ++pos)
        {
            position[pos] = std::stoi(line.substr(start,end - start));
            start = end + delim.length();
            end = line.find(delim, start);
        }
        fluidMask[counter] = static_cast<bool>(std::stoi(line.substr(start,end-start)));
        if(fluidMask[counter] == 0)
        {
            // rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),position[2]+1*Resolution/2);
            rotorPositionZ = std::min(static_cast<int>(rotorPositionZ),startpoint[2]+position[2]);
            // lattice.defineDynamics((numCells[0]-(position[0]+1*Resolution/4)-1), (numCells[0]-(position[0]+1*Resolution/4)-1)
                // , position[1]+1*Resolution/4, position[1]+1*Resolution/4, position[2]+1*Resolution/2, position[2]+1*Resolution/2
                // , &instances::getBounceBack<T,Lattice>());
            Index3D localIndex;
            if subDomInfo.isLocal((refDomain.coreGridSize()[0]-(startpoint[0]+position[0])-1),
                                  startpoint[1]+position[1],
                                  startpoint[2]+position[2],
                                  localIndex)
            {
              lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],
            		, &instances::getBounceBack<T,Lattice>());
              ++fuselageCells;
            
            }
        }

    }
    rotorPositionZ -= 0;
    std::cout << "Fuselage cells: " << fuselageCells << ", Rotorhubposition: " << rotorPositionZ << " on subdomain " << subDomInfo.localSubDomain << std::endl;
    return fuselageCells;
}

template<typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice& lattice, Dynamics<T,Lattice> &dynamics, const SubDomainInformation<T,Lattice<T>>& domainInfo,const SubDomainInformation<T,Lattice<T>>& refDomain )
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0]-1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1]-1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2]-1;

    T omega = dynamics.getOmega();

    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N;
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>> plane0N(omega,0.1);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,0, 1>> plane0P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 0,1,0>,PlaneFdBoundaryProcessor3D
        <T,Lattice, 0,1>> plane0P(omega,0.1);
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>> plane1N;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 1,-1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 1,-1>> plane1N(omega,0.1);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,1, 1>> plane1P;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 1,1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 1,1>> plane1P(omega,0.1);
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N;
	// static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P;
	//
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>> plane2N(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T,Lattice,2, 1>> plane2P(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 2,-1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 2,-1>> plane2N(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BasicDirichletBM<T,Lattice,VelocityBM, 2,1,0>,PlaneFdBoundaryProcessor3D
    	<T,Lattice, 2, 1>> plane2P(omega,0.1);

     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXLeftBorder,iY,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane0N);
          }
         }
    // lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0N);
    
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXRightBorder,iY,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane0P);
          }
         }
    // lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &plane0P);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYBottomBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane1N);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &plane1N);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYTopBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane1P);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBord er   , iZFrontBorder+1, iZBackBorder-1, &plane1P);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iY,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane2N);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &plane2N);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iY,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&plane2P);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &plane2P);

    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,-1>> edge0PN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1>> edge0NN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, 1>> edge0PP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, 1>> edge0NP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,-1>> edge1PN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1>> edge1NN;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, 1>> edge1PP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, 1>> edge1NP;
    // static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1>> edge2NN;
	//
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1, -1>> edge0PN(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1, -1>> edge0NN(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0, 1,  1>> edge0PP(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,  1>> edge0NP(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 0,-1, -1>> edge0NN(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 0,-1,  1>> edge0NP(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 0, 1, -1>> edge0PN(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 0, 1,  1>> edge0PP(omega,0.1);

    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1, -1>> edge1PN(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1, -1>> edge1NN(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1, 1,  1>> edge1PP(omega,0.1);
    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,  1>> edge1NP(omega,0.1);
	
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 1,-1, -1>> edge1NN(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 1,-1,  1>> edge1NP(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 1, 1, -1>> edge1PN(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 1, 1,  1>> edge1PP(omega,0.1);

    // static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, -1>> edge2NN(omega,0.1);
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 2,-1, -1>> edge2NN(omega,0.1);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1, 1>> edge2NP;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
    											OuterVelocityEdgeProcessor3D<T,Lattice, 2,-1, 1>> edge2NP(omega,0.1);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1,-1>> edge2PN;
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1,-1>> edge2PN(omega,0.1);
//    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2, 1, 1>> edge2PP;,
    static ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,FixedVelocityBM<T,Lattice,0>,
											    OuterVelocityEdgeProcessor3D<T,Lattice, 2, 1, 1>> edge2PP(omega,0.1);


     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYTopBorder,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0PN);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &edge0PN);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYBottomBorder,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0NN);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &edge0NN);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYTopBorder,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0PP);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &edge0PP);
    //
     for (unsigned iX = iXLeftBorder+1;iX<=iXRightBorder-1;++iX)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYBottomBorder,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge0NP);
          }
         }
    // lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &edge0NP);
    // 
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXLeftBorder,iY,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1PN);
          }
         }
    // lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PN);
    //
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXLeftBorder,iY,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1NN);
          }
         }
    // lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NN);
    //
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXRightBorder,iY,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1PP);
          }
         }
    // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &edge1PP);
    //
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXRightBorder,iY,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge1NP);
          }
         }
    // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edge1NP);

     for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2PN);
          }
         }
    // lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2PN);
    //
     for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2NN);
          }
         }
    // lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &edge2NN);
    //
     for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2PP);
          }
         }
    // lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2PP);
    //
     for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
        if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZ,localIndex))
        {
           lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&edge2NP);
          }
         }
    // lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &edge2NP);


    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1,-1>> cornerNNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1,-1>> cornerNPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1,-1, 1>> cornerNNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,-1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1,-1>> cornerPNN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1,-1>> cornerPPN;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1,-1, 1>> cornerPNP;
    static PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice, 1, 1, 1>> cornerPPP;

    Index3D localIndex;
    if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZFrontBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNNN);
    }
    // lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &cornerNNN);
    if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZFrontBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPNN);
    }
    // lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &cornerPNN);
    //
    if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZFrontBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNPN);
    }
    // lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &cornerNPN);
    //
    if (domainInfo.isLocal(iXLeftBorder,iYBottomBorder,iZBackBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNNP);
    }
    // lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &cornerNNP);
    //
    if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZFrontBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPPN);
    }
    // lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &cornerPPN);
    //
    if (domainInfo.isLocal(iXRightBorder,iYBottomBorder,iZBackBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPNP);
    }
    // lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &cornerPNP);
    //
    if (domainInfo.isLocal(iXLeftBorder,iYTopBorder,iZBackBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerNPP);
    }
    // lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &cornerNPP);
    //
    if (domainInfo.isLocal(iXRightBorder,iYTopBorder,iZBackBorder,localIndex))
    {
       lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&cornerPPP);
    }
    // lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &cornerPPP);

}

using ExtFieldType = PredefinedExternalField<T,Lattice>;

template<unsigned int RESOLUTION,typename RecvData,typename SendData,template<typename> class Memory>
void MultipleSteps(const double simTime, std::vector<T> latticePosition,
	UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> converter, std::vector<size_t> extent,ExtFieldType& externalFieldClass, 
	PredefinedExternalFluidMask<T,Lattice>& externalFluidMaskField, std::string externalFieldLocation, unsigned noTimesteps, T externalTimeStep,
  const SubDomainInformation<T,Lattice<T>> refSubDomain,
  CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler
  )
{
  using ReceiveDataGensim = RecvData;
  using SendDataGensim = SendData;
  using CouplingType = Coupling<T,HarmonicThrust<T,Lattice<T>>,LinearVelocity<T,Lattice<T>>>;


  std::cout << "timestep in lbm simulation " << std::setprecision(9) << converter.getPhysDeltaT() << std::setprecision(5) << std::endl;

  std::ofstream logFile("/2nd_scratch/ga69wij/shipApproach/shipApproach.out");

  T const rotorRadius = 4.92;
  T const rotorArea = rotorRadius*rotorRadius*M_PI;

  converter.print();
  
  auto domainInfo = commDataHandler.domainInfo;

  std::cout << "Calculation steps: " << converter.getLatticeTime(simTime) << std::endl;

  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.1);

  std::cout << "Create blockLattice.... ";


//  PredefinedTimeVariantExternalField<T,Lattice> externalFieldClass(externalFields, outsideVelocity,
//          2.0/converter.getPhysDeltaT(), extent[0], extent[1], extent[2], 1.0);
// ConstExternalField<T,Lattice> externalFieldClass(outsideVelocity);

  Index3D lattice_anchor_local = domainInfo.getLocalInfo().getLocalIdx(0,0,0);
  std::cout << "local Lattice anchor in lattice coords " << lattice_anchor_local[0] << " " << lattice_anchor_local[1] << " " << lattice_anchor_local[2] << " in subdomain " << lattice_anchor_local.subDomain << std::endl;
  Vec3<T> lattice_anchor_local_vec {lattice_anchor_local[0], lattice_anchor_local[1], lattice_anchor_local[2]};
  BlockLatticeALE3D<T, Lattice, ExtFieldType> lattice(domainInfo.getLocalInfo(),
          lattice_anchor_local_vec, &bulkDynamics, externalFieldClass);

  std::cout << "Finished!" << std::endl;

  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,domainInfo.getLocalInfo(),refSubDomain);

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::vector<T> rotorPosition = {static_cast<T>(refSubDomain[0]/2.),static_cast<T>(refSubDomain[1]/2.),static_cast<T>(refSubDomain[2])};
  size_t numCells = addFuselageCells<T,BlockLatticeALE3D<T, Lattice,ExtFieldType>,decltype(domainInfo.getLocalInfo()),RESOLUTION+1>
  	  	  	  	  	  (lattice,rotorPosition[2],refSubDomain,domainInfo.getLocalInfo());
  Index3D rotorPositionLocal;
  domainInfo.getLocalInfo().getLocalIdx(rotorPosition[0],rotorPosition[1],rotorPosition[2],rotorPositionLocal);

  Index3D localIndex;
  if (domInfo.getLocalInfo().isLocal(rotorPosition[0],rotorPositino[1],rotorPosition[2],localIndex))
  {
  Rotor<T> rotor(rotorRadius,rotorPosition,spacing);
  RotorCellData<T> rotorCellData(rotor,lattice, converter);
  CouplingType::CouplingDataType couplingData(rotorCellData);

  std::cout << "Number of rotorcells "<< rotorCellData._numberRotorCells << std::endl;

  for(unsigned int iCell = 0; iCell< rotorCellData._numberRotorCells; ++iCell)
      if(rotorCellData._cellPositionRadialCPU[iCell] < 0.223)
      {
          size_t position[3];
          util::getCellIndices3D(rotorCellData._cellIndexCPU[iCell],lattice.getNy(),lattice.getNz(),position);
          lattice.defineDynamics(position[0],position[1],position[2],&instances::getBounceBack<T,Lattice>());
      }

  std::cout << "z Position: " << rotorPosition[2] << std::endl;
  std::cout << "Finished!" << std::endl;

  std::cout << "Rotor Position: " << rotor.getCenter()[0] << "," << rotor.getCenter()[1] << "," << rotor.getCenter()[2] << std::endl;
  std::cout << "RotorlimitsX: " << rotor.getXLimits()[0] << " to " << rotor.getXLimits()[1] << std::endl;
  std::cout << "RotorlimitsY: " << rotor.getYLimits()[0] << " to " << rotor.getYLimits()[1] << std::endl;

  std::cout << "Number of theoretical rotor cells: " << rotor.getNumberRotorCells() << std::endl;

  std::cout << std::setprecision(6) << std::defaultfloat;

  std::cout << "Undim: " << converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea() << std::endl;
  }

  lattice.updateAnchorPoint(Vec3<T>{rotorPositionLocal[0],rotorPositionLocal[1],rotorPositionLocal[2]});
  std::cout << "Finished!" << std::endl;

  T u[3] = {0,0,0};
  T force[3] = {0.,0.,converter.getLatticeForce(std::pow(spacing,2)*20000./1.225)/76.046648};
  std::cout << "Init equilibrium.... ";
  for (int iX = 0; iX <= refSubDomain.coreGridSize()[0]; ++iX)
      for (int iY = 0; iY <= refSubDomain.coreGridSize()[1]; ++iY)
          for (int iZ = 0; iZ <= refSubDomain.coreGridSize()[2]; ++iZ)
          {
            Index3D localIdx;
            if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIdx))
            {  
              T vel[Lattice<T>::d] = { 0., 0., 0.};
              T rho[1];
              lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
          }

  std::cout << "Finished!" << std::endl;

  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;
  std::cout << "Copy GPU data to CPU.... ";
  lattice.saveInitialFluidMask();
  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;


  std::string name;
  std::string directory = "/2nd_scratch/ga69wij/AHS_paper/sfs2_approach_single_slimDomain_wallFloor_hoverend_noside/";
  name = "shipApproach_";
  name += std::to_string(RESOLUTION+1);
  name += "_" + std::to_string(sizeFactor) + "_" + std::to_string(domainInfo.getLocalInfo().localSubDomain); 
  std::string nameTrim = name + "_trim";

  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  BlockVTKwriterMulitlattice3D<T> vtkWriterTrim( nameTrim,domainInfo,false,true );
  vtkWriterTrim.addFunctor(densityFunctor);
  vtkWriterTrim.addFunctor(velocityFunctor);
  vtkWriterTrim.addFunctor(physVelocityFunctor);
  vtkWriterTrim.addFunctor(forceFunctor);
  vtkWriterTrim.addFunctor(fluidMaskFunctor);

  size_t originFull[3] = {domainInfo.refSubDomain.globalIndexStart[0],domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendFull[3] = {domainInfo.refSubDomain.globalIndexEnd[0],domainInfo.refSubDomain.globalIndexEnd[1],domainInfo.refSubDomain.globalIndexEnd[2]};

  vtkWriterTrim.write(0,originFull,extendFull);

  std::cout << "Initializing MultiLattice ...";
#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice,commDataHandler);
  ipcCommunication<T,Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilatticeCPU(lattice,commDataHandler);
  mpiCommunication<T,Lattice<T>> communication{commDataHandler};
#endif
  std::cout << "finished" << std::endl;

  if (domInfo.getLocalInfo().isLocal(rotorPosition[0],rotorPositino[1],rotorPosition[2],localIndex))
  {
    std::cout << "Opening networkport " << std::endl;
    NetworkInterfaceTCP<RecvData,SendData> networkCommunicatorGensim(8888,OUTPUTIP,8888,NETWORKBUFFERSIZE,true);
    SendDataGensim dataSendGensim;
  }
  //
  ReceiveDataGensim dataReceiveGensim;
  //
  static_assert(!std::is_same_v<ReceiveDataGensim,ThrustAtRotorsHarmonic<10>>,"No MPI implementation for these NetworkTypes");
  MPI_Datatype ThrustAtRotorsLocalMPI;
  int blocklengths[29] = {1,1,1,1,3,
                          3,3,3,3,
                          1,1,10,10,
                          1,10,10,
                          1,10,10,

                          1,1,10,10,
                          1,10,10,
                          1,10,10
                          } 
  MPI_Datatype types[29] = {MPI_INT,MPI_INT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,
                            MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,

                            MPI_BOOL,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,
                            MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,
                            MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,

                            MPI_BOOL,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,
                            MPI_FLOAT,MPI_FLOAT,MPI_FLOAT,
                            MPI_FLOAT,MPI_FLOAT,MPI_FLOAT
  }

  MPI_Aint base_address;
  MPI_Aint displacements[29];

  MPI_Get_address(&dataReceiveGensim,&base_address);
  MPI_Get_address(&dataReceiveGensim.timestep,&displacements[0]);
  MPI_Get_address(&dataReceiveGensim.simulationStatus,&displacements[1]);
  MPI_Get_address(&dataReceiveGensim.dt,&displacements[2]);
  MPI_Get_address(&dataReceiveGensim.dphi,&displacements[3]);
  MPI_Get_address(&dataReceiveGensim.AbsThrust,&displacements[4]);

  MPI_Get_address(&dataReceiveGensim.positions,&displacements[5]);
  MPI_Get_address(&dataReceiveGensim.attitudes,&displacements[6]);
  MPI_Get_address(&dataReceiveGensim.velocities,&displacements[7]);
  MPI_Get_address(&dataReceiveGensim.rotatorics,&displacements[8]);

  MPI_Get_address(&dataReceiveGensim.mainIsRunning,&displacements[9]);
  MPI_Get_address(&dataReceiveGensim.presMainMeanX,&displacements[10]);
  MPI_Get_address(&dataReceiveGensim.presMainSinX,&displacements[11]);
  MPI_Get_address(&dataReceiveGensim.presMainCosX,&displacements[12]);

  MPI_Get_address(&dataReceiveGensim.presMainMeanY,&displacements[13]);
  MPI_Get_address(&dataReceiveGensim.presMainSinY,&displacements[14]);
  MPI_Get_address(&dataReceiveGensim.presMainCosX,&displacements[15]);

  MPI_Get_address(&dataReceiveGensim.presMainMeanZ,&displacements[16]);
  MPI_Get_address(&dataReceiveGensim.presMainSinZ,&displacements[17]);
  MPI_Get_address(&dataReceiveGensim.presMainCosZ,&displacements[18]);

  MPI_Get_address(&dataReceiveGensim.tailIsRunning,&displacements[19]);
  MPI_Get_address(&dataReceiveGensim.presTailMeanX,&displacements[10]);
  MPI_Get_address(&dataReceiveGensim.presTailSinX,&displacements[21]);
  MPI_Get_address(&dataReceiveGensim.presTailCosX,&displacements[22]);

  MPI_Get_address(&dataReceiveGensim.presTailMeanY,&displacements[23]);
  MPI_Get_address(&dataReceiveGensim.presTailSinY,&displacements[24]);
  MPI_Get_address(&dataReceiveGensim.presTailCosX,&displacements[25]);

  MPI_Get_address(&dataReceiveGensim.presTailMeanZ,&displacements[26]);
  MPI_Get_address(&dataReceiveGensim.presTailSinZ,&displacements[27]);
  MPI_Get_address(&dataReceiveGensim.presTailCosZ,&displacements[28]);

  for (unsigned item=0;item<29;++item)
  {
    displacements[item]=MPI_Aint_diff(displacement[item],base_address);
  }

  MPI_Type_create_struct(29,blockLengths,displacements,types,&ThrustAtRotorsLocalMPI);
  MPI_Type_commit(&ThrustAtRotorsLocalMPI);

  util::Timer<T> timer(converter.getLatticeTime(simTime),lattice.getNx()*lattice.getNy()*lattice.getNz());
  timer.start();

  Vec3<T> position{0,0,0};
  Vec3<T> attitude{0,0,0};
  Vec3<T> velocity{0,0,0};
  Vec3<T> rotation{0,0,0};
  Vec3<T> attitudeDelta{0,0,0};

  // Vec3<T> positionALEInit{latticePosition[0]/spacing,latticePosition[1]/spacing,latticePosition[2]/spacing};
  Vec3<T> positionALEInit{-100.0/spacing,latticePosition[1]/spacing,latticePosition[2]/spacing}; // outside domain for trim
  // positionALEInit = {latticePosition[0]/spacing,latticePosition[1]/spacing,latticePosition[2]/spacing}; //set to position passed to multipleSteps function
  Vec3<T> positionALE{0,0,0};
  Vec3<T> velocityNondim{0,0,0};
  Vec3<T> rotationNondim{0,0,0};

  positionALE = positionALEInit;

  for  (unsigned i=0;i<10;++i)
  {
    dataReceiveGensim.presMainMeanX = 0;
    dataReceiveGensim.presMainSinX[i] = 0;
    dataReceiveGensim.presMainCosX[i] = 0;

    dataReceiveGensim.presMainMeanY = 0;
    dataReceiveGensim.presMainSinY[i] = 0;
    dataReceiveGensim.presMainCosY[i] = 0;

    dataReceiveGensim.presMainMeanZ = 0;
    dataReceiveGensim.presMainSinZ[i] = 0;
    dataReceiveGensim.presMainCosZ[i] = 0;
  }

  T undimFactorForce = converter.getLatticeForce(std::pow(spacing,2)/1.225)/rotor.getRotorArea();
  std::cout << "Undim: " << undimFactorForce << std::endl;
  std::cout << "RotorArea: "<< rotor.getRotorArea() << std::endl;
  dataReceiveGensim.presMainMeanZ = 2500*9.81;//*undimFactorForce;
  std::cout << "lattice force "<< dataReceiveGensim.presMainMeanZ << std::endl;
  

  velocityNondim(0) = 5/converter.getConversionFactorVelocity();
  velocityNondim(1) = 0.0/converter.getConversionFactorVelocity();
  velocityNondim(2) = 0;


  size_t preStep = 1;
  while(dataReceiveGensim.simulationStatus != 0)
  {
      if (domInfo.getLocalInfo().isLocal(rotorPosition[0],rotorPositino[1],rotorPosition[2],localIndex))
      {
        
      
      std::cout << "recieve with status " << dataReceiveGensim.simulationStatus << " with size " << sizeof(RecvData) << std::endl;
      networkCommunicatorGensim.recieve_data(dataReceiveGensim);
      std::cout << "recieved status " << dataReceiveGensim.simulationStatus << std::endl;
      dataReceiveGensim.positions[2] = 0;
      CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());
      }

      MPI_Bcast(&dataReceiveGensim,1,ThrustAtRotorsHarmonicMPI,domainInfo.getSubDomainNo(rotorPosition[0],rotorPosition[1],rotorPosition[2],MPI_COMM_WORLD));


      std::cout << "Thrust: " << dataReceiveGensim.presMainMeanX << "," << dataReceiveGensim.presMainMeanY << "," << dataReceiveGensim.presMainMeanZ << std::endl;
      logFile << "Thrust: " << dataReceiveGensim.presMainMeanX << "," << dataReceiveGensim.presMainMeanY << "," << dataReceiveGensim.presMainMeanZ << std::endl;

      std::cout << "Simulation status is: " << dataReceiveGensim.simulationStatus << std::endl;

      for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      {
        velocityNondim(iDim) = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
          rotationNondim(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
          // positionALE(iDim)       = positionALEInit(iDim) + dataReceiveGensim.positions[iDim]/spacing;
          positionALE(iDim)       = positionALEInit(iDim);
          attitude(iDim)          = dataReceiveGensim.attitudes[iDim]+attitudeDelta(iDim);

          std::cout << "Axis " << iDim << " : " << dataReceiveGensim.velocities[iDim] << ","
                  << dataReceiveGensim.rotatorics[iDim] << "," << dataReceiveGensim.positions[iDim] << ","
                  << dataReceiveGensim.attitudes[iDim] << std::endl;
          logFile << "Axis " << iDim << " : " << dataReceiveGensim.velocities[iDim] << ","
                  << dataReceiveGensim.rotatorics[iDim] << "," << dataReceiveGensim.positions[iDim] << ","
                  << dataReceiveGensim.attitudes[iDim] << std::endl;
      }

      unsigned int trimTime = converter.getLatticeTime(2);

      if(preStep == 1)
      {
          trimTime = converter.getLatticeTime(4);
      }

      // trimTime=1;
      std::cout << "beginning trim until " << trimTime << std::endl;

      for(unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
      {
#ifdef ENABLE_CUDA
          cudaDeviceSynchronize();	
          // collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
          collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
          HANDLE_ERROR(cudaPeekAtLastError());
          lattice.moveMeshGPU(velocityNondimt, rotationNondim,positionALE,attitude);
          lattice.updateFluidMaskGPU(velocityNondim,rotationNondim,positionALE,attitude,externalFluidMaskField);
#else
          collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
          // collideAndStreamMultilattice<NoDynamics<T,Lattice>>(lattice,commDataHandler,communication);
          lattice.moveMesh(movement, zeroVec, position, zeroVec);
          lattice.updateFluidMask(velocityNondim,rotationNondim,positionALE,attitude,externalFluidMaskField);
#endif
          
#ifdef ENABLE_CUDA
          cudaDeviceSynchronize();
          HANDLE_ERROR(cudaGetLastError());
#endif

          if (trimStep%100 == 0)
          std::cout << "timstep " << trimStep << std::endl;

          // lattice.copyDataToCPU();
          // vtkWriterTrim.write(trimStep);
      }

      lattice.copyDataToCPU();
      vtkWriterTrim.write(preStep,originFull,extendFull);

      std::cout << "Trimstep " << preStep << " finished" << std::endl;
      // logFile << "Trimstep " << preStep << " finished" << std::endl;

      if (domInfo.getLocalInfo().isLocal(rotorPosition[0],rotorPositino[1],rotorPosition[2],localIndex))
      {
      CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

      std::cout << "vi: " << dataSendGensim.velocityMainMean[0] << "," << dataSendGensim.velocityMainMean[1]
                                                          << "," << dataSendGensim.velocityMainMean[2] << std::endl;
      logFile << "vi: " << dataSendGensim.velocityMainMean[0] << "," << dataSendGensim.velocityMainMean[1]
                                                          << "," << dataSendGensim.velocityMainMean[2] << std::endl;

      networkCommunicatorGensim.send_data(dataSendGensim);

      std::cout << "Finished sending inflow" << std::endl;
      }
      ++preStep;

      if(dataReceiveGensim.simulationStatus == 0)
      {
          break;
      }

  }

  positionALEInit = {latticePosition[0]/spacing,latticePosition[1]/spacing,latticePosition[2]/spacing}; //set to position passed to multipleSteps function
  positionALE = positionALEInit;

  std::string nameSim = name + "_sim";
  BlockVTKwriterMultilattice3D<T> vtkWriterSim( nameSim, domainInfo,false,true );
  vtkWriterSim.addFunctor(densityFunctor);
  vtkWriterSim.addFunctor(velocityFunctor);
  vtkWriterSim.addFunctor(physVelocityFunctor);
  vtkWriterSim.addFunctor(forceFunctor);
  vtkWriterSim.addFunctor(fluidMaskFunctor);

  vtkWriterSim.write(0,originFull,extendFull);

  std::cout << "Starting time simulation" << std::endl;
  unsigned int print = 1;
  unsigned int externalFieldIndex = 1;
  T** externalFieldData=nullptr;
  std::vector<int> globalExtend(6UL);
  std::vector<float> spacingVec{};
  std::vector<std::string> filenames{};

  for(unsigned int timeStep = 1; timeStep < converter.getLatticeTime(simTime)-1; ++timeStep)
  {
        std::cout << "timestep " << timeStep << std::endl;

      if (domInfo.getLocalInfo().isLocal(rotorPosition[0],rotorPositino[1],rotorPosition[2],localIndex))
      {
        networkCommunicatorGensim.recieve_data(dataReceiveGensim);
        // dataReceiveGensim.positions[2] += 1999.42;

        CouplingType::writeReceiveData(dataReceiveGensim,lattice.getData(),rotorCellData,couplingData.getWriteData());
      }
      MPI_Bcast(&dataReceiveGensim,1,ThrustAtRotorsHarmonicMPI,domainInfo.getSubDomainNo(rotorPosition[0],rotorPosition[1],rotorPosition[2],MPI_COMM_WORLD));


        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        {
            velocityNondim(iDim)    = dataReceiveGensim.velocities[iDim]/converter.getConversionFactorVelocity();
            rotationNondim(iDim)    = dataReceiveGensim.rotatorics[iDim]*converter.getConversionFactorTime();
            // positionALE(iDim)       = positionALEInit(iDim) + dataReceiveGensim.positions[iDim]/spacing;
            attitude(iDim)          = dataReceiveGensim.attitudes[iDim]+attitudeDelta(iDim);
        }
        
        // if (timeStep <= converter.getLatticeTime(3.0))
        // {
        // velocityNondim(0) = 5/converter.getConversionFactorVelocity();
        // velocityNondim(1) = 0.0/converter.getConversionFactorVelocity();
        // velocityNondim(2) = 0;
        // }
        // if(timeStep > converter.getLatticeTime(3.0) and timeStep <= converter.getLatticeTime(4.0))
        // {

        // velocityNondim(0) = (5-(5/converter.getLatticeTime(1.0) * (timeStep-converter.getLatticeTime(3.0))))/converter.getConversionFactorVelocity();
        // velocityNondim(1) = 0.0/converter.getConversionFactorVelocity();
        // velocityNondim(2) = 0;

        // }
        // if (timeStep > converter.getLatticeTime(4.0))
        // {
          
        // velocityNondim(0) = 0.0/converter.getConversionFactorVelocity();
        // velocityNondim(1) = 0.0/converter.getConversionFactorVelocity();
        // velocityNondim(2) = 0;
        
        // }


        velocityNondim(1) = 0.0;
        velocityNondim(2) = 0.0;
        if (timeStep > converter.getLatticeTime(10.0)) // 10s flushing domain
        {
          positionALE(0) += velocityNondim(0)*converter.getConversionFactorVelocity()*converter.getPhysTime(1)/spacing; 
          positionALE(1) += velocityNondim(1)*converter.getConversionFactorVelocity()*converter.getPhysTime(1)/spacing; 
          positionALE(2) += velocityNondim(2)*converter.getConversionFactorVelocity()*converter.getPhysTime(1)/spacing; 
        }

        // if(true or timeStep%converter.getLatticeTime(0.05) == 0)
        // {
            // // std::cout << "position = " << dataReceiveGensim.positions[0] << "," << dataReceiveGensim.positions[1] << "," << dataReceiveGensim.positions[2] << std::endl;
            // // std::cout << "positionALE = " << positionALE(0) << "," << positionALE(1) << "," << positionALE(2) << std::endl;
           // // std::cout << "time = " << converter.getPhysTime(timeStep) << std::endl;

            // logFile << "position = " << dataReceiveGensim.positions[0] << "," << dataReceiveGensim.positions[1] << "," << dataReceiveGensim.positions[2] << std::endl;
            // logFile << "positionALE = " << positionALE(0) << "," << positionALE(1) << "," << positionALE(2) << std::endl;
           // logFile << "time = " << converter.getPhysTime(timeStep) << std::endl;

        // for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        // {
          // velocityNondim(iDim) = velocity(iDim)/converter.getConversionFactorVelocity();
            // rotationNondim(iDim)    = 0*converter.getConversionFactorTime();
            // positionALE(iDim)       = positionALEInit(iDim) + velocity(iDim)*converter.getPhysDeltaT()*timeStep/spacing;
            // attitude(iDim)          = 0;
        // }

        // if(timeStep%converter.getLatticeTime(0.2) == 0)
        // {
            std::cout << "positionALE = " << positionALE(0) << "," << positionALE(1) << "," << positionALE(2) << std::endl;
           std::cout << "time = " << converter.getPhysTime(timeStep) << std::endl;

            logFile << "positionALE = " << positionALE(0) << "," << positionALE(1) << "," << positionALE(2) << std::endl;
     	    logFile << "time = " << converter.getPhysTime(timeStep) << std::endl;
      		


        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

        if(timeStep%converter.getLatticeTime(0.5) == 0)
        {
            lattice.copyDataToCPU();
            vtkWriterSim.write(timeStep,originFull,extendFull);
            ++print;
        }

        if(timeStep%converter.getLatticeTime(externalTimeStep) == 0)
        {
          if (18774==(13680+18*externalFieldIndex))
            externalFieldIndex++;
          if (externalFieldIndex>630)
          externalFieldLocation = "/local_scratch/ga69wij/AHS_paper/sfs2_pressure_long_unscaled_flat_multilattice_polsky_flyin_res_546/vtkData/"; //1360
          if (externalFieldIndex>=870)
          externalFieldLocation = "/2nd_scratch/ga69wij/AHS_paper/sfs2_pressure_long_unscaled_flat_multilattice_polsky_flyin_res_546/vtkData/"; //1360
         

          filenames = std::vector<std::string>{
                      // externalFieldLocation+"sfs2_rank_0_velocity_0_iT00" +std::to_string(13680+18*externalFieldIndex) +".vti",
                      externalFieldLocation+"sfs2_rank_1_velocity_1_iT00" +std::to_string(13680+18*externalFieldIndex) +".vti",
                      externalFieldLocation+"sfs2_rank_2_velocity_2_iT00" +std::to_string(13680+18*externalFieldIndex) +".vti",
                      externalFieldLocation+"sfs2_rank_3_velocity_3_iT00" +std::to_string(13680+18*externalFieldIndex) +".vti",
                      };
           
          externalFieldData = readvti<T>(filenames,"physVelocity",globalExtend,spacingVec,converter.getLatticeVelocity(1.0));

          externalFieldClass.setNextExternalField(externalFieldData);
          externalFieldClass.dispatchFieldCopy();
          cudaDeviceSynchronize();

          
          for (unsigned field=0;field<Lattice<T>::d;++field)
            delete[] externalFieldData[field];

          externalFieldData=nullptr;
        
          ++externalFieldIndex;  
        }

        lattice.moveMeshGPU(velocityNondim,rotationNondim,positionALE,attitude);
	    	lattice.updateFluidMaskGPU(velocityNondim,rotationNondim,positionALE,attitude,externalFluidMaskField);
        HANDLE_ERROR(cudaGetLastError());


      if (domInfo.getLocalInfo().isLocal(rotorPosition[0],rotorPositino[1],rotorPosition[2],localIndex))
      {
        CouplingType::readSendData(dataSendGensim,lattice.getData(),rotorCellData,couplingData.getReadData());

        networkCommunicatorGensim.send_data(dataSendGensim);
      }

  }

  lattice.copyDataToCPU();
  vtkWriterSim.write(converter.getLatticeTime(simTime),originFull,extendFull);

  std::cout << "finished" << std::endl;

}

int main(int argc, char** argv)
{
#ifdef OLB_DEBUG
  std::cout << "this is a debug version" << std::endl;
#endif

#ifdef ENABLE_CUDA
    int rank = initIPC();
#else
    int rank = initMPI();
#endif
    const int Resolution = 95;
    T sizeFactor = 2.5;

    UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
    		Resolution
            ,0.3*1.0/std::sqrt(3)
            ,4.*4.92
            ,100.
            ,0.0000146072
            ,1.225
            ,0);

    converter.print();

    unsigned ghostLayer[] = {1,1,1};
    Index3D globalDomain{Resolution*sizeFactor,Resolution*sizeFactor,Resolution*sizeFactor,0};
    //std::cout << "argv 1 and 2 " << atoi(argv[1]) << " " << atoi(argv[2]) << std::endl;

    T const velocityConversion = converter.getConversionFactorVelocity();
    std::vector<int64_t> extent(3);

    using ReceiveDataGensim = ThrustAtRotorsHarmonic<10>;
    using SendDataGensim = VelocityAtRotorsLinear;

	// std::string const externalFieldLocation = "/2nd_scratch/ga69wij/sfs2_pressure_long_unscaled_flat_multilattice_res_546/vtkData/"; 54558
	std::string const externalFieldLocation = "/ssd_scratch/ga69wij/AHS_paper/sfs2_pressure_long_unscaled_flat_multilattice_polsky_flyin_res_546/vtkData/"; //1360

  // read fieldData
	std::vector<std::string> filenames;
  T** externalField;
  std::vector<int> globalExtend(6UL);
  std::vector<float> spacing{};
  unsigned noTimeSteps = 1;
  T externalTimeStep = 0.02;

  for (int i=0;i<1;++i)
  {
    filenames = std::vector<std::string>{
      // externalFieldLocation+"sfs2_rank_0_velocity_0_iT00" +std::to_string(13680+18*i) +".vti",
      externalFieldLocation+"sfs2_rank_1_velocity_1_iT00" +std::to_string(13680+18*i) +".vti",
      externalFieldLocation+"sfs2_rank_2_velocity_2_iT00" +std::to_string(13680+18*i) +".vti",
      externalFieldLocation+"sfs2_rank_3_velocity_3_iT00" +std::to_string(13680+18*i) +".vti",
    };

    externalField = readvti<T>(filenames,"physVelocity",globalExtend,spacing,converter.getLatticeVelocity(1.0));
  }


  std::vector<std::string> fluidMaskFiles
  {
    // externalFieldLocation + "sfs2_rank_0_fluidMask_0_iT0000000.vti",
    externalFieldLocation + "sfs2_rank_1_fluidMask_1_iT0000000.vti",
    externalFieldLocation + "sfs2_rank_2_fluidMask_2_iT0000000.vti",
    externalFieldLocation + "sfs2_rank_3_fluidMask_3_iT0000000.vti",
  };

  bool* fluidMaskExternal = readvti<bool>(fluidMaskFiles,"fluidmask",globalExtend,spacing,1.0)[0];

    T externalLatticeSpacing = 0.254003;
    T minimalShipBuildingBlock = 3; 
    T shipResolution = minimalShipBuildingBlock*182;
    // std::vector<T> latticePosition = {(globalExtend[1]-0.3*shipResolution*6.0/10.0-(17.5)*1/externalLatticeSpacing)*externalLatticeSpacing,(shipResolution/3-4)*externalLatticeSpacing,(165-36)*externalLatticeSpacing};//{900,409,100}; // small domain onr review
    std::vector<T> latticePosition = {(globalExtend[1]-1.25*(3*182))*externalLatticeSpacing+12.0,(globalExtend[3]/2)*externalLatticeSpacing,(165-40)*externalLatticeSpacing};//{900,409,100}; // full Ship

    Vec3<T> outsideVelocity{converter.getLatticeVelocity(-15.0),converter.getLatticeVelocity(0.),0.};
    PredefinedExternalField<T,Lattice> externalFieldClass(externalField, outsideVelocity,
			            globalExtend[1], globalExtend[3], globalExtend[5], converter.getConversionFactorLength()/externalLatticeSpacing);

    PredefinedExternalFluidMask<T,Lattice> externalFluidMaskField(fluidMaskExternal,globalExtend[1],globalExtend[3],globalExtend[5],converter.getConversionFactorLength()/externalLatticeSpacing);

    
      const SubDomainInformation<T,Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T,Lattice<T>>(globalDomain[0],globalDomain[1],globalDomain[2],0u,1u,ghostLayer);
      const DomainInformation<T,Lattice<T>> domainInfo = decomposeDomainAlongX(refSubDomain,rank,getNoRanks(),ghostLayer);
      // std::cout << domainInfo.getLocalInfo();
      CommunicationDataHandler<T,Lattice<T>,MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(domainInfo);


    const double simTime = noTimeSteps*externalTimeStep;

    std::vector<size_t> shipDomainExtend = {globalExtend[1],globalExtend[3],globalExtend[5]};
    std::cout << "Start of MulitpleSteps " <<std::endl;
	  MultipleSteps<Resolution,ReceiveDataGensim,SendDataGensim>(simTime,latticePosition,converter,shipDomainExtend,externalFieldClass,externalFluidMaskField,externalFieldLocation,noTimeSteps,externalTimeStep,refSubDomain,commDataHandler);
    std::cout << "success" << std::endl;

  // for (int i=0;i<noTimeSteps;++i)
  // {
    for (unsigned field=0;field<Lattice<T>::d;++field)
    {
      delete [] externalField[field];
    }
    // delete[] externalFields[i];
  // }

  delete[] fluidMaskExternal;
    
#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
    MPI_Finalize();
#else
    finalizeMPI();
#endif
	    
    return 0;
}
