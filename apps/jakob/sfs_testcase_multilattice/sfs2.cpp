/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define INPUTPORT 8888
#define OUTPUTPORT 8889
#define OUTPUTIP "192.168.0.250"
#define NETWORKBUFFERSIZE 50

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
// #include "boundary/boundaryCondition3D.h"
// #include "boundary/boundaryCondition3D.hh"
// #include "boundary/boundaryPostProcessors3D.h"
// #include "boundary/boundaryPostProcessors3D.hh"
// #include "boundary/momentaOnBoundaries.h"
// #include "boundary/momentaOnBoundaries.hh"
// #include "boundary/momentaOnBoundaries3D.h"
// #include "boundary/momentaOnBoundaries3D.hh"
// #include "dynamics/latticeDescriptors.h"
// #include "dynamics/smagorinskyBGKdynamics.h"
// #include "dynamics/smagorinskyBGKdynamics.hh"
// #include "dynamics/dynamics.h"
// #include "dynamics/dynamics.hh"
// #include "core/blockLattice3D.h"
// #include "core/blockLatticeALE3D.h"
// #include "core/singleton.h"
// #include "core/unitConverter.h"
// #include "io/blockVtkWriter3D.h"
// #include "dynamics/latticeDescriptors.hh"
// #include "core/blockLattice3D.hh"
// #include "core/blockLatticeALE3D.hh"
// #include "core/unitConverter.hh"
// #include "io/blockVtkWriter3D.hh"
// #include "functors/lattice/blockLatticeLocalF3D.h"
// #include "functors/lattice/blockLatticeLocalF3D.hh"
// #include "utilities/timer.h"
// #include "utilities/timer.hh"
#include "io/gpuIOFunctor.h"
#include <fstream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <thread>
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/mpiCommunication.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

T sumZ = 0;

static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,0,-1>> slipDynamics{};
static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,-1,0>> slipDynamicsYMinus{};
static PostProcessingDynamics<T,Lattice,SlipBoundaryProcessor3D<T,Lattice,0,1,0>> slipDynamicsYPlus{};

#include <fstream>
//#include <cuda_runtime.h>
//#include <device_launch_parameters>
#include <curand_kernel.h>
#include <ctime>

template<typename T,template <typename U> class Lattice>
class PowerLawBL{

public:
	PowerLawBL() = delete;

	PowerLawBL(SubDomainInformation<T,Lattice<T>> subDomainInfo,size_t iX0, size_t iX1, size_t iY0, size_t iY1, size_t iZ0, size_t iZ1, T turbulentInensity, T blHeight, T maxVel,int maxZ):
      subDomInfo_{subDomainInfo},
			startIndices{iX0,iY0,iZ0},
			endIndices{iX1,iY1,iZ1},
			size_((iX1-iX0)*(iY1-iY0)*(iZ1-iZ0)),
			turbulentIntensity_(turbulentInensity),
			blHeight_(blHeight),
			maxVel_(maxVel),
			cellCoordinate(maxZ)
	{}

	
    	__device__
    	void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
	{
	
		size_t localIndices[3];
		util::getCellIndices3D(threadIndex,subDomInfo_.localGridSize()[1],subDomInfo_.localGridSize()[2],localIndices);
    
    Index3D globalIndices = subDomInfo_.getGlobalIndex(Index3D{localIndices[0],localIndices[1],localIndices[2],subDomInfo_.localSubDomain});
	  
		if ((globalIndices[0]>=startIndices[0] && globalIndices[0]<endIndices[0]) && (globalIndices[1]>=startIndices[1] && globalIndices[1]<endIndices[1]) && (globalIndices[2]>=startIndices[2] && globalIndices[2]<endIndices[2]))
		{
            		T vel[Lattice<T>::d] = { 0., 0., 0.};
            		T rho=1.0;
           
		           	curandState state;
			          curand_init((unsigned long long)clock() + threadIndex,0,0,&state);

            		double nRandom1 =2*(curand_uniform_double(&state)-0.5);
            		double nRandom2 =2*(curand_uniform_double(&state)-0.5);
            		double nRandom3 =2*(curand_uniform_double(&state)-0.5);

            		T inverseZCoordinate = (cellCoordinate - static_cast<T>(globalIndices[2]));
            		T u_calc = -maxVel_;

            		if (inverseZCoordinate < blHeight_)
               			u_calc = -maxVel_*pow((inverseZCoordinate/blHeight_), 1./7.);

                // u_calc = inverseZCoordinate / cellCoordinate * -maxVel_;

                vel[0] = u_calc + turbulentIntensity_*nRandom1*maxVel_;
                vel[1] = 0.0 + turbulentIntensity_*nRandom2*maxVel_;
                vel[2] = 0.0 + turbulentIntensity_*nRandom3*maxVel_;
	
            		cellData[Lattice<T>::uIndex  ][threadIndex] =vel[0];
            		cellData[Lattice<T>::uIndex+1][threadIndex] =vel[1];
            		cellData[Lattice<T>::uIndex+2][threadIndex] =vel[2]; //0.05

        T uSqr = util::normSqr<T,Lattice<T>::d>(vel);
        for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
           cellData[iPop][threadIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, vel, uSqr);
        }

		}
		else
			return;
	}

	private:
  const SubDomainInformation<T,Lattice<T>> subDomInfo_;
	const size_t startIndices[Lattice<T>::d];
	const size_t endIndices[Lattice<T>::d];
	const size_t size_ = 0;

	const T turbulentIntensity_ = 0.0;
  const T blHeight_ = 0.0;
	const T maxVel_ = 0.0;

	const int cellCoordinate = 0;

};


template<typename T,template <typename U> class Lattice>
class ReadRhoUFunctor{

	public:
	ReadRhoUFunctor() = delete;

	OPENLB_HOST_DEVICE
	ReadRhoUFunctor(size_t iX0, size_t iX1, size_t iY0, size_t iY1, size_t iZ0, size_t iZ1, size_t ny, size_t nz):
			startIndices{iX0,iY0,iZ0},
			endIndices{iX1,iY1,iZ1},
			size_((iX1-iX0)*(iY1-iY0)*(iZ1-iZ0)),
			ny_(ny),
			nz_(nz)
	{
		for (int i =0;i<Lattice<T>::d+1;++i)
			HANDLE_ERROR(cudaMallocManaged(&data_[i],sizeof(T)*size_));

	}

    	OPENLB_HOST_DEVICE
    	void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
	{
	
		size_t indices[3];
		util::getCellIndices3D(threadIndex,ny_,nz_,indices);
	  
		if ((indices[0]>=startIndices[0] && indices[0]<endIndices[0]) && (indices[1]>=startIndices[1] && indices[1]<endIndices[1]) && (indices[2]>=startIndices[2] && indices[2]<endIndices[2]))
			{

		 		size_t localIndex = util::getCellIndex3D(indices[0]-startIndices[0],
									 indices[1]-startIndices[1],
									 indices[2]-startIndices[2],
									 endIndices[1]-startIndices[1],
									 endIndices[2]-startIndices[2]);
				
				cellData[Lattice<T>::rhoIndex][threadIndex] = data_[0][localIndex];		
				for (int i=0;i<Lattice<T>::d;++i)
					cellData[Lattice<T>::uIndex+i][threadIndex] = data_[1+i][localIndex];
			
			}
		else 
			return;

	}

	void readFromFile (std::string filename)
	{
		ifstream infile;
		infile.open (filename);
		std::string line;
  		 if (infile.is_open())
  		 {
    		 while ( getline (infile,line) )
    		 {
      			istringstream ss(line);
			int index=0;
			ss >> index;

			if (index<size_)
			for (int i=0;i<Lattice<T>::d+1;++i)
			    ss >> data_[i][index];
			
    		 }
    		 infile.close();
  		}
	}

	private:
	size_t startIndices[Lattice<T>::d];
	size_t endIndices[Lattice<T>::d];
	size_t ny_ = 0;
	size_t nz_ = 0;
	size_t size_ = 0;

	T* data_[Lattice<T>::d+1];

};


template<class Lattice,template <typename> class bullShitFuckopenLB>
void setBlockPosition(Lattice& lattice, T const deltaX)
{
	size_t nx = lattice.getNx();
	size_t ny = lattice.getNy();
	size_t nz = lattice.getNz();

	Vector<T,3> origin1(104./deltaX,50./deltaX,80./deltaX);
	Vector<T,3> extend1(40./deltaX,20./deltaX,5./deltaX);
	Vector<T,3> origin2(124./deltaX,50./deltaX,60./deltaX);
	Vector<T,3> extend2(20./deltaX,20./deltaX,25./deltaX);
	IndicatorCuboid3D<T> obstacle1(extend1,origin1);
	IndicatorCuboid3D<T> obstacle2(extend2,origin2);

	bool* __restrict__ fluidMask = lattice.getFluidMask();

	for(size_t iX = 1; iX < nx-1; ++iX)
		for(size_t iY = 1; iY < ny-1; ++iY)
			for(size_t iZ = 1; iZ < nz-1; ++iZ)
			{
							T location[3] = {iX, iY, iZ};
							bool isInside1 = false;
							bool isInside2 = false;
							obstacle1(&isInside1,location);
							obstacle2(&isInside2,location);

							bool isInside = isInside1 or isInside2;

							if(isInside)
							{
								// std::cout << fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] << std::endl;
								lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,bullShitFuckopenLB>());
								// fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] = false;
								// std::cout << fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] << std::endl;
								// std::cout << iX << "," << iY << "," << iZ << std::endl;
							}
							// else
								// lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,bullShitFuckopenLB>());
								// fluidMask[util::getCellIndex3D(iX,iY,iZ,ny,nz)] = true;
			}
	
	// lattice.copyFluidMaskToGPU();

}

template<typename T,typename S>
class IndicatorRightTriangleBlock3D { // TODO at the moment only valid for Z-Direction

  private:

    Vector<S,3> tip_;
    Vector<S,3> base1_; // 90deg angle has to be here!
    Vector<S,3> base2_;
    S           blockHeight_ = 0;
    double      deltaX_ = 0;


  public:
  IndicatorRightTriangleBlock3D() = delete;
  IndicatorRightTriangleBlock3D(Vector<S,3> tip,Vector<S,3> base1, Vector<S,3> base2, S blockHeight,double deltaX):
    tip_(tip),
    base1_(base1),
    base2_(base2),
    blockHeight_(blockHeight),
    deltaX_(deltaX)

  {
  }

  bool operator() (bool output[], const S input[]) 
  {
    int yMean {tip_[1]};
    int xGes {tip_[0]-base1_[0]};
    int xDist {tip_[0]-input[0]};
    int baseLength {base2_[1]-base1_[1]};

    if (input[0] < tip_[0] and input[0]> base1_[0] and input[0] > base2_[0]) 
    {
      if (abs(input[1] - (yMean)) < xDist/xGes * 0.5*baseLength) 
      {
        if (input[2] > tip_[2]-blockHeight_)
        {
          std::cout << "point inside tip " << input[0] << " " << input[1] << " " << input[2] << "\n";
          return true;
        }
      }
    }
    return false;
  }


};

// template<typename T, unsigned SIZE>
// std::ostream& operator << (std::ostream& os, const Vector<T,SIZE> vec)
// {
  // os << "[ ";
  // for (int i =0;i<SIZE;++i)
    // os << vec[i] << " ";
  // os << "] \n";

  // return os;

// }


template<class Lattice,template <typename> class whatever>
void setShipBoundaries (Lattice& lattice,const DomainInformation<T,whatever<T>>& domainInfo, Vector<T,3> shipTipPosition, int noCellsPerUnit,T deltaX)
{
	size_t nx = domainInfo.getLocalInfo().globalIndexEnd[0];
	size_t ny = domainInfo.getLocalInfo().globalIndexEnd[1];
	size_t nz = domainInfo.getLocalInfo().globalIndexEnd[2];

  Vector<T,3> shipBasePosition1 {shipTipPosition + Vector<T,3>{-54*noCellsPerUnit,-9*noCellsPerUnit,0}};
  Vector<T,3> shipBasePosition2 {shipTipPosition + Vector<T,3>{-54*noCellsPerUnit,9*noCellsPerUnit,0}};

  std::cout << "shipBasePosition2 " << shipBasePosition2 << std::endl;

  // IndicatorRightTriangleBlock3D<T,T>shipTip  {shipTipPosition,shipBasePosition1,shipBasePosition2,3*noCellsPerUnit,deltaX};

for(size_t iX = 1; iX < nx-1; ++iX)
	for(size_t iY = 1; iY < ny-1; ++iY)
		for(size_t iZ = 1; iZ < nz-1; ++iZ)
       {
        T yMean {shipTipPosition[1]};
        T xGes {shipTipPosition[0]-shipBasePosition1[0]};
        T xDist {shipTipPosition[0]-iX};
        T baseLength {shipBasePosition2[1]-shipBasePosition1[1]};

	//std::cout << iX << " " << iY << " " << iZ << std::endl;
        if (iX < shipTipPosition[0] and iX> shipBasePosition1[0]) 
        {
          if (abs(iY - (yMean)) < (double)xDist/(double)xGes * 0.5*baseLength) 
          {
            if (iZ > shipTipPosition[2]-6*noCellsPerUnit)
            {
              Index3D localIndex;
              if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIndex))
              {
                 lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,whatever>());
              }
              //std::cout << "point inside tip " << iX << " " << iY << " " << iZ << "\n";
							// lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,whatever>());
            }
          }
        }
      }


  Vector<T,3> mainHullExtend {92*noCellsPerUnit,18*noCellsPerUnit,14*noCellsPerUnit};
  Vector<T,3> mainHullOrigin {shipBasePosition2 + Vector<T,3>{-92*noCellsPerUnit,-18*noCellsPerUnit,-14*noCellsPerUnit}};

  std::cout << "hullOrigin " << mainHullOrigin << std::endl;
  std::cout << "hullExtend " << mainHullExtend << std::endl;

  IndicatorCuboid3D<T> mainHull {mainHullExtend,mainHullOrigin};

  Vector<T,3> mainChimneyExtend {8*noCellsPerUnit,4*noCellsPerUnit,8*noCellsPerUnit};
  Vector<T,3> mainChimneyOrigin {shipBasePosition1 + Vector<T,3>{(-92+22)*noCellsPerUnit,(+9-2)*noCellsPerUnit,(-14-8)*noCellsPerUnit}};

  std::cout << "chimneyOrigin " << mainChimneyOrigin << std::endl;
  std::cout << "chimneyExtend " << mainChimneyExtend << std::endl;

  IndicatorCuboid3D<T> chimney {mainChimneyExtend,mainChimneyOrigin};

  Vector<T,3> flightDeckExtend {128*noCellsPerUnit,18*noCellsPerUnit,6*noCellsPerUnit};
  Vector<T,3> flightDeckOrigin {shipBasePosition2+Vector<T,3>{-128*noCellsPerUnit,-18*noCellsPerUnit,-6*noCellsPerUnit}};

  IndicatorCuboid3D<T> flightDeck {flightDeckExtend,flightDeckOrigin};


	bool* __restrict__ fluidMask = lattice.getFluidMask();

  bool isInside [3];
	for(size_t iX = 1; iX < nx-1; ++iX)
		for(size_t iY = 1; iY < ny-1; ++iY)
			for(size_t iZ = 1; iZ < nz-1; ++iZ)
			{
							T location[3] = {iX, iY, iZ};

              if(mainHull(isInside,location) or flightDeck(isInside,location) or chimney(isInside,location))
							{
                Index3D localIndex;
                if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIndex))
                {
                   lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,whatever>());
                }
                // std::cout << "add boundary at " << iX << " " << iY << " " << iZ << "\n";
                // lattice.defineDynamics(iX,iX,iY,iY,iZ,iZ,&instances::getBounceBack<T,whatever>());
							}

      }


}



template<typename T, template <typename> class Lattice, class BlockLattice>
void defineBoundaries(BlockLattice &lattice, Dynamics<T,Lattice> &dynamics,const SubDomainInformation<T,Lattice<T>>& domainInfo, const SubDomainInformation<T,Lattice<T>>& refDomain )
{
    int iXLeftBorder =   refDomain.globalIndexStart[0];
    int iXRightBorder =  refDomain.globalIndexEnd[0]-1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder =    refDomain.globalIndexEnd[1]-1;
    int iZFrontBorder =  refDomain.globalIndexStart[2];
    int iZBackBorder =   refDomain.globalIndexEnd[2]-1;

    T omega = dynamics.getOmega();

    OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        createInterpBoundaryCondition3D<T,Lattice,
        ForcedLudwigSmagorinskyBGKdynamics>(lattice);

    // OnLatticeBoundaryCondition3D<T, Lattice>* boundaryCondition =
        // createInterpBoundaryCondition3D<T,Lattice,
        // ConstRhoBGKdynamics>(lattice);



    //boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );
    //
    //now done by powerlaw functor
    // boundaryCondition->addVelocityBoundary0P(iXRightBorder  ,iXRightBorder  ,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder+1,iZBackBorder-1, omega );

    // boundaryCondition->addImpedanceBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addImpedanceBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addImpedanceBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );
    // boundaryCondition->addVelocityBoundary1N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addVelocityBoundary1P(iXLeftBorder+1,iXRightBorder-1,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addPeriodicBoundary1N(iXLeftBorder+1,iXRightBorder,iYBottomBorder  ,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
    // boundaryCondition->addPeriodicBoundary1P(iXLeftBorder+1,iXRightBorder,iYTopBorder     ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );



    // boundaryCondition->addPeriodicBoundary1N(iXLeftBorder,iXRightBorder,iYBottomBorder  ,iYBottomBorder,iZFrontBorder,iZBackBorder, omega );
    // boundaryCondition->addPeriodicBoundary1P(iXLeftBorder,iXRightBorder,iYTopBorder     ,iYTopBorder   ,iZFrontBorder,iZBackBorder, omega );
    
    // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomB  order, iYBottomBorder , iZFrontBorder  , iZBackBorder , &slipDynamicsYMinus);
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iZ = iZFrontBorder;iZ<=iZBackBorder;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYBottomBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&slipDynamicsYMinus);
          }
         }

     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder , iZFrontBorder  , iZBackBorder , &slipDynamicsYPlus);
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iZ = iZFrontBorder;iZ<=iZBackBorder;++iZ)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iYTopBorder,iZ,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&slipDynamicsYPlus);
          }
         }



    // boundaryCondition->addVelocityBoundary2N(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZFrontBorder  ,iZFrontBorder , omega );
    // boundaryCondition->addVelocityBoundary2P(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder+1,iYTopBorder-1 ,iZBackBorder   ,iZBackBorder  , omega );

//      lattice.defineDynamics(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder , iXRightBorder  , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder  , iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder     , iYTopBorder   , iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
     // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
    // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &slipDynamics);
    //
     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
     
     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &slipDynamics);
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iY,iZFrontBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&slipDynamics);
          }
         }

     //lattice.defineDynamics(iXLeftBorder+1, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder  , iZFrontBorder , &instances::getBounceBack<T,Lattice>());
     
     // lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1 , iZBackBorder   , iZBackBorder  , &instances::getBounceBack<T,Lattice>());
     for (unsigned iX = iXLeftBorder;iX<=iXRightBorder;++iX)
         for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         {
          Index3D localIndex;
          if (domainInfo.isLocal(iX,iY,iZBackBorder,localIndex))
          {
             lattice.defineDynamics(localIndex[0],localIndex[1],localIndex[2],&instances::getBounceBack<T,Lattice>());
          }
         }

	  // boundaryCondition->addExternalImpedanceEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
	  // boundaryCondition->addExternalImpedanceEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
	  // boundaryCondition->addExternalImpedanceEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );
    // boundaryCondition->addExternalVelocityEdge0PN(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, omega );
    // boundaryCondition->addExternalVelocityEdge0NN(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, omega );
		// boundaryCondition->addExternalVelocityEdge0PP(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , omega );
		// boundaryCondition->addExternalVelocityEdge0NP(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , omega );

//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZFrontBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYTopBorder   ,iYTopBorder   ,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder+1,iXRightBorder-1,iYBottomBorder,iYBottomBorder,iZBackBorder ,iZBackBorder , &instances::getBounceBack<T,Lattice>());

      //boundaryCondition->addExternalImpedanceEdge1PN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
      //boundaryCondition->addExternalImpedanceEdge1NN(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

	 //  boundaryCondition->addExternalImpedanceEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
	 //  boundaryCondition->addExternalImpedanceEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

		// boundaryCondition->addExternalVelocityEdge1PP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , omega );
		// boundaryCondition->addExternalVelocityEdge1NP(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega );

//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder , iXLeftBorder , iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder , iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T,Lattice>());

      //boundaryCondition->addExternalImpedanceEdge2NN(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
      //boundaryCondition->addExternalImpedanceEdge2NP(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

	//   boundaryCondition->addExternalImpedanceEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
	//   boundaryCondition->addExternalImpedanceEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

		// boundaryCondition->addExternalVelocityEdge2PN(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, omega );
		// boundaryCondition->addExternalVelocityEdge2PP(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, omega );

//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYBottomBorder,iYBottomBorder,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iXRightBorder,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iXLeftBorder ,iYTopBorder   ,iYTopBorder   ,iZFrontBorder+1,iZBackBorder-1, &instances::getBounceBack<T,Lattice>());

      //boundaryCondition->addExternalImpedanceCornerNNN(iXLeftBorder ,iYBottomBorder,iZFrontBorder, omega);
      //boundaryCondition->addExternalImpedanceCornerNPN(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, omega);
      //boundaryCondition->addExternalImpedanceCornerNNP(iXLeftBorder ,iYBottomBorder,iZBackBorder , omega);
      //boundaryCondition->addExternalImpedanceCornerNPP(iXLeftBorder ,iYTopBorder   ,iZBackBorder , omega);

    // boundaryCondition->addExternalImpedanceCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
    // boundaryCondition->addExternalImpedanceCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
    // boundaryCondition->addExternalImpedanceCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
    // boundaryCondition->addExternalImpedanceCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);
    //
    // boundaryCondition->addExternalVelocityCornerPNN(iXRightBorder,iYBottomBorder,iZFrontBorder, omega);
    // boundaryCondition->addExternalVelocityCornerPPN(iXRightBorder,iYTopBorder   ,iZFrontBorder, omega);
    // boundaryCondition->addExternalVelocityCornerPNP(iXRightBorder,iYBottomBorder,iZBackBorder , omega);
    // boundaryCondition->addExternalVelocityCornerPPP(iXRightBorder,iYTopBorder   ,iZBackBorder , omega);
     // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder , iZFrontBorder  , iZBackBorder , &slipDynamics);
     // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder , iZFrontBorder  , iZBackBorder , &slipDynamics);

//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZFrontBorder, &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYBottomBorder,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXLeftBorder ,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
//      lattice.defineDynamics(iXRightBorder,iYTopBorder   ,iZBackBorder , &instances::getBounceBack<T,Lattice>());
    //boundaryCondition->addImpedanceBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
    
    // boundaryCondition->addPressureBoundary0N(iXLeftBorder  , iXLeftBorder   , iYBottomBorder+1, iYTopBorder-1 , iZFrontBorder+1, iZBackBorder-1, omega );
     for (unsigned iY = iYBottomBorder+1;iY<=iYTopBorder-1;++iY)
         for (unsigned iZ = iZFrontBorder+1;iZ<=iZBackBorder-1;++iZ)
         {
          Index3D localIndex;
          T vel[3] = {0.0,0.0,0.0};
          if (domainInfo.isLocal(iXLeftBorder,iY,iZ,localIndex))
          {
             boundaryCondition->addPressureBoundary0N(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2],omega);
             // lattice.defineRhoU(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
             // lattice.iniEquilibrium(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
          }
         }

}

template<template<typename> class Memory>
void MultipleSteps(const double simTime, int shipResolution,int minimalShipBuildingBlock,CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler) // numbers of cells per Shiplength
{

  int shipTipPositionDistanceToInflowBC = 1.5*shipResolution/2; 

  T viscosity = 0.000004426085106;
  //T viscosity = 0.0000184433;

  UnitConverterFromResolutionAndLatticeVelocity<T,Lattice> const converter(
          shipResolution
          ,0.3*1.0/std::sqrt(3)
          ,138.6857//0.5901446809// meters shiplength//4*4.92
          ,40
          ,0.0000146072
          ,1.225
          ,0);

  converter.print();

  // std::cout << commDataHandler << std::endl;

  auto domainInfo = commDataHandler.domainInfo;

  T conversionVelocity = converter.getConversionFactorVelocity();

  T deltaX = converter.getConversionFactorLength();

  T spacing = converter.getConversionFactorLength();

  T omega = converter.getLatticeRelaxationFrequency();

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega,0.025);
  GhostDynamics<T, Lattice> ghostLayerDynamics(1.0);
  // ConstRhoBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);

  std::cout << "Create blockLattice.... ";

  Vec3<T> velocity = {converter.getLatticeVelocity(-10.0),0.0,0.0};

  ConstExternalField<T,Lattice> externalFlow(velocity);

  Vec3<T> lattice_anchor {0., 0., 0.};
  // BlockLatticeALE3D<T, Lattice, ConstExternalField<T, Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1,
                                                                                              // lattice_anchor, &bulkDynamics, externalFlow);
  BlockLattice3D<T, Lattice> lattice(domainInfo.getLocalInfo(), &bulkDynamics);

  std::cout << "Finished!" << std::endl;


  std::cout << "Define boundaries.... ";
  defineBoundaries(lattice,bulkDynamics,domainInfo.getLocalInfo(),domainInfo.refSubDomain);
  // setBlockPosition<BlockLattice3D<T, Lattice, ConstExternalField<T, Lattice>>,Lattice>(lattice,deltaX);
  // setBlockPosition<BlockLattice3D<T, Lattice>,Lattice>(lattice,deltaX);
  std::cout << "total dimensions " << domainInfo.refSubDomain.globalIndexEnd[0] << " " << domainInfo.refSubDomain.globalIndexEnd[1]  << " " <<domainInfo.refSubDomain.globalIndexEnd[2] << std::endl;
  Vector<T,3> shipTipPosition {domainInfo.refSubDomain.globalIndexEnd[0]-shipTipPositionDistanceToInflowBC,0.5*domainInfo.refSubDomain.globalIndexEnd[1],domainInfo.refSubDomain.globalIndexEnd[2]};
  std::cout << "shipTipPosition " <<  shipTipPosition << std::endl;
  setShipBoundaries<BlockLattice3D<T,Lattice>,Lattice> (lattice,domainInfo,shipTipPosition, minimalShipBuildingBlock,deltaX);

  std::cout << "Finished!" << std::endl;
  std::cout << "Init GPU data.... ";
  lattice.initDataArrays();
  std::cout << "Finished!" << std::endl;

  std::cout << "Define ghostlayer to be bulkdynamics but fluidmask false ";
  for (unsigned iX=0;iX<domainInfo.getLocalInfo().localGridSize()[0];++iX)
  for (unsigned iY=0;iY<domainInfo.getLocalInfo().localGridSize()[1];++iY)
  for (unsigned iZ=0;iZ<domainInfo.getLocalInfo().localGridSize()[2];++iZ)
  {
    if (iX<domainInfo.getLocalInfo().ghostLayer[0] or iX>=domainInfo.getLocalInfo().localGridSize()[0]-domainInfo.getLocalInfo().ghostLayer[0])
      lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);

    if (iY<domainInfo.getLocalInfo().ghostLayer[1] or iY>=domainInfo.getLocalInfo().localGridSize()[1]-domainInfo.getLocalInfo().ghostLayer[1])
      lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
  
    if (iZ<domainInfo.getLocalInfo().ghostLayer[2] or iZ>=domainInfo.getLocalInfo().localGridSize()[2]-domainInfo.getLocalInfo().ghostLayer[2])
      lattice.defineDynamics(iX,iY,iZ,&ghostLayerDynamics);
  }

  std::cout << "Finished!" << std::endl;

  int shipHeight = 22*minimalShipBuildingBlock;
  int ablHeight = 0;//4*shipHeight;
  std::cout << "shipHeigt and ablHeigt vs domainHeight " << shipHeight << " " << ablHeight << " " << domainInfo.refSubDomain.globalIndexEnd[2] << std::endl;
  T targetVelocity =15;//30;//15.0;

  std::cout << "Init equilibrium.... ";
  for (int iX = domainInfo.refSubDomain.globalIndexStart[0]; iX <= domainInfo.refSubDomain.globalIndexEnd[0]; ++iX)
      for (int iY = domainInfo.refSubDomain.globalIndexStart[1]+1; iY <= domainInfo.refSubDomain.globalIndexEnd[1]-1; ++iY)
          for (int iZ = domainInfo.refSubDomain.globalIndexStart[2]; iZ <= domainInfo.refSubDomain.globalIndexEnd[2]; ++iZ)
          {
            T vel[Lattice<T>::d] = {0 , 0., 0.};
      // if(iX==iXRightBorder or ( (iY==0 or iY==iYTopBorder or iZ==0 or iZ==iZBackBorder) and iX!=0) ) 
      if(iX==domainInfo.refSubDomain.globalIndexEnd[0] and iY>domainInfo.refSubDomain.globalIndexStart[1] and iY<domainInfo.refSubDomain.globalIndexEnd[1] and iZ>domainInfo.refSubDomain.globalIndexStart[2] and iZ<domainInfo.refSubDomain.globalIndexEnd[2] )  
      { // flat velocity profile
        // vel[0] = converter.getLatticeVelocity(-20.);
        // 1/7 power law with turbulent intensity
            T obst_z = domainInfo.refSubDomain.globalIndexEnd[2];
            T obst_r = domainInfo.refSubDomain.globalIndexEnd[2];

            T maxVelocity = converter.getLatticeVelocity(targetVelocity);//0.111;


            double a = -1., b = 1.;

      
            double nRandom1 = rand()/(double)RAND_MAX*(b-a) + a;
            double nRandom2 = rand()/(double)RAND_MAX*(b-a) + a;
            double nRandom3 = rand()/(double)RAND_MAX*(b-a) + a;

            T inverseZCoordinate = (domainInfo.refSubDomain.globalIndexEnd[2] - iZ);
            T u_calc = -maxVelocity;
            if (inverseZCoordinate < ablHeight)
               u_calc = -maxVelocity*pow(static_cast<T>(inverseZCoordinate/ablHeight), 1./7.);

            vel[0] = -maxVelocity;// u_calc + 0.15*nRandom1*maxVelocity;// nRandom1*(0.00486); //(T) u_calc*(1 - cos((T) (twoPi*x))*sin((T) (twoPi*y))*exp((T) (-(x*x + y*y)))); //u_calc*(1 + 0.01*nRandom1);
            vel[1] =0.0; //+ 0.15*nRandom2*maxVelocity;//-(cos((T) (twoPi*y))*sin((T) (twoPi*y))*exp((T) (-(x*x + y*y)))); //0.01*u0*nRandom2;
            vel[2] =0.0;// + 0.15*maxVelocity*nRandom3; //0.05
      }

      Index3D localIndex;
      if (domainInfo.getLocalInfo().isLocal(iX,iY,iZ,localIndex))
      {
        lattice.defineRhoU(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
        lattice.iniEquilibrium(localIndex[0],localIndex[0],localIndex[1],localIndex[1],localIndex[2],localIndex[2], 1., vel);
      }
  }
  std::cout << "Finished!" << std::endl;


  std::cout << "Copy GPU data to CPU.... ";
  lattice.copyDataToGPU();
  //lattice.copyDataToCPU();
  std::cout << "Finished!" << std::endl;
  std::string name = "sfs2_rank_" + std::to_string(commDataHandler.domainInfo.localSubDomain) + "_";
  std::string directory = "/2nd_scratch/ga69wij/sfs2_pressure_long_unscaled_flat_multilattice_polsky_flyin_res_"+std::to_string(shipResolution)+"/";

  std::cout << "Running " << name << std::endl;

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriter( name, domainInfo,false );
  BlockLatticeDensity3D<T,Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice,0,converter);
  BlockLatticeForce3D<T,Lattice> forceFunctor(lattice);
  BlockLatticeCoordinates3D<T,Lattice> coordinateFunctor(lattice);
  BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir(directory);

  vtkWriter.addFunctor(densityFunctor);
  // vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  // vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(coordinateFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);


  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterFull( name+"fullDomain_", domainInfo );
  vtkWriterFull.addFunctor(densityFunctor);
  // vtkWriter.addFunctor(velocityFunctor);
  vtkWriterFull.addFunctor(physVelocityFunctor);
  // vtkWriter.addFunctor(forceFunctor);
  // vtkWriter.addFunctor(coordinateFunctor);
  vtkWriterFull.addFunctor(fluidMaskFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterFluidMask( name+"fluidMask_", domainInfo );
  vtkWriterFluidMask.addFunctor(fluidMaskFunctor);
  vtkWriterFluidMask.addFunctor(coordinateFunctor);

  BlockVTKwriterMultiLattice3D<T,Lattice<T>> vtkWriterVelocity( name+"velocity_", domainInfo );
  vtkWriterVelocity.addFunctor(physVelocityFunctor);


  util::Timer<T> timer(converter.getLatticeTime(simTime),domainInfo.refSubDomain.globalIndexEnd[0]*domainInfo.refSubDomain.globalIndexEnd[1]*domainInfo.refSubDomain.globalIndexEnd[2]);
  timer.start();

  std::cout << "Starting time simulation" << std::endl;
  std::cout << "Steps: " << converter.getLatticeTime(simTime) << std::endl;

  Vec3<T> zeroVec = {0.,0.,0.};
  Vec3<T> position = {0.,0.,0.};
  Vec3<T> movement = {converter.getLatticeVelocity(10),0.,0.};

  //PowerLawBL
  // in this case it is used to apply a uniform inflow boundary
  PowerLawBL<T,Lattice> powerLawBL(domainInfo.getLocalInfo(),domainInfo.refSubDomain.globalIndexEnd[0]-4,
      domainInfo.refSubDomain.globalIndexEnd[0]+2, domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexEnd[1]+1, domainInfo.refSubDomain.globalIndexStart[2], domainInfo.refSubDomain.globalIndexEnd[2]+1,0.0,ablHeight,converter.getLatticeVelocity(targetVelocity),domainInfo.refSubDomain.globalIndexEnd[2]);

  PowerLawBL<T,Lattice> powerLawBL_init(domainInfo.getLocalInfo(),domainInfo.refSubDomain.globalIndexStart[0],
      domainInfo.refSubDomain.globalIndexEnd[0]+2, domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexEnd[1]+1, domainInfo.refSubDomain.globalIndexStart[2], domainInfo.refSubDomain.globalIndexEnd[2]+1,0.0,ablHeight,converter.getLatticeVelocity(targetVelocity),domainInfo.refSubDomain.globalIndexEnd[2]);

  lattice.apply(powerLawBL_init);
  lattice.copyDataToCPU();

  // size_t origin[3] = {0,0,0};
  // size_t extend[3] = {domainInfo.refSubDomain.globalIndexEnd[0],domainInfo.refSubDomain.globalIndexEnd[1],domainInfo.refSubDomain.globalIndexEnd[2]};
  size_t origin[3] = {shipTipPosition[0]-2*shipResolution,domainInfo.refSubDomain.globalIndexEnd[1]/2 - 5,domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extend[3] = {shipTipPosition[0],domainInfo.refSubDomain.globalIndexEnd[1]/2 + 5,domainInfo.refSubDomain.globalIndexEnd[2]};

  size_t originFull[3] = {domainInfo.refSubDomain.globalIndexStart[0],domainInfo.refSubDomain.globalIndexStart[1],domainInfo.refSubDomain.globalIndexStart[2]};
  size_t extendFull[3] = {domainInfo.refSubDomain.globalIndexEnd[0],domainInfo.refSubDomain.globalIndexEnd[1],domainInfo.refSubDomain.globalIndexEnd[2]};

  size_t originWrite[3] = {shipTipPosition[0]-1.5*shipResolution,
                           domainInfo.refSubDomain.globalIndexEnd[1]/2 - shipResolution/3,
                           domainInfo.refSubDomain.globalIndexEnd[2]/2};
  size_t extendWrite[3] = {shipTipPosition[0]-1*shipResolution+0.3*shipResolution,
                           domainInfo.refSubDomain.globalIndexEnd[1]/2 + shipResolution/10,
                           domainInfo.refSubDomain.globalIndexEnd[2]};

  std::cout << "values for originWrite " << originWrite[0] << " " << originWrite[1] << " " << originWrite[2] << std::endl;
  std::cout << "values for extendWrite " << extendWrite[0] << " " << extendWrite[1] << " " << extendWrite[2] << std::endl;

  vtkWriter.write(0,origin,extend);
  vtkWriterFluidMask.write(0,originWrite,extendWrite);
  std::cout << "Finished writing first step" << std::endl;
  //
  // gpuFunctor::WriteRhoUFunctor<T,Lattice> rhoUWriter(domainInfo,static_cast<int>(shipTipPosition[0])-2*shipResolution, static_cast<int>(shipTipPosition[0]), domainInfo.refSubDomain.globalIndexStart[1], domainInfo.refSubDomain.globalIndexEnd[1], domainInfo.refSubDomain.globalIndexEnd[2]/2, domainInfo.refSubDomain.globalIndexEnd[2]);
  // std::cout << "Origins and extend " << static_cast<int>(shipTipPosition[0])-2*shipResolution << " " <<  static_cast<int>(shipTipPosition[0]) << " " << domainInfo.refSubDomain.globalIndexStart[1] <<" "<< domainInfo.refSubDomain.globalIndexEnd[1] <<" "<< domainInfo.refSubDomain.globalIndexEnd[2]/2<< " " << domainInfo.refSubDomain.globalIndexEnd[2] <<std::endl;

  // writeFluidMasktoFileMultiLattice (directory+"fluidMask/"+"fluidMask_Ship_rank_"+std::to_string(domainInfo.getLocalInfo().localSubDomain) + ".bin",lattice.getFluidMask(),domainInfo,static_cast<size_t>(shipTipPosition[0])-2*shipResolution, static_cast<size_t>(shipTipPosition[0]), static_cast<size_t>(domainInfo.refSubDomain.globalIndexStart[1]), domainInfo.refSubDomain.globalIndexEnd[1], domainInfo.refSubDomain.globalIndexEnd[2]/2,domainInfo.refSubDomain.globalIndexEnd[2]);

  // writeFluidMasktoFile (directory+"fluidMask/"+"fluidMask_Ship_ref_"+std::to_string(domainInfo.getLocalInfo().localSubDomain) + ".bin",lattice.getFluidMask(),domainInfo.getLocalInfo(),static_cast<size_t>(shipTipPosition[0])-2*shipResolution, static_cast<size_t>(shipTipPosition[0]), static_cast<size_t>(domainInfo.refSubDomain.globalIndexStart[1]), domainInfo.refSubDomain.globalIndexEnd[1], domainInfo.refSubDomain.globalIndexEnd[2]/2,domainInfo.refSubDomain.globalIndexEnd[2]);

  std::cout << "Initializing MultiLattice ...";
#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice,commDataHandler);
  ipcCommunication<T,Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilatticeCPU(lattice,commDataHandler);
  mpiCommunication<T,Lattice<T>> communication{commDataHandler};
#endif

  std::cout << "finished" << std::endl;
  //get random number in interval [a,b] to randomize the sampletime for averaging
  double a = -1., b = 1.;
  double nRandom = rand()/(double)RAND_MAX*(b-a) + a;

  for(unsigned int timeStep = 1; timeStep <= converter.getLatticeTime(simTime); ++timeStep)
  {
     // std::cout << "timestep " << timeStep << std::endl;

      lattice.apply(powerLawBL);

#ifdef ENABLE_CUDA
      cudaDeviceSynchronize();	
      collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
      HANDLE_ERROR(cudaPeekAtLastError());
#else
      collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>(lattice,commDataHandler,communication);
#endif
	  // lattice.moveMeshGPU(movement, zeroVec, position, zeroVec);
      
  // output for sampling
      if((timeStep%converter.getLatticeTime(1+0.5*nRandom) == 0 and timeStep > converter.getLatticeTime(5)))
      {
          std::cout << "Writing output for step: "<< timeStep<< std::endl;
#ifdef ENABLE_CUDA
          HANDLE_ERROR(cudaPeekAtLastError());
          lattice.copyDataToCPU();
          cudaDeviceSynchronize();
#endif
          vtkWriter.write(timeStep,origin,extend);

          nRandom = rand()/(double)RAND_MAX*(b-a) + a;
      }
      
    // if (timeStep%converter.getLatticeTime(15.0) == 0)
    // {
      // lattice.copyDataToCPU();
      // cudaDeviceSynchronize();
      // vtkWriterFull.write(timeStep,originFull,extendFull);
    // }

    if (timeStep >= converter.getLatticeTime(15.0) and timeStep <= converter.getLatticeTime(45.0))
    // // if (timeStep >= converter.getLatticeTime(1) and timeStep <= converter.getLatticeTime(80.0))
    {
      if(timeStep%converter.getLatticeTime(0.02) == 0)
      {
          // // lattice.apply(rhoUWriter);
          // // cudaDeviceSynchronize();
          // // rhoUWriter.writeToFile(directory+"shipOutput/"+"rhoU_Ship"+std::to_string(timeStep));
          // // rhoUWriter.writeToBinaryFile(directory+"shipOutput/"+"rhoU_Ship_"+std::to_string(timeStep)+ std::to_string(domainInfo.getLocalInfo().localSubDomain) + ".bin");
          // // rhoUWriter.writeToSingleBinaryFile(directory+"shipOutput/"+"rhoU_Ship"+std::to_string(timeStep)+".bin");

#ifdef ENABLE_CUDA
          HANDLE_ERROR(cudaPeekAtLastError());
          lattice.copyDataToCPU();
          cudaDeviceSynchronize();
#endif
          vtkWriterVelocity.write(timeStep,originWrite,extendWrite);

      }
      // // if(timeStep%converter.getLatticeTime(1.0)==0)
      // // {
          // // std::cout << "Writing output for step: "<< timeStep<< std::endl;
          // // HANDLE_ERROR(cudaPeekAtLastError());
          // // lattice.copyDataToCPU();
          // // cudaDeviceSynchronize();
          // // vtkWriter.write(timeStep);
      // // }
     } 
#ifdef ENABLE_CUDA
      cudaDeviceSynchronize();
#endif
     if(timeStep%converter.getLatticeTime(1.0)==0)
      vtkWriter.write(timeStep,origin,extend);

     std::cout << "timestep " << timeStep << " finished" << std::endl;
  }
  vtkWriter.write(converter.getLatticeTime(simTime),origin,extend);

#ifdef ENABLE_CUDA
  cudaDeviceSynchronize();
#endif

  timer.stop();

}

int main()
{
#ifdef OLB_DEBUG
  std::cout << "this is a debug version" << std::endl;
#endif

#ifdef ENABLE_CUDA
    int rank = initIPC();
#else
    int rank = initMPI();
#endif

    const double simTime = 50;

      const size_t shipResolution =91; //multiple of 182;

      int minimalShipBuildingBlock = shipResolution/91; // length for one unit block to build ship with in order for relative dimensions to fit

      // for ca 32gb of ram
      // int iXLeftBorder = 0;
      // int iXRightBorder = 6*shipResolution-1;
      // int iYBottomBorder = 0;
      // int iYTopBorder = 2*shipResolution-1;
      // int iZFrontBorder = 0;
      // int iZBackBorder = 2*shipResolution-1;

      // 16gb of ram

      int iXLeftBorder = 0;
      int iXRightBorder = 4*shipResolution-1;
      int iYBottomBorder = 0;
      int iYTopBorder = 1.5*shipResolution-1;
      int iZFrontBorder = 0;
      int iZBackBorder = 110*minimalShipBuildingBlock-1;

      unsigned ghostLayer[] = {1,1,1};
      // unsigned ghostLayer[] = {0,0,0};

      Index3D globalDomain{iXRightBorder+1,iYTopBorder+1,iZBackBorder+1,0};
      

      const SubDomainInformation<T,Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T,Lattice<T>>(globalDomain[0],globalDomain[1],globalDomain[2],0u,1u,ghostLayer);
      const DomainInformation<T,Lattice<T>> domainInfo = decomposeDomainAlongX(refSubDomain,rank,getNoRanks(),ghostLayer);
      std::cout << domainInfo.getLocalInfo();

      std::cout << "####" << std::endl;


      CommunicationDataHandler<T,Lattice<T>,MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(domainInfo);

      // std::cout << commDataHandler << std::endl; 
      // std::cout << "####################################" << std::endl;

      MultipleSteps(simTime,shipResolution,minimalShipBuildingBlock,commDataHandler);

      // std::cout << "####################################" << std::endl;
      // std::cout << "####################################" << std::endl;
      // std::cout << "####################################" << std::endl;
      // std::cout << "calculation with res " << shipResolution  << " finished " << std::endl;

#ifdef ENABLE_CUDA
    cudaDeviceSynchronize();
    MPI_Finalize();
#else
    finalizeMPI();
#endif

	return 0;
}
