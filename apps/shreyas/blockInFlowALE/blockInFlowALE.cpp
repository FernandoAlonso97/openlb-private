/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;
//using namespace olb::instances;

T smagoConstant = 0.095;

template <typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice &lattice, Dynamics<T, Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    auto* velocityMomenta0N = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
    auto* velocityPostProcessor0N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1> (iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor0N)>::type::PostProcessorType> velocityBCDynamics0N(omega, *velocityMomenta0N, smagoConstant);

    auto* velocityMomenta2N = new BasicDirichletBM<T, Lattice, VelocityBM, 2, -1, 0>;
    auto* velocityPostProcessor2N = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 2, -1> (iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 2, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor2N)>::type::PostProcessorType> velocityBCDynamics2N(omega, *velocityMomenta2N, smagoConstant);

    auto* velocityMomenta2P = new BasicDirichletBM<T, Lattice, VelocityBM, 2, 1, 0>;
    auto* velocityPostProcessor2P = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 2, 1> (iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 2, 1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor2P)>::type::PostProcessorType> velocityBCDynamics2P(omega, *velocityMomenta2P, smagoConstant);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    //lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0N);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &velocityBCDynamics0N);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1P);
    //lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &plane2N);
    //lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &plane2P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &velocityBCDynamics2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &velocityBCDynamics2P);


    auto* velocityMomentaEdge1NN = new FixedVelocityBM<T, Lattice, 0>;
    auto* velocityPostProcessorEdge1NN = new OuterVelocityEdgeProcessorGenerator3D<T, Lattice, 1, -1, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, FixedVelocityBM<T, Lattice, 0>, typename std::remove_reference<decltype(*velocityPostProcessorEdge1NN)>::type::PostProcessorType> velocityBCDynamicsEdge1NN(omega, *velocityMomentaEdge1NN);

    auto* velocityMomentaEdge1PN = new FixedVelocityBM<T, Lattice, 0>;
    auto* velocityPostProcessorEdge1PN = new OuterVelocityEdgeProcessorGenerator3D<T, Lattice, 1, 1, -1>(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder);
    static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, FixedVelocityBM<T, Lattice, 0>, typename std::remove_reference<decltype(*velocityPostProcessorEdge1PN)>::type::PostProcessorType> velocityBCDynamicsEdge1PN(omega, *velocityMomentaEdge1PN);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    //lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &velocityBCDynamicsEdge1PN);
    //lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &velocityBCDynamicsEdge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

}

template <typename T, template <typename> class Lattice, class Blocklattice>
IndicatorCuboid3D<T>& defineObstacle(Blocklattice &lattice, std::vector<int> limiter)
{

    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T blockXDim = iXRightBorder/4;
    T blockYDim = iYTopBorder/4;
    T blockZDim = iZBackBorder/8;

    Vector<T,3> center((T)iXRightBorder/2, (T)iYTopBorder/2, (T)iZBackBorder/2);

    static IndicatorCuboid3D<T> obstacleBlock(blockXDim, blockYDim, blockZDim, center);

    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T location[3] = {(T)iX, (T)iY, (T)iZ};
                bool isInside[1];
                obstacleBlock(isInside, location);
                if (isInside[0]) {
                    lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T,Lattice>());
                }
            }
    
    return obstacleBlock;
}

void MultipleSteps(const double simTime, unsigned int resolution)
{

    int iXLeftBorder = 0;
    int iXRightBorder = resolution;
    int iYBottomBorder = 0;
    int iYTopBorder = resolution;
    int iZFrontBorder = 0;
    int iZBackBorder = resolution;

    T domainWidth = 4.0;
    T physDensity = 1.225; // kg/m^3
    T physKinematicViscosity = 1.8e-5; // m^2/s

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), domainWidth, 1.0, physKinematicViscosity, physDensity, 0);

    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    Vec3<T> externalVelocity(converter.getLatticeVelocity(0.1), 0.0, 0.0);
    //Vec3<T> externalVelocity(converter.getLatticeVelocity(0.0), 0.0, 0.0);
    ConstExternalField<T, Lattice> constExternalField(externalVelocity);
    Vec3<T> latticeAnchor{(T)iXRightBorder/2.0, (T)iYTopBorder/2.0, (T)iZBackBorder/2.0};

    std::cout << "Create blockLattice.... ";
    BlockLatticeALE3D<T, Lattice, ConstExternalField<T, Lattice>> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, latticeAnchor, &bulkDynamics, constExternalField);

    std::cout << "Finished!" << std::endl;
    std::vector<int> limits = {iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZBackBorder};

    std::cout << "Define boundaries.... ";
    defineBoundaries(lattice, bulkDynamics, limits);
    //IndicatorCuboid3D<T>& obstacleBlock = defineObstacle<T, Lattice>(lattice, limits);

    // T testLoc[3] = {2.0, 2.0, 2.0};
    // bool testStuff[1];
    // obstacleBlock(testStuff, testLoc);
    // std::cout << "Test stuff: " << testStuff[0] << std::endl;

    std::cout << "Init GPU data.... ";
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... ";
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[3] = {converter.getLatticeVelocity(0.0), 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }

    lattice.copyDataToGPU();

    T desiredRotationRate = -(M_PI/4.0)/10.0; // rad/sec over 10 seconds, turn pi/4 radians.
    desiredRotationRate *= converter.getConversionFactorTime(); //convert to rad/timestep

    Vec3<T> position{0.0,0.0,0.0};
    Vec3<T> orientation{0.0,0.0,0.0};
    Vec3<T> translation{0.0,0.0,0.0};
    Vec3<T> rotation{0.0, 0.0, 0.0};
    //lattice.moveMeshGPU(translation, rotation, position, orientation);

    rotation = {0.0, desiredRotationRate, 0.0};
    translation = {0.01,0.01,-0.01};


    std::cout << "Finished!" << std::endl;

    unsigned int preStep = 1;
    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");
    BlockVTKwriter3D<T> writer("outputVTK");
    BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidMaskFunctor);
    writer.write(0);

    T finalLeftVelocity = converter.getLatticeVelocity(0.5);
    unsigned int rampUpSteps = trimTime/4;

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {
        // if ((trimStep % converter.getLatticeTime(0.1)) == 0 && trimStep <= rampUpSteps) {
        //     lattice.copyDataToCPU();
        //     T uVel = finalLeftVelocity*((T)trimStep/rampUpSteps);
        //     std::cout << "Trim step: " << trimStep << ", Defining uVel as " << uVel << std::endl;
        //     int iX = 0;
        //     for (int iY = 1; iY <= iYTopBorder-1; ++iY) {
        //         for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ) {
                    
        //             T vel[3] = {uVel, 0.0, 0.0};
        //             lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
        //             lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
        //         }
        //     }
        //     lattice.copyDataToGPU();
        // }
        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        lattice.moveMeshGPU(translation, rotation, position, orientation);
        orientation += rotation;
        position += translation;
        if (trimStep == converter.getLatticeTime(10.0)) {
            rotation = {0.0,0.0,0.0};
        }

        if ((trimStep % converter.getLatticeTime(0.5)) == 0)
        {
            std::cout << "Trim step:       " << trimStep << std::endl;
            std::cout << "Simulation time: " << converter.getPhysTime(trimStep) << std::endl;
            lattice.copyDataToCPU();
            writer.write(trimStep);
        }
    }
}

int main(int argc, char **argv)
{
    const double simTime = 50;
    unsigned int resolution = 32;
    MultipleSteps(simTime, resolution);
    return 0;
}
