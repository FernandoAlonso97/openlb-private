/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int resolution = 50;
const T desiredLatticeVel = 0.025;
const T desiredFreestream = 30.0;
const T boxLength = 1.0;
const T kinematicViscosity = 1.5E-5;

void MultipleSteps(const double simTime)
{


  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      50, desiredLatticeVel, boxLength, desiredFreestream, kinematicViscosity, 1.225, 0);

  converter.print();

  int iXLeftBorder = 0;
  int iXRightBorder = 8*resolution-1;
  int iYBottomBorder = 0;
  int iYTopBorder = 5*resolution-1;
  int iZFrontBorder = 0;
  int iZBackBorder = 5*resolution-1;

  T omega = converter.getLatticeRelaxationFrequency();
  // ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.025);
  EntropicDynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);
  // EntropicMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);
  // BGKdynamics<T,Lattice,BulkMomenta<T,Lattice>> bulkDynamics(omega);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

  std::cout << "just created teh lattice!" << std::endl;

  OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
      createInterpBoundaryCondition3D<T, Lattice,
                                      ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  static IniEquilibriumDynamics<T,Lattice> inletDynamics;
  lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZBackBorder, &inletDynamics);
 
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025*500);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, &impedanceOut);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &bottomFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &topFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &frontFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edgeXNNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edgeXPNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edgeXNPBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edgeXPPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edgePYNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &edgePYPBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &edgePNZBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &edgePPZBoundaryProcessor);

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &inletDynamics);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &inletDynamics);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &inletDynamics);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &inletDynamics);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &cornerPNNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &cornerPPNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &cornerPNPBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &cornerPPPBoundaryProcessor);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &inletDynamics);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &inletDynamics);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &inletDynamics);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &inletDynamics);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, 0.025*500);
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics2(omega, 0.025*10);
  lattice.defineDynamics(iXRightBorder-10, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, &spongeDynamics);
  lattice.defineDynamics(iXRightBorder-50, iXRightBorder-11, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, &spongeDynamics2);
  // lattice.defineDynamics(iXLeftBorder+1, iXLeftBorder+10, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, &spongeDynamics);

  //block
  lattice.defineDynamics(resolution*2, resolution*3, resolution*2, resolution*3, resolution*2, resolution*3, &instances::getBounceBack<T,Lattice>());

  lattice.initDataArrays();

  T reynoldsNumber = desiredFreestream*boxLength/kinematicViscosity;
  T latticeVel = converter.getLatticeVelocity(desiredFreestream);
  int simSteps = converter.getLatticeTime(simTime);
  printf("Reynolds number: %f, physical freestream: %f, lattice freestream: %f, desired lattice freestream: %f\n", reynoldsNumber, desiredFreestream, latticeVel, desiredLatticeVel);
  
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
        T vel[3] = {latticeVel, 0, 0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }
  lattice.copyDataToGPU();
  std::cout << "finished iniequil" << std::endl;

  BlockVTKwriter3D<T> vtkWriter("windTunnel");
  BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
  BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);
  BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);

  singleton::directories().setOutputDir("/data/ae-jral/sashok6/boxInWindTunnel_SRTEntropic/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(simSteps, lattice.getNx() * lattice.getNy() * lattice.getNz());
  timer.start();

  std::cout << "starting timestep loop" << std::endl;
  for (unsigned int iSteps = 1; iSteps <= simSteps; ++iSteps)
  {
   
    // lattice.collideAndStreamGPU<EntropicMRTdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    lattice.collideAndStreamGPU<EntropicDynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    // lattice.collideAndStreamGPU<BGKdynamics<T,Lattice,BulkMomenta<T,Lattice>>>();
    if (iSteps % 10 == 0) {
      timer.print(iSteps, 2);
      // lattice.copyDataToCPU();
      // vtkWriter.write(iSteps);
    }

    if (iSteps % converter.getLatticeTime(simTime/10.0) == 0) {
      lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
}

int main()
{
  const double simTime = 1.6;
  MultipleSteps(simTime);
  return 0;
}
