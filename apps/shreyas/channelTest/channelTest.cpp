/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime)
{
  int gridFactor = 2;

  int iXLeftBorder = 0;
  int iXRightBorder = 127*gridFactor;
  int iYBottomBorder = 0;
  int iYTopBorder = 63*gridFactor;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);
  BlockLattice2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);

  OnLatticeBoundaryCondition2D<T, Lattice> *boundaryCondition =
      createInterpBoundaryCondition2D<T, Lattice,
                                      BGKdynamics>(lattice);

  // OnLatticeBoundaryCondition2D<T, Lattice> *boundaryCondition = 
  //        createLocalBoundaryCondition2D<T, Lattice, BGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addVelocityBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYTopBorder, omega);
  //boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder, iYBottomBorder, omega);
  //boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder, iYTopBorder, omega);
  
  boundaryCondition->addPressureBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder, iYTopBorder, omega);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor2D<T, Lattice, 1, -1>> bottomBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, &bottomBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor2D<T, Lattice, 1, 1>> topBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, &topBoundaryProcessor);

  //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor2D<T, Lattice, -1, -1>> cornerNNBoundaryProcessor;
  //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor2D<T, Lattice, -1, 1>> cornerNPBoundaryProcessor;
  //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor2D<T, Lattice, 1, -1>> cornerPNBoundaryProcessor;
  //static PostProcessingDynamics<T, Lattice, PeriodicBoundaryCornerProcessor2D<T, Lattice, 1, 1>> cornerPPBoundaryProcessor;
  //lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, &cornerNNBoundaryProcessor);
  //lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, &cornerNPBoundaryProcessor);
  //lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, &cornerPNBoundaryProcessor);
  //lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, &cornerPPBoundaryProcessor);

//   static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor2D<T, Lattice>> bottomBoundaryProcessor;
//   lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, &bottomBoundaryProcessor);

//   static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor2D<T, Lattice>> topBoundaryProcessor;
//   lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, &topBoundaryProcessor);

//   boundaryCondition->addSlipBoundaryOverride(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, 0, -1);
//   boundaryCondition->addSlipBoundaryOverride(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, 0, 1);

  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      T vel[2] = {0, 0};
      lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
      lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

//   Dynamics<T, Lattice> *testDynamics = lattice.getDynamics(10, 1);
//   std::cout << testDynamics->isPostProcDynamics() << std::endl;
//   std::cout << boundaryProcessor.isPostProcDynamics() << std::endl;
//   Dynamics<T, Lattice> *testDynamics2 = &boundaryProcessor;
//   std::cout << testDynamics2->isPostProcDynamics() << std::endl;

  BlockVTKwriter2D<T> vtkWriter("poiseuille2DForced");
  BlockLatticeDensity2D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T, Lattice> physVelocityFunctor(lattice, converter);
  BlockLatticeForce2D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure2D<T, Lattice> physPressureFunctor(lattice, converter);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy());
  timer.start();

  int rampUpSteps = converter.getLatticeTime(simTime) / 2;
  T leftFinalVelocity = 0.05;

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    T currentLeftVelocity = leftFinalVelocity * iSteps / (double)rampUpSteps;
    currentLeftVelocity = currentLeftVelocity > leftFinalVelocity ? leftFinalVelocity : currentLeftVelocity;

    for (int iX = 0; iX <= 0; ++iX) {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {
        T vel[2] = {currentLeftVelocity, 0};
        lattice.defineRhoU(iX, iX, iY, iY, 1.0, vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, 1.0, vel);
      }
    }

    lattice.collideAndStream<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(0.5) == 0)
    {
      //lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
  timer.printSummary();
}

int main()
{
  const double simTime = 2;
  MultipleSteps(simTime);
  std::cout << "Program over!" << std::endl;
  return 0;
}
