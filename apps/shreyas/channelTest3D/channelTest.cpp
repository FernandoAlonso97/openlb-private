/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime)
{
  int iXLeftBorder = 0;
  int iXRightBorder = 127;
  int iYBottomBorder = 0;
  int iYTopBorder = 63;
  int iZFrontBorder = 0;
  int iZBackBorder = 11;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

  OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
      createInterpBoundaryCondition3D<T, Lattice,
                                      BGKdynamics>(lattice);

  // OnLatticeBoundaryCondition2D<T, Lattice> *boundaryCondition = 
  //        createLocalBoundaryCondition2D<T, Lattice, BGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addVelocityBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYTopBorder, iZBackBorder, iZFrontBorder, omega);
  //boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder, iYBottomBorder, omega);
  //boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder, iYTopBorder, omega);
  
  boundaryCondition->addPressureBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZBackBorder, iZFrontBorder, omega);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, -1>> bottomFaceBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZBackBorder, &bottomFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 1, 1>> topBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder, iZBackBorder, &topBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, -1>> frontBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &frontBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryProcessor3D<T, Lattice, 2, 1>> backBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYTopBorder, iZBackBorder, iZBackBorder, &backBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, PeriodicBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edgeXNNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edgeXPNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edgeXNPBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edgeXPPBoundaryProcessor);

//   static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor2D<T, Lattice>> bottomBoundaryProcessor;
//   lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, &bottomBoundaryProcessor);

//   static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor2D<T, Lattice>> topBoundaryProcessor;
//   lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, &topBoundaryProcessor);

//   boundaryCondition->addSlipBoundaryOverride(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, 0, -1);
//   boundaryCondition->addSlipBoundaryOverride(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, 0, 1);

  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
        T vel[3] = {0, 0, 0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }

//   Dynamics<T, Lattice> *testDynamics = lattice.getDynamics(10, 1);
//   std::cout << testDynamics->isPostProcDynamics() << std::endl;
//   std::cout << boundaryProcessor.isPostProcDynamics() << std::endl;
//   Dynamics<T, Lattice> *testDynamics2 = &boundaryProcessor;
//   std::cout << testDynamics2->isPostProcDynamics() << std::endl;

  BlockVTKwriter3D<T> vtkWriter("channelFlow3D");
  BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
  BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy());
  timer.start();

  int rampUpSteps = converter.getLatticeTime(simTime) / 2;
  T leftFinalVelocity = 0.05;

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    T currentLeftVelocity = leftFinalVelocity * iSteps / (double)rampUpSteps;
    currentLeftVelocity = currentLeftVelocity > leftFinalVelocity ? leftFinalVelocity : currentLeftVelocity;

    for (int iX = 0; iX <= 3; ++iX) {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {
        for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
          T vel[3] = {currentLeftVelocity, 0, 0};
          lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
          lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
        }
      }
    }

    lattice.collideAndStream<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(0.1) == 0)
    {
      //lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
}

int main()
{
  const double simTime = 10;
  MultipleSteps(simTime);
  return 0;
}
