/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"

#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinementUtil3D.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int fineResolution = 200;
const int nLevels = 3;
const int coarseResolution = fineResolution/pow(2.0, nLevels-1);
const T desiredLatticeVel = 0.025;
const T desiredFreestream = 34.03; //m/s
const T boxLength = 0.7172;
const T kinematicViscosity = 1.5E-5;
const T physDensity = 1.225;
const T frontalArea = 0.0154508; //m^2
const T smagoConst = 0.01;

//momentum exchange stuff

std::vector<T> xForce;
std::vector<T> yForce;
std::vector<T> zForce;
std::shared_ptr<memory_space::CudaDeviceHeap<T>> xForceGPU = nullptr;
std::shared_ptr<memory_space::CudaDeviceHeap<T>> yForceGPU = nullptr;
std::shared_ptr<memory_space::CudaDeviceHeap<T>> zForceGPU = nullptr;
std::shared_ptr<memory_space::CudaUnified<T*>> forceArrayGPU = nullptr;

size_t * momentumExchangeCells = nullptr;
T ** momentumExchangePostProcData = nullptr;
size_t momentumExchangeLength;

template <typename T, template <typename> class Lattice>
OPENLB_HOST_DEVICE
void momentumExchange(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData, 
                                        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outputForceArray, size_t index, size_t position, size_t ny, size_t nz) {
  
  T cellForce[3] = {0.0,0.0,0.0};

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    if (postProcData[GradWallModelProcessor3D<T,Lattice>::idxDirs() + iPop][position] > -0.1) {
      size_t neighborIndex = util::getCellIndex3D(index, Lattice<T>::c(Lattice<T>::opposite(iPop), 0), Lattice<T>::c(Lattice<T>::opposite(iPop), 1), Lattice<T>::c(Lattice<T>::opposite(iPop), 2), ny, nz);
      T popDiff = cellArray[iPop][index] + cellArray[Lattice<T>::opposite(iPop)][neighborIndex];
      for (int iD = 0; iD < 3; iD++)
        cellForce[iD] += popDiff*Lattice<T>::c(Lattice<T>::opposite(iPop), iD);
    }
  }

  for (int iD = 0; iD < 3; iD++) {
    outputForceArray[iD][position] = cellForce[iD];
  }

  

}

template <typename T, template <typename> class Lattice>
__global__ void momentumExchangeKernel(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData, 
                                        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outputForceArray, size_t * const OPENLB_RESTRICT cellIDs, size_t length, size_t ny, size_t nz) {

  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  momentumExchange<T,Lattice>(cellArray, postProcData, outputForceArray, cellIDs[threadIndex], threadIndex, ny, nz);
}



template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2];
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  static IniEquilibriumDynamics<T, Lattice> inletDynamics;

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025 * 500);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXLeftBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &impedanceOut);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder + 1, iXRightBorder - 1, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZFrontBorder, iXRightBorder - 1, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZBackBorder, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder, iXRightBorder - 1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder, iXRightBorder - 1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZBackBorder, iXRightBorder - 1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZBackBorder, iXRightBorder - 1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZBackBorder, iXLeftBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder, iXLeftBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZBackBorder, iXRightBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder, iXRightBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, iXRightBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder + 1, iXLeftBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder + 1, iXLeftBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder + 1, iXRightBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndex;
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, 0.025 * 500);
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics2(omega, 0.025 * 10);

  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 10, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 50, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 11, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics2);
}

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      coarseResolution, desiredLatticeVel, boxLength, desiredFreestream, kinematicViscosity, physDensity, 0);

  UnitConverter<T,Lattice> const fineConverter(RefinedGrid3D<T,Lattice>::getConverterForRefinementLevel(converter, 1));
  UnitConverter<T,Lattice> const doubleFineConverter(RefinedGrid3D<T,Lattice>::getConverterForRefinementLevel(converter, 2));

  converter.print();

  const int iXLeftBorder = 0;
  const int iXRightBorder = 8*coarseResolution-1;
  const int iYBottomBorder = 0;
  const int iYTopBorder = 3*coarseResolution-1;
  const int iZFrontBorder = 0;
  const int iZBackBorder = 3*coarseResolution-1;

  const int fineX = coarseResolution*2.25;
  const int fineY = coarseResolution*1.5;
  const int fineZ = coarseResolution*1.25;
  const int fineLeftOffset = coarseResolution*1.75;
  const int fineBottomOffset = coarseResolution*0.75;
  const int fineFrontOffset = coarseResolution*0.875;

  const int iXRightBorderFine = refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2);
  const int iYTopBorderFine = refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2);
  const int iZBackBorderFine = refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2);

  const int doubleFineX = coarseResolution*2*1.75;
  const int doubleFineY = coarseResolution*2*1;
  const int doubleFineZ = coarseResolution*2*0.6;
  const int doubleFineLeftOffset = coarseResolution*2*0.25;
  const int doubleFineBottomOffset = coarseResolution*2*0.25;
  const int doubleFineFrontOffset = coarseResolution*2*0.35;

  const int iXRightBorderDoubleFine = refinementutil::getRefinedRegionXFineLength3D(doubleFineX, doubleFineY, doubleFineZ, 2);
  const int iYTopBorderDoubleFine = refinementutil::getRefinedRegionXFineLength3D(doubleFineX, doubleFineY, doubleFineZ, 2);
  const int iZBackBorderDoubleFine = refinementutil::getRefinedRegionXFineLength3D(doubleFineX, doubleFineY, doubleFineZ, 2);

  T omega = converter.getLatticeRelaxationFrequency();
  // EntropicMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);
  // EntropicMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> childBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1));
  // EntropicMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> doubleChildBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 2));

  // EntropicDynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega);
  // EntropicDynamics<T, Lattice, BulkMomenta<T,Lattice>> childBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1));
  // EntropicDynamics<T, Lattice, BulkMomenta<T,Lattice>> doubleChildBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 2));

  // ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  // ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> childBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1), smagoConst);
  // ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> doubleChildBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 2), smagoConst);

  EntropicSmagorinskyMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  EntropicSmagorinskyMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> childBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1), smagoConst);
  EntropicSmagorinskyMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> doubleChildBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 2), smagoConst);

  RefinedGrid3D<T,Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
  lattice.addChild(&childBulkDynamics, fineX, fineY, fineZ, fineLeftOffset, fineBottomOffset, fineFrontOffset);
  lattice.children[0].addChild(&doubleChildBulkDynamics, doubleFineX, doubleFineY, doubleFineZ, doubleFineLeftOffset, doubleFineBottomOffset, doubleFineFrontOffset);

  unsigned ghostLayer[3] = {4,0,0};
  lattice.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

  std::cout << "Set up refinement..." << std::endl;
  lattice.setupRefinementGPU();
  lattice.setupMultilatticeGPU();

  std::cout << "Define boundaries.... "  << std::endl;
  // lattice.applyMasks();
  // defineBoundaries(*(lattice.getLatticePointer()), bulkDynamics, lattice._localSubDomain, lattice._refSubDomain);

  const stlT robinOffsetX = 0.25*(T)fineResolution;
  const stlT robinOffsetY = 0.5*(T)fineResolution;
  const stlT robinOffsetZ = 0.25*(T)fineResolution;

  // static PostProcessingDynamics<T,Lattice,GradBoundaryProcessor3D<T,Lattice>> gradBoundary;
  static PostProcessingDynamics<T,Lattice,GradWallModelProcessor3D<T,Lattice>> gradBoundary;
  
  STLreader<stlT> stlReader("robin_mod7_np_manscale_halfres.stl", doubleFineConverter.getConversionFactorLength()*0.05, 1.0);
  // STLreader<stlT> stlReader("robin_mod7_np_manscale_halfres.stl", doubleFineConverter.getConversionFactorLength()*0.15, 1.0);
  stlReader.print();

  std::set<size_t> outsideNeighborsSet;
  std::vector<std::vector<int>> outsideNeighborsReplacementPops;
  std::vector<std::vector<T>> outsideNeighborsReplacementPopDistances;
  std::vector<bool> outsideNeighborsBisections;

  std::map<size_t, std::array<T,1+3+1+3+3+Lattice<T>::q>> outsideNeighborsData;

  Index3D localIndex;

  std::set<size_t> insidePoints;

  Timer<T> stlTimer(1, 1);
  stlTimer.start();
  for (int iX = 0; iX <= iXRightBorderDoubleFine; iX++) {
    std::cout <<" outer loop iX " << iX << std::endl;
    for (int iY = 0; iY <= iYTopBorderDoubleFine; iY++)
      for (int iZ = 0; iZ <= iZBackBorderDoubleFine; iZ++) {
        stlT iXPhys = doubleFineConverter.getPhysLength((stlT)iX-robinOffsetX);
        stlT iYPhys = doubleFineConverter.getPhysLength((stlT)iY-robinOffsetY);
        stlT iZPhys = doubleFineConverter.getPhysLength((stlT)iZ-robinOffsetZ);
        stlT location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];
        stlReader(isInside, location);
        if (isInside[0]) {
          size_t index = util::getCellIndex3D(iX, iY, iZ, iYTopBorderDoubleFine+1, iZBackBorderDoubleFine+1);
          insidePoints.insert(index);
          if (lattice.children[0].children[0]._localSubDomain.isLocal(iX, iY, iZ, localIndex))
            lattice.children[0].children[0].getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());


          bool allNeighborsInside = true;
          std::vector<int> neighborsNotInside;
          for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
            stlT iXNeighborPhys = doubleFineConverter.getPhysLength((stlT)iX-robinOffsetX+(stlT)Lattice<T>::c(iPop, 0));
            stlT iYNeighborPhys = doubleFineConverter.getPhysLength((stlT)iY-robinOffsetY+(stlT)Lattice<T>::c(iPop, 1));
            stlT iZNeighborPhys = doubleFineConverter.getPhysLength((stlT)iZ-robinOffsetZ+(stlT)Lattice<T>::c(iPop, 2));
            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
            bool neighborInside[1];
            stlReader(neighborInside, neighborLocation);
            allNeighborsInside = allNeighborsInside && neighborInside[0];
            if (!neighborInside[0])
              neighborsNotInside.push_back(iPop);
          }

          if (!allNeighborsInside) {

            for (int outsideNeighbor : neighborsNotInside) {
              size_t outsideNeighborCellIndex = util::getCellIndex3D(iX + Lattice<T>::c(outsideNeighbor, 0), iY + Lattice<T>::c(outsideNeighbor, 1), iZ + Lattice<T>::c(outsideNeighbor, 2), iYTopBorderDoubleFine+1, iZBackBorderDoubleFine+1);
              auto result = outsideNeighborsSet.insert(outsideNeighborCellIndex);
              if (result.second) {
                
                olb::Vector<stlT,3> neighborPoint(doubleFineConverter.getPhysLength((stlT)iX-robinOffsetX+(stlT)Lattice<T>::c(outsideNeighbor, 0)),
                                              doubleFineConverter.getPhysLength((stlT)iY-robinOffsetY+(stlT)Lattice<T>::c(outsideNeighbor, 1)),
                                              doubleFineConverter.getPhysLength((stlT)iZ-robinOffsetZ+(stlT)Lattice<T>::c(outsideNeighbor, 2)));
                olb::Vector<stlT,3U> normal = stlReader.evalSurfaceNormalEfficient<Lattice>(neighborPoint, doubleFineConverter.getPhysLength(1.0));
                // olb::Vector<stlT,3U> normal = stlReader.evalSurfaceNormal(neighborPoint);
                // return;

              //   // std::cout << normal << std::endl;
              //   // std::cout << normal[0] << ", " << normal[1] << ", " << normal[2] << std::endl;

                bool useBisection = false;
                stlT distance = 0.5;
                bool success = stlReader.distance(distance, neighborPoint, normal*-1.0);
                distance = (distance/doubleFineConverter.getConversionFactorLength());
                if (!success || distance < 0.0 || distance > 1.5) {
                  // std::cout << "Major problem calculating distance on pt " << indices[0] << " " << indices[1] << " " << indices[2] << ": distance is " << distance << "; using bisection method \n";
                  // distance = (distance > 1.0 && distance < 1.2) ? 0.95 : 0.5;
                  useBisection  = true;
                }

                if (useBisection) {
                  stlT lowerLimit[3] = {neighborPoint[0], neighborPoint[1], neighborPoint[2]};
                  stlT upperLimit[3] = {neighborPoint[0] - 2.0*normal[0], neighborPoint[1] - 2.0*normal[1], neighborPoint[2] - 2.0*normal[2],};
                  bool averageInside[1];
                  const int nBisections = 12;
                  for (int nB = 0; nB < nBisections; nB++) {
                    stlT averagePt[3] = {(lowerLimit[0]+upperLimit[0])/2.0, (lowerLimit[1]+upperLimit[1])/2.0, (lowerLimit[2]+upperLimit[2])/2.0};
                    stlReader(averageInside, averagePt);
                    if (averageInside[0]) {
                      upperLimit[0] = averagePt[0];
                      upperLimit[1] = averagePt[1];
                      upperLimit[2] = averagePt[2];
                    }
                    else {
                      lowerLimit[0] = averagePt[0];
                      lowerLimit[1] = averagePt[1];
                      lowerLimit[2] = averagePt[2];
                    }
                  }
                  stlT distVector[3] = {lowerLimit[0]-neighborPoint[0], lowerLimit[1]-neighborPoint[1], lowerLimit[2]-neighborPoint[2]};
                  distance = sqrt(distVector[0]*distVector[0] + distVector[1]*distVector[1] + distVector[2]*distVector[2]);
                  distance = (distance/doubleFineConverter.getConversionFactorLength());
                  // std::cout << "Distance by bisection on pt " << indices[0] << " " << indices[1] << " " << indices[2] << " is " << distance << "\n";
                }


                outsideNeighborsData[outsideNeighborCellIndex][0] = doubleFineConverter.getLatticeRelaxationFrequency();
                outsideNeighborsData[outsideNeighborCellIndex][1] = normal[0];
                outsideNeighborsData[outsideNeighborCellIndex][2] = normal[1];
                outsideNeighborsData[outsideNeighborCellIndex][3] = normal[2];
                outsideNeighborsData[outsideNeighborCellIndex][4] = distance;


              } 
            }
          }
        }
      }
  }
  stlTimer.stop();
  stlTimer.printSummary();

  std::ofstream surfaceNormals("surfaceNormals.csv");
  surfaceNormals << "iX, iY, iZ, nX, nY, nZ" << std::endl;
  for (const auto& currentData : outsideNeighborsData) {
    size_t cellIndex = currentData.first;
    size_t indices[3];
    util::getCellIndices3D(cellIndex, iYTopBorderDoubleFine+1, iZBackBorderDoubleFine+1, indices);

    T nX = currentData.second.at(1);
    T nY = currentData.second.at(2);
    T nZ = currentData.second.at(3);

    surfaceNormals << indices[0] << ", " << indices[1] << ", " << indices[2] << ", " << nX << ", " << nY << ", " << nZ << std::endl;
  }

  surfaceNormals.close();

  std::cout << "Done!" << std::endl;
  return;
  
}

int main()
{
  const double simTime = 0.2;
  MultipleSteps(simTime);
  finalizeIPC();
  return 0;
}
