/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"

#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinementUtil3D.h"

#include "contrib/GPUSTLProcessing/octreeGPU.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int resolution = 700;
const T shipLength = 150.0;
const T smagoConst = 0.03;

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2];
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  static IniEquilibriumDynamics<T, Lattice> inletDynamics;

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025 * 500);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXLeftBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &impedanceOut);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder + 1, iXRightBorder - 1, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZFrontBorder, iXRightBorder - 1, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZBackBorder, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder, iXRightBorder - 1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder, iXRightBorder - 1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZBackBorder, iXRightBorder - 1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZBackBorder, iXRightBorder - 1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZBackBorder, iXLeftBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder, iXLeftBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZBackBorder, iXRightBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder, iXRightBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, iXRightBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder + 1, iXLeftBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder + 1, iXLeftBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder + 1, iXRightBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndex;
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);

}

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, 0.05, shipLength, 20.5, 1.4E-5, 1.225, 0); //approximately correct numbers

  T omega = converter.getLatticeRelaxationFrequency(); 

  const int iXLeftBorder = 0;
  const int iXRightBorder = 1.5*resolution-1;
  const int iYBottomBorder = 0;
  const int iYTopBorder = 0.6*resolution-1;
  const int iZFrontBorder = 0;
  const int iZBackBorder = 0.4*resolution-1;

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  
  RefinedGrid3D<T,Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
  
  unsigned ghostLayer[3] = {0,0,0};
  lattice.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

  std::cout << "Set up refinement..." << std::endl;
  lattice.setupRefinementGPU();
  lattice.setupMultilatticeGPU();

  std::cout << "Define boundaries.... "  << std::endl;
  lattice.applyMasks();
  defineBoundaries(*(lattice.getLatticePointer()), bulkDynamics, lattice._localSubDomain, lattice._refSubDomain);
  static PostProcessingDynamics<T,Lattice,GradBoundaryProcessor3D<T,Lattice>> gradBoundary;

  lattice.getLatticePointer()->dynamicsContainer.definePlaceholderDynamics(&gradBoundary);
  size_t gradAllocationSpace = 8000000;
  lattice.getLatticePointer()->getDataHandler(&gradBoundary)->setExtraAllocationSpace(gradAllocationSpace);

  std::cout << "Initial STL read..." << std::endl;

  const stlT shipOffsetX = (100.0)/converter.getConversionFactorLength();
  const stlT shipOffsetY = 0.3*(T)resolution;
  const stlT shipOffsetZ = (15.0)/converter.getConversionFactorLength();
  
  STLreader<stlT> stlReader("NATOGD_fullscale.stl", 0.25, 1.0/converter.getConversionFactorLength());
  stlReader.print();

  Octree<stlT> * parentOctree = stlReader.getTree();
  // parentOctree->printIDs();
  size_t origin[3] = {2,2,2};
  size_t extend[3] = {iXRightBorder-2, iYTopBorder-2, iZBackBorder-2};
  OctreeGPU<T,Lattice> gpuOctree(parentOctree, lattice._localSubDomain, lattice._refSubDomain, origin, extend);


  // for (int iX = 0; iX <= iXRightBorder; iX++)
  //   for (int iY = 0; iY <= iYTopBorder; iY++)
  //     for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
  //       stlT iXPhys = (iX - shipOffsetX);
  //       stlT iYPhys = (iY - shipOffsetY);
  //       stlT iZPhys = (iZ - shipOffsetZ);
  //       stlT location[3] = {iXPhys, iYPhys, iZPhys};
  //       bool inside[1];
  //       stlReader(inside, location);
  //       if (inside[0]) {
  //         Index3D localIndex;
  //         if (lattice._localSubDomain.isLocal(iX, iY, iZ, localIndex)) {
  //           // lattice.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T,Lattice>());
  //         }
  //       }

  //     }

  lattice.initDataArrays();
  // T vel[3] = {0.0,0.0,0.0};
  // lattice.iniEquilibrium(1.0, vel);
  // lattice.copyLatticesToGPU();

  bool * fluidMaskPtr = lattice.getLatticePointer()->cellData->gpuGetFluidMask();
  
  Vec3<T> offset((T)shipOffsetX, (T)shipOffsetY, (T)shipOffsetZ);
  Vec3<stlT> highPrecisionOffset(shipOffsetX, shipOffsetY, shipOffsetZ);
  std::cout << "Starting STL GPU check" << std::endl;
  gpuOctree.stlInside(offset, fluidMaskPtr);
  std::cout << "Done STL GPU check" << std::endl;
  lattice.copyLatticesToCPU();

  RefinedGridVTKManager3D<T,Lattice> writer("gpuSTLProcessing", lattice);
  writer.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeIndex3D<T,Lattice>>();
  // writer.write(0);

  for (int iX = 0; iX <= iXRightBorder; iX++)
    for (int iY = 0; iY <= iYTopBorder; iY++)
      for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
        Index3D localIndex;
        if (lattice._localSubDomain.isLocal(iX, iY, iZ, localIndex)) {
          lattice.getLatticePointer()->cellData->setFluidMaskForCell(true, localIndex[0], localIndex[1], localIndex[2]);
        }
      }
  lattice.copyLatticesToGPU();


  size_t * deviceGradIndices = lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().getDynamicsCellIDsGPU(&gradBoundary);
  T** devicePostProcData = lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().getPostProcDataGPU(&gradBoundary);
  std::cout << "Starting STL GPU boundary check" << std::endl;
  size_t numBoundaryCells = gpuOctree.stlBoundary(offset, deviceGradIndices, devicePostProcData, fluidMaskPtr);
  std::vector<size_t> cpuBoundaryCells(numBoundaryCells);
  lattice.getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuBoundaryCells);

  std::cout << "Done STL GPU boundary check" << std::endl;
  lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().transferToCPU(&gradBoundary, lattice.getLatticePointer()->getDataHandler(&gradBoundary));

  T** cpuGradData = lattice.getLatticePointer()->getDataHandler(&gradBoundary)->getPostProcData();
  size_t * cpuGradIndices = lattice.getLatticePointer()->getDataHandler(&gradBoundary)->getCellIDsData();
  cudaMemcpy(cpuGradIndices, deviceGradIndices, sizeof(size_t)*numBoundaryCells, cudaMemcpyDefault);
  std::ofstream gradData("gradData.csv");

  for (int i = 0; i < numBoundaryCells; i++) {
    size_t indices[3];
    util::getCellIndices3D(cpuGradIndices[i], lattice._localSubDomain.localGridSize()[1], lattice._localSubDomain.localGridSize()[2], indices);

    gradData << indices[0] << "," <<indices[1] <<"," << indices[2] <<"," << cpuGradData[11][i]<<","
                                                                         << cpuGradData[12][i]<<","
                                                                         << cpuGradData[13][i]<<","
                                                                         << cpuGradData[14][i]<<","
                                                                         << cpuGradData[15][i]<<","
                                                                         << cpuGradData[16][i]<<","
                                                                         << cpuGradData[17][i]<<","
                                                                         << cpuGradData[18][i]<<","
                                                                         << cpuGradData[19][i]<<","
                                                                         << cpuGradData[20][i]<<","
                                                                         << cpuGradData[21][i]<<","
                                                                         << cpuGradData[22][i]<<","
                                                                         << cpuGradData[23][i]<<","
                                                                         << cpuGradData[24][i]<<","
                                                                         << cpuGradData[25][i]<<","
                                                                         << cpuGradData[26][i]<<","
                                                                         << cpuGradData[27][i]<<","
                                                                         << cpuGradData[28][i]<<","
                                                                         << cpuGradData[29][i]<<"\n";

  }
  gradData.close();


  lattice.copyLatticesToCPU();
  writer.write(1);


}

int main()
{
  const double simTime = 50;
  MultipleSteps(simTime);
  finalizeIPC();
  return 0;
}