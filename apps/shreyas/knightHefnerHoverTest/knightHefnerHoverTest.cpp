/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "boundary/boundaryCondition3D.h"
#include "boundary/boundaryCondition3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "io/superVtmWriter3D.h"
#include "io/superVtmWriter3D.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template <typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice &lattice, Dynamics<T, Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0N);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &plane2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

}



T rotorRadius = 0.762; // meters
T rotorArea = M_PI*pow(rotorRadius,2); // m^2
T bladeAngularVelocity = 100.53; // rad/s
T physInducedVelocity = 3.830; // m/s, calculated by assuming C_T = 0.005, lambda (inflow ratio) = sqrt(C_T/2), and RPM of the blade = 960
T physDensity = 1.225; // kg/m^3
T physKinematicViscosity = 1.8e-5; // m^2/s
T smagoConstant = 0.075;
T domainWidth = 4.0;
T bladeTheta = 12.0*M_PI/180.0; // rad
T rotorTipSpeed = rotorRadius*bladeAngularVelocity;
T rotorSolidity = 0.1061;
T airfoilLiftSlope = 5.73; //see Leishman p120 bottom
T inboardCutoffRadius = 0.167; //account for the inner portion of the blade not doing much aerodynamically

T rotorY = 0.75*domainWidth;
T rotorMinX = domainWidth/2.0-rotorRadius;
T rotorMaxX = domainWidth/2.0+rotorRadius;
T rotorMinZ = domainWidth/2.0-rotorRadius;
T rotorMaxZ = domainWidth/2.0+rotorRadius;

T getThrustCoefficientFromInflowRatio(T inflowRatio, T rotorSolidity, T liftSlope, T theta) {
    T thrustCoefficient = 0.5*rotorSolidity*liftSlope*(theta/3.0-inflowRatio/2.0-theta*pow(inboardCutoffRadius,3)/3.0+inflowRatio*pow(inboardCutoffRadius,2)/2.0);
    return thrustCoefficient;
}

template <class Blocklattice>
void defineRotorThrust(T latticeVerticalForce, unsigned int resolution, Blocklattice &lattice) {

    int iY = (int) (rotorY*resolution/domainWidth);
    for (int iX = (int) (rotorMinX*resolution/domainWidth)-2; iX <= (int) (rotorMaxX*resolution/domainWidth)+2; ++iX) {
        for (int iZ = (int) (rotorMinZ*resolution/domainWidth)-2; iZ <= (int) (rotorMaxZ*resolution/domainWidth)+2; ++iZ) {
            //check if corners of grid point in the rotor circle:
            T currentLatticeRadiusCorner1 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX-0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ-0.5, 2));
            T currentLatticeRadiusCorner2 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX+0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ-0.5, 2));
            T currentLatticeRadiusCorner3 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX-0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ+0.5, 2));
            T currentLatticeRadiusCorner4 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX+0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ+0.5, 2));

            if (currentLatticeRadiusCorner1 <= rotorRadius*resolution/domainWidth && currentLatticeRadiusCorner2 <= rotorRadius*resolution/domainWidth && currentLatticeRadiusCorner3 <= rotorRadius*resolution/domainWidth && currentLatticeRadiusCorner4 <= rotorRadius*resolution/domainWidth) {
                T latticeForce[3] = {0.0, -latticeVerticalForce, 0.0};
                lattice.defineForce(iX, iX, iY, iY, iZ, iZ, latticeForce);
            }
            else if (currentLatticeRadiusCorner1 <= rotorRadius*resolution/domainWidth || currentLatticeRadiusCorner2 <= rotorRadius*resolution/domainWidth || currentLatticeRadiusCorner3 <= rotorRadius*resolution/domainWidth || currentLatticeRadiusCorner4 <= rotorRadius*resolution/domainWidth) {
                int subResolution = 10;
                T subResolutionDistance = 1.0/subResolution;
                int totalPointsSurveyed {0};
                int validPoints{0};
                for (T iXsubX = (T) iX-0.5; iXsubX <= iX+0.5; iXsubX+=subResolutionDistance) {
                    for (T iZsubZ = (T) iZ-0.5; iZsubZ <= iZ+0.5; iZsubZ+=subResolutionDistance) {
                        totalPointsSurveyed++;
                        T currentLatticeSubRadius = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iXsubX, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZsubZ, 2));

                        if (currentLatticeSubRadius <= rotorRadius*resolution/domainWidth) {
                            validPoints++;
                        }
                    }
                }
                T percentageCovered = (T) validPoints/totalPointsSurveyed;
                T latticeForce[3] = {0.0, -latticeVerticalForce*percentageCovered, 0.0};
                lattice.defineForce(iX, iX, iY, iY, iZ, iZ, latticeForce);
            }
        }
    }
}

template <class Blocklattice>
T calculateAverageLatticeInflow(unsigned int resolution, Blocklattice &lattice) {

    int gridCellsSampled = 0;
    T inflowVelocityRunningSum = 0.0;

    int iY = (int) (rotorY*resolution/domainWidth);
    for (int iX = (int) (rotorMinX*resolution/domainWidth)-2; iX <= (int) (rotorMaxX*resolution/domainWidth)+2; ++iX) {
        for (int iZ = (int) (rotorMinZ*resolution/domainWidth)-2; iZ <= (int) (rotorMaxZ*resolution/domainWidth)+2; ++iZ) {
            //check if corners of grid point in the rotor circle:
            T currentLatticeRadiusCorner1 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX-0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ-0.5, 2));
            T currentLatticeRadiusCorner2 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX+0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ-0.5, 2));
            T currentLatticeRadiusCorner3 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX-0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ+0.5, 2));
            T currentLatticeRadiusCorner4 = sqrt(pow((domainWidth/2.0)*resolution/domainWidth-iX+0.5, 2)+pow((domainWidth/2.0)*resolution/domainWidth-iZ+0.5, 2));

            if (currentLatticeRadiusCorner1 <= rotorRadius*resolution/domainWidth && currentLatticeRadiusCorner2 <= rotorRadius*resolution/domainWidth && currentLatticeRadiusCorner3 <= rotorRadius*resolution/domainWidth && currentLatticeRadiusCorner4 <= rotorRadius*resolution/domainWidth) {
                T currentCellVelocity[3];
                //std::cout << "Running get operation on cell " << iX << ", " << iY << ", " << iZ << std::endl;
                lattice.get(iX, iY, iZ).computeU(currentCellVelocity);
                //std::cout << "currentCelVelocity[1]: " << currentCellVelocity[1] << std::endl;
                inflowVelocityRunningSum += -currentCellVelocity[1]; //add the y-direction velocity to inflow velocity, negative sign because inflow is down.
                gridCellsSampled++;
            }
        }
    }

    return inflowVelocityRunningSum/gridCellsSampled;
}

void MultipleSteps(const double simTime, unsigned int resolution)
{    
    T gridSpacing = domainWidth/resolution;
    T gridArea = pow(gridSpacing,2);


    int iXLeftBorder = 0;
    int iXRightBorder = resolution;
    int iYBottomBorder = 0;
    int iYTopBorder = resolution;
    int iZFrontBorder = 0;
    int iZBackBorder = resolution;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), domainWidth, 2*physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create BlockLattice.... ";

    BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

    std::cout << "Finished!" << std::endl;

    std::vector<int> limits = {iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZBackBorder};

    std::cout << "Define boundaries.... ";
    defineBoundaries(lattice, bulkDynamics, limits);

    std::cout << "Init GPU data.... ";
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... ";
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
    
    T currentThrustCoefficient = getThrustCoefficientFromInflowRatio(0.0, rotorSolidity, airfoilLiftSlope, bladeTheta);
    T currentThrust = currentThrustCoefficient*physDensity*rotorArea*pow(rotorTipSpeed,2);
    T currentDiskLoading = currentThrust/rotorArea;
    T latticeVerticalForce = converter.getLatticeForce(currentDiskLoading*gridArea);
    defineRotorThrust(latticeVerticalForce, resolution, lattice);
    
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

    unsigned int preStep = 1;
    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");
    BlockVTKwriter3D<T> writer("outputVTK");
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.write(0);

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {
        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        //HANDLE_ERROR(cudaGetLastError());
        if ((trimStep % converter.getLatticeTime(0.01)) == 0)
        {
            std::cout << "Trim step:       " << trimStep << std::endl;
            std::cout << "Simulation time: " << converter.getPhysTime(trimStep) << std::endl;
            lattice.copyDataToCPU();
            writer.write(trimStep);
        }
        if ((trimStep % converter.getLatticeTime(0.1)) == 0) { //update rotor force every 0.1 physical seconds
            lattice.copyDataToCPU();


            std::cout << "got here 1" <<std::endl;
            T currentInflowVelocity = converter.getPhysVelocity(calculateAverageLatticeInflow(resolution, lattice));
            T currentInflowRatio = currentInflowVelocity/rotorTipSpeed;
            std::cout << "got here 2" <<std::endl;

            T currentThrustCoefficient = getThrustCoefficientFromInflowRatio(currentInflowRatio, rotorSolidity, airfoilLiftSlope, bladeTheta);
            T currentThrust = currentThrustCoefficient*physDensity*rotorArea*pow(rotorTipSpeed,2);
            T currentDiskLoading = currentThrust/rotorArea;
            T latticeVerticalForce = converter.getLatticeForce(currentDiskLoading*gridArea);
            defineRotorThrust(latticeVerticalForce, resolution, lattice);

            lattice.copyDataToGPU();

            std::cout << "Current Inflow Velocity: " << currentInflowVelocity << std::endl;
            std::cout << "Current Thrust Coefficient: " << currentThrustCoefficient << std::endl;
            std::cout << "Current Thrust: " << currentThrust << std::endl;
            std::cout << "Current Disk Loading: " << currentDiskLoading << std::endl;
            std::cout << "Grid Area: " << gridArea <<std::endl;
            std::cout << "Current Lattice Vertical Force: " << latticeVerticalForce << std::endl;

        }

        
    }
}

int main(int argc, char **argv)
{
    const double simTime = 10;
    unsigned int resolution = 64;
    MultipleSteps(simTime, resolution);
    return 0;
}
