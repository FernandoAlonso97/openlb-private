/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "core/affineTransform.h"

#include <cmath>
#include <algorithm>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.15;
const T domainWidth = 3.2;
const double simTime = 30;
unsigned int resolution = 64;

const T gridSpacing = domainWidth/resolution;
const T gridArea = pow(gridSpacing,2);

const T physInducedVelocity = 3.830; // m/s, calculated by assuming C_T = 0.005, lambda (inflow ratio) = sqrt(C_T/2), and RPM of the blade = 960
//const T physInducedVelocity = 2.9668;
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 1.8e-5; // m^2/s

const T rotorStartTime = 2.0;
const T rotorRampUpTime = 10;

const T BETUpdateInterval = 0.02; //s

/* Rotor Parameters */
const T rotorRadius = 0.762; // meters
const T rotorLatticeRadius = rotorRadius/gridSpacing;
const T rotorInboardCutoffRadius = 0.167; //account for the inner portion of the blade not doing much aerodynamically
const T rotorLatticeInboardCutoffRadius = rotorInboardCutoffRadius/gridSpacing;
const T rotorArea = M_PI*pow(rotorRadius,2)-M_PI*pow(rotorInboardCutoffRadius,2); // m^2
const T rotorNominalAngularVelocity = 100.53; //rad/s
const T rotorNominalTipSpeed = rotorRadius*rotorNominalAngularVelocity; //m/s
T rotorTipSpeed = 0.01; //m/s
const int numberOfBlades = 5;
T bladeTheta = 6.0*M_PI/180.0; // rad
const T rotorSolidity = 0.1061;
const T airfoilLiftSlope = 5.73; //see Leishman p120 bottom
const T airfoilDragCoefficient = 0.011; // see Leishman p124 
const T inducedPowerFactor = 1.25; //see Leishman p124 

const int rotorXExternalFrame = 0.5*resolution;
const int rotorYExternalFrame = 0.5*resolution; //center of the rotor
const int rotorZExternalFrame = 0.75*resolution;
const int rotorScanLength = ceil(2*1.1*rotorLatticeRadius);

const T rotorTiltYAxis = M_PI/12.0;

Matrix<T,3,3> externalTransformRotor(0.0, rotorTiltYAxis, 0.0, Rotation::ToSpace{});
Matrix<T,3,3> rotorTransformExternal(0.0, rotorTiltYAxis, 0.0, Rotation::ToBody{});

AffineTransform<T, 3> externalTransformRotorRotationOnly(externalTransformRotor);
AffineTransform<T, 3> rotorTransformExternalRotationOnly(rotorTransformExternal);

AffineTransform<T,3> externalTransformRotorTranslation;
AffineTransform<T,3> rotorTransformExternalTranslation;

Vec3<T> rotorNormalRotorFrame(0.0,0.0,1.0);
Vec3<T> rotorNormalExternalFrame = transform<T>(rotorNormalRotorFrame, externalTransformRotorRotationOnly);

Vec3<T> rotorTangentialRotorFrame(1.0,0.0,0.0);
Vec3<T> rotorTangentialExternalFrame = transform<T>(rotorTangentialRotorFrame, externalTransformRotorRotationOnly);

std::unordered_map<int, T> rotorCellWeights;
std::set<int> rotorInflowCellIDs;
std::unordered_map<int, T> rotorCellAzimuths;
std::unordered_map<int, T> rotorCellRadii;

/* Output Parameters */
bool outputVTKData = true;
bool outputRotorData = true;
bool outputDebugData = false;

const T vtkWriteInterval = 0.2; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics)
{
    int iXLeftBorder = 0;
    int iXRightBorder = lattice.getNx()-1;
    int iYBottomBorder = 0;
    int iYTopBorder = lattice.getNy()-1;
    int iZFrontBorder = 0;
    int iZBackBorder = lattice.getNz()-1;

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0N);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &plane2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

}

template <typename T, template<typename> class Lattice>
void defineRotorCellIDs(BlockLattice3D<T, Lattice> &lattice) {
    
    externalTransformRotorTranslation = externalTransformRotorRotationOnly;
    externalTransformRotorTranslation.applyToLeft(Vec3<T>(rotorXExternalFrame, rotorYExternalFrame, rotorZExternalFrame));

    rotorTransformExternalTranslation = rotorTransformExternalRotationOnly;
    rotorTransformExternalTranslation.applyToRight(Vec3<T>(-rotorXExternalFrame, -rotorYExternalFrame, -rotorZExternalFrame));

    //std::cout << transform(Vec3<T>((T)rotorXExternalFrame, (T)rotorYExternalFrame, (T)rotorZExternalFrame),rotorTransformExternalTranslation) << std::endl;

    int sampleResolution = 11; //per cell
    T sampleAreaWeight = pow(1.0/(T)sampleResolution, 2)/(M_PI*pow(rotorLatticeRadius,2)-M_PI*pow(rotorLatticeInboardCutoffRadius, 2));
    std::cout << "Sample area weight: " << sampleAreaWeight << std::endl;

    std::set<int> cellIDsOverlappingRotor;
    std::set<int> cellIDsOverlappingEmptySpace;
    std::set<int> cellIDsWithoutSignificantWeight;

    T significantWeightThreshold = 50*sampleAreaWeight;

    T currentZRotorFrame = 0.0; 
    for (int iX = 0; iX <= rotorScanLength*sampleResolution; iX++) {
        T currentXRotorFrame = -(T)rotorScanLength/2.0+(1.0/(T)sampleResolution)/2.0+iX*(1.0/(T)sampleResolution);
        for (int iY = 0; iY <= rotorScanLength*sampleResolution; iY++) {
            T currentYRotorFrame = -(T)rotorScanLength/2.0+(1.0/(T)sampleResolution)/2.0+iY*(1.0/(T)sampleResolution);

            Vec3<int> externalFrameCellCoords = vecToInt(transform(Vec3<T>(currentXRotorFrame, currentYRotorFrame, currentZRotorFrame),externalTransformRotorTranslation));
            T currentRadius = sqrt(pow(currentXRotorFrame,2)+pow(currentYRotorFrame,2));
            int currentCellID = getCellIndex3D(externalFrameCellCoords(0), externalFrameCellCoords(1), externalFrameCellCoords(2), lattice.getNy(), lattice.getNz());
            //std::cout << "Current lattice radius = " << currentRadius << std::endl;;
            if (currentRadius <= rotorLatticeRadius && currentRadius >= rotorLatticeInboardCutoffRadius) {
                cellIDsOverlappingRotor.insert(currentCellID);
                if (rotorCellWeights.find(currentCellID) == rotorCellWeights.end()) {
                    rotorCellWeights.insert({currentCellID, sampleAreaWeight});
                }
                else {
                    rotorCellWeights[currentCellID] = rotorCellWeights[currentCellID] + sampleAreaWeight;
                }

            }
            else {
                cellIDsOverlappingEmptySpace.insert(currentCellID);
            }

        }
    }


    for (auto currentCell : rotorCellWeights) {
        size_t indices[3];
        util::getCellIndices3D(currentCell.first, lattice.getNy(), lattice.getNz(), indices);

        int iX = indices[0]; int iY = indices[1]; int iZ = indices[2];

        Vec3<T> rotorFrameCellCoords = transform(Vec3<T>((T)iX, (T)iY, (T)iZ),rotorTransformExternalTranslation);
        T currentRadius = sqrt(pow(rotorFrameCellCoords(0),2) + pow(rotorFrameCellCoords(1),2))/rotorLatticeRadius;
        T currentAzimuth = atan2(rotorFrameCellCoords(1), rotorFrameCellCoords(0));

        rotorCellRadii.insert({currentCell.first, currentRadius});
        rotorCellAzimuths.insert({currentCell.first, currentAzimuth});

        if (currentCell.second < significantWeightThreshold)
            cellIDsWithoutSignificantWeight.insert(currentCell.first);
    }

    std::set<int> tempSet;

    std::set_difference(cellIDsOverlappingRotor.begin(), cellIDsOverlappingRotor.end(), cellIDsOverlappingEmptySpace.begin(), cellIDsOverlappingEmptySpace.end(), std::inserter(tempSet, tempSet.end())); //calculate the rotor inflow cell IDs    
    std::set_difference(tempSet.begin(), tempSet.end(), cellIDsWithoutSignificantWeight.begin(), cellIDsWithoutSignificantWeight.end(), std::inserter(rotorInflowCellIDs, rotorInflowCellIDs.end())); //calculate the rotor inflow cell IDs    

    std::cout << "Unpruned set size: " << tempSet.size() << std::endl;
    std::cout << "Pruned set size: " << rotorInflowCellIDs.size() << std::endl;


    // for (auto cellID : rotorInflowCellIDs) {
    //     std::cout << "Inflow cell: " << cellID << std::endl;
    // }
    // for (auto cellID : cellIDsOverlappingRotor) {
    //     std::cout << "rotor overlap cell: " << cellID << std::endl;
    // }
    // for (auto cellID : cellIDsOverlappingEmptySpace) {
    //     std::cout << "empty overlap cell: " << cellID << std::endl;
    // }
    T runningSum = 0.0;
    for (auto cell : rotorCellWeights) {
        //std::cout << "cell " << cell.first << " weight: " << cell.second << std::endl;
        runningSum += cell.second;
    }
    // for (auto cell : rotorCellAzimuths) {
    //     std::cout << "cell " << cell.first << " azimuth: " << cell.second << std::endl;
    // }
    for (auto cell : rotorCellRadii) {
        std::cout << "cell " << cell.first << " radii: " << cell.second << std::endl;
    }
    std::cout << "Total weight fraction: " << runningSum << std::endl;
}

std::vector<std::vector<T>> getHarmonicThrustFromHarmonicInflowAndAdvanceRatio(std::vector<T> harmonicInflow, T rotorSolidity, T liftSlope, T theta, T advanceRatio) {

    int azimuthalSteps = 72;
    T azimuthalStep = 2.0*M_PI/azimuthalSteps;

    int numHarmonics = 10;

    //simple rectangular integration, should be good enough
    int radialIntegrationSteps = 500;
    T rStep = (1.0-rotorInboardCutoffRadius/rotorRadius)/radialIntegrationSteps;
    std::vector<std::vector<T>> thrustCoefficientHarmonics;
    thrustCoefficientHarmonics.resize(numHarmonics, std::vector<T>(2)); // initialize harmonics vector
    
    for (int i = 0; i < azimuthalSteps; i++) {
        T currentAzimuth = azimuthalStep*i;

        T currentAzimuthalThrustCoefficient = 0.0;
        for (int j = 0; j < radialIntegrationSteps; j++) {
            T current_r = rotorInboardCutoffRadius/rotorRadius+j*rStep;
            T localInflow = harmonicInflow[0]+harmonicInflow[1]*current_r*sin(currentAzimuth)+harmonicInflow[2]*current_r*cos(currentAzimuth);
            T localInflowRatio = localInflow/rotorTipSpeed;
            T currentPhi = localInflowRatio/current_r;
            T prandtl_f = ((T)numberOfBlades/2.0)*(1.0-current_r)/(current_r*currentPhi);
            T prandtl_F = (2.0/M_PI)*acos(exp(-prandtl_f));
            if (isnan(prandtl_F)) {
                prandtl_F = 1.0;
            }
            T dThrustCoefficient = 0.5*rotorSolidity*liftSlope*(theta*pow(current_r+advanceRatio*sin(currentAzimuth),2)-localInflowRatio*(current_r+advanceRatio*sin(currentAzimuth))); //this expression takes into account advance ratio, even though we are simulating hover in this file
            dThrustCoefficient = dThrustCoefficient*prandtl_F; // apply the tip loss
            currentAzimuthalThrustCoefficient += dThrustCoefficient*rStep;
        }

        for (int j = 0; j < numHarmonics; j++) {
            thrustCoefficientHarmonics[j][0] += currentAzimuthalThrustCoefficient*cos(j*currentAzimuth)*2.0/azimuthalSteps;
            thrustCoefficientHarmonics[j][1] += currentAzimuthalThrustCoefficient*sin(j*currentAzimuth)*2.0/azimuthalSteps;
        }
    }
    return thrustCoefficientHarmonics;
}

T getPowerCoefficientFromHarmonicInflowAndAdvanceRatio(std::vector<T> harmonicInflow, T rotorSolidity, T liftSlope, T theta, T advanceRatio) {

    int azimuthalSteps = 72;
    T azimuthalStep = 2.0*M_PI/azimuthalSteps;

    //simple rectangular integration, should be good enough
    int radialIntegrationSteps = 500;
    T rStep = (1.0-rotorInboardCutoffRadius/rotorRadius)/radialIntegrationSteps;

    T powerCoefficient = 0;
    
    for (int i = 0; i < azimuthalSteps; i++) {
        T currentAzimuth = azimuthalStep*i;

        T currentAzimuthalPowerCoefficient = 0.0;
        for (int j = 0; j < radialIntegrationSteps; j++) {
            T current_r = rotorInboardCutoffRadius/rotorRadius+j*rStep;
            T localInflow = harmonicInflow[0]+harmonicInflow[1]*current_r*sin(currentAzimuth)+harmonicInflow[2]*current_r*cos(currentAzimuth);
            T localInflowRatio = localInflow/rotorTipSpeed;
            T currentPhi = localInflowRatio/current_r;

            T dInducedPowerCoefficient = 0.5*rotorSolidity*liftSlope*(theta*localInflowRatio*(current_r+advanceRatio*sin(currentAzimuth))-pow(localInflowRatio,2))*current_r; //this expression takes into account advance ratio, even though we are simulating hover in this file
            T dProfilePowerCoefficient = 0.5*rotorSolidity*liftSlope*((airfoilDragCoefficient/liftSlope)*pow((current_r+advanceRatio*sin(currentAzimuth)),2))*current_r;
            dInducedPowerCoefficient *= inducedPowerFactor; //apply power factor only on induced portion
            T dPowerCoefficient = dInducedPowerCoefficient + dProfilePowerCoefficient;
            currentAzimuthalPowerCoefficient += dPowerCoefficient*rStep;
        }
        powerCoefficient += currentAzimuthalPowerCoefficient*2.0/azimuthalSteps;
        
    }
    powerCoefficient *= 0.5; //a fourier series averaging quirk
    return powerCoefficient;
}

template <typename T, template<typename> class Lattice>
void defineRotorThrustFromHarmonicCoefficients(std::vector<std::vector<T>> harmonicThrustCoefficients, BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter) {

    for (auto rotorCell : rotorCellWeights) {
        size_t indices[3];
        util::getCellIndices3D(rotorCell.first, lattice.getNy(), lattice.getNz(), indices);

        int iX = indices[0]; int iY = indices[1]; int iZ = indices[2];
        T currentCellWeight = rotorCell.second;
    
        T currentCellAzimuth = rotorCellAzimuths[rotorCell.first];

        T currentCellThrustCoefficient = 0.0;
        currentCellThrustCoefficient += harmonicThrustCoefficients[0][0]*0.5;
        for (int i = 1; i < harmonicThrustCoefficients.size(); i++) {
            currentCellThrustCoefficient += harmonicThrustCoefficients[i][0]*cos(i*currentCellAzimuth)+harmonicThrustCoefficients[i][1]*sin(i*currentCellAzimuth);
        }

        T currentCellThrust = currentCellThrustCoefficient*physDensity*rotorArea*pow(rotorTipSpeed,2)*currentCellWeight;
        T latticeVerticalForce = converter.getLatticeForce(currentCellThrust);
        
        T latticeForce[3] = {-latticeVerticalForce*rotorNormalExternalFrame(0), -latticeVerticalForce*rotorNormalExternalFrame(1), -latticeVerticalForce*rotorNormalExternalFrame(2)};
        lattice.defineForce(iX, iX, iY, iY, iZ, iZ, latticeForce);
    }
}

template <typename T, template<typename> class Lattice>
std::vector<T> calculateHarmonicInflow(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter) {

    int gridCellsSampled = rotorInflowCellIDs.size();
    T inflowVelocityRunningSum = 0.0;
    T inflowSineVelocityRunningSum = 0.0;
    T inflowCosineVelocityRunningSum = 0.0;

    for (int rotorCellID : rotorInflowCellIDs) {
        size_t indices[3];
        util::getCellIndices3D(rotorCellID, lattice.getNy(), lattice.getNz(), indices);

        int iX = indices[0]; int iY = indices[1]; int iZ = indices[2];
       
        T currentCellVelocity[3];
        lattice.get(iX, iY, iZ).computeU(currentCellVelocity);
        Vec3<T> currentCellVelocityVector(currentCellVelocity[0], currentCellVelocity[1], currentCellVelocity[2]);

        T currentCellInflowVelocity = dotProduct(currentCellVelocityVector, rotorNormalExternalFrame);
        T currentCellRadius = rotorCellRadii[rotorCellID];
        T currentCellAzimuth = rotorCellAzimuths[rotorCellID];

        inflowVelocityRunningSum += -currentCellInflowVelocity; //add the y-direction velocity to inflow velocity, negative sign because inflow is down.
        inflowSineVelocityRunningSum += -currentCellInflowVelocity*currentCellRadius*sin(currentCellAzimuth);
        inflowCosineVelocityRunningSum += -currentCellInflowVelocity*currentCellRadius*cos(currentCellAzimuth);
    }

    T inflowHarmonicMean = converter.getPhysVelocity(inflowVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicSine = converter.getPhysVelocity(4.0*inflowSineVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicCosine = converter.getPhysVelocity(4.0*inflowCosineVelocityRunningSum/gridCellsSampled);

    std::vector<T> harmonicVector {inflowHarmonicMean, inflowHarmonicSine, inflowHarmonicCosine};
    return harmonicVector;
}

template <typename T, template<typename> class Lattice>
T calculateAverageAdvanceVelocity(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter) {

    int gridCellsSampled = rotorInflowCellIDs.size();
    T advanceVelocityRunningSum = 0.0;

    for (int rotorCellID : rotorInflowCellIDs) {
        size_t indices[3];
        util::getCellIndices3D(rotorCellID, lattice.getNy(), lattice.getNz(), indices);

        int iX = indices[0]; int iY = indices[1]; int iZ = indices[2];

        T currentCellVelocity[3];
        lattice.get(iX, iY, iZ).computeU(currentCellVelocity);
        Vec3<T> currentCellVelocityVector(currentCellVelocity[0], currentCellVelocity[1], currentCellVelocity[2]);

        T currentCellAdvanceVelocity = dotProduct(currentCellVelocityVector, rotorTangentialExternalFrame);

        advanceVelocityRunningSum += currentCellAdvanceVelocity; 
    }

    return converter.getPhysVelocity(advanceVelocityRunningSum/gridCellsSampled);
}

void MultipleSteps()
{    
    int iXRightBorder = resolution;
    int iYTopBorder = resolution;
    int iZBackBorder = resolution;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), domainWidth, 2*physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);

    std::cout << "Create BlockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

    std::cout << "Define boundaries.... "  << std::endl;
    defineBoundaries(lattice, bulkDynamics);

    std::cout << "Define rotor cells.... "  << std::endl;
    defineRotorCellIDs(lattice);

    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }

    std::cout << "Finished!" << std::endl;

    //std::vector<std::vector<T>> testHarmonicCoefficients = {{0.00337981,0.0}};
    //defineRotorThrustFromHarmonicCoefficients(testHarmonicCoefficients, lattice, converter);

    lattice.copyDataToGPU();


    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");
    BlockVTKwriter3D<T> writer("KHTiltedHover");
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeMiscInfo3D<T, Lattice> radiiFunctor(lattice, rotorCellRadii, "radii");
    BlockLatticeMiscInfo3D<T, Lattice> azimuthFunctor(lattice, rotorCellAzimuths, "azimuths", -100.0);

    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(radiiFunctor);
    writer.addFunctor(azimuthFunctor);
    if (outputVTKData)
        writer.write(0);

    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();

    PolynomialStartScale<T, T> rotorStartScale(converter.getLatticeTime(rotorRampUpTime), 1.0);

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {
        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            if (outputVTKData) {
                lattice.copyDataToCPU();
                writer.write(trimStep);
            }
        }

        if ((trimStep % converter.getLatticeTime(BETUpdateInterval)) == 0  && trimStep >= converter.getLatticeTime(rotorStartTime)) { //update rotor force every 0.02 physical seconds

            lattice.copyDataToCPU();

            if (trimStep <= converter.getLatticeTime(rotorStartTime+rotorRampUpTime)) { //account for rotor ramp up
                T frac[1];
                T iT[1] = {(T)trimStep - (T)converter.getLatticeTime(rotorStartTime)};
                rotorStartScale(frac, iT);
                rotorTipSpeed = rotorNominalTipSpeed*frac[0];
                if (rotorTipSpeed < 0.01)
                    rotorTipSpeed = 0.01; //prevent divide by zero in BET code
            }

            std::vector<T> currentHarmonicInflow = calculateHarmonicInflow(lattice, converter);
            T currentAdvanceVelocity = calculateAverageAdvanceVelocity(lattice, converter);
            T currentAdvanceRatio = currentAdvanceVelocity/rotorTipSpeed;
            std::vector<std::vector<T>> currentHarmonicThrustCoefficient = getHarmonicThrustFromHarmonicInflowAndAdvanceRatio(currentHarmonicInflow, rotorSolidity, airfoilLiftSlope, bladeTheta, currentAdvanceRatio);
            //std::vector<std::vector<T>> testHarmonicCoefficients = {{0.00337981,0.0}};
            //defineRotorThrustFromHarmonicCoefficients(testHarmonicCoefficients, lattice, converter);

            T currentPowerCoefficient = getPowerCoefficientFromHarmonicInflowAndAdvanceRatio(currentHarmonicInflow, rotorSolidity, airfoilLiftSlope, bladeTheta, currentAdvanceRatio);

            defineRotorThrustFromHarmonicCoefficients(currentHarmonicThrustCoefficient, lattice, converter);

            lattice.copyDataToGPU();

            if (outputRotorData) {
                std::cout << "Current Average Inflow Velocity: " << currentHarmonicInflow[0] << std::endl;
                std::cout << "Current Sine Inflow Velocity: " << currentHarmonicInflow[1] << std::endl;
                std::cout << "Current Cosine Inflow Velocity: " << currentHarmonicInflow[2] << std::endl;
                std::cout << "Current Advance Ratio: " << currentAdvanceRatio << std::endl;
                std::cout << "Current Mean Thrust Coefficient: " << currentHarmonicThrustCoefficient[0][0]*0.5 << std::endl;
                std::cout << "Current Mean Power Coefficient: " << currentPowerCoefficient << std::endl;
            }
        }
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    MultipleSteps();
    return 0;
}

