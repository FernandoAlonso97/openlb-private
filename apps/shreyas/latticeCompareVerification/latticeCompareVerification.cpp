/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef float T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "boundary/boundaryCondition3D.h"
#include "boundary/boundaryCondition3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "io/superVtmWriter3D.h"
#include "io/superVtmWriter3D.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template <typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice &lattice, Dynamics<T, Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0N);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &plane2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

    //static OnLatticeBoundaryCondition3D<T, Lattice>  
}

template <unsigned int RESOLUTION>
void MultipleSteps(const double simTime)
{

    int iXLeftBorder = 0;
    int iXRightBorder = RESOLUTION;
    int iYBottomBorder = 0;
    int iYTopBorder = RESOLUTION;
    int iZFrontBorder = 0;
    int iZBackBorder = RESOLUTION;

    T const length = 1.;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        iXRightBorder, 0.3 * 1.0 / std::sqrt(3), 4. * length, 40., 0.0000146072, 1.225, 0);

    converter.print();
    T spacing = converter.getConversionFactorLength();
    T cellSize = spacing * spacing;

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamicsReference(omega, 0.05);
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamicsVerification(omega, 0.05);
    
    // BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamicsReference(omega);
    // BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamicsVerification(omega);

    std::cout << "Create blockLattice.... ";

    BlockLattice3D<T, Lattice> refLattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamicsReference);
    BlockLattice3D<T, Lattice> verificationLattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamicsVerification);

    std::cout << "Finished!" << std::endl;

    std::vector<int> limits = {iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZBackBorder};

    std::cout << "Define boundaries.... ";
    defineBoundaries(refLattice, bulkDynamicsReference, limits);
    defineBoundaries(verificationLattice, bulkDynamicsVerification, limits);

    T u[3] = {0, 0, 0};
    std::cout << "Init equilibrium.... ";
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[Lattice<T>::d] = {0., 0., 0.};
                T rho[1];
                refLattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
                verificationLattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }

    T testForce[3] = {0.05, 0.03, 0.01};
    refLattice.defineForce(5,5,5,5,5,5,testForce);
    verificationLattice.defineForce(5,5,5,5,5,5,testForce);
    T testVel[3] = {-0.03, 0.04, 0.02};
    refLattice.iniEquilibrium(4,4,4,4,4,4, 1.0, testVel);
    verificationLattice.iniEquilibrium(4,4,4,4,4,4, 1.0, testVel);

    std::cout << "Finished!" << std::endl;

    std::cout << "Init GPU data.... ";
    refLattice.initDataArrays();
    verificationLattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    singleton::directories().setOutputDir("./tmp/");
    BlockVTKwriter3D<T> refWriter("outputVTKRef");
    BlockVTKwriter3D<T> verificationWriter("outputVTKVerification");

    BlockLatticePhysVelocity3D<T,Lattice> refPhysVelocityFunctor(refLattice, 0, converter);
    refWriter.addFunctor(refPhysVelocityFunctor);
    refWriter.write(0);

    BlockLatticePhysVelocity3D<T,Lattice> verificationPhysVelocityFunctor(verificationLattice, 0, converter);
    verificationWriter.addFunctor(verificationPhysVelocityFunctor);
    verificationWriter.write(0);

    refLattice.copyDataToGPU();
    verificationLattice.copyDataToGPU();

    for (unsigned int trimStep = 0; trimStep < 100; ++trimStep)
    {

        
        refLattice.fieldCollisionGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        // refLattice.fieldCollisionGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        refLattice.boundaryCollisionGPU();
        cudaDeviceSynchronize();
        refLattice.streamGPU();
        cudaDeviceSynchronize();
        refLattice.postProcessGPU();
        cudaDeviceSynchronize();

        verificationLattice.fastD3Q19BGKDynamicsFieldCollisionGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        // verificationLattice.fastD3Q19BGKDynamicsFieldCollisionGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        // verificationLattice.fieldCollisionGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        verificationLattice.boundaryCollisionGPU();
        cudaDeviceSynchronize();
        verificationLattice.streamGPU();
        cudaDeviceSynchronize();
        verificationLattice.postProcessGPU();
        cudaDeviceSynchronize();
        
        refLattice.copyDataToCPU();
        verificationLattice.copyDataToCPU();
        if (trimStep % 100 == 0) {
            refWriter.write(trimStep);
            verificationWriter.write(trimStep);
        }

        double maxDifference = 0.0;
        for (int iX = 1; iX <= iXRightBorder-1; ++iX)
            for (int iY = 1; iY <= iYTopBorder-1; ++iY)
                for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ)
                {
                    for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
                        T* refData = refLattice.getData(iX, iY, iZ, iPop);
                        T* verificationData = verificationLattice.getData(iX, iY, iZ, iPop);
                        double difference = fabs(*refData-*verificationData);
                        maxDifference = fmax(difference, maxDifference);
                    }
                }

        std::cout << "Max difference: " << maxDifference << std::endl;
    }
}

int main(int argc, char **argv)
{
    const double simTime = 10.0;
    MultipleSteps<127>(simTime);
    return 0;
}