/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"

#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinementUtil3D.h"

#include "core/affineTransform.h"

#include <chrono>
#include <omp.h>
#include <map>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int resolution = 700;
const T desiredLatticeVel = 0.05;
const T desiredFreestream = 20.5778; //m/s
const T shipLength = 150.0; //m
const T kinematicViscosity = 1.48E-5;
const T physDensity = 1.225;
const T smagoConst = 0.05;

const double simTime = 175; //s 
// const double simTime = 3; //s (testing version)

const T movementStart = 80.0; //s
// const T movementStart = 1.0; //s (testing version)
const T movementTimeOffset = -20.0; //s
const T sampleStart = 99.0; //s 
// const T sampleStart = 2.0; //s (testing version)
const T sampleInterval = 0.04; //s
T nextSampleTime = sampleStart;


size_t globalUpdateCheckPointsSize = 0;

std::map<T,std::array<T,3>> shipMotionData;
const std::string shipMotionFilename = "case9_realisticShipMotion.csv";

/** Read file into string. */
// https://stackoverflow.com/questions/18816126/c-read-the-whole-file-in-buffer
inline std::string slurp (const std::string& path) {
  std::ostringstream buf; 
  std::ifstream input (path.c_str()); 
  buf << input.rdbuf(); 
  return buf.str();
}

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2];
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  static IniEquilibriumDynamics<T, Lattice> inletDynamics;

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025 * 500);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXLeftBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &impedanceOut);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder + 1, iXRightBorder - 1, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZFrontBorder, iXRightBorder - 1, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZBackBorder, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder, iXRightBorder - 1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder, iXRightBorder - 1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZBackBorder, iXRightBorder - 1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZBackBorder, iXRightBorder - 1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZBackBorder, iXLeftBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder, iXLeftBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZBackBorder, iXRightBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder, iXRightBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, iXRightBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder + 1, iXLeftBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder + 1, iXLeftBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder + 1, iXRightBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndex;
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, smagoConst * 500);
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics2(omega, smagoConst * 10);

  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 10, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 50, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 11, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics2);

  //ship floor region
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder*0.26, iYTopBorder*0.5-resolution*0.085, 0, iXRightBorder*0.49, iYTopBorder*0.5+resolution*0.085, 0, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());
}


template <typename T, template <typename> class Lattice>
void identifyGlobalCheckNeighborhood(SubDomainInformation<T,Lattice<T>>& refSubDomain, std::vector<size_t>& globalGradPoints, std::vector<size_t>& globalUpdateCheckPoints) {

  std::cout << "identifyGlobalCheckNeighborhood()" << std::endl;
  globalUpdateCheckPoints.clear();

  const int neighborRadius = 2;
  int neighborIndex = 1;
  for (size_t outsideNeighbor : globalGradPoints) {

    // if (neighborIndex % 10000 == 0)
    //   std::cout << "Neighbor " << neighborIndex << " of " << globalGradPoints.size() << std::endl;
    neighborIndex++;

    size_t outsideNeighborIndices[3];
    util::getCellIndices3D(outsideNeighbor, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], outsideNeighborIndices);
    int outsideNeighborIndicesSigned[3] = {outsideNeighborIndices[0], outsideNeighborIndices[1], outsideNeighborIndices[2]};

    for (int iX = max(outsideNeighborIndicesSigned[0]-neighborRadius, 0); iX <= outsideNeighborIndicesSigned[0]+neighborRadius; iX++)
      for (int iY = max(outsideNeighborIndicesSigned[1]-neighborRadius, 0); iY <= outsideNeighborIndicesSigned[1]+neighborRadius; iY++)
        for (int iZ = max(outsideNeighborIndicesSigned[2]-neighborRadius, 1); iZ <= outsideNeighborIndicesSigned[2]+neighborRadius; iZ++) //ignore the ground plane in Z
        {
          size_t checkPointGlobalIndex = util::getCellIndex3D(iX ,iY, iZ, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2]);
          globalUpdateCheckPoints.push_back(checkPointGlobalIndex);
        }

  }

  std::sort(globalUpdateCheckPoints.begin(), globalUpdateCheckPoints.end());
  auto it = std::unique(globalUpdateCheckPoints.begin(), globalUpdateCheckPoints.end());
  globalUpdateCheckPoints.erase(it, globalUpdateCheckPoints.end());
  std::cout << "end identifyGlobalCheckNeighborhood()" << std::endl;
}

template <typename T, template <typename> class Lattice>
void updateGlobalCheckNeighborhood(SubDomainInformation<T,Lattice<T>>& refSubDomain, std::vector<size_t>& previousGlobalGradPoints, std::vector<size_t>& currentGlobalGradPoints, std::vector<size_t>& globalUpdateCheckPoints) {
  std::cout << "updateGlobalCheckNeighborhood()" << std::endl;
  const int neighborRadius = 2;

  auto startFirstSetOp = std::chrono::steady_clock::now();
  std::vector<size_t> globalFormerGradPoints;
  std::vector<size_t> globalNewGradPoints;
  std::set_difference(previousGlobalGradPoints.begin(), previousGlobalGradPoints.end(), currentGlobalGradPoints.begin(), currentGlobalGradPoints.end(), std::back_inserter(globalFormerGradPoints));
  std::set_difference(currentGlobalGradPoints.begin(), currentGlobalGradPoints.end(), previousGlobalGradPoints.begin(), previousGlobalGradPoints.end(), std::back_inserter(globalNewGradPoints));
  auto endFirstSetOp = std::chrono::steady_clock::now();
  std::cout << "First set op elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endFirstSetOp - startFirstSetOp).count() << std::endl;

  std::cout << "updateGlobalCheckNeighborhood() formerPoints " << globalFormerGradPoints.size() << ", newPoints " << globalNewGradPoints.size()  << std::endl;

  std::vector<size_t> pointsToRemove;
  std::vector<size_t> pointsToAdd;

  auto startFormer = std::chrono::steady_clock::now();
  for (size_t outsideNeighbor : globalFormerGradPoints) {
    size_t outsideNeighborIndices[3];
    util::getCellIndices3D(outsideNeighbor, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], outsideNeighborIndices);
    int outsideNeighborIndicesSigned[3] = {outsideNeighborIndices[0], outsideNeighborIndices[1], outsideNeighborIndices[2]};

    for (int iX = max(outsideNeighborIndicesSigned[0]-neighborRadius, 0); iX <= outsideNeighborIndicesSigned[0]+neighborRadius; iX++)
      for (int iY = max(outsideNeighborIndicesSigned[1]-neighborRadius, 0); iY <= outsideNeighborIndicesSigned[1]+neighborRadius; iY++)
        for (int iZ = max(outsideNeighborIndicesSigned[2]-neighborRadius, 1); iZ <= outsideNeighborIndicesSigned[2]+neighborRadius; iZ++) //ignore the ground plane in Z
        {
          size_t checkPointGlobalIndex = util::getCellIndex3D(iX ,iY, iZ, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2]);
          pointsToRemove.push_back(checkPointGlobalIndex);
        }
  }
  auto endFormer = std::chrono::steady_clock::now();
  std::cout << "Former determination elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endFormer - startFormer).count() << std::endl;

  auto startNew = std::chrono::steady_clock::now();
  for (size_t outsideNeighbor : globalNewGradPoints) {

    size_t outsideNeighborIndices[3];
    util::getCellIndices3D(outsideNeighbor, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], outsideNeighborIndices);
    int outsideNeighborIndicesSigned[3] = {outsideNeighborIndices[0], outsideNeighborIndices[1], outsideNeighborIndices[2]};

    for (int iX = max(outsideNeighborIndicesSigned[0]-neighborRadius, 0); iX <= outsideNeighborIndicesSigned[0]+neighborRadius; iX++)
      for (int iY = max(outsideNeighborIndicesSigned[1]-neighborRadius, 0); iY <= outsideNeighborIndicesSigned[1]+neighborRadius; iY++)
        for (int iZ = max(outsideNeighborIndicesSigned[2]-neighborRadius, 1); iZ <= outsideNeighborIndicesSigned[2]+neighborRadius; iZ++) //ignore the ground plane in Z
        {
          size_t checkPointGlobalIndex = util::getCellIndex3D(iX ,iY, iZ, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2]);
          pointsToAdd.push_back(checkPointGlobalIndex);
        }

  }
  auto endNew = std::chrono::steady_clock::now();
  std::cout << "New determination elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endNew - startNew).count() << std::endl;


  auto startSecondSetOp = std::chrono::steady_clock::now();

  auto startSecondSetOpP1 = std::chrono::steady_clock::now();
  // std::sort(pointsToRemove.begin(), pointsToRemove.end());
  // auto it = std::unique(pointsToRemove.begin(), pointsToRemove.end());
  // pointsToRemove.erase(it, pointsToRemove.end());
  auto endSecondSetOpP1 = std::chrono::steady_clock::now();

  auto startSecondSetOpP2 = std::chrono::steady_clock::now();
  std::sort(pointsToAdd.begin(), pointsToAdd.end());
  auto it = std::unique(pointsToAdd.begin(), pointsToAdd.end());
  pointsToAdd.erase(it, pointsToAdd.end());
  auto endSecondSetOpP2 = std::chrono::steady_clock::now();

  auto startSecondSetOpP3 = std::chrono::steady_clock::now();
  // std::vector<size_t> temp;
  // std::set_difference(globalUpdateCheckPoints.begin(), globalUpdateCheckPoints.end(), pointsToRemove.begin(), pointsToRemove.end(), std::back_inserter(temp));
  auto endSecondSetOpP3 = std::chrono::steady_clock::now();

  auto startSecondSetOpP4 = std::chrono::steady_clock::now();
  std::vector<size_t> temp = globalUpdateCheckPoints;
  globalUpdateCheckPoints.clear();
  std::set_union(temp.begin(), temp.end(), pointsToAdd.begin(), pointsToAdd.end(), std::back_inserter(globalUpdateCheckPoints));
  auto endSecondSetOpP4 = std::chrono::steady_clock::now();
  auto endSecondSetOp = std::chrono::steady_clock::now();
  std::cout << "Second set op elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSecondSetOp - startSecondSetOp).count() <<
            ", P1=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSecondSetOpP1 - startSecondSetOpP1).count() <<
            ", P2=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSecondSetOpP2 - startSecondSetOpP2).count() <<
            ", P3=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSecondSetOpP3 - startSecondSetOpP3).count() <<
            ", P4=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSecondSetOpP4 - startSecondSetOpP4).count() << std::endl;


  // std::sort(globalUpdateCheckPoints.begin(), globalUpdateCheckPoints.end());
  // auto it = std::unique(globalUpdateCheckPoints.begin(), globalUpdateCheckPoints.end());
  // globalUpdateCheckPoints.erase(it, globalUpdateCheckPoints.end());
  std::cout << "end updateGlobalCheckNeighborhood()" << std::endl;
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlLocationCalc(UnitConverter<T, Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, stlT pitch, stlT roll, stlT heave, size_t indices[3], stlT location[3]) {

  Vec3<stlT> point((stlT)indices[0], (stlT)indices[1], (stlT)indices[2]);

  Vec3<stlT> offsetVec(-baseOffsetX, -baseOffsetY, -baseOffsetZ - heave);
  Matrix<stlT,3,3> rotMat(roll, pitch, 0.0, Rotation::ToBody{});
  AffineTransform<stlT,3> affineTransform(rotMat);
  affineTransform.applyToRight(offsetVec);

  transformInPlace<stlT> (point, affineTransform);
  location[0] = point(0)*converter.getConversionFactorLength();
  location[1] = point(1)*converter.getConversionFactorLength();
  location[2] = point(2)*converter.getConversionFactorLength();
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlUpdate(STLreader<stlT>& stlReader, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, Vec3<T> shipVelocity, stlT pitch, stlT roll, stlT heave, Vec3<T> shipAngularVelocity, std::vector<size_t>& globalCheckPoints, std::vector<size_t>& globalBBPoints, std::vector<size_t>& globalGradPoints, SubDomainInformation<T,Lattice<T>>& localSubDomain, SubDomainInformation<T,Lattice<T>>& refSubDomain, std::vector<size_t>& localFormerBBPoints, std::vector<size_t>& localGradPoints, std::vector<std::array<T,1+3+1+3+3+Lattice<T>::q>>& localGradData) {

  std::cout << "Start stlUpdate" << std::endl;
  auto startSTL = std::chrono::steady_clock::now();
  auto startParallelCheck = std::chrono::steady_clock::now();
  localFormerBBPoints.clear();

  std::vector<size_t> checkPointsInside;
  std::vector<size_t> checkPointsOutside;

  std::vector<bool> parallelCheck(globalCheckPoints.size());

  #pragma omp parallel for
  for (int i = 0; i < globalCheckPoints.size(); i++) {
    size_t currentGlobalCheckPoint = globalCheckPoints.at(i);
    size_t indices[3];
    util::getCellIndices3D(currentGlobalCheckPoint, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], indices);
    // stlT iXPhys = converter.getPhysLength((stlT)indices[0]-baseOffsetX);
    // stlT iYPhys = converter.getPhysLength((stlT)indices[1]-baseOffsetY);
    // stlT iZPhys = converter.getPhysLength((stlT)indices[2]-baseOffsetZ);
    // stlT location[3] = {iXPhys, iYPhys, iZPhys};
    stlT location[3];
    stlLocationCalc(converter, baseOffsetX, baseOffsetY, baseOffsetZ, pitch, roll, heave, indices, location);

    bool inside[1];
    stlReader(inside, location);

    if (inside[0])
      parallelCheck.at(i) = true;
  }

  for (int i = 0; i < parallelCheck.size(); i++) {
    if (parallelCheck.at(i))
      checkPointsInside.push_back(globalCheckPoints.at(i));
    else
      checkPointsOutside.push_back(globalCheckPoints.at(i));
  }

  
  auto endParallelCheck = std::chrono::steady_clock::now();
  std::cout << "Finished first parallel loop, elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endParallelCheck - startParallelCheck).count() << std::endl;

  auto startSetOps = std::chrono::steady_clock::now();
  std::vector<size_t> globalFormerBBPoints;
  std::vector<size_t> globalNewBBPoints;
  std::set_intersection(globalBBPoints.begin(), globalBBPoints.end(), checkPointsOutside.begin(), checkPointsOutside.end(), std::back_inserter(globalFormerBBPoints));
  std::set_difference(checkPointsInside.begin(), checkPointsInside.end(), globalBBPoints.begin(), globalBBPoints.end(), std::back_inserter(globalNewBBPoints));
  auto endSetOps = std::chrono::steady_clock::now();

  std::cout << "Finished set diffs, formerBB: " << globalFormerBBPoints.size() << ", newBB: " << globalNewBBPoints.size() << ", elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSetOps - startSetOps).count() << std::endl;

  if (globalNewBBPoints.size() > 0 || globalFormerBBPoints.size() > 0 || true) {

    auto startNewCellProcessing = std::chrono::steady_clock::now();
    std::cout << "Start new cell processing, size of globalBBPoints: " << globalBBPoints.size() << std::endl;
    auto ib = std::begin(globalFormerBBPoints);
    auto iter = std::remove_if (
       std::begin(globalBBPoints), std::end(globalBBPoints),
       [&ib, &globalFormerBBPoints](int x) -> bool {
                       while  (ib != std::end(globalFormerBBPoints) && *ib < x) ++ib;
                       return (ib != std::end(globalFormerBBPoints) && *ib == x);
                     });

    // std::cout << "Done with remove if" << std::endl;
        
    globalBBPoints.erase(iter, globalBBPoints.end());

    globalBBPoints.insert(globalBBPoints.end(), globalNewBBPoints.begin(), globalNewBBPoints.end());
    // std::cout << "Done with erase and insert" << std::endl;
    std::sort(globalBBPoints.begin(), globalBBPoints.end());

    auto endNewCellProcessing = std::chrono::steady_clock::now();
    std::cout << "End new cell processing, size of globalBBPoints: " << globalBBPoints.size() << ", elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endNewCellProcessing - startNewCellProcessing).count() << std::endl;


    std::vector<size_t> currentGlobalGradPoints;

    std::cout << "Start grad determination" << std::endl;
    auto startGradDetermination = std::chrono::steady_clock::now();
    #pragma omp parallel 
    {
      std::vector<size_t> threadLocalGlobalGradPoints;
      #pragma omp for nowait
      for (int i = 0; i < checkPointsOutside.size(); i++) {
        size_t currentGlobalCheckPoint = checkPointsOutside.at(i);
        size_t indices[3];
        util::getCellIndices3D(currentGlobalCheckPoint, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], indices);
        bool isGrad = false;
        bool isGradAlt = false;

        for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
          /*
          size_t neighborIndices[3] = {indices[0] + Lattice<T>::c(iPop, 0), indices[1] + Lattice<T>::c(iPop, 1), indices[2] + Lattice<T>::c(iPop, 2)};
          // stlT iXPhysN = converter.getPhysLength((stlT)neighborIndices[0]-baseOffsetX);
          // stlT iYPhysN = converter.getPhysLength((stlT)neighborIndices[1]-baseOffsetY);
          // stlT iZPhysN = converter.getPhysLength((stlT)neighborIndices[2]-baseOffsetZ);
          // stlT neighborLocation[3] = {iXPhysN, iYPhysN, iZPhysN};
          stlT neighborLocation[3];
          stlLocationCalc(converter, baseOffsetX, baseOffsetY, baseOffsetZ, pitch, roll, heave, neighborIndices, neighborLocation);

          bool neighborInside[1];
          stlReader(neighborInside, neighborLocation);
          if (neighborInside[0]) {
            isGradAlt = true;
            // break;
          }
          */
          
          //Alternate approach that hopefully runs faster
          
          size_t neighborIndices[3] = {indices[0] + Lattice<T>::c(iPop, 0), indices[1] + Lattice<T>::c(iPop, 1), indices[2] + Lattice<T>::c(iPop, 2)};
          size_t neighborIndex = util::getCellIndex3D(neighborIndices[0], neighborIndices[1], neighborIndices[2], refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2]);
          auto lower = std::lower_bound(checkPointsInside.begin(), checkPointsInside.end(), neighborIndex);
          const bool found = lower != checkPointsInside.end() && *lower == neighborIndex;
          if (found) {
            isGrad = true;
            break;
          }
        }

        if (isGrad)
          threadLocalGlobalGradPoints.push_back(currentGlobalCheckPoint);
      }

      #pragma omp critical
      {
        currentGlobalGradPoints.insert(currentGlobalGradPoints.end(), threadLocalGlobalGradPoints.begin(), threadLocalGlobalGradPoints.end());
      }
    }

    std::sort(currentGlobalGradPoints.begin(), currentGlobalGradPoints.end());

    auto endGradDetermination = std::chrono::steady_clock::now();
    std::cout << "done with gradDetermination elapsed(ms)=" << std::chrono::duration_cast<std::chrono::milliseconds>(endGradDetermination - startGradDetermination).count() << std::endl;

    auto startUpdateGlobalCheck = std::chrono::steady_clock::now();
    if (globalCheckPoints.size() > 2*globalUpdateCheckPointsSize) {
      identifyGlobalCheckNeighborhood(refSubDomain, currentGlobalGradPoints, globalCheckPoints);
      globalUpdateCheckPointsSize = globalCheckPoints.size();
      std::cout << "PRUNING check points!" << std::endl;
    }
    else
      updateGlobalCheckNeighborhood(refSubDomain, globalGradPoints, currentGlobalGradPoints, globalCheckPoints);

    globalGradPoints = currentGlobalGradPoints;
    
    auto endUpdateGlobalCheck = std::chrono::steady_clock::now();
    std::cout << "updateGlobalCheck elapsed(ms)=" << std::chrono::duration_cast<std::chrono::milliseconds>(endUpdateGlobalCheck - startUpdateGlobalCheck).count() << std::endl;

    auto startDetermineLocal = std::chrono::steady_clock::now();
    for (int i = 0; i < globalFormerBBPoints.size(); i++) {
      size_t currentGlobalFormerBBPoint = globalFormerBBPoints.at(i);
      size_t indices[3];
      util::getCellIndices3D(currentGlobalFormerBBPoint, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], indices);
      Index3D localIndex;
      if (localSubDomain.isLocal(indices[0], indices[1], indices[2], localIndex)) {
        size_t localFormerBBIndex = util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2]);
        localFormerBBPoints.push_back(localFormerBBIndex);
      }
    }

    std::sort(localFormerBBPoints.begin(), localFormerBBPoints.end());

    localGradPoints.clear();
    for (int i = 0; i < globalGradPoints.size(); i++) {
      size_t currentGlobalGradPoint = globalGradPoints.at(i);
      size_t indices[3];
      util::getCellIndices3D(currentGlobalGradPoint, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], indices);
      Index3D localIndex;
      if (localSubDomain.isLocal(indices[0], indices[1], indices[2], localIndex)) {
        size_t localGradIndex = util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2]);
        localGradPoints.push_back(localGradIndex);
      }
    }

    std::sort(localGradPoints.begin(), localGradPoints.end());

    localGradData.resize(localGradPoints.size());
    auto endDetermineLocal = std::chrono::steady_clock::now();
    std::cout << "End determine local elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endDetermineLocal - startDetermineLocal).count() << std::endl;
  } 

  auto startGradData = std::chrono::steady_clock::now();
  stlT origin[3] = {baseOffsetX, baseOffsetY, baseOffsetZ+heave};
  #pragma omp parallel for
  for (int i = 0; i < localGradPoints.size(); i++) {
    size_t gradLocalIndex = localGradPoints.at(i);
    size_t indices[3];
    util::getCellIndices3D(gradLocalIndex, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], indices);
    Index3D localIndex(indices[0], indices[1], indices[2], localSubDomain.localSubDomain);
    Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);
    size_t globalIndices[3] = {globalIndex[0], globalIndex[1], globalIndex[2]};

    // stlT iXPhys = converter.getPhysLength((stlT)globalIndex[0]-baseOffsetX);
    // stlT iYPhys = converter.getPhysLength((stlT)globalIndex[1]-baseOffsetY);
    // stlT iZPhys = converter.getPhysLength((stlT)globalIndex[2]-baseOffsetZ);
    stlT location[3];
    stlLocationCalc(converter, baseOffsetX, baseOffsetY, baseOffsetZ, pitch, roll, heave, globalIndices, location);    

    std::array<T, 1+3+1+3+3+Lattice<T>::q> gradData;

    gradData[0] = converter.getLatticeRelaxationFrequency();
    gradData[1] = 0.0; // surface normal not needed;
    gradData[2] = 0.0; // surface normal not needed;
    gradData[3] = 0.0; // surface normal not needed;
    gradData[4] = 0.0; // surface normal not needed;

    Vec3<T> rotationArm(location[0]-origin[0], location[1]-origin[1], location[2]-origin[2]);
    rotationArm *= 1.0/converter.getConversionFactorLength();

    Vec3<T> angularVelocityComponent(shipAngularVelocity(1)*rotationArm(2)-shipAngularVelocity(2)*rotationArm(1), -shipAngularVelocity(0)*rotationArm(2)+shipAngularVelocity(2)*rotationArm(0), shipAngularVelocity(0)*rotationArm(1)-shipAngularVelocity(1)*rotationArm(0));

    gradData[8] = shipVelocity(0) + angularVelocityComponent(0);
    gradData[9] = shipVelocity(1) + angularVelocityComponent(0);
    gradData[10] = shipVelocity(2) + angularVelocityComponent(0);

    gradData[11] = -1.0; // zero population

    bool xNegInside = false, xPosInside = false, yNegInside = false, yPosInside = false, zNegInside = false, zPosInside = false;
    for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
      size_t neighborIndices[3] = {globalIndex[0] + Lattice<T>::c(iPop, 0), globalIndex[1] + Lattice<T>::c(iPop, 1), globalIndex[2] + Lattice<T>::c(iPop, 2)};
      // stlT iXPhysN = converter.getPhysLength((stlT)neighborIndices[0]-baseOffsetX);
      // stlT iYPhysN = converter.getPhysLength((stlT)neighborIndices[1]-baseOffsetY);
      // stlT iZPhysN = converter.getPhysLength((stlT)neighborIndices[2]-baseOffsetZ);
      // stlT nLocation[3] = {iXPhysN, iYPhysN, iZPhysN};
      stlT nLocation[3];
      stlLocationCalc(converter, baseOffsetX, baseOffsetY, baseOffsetZ, pitch, roll, heave, neighborIndices, nLocation);    

      bool neighborInside[1];
      stlReader(neighborInside, nLocation);

      if (neighborInside[0]) {
        switch (iPop) { //reference latticeDescriptors.h for the directions, it's just hardcoded here
          case 1:
            xNegInside = true;
            break;
          case 10:
            xPosInside = true;
            break;
          case 2:
            yNegInside = true;
            break;
          case 11:
            yPosInside = true;
            break;
          case 3:
            zNegInside = true;
            break;
          case 12:
            zPosInside = true;
            break;
          default:
            break;
        }

        bool useBisection = false;
        stlT distance = 0.5;
        Vector<stlT,3> directionVector(nLocation[0]-location[0], nLocation[1]-location[1], nLocation[2]-location[2]);
        bool success = stlReader.distance(distance, Vector<stlT, 3>(location[0], location[1], location[2]), directionVector);
        distance = (distance/converter.getConversionFactorLength()) / sqrt(Lattice<T>::c(iPop,0)*Lattice<T>::c(iPop,0) + Lattice<T>::c(iPop,1)*Lattice<T>::c(iPop,1) + Lattice<T>::c(iPop,2)*Lattice<T>::c(iPop,2));
        if (!success || distance > 1.0 || distance < 0.0) {
          // std::cout << "Major problem calculating distance on pt " << indices[0] << " " << indices[1] << " " << indices[2] << ": distance is " << distance << "; using bisection method \n";
          // distance = (distance > 1.0 && distance < 1.2) ? 0.95 : 0.5;
          useBisection  = true;
        }
        //bisection approach
        if (useBisection) {
          stlT lowerLimit[3] = {location[0], location[1], location[2]};
          stlT upperLimit[3] = {nLocation[0], nLocation[1], nLocation[2]};
          bool averageInside[1];
          const int nBisections = 12;
          for (int nB = 0; nB < nBisections; nB++) {
            stlT averagePt[3] = {(lowerLimit[0]+upperLimit[0])/2.0, (lowerLimit[1]+upperLimit[1])/2.0, (lowerLimit[2]+upperLimit[2])/2.0};
            stlReader(averageInside, averagePt);
            if (averageInside[0]) {
              upperLimit[0] = averagePt[0];
              upperLimit[1] = averagePt[1];
              upperLimit[2] = averagePt[2];
            }
            else {
              lowerLimit[0] = averagePt[0];
              lowerLimit[1] = averagePt[1];
              lowerLimit[2] = averagePt[2];
            }
          }
          stlT distVector[3] = {lowerLimit[0]-location[0], lowerLimit[1]-location[1], lowerLimit[2]-location[2]};
          distance = sqrt(distVector[0]*distVector[0] + distVector[1]*distVector[1] + distVector[2]*distVector[2]);
          distance = (distance/converter.getConversionFactorLength()) / sqrt(Lattice<T>::c(iPop,0)*Lattice<T>::c(iPop,0) + Lattice<T>::c(iPop,1)*Lattice<T>::c(iPop,1) + Lattice<T>::c(iPop,2)*Lattice<T>::c(iPop,2));
        }

        if (distance > 1.0 || distance < 0.0) {
          distance = 0.5;
        }
        gradData[11+Lattice<T>::opposite(iPop)] = distance;

      }
      else {
        gradData[11+Lattice<T>::opposite(iPop)] = -1.0;
      }
    } //end iPop loop
    if (xNegInside && xPosInside) //bad!
      gradData[5] = 0.0;
    else if (xNegInside)
      gradData[5] = 1.0;
    else if (xPosInside)
      gradData[5] = -1.0;
    else
      gradData[5] = 1.0;

    if (yNegInside && yPosInside) //bad!
      gradData[6] = 0.0;
    else if (yNegInside)
      gradData[6] = 1.0;
    else if (yPosInside)
      gradData[6] = -1.0;
    else
      gradData[6] = 1.0;

    if (zNegInside && zPosInside) //bad!
      gradData[7] = 0.0;
    else if (zNegInside)
      gradData[7] = 1.0;
    else if (zPosInside)
      gradData[7] = -1.0;
    else
      gradData[7] = 1.0;

    localGradData.at(i) = gradData;
  }
  auto endGradData = std::chrono::steady_clock::now();
  std::cout << "End grad data elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endGradData - startGradData).count() << std::endl;
  auto endSTL = std::chrono::steady_clock::now();
  std::cout << "End grad processing and stl update, total elapsed=" << std::chrono::duration_cast<std::chrono::milliseconds>(endSTL - startSTL).count() << std::endl;

}

template <typename T, typename stlT, template <typename> class Lattice>
void stlViz(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, std::string outputName, stlT pitch, stlT roll, stlT heave, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength();
      yCoord = yCoord/converter.getConversionFactorLength();
      zCoord = zCoord/converter.getConversionFactorLength();

      Vec3<T> coordVec(xCoord, yCoord, zCoord);
      Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
      AffineTransform<T,3> affineTransform(rotMat);
      Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
      affineTransform.applyToLeft(offsetVec);
      transformInPlace<T>(coordVec, affineTransform);

      outputSTL << processedString << coordVec(0) << " " << coordVec(1) << " " << coordVec(2) << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }


  outputSTL.close();
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlVizExtractionBox(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, std::string outputName, stlT pitch, stlT roll, stlT heave, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength();
      yCoord = yCoord/converter.getConversionFactorLength();
      zCoord = zCoord/converter.getConversionFactorLength();

      Vec3<T> coordVec(xCoord, yCoord, zCoord);
      Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
      AffineTransform<T,3> affineTransform(rotMat);
      Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
      affineTransform.applyToLeft(offsetVec);
      transformInPlace<T>(coordVec, affineTransform);

      coordVec *= converter.getConversionFactorLength();

      outputSTL << processedString << coordVec(0) << " " << coordVec(1) << " " << coordVec(2) << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }


  outputSTL.close();
}

template <typename T, template <typename> class Lattice>
__global__ void formerBBReinitialize(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t * const OPENLB_RESTRICT formerBBPoints, Vec3<T> shipVelocity, Vec3<T> shipAngularVelocity, SubDomainInformation<T,Lattice<T>> localSubDomain, T offsetX, T offsetY, T offsetZ, size_t length) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  size_t indices[3];
  util::getCellIndices3D(formerBBPoints[threadIndex], localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], indices);
  Index3D localIndex(indices[0], indices[1], indices[2], localSubDomain.localSubDomain);
  Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);
  Vec3<T> rotationArm((T)globalIndex[0]-offsetX, (T)globalIndex[1]-offsetY, (T)globalIndex[2]-offsetZ);
  Vec3<T> angularVelocityComponent(shipAngularVelocity(1)*rotationArm(2)-shipAngularVelocity(2)*rotationArm(1), -shipAngularVelocity(0)*rotationArm(2)+shipAngularVelocity(2)*rotationArm(0), shipAngularVelocity(0)*rotationArm(1)-shipAngularVelocity(1)*rotationArm(0));

  T rho = 1.0;
  T u[3] = {shipVelocity(0)+angularVelocityComponent(0), shipVelocity(1)+angularVelocityComponent(1), shipVelocity(2)+angularVelocityComponent(2)};
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    cellData[iPop][formerBBPoints[threadIndex]] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
  }
  
  cellData[Lattice<T>::rhoIndex][formerBBPoints[threadIndex]] = rho;
  for (int iD = 0; iD < Lattice<T>::d; iD++)
    cellData[Lattice<T>::uIndex+iD][formerBBPoints[threadIndex]] = u[iD];
}

template <typename T, template <typename> class Lattice>
stlT getShipHeavePosition(T time, UnitConverter<T,Lattice> converter) {

  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T heave1 = it1->second.at(0);
  T heave2 = it2->second.at(0);

  T interpFraction = (time-time1)/(time2-time1);

  T heave = heave1 + interpFraction*(heave2-heave1); //m
  return heave/(stlT)converter.getConversionFactorLength(); //lattice length units
}

template <typename T, template <typename> class Lattice>
T getShipHeaveVelocity(T time, UnitConverter<T,Lattice> converter) {

  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T heave1 = it1->second.at(0);
  T heave2 = it2->second.at(0);

  T heaveRate = (heave2-heave1)/(time2-time1); //m/s
  return converter.getLatticeVelocity(heaveRate);  //lattice vel units
}

template <typename T, template <typename> class Lattice>
stlT getShipPitchAngle(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T pitch1 = it1->second.at(2);
  T pitch2 = it2->second.at(2);

  T interpFraction = (time-time1)/(time2-time1);

  T pitch = pitch1 + interpFraction*(pitch2-pitch1); //rad
  return pitch; //rad
}

template <typename T, template <typename> class Lattice>
T getShipPitchRate(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T pitch1 = it1->second.at(2);
  T pitch2 = it2->second.at(2);

  T pitchRate = (pitch2-pitch1)/(time2-time1); //rad/s
  return pitchRate*converter.getConversionFactorTime(); //converts to rad/timestep
}

template <typename T, template <typename> class Lattice>
stlT getShipRollAngle(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T roll1 = it1->second.at(1);
  T roll2 = it2->second.at(1);

  T interpFraction = (time-time1)/(time2-time1);

  T roll = roll1 + interpFraction*(roll2-roll1); //rad
  return roll; //rad
}

template <typename T, template <typename> class Lattice>
T getShipRollRate(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T roll1 = it1->second.at(1);
  T roll2 = it2->second.at(1);

  T rollRate = (roll2-roll1)/(time2-time1); //rad/s
  return rollRate*converter.getConversionFactorTime(); //converts to rad/timestep
}

template <typename T, template <typename> class Lattice>
Vec3<T> getShipAngularVelocity(T time, UnitConverter<T,Lattice> converter) {
  T pitch = (T) getShipPitchAngle(time, converter);

  T pitchRate = getShipPitchRate(time, converter);
  T rollRate = getShipRollRate(time, converter);

  Vec3<T> pitchDirection(0.0, 1.0, 0.0);
  Vec3<T> rollDirection(cos(pitch), 0.0, -sin(pitch));

  pitchDirection *= pitchRate;
  rollDirection *= rollRate;

  Vec3<T> angularVelocity(0.0,0.0,0.0);
  angularVelocity(0) = pitchDirection(0) + rollDirection(0);
  angularVelocity(1) = pitchDirection(1) + rollDirection(1);
  angularVelocity(2) = pitchDirection(2) + rollDirection(2);
  return angularVelocity;
}



void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, desiredLatticeVel, shipLength, desiredFreestream, kinematicViscosity, physDensity, 0);
  converter.print();

  const int iXLeftBorder = 0;
  const int iXRightBorder = 4.5*resolution-1;
  const int iYBottomBorder = 0;
  const int iYTopBorder = 1.5*resolution-1;
  const int iZFrontBorder = 0;
  const int iZBackBorder = 1.0*resolution-1;

  std::cout << "Loading ship motion data..." << std::endl;
  std::ifstream shipMotionFile(shipMotionFilename);
  std::string line;
  std::getline(shipMotionFile, line); //read the headerline
  while (std::getline(shipMotionFile, line)) {
    std::istringstream iss(line);
    double t,b,c,d;
    std::string token;
    std::getline(iss, token, ',');
    t = std::stod(token);
    std::getline(iss, token, ',');
    b = std::stod(token);
    std::getline(iss, token, ',');
    c = std::stod(token);
    std::getline(iss, token, ',');
    d = std::stod(token);
    std::array<T,3> currentLineData = {b,c,d};
    shipMotionData.insert({t, currentLineData});
  }

  // Test read ship motion data
  // T testTime = 13.0;
  // std::cout << "Quantities at t = " << testTime << std::endl;
  // std::cout << "Heave: " << getShipHeavePosition(testTime, converter) << std::endl;
  // std::cout << "Roll: " << getShipRollAngle(testTime, converter) << std::endl;
  // std::cout << "Pitch: " << getShipPitchAngle(testTime, converter) << std::endl;

  // std::cout << "Heave rate: " << getShipHeaveVelocity(testTime, converter) << std::endl;
  // std::cout << "Roll rate: " << getShipRollRate(testTime, converter) << std::endl;
  // std::cout << "Pitch rate:  " << getShipPitchRate(testTime, converter) << std::endl;

  // std::cout << "Loaded ship motion data." << std::endl;

  T omega = converter.getLatticeRelaxationFrequency();
  
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  
  RefinedGrid3D<T,Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
  
  unsigned ghostLayer[3] = {2,0,0};
  lattice.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

  std::cout << "Set up refinement..." << std::endl;
  lattice.setupRefinementGPU();
  lattice.setupMultilatticeGPU();

  std::cout << "Define boundaries.... "  << std::endl;
  lattice.applyMasks();
  defineBoundaries(*(lattice.getLatticePointer()), bulkDynamics, lattice._localSubDomain, lattice._refSubDomain);

  std::cout << "Initial STL read..." << std::endl;

  const stlT shipOffsetX = (228.15+25)/converter.getConversionFactorLength();
  const stlT shipOffsetY = 0.75*(T)resolution;
  const stlT shipOffsetZ = (2.1)/converter.getConversionFactorLength();

  std::string stlAsString = slurp("NATOGD_fullscale.stl");

  static PostProcessingDynamics<T,Lattice,GradBoundaryProcessor3D<T,Lattice>> gradBoundary;
  lattice.getLatticePointer()->dynamicsContainer.definePlaceholderDynamics(&gradBoundary);
  
  STLreader<stlT> stlReader("NATOGD_fullscale.stl", shipLength/800.0, 1.0);
  stlReader.print();

  std::vector<size_t> globalGradPoints; //global indices of outside neighbors

  std::vector<size_t> globalUpdateCheckPoints;
  std::vector<size_t> globalBBPoints;
  
  //identify initial bounceback and grad points in global domain
  for (int iX = 0; iX <= iXRightBorder; iX++) {
    for (int iY = resolution*1.5*0.25; iY <= resolution*1.5*0.75; iY++)
      for (int iZ = 0; iZ <= resolution*0.5; iZ++) {
        stlT iXPhys = ((stlT)iX-shipOffsetX)*converter.getConversionFactorLength();
        stlT iYPhys = ((stlT)iY-shipOffsetY)*converter.getConversionFactorLength();
        stlT iZPhys = ((stlT)iZ-shipOffsetZ)*converter.getConversionFactorLength();
        stlT location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];
        stlReader(isInside, location);
        if (isInside[0]) {
          if (iZ != 0) {
            size_t globalCellIndex = util::getCellIndex3D(iX, iY, iZ, lattice._refSubDomain.localGridSize()[1], lattice._refSubDomain.localGridSize()[2]);
            globalBBPoints.push_back(globalCellIndex);
          }
          
          bool allNeighborsInside = true;
          std::vector<int> neighborsNotInside;
          for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
            stlT iXNeighborPhys = ((stlT)iX-shipOffsetX+(stlT)Lattice<T>::c(iPop, 0))*converter.getConversionFactorLength();
            stlT iYNeighborPhys = ((stlT)iY-shipOffsetY+(stlT)Lattice<T>::c(iPop, 1))*converter.getConversionFactorLength();
            stlT iZNeighborPhys = ((stlT)iZ-shipOffsetZ+(stlT)Lattice<T>::c(iPop, 2))*converter.getConversionFactorLength();
            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
            bool neighborInside[1];
            stlReader(neighborInside, neighborLocation);
            allNeighborsInside = allNeighborsInside && neighborInside[0];
            if (!neighborInside[0])
              neighborsNotInside.push_back(iPop);
          }

          if (!allNeighborsInside) {

            for (int outsideNeighbor : neighborsNotInside) {
              if (iZ + Lattice<T>::c(outsideNeighbor, 2) <= 0)
                continue;
              size_t outsideNeighborCellIndex = util::getCellIndex3D(iX + Lattice<T>::c(outsideNeighbor, 0), iY + Lattice<T>::c(outsideNeighbor, 1), iZ + Lattice<T>::c(outsideNeighbor, 2), lattice._refSubDomain.localGridSize()[1], lattice._refSubDomain.localGridSize()[2]);
              globalGradPoints.push_back(outsideNeighborCellIndex);
            }
          }
          
        }

      }
  }

  std::sort(globalBBPoints.begin(), globalBBPoints.end());

  std::sort(globalGradPoints.begin(), globalGradPoints.end());
  auto it = std::unique(globalGradPoints.begin(), globalGradPoints.end());
  globalGradPoints.erase(it, globalGradPoints.end());

  identifyGlobalCheckNeighborhood(lattice._refSubDomain, globalGradPoints, globalUpdateCheckPoints);
  globalUpdateCheckPointsSize = globalUpdateCheckPoints.size();

  std::cout << "initial STL read complete" << std::endl;
  MPI_Barrier(MPI_COMM_WORLD);

  std::vector<size_t> localFormerBBPoints = {};
  std::vector<size_t> localGradPoints = {};
  std::vector<std::array<T,1+3+1+3+3+Lattice<T>::q>> localGradData = {};

  //perform initialization of localGradPoints
  for (size_t globalGradPoint : globalGradPoints) {
    size_t globalIndices[3];
    util::getCellIndices3D(globalGradPoint, lattice._refSubDomain.localGridSize()[1], lattice._refSubDomain.localGridSize()[2], globalIndices);
    Index3D localIndex;
    if (lattice._localSubDomain.isLocal(globalIndices[0], globalIndices[1], globalIndices[2], localIndex)) {
      size_t localGradPoint = util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], lattice._localSubDomain.localGridSize()[1], lattice._localSubDomain.localGridSize()[2]);
      localGradPoints.push_back(localGradPoint);
    }
  }
  localGradData.resize(localGradPoints.size());

  stlT heave = getShipHeavePosition(movementTimeOffset, converter);
  stlT pitch = getShipPitchAngle(movementTimeOffset, converter);
  stlT roll = getShipRollAngle(movementTimeOffset, converter);
  Vec3<T> shipVelocity(0.0,0.0,getShipHeaveVelocity(movementTimeOffset, converter));
  Vec3<T> shipAngularVelocity = getShipAngularVelocity(movementTimeOffset, converter);

  stlUpdate<T,stlT,Lattice>(stlReader, converter, shipOffsetX, shipOffsetY, shipOffsetZ, shipVelocity, pitch, roll, heave, shipAngularVelocity, globalUpdateCheckPoints, globalBBPoints, globalGradPoints, lattice._localSubDomain, lattice._refSubDomain, localFormerBBPoints, localGradPoints, localGradData);
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "rank " << rank << " localGradPoints: " << localGradPoints.size() << std::endl;

  // std::ofstream debug2("after.csv");
  // for (size_t globalGradPoint : globalGradPoints) {
  //   size_t globalIndices[3];
  //   util::getCellIndices3D(globalGradPoint, lattice._refSubDomain.localGridSize()[1], lattice._refSubDomain.localGridSize()[2], globalIndices);
  //   debug2 << globalIndices[0] << ", " << globalIndices[1] << ", " << globalIndices[2] << "\n";
  // }
  // debug2.close();


  size_t gradAllocationSpace = localGradPoints.size()*2 + 1;
  lattice.getLatticePointer()->getDataHandler(&gradBoundary)->setExtraAllocationSpace(gradAllocationSpace);
  size_t formerBBAllocationSpace = localGradPoints.size()*2+1;
  memory_space::CudaDeviceHeap<size_t> localFormerBBPointsGPU(formerBBAllocationSpace);

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "initial STL update complete " << rank << std::endl;
  lattice.initDataArrays();

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Done init data arrays rank " << rank << std::endl;

  T reynoldsNumber = desiredFreestream*shipLength/kinematicViscosity;
  T latticeVel = converter.getLatticeVelocity(desiredFreestream);
  int simSteps = converter.getLatticeTime(simTime);
  printf("Reynolds number: %f, physical freestream: %f, lattice freestream: %f, desired lattice freestream: %f\n", reynoldsNumber, desiredFreestream, latticeVel, desiredLatticeVel);

  T vel[3] = {latticeVel, 0, 0};
  lattice.iniEquilibrium(1.0, vel);

  lattice.copyLatticesToGPU();

  std::cout << "finished iniequil" << std::endl;  

  RefinedGridVTKManager3D<T,Lattice> writer("movingNATOGD", lattice);
  writer.addFunctor<BlockLatticeDensity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  writer.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  writer.addFunctor<BlockLatticeIndex3D<T,Lattice>>();

  RefinedGridVTKManager3D<T,Lattice> limWriter("movingNATOGD_limited", lattice);
  limWriter.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  limWriter.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  size_t limWriterOrigin[3] = {(size_t)(iXRightBorder*0.20), size_t(iYTopBorder*0.5)-3, 0};
  size_t limWriterExtend[3] = {(size_t)(iXRightBorder*0.6), (size_t)(iYTopBorder*0.5)+3, (size_t)(iZBackBorder*0.5)};

  RefinedGridVTKManager3D<T,Lattice> extractWriter("movingNATOGD_extractBox", lattice, converter.getConversionFactorLength());
  extractWriter.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  size_t extractWriterOrigin[3] = {(size_t)(resolution*1.9), size_t(resolution*0.38), 0};
  size_t extractWriterExtend[3] = {(size_t)(resolution*2.625), (size_t)(resolution*0.9), (size_t)(resolution*0.3)};

  singleton::directories().setOutputDir("/data/ae-jral/sashok6/movingNATOGD_LiverpoolCase9Res700/");

  writer.write(0);
  limWriter.write(0, limWriterOrigin, limWriterExtend);
  // extractWriter.write(0, extractWriterOrigin, extractWriterExtend);
  if (rank == 0) {
    stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL", pitch, roll, heave,0);
    stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_lim", pitch, roll, heave, 0);
    // stlVizExtractionBox(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_extractBox", pitch, roll, heave, 0);
  }

  std::ofstream shipMovementOutputCSV("shipMovementOutputCSV.csv");
  shipMovementOutputCSV << "Timestep, sim time (s), movement time (s), pitch, roll, heave\n";

  std::cout << "Initial Grad BC specification, rank " << rank << " localGradPoints " << localGradPoints.size() << std::endl;
  if (localGradPoints.size() > 0) {
    auto gradDataHandler = lattice.getLatticePointer()->getDataHandler(&gradBoundary);
    auto gradCellIds = gradDataHandler->getCellIDs();
    auto gradBoundaryPostProcData = gradDataHandler->getPostProcData();

    std::cout << "Initial Grad BC specification, dynamically assigning grad points on rank " << rank << "; currently, size of gradCellIds: " << gradCellIds.size() << std::endl;

    gradDataHandler->overwriteCellIDs(localGradPoints);

    std::cout << "Now gradCellIds has size: " <<  gradDataHandler->getCellIDs().size() << std::endl;
  
    for (int momentaIndex = 0; momentaIndex < localGradPoints.size(); momentaIndex++) {
      for (int dataIndex = 0; dataIndex < 1+3+1+3+3+Lattice<T>::q; dataIndex++) {
        gradBoundaryPostProcData[dataIndex][momentaIndex] = localGradData.at(momentaIndex).at(dataIndex);
      }
    }

    lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().transferToGPU(&gradBoundary, gradDataHandler);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Starting timesteps..." << std::endl;
  util::Timer<T> timer(simSteps, lattice.calculateTotalLatticeUnits());
  timer.start();

  for (unsigned int iSteps = 1; iSteps <= simSteps; ++iSteps)
  {
    lattice.collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    //Print progress to console
    if (iSteps % 20 == 0) {
      timer.print(iSteps, 2);
      if (rank == 2)
        std::cout << "Pitch: " << pitch << ", roll: " << roll << ", heave: " << heave << std::endl;
    }

    //WHOLE VOLUME VISUALIZATION (run only once at beginning and once near end of simulation)
    if (iSteps % converter.getLatticeTime(simTime*0.95) == 0) {
      lattice.copyLatticesToCPU();
      writer.write(iSteps);

      if (rank == 0) {
        stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL", pitch, roll, heave, iSteps);
      }
    }

    T movementTime = converter.getPhysTime(iSteps) - movementStart + movementTimeOffset;

    //25Hz sample
    if (converter.getPhysTime(iSteps) >= nextSampleTime) {
      nextSampleTime += sampleInterval;

      lattice.copyLatticesToCPU();
      limWriter.write(iSteps, limWriterOrigin, limWriterExtend);
      extractWriter.write(iSteps, extractWriterOrigin, extractWriterExtend);

      if (rank == 0) {
        stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_lim", pitch, roll, heave, iSteps);
        stlVizExtractionBox(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_extractBox", pitch, roll, heave, iSteps);
        shipMovementOutputCSV << iSteps << ", " << converter.getPhysTime(iSteps) << ", " << movementTime << ", " << pitch << ", " << roll << ", " << heave << "\n";
        shipMovementOutputCSV.flush();
      }
    } 
    
    if (movementTime >= movementTimeOffset) {
      heave = getShipHeavePosition(movementTime, converter);
      pitch = getShipPitchAngle(movementTime, converter);
      roll = getShipRollAngle(movementTime, converter);
      shipVelocity = Vec3<T>(0.0,0.0,getShipHeaveVelocity(movementTime, converter));
      shipVelocity = getShipAngularVelocity(movementTime, converter);
      
      if (rank == 2 || rank == 3) //hard code hack for the ship GPUs
        stlUpdate<T,stlT,Lattice>(stlReader, converter, shipOffsetX, shipOffsetY, shipOffsetZ, shipVelocity, pitch, roll, heave, shipAngularVelocity, globalUpdateCheckPoints, globalBBPoints, globalGradPoints, lattice._localSubDomain, lattice._refSubDomain, localFormerBBPoints, localGradPoints, localGradData);

      if (localGradPoints.size() > 0) {
        auto gradDataHandler = lattice.getLatticePointer()->getDataHandler(&gradBoundary);
        auto gradCellIds = gradDataHandler->getCellIDs();
        auto gradBoundaryPostProcData = gradDataHandler->getPostProcData();

        if (gradCellIds.size() > gradAllocationSpace)
          std::cout << "WARNING! Grad allocation space of " << gradAllocationSpace << " exceeded! Memory crash imminent." << std::endl;

        gradDataHandler->overwriteCellIDs(localGradPoints);

        for (int momentaIndex = 0; momentaIndex < localGradPoints.size(); momentaIndex++) {
          for (int dataIndex = 0; dataIndex < 1+3+1+3+3+Lattice<T>::q; dataIndex++) {
            gradBoundaryPostProcData[dataIndex][momentaIndex] = localGradData.at(momentaIndex).at(dataIndex);
          }
        }

        lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().transferToGPU(&gradBoundary, gradDataHandler);
      }

      if (localFormerBBPoints.size() > 0) {
        std::cout << "Running BB reinitialize on rank " << rank << ", size: " << localFormerBBPoints.size() << std::endl;
        if (localFormerBBPoints.size() > formerBBAllocationSpace)
          std::cout << "WARNING! Former BB allocation space of " << formerBBAllocationSpace << " exceeded! Memory crash imminent." << std::endl;
        cudaMemcpy(localFormerBBPointsGPU.get(), &localFormerBBPoints[0], sizeof(size_t)*localFormerBBPoints.size(), cudaMemcpyHostToDevice);
        #ifdef __CUDACC__
          formerBBReinitialize<T,Lattice><<<localFormerBBPoints.size()/256+1, 256>>>(lattice.getLatticePointer()->cellData->gpuGetFluidData(), localFormerBBPointsGPU.get(), shipVelocity, shipAngularVelocity, lattice._localSubDomain, shipOffsetX, shipOffsetY, shipOffsetZ + heave, localFormerBBPoints.size());
        #endif
      }
      

      cudaDeviceSynchronize();
      if (localFormerBBPoints.size() > 0) {
        std::cout << "Finished BB reinitialize on rank " << rank << ", size: " << localFormerBBPoints.size() << std::endl;
      }
      MPI_Barrier(MPI_COMM_WORLD);
    }
  }

  timer.stop();
  timer.printSummary();  
  shipMovementOutputCSV.close();
}

int main()
{
  MultipleSteps(simTime);
  finalizeIPC();
  return 0;
}
