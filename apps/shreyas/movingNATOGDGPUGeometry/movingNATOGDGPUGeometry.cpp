/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"

#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinementUtil3D.h"

#include "core/affineTransform.h"

#include "contrib/GPUSTLProcessing/octreeGPU.h"

#include <chrono>
#include <omp.h>
#include <map>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int resolution = 700;
const T desiredLatticeVel = 0.05;
const T desiredFreestream = 9.897; //m/s (9.897, 19.24 knots for case 4), (9.867, 19.18 knots for case 6)
const T shipLength = 3.0; //m (scale model)
const T kinematicViscosity = 1.48E-5;
const T physDensity = 1.225;
const T smagoConst = 0.05;

const double simTime = 10; //s 

const T movementStart = 2.0; //s (testing version)
const T movementTimeOffset = -0.2; //s
const T sampleStart = 1.0; //s (testing version)
const T sampleInterval = 0.05; //s
T nextSampleTime = sampleStart;

std::map<T,std::array<T,3>> shipMotionData;
const std::string shipMotionFilename = "case4_modelScaleShipMotion.csv";
const std::string probeLocationFilename = "measurementPoints.csv";

/** Read file into string. */
// https://stackoverflow.com/questions/18816126/c-read-the-whole-file-in-buffer
inline std::string slurp (const std::string& path) {
  std::ostringstream buf; 
  std::ifstream input (path.c_str()); 
  buf << input.rdbuf(); 
  return buf.str();
}

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2];
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  static IniEquilibriumDynamics<T, Lattice> inletDynamics;

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025 * 500);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXLeftBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &impedanceOut);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder + 1, iXRightBorder - 1, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZFrontBorder, iXRightBorder - 1, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZBackBorder, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder, iXRightBorder - 1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder, iXRightBorder - 1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZBackBorder, iXRightBorder - 1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZBackBorder, iXRightBorder - 1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZBackBorder, iXLeftBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder, iXLeftBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZBackBorder, iXRightBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder, iXRightBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, iXRightBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder + 1, iXLeftBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder + 1, iXLeftBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder + 1, iXRightBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndex;
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, smagoConst * 500);
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics2(omega, smagoConst * 10);

  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 10, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 50, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 11, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics2);

  //ship floor region
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder*0.26, iYTopBorder*0.5-resolution*0.085, 0, iXRightBorder*0.49, iYTopBorder*0.5+resolution*0.085, 0, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());
}

template <typename T>
AffineTransform<T,3> stlTransformCalc(T baseOffsetX, T baseOffsetY, T baseOffsetZ, T pitch, T roll, T heave) {
  Vec3<T> offsetVec(-baseOffsetX, -baseOffsetY, -baseOffsetZ - heave);

  Matrix<T,3,3> rotMat(roll, pitch, (T)0.0, Rotation::ToBody{});
  AffineTransform<T,3> affineTransform(rotMat);
  affineTransform.applyToRight(offsetVec);
  return affineTransform;
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlLocationCalc(UnitConverter<T, Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, stlT pitch, stlT roll, stlT heave, size_t indices[3], stlT location[3]) {

  Vec3<stlT> point((stlT)indices[0], (stlT)indices[1], (stlT)indices[2]);

  Vec3<stlT> offsetVec(-baseOffsetX, -baseOffsetY, -baseOffsetZ - heave);
  Matrix<stlT,3,3> rotMat(roll, pitch, 0.0, Rotation::ToBody{});
  AffineTransform<stlT,3> affineTransform(rotMat);
  affineTransform.applyToRight(offsetVec);

  transformInPlace<stlT> (point, affineTransform);
  location[0] = point(0)*converter.getConversionFactorLength();
  location[1] = point(1)*converter.getConversionFactorLength();
  location[2] = point(2)*converter.getConversionFactorLength();
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlViz(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, std::string outputName, stlT pitch, stlT roll, stlT heave, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength();
      yCoord = yCoord/converter.getConversionFactorLength();
      zCoord = zCoord/converter.getConversionFactorLength();

      Vec3<T> coordVec(xCoord, yCoord, zCoord);
      Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
      AffineTransform<T,3> affineTransform(rotMat);
      Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
      affineTransform.applyToLeft(offsetVec);
      transformInPlace<T>(coordVec, affineTransform);

      outputSTL << processedString << coordVec(0) << " " << coordVec(1) << " " << coordVec(2) << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }


  outputSTL.close();
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlVizExtractionBox(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, std::string outputName, stlT pitch, stlT roll, stlT heave, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength();
      yCoord = yCoord/converter.getConversionFactorLength();
      zCoord = zCoord/converter.getConversionFactorLength();

      Vec3<T> coordVec(xCoord, yCoord, zCoord);
      Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
      AffineTransform<T,3> affineTransform(rotMat);
      Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
      affineTransform.applyToLeft(offsetVec);
      transformInPlace<T>(coordVec, affineTransform);

      coordVec *= converter.getConversionFactorLength();

      outputSTL << processedString << coordVec(0) << " " << coordVec(1) << " " << coordVec(2) << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }

  outputSTL.close();
}

template <typename T, template <typename> class Lattice>
__global__ void formerBBReinitialize(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t * const OPENLB_RESTRICT formerBBPoints, Vec3<T> shipVelocity, Vec3<T> shipAngularVelocity, SubDomainInformation<T,Lattice<T>> localSubDomain, AffineTransform<T,3> transform, size_t length) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  size_t indices[3];
  util::getCellIndices3D(formerBBPoints[threadIndex], localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], indices);
  Index3D localIndex(indices[0], indices[1], indices[2], localSubDomain.localSubDomain);
  Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);
  Vec3<T> originalPoint((T)globalIndex[0], (T)globalIndex[1], (T)globalIndex[2]);

  AffineTransform<T,3> invTransform;
  for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
          invTransform.m(i,j) = transform.m(j,i); //transpose

  Vec3<T> invTranslate(-transform.m(0,3), -transform.m(1,3), -transform.m(2,3));
  invTransform.applyToRight(invTranslate);

  Vec3<T> origin((T)0.0,(T)0.0,(T)0.0);
  transformInPlace<T>(origin, invTransform);

  Vec3<T> rotationArm((T)0.0,(T)0.0,(T)0.0);;
  for (int iD = 0; iD < 3; iD++)
      rotationArm(iD) = originalPoint(iD) - origin(iD);

  Vec3<T> angularVelocityComponent(shipAngularVelocity(1)*rotationArm(2)-shipAngularVelocity(2)*rotationArm(1), -shipAngularVelocity(0)*rotationArm(2)+shipAngularVelocity(2)*rotationArm(0), shipAngularVelocity(0)*rotationArm(1)-shipAngularVelocity(1)*rotationArm(0));

  T rho = 1.0;
  T u[3] = {shipVelocity(0)+angularVelocityComponent(0), shipVelocity(1)+angularVelocityComponent(1), shipVelocity(2)+angularVelocityComponent(2)};
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    cellData[iPop][formerBBPoints[threadIndex]] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
  }
  
  cellData[Lattice<T>::rhoIndex][formerBBPoints[threadIndex]] = rho;
  for (int iD = 0; iD < Lattice<T>::d; iD++)
    cellData[Lattice<T>::uIndex+iD][formerBBPoints[threadIndex]] = u[iD];
}

template <typename valueType, typename floatType>
OPENLB_HOST_DEVICE
floatType trilinearInterpolate(   valueType const v111,
                          valueType const v211,
                          valueType const v121,
                          valueType const v221,
                          valueType const v112,
                          valueType const v212,
                          valueType const v122,
                          valueType const v222,
                          floatType const a, floatType const b, floatType const c,
                          floatType const a_1, floatType const b_1, floatType const c_1) {

  return v111 * a_1 * b_1 * c_1 +
                                 v211 * a   * b_1 * c_1 +
                                 v121 * a_1 * b   * c_1 +
                                 v221 * a   * b   * c_1 +
                                 v112 * a_1 * b_1 * c   +
                                 v212 * a   * b_1 * c   +
                                 v122 * a_1 * b   * c   +
                                 v222 * a   * b   * c;
}

template <typename T, template <typename> class Lattice>
__global__ void probeMeasurements(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, 
                                  const T * const OPENLB_RESTRICT probeX,
                                  const T * const OPENLB_RESTRICT probeY,
                                  const T * const OPENLB_RESTRICT probeZ,
                                  const char * const OPENLB_RESTRICT shipFixed,
                                  SubDomainInformation<T,Lattice<T>> localSubDomain,
                                  T conversionFactorLength,
                                  T conversionFactorVelocity,
                                  T baseOffsetX,
                                  T baseOffsetY,
                                  T baseOffsetZ,
                                  T roll,
                                  T pitch,
                                  T heave,
                                  T * const OPENLB_RESTRICT velocityX,
                                  T * const OPENLB_RESTRICT velocityY,
                                  T * const OPENLB_RESTRICT velocityZ,
                                  size_t numPoints) {

  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= numPoints)
    return;

  // printf("in the kernel!\n");

  Vec3<T> probeIndices(probeX[threadIndex]/conversionFactorLength, probeY[threadIndex]/conversionFactorLength, probeZ[threadIndex]/conversionFactorLength);

  if (shipFixed[threadIndex]) {
    Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
    AffineTransform<T,3> affineTransform(rotMat);
    Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
    affineTransform.applyToLeft(offsetVec);
    transformInPlace<T>(probeIndices, affineTransform);
  }
  else {
    Matrix<T,3,3> rotMat(0.0, 0.0, 0.0, Rotation::ToSpace{});
    AffineTransform<T,3> affineTransform(rotMat);
    Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ); //heave is already in lattice units
    affineTransform.applyToLeft(offsetVec);
    transformInPlace<T>(probeIndices, affineTransform);
  }

  int x1 = (int) floor(probeIndices(0) - localSubDomain.globalIndexStart[0] + localSubDomain.ghostLayer[0]);
  int x2 = x1 + 1;
  int y1 = (int) floor(probeIndices(1) - localSubDomain.globalIndexStart[1] + localSubDomain.ghostLayer[1]);
  int y2 = y1 + 1;
  int z1 = (int) floor(probeIndices(2) - localSubDomain.globalIndexStart[2] + localSubDomain.ghostLayer[2]);
  int z2 = z1 + 1;

  // printf("Probe location: %f, %f, %f; shipFixed: %d; interp location 1: %d, %d, %d; interp location 2: %d, %d, %d\n",
  //           probeX[threadIndex], probeY[threadIndex], probeZ[threadIndex], shipFixed[threadIndex], x1, y1, z1, x2, y2, z2);

  T a = probeIndices(0) - localSubDomain.globalIndexStart[0] + localSubDomain.ghostLayer[0] - x1;
  T a_1 = 1 - a;
  T b = probeIndices(1) - localSubDomain.globalIndexStart[1] + localSubDomain.ghostLayer[1] - y1;
  T b_1 = 1 - b;
  T c = probeIndices(2) - localSubDomain.globalIndexStart[2] + localSubDomain.ghostLayer[2] - z1;
  T c_1 = 1 - c;

  T q111;
  T q211;
  T q121;
  T q221;
  T q112;
  T q212;
  T q122;
  T q222;

  Vec3<T> interpVelocity(0.0,0.0,0.0);

  for (int iD = 0; iD < 3; iD++) {
    q111 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y1, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q211 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y1, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q121 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y2, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q221 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y2, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q112 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y1, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q212 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y1, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q122 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y2, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q222 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y2, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];

    T interp = trilinearInterpolate<T,T>(q111, q211, q121, q221, q112, q212, q122, q222, a, b, c, a_1, b_1, c_1);
    // if (threadIndex == 0) {
    //   printf("iD: %d, interp: %f, q111: %f, q211: %f, q121: %f, q221: %f, q112: %f, q212: %f, q122: %f, q222: %f, a: %f, b: %f, c: %f, a_1: %f, b_1: %f, c_1: %f\n", iD, interp, q111, q211, q121, q221, q112, q212, q122, q222, a, b, c, a_1, b_1, c_1);
    // }

    interpVelocity(iD) = interp*conversionFactorVelocity;
  }

  if (shipFixed[threadIndex]) {
    Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToBody{});
    AffineTransform<T,3> affineTransform(rotMat);
    transformInPlace<T>(interpVelocity, affineTransform);
  }

  velocityX[threadIndex] = interpVelocity(0);
  velocityY[threadIndex] = interpVelocity(1);
  velocityZ[threadIndex] = interpVelocity(2);
}

template <typename T, template <typename> class Lattice>
T getShipHeavePosition(T time, UnitConverter<T,Lattice> converter) {

  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T heave1 = it1->second.at(0);
  T heave2 = it2->second.at(0);

  T interpFraction = (time-time1)/(time2-time1);

  T heave = heave1 + interpFraction*(heave2-heave1); //m
  return heave/converter.getConversionFactorLength(); //lattice length units
}

template <typename T, template <typename> class Lattice>
T getShipHeaveVelocity(T time, UnitConverter<T,Lattice> converter) {

  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T heave1 = it1->second.at(0);
  T heave2 = it2->second.at(0);

  T heaveRate = (heave2-heave1)/(time2-time1); //m/s
  return converter.getLatticeVelocity(heaveRate);  //lattice vel units
}

template <typename T, template <typename> class Lattice>
T getShipPitchAngle(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T pitch1 = it1->second.at(2);
  T pitch2 = it2->second.at(2);

  T interpFraction = (time-time1)/(time2-time1);

  T pitch = pitch1 + interpFraction*(pitch2-pitch1); //rad
  return pitch; //rad
}

template <typename T, template <typename> class Lattice>
T getShipPitchRate(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T pitch1 = it1->second.at(2);
  T pitch2 = it2->second.at(2);

  T pitchRate = (pitch2-pitch1)/(time2-time1); //rad/s
  return pitchRate*converter.getConversionFactorTime(); //converts to rad/timestep
}

template <typename T, template <typename> class Lattice>
T getShipRollAngle(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T roll1 = it1->second.at(1);
  T roll2 = it2->second.at(1);

  T interpFraction = (time-time1)/(time2-time1);

  T roll = roll1 + interpFraction*(roll2-roll1); //rad
  return roll; //rad
}

template <typename T, template <typename> class Lattice>
T getShipRollRate(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T roll1 = it1->second.at(1);
  T roll2 = it2->second.at(1);

  T rollRate = (roll2-roll1)/(time2-time1); //rad/s
  return rollRate*converter.getConversionFactorTime(); //converts to rad/timestep
}

template <typename T, template <typename> class Lattice>
Vec3<T> getShipAngularVelocity(T time, UnitConverter<T,Lattice> converter) {
  T pitch = (T) getShipPitchAngle(time, converter);

  T pitchRate = getShipPitchRate(time, converter);
  T rollRate = getShipRollRate(time, converter);

  Vec3<T> pitchDirection(0.0, 1.0, 0.0);
  Vec3<T> rollDirection(cos(pitch), 0.0, -sin(pitch));

  pitchDirection *= pitchRate;
  rollDirection *= rollRate;

  Vec3<T> angularVelocity(0.0,0.0,0.0);
  angularVelocity(0) = pitchDirection(0) + rollDirection(0);
  angularVelocity(1) = pitchDirection(1) + rollDirection(1);
  angularVelocity(2) = pitchDirection(2) + rollDirection(2);
  return angularVelocity;
}


void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, desiredLatticeVel, shipLength, desiredFreestream, kinematicViscosity, physDensity, 0);
  converter.print();

  const int iXLeftBorder = 0;
  const int iXRightBorder = 4.5*resolution-1;
  const int iYBottomBorder = 0;
  const int iYTopBorder = 1.5*resolution-1;
  const int iZFrontBorder = 0;
  const int iZBackBorder = 1.0*resolution-1;

  std::cout << "Loading ship motion data..." << std::endl;
  std::ifstream shipMotionFile(shipMotionFilename);
  std::string line;
  std::getline(shipMotionFile, line); //read the headerline
  while (std::getline(shipMotionFile, line)) {
    std::istringstream iss(line);
    double t,b,c,d;
    std::string token;
    std::getline(iss, token, ',');
    t = std::stod(token);
    std::getline(iss, token, ',');
    b = std::stod(token);
    std::getline(iss, token, ',');
    c = std::stod(token);
    std::getline(iss, token, ',');
    d = std::stod(token);
    std::array<T,3> currentLineData = {b,c,d};
    shipMotionData.insert({t, currentLineData});
  }

  std::cout << "Loading probe locations..." << std::endl;

  std::ifstream probeLocationFile(probeLocationFilename);
  int numProbes = 50;

  memory_space::CudaUnified<T> probeX(numProbes);
  memory_space::CudaUnified<T> probeY(numProbes);
  memory_space::CudaUnified<T> probeZ(numProbes);
  memory_space::CudaUnified<char> shipFixed(numProbes);
  memory_space::CudaUnified<T> velocityX(numProbes);
  memory_space::CudaUnified<T> velocityY(numProbes);
  memory_space::CudaUnified<T> velocityZ(numProbes);

  int currentProbe = 0;
  while (std::getline(probeLocationFile, line)) {
    std::istringstream iss(line);
    double x,y,z;
    char curShipFixed;
    std::string token;
    std::getline(iss, token, ',');
    x = std::stod(token);
    std::getline(iss, token, ',');
    y = std::stod(token);
    std::getline(iss, token, ',');
    z = std::stod(token);
    std::getline(iss, token, ',');
    curShipFixed = (char) std::stoi(token);

    probeX[currentProbe] = x + 0.832866;
    probeY[currentProbe] = y + 0.0;
    probeZ[currentProbe] = z + 0.143764;
    shipFixed[currentProbe] = curShipFixed;
    currentProbe++;
  }

  T omega = converter.getLatticeRelaxationFrequency();
  
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  RefinedGrid3D<T,Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
  
  unsigned ghostLayer[3] = {2,0,0};
  lattice.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

  std::cout << "Set up refinement..." << std::endl;
  lattice.setupRefinementGPU();
  lattice.setupMultilatticeGPU();

  std::cout << "Define boundaries.... "  << std::endl;
  lattice.applyMasks();
  defineBoundaries(*(lattice.getLatticePointer()), bulkDynamics, lattice._localSubDomain, lattice._refSubDomain);

  std::cout << "Initial STL read..." << std::endl;

  const T shipOffsetX = (228.15+25)/50.0/converter.getConversionFactorLength();
  const T shipOffsetY = 0.75*(T)resolution;
  const T shipOffsetZ = (2.1)/50.0/converter.getConversionFactorLength();

  std::string stlAsString = slurp("NATOGD_1_50scale.stl");

  static PostProcessingDynamics<T,Lattice,GradBoundaryProcessor3D<T,Lattice>> gradBoundary;
  lattice.getLatticePointer()->dynamicsContainer.definePlaceholderDynamics(&gradBoundary);

  size_t gradAllocationSpace = 500000; //set empirically
  lattice.getLatticePointer()->getDataHandler(&gradBoundary)->setExtraAllocationSpace(gradAllocationSpace);

  size_t formerBBAllocationSpace = gradAllocationSpace*2 + 1;
  memory_space::CudaDeviceHeap<size_t> localFormerBBPointsGPU(formerBBAllocationSpace);
  
  STLreader<stlT> stlReader("NATOGD_1_50scale.stl", 0.2, 1.0/converter.getConversionFactorLength()); //scale the STL to lattice units, set voxel size to 0.25 cell length
  stlReader.print();

  Octree<stlT> * parentOctree = stlReader.getTree();
  size_t origin[3] = {(size_t)(iXRightBorder*0.25), (size_t)(iYTopBorder*0.5 - resolution*0.12), (size_t)1};
  size_t extend[3] = {(size_t)(iXRightBorder*0.5), (size_t)(iYTopBorder*0.5 + resolution*0.12), (size_t)(iZBackBorder*0.3)};
  OctreeGPU<T,Lattice> gpuOctree(parentOctree, lattice._localSubDomain, lattice._refSubDomain, origin, extend);

  T heave = getShipHeavePosition(movementTimeOffset, converter);
  T pitch = getShipPitchAngle(movementTimeOffset, converter);
  T roll = getShipRollAngle(movementTimeOffset, converter);
  Vec3<T> shipVelocity(0.0,0.0,getShipHeaveVelocity(movementTimeOffset, converter));
  Vec3<T> shipAngularVelocity = getShipAngularVelocity(movementTimeOffset, converter);
  AffineTransform<T,3> shipTransform = stlTransformCalc(shipOffsetX, shipOffsetY, shipOffsetZ, pitch, roll, heave);

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Initial STL read complete on rank: " << rank << std::endl;
  lattice.initDataArrays();

  size_t * gradDeviceIndices = lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().getDynamicsCellIDsGPU(&gradBoundary);
  T ** gradDevicePostProcData = lattice.getLatticePointer()->dynamicsContainer.getGPUHandler().getPostProcDataGPU(&gradBoundary);

  size_t numFormerBBPoints = gpuOctree.stlInside(shipTransform, localFormerBBPointsGPU.get());
  size_t numGradPoints = gpuOctree.stlBoundary(shipTransform, gradDeviceIndices, gradDevicePostProcData, omega, shipVelocity, shipAngularVelocity);

  // auto deviceGradIndicesPtr = thrust::device_pointer_cast(gradDeviceIndices);
  // std::vector<size_t> cpuGradIndices(numGradPoints);
  // lattice.getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuGradIndices);
  // lattice.copyLatticesToCPU();

  // T** cpuGradData = lattice.getLatticePointer()->getDataHandler(&gradBoundary)->getPostProcData();
  // if (rank == 2) {
  //   std::ofstream gradData("gradData.csv");
  //   for (int i = 0; i < numGradPoints; i++) {
  //     size_t indices[3];
  //     util::getCellIndices3D(deviceGradIndicesPtr[i], lattice._localSubDomain.localGridSize()[1], lattice._localSubDomain.localGridSize()[2], indices);
  //     gradData << indices[0] << "," <<indices[1] <<"," << indices[2] <<"," << cpuGradData[11][i]<<","
  //                                                                        << cpuGradData[12][i]<<","
  //                                                                        << cpuGradData[13][i]<<","
  //                                                                        << cpuGradData[14][i]<<","
  //                                                                        << cpuGradData[15][i]<<","
  //                                                                        << cpuGradData[16][i]<<","
  //                                                                        << cpuGradData[17][i]<<","
  //                                                                        << cpuGradData[18][i]<<","
  //                                                                        << cpuGradData[19][i]<<","
  //                                                                        << cpuGradData[20][i]<<","
  //                                                                        << cpuGradData[21][i]<<","
  //                                                                        << cpuGradData[22][i]<<","
  //                                                                        << cpuGradData[23][i]<<","
  //                                                                        << cpuGradData[24][i]<<","
  //                                                                        << cpuGradData[25][i]<<","
  //                                                                        << cpuGradData[26][i]<<","
  //                                                                        << cpuGradData[27][i]<<","
  //                                                                        << cpuGradData[28][i]<<","
  //                                                                        << cpuGradData[29][i]<<"\n";

  //   }
  //   gradData.close();
  // }

  std::cout << "num grad points: " << numGradPoints << " on rank: " << rank << std::endl;

  T reynoldsNumber = desiredFreestream*shipLength/kinematicViscosity;
  T latticeVel = converter.getLatticeVelocity(desiredFreestream);
  int simSteps = converter.getLatticeTime(simTime);
  printf("Reynolds number: %f, physical freestream: %f, lattice freestream: %f, desired lattice freestream: %f\n", reynoldsNumber, desiredFreestream, latticeVel, desiredLatticeVel);

  T vel[3] = {latticeVel, 0, 0};
  lattice.iniEquilibrium(1.0, vel);
  lattice.copyLatticesToGPU();

  std::cout << "finished iniequil" << std::endl;  

  RefinedGridVTKManager3D<T,Lattice> writer("movingNATOGD", lattice);
  writer.addFunctor<BlockLatticeDensity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  writer.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  writer.addFunctor<BlockLatticeIndex3D<T,Lattice>>();

  RefinedGridVTKManager3D<T,Lattice> limWriter("movingNATOGD_limited", lattice);
  limWriter.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  limWriter.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  size_t limWriterOrigin[3] = {(size_t)(iXRightBorder*0.20), size_t(iYTopBorder*0.5)-2, 0};
  size_t limWriterExtend[3] = {(size_t)(iXRightBorder*0.6), (size_t)(iYTopBorder*0.5)+4, (size_t)(iZBackBorder*0.5)};

  RefinedGridVTKManager3D<T,Lattice> extractWriter("movingNATOGD_extractBox", lattice, converter.getConversionFactorLength());
  extractWriter.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  size_t extractWriterOrigin[3] = {(size_t)(resolution*1.9), size_t(resolution*0.38), 0};
  size_t extractWriterExtend[3] = {(size_t)(resolution*2.625), (size_t)(resolution*0.9), (size_t)(resolution*0.3)};

  singleton::directories().setOutputDir("/data/ae-jral/sashok6/movingNATOGD_validationCase4/");

  // writer.write(0);
  limWriter.write(0, limWriterOrigin, limWriterExtend);

  if (rank == 0) {
    stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL", pitch, roll, heave,0);
    stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_lim", pitch, roll, heave, 0);
  }

  std::ofstream shipMovementOutputCSV("shipMovementOutputCSV.csv");
  shipMovementOutputCSV << "Timestep, sim time (s), movement time (s), pitch, roll, heave\n";

  std::ofstream probeOutputCSV("probeOutputCSV.csv");
  probeOutputCSV << "Timestep, sim time (s), movement time (s), pitch, roll, heave, probe_i velx, probe_i vely, probe_i velz...\n";

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Starting timesteps..." << std::endl;
  util::Timer<T> timer(simSteps, lattice.calculateTotalLatticeUnits());
  timer.start();

  T cAndSMs = 0;
  T movementMs = 0;

  for (unsigned int iSteps = 1; iSteps <= simSteps; ++iSteps)
  {
    auto startCAndS = std::chrono::steady_clock::now();
    std::vector<size_t> cpuGradIndices(numGradPoints);
    lattice.getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuGradIndices);
    lattice.collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    cpuGradIndices.resize(0);
    lattice.getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuGradIndices);
    auto endCAndS = std::chrono::steady_clock::now();

    T movementTime = converter.getPhysTime(iSteps) - movementStart + movementTimeOffset;

    //Print progress to console
    if (iSteps % 20 == 0 && rank == 3) {
      timer.print(iSteps, 2);
      
      std::cout << "Pitch: " << pitch << ", roll: " << roll << ", heave: " << heave << std::endl;

      if (movementTime >= movementTimeOffset) {
        printf("Avg. timestep time: %f, average movement time: %f\n", cAndSMs/20.0/1000.0, movementMs/20.0/1000.0);
        cAndSMs = 0.0;
        movementMs = 0.0;
      }

      #ifdef __CUDACC__
      // std::cout << "Got here!\n" << std::endl;
      probeMeasurements<T,Lattice><<<numProbes/256+1, 256>>>(lattice.getLatticePointer()->cellData->gpuGetFluidData(), &probeX[0], &probeY[0], &probeZ[0], &shipFixed[0],
                                                              lattice._localSubDomain, converter.getConversionFactorLength(), converter.getConversionFactorVelocity(),
                                                              shipOffsetX, shipOffsetY, shipOffsetZ, roll, pitch, heave, &velocityX[0], &velocityY[0], &velocityZ[0], numProbes);
      #endif
      cudaDeviceSynchronize();
      CudaHandleError(cudaPeekAtLastError(), "main", 829);

      probeOutputCSV << iSteps << ", " << converter.getPhysTime(iSteps) << ", " << movementTime << ", " << pitch << ", " << roll << ", " << heave;
      for (int i = 0; i < numProbes; i++)
        probeOutputCSV << ", " << velocityX[i] << ", " << velocityY[i] << ", " << velocityZ[i];

      probeOutputCSV << "\n";
      probeOutputCSV.flush();

    }

    //WHOLE VOLUME VISUALIZATION (run only once at beginning and once near end of simulation)
    if (iSteps % converter.getLatticeTime(simTime*0.95) == 0) {
      lattice.copyLatticesToCPU();
      writer.write(iSteps);

      if (rank == 0) {
        stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL", pitch, roll, heave, iSteps);
      }
    }


    //25Hz sample
    if (converter.getPhysTime(iSteps) >= nextSampleTime) {
      nextSampleTime += sampleInterval;

      lattice.copyLatticesToCPU();
      limWriter.write(iSteps, limWriterOrigin, limWriterExtend);
      extractWriter.write(iSteps, extractWriterOrigin, extractWriterExtend);

      if (rank == 0) {
        stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_lim", pitch, roll, heave, iSteps);
        stlVizExtractionBox(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "NATOGD_STL_extractBox", pitch, roll, heave, iSteps);
        shipMovementOutputCSV << iSteps << ", " << converter.getPhysTime(iSteps) << ", " << movementTime << ", " << pitch << ", " << roll << ", " << heave << "\n";
        shipMovementOutputCSV.flush();
      }
    } 
    
    
    if (movementTime >= movementTimeOffset) {
      auto startMovement = std::chrono::steady_clock::now();
      heave = getShipHeavePosition(movementTime, converter);
      pitch = getShipPitchAngle(movementTime, converter);
      roll = getShipRollAngle(movementTime, converter);
      shipVelocity = Vec3<T>(0.0,0.0,getShipHeaveVelocity(movementTime, converter));
      shipAngularVelocity = getShipAngularVelocity(movementTime, converter);
      shipTransform = stlTransformCalc(shipOffsetX, shipOffsetY, shipOffsetZ, pitch, roll, heave);
      
      numFormerBBPoints = gpuOctree.stlInside(shipTransform, localFormerBBPointsGPU.get());
      numGradPoints = gpuOctree.stlBoundary(shipTransform, gradDeviceIndices, gradDevicePostProcData, omega, shipVelocity, shipAngularVelocity);

      std::vector<size_t> cpuGradIndices(numGradPoints);
      lattice.getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuGradIndices);

      if (numFormerBBPoints > 0) {
        std::cout << "Running BB reinitialize on rank " << rank << ", size: " << numFormerBBPoints << std::endl;
        if (numFormerBBPoints > formerBBAllocationSpace)
          std::cout << "WARNING! Former BB allocation space of " << formerBBAllocationSpace << " exceeded! Memory crash imminent." << std::endl;

        #ifdef __CUDACC__
        formerBBReinitialize<T,Lattice><<<numFormerBBPoints/256+1, 256>>>(lattice.getLatticePointer()->cellData->gpuGetFluidData(), 
                    localFormerBBPointsGPU.get(), shipVelocity, shipAngularVelocity, lattice._localSubDomain, shipTransform, numFormerBBPoints);
        cudaDeviceSynchronize();
        CudaHandleError(cudaPeekAtLastError(), "main", 900);
        #endif
      }
      auto endMovement = std::chrono::steady_clock::now();
      cAndSMs += std::chrono::duration_cast<std::chrono::microseconds>(endCAndS - startCAndS).count();
      movementMs += std::chrono::duration_cast<std::chrono::microseconds>(endMovement - startMovement).count();
    }

    MPI_Barrier(MPI_COMM_WORLD);
  }

  timer.stop();
  timer.printSummary();  
  shipMovementOutputCSV.close();
  probeOutputCSV.close();
}

int main()
{
  MultipleSteps(simTime);
  finalizeIPC();
  return 0;
}
