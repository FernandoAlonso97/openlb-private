/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"

#define Desc ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

#ifdef __CUDACC__
template <typename T, template <typename U> class Lattice>
__global__ void writeValueKernel(T * const OPENLB_RESTRICT inputData, T val, size_t length) {
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= length)
    return;

  inputData[threadIndex] = val;
} 
#endif

int numDevices = 8;
int n = 20;

int main()
{

  T** testPointerPointer = (T**) malloc(sizeof(T*)*5);
  T** secondPointerPointer = &testPointerPointer[3];
  
  T a = 3;
  T b = 7;
  testPointerPointer[0] = nullptr;
  testPointerPointer[1] = &a;
  testPointerPointer[2] = nullptr;
  testPointerPointer[3] = nullptr;
  testPointerPointer[4] = &b;
  for (int i = 0; i < 5; i++) {
    std::cout << testPointerPointer[i] << std::endl;
  }
  for (int i = 0; i < 2; i++) {
    std::cout << secondPointerPointer[i] << std::endl;
  }
  std::cout <<" test pointer arithmetic: " << *(testPointerPointer+1) << std::endl;

  return 0;

  int rank = initIPC();
  int noRanks = getNoRanks();

  int canAccessPeer;
  for (int device = 0; device < numDevices; device++)
    for (int compareDevice = 0; compareDevice < numDevices; compareDevice++) {
      cudaDeviceCanAccessPeer(&canAccessPeer, device, compareDevice);
      cudaDeviceSynchronize();
      std::cout << "Device " << device << " can access device " << compareDevice << ": " << canAccessPeer << std::endl;
    }

  CellBlockData<T,Desc> cellData(1,n);
  if (rank == 0) {
    cudaIpcMemHandle_t handle;
    HANDLE_ERROR(cudaIpcGetMemHandle(&handle, cellData.gpuGetFluidData()[0]));
    MPI_Send(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, 1, 0, MPI_COMM_WORLD);
    MPI_Send(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, 2, 0, MPI_COMM_WORLD);
  }
  T* rank0Data;
  if (rank == 1 || rank == 2) {
    cudaIpcMemHandle_t handle;
    MPI_Status status;
    MPI_Recv(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, 0, 0, MPI_COMM_WORLD, &status);
    cudaIpcOpenMemHandle((void**)&rank0Data, handle, cudaIpcMemLazyEnablePeerAccess);    
  }
  if (rank == 1) {
    #ifdef __CUDACC__
    writeValueKernel<T,Desc><<<1, 256>>>(rank0Data, (T)0.1, (size_t)n);
    #endif
    cudaDeviceSynchronize();
  }
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 2) {
    #ifdef __CUDACC__
    writeValueKernel<T,Desc><<<1, 256>>>(rank0Data, (T)0.5, (size_t)n*0.5);
    #endif
    cudaDeviceSynchronize();
  }
  
  MPI_Barrier(MPI_COMM_WORLD);
  cellData.copyCellDataToCPU();
  
  for (int cellIndex = 0; cellIndex < n; cellIndex++) {
    std::cout << "Rank " << rank << " CellData at index " << cellIndex << ": " << cellData.getCellData()[0][cellIndex] << std::endl;
  }

  if (rank == 0) {
    std::vector<double*> testVec;
    std::vector<double**> testDoublePointerVec;
    double testDouble = 3.5;
    double testDouble2 = 2.7;
    double* pointerDouble = &testDouble;
    testVec.push_back(&testDouble);
    testVec.push_back(nullptr);
    testVec.push_back(&testDouble2);
    testDoublePointerVec.push_back(&pointerDouble);
    testDoublePointerVec.push_back(nullptr);

    for (int i = 0; i < testVec.size(); i++) {
      bool yes = testVec[i];
      std::cout << testVec[i] << ", " << yes << std::endl;
    }
    for (int i = 0; i < testDoublePointerVec.size(); i++) {
      bool yes = testDoublePointerVec[i];
      std::cout << testDoublePointerVec[i] << ", " << yes << std::endl;
    }
  }

  cudaDeviceSynchronize();
  MPI_Finalize();

  return 0;
}
