/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice3D.hh"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.09;
const T domainWidth = 4.0;
const double simTime = 30;
unsigned int resolution = 64;

const T gridSpacing = domainWidth/resolution;
const T gridArea = pow(gridSpacing,2);

const T physInducedVelocity = 5.0; // m/s
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 1.8e-5; // m^2/s

const T rotorStartTime = 0.5;
const T rotorRampUpTime = 10;

const T BETUpdateInterval = 0.02;

/* Rotor Parameters */
const T rotorRadius = 1.0; // meters
const T rotorLatticeRadius = rotorRadius/gridSpacing;
const T rotorInboardCutoffRadius = 0.0; //account for the inner portion of the blade not doing much aerodynamically
const T rotorLatticeInboardCutoffRadius = rotorInboardCutoffRadius/gridSpacing;
const T rotorArea = M_PI*pow(rotorRadius,2); // m^2
T rotorThrottle = 0.01;
const T rotorThrust = 2*physDensity*rotorArea*pow(physInducedVelocity,2);

const int rotorY = 0.75*resolution; //center of the rotor
const int rotorX = 0.5*resolution;
const int rotorZ = 0.5*resolution;
const int rotorMinX = rotorX-ceil(rotorLatticeRadius)-1;
const int rotorMaxX = rotorX+ceil(rotorLatticeRadius)+1;
const int rotorMinZ = rotorZ-ceil(rotorLatticeRadius)-1;
const int rotorMaxZ = rotorZ+ceil(rotorLatticeRadius)+1;

std::unordered_map<int, T> rotorCellWeights;
std::vector<int> rotorInflowCellIDs;

/* Output Parameters */
bool outputVTKData = true;
bool outputRotorData = true;
bool outputDebugData = false;

const T vtkWriteInterval = 0.2; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
    initGhostLayer(domainInfo, lattice);

    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    Index3D localIndexStart;
    Index3D localIndexEnd;
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder,iYBottomBorder+1, iZFrontBorder+1, iXLeftBorder, iYTopBorder-1, iZBackBorder-1, localIndexStart, localIndexEnd)) 
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane0N);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZFrontBorder+1, iXRightBorder, iYTopBorder-1, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane0P);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZFrontBorder+1, iXRightBorder-1, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane1N);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZFrontBorder+1, iXRightBorder-1, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane1P);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder+1, iZFrontBorder, iXRightBorder-1, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane2N);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder+1, iZBackBorder, iXRightBorder-1, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZFrontBorder, iXRightBorder-1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0PN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZFrontBorder, iXRightBorder-1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0NN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZBackBorder, iXRightBorder-1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0PP);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZBackBorder, iXRightBorder-1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0NP);

    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder+1, iZBackBorder, iXLeftBorder, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1PN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder+1, iZFrontBorder, iXLeftBorder, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1NN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZBackBorder, iXRightBorder, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1PP);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZFrontBorder, iXRightBorder, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1NP);

    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder+1, iXRightBorder, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2PN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder+1, iXLeftBorder, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2NN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder+1, iXLeftBorder, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2NP);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder+1, iXRightBorder, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2PP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    Index3D localIndex;
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNN);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN);
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPN);
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNP);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP);
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPP);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP);
}

template <typename T, template<typename> class Lattice>
void defineRotorCellIDs(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &refDomain) {
    int sampleResolution = 100;

    int iY = rotorY;
    for (int iX = rotorMinX; iX <= rotorMaxX; iX++) {
        for (int iZ = rotorMinZ; iZ <= rotorMaxZ; iZ++) {
            int gridPointsInRotor = 0;

            for (int iXSubIndex = 0; iXSubIndex < sampleResolution; iXSubIndex++) {
                for (int iZSubIndex = 0; iZSubIndex < sampleResolution; iZSubIndex++) {
                    T iXSub = (iX-0.5) + (1.0/(2.0*(T)sampleResolution)) + (T)iXSubIndex/(T)sampleResolution;
                    T iZSub = (iZ-0.5) + (1.0/(2.0*(T)sampleResolution)) + (T)iZSubIndex/(T)sampleResolution;

                    T currentSubRadius = sqrt(pow((T)rotorX-iXSub, 2)+pow((T)rotorZ-iZSub, 2));
                    if (currentSubRadius <= rotorLatticeRadius && currentSubRadius >= rotorInboardCutoffRadius) 
                        gridPointsInRotor++;

                }
            }

            T currentCellWeight = (T)gridPointsInRotor/(T)pow(sampleResolution, 2);
            if (currentCellWeight > 0.00001) {
                int currentCellID = util::getCellIndex3D(iX, iY, iZ, refDomain.localGridSize()[1], refDomain.localGridSize()[2]);
                rotorCellWeights.insert({currentCellID, currentCellWeight});

                if (currentCellWeight > 0.999) {
                    rotorInflowCellIDs.push_back(currentCellID);
                }
            }
        }
    }
}

template <typename T, template<typename> class Lattice>
void defineRotorThrust(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain) {

    for (auto rotorCell : rotorCellWeights) {
        size_t indices[3];
        util::getCellIndices3D(rotorCell.first, refDomain.localGridSize()[1], refDomain.localGridSize()[2], indices);

        int iXGlobal = indices[0]; int iYGlobal = indices[1]; int iZGlobal = indices[2];
        Index3D localIndex;
        if (domainInfo.isLocal(iXGlobal, iYGlobal, iZGlobal, localIndex)) {
            int iX = localIndex[0]; int iY = localIndex[1]; int iZ = localIndex[2];

            T currentCellWeight = rotorCell.second;
        
            T currentCellThrust = rotorThrust*rotorThrottle*currentCellWeight;
            T latticeVerticalForce = converter.getLatticeForce(currentCellThrust*gridArea/rotorArea);

            T latticeForce[3] = {0.0, -latticeVerticalForce, 0.0};

            lattice.defineForce(iX, iX, iY, iY, iZ, iZ, latticeForce);
        }
    }
}

template <typename T, template<typename> class Lattice>
std::vector<T> calculateHarmonicInflow(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain) {

    int gridCellsSampled = rotorInflowCellIDs.size();
    T inflowVelocityRunningSum = 0.0;
    T inflowSineVelocityRunningSum = 0.0;
    T inflowCosineVelocityRunningSum = 0.0;

    for (int rotorCellID : rotorInflowCellIDs) {
        size_t indices[3];
        util::getCellIndices3D(rotorCellID, refDomain.localGridSize()[1], refDomain.localGridSize()[2], indices);

        int iXGlobal = indices[0]; int iYGlobal = indices[1]; int iZGlobal = indices[2];
        Index3D localIndex;
        if (domainInfo.isLocal(iXGlobal, iYGlobal, iZGlobal, localIndex)) {
            int iX = localIndex[0]; int iY = localIndex[1]; int iZ = localIndex[2];

            T currentCellVelocity[3];
            lattice.get(iX, iY, iZ).computeU(currentCellVelocity);

            T currentCellLatticeRadius = sqrt(pow(iXGlobal-rotorX, 2)+pow(rotorZ-iZGlobal, 2));
            T currentCellAzimuth = atan2(rotorZ-iZGlobal, iXGlobal-rotorX);

            inflowVelocityRunningSum += -currentCellVelocity[1]; //add the y-direction velocity to inflow velocity, negative sign because inflow is down.
            inflowSineVelocityRunningSum += -currentCellVelocity[1]*(currentCellLatticeRadius/rotorLatticeRadius)*sin(currentCellAzimuth);
            inflowCosineVelocityRunningSum += -currentCellVelocity[1]*(currentCellLatticeRadius/rotorLatticeRadius)*cos(currentCellAzimuth);
        }
    }

    T inflowHarmonicMean = converter.getPhysVelocity(inflowVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicSine = converter.getPhysVelocity(4.0*inflowSineVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicCosine = converter.getPhysVelocity(4.0*inflowCosineVelocityRunningSum/gridCellsSampled);

    std::vector<T> harmonicVector {inflowHarmonicMean, inflowHarmonicSine, inflowHarmonicCosine};
    return harmonicVector;
}

template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

    auto domainInfo = commDataHandler.domainInfo;

    int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
    int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
    int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;

    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
    std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

    Index3D localIndex;
    bool domainContainsRotor = domainInfo.getLocalInfo().isLocal(rotorX, rotorY, rotorZ, localIndex);

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), domainWidth, 2*physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.13);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

    std::cout << "Define boundaries.... " << std::endl;
    defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain);

    if (domainContainsRotor) {
        std::cout << "Define rotor cells.... "  << std::endl;
        defineRotorCellIDs(lattice, refSubDomain);
    }

    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... " << std::endl;
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {

                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
  ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice, commDataHandler);
  NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");

    BlockVTKwriterMultiLattice3D<T, Lattice<T>> writer("outputMultiVTK", domainInfo);
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    BlockLatticeFluidMask3D<T,Lattice> fluidMaskFunctor(lattice);
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    writer.addFunctor(fluidMaskFunctor);

    // size_t origin[3] = {8,31,8};
    // size_t extend[3] = {16,40,16};
    if (outputVTKData)
        writer.write(0, 0.0);

    MPI_Barrier(MPI_COMM_WORLD);
    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();

    PolynomialStartScale<T, T> rotorStartScale(converter.getLatticeTime(rotorRampUpTime), 1.0);

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {
#ifdef ENABLE_CUDA
        collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
        collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            if (outputVTKData) {
                lattice.copyDataToCPU();
                writer.write(trimStep, (float)trimStep);
            }
        }

        if (domainContainsRotor) {
            if ((trimStep % converter.getLatticeTime(BETUpdateInterval)) == 0  && trimStep >= converter.getLatticeTime(rotorStartTime)) {
                T frac[1];
                T iT[1] = {(T)trimStep - (T)converter.getLatticeTime(rotorStartTime)};
                rotorStartScale(frac, iT);
                rotorThrottle = frac[0];
                if (trimStep >= converter.getLatticeTime(rotorStartTime+rotorRampUpTime))
                    rotorThrottle = 1.0;

                lattice.copyDataToCPU();
                std::vector<T> currentHarmonicInflow = calculateHarmonicInflow(lattice, converter, domainInfo.getLocalInfo(), refSubDomain);
                defineRotorThrust(lattice, converter, domainInfo.getLocalInfo(), refSubDomain);

                if (outputRotorData) {
                    std::cout << "Current Average Inflow Velocity: " << currentHarmonicInflow[0] << std::endl;
                    std::cout << "Current Sine Inflow Velocity: " << currentHarmonicInflow[1] << std::endl;
                    std::cout << "Current Cosine Inflow Velocity: " << currentHarmonicInflow[2] << std::endl;
                }            
                lattice.copyDataToGPU();
            }
        }
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    int rank = initIPC();

    unsigned ghostLayer[] = {0,2,0};

    const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainApproximatelyAlongAxis<T, Lattice<T>>((size_t)(resolution), (size_t)(resolution), (size_t)(resolution), 1u, 0u, 1u, ghostLayer);
    const DomainInformation<T, Lattice<T>> subDomainInfo = domainInfoDecomposeDomainApproximatelyAlongAxis<T, Lattice<T>>(refSubDomain, 1, rank, getNoRanks(), ghostLayer);

    CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

    MultipleSteps(commDataHandler, refSubDomain);

    finalizeIPC();
    return 0;
}
