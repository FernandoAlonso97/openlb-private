/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid2D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid2D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager2D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager2D.hh"

#define Lattice ForcedD2Q9Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

bool debugVTK = false;

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  // int iXLeftBorder = 0;
  // int iXRightBorder = 1023;
  // int iYBottomBorder = 0;
  // int iYTopBorder = 8191;

  int iXLeftBorder = 0;
  int iXRightBorder = 149;
  int iYBottomBorder = 0;
  int iYTopBorder = 49;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
    50, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  // int iXLeftFineOffset = 384;
  // int iYBottomFineOffset = 100;
  // int refinedRegionXCoarse = 256;
  // int refinedRegionYCoarse = 7000;

  int iXLeftFineOffset = 15;
  int iYBottomFineOffset = 10;
  int refinedRegionXCoarse = 20;
  int refinedRegionYCoarse = 5;

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(converter.getLatticeRelaxationFrequency());
  RefinedGrid2D<T, Lattice> parent(iXRightBorder+1, iYTopBorder+1, &bulkDynamics, converter.getLatticeRelaxationFrequency());

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> fineBulkDynamics(parent._childOmega);
  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> fine2BulkDynamics(parent._childOmega);
  parent.addChild(&fineBulkDynamics, refinedRegionXCoarse, refinedRegionYCoarse, iXLeftFineOffset, iYBottomFineOffset);
  parent.addChild(&fineBulkDynamics, 36, 6, 54, 20);

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> doubleFineBulkDynamics(parent.children[0]._childOmega);
  parent.children[1].addChild(&doubleFineBulkDynamics, 5,4,2,2);

  unsigned ghostLayer[3] = {2,0,0};
  // unsigned ghostLayer[3] = {0,2,0};
  parent.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);
  
  int localiXLeftBorder = 0;
  int localiXRightBorder = parent._localSubDomain.localGridSize()[0]-1;
  int localiYBottomBorder = 0;
  int localiYTopBorder = parent._localSubDomain.localGridSize()[1]-1;

  // parent.setupRefinementCPU();
  MPI_Barrier(MPI_COMM_WORLD);
  parent.setupRefinementGPU();
  parent.setupMultilatticeGPU();

  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 0) {
    for (size_t index : parent.C2FindicesFInterpNonLocal) {
      std::cout << "Rank " << rank << " nonlocal index " << index << std::endl;
    }
    for (size_t index : parent.C2FInterpNonLocalChildIDs) {
      std::cout << "Rank " << rank << " nonlocal index child ID " << index << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor1) {
      std::cout << "Rank " << rank << " nonlocal neighbor 1 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor1GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 1 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor2) {
      std::cout << "Rank " << rank << " nonlocal neighbor 2 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor2GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 2 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor3) {
      std::cout << "Rank " << rank << " nonlocal neighbor 3 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor3GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 3 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor4) {
      std::cout << "Rank " << rank << " nonlocal neighbor 4 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor4GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 4 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FOutBufferIndicesInterpNonLocal) {
      std::cout << "Rank " << rank << " nonlocal outbuffer index " << index << std::endl;
    }
    for (size_t child  : parent.C2FOutBufferIndicesInterpNonLocalChildIDs) {
      std::cout << "Rank " << rank << " nonlocal outbuffer child " << child << std::endl;
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);
  if (rank == 1) {
    for (size_t index : parent.C2FindicesFInterpNonLocal) {
      std::cout << "Rank " << rank << " nonlocal index " << index << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor1) {
      std::cout << "Rank " << rank << " nonlocal neighbor 1 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor1GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 1 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor2) {
      std::cout << "Rank " << rank << " nonlocal neighbor 2 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor2GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 2 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor3) {
      std::cout << "Rank " << rank << " nonlocal neighbor 3 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor3GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 3 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FindicesFInterpNonLocalNeighbor4) {
      std::cout << "Rank " << rank << " nonlocal neighbor 4 index " << index << std::endl;
    }
    for (size_t gridID : parent.C2FindicesFInterpNonLocalNeighbor4GridIDs) {
      std::cout << "Rank " << rank << " nonlocal neighbor 4 gridID " << gridID << std::endl;
    }
    for (size_t index : parent.C2FOutBufferIndicesInterpNonLocal) {
      std::cout << "Rank " << rank << " nonlocal outbuffer index " << index << std::endl;
    }
    for (size_t child  : parent.C2FOutBufferIndicesInterpNonLocalChildIDs) {
      std::cout << "Rank " << rank << " nonlocal outbuffer child " << child << std::endl;
    }
  }
  
  parent.applyMasks();

  // prepareLattice
  auto *velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto *velocityPostProcessor = new StraightFdBoundaryProcessorGenerator2D<T, Lattice, 0, -1>(localiXLeftBorder, localiXLeftBorder, 1, localiYTopBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(converter.getLatticeRelaxationFrequency(), *velocityMomenta);

  auto *pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto *pressurePostProcessor = new StraightFdBoundaryProcessorGenerator2D<T, Lattice, 0, 1>(localiXRightBorder, localiXRightBorder, 1, localiYTopBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(converter.getLatticeRelaxationFrequency(), *pressureMomenta);

  for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++) {
    Index3D localIndex;
    if ((*parent._domainInfo).getLocalInfo().isLocal(iXLeftBorder, iY, 0, localIndex))
      parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], &velocityBCDynamics);
    if ((*parent._domainInfo).getLocalInfo().isLocal(iXRightBorder, iY, 0, localIndex))
      parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], &pressureBCDynamics);
  }
  for (unsigned iX = iXLeftBorder; iX <= iXRightBorder; iX++) {
    Index3D localIndex;
    if ((*parent._domainInfo).getLocalInfo().isLocal(iX, iYBottomBorder, 0, localIndex))
      parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], &instances::getBounceBack<T,Lattice>());
    if ((*parent._domainInfo).getLocalInfo().isLocal(iX, iYTopBorder, 0, localIndex))
      parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], &instances::getBounceBack<T,Lattice>());
  }

  T vel[2] = {0.0, 0.0};
  parent.initDataArrays();
  parent.iniEquilibrium(1.0, vel);

  RefinedGridVTKManager2D<T,Lattice> refinedVTKWriterCoarse("refinedPoiseuille", parent);
  refinedVTKWriterCoarse.addFunctor<BlockLatticeDensity2D<T,Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeVelocity2D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFluidMask2D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeIndex2D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticePopulation2D<T,Lattice>>(3);

  singleton::directories().setOutputDir("./tmp/");

  size_t writeOrigin[2] = {33,10};
  size_t writeExtend[2] = {58, 48};

  refinedVTKWriterCoarse.write(0);
  // refinedVTKWriterCoarse.write(0, writeOrigin, writeExtend);

  parent.copyLatticesToGPU();

  int rampUpSteps = converter.getLatticeTime(simTime) / 2;
  T leftFinalVelocity = 0.005;

  parent.getLatticePointer()->copyDataToCPU();
  for (int iX = 0; iX <= 0; ++iX) {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      Index3D localIndex;
      if (parent._domainInfo->getLocalInfo().isLocal(iX,iY,0,localIndex)) {
        T vel[2] = {leftFinalVelocity, 0};
        parent.getLatticePointer()->defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], 1.0, vel);
      }
    }
  }
  parent.getLatticePointer()->copyDataToGPU();
    
  MPI_Barrier(MPI_COMM_WORLD);
  util::Timer<T> timer(10000, parent.getLatticePointer()->getNx() * parent.getLatticePointer()->getNy());
  timer.start();
  for (unsigned int iSteps = 1; iSteps <= 10000; ++iSteps)
  {
    parent.collideAndStreamMultilatticeGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % 100 == 0)
      timer.print(iSteps, 2);

    
  }
  timer.stop();
  timer.printSummary();

  parent.copyLatticesToCPU();
  refinedVTKWriterCoarse.write(1);
  // refinedVTKWriterCoarse.write(1, writeOrigin, writeExtend);

  cudaDeviceSynchronize();
  MPI_Finalize();
}

int main()
{
  const double simTime = 0.02;
  MultipleSteps(simTime);
  return 0;
}
