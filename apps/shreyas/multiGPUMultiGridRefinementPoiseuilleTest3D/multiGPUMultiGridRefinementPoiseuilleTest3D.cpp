/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

using namespace olb;
using namespace olb::descriptors;

bool debugVTK = false;

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  int iXLeftBorder = 0;
  int iXRightBorder = 149;
  int iYBottomBorder = 0;
  int iYTopBorder = 49;
  int iZFrontBorder = 0;
  int iZBackBorder = 29;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
    50, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();


  int iXLeftFineOffset = 15;
  int iYBottomFineOffset = 10;
  int iZFrontFineOffset = 5;
  int refinedRegionXCoarse = 70;
  int refinedRegionYCoarse = 5;
  int refinedRegionZCoarse = 6;

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(converter.getLatticeRelaxationFrequency());
  RefinedGrid3D<T, Lattice> parent(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, converter.getLatticeRelaxationFrequency());

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> fineBulkDynamics(parent._childOmega);
  parent.addChild(&fineBulkDynamics, refinedRegionXCoarse, refinedRegionYCoarse, refinedRegionZCoarse, iXLeftFineOffset, iYBottomFineOffset, iZFrontFineOffset);
  parent.addChild(&fineBulkDynamics, 54, 20, 20, 20, 14, 7);

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> doubleFineBulkDynamics(parent.children[0]._childOmega);
  parent.children[1].addChild(&doubleFineBulkDynamics, 7, 6, 5, 10,11,12);

  unsigned ghostLayer[3] = {2,0,0};
  parent.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);
  
  int localiXLeftBorder = 0;
  int localiXRightBorder = parent._localSubDomain.localGridSize()[0]-1;
  int localiYBottomBorder = 0;
  int localiYTopBorder = parent._localSubDomain.localGridSize()[1]-1;
  int localiZFrontBorder = 0;
  int localiZBackBorder = parent._localSubDomain.localGridSize()[1]-1;

  parent.setupRefinementGPU();
  parent.setupMultilatticeGPU();

  parent.applyMasks();

  // prepareLattice
  auto *velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto *velocityPostProcessor = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(localiXLeftBorder, localiXLeftBorder, 1, localiYTopBorder - 1, 1, localiZFrontBorder-1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(converter.getLatticeRelaxationFrequency(), *velocityMomenta);

  auto *pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto *pressurePostProcessor = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(localiXRightBorder, localiXRightBorder, 1, localiYTopBorder - 1, 1, localiZFrontBorder-1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(converter.getLatticeRelaxationFrequency(), *pressureMomenta);

  for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++) {
    for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder -1; iZ++) {
      Index3D localIndex;
      if ((*parent._domainInfo).getLocalInfo().isLocal(iXLeftBorder, iY, iZ, localIndex))
        parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &velocityBCDynamics);
      if ((*parent._domainInfo).getLocalInfo().isLocal(iXRightBorder, iY, iZ, localIndex))
        parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &pressureBCDynamics);
    }
  }
  for (unsigned iX = iXLeftBorder; iX <= iXRightBorder; iX++) {
    for (unsigned iY = iYBottomBorder; iY <= iYTopBorder; iY++) {
      Index3D localIndex;
      if ((*parent._domainInfo).getLocalInfo().isLocal(iX, iY, iZFrontBorder, localIndex))
        parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T,Lattice>());
      if ((*parent._domainInfo).getLocalInfo().isLocal(iX, iY, iZBackBorder, localIndex))
        parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T,Lattice>());
    }

    for (unsigned iZ = iZFrontBorder; iZ <= iZBackBorder; iZ++) {
      Index3D localIndex;
      if ((*parent._domainInfo).getLocalInfo().isLocal(iX, iYBottomBorder, iZ, localIndex))
        parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T,Lattice>());
      if ((*parent._domainInfo).getLocalInfo().isLocal(iX, iYTopBorder, iZ, localIndex))
        parent.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T,Lattice>());
    }
  }

  parent.initDataArrays();
  T vel[3] = {0.0, 0.0, 0.0};
  parent.iniEquilibrium(1.0, vel);
 
  RefinedGridVTKManager3D<T,Lattice> refinedVTKWriterCoarse("refinedPoiseuille", parent);
  refinedVTKWriterCoarse.addFunctor<BlockLatticeDensity3D<T,Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeIndex3D<T, Lattice>>();

  singleton::directories().setOutputDir("./tmp/");

  size_t writeOrigin[3] = {20,5,5};
  size_t writeExtend[3] = {120,40,25};

  refinedVTKWriterCoarse.write(0);
  // refinedVTKWriterCoarse.write(0, writeOrigin, writeExtend);

  parent.copyLatticesToGPU();

  T leftFinalVelocity = 0.005;

  parent.getLatticePointer()->copyDataToCPU();
  for (int iX = 0; iX <= 0; ++iX) {
    for (int iY = 1; iY <= iYTopBorder-1; ++iY)
      for (int iZ = 1; iZ <= iZBackBorder-1; ++iZ)
      {
        Index3D localIndex;
        if (parent._domainInfo->getLocalInfo().isLocal(iX,iY,iZ,localIndex)) {
          T vel[3] = {leftFinalVelocity, 0.0, 0.0};
          parent.getLatticePointer()->defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
      }
    }
  }
  parent.getLatticePointer()->copyDataToGPU();
    
  MPI_Barrier(MPI_COMM_WORLD);
  util::Timer<T> timer(10000, parent.getLatticePointer()->getNx() * parent.getLatticePointer()->getNy() * parent.getLatticePointer()->getNz());
  timer.start();
  for (unsigned int iSteps = 1; iSteps <= 100; ++iSteps)
  {
    parent.collideAndStreamMultilatticeGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    
    if (iSteps % 100 == 0)
      timer.print(iSteps, 2);
  }
  timer.stop();
  timer.printSummary();

  parent.copyLatticesToCPU();
  refinedVTKWriterCoarse.write(1);
  // refinedVTKWriterCoarse.write(1, writeOrigin, writeExtend);

  cudaDeviceSynchronize();
  MPI_Finalize();
}

int main()
{
  const double simTime = 0.02;
  MultipleSteps(simTime);
  return 0;
}
