/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.hh"

#define Lattice ForcedD2Q9Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

int gridFactor = 1;

int iXLeftBorder = 0;
int iXRightBorder = 127 * gridFactor;
int iYBottomBorder = 0;
int iYTopBorder = 63 * gridFactor;

template <template <typename> class Memory>
void MultipleSteps(const double simTime, CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{

  auto domainInfo = commDataHandler.domainInfo;

  int localiXLeftBorder = 0;
  int localiXRightBorder = domainInfo.getLocalInfo().localGridSize()[0]-1;
  int localiYBottomBorder = 0;
  int localiYTopBorder = domainInfo.getLocalInfo().localGridSize()[1]-1;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  Index3D localIndex;
  bool domainContainsInlet = domainInfo.getLocalInfo().isLocal(0, 3, 0, localIndex);
  bool domainContainsOutlet = domainInfo.getLocalInfo().isLocal(iXRightBorder, 3, 0, localIndex);

  T omega = converter.getLatticeRelaxationFrequency();

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);
  BlockLattice2D<T, Lattice> lattice(domainInfo.getLocalInfo(), &bulkDynamics);

  // prepareLattice
  static GhostDynamics<T, Lattice> ghostDynamics(1.0);
  auto *velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto *velocityPostProcessor = new StraightFdBoundaryProcessorGenerator2D<T, Lattice, 0, -1>(localiXLeftBorder, localiXLeftBorder, 1, localiYTopBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(omega, *velocityMomenta);

  auto *pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto *pressurePostProcessor = new StraightFdBoundaryProcessorGenerator2D<T, Lattice, 0, 1>(localiXRightBorder, localiXRightBorder, 1, localiYTopBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(omega, *pressureMomenta);

  for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++) {
    Index3D localIndex;
    if (domainInfo.getLocalInfo().isLocal(iXLeftBorder, iY, 0, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], &velocityBCDynamics);
    if (domainInfo.getLocalInfo().isLocal(iXRightBorder, iY, 0, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], &pressureBCDynamics);
  }
  for (unsigned iX = iXLeftBorder; iX <= iXRightBorder; iX++) {
    Index3D localIndex;
    if (domainInfo.getLocalInfo().isLocal(iX, iYBottomBorder, 0, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], &instances::getBounceBack<T,Lattice>());
    if (domainInfo.getLocalInfo().isLocal(iX, iYTopBorder, 0, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], &instances::getBounceBack<T,Lattice>());
  }

  for (unsigned iX = 0; iX < domainInfo.getLocalInfo().localGridSize()[0]; iX++)
    for (unsigned iY = 0; iY < domainInfo.getLocalInfo().localGridSize()[1]; iY++) {
      if (iX < domainInfo.getLocalInfo().ghostLayer[0] || iX >= domainInfo.getLocalInfo().localGridSize()[0]-domainInfo.getLocalInfo().ghostLayer[0])
        lattice.defineDynamics(iX, iY, &ghostDynamics);
      if (iY < domainInfo.getLocalInfo().ghostLayer[1] || iY >= domainInfo.getLocalInfo().localGridSize()[1]-domainInfo.getLocalInfo().ghostLayer[1])
        lattice.defineDynamics(iX, iY, &ghostDynamics);
    }

  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= localiXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= localiYTopBorder; ++iY)
    {
      T vel[2] = {0, 0};
      lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
      lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  BlockVTKwriter2D<T> vtkWriter("poiseuilleTestGPU_"+std::to_string(domainInfo.localSubDomain));
  BlockLatticeDensity2D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T, Lattice> physVelocityFunctor(lattice, converter);
  BlockLatticeForce2D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure2D<T, Lattice> physPressureFunctor(lattice, converter);
  BlockLatticeFluidMask2D<T,Lattice> fluidMaskFunctor(lattice);

  BlockVTKwriterMultiLattice2D<T, Lattice<T>> vtkMultiWriter("poiseuilleTestGPUMultiWrite", domainInfo);
  BlockLatticeDensity2D<T, Lattice> multiDensityFunctor(lattice);
  BlockLatticeVelocity2D<T, Lattice> multiVelocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T, Lattice> multiPhysVelocityFunctor(lattice, converter);
  BlockLatticeForce2D<T, Lattice> multiForceFunctor(lattice);
  BlockLatticePhysPressure2D<T, Lattice> multiPhysPressureFunctor(lattice, converter);
  BlockLatticeFluidMask2D<T,Lattice> multiFluidMaskFunctor(lattice);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);
  vtkWriter.write(0);

  vtkMultiWriter.addFunctor(multiDensityFunctor);
  vtkMultiWriter.addFunctor(multiVelocityFunctor);
  vtkMultiWriter.addFunctor(multiPhysVelocityFunctor);
  vtkMultiWriter.addFunctor(multiForceFunctor);
  vtkMultiWriter.addFunctor(multiPhysPressureFunctor);
  vtkMultiWriter.addFunctor(multiFluidMaskFunctor);
  size_t origin[2] = {48,20};
  size_t extend[2] = {300,50};
  vtkMultiWriter.write(0,0.0, origin, extend);

  lattice.copyDataToGPU();

  std::cout << "init comms" << std::endl;

#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
  ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice, commDataHandler);
  NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif

  std::cout << "end init comms" << std::endl;

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy());
  timer.start();

  int rampUpSteps = converter.getLatticeTime(simTime) / 2;
  T leftFinalVelocity = 0.05;

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    T currentLeftVelocity = leftFinalVelocity * iSteps / (double)rampUpSteps;
    currentLeftVelocity = currentLeftVelocity > leftFinalVelocity ? leftFinalVelocity : currentLeftVelocity;

    if (domainContainsInlet && iSteps % converter.getLatticeTime(0.1) == 0) {
      lattice.copyDataToCPU();
      for (int iX = 0; iX <= 0; ++iX)
        for (int iY = 1; iY < iYTopBorder; ++iY)
        {
          Index3D localIndex;
          domainInfo.getLocalInfo().isLocal(iX, iY, 0, localIndex);
          T vel[2] = {currentLeftVelocity, 0};
          lattice.defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], 1.0, vel);
        }
      lattice.copyDataToGPU();
    }

#ifdef ENABLE_CUDA
    collideAndStreamMultilatticeGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
    collideAndStreamMultilattice<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(1.0) == 0)
    {
      lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
      vtkMultiWriter.write(iSteps,(float)iSteps, origin, extend);
    }
    //vtkWriter.write(iSteps);
  }

  timer.stop();
  timer.printSummary();
}

int main()
{
  const double simTime = 10;
  int rank = initIPC();
  int noRanks = getNoRanks();

  std::vector<size_t> refDomainSplits = {};
  std::vector<size_t> splitIndices = {50};

  unsigned ghostLayer[] = {2, 0, 0};

  // const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(iXRightBorder + 1), (size_t)(iYTopBorder + 1), (size_t)(0), 0u, 1u, ghostLayer);
  // const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks(), ghostLayer);
  const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAtIndices<T, Lattice<T>>((size_t)(iXRightBorder + 1), (size_t)(iYTopBorder + 1), (size_t)(0), 0, refDomainSplits, 0u, ghostLayer);
  const DomainInformation<T, Lattice<T>> subDomainInfo = domainInfoDecomposeDomainAtIndices<T, Lattice<T>>(refSubDomain, 0, splitIndices, rank, ghostLayer);

  if (rank == 0)
  {
    std::cout << "REF SUBDOMAIN INFO" << std::endl;
    std::cout << refSubDomain;
    std::cout << "Domain Info" << std::endl;
    std::cout << subDomainInfo;
    std::cout << "####" << std::endl;
  }

  CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

  MultipleSteps(simTime, commDataHandler, refSubDomain);

  finalizeIPC();

  return 0;
}
