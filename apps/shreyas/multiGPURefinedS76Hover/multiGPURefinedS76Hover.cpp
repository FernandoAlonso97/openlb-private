/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;
typedef double boundaryT;

#include "olb3D.h"
#include "olb3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"

#include <algorithm>
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.09;
const T domainWidth = 5.8;
const double simTime = 30;
unsigned int resolution = 64;

unsigned int fineLeftOffset = resolution/8;
unsigned int fineBottomOffset = resolution/4;
unsigned int fineFrontOffset = resolution/8;
unsigned int fineX = 0.75*resolution;
unsigned int fineY = (2.5/4.0)*resolution;
unsigned int fineZ = 0.75*resolution;

unsigned int doubleFineLeftOffset = (refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2)-4+1)/48;
unsigned int doubleFineBottomOffset = (refinementutil::getRefinedRegionYFineLength3D(fineX, fineY, fineZ, 2)-4+1)/2;
unsigned int doubleFineFrontOffset = (refinementutil::getRefinedRegionZFineLength3D(fineX, fineY, fineZ, 2)-4+1)/48;
unsigned int doubleFineX = (refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2)-4)-(refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2)-4+1)/24;
unsigned int doubleFineY = (refinementutil::getRefinedRegionYFineLength3D(fineX, fineY, fineZ, 2)-4+1)/2-resolution/16;
unsigned int doubleFineZ = (refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2)-4)-(refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2)-4+1)/24;

const T gridSpacing = 0.5*(domainWidth/resolution);
const T gridArea = pow(gridSpacing,2);

const T physInducedVelocity = 11.053; // m/s, calculated by assuming C_T = 0.005, lambda (inflow ratio) = sqrt(C_T/2), and RPM of the blade = 960
//const T physInducedVelocity = 2.9668;
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 1.8e-5; // m^2/s

const T rotorStartTime = 0.5;
const T rotorRampUpTime = 10;

const T BETUpdateInterval = 0.02; //s

/* Rotor Parameters */
const T rotorRadius = 1.423; // meters
const T rotorLatticeRadius = rotorRadius/gridSpacing;
const T rotorInboardCutoffRadius = 0.27; //account for the inner portion of the blade not doing much aerodynamically
const T rotorLatticeInboardCutoffRadius = rotorInboardCutoffRadius/gridSpacing;
const T rotorArea = M_PI*pow(rotorRadius,2)-M_PI*pow(rotorInboardCutoffRadius,2); // m^2
const T rotorNominalAngularVelocity = 155.35; //rad/s - determined by M_tip = 0.65
const T rotorNominalTipSpeed = rotorRadius*rotorNominalAngularVelocity; //m/s
T rotorTipSpeed = 0.01; //m/s
const int numberOfBlades = 4;
T collectivePitch = 1.8*M_PI/180.0; // rad
const T rootPitch = 7.5*M_PI/180.0;
const T linearTwist = -10.0*M_PI/180.0;
const T rotorSolidity = 0.07043;
const T airfoilLiftSlope = 5.73; //see Leishman p120 bottom
const T airfoilZeroLiftAngle = -1.75*M_PI/180.0; // estimate
const T airfoilDragCoefficient = 0.011; // see Leishman p124 
const T inducedPowerFactor = 1.25; //see Leishman p124, maybe not valid for twisted rotor?

// const int rotorY = 0.75*resolution; //center of the rotor
// const int rotorX = 0.5*resolution;
// const int rotorZ = 0.5*resolution;
// const int rotorMinX = rotorX-ceil(rotorLatticeRadius)-1;
// const int rotorMaxX = rotorX+ceil(rotorLatticeRadius)+1;
// const int rotorMinZ = rotorZ-ceil(rotorLatticeRadius)-1;
// const int rotorMaxZ = rotorZ+ceil(rotorLatticeRadius)+1;

const int rotorY = 0.8*(refinementutil::getRefinedRegionYFineLength3D(fineX, fineY, fineZ, 2)-4)+2; //center of the rotor
const int rotorX = 0.5*(refinementutil::getRefinedRegionXFineLength3D(fineX, fineY, fineZ, 2));
const int rotorZ = 0.5*(refinementutil::getRefinedRegionZFineLength3D(fineX, fineY, fineZ, 2));
const int rotorMinX = rotorX-ceil(rotorLatticeRadius)-1;
const int rotorMaxX = rotorX+ceil(rotorLatticeRadius)+1;
const int rotorMinZ = rotorZ-ceil(rotorLatticeRadius)-1;
const int rotorMaxZ = rotorZ+ceil(rotorLatticeRadius)+1;

std::unordered_map<int, T> rotorCellWeights;
std::vector<int> rotorInflowCellIDs;

/* Output Parameters */
bool outputVTKData = true;
bool outputRotorData = true;
bool outputDebugData = false;

const T vtkWriteInterval = 1.0; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
    int iXLeftBorder = refDomain.globalIndexStart[0];
    int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
    int iYBottomBorder = refDomain.globalIndexStart[1];
    int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
    int iZFrontBorder = refDomain.globalIndexStart[2];
    int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    Index3D localIndexStart;
    Index3D localIndexEnd;
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder,iYBottomBorder+1, iZFrontBorder+1, iXLeftBorder, iYTopBorder-1, iZBackBorder-1, localIndexStart, localIndexEnd)) 
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane0N);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZFrontBorder+1, iXRightBorder, iYTopBorder-1, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane0P);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZFrontBorder+1, iXRightBorder-1, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane1N);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZFrontBorder+1, iXRightBorder-1, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane1P);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder+1, iZFrontBorder, iXRightBorder-1, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane2N);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder+1, iZBackBorder, iXRightBorder-1, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZFrontBorder, iXRightBorder-1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0PN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZFrontBorder, iXRightBorder-1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0NN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZBackBorder, iXRightBorder-1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0PP);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZBackBorder, iXRightBorder-1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge0NP);

    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder+1, iZBackBorder, iXLeftBorder, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1PN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder+1, iZFrontBorder, iXLeftBorder, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1NN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZBackBorder, iXRightBorder, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1PP);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZFrontBorder, iXRightBorder, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge1NP);

    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder+1, iXRightBorder, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2PN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder+1, iXLeftBorder, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2NN);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder+1, iXLeftBorder, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2NP);
    if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder+1, iXRightBorder, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
        lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edge2PP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    Index3D localIndex;
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNN);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNN);
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPN);
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNP);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPN);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNP);
    if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPP);
    if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPP);

}

template <typename T, template<typename> class Lattice>
void defineHubBounceBack(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain) {

    int iXRightBorder = refDomain.localGridSize()[0]-1;
    int iYTopBorder = refDomain.localGridSize()[1]-1;
    int iZBackBorder = refDomain.localGridSize()[2]-1;

    Vector<T,3> hubLocation(rotorX, rotorY, rotorZ);
    T hubRadius = rotorLatticeInboardCutoffRadius*0.75;
    IndicatorSphere3D<T> hubBounceBack(hubLocation, hubRadius);

    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T location[3] = {(T)iX, (T)iY, (T)iZ};
                bool isInside[1];
                hubBounceBack(isInside, location);
                if (isInside[0]) {
                    Index3D localIndex;
                    if (domainInfo.isLocal(iX,iY,iZ, localIndex)) {
                        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T,Lattice>());
                    }
                }
            }
}


template <typename T, template<typename> class Lattice>
void defineRotorCellIDs(BlockLattice3D<T, Lattice> &lattice, const SubDomainInformation<T, Lattice<T>> &refDomain) {
    int sampleResolution = 100;

    int iY = rotorY;
    for (int iX = rotorMinX; iX <= rotorMaxX; iX++) {
        for (int iZ = rotorMinZ; iZ <= rotorMaxZ; iZ++) {
            int gridPointsInRotor = 0;

            for (int iXSubIndex = 0; iXSubIndex < sampleResolution; iXSubIndex++) {
                for (int iZSubIndex = 0; iZSubIndex < sampleResolution; iZSubIndex++) {
                    T iXSub = (iX-0.5) + (1.0/(2.0*(T)sampleResolution)) + (T)iXSubIndex/(T)sampleResolution;
                    T iZSub = (iZ-0.5) + (1.0/(2.0*(T)sampleResolution)) + (T)iZSubIndex/(T)sampleResolution;

                    T currentSubRadius = sqrt(pow((T)rotorX-iXSub, 2)+pow((T)rotorZ-iZSub, 2));
                    if (currentSubRadius <= rotorLatticeRadius && currentSubRadius >= rotorLatticeInboardCutoffRadius) 
                        gridPointsInRotor++;

                }
            }

            T currentCellWeight = (T)gridPointsInRotor/(T)pow(sampleResolution, 2);
            if (currentCellWeight > 0.00001) {
                int currentCellID = util::getCellIndex3D(iX, iY, iZ, refDomain.localGridSize()[1], refDomain.localGridSize()[2]);
                rotorCellWeights.insert({currentCellID, currentCellWeight});

                if (currentCellWeight > 0.999) {
                    rotorInflowCellIDs.push_back(currentCellID);
                }
            }
        }
    }
}

std::vector<std::vector<T>> getHarmonicThrustFromHarmonicInflowAndAdvanceRatio(std::vector<T> harmonicInflow, T rotorSolidity, T liftSlope, T collectivePitch, T advanceRatio) {

    int azimuthalSteps = 72;
    T azimuthalStep = 2.0*M_PI/azimuthalSteps;

    int numHarmonics = 10;

    //simple rectangular integration, should be good enough
    int radialIntegrationSteps = 500;
    T rStep = (1.0-rotorInboardCutoffRadius/rotorRadius)/radialIntegrationSteps;
    std::vector<std::vector<T>> thrustCoefficientHarmonics;
    thrustCoefficientHarmonics.resize(numHarmonics, std::vector<T>(2)); // initialize harmonics vector
    
    for (int i = 0; i < azimuthalSteps; i++) {
        T currentAzimuth = azimuthalStep*i;

        T currentAzimuthalThrustCoefficient = 0.0;
        for (int j = 0; j < radialIntegrationSteps; j++) {
            T current_r = rotorInboardCutoffRadius/rotorRadius+j*rStep;
            T theta = rootPitch+linearTwist*current_r+collectivePitch-airfoilZeroLiftAngle;
            T localInflow = harmonicInflow[0]+harmonicInflow[1]*current_r*sin(currentAzimuth)+harmonicInflow[2]*current_r*cos(currentAzimuth);
            T localInflowRatio = localInflow/rotorTipSpeed;
            T currentPhi = localInflowRatio/current_r;
            T prandtl_f = ((T)numberOfBlades/2.0)*(1.0-current_r)/(current_r*currentPhi);
            T prandtl_F = (2.0/M_PI)*acos(exp(-prandtl_f));
            if (isnan(prandtl_F)) {
                prandtl_F = 1.0;
            }
            T dThrustCoefficient = 0.5*rotorSolidity*liftSlope*(theta*pow(current_r+advanceRatio*sin(currentAzimuth),2)-localInflowRatio*(current_r+advanceRatio*sin(currentAzimuth))); //this expression takes into account advance ratio, even though we are simulating hover in this file
            dThrustCoefficient = dThrustCoefficient*prandtl_F; // apply the tip loss
            currentAzimuthalThrustCoefficient += dThrustCoefficient*rStep;
        }

        for (int j = 0; j < numHarmonics; j++) {
            thrustCoefficientHarmonics[j][0] += currentAzimuthalThrustCoefficient*cos(j*currentAzimuth)*2.0/azimuthalSteps;
            thrustCoefficientHarmonics[j][1] += currentAzimuthalThrustCoefficient*sin(j*currentAzimuth)*2.0/azimuthalSteps;
        }
    }
    return thrustCoefficientHarmonics;
}

T getPowerCoefficientFromHarmonicInflowAndAdvanceRatio(std::vector<T> harmonicInflow, T rotorSolidity, T liftSlope, T collectivePitch, T advanceRatio) {

    int azimuthalSteps = 72;
    T azimuthalStep = 2.0*M_PI/azimuthalSteps;

    //simple rectangular integration, should be good enough
    int radialIntegrationSteps = 500;
    T rStep = (1.0-rotorInboardCutoffRadius/rotorRadius)/radialIntegrationSteps;

    T powerCoefficient = 0;
    
    for (int i = 0; i < azimuthalSteps; i++) {
        T currentAzimuth = azimuthalStep*i;

        T currentAzimuthalPowerCoefficient = 0.0;
        for (int j = 0; j < radialIntegrationSteps; j++) {
            T current_r = rotorInboardCutoffRadius/rotorRadius+j*rStep;
            T theta = rootPitch+linearTwist*current_r+collectivePitch-airfoilZeroLiftAngle;
            T localInflow = harmonicInflow[0]+harmonicInflow[1]*current_r*sin(currentAzimuth)+harmonicInflow[2]*current_r*cos(currentAzimuth);
            T localInflowRatio = localInflow/rotorTipSpeed;
            T currentPhi = localInflowRatio/current_r;

            T dInducedPowerCoefficient = 0.5*rotorSolidity*liftSlope*(theta*localInflowRatio*(current_r+advanceRatio*sin(currentAzimuth))-pow(localInflowRatio,2))*current_r; //this expression takes into account advance ratio, even though we are simulating hover in this file
            T dProfilePowerCoefficient = 0.5*rotorSolidity*liftSlope*((airfoilDragCoefficient/liftSlope)*pow((current_r+advanceRatio*sin(currentAzimuth)),2))*current_r;
            dInducedPowerCoefficient *= inducedPowerFactor; //apply power factor only on induced portion
            T dPowerCoefficient = dInducedPowerCoefficient + dProfilePowerCoefficient;
            currentAzimuthalPowerCoefficient += dPowerCoefficient*rStep;
        }
        powerCoefficient += currentAzimuthalPowerCoefficient*2.0/azimuthalSteps;
        
    }
    powerCoefficient *= 0.5; //a fourier series averaging quirk
    return powerCoefficient;
}

template <typename T, template<typename> class Lattice>
void defineRotorThrustFromHarmonicCoefficients(std::vector<std::vector<T>> harmonicThrustCoefficients, BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain) {

    for (auto rotorCell : rotorCellWeights) {
        size_t indices[3];
        util::getCellIndices3D(rotorCell.first, refDomain.localGridSize()[1], refDomain.localGridSize()[2], indices);

        int iXGlobal = indices[0]; int iYGlobal = indices[1]; int iZGlobal = indices[2];
        Index3D localIndex;
        if (domainInfo.isLocal(iXGlobal, iYGlobal, iZGlobal, localIndex)) {
            int iX = localIndex[0]; int iY = localIndex[1]; int iZ = localIndex[2];

            T currentCellWeight = rotorCell.second;
        
            T currentCellAzimuth = atan2(rotorZ-iZGlobal, iXGlobal-rotorX);

            T currentCellThrustCoefficient = 0.0;
            currentCellThrustCoefficient += harmonicThrustCoefficients[0][0]*0.5;
            for (int i = 1; i < harmonicThrustCoefficients.size(); i++) {
                currentCellThrustCoefficient += harmonicThrustCoefficients[i][0]*cos(i*currentCellAzimuth)+harmonicThrustCoefficients[i][1]*sin(i*currentCellAzimuth);
            }

            T currentCellThrust = currentCellThrustCoefficient*physDensity*rotorArea*pow(rotorTipSpeed,2)*currentCellWeight;
            T latticeVerticalForce = converter.getLatticeForce(currentCellThrust*gridArea/rotorArea);

            T latticeForce[3] = {0.0, -latticeVerticalForce, 0.0};
            lattice.defineForce(iX, iX, iY, iY, iZ, iZ, latticeForce);
        }
    }
}

template <typename T, template<typename> class Lattice>
std::vector<T> calculateHarmonicInflow(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain) {

    int gridCellsSampled = rotorInflowCellIDs.size();
    T inflowVelocityRunningSum = 0.0;
    T inflowSineVelocityRunningSum = 0.0;
    T inflowCosineVelocityRunningSum = 0.0;

    for (int rotorCellID : rotorInflowCellIDs) {
        size_t indices[3];
        util::getCellIndices3D(rotorCellID, refDomain.localGridSize()[1], refDomain.localGridSize()[2], indices);

        int iXGlobal = indices[0]; int iYGlobal = indices[1]; int iZGlobal = indices[2];
        Index3D localIndex;
        if (domainInfo.isLocal(iXGlobal, iYGlobal, iZGlobal, localIndex)) {
            int iX = localIndex[0]; int iY = localIndex[1]; int iZ = localIndex[2];
       
            T currentCellVelocity[3];
            lattice.get(iX, iY, iZ).computeU(currentCellVelocity);
            T currentCellLatticeRadius = sqrt(pow(iXGlobal-rotorX, 2)+pow(rotorZ-iZGlobal, 2));
            T currentCellAzimuth = atan2(rotorZ-iZGlobal, iXGlobal-rotorX);

            inflowVelocityRunningSum += -currentCellVelocity[1]; //add the y-direction velocity to inflow velocity, negative sign because inflow is down.
            inflowSineVelocityRunningSum += -currentCellVelocity[1]*(currentCellLatticeRadius/rotorLatticeRadius)*sin(currentCellAzimuth);
            inflowCosineVelocityRunningSum += -currentCellVelocity[1]*(currentCellLatticeRadius/rotorLatticeRadius)*cos(currentCellAzimuth);
        }
    }

    T inflowHarmonicMean = converter.getPhysVelocity(inflowVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicSine = converter.getPhysVelocity(4.0*inflowSineVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicCosine = converter.getPhysVelocity(4.0*inflowCosineVelocityRunningSum/gridCellsSampled);

    std::vector<T> harmonicVector {inflowHarmonicMean, inflowHarmonicSine, inflowHarmonicCosine};
    return harmonicVector;
}

template <typename T, template<typename> class Lattice>
T calculateAverageAdvanceVelocity(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain) {

    int gridCellsSampled = rotorInflowCellIDs.size();
    T advanceVelocityRunningSum = 0.0;

    for (int rotorCellID : rotorInflowCellIDs) {
        size_t indices[3];
        util::getCellIndices3D(rotorCellID, refDomain.localGridSize()[1], refDomain.localGridSize()[2], indices);

        int iXGlobal = indices[0]; int iYGlobal = indices[1]; int iZGlobal = indices[2];
        Index3D localIndex;
        if (domainInfo.isLocal(iXGlobal, iYGlobal, iZGlobal, localIndex)) {
            int iX = localIndex[0]; int iY = localIndex[1]; int iZ = localIndex[2];

            T currentCellVelocity[3];
            lattice.get(iX, iY, iZ).computeU(currentCellVelocity);
            advanceVelocityRunningSum += currentCellVelocity[0]; 
        }
    }

    return converter.getPhysVelocity(advanceVelocityRunningSum/gridCellsSampled);
}

void MultipleSteps()
{    
    int rank = initIPC();
    int noRanks = getNoRanks();

    int iXRightBorder = resolution-1;
    int iYTopBorder = resolution-1;
    int iZBackBorder = resolution-1;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), domainWidth, 2.0*physInducedVelocity, physKinematicViscosity, physDensity, 0);

    UnitConverter<T,Lattice> fineConverter(RefinedGrid3D<T,Lattice>::getConverterForRefinementLevel(converter, 1));
    UnitConverter<T,Lattice> doubleFineConverter(RefinedGrid3D<T,Lattice>::getConverterForRefinementLevel(converter, 2));
    UnitConverter<T,Lattice> rotorConverter(fineConverter);
    // UnitConverter<T,Lattice> rotorConverter(doubleFineConverter);

    converter.print();
    fineConverter.print();

    T omega = converter.getLatticeRelaxationFrequency();

    std::cout << "omega: " << omega << ", child omega: " << RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1) << std::endl;

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConstant);
    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> childBulkDynamics(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1), smagoConstant);

    std::cout << "Create RefinedGrid.... " << std::endl;
    RefinedGrid3D<T,Lattice> parent(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
    parent.addChild(&childBulkDynamics, fineX, fineY, fineZ, fineLeftOffset, fineBottomOffset, fineFrontOffset);
    // parent.children[0].addChild(&doubleChildBulkDynamics, doubleFineX, doubleFineY, doubleFineZ, doubleFineLeftOffset, doubleFineBottomOffset, doubleFineFrontOffset);

    unsigned ghostLayer[3] = {0,2,0};
    parent.refinedDecomposeDomainEvenlyAlongAxis(rank, 1, noRanks, ghostLayer, 0.25);

    std::cout << "Set up refinement..." << std::endl;
    parent.setupRefinementGPU();
    parent.setupMultilatticeGPU();

    Index3D localIndex;
    bool domainContainsRotor = parent.children[0]._localSubDomain.isLocal(rotorX, rotorY, rotorZ, localIndex);

    std::cout << "Define boundaries.... "  << std::endl;
    parent.applyMasks();
    defineBoundaries(*(parent.getLatticePointer()), bulkDynamics, parent._localSubDomain, parent._refSubDomain);

    BlockLattice3D<T,Lattice>* rotorLattice = parent.children[0].getLatticePointer();
    // BlockLattice3D<T,Lattice>* rotorLattice = parent.children[0].children[0].getLatticePointer();
    SubDomainInformation<T,Lattice<T>> rotorLocalDomain = parent.children[0]._localSubDomain;
    SubDomainInformation<T,Lattice<T>> rotorRefDomain = parent.children[0]._refSubDomain;
    // SubDomainInformation<T,Lattice<T>> rotorLocalDomain = parent.children[0].children[0]._localSubDomain;
    // SubDomainInformation<T,Lattice<T>> rotorRefDomain = parent.children[0].children[0]._refSubDomain;


    defineHubBounceBack(*(parent.children[0].getLatticePointer()), parent.children[0]._localSubDomain, parent.children[0]._refSubDomain);

    std::cout << "Define rotor cells.... "  << std::endl;
    defineRotorCellIDs(*(parent.children[0].getLatticePointer()), parent.children[0]._refSubDomain);

    std::cout << "Init GPU data.... " << std::endl;
    parent.initDataArrays();
    T vel[3] = {0., 0., 0.};
    parent.iniEquilibrium(1.0, vel);
    std::cout << "Finished!" << std::endl;

    std::cout << "Rank " << rank << " Total number of parent cells (including masked): " << parent.getLatticePointer()->getNx()*parent.getLatticePointer()->getNy()*parent.getLatticePointer()->getNz() << std::endl;
    std::cout << "Total number of child cells: " << parent.children[0].getLatticePointer()->getNx()*parent.children[0].getLatticePointer()->getNy()*parent.children[0].getLatticePointer()->getNz() << std::endl;
    std::cout << "Total workload per parent timestep: " << parent.calculateTotalLatticeUnits() << std::endl;

    parent.copyLatticesToGPU();

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");

    RefinedGridVTKManager3D<T,Lattice> writer("S76Hover", parent);
    writer.addFunctor<BlockLatticePhysVelocity3D<T,Lattice>>(0, converter);
    writer.addFunctor<BlockLatticeForce3D<T, Lattice>>();
    writer.addFunctor<BlockLatticeFluidMask3D<T,Lattice>>();

    if (outputVTKData)
        writer.write(0);

    MPI_Barrier(MPI_COMM_WORLD);
    std::cout << "Starting timesteps..." << std::endl;
    Timer<T> timer(trimTime, parent.calculateTotalLatticeUnits());
    timer.start();

    PolynomialStartScale<T, T> rotorStartScale(converter.getLatticeTime(rotorRampUpTime), 1.0);

    for (unsigned int trimStep = 1; trimStep <= trimTime; ++trimStep)
    {
        parent.collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        // parent.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            (*parent.getLatticePointer()).getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            if (outputVTKData) {
                parent.copyLatticesToCPU();
                writer.write(trimStep);
            }
        }

        if ((trimStep % converter.getLatticeTime(BETUpdateInterval)) == 0  && trimStep >= converter.getLatticeTime(rotorStartTime) && domainContainsRotor) { //update rotor force every 0.02 physical seconds

            parent.copyLatticesToCPU();

            if (trimStep <= converter.getLatticeTime(rotorStartTime+rotorRampUpTime)) { //account for rotor ramp up
                T frac[1];
                T iT[1] = {(T)trimStep - (T)converter.getLatticeTime(rotorStartTime)};
                rotorStartScale(frac, iT);
                rotorTipSpeed = rotorNominalTipSpeed*frac[0];
                if (rotorTipSpeed < 0.01)
                    rotorTipSpeed = 0.01; //prevent divide by zero in BET code
            }

            std::vector<T> currentHarmonicInflow = calculateHarmonicInflow((*rotorLattice), rotorConverter, rotorLocalDomain, rotorRefDomain);
            T currentAdvanceVelocity = calculateAverageAdvanceVelocity((*rotorLattice), rotorConverter, rotorLocalDomain, rotorRefDomain);
            T currentAdvanceRatio = currentAdvanceVelocity/rotorTipSpeed;
            std::vector<std::vector<T>> currentHarmonicThrustCoefficient = getHarmonicThrustFromHarmonicInflowAndAdvanceRatio(currentHarmonicInflow, rotorSolidity, airfoilLiftSlope, collectivePitch, currentAdvanceRatio);
            T currentPowerCoefficient = getPowerCoefficientFromHarmonicInflowAndAdvanceRatio(currentHarmonicInflow, rotorSolidity, airfoilLiftSlope, collectivePitch, currentAdvanceRatio);

            defineRotorThrustFromHarmonicCoefficients(currentHarmonicThrustCoefficient, (*rotorLattice), rotorConverter, rotorLocalDomain, rotorRefDomain);

            parent.copyLatticesToGPU();

            if (outputRotorData) {
                std::cout << "Current Average Inflow Velocity: " << currentHarmonicInflow[0] << std::endl;
                std::cout << "Current Sine Inflow Velocity: " << currentHarmonicInflow[1] << std::endl;
                std::cout << "Current Cosine Inflow Velocity: " << currentHarmonicInflow[2] << std::endl;
                std::cout << "Current Advance Ratio: " << currentAdvanceRatio << std::endl;
                std::cout << "Current Mean Thrust Coefficient: " << currentHarmonicThrustCoefficient[0][0]*0.5 << std::endl;
                std::cout << "Current Mean Power Coefficient: " << currentPowerCoefficient << std::endl;
                std::cout << "Current C_T/sigma: " << currentHarmonicThrustCoefficient[0][0]*0.5/rotorSolidity << std::endl;
            }
        }
    }
    timer.stop();
    timer.printSummary();

    finalizeIPC();
}

int main(int argc, char **argv)
{
    MultipleSteps();
    return 0;
}

