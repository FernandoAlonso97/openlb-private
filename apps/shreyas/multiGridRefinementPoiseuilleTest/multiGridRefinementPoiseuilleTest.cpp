/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid2D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid2D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager2D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager2D.hh"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

bool debugVTK = false;

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  int iXLeftBorder = 0;
  int iXRightBorder = 149;
  int iYBottomBorder = 0;
  int iYTopBorder = 49;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
    50, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  UnitConverter<T,Lattice> fineConverter(RefinedGrid2D<T,Lattice>::getConverterForRefinementLevel(converter, 1));
  fineConverter.print();

  int iXLeftFineOffset = 15;
  int iYBottomFineOffset = 10;
  int refinedRegionXCoarse = 20;
  int refinedRegionYCoarse = 5;

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(converter.getLatticeRelaxationFrequency());
  RefinedGrid2D<T, Lattice> parent(iXRightBorder+1, iYTopBorder+1, &bulkDynamics, converter.getLatticeRelaxationFrequency());

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> fineBulkDynamics(RefinedGrid2D<T,Lattice>::getOmegaForRefinementLevel(converter.getLatticeRelaxationFrequency(), 1));
  parent.addChild(&fineBulkDynamics, refinedRegionXCoarse, refinedRegionYCoarse, iXLeftFineOffset, iYBottomFineOffset);
  parent.addChild(&fineBulkDynamics, 54, 20, 20, 14);

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> doubleFineBulkDynamics(RefinedGrid2D<T,Lattice>::getOmegaForRefinementLevel(converter.getLatticeRelaxationFrequency(), 2));
  parent.children[0].addChild(&doubleFineBulkDynamics, 7,4,4,2);

  parent.setupRefinementGPU();
  // parent.setupRefinementCPU();
  parent.applyMasks();
  // prepareLattice
  auto *velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto *velocityPostProcessor = new StraightFdBoundaryProcessorGenerator2D<T, Lattice, 0, -1>(0, 0, 1, iYTopBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(converter.getLatticeRelaxationFrequency(), *velocityMomenta);

  auto *pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto *pressurePostProcessor = new StraightFdBoundaryProcessorGenerator2D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, 1, iYTopBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(converter.getLatticeRelaxationFrequency(), *pressureMomenta);

  parent.getLatticePointer()->defineDynamics(0, 0, 1, iYTopBorder-1, &velocityBCDynamics);
  parent.getLatticePointer()->defineDynamics(iXRightBorder, iXRightBorder, 1, iYTopBorder-1, &pressureBCDynamics);

  parent.getLatticePointer()->defineDynamics(0,iXRightBorder,0,0, &instances::getBounceBack<T,Lattice>());
  parent.getLatticePointer()->defineDynamics(0, iXRightBorder, iYTopBorder, iYTopBorder, &instances::getBounceBack<T,Lattice>());

  parent.initDataArrays();
  T vel[2] = {0.0, 0.0};  
  parent.iniEquilibrium(1.0, vel);

  RefinedGridVTKManager2D<T,Lattice> refinedVTKWriterCoarse("refinedPoiseuille", parent);
  refinedVTKWriterCoarse.addFunctor<BlockLatticeDensity2D<T,Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeVelocity2D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFluidMask2D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeIndex2D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticePopulation2D<T,Lattice>>(3);

  singleton::directories().setOutputDir("./tmp/");

  size_t origin[2] = {10,5};
  size_t extend[2] = {21,20};

  // refinedVTKWriterCoarse.write(0, origin, extend);
  refinedVTKWriterCoarse.write(0);

  parent.copyLatticesToGPU();

  T leftFinalVelocity = 0.005;

  parent.getLatticePointer()->copyDataToCPU();
  for (int iX = 0; iX <= 0; ++iX) {
    for (int iY = 1; iY <= iYTopBorder-1; ++iY)
    {
      T vel[2] = {leftFinalVelocity, 0.0};
      parent.getLatticePointer()->defineRhoU(iX, iX, iY, iY, 1.0, vel); 
    }
  }
  parent.getLatticePointer()->copyDataToGPU();
    
  util::Timer<T> timer(10000, parent.getLatticePointer()->getNx() * parent.getLatticePointer()->getNy());
  timer.start();
  for (unsigned int iSteps = 1; iSteps <= 10000; ++iSteps)
  {
    parent.collideAndStreamGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    // parent.collideAndStream<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % 100 == 0) {
      timer.print(iSteps, 2);
      parent.copyLatticesToCPU();
      refinedVTKWriterCoarse.write(iSteps); 
    }   
  }
  timer.stop();
  timer.printSummary();

  
  // refinedVTKWriterCoarse.write(1, origin, extend);
  

  cudaDeviceSynchronize();
}

int main()
{
  const double simTime = 0.02;
  MultipleSteps(simTime);
  return 0;
}
