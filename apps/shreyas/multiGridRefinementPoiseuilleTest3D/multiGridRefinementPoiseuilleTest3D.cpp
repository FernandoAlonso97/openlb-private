/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

bool debugVTK = false;

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  bool runGPU = true;

  int iXLeftBorder = 0;
  int iXRightBorder = 149;
  int iYBottomBorder = 0;
  int iYTopBorder = 49;
  int iZFrontBorder = 0;
  int iZBackBorder = 29;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
    50, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  int iXLeftFineOffset = 15;
  int iYBottomFineOffset = 10;
  int iZFrontFineOffset = 5;
  int refinedRegionXCoarse = 10;
  int refinedRegionYCoarse = 5;
  int refinedRegionZCoarse = 6;

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(converter.getLatticeRelaxationFrequency());
  RefinedGrid3D<T, Lattice> parent(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, converter.getLatticeRelaxationFrequency());

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> fineBulkDynamics(parent._childOmega);
  parent.addChild(&fineBulkDynamics, refinedRegionXCoarse, refinedRegionYCoarse, refinedRegionZCoarse, iXLeftFineOffset, iYBottomFineOffset, iZFrontFineOffset);
  parent.addChild(&fineBulkDynamics, 54, 20, 20, 20, 14, 7);

  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> doubleFineBulkDynamics(parent.children[0]._childOmega);
  parent.children[1].addChild(&doubleFineBulkDynamics, 7, 6, 5, 10,11,12);

  if (runGPU) {
    parent.setupRefinementGPU();
  }
  else
    parent.setupRefinementCPU();

  parent.applyMasks();
  // prepareLattice
  auto *velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto *velocityPostProcessor = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(0, 0, 1, iYTopBorder - 1, 1, iZBackBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(converter.getLatticeRelaxationFrequency(), *velocityMomenta);

  auto *pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto *pressurePostProcessor = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(iXRightBorder, iXRightBorder, 1, iYTopBorder - 1, 1, iZBackBorder - 1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(converter.getLatticeRelaxationFrequency(), *pressureMomenta);

  parent.getLatticePointer()->defineDynamics(0, 0, 1, iYTopBorder-1, 1, iZBackBorder-1, &velocityBCDynamics);
  parent.getLatticePointer()->defineDynamics(iXRightBorder, iXRightBorder, 1, iYTopBorder-1, 1, iZBackBorder-1, &pressureBCDynamics);

  parent.getLatticePointer()->defineDynamics(0,iXRightBorder,0,0,0,iZBackBorder, &instances::getBounceBack<T,Lattice>());
  parent.getLatticePointer()->defineDynamics(0, iXRightBorder, iYTopBorder, iYTopBorder,0, iZBackBorder, &instances::getBounceBack<T,Lattice>());
  parent.getLatticePointer()->defineDynamics(0,iXRightBorder,0,iYTopBorder,0,0, &instances::getBounceBack<T,Lattice>());
  parent.getLatticePointer()->defineDynamics(0, iXRightBorder, 0, iYTopBorder, iZBackBorder, iZBackBorder, &instances::getBounceBack<T,Lattice>());

  parent.initDataArrays();
  T vel[3] = {0.0, 0.0, 0.0};
  parent.iniEquilibrium(1.0, vel);

  RefinedGridVTKManager3D<T,Lattice> refinedVTKWriterCoarse("refinedPoiseuille", parent);
  refinedVTKWriterCoarse.addFunctor<BlockLatticeDensity3D<T,Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeIndex3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFi<T,Lattice>>();

  singleton::directories().setOutputDir("./tmp/");

  size_t writeOrigin[3] = {20,5,5};
  size_t writeExtend[3] = {120,40,25};

  refinedVTKWriterCoarse.write(0);
  // refinedVTKWriterCoarse.write(0, writeOrigin, writeExtend);

  // for (size_t index : parent.C2FOutBufferIndicesC) {
  //   std::cout << "C2F out buffer C index " << index << std::endl;
  // }

  // for (size_t index : parent.C2FindicesFXInterp) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "X interp index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFYInterp) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Y interp index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFZInterp) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Z interp index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }

  // for (size_t index : parent.C2FindicesFXInterpLeftCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "X interp left corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFXInterpRightCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "X interp right corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFYInterpBottomCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Y interp bottom corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFYInterpTopCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Y interp top corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFZInterpFrontCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Z interp front corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFZInterpBackCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Z interp back corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFZDoubleInterp) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Z double interp index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFZDoubleInterpBackCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Z double interp back corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFZDoubleInterpFrontCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Z double interp front corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFYDoubleInterp) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Y double interp index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFYDoubleInterpBottomCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Y double interp bottom corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFYDoubleInterpTopCorner) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "Y double interp top corner index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FOutBufferIndicesC) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.getLatticePointer()->getNy(), parent.getLatticePointer()->getNz(), indices);
  //   std::cout << "C2F out buffer coarse index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesFCoarseDirect) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "C2F coarse direct fine index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.C2FindicesCCoarseDirect) {
  //   std::cout << "C2F coarse direct coarse index " << index << std::endl; 
  // }
  // for (size_t index : parent.F2COutBufferIndicesF) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.children[0].getLatticePointer()->getNy(), parent.children[0].getLatticePointer()->getNz(), indices);
  //   std::cout << "F2C out buffer fine index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.F2CindicesC) {
  //   size_t indices[3];
  //   util::getCellIndices3D(index, parent.getLatticePointer()->getNy(), parent.getLatticePointer()->getNz(), indices);
  //   std::cout << "F2C coarse index " << index << " with coordinates (" << indices[0] << ", " << indices[1] << ", " << indices[2] << ")" << std::endl; 
  // }
  // for (size_t index : parent.F2CindicesF) {
  //   std::cout << "F2C fine index " << index << std::endl; 
  // }

  parent.copyLatticesToGPU();

  T leftFinalVelocity = 0.005;

  parent.getLatticePointer()->copyDataToCPU();
  for (int iX = 0; iX <= 0; ++iX) 
    for (int iY = 1; iY <= iYTopBorder-1; ++iY)
      for (int iZ = 1; iZ <= iZBackBorder-1; iZ++)
      {
        T vel[3] = {leftFinalVelocity, 0.0, 0.0};
        parent.getLatticePointer()->defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel); 
      }
  parent.getLatticePointer()->copyDataToGPU();
    
  util::Timer<T> timer(10000, parent.getLatticePointer()->getNx() * parent.getLatticePointer()->getNy() * parent.getLatticePointer()->getNz());
  timer.start();

  for (unsigned int iSteps = 1; iSteps <= 100; ++iSteps)
  {
    if (runGPU) {
      parent.collideAndStreamGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    }
    else
      parent.collideAndStream<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % 5 == 0)
      timer.print(iSteps, 2);

    if (runGPU)
      parent.copyLatticesToCPU();
    refinedVTKWriterCoarse.write(iSteps);
    // refinedVTKWriterCoarse.write(0, writeOrigin, writeExtend);
  }
  timer.stop();
  timer.printSummary();

  //parent.copyLatticesToCPU();
  cudaDeviceSynchronize();
}

int main()
{
  const double simTime = 0.02;
  MultipleSteps(simTime);
  return 0;
}
