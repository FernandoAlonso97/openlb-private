/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef float T;
typedef double boundaryT;

#include "olb3D.h"
#include "olb3D.hh"
#include <typeinfo>
#include <iostream>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const T simTime)
{
  int gridFactor = 2;

  int iXLeftBorder = 0;
  int iXRightBorder = 100*gridFactor-1;
  int iYBottomBorder = 0;
  int iYTopBorder = 25*gridFactor-1;
  int iZFrontBorder = 0;
  int iZBackBorder = 25*gridFactor-1;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder+1, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);
  UnitConverter<float, Lattice> const floatConverter(
      (float)converter.getConversionFactorLength(), (float)converter.getConversionFactorTime(), (float)converter.getCharPhysLength(), (float)converter.getCharPhysVelocity(), (float)converter.getPhysViscosity(), (float)converter.getPhysDensity(), (float)converter.getCharPhysPressure());

  converter.print();
  floatConverter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.05);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

  OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
      createInterpBoundaryCondition3D<T, Lattice,
                                      BGKdynamics>(lattice);

  boundaryCondition->addVelocityBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, omega);
  boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega);
  boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, omega);
  boundaryCondition->addExternalVelocityEdge2NN(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, omega);
  boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, omega);

  boundaryCondition->addExternalVelocityCornerNNN(iXLeftBorder, iYBottomBorder, iZFrontBorder, omega);
  boundaryCondition->addExternalVelocityCornerNPN(iXLeftBorder, iYTopBorder, iZFrontBorder, omega);
  boundaryCondition->addExternalVelocityCornerNNP(iXLeftBorder, iYBottomBorder, iZBackBorder, omega);
  boundaryCondition->addExternalVelocityCornerNPP(iXLeftBorder, iYTopBorder, iZBackBorder, omega);

  //boundaryCondition->addPressureBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, omega);
  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1, boundaryT>> rightFaceBoundaryProcessor;
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, &rightFaceBoundaryProcessor);
  
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &bottomFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &topFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &frontFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edgeXNNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edgeXPNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edgeXNPBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edgeXPPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1, boundaryT>> edgePYNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1, boundaryT>> edgePYPBoundaryProcessor;

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edgePYNBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &edgePYPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1, boundaryT>> edgePNZBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1, boundaryT>> edgePPZBoundaryProcessor;

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &edgePNZBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &edgePPZBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1, boundaryT>> cornerPNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1, boundaryT>> cornerPPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1, boundaryT>> cornerPNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1, boundaryT>> cornerPPPBoundaryProcessor;

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &cornerPNNBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &cornerPPNBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &cornerPNPBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &cornerPPPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edgePYNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &edgePYPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &edgePNZBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &edgePPZBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &cornerPNNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &cornerPPNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &cornerPNPBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &cornerPPPBoundaryProcessor);

  Vector<float,3> cylinderEnd1(0.8f, 0.5f, 0.1f);
  Vector<float,3> cylinderEnd2(0.8f, 0.5f, 0.9f);
  IndicatorCylinder3D<float> obstacle(cylinderEnd1, cylinderEnd2, 0.199f);
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
        float location[3] = {floatConverter.getPhysLength((float)iX), floatConverter.getPhysLength((float)iY), floatConverter.getPhysLength((float)iZ)};
        bool isInside[1];
        obstacle(isInside, location);
        if (isInside[0])
          lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>());
      }
    }
  }

  lattice.initDataArrays();

  // setBoundaryValues
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
        T vel[3] = {0, 0, 0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }

  lattice.copyDataToGPU();

  BlockVTKwriter3D<T> vtkWriter("numericalPrecisionValidationChannel_"+ std::to_string(typeid(omega).name()) );
  BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
  BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
  BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy() * lattice.getNz());
  timer.start();

  int rampUpSteps = converter.getLatticeTime(simTime) / 2;
  T leftFinalVelocity = converter.getLatticeVelocity(1.38564);
  std::cout << "Inlet physical vel: " << converter.getPhysVelocity(leftFinalVelocity) << std::endl;
  std::cout << "Inlet lattice vel: " << leftFinalVelocity << std::endl;

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    T currentLeftVelocity = leftFinalVelocity * iSteps / (double)rampUpSteps;
    currentLeftVelocity = currentLeftVelocity > leftFinalVelocity ? leftFinalVelocity : currentLeftVelocity;

    if (iSteps % converter.getLatticeTime(0.05)) {
      lattice.copyDataToCPU();
      for (int iX = 0; iX <= 0; ++iX) {
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
            T vel[3] = {currentLeftVelocity, 0, 0};
            lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1.0, vel);
          }
        }
      }
      lattice.copyDataToGPU();
    }

    lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(1.0) == 0)
    {
      lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
  timer.printSummary();
}

int main()
{
  const T simTime = 25;
  MultipleSteps(simTime);
  return 0;
}
