/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime)
{
  int iXLeftBorder = 0;
  int iXRightBorder = 127;
  int iYBottomBorder = 0;
  int iYTopBorder = 63;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);
  BlockLattice2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);

  // prepareLattice
  auto* velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto* velocityPostProcessor = new StraightFdBoundaryProcessorGenerator2D<T,Lattice,0,-1>  (iXLeftBorder, iXLeftBorder, 1, iYTopBorder-1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(omega, *velocityMomenta);
  lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, &velocityBCDynamics);  //boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder, iYBottomBorder, omega);

  auto* pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto* pressurePostProcessor = new StraightFdBoundaryProcessorGenerator2D<T,Lattice,0,1>  (iXRightBorder, iXRightBorder, 1, iYTopBorder-1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(omega, *pressureMomenta);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, &pressureBCDynamics);

  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, &instances::getBounceBack<T, Lattice>());
  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, &instances::getBounceBack<T, Lattice>());

  lattice.initDataArrays();

  // setBoundaryValues

  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      T vel[2] = {0, 0};
      lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
      lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  lattice.copyDataToGPU();

//   Dynamics<T, Lattice> *testDynamics = lattice.getDynamics(10, 1);
//   std::cout << testDynamics->isPostProcDynamics() << std::endl;
//   std::cout << boundaryProcessor.isPostProcDynamics() << std::endl;
//   Dynamics<T, Lattice> *testDynamics2 = &boundaryProcessor;
//   std::cout << testDynamics2->isPostProcDynamics() << std::endl;

  BlockVTKwriter2D<T> vtkWriter("poiseuilleTestGPU_runonGPU");
  BlockLatticeDensity2D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T, Lattice> physVelocityFunctor(lattice, converter);
  BlockLatticeForce2D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure2D<T, Lattice> physPressureFunctor(lattice, converter);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy());
  timer.start();

  int rampUpSteps = converter.getLatticeTime(simTime) / 2;
  T leftFinalVelocity = 0.05;

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    T currentLeftVelocity = leftFinalVelocity * iSteps / (double)rampUpSteps;
    currentLeftVelocity = currentLeftVelocity > leftFinalVelocity ? leftFinalVelocity : currentLeftVelocity;

    lattice.copyDataToCPU();
    for (int iX = 0; iX <= 0; ++iX) {
      for (int iY = 0; iY <= iYTopBorder; ++iY)
      {
        T vel[2] = {currentLeftVelocity, 0};
        lattice.defineRhoU(iX, iX, iY, iY, 1.0, vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, 1.0, vel);
      }
    }
    lattice.copyDataToGPU();

    lattice.collideAndStreamGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(0.01) == 0)
    {
      lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
}

int main()
{
  const double simTime = 10;
  MultipleSteps(simTime);
  return 0;
}
