/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"

#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinementUtil3D.h"

#include "core/affineTransform.h"

#include "contrib/GPUSTLProcessing/octreeGPU.h"

#include <chrono>
#include <omp.h>
#include <map>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int resolution = 350;
const unsigned int fineResolution = resolution*2;
const T desiredLatticeVel = 0.05;
const T desiredFreestream = 10.160; //m/s (9.897, 19.24 knots for case 4), (9.867, 19.18 knots for case 6), (10.16, 19.75 knots for case 7)
const T shipLength = 3.0; //m (scale model)
const T kinematicViscosity = 1.48E-5;
const T physDensity = 1.225;
const T smagoConst = 0.05;

const double simTime = 6.0; //s 

const T movementStart = 0.8; //s 
const T movementTimeOffset = -0.2; //s
const T sampleStart = 1.0; //s 
const T sampleInterval = 0.001; //s (increased from 0.0008)
T nextSampleTime = sampleStart;

std::map<T,std::array<T,3>> shipMotionData;
const std::string shipMotionFilename = "case7_modelScaleShipMotion.csv";
const std::string probeLocationFilename = "measurementPointsNew.csv";

auto startTic = std::chrono::steady_clock::now();
auto startTic2 = std::chrono::steady_clock::now();

const int numTimers = 10;
std::vector<std::chrono::time_point<std::chrono::steady_clock>> startTics(numTimers);
std::vector<T> ticAccumulations(numTimers);
std::vector<int> ticAccumulationNumbers(numTimers);

void tic(int ticID) {
  startTics.at(ticID) = std::chrono::steady_clock::now();
}

void toc(int ticID, int numAvg, std::string message) {
  auto stopTic = std::chrono::steady_clock::now();
  int diff = std::chrono::duration_cast<std::chrono::microseconds>(stopTic - startTics.at(ticID)).count();
  T msDiff = (T)diff/1000.0;
  ticAccumulations.at(ticID) += msDiff;
  ticAccumulationNumbers.at(ticID)++;

  if (ticAccumulationNumbers.at(ticID) == numAvg) {
    T avgMsDiff = ticAccumulations.at(ticID) / (T) numAvg;
    std::cout << message << ": " << avgMsDiff << " ms on rank " << getRank() << ", averaged over " << numAvg << " steps" << std::endl;
    ticAccumulations.at(ticID) = 0.0;
    ticAccumulationNumbers.at(ticID) = 0; 
  }
}

/** Read file into string. */
// https://stackoverflow.com/questions/18816126/c-read-the-whole-file-in-buffer
inline std::string slurp (const std::string& path) {
  std::ostringstream buf; 
  std::ifstream input (path.c_str()); 
  buf << input.rdbuf(); 
  return buf.str();
}

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2] + 1; //Increment by 1 for the sake of the refinement
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  static IniEquilibriumDynamics<T, Lattice> inletDynamics;

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025 * 500);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXLeftBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &impedanceOut);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder + 1, iXRightBorder - 1, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZFrontBorder, iXRightBorder - 1, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZBackBorder, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder, iXRightBorder - 1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder, iXRightBorder - 1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZBackBorder, iXRightBorder - 1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZBackBorder, iXRightBorder - 1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZBackBorder, iXLeftBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder, iXLeftBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZBackBorder, iXRightBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder, iXRightBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, iXRightBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder + 1, iXLeftBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder + 1, iXLeftBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder + 1, iXRightBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndex;
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, smagoConst * 500);
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics2(omega, smagoConst * 10);

  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 10, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 50, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 11, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics2);

  //non grid region
  if (domainInfo.isRegionLocalToValidGhostLayer(0, 0, 0, iXRightBorder, iYTopBorder, 0, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());
}

template <typename T, template <typename> class Lattice>
void defineFineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2] + 2; //Increment by 2 for the sake of the refinement
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  Index3D localIndexStart;
  Index3D localIndexEnd;

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  //slip
  if (domainInfo.isRegionLocalToValidGhostLayer(0, 0, iZFrontBorder, iXRightBorder, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);

  //non grid region
  if (domainInfo.isRegionLocalToValidGhostLayer(0, 0, 0, iXRightBorder, iYTopBorder, 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());

  //ship floor region
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder*0.125, iYTopBorder*0.05, iZFrontBorder, iXRightBorder*0.75, iYTopBorder*0.95, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());
}

template <typename T>
AffineTransform<T,3> stlTransformCalc(T baseOffsetX, T baseOffsetY, T baseOffsetZ, T pitch, T roll, T heave) {
  Vec3<T> offsetVec(-baseOffsetX, -baseOffsetY, -baseOffsetZ - heave);

  Matrix<T,3,3> rotMat(roll, pitch, (T)0.0, Rotation::ToBody{});
  AffineTransform<T,3> affineTransform(rotMat);
  affineTransform.applyToRight(offsetVec);
  return affineTransform;
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlLocationCalc(UnitConverter<T, Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, stlT pitch, stlT roll, stlT heave, size_t indices[3], stlT location[3]) {

  Vec3<stlT> point((stlT)indices[0], (stlT)indices[1], (stlT)indices[2]);

  Vec3<stlT> offsetVec(-baseOffsetX, -baseOffsetY, -baseOffsetZ - heave);
  Matrix<stlT,3,3> rotMat(roll, pitch, 0.0, Rotation::ToBody{});
  AffineTransform<stlT,3> affineTransform(rotMat);
  affineTransform.applyToRight(offsetVec);

  transformInPlace<stlT> (point, affineTransform);
  location[0] = point(0)*converter.getConversionFactorLength();
  location[1] = point(1)*converter.getConversionFactorLength();
  location[2] = point(2)*converter.getConversionFactorLength();
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlViz(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, std::string outputName, stlT pitch, stlT roll, stlT heave, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength();
      yCoord = yCoord/converter.getConversionFactorLength();
      zCoord = zCoord/converter.getConversionFactorLength();

      Vec3<T> coordVec(xCoord, yCoord, zCoord);
      Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
      AffineTransform<T,3> affineTransform(rotMat);
      Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
      affineTransform.applyToLeft(offsetVec);
      transformInPlace<T>(coordVec, affineTransform);

      outputSTL << processedString << coordVec(0) << " " << coordVec(1) << " " << coordVec(2) << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }


  outputSTL.close();
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlVizExtractionBox(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT baseOffsetX, stlT baseOffsetY, stlT baseOffsetZ, std::string outputName, stlT pitch, stlT roll, stlT heave, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength();
      yCoord = yCoord/converter.getConversionFactorLength();
      zCoord = zCoord/converter.getConversionFactorLength();

      Vec3<T> coordVec(xCoord, yCoord, zCoord);
      Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
      AffineTransform<T,3> affineTransform(rotMat);
      Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
      affineTransform.applyToLeft(offsetVec);
      transformInPlace<T>(coordVec, affineTransform);

      coordVec *= converter.getConversionFactorLength();

      outputSTL << processedString << coordVec(0) << " " << coordVec(1) << " " << coordVec(2) << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }

  outputSTL.close();
}

template <typename T, template <typename> class Lattice>
__global__ void formerBBReinitialize(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t * const OPENLB_RESTRICT formerBBPoints, Vec3<T> shipVelocity, Vec3<T> shipAngularVelocity, SubDomainInformation<T,Lattice<T>> localSubDomain, AffineTransform<T,3> transform, size_t length) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  size_t indices[3];
  util::getCellIndices3D(formerBBPoints[threadIndex], localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], indices);
  Index3D localIndex(indices[0], indices[1], indices[2], localSubDomain.localSubDomain);
  Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);
  Vec3<T> originalPoint((T)globalIndex[0], (T)globalIndex[1], (T)globalIndex[2]);

  AffineTransform<T,3> invTransform;
  for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
          invTransform.m(i,j) = transform.m(j,i); //transpose

  Vec3<T> invTranslate(-transform.m(0,3), -transform.m(1,3), -transform.m(2,3));
  invTransform.applyToRight(invTranslate);

  Vec3<T> origin((T)0.0,(T)0.0,(T)0.0);
  transformInPlace<T>(origin, invTransform);

  Vec3<T> rotationArm((T)0.0,(T)0.0,(T)0.0);;
  for (int iD = 0; iD < 3; iD++)
      rotationArm(iD) = originalPoint(iD) - origin(iD);

  Vec3<T> angularVelocityComponent(shipAngularVelocity(1)*rotationArm(2)-shipAngularVelocity(2)*rotationArm(1), -shipAngularVelocity(0)*rotationArm(2)+shipAngularVelocity(2)*rotationArm(0), shipAngularVelocity(0)*rotationArm(1)-shipAngularVelocity(1)*rotationArm(0));

  T rho = 1.0;
  T u[3] = {shipVelocity(0)+angularVelocityComponent(0), shipVelocity(1)+angularVelocityComponent(1), shipVelocity(2)+angularVelocityComponent(2)};
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    cellData[iPop][formerBBPoints[threadIndex]] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
  }
  
  cellData[Lattice<T>::rhoIndex][formerBBPoints[threadIndex]] = rho;
  for (int iD = 0; iD < Lattice<T>::d; iD++)
    cellData[Lattice<T>::uIndex+iD][formerBBPoints[threadIndex]] = u[iD];
}

template <typename valueType, typename floatType>
OPENLB_HOST_DEVICE
floatType trilinearInterpolate(   valueType const v111,
                          valueType const v211,
                          valueType const v121,
                          valueType const v221,
                          valueType const v112,
                          valueType const v212,
                          valueType const v122,
                          valueType const v222,
                          floatType const a, floatType const b, floatType const c,
                          floatType const a_1, floatType const b_1, floatType const c_1) {

  return v111 * a_1 * b_1 * c_1 +
                                 v211 * a   * b_1 * c_1 +
                                 v121 * a_1 * b   * c_1 +
                                 v221 * a   * b   * c_1 +
                                 v112 * a_1 * b_1 * c   +
                                 v212 * a   * b_1 * c   +
                                 v122 * a_1 * b   * c   +
                                 v222 * a   * b   * c;
}

template <typename T, template <typename> class Lattice>
__global__ void probeMeasurements(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, 
                                  const T * const OPENLB_RESTRICT probeX,
                                  const T * const OPENLB_RESTRICT probeY,
                                  const T * const OPENLB_RESTRICT probeZ,
                                  const char * const OPENLB_RESTRICT shipFixed,
                                  SubDomainInformation<T,Lattice<T>> localSubDomain,
                                  T conversionFactorLength,
                                  T conversionFactorVelocity,
                                  T baseOffsetX,
                                  T baseOffsetY,
                                  T baseOffsetZ,
                                  T roll,
                                  T pitch,
                                  T heave,
                                  T * const OPENLB_RESTRICT velocityX,
                                  T * const OPENLB_RESTRICT velocityY,
                                  T * const OPENLB_RESTRICT velocityZ,
                                  size_t numPoints) {

  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= numPoints)
    return;


  Vec3<T> probeIndices(probeX[threadIndex]/conversionFactorLength, probeY[threadIndex]/conversionFactorLength, probeZ[threadIndex]/conversionFactorLength);

  if (shipFixed[threadIndex]) {
    Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
    AffineTransform<T,3> affineTransform(rotMat);
    Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
    affineTransform.applyToLeft(offsetVec);
    transformInPlace<T>(probeIndices, affineTransform);
  }
  else {
    Matrix<T,3,3> rotMat(0.0, 0.0, 0.0, Rotation::ToSpace{});
    AffineTransform<T,3> affineTransform(rotMat);
    Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ); //heave is already in lattice units
    affineTransform.applyToLeft(offsetVec);
    transformInPlace<T>(probeIndices, affineTransform);
  }

  Index3D localIndex;
  if (!localSubDomain.isLocal((size_t)probeIndices(0), (size_t)probeIndices(1), (size_t) probeIndices(2), localIndex)) {
    velocityX[threadIndex] = -1000000000000000;
    velocityY[threadIndex] = -1000000000000000;
    velocityZ[threadIndex] = -1000000000000000;
    return;
  }

  int x1 = (int) floor(probeIndices(0) - localSubDomain.globalIndexStart[0] + localSubDomain.ghostLayer[0]);
  int x2 = x1 + 1;
  int y1 = (int) floor(probeIndices(1) - localSubDomain.globalIndexStart[1] + localSubDomain.ghostLayer[1]);
  int y2 = y1 + 1;
  int z1 = (int) floor(probeIndices(2) - localSubDomain.globalIndexStart[2] + localSubDomain.ghostLayer[2]);
  int z2 = z1 + 1;

  // if (threadIndex == 0)
  //   printf("Thread index %d, x1 %d, y1 %d, z1 %d, localSubDomain: %d\n", (int)threadIndex, x1, y1, z1, (int)localSubDomain.localSubDomain);

  T a = probeIndices(0) - localSubDomain.globalIndexStart[0] + localSubDomain.ghostLayer[0] - x1;
  T a_1 = 1 - a;
  T b = probeIndices(1) - localSubDomain.globalIndexStart[1] + localSubDomain.ghostLayer[1] - y1;
  T b_1 = 1 - b;
  T c = probeIndices(2) - localSubDomain.globalIndexStart[2] + localSubDomain.ghostLayer[2] - z1;
  T c_1 = 1 - c;

  T q111;
  T q211;
  T q121;
  T q221;
  T q112;
  T q212;
  T q122;
  T q222;

  Vec3<T> interpVelocity(0.0,0.0,0.0);

  for (int iD = 0; iD < 3; iD++) {
    q111 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y1, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q211 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y1, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q121 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y2, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q221 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y2, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q112 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y1, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q212 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y1, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q122 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y2, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q222 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y2, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];

    T interp = trilinearInterpolate<T,T>(q111, q211, q121, q221, q112, q212, q122, q222, a, b, c, a_1, b_1, c_1);
    // if (threadIndex == 0) {
    //   printf("iD: %d, interp: %f, q111: %f, q211: %f, q121: %f, q221: %f, q112: %f, q212: %f, q122: %f, q222: %f, a: %f, b: %f, c: %f, a_1: %f, b_1: %f, c_1: %f\n", iD, interp, q111, q211, q121, q221, q112, q212, q122, q222, a, b, c, a_1, b_1, c_1);
    // }

    interpVelocity(iD) = interp*conversionFactorVelocity;
  }

  if (shipFixed[threadIndex]) {
    Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToBody{});
    AffineTransform<T,3> affineTransform(rotMat);
    transformInPlace<T>(interpVelocity, affineTransform);
  }

  velocityX[threadIndex] = interpVelocity(0);
  velocityY[threadIndex] = interpVelocity(1);
  velocityZ[threadIndex] = interpVelocity(2);
}

template <typename T, template <typename> class Lattice>
__global__ void shipFixedBoxMeasurements(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, 
                                  const T boxOriginX,
                                  const T boxOriginY,
                                  const T boxOriginZ,
                                  const size_t nx,
                                  const size_t ny, 
                                  const size_t nz,
                                  SubDomainInformation<T,Lattice<T>> localSubDomain,
                                  T conversionFactorLength,
                                  T conversionFactorVelocity,
                                  T baseOffsetX,
                                  T baseOffsetY,
                                  T baseOffsetZ,
                                  T roll,
                                  T pitch,
                                  T heave,
                                  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT destCellData) {

  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= nx*ny*nz)
    return;

  size_t boxIndices[3];
  util::getCellIndices3D(threadIndex, ny, nz, boxIndices);

  Vec3<T> probeIndices(boxOriginX/conversionFactorLength + boxIndices[0], boxOriginY/conversionFactorLength + boxIndices[1], boxOriginZ/conversionFactorLength + boxIndices[2]);

  Matrix<T,3,3> rotMat(roll, pitch, 0.0, Rotation::ToSpace{});
  AffineTransform<T,3> affineTransform(rotMat);
  Vec3<T> offsetVec(baseOffsetX,baseOffsetY,baseOffsetZ+heave); //heave is already in lattice units
  affineTransform.applyToLeft(offsetVec);
  transformInPlace<T>(probeIndices, affineTransform);

  Index3D localIndex;
  if (!localSubDomain.isLocal((size_t)probeIndices(0), (size_t)probeIndices(1), (size_t) probeIndices(2), localIndex)) {
    for (int iD = 0; iD < 3; iD++)
      destCellData[Lattice<T>::uIndex+iD][threadIndex] = -1000000000.0;
    return;
  }

  int x1 = (int) floor(probeIndices(0) - localSubDomain.globalIndexStart[0] + localSubDomain.ghostLayer[0]);
  int x2 = x1 + 1;
  int y1 = (int) floor(probeIndices(1) - localSubDomain.globalIndexStart[1] + localSubDomain.ghostLayer[1]);
  int y2 = y1 + 1;
  int z1 = (int) floor(probeIndices(2) - localSubDomain.globalIndexStart[2] + localSubDomain.ghostLayer[2]);
  int z2 = z1 + 1;

  T a = probeIndices(0) - localSubDomain.globalIndexStart[0] + localSubDomain.ghostLayer[0] - x1;
  T a_1 = 1 - a;
  T b = probeIndices(1) - localSubDomain.globalIndexStart[1] + localSubDomain.ghostLayer[1] - y1;
  T b_1 = 1 - b;
  T c = probeIndices(2) - localSubDomain.globalIndexStart[2] + localSubDomain.ghostLayer[2] - z1;
  T c_1 = 1 - c;

  T q111;
  T q211;
  T q121;
  T q221;
  T q112;
  T q212;
  T q122;
  T q222;

  Vec3<T> interpVelocity(0.0,0.0,0.0);

  for (int iD = 0; iD < 3; iD++) {
    q111 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y1, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q211 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y1, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q121 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y2, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q221 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y2, z1, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q112 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y1, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q212 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y1, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q122 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x1, y2, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];
    q222 = cellData[Lattice<T>::uIndex+iD][util::getCellIndex3D(x2, y2, z2, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2])];

    T interp = trilinearInterpolate<T,T>(q111, q211, q121, q221, q112, q212, q122, q222, a, b, c, a_1, b_1, c_1);

    interpVelocity(iD) = interp;
  }

  Matrix<T,3,3> rotMat2(roll, pitch, 0.0, Rotation::ToBody{});
  AffineTransform<T,3> affineTransform2(rotMat2);
  transformInPlace<T>(interpVelocity, affineTransform2);

  for (int iD = 0; iD < 3; iD++)
    destCellData[Lattice<T>::uIndex+iD][threadIndex] = interpVelocity(iD);
}

template <typename T, template <typename> class Lattice>
T getShipHeavePosition(T time, UnitConverter<T,Lattice> converter) {

  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T heave1 = it1->second.at(0);
  T heave2 = it2->second.at(0);

  T interpFraction = (time-time1)/(time2-time1);

  T heave = heave1 + interpFraction*(heave2-heave1); //m
  return heave/converter.getConversionFactorLength(); //lattice length units
}

template <typename T, template <typename> class Lattice>
T getShipHeaveVelocity(T time, UnitConverter<T,Lattice> converter) {

  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T heave1 = it1->second.at(0);
  T heave2 = it2->second.at(0);

  T heaveRate = (heave2-heave1)/(time2-time1); //m/s
  return converter.getLatticeVelocity(heaveRate);  //lattice vel units
}

template <typename T, template <typename> class Lattice>
T getShipPitchAngle(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T pitch1 = it1->second.at(2);
  T pitch2 = it2->second.at(2);

  T interpFraction = (time-time1)/(time2-time1);

  T pitch = pitch1 + interpFraction*(pitch2-pitch1); //rad
  return pitch; //rad
}

template <typename T, template <typename> class Lattice>
T getShipPitchRate(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T pitch1 = it1->second.at(2);
  T pitch2 = it2->second.at(2);

  T pitchRate = (pitch2-pitch1)/(time2-time1); //rad/s
  return pitchRate*converter.getConversionFactorTime(); //converts to rad/timestep
}

template <typename T, template <typename> class Lattice>
T getShipRollAngle(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T roll1 = it1->second.at(1);
  T roll2 = it2->second.at(1);

  T interpFraction = (time-time1)/(time2-time1);

  T roll = roll1 + interpFraction*(roll2-roll1); //rad
  return roll; //rad
}

template <typename T, template <typename> class Lattice>
T getShipRollRate(T time, UnitConverter<T,Lattice> converter) {
  auto it1 = (--shipMotionData.upper_bound(time));
  auto it2 = it1++;

  T time1 = it1->first;
  T time2 = it2->first;

  T roll1 = it1->second.at(1);
  T roll2 = it2->second.at(1);

  T rollRate = (roll2-roll1)/(time2-time1); //rad/s
  return rollRate*converter.getConversionFactorTime(); //converts to rad/timestep
}

template <typename T, template <typename> class Lattice>
Vec3<T> getShipAngularVelocity(T time, UnitConverter<T,Lattice> converter) {
  T pitch = (T) getShipPitchAngle(time, converter);

  T pitchRate = getShipPitchRate(time, converter);
  T rollRate = getShipRollRate(time, converter);

  Vec3<T> pitchDirection(0.0, 1.0, 0.0);
  Vec3<T> rollDirection(cos(pitch), 0.0, -sin(pitch));

  pitchDirection *= pitchRate;
  rollDirection *= rollRate;

  Vec3<T> angularVelocity(0.0,0.0,0.0);
  angularVelocity(0) = pitchDirection(0) + rollDirection(0);
  angularVelocity(1) = pitchDirection(1) + rollDirection(1);
  angularVelocity(2) = pitchDirection(2) + rollDirection(2);
  return angularVelocity;
}

template <typename T, template <typename> class Lattice>
void moveShipFunctor(int timeStep, std::vector<void*> args) {
  tic(1);
  UnitConverter<T,Lattice>* converterPtr = (UnitConverter<T,Lattice>*) args.at(0);
  T currTime = converterPtr->getPhysTime(timeStep);

  T* heavePtr = (T*) args.at(1);
  T* pitchPtr = (T*) args.at(2);
  T* rollPtr = (T*) args.at(3);
  Vec3<T>* shipVelocityPtr = (Vec3<T>*) args.at(4);
  Vec3<T>* shipAngularVelocityPtr = (Vec3<T>*) args.at(5);
  AffineTransform<T,3>* shipTransformPtr = (AffineTransform<T,3>*) args.at(6);

  size_t* numFormerBBPointsPtr = (size_t*) args.at(7);
  size_t* numGradPointsPtr = (size_t*) args.at(8);
  DynamicsDataHandler<T>* dataHandlerPtr = (DynamicsDataHandler<T>*) args.at(9);

  T* shipOffsetXPtr = (T*) args.at(10);
  T* shipOffsetYPtr = (T*) args.at(11);
  T* shipOffsetZPtr = (T*) args.at(12);

  OctreeGPU<T,Lattice>* gpuOctreePtr = (OctreeGPU<T,Lattice>*) args.at(13);
  size_t * localFormerBBPointsGPUPtr = (size_t*) args.at(14);
  size_t * gradDeviceIndicesPtr = (size_t*) args.at(15);
  T** gradDevicePostProcDataPtr = (T**) args.at(16);
  RefinedGrid3D<T,Lattice>* refinedLatticePtr = (RefinedGrid3D<T,Lattice>*) args.at(17);
  T omega = converterPtr->getLatticeRelaxationFrequency();
  
  T movementTime = currTime - movementStart + movementTimeOffset;
  // std::cout << "Got here 0" << std::endl;
  if (movementTime >= movementTimeOffset) {
    // std::cout << "Got here 1" << std::endl;
    *heavePtr = getShipHeavePosition(movementTime, *converterPtr);
    // std::cout << "Got here 2" << std::endl;
    *pitchPtr = getShipPitchAngle(movementTime, *converterPtr);
    // std::cout << "Got here 3" << std::endl;
    *rollPtr = getShipRollAngle(movementTime, *converterPtr);
    // std::cout << "Got here 4" << std::endl;
    *shipVelocityPtr = Vec3<T>(0.0,0.0,getShipHeaveVelocity(movementTime, *converterPtr));
    // std::cout << "Got here 5" << std::endl;
    *shipAngularVelocityPtr = getShipAngularVelocity(movementTime, *converterPtr);
    // std::cout << "Got here 6" << std::endl;
    *shipTransformPtr = stlTransformCalc(*shipOffsetXPtr, *shipOffsetYPtr, *shipOffsetZPtr, *pitchPtr, *rollPtr, *heavePtr);
    // std::cout << "Got here 7" << std::endl;

    *numFormerBBPointsPtr = gpuOctreePtr->stlInside(*shipTransformPtr, localFormerBBPointsGPUPtr);
    // std::cout << "Got here 8" << std::endl;
    *numGradPointsPtr = gpuOctreePtr->stlBoundary(*shipTransformPtr, gradDeviceIndicesPtr, gradDevicePostProcDataPtr, omega, *shipVelocityPtr, *shipAngularVelocityPtr);
    // std::cout << "Got here 9" << std::endl;

    std::vector<size_t> cpuGradIndices(*numGradPointsPtr);
    // std::cout << "Got here 10" << std::endl;

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    // std::cout << "Data handler pointer address: " << dataHandlerPtr << std::endl;

    dataHandlerPtr->overwriteCellIDs(cpuGradIndices);
    // std::cout << "Got here 11" << std::endl;

    if (*numFormerBBPointsPtr > 0) {
      // std::cout << "Got here 12" << std::endl;
      std::cout << "Running BB reinitialize, size: " << *numFormerBBPointsPtr << std::endl;
      
      #ifdef __CUDACC__
      formerBBReinitialize<T,Lattice><<<*numFormerBBPointsPtr/256+1, 256>>>(refinedLatticePtr->getLatticePointer()->cellData->gpuGetFluidData(), 
                  localFormerBBPointsGPUPtr, *shipVelocityPtr, *shipAngularVelocityPtr, refinedLatticePtr->_localSubDomain, *shipTransformPtr, *numFormerBBPointsPtr);
      cudaDeviceSynchronize();
      #endif
    }
    // std::cout << "Got here 13" << std::endl;
  }
  toc(1, 1000, "Ship movement");
}


void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, desiredLatticeVel, shipLength, desiredFreestream, kinematicViscosity, physDensity, 0);
  UnitConverter<T, Lattice> fineConverter(RefinedGrid3D<T,Lattice>::getConverterForRefinementLevel(converter, 1));
  converter.print();

  const int iXLeftBorder = 0;
  const int iXRightBorder = 4.5*resolution-1;
  const int iYBottomBorder = 0;
  const int iYTopBorder = 1.5*resolution-1;
  const int iZFrontBorder = 0;
  const int iZBackBorder = 1.0*resolution-1;

  const int iXLeftBorderFine = 0.2*iXRightBorder;
  const int iYBottomBorderFine = 0.35*iYTopBorder;
  const int iZFrontBorderFine = 1;
  const int iXLengthFine = 0.4*iXRightBorder;
  const int iYLengthFine = 0.3*iYTopBorder;
  const int iZLengthFine = 0.5*iZBackBorder;

  std::cout << "Loading ship motion data..." << std::endl;
  std::ifstream shipMotionFile(shipMotionFilename);
  std::string line;
  std::getline(shipMotionFile, line); //read the headerline
  while (std::getline(shipMotionFile, line)) {
    std::istringstream iss(line);
    double t,b,c,d;
    std::string token;
    std::getline(iss, token, ',');
    t = std::stod(token);
    std::getline(iss, token, ',');
    b = std::stod(token);
    std::getline(iss, token, ',');
    c = std::stod(token);
    std::getline(iss, token, ',');
    d = std::stod(token);
    std::array<T,3> currentLineData = {b,c,d};
    shipMotionData.insert({t, currentLineData});
  }

  std::cout << "Loading probe locations..." << std::endl;

  std::ifstream probeLocationFile(probeLocationFilename);
  int numProbes = 170;

  memory_space::CudaUnified<T> probeX(numProbes);
  memory_space::CudaUnified<T> probeY(numProbes);
  memory_space::CudaUnified<T> probeZ(numProbes);
  memory_space::CudaUnified<char> shipFixed(numProbes);
  memory_space::CudaUnified<T> velocityX(numProbes);
  memory_space::CudaUnified<T> velocityY(numProbes);
  memory_space::CudaUnified<T> velocityZ(numProbes);

  int currentProbe = 0;
  while (std::getline(probeLocationFile, line)) {
    std::istringstream iss(line);
    double x,y,z;
    char curShipFixed;
    std::string token;
    std::getline(iss, token, ',');
    x = std::stod(token);
    std::getline(iss, token, ',');
    y = std::stod(token);
    std::getline(iss, token, ',');
    z = std::stod(token);
    std::getline(iss, token, ',');
    curShipFixed = (char) std::stoi(token);

    probeX[currentProbe] = x + 0.790194; //updated origin!
    probeY[currentProbe] = y + 0.0;
    probeZ[currentProbe] = z + 0.143764;
    shipFixed[currentProbe] = curShipFixed;
    currentProbe++;
  }

  const T boxOriginX = 0.790194 + 0.2; //updated origin!
  const T boxOriginY = -0.3;
  const T boxOriginZ = 0.125; //0.143764 - 0.018764
  const size_t boxNx = (size_t) (0.2/fineConverter.getConversionFactorLength()) + 1;
  const size_t boxNy = (size_t) (0.6/fineConverter.getConversionFactorLength()) + 1;
  const size_t boxNz = (size_t) (0.275/fineConverter.getConversionFactorLength()) + 1;

  const T midBoxOriginX = -1.5; //m
  const T midBoxOriginY = -fineConverter.getConversionFactorLength()/2.0;
  const T midBoxOriginZ = 0.125; //m

  const size_t midBoxNx = (size_t) (3.1/fineConverter.getConversionFactorLength()) + 1;
  const size_t midBoxNy = 2;
  const size_t midBoxNz = (size_t) (0.7/fineConverter.getConversionFactorLength()) + 1;

  T omega = converter.getLatticeRelaxationFrequency();
  T fineOmega = RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1);
  
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamicsFine(RefinedGrid3D<T,Lattice>::getOmegaForRefinementLevel(omega, 1), smagoConst);
  RefinedGrid3D<T,Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
  lattice.addChild(&bulkDynamicsFine, iXLengthFine, iYLengthFine, iZLengthFine, iXLeftBorderFine, iYBottomBorderFine, iZFrontBorderFine);
  lattice.children[0].setAdjacentToBoundary(false, false, false, false, true, false);
    
  unsigned ghostLayer[3] = {2,0,0};
  lattice.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

  std::cout << "Set up refinement..." << std::endl;
  lattice.setupRefinementGPU();
  lattice.setupMultilatticeGPU();

  std::cout << "Define boundaries.... "  << std::endl;
  lattice.applyMasks();
  defineBoundaries(*(lattice.getLatticePointer()), bulkDynamics, lattice._localSubDomain, lattice._refSubDomain);
  defineFineBoundaries(*(lattice.children[0].getLatticePointer()), bulkDynamicsFine, lattice.children[0]._localSubDomain, lattice.children[0]._refSubDomain);

  std::cout << "Initial STL read..." << std::endl;

  T globalShipOffsetX = (228.15+25)/50.0/converter.getConversionFactorLength();
  T globalShipOffsetY = 0.75*(T)resolution;
  T globalShipOffsetZ = (2.1)/50.0/converter.getConversionFactorLength() + 1;

  T shipOffsetX = globalShipOffsetX*2 - lattice.children[0]._globalOffset[0] + lattice.children[0]._interpLayerThickness;
  T shipOffsetY = globalShipOffsetY*2 - lattice.children[0]._globalOffset[1] + lattice.children[0]._interpLayerThickness;
  T shipOffsetZ = globalShipOffsetZ*2 - lattice.children[0]._globalOffset[2] + lattice.children[0]._interpLayerThickness;

  std::string stlAsString = slurp("NATOGD_1_50scale_updatedorigin.stl");

  static PostProcessingDynamics<T,Lattice,GradBoundaryProcessor3D<T,Lattice>> gradBoundary;
  lattice.children[0].getLatticePointer()->dynamicsContainer.definePlaceholderDynamics(&gradBoundary);

  size_t gradAllocationSpace = 500000; //set empirically
  lattice.children[0].getLatticePointer()->getDataHandler(&gradBoundary)->setExtraAllocationSpace(gradAllocationSpace);

  size_t formerBBAllocationSpace = gradAllocationSpace*2 + 1;
  memory_space::CudaDeviceHeap<size_t> localFormerBBPointsGPU(formerBBAllocationSpace);
  
  STLreader<stlT> stlReader("NATOGD_1_50scale_updatedorigin.stl", 0.2, 1.0/fineConverter.getConversionFactorLength()); //scale the STL to lattice units, set voxel size to 0.25 cell length
  stlReader.print();

  Octree<stlT> * parentOctree = stlReader.getTree();
  size_t origin[3] = {(size_t)(iXLengthFine*2*0.125), (size_t)(0), (size_t)3};
  size_t extend[3] = {(size_t)(iXLengthFine*2*0.75), (size_t)(iYLengthFine*2-1), (size_t)(iZLengthFine*2*0.6)};
  OctreeGPU<T,Lattice> gpuOctree(parentOctree, lattice.children[0]._localSubDomain, lattice.children[0]._refSubDomain, origin, extend);

  T heave = getShipHeavePosition(movementTimeOffset, fineConverter);
  T pitch = getShipPitchAngle(movementTimeOffset, fineConverter);
  T roll = getShipRollAngle(movementTimeOffset, fineConverter);
  Vec3<T> shipVelocity(0.0,0.0,getShipHeaveVelocity(movementTimeOffset, fineConverter));
  Vec3<T> shipAngularVelocity = getShipAngularVelocity(movementTimeOffset, fineConverter);
  AffineTransform<T,3> shipTransform = stlTransformCalc(shipOffsetX, shipOffsetY, shipOffsetZ, pitch, roll, heave);

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Initial STL read complete on rank: " << rank << std::endl;
  lattice.initDataArrays();

  size_t * gradDeviceIndices = lattice.children[0].getLatticePointer()->dynamicsContainer.getGPUHandler().getDynamicsCellIDsGPU(&gradBoundary);
  T ** gradDevicePostProcData = lattice.children[0].getLatticePointer()->dynamicsContainer.getGPUHandler().getPostProcDataGPU(&gradBoundary);

  size_t numFormerBBPoints = gpuOctree.stlInside(shipTransform, localFormerBBPointsGPU.get());
  size_t numGradPoints = gpuOctree.stlBoundary(shipTransform, gradDeviceIndices, gradDevicePostProcData, fineOmega, shipVelocity, shipAngularVelocity);

  std::cout << "num grad points: " << numGradPoints << " on rank: " << rank << std::endl;

  T reynoldsNumber = desiredFreestream*shipLength/kinematicViscosity;
  T latticeVel = converter.getLatticeVelocity(desiredFreestream);
  int simSteps = converter.getLatticeTime(simTime);
  printf("Reynolds number: %f, physical freestream: %f, lattice freestream: %f, desired lattice freestream: %f\n", reynoldsNumber, desiredFreestream, latticeVel, desiredLatticeVel);

  T vel[3] = {latticeVel, 0, 0};
  lattice.iniEquilibrium(1.0, vel);
  lattice.copyLatticesToGPU();

  std::cout << "finished iniequil" << std::endl;  

  RefinedGridVTKManager3D<T,Lattice> writer("movingNATOGD", lattice);
  writer.addFunctor<BlockLatticeDensity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  writer.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  writer.addFunctor<BlockLatticeIndex3D<T,Lattice>>();

  RefinedGridVTKManager3D<T,Lattice> limWriter("movingNATOGD_limited", lattice);
  limWriter.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  limWriter.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  size_t limWriterOrigin[3] = {(size_t)(iXRightBorder*0.23), (size_t)(iYTopBorder*0.5)-1, 0};
  size_t limWriterExtend[3] = {(size_t)(iXRightBorder*0.6), (size_t)(iYTopBorder*0.5)+1, (size_t)(iZBackBorder*0.55)};

  RefinedGrid3D<T,Lattice> shipFixedExtractLattice(boxNx, boxNy, boxNz, &bulkDynamicsFine, fineOmega);
  shipFixedExtractLattice.setupRefinementGPU();
  RefinedGridVTKManager3D<T,Lattice> shipFixedExtractWriter("shipFixedExtractBox", shipFixedExtractLattice, fineConverter.getConversionFactorLength());
  shipFixedExtractWriter.addFunctor<BlockLatticePhysVelocity3D<T,Lattice>>(0, fineConverter);

  RefinedGrid3D<T,Lattice> shipFixedMidExtractLattice(midBoxNx, midBoxNy, midBoxNz, &bulkDynamicsFine, fineOmega);
  shipFixedMidExtractLattice.setupRefinementGPU();
  RefinedGridVTKManager3D<T,Lattice> shipFixedMidExtractWriter("shipFixedMidExtractBox", shipFixedMidExtractLattice, fineConverter.getConversionFactorLength());
  shipFixedMidExtractWriter.addFunctor<BlockLatticePhysVelocity3D<T,Lattice>>(0, fineConverter);

  singleton::directories().setOutputDir("/data/ae-jral/sashok6/movingNATOGD_validationCase7/");

  // writer.write(0);
  limWriter.write(0, limWriterOrigin, limWriterExtend);

  if (rank == 0) {
    stlViz(stlAsString, converter, globalShipOffsetX, globalShipOffsetY, globalShipOffsetZ, "NATOGD_STL", pitch, roll, heave*0.5, 0);
    stlViz(stlAsString, converter, globalShipOffsetX, globalShipOffsetY, globalShipOffsetZ, "NATOGD_STL_lim", pitch, roll, heave*0.5, 0);
  }

  std::ofstream shipMovementOutputCSV("shipMovementOutputCSV.csv");
  shipMovementOutputCSV << "Timestep, sim time (s), movement time (s), pitch, roll, heave\n";

  std::ofstream probeOutputCSV("probeOutputCSV.csv");
  probeOutputCSV << "Timestep, sim time (s), movement time (s), pitch, roll, heave, probe_i velx, probe_i vely, probe_i velz...\n";


  //initialize refined movement functor
  std::vector<void*> argVector;
  argVector.push_back((void*) &fineConverter);
  argVector.push_back((void*) &heave);
  argVector.push_back((void*) &pitch);
  argVector.push_back((void*) &roll);
  argVector.push_back((void*) &shipVelocity);
  argVector.push_back((void*) &shipAngularVelocity);
  argVector.push_back((void*) &shipTransform);
  argVector.push_back((void*) &numFormerBBPoints);
  argVector.push_back((void*) &numGradPoints);
  argVector.push_back((void*) lattice.children[0].getLatticePointer()->getDataHandler(&gradBoundary));
  argVector.push_back((void*) &shipOffsetX);
  argVector.push_back((void*) &shipOffsetY);
  argVector.push_back((void*) &shipOffsetZ);
  argVector.push_back((void*) &gpuOctree);
  argVector.push_back((void*) localFormerBBPointsGPU.get());
  argVector.push_back((void*) gradDeviceIndices);
  argVector.push_back((void*) gradDevicePostProcData);
  argVector.push_back((void*) &(lattice.children[0]));

  lattice.children[0].registerPostCollideAndStreamFunctor(&moveShipFunctor<T,Lattice>, argVector);

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Starting timesteps..." << std::endl;
  util::Timer<T> timer(simSteps, lattice.calculateTotalLatticeUnits());
  timer.start();

  T cAndSMs = 0;
  T movementMs = 0;

  for (unsigned int iSteps = 1; iSteps <= simSteps; ++iSteps)
  {
    tic(0);
    std::vector<size_t> cpuGradIndices(numGradPoints);
    lattice.children[0].getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuGradIndices);
    lattice.collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    cpuGradIndices.resize(0);
    lattice.children[0].getLatticePointer()->getDataHandler(&gradBoundary)->overwriteCellIDs(cpuGradIndices);
    toc(0, 500, "Collide and stream");

    T movementTime = converter.getPhysTime(iSteps) - movementStart + movementTimeOffset;

    //Print progress to console
    if (iSteps % 10 == 0) {
      if (rank == 0) {
        timer.print(iSteps, 2);      
        std::cout << "Pitch: " << pitch << ", roll: " << roll << ", heave: " << heave << std::endl;
      }      
    }

    //WHOLE VOLUME VISUALIZATION (run only once at beginning and once near end of simulation)
    if (iSteps % converter.getLatticeTime(simTime*0.95) == 0) {
      lattice.copyLatticesToCPU();
      writer.write(iSteps);

      if (rank == 0) {
        stlViz(stlAsString, converter, globalShipOffsetX, globalShipOffsetY, globalShipOffsetZ, "NATOGD_STL", pitch, roll, heave*0.5, iSteps);
      }
    }


    //25Hz sample
    if (converter.getPhysTime(iSteps) >= nextSampleTime) {
      tic(7);
      nextSampleTime += sampleInterval;

      lattice.copyVizToCPU(false, true, false, false);
      limWriter.write(iSteps, limWriterOrigin, limWriterExtend);

      if (rank == 0) {
        stlViz(stlAsString, converter, globalShipOffsetX, globalShipOffsetY, globalShipOffsetZ, "NATOGD_STL_lim", pitch, roll, heave*0.5, iSteps);
        shipMovementOutputCSV << iSteps << ", " << converter.getPhysTime(iSteps) << ", " << movementTime << ", " << pitch << ", " << roll << ", " << heave << "\n";
        shipMovementOutputCSV.flush();
      }

      #ifdef __CUDACC__
      probeMeasurements<T,Lattice><<<numProbes/256+1, 256>>>(lattice.children[0].getLatticePointer()->cellData->gpuGetFluidData(), &probeX[0], &probeY[0], &probeZ[0], &shipFixed[0],
                                                              lattice.children[0]._localSubDomain, fineConverter.getConversionFactorLength(), fineConverter.getConversionFactorVelocity(),
                                                              shipOffsetX, shipOffsetY, shipOffsetZ, roll, pitch, heave, &velocityX[0], &velocityY[0], &velocityZ[0], numProbes);
      #endif
      cudaDeviceSynchronize();
      CudaHandleError(cudaPeekAtLastError(), "main", 829);
      MPI_Barrier(MPI_COMM_WORLD);

      std::vector<T> MPIReducedVelocityX(numProbes);
      std::vector<T> MPIReducedVelocityY(numProbes);
      std::vector<T> MPIReducedVelocityZ(numProbes);

      MPI_Reduce(&velocityX[0], &MPIReducedVelocityX[0], numProbes, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      MPI_Reduce(&velocityY[0], &MPIReducedVelocityY[0], numProbes, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      MPI_Reduce(&velocityZ[0], &MPIReducedVelocityZ[0], numProbes, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      if (rank == 0) {
        probeOutputCSV << iSteps << ", " << converter.getPhysTime(iSteps) << ", " << movementTime << ", " << pitch << ", " << roll << ", " << heave;
        for (int i = 0; i < numProbes; i++)
          probeOutputCSV << ", " << MPIReducedVelocityX[i] << ", " << MPIReducedVelocityY[i] << ", " << MPIReducedVelocityZ[i];

        probeOutputCSV << "\n";
        probeOutputCSV.flush();
      }

      #ifdef __CUDACC__
      shipFixedBoxMeasurements<T,Lattice><<<boxNx*boxNy*boxNz/256+1, 256>>>(lattice.children[0].getLatticePointer()->cellData->gpuGetFluidData(), boxOriginX, boxOriginY, boxOriginZ, boxNx, boxNy, boxNz,
                                                              lattice.children[0]._localSubDomain, fineConverter.getConversionFactorLength(), fineConverter.getConversionFactorVelocity(),
                                                              shipOffsetX, shipOffsetY, shipOffsetZ, roll, pitch, heave, shipFixedExtractLattice.getLatticePointer()->cellData->gpuGetFluidData());
      shipFixedBoxMeasurements<T,Lattice><<<midBoxNx*midBoxNy*midBoxNz/256+1, 256>>>(lattice.children[0].getLatticePointer()->cellData->gpuGetFluidData(), midBoxOriginX, midBoxOriginY, midBoxOriginZ, midBoxNx, midBoxNy, midBoxNz,
                                                              lattice.children[0]._localSubDomain, fineConverter.getConversionFactorLength(), fineConverter.getConversionFactorVelocity(),
                                                              shipOffsetX, shipOffsetY, shipOffsetZ, roll, pitch, heave, shipFixedMidExtractLattice.getLatticePointer()->cellData->gpuGetFluidData());
      #endif
      cudaDeviceSynchronize();
      CudaHandleError(cudaPeekAtLastError(), "main", 1079);
      MPI_Barrier(MPI_COMM_WORLD);

      MPI_Reduce(shipFixedExtractLattice.getLatticePointer()->cellData->gpuGetFluidData()[Lattice<T>::uIndex+0], shipFixedExtractLattice.getLatticePointer()->cellData->getCellData()[Lattice<T>::uIndex+0], boxNx*boxNy*boxNz, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      MPI_Reduce(shipFixedExtractLattice.getLatticePointer()->cellData->gpuGetFluidData()[Lattice<T>::uIndex+1], shipFixedExtractLattice.getLatticePointer()->cellData->getCellData()[Lattice<T>::uIndex+1], boxNx*boxNy*boxNz, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      MPI_Reduce(shipFixedExtractLattice.getLatticePointer()->cellData->gpuGetFluidData()[Lattice<T>::uIndex+2], shipFixedExtractLattice.getLatticePointer()->cellData->getCellData()[Lattice<T>::uIndex+2], boxNx*boxNy*boxNz, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      MPI_Reduce(shipFixedMidExtractLattice.getLatticePointer()->cellData->gpuGetFluidData()[Lattice<T>::uIndex+0], shipFixedMidExtractLattice.getLatticePointer()->cellData->getCellData()[Lattice<T>::uIndex+0], midBoxNx*midBoxNy*midBoxNz, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      MPI_Reduce(shipFixedMidExtractLattice.getLatticePointer()->cellData->gpuGetFluidData()[Lattice<T>::uIndex+1], shipFixedMidExtractLattice.getLatticePointer()->cellData->getCellData()[Lattice<T>::uIndex+1], midBoxNx*midBoxNy*midBoxNz, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
      MPI_Reduce(shipFixedMidExtractLattice.getLatticePointer()->cellData->gpuGetFluidData()[Lattice<T>::uIndex+2], shipFixedMidExtractLattice.getLatticePointer()->cellData->getCellData()[Lattice<T>::uIndex+2], midBoxNx*midBoxNy*midBoxNz, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

      if (rank == 0) {
        shipFixedExtractWriter.write(iSteps);
        shipFixedMidExtractWriter.write(iSteps);
      }
      MPI_Barrier(MPI_COMM_WORLD);
      toc(7, 20, "total postprocess");
    } 
    MPI_Barrier(MPI_COMM_WORLD);
  }

  timer.stop();
  timer.printSummary();  
  shipMovementOutputCSV.close();
  probeOutputCSV.close();
}

int main()
{
  MultipleSteps(simTime);
  finalizeIPC();
  return 0;
}
