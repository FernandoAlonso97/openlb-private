/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  int iXLeftBorder = 0;
  int iXRightBorder = 149;
  int iYBottomBorder = 0; 
  int iYTopBorder = 49;
  int iZFrontBorder = 1; //shift floor up one grid
  int iZBackBorder = 29;

  // UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
  //   150, 0.1 * 1.0 / std::sqrt(3), 10., 15*3., 0.001, 1.225, 0);

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
    50, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  int iXLeftFineOffset = 40;
  int iYBottomFineOffset = 10;
  int iZFrontFineOffset = 1;
  int refinedRegionXCoarse = 70;
  int refinedRegionYCoarse = 30;
  int refinedRegionZCoarse = 6;

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(converter.getLatticeRelaxationFrequency(), 0.025);
  RefinedGrid3D<T, Lattice> parent(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, converter.getLatticeRelaxationFrequency());

  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> fineBulkDynamics(parent._childOmega, 0.025);
  parent.addChild(&fineBulkDynamics, refinedRegionXCoarse, refinedRegionYCoarse, refinedRegionZCoarse, iXLeftFineOffset, iYBottomFineOffset, iZFrontFineOffset);
  parent.children[0].setAdjacentToBoundary(false, false, false, false, true, false);

  int iXLeftBorderFine = 0;
  int iXRightBorderFine = refinementutil::getRefinedRegionXFineLength3D(refinedRegionXCoarse, refinedRegionYCoarse, refinedRegionZCoarse, 2)-1;
  int iYBottomBorderFine = 0;
  int iYTopBorderFine = refinementutil::getRefinedRegionYFineLength3D(refinedRegionXCoarse, refinedRegionYCoarse, refinedRegionZCoarse, 2)-1;
  int iZFrontBorderFine = 2;
  int iZBackBorderFine = refinementutil::getRefinedRegionZFineLength3D(refinedRegionXCoarse, refinedRegionYCoarse, refinedRegionZCoarse, 2)-1;

  unsigned ghostLayer[3] = {2,0,0};
  parent.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);
  
  int localiXLeftBorder = 0;
  int localiXRightBorder = parent._localSubDomain.localGridSize()[0]-1;
  int localiYBottomBorder = 0;
  int localiYTopBorder = parent._localSubDomain.localGridSize()[1]-1;
  int localiZFrontBorder = 0;
  int localiZBackBorder = parent._localSubDomain.localGridSize()[1]-1;

  parent.setupRefinementGPU();
  parent.setupMultilatticeGPU();

  parent.applyMasks();

  // prepareLattice
  auto *velocityMomenta = new BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>;
  auto *velocityPostProcessor = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, -1>(localiXLeftBorder, localiXLeftBorder, 1, localiYTopBorder - 1, 1, localiZFrontBorder-1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, VelocityBM, 0, -1, 0>, typename std::remove_reference<decltype(*velocityPostProcessor)>::type::PostProcessorType> velocityBCDynamics(converter.getLatticeRelaxationFrequency(), *velocityMomenta);

  auto *pressureMomenta = new BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>;
  auto *pressurePostProcessor = new PlaneFdBoundaryProcessorGenerator3D<T, Lattice, 0, 1>(localiXRightBorder, localiXRightBorder, 1, localiYTopBorder - 1, 1, localiZFrontBorder-1);
  static BGKdynamics<T, Lattice, BasicDirichletBM<T, Lattice, PressureBM, 0, 1, 0>, typename std::remove_reference<decltype(*pressurePostProcessor)>::type::PostProcessorType> pressureBCDynamics(converter.getLatticeRelaxationFrequency(), *pressureMomenta);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, iXLeftBorder, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &velocityBCDynamics);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder+1, iZFrontBorder+1, iXRightBorder, iYTopBorder-1, iZBackBorder-1, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &pressureBCDynamics);

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder+1, iZFrontBorder, iXRightBorder-1, iYTopBorder-1, iZFrontBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder+1, iZBackBorder, iXRightBorder-1, iYTopBorder-1, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZFrontBorder+1, iXRightBorder-1, iYBottomBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZFrontBorder+1, iXRightBorder-1, iYTopBorder, iZBackBorder-1, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZFrontBorder, iXRightBorder-1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZFrontBorder, iXRightBorder-1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYBottomBorder, iZBackBorder, iXRightBorder-1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder+1, iYTopBorder, iZBackBorder, iXRightBorder-1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, iXRightBorder, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgePYNBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, iXRightBorder, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgePYPBoundaryProcessor);

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, iXRightBorder, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgePNZBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, iXRightBorder, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgePPZBoundaryProcessor);

  if (parent._domainInfo->getLocalInfo().isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndexStart))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexStart[1], localIndexStart[2], &cornerPNNBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndexStart))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexStart[1], localIndexStart[2], &cornerPPNBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndexStart))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexStart[1], localIndexStart[2], &cornerPNPBoundaryProcessor);
  if (parent._domainInfo->getLocalInfo().isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndexStart))
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexStart[1], localIndexStart[2], &cornerPPPBoundaryProcessor);

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, 0, iXRightBorder, iYTopBorder, 0, localIndexStart, localIndexEnd)) //extra cell at z bottom
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());

  if (parent._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftFineOffset+1, iYBottomFineOffset+1, iZFrontBorder, iXLeftFineOffset+refinedRegionXCoarse-2, iYBottomFineOffset+refinedRegionYCoarse-2, iZFrontBorder, localIndexStart, localIndexEnd)) //extra cell at z bottom + 1 below fine domain
    parent.getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());

  if (parent.children[0]._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorderFine, iYBottomBorderFine, 0, iXRightBorderFine, iYTopBorderFine, 1, localIndexStart, localIndexEnd))
    parent.children[0].getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());

  if (parent.children[0]._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorderFine, iYBottomBorderFine, iZFrontBorderFine, iXRightBorderFine, iYTopBorderFine, iZFrontBorderFine, localIndexStart, localIndexEnd))
    parent.children[0].getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);

  if (parent.children[0]._domainInfo->getLocalInfo().isRegionLocalToValidGhostLayer(iXLeftBorderFine+50, iYBottomBorderFine+10, 2, iXRightBorderFine-50, iYTopBorderFine-10, 6, localIndexStart, localIndexEnd)) //rectangular obstacle
    parent.children[0].getLatticePointer()->defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());


  parent.initDataArrays();
  T vel[3] = {0.0, 0.0, 0.0};
  parent.iniEquilibrium(1.0, vel);
 
  RefinedGridVTKManager3D<T,Lattice> refinedVTKWriterCoarse("refinedSlipFloor", parent);
  refinedVTKWriterCoarse.addFunctor<BlockLatticeDensity3D<T,Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeIndex3D<T, Lattice>>();
  refinedVTKWriterCoarse.addFunctor<BlockLatticeFi<T, Lattice>>();

  singleton::directories().setOutputDir("./tmp/");

  size_t writeOrigin[3] = {20,5,5};
  size_t writeExtend[3] = {120,40,25};

  refinedVTKWriterCoarse.write(0);
  // refinedVTKWriterCoarse.write(0, writeOrigin, writeExtend);

  parent.copyLatticesToGPU();

  T leftFinalVelocity = 0.003;
    
  MPI_Barrier(MPI_COMM_WORLD);
  util::Timer<T> timer(10000, parent.getLatticePointer()->getNx() * parent.getLatticePointer()->getNy() * parent.getLatticePointer()->getNz());
  timer.start();
  for (unsigned int iSteps = 1; iSteps <= 10000; ++iSteps)
  {
    parent.getLatticePointer()->copyDataToCPU();
    for (int iX = iXLeftBorder; iX <= iXLeftBorder; ++iX) {
      for (int iY = iYBottomBorder+1; iY <= iYTopBorder-1; ++iY)
        for (int iZ = iZFrontBorder+1; iZ <= iZBackBorder-1; ++iZ)
        {
          Index3D localIndex;
          if (parent._domainInfo->getLocalInfo().isLocal(iX,iY,iZ,localIndex)) {
            T currentLeftVelocity = min((T)iSteps/(T)1000, 1.0) * leftFinalVelocity;
            T vel[3] = {currentLeftVelocity, 0.0, 0.0};
            parent.getLatticePointer()->defineRhoU(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], 1.0, vel);
        }
      }
    }
    parent.getLatticePointer()->copyDataToGPU();

    parent.collideAndStreamMultilatticeGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    
    if (iSteps % 100 == 0) {
      timer.print(iSteps, 2);
      parent.copyLatticesToCPU();
      refinedVTKWriterCoarse.write(iSteps);      
    }
    
  }
  timer.stop();
  timer.printSummary();

  
  // refinedVTKWriterCoarse.write(1, writeOrigin, writeExtend);

  cudaDeviceSynchronize();
  MPI_Finalize();
}

int main()
{
  const double simTime = 0.02;
  MultipleSteps(simTime);
  return 0;
}
