/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD2Q9LATTICE
typedef double T;

#include "olb2D.h"
#include "olb2D.hh"

#define Lattice ForcedD2Q9Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime)
{
  int gridFactor = 1;

  int iXLeftBorder = 0;
  int iXRightBorder = 127*gridFactor;
  int iYBottomBorder = 0;
  int iYTopBorder = 63*gridFactor;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.0);
  BlockLattice2D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);
 
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, RegularizedVelocityBM<T, Lattice, 0, -1, 0>, RegularizedBoundaryProcessor2D<T,Lattice,0,-1>> velocityBCDynamics(omega, 0.0);
  lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, &velocityBCDynamics);  //boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder, iYBottomBorder, omega);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, RegularizedPressureBM<T, Lattice, 0, 1, 0>, RegularizedBoundaryProcessor2D<T,Lattice,0,1>> pressureBCDynamics(omega, 0.0);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, &pressureBCDynamics);

  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, &instances::getBounceBack<T, Lattice>());
  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, &instances::getBounceBack<T, Lattice>());

  lattice.initDataArrays();

  // setBoundaryValues
  T leftFinalVelocity = 0.05;
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      T vel[2] = {leftFinalVelocity, 0};
      lattice.defineRhoU(iX, iX, iY, iY, 1., vel);
      lattice.iniEquilibrium(iX, iX, iY, iY, 1., vel);
    }
  }

  BlockVTKwriter2D<T> vtkWriter("poiseuilleTestGPU");
  BlockLatticeDensity2D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity2D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity2D<T, Lattice> physVelocityFunctor(lattice, converter);
  BlockLatticeForce2D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure2D<T, Lattice> physPressureFunctor(lattice, converter);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy());
  timer.start();

  lattice.copyDataToGPU();

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(1.0) == 0)
    {
      lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
  timer.printSummary();
}

int main()
{
  const double simTime = 50;
  MultipleSteps(simTime);
  return 0;
}
