/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

void MultipleSteps(const double simTime)
{
  int gridFactor = 1;

  int iXLeftBorder = 0;
  int iXRightBorder = 127*gridFactor;
  int iYBottomBorder = 0;
  int iYTopBorder = 63*gridFactor;
  int iZFrontBorder = 0;
  int iZBackBorder = 31*gridFactor;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      iYTopBorder, 0.1 * 1.0 / std::sqrt(3), 1., 1., 0.01, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.0);
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);
 
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, RegularizedVelocityBM<T, Lattice, 0, -1, 0>, RegularizedBoundaryProcessor3D<T,Lattice,0,-1>> velocityBCDynamics(omega, 0.0);
  lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder + 1, iZBackBorder - 1, &velocityBCDynamics);  //boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder, iYBottomBorder, omega);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, RegularizedPressureBM<T, Lattice, 0, 1, 0>, RegularizedBoundaryProcessor3D<T,Lattice,0,1>> pressureBCDynamics(omega, 0.0);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder + 1, iZBackBorder - 1, &pressureBCDynamics);

  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZBackBorder, &instances::getBounceBack<T, Lattice>());
  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZBackBorder, &instances::getBounceBack<T, Lattice>());
  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &instances::getBounceBack<T, Lattice>());
  lattice.defineDynamics(iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZBackBorder, iZBackBorder, &instances::getBounceBack<T, Lattice>());

  lattice.initDataArrays();

  // setBoundaryValues
  T leftFinalVelocity = 0.05;
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
        T vel[3] = {leftFinalVelocity, 0, 0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }

  BlockVTKwriter3D<T> vtkWriter("poiseuilleTestGPU");
  BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
  BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy());
  timer.start();

  lattice.copyDataToGPU();

  for (unsigned int iSteps = 1; iSteps <= converter.getLatticeTime(simTime); ++iSteps)
  {
    lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if (iSteps % converter.getLatticeTime(0.5) == 0)
      timer.print(iSteps, 2);

    if (iSteps % converter.getLatticeTime(1.0) == 0)
    {
      lattice.copyDataToCPU();
      vtkWriter.write(iSteps);
    }
  }

  timer.stop();
  timer.printSummary();
}

int main()
{
  const double simTime = 50;
  MultipleSteps(simTime);
  return 0;
}
