/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "communication/writeFunctorToGPU.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/readWriteFunctorsGPU.h"
#include <cmath>
#include <chrono>
#include <thread>
#include <cstdlib>
#include <fstream>
#include "io/gpuIOFunctor.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"

#define Lattice ForcedD3Q19Descriptor

#ifdef ENABLE_CUDA
#define MemSpace memory_space::CudaDeviceHeap
#else
#define MemSpace memory_space::HostHeap
#endif

using namespace olb;
using namespace olb::descriptors;

//Simulation Parameters
const T simTime = 100.0;

const T smagoConst = 0.025;

const T shipLength = 138.686;
const unsigned int resolution = 180;

T finalFreestreamVelocity = 15.0;
const T freestreamStartTime = 0.0;
const T freestreamRampUpTime = 20.0;
const T freestreamUpdateInterval = 0.02;

//Output parameters
bool outputVTKData = true;
bool outputDebugData = false;
const T vtkWriteInterval = 1.0;

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{

  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2];
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition =
      createInterpBoundaryCondition3D<T, Lattice, ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  // boundaryCondition->addVelocityBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, omega);
  // boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, omega);
  // boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, omega);
  //boundaryCondition->addExternalVelocityEdge2NN(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);
  //boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, omega);

  // boundaryCondition->addExternalVelocityCornerNNN(iXLeftBorder, iYBottomBorder, iZFrontBorder, omega);
  // boundaryCondition->addExternalVelocityCornerNPN(iXLeftBorder, iYTopBorder, iZFrontBorder, omega);
  // boundaryCondition->addExternalVelocityCornerNNP(iXLeftBorder, iYBottomBorder, iZBackBorder, omega);
  // boundaryCondition->addExternalVelocityCornerNPP(iXLeftBorder, iYTopBorder, iZBackBorder, omega);

  // boundaryCondition->addPressureBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, omega);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
  {
    for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
    {
      Index3D localIndex;
      if (domainInfo.isLocal(iXLeftBorder, iY, iZ, localIndex))
        boundaryCondition->addVelocityBoundary0N(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
      if (domainInfo.isLocal(iXRightBorder, iY, iZ, localIndex))
        boundaryCondition->addPressureBoundary0P(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
    }
  }
  for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
  {
    for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
    {
      Index3D localIndex;
      if (domainInfo.isLocal(iX, iYBottomBorder, iZ, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
      if (domainInfo.isLocal(iX, iYTopBorder, iZ, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &topFaceBoundaryProcessor);
    }
  }
  for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
  {
    for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
    {
      Index3D localIndex;
      if (domainInfo.isLocal(iX, iY, iZFrontBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &frontFaceBoundaryProcessor);
      if (domainInfo.isLocal(iX, iY, iZBackBorder, localIndex))
        lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &backFaceBoundaryProcessor);
    }
  }

  // //lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &bottomFaceBoundaryProcessor);
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T, Lattice>());
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &topFaceBoundaryProcessor);
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &frontFaceBoundaryProcessor);
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edgeXNNBoundaryProcessor);
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edgeXPNBoundaryProcessor);
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edgeXNPBoundaryProcessor);
  // lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edgeXPPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edgePYNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &edgePYPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &edgePNZBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &edgePPZBoundaryProcessor);

  for (unsigned iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; iX++)
  {
    Index3D localIndex;
    if (domainInfo.isLocal(iX, iYTopBorder, iZFrontBorder, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgeXPNBoundaryProcessor);
    if (domainInfo.isLocal(iX, iYBottomBorder, iZFrontBorder, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgeXNNBoundaryProcessor);
    if (domainInfo.isLocal(iX, iYTopBorder, iZBackBorder, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgeXPPBoundaryProcessor);
    if (domainInfo.isLocal(iX, iYBottomBorder, iZBackBorder, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgeXNPBoundaryProcessor);
  }

  for (unsigned iY = iYBottomBorder + 1; iY <= iYTopBorder - 1; iY++)
  {
    Index3D localIndex;
    if (domainInfo.isLocal(iXLeftBorder, iY, iZBackBorder, localIndex))
      boundaryCondition->addExternalVelocityEdge1PN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1PN);
    if (domainInfo.isLocal(iXLeftBorder, iY, iZFrontBorder, localIndex))
      boundaryCondition->addExternalVelocityEdge1NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge1NN);
    if (domainInfo.isLocal(iXRightBorder, iY, iZBackBorder, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgePYNBoundaryProcessor);
    if (domainInfo.isLocal(iXRightBorder, iY, iZFrontBorder, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgePYPBoundaryProcessor);
  }

  for (unsigned iZ = iZFrontBorder + 1; iZ <= iZBackBorder - 1; iZ++)
  {
    Index3D localIndex;
    if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZ, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgePNZBoundaryProcessor);
    if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZ, localIndex))
      boundaryCondition->addExternalVelocityEdge2NN(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NN);
    if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZ, localIndex))
      boundaryCondition->addExternalVelocityEdge2NP(localIndex[0], localIndex[0], localIndex[1], localIndex[1], localIndex[2], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edge2NP);
    if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZ, localIndex))
      lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &edgePPZBoundaryProcessor);
  }

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &cornerPNNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &cornerPPNBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &cornerPNPBoundaryProcessor);
  // lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &cornerPPPBoundaryProcessor);

  Index3D localIndex;
  if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    boundaryCondition->addExternalVelocityCornerNNN(localIndex[0], localIndex[1], localIndex[2], omega);
    //lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNN);
  if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNNBoundaryProcessor);
  if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    boundaryCondition->addExternalVelocityCornerNPN(localIndex[0], localIndex[1], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPN);
  if (domainInfo.isLocal(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    boundaryCondition->addExternalVelocityCornerNNP(localIndex[0], localIndex[1], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNNP);
  if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPNBoundaryProcessor);
  if (domainInfo.isLocal(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPNPBoundaryProcessor);
  if (domainInfo.isLocal(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    boundaryCondition->addExternalVelocityCornerNPP(localIndex[0], localIndex[1], localIndex[2], omega);
    // lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerNPP);
  if (domainInfo.isLocal(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &cornerPPPBoundaryProcessor);
}

template <template <typename> class Memory>
void MultipleSteps(CommunicationDataHandler<T, Lattice<T>, Memory> &commDataHandler, const SubDomainInformation<T, Lattice<T>> &refSubDomain)
{
  auto domainInfo = commDataHandler.domainInfo;

  int iXRightBorder = domainInfo.getLocalInfo().localGridSize()[0] - 1;
  int iYTopBorder = domainInfo.getLocalInfo().localGridSize()[1] - 1;
  int iZBackBorder = domainInfo.getLocalInfo().localGridSize()[2] - 1;

  std::cout << "SubDomain #" << domainInfo.localSubDomain << " iXRightBorder: " << iXRightBorder << std::endl;
  std::cout << "SubDomain #" << domainInfo.localSubDomain << " iYTopBorder: " << iYTopBorder << std::endl;
  std::cout << "SubDomain #" << domainInfo.localSubDomain << " iZBackBorder: " << iZBackBorder << std::endl;

  Index3D localIndex;
  bool domainContainsInflow = domainInfo.getLocalInfo().isLocal(0, 0, 0, localIndex);

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, 0.1 * 1.0 / std::sqrt(3), shipLength, 2 * finalFreestreamVelocity, 1.461E-5, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConst);

  std::cout << "Create BlockLattice..." << std::endl;
  BlockLattice3D<T, Lattice> lattice(commDataHandler.domainInfo.getLocalInfo(), &bulkDynamics);

  std::cout << "Define boundaries..." << std::endl;
  defineBoundaries(lattice, bulkDynamics, domainInfo.getLocalInfo(), refSubDomain);

  const T shipOffsetX = 0.75 * (T)resolution;
  const T shipOffsetY = 0;
  const T shipOffsetZ = (T)iZBackBorder / 2 + 0.5;

  std::cout << "Loading STL..." << std::endl;
  STLreader<T> stlReader("SFS2_STL_ASCII_updatedorigin.STL", converter.getConversionFactorLength(), 1.0);
  stlReader.print();

  std::cout << "Loaded! Assigning bounceback cells.." << std::endl;

  for (int iX = 0; iX <= refSubDomain.globalIndexEnd[0] - 1; iX++)
    for (int iY = 0; iY <= refSubDomain.globalIndexEnd[1] - 1; iY++)
      for (int iZ = 0; iZ <= refSubDomain.globalIndexEnd[2] - 1; iZ++)
      {
        T iXPhys = converter.getPhysLength((T)iX - shipOffsetX);
        T iYPhys = converter.getPhysLength((T)iY - shipOffsetY);
        T iZPhys = converter.getPhysLength((T)iZ - shipOffsetZ);
        T location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];

        stlReader(isInside, location);
        if (isInside[0])
        {
          std::cout << "Point inside: " << iX << ", " << iY << ", " << iZ << std::endl;
          Index3D localIndex;
          if (domainInfo.getLocalInfo().isLocal(iX, iY, iZ, localIndex))
            lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
        }
      }

  std::cout << "Init GPU data..." << std::endl;
  lattice.initDataArrays();

  std::cout << "Init equilibrium..." << std::endl;
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
      {
        T vel[3] = {0, 0, 0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }

  std::unique_ptr<WriteFunctorByRangeToGPU3D<T, Lattice, VelocityFunctor>> velocityWriter;
  if (domainContainsInflow)
  {
    velocityWriter = std::unique_ptr<WriteFunctorByRangeToGPU3D<T, Lattice, VelocityFunctor>>(new WriteFunctorByRangeToGPU3D<T, Lattice, VelocityFunctor>(lattice, 0, 0, 0, iYTopBorder, 0, iZBackBorder));
  }

  lattice.copyDataToGPU();
  std::cout << "Finished!" << std::endl;

#ifdef ENABLE_CUDA
  initalizeCommDataMultilatticeGPU(lattice, commDataHandler);
  ipcCommunication<T, Lattice<T>> communication(commDataHandler);
#else
  initalizeCommDataMultilattice(lattice, commDataHandler);
  NumaSwapCommunication<T, Lattice<T>, MemSpace> communication{commDataHandler};
#endif


  singleton::directories().setOutputDir("./tmp/");
  BlockVTKwriter3D<T> vtkWriter("sfs2MultiGPU_" + std::to_string(domainInfo.localSubDomain));
  BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
  BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
  BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy() * lattice.getNz());
  timer.start();

  PolynomialStartScale<T, T> freestreamStartScale(converter.getLatticeTime(freestreamRampUpTime), 1.0);

  unsigned int trimTime = converter.getLatticeTime(simTime);

  for (unsigned int trimStep = 0; trimStep <= converter.getLatticeTime(simTime); trimStep++)
  {
#ifdef ENABLE_CUDA
    collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#else
    collideAndStreamMultilattice<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>(lattice, commDataHandler, communication);
#endif

    if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
    {
      timer.update(trimStep);
      timer.printStep();
      lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

      if (outputVTKData)
      {
        lattice.copyDataToCPU();
        vtkWriter.write(trimStep);
      }
    }

    if (trimStep % converter.getLatticeTime(freestreamUpdateInterval) == 0 && trimStep >= converter.getLatticeTime(freestreamStartTime) && trimStep <= converter.getLatticeTime(freestreamStartTime + freestreamRampUpTime))
    {

      T frac[1];
      T iT[1] = {(T)trimStep - (T)converter.getLatticeTime(freestreamStartTime)};
      freestreamStartScale(frac, iT);
      T currentInletLatticeVelocity = converter.getLatticeVelocity(finalFreestreamVelocity * frac[0]);

      if (domainContainsInflow)
      {
        int iX = 0;
        for (int iY = 0; iY <= iYTopBorder; ++iY)
        {
          for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
          {
            T vel[3] = {currentInletLatticeVelocity, 0, 0};

            velocityWriter->setByGlobalIndex(iX, iY, iZ)[0] = vel[0];
            velocityWriter->setByGlobalIndex(iX, iY, iZ)[1] = vel[1];
            velocityWriter->setByGlobalIndex(iX, iY, iZ)[2] = vel[2];
          }
        }

        velocityWriter->transferAndWrite();
      }
    }
  }

  timer.stop();
}

int main()
{

  int iXRightBorder = 4 * resolution - 1;
  int iYTopBorder = 110 * resolution / 182 - 1;
  int iZBackBorder = 1.5 * resolution - 1;
  std::vector<int> limits = {iXRightBorder, iYTopBorder, iZBackBorder};

  int rank = initIPC();

  const SubDomainInformation<T, Lattice<T>> refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>((size_t)(iXRightBorder + 1), (size_t)(iYTopBorder + 1), (size_t)(iZBackBorder + 1), 0u, 1u);
  const DomainInformation<T, Lattice<T>> subDomainInfo = decomposeDomainAlongX<T, Lattice<T>>(refSubDomain, rank, getNoRanks());

  if (rank == 0)
  {
    std::cout << "REF SUBDOMAIN INFO" << std::endl;
    std::cout << refSubDomain;
    std::cout << "Domain Info" << std::endl;
    std::cout << subDomainInfo;
    std::cout << "####" << std::endl;
  }

  CommunicationDataHandler<T, Lattice<T>, MemSpace> commDataHandler = createCommunicationDataHandler<MemSpace>(subDomainInfo);

  std::cout << commDataHandler << std::endl;
  std::cout << "####################################" << std::endl;

  MultipleSteps(commDataHandler, refSubDomain);

  cudaDeviceSynchronize();
  MPI_Finalize();
  return 0;
}
