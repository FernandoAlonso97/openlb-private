/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "communication/writeFunctorToGPU.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/readWriteFunctorsGPU.h"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;


//Simulation Parameters
const T simTime = 100.0;

const T smagoConst = 0.03;

const T shipLength = 138.686;
const unsigned int resolution = 180;

T finalFreestreamVelocity = 15.0;
const T freestreamStartTime = 0.0;
const T freestreamRampUpTime = 20.0;
const T freestreamUpdateInterval = 0.02;

//Output parameters
bool outputVTKData = true;
bool outputDebugData = false;
const T vtkWriteInterval = 1.0;

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics) {

  int iXLeftBorder = 0;
  int iXRightBorder = lattice.getNx()-1;
  int iYBottomBorder = 0;
  int iYTopBorder = lattice.getNy()-1;
  int iZFrontBorder = 0;
  int iZBackBorder = lattice.getNz()-1;

  T omega = dynamics.getOmega();

  OnLatticeBoundaryCondition3D<T, Lattice> *boundaryCondition = 
    createInterpBoundaryCondition3D<T, Lattice, ForcedLudwigSmagorinskyBGKdynamics>(lattice);

  boundaryCondition->addVelocityBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, omega);
  boundaryCondition->addExternalVelocityEdge1NN(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, omega);
  boundaryCondition->addExternalVelocityEdge1PN(iXLeftBorder, iXLeftBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, omega);
  boundaryCondition->addExternalVelocityEdge2NN(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, omega);
  boundaryCondition->addExternalVelocityEdge2NP(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, omega);

  boundaryCondition->addExternalVelocityCornerNNN(iXLeftBorder, iYBottomBorder, iZFrontBorder, omega);
  boundaryCondition->addExternalVelocityCornerNPN(iXLeftBorder, iYTopBorder, iZFrontBorder, omega);
  boundaryCondition->addExternalVelocityCornerNNP(iXLeftBorder, iYBottomBorder, iZBackBorder, omega);
  boundaryCondition->addExternalVelocityCornerNPP(iXLeftBorder, iYTopBorder, iZBackBorder, omega);

  boundaryCondition->addPressureBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder+1, iZBackBorder-1, omega);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  //lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &bottomFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &instances::getBounceBack<T, Lattice>());
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &topFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &frontFaceBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edgeXNNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edgeXPNBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edgeXNPBoundaryProcessor);
  lattice.defineDynamics(iXLeftBorder+1, iXRightBorder-1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edgeXPPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZFrontBorder, iZFrontBorder, &edgePYNBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder+1, iYTopBorder-1, iZBackBorder, iZBackBorder, &edgePYPBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder+1, iZBackBorder-1, &edgePNZBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder+1, iZBackBorder-1, &edgePPZBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &cornerPNNBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &cornerPPNBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &cornerPNPBoundaryProcessor);
  lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &cornerPPPBoundaryProcessor);
}


void MultipleSteps()
{
  int iXRightBorder = 4*resolution-1;
  int iYTopBorder = 110*resolution/182-1;
  int iZBackBorder = 1.5*resolution-1;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, 0.1 * 1.0 / std::sqrt(3), shipLength, 2*finalFreestreamVelocity, 1.461E-5, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, smagoConst);

  std::cout << "Create BlockLattice..." << std::endl;
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

  std::cout << "Define boundaries..." << std::endl;
  defineBoundaries(lattice, bulkDynamics);

  const T shipOffsetX = 0.75*(T)resolution;
  const T shipOffsetY = 0;
  const T shipOffsetZ = (T)iZBackBorder/2+0.5;


  std::cout << "Loading STL..." << std::endl;
  STLreader<T> stlReader("SFS2_STL_ASCII_updatedorigin.STL", converter.getConversionFactorLength(), 1.0);
  stlReader.print();

  std::cout << "Loaded! Assigning bounceback cells.." << std::endl;

  for (int iX = 0; iX <= iXRightBorder; iX++)
    for (int iY = 0; iY <= iYTopBorder; iY++)
      for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
        T iXPhys = converter.getPhysLength((T)iX-shipOffsetX);
        T iYPhys = converter.getPhysLength((T)iY-shipOffsetY);
        T iZPhys = converter.getPhysLength((T)iZ-shipOffsetZ);
        T location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];
        stlReader(isInside, location);
        if (isInside[0]) {
          std::cout << "Point inside: " << iX << ", " << iY << ", " << iZ << std::endl;
          lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>());
        }

      }

  std::cout << "Init GPU data..." << std::endl;
  lattice.initDataArrays();


  std::cout << "Init equilibrium..." << std::endl;
  for (int iX = 0; iX <= iXRightBorder; ++iX)
  {
    for (int iY = 0; iY <= iYTopBorder; ++iY)
    {
      for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
        T vel[3] = {0, 0, 0};
        lattice.defineRhoU(iX, iX, iY, iY, iZ, iZ, 1., vel);
        lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
      }
    }
  }


  WriteFunctorByRangeToGPU3D<T, Lattice, VelocityFunctor> velocityWriter(lattice, 0, 0, 0, iYTopBorder, 0, iZBackBorder);

  lattice.copyDataToGPU();

  BlockVTKwriter3D<T> vtkWriter("sfs2SingleGPU");
  BlockLatticeDensity3D<T, Lattice> densityFunctor(lattice);
  BlockLatticeVelocity3D<T, Lattice> velocityFunctor(lattice);
  BlockLatticePhysVelocity3D<T, Lattice> physVelocityFunctor(lattice, 0, converter);
  BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
  BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
  BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(densityFunctor);
  vtkWriter.addFunctor(velocityFunctor);
  vtkWriter.addFunctor(physVelocityFunctor);
  vtkWriter.addFunctor(forceFunctor);
  vtkWriter.addFunctor(physPressureFunctor);
  vtkWriter.addFunctor(fluidMaskFunctor);
  vtkWriter.write(0);

  util::Timer<T> timer(converter.getLatticeTime(simTime), lattice.getNx() * lattice.getNy() * lattice.getNz());
  timer.start();

  PolynomialStartScale<T, T> freestreamStartScale(converter.getLatticeTime(freestreamRampUpTime), 1.0);

  unsigned int trimTime = converter.getLatticeTime(simTime);

  for (unsigned int trimStep = 0; trimStep <= converter.getLatticeTime(simTime); trimStep++)
  {
    lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

    if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0){
      timer.update(trimStep);
      timer.printStep();
      lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

      if (outputVTKData) {
        lattice.copyDataToCPU();
        vtkWriter.write(trimStep);
      }
    }

    if (trimStep % converter.getLatticeTime(freestreamUpdateInterval) == 0 && trimStep >= converter.getLatticeTime(freestreamStartTime) && trimStep <= converter.getLatticeTime(freestreamStartTime+freestreamRampUpTime)) {

      //lattice.copyDataToCPU();

      T frac[1];
      T iT[1] = {(T)trimStep - (T) converter.getLatticeTime(freestreamStartTime)};
      freestreamStartScale(frac, iT);
      T currentInletLatticeVelocity = converter.getLatticeVelocity(finalFreestreamVelocity*frac[0]);

      int iX = 0;
      for (int iY = 0; iY <= iYTopBorder; ++iY) {
        for (int iZ = 0; iZ <= iZBackBorder; ++iZ) {
          T vel[3] = {currentInletLatticeVelocity, 0, 0};

          velocityWriter.setByGlobalIndex(iX, iY, iZ)[0] = vel[0];
          velocityWriter.setByGlobalIndex(iX, iY, iZ)[1] = vel[1];
          velocityWriter.setByGlobalIndex(iX, iY, iZ)[2] = vel[2];
        }
      }

      velocityWriter.transferAndWrite();
    }

  }

  timer.stop();
}

int main()
{
  MultipleSteps();
  return 0;
}
