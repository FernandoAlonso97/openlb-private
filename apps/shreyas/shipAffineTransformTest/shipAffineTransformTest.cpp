/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include "core/affineTransform.h"

#define Lattice ForcedD3Q19Descriptor

using namespace olb;

int main()
{
  Mat3<T> rotMat(0.5236, 0, 0, Rotation::ToBody{}); //corresponds to rotx(-30)
  std::cout << rotMat << std::endl;

  rotMat = Mat3<T>(0.5236, 0, 0, Rotation::ToSpace{}); //corresponds to rotx(30)
  std::cout << rotMat << std::endl;

  rotMat = Mat3<T>(0, 0.5236, 0, Rotation::ToSpace{}); //corresponds to roty(30)
  std::cout << rotMat << std::endl;

  rotMat = Mat3<T>(0, 0.0, 0.5236, Rotation::ToSpace{}); //corresponds to rotz(30)
  std::cout << rotMat << std::endl;

  rotMat = Mat3<T>(0.5236, 0.2618, 0.0, Rotation::ToSpace{}); //corresponds to roty(15)*rotx(30) - THIS IS WHAT WE NEED FOR THE STL
  std::cout << rotMat << std::endl;
  Vec3<T> testPoint(1,1,0);
  AffineTransform<T,3> testTransform(rotMat);
  transformInPlace(testPoint, testTransform);
  std::cout << testPoint;

  rotMat = Mat3<T>(0.5236, 0.2618, 0.0, Rotation::ToBody{}); //corresponds to inv(roty(15)*rotx(30)) or alternatively rotx(-30)*roty(-15)
  std::cout << rotMat << std::endl;

  rotMat = Mat3<T>(0.7854, 0, 0, Rotation::ToBody{});

  AffineTransform<T,3> transform(rotMat);
  Vec3<T> translation(0,0,0.5);
  transform.applyToLeft(translation);

  Vec3<T> samplePoint(10,1,1);
  transformInPlace(samplePoint, transform);
  std::cout << samplePoint << std::endl;

  rotMat = Mat3<T>(0.7854, 0, 0, Rotation::ToSpace{});
  AffineTransform<T,3> invTransform(rotMat);
  Vec3<T> invTranslation(0,0,-0.5);
  invTransform.applyToRight(invTranslation);
  transformInPlace(samplePoint, invTransform);
  std::cout << samplePoint << std::endl;

  return 0;
}
