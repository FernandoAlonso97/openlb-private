/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"

#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGrid3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.h"
#include "contrib/domainDecomposition/localGridRefinement/refinedGridVTKManager3D.hh"
#include "contrib/domainDecomposition/localGridRefinement/refinementUtil3D.h"

#include <omp.h>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

const unsigned int resolution = 200;
const T desiredLatticeVel = 0.05;
const T desiredFreestream = 20.5778; //m/s
const T shipLength = 150.0; //m
const T kinematicViscosity = 1.48E-5;
const T physDensity = 1.225;
const T smagoConst = 0.03;

const T movementLength = 10; //m

/** Read file into string. */
// https://stackoverflow.com/questions/18816126/c-read-the-whole-file-in-buffer
inline std::string slurp (const std::string& path) {
  std::ostringstream buf; 
  std::ifstream input (path.c_str()); 
  buf << input.rdbuf(); 
  return buf.str();
}

template <typename T, template <typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics, const SubDomainInformation<T, Lattice<T>> &domainInfo, const SubDomainInformation<T, Lattice<T>> &refDomain)
{
  int iXLeftBorder = refDomain.globalIndexStart[0];
  int iXRightBorder = refDomain.globalIndexEnd[0] - 1;
  int iYBottomBorder = refDomain.globalIndexStart[1];
  int iYTopBorder = refDomain.globalIndexEnd[1] - 1;
  int iZFrontBorder = refDomain.globalIndexStart[2];
  int iZBackBorder = refDomain.globalIndexEnd[2] - 1;

  T omega = dynamics.getOmega();

  static IniEquilibriumDynamics<T, Lattice> inletDynamics;

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> impedanceOut(omega, 0.025 * 500);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 0>> bottomFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 0>> topFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, -1>> frontFaceBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 0, 1>> backFaceBoundaryProcessor;

  Index3D localIndexStart;
  Index3D localIndexEnd;
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXLeftBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &impedanceOut);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder + 1, iXRightBorder - 1, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &bottomFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &topFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZFrontBorder, iXRightBorder - 1, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &frontFaceBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder + 1, iZBackBorder, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &backFaceBoundaryProcessor);

  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, -1>> edgeXNNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, -1>> edgeXPNBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, -1, 1>> edgeXNPBoundaryProcessor;
  static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 0, 1, 1>> edgeXPPBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZFrontBorder, iXRightBorder - 1, iYTopBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZFrontBorder, iXRightBorder - 1, iYBottomBorder, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNNBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYTopBorder, iZBackBorder, iXRightBorder - 1, iYTopBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXPPBoundaryProcessor);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder + 1, iYBottomBorder, iZBackBorder, iXRightBorder - 1, iYBottomBorder, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &edgeXNPBoundaryProcessor);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, -1>> edgePYNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 0, 1>> edgePYPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 0>> edgePNZBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 0>> edgePPZBoundaryProcessor;

  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZBackBorder, iXLeftBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder + 1, iZFrontBorder, iXLeftBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZBackBorder, iXRightBorder, iYTopBorder - 1, iZBackBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder + 1, iZFrontBorder, iXRightBorder, iYTopBorder - 1, iZFrontBorder, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder + 1, iXRightBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder + 1, iXLeftBorder, iYBottomBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder + 1, iXLeftBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder + 1, iXRightBorder, iYTopBorder, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &inletDynamics);

  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, -1>> cornerPNNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, -1>> cornerPPNBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, -1, 1>> cornerPNPBoundaryProcessor;
  // static PostProcessingDynamics<T, Lattice, SlipBoundaryProcessor3D<T, Lattice, 1, 1, 1>> cornerPPPBoundaryProcessor;

  Index3D localIndex;
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZFrontBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYBottomBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXLeftBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);
  if (domainInfo.isLocalToValidGhostLayer(iXRightBorder, iYTopBorder, iZBackBorder, localIndex))
    lattice.defineDynamics(localIndex[0], localIndex[1], localIndex[2], &inletDynamics);

  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics(omega, smagoConst * 500);
  static ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> spongeDynamics2(omega, smagoConst * 10);

  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 10, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 1, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics);
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder - 50, iYBottomBorder + 1, iZFrontBorder + 1, iXRightBorder - 11, iYTopBorder - 1, iZBackBorder - 1, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &spongeDynamics2);

  //ship floor region
  if (domainInfo.isRegionLocalToValidGhostLayer(iXRightBorder*0.28, iYTopBorder*0.5-resolution*0.085, 0, iXRightBorder*0.47, iYTopBorder*0.5+resolution*0.085, 0, localIndexStart, localIndexEnd))
    lattice.defineDynamics(localIndexStart[0], localIndexEnd[0], localIndexStart[1], localIndexEnd[1], localIndexStart[2], localIndexEnd[2], &instances::getBounceBack<T,Lattice>());
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlUpdate(STLreader<stlT>& stlReader, UnitConverter<T,Lattice> converter, stlT offsetX, stlT offsetY, stlT offsetZ, std::vector<size_t>& localCheckPoints, SubDomainInformation<T,Lattice<T>>& localSubDomain, SubDomainInformation<T,Lattice<T>>& refSubDomain, std::vector<size_t>& previousBBPoints, std::vector<size_t>& newBBPoints, std::vector<size_t>& formerBBPoints, std::vector<size_t>& previousGradPoints, std::vector<size_t>& gradPoints, std::vector<size_t>& localGradPoints, std::vector<std::array<T,1+3+1+3+3+Lattice<T>::q>>& localGradData) {

  std::vector<size_t> BBPoints;

  #pragma omp parallel 
  {
    std::vector<size_t> threadLocalBBPoints;
    #pragma omp for nowait
    for (int i = 0; i < localCheckPoints.size(); i++) {
      size_t localCellIndex = localCheckPoints.at(i);
      size_t indices[3];
      util::getCellIndices3D(localCellIndex, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], indices);
      Index3D localIndex(indices[0], indices[1], indices[2], localSubDomain.localSubDomain);
      Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);

      stlT iXPhys = converter.getPhysLength((stlT)globalIndex[0]-offsetX);
      stlT iYPhys = converter.getPhysLength((stlT)globalIndex[1]-offsetY);
      stlT iZPhys = converter.getPhysLength((stlT)globalIndex[2]-offsetZ);
      stlT location[3] = {iXPhys, iYPhys, iZPhys};
      bool inside[1];
      stlReader(inside, location);
      if (inside[0]) {
          threadLocalBBPoints.push_back(localCellIndex);
      }
      
    }

    #pragma omp critical
    {
      BBPoints.insert(BBPoints.end(), std::make_move_iterator(threadLocalBBPoints.begin()), std::make_move_iterator(threadLocalBBPoints.end()));
    }

  }

  std::sort(BBPoints.begin(), BBPoints.end());

  std::set_difference(previousBBPoints.begin(), previousBBPoints.end(), BBPoints.begin(), BBPoints.end(), std::inserter(formerBBPoints, formerBBPoints.begin()));
  std::set_difference(BBPoints.begin(), BBPoints.end(), previousBBPoints.begin(), previousBBPoints.end(), std::inserter(newBBPoints, newBBPoints.begin()));

  std::cout << "Size of BBPoints: " << BBPoints.size() << std::endl;

  #pragma omp parallel
  {
    std::vector<size_t> threadLocalGradPoints;
    #pragma omp for nowait
    for (int i = 0; i < previousGradPoints.size(); i++) {
      
      size_t gradGlobalIndex = previousGradPoints.at(i);
      size_t indices[3];
      util::getCellIndices3D(gradGlobalIndex, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], indices);

      for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
        size_t testIndices[3] = {indices[0] + Lattice<T>::c(iPop,0), indices[1] + Lattice<T>::c(iPop,1), indices[2] + Lattice<T>::c(iPop,2)};
        if (testIndices[2] == 0)
          continue;
        
        stlT iXPhys = converter.getPhysLength((stlT)testIndices[0]-offsetX);
        stlT iYPhys = converter.getPhysLength((stlT)testIndices[1]-offsetY);
        stlT iZPhys = converter.getPhysLength((stlT)testIndices[2]-offsetZ);
        stlT location[3] = {iXPhys, iYPhys, iZPhys};
        bool inside[1];
        stlReader(inside, location);
        if (inside[0])
          continue;

        for (int nPop = 1; nPop < Lattice<T>::q; nPop++) {
          size_t neighborIndices[3] = {testIndices[0] + Lattice<T>::c(nPop,0), testIndices[1] + Lattice<T>::c(nPop,1), testIndices[2] + Lattice<T>::c(nPop,2)};
          stlT iXPhysN = converter.getPhysLength((stlT)neighborIndices[0]-offsetX);
          stlT iYPhysN = converter.getPhysLength((stlT)neighborIndices[1]-offsetY);
          stlT iZPhysN = converter.getPhysLength((stlT)neighborIndices[2]-offsetZ);
          stlT nLocation[3] = {iXPhysN, iYPhysN, iZPhysN};
          bool neighborInside[1];
          stlReader(neighborInside, nLocation);
          if (neighborInside[0]) {
            size_t neighborGlobalIndex = util::getCellIndex3D(testIndices[0], testIndices[1], testIndices[2], refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2]);
            threadLocalGradPoints.push_back(neighborGlobalIndex);
            break;
          }
        }
      }
    }

    #pragma omp critical
    {
      gradPoints.insert(gradPoints.end(), std::make_move_iterator(threadLocalGradPoints.begin()), std::make_move_iterator(threadLocalGradPoints.end()));
    }
  }

  std::sort(gradPoints.begin(), gradPoints.end());
  auto it = std::unique(gradPoints.begin(), gradPoints.end());
  gradPoints.resize(std::distance(gradPoints.begin(), it));


  for (int i = 0; i < gradPoints.size(); i++) {
    size_t gradGlobalIndex = gradPoints.at(i);
    size_t indices[3];
    util::getCellIndices3D(gradGlobalIndex, refSubDomain.localGridSize()[1], refSubDomain.localGridSize()[2], indices);
    Index3D localIndex;
    if (localSubDomain.isLocal(indices[0], indices[1], indices[2], localIndex)) {
      size_t localCellIndex = util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2]);
      localGradPoints.push_back(localCellIndex);
    }
  }

  localGradData.resize(localGradPoints.size());

  #pragma omp parallel for
  for (int i = 0; i < localGradPoints.size(); i++) {
    size_t gradLocalIndex = localGradPoints.at(i);
    size_t indices[3];
    util::getCellIndices3D(gradLocalIndex, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], indices);
    Index3D localIndex(indices[0], indices[1], indices[2], localSubDomain.localSubDomain);
    Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);

    stlT iXPhys = converter.getPhysLength((stlT)globalIndex[0]-offsetX);
    stlT iYPhys = converter.getPhysLength((stlT)globalIndex[1]-offsetY);
    stlT iZPhys = converter.getPhysLength((stlT)globalIndex[2]-offsetZ);

    std::array<T, 1+3+1+3+3+Lattice<T>::q> gradData;

    gradData[0] = converter.getLatticeRelaxationFrequency();
    gradData[1] = 0.0; // surface normal not needed;
    gradData[2] = 0.0; // surface normal not needed;
    gradData[3] = 0.0; // surface normal not needed;
    gradData[4] = 0.0; // surface normal not needed;

    gradData[8] = 0.0; //wall velocity TODO
    gradData[9] = 0.0; //wall velocity TODO
    gradData[10] = 0.0; //wall velocity TODO

    gradData[11] = -1.0; // zero population

    bool xNegInside = false, xPosInside = false, yNegInside = false, yPosInside = false, zNegInside = false, zPosInside = false;

    for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
      size_t neighborIndices[3] = {globalIndex[0] + Lattice<T>::c(iPop, 0), globalIndex[1] + Lattice<T>::c(iPop, 1), globalIndex[2] + Lattice<T>::c(iPop, 2)};
      stlT iXPhysN = converter.getPhysLength((stlT)neighborIndices[0]-offsetX);
      stlT iYPhysN = converter.getPhysLength((stlT)neighborIndices[1]-offsetY);
      stlT iZPhysN = converter.getPhysLength((stlT)neighborIndices[2]-offsetZ);
      stlT nLocation[3] = {iXPhysN, iYPhysN, iZPhysN};
      bool neighborInside[1];
      stlReader(neighborInside, nLocation);

      if (neighborInside[0]) {
        switch (iPop) { //reference latticeDescriptors.h for the directions, it's just hardcoded here
          case 1:
            xNegInside = true;
            break;
          case 10:
            xPosInside = true;
            break;
          case 2:
            yNegInside = true;
            break;
          case 11:
            yPosInside = true;
            break;
          case 3:
            zNegInside = true;
            break;
          case 12:
            zPosInside = true;
            break;
          default:
            break;
        }


        bool useBisection = false;
        stlT distance = 0.5;
        bool success = stlReader.distance(distance, Vector<stlT, 3>(iXPhys, iYPhys, iZPhys), Vector<stlT, 3>(Lattice<T>::c(iPop, 0), Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2)));
        distance = (distance/converter.getConversionFactorLength()) / sqrt(Lattice<T>::c(iPop,0)*Lattice<T>::c(iPop,0) + Lattice<T>::c(iPop,1)*Lattice<T>::c(iPop,1) + Lattice<T>::c(iPop,2)*Lattice<T>::c(iPop,2));
        if (!success || distance > 1.0 || distance < 0.0) {
          // std::cout << "Major problem calculating distance on pt " << indices[0] << " " << indices[1] << " " << indices[2] << ": distance is " << distance << "; using bisection method \n";
          // distance = (distance > 1.0 && distance < 1.2) ? 0.95 : 0.5;
          useBisection  = true;
        }

        //bisection approach
        if (useBisection) {
          stlT lowerLimit[3] = {iXPhys, iYPhys, iZPhys};
          stlT upperLimit[3] = {iXPhysN, iYPhysN, iZPhysN};
          bool averageInside[1];
          const int nBisections = 12;
          for (int nB = 0; nB < nBisections; nB++) {
            stlT averagePt[3] = {(lowerLimit[0]+upperLimit[0])/2.0, (lowerLimit[1]+upperLimit[1])/2.0, (lowerLimit[2]+upperLimit[2])/2.0};
            stlReader(averageInside, averagePt);
            if (averageInside[0]) {
              upperLimit[0] = averagePt[0];
              upperLimit[1] = averagePt[1];
              upperLimit[2] = averagePt[2];
            }
            else {
              lowerLimit[0] = averagePt[0];
              lowerLimit[1] = averagePt[1];
              lowerLimit[2] = averagePt[2];
            }
          }
          stlT distVector[3] = {lowerLimit[0]-iXPhys, lowerLimit[1]-iYPhys, lowerLimit[2]-iZPhys};
          distance = sqrt(distVector[0]*distVector[0] + distVector[1]*distVector[1] + distVector[2]*distVector[2]);
          distance = (distance/converter.getConversionFactorLength()) / sqrt(Lattice<T>::c(iPop,0)*Lattice<T>::c(iPop,0) + Lattice<T>::c(iPop,1)*Lattice<T>::c(iPop,1) + Lattice<T>::c(iPop,2)*Lattice<T>::c(iPop,2));
        }

        if (distance > 1.0 || distance < 0.0) {
          distance = 0.5;
        }

        gradData[11+Lattice<T>::opposite(iPop)] = distance;

      }
      else {
        gradData[11+Lattice<T>::opposite(iPop)] = -1.0;
      }
    } //end iPop loop

    if (xNegInside && xPosInside) //bad!
      gradData[5] = 0.0;
    else if (xNegInside)
      gradData[5] = 1.0;
    else if (xPosInside)
      gradData[5] = -1.0;
    else
      gradData[5] = 1.0;

    if (yNegInside && yPosInside) //bad!
      gradData[6] = 0.0;
    else if (yNegInside)
      gradData[6] = 1.0;
    else if (yPosInside)
      gradData[6] = -1.0;
    else
      gradData[6] = 1.0;

    if (zNegInside && zPosInside) //bad!
      gradData[7] = 0.0;
    else if (zNegInside)
      gradData[7] = 1.0;
    else if (zPosInside)
      gradData[7] = -1.0;
    else
      gradData[7] = 1.0;


    localGradData.at(i) = gradData;
  }
}

template <typename T, typename stlT, template <typename> class Lattice>
void stlViz(std::string stlFileContents, UnitConverter<T,Lattice> converter, stlT offsetX, stlT offsetY, stlT offsetZ, std::string outputName, int iT) {

  std::istringstream stlStream(stlFileContents);
  std::ofstream outputSTL(singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl");
  std::cout << "writing to file: " << singleton::directories().getVtkOutDir()+outputName+"_iT"+std::to_string(iT)+".stl" << std::endl;

  std::string line;
  while (std::getline(stlStream, line)) {

    if (line.find(std::string("vertex")) != std::string::npos) {
      //vertex transformation
      size_t pos = line.find(std::string("vertex"));
      std::string processedString = line.substr(0, pos+7);

      std::istringstream lineStream(line);
      std::string temp;
      lineStream >> temp;

      T xCoord; T yCoord; T zCoord;
      lineStream >> xCoord;
      lineStream >> yCoord;
      lineStream >> zCoord;

      xCoord = xCoord/converter.getConversionFactorLength() + offsetX;
      yCoord = yCoord/converter.getConversionFactorLength() + offsetY;
      zCoord = zCoord/converter.getConversionFactorLength() + offsetZ;

      outputSTL << processedString << xCoord << " " << yCoord << " " << zCoord << std::endl;
    }
    else {
      outputSTL << line << std::endl;
    }
  }


  outputSTL.close();
}

void MultipleSteps(const double simTime)
{
  int rank = initIPC();
  int noRanks = getNoRanks();

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, desiredLatticeVel, shipLength, desiredFreestream, kinematicViscosity, physDensity, 0);

  converter.print();

  const int iXLeftBorder = 0;
  const int iXRightBorder = 4.5*resolution-1;
  const int iYBottomBorder = 0;
  const int iYTopBorder = 1.5*resolution-1;
  const int iZFrontBorder = 0;
  const int iZBackBorder = 1.0*resolution-1;

  T omega = converter.getLatticeRelaxationFrequency();
  
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  // EntropicSmagorinskyMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>> bulkDynamics(omega, smagoConst);
  
  RefinedGrid3D<T,Lattice> lattice(iXRightBorder+1, iYTopBorder+1, iZBackBorder+1, &bulkDynamics, omega);
  
  unsigned ghostLayer[3] = {2,0,0};
  lattice.refinedDecomposeDomainEvenlyAlongAxis(rank, 0, noRanks, ghostLayer);

  std::cout << "Set up refinement..." << std::endl;
  lattice.setupRefinementGPU();
  lattice.setupMultilatticeGPU();

  std::cout << "Define boundaries.... "  << std::endl;
  lattice.applyMasks();
  defineBoundaries(*(lattice.getLatticePointer()), bulkDynamics, lattice._localSubDomain, lattice._refSubDomain);

  std::cout << "Initial STL read..." << std::endl;

  const stlT shipOffsetX = (228.15+25)/converter.getConversionFactorLength();
  const stlT shipOffsetY = 0.75*(T)resolution;
  const stlT shipOffsetZ = (2.1)/converter.getConversionFactorLength();

  std::string stlAsString = slurp("NATOGD_fullscale.stl");
  stlViz(stlAsString, converter, shipOffsetX, shipOffsetY, shipOffsetZ, "stlTest", 0);
  return;

  static PostProcessingDynamics<T,Lattice,GradBoundaryProcessor3D<T,Lattice>> gradBoundary;
  
  STLreader<stlT> stlReader("NATOGD_fullscale.stl", shipLength/800.0, 1.0);
  stlReader.print();

  std::set<size_t> outsideNeighborsSet;
  std::vector<std::vector<int>> outsideNeighborsReplacementPops; //only used for logging purposes
  std::vector<std::vector<T>> outsideNeighborsReplacementPopDistances; //only used for logging purposes
  std::vector<bool> outsideNeighborsBisections; //only used for logging purposes

  std::map<size_t, std::array<T,1+3+1+3+3+Lattice<T>::q>> outsideNeighborsData; //actually used for grad boundary

  std::vector<size_t> localUpdateCheckPoints;
  std::vector<size_t> currentLocalBBPoints;

  Index3D localIndex;
  
  //identify initial bounceback and grad points in global domain
  for (int iX = lattice._localSubDomain.globalIndexStart[0]; iX <= lattice._localSubDomain.globalIndexEnd[0]-1; iX++) {
    std::cout << "iX " << iX << std::endl;
    for (int iY = resolution*1.5*0.25; iY <= resolution*1.5*0.75; iY++)
      for (int iZ = 0; iZ <= resolution*0.5; iZ++) {
        stlT iXPhys = converter.getPhysLength((stlT)iX-shipOffsetX);
        stlT iYPhys = converter.getPhysLength((stlT)iY-shipOffsetY);
        stlT iZPhys = converter.getPhysLength((stlT)iZ-shipOffsetZ);
        stlT location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];
        stlReader(isInside, location);
        if (isInside[0]) {
          if (lattice._localSubDomain.isLocal(iX, iY, iZ, localIndex)) {
            lattice.getLatticePointer()->defineDynamics(localIndex[0], localIndex[1], localIndex[2], &instances::getBounceBack<T, Lattice>());
            if (iZ != 0) {
              size_t localCellIndex = util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], lattice._localSubDomain.localGridSize()[1], lattice._localSubDomain.localGridSize()[2]);
              currentLocalBBPoints.push_back(localCellIndex);
            }
          }

          bool allNeighborsInside = true;
          std::vector<int> neighborsNotInside;
          for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
            stlT iXNeighborPhys = converter.getPhysLength((stlT)iX-shipOffsetX+(stlT)Lattice<T>::c(iPop, 0));
            stlT iYNeighborPhys = converter.getPhysLength((stlT)iY-shipOffsetY+(stlT)Lattice<T>::c(iPop, 1));
            stlT iZNeighborPhys = converter.getPhysLength((stlT)iZ-shipOffsetZ+(stlT)Lattice<T>::c(iPop, 2));
            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
            bool neighborInside[1];
            stlReader(neighborInside, neighborLocation);
            allNeighborsInside = allNeighborsInside && neighborInside[0];
            if (!neighborInside[0])
              neighborsNotInside.push_back(iPop);
          }

          if (!allNeighborsInside) {

            for (int outsideNeighbor : neighborsNotInside) {
              if (iZ + Lattice<T>::c(outsideNeighbor, 2) <= 0)
                continue;
              size_t outsideNeighborCellIndex = util::getCellIndex3D(iX + Lattice<T>::c(outsideNeighbor, 0), iY + Lattice<T>::c(outsideNeighbor, 1), iZ + Lattice<T>::c(outsideNeighbor, 2), iYTopBorder+1, iZBackBorder+1);
              if (outsideNeighborCellIndex == 12689999) {
                std::cout << "iX, iY, iZ: " << iX << " " << iY << " " << iZ << std::endl;
              }
              auto result = outsideNeighborsSet.insert(outsideNeighborCellIndex);
            }
          }
          
        }

      }
  }


  int latticeMovementLength = std::ceil(movementLength/converter.getConversionFactorLength());
  int neighborIndex = 1;
  for (size_t outsideNeighbor : outsideNeighborsSet) {

    std::cout << "Neighbor " << neighborIndex << " of " << outsideNeighborsSet.size() << std::endl;
    neighborIndex++;

    size_t outsideNeighborIndices[3];
    util::getCellIndices3D(outsideNeighbor, iYTopBorder+1, iZBackBorder+1, outsideNeighborIndices);
    int outsideNeighborIndicesSigned[3] = {outsideNeighborIndices[0], outsideNeighborIndices[1], outsideNeighborIndices[2]};

    for (int iX = max(outsideNeighborIndicesSigned[0]-latticeMovementLength, 0); iX <= outsideNeighborIndicesSigned[0]+latticeMovementLength; iX++)
      for (int iY = max(outsideNeighborIndicesSigned[1]-latticeMovementLength, 0); iY <= outsideNeighborIndicesSigned[1]+latticeMovementLength; iY++)
        for (int iZ = max(outsideNeighborIndicesSigned[2]-latticeMovementLength, 1); iZ <= outsideNeighborIndicesSigned[2]+latticeMovementLength; iZ++) //ignore the ground plane in Z
          if (lattice._localSubDomain.isLocal(iX, iY, iZ, localIndex)) {
            size_t localCellIndex = util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], lattice._localSubDomain.localGridSize()[1], lattice._localSubDomain.localGridSize()[2]);
            localUpdateCheckPoints.push_back(localCellIndex);
          }

    if (neighborIndex % 5000 == 0) { //sort periodically for optimization
      std::sort(localUpdateCheckPoints.begin(), localUpdateCheckPoints.end());
      auto it = std::unique(localUpdateCheckPoints.begin(), localUpdateCheckPoints.end());
      localUpdateCheckPoints.erase(it, localUpdateCheckPoints.end());
    }
  }

  std::sort(localUpdateCheckPoints.begin(), localUpdateCheckPoints.end());
  auto it = std::unique(localUpdateCheckPoints.begin(), localUpdateCheckPoints.end());
  localUpdateCheckPoints.erase(it, localUpdateCheckPoints.end());
  std::vector<size_t> currentGradPoints(outsideNeighborsSet.begin(), outsideNeighborsSet.end());
  std::sort(currentLocalBBPoints.begin(), currentLocalBBPoints.end());

  std::cout << "initial STL read complete" << std::endl;
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Proceeding with an efficient STL read" << std::endl;
  util::Timer<T> stlTimer(1, lattice.calculateTotalLatticeUnits());
  stlTimer.start();

  std::cout << "Size of currentBBPoints: " << currentLocalBBPoints.size() << std::endl;

  std::vector<size_t> formerBBPoints;
  std::vector<size_t> newBBPoints;
  std::vector<size_t> newGradPoints;
  std::vector<size_t> localGradPoints;
  std::vector<std::array<T,1+3+1+3+3+Lattice<T>::q>> localGradData;

  stlUpdate<T,stlT,Lattice>(stlReader, converter, shipOffsetX, shipOffsetY, shipOffsetZ, localUpdateCheckPoints, lattice._localSubDomain, lattice._refSubDomain, currentLocalBBPoints, newBBPoints, formerBBPoints, currentGradPoints, newGradPoints, localGradPoints, localGradData);
  stlTimer.stop();
  stlTimer.printSummary();

  std::cout << "Size of formerBBPoints: " << formerBBPoints.size() << std::endl;
  std::cout << "Size of newBBPoints: " << newBBPoints.size() << std::endl;

  std::cout << "Size of currentGradPoints: " << currentGradPoints.size() << std::endl;
  std::cout << "Size of newGradPoints: " << newGradPoints.size() << std::endl;

  for (size_t gradIndex : localGradPoints) {
    size_t indices[3];
    util::getCellIndices3D(gradIndex, lattice._localSubDomain.localGridSize()[1], lattice._localSubDomain.localGridSize()[2], indices);
    lattice.getLatticePointer()->defineDynamics(indices[0], indices[1], indices[2], &gradBoundary);
  }

  stlTimer.stop();
  stlTimer.printSummary();

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Done preprocess rank " << rank << std::endl;
  lattice.initDataArrays();
  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Done init data arrays rank " << rank << std::endl;

  std::cout << " rank " << rank << " localGradPoints " << localGradPoints.size() << std::endl;
  if (localGradPoints.size() > 0) {
    auto gradDataHandler = lattice.getLatticePointer()->getDataHandler(&gradBoundary);
    auto gradCellIds = gradDataHandler->getCellIDs();
    auto gradBoundaryPostProcData = gradDataHandler->getPostProcData();
  
    for (int momentaIndex = 0; momentaIndex < localGradPoints.size(); momentaIndex++) {
      for (int dataIndex = 0; dataIndex < 1+3+1+3+3+Lattice<T>::q; dataIndex++) {
        gradBoundaryPostProcData[dataIndex][momentaIndex] = localGradData.at(momentaIndex).at(dataIndex);
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Done specifying BC data " << rank << std::endl;

  T reynoldsNumber = desiredFreestream*shipLength/kinematicViscosity;
  T latticeVel = converter.getLatticeVelocity(desiredFreestream);
  int simSteps = converter.getLatticeTime(simTime);
  printf("Reynolds number: %f, physical freestream: %f, lattice freestream: %f, desired lattice freestream: %f\n", reynoldsNumber, desiredFreestream, latticeVel, desiredLatticeVel);

  T vel[3] = {latticeVel, 0, 0};
  lattice.iniEquilibrium(1.0, vel);

  lattice.copyLatticesToGPU();

  std::cout << "finished iniequil" << std::endl;  

  RefinedGridVTKManager3D<T,Lattice> writer("staticNATOGD2", lattice);
  writer.addFunctor<BlockLatticeDensity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeVelocity3D<T, Lattice>>();
  writer.addFunctor<BlockLatticeFluidMask3D<T, Lattice>>();
  writer.addFunctor<BlockLatticePhysVelocity3D<T, Lattice>>(0, converter);
  writer.addFunctor<BlockLatticeIndex3D<T,Lattice>>();

  singleton::directories().setOutputDir("/data/ae-jral/sashok6/staticNATOGD/");

  writer.write(0);

  MPI_Barrier(MPI_COMM_WORLD);
  std::cout << "Starting timesteps..." << std::endl;
  util::Timer<T> timer(simSteps, lattice.calculateTotalLatticeUnits());
  timer.start();

  for (unsigned int iSteps = 1; iSteps <= simSteps; ++iSteps)
  {
   
    lattice.collideAndStreamMultilatticeGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
    // lattice.collideAndStreamMultilatticeGPU<EntropicSmagorinskyMRTdynamics<T, Lattice, BulkMomenta<T,Lattice>>>();

    if (iSteps % 100 == 0) {
      timer.print(iSteps, 2);

    }

    if (iSteps % converter.getLatticeTime(simTime/10.0) == 0) {
      lattice.copyLatticesToCPU();
      writer.write(iSteps);
    }
  }

  timer.stop();
  timer.printSummary();  
}

int main()
{
  const double simTime = 50;
  MultipleSteps(simTime);
  finalizeIPC();
  return 0;
}
