clear all; close all; clc;
%% 
outerBB = importdata("outerBounceBack.csv");
outerBB = outerBB.data;

outsideNeighbors = importdata("outsideNeighbors.csv");
outsideNeighbors = outsideNeighbors.data;

%%
figure();
plot3(outerBB(:,1), outerBB(:,2), outerBB(:,3), 'k.');
axis equal;
hold on;
plot3(outsideNeighbors(:,1), outsideNeighbors(:,2), outsideNeighbors(:,3), 'r.');
axis equal;
hold on;
quiver3(outsideNeighbors(:,1), outsideNeighbors(:,2), outsideNeighbors(:,3), outsideNeighbors(:,4), outsideNeighbors(:,5), outsideNeighbors(:,6), 'b');