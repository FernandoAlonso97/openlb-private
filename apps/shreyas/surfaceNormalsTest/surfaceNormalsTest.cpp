/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/
#define FORCEDD3Q19LATTICE
typedef double T;
typedef long double stlT;

#include "olb3D.h"
#include "olb3D.hh"
#include "communication/writeFunctorToGPU.h"
#include "communication/readFunctorFromGPU.h"
#include "communication/readWriteFunctorsGPU.h"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;


//Simulation Parameters


const T shipLength = 138.686;
const unsigned int resolution = 180;

//Output parameters
bool outputVTKData = true;
bool outputDebugData = false;


void MultipleSteps()
{
  int iXRightBorder = 4*resolution-1;
  int iYTopBorder = 110*resolution/182-1;
  int iZBackBorder = 1.5*resolution-1;

  UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
      resolution, 0.1 * 1.0 / std::sqrt(3), shipLength, 1, 1.461E-5, 1.225, 0);

  converter.print();

  T omega = converter.getLatticeRelaxationFrequency();
  ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.1);

  std::cout << "Create BlockLattice..." << std::endl;
  BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);


  const stlT shipOffsetX = 0.75*(T)resolution;
  const stlT shipOffsetY = 30;
  const stlT shipOffsetZ = (T)iZBackBorder/2+0.5;

  std::cout << "T numeric limit: " << std::numeric_limits<T>::epsilon() << std::endl;
  std::cout << "stlT numeric limit: " << std::numeric_limits<stlT>::epsilon() << std::endl;
  std::cout << "Loading STL..." << std::endl;
  STLreader<stlT> stlReader("SFS2_STL_ASCII_updatedorigin.STL", converter.getConversionFactorLength()*0.1, 1.0);
  stlReader.print();

  std::cout << "Loaded! Assigning bounceback cells.." << std::endl;

  std::ofstream outerBounceBack("outerBounceBack.csv");
  outerBounceBack << "iX, iY, iZ\n";

  std::ofstream outsideNeighbors("outsideNeighbors.csv");
  outsideNeighbors << "iX, iY, iZ, nX, nY, nZ\n";

  std::set<size_t> outsideNeighborsSet;
  std::vector<std::vector<int>> outsideNeighborsReplacementPops;

  for (int iX = 0; iX <= iXRightBorder; iX++)
    for (int iY = 0; iY <= iYTopBorder; iY++)
      for (int iZ = 0; iZ <= iZBackBorder; iZ++) {
        stlT iXPhys = converter.getPhysLength((stlT)iX-shipOffsetX);
        stlT iYPhys = converter.getPhysLength((stlT)iY-shipOffsetY);
        stlT iZPhys = converter.getPhysLength((stlT)iZ-shipOffsetZ);
        stlT location[3] = {iXPhys, iYPhys, iZPhys};
        bool isInside[1];
        stlReader(isInside, location);
        if (isInside[0]) {
          lattice.defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>());

          bool allNeighborsInside = true;
          std::vector<int> neighborsNotInside;
          for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
            stlT iXNeighborPhys = converter.getPhysLength((stlT)iX-shipOffsetX+(stlT)Lattice<T>::c(iPop, 0));
            stlT iYNeighborPhys = converter.getPhysLength((stlT)iY-shipOffsetY+(stlT)Lattice<T>::c(iPop, 1));
            stlT iZNeighborPhys = converter.getPhysLength((stlT)iZ-shipOffsetZ+(stlT)Lattice<T>::c(iPop, 2));
            stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
            bool neighborInside[1];
            stlReader(neighborInside, neighborLocation);
            allNeighborsInside = allNeighborsInside && neighborInside[0];
            if (!neighborInside[0])
              neighborsNotInside.push_back(iPop);
            // printf("iX = %d, iY = %d, iZ = %d, neighbor %d inside: %d\n", iX, iY, iZ, iPop, neighborInside[0]);
          }

          if (!allNeighborsInside) {
            outerBounceBack << iX << "," << iY << "," << iZ << "\n";

            for (int outsideNeighbor : neighborsNotInside) {
              size_t outsideNeighborCellIndex = util::getCellIndex3D(iX + Lattice<T>::c(outsideNeighbor, 0), iY + Lattice<T>::c(outsideNeighbor, 1), iZ + Lattice<T>::c(outsideNeighbor, 2), iYTopBorder+1, iZBackBorder+1);
              auto result = outsideNeighborsSet.insert(outsideNeighborCellIndex);
              if (result.second) {
                
                olb::Vector<stlT,3> neighborPoint(converter.getPhysLength((stlT)iX-shipOffsetX+(stlT)Lattice<T>::c(outsideNeighbor, 0)),
                                               converter.getPhysLength((stlT)iY-shipOffsetY+(stlT)Lattice<T>::c(outsideNeighbor, 1)),
                                               converter.getPhysLength((stlT)iZ-shipOffsetZ+(stlT)Lattice<T>::c(outsideNeighbor, 2)));
                olb::Vector<stlT,3U> normal = stlReader.evalSurfaceNormal(neighborPoint);
                if (iX + Lattice<T>::c(outsideNeighbor, 0) == 141 && iY + Lattice<T>::c(outsideNeighbor, 1) == 31 && iZ + Lattice<T>::c(outsideNeighbor, 2) == 136) {
                  normal = stlReader.evalSurfaceNormal(neighborPoint, true);
                }
                outsideNeighbors << iX + Lattice<T>::c(outsideNeighbor, 0) << ", " << iY + Lattice<T>::c(outsideNeighbor, 1) << ", " << iZ + Lattice<T>::c(outsideNeighbor, 2) << ", "
                                 << normal[0] << ", " << normal[1] << ", " << normal[2] << "\n";
              }
            }
          }
          
        }

      }

  outerBounceBack.close();
  outsideNeighbors.close();

  for (size_t outsideNeighbor : outsideNeighborsSet) {
    size_t indices[3];
    util::getCellIndices3D(outsideNeighbor, iYTopBorder+1, iZBackBorder+1, indices);
    std::vector<int> replacementPops;
    for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
      stlT iXNeighborPhys = converter.getPhysLength((stlT)indices[0]-shipOffsetX+(stlT)Lattice<T>::c(iPop,0));
      stlT iYNeighborPhys = converter.getPhysLength((stlT)indices[1]-shipOffsetY+(stlT)Lattice<T>::c(iPop,1));
      stlT iZNeighborPhys = converter.getPhysLength((stlT)indices[2]-shipOffsetZ+(stlT)Lattice<T>::c(iPop,2));
      stlT neighborLocation[3] = {iXNeighborPhys, iYNeighborPhys, iZNeighborPhys};
      bool neighborInside[1];
      stlReader(neighborInside, neighborLocation);
      if (neighborInside[0]) {
        replacementPops.push_back(iPop);
      }
    }
    outsideNeighborsReplacementPops.push_back(replacementPops);
  }

  std::ofstream replacementPopsCSV("replacementPops.csv");
  for (int boundaryPt = 0; boundaryPt < outsideNeighborsSet.size(); boundaryPt++) {
    auto it = outsideNeighborsSet.begin();
    std::advance(it, boundaryPt);
    size_t outsideNeighbor = *it;
    size_t indices[3];
    util::getCellIndices3D(outsideNeighbor, iYTopBorder+1, iZBackBorder+1, indices);
    std::vector<int> replacementPops = outsideNeighborsReplacementPops.at(boundaryPt);
    replacementPopsCSV << indices[0] << ", " << indices[1] << ", " << indices[2];
    for (int pop : replacementPops)
      replacementPopsCSV << ", " << pop;
    replacementPopsCSV << "\n";
  }
  replacementPopsCSV.close();


  std::cout << "Init GPU data..." << std::endl;
  lattice.initDataArrays();

  lattice.copyDataToGPU();

  BlockVTKwriter3D<T> vtkWriter("sfs2SingleGPU");
  BlockLatticeFluidMask3D<T, Lattice> fluidMaskFunctor(lattice);

  singleton::directories().setOutputDir("./tmp/");

  vtkWriter.addFunctor(fluidMaskFunctor);
  vtkWriter.write(0);
}

int main()
{
  MultipleSteps();
  return 0;
}
