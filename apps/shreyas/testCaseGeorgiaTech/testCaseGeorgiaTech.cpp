/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"
#include "core/unitConverter.h"
#include "core/unitConverter.hh"
#include "dynamics/smagorinskyBGKdynamics.h"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "boundary/boundaryPostProcessors3D.h"
#include "boundary/boundaryPostProcessors3D.hh"
#include "boundary/boundaryCondition3D.h"
#include "boundary/boundaryCondition3D.hh"
#include "io/blockVtkWriter3D.h"
#include "io/blockVtkWriter3D.hh"
#include "functors/genericF.h"
#include "functors/genericF.hh"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/blockBaseF3D.hh"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"
#include "utilities/timer.h"
#include "utilities/timer.hh"
#include "io/superVtmWriter3D.h"
#include "io/superVtmWriter3D.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

template <typename T, template <typename> class Lattice, class Blocklattice>
void defineBoundaries(Blocklattice &lattice, Dynamics<T, Lattice> &dynamics, std::vector<int> limiter)
{
    int iXLeftBorder = limiter[0];
    int iXRightBorder = limiter[1];
    int iYBottomBorder = limiter[2];
    int iYTopBorder = limiter[3];
    int iZFrontBorder = limiter[4];
    int iZBackBorder = limiter[5];

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0N);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &plane2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

    //static OnLatticeBoundaryCondition3D<T, Lattice>  
}

template <unsigned int RESOLUTION>
void MultipleSteps(const double simTime)
{

    int iXLeftBorder = 0;
    int iXRightBorder = RESOLUTION;
    int iYBottomBorder = 0;
    int iYTopBorder = RESOLUTION;
    int iZFrontBorder = 0;
    int iZBackBorder = RESOLUTION;

    T const length = 1.;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        iXRightBorder, 0.3 * 1.0 / std::sqrt(3), 4. * length, 40., 0.0000146072, 1.225, 0);

    converter.print();
    T spacing = converter.getConversionFactorLength();
    T cellSize = spacing * spacing;

    T omega = converter.getLatticeRelaxationFrequency();

    //ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.0);
    BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega);

    std::cout << "Create blockLattice.... ";

    BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

    std::cout << "Finished!" << std::endl;

    std::vector<int> limits = {iXLeftBorder, iXRightBorder, iYBottomBorder, iYTopBorder, iZFrontBorder, iZBackBorder};

    std::cout << "Define boundaries.... ";
    defineBoundaries(lattice, bulkDynamics, limits);

    T u[3] = {0, 0, 0};
    std::cout << "Init equilibrium.... ";
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[Lattice<T>::d] = {0., 0., converter.getLatticeVelocity(0.)};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }

    std::cout << "Finished!" << std::endl;

    std::cout << "Init GPU data.... ";
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    unsigned int preStep = 1;
    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("./tmp/");
    BlockVTKwriter3D<T> writer("outputVTK");
    SuperVTMwriter3D<T> vtmWriter("outputVTK");
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    writer.addFunctor(physVelocityFunctor);
    writer.write(0);

    lattice.copyDataToGPU();

    for (unsigned int trimStep = 0; trimStep < 1; ++trimStep)
    {
        //lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        lattice.collideAndStreamGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();

        // lattice.fastD3Q19BGKDynamicsFieldCollisionGPU<BGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();
        // lattice.boundaryCollisionGPU();
        // cudaDeviceSynchronize();
        // lattice.streamGPU();
        // cudaDeviceSynchronize();
        // lattice.postProcessGPU();
        // cudaDeviceSynchronize();
        
        if ((trimStep % converter.getLatticeTime(0.1)) == 0)
        {
            std::cout << "Trim step:       " << trimStep << std::endl;
            std::cout << "Simulation time: " << converter.getPhysTime(trimStep) << std::endl;

            lattice.copyDataToCPU();
            writer.write(trimStep);
        }
    }
}

int main(int argc, char **argv)
{
    const double simTime = 0.2;
    MultipleSteps<200>(simTime);
    return 0;
}
