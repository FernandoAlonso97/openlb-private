/*  This file is part of the OpenLB library
*
*  Copyright (C) 2019 Bastian Horvat
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

//#define OUTPUTIP "192.168.0.250"

#define FORCEDD3Q19LATTICE 1
typedef double T;

#include "olb3D.h"
#include "olb3D.hh"
#include <cmath>

#define Lattice ForcedD3Q19Descriptor

using namespace olb;
using namespace olb::descriptors;

/* Simulation Parameters */
T smagoConstant = 0.15;
const T domainWidth = 4.0;
const double simTime = 30;
unsigned int resolution = 64;

const T gridSpacing = domainWidth/resolution;
const T gridArea = pow(gridSpacing,2);

const T physInducedVelocity = 5.0; // m/s
const T physDensity = 1.225; // kg/m^3
const T physKinematicViscosity = 1.8e-5; // m^2/s

const T rotorStartTime = 2.0;
const T rotorRampUpTime = 10;

const T BETUpdateInterval = 0.02;

/* Rotor Parameters */
const T rotorRadius = 1.0; // meters
const T rotorLatticeRadius = rotorRadius/gridSpacing;
const T rotorInboardCutoffRadius = 0.0; //account for the inner portion of the blade not doing much aerodynamically
const T rotorLatticeInboardCutoffRadius = rotorInboardCutoffRadius/gridSpacing;
const T rotorArea = M_PI*pow(rotorRadius,2); // m^2
T rotorThrottle = 0.01;
const T rotorThrust = 2*physDensity*rotorArea*pow(physInducedVelocity,2);

const int rotorY = 0.75*resolution; //center of the rotor
const int rotorX = 0.5*resolution;
const int rotorZ = 0.5*resolution;
const int rotorMinX = rotorX-ceil(rotorLatticeRadius)-1;
const int rotorMaxX = rotorX+ceil(rotorLatticeRadius)+1;
const int rotorMinZ = rotorZ-ceil(rotorLatticeRadius)-1;
const int rotorMaxZ = rotorZ+ceil(rotorLatticeRadius)+1;

std::unordered_map<int, T> rotorCellWeights;
std::vector<int> rotorInflowCellIDs;

/* Output Parameters */
bool outputVTKData = true;
bool outputRotorData = true;
bool outputDebugData = false;

const T vtkWriteInterval = 0.2; // s

template <typename T, template<typename> class Lattice>
void defineBoundaries(BlockLattice3D<T, Lattice> &lattice, Dynamics<T, Lattice> &dynamics)
{
    int iXLeftBorder = 0;
    int iXRightBorder = lattice.getNx()-1;
    int iYBottomBorder = 0;
    int iYTopBorder = lattice.getNy()-1;
    int iZFrontBorder = 0;
    int iZBackBorder = lattice.getNz()-1;

    T omega = dynamics.getOmega();

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, -1>> plane0N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 0, 1>> plane0P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, -1>> plane1N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 1, 1>> plane1P;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, -1>> plane2N;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryProcessor3D<T, Lattice, 2, 1>> plane2P;

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0N);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder + 1, iZBackBorder - 1, &plane0P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &plane1P);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &plane2N);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &plane2P);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, -1>> edge0PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, -1>> edge0NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1, 1>> edge0PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1, 1>> edge0NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, -1>> edge1PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, -1>> edge1NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, 1, 1>> edge1PP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1, 1>> edge1NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, -1>> edge2NN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, -1, 1>> edge2NP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, -1>> edge2PN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2, 1, 1>> edge2PP;

    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZFrontBorder, iZFrontBorder, &edge0PN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZFrontBorder, iZFrontBorder, &edge0NN);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, iZBackBorder, iZBackBorder, &edge0PP);
    lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, iZBackBorder, iZBackBorder, &edge0NP);

    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZBackBorder, iZBackBorder, &edge1PP);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, iZFrontBorder, iZFrontBorder, &edge1NP);

    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PN);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYBottomBorder, iYBottomBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NN);
    lattice.defineDynamics(iXRightBorder, iXRightBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2PP);
    lattice.defineDynamics(iXLeftBorder, iXLeftBorder, iYTopBorder, iYTopBorder, iZFrontBorder + 1, iZBackBorder - 1, &edge2NP);

    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, -1>> cornerNNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, -1>> cornerNPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, -1, 1>> cornerNNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, -1, 1, 1>> cornerNPP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, -1>> cornerPNN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, -1>> cornerPPN;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, -1, 1>> cornerPNP;
    static PostProcessingDynamics<T, Lattice, ImpedanceBoundaryCornerProcessor3D<T, Lattice, 1, 1, 1>> cornerPPP;

    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZFrontBorder, &cornerNNN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZFrontBorder, &cornerPNN);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZFrontBorder, &cornerNPN);
    lattice.defineDynamics(iXLeftBorder, iYBottomBorder, iZBackBorder, &cornerNNP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZFrontBorder, &cornerPPN);
    lattice.defineDynamics(iXRightBorder, iYBottomBorder, iZBackBorder, &cornerPNP);
    lattice.defineDynamics(iXLeftBorder, iYTopBorder, iZBackBorder, &cornerNPP);
    lattice.defineDynamics(iXRightBorder, iYTopBorder, iZBackBorder, &cornerPPP);

}

template <typename T, template<typename> class Lattice>
void defineRotorCellIDs(BlockLattice3D<T, Lattice> &lattice) {
    int sampleResolution = 100;

    int iY = rotorY;
    for (int iX = rotorMinX; iX <= rotorMaxX; iX++) {
        for (int iZ = rotorMinZ; iZ <= rotorMaxZ; iZ++) {
            int gridPointsInRotor = 0;

            for (int iXSubIndex = 0; iXSubIndex < sampleResolution; iXSubIndex++) {
                for (int iZSubIndex = 0; iZSubIndex < sampleResolution; iZSubIndex++) {
                    T iXSub = (iX-0.5) + (1.0/(2.0*(T)sampleResolution)) + (T)iXSubIndex/(T)sampleResolution;
                    T iZSub = (iZ-0.5) + (1.0/(2.0*(T)sampleResolution)) + (T)iZSubIndex/(T)sampleResolution;

                    T currentSubRadius = sqrt(pow((T)rotorX-iXSub, 2)+pow((T)rotorZ-iZSub, 2));
                    if (currentSubRadius <= rotorLatticeRadius && currentSubRadius >= rotorInboardCutoffRadius) 
                        gridPointsInRotor++;

                }
            }

            T currentCellWeight = (T)gridPointsInRotor/(T)pow(sampleResolution, 2);
            if (currentCellWeight > 0.00001) {
                int currentCellID = util::getCellIndex3D(iX, iY, iZ, lattice.getNy(), lattice.getNz());
                rotorCellWeights.insert({currentCellID, currentCellWeight});

                if (currentCellWeight > 0.999) {
                    rotorInflowCellIDs.push_back(currentCellID);
                }
            }
        }
    }
}

template <typename T, template<typename> class Lattice>
void defineRotorThrust(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter) {

    for (auto rotorCell : rotorCellWeights) {
        size_t indices[3];
        util::getCellIndices3D(rotorCell.first, lattice.getNy(), lattice.getNz(), indices);

        int iX = indices[0]; int iY = indices[1]; int iZ = indices[2];
        T currentCellWeight = rotorCell.second;
    
        T currentCellThrust = rotorThrust*rotorThrottle*currentCellWeight;
        T latticeVerticalForce = converter.getLatticeForce(currentCellThrust*gridArea/rotorArea);

        T latticeForce[3] = {0.0, -latticeVerticalForce, 0.0};

        lattice.defineForce(iX, iX, iY, iY, iZ, iZ, latticeForce);
    }
}

template <typename T, template<typename> class Lattice>
std::vector<T> calculateHarmonicInflow(BlockLattice3D<T, Lattice> &lattice, UnitConverter<T, Lattice> converter) {

    int gridCellsSampled = rotorInflowCellIDs.size();
    T inflowVelocityRunningSum = 0.0;
    T inflowSineVelocityRunningSum = 0.0;
    T inflowCosineVelocityRunningSum = 0.0;

    for (int rotorCellID : rotorInflowCellIDs) {
        size_t indices[3];
        util::getCellIndices3D(rotorCellID, lattice.getNy(), lattice.getNz(), indices);

        int iX = indices[0]; int iY = indices[1]; int iZ = indices[2];
       
        T currentCellVelocity[3];
        lattice.get(iX, iY, iZ).computeU(currentCellVelocity);

        T currentCellLatticeRadius = sqrt(pow(iX-rotorX, 2)+pow(rotorZ-iZ, 2));
        T currentCellAzimuth = atan2(rotorZ-iZ, iX-rotorX);

        inflowVelocityRunningSum += -currentCellVelocity[1]; //add the y-direction velocity to inflow velocity, negative sign because inflow is down.
        inflowSineVelocityRunningSum += -currentCellVelocity[1]*(currentCellLatticeRadius/rotorLatticeRadius)*sin(currentCellAzimuth);
        inflowCosineVelocityRunningSum += -currentCellVelocity[1]*(currentCellLatticeRadius/rotorLatticeRadius)*cos(currentCellAzimuth);
    }

    T inflowHarmonicMean = converter.getPhysVelocity(inflowVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicSine = converter.getPhysVelocity(4.0*inflowSineVelocityRunningSum/gridCellsSampled);
    T inflowHarmonicCosine = converter.getPhysVelocity(4.0*inflowCosineVelocityRunningSum/gridCellsSampled);

    std::vector<T> harmonicVector {inflowHarmonicMean, inflowHarmonicSine, inflowHarmonicCosine};
    return harmonicVector;
}

void MultipleSteps()
{

    int iXRightBorder = resolution;
    int iYTopBorder = resolution;
    int iZBackBorder = resolution;

    UnitConverterFromResolutionAndLatticeVelocity<T, Lattice> const converter(
        resolution, 0.3 * 1.0 / std::sqrt(3), domainWidth, 2*physInducedVelocity, physKinematicViscosity, physDensity, 0);

    converter.print();

    T omega = converter.getLatticeRelaxationFrequency();

    ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>> bulkDynamics(omega, 0.13);

    std::cout << "Create blockLattice.... " << std::endl;
    BlockLattice3D<T, Lattice> lattice(iXRightBorder + 1, iYTopBorder + 1, iZBackBorder + 1, &bulkDynamics);

    std::cout << "Define boundaries.... " << std::endl;
    defineBoundaries(lattice, bulkDynamics);

    std::cout << "Define rotor cells.... "  << std::endl;
    defineRotorCellIDs(lattice);

    std::cout << "Init GPU data.... " << std::endl;
    lattice.initDataArrays();
    std::cout << "Finished!" << std::endl;

    std::cout << "Init equilibrium.... ";
    for (int iX = 0; iX <= iXRightBorder; ++iX)
        for (int iY = 0; iY <= iYTopBorder; ++iY)
            for (int iZ = 0; iZ <= iZBackBorder; ++iZ)
            {
                T vel[3] = {0., 0., 0.};
                T rho[1];
                lattice.iniEquilibrium(iX, iX, iY, iY, iZ, iZ, 1., vel);
            }
    lattice.copyDataToGPU();
    std::cout << "Finished!" << std::endl;

    unsigned int trimTime = converter.getLatticeTime(simTime);

    singleton::directories().setOutputDir("/ocean/projects/eve210001p/shreyasashok/simData/actuatorDiskTest/");
    BlockVTKwriter3D<T> writer("actuatorDiskVTK");
    BlockLatticeVelocity3D<T,Lattice> velocityFunctor(lattice);
    BlockLatticePhysVelocity3D<T,Lattice> physVelocityFunctor(lattice, 0, converter);
    BlockLatticeForce3D<T, Lattice> forceFunctor(lattice);
    BlockLatticePhysPressure3D<T, Lattice> physPressureFunctor(lattice, 0, converter);
    writer.addFunctor(velocityFunctor);
    writer.addFunctor(physVelocityFunctor);
    writer.addFunctor(forceFunctor);
    writer.addFunctor(physPressureFunctor);
    if (outputVTKData)
        writer.write(0);

    Timer<T> timer(trimTime, lattice.getNx()*lattice.getNy()*lattice.getNz());
    timer.start();

    PolynomialStartScale<T, T> rotorStartScale(converter.getLatticeTime(rotorRampUpTime), 1.0);

    for (unsigned int trimStep = 0; trimStep < trimTime; ++trimStep)
    {
        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, Lattice, BulkMomenta<T, Lattice>>>();


        if ((trimStep % converter.getLatticeTime(vtkWriteInterval)) == 0)
        {
            timer.update(trimStep);
            timer.printStep();
            lattice.getStatistics().print(trimStep, converter.getPhysTime(trimStep));

            if (outputVTKData) {
                lattice.copyDataToCPU();
                writer.write(trimStep);
            }
        }

        if ((trimStep % converter.getLatticeTime(BETUpdateInterval)) == 0  && trimStep >= converter.getLatticeTime(rotorStartTime)) {
            T frac[1];
            T iT[1] = {(T)trimStep - (T)converter.getLatticeTime(rotorStartTime)};
            rotorStartScale(frac, iT);
            rotorThrottle = frac[0];
            if (trimStep >= converter.getLatticeTime(rotorStartTime+rotorRampUpTime))
                rotorThrottle = 1.0;

            lattice.copyDataToCPU();
            std::vector<T> currentHarmonicInflow = calculateHarmonicInflow(lattice, converter);
            defineRotorThrust(lattice, converter);

            if (outputRotorData) {
                std::cout << "Current Average Inflow Velocity: " << currentHarmonicInflow[0] << std::endl;
                std::cout << "Current Sine Inflow Velocity: " << currentHarmonicInflow[1] << std::endl;
                std::cout << "Current Cosine Inflow Velocity: " << currentHarmonicInflow[2] << std::endl;
            }            
            lattice.copyDataToGPU();
        }
    }
    timer.stop();
    timer.printSummary();
}

int main(int argc, char **argv)
{
    MultipleSteps();
    return 0;
}
