#!/bin/bash


# usage:
# parameters given to script will be formatted according to OPTIONS by astyle
#
# e.g.    ./astyleSpecific.sh file.cpp file2.h file3.hh
#
# contact: albert.mink@kit.edu


##documentation: http://astyle.sourceforge.net/astyle.html
#
### defaults parameters ###
# -s2   uses 2 spaced per indent
# -n    no backup files
# -O    don't break one-line blocks
# -c    convert tabs into spaces
# -A4   uses stroustrup brackets style
# -D    space padding around parenthesis on the inside only
# -H    turns if(...) to if (...), resp while,for,else,...
# -j    add brackets to unbracketed one line conditional
# -Y    comments beginning in column one
# -f    Pad empty lines around header blocks (e.g. 'if ', 'for ', 'while '...).
# -p    pad space around operators (+,-,..)
# -xe   delete empty lines


# adds all parameters/arguments passed to script into a single line
# $@

# Is astyle installed?
type astyle >/dev/null 2>&1 || { echo >&2 "I require 'astyle' but it's not installed. Install astyle by: sudo apt-get install astyle"; exit 1; }

OPTIONS=" -s2 -A4 --mode=c -n -c -j -H --options=none"
astyle $OPTIONS $@
