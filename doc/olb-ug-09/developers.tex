\section{For Developers}\label{sec:forDevelopers}

\subsection{Coding rules}\label{sec:codingRules}

In general the code follows the rules of the \textit{Google C++ Style Guide} \cite{google-style-guide-web}. However, in some points seperate rules apply, others are listed here again due to their great importancy.

\begin{itemize}
	\item indentation: 2 whitespaces, no tabs
	\item line length: 80 characters recommended, comments may be longer
          (might be exceeded in exceptional cases for a maintainable layout like in \texttt{src/core/units.hh})
	\item naming convention: everything in mixed case (e.g. a function performing a collision and a streaming step is called \textit{collideAndStream()} )
	\begin{itemize}
		\item variable names are in mixed case, beginning with a small letter
		\item function names are in mixed case, beginning with a small letter
		\item class names    are in mixed case, beginning with a capital letter
		\item class instances like class names, beginning with a small letter
		\item functions to set/access class internal variables are called \textit{setVariableName()} and \textit{getVariableName()} respectively
	\end{itemize}
	\item output with \texttt{OstreamManager} (instead of \textit{printf()}, for usage details see Section~\ref{sec:consoleOutput})
	\begin{itemize}
	\item status information in semi-csv-style: \textit{variableName} + `=' + \textit{value} + `; ' + \textit{space}
	%example: cylinder2d
	\begin{verbatim}
	  step=1700; avEnergy=5.55583e-05; avRho=1.00005; uMax=0.0321654
	\end{verbatim}
	\item error messages: \textit{`Error:' + space + small text}
	\begin{verbatim}
	  Error: file not found
	\end{verbatim}
	\end{itemize}
	\item documentation with \texttt{doxygen}
	\begin{itemize}
		\item functions are commented by a preceding doxygen-comment in the corresponding header file: \texttt{///comment\ldots}
		\item classes have a descriptive text, containing at best an example usage (also doxygen-style): \texttt{/** comment */}
	\end{itemize}
	\item comments
	\begin{itemize}
		\item Comments should be as descriptive as possible. As such, try to write complete sentences and mind a proper capitalization, spelling and punctuation. This will greatly improve readability!
	\end{itemize}
	\item files and names
	\begin{itemize}
		\item only one public class per file
		\item file names are class names beginning with a lower case character
	\end{itemize}
\end{itemize}

The OpenLB source code is written as \textit{generic} (based on the template parameter T instead of one fixed data type like double) \textit{high performance} (that means also to pass variables by reference and not as a copy) \textit{code} to benefit from user defined precision or AD-techniques like in \cite{griewank-ad:00}.
Therefore we use three different file extensions:
\begin{center}
	\begin{tabular}{|lp{0.75\textwidth}|}
		\hline
		{*}.h   & is a normal (often template-based) header file\\
		{*}.hh  & contains the actual source code if templates have been used\\
		{*}.cpp & contains the actual source code if templates have not been used, otherwise
		the template parameters for the target \textit{precompiled} are here explicitly
		instanciated (defined)\\
		\hline
	\end{tabular}
\end{center}
Further details concerning this template based programming can be found in \cite{vand-template:03}.
Furthermore, there are some points we recommend to consider for a readable code:
\begin{itemize}
	\item the equal sign `$=$' should be surrounded by whitespaces
	\item rule of \textit{`multiplication and division before addition and subtraction'} using whitespaces:\\
	 instead of \texttt{ z = a * x + b * y } write \texttt{ z = a*x + b*y }
	 \item order of parameters: function(input\_params, output\_params)
	 \item order of sections: public, protected, private
	 \item order of includes: \texttt{className.h}, C/C++ libraries, foreign \texttt{.h}, OpenLB's \texttt{.h}
	 \item don't use relative paths like `\texttt{../src/core/units.h}', it's unix specific and not necessary
	 \item \texttt{template<typename T>} is written in OpenLB one line above the actual function definition due to readability
\end{itemize}
Finally, keep in mind that even OpenLB's programmers are not perfect. So the code might not follow these rules to 100\%. In case of doubt look around and stick to the most important rule:\\
\begin{center} \textbf{Be consistent with the surrounding code!} \end{center}


\subsection{GIT Repository}\label{sec:forDevelopers}

The source code of the OpenLB project is available in a private GIT-repository (\url{http://www.en.wikipedia.org/wiki/Git_(software)}). The repository enables a controlled way of writing code together. Any change is tracked and will be visible and available to the others which avoids a lot of trouble by means of finding bugs very fast. There are a few very important rules you should follow. This will make the life for every programmer, including yourself(!), much easier:
\begin{itemize}
	\item Write your name in the top of any file you create. This will make you the person which needs to be asked if someone want to change an interfaces or if someone reports a bug.
	\item	Never change interfaces without contacting one of the programmer mentioned in the top of the file. 
	\item	Before pushing your changes, make sure that you have respected all coding rules (cf. Sec. \ref{sec:codingRules}) and that all examples are compiling fine. You can use the command \begin{verbatim} make samples \end{verbatim} from the root directory. Also check that the examples compile in both compile modes, \texttt{ precompiled, generic}, and in sequential as well as in parallel mode. 
\end{itemize}

Access can be provided for every core developer. Please, send an  email to \url{contact@openlb.net} with you full name and private address. Then, you will receive an email with your login name and a password that you should change as soon as possible on the website of the provider \textit{Cloudforge}, is available at \url{https://app.cloudforge.com/}. Therefore, you also will be asked to enter the domain name which is \textit{openlb}.

Already an expert? Copy the above URL \url{https://yourLoginName@openlb.git.cloudforge.com/openlb.git} (replace \textit{yourLoginName} with your own login name!) into your Git client and start cloning. Or navigate to the Project Page. Or for beginners, keep reading.

A detailed overview about GIT can be found e.g. in \cite{haenel:11}. A short helpful overview you can download at \url{http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf} and a complete introduction is given by \url{http://git-scm.com/book/en}.

\subsubsection*{Getting started in four steps:}

\begin{enumerate}
	\item Install a Git Client: CloudForge Git repositories require Git Client 1.7.x and above. 
    \begin{itemize}
	    \item MacOS: Download git installer
	    \item Windows: Download SmartGit
	    \item Linux: Install from source or via your package management utility. e.g.: \begin{verbatim} yum install git-core \end{verbatim} or \begin{verbatim} apt-get install git-core \end{verbatim}
    \end{itemize}
  \item Clone your Git Repository by
    \begin{verbatim}
      cd ~ 
      mkdir gitSources
      cd gitSources 
      git clone https://yourLoginName@openlb.git.cloudforge.com/openlb.git
    \end{verbatim}
	Of course, replace \textit{yourLoginName} with your own login name. Now, the files are being copied to a folder called \textit{openlb}. 
	\item Add some files and directories: Now you've referenced your on-line repository, it's time to add something. Create or edit a file, for example:
    \begin{verbatim} 
      cd openlb
      echo "first line of my first file... yay!" > helloworld.txt
    \end{verbatim}
	Now that you have changed something you can add these changes to the index using
    \begin{verbatim}
      git add helloworld.txt
    \end{verbatim}
	The next step is to commit the changes. Before you commit for the first time, config your user email and user name. You will have to do that only once, not every time you commit. The string after \texttt{-m} is the commit message.
    \begin{verbatim}
      git config --global user.email "you@example.com"
      git config --global user.name "Your Name"
      git commit -m "test: first commit"
    \end{verbatim}
	At any time during that process, you can look up what you have already done using
    \begin{verbatim}
      git status
    \end{verbatim}
	\item Push it to the master repository: 
	  \begin{verbatim} 
      git push 
	  \end{verbatim}
\end{enumerate}

The provider of the service \textit{Cloudforge} also has a web interface, which is available at \url{https://app.cloudforge.com}.
You will be asked there to provide the domain, which is openlb, your login name and finally your password. 

\subsection{Release Scripts}
In order to remove the \^M line endings in unix:
\begin{verbatim} 
  find . -name "*" | xargs dos2unix
\end{verbatim} 
A script that ensures the coding rules can be found in apps/lukas/indent.sh and can be executed by:
\begin{verbatim} 
  ./indent.sh src/
\end{verbatim} 
This creates a directory src-indented, which can be renamed back to src by hand.

\subsection{Working with HC3 (at Steinbuch Centre for Computing of KIT)}
Working with the HC3 requires a KIT account. A form has to be filled as well, which can be found here
\begin{verbatim}
  http://www.scc.kit.edu/downloads/sdo/Antrag_Benutzernummer_hc3.pdf
\end{verbatim}
To log in use the command
\begin{verbatim}
  ssh KIT-account@hc3.scc.kit.edu
\end{verbatim}
or, if you want to use a GUI-based application,
\begin{verbatim}
  ssh -X KIT-account@hc3.scc.kit.edu
\end{verbatim}
Some modulefiles are loaded automatically and can be displayed by the command
\begin{verbatim}
  module list
\end{verbatim}
Other modulefiles can be loaded, using, for example,
\begin{verbatim}
  module add gcc
  module add openmpi
\end{verbatim}
To compile, use the \texttt{make} command as usual. If a parallel application is run using the \texttt{mpirun} command, the login node is used which leads to some restrictions.\\
Instead batch jobs can be submitted using the command \texttt{job\_submit} , for example
\begin{verbatim}
  job_submit -p 256 -cp -t4000 -m4000 mpirun cylinder3d
\end{verbatim}
which starts a job with 256 tasks in the production pool (use \texttt{-cd} instead for development pool) with 4000 minutes maximum CPU time and 4000MB maximum memory requirement per task.\\
Your queued or running jobs can be shown using
\begin{verbatim}
  job_queue
\end{verbatim}
and canceled using
\begin{verbatim}
  job_cancel JOBNR
\end{verbatim}
The output goes to a file and can be viewed like this, for example:
\begin{verbatim}
  vi job_hc3_JOBNR.out
\end{verbatim}
A brief information for new users can also be found here
\begin{verbatim}
  http://www.scc.kit.edu/scc/docs/HP-XC/ug/erstinfhc.pdf
\end{verbatim}
and there is a more detailed user guide as well
\begin{verbatim}
  http://www.scc.kit.edu/scc/docs/HP-XC/ug/ug3k.pdf
\end{verbatim}


\subsection{Working on HORST}
The IP-address of Horst is 172.23.20.188 and its queuing system is called TORQUE.
Horst has got 8 nodes and each node is equipped with 16 physical cores respectively 64GB of RAM.
An brief introduction to the queuing system is given by \url{https://rcc.its.psu.edu/user_guides/system_utilities/pbs/}.


