#!/bin/bash
#
#  This file is part of the OpenLB library
#
#  Copyright (C) 2014 Albert Mink
#  E-mail contact: info@openlb.net
#  The most recent release of OpenLB can be downloaded at
#  <http://www.openlb.net/>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA  02110-1301, USA.


#
# navigate to the folder where script is located, via bash command cd .....
# make file executable with:
# chmod +x my_script.sh
#
# execute script by:
# ./my_script.sh


echo "I install all basic programs to compile and develop openLB."
echo "(Feel free to adapt it to your own preferences.)"


# cares about paraview
# fist it verifies if paraview have been yet installed.
# if not, it asks you to install it, you may answer y or n.
#   and checks at the end if installation was successful.
if  dpkg -s paraview
  then
    echo "paraview was already installed, version is: "
    paraview -V
  else
    read -p "Do you want to install paraview (y or n): " answ
    if [ "$answ" == "y" ]
      then
        sudo apt-get install paraview -y
    fi
    unset answ
    if  dpkg -s paraview
      then
        echo "paraview installation done."
      else
        echo "paraview installation failed."
    fi    
fi


if  dpkg -s g++
  then
    echo "g++ was already installed, version is: "
    g++ --version
  else
    sudo apt-get install g++ -y
    if  dpkg -s g++
      then
        echo "g++ installation done."
      else
        echo "g++ installation failed."
    fi
fi

echo "Install openMPI"
sudo apt-get install openmpi-bin openmpi-doc libopenmpi-dev -y

if  dpkg -s imagemagick
  then
    echo "imagemagick was already installed."
  else
    sudo apt-get install imagemagick -y
    if  dpkg -s imagemagick
      then
        echo "imagemagick installation done."
      else
        echo "imagemagick installation failed."
    fi    
fi


###############################################
#############for developer only################
###############################################

if  dpkg -s git-core
  then
    echo "git-core was already installed."
  else
    read -p "Install git-core and gitk (y o n): " answ
    if [ "$answ" == "y" ]
      then
        sudo apt-get install git-core gitk -y
    fi
    unset answ
    if  dpkg -s git-core
      then
        echo "git-core installation done."
      else
        echo "git-core installation failed."
    fi
fi

# BEGIN initialize git
PATH_TO_GIT=gitSources
read -p "Clone git to ~/$PATH_TO_GIT (y o n): " answ
if [ "$answ" == "y" ]
  then
    read -p "Please enter you cloudforge user name: " cloudforge_id

    cd ~
    mkdir -p $PATH_TO_GIT
    cd $PATH_TO_GIT

    #git clone https://$cloudforge_id@openlb.git.cloudforge.com/openlb.git 

    git clone ssh://git_openlb@openlb.git.cloudforge.com/openlb.git 

    echo "Git wants to know your name and email address."
    echo "[Your pushes will be referred to your name and email address.]"
    read -p "name: " git_name
    read -p "mail: " git_mail

    cd openlb

    git config --local user.name $git_name
    git config --local user.email $git_mail

    # get colors
    git config --local color.ui true

    make Makefile.inc
fi
# END initialize git

echo "bye."



###########################
###common used tools#######
###########################

# documentation helpers
#sudo apt-get install texlive -y
#sudo apt-get install texlive-full -y
#sudo apt-get install doxygen -y

# editor plugin and misc
#sudo apt-get install gedit-plugins -y
#sudo apt-get install indicator-multiload -y
#sudo apt-get install chromium-browser -y

# get new g++ compiler
# works fine if no g++ is installed
# to get "dual" compile you have to add:
# sudo update-alternatives --config g++
# to the end
#sudo apt-apt remove g++ -y
#sudo add-apt-repository ppa:ubuntu-toolchain-r/test
#sudo apt-get update
#sudo apt-get install g++-4.9 -y
#sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.9 10

