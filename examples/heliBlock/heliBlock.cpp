/*
 * densitySphere.cpp
 *
 *  Created on: May 23, 2018
 *      Author: Bastian Horvat
 */

#define FORCEDD3Q19LATTICE
typedef double T;


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif

#include <cmath>
#include <iostream>
#include <fstream>


using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

//#define DESCRIPTOR D3Q19Descriptor
#define DESCRIPTOR ForcedD3Q19Descriptor

const int N = 32;
const T radius = 4.92;
const T length = 4*radius;
const T height = 4*radius;
const T width = 4*radius;
const T maxT = 100;


bool caseSphere = false;
bool isGPU = false;
std::string caseName = "heliBlock";


template <typename T>
class MyRectanglePoiseuille3D final : public AnalyticalF3D<T,T> {
protected:
  OstreamManager clout;
  std::vector<T> x0;
  std::vector<T> x1;
  std::vector<T> x2;
  std::vector<T> maxVelocity;

public:
  MyRectanglePoiseuille3D(std::vector<T>& x0_, std::vector<T>& x1_, std::vector<T>& x2_, std::vector<T>& maxVelocity_);
  /// constructor from material numbers
  /** offsetX,Y,Z is a positive number denoting the distance from border
    * voxels of material_ to the zerovelocity boundary */
  MyRectanglePoiseuille3D(BlockGeometry3D<T>& blockGeometry_, int material_, std::vector<T>& maxVelocity_, T offsetX, T offsetY, T offsetZ);
  bool operator()(T output[], const T x[]) override;
};

template <typename T>
MyRectanglePoiseuille3D<T>::MyRectanglePoiseuille3D(std::vector<T>& x0_, std::vector<T>& x1_,
    std::vector<T>& x2_, std::vector<T>& maxVelocity_)
  : AnalyticalF3D<T,T>(3), clout(std::cout,"RectanglePoiseille3D"), x0(x0_), x1(x1_),
    x2(x2_), maxVelocity(maxVelocity_) { }


template <typename T>
MyRectanglePoiseuille3D<T>::MyRectanglePoiseuille3D(BlockGeometry3D<T>& blockGeometry_,
    int material_, std::vector<T>& maxVelocity_, T offsetX, T offsetY, T offsetZ)
  : AnalyticalF3D<T,T>(3), clout(std::cout, "RectanglePoiseuille3D"), maxVelocity(maxVelocity_)
{
  std::vector<T> min = blockGeometry_.getStatistics().getMinPhysR(material_);
  std::vector<T> max = blockGeometry_.getStatistics().getMaxPhysR(material_);
  // set three corners of the plane, move by offset to land on the v=0
  // boundary and adapt certain values depending on the orthogonal axis to get
  // different points
  x0 = min;
  x1 = min;
  x2 = min;
  if ( util::nearZero(max[0]-min[0]) ) { // orthogonal to x-axis
    x0[1] -= offsetY;
    x0[2] -= offsetZ;
    x1[1] -= offsetY;
    x1[2] -= offsetZ;
    x2[1] -= offsetY;
    x2[2] -= offsetZ;

    x1[1] = max[1] + offsetY;
    x2[2] = max[2] + offsetZ;
  } else if ( util::nearZero(max[1]-min[1]) ) { // orthogonal to y-axis
    x0[0] -= offsetX;
    x0[2] -= offsetZ;
    x1[0] -= offsetX;
    x1[2] -= offsetZ;
    x2[0] -= offsetX;
    x2[2] -= offsetZ;

    x1[0] = max[0] + offsetX;
    x2[2] = max[2] + offsetZ;
  } else if ( util::nearZero(max[2]-min[2]) ) { // orthogonal to z-axis
    x0[0] -= offsetX;
    x0[1] -= offsetY;
    x1[0] -= offsetX;
    x1[1] -= offsetY;
    x2[0] -= offsetX;
    x2[1] -= offsetY;

    x1[0] = max[0] + offsetX;
    x2[1] = max[1] + offsetY;
  } else {
    clout << "Error: constructor from material number works only for axis-orthogonal planes" << std::endl;
  }
}


template <typename T>
bool MyRectanglePoiseuille3D<T>::operator()(T output[], const T x[])
{
  std::vector<T> velocity(3,T());

  // create plane normals and supports, (E1,E2) and (E3,E4) are parallel
  std::vector<std::vector<T> > n(4,std::vector<T>(3,0)); // normal vectors
  std::vector<std::vector<T> > s(4,std::vector<T>(3,0)); // support vectors
  for (int iD = 0; iD < 3; iD++) {
    n[0][iD] =  x1[iD] - x0[iD];
    n[1][iD] = -x1[iD] + x0[iD];
    n[2][iD] =  x2[iD] - x0[iD];
    n[3][iD] = -x2[iD] + x0[iD];
    s[0][iD] = x0[iD];
    s[1][iD] = x1[iD];
    s[2][iD] = x0[iD];
    s[3][iD] = x2[iD];
  }
  for (int iP = 0; iP < 4; iP++) {
    n[iP] = util::normalize(n[iP]);
  }

  // determine plane coefficients in the coordinate equation E_i: Ax+By+Cz+D=0
  // given form: (x-s)*n=0 => A=n1, B=n2, C=n3, D=-(s1n1+s2n2+s3n3)
  std::vector<T> A(4,0);
  std::vector<T> B(4,0);
  std::vector<T> C(4,0);
  std::vector<T> D(4,0);

  for (int iP = 0; iP < 4; iP++) {
    A[iP] = n[iP][0];
    B[iP] = n[iP][1];
    C[iP] = n[iP][2];
    D[iP] = -(s[iP][0]*n[iP][0] + s[iP][1]*n[iP][1] + s[iP][2]*n[iP][2]);
  }

  // determine distance to the walls by formula
  std::vector<T> d(4,0);
  T aabbcc(0);
  for (int iP = 0; iP < 4; iP++) {
    aabbcc = A[iP]*A[iP] + B[iP]*B[iP] + C[iP]*C[iP];
    d[iP] = fabs(A[iP]*x[0]+B[iP]*x[1]+C[iP]*x[2]+D[iP])*pow(aabbcc,-0.5);
  }

  // length and width of the rectangle
  std::vector<T> l(2,0);
  l[0] = d[0] + d[1];
  l[1] = d[2] + d[3];

  T positionFactor = 16.*d[0]/l[0]*d[1]/l[0]*d[2]/l[1]*d[3]/l[1]; // between 0 and 1

  output[0] = maxVelocity[0]*positionFactor;
  output[1] = maxVelocity[1]*positionFactor;
  output[2] = maxVelocity[2]*positionFactor;
  return true;
}

template <typename T>
class gaussSphere : public AnalyticalF3D<T,T> {

protected:
  T x_, y_, z_;
  T valueMax_;
  T valueBase_;
  T radius_;

// initial solution of the TGV
public:
  gaussSphere(T center[], T radius, T valueMax, T valueBase) : AnalyticalF3D<T,T>(1)
    ,x_(center[0])
    ,y_(center[1])
    ,z_(center[2])
    ,valueMax_(valueMax)
    ,valueBase_(valueBase)
    ,radius_(radius)
  {}
  bool operator()(T output[], const T input[]) override
  {
    T radius = std::sqrt(std::pow(input[0]-x_,2)+std::pow(input[1]-y_,2)+std::pow(input[2]-z_,2));
    output[0] = valueBase_+(valueMax_ - valueBase_)*std::exp( -std::pow(radius,2)/std::pow(radius_/2.2,2) );

    return true;
  };
};

template <typename T>
class sineThrust : public AnalyticalF3D<T,T> {

protected:
  T x_, y_, z_;
  T thrustX_;
  T thrustY_;
  T thrustZ_;
  T radius_;

// initial solution of the TGV
public:
  sineThrust(T center[], T radius, std::vector<T> force) : AnalyticalF3D<T,T>(1)
    ,x_(center[0])
    ,y_(center[1])
    ,z_(center[2])
	,thrustX_(force[0])
	,thrustY_(force[1])
	,thrustZ_(force[2])
    ,radius_(radius)
  {}
  bool operator()(T output[], const T input[]) override
  {
    T radius = std::sqrt(std::pow(input[0]-x_,2)+std::pow(input[1]-y_,2)+std::pow(input[2]-z_,2));
    if(radius<=radius_)
    {
    	output[0] = thrustX_*std::sin(3.14159/(2.0*radius_)*radius);
    	output[1] = thrustY_*std::sin(3.14159/(2.0*radius_)*radius);
    	output[2] = thrustZ_*std::sin(3.14159/(2.0*radius_)*radius);
    }
    else
    {
    	output[0] = 0;
		output[1] = 0;
		output[2] = 0;
    }

    return true;
  };
};

void prepareGeometry(UnitConverter<T,DESCRIPTOR> converter
    , BlockGeometry3D<T>& blockGeometry)
{
  Vector<T,3> extend(length,height,width);
  Vector<T,3> origin;
  IndicatorCuboid3D<T> block(extend,origin);

  T deltaX = converter.getConversionFactorLength();

  blockGeometry.rename(0,2,block);
  blockGeometry.rename(2,1,1,1,1);

  Vector<T,3> extendInflow(deltaX,height+deltaX
      ,width-2*deltaX);
  Vector<T,3> originInflow(0-0.5*deltaX,0,0+deltaX);
  IndicatorCuboid3D<T> inflow(extendInflow,originInflow);

  Vector<T,3> extendOutflow(deltaX
      ,height+deltaX,width-1*deltaX);
  Vector<T,3> originOutflow(length-0.5*deltaX,0,0+deltaX);
  IndicatorCuboid3D<T> outflow(extendOutflow,originOutflow);

  Vector<T,3> extendTop(length-deltaX, deltaX, width-1.5*deltaX);
  Vector<T,3> originTop(0+deltaX,height-0.5*deltaX,0+deltaX);
  IndicatorCuboid3D<T> top(extendTop, originTop);

  Vector<T,3> extendTopMinusOne(length-deltaX, deltaX, width-1*deltaX);
  Vector<T,3> originTopMinusOne(0+deltaX,height-1.5*deltaX,0+deltaX);
  IndicatorCuboid3D<T> topMinusOne(extendTopMinusOne,originTopMinusOne);

  Vector<T,3> extendBottom(length-deltaX, 0.5*deltaX, width -1.5*deltaX);
  Vector<T,3> originBottom(0+deltaX,0-0.5*deltaX,0+deltaX);
  IndicatorCuboid3D<T> bottom(extendBottom, originBottom);

  Vector<T,3> extendFront(length,height+1.5*deltaX,deltaX);
  Vector<T,3> originFront(0,0,width-0.5*deltaX);
  IndicatorCuboid3D<T> front(extendFront, originFront);

  Vector<T,3> extendBack(length,height,deltaX);
  Vector<T,3> originBack(0,0,0-0.5*deltaX);
  IndicatorCuboid3D<T> back(extendBack, originBack);

  Vector<T,3> originSphere(0.3*length,0.3*height,0.3*width);
  T radiusSphere = 0.1*width;

  IndicatorSphere3D<T> sphere(originSphere,radiusSphere);

  Vector<T,3> originRotorBottom(0.5*length,0.5*height,0.5*width);
  Vector<T,3> originRotorTop(0.5*length,0.5*height+1.0*deltaX,0.5*width);

  Vector<T,3> normalRotor(0,0,1);

  IndicatorCylinder3D<T> rotor(originRotorBottom,originRotorTop,radius);

//  IndicatorCuboid3D<T> rotor(4.1*radius,deltaX,4.1*radius,originRotorBottom);


  blockGeometry.rename(2,2,inflow);
  blockGeometry.rename(2,3,outflow);
  blockGeometry.rename(2,4,top);
  blockGeometry.rename(2,5,bottom);
  blockGeometry.rename(2,6,front);
  blockGeometry.rename(2,7,back);


  // Removes all not needed boundary voxels outside the surface
  blockGeometry.clean();
  // Removes all not needed boundary voxels inside the surface
//  blockGeometry.innerClean();
  if(caseSphere)
      blockGeometry.rename(1,8,sphere);
  else
  {
      blockGeometry.rename(1,8,rotor);
  }

//  blockGeometry.rename(1,9,topMinusOne);
  blockGeometry.checkForErrors();

}

void prepareLattice(UnitConverter<T,DESCRIPTOR> converter
    , BlockLattice3D<T, DESCRIPTOR>& blockLattice
    , Dynamics<T, DESCRIPTOR>& bulkDynamics
    , OnLatticeBoundaryCondition3D<T,DESCRIPTOR>& boundaryCondition
    , BlockGeometry3D<T>& blockGeometry )
{
  const T omega = converter.getLatticeRelaxationFrequency();
//  blockLattice.defineDynamics( blockGeometry, 0, &instances::getNoDynamics<T,DESCRIPTOR>() );

//  blockLattice.defineDynamics( blockGeometry, 1, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 2, &instances::getBounceBack<T,DESCRIPTOR>());
//  blockLattice.defineDynamics( blockGeometry, 2, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 3, &instances::getBounceBack<T,DESCRIPTOR>());
//  blockLattice.defineDynamics( blockGeometry, 3, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 4, &instances::getBounceBack<T,DESCRIPTOR>());
//  blockLattice.defineDynamics( blockGeometry, 4, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 5, &instances::getBounceBack<T,DESCRIPTOR>());
//  blockLattice.defineDynamics( blockGeometry, 5, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 6, &instances::getBounceBack<T,DESCRIPTOR>());
//  blockLattice.defineDynamics( blockGeometry, 6, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 7, &instances::getBounceBack<T,DESCRIPTOR>());
//  blockLattice.defineDynamics( blockGeometry, 7, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 8, &bulkDynamics);

//  blockLattice.defineDynamics( blockGeometry, 9, &bulkDynamics);


  blockLattice.defineDynamics( blockGeometry, 0, &instances::getNoDynamics<T,DESCRIPTOR>() );
//  blockLattice.defineDynamics( blockGeometry, 1, &bulkDynamics );
//  blockLattice.defineDynamics( blockGeometry, 8, &bulkDynamics);

  //  	boundaryCondition.addPressureBoundary( blockGeometry, 2, omega);
//    boundaryCondition.addImpedanceBoundary( blockGeometry, 2, omega );
  boundaryCondition.addPeriodicBoundary( blockGeometry, 2, omega);
  //    boundaryCondition.addPressureBoundary( blockGeometry, 3, omega);
//    boundaryCondition.addImpedanceBoundary( blockGeometry, 3, omega );
  boundaryCondition.addPeriodicBoundary( blockGeometry, 3, omega);
//    boundaryCondition.addVelocityBoundary( blockGeometry,4,omega);
//    	boundaryCondition.addPressureBoundary( blockGeometry, 4, omega);
    boundaryCondition.addImpedanceBoundary( blockGeometry, 4, omega);
//    boundaryCondition.addPeriodicBoundary( blockGeometry, 4, omega);
//      boundaryCondition.addPressureBoundary( blockGeometry, 5, omega);
    boundaryCondition.addImpedanceBoundary( blockGeometry, 5, omega);
//    boundaryCondition.addPeriodicBoundary( blockGeometry, 5, omega);
//      boundaryCondition.addPressureBoundary( blockGeometry, 6, omega);
//    boundaryCondition.addImpedanceBoundary( blockGeometry, 6, omega);
    boundaryCondition.addPeriodicBoundary( blockGeometry, 6, omega);
  //    boundaryCondition.addPressureBoundary( blockGeometry, 7, omega);
//    boundaryCondition.addImpedanceBoundary( blockGeometry, 7, omega);
    boundaryCondition.addPeriodicBoundary( blockGeometry, 7, omega);

//    boundaryCondition.addImpedanceBoundaryIncompressible( blockGeometry, 2, omega);
//    boundaryCondition.addImpedanceBoundaryIncompressible( blockGeometry, 3, omega);
//    boundaryCondition.addImpedanceBoundaryIncompressible( blockGeometry, 4, omega);
//    boundaryCondition.addImpedanceBoundaryIncompressible( blockGeometry, 5, omega);
//    boundaryCondition.addImpedanceBoundaryIncompressible( blockGeometry, 6, omega);
//    boundaryCondition.addImpedanceBoundaryIncompressible( blockGeometry, 7, omega);

  // Make the lattice ready for simulation
    blockLattice.initDataArrays();
//  blockLattice.initialize();

  AnalyticalConst3D<T,T> ux( 0. );
  AnalyticalConst3D<T,T> uy( 0. );
  AnalyticalConst3D<T,T> uz( 0. );
  AnalyticalConst3D<T,T> rho( 1. );
  AnalyticalConst3D<T,T> rhoLine( 1.8 );
  T sphereCenter[3] = {0.3*length,0.3*height,0.3*width};
  gaussSphere<T> rhoSphere( sphereCenter, 0.2*width, 1.8, 1. );
  AnalyticalComposed3D<T,T> u( ux,uy,uz );

  blockLattice.defineRhoU( blockGeometry, 1, rho, u );
  blockLattice.defineRhoU( blockGeometry, 2, rho, u );
  blockLattice.defineRhoU( blockGeometry, 3, rho, u );
  blockLattice.defineRhoU( blockGeometry, 4, rho, u );
  blockLattice.defineRhoU( blockGeometry, 5, rho, u );
  blockLattice.defineRhoU( blockGeometry, 6, rho, u );
  blockLattice.defineRhoU( blockGeometry, 7, rho, u );
  if(caseSphere)
      blockLattice.defineRhoU( blockGeometry, 8, rhoSphere, u );
  else
      blockLattice.defineRhoU( blockGeometry, 8, rho, u );
//  blockLattice.defineRhoU( blockGeometry, 9, rho, u );

  blockLattice.iniEquilibrium( blockGeometry, 1, rho, u );
  blockLattice.iniEquilibrium( blockGeometry, 2, rho, u );
  blockLattice.iniEquilibrium( blockGeometry, 3, rho, u );
  blockLattice.iniEquilibrium( blockGeometry, 4, rho, u );
  blockLattice.iniEquilibrium( blockGeometry, 5, rho, u );
  blockLattice.iniEquilibrium( blockGeometry, 6, rho, u );
  blockLattice.iniEquilibrium( blockGeometry, 7, rho, u );
  if(caseSphere)
      blockLattice.iniEquilibrium( blockGeometry, 8, rhoSphere, u );
  else
      blockLattice.iniEquilibrium( blockGeometry, 8, rho, u );

//  T uTmp[3] = {0};
//  T rhoTmp = 1.4;
//  for(int iX = 0; iX<blockLattice.getNx(); ++iX)
//      for(int iZ = 0; iZ<blockLattice.getNz(); ++iZ)
//      {
//          blockLattice.defineRhoU(iX,iX,25,25,iZ,iZ,rhoTmp,uTmp);
//          blockLattice.iniEquilibrium(iX,iX,25,25,iZ,iZ,rhoTmp,uTmp);
//      }
//  blockLattice.iniEquilibrium( blockGeometry, 9, rho, u );

  std::cout << "Prepare Lattice... OK" << std::endl;
}

void setBoundaryValues( UnitConverter<T,DESCRIPTOR> const & converter
                        , BlockLattice3D<T,DESCRIPTOR>& blockLattice
                        , int iT
                        , BlockGeometry3D<T>& blockGeometry )
{

  int iTupdate = 5;
  int iTmaxStart = converter.getLatticeTime( maxT*0.1 );

//  if ( iT%iTupdate == 0 && iT<= iTmaxStart )
  if(iT ==0)
  {
    PolynomialStartScale<T,int> StartScale( iTmaxStart, T( 1 ) );

    int iTvec[1] = {iT};
    T frac[1] = {};

    StartScale( frac,iTvec );

    std::vector<T> forceVector(3,T());
    forceVector[1] = -converter.getLatticeDiskLoading(0);


    AnalyticalConst3D<T,T> force(forceVector);
    T rotorOrigin[3] = {0.5*length,0.5*height,0.5*width};
    sineThrust<T> force2(rotorOrigin,radius,forceVector);

    if(isGPU)
        blockLattice.copyDataToCPU();

    if(!caseSphere)
        blockLattice.defineExternalField(blockGeometry,8,DESCRIPTOR<T>::ExternalField::forceBeginsAt(),
                DESCRIPTOR<T>::ExternalField::sizeOfForce(), force);
    if(isGPU)
        blockLattice.copyDataToGPU();
  }
  else if(iT == 1)
  {
      std::vector<T> forceVector(3,T());
      AnalyticalConst3D<T,T> force(forceVector);

      if(isGPU)
          blockLattice.copyDataToCPU();
      blockLattice.defineExternalField(blockGeometry,8,DESCRIPTOR<T>::ExternalField::forceBeginsAt(),
              DESCRIPTOR<T>::ExternalField::sizeOfForce(), force);
      if(isGPU)
          blockLattice.copyDataToGPU();
  }

}

void getResults(UnitConverter<T,DESCRIPTOR> const& converter
    , BlockLattice3D<T,DESCRIPTOR>& blockLattice
    , int iT
    , BlockGeometry3D<T>& blockGeometry
    , Timer<T>& timer
    , bool converged)
{

  BlockVTKwriter3D<T> vtkWriter( caseName );
  if ( false || (iT%converter.getLatticeTime( 0.1 )==0 && iT>0) || converged )
  {
    timer.update( iT );
    timer.printStep( 2 );
  }

  // Writes the VTK files
  if ( false || ( iT%converter.getLatticeTime(1.0) )==0 || converged )
  {
    if(isGPU)
        blockLattice.copyDataToCPU();

    BlockLatticePhysVelocity3D<T, DESCRIPTOR> velocity( blockLattice, 0, converter );
    BlockLatticePhysPressure3D<T, DESCRIPTOR> pressure( blockLattice, 0, converter );
    BlockLatticeDensity3D<T,DESCRIPTOR> latticeDensity( blockLattice );
    BlockLatticeForce3D<T,DESCRIPTOR> latticeForce( blockLattice );
    vtkWriter.addFunctor( velocity );
    vtkWriter.addFunctor( pressure );
    vtkWriter.addFunctor( latticeDensity );
    vtkWriter.addFunctor( latticeForce );

    vtkWriter.write( iT );
  }

}

int main(int argc, char** argv){

  olbInit(&argc, &argv);

  singleton::directories().setOutputDir("/scratch/BHorvat/heliBlock/");
  OstreamManager clout (std::cout, "main");

  UnitConverterFromResolutionAndLatticeVelocity<T,DESCRIPTOR> const converter(
		  N
		  ,0.3*1.0/std::sqrt(3)
  	  	  ,length
		  ,80
		  ,0.0000146072
		  ,1.225
		  ,0);

  converter.print();
  converter.write(caseName);

  T deltaX = converter.getConversionFactorLength();
  Vector<T,3> origin(0,0,0);
  Vector<T,3> extend(length,height,width);
  IndicatorCuboid3D<T> cube (extend, origin);

  Cuboid3D<T> cuboid(cube, converter.getConversionFactorLength() );
  BlockGeometry3D<T> blockGeometry( cuboid );

  prepareGeometry(converter, blockGeometry);

  ForcedLudwigSmagorinskyBGKdynamics<T, DESCRIPTOR, BulkMomenta<T, DESCRIPTOR>> bulkDynamics (
      converter.getLatticeRelaxationFrequency(), 0.1);

  BlockLattice3D<T, DESCRIPTOR> lattice( blockGeometry.getNx(), blockGeometry.getNy(), blockGeometry.getNz(), &bulkDynamics );


  OnLatticeBoundaryCondition3D<T,DESCRIPTOR>*
    boundaryCondition = createInterpBoundaryCondition3D<T,DESCRIPTOR,LudwigSmagorinskyBGKdynamics>( lattice );

  prepareLattice(converter, lattice, bulkDynamics, *boundaryCondition, blockGeometry);

  BlockLatticeGeometry3D<T, DESCRIPTOR> geometry( lattice, blockGeometry );

  BlockVTKwriter3D<T> vtkWriter( caseName );
  vtkWriter.write( geometry );

  util::ValueTracer<T> converge( converter.getLatticeTime(1.0), 1e-3 );

  if(isGPU)
      lattice.copyDataToGPU();

  Timer<T> timer( converter.getLatticeTime( maxT ), blockGeometry.getStatistics().getNvoxel() );
  timer.start();

  for ( int iT = 0; iT < converter.getLatticeTime( maxT ); ++iT ) {
    setBoundaryValues(converter, lattice, iT, blockGeometry);
//    if(isGPU)
//        lattice.collideAndStreamGPU<ForcedLudwigSmagorinskyBGKdynamics<T, DESCRIPTOR, BulkMomenta<T, DESCRIPTOR> > >();
//    else
        lattice.collideAndStream<ForcedLudwigSmagorinskyBGKdynamics<T,DESCRIPTOR,BulkMomenta<T,DESCRIPTOR> > >();

    converge.takeValue( lattice.getStatistics().getAverageEnergy(), true );
    if( iT == converter.getLatticeTime( maxT ))
    {
    	std::cout << "Simulation has converged" << std::endl;
    	getResults( converter, lattice, iT, blockGeometry, timer, true);
    	break;
    }
    else
    {
    	getResults( converter, lattice, iT, blockGeometry, timer, false);
    }
  }

  timer.stop();
  timer.printSummary();

  delete boundaryCondition;

  return 0;
}
