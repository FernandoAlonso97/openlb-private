/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/* rayleighBenard2d.cpp:
 * Rayleigh-Benard convection rolls in 2D, simulated with
 * the thermal LB model by Z. Guo e.a., between a hot plate at
 * the bottom and a cold plate at the top.
 */


#include "olb3D.h"
#include "olb3D.hh"   // use only generic version!
#include <vector>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>


using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace std;

typedef double T;


#define NSDESCRIPTOR ForcedD3Q19Descriptor
#define TDESCRIPTOR AdvectionDiffusionD3Q7Descriptor




// const int maxIter  = 1000000;
// const int saveIter = 5000;

// Parameters for the simulation setup
T lx  = 1.0;      // length of the channel
T ly  = 1.0;      // height of the channel
T lz  = 1.0;      // depth of the channel
int N = 20;        // resolution of the model
const T Re = 5.;       // Reynolds number
const T Ra = 100.;     // Rayleigh number
const T Pr = 0.71;     // Prandtl number
const T maxPhysT = 1e4; // max. simulation time in s, SI unit
const T epsilon = 1.e-4; // precision of the convergence (residuum)

const T Tcold = 273.15;
const T Thot = 274.15;

void AnlayticalSoln(TLBconverter<T> &converter, SuperLattice3D<T, NSDESCRIPTOR> &NSlattice, SuperLattice3D<T, TDESCRIPTOR> &ADlattice, T Re, T Pr, T Ly, T Tc, T Th, int nx, int ny, int nz)
{
  T uan;
  T Tan;
  T uan_norm[ny];
  T Tan_norm[ny];
  T unum[ny];
  T Tnum[ny];
  T dT = Th - Tc;
  T To = (Th + Tc) / 2.0;
  T y;
  T u[3];
  T uo= converter.getLatticeU();
  //T uc[2];

  T l1_u = 0.0;
  T l1_T = 0.0;
  T l2_u = 0.0;
  T l2_T = 0.0;
  T linf_u = 0.0;
  T linf_T = 0.0;
  T l1_u_rel = 0.0;
  T l1_T_rel = 0.0;
  T l2_u_rel = 0.0;
  T l2_T_rel = 0.0;
  T linf_u_rel = 0.0;
  T linf_T_rel = 0.0;

  // int my_rank;
  // MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  //
  // char str[15];
  // sprintf(str, "./tmp/analytical_%d.txt", my_rank);

  ofstream file;
  file.open("./tmp/analytical.dat");

  for (int iY = 0; iY < ny; iY++) {
    y = (T(iY) / (T(ny) - 1.0))*Ly;
    uan = ((exp(Re*y / Ly) - 1) / (exp(Re) - 1));
    Tan = To + dT*((exp(Pr*Re*y / Ly) - 1) / (exp(Pr*Re) - 1));
    uan_norm[iY] = uan;
    Tan_norm[iY] = (Tan - To) / dT;
    NSlattice.getBlockLattice(0).get(nx / 2, iY, nz / 2).computeU(u);
    unum[iY] = u[0] / uo;
    Tnum[iY] = (ADlattice.getBlockLattice(0).get(nx / 2, iY, nz / 2).computeRho() - Tc) / dT;

    l1_u += fabs(unum[iY] - uan_norm[iY]);
    l1_u_rel += fabs(uan_norm[iY]);

    l1_T += fabs(Tnum[iY] - Tan_norm[iY]);
    l1_T_rel += fabs(Tan_norm[iY]);

    l2_u += pow(unum[iY] - uan_norm[iY], 2);
    l2_u_rel += pow(uan_norm[iY], 2);

    l2_T += pow(Tnum[iY] - Tan_norm[iY], 2);
    l2_T_rel += pow(Tan_norm[iY], 2);

    linf_u = fabs(unum[iY] - uan_norm[iY]) > linf_u ? fabs(unum[iY] - uan_norm[iY]) : linf_u;
    linf_u_rel = fabs(uan_norm[iY]) > linf_u_rel ? fabs(uan_norm[iY]) : linf_u_rel;

    linf_T = fabs(Tnum[iY] - Tan_norm[iY]) > linf_T ? fabs(Tnum[iY] - Tan_norm[iY]) : linf_T;
    linf_T_rel = fabs(Tan_norm[iY]) > linf_T_rel ? fabs(Tan_norm[iY]) : linf_T_rel;

    if (singleton::mpi().isMainProcessor()) {
      if (iY == 0) {
        file << "VARIABLES=\x22y\x22,\x22u_an\x22,\x22T_an\x22,\x22u_num\x22,\x22T_num\x22,\n";
      }
      file << iY << "     " << uan_norm[iY] << "    " << Tan_norm[iY] << "     " << unum[iY] << "    " << Tnum[iY] << endl;
    }
  }

  // cout << l1_u << "\t" << l1_u_rel << "\t" << l1_u / l1_u_rel << endl;
  // cout << l1_T << "\t" << l1_T_rel << "\t" << l1_T / l1_T_rel << endl;

  l2_u = sqrt(l2_u);
  l2_u_rel = sqrt(l2_u_rel);

  l2_T = sqrt(l2_T);
  l2_T_rel = sqrt(l2_T_rel);

  cout
  //<< l2_u << "\t" << l2_u_rel << "\t"
      << l2_u / l2_u_rel << endl;
  cout
  //<< l2_T << "\t" << l2_T_rel << "\t"
      << l2_T / l2_T_rel << endl;

  //cout << linf_u << "\t" << linf_u_rel << "\t" << linf_u / linf_u_rel << endl;
  //cout << linf_T << "\t" << linf_T_rel << "\t" << linf_T / linf_T_rel << endl;

  file.close();
}





// analytical solution from point light source in infinte domain
// appliacation from R3 to R1.
// effective for x in R3, only the distance to (0,0) is needed.
// documentation e.g. Biomedical Optics, Lihong V. Wang Hsin-I Wu
template <typename T, typename S>
class AnalyticalVelocityPorousPlate3D : public AnalyticalF3D<T, S> {
private:
  T _Re;
  T _u0;
  T _ly;
public:
  AnalyticalVelocityPorousPlate3D(T Re, T u0, T ly) : AnalyticalF3D<T, S>(1)
  {
    this->getName() = "AnalyticalVelocityPorousPlate3D";
    _Re = Re;
    _u0 = u0;
    _ly = ly;
  };

  bool operator()(T output[1], const S x[3])
  {
    output[0] = _u0*((exp(_Re* x[1] / _ly) - 1) / (exp(_Re) - 1));
    return true;
  };
};


template <typename T, typename S>
class AnalyticalTemperaturePorousPlate3D : public AnalyticalF3D<T, S> {
private:
  T _Re;
  T _Pr;
  T _ly;
  T _T0;
  T _deltaT;
public:
  AnalyticalTemperaturePorousPlate3D(T Re, T u0, T ly, T T0, T deltaT) : AnalyticalF3D<T, S>(1)
  {
    this->getName() = "AnalyticalTemperaturePorousPlate3D";
    _Re = Re;
    _Pr = Pr;
    _ly = ly;
    _T0 = T0;
    _deltaT = deltaT;
  };

  bool operator()(T output[1], const S x[3])
  {
    output[0] = _T0 + _deltaT*((exp(_Pr*_Re*x[1] / _ly) - 1) / (exp(_Pr*_Re) - 1));
    return true;
  };
};

void error( SuperGeometry3D<T>& superGeometry,
            SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
            SuperLattice3D<T, TDESCRIPTOR>& ADlattice,
            TLBconverter<T> const& converter,
            T Re )
{
  OstreamManager clout( std::cout, "error" );

  int input[1];
  T normAnaSol[1], absErr[1], relErr[1];

  AnalyticalVelocityPorousPlate3D<T,T> uSol(Re, converter.getCharU(), converter.getCharL());
  SuperLatticePhysVelocity3D<T,NSDESCRIPTOR> u1(NSlattice,converter);
  SuperLatticeFfromAnalyticalF3D<T,NSDESCRIPTOR> uSolLattice(uSol,NSlattice);

  SuperL2Norm3D<T> uL2Norm( uSolLattice - u1, superGeometry, 1 );
  SuperL2Norm3D<T> uSolL2Norm( uSolLattice, superGeometry, 1 );
  uL2Norm( absErr, input );
  uSolL2Norm( normAnaSol, input );
  relErr[0] = absErr[0] / normAnaSol[0];
  clout << "velocity-L2-error(abs)=" << absErr[0] << "; velocity-L2-error(rel)=" << relErr[0]*100. << "%" << std::endl;


  int inputT[1];
  T normAnaSolT[1], absErrT[1], relErrT[1];

  AnalyticalTemperaturePorousPlate3D<T,T> TSol(Re, converter.getPr(), converter.getCharL(), converter.getTemperatureLevel(), converter.getCharDeltaT());
  SuperLatticePhysTemperature3D<T, TDESCRIPTOR> T1(ADlattice, converter);
  SuperLatticeFfromAnalyticalF3D<T, TDESCRIPTOR> TSolLattice(TSol, ADlattice);

  SuperL2Norm3D<T> TL2Norm(TSolLattice - T1, superGeometry, 1);
  SuperL2Norm3D<T> TSolL2Norm(TSolLattice, superGeometry, 1);
  TL2Norm( absErrT, inputT );
  TSolL2Norm( normAnaSolT, inputT );
  relErrT[0] = absErrT[0] / normAnaSolT[0];
  clout << "temperature-L2-error(abs)=" << absErrT[0] << "; temperature-L2-error(rel)=" << relErrT[0]*100. << "%" << std::endl;


}



/// Stores geometry information in form of material numbers
void prepareGeometry(SuperGeometry3D<T>& superGeometry,
                     TLBconverter<T> &converter)
{

  OstreamManager clout(std::cout,"prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0,2);
  superGeometry.rename(2,1,0,1,0);
  //superGeometry.clean();

  std::vector<T> extend( 3, T(0) );
  extend[0] = lx;
  extend[1] = converter.getLatticeL();
  extend[2] = lz;
  std::vector<T> origin( 3, T(0) );
  IndicatorCuboid3D<T> bottom(extend, origin);
  /// Set material number for bottom
  superGeometry.rename(2,3,1,bottom);

  /// Removes all not needed boundary voxels outside the surface
  superGeometry.clean();
  /// Removes all not needed boundary voxels inside the surface
  superGeometry.innerClean();
  superGeometry.checkForErrors();

  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;
}

void prepareLattice( TLBconverter<T> &converter,
                     SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                     SuperLattice3D<T, TDESCRIPTOR>& ADlattice,
                     ForcedBGKdynamics<T, NSDESCRIPTOR> &bulkDynamics,
                     Dynamics<T, TDESCRIPTOR>& advectionDiffusionBulkDynamics,
                     sOnLatticeBoundaryCondition3D<T,NSDESCRIPTOR>& NSboundaryCondition,
                     sOnLatticeBoundaryCondition3D<T,TDESCRIPTOR>& TboundaryCondition,
                     SuperGeometry3D<T>& superGeometry )
{

  OstreamManager clout(std::cout,"prepareLattice");

  double Tomega  = converter.getOmegaT();
  double NSomega = converter.getOmega();

  /// define lattice Dynamics
  clout << "defining dynamics" << endl;

  ADlattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, TDESCRIPTOR>());
  NSlattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, NSDESCRIPTOR>());

  ADlattice.defineDynamics(superGeometry, 1, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 2, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 3, &advectionDiffusionBulkDynamics);
  NSlattice.defineDynamics(superGeometry, 1, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 2, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 3, &bulkDynamics);


  /// sets boundary
  NSboundaryCondition.addVelocityBoundary(superGeometry, 2, NSomega);
  NSboundaryCondition.addVelocityBoundary(superGeometry, 3, NSomega);
  TboundaryCondition.addTemperatureBoundary(superGeometry, 2, Tomega);
  TboundaryCondition.addTemperatureBoundary(superGeometry, 3, Tomega);
}

void setBoundaryValues(TLBconverter<T> &converter,
                       SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                       SuperLattice3D<T, TDESCRIPTOR>& ADlattice,
                       int iT, SuperGeometry3D<T>& superGeometry)
{

  if (iT == 0) {

    typedef advectionDiffusionLbHelpers<T,TDESCRIPTOR> TlbH;

    /// for each material set the defineRhoU and the Equilibrium
    std::vector<T> zero(2,T());
    T zerovel[TDESCRIPTOR<T>::d] = {0.,0.};
    AnalyticalConst3D<T,T> u(zero);
    AnalyticalConst3D<T,T> rho(1.);
    AnalyticalConst3D<T,T> force(zero);

    // T u_Re = Re * 0.0168523 / 4.0;

    T u_Re = Re * converter.getLatticeNu() * converter.getDeltaX();
    // T u_Re = converter.getLatticeU();
    // cout << converter.getU() << "/" << converter.getU() * (Re / 1. * 1.8e-5);
    //
    //   OstreamManager clout(std::cout,"u_Re");
    //   clout << u_Re << endl;

    AnalyticalConst3D<T,T> u_top(converter.getLatticeU(), u_Re);
    AnalyticalConst3D<T,T> u_bot(0.0, u_Re);
    // AnalyticalConst2D<T,T> u_bot[] = { (T)0.0, (T)0.1 };

    NSlattice.defineRhoU(superGeometry, 1, rho, u);
    NSlattice.iniEquilibrium(superGeometry, 1, rho, u);
    NSlattice.defineExternalField(superGeometry, 1,
                                  NSDESCRIPTOR<T>::ExternalField::forceBeginsAt,
                                  NSDESCRIPTOR<T>::ExternalField::sizeOfForce, force );
    NSlattice.defineRhoU(superGeometry, 2, rho, u_top);
    NSlattice.iniEquilibrium(superGeometry, 2, rho, u_top);
    NSlattice.defineExternalField(superGeometry, 2,
                                  NSDESCRIPTOR<T>::ExternalField::forceBeginsAt,
                                  NSDESCRIPTOR<T>::ExternalField::sizeOfForce, force );
    NSlattice.defineRhoU(superGeometry, 3, rho, u_bot);
    NSlattice.iniEquilibrium(superGeometry, 3, rho, u_bot);
    NSlattice.defineExternalField(superGeometry, 3,
                                  NSDESCRIPTOR<T>::ExternalField::forceBeginsAt,
                                  NSDESCRIPTOR<T>::ExternalField::sizeOfForce, force );

    AnalyticalConst3D<T,T> Cold(converter.latticeTemperature(Tcold));
    AnalyticalConst3D<T,T> Hot(converter.latticeTemperature(Thot));

    std::vector<T> tEqCold(TDESCRIPTOR<T>::q);
    std::vector<T> tEqHot(TDESCRIPTOR<T>::q);

    for (int iPop = 0; iPop < TDESCRIPTOR<T>::q; ++iPop) {
      tEqCold[iPop] = TlbH::equilibrium(iPop,converter.latticeTemperature(Tcold),zerovel);
      tEqHot[iPop] = TlbH::equilibrium(iPop,converter.latticeTemperature(Thot),zerovel);
    }

    AnalyticalConst3D<T,T> EqCold(tEqCold);
    AnalyticalConst3D<T,T> EqHot(tEqHot);

    ADlattice.defineRho(superGeometry, 1, Cold);
    ADlattice.definePopulations(superGeometry, 1, EqCold);
    ADlattice.defineExternalField(superGeometry, 1,
                                  TDESCRIPTOR<T>::ExternalField::velocityBeginsAt,
                                  TDESCRIPTOR<T>::ExternalField::sizeOfVelocity, u );
    ADlattice.defineRho(superGeometry, 3, Cold);
    ADlattice.definePopulations(superGeometry, 3, EqCold);
    ADlattice.defineExternalField(superGeometry, 3,
                                  TDESCRIPTOR<T>::ExternalField::velocityBeginsAt,
                                  TDESCRIPTOR<T>::ExternalField::sizeOfVelocity, u );
    ADlattice.defineRho(superGeometry, 2, Hot);
    ADlattice.definePopulations(superGeometry, 2, EqHot);
    ADlattice.defineExternalField(superGeometry, 2,
                                  TDESCRIPTOR<T>::ExternalField::velocityBeginsAt,
                                  TDESCRIPTOR<T>::ExternalField::sizeOfVelocity, u );

    /// Make the lattice ready for simulation
    NSlattice.initialize();
    ADlattice.initialize();
  }
}

void getResults(TLBconverter<T> &converter,
                SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                SuperLattice3D<T, TDESCRIPTOR>& ADlattice, int iT,
                SuperGeometry3D<T>& superGeometry,
                Timer<T>& timer,
                bool converged)
{

  OstreamManager clout(std::cout,"getResults");

  SuperVTMwriter3D<T> vtkWriter("thermalPorousPlate3d");
  SuperLatticePhysVelocity3D<T, NSDESCRIPTOR> velocity(NSlattice, converter);
  SuperLatticePhysPressure3D<T, NSDESCRIPTOR> presure(NSlattice, converter);
  SuperLatticePhysTemperature3D<T, TDESCRIPTOR> temperature(ADlattice, converter);
  vtkWriter.addFunctor( presure );
  vtkWriter.addFunctor( velocity );
  vtkWriter.addFunctor( temperature );

  const int vtkIter = converter.numTimeSteps(5.0);

  if (iT == 0) {
    /// Writes the converter log file
    writeLogFile(converter,"thermalPorousPlate3d");

    /// Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeGeometry3D<T, NSDESCRIPTOR> geometry(NSlattice, superGeometry);
    SuperLatticeCuboid3D<T, NSDESCRIPTOR> cuboid(NSlattice);
    SuperLatticeRank3D<T, NSDESCRIPTOR> rank(NSlattice);
    vtkWriter.write(geometry);
    vtkWriter.write(cuboid);
    vtkWriter.write(rank);

    vtkWriter.createMasterFile();
  }

  /// Writes the VTK files
  if (iT%vtkIter == 0 || converged) {
    NSlattice.getStatistics().print(iT,converter.physTime(iT));
    timer.print(iT);
    error(superGeometry, NSlattice, ADlattice, converter, Re);

    vtkWriter.write(iT);

    /* BlockLatticeReduction3D<T, TDESCRIPTOR> planeReduction(temperature);
     BlockGifWriter<T> gifWriter;
     gifWriter.write(planeReduction, iT, "temperature");*/
  }

}

T computeNusselt(SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                 SuperLattice3D<T,TDESCRIPTOR> &ADlattice,
                 TLBconverter<T> &converter)
{
  T u_T = T();
  for (int iC = 0; iC < ADlattice.getLoadBalancer().size(); iC++) {
    int nx = ADlattice.getBlockLattice(iC).getNx();
    int ny = ADlattice.getBlockLattice(iC).getNy();
    int nz = ADlattice.getBlockLattice(iC).getNz();

    for (int iX = 0; iX < nx; ++iX) {
      for (int iY = 0; iY < ny; ++iY) {
        for (int iZ = 0; iZ < nz; ++iZ) {
          T uy[2];
          NSlattice.getBlockLattice(iC).get(iX, iY, iZ).computeU(uy);
          u_T += uy[1] * ADlattice.getBlockLattice(iC).get(iX, iY, iZ).computeRho();
        }
      }
    }
  }

#ifdef PARALLEL_MODE_MPI
  singleton::mpi().reduceAndBcast(u_T, MPI_SUM);
#endif
  T nusselt = (T)1 + u_T*converter.getDeltaX() / (converter.getLatticeAlpha() * converter.getCharDeltaT());

  return nusselt;
}

int main(int argc, char *argv[])
{

  /// === 1st Step: Initialization ===
  OstreamManager clout(std::cout,"main");
  olbInit(&argc, &argv);
  singleton::directories().setOutputDir("./tmp/");

  if (argc == 2) {
    N = atoi(argv[1]);
  }

  /// TLBconverter(int dim, T latticeL, T latticeU, T charNu, T charLambda, T charCp, T charBeta, T charDeltaT, T charL = 1, T charU = 1,
  ///              T charRho = 1, T charT = 273.15, T pressureLevel = 0)

  TLBconverter<T> converter(
    3,  // dim
    (T) 1.0/N, // latticeL
    (T) 0.1*5./N,  // latticeU
    (T) 1e-3,  // charNu
    (T) 0.03,      // charLambda
    (T) Pr * 0.03 / 1e-3 / 1.0,    // charCp = Pr * charLambda / charNu / charRho
    (T) Ra * 1e-3 * 1e-3 / Pr / 9.81 / (Thot - Tcold) / pow(1.0, 3), // charBeta = Ra * charNu^2 / Pr / g / charDeltaT / charL^3
    (T) Thot - Tcold, // charDeltaT
    (T) 1.0, // charL
    (T) sqrt( 9.81 * Ra * 1e-3 * 1e-3 / Pr / 9.81 / (Thot - Tcold) / pow(1.0, 3) * (Thot - Tcold) * 1.0 ) // charU
  );
  converter.print();

  // clout << "Starting simulation with parameters Ra=" << Ra << " Pr=" << Pr << " Re=" << Re << " N=" << N << " dt=" << converter.getDeltaT() << endl;
  // writeLogFile(converter, "rayleighBenard2d");

  // const double Raprova = converter.getN() * converter.getN() *
  //                        converter.getN() * converter.getDeltaTemperature() *
  //                        converter.getGravity() / (converter.getNu()*converter.getKappa());
  //
  // const double Prprova = converter.getNu() / converter.getKappa();
  //
  // clout << Raprova << " " << Prprova << endl;
  clout << converter.latticeTemperature(Tcold) << " " << converter.latticeTemperature(Thot) << endl;
  clout << converter.physAlpha() << " " << converter.physTemperature() << endl;
  clout << converter.getPr() << " " << converter.getRa() << endl;
  clout << converter.getOmega() << " " << converter.getOmegaT() << endl;
  clout << sqrt(converter.getPr()/converter.getRa()) * converter.getDeltaT() / (converter.getDeltaX()*converter.getDeltaX()) << "\t" << converter.getLatticeNu() << endl;

  /// === 2nd Step: Prepare Geometry ===
  lz = 3.0 * converter.getLatticeL();
  std::vector<T> extend(3,T());
  extend[0] = lx;
  extend[1] = ly;
  extend[2] = lz;
  std::vector<T> origin(3,T());
  IndicatorCuboid3D<T> cuboid(extend, origin);

  /// Instantiation of a cuboidGeometry with weights
#ifdef PARALLEL_MODE_MPI
  const int noOfCuboids = singleton::mpi().getSize();
#else
  const int noOfCuboids = 7;
#endif
  CuboidGeometry3D<T> cuboidGeometry(cuboid, converter.getLatticeL(), noOfCuboids);
  cuboidGeometry.setPeriodicity(true,false, true);

  /// Instantiation of a loadBalancer
  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);

  /// Instantiation of a superGeometry
  SuperGeometry3D<T> superGeometry(cuboidGeometry, loadBalancer, 2);

  prepareGeometry(superGeometry, converter);

  /// === 3rd Step: Prepare Lattice ===

  SuperLattice3D<T, TDESCRIPTOR> ADlattice(superGeometry);
  SuperLattice3D<T, NSDESCRIPTOR> NSlattice(superGeometry);

  sOnLatticeBoundaryCondition3D<T, NSDESCRIPTOR> NSboundaryCondition(NSlattice);
  createLocalBoundaryCondition3D<T, NSDESCRIPTOR>(NSboundaryCondition);

  sOnLatticeBoundaryCondition3D<T, TDESCRIPTOR> TboundaryCondition(ADlattice);
  createAdvectionDiffusionBoundaryCondition3D<T, TDESCRIPTOR>(TboundaryCondition);

  ForcedBGKdynamics<T, NSDESCRIPTOR> NSbulkDynamics(
    converter.getOmega(),
    instances::getBulkMomenta<T,NSDESCRIPTOR>());

  AdvectionDiffusionBGKdynamics<T, TDESCRIPTOR> TbulkDynamics (
    converter.getOmegaT(),
    instances::getAdvectionDiffusionBulkMomenta<T,TDESCRIPTOR>()
  );

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  // This coupling must be necessarily be put on the Navier-Stokes lattice!!
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

  int nx = converter.numCells(lx) + 1;
  int ny = converter.numCells(ly) + 1;
  int nz = converter.numCells(lz) + 1;

  std::vector<T> dir;
  dir.push_back(T());
  dir.push_back((T)1);

  double forceconvfactor = converter.getCharDeltaT()*converter.getCharBeta();

  NavierStokesAdvectionDiffusionCouplingGenerator3D<T,NSDESCRIPTOR>
  coupling(0, converter.numCells(lx), 0, converter.numCells(lx), 0, converter.numCells(lz),
           converter.getGravity(), converter.latticeTemperature(Tcold), 1., forceconvfactor, dir);

  NSlattice.addLatticeCoupling(superGeometry, 1, coupling, ADlattice);
  NSlattice.addLatticeCoupling(superGeometry, 2, coupling, ADlattice);
  NSlattice.addLatticeCoupling(superGeometry, 3, coupling, ADlattice);
  NSlattice.addLatticeCoupling(superGeometry, 4, coupling, ADlattice);

  prepareLattice(converter,
                 NSlattice, ADlattice,
                 NSbulkDynamics, TbulkDynamics,
                 NSboundaryCondition, TboundaryCondition, superGeometry );


  /// === 4th Step: Main Loop with Timer ===
  Timer<T> timer(converter.numTimeSteps(maxPhysT), superGeometry.getStatistics().getNvoxel() );
  timer.start();

  util::ValueTracer<T> converge(converter.numTimeSteps(1.0),epsilon);
  for (int iT = 0; iT < converter.numTimeSteps(maxPhysT); ++iT) {

    if (converge.hasConverged()) {
      clout << "Simulation converged." << endl;
      getResults(converter, NSlattice, ADlattice, iT, superGeometry, timer, converge.hasConverged());

      //AnlayticalSoln(converter, NSlattice, ADlattice,  Re, Pr, 1.0 /*Ly*/, 0 /*Tc*/, 1.0 /*Th*/, nx, ny,nz);

      clout << "Time " << iT << "." << std::endl;
      clout << "Nusselt = " << computeNusselt(NSlattice, ADlattice, converter) << endl;

      break;
    }

    /// === 5th Step: Definition of Initial and Boundary Conditions ===
    setBoundaryValues(converter, NSlattice, ADlattice, iT, superGeometry);

    /// === 6th Step: Collide and Stream Execution ===
    ADlattice.collideAndStream();
    NSlattice.collideAndStream();

    NSlattice.executeCoupling();

    /// === 7th Step: Computation and Output of the Results ===
    getResults(converter, NSlattice, ADlattice, iT, superGeometry, timer, converge.hasConverged());
    converge.takeValue(ADlattice.getStatistics().getAverageEnergy(),false);
  }
}
