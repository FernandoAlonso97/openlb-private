/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

// natural convection of air in a square cavity in 2D


#include "olb2D.h"
#include "olb2D.hh"   // use only generic version!

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace std;

typedef double T;

#define NSDESCRIPTOR ForcedDynOmegaD2Q9Descriptor
#define TDESCRIPTOR SmagorinskyAdvectionDiffusionD2Q5Descriptor

// Parameters for the simulation setup
double Ra = 1e4;  // Rayleigh-Zahl
const double Pr = 0.71; // Prandtl-Zahl

T lx;

int N = 40; // resolution of the model

const T maxPhysT = 1e4;   // max. simulation time in s, SI unit
const T epsilon = 1.e-3;  // precision of the convergence (residuum)

const T Tcold = 275.15;
const T Thot = 285.15;
const T Tmean = (Tcold + Thot) / 2.0;

/// Values from the literature studies from Davis & Dixit
T LitVelocity3[] = { 3.649, 3.696, 1.013 };
T LitPosition3[] = { 0.813, 0.178 };
T LitVelocity4[] = { 16.178, 19.617, 1.212 };
T LitPosition4[] = { 0.823, 0.119 };
T LitVelocity5[] = { 34.730, 68.590, 1.975 };
T LitPosition5[] = { 0.855, 0.066 };
T LitVelocity6[] = { 64.530, 219.36, 3.400 };
T LitPosition6[] = { 0.850, 0.036 };
T LitVelocity7[] = { 164.24, 701.92, 4.831};
T LitPosition7[] = { 0.851, 0.020 };
T LitVelocity8[] = { 389.88, 2241.37, 5.749};
T LitPosition8[] = { 0.937, 0.011 };
T LitVelocity9[] = { 503.24, 6820.07, 13.552};
T LitPosition9[] = { 0.966, 0.0064 };
T LitVelocity10[] = { 2323.00, 21463.00, 9.239};
T LitPosition10[] = { 0.940, 0.491 };
T LitNusselt3 = 1.117;
T LitNusselt4 = 2.238;
T LitNusselt5 = 4.509;
T LitNusselt6 = 8.817;
T LitNusselt7 = 16.790;
T LitNusselt8 = 30.506;
T LitNusselt9 = 57.350;
T LitNusselt10 = 103.663;

/// Compute the nusselt number at the left wall
T computeNusselt(SuperGeometry2D<T>& superGeometry,
                 SuperLattice2D<T, NSDESCRIPTOR>& NSlattice,
                 SuperLattice2D<T, TDESCRIPTOR>& ADlattice,
                 TLBconverter<T> &converter,
                 SmagorinskyForcedBGKdynamics<T, NSDESCRIPTOR>& NSdynamics,
                 SmagorinskyAdvectionDiffusionBGKdynamics<T, TDESCRIPTOR>& ADdynamics )
{

  OstreamManager clout(std::cout,"computeNusselt");

  int voxel = 0, material = 0;
  double T_x = 0, T_x_phys = 0;
  double T_xplus1 = 0, T_xplus1_phys = 0;
  double T_xplus2 = 0, T_xplus2_phys = 0;
  double T_xplus3 = 0;
  double x_phys = 0;
  double centraldiff_phys = 0;
  double Q_quer = 0, Q_quer_ber = 0;
  double q_2ndOrder = 0, q_3rdOrder = 0;
  double q_uT = 0;
  double q_j = 0;
  double u_xplus1[2];

  int voxelPr = 0;
  T PrTurb = 0.0;
  T tauT = 1. / converter.getOmegaT();
  T tau = 1. / converter.getOmega();

  for (int iC = 0; iC < NSlattice.getLoadBalancer().size(); iC++) {
    int nx = NSlattice.getBlockLattice(iC).getNx()- 1;
    int ny = NSlattice.getBlockLattice(iC).getNy();


    for (int iX = 0; iX < nx; ++iX) {
      for (int iY = 0; iY < ny; ++iY) {

        material = superGeometry.getBlockGeometry(iC).getMaterial(iX,iY);

        T_x = ADlattice.getBlockLattice(iC).get(iX,iY).computeRho();
        T_x_phys = converter.physTemperature(T_x);
        T_xplus1 = ADlattice.getBlockLattice(iC).get(iX+1,iY).computeRho();
        NSlattice.getBlockLattice(iC).get(iX+1,iY).computeU(u_xplus1);
        T_xplus1_phys = converter.physTemperature(T_xplus1);
        T_xplus2 = ADlattice.getBlockLattice(iC).get(iX+2,iY).computeRho();
        T_xplus3 = ADlattice.getBlockLattice(iC).get(iX+3,iY).computeRho();
        x_phys = converter.physLength();
        centraldiff_phys = (T_x_phys - T_xplus1_phys)/x_phys;

        Q_quer = centraldiff_phys*converter.getCharL()/10.0;
        if ( material == 2 ) {
          Q_quer_ber = Q_quer_ber + Q_quer;
          q_2ndOrder += (3.0*T_x - 4.0*T_xplus1 + 1.0*T_xplus2)/2.0/converter.getDeltaX();
          q_3rdOrder += (11./6.*T_x - 3.*T_xplus1 + 3./2.*T_xplus2 - 1./3. * T_xplus3)/converter.getDeltaX();
          q_uT += u_xplus1[0]*T_xplus1;

          T j[3];
          ADlattice.getBlockLattice(iC).get(iX+1,iY).computeJ(j);
          T rho = NSlattice.getBlockLattice(iC).get(iX+1,iY).computeRho();

          T latticeCp = converter.getCharCp() * converter.getCharDeltaT() / converter.physVelocity() / converter.physVelocity();

          // T flux_x = rho * latticeCp * j[0] * (tau - 0.5) / tau;
          T flux_x = j[0] * (tauT - 0.5) / tauT;

          q_j += flux_x;
          voxel++;
        }
        else if ( material == 1) {
          T tauEff = 1. / NSdynamics.getSmagorinskyOmega( NSlattice.getBlockLattice(iC).get(iX,iY) );
          T tauTEff = 1. / ADdynamics.getSmagorinskyOmega( ADlattice.getBlockLattice(iC).get(iX,iY) );
          T nuTurb = NSDESCRIPTOR<T>::invCs2 * (tauEff - tau);
          T alphaTurb = TDESCRIPTOR<T>::invCs2 * (tauTEff - tauT);
          PrTurb += nuTurb / alphaTurb;
          voxelPr++;
          clout << nuTurb << "; " << alphaTurb << "; " << nuTurb / alphaTurb << "; " << endl;
        }
      }
    }
  }

#ifdef PARALLEL_MODE_MPI
  singleton::mpi().reduceAndBcast(Q_quer_ber, MPI_SUM);
  singleton::mpi().reduceAndBcast(q_2ndOrder, MPI_SUM);
  singleton::mpi().reduceAndBcast(q_j, MPI_SUM);
  singleton::mpi().reduceAndBcast(q_uT, MPI_SUM);
  singleton::mpi().reduceAndBcast(PrTurb, MPI_SUM);
  singleton::mpi().reduceAndBcast(voxelPr, MPI_SUM);
  singleton::mpi().reduceAndBcast(voxel, MPI_SUM);
#endif

  q_j = q_j / (T)voxel;
  PrTurb /= (T)voxelPr;

  T nusselt = Q_quer_ber/(T)voxel;
  T nusselt_2nd = q_2ndOrder/(T)voxel;
  T nusselt_by_uT = 1.0 + q_uT/(T)voxel / converter.physAlpha() / converter.getCharL();
  T nusselt_by_j = q_j / converter.getLatticeAlpha() * converter.numCells(converter.getCharL());

  clout << "Nu_0 (2nd Order): " << nusselt_2nd;
  clout << " Nu_0 (uT): " << nusselt_by_uT;
  clout << " Nu_0 (j): " << nusselt_by_j;

  // compare to De Val Davis' benchmark solutions
  if (Ra == 1e3) {
    clout << "; Error: " << (T) (1.117 - nusselt_2nd)/1.117 * 100.0 << " %" << endl;
    clout << "; Error: " << (T) (1.117 - nusselt_by_j)/1.117 * 100.0 << " %" << endl;
  }
  else if (Ra == 1e4) {
    clout << "; Error: " << (T) (2.238 - nusselt_2nd)/2.238 * 100.0 << " %" << endl;
    clout << "; Error: " << (T) (2.238 - nusselt_by_j)/2.238 * 100.0 << " %" << endl;
  }
  else if (Ra == 1e5) {
    clout << "; Error: " << (T) (4.509 - nusselt_2nd)/4.509 * 100.0 << " %" << endl;
    clout << "; Error: " << (T) (4.509 - nusselt_by_j)/4.509 * 100.0 << " %" << endl;
  }
  else if (Ra == 1e6) {
    clout << "; Error: " << (T) (8.817 - nusselt_2nd)/8.817 * 100.0 << " %" << endl;
    clout << "; Error: " << (T) (8.817 - nusselt_by_j)/8.817 * 100.0 << " %" << endl;
  }
  else {
    clout << endl;
  }

  clout << "Pr_turb = " << PrTurb << endl;

  return nusselt_2nd;
}

/// Stores geometry information in form of material numbers
void prepareGeometry(SuperGeometry2D<T>& superGeometry,
                     TLBconverter<T> &converter)
{

  OstreamManager clout(std::cout,"prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0,4);

  std::vector<T> extend(2,T());
  extend[0] = lx;
  extend[1] = lx;
  std::vector<T> origin(2,T());
  origin[0] = converter.getLatticeL();
  origin[1] = 0.5*converter.getLatticeL();
  IndicatorCuboid2D<T> cuboid2(extend, origin);

  superGeometry.rename(4,1,cuboid2);

  std::vector<T> extendwallleft(2,T(0));
  extendwallleft[0] = converter.getLatticeL();
  extendwallleft[1] = lx;
  std::vector<T> originwallleft(2,T(0));
  originwallleft[0] = 0.0;
  originwallleft[1] = 0.0;
  IndicatorCuboid2D<T> wallleft(extendwallleft, originwallleft);

  std::vector<T> extendwallright(2,T(0));
  extendwallright[0] = converter.getLatticeL();
  extendwallright[1] = lx;
  std::vector<T> originwallright(2,T(0));
  originwallright[0] = lx+converter.getLatticeL();
  originwallright[1] = 0.0;
  IndicatorCuboid2D<T> wallright(extendwallright, originwallright);

  superGeometry.rename(4,2,1,wallleft);
  superGeometry.rename(4,3,1,wallright);


  /// Removes all not needed boundary voxels outside the surface
  superGeometry.clean();
  /// Removes all not needed boundary voxels inside the surface
  superGeometry.innerClean();
  superGeometry.checkForErrors();

  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;

}

void prepareLattice( TLBconverter<T> &converter,
                     SuperLattice2D<T, NSDESCRIPTOR>& NSlattice,
                     SuperLattice2D<T, TDESCRIPTOR>& ADlattice,
                     ForcedBGKdynamics<T, NSDESCRIPTOR> &bulkDynamics,
                     Dynamics<T, TDESCRIPTOR>& advectionDiffusionBulkDynamics,
                     sOnLatticeBoundaryCondition2D<T,NSDESCRIPTOR>& NSboundaryCondition,
                     sOnLatticeBoundaryCondition2D<T,TDESCRIPTOR>& TboundaryCondition,
                     SuperGeometry2D<T>& superGeometry )
{

  OstreamManager clout(std::cout,"prepareLattice");
  clout << "Prepare Lattice ..." << std::endl;

  double omega  = converter.getOmega();
  double Tomega  = converter.getOmegaT();

  ADlattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, TDESCRIPTOR>());
  NSlattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, NSDESCRIPTOR>());

  ADlattice.defineDynamics(superGeometry, 1, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 2, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 3, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 4, &instances::getBounceBack<T, TDESCRIPTOR>());

  NSlattice.defineDynamics(superGeometry, 1, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 2, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 3, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 4, &instances::getBounceBack<T, NSDESCRIPTOR>());

  /// sets boundary
  TboundaryCondition.addTemperatureBoundary(superGeometry, 2, Tomega);
  TboundaryCondition.addTemperatureBoundary(superGeometry, 3, Tomega);
  NSboundaryCondition.addVelocityBoundary(superGeometry, 2, omega);
  NSboundaryCondition.addVelocityBoundary(superGeometry, 3, omega);

  /// define initial conditions
  AnalyticalConst2D<T,T> rho(1.);
  AnalyticalConst2D<T,T> u0(0.0, 0.0);
  AnalyticalConst2D<T,T> T_cold(converter.latticeTemperature(Tcold));
  AnalyticalConst2D<T,T> T_hot(converter.latticeTemperature(Thot));
  AnalyticalConst2D<T,T> T_mean(converter.latticeTemperature(Tmean));

  /// for each material set Rho, U and the Equilibrium
  NSlattice.defineRhoU(superGeometry, 1, rho, u0);
  NSlattice.iniEquilibrium(superGeometry, 1, rho, u0);

  ADlattice.defineRho(superGeometry, 1, T_mean);
  ADlattice.iniEquilibrium(superGeometry, 1, T_mean, u0);
  ADlattice.defineRho(superGeometry, 2, T_hot);
  ADlattice.iniEquilibrium(superGeometry, 2, T_hot, u0);
  ADlattice.defineRho(superGeometry, 3, T_cold);
  ADlattice.iniEquilibrium(superGeometry, 3, T_cold, u0);

  /// Make the lattice ready for simulation
  NSlattice.initialize();
  ADlattice.initialize();

  clout << "Prepare Lattice ... OK" << std::endl;
}

void setBoundaryValues(TLBconverter<T> &converter,
                       SuperLattice2D<T, NSDESCRIPTOR>& NSlattice,
                       SuperLattice2D<T, TDESCRIPTOR>& ADlattice,
                       int iT, SuperGeometry2D<T>& superGeometry)
{

  // nothing to do here

}

void getResults(TLBconverter<T> &converter,
                SuperLattice2D<T, NSDESCRIPTOR>& NSlattice,
                SuperLattice2D<T, TDESCRIPTOR>& ADlattice, int iT,
                SuperGeometry2D<T>& superGeometry,
                Timer<T>& timer,
                bool converged)
{

  OstreamManager clout(std::cout,"getResults");

  SuperVTMwriter2D<T> vtkWriter("thermalNaturalConvection2D");
  SuperLatticePhysVelocity2D<T, NSDESCRIPTOR> velocity(NSlattice, converter);
  SuperLatticePhysPressure2D<T, NSDESCRIPTOR> presure(NSlattice, converter);
  SuperLatticePhysTemperature2D<T, TDESCRIPTOR> temperature(ADlattice, converter);
  vtkWriter.addFunctor( presure );
  vtkWriter.addFunctor( velocity );
  vtkWriter.addFunctor( temperature );

  const int vtkIter = converter.numTimeSteps(0.01);

  if (iT == 0) {
    /// Writes the converter log file
    writeLogFile(converter,"thermalNaturalConvection2D");

    /// Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeGeometry2D<T, NSDESCRIPTOR> geometry(NSlattice, superGeometry);
    SuperLatticeCuboid2D<T, NSDESCRIPTOR> cuboid(NSlattice);
    SuperLatticeRank2D<T, NSDESCRIPTOR> rank(NSlattice);
    vtkWriter.write(geometry);
    vtkWriter.write(cuboid);
    vtkWriter.write(rank);

    vtkWriter.createMasterFile();
  }

  /// Writes the VTK files
  if (converged) {

    SuperLatticePhysVelocity2D<T, NSDESCRIPTOR> velocityField(NSlattice, converter);
    AnalyticalFfromSuperLatticeF2D<T, NSDESCRIPTOR> interpolation(velocityField, true, 1);

    timer.update(iT);
    timer.printStep();

    /// NSLattice statistics console output
    NSlattice.getStatistics().print(iT,converter.physTime(iT));
    /// ADLattice statistics console output
    ADlattice.getStatistics().print(iT,converter.physTime(iT));

    vtkWriter.write(iT);

    BlockLatticeReduction2D<T, TDESCRIPTOR> planeReduction(temperature);
    BlockGifWriter<T> gifWriter;
    gifWriter.write(planeReduction, Tcold-.1, Thot+.1, iT, "temperature");

    SuperEuklidNorm2D<T, NSDESCRIPTOR> normVel( velocity );
    BlockLatticeReduction2D<T, NSDESCRIPTOR> planeReduction2(normVel);
    BlockGifWriter<T> gifWriter2;
    gifWriter2.write( planeReduction2, iT, "velocity" );

    ///Write Nusselt Number
    int voxel = 0, material = 0;
    double T_x = 0, T_x_phys = 0;
    double T_xplus1 = 0, T_xplus1_phys = 0;
    double T_xplus2 = 0, T_xplus2_phys = 0;
    double T_xplus3 = 0;
    double x_phys = 0;
    double centraldiff_phys = 0;
    double Q_quer = 0, Q_quer_ber = 0;
    double q_2ndOrder = 0, q_3rdOrder = 0;
    double q_uT = 0;
    double q_j = 0;
    double u_xplus1[2];

    for (int iC = 0; iC < NSlattice.getLoadBalancer().size(); iC++) {
      int ny = NSlattice.getBlockLattice(iC).getNy();

      int iX = 0;

      for (int iY = 0; iY < ny; ++iY) {

        material = superGeometry.getBlockGeometry(iC).getMaterial(iX,iY);

        T_x = ADlattice.getBlockLattice(iC).get(iX,iY).computeRho();
        T_x_phys = converter.physTemperature(T_x);
        T_xplus1 = ADlattice.getBlockLattice(iC).get(iX+1,iY).computeRho();
        NSlattice.getBlockLattice(iC).get(iX+1,iY).computeU(u_xplus1);
        T_xplus1_phys = converter.physTemperature(T_xplus1);
        T_xplus2 = ADlattice.getBlockLattice(iC).get(iX+2,iY).computeRho();
        T_xplus3 = ADlattice.getBlockLattice(iC).get(iX+3,iY).computeRho();
        x_phys = converter.physLength();
        centraldiff_phys = (T_x_phys - T_xplus1_phys)/x_phys;

        Q_quer = centraldiff_phys*converter.getCharL()/10.0;
        if ( material == 2 ) {
          Q_quer_ber = Q_quer_ber + Q_quer;
          q_2ndOrder += (3.0*T_x - 4.0*T_xplus1 + 1.0*T_xplus2)/2.0/converter.getDeltaX();
          q_3rdOrder += (11./6.*T_x - 3.*T_xplus1 + 3./2.*T_xplus2 - 1./3. * T_xplus3)/converter.getDeltaX();
          q_uT += u_xplus1[0]*T_xplus1;

          T j[3];
          ADlattice.getBlockLattice(iC).get(iX+1,iY).computeJ(j);
          T rho = NSlattice.getBlockLattice(iC).get(iX+1,iY).computeRho();

          T latticeCp = converter.getCharCp() * converter.getCharDeltaT() / converter.physVelocity() / converter.physVelocity();

          T tau = 1. / converter.getOmegaT();

          // T flux_x = rho * latticeCp * j[0] * (tau - 0.5) / tau;
          T flux_x = j[0] * (tau - 0.5) / tau;

          q_j += flux_x;
          voxel++;
        }
      }
    }

#ifdef PARALLEL_MODE_MPI
    singleton::mpi().reduceAndBcast(Q_quer_ber, MPI_SUM);
    singleton::mpi().reduceAndBcast(q_2ndOrder, MPI_SUM);
    singleton::mpi().reduceAndBcast(q_j, MPI_SUM);
    singleton::mpi().reduceAndBcast(q_uT, MPI_SUM);
    singleton::mpi().reduceAndBcast(voxel, MPI_SUM);
#endif

    q_j = q_j / (T)voxel;

    T nusselt = Q_quer_ber/(T)voxel;
    T nusselt_2nd = q_2ndOrder/(T)voxel;
    T nusselt_by_uT = 1.0 + q_uT/(T)voxel / converter.physAlpha() / converter.getCharL();
    T nusselt_by_j = q_j / converter.getLatticeAlpha() * converter.numCells(converter.getCharL());

    clout << "Nu_0 (2nd Order): " << nusselt_2nd;
    clout << " Nu_0 (uT): " << nusselt_by_uT;
    clout << " Nu_0 (j): " << nusselt_by_j;

    // compare to De Val Davis' benchmark solutions
    if (Ra == 1e3) {
      clout << "; Error: " << (T) (1.117 - nusselt_2nd)/1.117 * 100.0 << " %" << endl;
      clout << "; Error: " << (T) (1.117 - nusselt_by_j)/1.117 * 100.0 << " %" << endl;
    }
    else if (Ra == 1e4) {
      clout << "; Error: " << (T) (2.238 - nusselt_2nd)/2.238 * 100.0 << " %" << endl;
      clout << "; Error: " << (T) (2.238 - nusselt_by_j)/2.238 * 100.0 << " %" << endl;
    }
    else if (Ra == 1e5) {
      clout << "; Error: " << (T) (4.509 - nusselt_2nd)/4.509 * 100.0 << " %" << endl;
      clout << "; Error: " << (T) (4.509 - nusselt_by_j)/4.509 * 100.0 << " %" << endl;
    }
    else if (Ra == 1e6) {
      clout << "; Error: " << (T) (8.817 - nusselt_2nd)/8.817 * 100.0 << " %" << endl;
      clout << "; Error: " << (T) (8.817 - nusselt_by_j)/8.817 * 100.0 << " %" << endl;
    }
    else {
      clout << endl;
    }


    clout << "Simulation converged." << endl;
    clout << "Time " << iT << "." << std::endl;

    /// Initialize vectors for data output
    T xVelocity[2] = { T() };
    T outputVelX[2] = { T() };
    T yVelocity[2] = { T() };
    T outputVelY[2] = { T() };
    const int outputSize = 1000;
    Vector<T, outputSize> velX;
    Vector<T, outputSize> posX;
    Vector<T, outputSize> velY;
    Vector<T, outputSize> posY;

    /// loop for the resolution of the cavity at x = lx/2 in yDirection and vice versa
    for (int n = 0; n < outputSize; ++n) {
      double yPosition[2] = { lx / 2, lx * n / (T) outputSize };
      double xPosition[2] = { lx * n / (T) outputSize, lx / 2 };

      /// Interpolate xVelocity at x = lx/2 for each yPosition
      interpolation(xVelocity, yPosition);
      interpolation(yVelocity, xPosition);
      /// Store the interpolated values to compare them among each other in order to detect the maximum
      velX[n] = xVelocity[0];
      posY[n] = yPosition[1];
      velY[n] = yVelocity[1];
      posX[n] = xPosition[0];

      /// Initialize output with the corresponding velocities and positions at the origin
      if (n == 0) {
        outputVelX[0] = velX[0];
        outputVelX[1] = posY[0];
        outputVelY[0] = velY[0];
        outputVelY[1] = posX[0];
      }
      /// look for the maximum velocity in xDirection and the corresponding position in yDirection
      if (n > 0 && velX[n] > outputVelX[0]) {
        outputVelX[0] = velX[n];
        outputVelX[1] = posY[n];
      }
      /// look for the maximum velocity in yDirection and the corresponding position in xDirection
      if (n > 0 && velY[n] > outputVelY[0]) {
        outputVelY[0] = velY[n];
        outputVelY[1] = posX[n];
      }
    }

    /// write the result to an output textFile
    if (singleton::mpi().isMainProcessor()) {
      std::fstream fs;
      fs.open("output.txt",
              std::fstream::in | std::fstream::out | std::fstream::app);

      fs << "Resolution"<<"; iT" << "; Rayleigh" <<"; xVelocity in yDir" << "; yVelocity in xDir" <<
         "; yMaxVel/xMaxVel" << "; yCoord of xMaxVel" << "; xCoord of yMaxVel" << "; Nusselt" <<
         "; ErrorXVel" << "; ErrorYVel" << "; ErrorX/YVel" << "; ErrorYCoord" << "; ErrorXCoord" << "; ErrorNusselt" << endl;

      fs << N << "; " << iT << "; " << Ra << "; ";
      fs << outputVelX[0] / converter.physAlpha() * converter.getCharL() << "; " <<
         outputVelY[0] / converter.physAlpha() * converter.getCharL() << "; " <<
         outputVelY[0] / outputVelX[0] << "; " << outputVelX[1]/lx << "; "<<
         outputVelY[1]/lx << "; " << nusselt_2nd << "; ";

      /// compute the relative error between the simulation result and the literature study
      // for low Rayleigh numbers: compare to Davis benchmark solution
      if (Ra == 1e3) {
        fs <<   (T) fabs((LitVelocity3[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity3[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity3[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity3[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity3[2] - outputVelY[0] / outputVelX[0])  / LitVelocity3[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition3[0] - outputVelX[1] / lx) / LitPosition3[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition3[1] - outputVelY[1] / lx) / LitPosition3[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt3 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      else if (Ra == 1e4) {
        fs <<   (T) fabs((LitVelocity4[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity4[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity4[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity4[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity4[2] - outputVelY[0] / outputVelX[0])  / LitVelocity4[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition4[0] - outputVelX[1] / lx) / LitPosition4[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition4[1] - outputVelY[1] / lx) / LitPosition4[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt4 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      else if (Ra == 1e5) {
        fs <<   (T) fabs((LitVelocity5[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity5[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity5[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity5[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity5[2] - outputVelY[0] / outputVelX[0])  / LitVelocity5[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition5[0] - outputVelX[1] / lx) / LitPosition5[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition5[1] - outputVelY[1] / lx) / LitPosition5[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt5 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      else if (Ra == 1e6) {
        fs <<   (T) fabs((LitVelocity6[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity6[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity6[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity6[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity6[2] - outputVelY[0] / outputVelX[0])  / LitVelocity6[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition6[0] - outputVelX[1] / lx) / LitPosition6[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition6[1] - outputVelY[1] / lx) / LitPosition6[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt6 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      // for high Rayleigh numbers: compare to Dixit benchmark solution
      else if (Ra == 1e7) {
        fs <<   (T) fabs((LitVelocity7[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity7[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity7[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity7[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity7[2] - outputVelY[0] / outputVelX[0])  / LitVelocity7[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition7[0] - outputVelX[1] / lx) / LitPosition7[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition7[1] - outputVelY[1] / lx) / LitPosition7[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt7 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      else if (Ra == 1e8) {
        fs <<   (T) fabs((LitVelocity8[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity8[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity8[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity8[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity8[2] - outputVelY[0] / outputVelX[0])  / LitVelocity8[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition8[0] - outputVelX[1] / lx) / LitPosition8[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition8[1] - outputVelY[1] / lx) / LitPosition8[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt8 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      else if (Ra == 1e9) {
        fs <<   (T) fabs((LitVelocity9[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity9[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity9[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity9[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity9[2] - outputVelY[0] / outputVelX[0])  / LitVelocity9[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition9[0] - outputVelX[1] / lx) / LitPosition9[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition9[1] - outputVelY[1] / lx) / LitPosition9[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt9 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      else if (Ra == 10e10) {
        fs <<   (T) fabs((LitVelocity10[0] - outputVelX[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity10[0]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity10[1] - outputVelY[0] / converter.physAlpha() * converter.getCharL()) / LitVelocity10[1]) * 100.0  << " %" << "; " <<
           (T) fabs((LitVelocity10[2] - outputVelY[0] / outputVelX[0])  / LitVelocity10[2]) * 100.0  << " %" << "; " <<
           (T) fabs((LitPosition10[0] - outputVelX[1] / lx) / LitPosition10[0]) * 100 << "%" << "; " <<
           (T) fabs((LitPosition10[1] - outputVelY[1] / lx) / LitPosition10[1]) * 100 << "%" << "; " <<
           (T) fabs((LitNusselt10 - nusselt_2nd) / nusselt_2nd) * 100 << "%" << "; " << endl;
      }
      fs.close();
    }
  }
}

int main(int argc, char *argv[])
{

  /// === 1st Step: Initialization ===
  OstreamManager clout(std::cout,"main");
  olbInit(&argc, &argv);
  singleton::directories().setOutputDir("./tmp/");

  T tau = 0.51;

  if (argc>=2) {
    tau = atof(argv[1]);
  }
  if (argc>=3) {
    N = atof(argv[2]);
  }
  if (argc==4) {
    Ra = atof(argv[3]);
  }

  lx  = pow(Ra * 15.126e-6 * 15.126e-6 / Pr / 9.81 / (Thot - Tcold) / 0.00341, (T) 1/3);  // length of the square
  T charL = lx;
  T charU = 1.0 / lx /( Pr * 25.684e-3 / 15.126e-6 / 1.0 * 1.0 / 25.684e-3);

  if (Ra==1e3) {
    charL = LitPosition3[1];
    charU *= LitVelocity3[1];
  }
  if (Ra==1e4) {
    charL = LitPosition4[1];
    charU *= LitVelocity4[1];
  }
  if (Ra==1e5) {
    charL = LitPosition5[1];
    charU *= LitVelocity5[1];
  }
  if (Ra==1e6) {
    charL = LitPosition6[1];
    charU *= LitVelocity6[1];
  }
  if (Ra==1e7) {
    charL = LitPosition7[1];
    charU *= LitVelocity7[1];
  }
  if (Ra==1e8) {
    charL = LitPosition8[1];
    charU *= LitVelocity8[1];
  }
  if (Ra==1e9) {
    charL = LitPosition9[1];
    charU *= LitVelocity9[1];
  }
  if (Ra==1e10) {
    charL = LitPosition10[1];
    charU *= LitVelocity10[1];
  }

  T charLatticeVelocity = 0.1 / sqrt(NSDESCRIPTOR<T>::invCs2);
  // T charLatticeVelocity = 0.1;

  // N = lx / (15.126e-6 * charLatticeVelocity / charU * NSDESCRIPTOR<T>::invCs2 / (tau - 0.5));  // (physViscosity * charLatticeVelocity / charPhysVelocity * Lattice<T>::invCs2 / (latticeRelaxationTime - 0.5)),

  T yPlus = tau;
  T uFric = sqrt(0.026 / 2. * pow(15.126e-6 / charU / lx / LitPosition8[1], 1./7.) ) * charU;
  clout << uFric << endl;
  T spacing = yPlus * 15.126e-6 / uFric;
  T spacingKol = pow( pow( 15.126e-6 / charU, 3) * lx * LitPosition8[1], 0.25 );
  clout << "y+ = " << yPlus << "; spacing = " << spacing  << "; Kolmogorov spacing = " << spacingKol << endl;

  N = lx / spacing;

  TLBconverter<T> converter(   //Luft bei 1 bar    //Gesetzt wie bei Hortmann
    (T) 2,  // dim
    (T) spacing, // latticeL
    (T) charLatticeVelocity,  // latticeU
    (T) 15.126e-6,  // charNu
    (T) 25.684e-3,      // charLambda
    (T) Pr * 25.684e-3 / 15.126e-6 / 1.0,    // charCp = Pr*charLambda/charNu/charRho
    (T) Ra * 15.126e-6 * 15.126e-6 / Pr / 9.81 / (Thot - Tcold) / pow(lx, 3), // charBeta = Ra*charNu²/Pr/g/deltaT/charL^3
    (T) Thot - Tcold, // charDeltaT
    (T) lx, // charL
    (T) charU, // charU = charNu / charL * sqrt( Ra / Pr )
    (T) 1.0, // charRho
    (T) Tcold //temperaturelevel
  );
  converter.print();

  /// === 2nd Step: Prepare Geometry ===
  std::vector<T> extend(2,T());
  extend[0] = lx + 2*converter.getLatticeL();
  extend[1] = lx + 2*converter.getLatticeL();
  std::vector<T> origin(2,T());
  IndicatorCuboid2D<T> cuboid(extend, origin);

  /// Instantiation of an empty cuboidGeometry
  CuboidGeometry2D<T> cuboidGeometry(cuboid, converter.getLatticeL(), singleton::mpi().getSize());

  /// Instantiation of a loadBalancer
  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);

  /// Instantiation of a superGeometry
  SuperGeometry2D<T> superGeometry(cuboidGeometry, loadBalancer, 2);

  prepareGeometry(superGeometry, converter);

  /// === 3rd Step: Prepare Lattice ===

  SuperLattice2D<T, TDESCRIPTOR> ADlattice(superGeometry);
  SuperLattice2D<T, NSDESCRIPTOR> NSlattice(superGeometry);

  sOnLatticeBoundaryCondition2D<T, NSDESCRIPTOR> NSboundaryCondition(NSlattice);
  createLocalBoundaryCondition2D<T, NSDESCRIPTOR>(NSboundaryCondition);

  sOnLatticeBoundaryCondition2D<T, TDESCRIPTOR> TboundaryCondition(ADlattice);
  createAdvectionDiffusionBoundaryCondition2D<T, TDESCRIPTOR>(TboundaryCondition);

  SmagorinskyForcedBGKdynamics<T, NSDESCRIPTOR> NSbulkDynamics(
    converter.getOmega(),
    instances::getBulkMomenta<T,NSDESCRIPTOR>(),
    0.08);

  SmagorinskyAdvectionDiffusionBGKdynamics<T, TDESCRIPTOR> TbulkDynamics (
    converter.getOmegaT(),
    instances::getAdvectionDiffusionBulkMomenta<T,TDESCRIPTOR>(),
    0.08);

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  // This coupling must be necessarily be put on the Navier-Stokes lattice!!
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

  std::vector<T> dir(2,T());
  dir[1] = 1.;

  double forceconvfactor = converter.getCharDeltaT()*converter.getCharBeta();

  NavierStokesAdvectionDiffusionCouplingGenerator2D<T,NSDESCRIPTOR>
  coupling(0, converter.numCells(lx), 0, converter.numCells(lx),
           converter.getGravity(), converter.latticeTemperature(Tcold), 1., forceconvfactor, dir);

  NSlattice.addLatticeCoupling(superGeometry, 1, coupling, ADlattice);

  prepareLattice(converter,
                 NSlattice, ADlattice,
                 NSbulkDynamics, TbulkDynamics,
                 NSboundaryCondition, TboundaryCondition, superGeometry );


  /// === 4th Step: Main Loop with Timer ===
  Timer<T> timer(converter.numTimeSteps(maxPhysT), superGeometry.getStatistics().getNvoxel() );
  timer.start();

  util::ValueTracer<T> converge(5,epsilon);
  for (int iT = 0; iT < converter.numTimeSteps(maxPhysT); ++iT) {

    if (converge.hasConverged()) {
      clout << "Simulation converged." << endl;
      getResults(converter, NSlattice, ADlattice, iT, superGeometry, timer, converge.hasConverged());

      clout << "Time " << iT << "." << std::endl;

      break;
    }

    /// === 5th Step: Definition of Initial and Boundary Conditions ===
    setBoundaryValues(converter, NSlattice, ADlattice, iT, superGeometry);

    /// === 6th Step: Collide and Stream Execution ===
    ADlattice.collideAndStream();
    NSlattice.collideAndStream();

    NSlattice.executeCoupling();

    /// === 7th Step: Computation and Output of the Results ===
    getResults(converter, NSlattice, ADlattice, iT, superGeometry, timer, converge.hasConverged());
    if (iT % 1000 == 0) {
      timer.update(iT);
      timer.printStep();
      converge.takeValue(computeNusselt(superGeometry, NSlattice, ADlattice, converter, NSbulkDynamics, TbulkDynamics),true);
    }
  }

  timer.stop();
  timer.printSummary();
}
