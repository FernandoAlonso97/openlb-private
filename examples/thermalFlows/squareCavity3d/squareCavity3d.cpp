/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

// natural convection of air in a square cavity in 3D

#include "olb3D.h"
#include "olb3D.hh"   // use only generic version!

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace std;

typedef double T;

#define NSDESCRIPTOR ForcedD3Q19Descriptor
#define TDESCRIPTOR AdvectionDiffusionD3Q7Descriptor

// Parameters for the simulation setup
double Ra = 1e4;  // Rayleigh-Zahl
const double Pr = 0.71; // Prandtl-Zahl

T lx;

int N = 40; // resolution of the model

const T maxPhysT = 1e4;   // max. simulation time in s, SI unit
const T epsilon = 1.e-8;  // precision of the convergence (residuum)

const T Tcold = 275.15;
const T Thot = 285.15;
const T Tmean = (Tcold + Thot) / 2.0;

/// Stores geometry information in form of material numbers
void prepareGeometry(SuperGeometry3D<T>& superGeometry,
                     TLBconverter<T> &converter) {

  OstreamManager clout(std::cout,"prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0,4);

  std::vector<T> extend(3,T());
  extend[0] = lx;
  extend[1] = lx;
  extend[2] = 3.0 * converter.getLatticeL();
  std::vector<T> origin(3,T());
  origin[0] = converter.getLatticeL();
  origin[1] = 0.5*converter.getLatticeL();
  origin[2] = 0.0;
  IndicatorCuboid3D<T> cuboid2(extend, origin);

  superGeometry.rename(4,1,cuboid2);

  std::vector<T> extendwallleft(3,T(0));
  extendwallleft[0] = converter.getLatticeL();
  extendwallleft[1] = lx;
  extendwallleft[2] = 0.1;
  std::vector<T> originwallleft(3,T(0));
  originwallleft[0] = 0.0;
  originwallleft[1] = 0.0;
  originwallleft[2] = 0.0;
  IndicatorCuboid3D<T> wallleft(extendwallleft, originwallleft);

  std::vector<T> extendwallright(3,T(0));
  extendwallright[0] = converter.getLatticeL();
  extendwallright[1] = lx;
  extendwallright[2] = 0.1;
  std::vector<T> originwallright(3,T(0));
  originwallright[0] = lx+converter.getLatticeL();
  originwallright[1] = 0.0;
  originwallright[2] = 0.0;
  IndicatorCuboid3D<T> wallright(extendwallright, originwallright);

  superGeometry.rename(4,2,1,wallleft);
  superGeometry.rename(4,3,1,wallright);


  /// Removes all not needed boundary voxels outside the surface
  superGeometry.clean();
  /// Removes all not needed boundary voxels inside the surface
  superGeometry.innerClean();
  superGeometry.checkForErrors();

  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;
}

void prepareLattice( TLBconverter<T> &converter,
                     SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                     SuperLattice3D<T, TDESCRIPTOR>& ADlattice,
                     Dynamics<T, NSDESCRIPTOR> &bulkDynamics,
                     Dynamics<T, TDESCRIPTOR>& advectionDiffusionBulkDynamics,
                     sOnLatticeBoundaryCondition3D<T,NSDESCRIPTOR>& NSboundaryCondition,
                     sOnLatticeBoundaryCondition3D<T,TDESCRIPTOR>& TboundaryCondition,
                     SuperGeometry3D<T>& superGeometry )
{

  OstreamManager clout(std::cout,"prepareLattice");
  clout << "Prepare Lattice ..." << std::endl;

  double omega  = converter.getOmega();
  double Tomega  = converter.getOmegaT();

  /// define lattice Dynamics
  ADlattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, TDESCRIPTOR>());
  NSlattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, NSDESCRIPTOR>());

  ADlattice.defineDynamics(superGeometry, 1, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 2, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 3, &advectionDiffusionBulkDynamics);
  ADlattice.defineDynamics(superGeometry, 4, &instances::getBounceBack<T, TDESCRIPTOR>());

  NSlattice.defineDynamics(superGeometry, 1, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 2, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 3, &bulkDynamics);
  NSlattice.defineDynamics(superGeometry, 4, &instances::getBounceBack<T, NSDESCRIPTOR>());

  /// sets boundary
  TboundaryCondition.addTemperatureBoundary(superGeometry, 2, Tomega);
  TboundaryCondition.addTemperatureBoundary(superGeometry, 3, Tomega);
  NSboundaryCondition.addVelocityBoundary(superGeometry, 2, omega);
  NSboundaryCondition.addVelocityBoundary(superGeometry, 3, omega);

  /// define initial conditions
  AnalyticalConst3D<T,T> rho(1.);
  AnalyticalConst3D<T,T> u0(0.0, 0.0, 0.0);
  AnalyticalConst3D<T,T> T_cold(converter.latticeTemperature(Tcold));
  AnalyticalConst3D<T,T> T_hot(converter.latticeTemperature(Thot));
  AnalyticalConst3D<T,T> T_mean(converter.latticeTemperature(Tmean));

  /// for each material set Rho, U and the Equilibrium
  NSlattice.defineRhoU(superGeometry, 1, rho, u0);
  NSlattice.iniEquilibrium(superGeometry, 1, rho, u0);

  ADlattice.defineRho(superGeometry, 1, T_mean);
  ADlattice.iniEquilibrium(superGeometry, 1, T_mean, u0);
  ADlattice.defineRho(superGeometry, 2, T_hot);
  ADlattice.iniEquilibrium(superGeometry, 2, T_hot, u0);
  ADlattice.defineRho(superGeometry, 3, T_cold);
  ADlattice.iniEquilibrium(superGeometry, 3, T_cold, u0);

  /// Make the lattice ready for simulation
  NSlattice.initialize();
  ADlattice.initialize();

  clout << "Prepare Lattice ... OK" << std::endl;
}

void setBoundaryValues(TLBconverter<T> &converter,
                       SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                       SuperLattice3D<T, TDESCRIPTOR>& ADlattice,
                       int iT, SuperGeometry3D<T>& superGeometry)
{

  // nothing to do here

}

void getResults(TLBconverter<T> &converter,
                SuperLattice3D<T, NSDESCRIPTOR>& NSlattice,
                SuperLattice3D<T, TDESCRIPTOR>& ADlattice, int iT,
                SuperGeometry3D<T>& superGeometry,
                Timer<T>& timer,
                bool converged)
{

  OstreamManager clout(std::cout,"getResults");

  SuperVTMwriter3D<T> vtkWriter("thermalNaturalConvection3D");
  SuperLatticeGeometry3D<T, NSDESCRIPTOR> geometry(NSlattice, superGeometry);
  SuperLatticePhysVelocity3D<T, NSDESCRIPTOR> velocity(NSlattice, converter);
  SuperLatticePhysPressure3D<T, NSDESCRIPTOR> presure(NSlattice, converter);
  SuperLatticePhysTemperature3D<T, TDESCRIPTOR> temperature(ADlattice, converter);
  vtkWriter.addFunctor( geometry );
  vtkWriter.addFunctor( presure );
  vtkWriter.addFunctor( velocity );
  vtkWriter.addFunctor( temperature );

  const int vtkIter = converter.numTimeSteps(5.0);

  if (iT == 0) {
    /// Writes the converter log file
    writeLogFile(converter,"squareCavity3d");

    /// Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeCuboid3D<T, NSDESCRIPTOR> cuboid(NSlattice);
    SuperLatticeRank3D<T, NSDESCRIPTOR> rank(NSlattice);
    vtkWriter.write(cuboid);
    vtkWriter.write(rank);

    vtkWriter.createMasterFile();
  }

  /// Writes the VTK files
  if (iT%vtkIter == 0 || converged) {

    timer.update(iT);
    timer.printStep();

    /// NSLattice statistics console output
    NSlattice.getStatistics().print(iT,converter.physTime(iT));
    /// ADLattice statistics console output
    ADlattice.getStatistics().print(iT,converter.physTime(iT));

    vtkWriter.write(iT);

    BlockLatticeReduction3D<T, TDESCRIPTOR> planeReduction(temperature, 0, 0, -1);
    BlockGifWriter<T> gifWriter;
    gifWriter.write(planeReduction, Tcold*0.98, Thot*1.02, iT, "temperature");

    SuperEuklidNorm3D<T, NSDESCRIPTOR> normVel( velocity );
    BlockLatticeReduction3D<T, NSDESCRIPTOR> planeReduction2(normVel , 0, 0, -1 );
    BlockGifWriter<T> gifWriter2;
    gifWriter2.write( planeReduction2, iT, "velocity" );

      ///Write Nusselt Number
      int voxel = 0, material = 0;
      double T_x = 0, T_x_phys = 0;
      double T_xplus1 = 0, T_xplus1_phys = 0;
      double T_xplus2 = 0, T_xplus2_phys = 0;
      double x_phys = 0;
      double centraldiff_phys = 0;
      double Q_quer = 0, Q_quer_ber = 0;
      double q_2ndOrdner = 0;
      double q_uT = 0;
      double u_xplus1[2];

      for (int iC = 0; iC < NSlattice.getLoadBalancer().size(); iC++) {
        int ny = NSlattice.getBlockLattice(iC).getNy();

        int iX = 0;
        int iZ = 1;

        for (int iY = 0; iY < ny; ++iY) {

          material = superGeometry.getBlockGeometry(iC).getMaterial(iX,iY,iZ);

          T_x = ADlattice.getBlockLattice(iC).get(iX,iY,iZ).computeRho();
          T_x_phys = converter.physTemperature(T_x);
          T_xplus1 = ADlattice.getBlockLattice(iC).get(iX+1,iY,iZ).computeRho();
          NSlattice.getBlockLattice(iC).get(iX+1,iY,iZ).computeU(u_xplus1);
          T_xplus1_phys = converter.physTemperature(T_xplus1);
          T_xplus2 = ADlattice.getBlockLattice(iC).get(iX+2,iY,iZ).computeRho();
          x_phys = converter.physLength();
          centraldiff_phys = (T_x_phys - T_xplus1_phys)/x_phys;

          Q_quer = centraldiff_phys*converter.getCharL()/10.0;
          if ( material == 2 ) {
            Q_quer_ber = Q_quer_ber + Q_quer;
            q_2ndOrdner += (3.0*T_x - 4.0*T_xplus1 + 1.0*T_xplus2)/2.0/converter.getDeltaX();
            q_uT += u_xplus1[0]*T_xplus1;
            voxel++;
          }
        }
      }

      #ifdef PARALLEL_MODE_MPI
        singleton::mpi().reduceAndBcast(Q_quer_ber, MPI_SUM);
        singleton::mpi().reduceAndBcast(q_2ndOrdner, MPI_SUM);
        singleton::mpi().reduceAndBcast(q_uT, MPI_SUM);
        singleton::mpi().reduceAndBcast(voxel, MPI_SUM);
      #endif

      T nusselt = Q_quer_ber/(T)voxel;
      T nusselt_2nd = q_2ndOrdner/(T)voxel;
      T nusselt_by_uT = 1.0 + q_uT/(T)voxel / converter.physAlpha() / converter.getCharL();

      clout << "Nu_0: " << nusselt << "; Nu_0_uT: " << nusselt_by_uT << "; Nu_0 (2nd Order): " << nusselt_2nd;

      // compare to De Val Davis' benchmark solutions
      if (Ra == 1e3) {
        clout << "; Error (2nd): " << (T) (1.117 - nusselt_2nd)/1.117 * 100.0 << " %" << endl;
      } else if (Ra == 1e4) {
        clout << "; Error (2nd): " << (T) (2.238 - nusselt_2nd)/2.238 * 100.0 << " %" << endl;
      } else if (Ra == 1e5) {
        clout << "; Error (2nd): " << (T) (4.509 - nusselt_2nd)/4.509 * 100.0 << " %" << endl;
      } else if (Ra == 1e6) {
        clout << "; Error (2nd): " << (T) (8.817 - nusselt_2nd)/8.817 * 100.0 << " %" << endl;
      } else {
        clout << endl;
      }

  }

}


int main(int argc, char *argv[])
{

  /// === 1st Step: Initialization ===
  OstreamManager clout(std::cout,"main");
  olbInit(&argc, &argv);
  singleton::directories().setOutputDir("./tmp/");

  T tau = 0.51;

  if(argc>=2) tau = atof(argv[1]);
  if(argc>=3) N = atof(argv[2]);
  if(argc==4) Ra = atof(argv[3]);

  lx  = pow(Ra * 15.126e-6 * 15.126e-6 / Pr / 9.81 / (Thot - Tcold) / 0.00341, (T) 1/3);  // length of the square
  //lx = 1.0;

  TLBconverter<T> converter(   //Luft bei 1 bar    //Gesetzt wie bei Hortmann
    (T) 3,  // dim
    (T) lx/N, // latticeL
    (T) sqrt(Ra / Pr) * (tau - 0.5) / 3 / N,  // latticeU
    (T) 15.126e-6,  // charNu
    (T) 25.684e-3,      // charLambda
    (T) Pr * 25.684e-3 / 15.126e-6 / 1.0,    // charCp = Pr*charLambda/charNu/charRho
    (T) Ra * 15.126e-6 * 15.126e-6 / Pr / 9.81 / (Thot - Tcold) / pow(lx, 3), // charBeta = Ra*charNu²/Pr/g/deltaT/charL^3
    (T) Thot - Tcold, // charDeltaT
    (T) lx, // charL
    (T) 15.126e-6 / lx * sqrt( Ra / Pr ), // charU = charNu / charL * sqrt( Ra / Pr )
    (T) 1.0, // charRho
    (T) Tcold //temperaturelevel
  );
  converter.print();

  /// === 2nd Step: Prepare Geometry ===
  std::vector<T> extend(3,T());
  extend[0] = lx + 2*converter.getLatticeL();
  extend[1] = lx + converter.getLatticeL();
  extend[2] = 3.*converter.getLatticeL();
  std::vector<T> origin(3,T());
  IndicatorCuboid3D<T> cuboid(extend, origin);

  /// Instantiation of an empty cuboidGeometry
  CuboidGeometry3D<T> cuboidGeometry(cuboid, converter.getLatticeL(), singleton::mpi().getSize());
  cuboidGeometry.setPeriodicity(false, false, true);  // x, y, z

  /// Instantiation of a loadBalancer
  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);

  /// Instantiation of a superGeometry
  SuperGeometry3D<T> superGeometry(cuboidGeometry, loadBalancer, 2);

  prepareGeometry(superGeometry, converter);

  /// === 3rd Step: Prepare Lattice ===

  SuperLattice3D<T, TDESCRIPTOR> ADlattice(superGeometry);
  SuperLattice3D<T, NSDESCRIPTOR> NSlattice(superGeometry);

  sOnLatticeBoundaryCondition3D<T, NSDESCRIPTOR> NSboundaryCondition(NSlattice);
  createLocalBoundaryCondition3D<T, NSDESCRIPTOR>(NSboundaryCondition);

  sOnLatticeBoundaryCondition3D<T, TDESCRIPTOR> TboundaryCondition(ADlattice);
  createAdvectionDiffusionBoundaryCondition3D<T, TDESCRIPTOR>(TboundaryCondition);

  ForcedBGKdynamics<T, NSDESCRIPTOR> NSbulkDynamics(
    converter.getOmega(),
    instances::getBulkMomenta<T,NSDESCRIPTOR>());

  AdvectionDiffusionBGKdynamics<T, TDESCRIPTOR> TbulkDynamics (
    converter.getOmegaT(),
    instances::getAdvectionDiffusionBulkMomenta<T,TDESCRIPTOR>());

  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  // This coupling must be necessarily be put on the Navier-Stokes lattice!!
  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

  std::vector<T> dir(3,T());
  dir[1] = 1.;

  double forceconvfactor = converter.getCharDeltaT()*converter.getCharBeta();

  NavierStokesAdvectionDiffusionCouplingGenerator3D<T,NSDESCRIPTOR>
  coupling(0, converter.numCells(lx), 0, converter.numCells(lx), 0, 3,
           converter.getGravity(), converter.latticeTemperature(Tcold), 1., forceconvfactor, dir);

  NSlattice.addLatticeCoupling(superGeometry, 1, coupling, ADlattice);

  prepareLattice(converter,
                 NSlattice, ADlattice,
                 NSbulkDynamics, TbulkDynamics,
                 NSboundaryCondition, TboundaryCondition, superGeometry );


  /// === 4th Step: Main Loop with Timer ===
  Timer<T> timer(converter.numTimeSteps(maxPhysT), superGeometry.getStatistics().getNvoxel() );
  timer.start();

  util::ValueTracer<T> converge(converter.numTimeSteps(5.0),epsilon);
  for (int iT = 0; iT < converter.numTimeSteps(maxPhysT); ++iT) {

    if (converge.hasConverged()) {
      clout << "Simulation converged." << endl;
      getResults(converter, NSlattice, ADlattice, iT, superGeometry, timer, converge.hasConverged());

      clout << "Time " << iT << "." << std::endl;

      break;
    }

    /// === 5th Step: Definition of Initial and Boundary Conditions ===
    setBoundaryValues(converter, NSlattice, ADlattice, iT, superGeometry);

    /// === 6th Step: Collide and Stream Execution ===
    ADlattice.collideAndStream();
    NSlattice.collideAndStream();

    NSlattice.executeCoupling();

    /// === 7th Step: Computation and Output of the Results ===
    getResults(converter, NSlattice, ADlattice, iT, superGeometry, timer, converge.hasConverged());
    converge.takeValue(NSlattice.getStatistics().getAverageEnergy(),true);
  }

  timer.stop();
  timer.printSummary();
}
