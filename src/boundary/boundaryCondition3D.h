/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file A helper for initialising 3D boundaries -- header file.  */

#ifndef BOUNDARY_CONDITION_3D_H
#define BOUNDARY_CONDITION_3D_H

#include "core/blockLatticeStructure3D.hh"
#include "momentaOnBoundaries3D.hh"
#include "boundaryPostProcessors3D.hh"
#include "dynamics/dynamics.hh"
#include "dynamics/smagorinskyBGKdynamics.hh"
#include "geometry/blockGeometryStatistics3D.hh"

namespace olb {

template<typename T, template<typename U> class Lattice>
class OnLatticeBoundaryCondition3D {
public:
  virtual ~OnLatticeBoundaryCondition3D() { }

  virtual void addVelocityBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addVelocityBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addVelocityBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addVelocityBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addVelocityBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addVelocityBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addPressureBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPressureBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPressureBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPressureBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPressureBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPressureBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addConvectionBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;
  virtual void addConvectionBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;
  virtual void addConvectionBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;
  virtual void addConvectionBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;
  virtual void addConvectionBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;
  virtual void addConvectionBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;

  virtual void addImpedanceBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addImpedanceFixedRefBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceFixedRefBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceFixedRefBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceFixedRefBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceFixedRefBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceFixedRefBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addImpedanceBoundaryIncompressible0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundaryIncompressible0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundaryIncompressible1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundaryIncompressible1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundaryIncompressible2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addImpedanceBoundaryIncompressible2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addExternalVelocityEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalVelocityEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addExternalImpedanceEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalImpedanceEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addInternalVelocityEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addInternalVelocityEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addExternalVelocityCornerNNN(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerNNP(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerNPN(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerNPP(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerPNN(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerPNP(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerPPN(int x, int y, int z, T omega) =0;
  virtual void addExternalVelocityCornerPPP(int x, int y, int z, T omega) =0;

  virtual void addExternalImpedanceCornerNNN(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerNNP(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerNPN(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerNPP(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerPNN(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerPNP(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerPPN(int x, int y, int z, T omega) =0;
  virtual void addExternalImpedanceCornerPPP(int x, int y, int z, T omega) =0;

  virtual void addInternalVelocityCornerNNN(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerNNP(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerNPN(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerNPP(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerPNN(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerPNP(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerPPN(int x, int y, int z, T omega) =0;
  virtual void addInternalVelocityCornerPPP(int x, int y, int z, T omega) =0;

  virtual void addPeriodicBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPeriodicBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPeriodicBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPeriodicBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPeriodicBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPeriodicBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addExternalPeriodicEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addExternalPeriodicEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;

  virtual void addExternalPeriodicCornerNNN(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerNNP(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerNPN(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerNPP(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerPNN(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerPNP(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerPPN(int x, int y, int z, T omega) =0;
  virtual void addExternalPeriodicCornerPPP(int x, int y, int z, T omega) =0;


  virtual BlockLatticeStructure3D<T,Lattice>& getBlock() =0;
  virtual BlockLatticeStructure3D<T,Lattice> const& getBlock() const =0;

  /// adds a pressure or velocity boundary for one material and a range (x0-x1, y0-y1, z0-z1) or the whole geometry
  virtual void addVelocityBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addVelocityBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) =0;

  virtual void addSlipBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1) =0;
  virtual void addSlipBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material) =0;

  virtual void addPressureBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega) =0;
  virtual void addPressureBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) =0;

  virtual void addConvectionBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) =0;
  virtual void addConvectionBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega, T* uAv=NULL) =0;

  virtual void addImpedanceBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega) = 0;
  virtual void addImpedanceBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) = 0;

  virtual void addImpedanceBoundaryIncompressible(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega) = 0;
  virtual void addImpedanceBoundaryIncompressible(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) = 0;

  virtual void addPeriodicBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega) = 0;
  virtual void addPeriodicBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) = 0;

  virtual void outputOn() =0;
  virtual void outputOff() =0;

};


////////// Factory functions //////////////////////////////////////////////////

template<typename T, template<typename U> class Lattice, typename MixinDynamics>
OnLatticeBoundaryCondition3D<T,Lattice>*
createLocalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block);

template<typename T, template<typename U> class Lattice, template<typename, template<typename> class, class, class> class MixinDynamics>
OnLatticeBoundaryCondition3D<T,Lattice>*
createInterpBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block);

template<typename T, template<typename U> class Lattice, template<typename, template<typename> class>class Momenta>
OnLatticeBoundaryCondition3D<T,Lattice>*
createLocalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block)
{
  return createLocalBoundaryCondition3D<T,Lattice,RLBdynamics<T,Lattice, Momenta<T,Lattice>> >(block);
}

template<typename T, template<typename U> class Lattice, template<typename, template<typename> class>class Momenta>
OnLatticeBoundaryCondition3D<T,Lattice>*
createInterpBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block)
{
  return createInterpBoundaryCondition3D<T,Lattice,BGKdynamics<T,Lattice, Momenta<T,Lattice>> >(block);
}

}

#endif
