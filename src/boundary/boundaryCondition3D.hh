/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A helper for initialising 3D boundaries -- generic implementation.
 */
#ifndef BOUNDARY_CONDITION_3D_HH
#define BOUNDARY_CONDITION_3D_HH

#include "boundaryCondition3D.h"
#include "boundaryInstantiator3D.h"

namespace olb {

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
class RegularizedBoundaryManager3D {
public:
  template<int direction, int orientation> static RegularizedVelocityBM<T,Lattice, direction,orientation,0>*
  getVelocityBoundaryMomenta();
  template<int direction, int orientation,class Momenta,class PostProcessor> static Dynamics<T,Lattice>*
  getVelocityBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static NoPostProcessorGenerator<T,Lattice>*
  getVelocityBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int direction, int orientation> static Momenta<T,Lattice>*
  getPressureBoundaryMomenta();
  template<int direction, int orientation, class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getPressureBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static NoPostProcessorGenerator<T,Lattice>*
  getPressureBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int direction, int orientation> static NoPostProcessorGenerator<T,Lattice>*
  getConvectionBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1, T* uAv=nullptr);

  template<int plane, int normal1, int normal2> static FixedVelocityBM<T,Lattice,0>*
  getExternalVelocityEdgeMomenta();
  template<int plane, int normal1, int normal2,class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getExternalVelocityEdgeDynamics(T omega, Momenta& momenta);
  template<int plane, int normal1, int normal2> static OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>*
  getExternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int plane, int normal1, int normal2> static InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,0>*
  getInternalVelocityEdgeMomenta();
  template<int plane, int normal1, int normal2,class Momenta,class PostProcessor> static Dynamics<T,Lattice>*
  getInternalVelocityEdgeDynamics(T omega, Momenta& momenta);
  template<int plane, int normal1, int normal2> static NoPostProcessorGenerator<T,Lattice>*
  getInternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int xNormal, int yNormal, int zNormal> static FixedVelocityBM<T,Lattice,0>*
  getExternalVelocityCornerMomenta();
  template<int xNormal, int yNormal, int zNormal,class Momenta,class PostProcessor> static Dynamics<T,Lattice>*
  getExternalVelocityCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal, int zNormal> static OuterVelocityCornerProcessorGenerator3D<T,Lattice,xNormal,yNormal,zNormal>*
  getExternalVelocityCornerProcessor(int x, int y, int z);

  template<int xNormal, int yNormal, int zNormal> static InnerCornerVelBM3D<T,Lattice,xNormal, yNormal,zNormal,0>*
  getInternalVelocityCornerMomenta();
  template<int xNormal, int yNormal, int zNormal,class Momenta,class PostProcessor> static Dynamics<T,Lattice>*
  getInternalVelocityCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal, int zNormal> static NoPostProcessorGenerator<T,Lattice>*
  getInternalVelocityCornerProcessor(int x, int y, int z);

  template<int direction, int orientation>
  struct ImpedancePlaneBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int direction, int orientation>
  struct ImpedanceFixedRefPlaneBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int direction, int orientation>
  struct ImpedancePlaneBoundaryIncompressible{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3DIncompressible<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int plane, int normal1, int normal2>
  struct ImpedanceEdgeBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,plane,normal1,normal2>> DynamicsType;
  };

  template<int xNormal, int yNormal, int zNormal>
  struct ImpedanceCornerBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,xNormal,yNormal,zNormal>> DynamicsType;
  };

  template<int direction, int orientation>
  struct PeriodicPlaneBoundary{
      typedef PostProcessingDynamics<T,Lattice,PeriodicBoundaryProcessor3D<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int plane, int normal1, int normal2>
  struct PeriodicEdgeBoundary{
      typedef PostProcessingDynamics<T,Lattice,PeriodicBoundaryEdgeProcessor3D<T,Lattice,plane,normal1,normal2>> DynamicsType;
  };

  template<int xNormal, int yNormal, int zNormal>
  struct PeriodicCornerBoundary{
      typedef PostProcessingDynamics<T,Lattice,PeriodicBoundaryCornerProcessor3D<T,Lattice,xNormal,yNormal,zNormal>> DynamicsType;
  };
};


template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
class InterpolationBoundaryManager3D {
public:
  template<int direction, int orientation> static BasicDirichletBM<T,Lattice,VelocityBM, direction,orientation,0>*
  getVelocityBoundaryMomenta();
  template<int direction, int orientation, class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getVelocityBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static PlaneFdBoundaryProcessorGenerator3D<T,Lattice, direction,orientation>*
  getVelocityBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int direction, int orientation> static BasicDirichletBM<T,Lattice,PressureBM, direction,orientation,0>*
  getPressureBoundaryMomenta();
  template<int direction, int orientation,class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getPressureBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static PlaneFdBoundaryProcessorGenerator3D<T,Lattice, direction,orientation>*
  getPressureBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int direction, int orientation> static StraightConvectionBoundaryProcessorGenerator3D<T,Lattice,direction, orientation>*
  getConvectionBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1, T* uAv=nullptr);

  template<int plane, int normal1, int normal2> static FixedVelocityBM<T,Lattice,0>*
  getExternalVelocityEdgeMomenta();
  template<int plane, int normal1, int normal2, class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getExternalVelocityEdgeDynamics(T omega, Momenta& momenta);
  template<int plane, int normal1, int normal2> static OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>*
  getExternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int plane, int normal1, int normal2> static InnerEdgeVelBM3D<T,Lattice, plane,normal1,normal2,0>*
  getInternalVelocityEdgeMomenta();
  template<int plane, int normal1, int normal2, class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getInternalVelocityEdgeDynamics(T omega, Momenta& momenta);
  template<int plane, int normal1, int normal2> static NoPostProcessorGenerator<T,Lattice>*
  getInternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1);

  template<int xNormal, int yNormal, int zNormal> static FixedVelocityBM<T,Lattice,0>*
  getExternalVelocityCornerMomenta();
  template<int xNormal, int yNormal, int zNormal, class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getExternalVelocityCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal, int zNormal> static OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal>*
  getExternalVelocityCornerProcessor(int x, int y, int z);

  template<int xNormal, int yNormal, int zNormal> static InnerCornerVelBM3D<T,Lattice, xNormal,yNormal,zNormal,0>*
  getInternalVelocityCornerMomenta();
  template<int xNormal, int yNormal, int zNormal, class Momenta, class PostProcessor> static Dynamics<T,Lattice>*
  getInternalVelocityCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal, int zNormal> static NoPostProcessorGenerator<T,Lattice>*
  getInternalVelocityCornerProcessor(int x, int y, int z);

  template<int direction, int orientation>
  struct ImpedancePlaneBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int direction, int orientation>
  struct ImpedanceFixedRefPlaneBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int direction, int orientation>
  struct ImpedancePlaneBoundaryIncompressible{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryProcessor3DIncompressible<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int plane, int normal1, int normal2>
  struct ImpedanceEdgeBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,plane,normal1,normal2>> DynamicsType;
  };

  template<int xNormal, int yNormal, int zNormal>
  struct ImpedanceCornerBoundary{
      typedef PostProcessingDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,xNormal,yNormal,zNormal>> DynamicsType;
  };

  template<int direction, int orientation>
  struct PeriodicPlaneBoundary{
      typedef PostProcessingDynamics<T,Lattice,PeriodicBoundaryProcessor3D<T,Lattice,direction,orientation>> DynamicsType;
  };

  template<int plane, int normal1, int normal2>
  struct PeriodicEdgeBoundary{
      typedef PostProcessingDynamics<T,Lattice,PeriodicBoundaryEdgeProcessor3D<T,Lattice,plane,normal1,normal2>> DynamicsType;
  };

  template<int xNormal, int yNormal, int zNormal>
  struct PeriodicCornerBoundary{
      typedef PostProcessingDynamics<T,Lattice,PeriodicBoundaryCornerProcessor3D<T,Lattice,xNormal,yNormal,zNormal>> DynamicsType;
  };
};


////////// RegularizedBoundaryManager3D /////////////////////////////////////////

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
RegularizedVelocityBM<T,Lattice, direction,orientation,0>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::getVelocityBoundaryMomenta()
{
  static RegularizedVelocityBM<T,Lattice, direction,orientation,0> momenta;
  return &momenta;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation, class Momenta,class PostProcessor>
Dynamics<T,Lattice>* RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getVelocityBoundaryDynamics(T omega, Momenta& momenta)
{
  static CombinedRLBdynamics<T,Lattice, MixinDynamics<T,Lattice,Momenta,PostProcessor>, Momenta> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
NoPostProcessorGenerator<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getVelocityBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return nullptr;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
Momenta<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::getPressureBoundaryMomenta()
{
  return new RegularizedPressureBM<T,Lattice, direction,orientation,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation, class Momenta, class PostProcessor>
Dynamics<T,Lattice>* RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getPressureBoundaryDynamics(T omega, Momenta& momenta)
{
  static CombinedRLBdynamics<T,Lattice, MixinDynamics<T,Lattice,Momenta,PostProcessor>, Momenta> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
NoPostProcessorGenerator<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getPressureBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return nullptr;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
NoPostProcessorGenerator<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getConvectionBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1, T* uAv)
{
  return nullptr;
}


template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
FixedVelocityBM<T,Lattice,0>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::getExternalVelocityEdgeMomenta()
{
  return new FixedVelocityBM<T,Lattice,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2, class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityEdgeDynamics(T omega, Momenta& momenta)
{
  static MixinDynamics<T,Lattice,Momenta,PostProcessor> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return new OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>(x0,x1, y0,y1, z0,z1);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
InnerEdgeVelBM3D<T,Lattice, plane,normal1,normal2,0>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::getInternalVelocityEdgeMomenta()
{
  return new InnerEdgeVelBM3D<T,Lattice, plane,normal1,normal2,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2, class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityEdgeDynamics(T omega, Momenta& momenta)
{
  static CombinedRLBdynamics<T,Lattice, MixinDynamics<T,Lattice,Momenta,PostProcessor>,Momenta> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
NoPostProcessorGenerator<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return nullptr;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
FixedVelocityBM<T,Lattice,0>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::getExternalVelocityCornerMomenta()
{
  return new FixedVelocityBM<T,Lattice,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal, class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityCornerDynamics(T omega, Momenta& momenta)
{
  static MixinDynamics<T,Lattice,Momenta,PostProcessor> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityCornerProcessor(int x, int y, int z)
{
  return new OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal> (x,y,z);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
InnerCornerVelBM3D<T,Lattice, xNormal,yNormal,zNormal,0>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::getInternalVelocityCornerMomenta()
{
  return new InnerCornerVelBM3D<T,Lattice, xNormal,yNormal,zNormal,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal, class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityCornerDynamics(T omega, Momenta& momenta)
{
  static CombinedRLBdynamics<T,Lattice, MixinDynamics<T,Lattice,Momenta,PostProcessor>,Momenta> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
NoPostProcessorGenerator<T,Lattice>*
RegularizedBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityCornerProcessor(int x, int y, int z)
{
  return nullptr;
}


////////// InterpolationBoundaryManager3D /////////////////////////////////////////

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
BasicDirichletBM<T,Lattice,VelocityBM, direction,orientation,0>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::getVelocityBoundaryMomenta()
{
  static BasicDirichletBM<T,Lattice,VelocityBM, direction,orientation,0> momenta;
  return &momenta;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation, class Momenta, class PostProcessor>
Dynamics<T,Lattice>* InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getVelocityBoundaryDynamics(T omega, Momenta& momenta)
{
  static MixinDynamics<T,Lattice,Momenta,PostProcessor> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
PlaneFdBoundaryProcessorGenerator3D<T,Lattice, direction,orientation>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getVelocityBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return new PlaneFdBoundaryProcessorGenerator3D
         <T,Lattice, direction,orientation>(x0,x1, y0,y1, z0,z1);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
BasicDirichletBM<T,Lattice,PressureBM, direction,orientation,0>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::getPressureBoundaryMomenta()
{
  return new BasicDirichletBM<T,Lattice,PressureBM, direction,orientation,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation,class Momenta, class PostProcessor>
Dynamics<T,Lattice>* InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getPressureBoundaryDynamics(T omega, Momenta& momenta)
{
  static MixinDynamics<T,Lattice,Momenta,PostProcessor> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
PlaneFdBoundaryProcessorGenerator3D<T,Lattice, direction,orientation>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getPressureBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return new PlaneFdBoundaryProcessorGenerator3D
         <T,Lattice, direction,orientation>(x0, x1, y0, y1, z0, z1);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int direction, int orientation>
StraightConvectionBoundaryProcessorGenerator3D<T,Lattice,direction, orientation>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getConvectionBoundaryProcessor(int x0, int x1, int y0, int y1, int z0, int z1, T* uAv)
{
  return new StraightConvectionBoundaryProcessorGenerator3D
         <T,Lattice,direction,orientation>(x0, x1, y0, y1, z0, z1, uAv);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
FixedVelocityBM<T,Lattice,0>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::getExternalVelocityEdgeMomenta()
{
  return new FixedVelocityBM<T,Lattice,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2,class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityEdgeDynamics(T omega, Momenta& momenta)
{
  static MixinDynamics<T,Lattice,Momenta,PostProcessor> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return new OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>(x0,x1, y0,y1, z0,z1);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
InnerEdgeVelBM3D<T,Lattice, plane,normal1,normal2,0>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::getInternalVelocityEdgeMomenta()
{
  return new InnerEdgeVelBM3D<T,Lattice, plane,normal1,normal2,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2, class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityEdgeDynamics(T omega, Momenta& momenta)
{
  static CombinedRLBdynamics<T,Lattice, MixinDynamics<T,Lattice,Momenta,PostProcessor>, Momenta> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int plane, int normal1, int normal2>
NoPostProcessorGenerator<T,Lattice>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityEdgeProcessor(int x0, int x1, int y0, int y1, int z0, int z1)
{
  return nullptr;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
FixedVelocityBM<T,Lattice,0>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::getExternalVelocityCornerMomenta()
{
  return new FixedVelocityBM<T,Lattice,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal,class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityCornerDynamics(T omega, Momenta& momenta)
{
  static MixinDynamics<T,Lattice,Momenta,PostProcessor> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getExternalVelocityCornerProcessor(int x, int y, int z)
{
  return new OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal> (x,y,z);
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
InnerCornerVelBM3D<T,Lattice, xNormal,yNormal,zNormal,0>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::getInternalVelocityCornerMomenta()
{
  return new InnerCornerVelBM3D<T,Lattice, xNormal,yNormal,zNormal,0>;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal,class Momenta, class PostProcessor>
Dynamics<T,Lattice>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityCornerDynamics(T omega, Momenta& momenta)
{
  static CombinedRLBdynamics<T,Lattice, MixinDynamics<T,Lattice,Momenta,PostProcessor>, Momenta> dynamics(omega, momenta);
  return &dynamics;
}

template<typename T, template<typename U> class Lattice,  template<typename, template<typename> class, class, class> class MixinDynamics>
template<int xNormal, int yNormal, int zNormal>
NoPostProcessorGenerator<T,Lattice>*
InterpolationBoundaryManager3D<T,Lattice,MixinDynamics>::
getInternalVelocityCornerProcessor(int x, int y, int z)
{
  return nullptr;
}


////////// Factory functions //////////////////////////////////////////////////

template<typename T, template<typename U> class Lattice, template<typename, template<typename> class, class, class> class MixinDynamics>
OnLatticeBoundaryCondition3D<T,Lattice>* createLocalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block)
{
  return new BoundaryConditionInstantiator3D <
         T, Lattice,
         RegularizedBoundaryManager3D<T,Lattice, MixinDynamics> > (block);
}

template<typename T, template<typename U> class Lattice, template<typename, template<typename> class, class, class> class MixinDynamics>
OnLatticeBoundaryCondition3D<T,Lattice>* createInterpBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block)
{
  return new BoundaryConditionInstantiator3D <
         T, Lattice,
         InterpolationBoundaryManager3D<T,Lattice, MixinDynamics> > (block);
}

}  // namespace olb

#endif
