/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file A helper for initialising 3D boundaries -- header file.  */
#ifndef BOUNDARY_INSTANTIATOR_3D_H
#define BOUNDARY_INSTANTIATOR_3D_H

#include "boundaryCondition3D.h"
#include "geometry/blockGeometry3D.h"
#include "geometry/blockGeometryStatistics3D.h"
#include "io/ostreamManager.h"

namespace olb {

template<typename T> class BlockGeometryStatistics3D;

template<typename T, template<typename U> class Lattice, class BoundaryManager>
class BoundaryConditionInstantiator3D : public OnLatticeBoundaryCondition3D<T,Lattice> {
public:
  BoundaryConditionInstantiator3D( BlockLatticeStructure3D<T,Lattice>& block_ );
  ~BoundaryConditionInstantiator3D() override;

  void addVelocityBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addVelocityBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addVelocityBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addVelocityBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addVelocityBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addVelocityBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addSlipBoundary(int x0, int x1, int y0, int y1, int z0, int z1, int discreteNormalX, int discreteNormalY, int discreteNormalZ);

  void addPressureBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPressureBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPressureBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPressureBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPressureBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPressureBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addConvectionBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) override;
  void addConvectionBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) override;
  void addConvectionBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) override;
  void addConvectionBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) override;
  void addConvectionBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) override;
  void addConvectionBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL) override;

  void addImpedanceBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addImpedanceFixedRefBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceFixedRefBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceFixedRefBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceFixedRefBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceFixedRefBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceFixedRefBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addImpedanceBoundaryIncompressible0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundaryIncompressible0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundaryIncompressible1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundaryIncompressible1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundaryIncompressible2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addImpedanceBoundaryIncompressible2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addExternalVelocityEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalVelocityEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addInternalVelocityEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addInternalVelocityEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addExternalImpedanceEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalImpedanceEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addExternalVelocityCornerNNN(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerNNP(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerNPN(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerNPP(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerPNN(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerPNP(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerPPN(int x, int y, int z, T omega) override;
  void addExternalVelocityCornerPPP(int x, int y, int z, T omega) override;

  void addExternalImpedanceCornerNNN(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerNNP(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerNPN(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerNPP(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerPNN(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerPNP(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerPPN(int x, int y, int z, T omega) override;
  void addExternalImpedanceCornerPPP(int x, int y, int z, T omega) override;

  void addInternalVelocityCornerNNN(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerNNP(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerNPN(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerNPP(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerPNN(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerPNP(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerPPN(int x, int y, int z, T omega) override;
  void addInternalVelocityCornerPPP(int x, int y, int z, T omega) override;

  void addPeriodicBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPeriodicBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPeriodicBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPeriodicBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPeriodicBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addPeriodicBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addExternalPeriodicEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;
  void addExternalPeriodicEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega) override;

  void addExternalPeriodicCornerNNN(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerNNP(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerNPN(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerNPP(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerPNN(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerPNP(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerPPN(int x, int y, int z, T omega) override;
  void addExternalPeriodicCornerPPP(int x, int y, int z, T omega) override;

  void addVelocityBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1,
                           T omega) override;
  void addVelocityBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) override;

  void addSlipBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1) override;
  void addSlipBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material) override;

  void addPressureBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1,
                           T omega) override;
  void addPressureBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) override;

  void addConvectionBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1,
                             T omega, T* uAv=NULL) override;
  void addConvectionBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega, T* uAv=NULL) override;

  void addImpedanceBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1,
                            T omega) override;
  void addImpedanceBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) override;

  void addImpedanceBoundaryIncompressible(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1,
                            T omega) override;
  void addImpedanceBoundaryIncompressible(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) override;

  void addPeriodicBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1,
                            T omega) override;
  void addPeriodicBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega) override;


  BlockLatticeStructure3D<T,Lattice>& getBlock() override;
  BlockLatticeStructure3D<T,Lattice> const& getBlock() const override;

  void outputOn() override;
  void outputOff() override;

private:
  template<int direction, int orientation>
  void addVelocityBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int direction, int orientation>
  void addPressureBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int direction, int orientation>
  void addConvectionBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv=NULL);
  template<int direction, int orientation>
  void addImpedanceBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int direction, int orientation>
  void addImpedanceFixedRefBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int direction, int orientation>
  void addImpedanceBoundaryIncompressible(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int plane, int normal1, int normal2>
  void addExternalVelocityEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int plane, int normal1, int normal2>
  void addInternalVelocityEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int plane, int normal1, int normal2>
  void addExternalImpedanceEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int normalX, int normalY, int normalZ>
  void addExternalVelocityCorner(int x, int y, int z, T omega);
  template<int normalX, int normalY, int normalZ>
  void addInternalVelocityCorner(int x, int y, int z, T omega);
  template<int normalX, int normalY, int normalZ>
  void addExternalImpedanceCorner(int x, int y, int z, T omega);

  template<int direction, int orientation>
  void addPeriodicBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int plane, int normal1, int normal2>
  void addExternalPeriodicEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega);
  template<int normalX, int normalY, int normalZ>
  void addExternalPeriodicCorner(int x, int y, int z, T omega);
private:
  BlockLatticeStructure3D<T,Lattice>& block;
  std::vector<Momenta<T,Lattice>*>  momentaVector;
  std::vector<Dynamics<T,Lattice>*> dynamicsVector;
  bool _output;
  mutable OstreamManager clout;
};


///////// class BoundaryConditionInstantiator3D ////////////////////////

template<typename T, template<typename U> class Lattice, class BoundaryManager>
BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::BoundaryConditionInstantiator3D (
  BlockLatticeStructure3D<T,Lattice>& block_)
  : block(block_), _output(false), clout(std::cout,"BoundaryConditionInstantiator3D")
{ }

template<typename T, template<typename U> class Lattice, class BoundaryManager>
BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::~BoundaryConditionInstantiator3D()
{
  for (unsigned iDynamics=0; iDynamics<dynamicsVector.size(); ++iDynamics) {
    delete dynamicsVector[iDynamics];
  }
  for (unsigned iMomenta=0; iMomenta<dynamicsVector.size(); ++iMomenta) {
    delete momentaVector[iMomenta];
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

  auto* momenta
    = BoundaryManager::template getVelocityBoundaryMomenta<direction,orientation>();
  auto* postProcessor
    = BoundaryManager::template getVelocityBoundaryProcessor<direction,orientation>(x0,x1, y0,y1, z0,z1);
  auto* dynamics
    = BoundaryManager::template getVelocityBoundaryDynamics<direction,orientation,
    typename std::remove_reference<decltype(*momenta)>::type,
    typename std::remove_reference<decltype(*postProcessor)>::type::PostProcessorType>(omega, *momenta);

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, dynamics);

  if (_output) {
      clout << "addVelocityBoundary<" << direction << ","<< orientation << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << ", " << omega << " )" << std::endl;
  }
}

// Slip BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T, Lattice, BoundaryManager>::addSlipBoundary(
  int x0, int x1, int y0, int y1, int z0, int z1, int discreteNormalX, int discreteNormalY, int discreteNormalZ)
{
  OLB_PRECONDITION(x0==x1 || y0==y1 || z0==z1);

  for (int iX = x0; iX <= x1; ++iX) {
    for (int iY = y0; iY <= y1; ++iY) {
      for (int iZ = z0; iZ <= z1; ++iZ) {
        if (_output) {
          clout << "addSlipBoundary<" << discreteNormalX << ","<< discreteNormalY << ","<< discreteNormalZ << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << " )" << std::endl;
        }
      }
    }
  }
  std::cout << "add slip boundary not working" << std::endl; 
  assert(false); //TODO
  // PostProcessorGenerator3D<T, Lattice>* postProcessor = new SlipBoundaryProcessorGenerator3D<T, Lattice>(x0, x1, y0, y1, z0, z1, discreteNormalX, discreteNormalY, discreteNormalZ);
  // if (postProcessor) {
    // this->getBlock().addPostProcessor(*postProcessor);
  // }
}

// Pressure BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
    OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

    auto* momenta
      = BoundaryManager::template getPressureBoundaryMomenta<direction,orientation>();
    auto* postProcessor
      = BoundaryManager::template getPressureBoundaryProcessor<direction,orientation>(x0,x1, y0,y1, z0,z1);
    auto* dynamics
      = BoundaryManager::template getPressureBoundaryDynamics<direction,orientation,
      typename std::remove_reference<decltype(*momenta)>::type,
      typename std::remove_reference<decltype(*postProcessor)>::type::PostProcessorType>(omega, *momenta);

    this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, dynamics);

    if (_output) {
        clout << "addPressureBoundary<" << direction << ","<< orientation << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << ", " << omega << " )" << std::endl;
    }
}

// Convection BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

  for (int iX=x0; iX<=x1; ++iX) {
    for (int iY=y0; iY<=y1; ++iY) {
      for (int iZ=z0; iZ<=z1; ++iZ) {
        if (_output) {
          clout << "addConvectionBoundary<" << direction << ", " << orientation << ">(" << iX << ", " << iX << ", "<< iY << ", " << iY << ", " << iZ << ", " << iZ << ", "<< omega << " )" << std::endl;
        }
      }
    }
  }

  PostProcessorGenerator3D<T,Lattice>* postProcessor
    = BoundaryManager::template getConvectionBoundaryProcessor<direction,orientation>(x0,x1, y0,y1, z0,z1, uAv);
  if (postProcessor) {
    this->getBlock().addPostProcessor(*postProcessor);
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

//  static NoDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,direction,orientation>> dynamics;
  static typename BoundaryManager::template ImpedancePlaneBoundary<direction,orientation>::DynamicsType dynamics;

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, &dynamics);

  if (_output) {
      clout << "addVelocityBoundary<" << direction << ","<< orientation << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << ", " << omega << " )" << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

  static typename BoundaryManager::template ImpedanceFixedRefPlaneBoundary<direction,orientation>::DynamicsType dynamics;

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, &dynamics);

  if (_output) {
      clout << "addVelocityBoundary<" << direction << ","<< orientation << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << ", " << omega << " )" << std::endl;
  }
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

//    static NoDynamics<T,Lattice,ImpedanceBoundaryProcessor3D<T,Lattice,direction,orientation>> dynamics;
  static typename BoundaryManager::template ImpedancePlaneBoundaryIncompressible<direction,orientation>::DynamicsType dynamics;

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, &dynamics);

  if (_output) {
      clout << "addVelocityBoundary<" << direction << ","<< orientation << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << ", " << omega << " )" << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int plane, int normal1, int normal2>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION(
    ( x0==x1 && y0==y1 ) ||
    ( x0==x1 && z0==z1 ) ||
    ( y0==y1 && z0==z1 ) );

  auto* momenta
    = BoundaryManager::template getExternalVelocityEdgeMomenta<plane,normal1,normal2>();
  auto* postProcessor
    = BoundaryManager::template getExternalVelocityEdgeProcessor<plane,normal1,normal2>(x0,x1, y0,y1, z0,z1);
  auto* dynamics
    = BoundaryManager::template getExternalVelocityEdgeDynamics<plane,normal1,normal2,
    typename std::remove_reference<decltype(*momenta)>::type,
    typename std::remove_reference<decltype(*postProcessor)>::type::PostProcessorType>(omega, *momenta);

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, dynamics);

}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int plane, int normal1, int normal2>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  if (!(( x0==x1 && y0==y1 ) ||
        ( x0==x1 && z0==z1 ) ||
        ( y0==y1 && z0==z1 ) )) {
    clout << x0 <<" "<< x1 <<" "<< y0 <<" "<< y1 <<" "<< z0 <<" "<< z1 << std::endl;
  }

  OLB_PRECONDITION(
    ( x0==x1 && y0==y1 ) ||
    ( x0==x1 && z0==z1 ) ||
    ( y0==y1 && z0==z1 ) );

  auto* momenta
    = BoundaryManager::template getInternalVelocityEdgeMomenta<plane,normal1,normal2>();
  auto* postProcessor
    = BoundaryManager::template getInternalVelocityEdgeProcessor<plane,normal1,normal2>(x0,x1, y0,y1, z0,z1);
  auto* dynamics
    = BoundaryManager::template getInternalVelocityEdgeDynamics<plane,normal1,normal2,
    typename std::remove_reference<decltype(*momenta)>::type,
    typename std::remove_reference<decltype(*postProcessor)>::type::PostProcessorType>(omega, *momenta);

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, dynamics);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int plane, int normal1, int normal2>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION(
    ( x0==x1 && y0==y1 ) ||
    ( x0==x1 && z0==z1 ) ||
    ( y0==y1 && z0==z1 ) );

//  static NoDynamics<T,Lattice,ImpedanceBoundaryEdgeProcessor3D<T,Lattice,plane,normal1,normal2>> dynamics;
  static typename BoundaryManager::template ImpedanceEdgeBoundary<plane,normal1,normal2>::DynamicsType dynamics;

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, &dynamics);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int xNormal, int yNormal, int zNormal>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCorner(int x, int y, int z, T omega)
{
    auto* momenta
      = BoundaryManager::template getExternalVelocityCornerMomenta<xNormal,yNormal,zNormal>();
    auto* postProcessor
      = BoundaryManager::template getExternalVelocityCornerProcessor<xNormal,yNormal,zNormal>(x, y, z);
    auto* dynamics
      = BoundaryManager::template getExternalVelocityCornerDynamics<xNormal,yNormal,zNormal,
      typename std::remove_reference<decltype(*momenta)>::type,
      typename std::remove_reference<decltype(*postProcessor)>::type::PostProcessorType>(omega, *momenta);

  this->getBlock().defineDynamics(x,x,y,y,z,z, dynamics);

  if (_output) {
    clout << "addExternalVelocityCorner<" << xNormal << ", " << yNormal << ", " << zNormal << ">(" << x << ", " << y << ", "<< z << omega << " )" << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int xNormal, int yNormal, int zNormal>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCorner(int x, int y, int z, T omega)
{

  auto* momenta
    = BoundaryManager::template getInternalVelocityCornerMomenta<xNormal,yNormal,zNormal>();
  auto* postProcessor
    = BoundaryManager::template getInternalVelocityCornerProcessor<xNormal,yNormal,zNormal>(x, y, z);
  auto* dynamics
    = BoundaryManager::template getInternalVelocityCornerDynamics<xNormal,yNormal,zNormal,
    typename std::remove_reference<decltype(*momenta)>::type,
    typename std::remove_reference<decltype(*postProcessor)>::type::PostProcessorType>(omega, *momenta);

  this->getBlock().defineDynamics(x,x,y,y,z,z, dynamics);

  if (_output) {
    clout << "addInternalVelocityCorner<" << xNormal << ", " << yNormal << ", " << zNormal << ">(" << x << ", " << y << ", "<< z << omega << " )" << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int xNormal, int yNormal, int zNormal>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCorner(int x, int y, int z, T omega)
{

//    static NoDynamics<T,Lattice,ImpedanceBoundaryCornerProcessor3D<T,Lattice,xNormal,yNormal,zNormal>> dynamics;
    static typename BoundaryManager::template ImpedanceCornerBoundary<xNormal,yNormal,zNormal>::DynamicsType dynamics;

    this->getBlock().defineDynamics(x,x,y,y,z,z, &dynamics);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  std::vector<int> discreteNormal(4,0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {

        if (blockGeometryStructure.getMaterial(iX, iY, iZ)==material) {
          discreteNormal = blockGeometryStructure.getStatistics().getType(iX,iY,iZ);
          if (discreteNormal[0] == 0) {

            if (discreteNormal[1] != 0 && discreteNormal[1] == -1) {

              addVelocityBoundary<0,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] != 0 && discreteNormal[1] == 1) {

              addVelocityBoundary<0,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == -1) {

              addVelocityBoundary<1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == 1) {

              addVelocityBoundary<1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }


            else if (discreteNormal[3] != 0 && discreteNormal[3] == -1) {

              addVelocityBoundary<2,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[3] != 0 && discreteNormal[3] == 1) {

              addVelocityBoundary<2,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }

          else if (discreteNormal[0] == 1) {

            if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalVelocityCorner<1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addExternalVelocityCorner<1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addExternalVelocityCorner<1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

              addExternalVelocityCorner<1,-1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalVelocityCorner<-1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addExternalVelocityCorner<-1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addExternalVelocityCorner<-1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

              addExternalVelocityCorner<-1,-1,-1>(iX,iY,iZ, omega);

            }
            ///                     addExternalVelocityCorner<discreteNormal[1],discreteNormal[2],discreteNormal[3]>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 2) {

            if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addInternalVelocityCorner<1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addExternalVelocityCorner<1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addInternalVelocityCorner<1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

              addInternalVelocityCorner<1,-1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addInternalVelocityCorner<-1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addInternalVelocityCorner<-1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addInternalVelocityCorner<-1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

              addInternalVelocityCorner<-1,-1,-1>(iX,iY,iZ, omega);

            }
            ///                     addInternalVelocityCorner<discreteNormal[1],discreteNormal[2],discreteNormal[3]>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 3) {

            if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalVelocityEdge<0,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addExternalVelocityEdge<0,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addExternalVelocityEdge<0,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

              addExternalVelocityEdge<0,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

              addExternalVelocityEdge<1,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

              addExternalVelocityEdge<1,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

              addExternalVelocityEdge<1,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

              addExternalVelocityEdge<1,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

              addExternalVelocityEdge<2,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

              addExternalVelocityEdge<2,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

              addExternalVelocityEdge<2,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

              addExternalVelocityEdge<2,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }

          else if (discreteNormal[0] == 4) {

            if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addInternalVelocityEdge<0,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addInternalVelocityEdge<0,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addInternalVelocityEdge<0,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

              addInternalVelocityEdge<0,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

              addInternalVelocityEdge<1,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

              addInternalVelocityEdge<1,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

              addInternalVelocityEdge<1,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

              addInternalVelocityEdge<1,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

              addInternalVelocityEdge<2,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

              addInternalVelocityEdge<2,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

              addInternalVelocityEdge<2,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

              addInternalVelocityEdge<2,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega)
{

  addVelocityBoundary(blockGeometryStructure, material, 0, blockGeometryStructure.getNx()-1, 0, blockGeometryStructure.getNy()-1, 0, blockGeometryStructure.getNz()-1, omega);

}

// Slip BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T, Lattice, BoundaryManager>::addSlipBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1)
{
  std::vector<int> discreteNormal(4, 0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {
        const BlockGeometryStructure3D<T>& bgs = blockGeometryStructure;
        if (bgs.get(iX, iY, iZ) == material) {
          discreteNormal = blockGeometryStructure.getStatistics().getType(iX, iY, iZ);
          if (discreteNormal[1]!=0 || discreteNormal[2]!=0 || discreteNormal[3]!=0) {
            addSlipBoundary(iX, iX, iY, iY, iZ, iZ, discreteNormal[1], discreteNormal[2], discreteNormal[3]);
          } else {
            clout << "Warning: Could not addSlipBoundary (" << iX << ", " << iY << ", " << iZ << "), discreteNormal=(" << discreteNormal[0] <<","<< discreteNormal[1] <<","<< discreteNormal[2] <<","<< discreteNormal[3] <<"), set to bounceBack" << std::endl;
            this->getBlock().defineDynamics(iX, iY, iZ, &instances::getBounceBack<T, Lattice>() );
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T, Lattice, BoundaryManager>::addSlipBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material)
{
  addSlipBoundary(blockGeometryStructure, material, 0,
                  blockGeometryStructure.getNx()-1, 0,
                  blockGeometryStructure.getNy()-1, 0,
                  blockGeometryStructure.getNz()-1);
}


// Pressure BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  std::vector<int> discreteNormal(4,0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {

        if (blockGeometryStructure.getMaterial(iX, iY, iZ)==material) {

          discreteNormal = blockGeometryStructure.getStatistics().getType(iX,iY,iZ);

          if (discreteNormal[0] == 0) {

            if (discreteNormal[1] != 0 && discreteNormal[1] == -1) {

              addPressureBoundary<0,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] != 0 && discreteNormal[1] == 1) {

              addPressureBoundary<0,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == -1) {

              addPressureBoundary<1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == 1) {

              addPressureBoundary<1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }


            else if (discreteNormal[3] != 0 && discreteNormal[3] == -1) {

              addPressureBoundary<2,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[3] != 0 && discreteNormal[3] == 1) {

              addPressureBoundary<2,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }
        }
      }
    }
  }
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega)
{

  addPressureBoundary(blockGeometryStructure, material, 0, blockGeometryStructure.getNx()-1, 0, blockGeometryStructure.getNy()-1, 0, blockGeometryStructure.getNz()-1, omega);
}

// Convection BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  std::vector<int> discreteNormal(4,0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {

        if (blockGeometryStructure.getMaterial(iX, iY, iZ)==material) {

          discreteNormal = blockGeometryStructure.getStatistics().getType(iX,iY,iZ);

          if (discreteNormal[0] == 0) {

            if (discreteNormal[1] != 0 && discreteNormal[1] == -1) {

              addConvectionBoundary<0,-1>(iX,iX,iY,iY,iZ,iZ, omega, uAv);

            }

            else if (discreteNormal[1] != 0 && discreteNormal[1] == 1) {

              addConvectionBoundary<0,1>(iX,iX,iY,iY,iZ,iZ, omega, uAv);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == -1) {

              addConvectionBoundary<1,-1>(iX,iX,iY,iY,iZ,iZ, omega, uAv);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == 1) {

              addConvectionBoundary<1,1>(iX,iX,iY,iY,iZ,iZ, omega, uAv);

            }


            else if (discreteNormal[3] != 0 && discreteNormal[3] == -1) {

              addConvectionBoundary<2,-1>(iX,iX,iY,iY,iZ,iZ, omega, uAv);

            }

            else if (discreteNormal[3] != 0 && discreteNormal[3] == 1) {

              addConvectionBoundary<2,1>(iX,iX,iY,iY,iZ,iZ, omega, uAv);

            }
          }
        }
      }
    }
  }
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega, T* uAv)
{

  addConvectionBoundary(blockGeometryStructure, material, 0, blockGeometryStructure.getNx()-1, 0, blockGeometryStructure.getNy()-1, 0, blockGeometryStructure.getNz()-1, omega, uAv);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  std::vector<int> discreteNormal(4,0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {

        if (blockGeometryStructure.getMaterial(iX, iY, iZ)==material) {
          discreteNormal = blockGeometryStructure.getStatistics().getType(iX,iY,iZ);
          if (discreteNormal[0] == 0) {

            if (discreteNormal[1] != 0 && discreteNormal[1] == -1) {

                addImpedanceBoundary<0,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] != 0 && discreteNormal[1] == 1) {

                addImpedanceBoundary<0,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == -1) {

                addImpedanceBoundary<1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == 1) {

                addImpedanceBoundary<1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }


            else if (discreteNormal[3] != 0 && discreteNormal[3] == -1) {

                addImpedanceBoundary<2,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[3] != 0 && discreteNormal[3] == 1) {

                addImpedanceBoundary<2,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }

          else if (discreteNormal[0] == 1) {

            if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalImpedanceCorner<1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addExternalImpedanceCorner<1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addExternalImpedanceCorner<1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

                addExternalImpedanceCorner<1,-1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

                addExternalImpedanceCorner<-1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

                addExternalImpedanceCorner<-1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

                addExternalImpedanceCorner<-1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

                addExternalImpedanceCorner<-1,-1,-1>(iX,iY,iZ, omega);

            }
            ///                     addExternalVelocityCorner<discreteNormal[1],discreteNormal[2],discreteNormal[3]>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 2) {
            throw std::logic_error( "Internal Corner not possible for impedance boundary" );
            ///                     addInternalVelocityCorner<discreteNormal[1],discreteNormal[2],discreteNormal[3]>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 3) {

            if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalImpedanceEdge<0,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

                addExternalImpedanceEdge<0,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<0,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<0,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

                addExternalImpedanceEdge<1,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

                addExternalImpedanceEdge<1,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<1,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<1,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }

          else if (discreteNormal[0] == 4) {
            throw std::logic_error( "Internal edge not possible for impedance boundary" );
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  std::vector<int> discreteNormal(4,0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {

        if (blockGeometryStructure.getMaterial(iX, iY, iZ)==material) {
          discreteNormal = blockGeometryStructure.getStatistics().getType(iX,iY,iZ);
          if (discreteNormal[0] == 0) {

            if (discreteNormal[1] != 0 && discreteNormal[1] == -1) {

                addImpedanceBoundaryIncompressible<0,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] != 0 && discreteNormal[1] == 1) {

                addImpedanceBoundaryIncompressible<0,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == -1) {

                addImpedanceBoundaryIncompressible<1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[2] != 0 && discreteNormal[2] == 1) {

                addImpedanceBoundaryIncompressible<1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }


            else if (discreteNormal[3] != 0 && discreteNormal[3] == -1) {

                addImpedanceBoundaryIncompressible<2,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[3] != 0 && discreteNormal[3] == 1) {

                addImpedanceBoundaryIncompressible<2,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }

          else if (discreteNormal[0] == 1) {

            if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalImpedanceCorner<1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

              addExternalImpedanceCorner<1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

              addExternalImpedanceCorner<1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

                addExternalImpedanceCorner<1,-1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

                addExternalImpedanceCorner<-1,1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

                addExternalImpedanceCorner<-1,-1,1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

                addExternalImpedanceCorner<-1,1,-1>(iX,iY,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

                addExternalImpedanceCorner<-1,-1,-1>(iX,iY,iZ, omega);

            }
            ///                     addExternalVelocityCorner<discreteNormal[1],discreteNormal[2],discreteNormal[3]>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 2) {

            throw std::logic_error( "Internal Corner not possible for impedance boundary" );
            ///                     addInternalVelocityCorner<discreteNormal[1],discreteNormal[2],discreteNormal[3]>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 3) {

            if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == 1) {

              addExternalImpedanceEdge<0,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == 1) {

                addExternalImpedanceEdge<0,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<0,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<0,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

                addExternalImpedanceEdge<1,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == 1) {

                addExternalImpedanceEdge<1,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<1,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == -1) {

                addExternalImpedanceEdge<1,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 0) {

                addExternalImpedanceEdge<2,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            }
          }

          else if (discreteNormal[0] == 4) {

            throw std::logic_error( "Internal edge not possible for impedance boundary" );

          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  std::vector<int> discreteNormal(4,0);
  for (int iX = x0; iX <= x1; iX++) {
    for (int iY = y0; iY <= y1; iY++) {
      for (int iZ = z0; iZ <= z1; iZ++) {

        if (blockGeometryStructure.getMaterial(iX, iY, iZ)==material)
        {
          discreteNormal = blockGeometryStructure.getStatistics().getType(iX,iY,iZ);
          if (discreteNormal[0] == 0)
          {

            if (discreteNormal[1] != 0 && discreteNormal[1] == -1)
                addPeriodicBoundary<0,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] != 0 && discreteNormal[1] == 1)
                addPeriodicBoundary<0,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[2] != 0 && discreteNormal[2] == -1)
                addPeriodicBoundary<1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[2] != 0 && discreteNormal[2] == 1)
                addPeriodicBoundary<1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[3] != 0 && discreteNormal[3] == -1)
                addPeriodicBoundary<2,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[3] != 0 && discreteNormal[3] == 1)
                addPeriodicBoundary<2,1>(iX,iX,iY,iY,iZ,iZ, omega);
          }

          else if (discreteNormal[0] == 1)
          {
            if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 1)
              addExternalPeriodicCorner<1,1,1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 1)
              addExternalPeriodicCorner<1,-1,1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == -1)
              addExternalPeriodicCorner<1,1,-1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == -1)
                addExternalPeriodicCorner<1,-1,-1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 1)
                addExternalPeriodicCorner<-1,1,1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 1)
                addExternalPeriodicCorner<-1,-1,1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == -1)
                addExternalPeriodicCorner<-1,1,-1>(iX,iY,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == -1)
                addExternalPeriodicCorner<-1,-1,-1>(iX,iY,iZ, omega);
          }

          else if (discreteNormal[0] == 2)
            throw std::logic_error( "Internal Corner not possible for Periodic boundary" );

          else if (discreteNormal[0] == 3)
          {
            if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == 1)
              addExternalPeriodicEdge<0,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == 1)
                addExternalPeriodicEdge<0,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 0 && discreteNormal[2] == 1 && discreteNormal[3] == -1)
                addExternalPeriodicEdge<0,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 0 && discreteNormal[2] == -1 && discreteNormal[3] == -1)
                addExternalPeriodicEdge<0,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == 1)
                addExternalPeriodicEdge<1,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == 1)
                addExternalPeriodicEdge<1,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 0 && discreteNormal[3] == -1)
                addExternalPeriodicEdge<1,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 0 && discreteNormal[3] == -1)
                addExternalPeriodicEdge<1,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == 1 && discreteNormal[3] == 0)
                addExternalPeriodicEdge<2,1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == 1 && discreteNormal[3] == 0)
                addExternalPeriodicEdge<2,-1,1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == 1 && discreteNormal[2] == -1 && discreteNormal[3] == 0)
                addExternalPeriodicEdge<2,1,-1>(iX,iX,iY,iY,iZ,iZ, omega);

            else if (discreteNormal[1] == -1 && discreteNormal[2] == -1 && discreteNormal[3] == 0)
                addExternalPeriodicEdge<2,-1,-1>(iX,iX,iY,iY,iZ,iZ, omega);
          }

          else if (discreteNormal[0] == 4)
            throw std::logic_error( "Internal edge not possible for Periodic boundary" );
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

  static typename BoundaryManager::template PeriodicPlaneBoundary<direction,orientation>::DynamicsType dynamics;

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, &dynamics);

  if (_output) {
      clout << "addVelocityBoundary<" << direction << ","<< orientation << ">("  << x0 << ", "<< x1 << ", " << y0 << ", " << y1 << ", " << z0 << ", " << z1 << ", " << omega << " )" << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int plane, int normal1, int normal2>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  OLB_PRECONDITION(
    ( x0==x1 && y0==y1 ) ||
    ( x0==x1 && z0==z1 ) ||
    ( y0==y1 && z0==z1 ) );

  static typename BoundaryManager::template PeriodicEdgeBoundary<plane,normal1,normal2>::DynamicsType dynamics;

  this->getBlock().defineDynamics(x0,x1,y0,y1,z0,z1, &dynamics);
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int xNormal, int yNormal, int zNormal>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCorner(int x, int y, int z, T omega)
{

    static typename BoundaryManager::template PeriodicCornerBoundary<xNormal,yNormal,zNormal>::DynamicsType dynamics;

    this->getBlock().defineDynamics(x,x,y,y,z,z, &dynamics);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega)
{
  addImpedanceBoundary(blockGeometryStructure, material, 0, blockGeometryStructure.getNx()-1, 0, blockGeometryStructure.getNy()-1, 0, blockGeometryStructure.getNz()-1, omega);
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega)
{
  addImpedanceBoundaryIncompressible(blockGeometryStructure, material, 0, blockGeometryStructure.getNx()-1, 0, blockGeometryStructure.getNy()-1, 0, blockGeometryStructure.getNz()-1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary(BlockGeometryStructure3D<T>& blockGeometryStructure, int material, T omega)
{
  addPeriodicBoundary(blockGeometryStructure, material, 0, blockGeometryStructure.getNx()-1, 0, blockGeometryStructure.getNy()-1, 0, blockGeometryStructure.getNz()-1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addVelocityBoundary<0,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addVelocityBoundary<0,1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addVelocityBoundary<1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addVelocityBoundary<1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addVelocityBoundary<2,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addVelocityBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addVelocityBoundary<2, 1>(x0,x1,y0,y1,z0,z1, omega);
}

// Pressure BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPressureBoundary<0,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPressureBoundary<0,1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPressureBoundary<1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPressureBoundary<1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPressureBoundary<2,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPressureBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPressureBoundary<2, 1>(x0,x1,y0,y1,z0,z1, omega);
}

// Convection BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  addConvectionBoundary<0,-1>(x0,x1,y0,y1,z0,z1, omega, uAv);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  addConvectionBoundary<0,1>(x0,x1,y0,y1,z0,z1, omega, uAv);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  addConvectionBoundary<1,-1>(x0,x1,y0,y1,z0,z1, omega, uAv);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  addConvectionBoundary<1, 1>(x0,x1,y0,y1,z0,z1, omega, uAv);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  addConvectionBoundary<2,-1>(x0,x1,y0,y1,z0,z1, omega, uAv);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addConvectionBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega, T* uAv)
{
  addConvectionBoundary<2, 1>(x0,x1,y0,y1,z0,z1, omega, uAv);
}

// Impedance BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundary<0,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundary<0,1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundary<1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundary<1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundary<2,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundary<2, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceFixedRefBoundary<0,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceFixedRefBoundary<0,1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceFixedRefBoundary<1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceFixedRefBoundary<1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceFixedRefBoundary<2,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceFixedRefBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceFixedRefBoundary<2, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundaryIncompressible<0,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundaryIncompressible<0,1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundaryIncompressible<1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundaryIncompressible<1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundaryIncompressible<2,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addImpedanceBoundaryIncompressible2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addImpedanceBoundaryIncompressible<2, 1>(x0,x1,y0,y1,z0,z1, omega);
}


// Velocity BC

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<0,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<0,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<0, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<0, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<1,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<1,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<1, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<1, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<2,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<2,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<2, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalVelocityEdge<2, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}



template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<0,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<0,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<0, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<0, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<1,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<1,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<1, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<1, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<2,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<2,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<2, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addInternalVelocityEdge<2, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<0,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<0,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<0, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<0, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<1,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<1,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<1, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<1, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<2,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<2,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<2, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalImpedanceEdge<2, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}



template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerNNN(int x, int y, int z, T omega)
{
  addExternalVelocityCorner<-1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerNNP(int x, int y, int z, T omega)
{
  addExternalVelocityCorner<-1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerNPN(int x, int y, int z, T omega)
{
  addExternalVelocityCorner<-1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerNPP(int x, int y, int z, T omega)
{
  addExternalVelocityCorner<-1, 1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerPNN(int x, int y, int z, T omega)
{
  addExternalVelocityCorner< 1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerPNP(int x, int y, int z, T omega)
{
  addExternalVelocityCorner< 1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerPPN(int x, int y, int z, T omega)
{
  addExternalVelocityCorner< 1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalVelocityCornerPPP(int x, int y, int z, T omega)
{
  addExternalVelocityCorner< 1, 1, 1>(x,y,z, omega);
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerNNN(int x, int y, int z, T omega)
{
  addInternalVelocityCorner<-1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerNNP(int x, int y, int z, T omega)
{
  addInternalVelocityCorner<-1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerNPN(int x, int y, int z, T omega)
{
  addInternalVelocityCorner<-1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerNPP(int x, int y, int z, T omega)
{
  addInternalVelocityCorner<-1, 1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerPNN(int x, int y, int z, T omega)
{
  addInternalVelocityCorner< 1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerPNP(int x, int y, int z, T omega)
{
  addInternalVelocityCorner< 1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerPPN(int x, int y, int z, T omega)
{
  addInternalVelocityCorner< 1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addInternalVelocityCornerPPP(int x, int y, int z, T omega)
{
  addInternalVelocityCorner< 1, 1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerNNN(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner<-1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerNNP(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner<-1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerNPN(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner<-1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerNPP(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner<-1, 1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerPNN(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner< 1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerPNP(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner< 1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerPPN(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner< 1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalImpedanceCornerPPP(int x, int y, int z, T omega)
{
  addExternalImpedanceCorner< 1, 1, 1>(x,y,z, omega);
}

//// Periodic Boundary

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPeriodicBoundary<0,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPeriodicBoundary<0,1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPeriodicBoundary<1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPeriodicBoundary<1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPeriodicBoundary<2,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addPeriodicBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addPeriodicBoundary<2, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<0,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<0,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<0, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<0, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<1,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<1,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<1, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<1, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<2,-1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<2,-1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<2, 1,-1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1, T omega)
{
  addExternalPeriodicEdge<2, 1, 1>(x0,x1,y0,y1,z0,z1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerNNN(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner<-1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerNNP(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner<-1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerNPN(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner<-1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerNPP(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner<-1, 1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerPNN(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner< 1,-1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerPNP(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner< 1,-1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerPPN(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner< 1, 1,-1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::
addExternalPeriodicCornerPPP(int x, int y, int z, T omega)
{
  addExternalPeriodicCorner< 1, 1, 1>(x,y,z, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
BlockLatticeStructure3D<T,Lattice>& BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::getBlock()
{
  return block;
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
BlockLatticeStructure3D<T,Lattice> const& BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::getBlock() const
{
  return block;
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::outputOn()
{
  _output = true;
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void BoundaryConditionInstantiator3D<T,Lattice,BoundaryManager>::outputOff()
{
  _output = false;
}


}  // namespace olb

#endif
