/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef FD_BOUNDARIES_2D_HH
#define FD_BOUNDARIES_2D_HH

#include "boundaryPostProcessors2D.h"
#include "core/finiteDifference2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "dynamics/lbHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"

namespace olb {

///////////  StraightFdBoundaryProcessor2D ///////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation>
StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
StraightFdBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_)
{
  OLB_PRECONDITION(x0==x1 || y0==y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
StraightFdBoundaryProcessor2D()
  : x0(0), x1(0), y0(0), y1(0)
{}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_)
{
  using namespace olb::util::tensorIndices2D;

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    int iX;

#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d];
      for (int iY=newY0; iY<=newY1; ++iY) {
        CellView<T,Lattice> cell = blockLattice.get(iX,iY);
        Dynamics<T,Lattice>* dynamics = blockLattice.getDynamics(iX, iY);

        T rho, u[Lattice<T>::d];
        cell.computeRhoU(rho,u);

        interpolateGradients<0>(blockLattice, dx_u, iX, iY);
        interpolateGradients<1>(blockLattice, dy_u, iX, iY);
        T dx_ux = dx_u[0];
        T dy_ux = dy_u[0];
        T dx_uy = dx_u[1];
        T dy_uy = dy_u[1];
        T omega = dynamics->getOmega();
        T sToPi = - rho / Lattice<T>::invCs2() / omega;
        T pi[util::TensorVal<Lattice<T> >::n];
        pi[xx] = (T)2 * dx_ux * sToPi;
        pi[yy] = (T)2 * dy_uy * sToPi;
        pi[xy] = (dx_uy + dy_ux) * sToPi;

        // Computation of the particle distribution functions
        // according to the regularized formula

        T uSqr = util::normSqr<T,2>(u);
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
          cell[iPop] = dynamics -> computeEquilibrium(iPop,rho,u,uSqr) +
                       firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
template<class Dynamics>
void StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, T const * const OPENLB_RESTRICT collisionData,
		size_t index, size_t indexCalcHelper[Lattice<T>::d])
{
  using namespace olb::util::tensorIndices2D;

  size_t ny = indexCalcHelper[1];

      T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d];

      T rho, u[Lattice<T>::d];

      rho = util::getRho<T,Lattice>(cellData, index);

      interpolateGradients<0>(cellData, dx_u, index, ny);
      interpolateGradients<1>(cellData, dy_u, index, ny);
      const T dx_ux = dx_u[0];
      const T dy_ux = dy_u[0];
      const T dx_uy = dx_u[1];
      const T dy_uy = dy_u[1];

      //TODO: Get actual omega
      const T omega = collisionData[Dynamics::omegaIndex];
      const T sToPi = - rho / Lattice<T>::invCs2() / omega;
      T pi[util::TensorVal<Lattice<T> >::n];
      pi[xx] = (T)2 * dx_ux * sToPi;
      pi[yy] = (T)2 * dy_uy * sToPi;
      pi[xy] = (dx_uy + dy_ux) * sToPi;

      // Computation of the particle distribution functions
      // according to the regularized formula
      util::getU<T,Lattice>(cellData, index, u);

      T uSqr = util::normSqr<T,2>(u);
      for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
        cellData[iPop][index] = Dynamics::computeEquilibriumStatic(iPop,rho,u,uSqr) +
                     firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
      }
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
template<class Dynamics>
void StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  processSubDomain<Dynamics>(cellData, momentaData, collisionData, index, indexCalcHelper);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
template<int deriveDirection>
void StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T,Lattice,direction,orientation,direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
template<int deriveDirection>
void StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		T velDeriv[Lattice<T>::d], size_t index, size_t ny)
{
  fd::DirectedGradients2D<T,Lattice,direction,orientation,direction==deriveDirection>::
  interpolateVector(cellData, velDeriv, index, ny);
}

////////  StraightFdBoundaryProcessorGenerator2D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction,int orientation>
StraightFdBoundaryProcessorGenerator2D<T,Lattice, direction,orientation>::
StraightFdBoundaryProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_)
  : PostProcessorGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_)
{ }

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor2D<T,Lattice>*
StraightFdBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::generate() const
{
  return new StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation>
         ( this->x0, this->x1, this->y0, this->y1);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor2D<T,Lattice>*
StraightFdBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::generateStatic() const
{
  static StraightFdBoundaryProcessor2D<T,Lattice,direction,orientation> postProcessor;
  return &postProcessor;
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessorGenerator2D<T,Lattice>*
StraightFdBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new StraightFdBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>
         (this->x0, this->x1, this->y0, this->y1);
}


////////  StraightConvectionBoundaryProcessor2D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation>
StraightConvectionBoundaryProcessor2D<T,Lattice,direction,orientation>::
StraightConvectionBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_, T* uAv_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_), uAv(uAv_)
{
  OLB_PRECONDITION(x0==x1 || y0==y1);

  saveCell = new T** [(size_t)(x1_-x0_+1)];
  for (int iX=0; iX<=x1_-x0_; ++iX) {
    saveCell[iX] = new T* [(size_t)(y1_-y0_+1)];
    for (int iY=0; iY<=y1_-y0_; ++iY) {
      saveCell[iX][iY] = new T [(size_t)(Lattice<T>::q)];
      for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
        saveCell[iX][iY][iPop] = T();
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
StraightConvectionBoundaryProcessor2D<T,Lattice,direction,orientation>::
~StraightConvectionBoundaryProcessor2D()
{
  for (int iX=0; iX<=x1-x0; ++iX) {
    for (int iY=0; iY<=y1-y0; ++iY) {
      delete [] saveCell[iX][iY];
    }
    delete [] saveCell[iX];
  }
  delete [] saveCell;
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void StraightConvectionBoundaryProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_)
{
  using namespace olb::util::tensorIndices2D;

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    int iX;
#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        CellView<T,Lattice> cell = blockLattice.get(iX,iY);
        for (int iPop = 0; iPop < Lattice<T>::q ; ++iPop) {
          if (Lattice<T>::c(iPop,direction)==-orientation) {
            cell[iPop] = saveCell[iX-newX0][iY-newY0][iPop];
          }
        }

        T rho0, u0[2];
        T rho1, u1[2];
        T rho2, u2[2];
        if (direction==0) {
          blockLattice.get(iX,iY).computeRhoU(rho0,u0);
          blockLattice.get(iX-orientation,iY).computeRhoU(rho1,u1);
          blockLattice.get(iX-orientation*2,iY).computeRhoU(rho2,u2);
        } else {
          blockLattice.get(iX,iY).computeRhoU(rho0,u0);
          blockLattice.get(iX,iY-orientation).computeRhoU(rho1,u1);
          blockLattice.get(iX,iY-orientation*2).computeRhoU(rho2,u2);
        }

        rho0 = T(1);
        rho1 = T(1);
        rho2 = T(1);
        T uDelta[2];
        T uAverage = rho0*u0[direction];
        if (uAv!=nullptr) {
          uAverage = *uAv;
        }
        uDelta[0]=-uAverage*0.5*(3*rho0*u0[0]-4*rho1*u1[0]+rho2*u2[0]);
        uDelta[1]=-uAverage*0.5*(3*rho0*u0[1]-4*rho1*u1[1]+rho2*u2[1]);

        for (int iPop = 0; iPop < Lattice<T>::q ; ++iPop) {
          if (Lattice<T>::c(iPop,direction) == -orientation) {
            saveCell[iX-newX0][iY-newY0][iPop] = cell[iPop] + Lattice<T>::invCs2()*Lattice<T>::t(iPop)*(uDelta[0]*Lattice<T>::c(iPop,0)+uDelta[1]*Lattice<T>::c(iPop,1));
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void StraightConvectionBoundaryProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}


////////  StraightConvectionBoundaryProcessorGenerator2D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction,int orientation>
StraightConvectionBoundaryProcessorGenerator2D<T,Lattice, direction,orientation>::
StraightConvectionBoundaryProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_, T* uAv_)
  : PostProcessorGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_), uAv(uAv_)
{ }

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor2D<T,Lattice>*
StraightConvectionBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::generate() const
{
  return new StraightConvectionBoundaryProcessor2D<T,Lattice,direction,orientation>
         ( this->x0, this->x1, this->y0, this->y1, uAv);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessorGenerator2D<T,Lattice>*
StraightConvectionBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new StraightConvectionBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>
         (this->x0, this->x1, this->y0, this->y1, uAv);
}


////////  SlipBoundaryProcessor2D ////////////////////////////////

template<typename T, template<typename U> class Lattice>
SlipBoundaryProcessor2D<T,Lattice>::
SlipBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_, int discreteNormalX, int discreteNormalY)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_)
{
  OLB_PRECONDITION(x0==x1 || y0==y1);
  reflectionPop[0] = 0;
  for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
    reflectionPop[iPop] = 0;
    // iPop are the directions which ointing into the fluid, discreteNormal is pointing outwarts
    if (Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY < 0) {
      // std::cout << "-----" <<s td::endl;
      int mirrorDirection0;
      int mirrorDirection1;
      int mult = 1;
      if (discreteNormalX*discreteNormalY==0) {
        mult = 2;
      }
      mirrorDirection0 = (Lattice<T>::c(iPop,0) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY )*discreteNormalX);
      mirrorDirection1 = (Lattice<T>::c(iPop,1) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY )*discreteNormalY);

      // computes mirror jPop
      for (reflectionPop[iPop] = 1; reflectionPop[iPop] < Lattice<T>::q ; reflectionPop[iPop]++) {
        if (Lattice<T>::c(iPop,0)==mirrorDirection0 && Lattice<T>::c(iPop,1)==mirrorDirection1 ) {
          break;
        }
      }
      //std::cout <<iPop << " to "<< jPop <<" for discreteNormal= "<< discreteNormalX << "/"<<discreteNormalY <<std::endl;
    }
  }
}

template<typename T, template<typename U> class Lattice>
void SlipBoundaryProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_)
{
  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    int iX;
#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iPop = 1; iPop < Lattice<T>::q ; ++iPop) {
          if (reflectionPop[iPop]!=0) {
            //do reflection
            blockLattice.get(iX,iY)[iPop] = blockLattice.get(iX,iY)[reflectionPop[iPop]];
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void SlipBoundaryProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

////////  SlipBoundaryProcessorGenerator2D ////////////////////////////////

template<typename T, template<typename U> class Lattice>
SlipBoundaryProcessorGenerator2D<T,Lattice>::
SlipBoundaryProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_, int discreteNormalX_, int discreteNormalY_)
  : PostProcessorGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_), discreteNormalX(discreteNormalX_), discreteNormalY(discreteNormalY_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>*
SlipBoundaryProcessorGenerator2D<T,Lattice>::generate() const
{
  return new SlipBoundaryProcessor2D<T,Lattice>
         ( this->x0, this->x1, this->y0, this->y1, discreteNormalX, discreteNormalY);
}

template<typename T, template<typename U> class Lattice>
PostProcessorGenerator2D<T,Lattice>*
SlipBoundaryProcessorGenerator2D<T,Lattice>::clone() const
{
  return new SlipBoundaryProcessorGenerator2D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, discreteNormalX, discreteNormalY);
}

/////////// OuterVelocityCornerProcessor2D /////////////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>::
OuterVelocityCornerProcessor2D(int x_, int y_)
  : x(x_), y(y_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>::
OuterVelocityCornerProcessor2D()
  : x(0), y(0)
{ }

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
void OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  using namespace olb::util::tensorIndices2D;

  T rho10 = blockLattice.get(x-1*xNormal, y-0*yNormal).computeRho();
  T rho01 = blockLattice.get(x-0*xNormal, y-1*yNormal).computeRho();

  T rho20 = blockLattice.get(x-2*xNormal, y-0*yNormal).computeRho();
  T rho02 = blockLattice.get(x-0*xNormal, y-2*yNormal).computeRho();

  T rho = (T)2/(T)3*(rho01+rho10) - (T)1/(T)6*(rho02+rho20);

  T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::interpolateVector(dy_u, blockLattice, x,y);
  T dx_ux = dx_u[0];
  T dy_ux = dy_u[0];
  T dx_uy = dx_u[1];
  T dy_uy = dy_u[1];

  CellView<T,Lattice> cell = blockLattice.get(x,y);
  Dynamics<T,Lattice>* dynamics = blockLattice.getDynamics(x, y);
  T omega = dynamics -> getOmega();

  T sToPi = - rho / Lattice<T>::invCs2() / omega;
  T pi[util::TensorVal<Lattice<T> >::n];
  pi[xx] = (T)2 * dx_ux * sToPi;
  pi[yy] = (T)2 * dy_uy * sToPi;
  pi[xy] = (dx_uy + dy_ux) * sToPi;

  // Computation of the particle distribution functions
  // according to the regularized formula
  T u[Lattice<T>::d];
  blockLattice.get(x,y).computeU(u);

  T uSqr = util::normSqr<T,2>(u);
  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] =
      dynamics -> computeEquilibrium(iPop,rho,u,uSqr) +
      firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
template<class Dynamics>
void OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  using namespace olb::util::tensorIndices2D;

  size_t ny = indexCalcHelper[1];

  size_t index10 = index;
  size_t index01 = index;
  size_t index20 = index;
  size_t index02 = index;

  if(xNormal > 0)
  {
    index10 -= ny;
    index20 -= 2*ny;
  }
  else if(xNormal < 0)
  {
    index10 += ny;
    index20 += 2*ny;
  }

  if(yNormal > 0)
  {
    index01 -= 1;
    index02 -= 2;
  }
  else if(yNormal < 0)
  {
    index01 += 1;
    index02 += 2;
  }

  const T rho10 = util::getRho<T,Lattice>(cellData, index10);
  const T rho01 = util::getRho<T,Lattice>(cellData, index01);

  const T rho20 = util::getRho<T,Lattice>(cellData, index20);
  const T rho02 = util::getRho<T,Lattice>(cellData, index02);

  T rho = (T)2/(T)3*(rho01+rho10) - (T)1/(T)6*(rho02+rho20);

  T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::interpolateVector(cellData,dx_u,index,ny);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::interpolateVector(cellData,dy_u,index,ny);
  const T dx_ux = dx_u[0];
  const T dy_ux = dy_u[0];
  const T dx_uy = dx_u[1];
  const T dy_uy = dy_u[1];

  const T omega = collisionData[Dynamics::omegaIndex];

  T sToPi = - rho / Lattice<T>::invCs2() / omega;
  T pi[util::TensorVal<Lattice<T> >::n];
  pi[xx] = (T)2 * dx_ux * sToPi;
  pi[yy] = (T)2 * dy_uy * sToPi;
  pi[xy] = (dx_uy + dy_ux) * sToPi;

  // Computation of the particle distribution functions
  // according to the regularized formula
  T u[Lattice<T>::d];
  util::getU<T,Lattice>(cellData,index,u);

  T uSqr = util::normSqr<T,2>(u);
  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop][index] =
      Dynamics::computeEquilibriumStatic(iPop,rho,u,uSqr) +
      firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
void OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_ )
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
template<class Dynamics>
void OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>::
processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		T const * const OPENLB_RESTRICT collisionData,
		size_t index, size_t indexCalcHelper[Lattice<T>::d])
{
  process(cellData, momentaData, index, collisionData, indexCalcHelper);
}


////////  OuterVelocityCornerProcessorGenerator2D ////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
OuterVelocityCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::
OuterVelocityCornerProcessorGenerator2D(int x_, int y_)
  : PostProcessorGenerator2D<T,Lattice>(x_, x_, y_, y_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
PostProcessor2D<T,Lattice>*
OuterVelocityCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::generate() const
{
  return new OuterVelocityCornerProcessor2D<T, Lattice, xNormal, yNormal>
         ( this->x0, this->y0);
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
PostProcessor2D<T,Lattice>*
OuterVelocityCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::generateStatic() const
{
  static OuterVelocityCornerProcessor2D<T,Lattice,xNormal,yNormal> postProcessor;
  return &postProcessor;
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
PostProcessorGenerator2D<T,Lattice>*
OuterVelocityCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::
clone() const
{
  return new OuterVelocityCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>
         ( this->x0, this->y0);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void RegularizedBoundaryProcessor2D<T,Lattice,direction,orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  T rho; 
  T u[Lattice<T>::d];

  Dynamics::MomentaType::computeRhoU(cellData, index, momentaData, 0, rho, u);

  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  T pi[util::TensorVal<Lattice<T> >::n];
  BoundaryHelpers<T,Lattice,direction,orientation>::computeStress(cellData, index, rho, u, pi);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    cellData[iPop][index] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) + firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void ImpedanceBoundaryProcessor2D<T,Lattice,direction,orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const T cs = 1/std::sqrt(Lattice<T>::invCs2());
    const T csSq = 1/Lattice<T>::invCs2();
//#define DEBUG_POSTPROC

#ifdef DEBUG_POSTPROC
    std::stringstream debugStream;
#endif

    for(unsigned int i=0; i<Lattice<T>::q; ++i)
    {
        cellData[i][index] += Lattice<T>::t(i);
#ifdef DEBUG_POSTPROC
        debugStream << cellData[i][index] << " ";
#endif
    }

    const T rhoOld = cellData[Lattice<T>::rhoIndex][index];
    T uOld[Lattice<T>::d];

    for(unsigned int iDim=0; iDim < Lattice<T>::d; ++iDim)
      uOld[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

#ifdef DEBUG_POSTPROC
    debugStream << std::endl << std::endl;
    debugStream << "Direction: " << direction << "; Orientation: " << orientation << std::endl;
    debugStream << "Calc rhoWall" << std::endl;
#endif
    T rhoWall = 0;
    for(int iPop=0; iPop<Lattice<T>::q; ++iPop)
    {
      const int factor = (std::abs( Lattice<T>::c(iPop,direction)*(Lattice<T>::c(iPop,direction)+orientation) )
                          + 1 - std::abs(Lattice<T>::c(iPop,direction)) );
      rhoWall += factor*cellData[iPop][index];
#ifdef DEBUG_POSTPROC
//      if(factor)
//          debugStream << factor << "*" << impedanceBoundaryHelpers::f[iPop] << " ";
#endif
    }

#ifdef DEBUG_POSTPROC
    debugStream << std::endl << "RhoWall: " << rhoWall << std::endl << std::endl;
    debugStream << "Calc temp and weights" << std::endl;
#endif

    T temp[Lattice<T>::d] = {0};

    for(int counter=0; counter < Lattice<T>::d-1; ++counter)
    {
#ifdef DEBUG_POSTPROC
        debugStream << "Direction: " << inPlane(counter) << std::endl;
#endif
        T weights = 0;
        for(int iPop=1; iPop<Lattice<T>::q; ++iPop)
        {
          bool isInAxisDirection = ( 1-std::abs(Lattice<T>::c(iPop,direction)) );
          // TODO: Verify if this is correct
        //                  if(Lattice<T>::d ==3)
        //                  {
        //                      isInAxisDirection = (isInAxisDirection && (1-std::abs(Lattice<T>::c[iPop][inPlane( (counter+1)%2 )])) );
        //                  }
          temp[inPlane(counter)] += cellData[iPop][index]*Lattice<T>::c(iPop,inPlane(counter))*isInAxisDirection;
          weights += Lattice<T>::t(iPop)*std::abs(Lattice<T>::c(iPop,inPlane(counter)))*isInAxisDirection;

#ifdef DEBUG_POSTPROC
//          if(isInAxisDirection)
//              debugStream << Lattice<T>::c(iPop,inPlane(counter)) << "*" << impedanceBoundaryHelpers::f[iPop] << "(" << iPop << ") ";
#endif

        }

#ifdef DEBUG_POSTPROC
          debugStream << std::endl << "Factor: " << csSq/weights << ", Sum(fi): " << temp[inPlane(counter)] << std::endl;
#endif

          temp[inPlane(counter)] *= csSq/weights;
#ifdef DEBUG_POSTPROC
          debugStream << "Temp[" << inPlane(counter) << "]: " << temp[inPlane(counter)] << "   ";
          debugStream << std::endl << std::endl;
#endif
    }

    T uNew[Lattice<T>::d];
    T deltaU[Lattice<T>::d];

    const T rhoOverRhoWall = rhoOld/rhoWall;

#ifdef DEBUG_POSTPROC
    debugStream << "RhoOld: " << rhoOld << " RhoWall: " << rhoWall << std::endl;
    for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
      debugStream << "uOld[" << iDim << "]: " << uOld[iDim] << std::endl;
#endif

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    bool isNan = false;

    if(true)
    {
        T uNewPlus[Lattice<T>::d];
        T deltaUPlus[Lattice<T>::d];

        T uNewMinus[Lattice<T>::d];
        T deltaUMinus[Lattice<T>::d];

        T variance = 0.001*uNew[direction] + eps;
        uNewPlus[direction] = uNew[direction] + variance;
        uNewMinus[direction] = uNew[direction] - variance;

    #ifdef DEBUG_POSTPROC
        debugStream << "uNew[" << direction << "] init: " << uNew[direction] << std::endl;
    #endif


        // bool converged = false;
        T uIncrementOld = 0;
        T relaxationFactor = 1;

        for(int iter = 0; iter < iterMax; ++iter)
        {
          deltaU[direction] = uNew[direction] - uOld[direction];

          T deltaUAbs = deltaU[direction]*deltaU[direction];

    #ifdef DEBUG_POSTPROC
          debugStream << "uNew[" << direction << "] init: " << uNew[direction] << std::endl;
    #endif

          deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
          deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

          T deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
          T deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

          for(int counter=0; counter<Lattice<T>::d-1; ++counter)
          {
              uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

              deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

              deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

              uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
              uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

              deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
              deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

              deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
              deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

    #ifdef DEBUG_POSTPROC
              debugStream << "uNew[" << inPlane(counter) << "] init: " << uNew[inPlane(counter)] << std::endl;
    #endif
          }

          deltaUAbs = std::sqrt(deltaUAbs);

          deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
          deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

    #ifdef DEBUG_POSTPROC
          debugStream << std::endl;
    #endif

          T onePlusMinusU = 1+orientation*uNew[direction];

          T rho = rhoWall/onePlusMinusU;

          T signDeltaUDirection = util::scaledSign(deltaU[direction]);

          T c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

          T onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
          T onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

          T rhoPlus = rhoWall/onePlusMinusUPlus;
          T rhoMinus = rhoWall/onePlusMinusUMinus;

          T signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
          T signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

          T c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
                  + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
          T c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
                  + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

          T c2Differential = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

    //      T preFactor = rho/onePlusMinusU;
    //
    //      T c2 = preFactor + 0.5*preFactor*deltaUAbs*deltaUAbs - orientation*rho*deltaU[direction]
    //                + signDeltaUDirection*cs*(- orientation*preFactor*deltaUAbs + rho/deltaUAbs*deltaU[direction]);
    //      for(int counter=0; counter<Lattice<T>::d-1; ++counter)
    //          c2 += -rho/rhoWall*( deltaU[inPlane(counter)]*temp[inPlane(counter)] )
    //                +orientation*( signDeltaUDirection*cs*rho/(deltaUAbs*rhoWall) )*deltaU[inPlane(counter)]*temp[inPlane(counter)];

    #ifdef DEBUG_POSTPROC
//          debugStream << "I(vx): " << c1 << std::endl;
//          debugStream << "d[I(vx)]/d[vx]: " << c2 << std::endl;
    #endif

          T c2 = c2Differential;

    #if defined(DEBUG_POSTPROC)
          debugStream << "I(vx+eps): " << c1Plus << std::endl;
          debugStream << "I(vx-eps): " << c1Minus << std::endl;
          debugStream << "(I(vx+eps) - I(vx-eps))/(2*eps): " << c2Differential << std::endl;
          debugStream << "d(vx): " << relaxationFactor*c1/c2 << std::endl << std::endl;
    #endif

          if(std::abs(c2) < eps)
          {
    #ifdef DEBUG_POSTPROC
              debugStream << "Iterations: " << iter << "   ";
              debugStream << "I(vx): " << c1 << std::endl << std::endl;
    #endif
              // converged = true;
              break;
          }

          T uIncrement = c1/c2;

          if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
          {
              relaxationFactor *=0.5;
    #ifdef DEBUG_POSTPROC
              debugStream << "RelaxationFactor: " << relaxationFactor << std::endl << std::endl;;
    #endif
          }

          uNew[direction] -= relaxationFactor*uIncrement;

          uIncrementOld = uIncrement;

          variance = 0.001*uNew[direction] + eps;
          uNewPlus[direction] = uNew[direction] + variance;
          uNewMinus[direction] = uNew[direction] - variance;

          if(std::abs(uIncrement) < eps)
          {
    #ifdef DEBUG_POSTPROC
              debugStream << "Iterations: " << iter << "    " << "delta U: " << uIncrement << std::endl << std::endl;
    #endif
              // converged = true;
              break;
          }

          if(std::isnan(uNew[direction]))
          {
              isNan = true;
              break;
          }
        } // end for

        T rho = rhoWall/(1+orientation*uNew[direction]);

        for(int counter=0; counter<Lattice<T>::d-1; ++counter)
          uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

        for(int iPop=1; iPop<Lattice<T>::q; ++iPop)
        {
          const bool isNotInDirection = 1+orientation*Lattice<T>::c(iPop,direction);

          if(!isNotInDirection)
          {
              const int iPopOpposite = Lattice<T>::opposite(iPop);
    #ifdef DEBUG_POSTPROC
    //          debugStream << impedanceBoundaryHelpers::f[iPop] << " = " << impedanceBoundaryHelpers::f[iPopOpposite];
    #endif
              cellData[iPop][index] = cellData[iPopOpposite][index];
              for(int axis = 0; axis<Lattice<T>::d; ++axis)
              {
                  cellData[iPop][index] += rho*Lattice<T>::t(iPop)/csSq * (-Lattice<T>::c(iPopOpposite,axis)*uNew[axis]+Lattice<T>::c(iPop,axis)*uNew[axis]);
    #ifdef DEBUG_POSTPROC
    //              debugStream << " + " << Lattice<T>::t(iPop)/csSq << "*" << "("
    //                      << -Lattice<T>::c(iPopOpposite,axis)<< "*u[" << axis << "]+"
    //                      << Lattice<T>::c(iPop,axis) << "*u[" << axis << "])";
    #endif
              }
    #ifdef DEBUG_POSTPROC
    //          debugStream << std::endl << std::endl;
    #endif
              }
          }
    }

      for(int i=0; i<Lattice<T>::q; ++i)
      {
#ifdef DEBUG_POSTPROC
          debugStream << cellData[i][index] << " ";
#endif
        cellData[i][index] -= Lattice<T>::t(i);
      }
#ifdef DEBUG_POSTPROC
      debugStream << std::endl << std::endl;
#endif

      if(isNan or false)
      {
#ifdef DEBUG_POSTPROC
          debugStream << "Not converged" << std::endl;
//          std::cout << debugStream.str();
          std::exit(33);
#endif
      }

//#undef DEBUG_POSTPROC

}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
template<class Dynamics>
void ImpedanceBoundaryCornerProcessor2D<T, Lattice, xNormal, yNormal>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  const T normalVector[3] = {xNormal,yNormal};


  T rhoTmp{0};
  T rho = 1.0;

  for(int i=0; i<Lattice<T>::q; ++i)
  {
      cellData[i][index] += Lattice<T>::t(i);
  }

  for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
  {
      int isParallelToNormal = 0;

      for(unsigned int iDim = 0; iDim<Lattice<T>::d; ++iDim)
      {
          if(Lattice<T>::c(iPop,iDim) == -normalVector[iDim])
              isParallelToNormal += 1;
          else if(Lattice<T>::c(iPop,iDim) == normalVector[iDim])
              isParallelToNormal -= 1;
      }

      if(isParallelToNormal > 0)
      {
          cellData[iPop][index] = cellData[Lattice<T>::opposite(iPop)][index];
          rhoTmp += 2*cellData[iPop][index];
      }
  }


  unsigned int firstTangential = 0;

  for(unsigned int iPop = 1; iPop<Lattice<T>::q; ++iPop)
  {
      unsigned int isTangentialToEdge = 0;

      for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
      {
          if(Lattice<T>::c(iPop,iDim) == -normalVector[iDim])
              isTangentialToEdge += 1;
          else if(Lattice<T>::c(iPop,iDim) == normalVector[iDim])
              isTangentialToEdge += 3;
      }

      if(isTangentialToEdge == 4)
      {
          cellData[iPop][index] = T{1}/T{18}*(rho-rhoTmp);
          if(!firstTangential)
              firstTangential = iPop;
      }
  }
  cellData[0][index] = cellData[firstTangential][index]*T{16};

//  for(unsigned int iPop=0; iPop<Lattice<T>::q; ++iPop)
//      printf("%lf,%i,%lu\n",cellData[iPop][index],iPop,index);

  for(unsigned int iPop=0; iPop<Lattice<T>::q; ++iPop)
  {
      cellData[iPop][index] -= Lattice<T>::t(iPop);
  }

}


template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void PeriodicBoundaryProcessor2D<T, Lattice, direction, orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t oneDimensionalOffset = 1;

    for(unsigned int iDirection = 1; iDirection > direction; --iDirection)
        oneDimensionalOffset *= indexCalcHelper[iDirection];

    oneDimensionalOffset *= indexCalcHelper[direction]-1;

//    std::stringstream debugStream;


//    debugStream << "direction: " << direction << " orientation: " << orientation << std::endl;
//    debugStream << "Index: " << index << " Offset: " << oneDimensionalOffset << std::endl;

//    size_t indices[2];
//    util::getCellIndices2D(index,indexCalcHelper[1],indices);

//    debugStream << indices[0] << ", " << indices[1] << " :: "
//    << util::getCellIndex2D(indices[0],indices[1],indexCalcHelper[1]) << std::endl;

    size_t indexOpposite = index;

    switch(orientation)
    {
        case 1:
            indexOpposite -= oneDimensionalOffset;
            break;
        case -1:
            indexOpposite += oneDimensionalOffset;
            break;
        default:
            printf("Periodic boundary with no orientation detected! Fix your code!\n");
    }

    for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
       if(Lattice<T>::c(iPop,direction) == -orientation)
       {
           cellData[iPop][index] = cellData[iPop][indexOpposite];
//           debugStream << "index: " << indexOpposite << ", " <<impedanceBoundaryHelpers::f[iPop] << std::endl;
       }


//    for(unsigned int iPop=0; iPop<Lattice<T>::q; ++iPop)
//        if(cellData[iPop][index] != 0)
//        {
//            std::cout << debugStream.str();
//            std::cout << impedanceBoundaryHelpers::f[iPop] << " : " << cellData[iPop][index] << " :: " << cellData[iPop][indexOpposite] << std::endl;
//            std::exit(33);
//        }

}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
template<class Dynamics>
void PeriodicBoundaryCornerProcessor2D<T, Lattice, xNormal, yNormal>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t oneDimensionalOffset1 = 1;
    size_t oneDimensionalOffset2 = 1;

    for(unsigned int iDirection = 1; iDirection > 0; --iDirection)
        oneDimensionalOffset1 *= indexCalcHelper[iDirection];

    oneDimensionalOffset1 *= indexCalcHelper[0]-1;
    oneDimensionalOffset2 *= indexCalcHelper[1]-1;

//    std::stringstream debugStream;

//    debugStream << oneDimensionalOffset1 << "," << oneDimensionalOffset2 << std::endl;

    for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
    {
       size_t indexOpposite;
       if(Lattice<T>::c(iPop,0) == -xNormal && Lattice<T>::c(iPop,1) == -yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
//           debugStream << "index: " << indexOpposite << ", " << impedanceBoundaryHelpers::f[iPop] << std::endl;
       }
       else if(Lattice<T>::c(iPop,0) == -xNormal && Lattice<T>::c(iPop,1) == yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
//           debugStream << "index: " << indexOpposite << ", " << impedanceBoundaryHelpers::f[iPop] << std::endl;
       }
       else if(Lattice<T>::c(iPop,0) == xNormal && Lattice<T>::c(iPop,1) == -yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
//           debugStream << "index: " << indexOpposite << ", " << impedanceBoundaryHelpers::f[iPop] << std::endl;
       }
       else if(Lattice<T>::c(iPop,0) == -xNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
//           debugStream << "index: " << indexOpposite << ", " << impedanceBoundaryHelpers::f[iPop] << std::endl;
       }
       else if(Lattice<T>::c(iPop,1) == -yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
//           debugStream << "index: " << indexOpposite << ", " << impedanceBoundaryHelpers::f[iPop] << std::endl;
       }
    }

//    for(unsigned int iPop=0; iPop<Lattice<T>::q; ++iPop)
//        if(cellData[iPop][index] != 0)
//        {
//            std::cout << debugStream.str();
//            std::cout << impedanceBoundaryHelpers::f[iPop] << " : " << cellData[iPop][index] << std::endl;
//            std::exit(33);
//        }

}

}  // namespace olb

#endif
