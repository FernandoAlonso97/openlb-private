/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  Generic collision, which modifies the particle distribution
 *  functions, implemented by Orestis Malaspinas, 2007
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef BOUNDARY_POST_PROCESSORS_3D_H
#define BOUNDARY_POST_PROCESSORS_3D_H

#include "core/postProcessing.h"
#include "momentaOnBoundaries.h"
#include "core/blockLattice3D.h"

namespace olb {

/**
* This class computes the skordos BC
* on a plane wall in 3D but with a limited number of terms added to the
* equilibrium distributions (i.e. only the Q_i : Pi term)
*/
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class PlaneFdBoundaryProcessor3D : public LocalPostProcessor3D<T,Lattice> {
public:
  static const int numberDataEntries = 0;

  PlaneFdBoundaryProcessor3D (int x0_, int x1_, int y0_, int y1_, int z0_, int z1_);
  int extent() const override
  {
    return 1;
  }
  int extent(int whichDirection) const override
  {
    return 1;
  }
  void process(BlockLattice3D<T,Lattice>& blockLattice) override;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  void processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_, int z0_, int z1_ ) override;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

private:
  template<int deriveDirection>
  void interpolateGradients (
    BlockLattice3D<T,Lattice> const& blockLattice,
    T velDeriv[Lattice<T>::d], int iX, int iY, int iZ ) const;

  template<int deriveDirection>
  OPENLB_HOST_DEVICE
  static void interpolateGradients(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t index,
          size_t ny, size_t nz, T velDeriv[Lattice<T>::d]);
private:
  int x0, x1, y0, y1, z0, z1;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class PlaneFdBoundaryProcessorGenerator3D : public PostProcessorGenerator3D<T,Lattice> {
public:
  typedef PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation> PostProcessorType;
  PlaneFdBoundaryProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_);
  PostProcessor3D<T,Lattice>* generate() const override;
  PostProcessorGenerator3D<T,Lattice>*  clone() const override;
};


/**
* This class computes a convection BC on a flat wall in 2D
*/
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class StraightConvectionBoundaryProcessor3D : public LocalPostProcessor3D<T,Lattice> {
public:
  StraightConvectionBoundaryProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, T* uAv_ = NULL);
  ~StraightConvectionBoundaryProcessor3D() override;
  int extent() const override
  {
    return 1;
  }
  int extent(int whichDirection) const override
  {
    return 1;
  }
  void process(BlockLattice3D<T,Lattice>& blockLattice) override;
  void processSubDomain ( BlockLattice3D<T,Lattice>& blockLattice,
                                  int x0_, int x1_, int y0_, int y1_ , int z0_, int z1_) override;
private:
  int x0, x1, y0, y1, z0, z1;
  T**** saveCell;
  T* uAv;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class StraightConvectionBoundaryProcessorGenerator3D : public PostProcessorGenerator3D<T,Lattice> {
public:
  typedef StraightConvectionBoundaryProcessor3D<T,Lattice,direction,orientation> PostProcessorType;
  StraightConvectionBoundaryProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, T* uAv_ = NULL);
  PostProcessor3D<T,Lattice>* generate() const override;
  PostProcessorGenerator3D<T,Lattice>*  clone() const override;
private:
  T* uAv;
};

/**
* This class computes the skordos BC
* on a convex edge wall in 3D but with a limited number of terms added to the
* equilibrium distributions (i.e. only the Q_i : Pi term)
*/
template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2>
class OuterVelocityEdgeProcessor3D : public LocalPostProcessor3D<T,Lattice> {
public:
  enum { direction1 = (plane+1)%3, direction2 = (plane+2)%3 };
public:
  static const int numberDataEntries = 0;

  OuterVelocityEdgeProcessor3D (
    int x0_, int x1_, int y0_, int y1_, int z0_, int z1_ );
  int extent() const override
  {
    return 2;
  }
  int extent(int whichDirection) const override
  {
    return 2;
  }
  void process(BlockLattice3D<T,Lattice>& blockLattice) override;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  void processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_,
                                int z0_, int z1_ ) override;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          size_t index, size_t indexCalcHelper[Lattice<T>::d]);

private:
  T getNeighborRho(int x, int y, int z, int step1, int step2,
                   BlockLattice3D<T,Lattice> const& blockLattice);
  OPENLB_HOST_DEVICE
  static T getNeighborRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t index,
          int step1, int step2, size_t ny, size_t nz);
  template<int deriveDirection, int orientation>
  void interpolateGradients (
    BlockLattice3D<T,Lattice> const& blockLattice,
    T velDeriv[Lattice<T>::d], int iX, int iY, int iZ ) const;
  template<int deriveDirection, int orientation>
  OPENLB_HOST_DEVICE
  static void interpolateGradients (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t index, size_t ny, size_t nz, T velDeriv[Lattice<T>::d]);
private:
  int x0, x1, y0, y1, z0, z1;
};

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2>
class OuterVelocityEdgeProcessorGenerator3D
  : public PostProcessorGenerator3D<T,Lattice> {
public:
  typedef OuterVelocityEdgeProcessor3D<T,Lattice,plane,normal1,normal2> PostProcessorType;
  OuterVelocityEdgeProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_,
                                        int z0_, int z1_);
  PostProcessor3D<T,Lattice>* generate() const override;
  PostProcessorGenerator3D<T,Lattice>*  clone() const override;
};


template<typename T, template<typename U> class Lattice,
         int xNormal, int yNormal, int zNormal>
class OuterVelocityCornerProcessor3D : public LocalPostProcessor3D<T,Lattice> {
public:

  static const int numberDataEntries = 0;

  OuterVelocityCornerProcessor3D(int x_, int y_, int z_);
  int extent() const override
  {
    return 2;
  }
  int extent(int whichDirection) const override
  {
    return 2;
  }
  void process(BlockLattice3D<T,Lattice>& blockLattice) override;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  void processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_,
                                int z0_, int z1_ ) override;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          size_t index, size_t indexCalcHelper[Lattice<T>::d]);
private:
  int x,y,z;
};

template<typename T, template<typename U> class Lattice,
         int xNormal, int yNormal, int zNormal>
class OuterVelocityCornerProcessorGenerator3D
  : public PostProcessorGenerator3D<T,Lattice> {
public:
  typedef OuterVelocityCornerProcessor3D<T,Lattice,xNormal,yNormal,zNormal> PostProcessorType;
  OuterVelocityCornerProcessorGenerator3D(int x_, int y_, int z_);
  PostProcessor3D<T,Lattice>* generate() const override;
  PostProcessorGenerator3D<T,Lattice>*  clone() const override;
};

/**
* This class computes a slip BC in 3D
*/

template<typename T, template<typename U> class Lattice,int discreteNormalX,int discreteNormalY, int discreteNormalZ>
class SlipBoundaryProcessor3D {
public:
  static const int numberDataEntries = 0;

  SlipBoundaryProcessor3D() = delete;
  

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);
};

template<typename T, template<typename U> class Lattice>
class CurvedSlipBoundaryProcessor3D {
public:
  static constexpr int constData   = 2+Lattice<T>::d;
  static constexpr int sizeDataSet = 2;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  CurvedSlipBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxNumNeighbour() { return 0; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxTau()          { return 1; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxNormal()       { return 2; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDir(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDelta(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData+1; };

};

template<typename T, template<typename U> class Lattice>
class CurvedBouzidiBoundaryProcessor3D {
public:
  static constexpr int constData   = 2+Lattice<T>::d;
  static constexpr int sizeDataSet = 2;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  CurvedBouzidiBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxNumNeighbour() { return 0; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxTau()          { return 1; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxNormal()       { return 2; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDir(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDelta(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData+1; };

};

template<typename T, template<typename U> class Lattice>
class CurvedBouzidiQuadraticBoundaryProcessor3D {
public:
  static constexpr int constData   = 2+Lattice<T>::d;
  static constexpr int sizeDataSet = 2;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  CurvedBouzidiQuadraticBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxNumNeighbour() { return 0; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxTau()          { return 1; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxNormal()       { return 2; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDir(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDelta(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData+1; };

};


template<typename T, template<typename U> class Lattice>
class CurvedYuBoundaryProcessor3D {
public:
  static constexpr int constData   = 2+Lattice<T>::d;
  static constexpr int sizeDataSet = 2;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  CurvedYuBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxNumNeighbour() { return 0; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxTau()          { return 1; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxNormal()       { return 2; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDir(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDelta(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData+1; };

};



template<typename T, template<typename U> class Lattice>
class CurvedWallBoundaryProcessor3D {
public:
  static constexpr int constData   = 2+Lattice<T>::d;
  static constexpr int sizeDataSet = 2;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  CurvedWallBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxNumNeighbour() { return 0; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxTau()          { return 1; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxVel() { return 2; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDir(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDelta(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData+1; };

};

///isabel's simpler curved bc
template<typename T, template<typename U> class Lattice>
class BounceBackMovingBoundaryBoundaryProcessor3D {
public:
  static constexpr int constData   = 2+Lattice<T>::d;
  static constexpr int sizeDataSet = 2;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  BounceBackMovingBoundaryBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxNumNeighbour() { return 0; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxTau()          { return 1; };
  OPENLB_HOST_DEVICE
  static constexpr size_t idxNormal()       { return 2; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDir(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDelta(unsigned int const iNeighbour) { return iNeighbour*(sizeDataSet)+constData+1; };
};

template<typename T, template<typename U> class Lattice>
class GradBoundaryProcessor3D {
public: 
  static constexpr int constData   = 1+Lattice<T>::d+1+Lattice<T>::d+Lattice<T>::d; //omega, wall normal, wall normal dist, finite-diff directions, u wall
  static constexpr int sizeDataSet = 1;
  static constexpr int numberDataEntries = Lattice<T>::q*sizeDataSet+constData;

  GradBoundaryProcessor3D() = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static constexpr size_t idxOmega() { return 0; };  

  OPENLB_HOST_DEVICE
  static constexpr size_t idxWNDirs() { return 1; };  

  OPENLB_HOST_DEVICE
  static constexpr size_t idxWNDist() { return 4; };  

  OPENLB_HOST_DEVICE
  static constexpr size_t idxFDDirs() { return 2+Lattice<T>::d; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxWallVel() { return 2+2*Lattice<T>::d; };

  OPENLB_HOST_DEVICE
  static constexpr size_t idxDirs() { return 2+3*Lattice<T>::d; };

};

// template<typename T, template<typename U> class Lattice>
// class SlipBoundaryProcessorGenerator3D : public PostProcessorGenerator3D<T,Lattice> {
// public:
  // typedef SlipBoundaryProcessor3D<T,Lattice> PostProcessorType;
  // SlipBoundaryProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, int discreteNormalX_, int discreteNormalY_, int discreteNormalZ_);
  // PostProcessor3D<T,Lattice>* generate() const override;
  // PostProcessorGenerator3D<T,Lattice>*  clone() const override;
// private:
  // int discreteNormalX;
  // int discreteNormalY;
  // int discreteNormalZ;
// };
//

template <typename T, template<typename U> class Lattice, int direction, int orientation>
class RegularizedBoundaryProcessor3D {
public:
  static const int numberDataEntries = 0;

  RegularizedBoundaryProcessor3D() = delete;
  
  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};

typedef double ImpedanceFloat; 

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ImpedanceBoundaryProcessor3D {
public:

  static const int numberDataEntries = 0;

  ImpedanceBoundaryProcessor3D () = delete;

  OPENLB_HOST_DEVICE
  static constexpr int inPlane(int counter)
  {
      return (direction+1+counter)%3;
  }

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
  static constexpr unsigned int iterMax = 50;
  static constexpr T eps = 0.000001;
  static constexpr T inPlaneRelaxation = 0.99;
  static constexpr T varianceFactor = 0.001;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryProcessor3D<T,Lattice,0,-1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 0;
    static constexpr signed   int orientation = -1;
    ImpedanceBoundaryProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (0+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
    static constexpr T varianceFactor = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::varianceFactor;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryProcessor3D<T,Lattice,0,1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 0;
    static constexpr signed   int orientation = 1;
    ImpedanceBoundaryProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (0+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
    static constexpr T varianceFactor = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::varianceFactor;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryProcessor3D<T,Lattice,1,-1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 1;
    static constexpr signed   int orientation = -1;
    ImpedanceBoundaryProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (1+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
    static constexpr T varianceFactor = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::varianceFactor;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryProcessor3D<T,Lattice,1,1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 1;
    static constexpr signed   int orientation = 1;
    ImpedanceBoundaryProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (1+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
    static constexpr T varianceFactor = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::varianceFactor;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryProcessor3D<T,Lattice,2,-1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 2;
    static constexpr signed   int orientation = -1;
    ImpedanceBoundaryProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (2+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
    static constexpr T varianceFactor = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::varianceFactor;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryProcessor3D<T,Lattice,2,1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 2;
    static constexpr signed   int orientation = 1;
    ImpedanceBoundaryProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (2+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
    static constexpr T varianceFactor = ImpedanceBoundaryProcessor3D<T,Lattice,0,0>::varianceFactor;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ImpedanceBoundaryFixedRefProcessor3D {
public:
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

  OPENLB_HOST_DEVICE
  static constexpr int inPlane(int counter)
  {
      return (direction+1+counter)%3;
  }

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
  static const int numberDataEntries = 0;
  static constexpr unsigned int iterMax = 1;
  static constexpr T eps = 0.000001;
  static constexpr T inPlaneRelaxation = 0.99;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,-1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 0;
    static constexpr signed   int orientation = -1;
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (0+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 0;
    static constexpr signed   int orientation = 1;
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (0+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,1,-1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 1;
    static constexpr signed   int orientation = -1;
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (1+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,1,1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 1;
    static constexpr signed   int orientation = 1;
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (1+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,2,-1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 2;
    static constexpr signed   int orientation = -1;
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (2+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
};

template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,2,1> {
public:
    static const int numberDataEntries = 0;
    static constexpr unsigned int direction = 2;
    static constexpr signed   int orientation = 1;
    ImpedanceBoundaryFixedRefProcessor3D () = delete;

    OPENLB_HOST_DEVICE
    static constexpr int inPlane(int counter)
    {
        return (2+1+counter)%3;
    }

    template<class Dynamics>
    OPENLB_HOST_DEVICE
    static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

public:
    static constexpr unsigned int iterMax = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::iterMax;
    static constexpr T eps = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::eps;
    static constexpr T inPlaneRelaxation = ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,0>::inPlaneRelaxation;
};

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
class ImpedanceBoundaryEdgeProcessor3D {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (plane+1)%3, direction2 = (plane+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};

//////////////SPECIALISATIONS/////////////////////////////////////////////
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,-1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (0+1)%3, direction2 = (0+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,-1,1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (0+1)%3, direction2 = (0+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,1,1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (0+1)%3, direction2 = (0+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,0,1,-1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (0+1)%3, direction2 = (0+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,-1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (1+1)%3, direction2 = (1+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,-1,1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (1+1)%3, direction2 = (1+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,1,1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (1+1)%3, direction2 = (1+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,1,1,-1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (1+1)%3, direction2 = (1+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,-1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (2+1)%3, direction2 = (2+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,-1,1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (2+1)%3, direction2 = (2+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,1,1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (2+1)%3, direction2 = (2+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
template<typename T, template<typename U> class Lattice>
class ImpedanceBoundaryEdgeProcessor3D<T,Lattice,2,1,-1> {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryEdgeProcessor3D () = delete;

    enum { direction1 = (2+1)%3, direction2 = (2+2)%3 };
    enum {parallelToNormal = 1, perpendicularToNormal = 2, tangentialToBoundary = 3, parallelToEdgeAxis = 4};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};
///////////////////////////////////////////////////////////////////////
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
class ImpedanceBoundaryCornerProcessor3D {
public:
    static const int numberDataEntries = 0;
  ImpedanceBoundaryCornerProcessor3D () = delete;

  enum{parallelToNormal = 5, tangentialToEdge = 6};

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ImpedanceBoundaryProcessor3DIncompressible {
public:
    static const int numberDataEntries = 0;
    ImpedanceBoundaryProcessor3DIncompressible () = delete;

  OPENLB_HOST_DEVICE
  static constexpr int inPlane(int counter)
  {
      return (direction+1+counter)%3;
  }

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

private:
  static constexpr unsigned int iterMax = 50;
  static constexpr double eps = 0.000001;
  static constexpr double inPlaneRelaxation = 0.98;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class PeriodicBoundaryProcessor3D {
public:
    static const int numberDataEntries = 0;
    PeriodicBoundaryProcessor3D () = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
class PeriodicBoundaryEdgeProcessor3D {
public:
    static const int numberDataEntries = 0;
  PeriodicBoundaryEdgeProcessor3D () = delete;

  enum { direction1 = (plane+1)%3, direction2 = (plane+2)%3 };

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
              T const * const OPENLB_RESTRICT collisionData,
              T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
              size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
class PeriodicBoundaryCornerProcessor3D {
public:
    static const int numberDataEntries = 0;
    PeriodicBoundaryCornerProcessor3D () = delete;

  template<class Dynamics>
  OPENLB_HOST_DEVICE
  static void process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]);

};

}

#endif
