/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef BOUNDARY_POST_PROCESSORS_3D_HH
#define BOUNDARY_POST_PROCESSORS_3D_HH

#include "boundaryPostProcessors3D.h"
#include "core/finiteDifference3D.h"
#include "core/blockLattice3D.h"
#include "dynamics/firstOrderLbHelpers.h"
#include "core/util.h"
#include "core/metaTemplateHelpers.h"
#include "utilities/vectorHelpers.h"

namespace olb {

////////  PlaneFdBoundaryProcessor3D ///////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::
PlaneFdBoundaryProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_)
{
  OLB_PRECONDITION(x0==x1 || y0==y1 || z0==z1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  using namespace olb::util::tensorIndices3D;

  int newX0, newX1, newY0, newY1, newZ0, newZ1;
  if ( util::intersect (
         x0, x1, y0, y1, z0, z1,
         x0_, x1_, y0_, y1_, z0_, z1_,
         newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {
    int iX;

#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d], dz_u[Lattice<T>::d];
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          CellView<T,Lattice> cell = blockLattice.get(iX,iY,iZ);
          Dynamics<T,Lattice>* dynamics = blockLattice.getDynamics(iX, iY, iZ);
          T rho, u[Lattice<T>::d];
          cell.computeRhoU(rho,u);

          interpolateGradients<0> ( blockLattice, dx_u, iX, iY, iZ );
          interpolateGradients<1> ( blockLattice, dy_u, iX, iY, iZ );
          interpolateGradients<2> ( blockLattice, dz_u, iX, iY, iZ );

          T dx_ux = dx_u[0];
          T dy_ux = dy_u[0];
          T dz_ux = dz_u[0];
          T dx_uy = dx_u[1];
          T dy_uy = dy_u[1];
          T dz_uy = dz_u[1];
          T dx_uz = dx_u[2];
          T dy_uz = dy_u[2];
          T dz_uz = dz_u[2];
          T omega = dynamics->getOmega();
          T sToPi = - rho / Lattice<T>::invCs2() / omega;
          T pi[util::TensorVal<Lattice<T> >::n];
          pi[xx] = (T)2 * dx_ux * sToPi;
          pi[yy] = (T)2 * dy_uy * sToPi;
          pi[zz] = (T)2 * dz_uz * sToPi;
          pi[xy] = (dx_uy + dy_ux) * sToPi;
          pi[xz] = (dx_uz + dz_ux) * sToPi;
          pi[yz] = (dy_uz + dz_uy) * sToPi;

          // Computation of the particle distribution functions
          // according to the regularized formula
          T uSqr = util::normSqr<T,Lattice<T>::d>(u);
          for (int iPop = 0; iPop < Lattice<T>::q; ++iPop)
            cell[iPop] = dynamics -> computeEquilibrium(iPop,rho,u,uSqr) +
                         firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::
processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    using namespace olb::util::tensorIndices3D;
    size_t ny = indexCalcHelper[1];
    size_t nz = indexCalcHelper[2];

    T u[Lattice<T>::d];
    util::getU<T,Lattice>(cellData,index,u);
    // Dynamics::MomentaType::computeU(cellData,index,momentaData,position,u);
    // for (unsigned i=0;i<Lattice<T>::d;++i)
      // cellData[Lattice<T>::uIndex+i][index] = u[i];

    T rho = util::getRho<T,Lattice>(cellData,index);
    // T rho = Dynamics::MomentaType::computeRho(cellData,index,momentaData,position);
      // cellData[Lattice<T>::rhoIndex][index] = rho;

    T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d], dz_u[Lattice<T>::d];

    interpolateGradients<0> ( cellData,index,ny,nz, dx_u);
    interpolateGradients<1> ( cellData,index,ny,nz, dy_u);
    interpolateGradients<2> ( cellData,index,ny,nz, dz_u);

    T dx_ux = dx_u[0];
    T dy_ux = dy_u[0];
    T dz_ux = dz_u[0];
    T dx_uy = dx_u[1];
    T dy_uy = dy_u[1];
    T dz_uy = dz_u[1];
    T dx_uz = dx_u[2];
    T dy_uz = dy_u[2];
    T dz_uz = dz_u[2];
    T omega = collisionData[Dynamics::omegaIndex];
    T sToPi = - rho / Lattice<T>::invCs2() / omega;
    T pi[util::TensorVal<Lattice<T> >::n];
    pi[xx] = (T)2 * dx_ux * sToPi;
    pi[yy] = (T)2 * dy_uy * sToPi;
    pi[zz] = (T)2 * dz_uz * sToPi;
    pi[xy] = (dx_uy + dy_ux) * sToPi;
    pi[xz] = (dx_uz + dz_ux) * sToPi;
    pi[yz] = (dy_uz + dz_uy) * sToPi;

    // Computation of the particle distribution functions
    // according to the regularized formula
    T uSqr = util::normSqr<T,Lattice<T>::d>(u);
    for (int iPop = 0; iPop < Lattice<T>::q; ++iPop)
      cellData[iPop][index] = Dynamics::computeEquilibriumStatic(iPop,rho,u,uSqr) +
                   firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
 
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    processSubDomain<Dynamics>(cellData, momentaData, collisionData, index, position, indexCalcHelper);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice3D<T,Lattice> const& blockLattice, T velDeriv[Lattice<T>::d],
                     int iX, int iY, int iZ) const
{
  fd::DirectedGradients3D<T, Lattice, direction, orientation, deriveDirection, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY, iZ);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void PlaneFdBoundaryProcessor3D<T,Lattice,direction,orientation>::
interpolateGradients(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t index,
        size_t ny, size_t nz, T velDeriv[Lattice<T>::d])
{
  fd::DirectedGradients3D<T, Lattice, direction, orientation, deriveDirection, direction==deriveDirection>::
  interpolateVector(cellData,index,ny,nz,velDeriv);
}


////////  PlaneFdBoundaryProcessorGenerator3D ///////////////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PlaneFdBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>::
PlaneFdBoundaryProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
  : PostProcessorGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor3D<T,Lattice>* PlaneFdBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>::
generate() const
{
  return new PlaneFdBoundaryProcessor3D<T,Lattice, direction,orientation>
         ( this->x0, this->x1, this->y0, this->y1, this->z0, this->z1 );
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessorGenerator3D<T,Lattice>*
PlaneFdBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>::clone() const
{
  return new PlaneFdBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1);
}


////////  StraightConvectionBoundaryProcessorGenerator3D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation>
StraightConvectionBoundaryProcessor3D<T,Lattice,direction,orientation>::
StraightConvectionBoundaryProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, T* uAv_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_), uAv(uAv_)
{
  OLB_PRECONDITION(x0==x1 || y0==y1 || z0==z1);

  saveCell = new T*** [(size_t)(x1_-x0_+1)];
  for (int iX=0; iX<=x1_-x0_; ++iX) {
    saveCell[iX] = new T** [(size_t)(y1_-y0_+1)];
    for (int iY=0; iY<=y1_-y0_; ++iY) {
      saveCell[iX][iY] = new T* [(size_t)(z1_-z0_+1)];
      for (int iZ=0; iZ<=z1_-z0_; ++iZ) {
        saveCell[iX][iY][iZ] = new T [(size_t)(Lattice<T>::q)];
        for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
          // default set to -1 in order to avoid wrong results at first call
          saveCell[iX][iY][iZ][iPop] = T(-1);
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
StraightConvectionBoundaryProcessor3D<T,Lattice,direction,orientation>::
~StraightConvectionBoundaryProcessor3D()
{
  for (int iX=0; iX<=x1-x0; ++iX) {
    for (int iY=0; iY<=y1-y0; ++iY) {
      for (int iZ=0; iZ<=z1-z0; ++iZ) {
        delete [] saveCell[iX][iY][iZ];
      }
      delete [] saveCell[iX][iY];
    }
    delete [] saveCell[iX];
  }
  delete [] saveCell;
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void StraightConvectionBoundaryProcessor3D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  int newX0, newX1, newY0, newY1, newZ0, newZ1;
  if ( util::intersect (
         x0, x1, y0, y1, z0, z1,
         x0_, x1_, y0_, y1_, z0_, z1_,
         newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {

    int iX;
#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          CellView<T,Lattice> cell = blockLattice.get(iX,iY,iZ);
          for (int iPop = 0; iPop < Lattice<T>::q ; ++iPop) {
            if (Lattice<T>::c(iPop,direction)==-orientation) {
              // using default -1 avoids wrong first call
              if (!util::nearZero(1 + saveCell[iX-newX0][iY-newY0][iZ-newZ0][iPop]) ) {
                cell[iPop] = saveCell[iX-newX0][iY-newY0][iZ-newZ0][iPop];
              }
            }
          }

          T rho0, u0[3];
          T rho1, u1[3];
          T rho2, u2[3];
          if (direction==0) {
            blockLattice.get(iX,iY,iZ).computeRhoU(rho0,u0);
            blockLattice.get(iX-orientation,iY,iZ).computeRhoU(rho1,u1);
            blockLattice.get(iX-orientation*2,iY,iZ).computeRhoU(rho2,u2);
          } else if (direction==1) {
            blockLattice.get(iX,iY,iZ).computeRhoU(rho0,u0);
            blockLattice.get(iX,iY-orientation,iZ).computeRhoU(rho1,u1);
            blockLattice.get(iX,iY-orientation*2,iZ).computeRhoU(rho2,u2);
          } else {
            blockLattice.get(iX,iY,iZ).computeRhoU(rho0,u0);
            blockLattice.get(iX,iY,iZ-orientation).computeRhoU(rho1,u1);
            blockLattice.get(iX,iY,iZ-orientation*2).computeRhoU(rho2,u2);
          }

          // rho0 = T(1); rho1 = T(1); rho2 = T(1);

          T uDelta[3];
          T uAverage = rho0*u0[direction];
          if (uAv!=nullptr) {
            uAverage = *uAv * rho0;
          }
          uDelta[0]=-uAverage*0.5*(3*rho0*u0[0]-4*rho1*u1[0]+rho2*u2[0]);
          uDelta[1]=-uAverage*0.5*(3*rho0*u0[1]-4*rho1*u1[1]+rho2*u2[1]);
          uDelta[2]=-uAverage*0.5*(3*rho0*u0[2]-4*rho1*u1[2]+rho2*u2[2]);

          for (int iPop = 0; iPop < Lattice<T>::q ; ++iPop) {
            if (Lattice<T>::c(iPop,direction) == -orientation) {
              saveCell[iX-newX0][iY-newY0][iZ-newZ0][iPop] = cell[iPop] + Lattice<T>::invCs2()*Lattice<T>::t(iPop)*(uDelta[0]*Lattice<T>::c(iPop,0)+uDelta[1]*Lattice<T>::c(iPop,1)+uDelta[2]*Lattice<T>::c(iPop,2));
            }
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void StraightConvectionBoundaryProcessor3D<T,Lattice,direction,orientation>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
}


////////  StraightConvectionBoundaryProcessorGenerator2D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction,int orientation>
StraightConvectionBoundaryProcessorGenerator3D<T,Lattice, direction,orientation>::
StraightConvectionBoundaryProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, T* uAv_)
  : PostProcessorGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_), uAv(uAv_)
{ }

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor3D<T,Lattice>*
StraightConvectionBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>::generate() const
{
  return new StraightConvectionBoundaryProcessor3D<T,Lattice,direction,orientation>
         ( this->x0, this->x1, this->y0, this->y1, this->z0, this->z1, uAv);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessorGenerator3D<T,Lattice>*
StraightConvectionBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>::clone() const
{
  return new StraightConvectionBoundaryProcessorGenerator3D<T,Lattice,direction,orientation>
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1, uAv);
}


////////  OuterVelocityEdgeProcessor3D ///////////////////////////////////

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
OuterVelocityEdgeProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_)
{
  OLB_PRECONDITION (
    (plane==2 && x0==x1 && y0==y1) ||
    (plane==1 && x0==x1 && z0==z1) ||
    (plane==0 && y0==y1 && z0==z1)     );

}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
void OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  using namespace olb::util::tensorIndices3D;

  int newX0, newX1, newY0, newY1, newZ0, newZ1;
  if ( util::intersect ( x0, x1, y0, y1, z0, z1,
                         x0_, x1_, y0_, y1_, z0_, z1_,
                         newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {
    int iX;

#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          CellView<T,Lattice> cell = blockLattice.get(iX,iY,iZ);
          Dynamics<T,Lattice>* dynamics = blockLattice.getDynamics(iX, iY, iZ);

          T rho10 = getNeighborRho(iX,iY,iZ,1,0, blockLattice);
          T rho01 = getNeighborRho(iX,iY,iZ,0,1, blockLattice);
          T rho20 = getNeighborRho(iX,iY,iZ,2,0, blockLattice);
          T rho02 = getNeighborRho(iX,iY,iZ,0,2, blockLattice);
          T rho = (T)2/(T)3*(rho01+rho10)-(T)1/(T)6*(rho02+rho20);

          T dA_uB_[3][3];
          interpolateGradients<plane,0>            ( blockLattice, dA_uB_[0], iX, iY, iZ );
          interpolateGradients<direction1,normal1> ( blockLattice, dA_uB_[1], iX, iY, iZ );
          interpolateGradients<direction2,normal2> ( blockLattice, dA_uB_[2], iX, iY, iZ );
          T dA_uB[3][3];
          for (int iBeta=0; iBeta<3; ++iBeta) {
            dA_uB[plane][iBeta]      = dA_uB_[0][iBeta];
            dA_uB[direction1][iBeta] = dA_uB_[1][iBeta];
            dA_uB[direction2][iBeta] = dA_uB_[2][iBeta];
          }
          T omega = dynamics -> getOmega();
          T sToPi = - rho / Lattice<T>::invCs2() / omega;
          T pi[util::TensorVal<Lattice<T> >::n];
          pi[xx] = (T)2 * dA_uB[0][0] * sToPi;
          pi[yy] = (T)2 * dA_uB[1][1] * sToPi;
          pi[zz] = (T)2 * dA_uB[2][2] * sToPi;
          pi[xy] = (dA_uB[0][1]+dA_uB[1][0]) * sToPi;
          pi[xz] = (dA_uB[0][2]+dA_uB[2][0]) * sToPi;
          pi[yz] = (dA_uB[1][2]+dA_uB[2][1]) * sToPi;

          // Computation of the particle distribution functions
          // according to the regularized formula
          T u[Lattice<T>::d];
          cell.computeU(u);
          T uSqr = util::normSqr<T,Lattice<T>::d>(u);

          for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            cell[iPop] = dynamics -> computeEquilibrium(iPop,rho,u,uSqr) +
                         firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
template<class Dynamics>
void OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        size_t index, size_t indexCalcHelper[Lattice<T>::d])
{
  using namespace olb::util::tensorIndices3D;

  size_t ny = indexCalcHelper[1];
  size_t nz = indexCalcHelper[2];

  T rho10 = getNeighborRho(cellData,index,1,0,ny,nz);
  T rho01 = getNeighborRho(cellData,index,0,1,ny,nz);
  T rho20 = getNeighborRho(cellData,index,2,0,ny,nz);
  T rho02 = getNeighborRho(cellData,index,0,2,ny,nz);
  T rho = (T)2/(T)3*(rho01+rho10)-(T)1/(T)6*(rho02+rho20);

  T dA_uB_[3][3];
  interpolateGradients<plane,0>            ( cellData,index,ny,nz, dA_uB_[0]);
  interpolateGradients<direction1,normal1> ( cellData,index,ny,nz, dA_uB_[1]);
  interpolateGradients<direction2,normal2> ( cellData,index,ny,nz, dA_uB_[2]);

  T dA_uB[3][3];
  for (int iBeta=0; iBeta<3; ++iBeta) {
    dA_uB[plane][iBeta]      = dA_uB_[0][iBeta];
    dA_uB[direction1][iBeta] = dA_uB_[1][iBeta];
    dA_uB[direction2][iBeta] = dA_uB_[2][iBeta];
  }

  T omega = collisionData[Dynamics::omegaIndex];
  T sToPi = - rho / Lattice<T>::invCs2() / omega;
  T pi[util::TensorVal<Lattice<T> >::n];
  pi[xx] = (T)2 * dA_uB[0][0] * sToPi;
  pi[yy] = (T)2 * dA_uB[1][1] * sToPi;
  pi[zz] = (T)2 * dA_uB[2][2] * sToPi;
  pi[xy] = (dA_uB[0][1]+dA_uB[1][0]) * sToPi;
  pi[xz] = (dA_uB[0][2]+dA_uB[2][0]) * sToPi;
  pi[yz] = (dA_uB[1][2]+dA_uB[2][1]) * sToPi;

  // Computation of the particle distribution functions
  // according to the regularized formula
  T u[Lattice<T>::d];

  util::getU<T,Lattice>(cellData,index,u);

  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop][index] = Dynamics::computeEquilibriumStatic(iPop,rho,u,uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
void OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
template<class Dynamics>
void OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  processSubDomain<Dynamics>(cellData, momentaData, collisionData, index, indexCalcHelper);
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
T OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
getNeighborRho(int x, int y, int z, int step1, int step2, BlockLattice3D<T,Lattice> const& blockLattice)
{
  int coords[3] = {x, y, z};
  coords[direction1] += -normal1*step1;
  coords[direction2] += -normal2*step2;
  return blockLattice.get(coords[0], coords[1], coords[2]).computeRho();
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
T OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
getNeighborRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t index,
        int step1, int step2, size_t ny, size_t nz)
{
  int steps[Lattice<T>::d];
  steps[plane] = 0;
  steps[direction1] = -normal1*step1;
  steps[direction2] = -normal2*step2;

  size_t index12 = index + steps[2] + steps[1]*nz + steps[0]*nz*ny;
  return util::getRho<T,Lattice>(cellData,index12);
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
template<int deriveDirection, int orientation>
void OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
interpolateGradients(BlockLattice3D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d],
                     int iX, int iY, int iZ) const
{
  fd::DirectedGradients3D<T,Lattice,deriveDirection,orientation,deriveDirection,deriveDirection!=plane>::
  interpolateVector(velDeriv, blockLattice, iX, iY, iZ);
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
template<int deriveDirection, int orientation>
void OuterVelocityEdgeProcessor3D<T,Lattice, plane,normal1,normal2>::
interpolateGradients (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t index, size_t ny, size_t nz, T velDeriv[Lattice<T>::d])
{
    fd::DirectedGradients3D<T,Lattice,deriveDirection,orientation,deriveDirection,deriveDirection!=plane>::
    interpolateVector(cellData,index,ny,nz,velDeriv);
}

////////  OuterVelocityEdgeProcessorGenerator3D ///////////////////////////////

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>::
OuterVelocityEdgeProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
  : PostProcessorGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_)
{ }

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
PostProcessor3D<T,Lattice>*
OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>::
generate() const
{
  return new OuterVelocityEdgeProcessor3D < T,Lattice, plane,normal1,normal2 >
         ( this->x0, this->x1, this->y0, this->y1, this->z0, this->z1);
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
PostProcessorGenerator3D<T,Lattice>*
OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2>::clone() const
{
  return new OuterVelocityEdgeProcessorGenerator3D<T,Lattice, plane,normal1,normal2 >
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1);
}

/////////// OuterVelocityCornerProcessor3D /////////////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
OuterVelocityCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
OuterVelocityCornerProcessor3D ( int x_, int y_, int z_ )
  : x(x_), y(y_), z(z_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
void OuterVelocityCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  using namespace olb::util::tensorIndices3D;
  CellView<T,Lattice> cell = blockLattice.get(x,y,z);
  Dynamics<T,Lattice>* dynamics = blockLattice.getDynamics(x, y, z);

  T rho100 = blockLattice.get(x - 1*xNormal, y - 0*yNormal, z - 0*zNormal).computeRho();
  T rho010 = blockLattice.get(x - 0*xNormal, y - 1*yNormal, z - 0*zNormal).computeRho();
  T rho001 = blockLattice.get(x - 0*xNormal, y - 0*yNormal, z - 1*zNormal).computeRho();
  T rho200 = blockLattice.get(x - 2*xNormal, y - 0*yNormal, z - 0*zNormal).computeRho();
  T rho020 = blockLattice.get(x - 0*xNormal, y - 2*yNormal, z - 0*zNormal).computeRho();
  T rho002 = blockLattice.get(x - 0*xNormal, y - 0*yNormal, z - 2*zNormal).computeRho();
  T rho = (T)4/(T)9 * (rho001 + rho010 + rho100) - (T)1/(T)9 * (rho002 + rho020 + rho200);

  T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d], dz_u[Lattice<T>::d];
  fd::DirectedGradients3D<T, Lattice, 0, xNormal, 0, true>::interpolateVector(dx_u, blockLattice, x,y,z);
  fd::DirectedGradients3D<T, Lattice, 1, yNormal, 0, true>::interpolateVector(dy_u, blockLattice, x,y,z);
  fd::DirectedGradients3D<T, Lattice, 2, zNormal, 0, true>::interpolateVector(dz_u, blockLattice, x,y,z);

  T dx_ux = dx_u[0];
  T dy_ux = dy_u[0];
  T dz_ux = dz_u[0];
  T dx_uy = dx_u[1];
  T dy_uy = dy_u[1];
  T dz_uy = dz_u[1];
  T dx_uz = dx_u[2];
  T dy_uz = dy_u[2];
  T dz_uz = dz_u[2];
  T omega = dynamics -> getOmega();
  T sToPi = - rho / Lattice<T>::invCs2() / omega;
  T pi[util::TensorVal<Lattice<T> >::n];
  pi[xx] = (T)2 * dx_ux * sToPi;
  pi[yy] = (T)2 * dy_uy * sToPi;
  pi[zz] = (T)2 * dz_uz * sToPi;
  pi[xy] = (dx_uy + dy_ux) * sToPi;
  pi[xz] = (dx_uz + dz_ux) * sToPi;
  pi[yz] = (dy_uz + dz_uy) * sToPi;

  // Computation of the particle distribution functions
  // according to the regularized formula
  T u[Lattice<T>::d];
  cell.computeU(u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = dynamics -> computeEquilibrium(iPop,rho,u,uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }

}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
template<class Dynamics>
void OuterVelocityCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    using namespace olb::util::tensorIndices3D;

    size_t ny = indexCalcHelper[1];
    size_t nz = indexCalcHelper[2];

    const size_t index100 = static_cast<size_t>(static_cast<long long>(index) - 0*zNormal - 0*yNormal*static_cast<long long>(nz) - 1*xNormal*static_cast<long long>(ny)*static_cast<long long>(nz));
    const size_t index010 = static_cast<size_t>(static_cast<long long>(index) - 0*zNormal - 1*yNormal*static_cast<long long>(nz) - 0*xNormal*static_cast<long long>(ny)*static_cast<long long>(nz));
    const size_t index001 = static_cast<size_t>(static_cast<long long>(index) - 1*zNormal - 0*yNormal*static_cast<long long>(nz) - 0*xNormal*static_cast<long long>(ny)*static_cast<long long>(nz));
    const size_t index200 = static_cast<size_t>(static_cast<long long>(index) - 0*zNormal - 0*yNormal*static_cast<long long>(nz) - 2*xNormal*static_cast<long long>(ny)*static_cast<long long>(nz));
    const size_t index020 = static_cast<size_t>(static_cast<long long>(index) - 0*zNormal - 2*yNormal*static_cast<long long>(nz) - 0*xNormal*static_cast<long long>(ny)*static_cast<long long>(nz));
    const size_t index002 = static_cast<size_t>(static_cast<long long>(index) - 2*zNormal - 0*yNormal*static_cast<long long>(nz) - 0*xNormal*static_cast<long long>(ny)*static_cast<long long>(nz));

    const T rho100 = util::getRho<T,Lattice>(cellData,index100);
    const T rho010 = util::getRho<T,Lattice>(cellData,index010);
    const T rho001 = util::getRho<T,Lattice>(cellData,index001);
    const T rho200 = util::getRho<T,Lattice>(cellData,index200);
    const T rho020 = util::getRho<T,Lattice>(cellData,index020);
    const T rho002 = util::getRho<T,Lattice>(cellData,index002);

    const T rho = (T)4/(T)9 * (rho001 + rho010 + rho100) - (T)1/(T)9 * (rho002 + rho020 + rho200);

    T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d], dz_u[Lattice<T>::d];
    fd::DirectedGradients3D<T, Lattice, 0, xNormal, 0, true>::interpolateVector(cellData,index,ny,nz,dx_u);
    fd::DirectedGradients3D<T, Lattice, 1, yNormal, 0, true>::interpolateVector(cellData,index,ny,nz,dy_u);
    fd::DirectedGradients3D<T, Lattice, 2, zNormal, 0, true>::interpolateVector(cellData,index,ny,nz,dz_u);

    const T dx_ux = dx_u[0];
    const T dy_ux = dy_u[0];
    const T dz_ux = dz_u[0];
    const T dx_uy = dx_u[1];
    const T dy_uy = dy_u[1];
    const T dz_uy = dz_u[1];
    const T dx_uz = dx_u[2];
    const T dy_uz = dy_u[2];
    const T dz_uz = dz_u[2];
    const T omega = collisionData[Dynamics::omegaIndex];
    const T sToPi = - rho / Lattice<T>::invCs2() / omega;

    T pi[util::TensorVal<Lattice<T> >::n];
    pi[xx] = (T)2 * dx_ux * sToPi;
    pi[yy] = (T)2 * dy_uy * sToPi;
    pi[zz] = (T)2 * dz_uz * sToPi;
    pi[xy] = (dx_uy + dy_ux) * sToPi;
    pi[xz] = (dx_uz + dz_ux) * sToPi;
    pi[yz] = (dy_uz + dz_uy) * sToPi;

    // Computation of the particle distribution functions
    // according to the regularized formula
    T u[Lattice<T>::d];
    util::getU<T,Lattice>(cellData,index,u);
    T uSqr = util::normSqr<T,Lattice<T>::d>(u);

    for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
      cellData[iPop][index] = Dynamics::computeEquilibriumStatic(iPop,rho,u,uSqr) +
                   firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
    }

}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
void OuterVelocityCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  if (util::contained(x, y, z, x0_, x1_, y0_, y1_, z0_, z1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
template<class Dynamics>
void OuterVelocityCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
processSubDomain(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          size_t index, size_t indexCalcHelper[Lattice<T>::d])
{
    process<Dynamics>(cellData,momentaData,collisionData,index,indexCalcHelper);
}

////////  OuterVelocityCornerProcessorGenerator3D ///////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal>::
OuterVelocityCornerProcessorGenerator3D(int x_, int y_, int z_)
  : PostProcessorGenerator3D<T,Lattice>(x_,x_, y_,y_, z_,z_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
PostProcessor3D<T,Lattice>*
OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal>::
generate() const
{
  return new OuterVelocityCornerProcessor3D<T,Lattice, xNormal,yNormal,zNormal>
         ( this->x0, this->y0, this->z0 );
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
PostProcessorGenerator3D<T,Lattice>*
OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal,yNormal,zNormal>::clone() const
{
  return new OuterVelocityCornerProcessorGenerator3D<T,Lattice, xNormal, yNormal, zNormal>
         (this->x0, this->y0, this->z0);
}


////////  SlipBoundaryProcessor3D ////////////////////////////////

template<typename T, template<typename U> class Lattice,int discreteNormalX,int discreteNormalY,int discreteNormalZ>
OPENLB_HOST_DEVICE
int getReflectionPop (int iPop)
{
 
  if (Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ < 0) 
  {
      // std::cout << "-----" <<s td::endl;
      int mirrorDirection0;
      int mirrorDirection1;
      int mirrorDirection2;
      int mult = 2 / (discreteNormalX*discreteNormalX + discreteNormalY*discreteNormalY + discreteNormalZ*discreteNormalZ);

      mirrorDirection0 = (Lattice<T>::c(iPop,0) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ)*discreteNormalX);
      mirrorDirection1 = (Lattice<T>::c(iPop,1) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ)*discreteNormalY);
      mirrorDirection2 = (Lattice<T>::c(iPop,2) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ)*discreteNormalZ);

      // bounce back for the case discreteNormalX = discreteNormalY = discreteNormalZ = 1, that is mult=0
      if (mult == 0) {
        mirrorDirection0 = -Lattice<T>::c(iPop,0);
        mirrorDirection1 = -Lattice<T>::c(iPop,1);
        mirrorDirection2 = -Lattice<T>::c(iPop,2);
      }

      // computes mirror jPop
      for (int retval = 1; retval < Lattice<T>::q ; ++retval) {
        if (Lattice<T>::c(retval,0)==mirrorDirection0 && Lattice<T>::c(retval,1)==mirrorDirection1 && Lattice<T>::c(retval,2)==mirrorDirection2) {
          return retval;
        }
      }

      return 0; 
  }

  else
    return 0;
}


template<typename T, template<typename U> class Lattice,int discreteNormalX,int discreteNormalY,int discreteNormalZ>
template<class Dynamics>
void SlipBoundaryProcessor3D<T,Lattice,discreteNormalX,discreteNormalY,discreteNormalZ>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    for (int iPop = 1; iPop < Lattice<T>::q ; ++iPop) {
      if (getReflectionPop<T,Lattice,discreteNormalX,discreteNormalY,discreteNormalZ>(iPop)!=0) {
        //do reflection
        // cellData[Lattice<T>::opposite(iPop)][index] = 
        // cellData[Lattice<T>::opposite(getReflectionPop<T,Lattice,discreteNormalX,discreteNormalY,discreteNormalZ>(iPop))][index];
        cellData[(iPop)][index] = 
        cellData[(getReflectionPop<T,Lattice,discreteNormalX,discreteNormalY,discreteNormalZ>(iPop))][index];
      }
    }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void CurvedSlipBoundaryProcessor3D<T,Lattice>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t nNeighbor = static_cast<size_t>(std::round(postProcData[idxNumNeighbour()][position]));
    T const tau = postProcData[idxTau()][position];

    T normal[3];
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        normal[iDim] = postProcData[idxNormal()+iDim][position];

    T uf[3] = {0,0,0};
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uf[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

    T uw[3];
    float absUw = 0;
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        absUw += uf[iDim]*postProcData[idxNormal()+iDim][position];

    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uw[iDim] = uf[iDim]-postProcData[idxNormal()+iDim][position]*absUw;

    for(unsigned int iN = 0; iN < nNeighbor; ++iN) {
        unsigned int const iPop = static_cast<unsigned int>(std::round(postProcData[idxDir(iN)][position]));
        unsigned int const iPopOpp = Lattice<T>::opposite(iPop);

        T const delta = postProcData[idxDelta(iN)][position];

        size_t indexB = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);

        T ubf[3];
        T xi;

        if(delta >= 0.5) {
            xi = (2*delta-1)/(tau+0.5);
            size_t const indexFF = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                    Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                    indexCalcHelper[1],indexCalcHelper[2]);
            for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                ubf[iDim] = cellData[Lattice<T>::uIndex+iDim][index]*(1-3/(2*delta))+uw[iDim]*3/(2*delta);
        }
        else {
            xi = (2*delta-1)/(tau-2);
            size_t const indexFF = util::getCellIndex3D(index,Lattice<T>::c(iPopOpp,0),
                    Lattice<T>::c(iPopOpp,1),Lattice<T>::c(iPopOpp,2),
                    indexCalcHelper[1],indexCalcHelper[2]);
            for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                ubf[iDim] = cellData[Lattice<T>::uIndex+iDim][indexFF];
        }

        T scalbf = 0;
        T scalf  = 0;
        T scalff = 0;
        T scalw  = 0;

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim) {
            scalbf += Lattice<T>::c(iPop,iDim)*ubf[iDim];
            scalf  += Lattice<T>::c(iPop,iDim)*uf[iDim];
            scalff += uf[iDim]*uf[iDim];
            scalw  += Lattice<T>::c(iPopOpp,iDim)*uw[iDim];
        }

        T const cs2 = Lattice<T>::invCs2();
        T const fstar = Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*
                (1 + 3*scalbf + 4.5*scalf*scalf - 1.5*scalff);
        T const tmp = 2*Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*3*scalw;

        cellData[iPopOpp][index] = ((1-xi)*(cellData[iPop][indexB] + Lattice<T>::t(iPop)) + xi*fstar + tmp)-Lattice<T>::t(iPop);
    }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void CurvedBouzidiBoundaryProcessor3D<T,Lattice>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t nNeighbor = static_cast<size_t>(std::round(postProcData[idxNumNeighbour()][position]));
    T const tau = postProcData[idxTau()][position];

    T normal[3];
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        normal[iDim] = postProcData[idxNormal()+iDim][position];

    T uf[3] = {0,0,0};
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uf[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

    T uw[3];
    float absUw = 0;
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        absUw += uf[iDim]*postProcData[idxNormal()+iDim][position];

    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uw[iDim] = uf[iDim]-postProcData[idxNormal()+iDim][position]*absUw;

    for(unsigned int iN = 0; iN < nNeighbor; ++iN) {
        unsigned int const iPop = static_cast<unsigned int>(std::round(postProcData[idxDir(iN)][position]));
        unsigned int const iPopOpp = Lattice<T>::opposite(iPop);

        T const delta = postProcData[idxDelta(iN)][position];

        size_t indexB = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);
        size_t indexBB = util::getCellIndex3D(index,-Lattice<T>::c(iPop,0),
                -Lattice<T>::c(iPop,1),-Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);
        size_t indexFF = util::getCellIndex3D(index,Lattice<T>::c(iPopOpp,0),
                Lattice<T>::c(iPopOpp,1),Lattice<T>::c(iPopOpp,2),
                indexCalcHelper[1],indexCalcHelper[2]);

        T scalw  = 0;

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim) {
            scalw  += Lattice<T>::c(iPopOpp,iDim)*uw[iDim]; //ea*uw
        }

        T const tmp = 2*Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*3*scalw;

        if(delta >= 0.5) {
            cellData[iPopOpp][index] = (1/(2*delta))*(cellData[iPop][indexB])+((2*delta-1)/(2*delta))*(cellData[iPopOpp][indexB]); 
        }
        else {
            cellData[iPopOpp][index] = (2*delta)*(cellData[iPop][indexB])+(1-2*delta)*(cellData[iPop][indexFF]);
        }
    }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void CurvedBouzidiQuadraticBoundaryProcessor3D<T,Lattice>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t nNeighbor = static_cast<size_t>(std::round(postProcData[idxNumNeighbour()][position]));
    T const tau = postProcData[idxTau()][position];

    T normal[3];
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        normal[iDim] = postProcData[idxNormal()+iDim][position];

    T uf[3] = {0,0,0};
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uf[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

    T uw[3];
    float absUw = 0;
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        absUw += uf[iDim]*postProcData[idxNormal()+iDim][position];

    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uw[iDim] = uf[iDim]-postProcData[idxNormal()+iDim][position]*absUw;

    for(unsigned int iN = 0; iN < nNeighbor; ++iN) {
        unsigned int const iPop = static_cast<unsigned int>(std::round(postProcData[idxDir(iN)][position]));
        unsigned int const iPopOpp = Lattice<T>::opposite(iPop);

        T const delta = postProcData[idxDelta(iN)][position];

        size_t indexB = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);
        size_t indexBB = util::getCellIndex3D(index,-Lattice<T>::c(iPop,0),
                -Lattice<T>::c(iPop,1),-Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);

        size_t index2BB = util::getCellIndex3D(index,-2*Lattice<T>::c(iPop,0),
                -2*Lattice<T>::c(iPop,1),-2*Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);

        T ubf[3];
        T xi;

        T scalbf = 0;
        T scalf  = 0;
        T scalff = 0;
        T scalw  = 0;

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim) {
            scalbf += Lattice<T>::c(iPop,iDim)*ubf[iDim]; //ea*ubf
            scalf  += Lattice<T>::c(iPop,iDim)*uf[iDim]; //ea*uf
            scalff += uf[iDim]*uf[iDim]; //uf*uf
            scalw  += Lattice<T>::c(iPopOpp,iDim)*uw[iDim]; //ea*uw
        }

        T const tmp = 2*Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*3*scalw;

        if(delta >= 0.5) {
            cellData[iPopOpp][index] = (1/(delta*(2*delta+1)))*(cellData[iPop][indexB]+tmp)+((2*delta-1)/(delta))*(cellData[iPopOpp][indexB])+((1-2*delta)/(1+2*delta))*(cellData[iPopOpp][indexBB]);
    }
        else {
            cellData[iPopOpp][index] = delta*(2*delta+1)*(cellData[iPop][indexB])+(1+2*delta)*(1-2*delta)*(cellData[iPop][indexBB])-delta*(1-2*delta)*(cellData[iPop][index2BB])+tmp;  
        }
    }
}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void CurvedYuBoundaryProcessor3D<T,Lattice>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t nNeighbor = static_cast<size_t>(std::round(postProcData[idxNumNeighbour()][position]));
    T const tau = postProcData[idxTau()][position];

    T normal[3];
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        normal[iDim] = postProcData[idxNormal()+iDim][position];

    T uf[3] = {0,0,0};
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uf[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

    T uw[3];
    float absUw = 0;
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        absUw += uf[iDim]*postProcData[idxNormal()+iDim][position];

    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim){
        uw[iDim] = uf[iDim]-postProcData[idxNormal()+iDim][position]*absUw;
    }

    for(unsigned int iN = 0; iN < nNeighbor; ++iN) {
        unsigned int const iPop = static_cast<unsigned int>(std::round(postProcData[idxDir(iN)][position]));
        unsigned int const iPopOpp = Lattice<T>::opposite(iPop);

        T const delta = postProcData[idxDelta(iN)][position];

        size_t indexB = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);
        size_t indexBB = util::getCellIndex3D(index,-Lattice<T>::c(iPop,0),
                -Lattice<T>::c(iPop,1),-Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);
        size_t indexOpp = util::getCellIndex3D(index,Lattice<T>::c(iPopOpp,0),
                Lattice<T>::c(iPopOpp,1),Lattice<T>::c(iPopOpp,2),
                indexCalcHelper[1],indexCalcHelper[2]);
        size_t index2BB = util::getCellIndex3D(index,-2*Lattice<T>::c(iPop,0),
                -2*Lattice<T>::c(iPop,1),-2*Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);

        T ubf[3];
        T xi;
        T scalw  = 0;

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim) {
            scalw  += Lattice<T>::c(iPopOpp,iDim)*uw[iDim]; //ea*uw
        }
        T const tmp = 2*Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*3*scalw;
       
        T wall = cellData[iPop][index]+delta*(cellData[iPop][indexB]-cellData[iPop][index]);
        T wallOpp = wall+tmp;
        cellData[iPopOpp][index] = wallOpp+(delta/(1+delta))*(cellData[iPopOpp][indexOpp]-wallOpp);
    }
}


///isabel's simpler curved bc
template <typename T, template <typename U> class Lattice>
template <class Dynamics>
void BounceBackMovingBoundaryBoundaryProcessor3D<T, Lattice>::
    process(T *const OPENLB_RESTRICT *const OPENLB_RESTRICT cellData,
            T *const OPENLB_RESTRICT *const OPENLB_RESTRICT momentaData,
            T const *const OPENLB_RESTRICT collisionData,
            T *const OPENLB_RESTRICT *const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  size_t nNeighbor = static_cast<size_t>(std::round(postProcData[idxNumNeighbour()][position]));
  T const tau = postProcData[idxTau()][position];

  T normal[3];
  for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    normal[iDim] = postProcData[idxNormal() + iDim][position];

  T uf[3] = {0, 0, 0};
  for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    uf[iDim] = cellData[Lattice<T>::uIndex + iDim][index]; //if index

  T uw[3];
  float absUw = 0;
  for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    absUw += uf[iDim] * postProcData[idxNormal() + iDim][position];

  for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
  {
    uw[iDim] = uf[iDim] - postProcData[idxNormal() + iDim][position] * absUw;
  }

  for (unsigned int iN = 0; iN < nNeighbor; ++iN)
  {
    unsigned int const iPop = static_cast<unsigned int>(std::round(postProcData[idxDir(iN)][position]));
    unsigned int const iPopOpp = Lattice<T>::opposite(iPop);

    T const delta = postProcData[idxDelta(iN)][position];

    size_t indexB = util::getCellIndex3D(index, Lattice<T>::c(iPop, 0),
                                          Lattice<T>::c(iPop, 1), Lattice<T>::c(iPop, 2),
                                          indexCalcHelper[1], indexCalcHelper[2]);
    size_t indexBB = util::getCellIndex3D(index, -Lattice<T>::c(iPop, 0),
                                          -Lattice<T>::c(iPop, 1), -Lattice<T>::c(iPop, 2),
                                          indexCalcHelper[1], indexCalcHelper[2]);

    T scalw = 0;

    for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    {
      scalw += Lattice<T>::c(iPopOpp, iDim) * uw[iDim]; //ea*uw
    }

    T const tmp = 2 * Lattice<T>::t(iPop) * cellData[Lattice<T>::rhoIndex][index] * 3 * scalw;
    cellData[iPopOpp][index] = cellData[iPop][indexB] + tmp;
  }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void CurvedWallBoundaryProcessor3D<T,Lattice>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t nNeighbor = static_cast<size_t>(std::round(postProcData[idxNumNeighbour()][position]));
    T const tau = postProcData[idxTau()][position];

    T uw[3];

    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
        uw[iDim] = postProcData[idxVel()+iDim][position];

    for(unsigned int iN = 0; iN < nNeighbor; ++iN) {

        unsigned int const iPop = static_cast<unsigned int>(std::round(postProcData[idxDir(iN)][position]));
        unsigned int const iPopOpp = Lattice<T>::opposite(iPop);

        T const delta = postProcData[idxDelta(iN)][position];

        T uf[3] = {0,0,0};
        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
            uf[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

        size_t indexB = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                indexCalcHelper[1],indexCalcHelper[2]);

        T ubf[3];
        T xi;

        if(delta >= 0.5) {
            xi = (2*delta-1)/(tau+0.5);
            size_t const indexFF = util::getCellIndex3D(index,Lattice<T>::c(iPop,0),
                    Lattice<T>::c(iPop,1),Lattice<T>::c(iPop,2),
                    indexCalcHelper[1],indexCalcHelper[2]);
            for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                ubf[iDim] = cellData[Lattice<T>::uIndex+iDim][index]*(1-3/(2*delta))+uw[iDim]*3/(2*delta);
        }
        else {
            xi = (2*delta-1)/(tau-2);
            size_t const indexFF = util::getCellIndex3D(index,Lattice<T>::c(iPopOpp,0),
                    Lattice<T>::c(iPopOpp,1),Lattice<T>::c(iPopOpp,2),
                    indexCalcHelper[1],indexCalcHelper[2]);
            for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
                ubf[iDim] = cellData[Lattice<T>::uIndex+iDim][indexFF];
        }

        T scalbf = 0;
        T scalf  = 0;
        T scalff = 0;
        T scalw  = 0;

        for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim) {
            scalbf += Lattice<T>::c(iPop,iDim)*ubf[iDim];
            scalf  += Lattice<T>::c(iPop,iDim)*uf[iDim];
            scalff += uf[iDim]*uf[iDim];
            scalw  += Lattice<T>::c(iPopOpp,iDim)*uw[iDim];
        }

        T const cs2 = Lattice<T>::invCs2();
        T const fstar = Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*
                (1 + 3*scalbf + 4.5*scalf*scalf - 1.5*scalff);
        T const tmp = 2*Lattice<T>::t(iPop)*cellData[Lattice<T>::rhoIndex][index]*3*scalw;

        cellData[iPopOpp][index] = ((1-xi)*(cellData[iPop][indexB] + Lattice<T>::t(iPop)) + xi*fstar + tmp)-Lattice<T>::t(iPop);
    }
}

// template<typename T, template<typename U> class Lattice>
// SlipBoundaryProcessor3D<T,Lattice>::
// SlipBoundaryProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, int discreteNormalX, int discreteNormalY, int discreteNormalZ)
  // : x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_)
// {
  // OLB_PRECONDITION(x0==x1 || y0==y1 || z0==z1);
  // reflectionPop[0] = 0;
  // for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
    // reflectionPop[iPop] = 0;
    // // iPop are the directions which pointing into the fluid, discreteNormal is pointing outwarts
    // if (Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ < 0) {
      // // std::cout << "-----" <<s td::endl;
      // int mirrorDirection0;
      // int mirrorDirection1;
      // int mirrorDirection2;
      // int mult = 2 / (discreteNormalX*discreteNormalX + discreteNormalY*discreteNormalY + discreteNormalZ*discreteNormalZ);

      // mirrorDirection0 = (Lattice<T>::c(iPop,0) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ)*discreteNormalX);
      // mirrorDirection1 = (Lattice<T>::c(iPop,1) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ)*discreteNormalY);
      // mirrorDirection2 = (Lattice<T>::c(iPop,2) - mult*(Lattice<T>::c(iPop,0)*discreteNormalX + Lattice<T>::c(iPop,1)*discreteNormalY + Lattice<T>::c(iPop,2)*discreteNormalZ)*discreteNormalZ);

      // // bounce back for the case discreteNormalX = discreteNormalY = discreteNormalZ = 1, that is mult=0
      // if (mult == 0) {
        // mirrorDirection0 = -Lattice<T>::c(iPop,0);
        // mirrorDirection1 = -Lattice<T>::c(iPop,1);
        // mirrorDirection2 = -Lattice<T>::c(iPop,2);
      // }

      // // computes mirror jPop
      // for (reflectionPop[iPop] = 1; reflectionPop[iPop] < Lattice<T>::q ; reflectionPop[iPop]++) {
        // if (Lattice<T>::c(reflectionPop[iPop],0)==mirrorDirection0 && Lattice<T>::c(reflectionPop[iPop],1)==mirrorDirection1 && Lattice<T>::c(reflectionPop[iPop],2)==mirrorDirection2) {
          // break;
        // }
      // }
      // //std::cout <<iPop << " to "<< jPop <<" for discreteNormal= "<< discreteNormalX << "/"<<discreteNormalY <<std::endl;
    // }
  // }
// }

// template<typename T, template<typename U> class Lattice>
// void SlipBoundaryProcessor3D<T,Lattice>::
// processSubDomain(BlockLattice3D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
// {
  // int newX0, newX1, newY0, newY1, newZ0, newZ1;
  // if ( util::intersect (
         // x0, x1, y0, y1, z0, z1,
         // x0_, x1_, y0_, y1_, z0_, z1_,
         // newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {

    // int iX;
// #ifdef PARALLEL_MODE_OMP
    // #pragma omp parallel for
// #endif
    // for (iX=newX0; iX<=newX1; ++iX) {
      // for (int iY=newY0; iY<=newY1; ++iY) {
        // for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          // for (int iPop = 1; iPop < Lattice<T>::q ; ++iPop) {
            // if (reflectionPop[iPop]!=0) {
              // //do reflection
              // blockLattice.get(iX,iY,iZ)[iPop] = blockLattice.get(iX,iY,iZ)[reflectionPop[iPop]];
            // }
          // }
        // }
      // }
    // }
  // }
// }

// template<typename T, template<typename U> class Lattice>
// void SlipBoundaryProcessor3D<T,Lattice>::
// process(BlockLattice3D<T,Lattice>& blockLattice)
// {
  // processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
// }

////////  SlipBoundaryProcessorGenerator3D ////////////////////////////////

// template<typename T, template<typename U> class Lattice>
// SlipBoundaryProcessorGenerator3D<T,Lattice>::
// SlipBoundaryProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, int discreteNormalX_, int discreteNormalY_, int discreteNormalZ_)
  // : PostProcessorGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_), discreteNormalX(discreteNormalX_), discreteNormalY(discreteNormalY_), discreteNormalZ(discreteNormalZ_)
// { }

// template<typename T, template<typename U> class Lattice>
// PostProcessor3D<T,Lattice>*
// SlipBoundaryProcessorGenerator3D<T,Lattice>::generate() const
// {
  // return new SlipBoundaryProcessor3D<T,Lattice>
         // ( this->x0, this->x1, this->y0, this->y1, this->z0, this->z1, discreteNormalX, discreteNormalY, discreteNormalZ);
// }

// template<typename T, template<typename U> class Lattice>
// PostProcessorGenerator3D<T,Lattice>*
// SlipBoundaryProcessorGenerator3D<T,Lattice>::clone() const
// {
  // return new SlipBoundaryProcessorGenerator3D<T,Lattice>
         // (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1, discreteNormalX, discreteNormalY, discreteNormalZ);
// }

template <typename T, template<typename U> class Lattice>
template <class Dynamics>
void GradBoundaryProcessor3D<T,Lattice>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d]) {
  
  size_t cellIndices[3];
  util::getCellIndices3D(index, indexCalcHelper[1], indexCalcHelper[2], cellIndices);

  //calculate rho and u target
  int nD = 0;
  T uTgt[3] = {0.0,0.0,0.0};
  T rho = 1.0;
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    if (postProcData[idxDirs() + iPop][position] > (T)-0.9) {
      nD++;

      rho += cellData[Lattice<T>::opposite(iPop)][index] + 6*Lattice<T>::t(iPop)*(Lattice<T>::c(iPop,0)*postProcData[idxWallVel()+0][position] + 
                                                                                  Lattice<T>::c(iPop,1)*postProcData[idxWallVel()+1][position] + 
                                                                                  Lattice<T>::c(iPop,2)*postProcData[idxWallVel()+2][position]);

      for (int iD = 0; iD < Lattice<T>::d; iD++) {
        size_t destIndex = util::getCellIndex3D(cellIndices[0]+Lattice<T>::c(iPop, iD), cellIndices[1]+Lattice<T>::c(iPop, iD), cellIndices[2]+Lattice<T>::c(iPop, iD), 
                                                          indexCalcHelper[1], indexCalcHelper[2]);
        uTgt[iD] += (postProcData[idxWallVel()+iD][position] + postProcData[idxDirs() + iPop][position]*cellData[Lattice<T>::uIndex + iD][destIndex])
                      / ((T)1.0 + postProcData[idxDirs() + iPop][position]);
      }
    }
    else {
      rho += cellData[iPop][index];
    }
  }

  for (int iD = 0; iD < Lattice<T>::d; iD++) { 
    uTgt[iD] = uTgt[iD] / nD;
  }

  //calculate finite differences
  T finiteDiffs[Lattice<T>::d*Lattice<T>::d];
  
  for (int dD = 0; dD < Lattice<T>::d; dD++)  {
    size_t destCellIndices[3] = {cellIndices[0], cellIndices[1], cellIndices[2]};
    destCellIndices[dD] += (int)postProcData[idxFDDirs() + dD][position];
    size_t leftIndex = index;
    size_t rightIndex = util::getCellIndex3D(destCellIndices[0], destCellIndices[1], destCellIndices[2], indexCalcHelper[1], indexCalcHelper[2]);
    

    for (int uD = 0; uD < Lattice<T>::d; uD++) {
      finiteDiffs[uD + dD*Lattice<T>::d] = cellData[Lattice<T>::uIndex + uD][rightIndex] - uTgt[uD];
      if (leftIndex > rightIndex) {
        finiteDiffs[uD + dD*Lattice<T>::d] *= -1;
      }
    }
  }
  
  T sToPi = - rho / Lattice<T>::invCs2() / postProcData[idxOmega()][position];
  T pi[util::TensorVal<Lattice<T> >::n];
  using namespace olb::util::tensorIndices3D;
  pi[xx] = (T)2 * finiteDiffs[0] * sToPi; //dx_ux
  pi[yy] = (T)2 * finiteDiffs[4] * sToPi; //dy_uy
  pi[zz] = (T)2 * finiteDiffs[8] * sToPi; //dz_uz
  pi[xy] = (finiteDiffs[1] + finiteDiffs[3]) * sToPi; //dx_uy, dy_ux
  pi[xz] = (finiteDiffs[2] + finiteDiffs[6]) * sToPi; //dx_uz, dz_ux
  pi[yz] = (finiteDiffs[5] + finiteDiffs[7]) * sToPi; //dy_uz, dz_uy

  T uSqr = util::normSqr<T,Lattice<T>::d>(uTgt);
  

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    if (postProcData[idxDirs() + iPop][position] > -0.9) {
      cellData[iPop][index] = Dynamics::computeEquilibriumStatic(iPop,rho,uTgt,uSqr) +
                  firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void RegularizedBoundaryProcessor3D<T,Lattice,direction,orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
  T rho; 
  T u[Lattice<T>::d];

  Dynamics::MomentaType::computeRhoU(cellData, index, momentaData, 0, rho, u);

  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  T pi[util::TensorVal<Lattice<T> >::n];
  BoundaryHelpers<T,Lattice,direction,orientation>::computeStress(cellData, index, rho, u, pi);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    cellData[iPop][index] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) + firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}


////////  ImpedanceBoundaryPostProcessor3D ////////////////////////////////
// This implementation runs into trouble with optimization
// hence specializations are provided below
template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,direction,orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          T const * const OPENLB_RESTRICT collisionData,
          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
          size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();
// #define DEBUG_POSImpedanceFloatPROC

#ifdef DEBUG_POSImpedanceFloatPROC
    std::stringstream debugStream;
#endif

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

#ifdef DEBUG_POSImpedanceFloatPROC
    debugStream << std::endl << std::endl;
    debugStream << "Direction: " << direction << "; Orientation: " << orientation << std::endl;
#endif
    ImpedanceFloat rhoWall = 0;
    for(int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
    {
      const int factor = (std::abs( Lattice<ImpedanceFloat>::c(iPop,direction)*(Lattice<ImpedanceFloat>::c(iPop,direction)+orientation) )
                          + 1 - std::abs(Lattice<ImpedanceFloat>::c(iPop,direction)) );
      rhoWall += factor*cellDataCache[iPop];
#ifdef DEBUG_POSImpedanceFloatPROC
      if(factor)
          debugStream << factor << "*" << impedanceBoundaryHelpers::f[iPop] << "(" << iPop << ") " << " ";
#endif
    }

#ifdef DEBUG_POSImpedanceFloatPROC
    debugStream << std::endl << "RhoWall: " << rhoWall << std::endl << std::endl;
#endif

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};

    for(int counter=0; counter < Lattice<ImpedanceFloat>::d-1; ++counter)
    {
#ifdef DEBUG_POSImpedanceFloatPROC
        debugStream << "Direction: " << inPlane(counter) << std::endl;
#endif
        ImpedanceFloat weights = 0;
        for(int iPop=1; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
          bool isInAxisDirection = ( 1-std::abs(Lattice<ImpedanceFloat>::c(iPop,direction)) );
          temp[inPlane(counter)] += cellDataCache[iPop]*Lattice<ImpedanceFloat>::c(iPop,inPlane(counter))*isInAxisDirection;
          weights += Lattice<ImpedanceFloat>::t(iPop)*std::abs(Lattice<ImpedanceFloat>::c(iPop,inPlane(counter)))*isInAxisDirection;

#ifdef DEBUG_POSImpedanceFloatPROC
          if(isInAxisDirection)
              debugStream << Lattice<ImpedanceFloat>::c(iPop,inPlane(counter)) << "*" << impedanceBoundaryHelpers::f[iPop] << "(" << iPop << ") ";
#endif

        }

#ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << std::endl << "Factor: " << csSq/weights << " (" << weights << ")" << ", Sum(fi): " << temp[inPlane(counter)] << std::endl;
#endif

          temp[inPlane(counter)] *= csSq/weights;
#ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << "ImpedanceFloatemp[" << inPlane(counter) << "]: " << temp[inPlane(counter)] << "   ";
          debugStream << std::endl << std::endl;
#endif
    }

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

#ifdef DEBUG_POSImpedanceFloatPROC
    debugStream << "RhoOld: " << rhoOld << " RhoWall: " << rhoWall << std::endl;
    for(unsigned int iDim=0; iDim<Lattice<ImpedanceFloat>::d; ++iDim)
      debugStream << "uOld[" << iDim << "]: " << uOld[iDim] << std::endl;
#endif

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    bool isNan = false;

        ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
        ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

        ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
        ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

        ImpedanceFloat variance = 0.001*uNew[direction] + eps;
        uNewPlus[direction] = uNew[direction] + variance;
        uNewMinus[direction] = uNew[direction] - variance;

    #ifdef DEBUG_POSImpedanceFloatPROC
        debugStream << "uNew[" << direction << "] init: " << uNew[direction] << std::endl;
    #endif


        // bool converged = false;
        ImpedanceFloat uIncrementOld = 0;
        ImpedanceFloat relaxationFactor = 1;

        for(int iter = 0; iter < iterMax; ++iter)
        {
          deltaU[direction] = uNew[direction] - uOld[direction];

          ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

    #ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << "uNew[" << direction << "] init: " << uNew[direction] << std::endl;
    #endif

          deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
          deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

          ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
          ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

          for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
          {
              uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

              deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

              deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

              uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
              uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

              deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
              deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

              deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
              deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

    #ifdef DEBUG_POSImpedanceFloatPROC
              debugStream << "uNew[" << inPlane(counter) << "] init: " << uNew[inPlane(counter)] << std::endl;
    #endif
          }

          deltaUAbs = std::sqrt(deltaUAbs);

          deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
          deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

    #ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << std::endl;
    #endif

          ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

          ImpedanceFloat rho = rhoWall/onePlusMinusU;

          ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

          ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

          ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
          ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

          ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
          ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

          ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
          ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

          ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
                  + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
          ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
                  + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

          ImpedanceFloat c2Differential = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

    //      ImpedanceFloat preFactor = rho/onePlusMinusU;
    //
    //      ImpedanceFloat c2 = preFactor + 0.5*preFactor*deltaUAbs*deltaUAbs - orientation*rho*deltaU[direction]
    //                + signDeltaUDirection*cs*(- orientation*preFactor*deltaUAbs + rho/deltaUAbs*deltaU[direction]);
    //      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
    //          c2 += -rho/rhoWall*( deltaU[inPlane(counter)]*temp[inPlane(counter)] )
    //                +orientation*( signDeltaUDirection*cs*rho/(deltaUAbs*rhoWall) )*deltaU[inPlane(counter)]*temp[inPlane(counter)];

    #ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << "I(vx): " << c1 << std::endl;
          debugStream << "d[I(vx)]/d[vx]: " << c2Differential << std::endl;
    #endif

          ImpedanceFloat c2 = c2Differential;

    #if defined(DEBUG_POSImpedanceFloatPROC)
          debugStream << "I(vx+eps): " << c1Plus << std::endl;
          debugStream << "I(vx-eps): " << c1Minus << std::endl;
          debugStream << "(I(vx+eps) - I(vx-eps))/(2*eps): " << c2 << std::endl;
          debugStream << "d(vx): " << relaxationFactor*c1/c2 << std::endl << std::endl;
    #endif

          if(std::abs(c2) < eps)
          {
    #ifdef DEBUG_POSImpedanceFloatPROC
              debugStream << "Iterations: " << iter << "   ";
              debugStream << "I(vx): " << c1 << std::endl << std::endl;
    #endif
              // converged = true;
              break;
          }

          ImpedanceFloat uIncrement = c1/c2;

          if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
          {
              relaxationFactor *=0.5;
    #ifdef DEBUG_POSImpedanceFloatPROC
              debugStream << "RelaxationFactor: " << relaxationFactor << std::endl << std::endl;;
    #endif
          }

          uNew[direction] -= relaxationFactor*uIncrement;

          uIncrementOld = uIncrement;

          variance = 0.001*uNew[direction] + eps;
          uNewPlus[direction] = uNew[direction] + variance;
          uNewMinus[direction] = uNew[direction] - variance;

          if(std::abs(uIncrement) < eps)
          {
    #ifdef DEBUG_POSImpedanceFloatPROC
              debugStream << "Iterations: " << iter << "    " << "delta U: " << uIncrement << std::endl << std::endl;
    #endif
              // converged = true;
              break;
          }

          if(std::isnan(uNew[direction]))
          {
              isNan = true;
              break;
          }
        } // end for

        ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

        for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
          uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

        for(int iPop=1; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
          const bool isNotInDirection = 1+orientation*Lattice<ImpedanceFloat>::c(iPop,direction);

          if(!isNotInDirection)
          {
              const int iPopOpposite = Lattice<ImpedanceFloat>::opposite(iPop);
    #ifdef DEBUG_POSImpedanceFloatPROC
              debugStream << impedanceBoundaryHelpers::f[iPop] << "(" << iPop << ") " << " = " << impedanceBoundaryHelpers::f[iPopOpposite] << "(" << iPopOpposite << ") ";
    #endif
              cellDataCache[iPop] = cellDataCache[iPopOpposite];
              for(int axis = 0; axis<Lattice<ImpedanceFloat>::d; ++axis)
              {
                  cellDataCache[iPop] += rho*Lattice<ImpedanceFloat>::t(iPop)/csSq * (-Lattice<ImpedanceFloat>::c(iPopOpposite,axis)*uNew[axis]+Lattice<ImpedanceFloat>::c(iPop,axis)*uNew[axis]);
    #ifdef DEBUG_POSImpedanceFloatPROC
                  debugStream << " + " << Lattice<ImpedanceFloat>::t(iPop)/csSq << "*" << "("
                          << -Lattice<ImpedanceFloat>::c(iPopOpposite,axis)<< "*u[" << axis << "]+"
                          << Lattice<ImpedanceFloat>::c(iPop,axis) << "*u[" << axis << "])";
    #endif
              }
    #ifdef DEBUG_POSImpedanceFloatPROC
              debugStream << std::endl << std::endl;
    #endif
              }
          }


      for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
      {
#ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << cellDataCache[i] << "(" << i << ") ";
#endif
        cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
      }
#ifdef DEBUG_POSImpedanceFloatPROC
      debugStream << std::endl << std::endl;
#endif

      if(isNan or true)
      {
#ifdef DEBUG_POSImpedanceFloatPROC
          debugStream << "uNew: " << uNew[0] << "," << uNew[1] << "," << uNew[2] << " RhoNew: " << rho << std::endl;
          debugStream << "Not converged" << std::endl;
          std::cout << debugStream.str();
#endif
      }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );

//#undef DEBUG_POSImpedanceFloatPROC

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,0,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellDataCache[0] + cellDataCache[2] + cellDataCache[3] + cellDataCache[8] +
          cellDataCache[9] + cellDataCache[11] + cellDataCache[12] + cellDataCache[17] + cellDataCache[18] +
          2.0*(cellDataCache[1] + cellDataCache[4] + cellDataCache[5] + cellDataCache[6] + cellDataCache[7]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[1] = cellDataCache[11]+cellDataCache[17]+cellDataCache[18]-cellDataCache[2]-cellDataCache[8]-cellDataCache[9];
    weights = Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(18)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(9);
    temp[1] *= csSq/weights;

    temp[2] = cellDataCache[9]+cellDataCache[12]+cellDataCache[17]-cellDataCache[3]-cellDataCache[8]-cellDataCache[18];
    weights = Lattice<ImpedanceFloat>::t(9)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(18);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

    if(std::abs(c2) < eps)
    {
    //  converged = true;
     break;
    }

    ImpedanceFloat uIncrement = c1/c2;

    if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
    {
     relaxationFactor *=0.5;
    }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = varianceFactor*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for


    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellDataCache[10] = cellDataCache[1] + rho*2./6.*uNew[0];
    cellDataCache[13] = cellDataCache[4] + rho*2./12.*(uNew[0] + uNew[1]);
    cellDataCache[14] = cellDataCache[5] + rho*2./12.*(uNew[0] - uNew[1]);
    cellDataCache[15] = cellDataCache[6] + rho*2./12.*(uNew[0] + uNew[2]);
    cellDataCache[16] = cellDataCache[7] + rho*2./12.*(uNew[0] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }
    uNew[direction] *= 0.99;

      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,0,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellDataCache[0] + cellDataCache[2] + cellDataCache[3] + cellDataCache[8] +
          cellDataCache[9] + cellDataCache[11] + cellDataCache[12] + cellDataCache[17] + cellDataCache[18] +
          2.0*(cellDataCache[10] + cellDataCache[13] + cellDataCache[14] + cellDataCache[15] + cellDataCache[16]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[1] = cellDataCache[11]+cellDataCache[17]+cellDataCache[18]-cellDataCache[2]-cellDataCache[8]-cellDataCache[9];
    weights = Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(18)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(9);
    temp[1] *= csSq/weights;

    temp[2] = cellDataCache[9]+cellDataCache[12]+cellDataCache[17]-cellDataCache[3]-cellDataCache[8]-cellDataCache[18];
    weights = Lattice<ImpedanceFloat>::t(9)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(18);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = varianceFactor*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellDataCache[1] = cellDataCache[10] - rho*2./6.*uNew[0];
    cellDataCache[4] = cellDataCache[13] - rho*2./12.*(uNew[0] + uNew[1]);
    cellDataCache[5] = cellDataCache[14] - rho*2./12.*(uNew[0] - uNew[1]);
    cellDataCache[6] = cellDataCache[15] - rho*2./12.*(uNew[0] + uNew[2]);
    cellDataCache[7] = cellDataCache[16] - rho*2./12.*(uNew[0] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }

      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellDataCache[0] + cellDataCache[1] + cellDataCache[3] + cellDataCache[6] +
          cellDataCache[7] + cellDataCache[10] + cellDataCache[12] + cellDataCache[15] + cellDataCache[16] +
          2.0*(cellDataCache[2] + cellDataCache[4] + cellDataCache[8] + cellDataCache[9] + cellDataCache[14]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellDataCache[10]+cellDataCache[15]+cellDataCache[16]-cellDataCache[1]-cellDataCache[6]-cellDataCache[7];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(16)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(7);
    temp[0] *= csSq/weights;

    temp[2] = cellDataCache[7]+cellDataCache[12]+cellDataCache[15]-cellDataCache[3]-cellDataCache[6]-cellDataCache[16];
    weights = Lattice<ImpedanceFloat>::t(7)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(16);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

         if(std::abs(c2) < eps)
         {
            //  converged = true;
             break;
         }

         ImpedanceFloat uIncrement = c1/c2;

         if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
         {
             relaxationFactor *=0.5;
         }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = varianceFactor*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellDataCache[11] = cellDataCache[2] + rho*2./6.*uNew[1];
    cellDataCache[5] = cellDataCache[14] + rho*2./12.*(-uNew[0] + uNew[1]);
    cellDataCache[13] = cellDataCache[4] + rho*2./12.*( uNew[0] + uNew[1]);
    cellDataCache[17] = cellDataCache[8] + rho*2./12.*( uNew[1] + uNew[2]);
    cellDataCache[18] = cellDataCache[9] + rho*2./12.*( uNew[1] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }

      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

    ImpedanceFloat rhoWall = cellDataCache[0] + cellDataCache[1] + cellDataCache[3] + cellDataCache[6] +
          cellDataCache[7] + cellDataCache[10] + cellDataCache[12] + cellDataCache[15] + cellDataCache[16] +
          2.0*(cellDataCache[5] + cellDataCache[11] + cellDataCache[13] + cellDataCache[17] + cellDataCache[18]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellDataCache[10]+cellDataCache[15]+cellDataCache[16]-cellDataCache[1]-cellDataCache[6]-cellDataCache[7];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(16)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(7);
    temp[0] *= csSq/weights;

    temp[2] = cellDataCache[7]+cellDataCache[12]+cellDataCache[15]-cellDataCache[3]-cellDataCache[6]-cellDataCache[16];
    weights = Lattice<ImpedanceFloat>::t(7)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(16);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = varianceFactor*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellDataCache[2] = cellDataCache[11] - rho*2./6.* uNew[1];
    cellDataCache[14] = cellDataCache[5] - rho*2./12.*(-uNew[0] + uNew[1]);
    cellDataCache[4] = cellDataCache[13] - rho*2./12.*( uNew[0] + uNew[1]);
    cellDataCache[8] = cellDataCache[17] - rho*2./12.*( uNew[1] + uNew[2]);
    cellDataCache[9] = cellDataCache[18] - rho*2./12.*( uNew[1] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }

      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,2,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellDataCache[0] + cellDataCache[1] + cellDataCache[2] + cellDataCache[4] +
          cellDataCache[5] + cellDataCache[10] + cellDataCache[11] + cellDataCache[13] + cellDataCache[14] +
          2.0*(cellDataCache[3] + cellDataCache[6] + cellDataCache[8] + cellDataCache[16] + cellDataCache[18]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellDataCache[10]+cellDataCache[13]+cellDataCache[14]-cellDataCache[1]-cellDataCache[4]-cellDataCache[5];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(14)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(5);
    temp[0] *= csSq/weights;

    temp[1] = cellDataCache[5]+cellDataCache[11]+cellDataCache[13]-cellDataCache[2]-cellDataCache[4]-cellDataCache[14];
    weights = Lattice<ImpedanceFloat>::t(5)+Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(14);
    temp[1] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = varianceFactor*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellDataCache[12] = cellDataCache[3] + rho*2./6.*uNew[2];
    cellDataCache[7] = cellDataCache[16] + rho*2./12.*(-uNew[0] + uNew[2]);
    cellDataCache[9] = cellDataCache[18] + rho*2./12.*(-uNew[1] + uNew[2]);
    cellDataCache[15] = cellDataCache[6] + rho*2./12.*( uNew[0] + uNew[2]);
    cellDataCache[17] = cellDataCache[8] + rho*2./12.*( uNew[1] + uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }

      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryProcessor3D<T,Lattice,2,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = cellDataCache[Lattice<ImpedanceFloat>::uIndex+iDim];

    ImpedanceFloat rhoWall = cellDataCache[0] + cellDataCache[1] + cellDataCache[2] + cellDataCache[4] +
          cellDataCache[5] + cellDataCache[10] + cellDataCache[11] + cellDataCache[13] + cellDataCache[14] +
          2.0*(cellDataCache[7] + cellDataCache[9] + cellDataCache[12] + cellDataCache[15] + cellDataCache[17]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellDataCache[10]+cellDataCache[13]+cellDataCache[14]-cellDataCache[1]-cellDataCache[4]-cellDataCache[5];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(14)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(5);
    temp[0] *= csSq/weights;

    temp[1] = cellDataCache[5]+cellDataCache[11]+cellDataCache[13]-cellDataCache[2]-cellDataCache[4]-cellDataCache[14];
    weights = Lattice<ImpedanceFloat>::t(5)+Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(14);
    temp[1] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = varianceFactor*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellDataCache[3] = cellDataCache[12] - rho*2./6.* uNew[2];
    cellDataCache[16] = cellDataCache[7] - rho*2./12.*(-uNew[0] + uNew[2]);
    cellDataCache[18] = cellDataCache[9] - rho*2./12.*(-uNew[1] + uNew[2]);
    cellDataCache[6] = cellDataCache[15] - rho*2./12.*( uNew[0] + uNew[2]);
    cellDataCache[8] = cellDataCache[17] - rho*2./12.*( uNew[1] + uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }

      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellData[i][index] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = 1.0;
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = 0.0;

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellData[0][index] + cellData[2][index] + cellData[3][index] + cellData[8][index] +
          cellData[9][index] + cellData[11][index] + cellData[12][index] + cellData[17][index] + cellData[18][index] +
          2.0*(cellData[1][index] + cellData[4][index] + cellData[5][index] + cellData[6][index] + cellData[7][index]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[1] = cellData[11][index]+cellData[17][index]+cellData[18][index]-cellData[2][index]-cellData[8][index]-cellData[9][index];
    weights = Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(18)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(9);
    temp[1] *= csSq/weights;

    temp[2] = cellData[9][index]+cellData[12][index]+cellData[17][index]-cellData[3][index]-cellData[8][index]-cellData[18][index];
    weights = Lattice<ImpedanceFloat>::t(9)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(18);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

    if(std::abs(c2) < eps)
    {
    //  converged = true;
     break;
    }

    ImpedanceFloat uIncrement = c1/c2;

    if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
    {
     relaxationFactor *=0.5;
    }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = 0.001*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellData[10][index] = cellData[1][index] + rho*2./6.*uNew[0];
    cellData[13][index] = cellData[4][index] + rho*2./12.*(uNew[0] + uNew[1]);
    cellData[14][index] = cellData[5][index] + rho*2./12.*(uNew[0] - uNew[1]);
    cellData[15][index] = cellData[6][index] + rho*2./12.*(uNew[0] + uNew[2]);
    cellData[16][index] = cellData[7][index] + rho*2./12.*(uNew[0] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellData[i][index] -= Lattice<ImpedanceFloat>::t(i);
  }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,0,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellData[i][index] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = 1.0;
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = 0.0;

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellData[0][index] + cellData[2][index] + cellData[3][index] + cellData[8][index] +
          cellData[9][index] + cellData[11][index] + cellData[12][index] + cellData[17][index] + cellData[18][index] +
          2.0*(cellData[10][index] + cellData[13][index] + cellData[14][index] + cellData[15][index] + cellData[16][index]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[1] = cellData[11][index]+cellData[17][index]+cellData[18][index]-cellData[2][index]-cellData[8][index]-cellData[9][index];
    weights = Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(18)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(9);
    temp[1] *= csSq/weights;

    temp[2] = cellData[9][index]+cellData[12][index]+cellData[17][index]-cellData[3][index]-cellData[8][index]-cellData[18][index];
    weights = Lattice<ImpedanceFloat>::t(9)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(17)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(8)+Lattice<ImpedanceFloat>::t(18);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = 0.001*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellData[1][index] = cellData[10][index] - rho*2./6.*uNew[0];
    cellData[4][index] = cellData[13][index] - rho*2./12.*(uNew[0] + uNew[1]);
    cellData[5][index] = cellData[14][index] - rho*2./12.*(uNew[0] - uNew[1]);
    cellData[6][index] = cellData[15][index] - rho*2./12.*(uNew[0] + uNew[2]);
    cellData[7][index] = cellData[16][index] - rho*2./12.*(uNew[0] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellData[i][index] -= Lattice<ImpedanceFloat>::t(i);
  }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellData[i][index] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = 1.0;
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = 0.0;

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellData[0][index] + cellData[1][index] + cellData[3][index] + cellData[6][index] +
          cellData[7][index] + cellData[10][index] + cellData[12][index] + cellData[15][index] + cellData[16][index] +
          2.0*(cellData[2][index] + cellData[4][index] + cellData[8][index] + cellData[9][index] + cellData[14][index]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellData[10][index]+cellData[15][index]+cellData[16][index]-cellData[1][index]-cellData[6][index]-cellData[7][index];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(16)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(7);
    temp[0] *= csSq/weights;

    temp[2] = cellData[7][index]+cellData[12][index]+cellData[15][index]-cellData[3][index]-cellData[6][index]-cellData[16][index];
    weights = Lattice<ImpedanceFloat>::t(7)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(16);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

         if(std::abs(c2) < eps)
         {
            //  converged = true;
             break;
         }

         ImpedanceFloat uIncrement = c1/c2;

         if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
         {
             relaxationFactor *=0.5;
         }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = 0.001*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellData[11][index] = cellData[2][index] + rho*2./6.*uNew[1];
    cellData[5][index] = cellData[14][index] + rho*2./12.*(-uNew[0] + uNew[1]);
    cellData[13][index] = cellData[4][index] + rho*2./12.*( uNew[0] + uNew[1]);
    cellData[17][index] = cellData[8][index] + rho*2./12.*( uNew[1] + uNew[2]);
    cellData[18][index] = cellData[9][index] + rho*2./12.*( uNew[1] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellData[i][index] -= Lattice<ImpedanceFloat>::t(i);
  }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellData[i][index] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = 1.0;
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = 0.0;

    ImpedanceFloat rhoWall = cellData[0][index] + cellData[1][index] + cellData[3][index] + cellData[6][index] +
          cellData[7][index] + cellData[10][index] + cellData[12][index] + cellData[15][index] + cellData[16][index] +
          2.0*(cellData[5][index] + cellData[11][index] + cellData[13][index] + cellData[17][index] + cellData[18][index]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellData[10][index]+cellData[15][index]+cellData[16][index]-cellData[1][index]-cellData[6][index]-cellData[7][index];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(16)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(7);
    temp[0] *= csSq/weights;

    temp[2] = cellData[7][index]+cellData[12][index]+cellData[15][index]-cellData[3][index]-cellData[6][index]-cellData[16][index];
    weights = Lattice<ImpedanceFloat>::t(7)+Lattice<ImpedanceFloat>::t(12)+Lattice<ImpedanceFloat>::t(15)+Lattice<ImpedanceFloat>::t(3)+Lattice<ImpedanceFloat>::t(6)+Lattice<ImpedanceFloat>::t(16);
    temp[2] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = 0.001*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellData[2][index] = cellData[11][index] - rho*2./6.* uNew[1];
    cellData[14][index] = cellData[5][index] - rho*2./12.*(-uNew[0] + uNew[1]);
    cellData[4][index] = cellData[13][index] - rho*2./12.*( uNew[0] + uNew[1]);
    cellData[8][index] = cellData[17][index] - rho*2./12.*( uNew[1] + uNew[2]);
    cellData[9][index] = cellData[18][index] - rho*2./12.*( uNew[1] - uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellData[i][index] -= Lattice<ImpedanceFloat>::t(i);
  }
}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,2,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellData[i][index] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = 1.0;
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = 0.0;

    ImpedanceFloat rhoWall = 0;

    rhoWall += cellData[0][index] + cellData[1][index] + cellData[2][index] + cellData[4][index] +
          cellData[5][index] + cellData[10][index] + cellData[11][index] + cellData[13][index] + cellData[14][index] +
          2.0*(cellData[3][index] + cellData[6][index] + cellData[8][index] + cellData[16][index] + cellData[18][index]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellData[10][index]+cellData[13][index]+cellData[14][index]-cellData[1][index]-cellData[4][index]-cellData[5][index];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(14)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(5);
    temp[0] *= csSq/weights;

    temp[1] = cellData[5][index]+cellData[11][index]+cellData[13][index]-cellData[2][index]-cellData[4][index]-cellData[14][index];
    weights = Lattice<ImpedanceFloat>::t(5)+Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(14);
    temp[1] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = 0.001*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellData[12][index] = cellData[3][index] + rho*2./6.*uNew[2];
    cellData[7][index] = cellData[16][index] + rho*2./12.*(-uNew[0] + uNew[2]);
    cellData[9][index] = cellData[18][index] + rho*2./12.*(-uNew[1] + uNew[2]);
    cellData[15][index] = cellData[6][index] + rho*2./12.*( uNew[0] + uNew[2]);
    cellData[17][index] = cellData[8][index] + rho*2./12.*( uNew[1] + uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellData[i][index] -= Lattice<ImpedanceFloat>::t(i);
  }

}

template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryFixedRefProcessor3D<T,Lattice,2,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const ImpedanceFloat cs = 1/std::sqrt(Lattice<ImpedanceFloat>::invCs2());
    const ImpedanceFloat csSq = 1/Lattice<ImpedanceFloat>::invCs2();

    for(unsigned int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
    {
        cellData[i][index] += Lattice<ImpedanceFloat>::t(i);
    }

    const ImpedanceFloat rhoOld = 1.0;
    ImpedanceFloat uOld[Lattice<ImpedanceFloat>::d];

    for(unsigned int iDim=0; iDim < Lattice<ImpedanceFloat>::d; ++iDim)
      uOld[iDim] = 0.0;

    ImpedanceFloat rhoWall = cellData[0][index] + cellData[1][index] + cellData[2][index] + cellData[4][index] +
          cellData[5][index] + cellData[10][index] + cellData[11][index] + cellData[13][index] + cellData[14][index] +
          2.0*(cellData[7][index] + cellData[9][index] + cellData[12][index] + cellData[15][index] + cellData[17][index]);

    ImpedanceFloat temp[Lattice<ImpedanceFloat>::d] = {0};
    ImpedanceFloat weights = 0;


    temp[0] = cellData[10][index]+cellData[13][index]+cellData[14][index]-cellData[1][index]-cellData[4][index]-cellData[5][index];
    weights = Lattice<ImpedanceFloat>::t(10)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(14)+Lattice<ImpedanceFloat>::t(1)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(5);
    temp[0] *= csSq/weights;

    temp[1] = cellData[5][index]+cellData[11][index]+cellData[13][index]-cellData[2][index]-cellData[4][index]-cellData[14][index];
    weights = Lattice<ImpedanceFloat>::t(5)+Lattice<ImpedanceFloat>::t(11)+Lattice<ImpedanceFloat>::t(13)+Lattice<ImpedanceFloat>::t(2)+Lattice<ImpedanceFloat>::t(4)+Lattice<ImpedanceFloat>::t(14);
    temp[1] *= csSq/weights;

    ImpedanceFloat uNew[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaU[Lattice<ImpedanceFloat>::d];

    const ImpedanceFloat rhoOverRhoWall = rhoOld/rhoWall;

    uNew[direction] = uOld[direction] +orientation*(csSq*rhoOverRhoWall + cs) -orientation*std::sqrt( std::pow(csSq*rhoOverRhoWall+cs,2)
                      - 2 * csSq * (rhoOverRhoWall*(-orientation*uOld[direction] -1)+1) );

    __attribute__ ((unused)) bool isNan = false; //only used in debug mode, this suppresses a warning

    ImpedanceFloat uNewPlus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUPlus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat uNewMinus[Lattice<ImpedanceFloat>::d];
    ImpedanceFloat deltaUMinus[Lattice<ImpedanceFloat>::d];

    ImpedanceFloat variance = 0.001*uNew[direction] + eps;
    uNewPlus[direction] = uNew[direction] + variance;
    uNewMinus[direction] = uNew[direction] - variance;

    // bool converged = false;
    ImpedanceFloat uIncrementOld = 0;
    ImpedanceFloat relaxationFactor = 1;

    for(int iter = 0; iter < iterMax; ++iter)
    {
      deltaU[direction] = uNew[direction] - uOld[direction];

      ImpedanceFloat deltaUAbs = deltaU[direction]*deltaU[direction];

      deltaUPlus[direction] = uNewPlus[direction] - uOld[direction];
      deltaUMinus[direction] = uNewMinus[direction] - uOld[direction];

      ImpedanceFloat deltaUAbsPlus = deltaUPlus[direction]*deltaUPlus[direction];
      ImpedanceFloat deltaUAbsMinus = deltaUMinus[direction]*deltaUMinus[direction];

      for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      {
          uNew[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNew[direction]);

          deltaU[inPlane(counter)] = uNew[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbs += deltaU[inPlane(counter)]*deltaU[inPlane(counter)];

          uNewPlus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewPlus[direction]);
          uNewMinus[inPlane(counter)] = temp[inPlane(counter)]/rhoWall*(1+orientation*uNewMinus[direction]);

          deltaUPlus[inPlane(counter)] = uNewPlus[inPlane(counter)] - uOld[inPlane(counter)];
          deltaUMinus[inPlane(counter)] = uNewMinus[inPlane(counter)] - uOld[inPlane(counter)];

          deltaUAbsPlus += deltaUPlus[inPlane(counter)]*deltaUPlus[inPlane(counter)];
          deltaUAbsMinus += deltaUMinus[inPlane(counter)]*deltaUMinus[inPlane(counter)];

      }

      deltaUAbs = std::sqrt(deltaUAbs);

      deltaUAbsPlus = std::sqrt(deltaUAbsPlus);
      deltaUAbsMinus = std::sqrt(deltaUAbsMinus);

      ImpedanceFloat onePlusMinusU = 1+orientation*uNew[direction];

      ImpedanceFloat rho = rhoWall/onePlusMinusU;

      ImpedanceFloat signDeltaUDirection = util::scaledSign(deltaU[direction]);

      ImpedanceFloat c1 = -orientation*(rho-rhoOld)*csSq - orientation*0.5*rho*deltaUAbs*deltaUAbs + signDeltaUDirection*deltaUAbs*rho*cs;

      ImpedanceFloat onePlusMinusUPlus = 1+orientation*uNewPlus[direction];
      ImpedanceFloat onePlusMinusUMinus = 1+orientation*uNewMinus[direction];

      ImpedanceFloat rhoPlus = rhoWall/onePlusMinusUPlus;
      ImpedanceFloat rhoMinus = rhoWall/onePlusMinusUMinus;

      ImpedanceFloat signDeltaUDirectionPlus = util::scaledSign(deltaUPlus[direction]);
      ImpedanceFloat signDeltaUDirectionMinus = util::scaledSign(deltaUMinus[direction]);

      ImpedanceFloat c1Plus =  -orientation*(rhoPlus-rhoOld)*csSq - orientation*0.5*rhoPlus*deltaUAbsPlus*deltaUAbsPlus
              + signDeltaUDirectionPlus*deltaUAbsPlus*rhoPlus*cs;;
      ImpedanceFloat c1Minus = -orientation*(rhoMinus-rhoOld)*csSq - orientation*0.5*rhoMinus*deltaUAbsMinus*deltaUAbsMinus
              + signDeltaUDirectionMinus*deltaUAbsMinus*rhoMinus*cs;

      ImpedanceFloat c2 = (c1Plus - c1Minus)/(uNewPlus[direction]-uNewMinus[direction]);

      if(std::abs(c2) < eps)
      {
          // converged = true;
          break;
      }

      ImpedanceFloat uIncrement = c1/c2;

      if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
      {
          relaxationFactor *=0.5;
      }

      uNew[direction] -= relaxationFactor*uIncrement;

      uIncrementOld = uIncrement;

      variance = 0.001*uNew[direction] + eps;
      uNewPlus[direction] = uNew[direction] + variance;
      uNewMinus[direction] = uNew[direction] - variance;

      if(std::abs(uIncrement) < eps)
      {
          // converged = true;
          break;
      }

      if(std::isnan(uNew[direction]))
      {
          break;
      }
    } // end for

    ImpedanceFloat rho = rhoWall/(1+orientation*uNew[direction]);

    for(int counter=0; counter<Lattice<ImpedanceFloat>::d-1; ++counter)
      uNew[inPlane(counter)] = inPlaneRelaxation*temp[inPlane(counter)]/rho;

    cellData[3][index] = cellData[12][index] - rho*2./6.* uNew[2];
    cellData[16][index] = cellData[7][index] - rho*2./12.*(-uNew[0] + uNew[2]);
    cellData[18][index] = cellData[9][index] - rho*2./12.*(-uNew[1] + uNew[2]);
    cellData[6][index] = cellData[15][index] - rho*2./12.*( uNew[0] + uNew[2]);
    cellData[8][index] = cellData[17][index] - rho*2./12.*( uNew[1] + uNew[2]);

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
    cellData[i][index] -= Lattice<ImpedanceFloat>::t(i);
  }
}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, plane, normal1, normal2>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
          ImpedanceFloat rhoOld = 1.0;//cellData[Lattice<T>::rhoIndex][index];

          for(unsigned int iPop=0; iPop<Lattice<T>::q; ++iPop)
              cellData[iPop][index] += Lattice<T>::t(iPop);

          ImpedanceFloat rhoTmp = 0;
#define DEBUG_POSTPROC 1
#ifdef DEBUG_POSTPROC
          std::stringstream debugStream;
#endif

          for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
          {
            if( !(Lattice<T>::c(iPop,direction1)+normal1) || !(Lattice<T>::c(iPop,direction2)+normal2) )
            {
                cellData[iPop][index] = cellData[Lattice<T>::opposite(iPop)][index];
#ifdef DEBUG_POSTPROC
                debugStream <<"cellData["<<iPop<<"][index]" << " = " << "cellData[" << Lattice<T>::opposite(iPop)<<"][index];"; 
#endif
                if( Lattice<T>::c(iPop,plane) )
                {
                    for(unsigned int iPopInner = 1; iPopInner<Lattice<T>::q; ++iPopInner)
                    {
                        cellData[iPop][index] -= 0.25*cellData[iPopInner][index]*Lattice<T>::c(iPopInner,plane)*Lattice<T>::c(iPop,plane)
                                           *bool( (Lattice<T>::c(iPopInner,direction1)==0 && Lattice<T>::c(iPopInner,direction2) == 0) );
#ifdef DEBUG_POSTPROC
                        if(bool( (Lattice<T>::c(iPopInner,direction1)==0 && Lattice<T>::c(iPopInner,direction2) == 0) ))
                            debugStream << " -" << Lattice<T>::c(iPopInner,plane)*Lattice<T>::c(iPop,plane)*0.25 << "*" << "cellData[" << iPopInner <<"][index];";
#endif
                    }
                }
            }

#ifdef DEBUG_POSTPROC
            debugStream << std::endl;
#endif

            if( not ( ((Lattice<T>::c(iPop,direction1) == normal1) && (Lattice<T>::c(iPop,direction2) == -normal2))
                     or ((Lattice<T>::c(iPop,direction1) == -normal1) && (Lattice<T>::c(iPop,direction2) == normal2)) ) )
            {
                rhoTmp += cellData[iPop][index];
#ifdef DEBUG_POSTPROC
                debugStream << "rhoTmp += " << "cellData[" << iPop << "][index];" << std::endl;
#endif
            }

          }

          for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
          {
            if( ((Lattice<T>::c(iPop,direction1) == normal1) && (Lattice<T>::c(iPop,direction2) == -normal2))
                    or ((Lattice<T>::c(iPop,direction1) == -normal1) && (Lattice<T>::c(iPop,direction2) == normal2)) )
            {
                cellData[iPop][index] = (1.0/14.0) * (rhoOld - rhoTmp);
#ifdef DEBUG_POSTPROC
                debugStream << "cellData[" << iPop << "][index]"<<  " = 1/14*(rhoOld-rhoTmp);" << std::endl;
#endif
            }
          }

          cellData[0][index] = (12.0/14.0) * (rhoOld - rhoTmp);

#ifdef DEBUG_POSTPROC
           debugStream << "cellData[0][index]"<< " = 12/14*(rhoOld-rhoTmp);" << std::endl;
           std::cout << debugStream.str() << std::endl;
#endif

        for(unsigned int iPop=0; iPop<Lattice<T>::q; ++iPop)
        {
            cellData[iPop][index] -= Lattice<T>::t(iPop);
        }

}

/// Specialisations fo ImpedanceBoundaryEdgeProcessor3D ////////////////////////////////////////////////////////////////
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex][index];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = cellDataCache[1];
          rhoTmp += cellDataCache[2];
          rhoTmp += cellDataCache[3];
          rhoTmp += cellDataCache[4];

          cellDataCache[5] = cellDataCache[14] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[5];
          rhoTmp += cellDataCache[6];

          cellDataCache[7] = cellDataCache[16] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[7];
          rhoTmp += cellDataCache[8];

          cellDataCache[9]=cellDataCache[18];
          
          rhoTmp += cellDataCache[10];

          cellDataCache[11]=cellDataCache[2];

          rhoTmp += cellDataCache[11];

          cellDataCache[12]=cellDataCache[3];

          rhoTmp += cellDataCache[12];

          cellDataCache[13] = cellDataCache[4] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[13];
          rhoTmp += cellDataCache[14];

          cellDataCache[15] = cellDataCache[6] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[15];
          rhoTmp += cellDataCache[16];

          cellDataCache[17]=cellDataCache[8];

          rhoTmp += cellDataCache[17];

          cellDataCache[9]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[18]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, -1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = cellDataCache[1];
          rhoTmp += cellDataCache[2];
          cellDataCache[3]=cellDataCache[12];
          rhoTmp += cellDataCache[3];
          rhoTmp += cellDataCache[4];

          cellDataCache[5] = cellDataCache[14] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[5];

          cellDataCache[6] = cellDataCache[15] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[6];

          rhoTmp += cellDataCache[7];

          cellDataCache[8]=cellDataCache[17];
          
          rhoTmp += cellDataCache[9];
          rhoTmp += cellDataCache[10];

          cellDataCache[11]=cellDataCache[2];

          rhoTmp += cellDataCache[11];
          rhoTmp += cellDataCache[12];

          cellDataCache[13] = cellDataCache[4] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[13];
          rhoTmp += cellDataCache[14];
          rhoTmp += cellDataCache[15];

          cellDataCache[16] = cellDataCache[7] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[16];

          cellDataCache[17]=cellDataCache[8];
          cellDataCache[18]=cellDataCache[9];

          rhoTmp += cellDataCache[18];

          cellDataCache[8]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[17]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = cellDataCache[1];
          cellDataCache[2]=cellDataCache[11];
          rhoTmp += cellDataCache[2];
          cellDataCache[3]=cellDataCache[12];
          rhoTmp += cellDataCache[3];

          cellDataCache[4] = cellDataCache[13] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[4];
          rhoTmp += cellDataCache[5];

          cellDataCache[6] = cellDataCache[15] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[6];

          rhoTmp += cellDataCache[7];

          cellDataCache[8]=cellDataCache[17];
          
          rhoTmp += cellDataCache[8];

          cellDataCache[9]=cellDataCache[18];

          rhoTmp += cellDataCache[10];
          rhoTmp += cellDataCache[11];
          rhoTmp += cellDataCache[12];
          rhoTmp += cellDataCache[13];

          cellDataCache[14] = cellDataCache[5] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[14];
          rhoTmp += cellDataCache[15];

          cellDataCache[16] = cellDataCache[7] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[16];
          rhoTmp += cellDataCache[17];

          cellDataCache[9]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[18]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 0, 1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = cellDataCache[1];
          cellDataCache[2]=cellDataCache[11];
          rhoTmp += cellDataCache[2];
          rhoTmp += cellDataCache[3];

          cellDataCache[4] = cellDataCache[13] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[4];
          rhoTmp += cellDataCache[5];
          rhoTmp += cellDataCache[6];

          cellDataCache[7] = cellDataCache[16] - 0.25 *cellDataCache[1] + 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[7];

          cellDataCache[8]=cellDataCache[17];
          cellDataCache[9]=cellDataCache[18];

          rhoTmp += cellDataCache[9];
          rhoTmp += cellDataCache[10];
          rhoTmp += cellDataCache[11];

          cellDataCache[12]=cellDataCache[3];

          rhoTmp += cellDataCache[12];
          rhoTmp += cellDataCache[13];

          cellDataCache[14] = cellDataCache[5] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[14];

          cellDataCache[15] = cellDataCache[6] + 0.25 *cellDataCache[1] - 0.25*cellDataCache[10];

          rhoTmp += cellDataCache[15];
          rhoTmp += cellDataCache[16];
          rhoTmp += cellDataCache[18];

          cellDataCache[8]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[17]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = cellDataCache[1];
          rhoTmp += cellDataCache[2];
          rhoTmp += cellDataCache[3];
          rhoTmp += cellDataCache[4];
          rhoTmp += cellDataCache[5];
          rhoTmp += cellDataCache[6];

          cellDataCache[7]=cellDataCache[16];

          rhoTmp += cellDataCache[8];

          cellDataCache[9] = cellDataCache[18] - 0.25 *cellDataCache[2] + 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[9];

          cellDataCache[10]=cellDataCache[1];

          rhoTmp += cellDataCache[10];
          rhoTmp += cellDataCache[11];

          cellDataCache[12]=cellDataCache[3];

          rhoTmp += cellDataCache[12];

          cellDataCache[13] = cellDataCache[4] + 0.25 *cellDataCache[2] - 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[13];

          cellDataCache[14] = cellDataCache[5] - 0.25 *cellDataCache[2] + 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[14];

          cellDataCache[15]=cellDataCache[6];

          rhoTmp += cellDataCache[15];

          cellDataCache[16]=cellDataCache[7];

          cellDataCache[17] = cellDataCache[8] + 0.25 *cellDataCache[2] - 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[17];
          rhoTmp += cellDataCache[18];

          cellDataCache[7]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[16]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1, -1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex][index];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          cellDataCache[1]=cellDataCache[10];
          ImpedanceFloat rhoTmp = cellDataCache[1];
          rhoTmp += cellDataCache[2];
          rhoTmp += cellDataCache[3];

          cellDataCache[4] = cellDataCache[13] - 0.25 *cellDataCache[2] + 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[4];

          cellDataCache[5] = cellDataCache[14] + 0.25 *cellDataCache[2] - 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[5];

          cellDataCache[6]=cellDataCache[15];
          cellDataCache[7]=cellDataCache[16];

          rhoTmp += cellDataCache[7];
          rhoTmp += cellDataCache[8];

          cellDataCache[9] = cellDataCache[18] - 0.25 *cellDataCache[2] + 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[9];
          rhoTmp += cellDataCache[10];
          rhoTmp += cellDataCache[11];

          cellDataCache[12]=cellDataCache[3];

          rhoTmp += cellDataCache[12];


          rhoTmp += cellDataCache[13];


          rhoTmp += cellDataCache[14];

          cellDataCache[15]=cellDataCache[6];

          rhoTmp += cellDataCache[16];

          cellDataCache[17] = cellDataCache[8] + 0.25 *cellDataCache[2] - 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[17];
          rhoTmp += cellDataCache[18];

          cellDataCache[6]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[15]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1,1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex][index];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          cellDataCache[1]=cellDataCache[10];
          ImpedanceFloat rhoTmp = cellDataCache[1];
          rhoTmp += cellDataCache[2];

          cellDataCache[3]=cellDataCache[12];
          rhoTmp += cellDataCache[3];

          cellDataCache[4] = cellDataCache[13] - 0.25 *cellDataCache[2] + 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[4];

          cellDataCache[5] = cellDataCache[14] + 0.25 *cellDataCache[2] - 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[5];

          cellDataCache[6]=cellDataCache[15];

          rhoTmp += cellDataCache[6];

          cellDataCache[7]=cellDataCache[16];

          cellDataCache[8] = cellDataCache[17] - 0.25 *cellDataCache[2] + 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[8];
          rhoTmp += cellDataCache[9];
          rhoTmp += cellDataCache[10];
          rhoTmp += cellDataCache[11];
          rhoTmp += cellDataCache[12];
          rhoTmp += cellDataCache[13];
          rhoTmp += cellDataCache[14];
          rhoTmp += cellDataCache[15];

          cellDataCache[16]=cellDataCache[7];

          rhoTmp += cellDataCache[17];

          cellDataCache[18] = cellDataCache[9] + 0.25 *cellDataCache[2] - 0.25*cellDataCache[11];

          rhoTmp += cellDataCache[18];

          cellDataCache[7]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[16]=1.0/14.0*(rhoOld-rhoTmp);
          cellDataCache[0]=12.0/14.0*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 1,1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = 0.0;
          rhoTmp += cellDataCache[1];

          rhoTmp += cellDataCache[2];
           cellDataCache[3] = cellDataCache[12];
          rhoTmp += cellDataCache[3];

          rhoTmp += cellDataCache[4];

          rhoTmp += cellDataCache[5];
          cellDataCache[6] = cellDataCache[15];

          rhoTmp += cellDataCache[7];
          cellDataCache[8] = cellDataCache[17] -0.25*cellDataCache[2] +0.25*cellDataCache[11];
          rhoTmp += cellDataCache[8];

          rhoTmp += cellDataCache[9];
          cellDataCache[10] = cellDataCache[1];
          rhoTmp += cellDataCache[10];

          rhoTmp += cellDataCache[11];

          rhoTmp += cellDataCache[12];
          cellDataCache[13] = cellDataCache[4] +0.25*cellDataCache[2] -0.25*cellDataCache[11];
          rhoTmp += cellDataCache[13];
          cellDataCache[14] = cellDataCache[5] -0.25*cellDataCache[2] +0.25*cellDataCache[11];
          rhoTmp += cellDataCache[14];
          cellDataCache[15] = cellDataCache[6];
          cellDataCache[16] = cellDataCache[7];
          rhoTmp += cellDataCache[16];

          rhoTmp += cellDataCache[17];
          cellDataCache[18] = cellDataCache[9] +0.25*cellDataCache[2] -0.25*cellDataCache[11];
          rhoTmp += cellDataCache[18];
          cellDataCache[6] = 1./14.*(rhoOld-rhoTmp);
          cellDataCache[15] = 1./14.*(rhoOld-rhoTmp);
          cellDataCache[0] = 12./14.*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2,-1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

        ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex][index];

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
          cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

        ImpedanceFloat rhoTmp = 0.0;
        rhoTmp += cellDataCache[1];

        rhoTmp += cellDataCache[2];

        rhoTmp += cellDataCache[3];

        rhoTmp += cellDataCache[4];

        cellDataCache[5] = cellDataCache[14];

        rhoTmp += cellDataCache[6];

        rhoTmp += cellDataCache[7];

        rhoTmp += cellDataCache[8];

        rhoTmp += cellDataCache[9];

        cellDataCache[10] = cellDataCache[1];
        rhoTmp += cellDataCache[10];
        cellDataCache[11] = cellDataCache[2];
        rhoTmp += cellDataCache[11];

        rhoTmp += cellDataCache[12];
        cellDataCache[13] = cellDataCache[4];
        rhoTmp += cellDataCache[13];
        cellDataCache[14] = cellDataCache[5];
        cellDataCache[15] = cellDataCache[6] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
        rhoTmp += cellDataCache[15];
        cellDataCache[16] = cellDataCache[7] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
        rhoTmp += cellDataCache[16];
        cellDataCache[17] = cellDataCache[8] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
        rhoTmp += cellDataCache[17];
        cellDataCache[18] = cellDataCache[9] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
        rhoTmp += cellDataCache[18];
        cellDataCache[5] = 1./14.*(rhoOld-rhoTmp);
        cellDataCache[14] = 1./14.*(rhoOld-rhoTmp);
        cellDataCache[0] = 12./14.*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2,-1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = 0.0;
            rhoTmp += cellDataCache[1];
            cellDataCache[2] = cellDataCache[11];
            rhoTmp += cellDataCache[2];

            rhoTmp += cellDataCache[3];
            cellDataCache[4] = cellDataCache[13];

            rhoTmp += cellDataCache[5];

            rhoTmp += cellDataCache[6];

            rhoTmp += cellDataCache[7];
            cellDataCache[8] = cellDataCache[17] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
            rhoTmp += cellDataCache[8];
            cellDataCache[9] = cellDataCache[18] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
            rhoTmp += cellDataCache[9];
            cellDataCache[10] = cellDataCache[1];
            rhoTmp += cellDataCache[10];

            rhoTmp += cellDataCache[11];

            rhoTmp += cellDataCache[12];
            cellDataCache[13] = cellDataCache[4];
            cellDataCache[14] = cellDataCache[5];
            rhoTmp += cellDataCache[14];
            cellDataCache[15] = cellDataCache[6] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
            rhoTmp += cellDataCache[15];
            cellDataCache[16] = cellDataCache[7] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
            rhoTmp += cellDataCache[16];

            rhoTmp += cellDataCache[17];

            rhoTmp += cellDataCache[18];
            cellDataCache[4] = 1./14.*(rhoOld-rhoTmp);
            cellDataCache[13] = 1./14.*(rhoOld-rhoTmp);
            cellDataCache[0] = 12./14.*(rhoOld-rhoTmp);

        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2,1,1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = 0.0;
          cellDataCache[1] = cellDataCache[10];
          rhoTmp += cellDataCache[1];
          cellDataCache[2] = cellDataCache[11];
          rhoTmp += cellDataCache[2];

          rhoTmp += cellDataCache[3];
          cellDataCache[4] = cellDataCache[13];
          rhoTmp += cellDataCache[4];
          cellDataCache[5] = cellDataCache[14];
          cellDataCache[6] = cellDataCache[15] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
          rhoTmp += cellDataCache[6];
          cellDataCache[7] = cellDataCache[16] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
          rhoTmp += cellDataCache[7];
          cellDataCache[8] = cellDataCache[17] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
          rhoTmp += cellDataCache[8];
          cellDataCache[9] = cellDataCache[18] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
          rhoTmp += cellDataCache[9];

          rhoTmp += cellDataCache[10];

          rhoTmp += cellDataCache[11];

          rhoTmp += cellDataCache[12];

          rhoTmp += cellDataCache[13];
          cellDataCache[14] = cellDataCache[5];

          rhoTmp += cellDataCache[15];

          rhoTmp += cellDataCache[16];

          rhoTmp += cellDataCache[17];

          rhoTmp += cellDataCache[18];
          cellDataCache[5] = 1./14.*(rhoOld-rhoTmp);
          cellDataCache[14] = 1./14.*(rhoOld-rhoTmp);
          cellDataCache[0] = 12./14.*(rhoOld-rhoTmp);
        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
template<typename T, template<typename U> class Lattice>
template<class Dynamics>
void ImpedanceBoundaryEdgeProcessor3D<T, Lattice, 2,1,-1>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

          ImpedanceFloat rhoOld = 1.0;//cellDataCache[Lattice<ImpedanceFloat>::rhoIndex];

          for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
              cellDataCache[iPop] += Lattice<ImpedanceFloat>::t(iPop);

          ImpedanceFloat rhoTmp = 0.0;
          cellDataCache[1] = cellDataCache[10];
          rhoTmp += cellDataCache[1];

          rhoTmp += cellDataCache[2];

          rhoTmp += cellDataCache[3];
          cellDataCache[4] = cellDataCache[13];
          cellDataCache[5] = cellDataCache[14];
          rhoTmp += cellDataCache[5];
          cellDataCache[6] = cellDataCache[15] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
          rhoTmp += cellDataCache[6];
          cellDataCache[7] = cellDataCache[16] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
          rhoTmp += cellDataCache[7];

          rhoTmp += cellDataCache[8];

          rhoTmp += cellDataCache[9];

          rhoTmp += cellDataCache[10];
          cellDataCache[11] = cellDataCache[2];
          rhoTmp += cellDataCache[11];

          rhoTmp += cellDataCache[12];
          cellDataCache[13] = cellDataCache[4];

          rhoTmp += cellDataCache[14];

          rhoTmp += cellDataCache[15];

          rhoTmp += cellDataCache[16];
          cellDataCache[17] = cellDataCache[8] +0.25*cellDataCache[3] -0.25*cellDataCache[12];
          rhoTmp += cellDataCache[17];
          cellDataCache[18] = cellDataCache[9] -0.25*cellDataCache[3] +0.25*cellDataCache[12];
          rhoTmp += cellDataCache[18];
          cellDataCache[4] = 1./14.*(rhoOld-rhoTmp);
          cellDataCache[13] = 1./14.*(rhoOld-rhoTmp);
          cellDataCache[0] = 12./14.*(rhoOld-rhoTmp);


        for(unsigned int iPop=0; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
        {
            cellDataCache[iPop] -= Lattice<ImpedanceFloat>::t(iPop);
        }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<T>( cellDataCache[i] );


}
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
template<class Dynamics>
void ImpedanceBoundaryCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    const unsigned dimension = Lattice<ImpedanceFloat>::q+1+Lattice<ImpedanceFloat>::d;
    ImpedanceFloat cellDataCache [dimension];
    for (unsigned i=0;i<dimension;++i)
      cellDataCache[i] = cellData[i][index];

  const ImpedanceFloat normalVector[3] = {xNormal,yNormal,zNormal};


  ImpedanceFloat rhoTmp{0};
  ImpedanceFloat rho = 1.0;

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
      cellDataCache[i] += Lattice<ImpedanceFloat>::t(i);
  }

  for(unsigned int iPop=1; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
  {
      int isParallelToNormal = 0;

      for(unsigned int iDim = 0; iDim<Lattice<ImpedanceFloat>::d; ++iDim)
      {
          if(Lattice<ImpedanceFloat>::c(iPop,iDim) == -normalVector[iDim])
              isParallelToNormal += 1;
          else if(Lattice<ImpedanceFloat>::c(iPop,iDim) == normalVector[iDim])
              isParallelToNormal -= 1;
      }

      if(isParallelToNormal > 0)
      {
          cellDataCache[iPop] = cellDataCache[Lattice<ImpedanceFloat>::opposite(iPop)];
          rhoTmp += 2*cellDataCache[iPop];
      }
  }


  unsigned int firstTangential = 0;

  for(unsigned int iPop = 1; iPop<Lattice<ImpedanceFloat>::q; ++iPop)
  {
      unsigned int isTangentialToEdge = 0;

      for(unsigned int iDim=0; iDim<Lattice<ImpedanceFloat>::d; ++iDim)
      {
          if(Lattice<ImpedanceFloat>::c(iPop,iDim) == -normalVector[iDim])
              isTangentialToEdge += 1;
          else if(Lattice<ImpedanceFloat>::c(iPop,iDim) == normalVector[iDim])
              isTangentialToEdge += 3;
      }

      if(isTangentialToEdge == 4)
      {
          cellDataCache[iPop] = ImpedanceFloat{1}/ImpedanceFloat{18}*(rho-rhoTmp);
          if(!firstTangential)
              firstTangential = iPop;
      }
  }
  cellDataCache[0] = cellDataCache[firstTangential]*ImpedanceFloat{12};

  for(int i=0; i<Lattice<ImpedanceFloat>::q; ++i)
  {
      cellDataCache[i] -= Lattice<ImpedanceFloat>::t(i);
  }
      for (unsigned i=0;i<dimension;++i)
        cellData[i][index] = static_cast<ImpedanceFloat>( cellDataCache[i] );


}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void ImpedanceBoundaryProcessor3DIncompressible<T,Lattice,direction,orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
        size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{

    const T cs = 1.0/std::sqrt(3.0);
    const T csSq = 1.0/3.0;
    const T temp = csSq + cs;

//    std::stringstream debugStream;
//    debugStream << "direction: " << direction << "     orientation: " << orientation << std::endl;
    T uOld[Lattice<T>::d];

    for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
        uOld[iDim] = cellData[Lattice<T>::uIndex+iDim][index];

    const T rhoOld = cellData[Lattice<T>::rhoIndex][index];

    for(int i=0; i<Lattice<T>::q; ++i)
    {
      cellData[i][index] += Lattice<T>::t(i);
//      debugStream << impedanceBoundaryHelpers::f[i] << ": " << cellData[i][index] << std::endl;
    }

//    debugStream << std::endl;

    __attribute__ ((unused)) T rhoNew{0};
    T uNew[3] = {0};

    T rhoInPlane{0};
    T deltaUInPlaneSq[3] = {0};

//    debugStream << "rhoInPlane = ";

    for(int i=0; i<Lattice<T>::q; ++i)
    {
        rhoInPlane += ( (1-std::abs(Lattice<T>::c(i,direction)))+2.0*!(1-orientation*Lattice<T>::c(i,direction)) )*cellData[i][index];
//        if(( (1-std::abs(Lattice<T>::c(i,direction)))+2.0*!(1-orientation*Lattice<T>::c(i,direction)) ))
//            debugStream << ( (1-std::abs(Lattice<T>::c(i,direction)))+2.0*!(1-orientation*Lattice<T>::c(i,direction)) ) << "*" << impedanceBoundaryHelpers::f[i] << " + ";
    }

//    debugStream << std::endl;

//    debugStream << "uNew[inPlane] = ";

    uNew[direction] = uOld[direction] +orientation*temp -orientation*std::sqrt(temp*temp+2.0*csSq*(rhoOld+orientation*uOld[direction]-rhoInPlane));
    for(int i=0; i<Lattice<T>::q; ++i)
    {
        uNew[inPlane(0)] += 1.5* cellData[i][index]*Lattice<T>::c(i,inPlane(0))*(1-std::abs(Lattice<T>::c(i,direction)));

//        if(Lattice<T>::c(i,inPlane(0))*(1-std::abs(Lattice<T>::c(i,direction))))
//            debugStream << Lattice<T>::c(i,inPlane(0))*(1-std::abs(Lattice<T>::c(i,direction))) <<"*1.5*" << impedanceBoundaryHelpers::f[i] << " + ";

        uNew[inPlane(1)] += 1.5* cellData[i][index]*Lattice<T>::c(i,inPlane(1))*(1-std::abs(Lattice<T>::c(i,direction)));
    }
//    debugStream << std::endl;

    deltaUInPlaneSq[inPlane(0)] = (uNew[inPlane(0)]-uOld[inPlane(0)])*(uNew[inPlane(0)]-uOld[inPlane(0)]);
    deltaUInPlaneSq[inPlane(1)] = (uNew[inPlane(1)]-uOld[inPlane(1)])*(uNew[inPlane(1)]-uOld[inPlane(1)]);

    // bool converged = false;

    T uIncrementOld = 0;
    T relaxationFactor = 1;
    bool isNan = false;

//
//    debugStream << "uNew init: " << uNew[direction] << std::endl;
//    debugStream << "uNew[" << inPlane(0) << "]:" << uNew[inPlane(0)] << std::endl;
//    debugStream << "uNew[" << inPlane(1) << "]:" << uNew[inPlane(1)] << std::endl;
//    debugStream << "rhoInPlane: " << rhoInPlane << std::endl;
//    debugStream << "uOld: " << uOld[0] << ", " << uOld[1] << ", " << uOld[2] << std::endl;
//    debugStream << "rhoOld: " << rhoOld << std::endl << std::endl;

    if ( sqrt( deltaUInPlaneSq[inPlane(0)]+deltaUInPlaneSq[inPlane(1)] ) > eps )
    {

        for(int iter=0; iter<iterMax; ++iter)
        {

          T deltaUNormal = uNew[direction] - uOld[direction];
          T deltaUNormalSq = deltaUNormal*deltaUNormal;

          T variance = 0.001*uNew[direction] + eps;
          T uNewPlus = uNew[direction] + variance;
          T uNewMinus = uNew[direction] - variance;

          T deltaUNormalPlus = uNewPlus - uOld[direction];
          T deltaUNormalMinus = uNewMinus - uOld[direction];

          T deltaUNormalSqPlus = deltaUNormalPlus*deltaUNormalPlus;
          T deltaUNormalSqMinus = deltaUNormalMinus*deltaUNormalMinus;

          T signdvZ = util::scaledSign(deltaUNormal);
    //      debugStream << deltaUNormalSq << " " << deltaUInPlaneSq[inPlane(0)] << " " << deltaUInPlaneSq[inPlane(1)];
          T deltaUAbs = sqrt(deltaUNormalSq+deltaUInPlaneSq[inPlane(0)]+deltaUInPlaneSq[inPlane(1)]);

          T deltaUAbsPlus = sqrt(deltaUNormalSqPlus+deltaUInPlaneSq[inPlane(0)]+deltaUInPlaneSq[inPlane(1)]);
          T deltaUAbsMinus = sqrt(deltaUNormalSqMinus+deltaUInPlaneSq[inPlane(0)]+deltaUInPlaneSq[inPlane(1)]);

          T c1 = -((deltaUAbs -orientation*signdvZ*cs)*deltaUNormal -orientation*csSq*deltaUAbs);

          if (fabs(c1)<eps)
          {
            // converged = true;
            break;
          }

    //      T c2 = (csSq * (rhoInPlane -orientation*uNew[direction] - rhoOld) + deltaUAbs*(-orientation*signdvZ*cs + 0.5*deltaUAbs)) * deltaUAbs;
          T rho = rhoInPlane/(1+orientation*uNew[direction]);
          T c2 = -orientation*(rho - rhoOld)*csSq-orientation*0.5*deltaUAbs*deltaUAbs+signdvZ*deltaUAbs*cs;

          if( fabs(c2) < eps)
          {
              // converged = true;
              break;
          }
    //
    //      debugStream << -orientation*(rho - rhoOld)*csSq << std::endl;
    //      debugStream << -orientation*0.5*deltaUAbs*deltaUAbs << std::endl;
    //      debugStream << signdvZ*deltaUAbs*cs << std::endl;

          T rhoPlus = rhoInPlane/(1+orientation*uNewPlus);
          T rhoMinus = rhoInPlane/(1+orientation*uNewMinus);
          T c2Plus = -orientation*(rhoPlus - rhoOld)*csSq-orientation*0.5*deltaUAbsPlus*deltaUAbsPlus+signdvZ*deltaUAbsPlus*cs;
          T c2Minus = -orientation*(rhoMinus - rhoOld)*csSq-orientation*0.5*deltaUAbsMinus*deltaUAbsMinus+signdvZ*deltaUAbsMinus*cs;

          c1 = (c2Plus-c2Minus)/(2*variance);

    //      debugStream << "c2 : " << c2 << "   c2' : " << c1 << std::endl;
    //      debugStream << "c2Plus: " << c2Plus << "   c2Minus: " << c2Minus << std::endl;
    //      debugStream << "Variance: " << variance << std::endl;

          T uIncrement = c2/c1;


          if( uIncrementOld && (std::signbit(uIncrement) != std::signbit(uIncrementOld)))
          {
              relaxationFactor *=0.5;
          }

    //      debugStream << "uNew[t]: " << uNew[direction] << std::endl;

          uNew[direction] -= relaxationFactor*uIncrement;

    //      debugStream << "relaxFac: " << relaxationFactor << std::endl;
    //      debugStream << "delta U: " << deltaUAbs << "   rho: " << rho << "   sign(vx): " << signdvZ << std::endl;
    //      debugStream << "uIncrement: " << uIncrement << std::endl;
    //      debugStream << "uNew[t+1]: " << uNew[direction] << std::endl << std::endl;

          uIncrementOld = uIncrement;

          if (fabs(uIncrement)<eps)
          {
            // converged = true;
            break;
          }

          if( std::isnan(uNew[direction]))
          {
              isNan = true;
              break;
          }

        }
    }

    if(isNan)
    {
//        std::cout << debugStream.str();
//        std::exit(33);

    }

    rhoNew = rhoInPlane -orientation*uNew[direction];
    uNew[inPlane(0)]*=inPlaneRelaxation;
    uNew[inPlane(1)]*=inPlaneRelaxation;

    for(int i=0; i<Lattice<T>::q; ++i)
    {
//        debugStream << impedanceBoundaryHelpers::f[i] << " = ";
        cellData[i][index] = !(1+orientation*Lattice<T>::c(i,direction))
              *(
                  cellData[Lattice<T>::opposite(i)][index]
                   + 1/T{6}*(
                    (2-std::abs(Lattice<T>::c(i,inPlane(0)))-std::abs(Lattice<T>::c(i,inPlane(1))))*Lattice<T>::c(i,direction)*uNew[direction]
                   + Lattice<T>::c(i,inPlane(0))*uNew[inPlane(0)]
                   + Lattice<T>::c(i,inPlane(1))*uNew[inPlane(1)])
               )
              + bool((1+orientation*Lattice<T>::c(i,direction)))*cellData[i][index];

//        if( !(1+orientation*Lattice<T>::c(i,direction)) )
//            debugStream << impedanceBoundaryHelpers::f[Lattice<T>::opposite(i)] << " + " <<
//            (2-std::abs(Lattice<T>::c(i,inPlane(0)))-std::abs(Lattice<T>::c(i,inPlane(1))))*Lattice<T>::c(i,direction) << "*uNew[" << direction << "]" <<
//                               " + " << Lattice<T>::c(i,inPlane(0)) << "*uNew[" << inPlane(0) << "]" <<
//                               " + " << Lattice<T>::c(i,inPlane(1)) << "*uNew[" << inPlane(1) << "]" << std::endl;
//        else
//            debugStream << impedanceBoundaryHelpers::f[i] << std::endl;



    }

    for(int i=0; i<Lattice<T>::q; ++i)
      cellData[i][index] -= Lattice<T>::t(i);

//    T rhoTMp = 0;
//
//    for(int i=0; i<Lattice<T>::q; ++i)
//        rhoTMp += cellData[i][index];
//
//    rhoTMp += 1;
//    if(rhoTMp > 1.1)
//    {
//        std::cout << "i am here" << std::endl;
//        std::cout << debugStream.str();
//        std::exit(33);
//    }

}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<class Dynamics>
void PeriodicBoundaryProcessor3D<T, Lattice, direction, orientation>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t oneDimensionalOffset = 1;

    for(unsigned int iDirection = 2; iDirection > direction; --iDirection)
        oneDimensionalOffset *= indexCalcHelper[iDirection];

    oneDimensionalOffset *= indexCalcHelper[direction]-1;

    size_t indices[3];
    util::getCellIndices3D(index,indexCalcHelper[1],indexCalcHelper[2],indices);

    size_t indexOpposite = index;

    switch(orientation)
    {
        case 1:
            indexOpposite -= oneDimensionalOffset;
            break;
        case -1:
            indexOpposite += oneDimensionalOffset;
            break;
        default:
            printf("Periodic boundary with no orientation detected! Fix your code!\n");
    }

    for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
       if(Lattice<T>::c(iPop,direction) == -orientation)
       {
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }

}

template<typename T, template<typename U> class Lattice, int plane, int normal1, int normal2>
template<class Dynamics>
void PeriodicBoundaryEdgeProcessor3D<T, Lattice, plane, normal1, normal2>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t oneDimensionalOffset1 = 1;
    size_t oneDimensionalOffset2 = 1;

    for(unsigned int iDirection = 2; iDirection > direction1; --iDirection)
        oneDimensionalOffset1 *= indexCalcHelper[iDirection];

    for(unsigned int iDirection = 2; iDirection > direction2; --iDirection)
        oneDimensionalOffset2 *= indexCalcHelper[iDirection];

    oneDimensionalOffset1 *= indexCalcHelper[direction1]-1;
    oneDimensionalOffset2 *= indexCalcHelper[direction2]-1;

    size_t indexOppositeNormal1 = index;
    size_t indexOppositeNormal2 = index;
    size_t indexOppositeDiagonal = index;

    switch(normal1)
    {
        case 1:
            indexOppositeNormal1 -= oneDimensionalOffset1;
            indexOppositeDiagonal -= oneDimensionalOffset1;
            break;
        case -1:
            indexOppositeNormal1 += oneDimensionalOffset1;
            indexOppositeDiagonal += oneDimensionalOffset1;
            break;
        default:
            printf("Periodic boundary with no orientation detected! Fix your code!\n");
    }

    switch(normal2)
    {
        case 1:
            indexOppositeNormal2 -= oneDimensionalOffset2;
            indexOppositeDiagonal -= oneDimensionalOffset2;
            break;
        case -1:
            indexOppositeNormal2 += oneDimensionalOffset2;
            indexOppositeDiagonal += oneDimensionalOffset2;
            break;
        default:
            printf("Periodic boundary with no orientation detected! Fix your code!\n");
    }

    for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
    {
       if(Lattice<T>::c(iPop,direction1) == -normal1 && Lattice<T>::c(iPop,direction2) == -normal2)
       {
           cellData[iPop][index] = cellData[iPop][indexOppositeDiagonal];
       }
       else if(Lattice<T>::c(iPop,direction1) == -normal1)
       {
           cellData[iPop][index] = cellData[iPop][indexOppositeNormal1];
       }
       else if(Lattice<T>::c(iPop,direction2) == -normal2)
       {
           cellData[iPop][index] = cellData[iPop][indexOppositeNormal2];
       }
    }

}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal, int zNormal>
template<class Dynamics>
void PeriodicBoundaryCornerProcessor3D<T, Lattice, xNormal, yNormal, zNormal>::
process(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            T const * const OPENLB_RESTRICT collisionData,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
            size_t index, size_t position, size_t indexCalcHelper[Lattice<T>::d])
{
    size_t oneDimensionalOffset1 = 1;
    size_t oneDimensionalOffset2 = 1;
    size_t oneDimensionalOffset3 = 1;

    for(unsigned int iDirection = 2; iDirection > 0; --iDirection)
        oneDimensionalOffset1 *= indexCalcHelper[iDirection];

    for(unsigned int iDirection = 2; iDirection > 1; --iDirection)
        oneDimensionalOffset2 *= indexCalcHelper[iDirection];

    oneDimensionalOffset1 *= indexCalcHelper[0]-1;
    oneDimensionalOffset2 *= indexCalcHelper[1]-1;
    oneDimensionalOffset3 *= indexCalcHelper[2]-1;


    for(unsigned int iPop=1; iPop<Lattice<T>::q; ++iPop)
    {
       size_t indexOpposite;
       if(Lattice<T>::c(iPop,0) == -xNormal && Lattice<T>::c(iPop,1) == -yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,0) == -xNormal && Lattice<T>::c(iPop,2) == -zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1)-zNormal*static_cast<long long>(oneDimensionalOffset3));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,1) == -yNormal && Lattice<T>::c(iPop,2) == -zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-yNormal*static_cast<long long>(oneDimensionalOffset2)-zNormal*static_cast<long long>(oneDimensionalOffset3));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,0) == -xNormal && Lattice<T>::c(iPop,1) == yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,0) == xNormal && Lattice<T>::c(iPop,1) == -yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,0) == -xNormal && Lattice<T>::c(iPop,2) == zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,0) == xNormal && Lattice<T>::c(iPop,2) == -zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-zNormal*static_cast<long long>(oneDimensionalOffset3));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,1) == -yNormal && Lattice<T>::c(iPop,2) == zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,1) == yNormal && Lattice<T>::c(iPop,2) == -zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-zNormal*static_cast<long long>(oneDimensionalOffset3));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,0) == -xNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-xNormal*static_cast<long long>(oneDimensionalOffset1));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,1) == -yNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-yNormal*static_cast<long long>(oneDimensionalOffset2));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
       else if(Lattice<T>::c(iPop,2) == -zNormal)
       {
           indexOpposite = static_cast<size_t>(static_cast<long long>(index)-zNormal*static_cast<long long>(oneDimensionalOffset3));
           cellData[iPop][index] = cellData[iPop][indexOpposite];
       }
    }
}

}  // namespace olb

#endif
