
/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau 
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Implementation of boundary cell dynamics -- header file.
 */
#ifndef FIELDEXTRACTOR_H
#define FIELDEXTRACTOR_H

namespace olb {

template<int startIndex, int numberOfEntries>
struct FieldExtractor {

  OPENLB_HOST_DEVICE
  constexpr size_t operator[] (const size_t index) const
  {
	#if defined (__CUDA_ARCH__)
    return index < numberOfEntries ? startIndex + index : 0.0/0;//throw(std::out_of_range("Index exceeds range for corresponding field"));
    #else
    return index < numberOfEntries ? startIndex + index : throw(std::out_of_range("Index exceeds range for corresponding field"));
	#endif
  }

OPENLB_HOST_DEVICE
static constexpr size_t getFieldIndex (const size_t index)
{
	#if defined (__CUDA_ARCH__)
    return index < numberOfEntries ? startIndex + index : 0.0/0;//throw(std::out_of_range("Index exceeds range for corresponding field"));
    #else
    return index < numberOfEntries ? startIndex + index : throw(std::out_of_range("Index exceeds range for corresponding field"));
	#endif
}
};



} // end of namespace
#endif

