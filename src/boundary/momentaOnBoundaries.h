/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Implementation of boundary cell dynamics -- header file.
 */
#ifndef MOMENTA_ON_BOUNDARIES_H
#define MOMENTA_ON_BOUNDARIES_H

#include "dynamics/dynamics.h"
#include "core/util.h"
#include "core/cell.h"
#include "core/config.h"
#include "fieldExtractor.h"
#include "core/metaTemplateHelpers.h"

namespace olb {

template<typename T, template<typename U> class Lattice, int direction, int orientation>
OPENLB_HOST_DEVICE
T computeRhoVelocityBM(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, size_t u_direction_index)
{
  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cellData);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cellData);

  T rho =((T)2*rhoNormal+rhoOnWall+(T)1) /
         ((T)1+(T)orientation*momentaData[u_direction_index]);

  return rho;
}
template<typename T, template<typename U> class Lattice, int direction, int orientation>
OPENLB_HOST_DEVICE
T computeRhoVelocityBM(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, size_t u_direction_index)
{
  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cellData,cellIndex);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cellData,cellIndex);

  T rho =((T)2*rhoNormal+rhoOnWall+(T)1) /
         ((T)1+(T)orientation*momentaData[u_direction_index][momentaIndex]);

  return rho;
}

template<typename T, template<typename U> class Lattice> class CellView;

/// Dirichlet condition on velocity and/or pressure
template<typename T, template<typename U> class Lattice, int dataOffset>
class DirichletBoundaryMomenta : public Momenta<T,Lattice> {
  static constexpr int numberDataEntries = 0;
};

template<typename T, template<typename U> class Lattice, int dataOffset>
class EquilibriumBM : public DirichletBoundaryMomenta<T,Lattice,dataOffset+1+Lattice<T>::d> {
public:
  static constexpr int numberDataEntries = 1+Lattice<T>::d+
  DirichletBoundaryMomenta<T,Lattice,dataOffset+1+Lattice<T>::d>::numberDataEntries;
  EquilibriumBM();
  EquilibriumBM( T rho, const T u[Lattice<T>::d] );
  OPENLB_HOST_DEVICE
  T computeRho( CellView<T,Lattice> const& cell ) const override;
  OPENLB_HOST_DEVICE
  void computeU( CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const override;
  void computeJ( CellView<T,Lattice> const& cell, T j[Lattice<T>::d] ) const override;
  OPENLB_HOST_DEVICE
  void computeStress( CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  void defineRho( CellView<T,Lattice>& cell, T rho) override;
  void defineU( CellView<T,Lattice>& cell, const T u[Lattice<T>::d]) override;
  void defineAllMomenta( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d],
                                 const T pi[util::TensorVal<Lattice<T> >::n] ) override;
private:
  T _rho;
  T _u[Lattice<T>::d];   ///< value of the velocity on the boundary

};

struct VelocityBMTag {};

/// Computation of velocity momenta on a velocity boundary
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
class VelocityBM : public VelocityBMTag {
public:
  static constexpr int numberDataEntries = Lattice<T>::d;
  /// Default Constructor: initialization to zero
  VelocityBM();
  VelocityBM(const T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const ;
  OPENLB_HOST_DEVICE
  T computeRho(CellDataArray<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex) ;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData) ;

  OPENLB_HOST_DEVICE
  void computeU ( CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  void computeU ( CellDataArray<T,Lattice> const& cell, T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeU (const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * OPENLB_RESTRICT const * OPENLB_RESTRICT momentaData, size_t momentaIndex, T u[Lattice<T>::d]) ;
  OPENLB_HOST_DEVICE
  static void computeU (const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u) ;

  void computeJ ( CellView<T,Lattice> const& cell, T j[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeJ ( const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j );
  OPENLB_HOST_DEVICE
  static void computeJ ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j );
  OPENLB_HOST_DEVICE
  void computeU(T u[Lattice<T>::d]) const;

  template<int width>
  OPENLB_HOST_DEVICE
  void computeRhoU(CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width]) const;
  OPENLB_HOST_DEVICE
  void computeRhoU ( CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  void computeRhoU ( CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex, T& rho, T u[Lattice<T>::d]) ;
  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u) ;

  void defineRho(CellView<T,Lattice>& cell, T rho) ;
  OPENLB_HOST_DEVICE
  static void defineRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex , T rho) ;
  OPENLB_HOST_DEVICE
  static void defineRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho) ;

  void defineU(CellView<T,Lattice>& cell, const T u[Lattice<T>::d]) ;
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, const T u[Lattice<T>::d]) ;
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT u) ;

  void defineAllMomenta( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d],
                                 const T pi[util::TensorVal<Lattice<T> >::n] );
  static void defineAllMomenta( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d],
                                 const T pi[util::TensorVal<Lattice<T> >::n] );
  static void defineAllMomenta( T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi);

  void defineRhoU ( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d]);
  static void defineRhoU (  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d]);
  static void defineRhoU (T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,const T rho, const T * const OPENLB_RESTRICT u);

private:
  T _u[Lattice<T>::d];   ///< value of the velocity on the boundary
  using new_u=FieldExtractor<dataOffset,Lattice<T>::d>;
  using cached_u=FieldExtractor<0,Lattice<T>::d>;
};

/// Computation of velocity momenta on a velocity boundary
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
class PressureBM {
public:
  static constexpr int numberDataEntries = Lattice<T>::d;
  /// Default Constructor: initialization to u=0, rho=1
  PressureBM();
  /// Constructor with boundary initialization
  PressureBM(const T values_[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  T computeRho(CellDataArray<T,Lattice> const& data) const;
  OPENLB_HOST_DEVICE
  T computeRho() const;
  OPENLB_HOST_DEVICE
  static T computeRho (const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex) ;
  OPENLB_HOST_DEVICE
  static T computeRho (const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData);

  template<int width>
  void computeRhoU(CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width]) const;
  OPENLB_HOST_DEVICE
  void computeRhoU ( CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d]) const;
  void computeRhoU ( CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  static void computeRhoU ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T u[Lattice<T>::d]) ;
  OPENLB_HOST_DEVICE
  static void computeRhoU (const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT  momentaData, T& rho, T * const OPENLB_RESTRICT u) ;

  OPENLB_HOST_DEVICE
  void computeU( CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  void computeU( CellDataArray<T,Lattice> const& data, T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T u[Lattice<T>::d] ) ;
  OPENLB_HOST_DEVICE
  static void computeU(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u) ;

  void computeJ( CellView<T,Lattice> const& cell, T j[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeJ( const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j);
  OPENLB_HOST_DEVICE
  static void computeJ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j);

  void defineRho(CellView<T,Lattice>& cell, T rho);
  void defineRho(T rho);
  OPENLB_HOST_DEVICE
  static void defineRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,T rho);
  OPENLB_HOST_DEVICE
  static void defineRho(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,T rho);

  void defineU(CellView<T,Lattice>& cell, const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineU( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineU(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT u);

  void defineAllMomenta( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d],
                                 const T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d], const T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi );

  void defineRhoU ( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineRhoU ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineRhoU (const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u);

private:
  /// Velocity/Density on boundary.
  /** Contains velocity on the boundary, except for values[direction] that
   * contains a prescription for the density.
   */
  T _values[Lattice<T>::d];
  using __new__values=FieldExtractor<dataOffset,Lattice<T>::d>;
  using cached_values=FieldExtractor<0,Lattice<T>::d>;
};

/// Here, the stress is computed from the particle distribution functions
template<typename T, template<typename U> class Lattice, int dataOffset>
class FreeStressBM {
public:
  static constexpr int numberDataEntries = 0;

  OPENLB_HOST_DEVICE
  void computeStress( CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeStress( CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeStress( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d],T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void computeStress( const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u,T * const OPENLB_RESTRICT pi );
};

/// Use special trick to compute u resp. rho, but compute pi from part. distr. functions
template<typename T, template<typename U> class Lattice,
         template <
           typename T_, template<typename U_> class Lattice_,
           int direction_, int orientation_ , int dataOffset_>
         class HydroBM,
         int direction, int orientation, int dataOffset>
class BasicDirichletBM : public FreeStressBM<T, Lattice, dataOffset>, public HydroBM<T,Lattice,direction,orientation, dataOffset+FreeStressBM<T,Lattice,dataOffset>::numberDataEntries> {
public:
  static constexpr int numberDataEntries = FreeStressBM<T, Lattice, dataOffset>::numberDataEntries + HydroBM<T,Lattice,direction,orientation, dataOffset+FreeStressBM<T,Lattice,dataOffset>::numberDataEntries>::numberDataEntries;
  static constexpr int dataOffset_ = dataOffset;
};

/// Computation of the stress tensor for regularized boundary
template<typename T, template<typename U> class Lattice, int direction, int orientation, int DataOffset>
class RegularizedBM  {
public:
  static constexpr int numberDataEntries = 0;
  /// Stress tensor
  OPENLB_HOST_DEVICE
  void computeStress( CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeStress( CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeStress( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d],T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void computeStress( const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u,T * const OPENLB_RESTRICT pi );
};

/// Regularized velocity boundary node
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
class RegularizedVelocityBM : public RegularizedBM<T,Lattice,direction,orientation, dataOffset>, public VelocityBM<T,Lattice,direction,orientation, dataOffset+RegularizedBM<T,Lattice,direction,orientation, dataOffset>::numberDataEntries > {
public:
  static constexpr int numberDataEntries = VelocityBM<T,Lattice,direction,orientation, dataOffset>::numberDataEntries;
  static constexpr int dataOffset_ = dataOffset; 
};

/// Regularized pressure boundary node
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
class RegularizedPressureBM : public RegularizedBM<T,Lattice,direction,orientation, dataOffset>, public PressureBM<T,Lattice,direction,orientation, dataOffset+RegularizedBM<T,Lattice,direction,orientation, dataOffset>::numberDataEntries> {
public:
  static constexpr int numberDataEntries = PressureBM<T,Lattice,direction,orientation, dataOffset>::numberDataEntries;
  static constexpr int dataOffset_ = dataOffset;
};

/// In this class, the velocity is fixed
/**
 * As opposed to VelocityBM, the pressure is however not
 * computed from a special trick on the boundary, but the
 * same way it would be in the bulk.
 */
template<typename T, template<typename U> class Lattice, int dataOffset>
class FixedVelocityBM final {
public:
  static constexpr int numberDataEntries = BulkMomenta<T,Lattice>::numberDataEntries + Lattice<T>::d;
  static constexpr int dataOffset_ = dataOffset;

  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  T computeRho(CellDataArray<T,Lattice> const& data) const ;
  OPENLB_HOST_DEVICE
  T computeRho(CellDataArray<T,Lattice> const& data, unsigned int pos) const;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData);
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex);

  OPENLB_HOST_DEVICE
  void computeU( CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  void computeU( CellDataArray<T,Lattice> const& data, T u[Lattice<T>::d] ) const;
  template<int width>
  OPENLB_HOST_DEVICE
  void computeU( T u[Lattice<T>::d][width]) const;
  OPENLB_HOST_DEVICE
  static void computeU( const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u );
  OPENLB_HOST_DEVICE
  static void computeU( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT u );


  void computeJ( CellView<T,Lattice> const& cell, T j[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeJ( const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j );
  OPENLB_HOST_DEVICE
  static void computeJ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j );

  OPENLB_HOST_DEVICE
  void computeStress( CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeStress( CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d],
                              T pi[util::TensorVal<Lattice<T> >::n] ) const ;
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi );

  OPENLB_HOST_DEVICE
  void computeRhoU( CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  void computeRhoU( CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d]) const;
  template<int width>
  OPENLB_HOST_DEVICE
  void computeRhoU( CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width]) const;
  OPENLB_HOST_DEVICE
  static void computeRhoU( const T * const OPENLB_RESTRICT cellDataData, const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void computeRhoU( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u);

  OPENLB_HOST_DEVICE
  void computeAllMomenta( CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d],
                                  T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeAllMomenta( CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d],
                                  T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeAllMomenta( const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi);
  OPENLB_HOST_DEVICE
  static void computeAllMomenta( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi);

  void defineRho(CellView<T,Lattice>& cell, T rho);
  static void defineRho(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho);
  OPENLB_HOST_DEVICE
  static void defineRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, T rho);

  void defineU(CellView<T,Lattice>& cell, const T u[Lattice<T>::d]);
  static void defineU(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,const T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,const T * const OPENLB_RESTRICT u);

  void defineRhoU( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d]);
  static void defineRhoU (T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void defineRhoU (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u);

  void defineAllMomenta( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d],
                                 const T pi[util::TensorVal<Lattice<T> >::n] );
  static void defineAllMomenta( T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u,
                                 const T * const OPENLB_RESTRICT pi );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u,
                                 const T * const OPENLB_RESTRICT pi );

private:
  BulkMomenta<T,Lattice> _basicMomenta;
  T _fixU[Lattice<T>::d];
  using cached_fixU = FieldExtractor<0,Lattice<T>::d>;
};

}  // namespace olb


#endif

