/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Implementation of boundary cell dynamics -- generic implementation.
 */
#ifndef MOMENTA_ON_BOUNDARIES_HH
#define MOMENTA_ON_BOUNDARIES_HH

#include <limits>
#include "momentaOnBoundaries.h"
#include "dynamics/lbHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"

namespace olb {

////////////////////// Class EquilibriumBM //////////////////////

template<typename T, template<typename U> class Lattice, int dataOffset>
EquilibriumBM<T,Lattice,dataOffset>::EquilibriumBM()
{
  _rho = (T)1;
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
EquilibriumBM<T,Lattice,dataOffset>::EquilibriumBM(T rho, const T u[Lattice<T>::d])
{
  _rho = rho;
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
T EquilibriumBM<T,Lattice,dataOffset>::computeRho( CellView<T,Lattice> const& cell ) const
{
  return _rho;
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void EquilibriumBM<T,Lattice,dataOffset>::computeU(
  CellView<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _u[iD];
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void EquilibriumBM<T,Lattice,dataOffset>::computeJ (
  CellView<T,Lattice> const& cell, T j[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = _u[iD]*_rho;
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void EquilibriumBM<T,Lattice,dataOffset>::computeStress( CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  for (int iPi=0; iPi<util::TensorVal<Lattice<T> >::n; ++iPi) {
    pi[iPi] = T();
  }
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void EquilibriumBM<T,Lattice,dataOffset>::defineRho( CellView<T,Lattice>& cell, T rho )
{
  _rho = rho;
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void EquilibriumBM<T,Lattice,dataOffset>::defineU(
  CellView<T,Lattice>& cell, const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void EquilibriumBM<T,Lattice,dataOffset>::defineAllMomenta( CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d], const T pi[util::TensorVal<Lattice<T> >::n] )
{
  defineRho(cell, rho);
  defineU(cell, u);
}



////////////////////// Class VelocityBM //////////////////////

// template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
// FieldExtractor<dataOffset,Lattice<T>::d> VelocityBM<T,Lattice,direction,orientation,dataOffset>::new_u;

// template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
// FieldExtractor<0,Lattice<T>::d> VelocityBM<T,Lattice,direction,orientation,dataOffset>::cached_u;

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
VelocityBM<T,Lattice,direction,orientation,dataOffset>::VelocityBM()
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
VelocityBM<T,Lattice,direction,orientation,dataOffset>::VelocityBM(const T u[Lattice<T>::d] )
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
  }
}





template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRho( CellView<T,Lattice> const& cell ) const
{
  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cell);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cell);

  T rho =((T)2*rhoNormal+rhoOnWall+(T)1) /
         ((T)1+(T)orientation*this->_u[direction]);

  return rho;
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRho( CellDataArray<T,Lattice> const& data ) const
{
  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(data);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(data);

  T rho =((T)2*rhoNormal+rhoOnWall+(T)1) /
         ((T)1+(T)orientation*this->_u[direction]);

  return rho;
}


template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex) 
{
  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cellData, cellIndex);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cellData, cellIndex);

  T rho =((T)2*rhoNormal+rhoOnWall+(T)1) /
//         ((T)1+(T)orientation*momentaData[new_u::getFieldIndex(direction)][momentaIndex]);
		  ((T)1+(T)orientation*cellData[Lattice<T>::uIndex + direction][cellIndex]);

  return rho;
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRho(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData) 
{
    computeRhoVelocityBM<T,Lattice,direction,orientation>(cellData, momentaData, cached_u::getFieldIndex(direction));
}




template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeU (
  CellView<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  computeU(u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeU (
  CellDataArray<T,Lattice> const& data, T u[Lattice<T>::d]) const
{
  computeU(u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T u[Lattice<T>::d]) 
{
  for (int iD=0;iD < Lattice<T>::d;++iD)
//    u[iD] = momentaData[new_u::getFieldIndex(iD)][momentaIndex];
	u[iD] = cellData[Lattice<T>::uIndex + iD][cellIndex];
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeU (
  const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u) 
{
  for (int iD=0;iD < Lattice<T>::d;++iD)
    u[iD] = momentaData[cached_u::getFieldIndex(iD)];
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeJ (
  CellView<T,Lattice> const& cell, T j[Lattice<T>::d]) const
{
  T rho = computeRho(cell);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = _u[iD]*rho;
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j)
{
  T rho = computeRho(cellData);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = momentaData[cached_u::getFieldIndex(iD)]*rho;
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, T * const OPENLB_RESTRICT j)
{
  T rho = computeRho(cellData,cellIndex, momentaData, momentaIndex);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = momentaData[cached_u::getFieldIndex(iD)][momentaIndex]*rho;
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeU( T u[Lattice<T>::d] ) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _u[iD];
  }
}




template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
template<int width>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU(CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    for (unsigned int i = 0; i < width; ++i) {
      u[iD][i] = _u[iD];
    }
  }
  assert(width == 1);
  rho[0] = computeRho(data);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d] ) const
{
	rho = computeRho(cell);
	computeU(cell, u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU (
  CellDataArray<T,Lattice> const& data,
  T& rho, T u[Lattice<T>::d] ) const
{
	rho = computeRho(data);
	computeU(data, u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T u[Lattice<T>::d]) 
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
//    u[iD] = momentaData[new_u::getFieldIndex(iD)][momentaIndex];
	u[iD] = cellData[Lattice<T>::uIndex + iD][cellIndex];
  }
  rho = computeRho(cellData,cellIndex,momentaData,momentaIndex);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u) 
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = momentaData[cached_u::getFieldIndex(iD)];
//	u[iD] = cellData[Lattice<T>::uIndex + iD][cellIndex];
  }
  rho = computeRho(cellData,momentaData);
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineRho(CellView<T,Lattice>& cell, T rho )
{
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, T rho )
{
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineRho(const T * const OPENLB_RESTRICT cellData,const  T * const OPENLB_RESTRICT momentaData, T rho )
{
}


template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineU (
  CellView<T,Lattice>& cell, const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
    cell[Lattice<T>::uIndex + iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex , const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
//    momentaData[new_u::getFieldIndex(iD)][momentaIndex] = u[iD];
    cellData[Lattice<T>::uIndex + iD][cellIndex] = u[iD];
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineU (
 T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    momentaData[cached_u::getFieldIndex[iD]] = u[iD];
    cellData[Lattice<T>::uIndex + iD] = u[iD];
  }
}




template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineAllMomenta (
  CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d], const T pi[util::TensorVal<Lattice<T> >::n] )
{
  this->defineRhoU(cell, rho, u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineAllMomenta (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d], const T pi[util::TensorVal<Lattice<T> >::n] )
{
  defineRhoU(cellData, cellIndex, momentaData, momentaIndex,rho, u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineAllMomenta (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi )
{
  defineRhoU(cellData, momentaData,rho, u);
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cell, rho);
  defineU(cell, u);

}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineRhoU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cellData, cellIndex, momentaData, momentaIndex, rho);
  defineU(cellData, cellIndex, momentaData, momentaIndex, u);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void VelocityBM<T,Lattice,direction,orientation,dataOffset>::defineRhoU (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u)
{
  defineRho(cellData, momentaData, rho);
  defineU(cellData, momentaData, u);
}

////////////////////// Class PressureBM //////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
PressureBM<T,Lattice,direction,orientation,dataOffset>::PressureBM()
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _values[iD] = T();
  }
  _values[direction] = 1.;
}

/** It takes as argument the value of the tangential velocity
 * components, and the pressure, to be imposed on the boundary.
 */
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
PressureBM<T,Lattice,direction,orientation,dataOffset>::PressureBM(const T values[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _values[iD] = values[iD];
  }
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRho( CellView<T,Lattice> const& cell ) const
{
  return computeRho();
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRho( CellDataArray<T,Lattice> const& data ) const
{
  return computeRho();
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRho() const
{
  return _values[direction];
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex) 
{
  return cellData[Lattice<T>::rhoIndex][cellIndex];//momentaData[direction][momentaIndex];
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
T PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRho(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData) 
{
  return momentaData[direction];
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
template<int width>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU( CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width]) const
{
  static_assert(width == 1, "");
  rho[0] = computeRho();

  assert(u[0][0] == 0);
  assert(u[1][0] == 0);
  T u2[Lattice<T>::d] = {0.0, 0.0};
  computeU(data, u2);

  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD][0] = u2[iD];
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d] ) const
{
	rho = computeRho(cell);
	computeU(cell, u);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T& rho, T u[Lattice<T>::d] )
{
	rho = computeRho(cellData,cellIndex,momentaData,momentaIndex);
	computeU(cellData,cellIndex,momentaData,momentaIndex, u);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeRhoU (
 const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData,
  T& rho, T * const OPENLB_RESTRICT u )
{
	rho = computeRho(cellData,momentaData);
	computeU(cellData,momentaData,u);
}




template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeU (
  CellView<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _values[iD];
  }
  T rho = _values[direction];

  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cell);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cell);

  u[direction] = (T)orientation*( ((T)2*rhoNormal+rhoOnWall+(T)1 ) / rho-(T)1 );
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeU (
  CellDataArray<T,Lattice> const& data, T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _values[iD];
  }
  T rho = _values[direction];

  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(data);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(data);

  u[direction] = (T)orientation*( ((T)2*rhoNormal+rhoOnWall+(T)1 ) / rho-(T)1 );
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeU ( 
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = cellData[Lattice<T>::uIndex+iD][cellIndex];
  }
  T rho = cellData[Lattice<T>::rhoIndex][cellIndex];//momentaData[__new__values::getFieldIndex(direction)][momentaIndex];

  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cellData, cellIndex);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cellData, cellIndex);

  u[direction] = (T)orientation*( ((T)2*rhoNormal+rhoOnWall+(T)1 ) / rho-(T)1 );
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeU (
 const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = momentaData[cached_values::getFieldIndex(iD)];
  }
  T rho = momentaData[cached_values::getFieldIndex(direction)];

  T rhoOnWall = CalcRhoWall<T,Lattice,direction,0>(cellData);
  T rhoNormal = CalcRhoWall<T,Lattice,direction,orientation>(cellData);

  u[direction] = (T)orientation*( ((T)2*rhoNormal+rhoOnWall+(T)1 ) / rho-(T)1 );
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeJ (
  CellView<T,Lattice> const& cell, T j[Lattice<T>::d]) const
{
  computeU(cell, j);
  T rho = computeRho(cell);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] *= rho;
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j) 
{
  computeU(cellData, j);
  T rho = computeRho(cellData);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] *= rho;
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT *  const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j) 
{
  computeU(cellData,cellIndex,momentaData, momentaIndex, j);
  T rho = computeRho(cellData, cellIndex, momentaData, momentaIndex);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] *= rho;
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  defineRho(rho);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRho(T rho )
{
  assert(false);
  _values[direction] = rho;
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex,T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex ,T rho )
{
  momentaData[direction][momentaIndex] = rho;
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRho(const T * const cellData,T * const OPENLB_RESTRICT momentaData,T rho )
{
  momentaData[direction] = rho;
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineU(CellView<T,Lattice>& cell, const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    if (iD != direction) {
      _values[iD] = u[iD];
    }
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    if (iD != direction) {
      momentaData[iD][momentaIndex] = u[iD];
    }
  }
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineU(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    if (iD != direction) {
      momentaData[iD] = u[iD];
    }
  }
}



template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cell, rho);
  defineU(cell, u);

}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cellData, cellIndex,momentaData,momentaIndex, rho);
  defineU(cellData, cellIndex, momentaData, momentaIndex, u);

}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineRhoU (
  const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u)
{
  defineRho(cellData, momentaData, rho);
  defineU(cellData, momentaData, u);

}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineAllMomenta(
  CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d], const T pi[util::TensorVal<Lattice<T> >::n] )
{
  this->defineRhoU(cell, rho, u);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineAllMomenta(
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d], const T pi[util::TensorVal<Lattice<T> >::n] )
{
  defineRhoU(cellData, cellIndex, momentaData, momentaIndex, rho, u);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void PressureBM<T,Lattice,direction,orientation,dataOffset>::defineAllMomenta(
  const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi )
{
  defineRhoU(cellData, momentaData, rho, u);
}

////////  FreeStressBM //////////////////////////////////////////////

template<typename T, template<typename U> class Lattice, int dataOffset>
void FreeStressBM<T,Lattice,dataOffset>::computeStress (
  CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  lbHelpers<T,Lattice>::computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FreeStressBM<T,Lattice,dataOffset>::computeStress (
  CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  lbHelpers<T,Lattice>::computeStress(data, rho, u, pi);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FreeStressBM<T,Lattice,dataOffset>::computeStress (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) 
{
  lbHelpers<T,Lattice>::computeStress(cellData, cellIndex, rho, u, pi);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FreeStressBM<T,Lattice,dataOffset>::computeStress (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi ) 
{
  lbHelpers<T,Lattice>::computeStress(cellData, rho, u, pi);
}



////////////////////// Class RegularizedBM //////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void RegularizedBM<T,Lattice,direction,orientation,dataOffset>::computeStress (
  CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  // TODO: following function needs to be implemented properly for OPENLB_HOST_DEVICE, for now commented out
  // BoundaryHelpers<T,Lattice,direction,orientation>::computeStress(cell, rho, u, pi);
  assert("Not implemented" && false);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void RegularizedBM<T,Lattice,direction,orientation,dataOffset>::computeStress (
  CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  // TODO: following function needs to be implemented properly for OPENLB_HOST_DEVICE, for now commented out
  // BoundaryHelpers<T,Lattice,direction,orientation>::computeStress(data, rho, u, pi);
  assert("Not implemented" && false);
} 

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void RegularizedBM<T,Lattice,direction,orientation,dataOffset>::computeStress( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, 
  T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi)
{
  BoundaryHelpers<T,Lattice,direction,orientation>::computeStress(cellData, cellIndex, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation, int dataOffset>
void RegularizedBM<T,Lattice,direction,orientation,dataOffset>::computeStress( const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi)
{
  BoundaryHelpers<T,Lattice,direction,orientation>::computeStress(cellData, rho, u, pi);
}

////////////////////// Class FixedVelocityBM //////////////////////////

template<typename T, template<typename U> class Lattice, int dataOffset>
T FixedVelocityBM<T,Lattice,dataOffset>::computeRho(CellView<T,Lattice> const& cell) const
{
  return _basicMomenta.computeRho(cell);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
T FixedVelocityBM<T,Lattice,dataOffset>::computeRho(CellDataArray<T,Lattice> const& data) const
{
  return _basicMomenta.computeRho(data);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
T FixedVelocityBM<T,Lattice,dataOffset>::computeRho(CellDataArray<T,Lattice> const& data, unsigned int pos) const
{
  assert(pos == 0);
  return _basicMomenta.computeRho(data);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
T FixedVelocityBM<T,Lattice,dataOffset>::computeRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData)
{
  return BulkMomenta<T,Lattice>::computeRho(cellData, momentaData);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
T FixedVelocityBM<T,Lattice,dataOffset>::computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex)
{
  return BulkMomenta<T,Lattice>::computeRho(cellData,cellIndex, momentaData, momentaIndex);
}



template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeU(CellView<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _fixU[iD];
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeU(CellDataArray<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _fixU[iD];
  }
}

template<typename T, template<typename U> class Lattice, int dataOffset>
template<int width>
void FixedVelocityBM<T,Lattice,dataOffset>::computeU(T u[Lattice<T>::d][width]) const
{
  for (int i = 0; i < width; ++i) {
    for (int iD=0; iD<Lattice<T>::d; ++iD) {
      u[iD][i] = _fixU[iD];
    }
  }
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeU(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = momentaData[cached_fixU::getFieldIndex(iD)];
  }
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
//    u[iD] = momentaData[cached_fixU::getFieldIndex(iD)][momentaIndex];
	  u[iD] = cellData[Lattice<T>::uIndex+iD][cellIndex];
  }
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeJ(CellView<T,Lattice> const& cell, T j[Lattice<T>::d]) const
{
  T rho = computeRho(cell);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = _fixU[iD]*rho;
  }
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeJ(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j) 
{
  T rho = computeRho(cellData);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = momentaData[cached_fixU::getFieldIndex(iD)]*rho;
  }
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeJ(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j) 
{
  T rho = computeRho(cellData,cellIndex,momentaData,momentaIndex);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = momentaData[cached_fixU::getFieldIndex(iD)][momentaIndex]*rho;
  }
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeStress (
  CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  _basicMomenta.computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeStress (
  CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  _basicMomenta.computeStress(data, rho, u, pi);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeStress (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
{
  BulkMomenta<T,Lattice>::computeStress(cellData,momentaData, rho, u, pi);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeStress (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
{
  BulkMomenta<T,Lattice>::computeStress(cellData,cellIndex, momentaData,momentaIndex, rho, u, pi);
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeRhoU (
  CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d] ) const
{
  rho = computeRho(cell);
  computeU(cell,u);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeRhoU (
  CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d] ) const
{
  rho = computeRho(data);
  computeU(data,u);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
template<int width>
void FixedVelocityBM<T,Lattice,dataOffset>::computeRhoU (
  CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width]) const
{
  rho[0] = computeRho(data, width - 1);
  computeU<width>(u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeRhoU (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u )
{
  rho = computeRho(cellData,momentaData);
  computeU(cellData,momentaData,u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u )
{
  rho = computeRho(cellData,cellIndex,momentaData,momentaIndex);
  computeU(cellData,cellIndex, momentaData,momentaIndex,u);
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeAllMomenta (
  CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  _basicMomenta.computeAllMomenta(cell, rho, u, pi);
  computeU(cell, u);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeAllMomenta (
  CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d], T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  _basicMomenta.computeAllMomenta(data, rho, u, pi);
  computeU(data, u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeAllMomenta (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
{
  BulkMomenta<T,Lattice>::computeAllMomenta(cellData,momentaData, rho, u, pi);
  computeU(cellData, u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::computeAllMomenta (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
{
  BulkMomenta<T,Lattice>::computeAllMomenta(cellData,cellIndex,momentaData, momentaIndex, rho, u, pi);
  computeU(cellData,cellIndex, u);
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  _basicMomenta.defineRho(cell, rho);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineRho(T * const OPENLB_RESTRICT cellData,
    T * const OPENLB_RESTRICT momentaData,T rho)
{
  BulkMomenta<T,Lattice>::defineRho(cellData, momentaData, rho);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex,T rho)
{
  BulkMomenta<T,Lattice>::defineRho(cellData,cellIndex, momentaData,momentaIndex, rho);
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineU(CellView<T,Lattice>& cell, const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _fixU[iD] = u[iD];
  }
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineU(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,const T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    momentaData[cached_fixU::getFieldIndex(iD)] = u[iD];
  }
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, const T * const OPENLB_RESTRICT u)
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
//    momentaData[cached_fixU::getFieldIndex(iD)][momentaIndex] = u[iD];
    cellData[Lattice<T>::uIndex+iD][cellIndex] = u[iD];
  }
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineRhoU(CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d])
{
  defineRho(cell,rho);
  defineU(cell,u);
}

template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineRhoU (T * const OPENLB_RESTRICT cellData,
    T * const OPENLB_RESTRICT momentaData,T rho, const T * const OPENLB_RESTRICT u)
{
  defineRho(cellData,momentaData,rho);
  defineU(cellData,momentaData,u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineRhoU (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,T rho, const T * const OPENLB_RESTRICT u)
{
  defineRho(cellData,cellIndex,momentaData,momentaIndex,rho);
  defineU(cellData,cellIndex, momentaData, momentaIndex,u);
}


template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineAllMomenta( CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] )
{
  _basicMomenta.defineAllMomenta(cell, rho, u, pi);
  defineU(cell,u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineAllMomenta(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi )
{
  BulkMomenta<T,Lattice>::defineAllMomenta(cellData,momentaData, rho, u, pi);
  defineU(cellData,momentaData,u);
}
template<typename T, template<typename U> class Lattice, int dataOffset>
void FixedVelocityBM<T,Lattice,dataOffset>::defineAllMomenta(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi )
{
  BulkMomenta<T,Lattice>::defineAllMomenta(cellData,cellIndex,momentaData,momentaIndex, rho, u, pi);
  defineU(cellData,cellIndex,momentaData,momentaIndex,u);
}


}  // namespace olb

#endif
