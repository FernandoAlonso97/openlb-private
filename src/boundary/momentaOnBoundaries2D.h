/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Local boundary cell 2D dynamics -- header file.
 */
#ifndef MOMENTA_ON_BOUNDARIES_2D_H
#define MOMENTA_ON_BOUNDARIES_2D_H

#include "momentaOnBoundaries.h"
#include "core/blockData2D.h"
#include "core/config.h"

namespace olb {

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
class InnerCornerVelBM2D  {
public:
  static const int numberDataEntries = Lattice<T>::d;
  static const int dataOffset_ = dataOffset;

  /// Default Constructor: initialization to zero
  InnerCornerVelBM2D();
  /// Constructor with boundary initialization
  InnerCornerVelBM2D(const T u_[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  T computeRho(CellDataArray<T,Lattice> const& data) const;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData);
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex);

  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const ;
  OPENLB_HOST_DEVICE
  void computeU (
    CellDataArray<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const ;
  OPENLB_HOST_DEVICE
  void computeU(T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  static void computeU (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u );
  OPENLB_HOST_DEVICE
  static void computeU (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT u );

  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const ;
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,  T * const OPENLB_RESTRICT j ) ;
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j ) ;

  /// Compute fluid velocity and particle density on the cell.
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellDataArray<T,Lattice> const& data,
    T& rho, T u[Lattice<T>::d]) const;
  template<int width>
  OPENLB_HOST_DEVICE
  void computeRhoU(CellDataArray<T,Lattice> const& data, T* rho, T [Lattice<T>::d][width]) const;
  OPENLB_HOST_DEVICE
  static void computeRhoU (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,  T& rho, T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void computeRhoU (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u);

  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellDataArray<T,Lattice> const& data,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
   const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi ) ;
  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
   const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi ) ;

  void defineRho(CellView<T,Lattice>& cell, T rho) ;
  OPENLB_HOST_DEVICE
  static void defineRho( T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho) ;
  OPENLB_HOST_DEVICE
  static void defineRho( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho) ;

  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) ;
  void defineU(const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
                       const T * const OPENLB_RESTRICT u) ;
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, const T * const OPENLB_RESTRICT u) ;

  /// Define fluid velocity and particle density on the cell.
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineRhoU (
    T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u);
  static void defineRhoU (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u);

  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi );

  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi );
  /// Stress tensor
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const ;
  OPENLB_HOST_DEVICE
  void computeStress (
    CellDataArray<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const ;
  OPENLB_HOST_DEVICE
  static void computeStress (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi ) ;
  OPENLB_HOST_DEVICE
  static void computeStress (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi ) ;

private:
  using cached_u = FieldExtractor<dataOffset,Lattice<T>::d>;
};


}

#endif
