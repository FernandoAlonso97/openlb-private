/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Implementation of boundary cell dynamics -- generic implementation.
 */
#ifndef MOMENTA_ON_BOUNDARIES_2D_HH
#define MOMENTA_ON_BOUNDARIES_2D_HH

#include "momentaOnBoundaries2D.h"
#include "dynamics/lbHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"

namespace olb {

////////////////////// Class InnerCornerVelBM2D ///////////////

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::InnerCornerVelBM2D()
{ }

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::InnerCornerVelBM2D (
  const T u_[Lattice<T>::d])
{ }

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
  OPENLB_HOST_DEVICE
T InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRho (
  CellView<T,Lattice> const& cell ) const
{
	assert(false);
	return 0;
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
T InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRho (
  CellDataArray<T,Lattice> const& data ) const
{
	assert(false);
	return 0;
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
T InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRho (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData)
{
  return (computeRhoVelocityBM<T, Lattice, 0, normalX>(cellData, momentaData, 0) +
      computeRhoVelocityBM<T, Lattice, 1, normalY>(cellData, momentaData, 0)) * T(0.5);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
T InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRho (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex)
{
  return (computeRhoVelocityBM<T, Lattice, 0, normalX>(cellData,cellIndex, momentaData, momentaIndex, 0) +
      computeRhoVelocityBM<T, Lattice, 1, normalY>(cellData,cellIndex, momentaData, momentaIndex, 0)) * T(0.5);
}



template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeU (
  CellView<T,Lattice> const& cell,
  T u[Lattice<T>::d] ) const
{
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeU (
  CellDataArray<T,Lattice> const& data,
  T u[Lattice<T>::d] ) const
{
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeU (
  T u[Lattice<T>::d] ) const
{
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeU (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T * const OPENLB_RESTRICT u ) 
{
  for (int iD=0;iD < Lattice<T>::d;++iD)
    u[iD] = momentaData[cached_u::getFieldIndex(iD)];
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,  T * const OPENLB_RESTRICT u ) 
{
  for (int iD=0;iD < Lattice<T>::d;++iD)
    u[iD] = momentaData[cached_u::getFieldIndex(iD)][momentaIndex];
}




template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeJ (
  CellView<T,Lattice> const& cell,
  T j[Lattice<T>::d] ) const
{
  computeU(cell, j);
  T rho = computeRho(cell);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] *= rho;
  }
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T * const OPENLB_RESTRICT j ) 
{
  computeU(cellData,momentaData, j);
  T rho = computeRho(cellData,momentaData);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] *= rho;
  }
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,
  T * const OPENLB_RESTRICT j ) 
{
  computeU(cellData, cellIndex ,momentaData, momentaIndex, j);
  T rho = computeRho(cellData, cellIndex, momentaData, momentaIndex);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] *= rho;
  }
}



template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRhoU (
  CellDataArray<T,Lattice> const& data,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = computeRho(data);
  computeU(data, u);

}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = computeRho(cell);
  computeU(cell, u);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
template <int width>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRhoU (
  CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width] ) const
{
  assert(false);
  static_assert(width == 1, "Make sure not more than one entry");
  assert(u[0][0] == 0.0);
  assert(u[1][0] == 0.0);
  T u2[Lattice<T>::d] = {0.0, 0.0};;
  this->computeU(data, u2);
  for (int i = 0; i < 2; ++i)
    u[i][0] = u2[i];
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRhoU (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T& rho, T * const OPENLB_RESTRICT u) 
{
  rho = computeRho(cellData,momentaData);
  computeU(cellData,momentaData, u);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,  T& rho, T * const OPENLB_RESTRICT u) 
{
  rho = computeRho(cellData,cellIndex,momentaData,momentaIndex);
  computeU(cellData,cellIndex,momentaData,momentaIndex, u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeAllMomenta (
		CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d],T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(cell, rho, u);
  computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeAllMomenta (
		CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d],T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(data, rho, u);
  computeStress(data, rho, u, pi);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeAllMomenta (
		const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T& rho, T * const OPENLB_RESTRICT u,T * const OPENLB_RESTRICT pi ) 
{
  computeRhoU(cellData,momentaData, rho, u);
  computeStress(cellData, momentaData, rho, u, pi);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeAllMomenta (
		const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u,T * const OPENLB_RESTRICT pi ) 
{
  computeRhoU(cellData,cellIndex,momentaData,momentaIndex, rho, u);
  computeStress(cellData,cellIndex, momentaData,momentaIndex, rho, u, pi);
}




template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineRho (
  CellView<T,Lattice>& cell, T rho )
{ }
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineRho (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho )
{ }
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineRho (
 const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, T rho )
{ }

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d] )
{
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineU (
  const T u[Lattice<T>::d] )
{
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineU (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  const T * const OPENLB_RESTRICT u )
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    momentaData[cached_u::getFieldIndex(iD)] = u[iD];
    cellData[Lattice<T>::uIndex + iD] = u[iD];
  }
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  const T * const OPENLB_RESTRICT u )
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    momentaData[cached_u::getFieldIndex(iD)][momentaIndex] = u[iD];
    cellData[Lattice<T>::uIndex + iD][cellIndex] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cell, rho);
  defineU(cell, u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineRhoU (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u)
{
  defineRho(cellData,momentaData, rho);
  defineU(cellData,momentaData, u);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineRhoU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u)
{
  defineRho(cellData,cellIndex,momentaData,momentaIndex, rho);
  defineU(cellData,cellIndex,momentaData,momentaIndex, u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineAllMomenta (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi )
{
    defineU(cellData, u);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::defineAllMomenta (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi )
{
    defineU(cellData,cellIndex,momentaData,momentaIndex, u);
}




template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  typedef lbHelpers<T,Lattice> lbH;
  CellView<T,Lattice> newCell(cell);
  int v[Lattice<T>::d] = { -normalX, -normalY };
  int unknownF  = util::findVelocity<Lattice<T> >(v);

  if (unknownF != Lattice<T>::q) {
    int oppositeF = util::opposite<Lattice<T> >(unknownF);

    T uSqr = util::normSqr<T,Lattice<T>::d>(u);

    newCell[unknownF] = newCell[oppositeF]
                        - lbH::equilibrium(oppositeF, rho, u, uSqr)
                        + lbH::equilibrium(unknownF, rho, u, uSqr);
  }

  lbH::computeStress(newCell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeStress (
  CellDataArray<T,Lattice> const& data,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  typedef lbHelpers<T,Lattice> lbH;
  T tempData[Lattice<T>::q][1];
  CellDataArray<T,Lattice> newCellData;
  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop)
  {
    tempData[iPop][0] = data.data[iPop][0];
    newCellData.data[iPop] = tempData[iPop];
  }

  int v[Lattice<T>::d] = { -normalX, -normalY };
  int unknownF  = util::findVelocity<Lattice<T> >(v);

  if (unknownF != Lattice<T>::q) {
    int oppositeF = util::opposite<Lattice<T> >(unknownF);

    T uSqr = util::normSqr<T,Lattice<T>::d>(u);

    tempData[unknownF][0] = tempData[oppositeF][0]
                        - lbH::equilibrium(oppositeF, rho, u, uSqr)
                        + lbH::equilibrium(unknownF, rho, u, uSqr);
  }

  lbH::computeStress(newCellData, rho, u, pi);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeStress (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  typedef lbHelpers<T,Lattice> lbH;
  T newCell[Lattice<T>::q];

  int v[Lattice<T>::d] = { -normalX, -normalY };
  int unknownF  = util::findVelocity<Lattice<T> >(v);

  if (unknownF != Lattice<T>::q) {
    int oppositeF = util::opposite<Lattice<T> >(unknownF);

    T uSqr = util::normSqr<T,Lattice<T>::d>(u);

    newCell[unknownF] = newCell[oppositeF]
                        - lbH::equilibrium(oppositeF, rho, u, uSqr)
                        + lbH::equilibrium(unknownF, rho, u, uSqr);
  }

  lbH::computeStress(newCell, rho, u, pi);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM2D<T,Lattice,normalX,normalY,dataOffset>::computeStress (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  typedef lbHelpers<T,Lattice> lbH;
  T newCell[Lattice<T>::q] = {0};

  int v[Lattice<T>::d] = { -normalX, -normalY };
  int unknownF  = util::findVelocity<Lattice<T> >(v);

  if (unknownF != Lattice<T>::q) {
    int oppositeF = util::opposite<Lattice<T> >(unknownF);

    T uSqr = util::normSqr<T,Lattice<T>::d>(u);

    newCell[unknownF] = newCell[oppositeF]
                        - lbH::equilibrium(oppositeF, rho, u, uSqr)
                        + lbH::equilibrium(unknownF, rho, u, uSqr);
  }

  lbH::computeStress(newCell, rho, u, pi);
}


}  // namespace olb

#endif
