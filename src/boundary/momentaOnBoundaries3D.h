/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Local boundary cell 3D dynamics -- header file.
 */
#ifndef MOMENTA_ON_BOUNDARIES_3D_H
#define MOMENTA_ON_BOUNDARIES_3D_H

#include "momentaOnBoundaries.h"
#include "core/config.h"

namespace olb {

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
class InnerEdgeVelBM3D {
public:
  enum { direction1 = (plane+1)%3, direction2 = (plane+2)%3 };
  // static const int numberDataEntries = Lattice<T>::d;
  static const int numberDataEntries = 0;
  static const int dataOffset_ = dataOffset;
public:
  /// Default Constructor: initialization to zero
  InnerEdgeVelBM3D();
  /// Constructor with boundary initialization
  InnerEdgeVelBM3D(const T u_[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex);
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeU (const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT u );

  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT j);

  OPENLB_HOST_DEVICE
  void computeU(T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u) ;

  OPENLB_HOST_DEVICE
  void computeRhoU(CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d]) const;


  OPENLB_HOST_DEVICE
  void defineRho(CellView<T,Lattice>& cell, T rho);

  OPENLB_HOST_DEVICE
  static void defineRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho);

  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
                       const T * const OPENLB_RESTRICT u) ;
  void defineU(const T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static void defineRhoU (  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u);

  OPENLB_HOST_DEVICE
  void defineRhoU ( CellView<T,Lattice> & cell, T rho, const T u[Lattice<T>::d]) ;


  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi);
  /// Stress tensor
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] )const;
  OPENLB_HOST_DEVICE
  static void computeStress (
   const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi);

  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi);

  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> & cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n]) const;
//private:
  //RegularizedVelocityBM<T,Lattice,direction1,normal1> momenta1;
  //RegularizedVelocityBM<T,Lattice,direction2,normal2> momenta2;
  //using cached_u = FieldExtractor<dataOffset,Lattice<T>::d>;
};


template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
class InnerCornerVelBM3D {
public:
  static const int numberDataEntries = 0;
  static const int dataOffset_ = dataOffset;
  /// Default Constructor: initialization to zero
  InnerCornerVelBM3D();
  /// Constructor with boundary initialization
  InnerCornerVelBM3D(const T u_[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex);

  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] )const;
  OPENLB_HOST_DEVICE
  static void computeU (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT u );
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] )const;
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT j );
  OPENLB_HOST_DEVICE
  void computeU(T u[Lattice<T>::d]) const;

  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u) ;

  OPENLB_HOST_DEVICE
  void computeRhoU(CellView<T,Lattice> const& cell, T& rho, T u [Lattice<T>::d])const;


  void defineRho(CellView<T,Lattice>& cell, T rho);
  OPENLB_HOST_DEVICE
  static void defineRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex, T rho);
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
                       const T * const OPENLB_RESTRICT u);
  void defineU(const T u[Lattice<T>::d]);
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] );

  OPENLB_HOST_DEVICE
  static void defineRhoU (  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u);

  OPENLB_HOST_DEVICE
  void defineRhoU (CellView<T,Lattice> & cell, T rho, const T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi );
  /// Stress tensor
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeStress (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );

  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi);

  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> & cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n]) const;
// private:
  // RegularizedVelocityBM<T,Lattice,0,normalX> xMomenta;
  // RegularizedVelocityBM<T,Lattice,1,normalY> yMomenta;
  // RegularizedVelocityBM<T,Lattice,2,normalZ> zMomenta;
};

}

#endif
