/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Local boundary cell 3D dynamics -- generic implementation.
 */
#ifndef MOMENTA_ON_BOUNDARIES_3D_HH
#define MOMENTA_ON_BOUNDARIES_3D_HH

#include "momentaOnBoundaries3D.h"
#include "dynamics/lbHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"

namespace olb {

////////////////////// Class InnerEdgeVelBM3D ///////////////

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::
InnerEdgeVelBM3D()
{ }

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::
InnerEdgeVelBM3D(const T u_[Lattice<T>::d])
{ }

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
T InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeRho (
  CellView<T,Lattice> const& cell ) const 
{
  //added since the momenta do no longer hold other momenta
  assert(false);
  //legacy code
  //return (momenta1.computeRho(cell) + momenta2.computeRho(cell)) / (T)2;
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
T InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeRho(
        const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
        const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex)
{
  return T(0.5)*(computeRhoVelocityBM<T,Lattice,direction1,normal1>(cellData,cellIndex,momentaData,momentaIndex,0 ) + 
                 computeRhoVelocityBM<T,Lattice,direction2,normal2>(cellData,cellIndex,momentaData,momentaIndex,0 ));
}

template<typename T, template<typename U> class Lattice,
		int plane, int normal1, int normal2, int dataOffset>
 OPENLB_HOST_DEVICE
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU (
 CellView<T,Lattice> const& cell,
 T u[Lattice<T>::d] ) const
{
  assert(false);
  // VelocityBM<T,Lattice,direction1,normal1,dataOffset>::computeU(cell,u);
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU (
  const T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T * const OPENLB_RESTRICT u )
{
  VelocityBM<T,Lattice,direction1,normal1,dataOffset>::computeU(cellData,cellIndex,momentaData,momentaIndex,u);
}

template<typename T, template<typename U> class Lattice,
		int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeJ (
 CellView<T,Lattice> const& cell,
 T j[Lattice<T>::d] ) const
{
  assert(false);
  // VelocityBM<T,Lattice,direction1,normal1,dataOffset>::computeJ(cell,j);
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeJ (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,
  T * const OPENLB_RESTRICT j )
{
  VelocityBM<T,Lattice,direction1,normal1,dataOffset>::computeJ(cellData,cellIndex,momentaData,momentaIndex,j);
}

// template<typename T, template<typename U> class Lattice,
		// int plane, int normal1, int normal2, int dataOffset>
 // OPENLB_HOST_DEVICE
// void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU (
 // T u[Lattice<T>::d] ) const
// {
  // VelocityBM<T,Lattice,direction1,normal1,dataOffset>::computeU(cell,u);
// }

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeRhoU(
		const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u)
{
  rho=0.0;
  InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU(cellData,cellIndex,momentaData,momentaIndex,u);
}
template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeRhoU(
		CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d]) const
{
  assert(false);
  // rho=0.0;
  // InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU(cell,u);
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineRho (
  CellView<T,Lattice>& cell, T rho )
{
  assert(false);
}


template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineRho (
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
        const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho)
{ }

template<typename T, template<typename U> class Lattice,
		int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineU (
 CellView<T,Lattice>& cell,
 const T u[Lattice<T>::d] )
{
  assert(false);
  // VelocityBM<T,Lattice,direction1,normal1,dataOffset>::defineU(cell,u);
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,
  const T * const OPENLB_RESTRICT u)
{
  VelocityBM<T,Lattice,direction1,normal1,dataOffset>::defineU(cellData,cellIndex,momentaData,momentaIndex,u);
}

//template<typename T, template<typename U> class Lattice,
//         int plane, int normal1, int normal2, int dataOffset>
//void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineU (
//  const T u[Lattice<T>::d] )
//{
//  momenta1.defineU(u);
//  momenta2.defineU(u);
//}

 

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineRhoU (
	  	T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u)
{
  InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineU(cellData,cellIndex,momentaData,momentaIndex,u); 
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineRhoU (
	  	CellView<T,Lattice> & cell, T rho, const T u[Lattice<T>::d])
{
  assert(false);
  // InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineU(cell,u); 
}

template<typename T, template<typename U> class Lattice,
		int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::
defineAllMomenta (
 CellView<T,Lattice>& cell,
 T rho, const T u[Lattice<T>::d],
 const T pi[util::TensorVal<Lattice<T> >::n] )
{
  assert(false);
  // InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::defineU(cell,u); 
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::
defineAllMomenta (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi)
{
  VelocityBM<T,Lattice,direction1,normal1,dataOffset>::defineU(cellData,cellIndex,momentaData,momentaIndex,u);
}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::
computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  typedef lbHelpers<T,Lattice> lbH;

  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  CellView<T,Lattice> newCell(cell);
  for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
    if ( (Lattice<T>::c(iPop,direction1) == -normal1) &&
         (Lattice<T>::c(iPop,direction2) == -normal2) ) {
      int opp = util::opposite<Lattice<T> >(iPop);
      newCell[iPop] = newCell[opp]
                      - lbH::equilibrium(opp, rho, u, uSqr)
                      + lbH::equilibrium(iPop, rho, u, uSqr);
    }
  }
  lbH::computeStress(newCell, rho, u, pi);
}
template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::
computeStress (
        const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
        T rho, const T * const OPENLB_RESTRICT u,
        T * const OPENLB_RESTRICT pi)
{
//  typedef lbHelpers<T,Lattice> lbH;
//
//  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
//
//  CellView<T,Lattice> newCell(cell);
//  for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
//    if ( (Lattice<T>::c(iPop)[direction1] == -normal1) &&
//         (Lattice<T>::c(iPop)[direction2] == -normal2) ) {
//      int opp = util::opposite<Lattice<T> >(iPop);
//      newCell[iPop] = newCell[opp]
//                      - lbH::equilibrium(opp, rho, u, uSqr)
//                      + lbH::equilibrium(iPop, rho, u, uSqr);
//    }
//  }
//  lbH::computeStress(newCell, rho, u, pi);
    // TODO
    assert(false);
}
template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeAllMomenta (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi)
{
  rho=0.0;
  InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU(cellData,cellIndex,momentaData,momentaIndex,u);
  InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeStress(cellData,cellIndex,momentaData,momentaIndex,rho,u,pi);

}

template<typename T, template<typename U> class Lattice,
         int plane, int normal1, int normal2, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeAllMomenta (
    CellView<T,Lattice> & cell,
    T& rho, T u [Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n]) const
{
  assert(false);
  // rho=0.0;
  // InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeU(cell,u);
  // InnerEdgeVelBM3D<T,Lattice,plane,normal1,normal2,dataOffset>::computeStress(cell,rho,u,pi);

}


////////////////////// Class InnerCornerVelBM3D ///////////////

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::InnerCornerVelBM3D()
{ }

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::InnerCornerVelBM3D (
  const T u_[Lattice<T>::d])
{ }

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
T InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeRho (
   const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex) 
{
  return (computeRhoVelocityBM<T,Lattice,0,normalX>(cellData,cellIndex,momentaData,momentaIndex,0 ) +
		  computeRhoVelocityBM<T,Lattice,1,normalY>(cellData,cellIndex,momentaData,momentaIndex,0 ) +
		  computeRhoVelocityBM<T,Lattice,2,normalZ>(cellData,cellIndex,momentaData,momentaIndex,0 ))/
	      T(3);
}

template<typename T, template<typename U> class Lattice,
		int normalX, int normalY, int normalZ, int dataOffset>
 OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU (
 CellView<T,Lattice> const& cell,
 T u[Lattice<T>::d] ) const
{
  assert(false);
  // VelocityBM<T,Lattice,0,normalX,dataOffset>::computeU(cell,u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T * const OPENLB_RESTRICT u )
{
  VelocityBM<T,Lattice,0,normalX,dataOffset>::computeU(cellData,cellIndex,momentaData,momentaIndex,u);
}

template<typename T, template<typename U> class Lattice,
		int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeJ (
 CellView<T,Lattice> const& cell,
 T j[Lattice<T>::d] )const
{
  assert(false);
  // VelocityBM<T,Lattice,0,normalX,dataOffset>::computeJ(cell,j);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeJ (
      const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
	    T * const OPENLB_RESTRICT j )
{
  VelocityBM<T,Lattice,0,normalX,dataOffset>::computeJ(cellData,cellIndex,momentaData,momentaIndex,j);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU (
  T u[Lattice<T>::d] ) const
{
  assert(false);
  //old code
  // xMomenta.computeU(u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeRhoU(
		const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex, T& rho, T * const OPENLB_RESTRICT u)
{
  rho=0.0;
  InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU(cellData,cellIndex,momentaData,momentaIndex,u);
}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeRhoU(
		CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d]) const
{
  assert(false);
  // rho=0.0;
  // InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU(cell,u);
}


template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineRho (
  CellView<T,Lattice>& cell, T rho )
{ 
  assert(false);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineRho (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho )
{ }

template<typename T, template<typename U> class Lattice,
		int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineU (
 CellView<T,Lattice>& cell,
 const T u[Lattice<T>::d] )
{
  assert(false);
  // VelocityBM<T,Lattice,0,normalX,dataOffset>::defineU(cell,u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineU (
      T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  const T * const OPENLB_RESTRICT u )
{
  VelocityBM<T,Lattice,0,normalX,dataOffset>::defineU(cellData,cellIndex,momentaData,momentaIndex,u);
}

//template<typename T, template<typename U> class Lattice,
//         int normalX, int normalY, int normalZ, int dataOffset>
//void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineU (
//  const T u[Lattice<T>::d] )
//{
//  xMomenta.defineU(u);
//  yMomenta.defineU(u);
//  zMomenta.defineU(u);
//}

template<typename T, template<typename U> class Lattice,
		int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineAllMomenta (
 CellView<T,Lattice>& cell,
 T rho, const T u[Lattice<T>::d],
 const T pi[util::TensorVal<Lattice<T> >::n] )
{
  assert(false);
  // VelocityBM<T,Lattice,0,normalX,dataOffset>::defineU(cell,u);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineRhoU (
	  	T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho, const T * const OPENLB_RESTRICT u)
{
  InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineU(cellData,cellIndex,momentaData,momentaIndex,u); 
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineRhoU (
	  	CellView<T,Lattice> & cell, T rho, const T u[Lattice<T>::d])
{
  assert(false);
  // InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineU(cell,u); 
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::defineAllMomenta (
   T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi )
{
  VelocityBM<T,Lattice,0,normalX,dataOffset>::defineAllMomenta(cellData,cellIndex,momentaData,momentaIndex,rho,u,pi);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  typedef lbHelpers<T,Lattice> lbH;
  CellView<T,Lattice> newCell(cell);
  int v[Lattice<T>::d] = { -normalX, -normalY, -normalZ };
  int unknownF  = util::findVelocity<Lattice<T> >(v);

  if (unknownF != Lattice<T>::q) {
    int oppositeF = util::opposite<Lattice<T> >(unknownF);

    T uSqr = util::normSqr<T,Lattice<T>::d>(u);

    newCell[unknownF] = newCell[oppositeF]
                        - lbH::equilibrium(oppositeF, rho, u, uSqr)
                        + lbH::equilibrium(unknownF, rho, u, uSqr);
  }

  lbH::computeStress(newCell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeStress (
      const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, 
  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi ) 
{
//  typedef lbHelpers<T,Lattice> lbH;
//  CellView<T,Lattice> newCell(cell);
//  int v[Lattice<T>::d] = { -normalX, -normalY, -normalZ };
//  int unknownF  = util::findVelocity<Lattice<T> >(v);
//
//  if (unknownF != Lattice<T>::q) {
//    int oppositeF = util::opposite<Lattice<T> >(unknownF);
//
//    T uSqr = util::normSqr<T,Lattice<T>::d>(u);
//
//    newCell[unknownF] = newCell[oppositeF]
//                        - lbH::equilibrium(oppositeF, rho, u, uSqr)
//                        + lbH::equilibrium(unknownF, rho, u, uSqr);
//  }
//
//  lbH::computeStress(newCell, rho, u, pi);
    //TODO
    assert(false);
}

template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeAllMomenta (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi)
{
  rho=0.0;
  InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU(cellData,cellIndex,momentaData,momentaIndex,u);
  InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeStress(cellData,cellIndex,momentaData,momentaIndex,rho,u,pi);

}
template<typename T, template<typename U> class Lattice,
         int normalX, int normalY, int normalZ, int dataOffset>
  OPENLB_HOST_DEVICE
void InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeAllMomenta (
    CellView<T,Lattice> & cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n]) const
{
  assert(false);
  // rho=0.0;
  // InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeU(cell,u);
  // InnerCornerVelBM3D<T,Lattice,normalX,normalY,normalZ,dataOffset>::computeStress(cell,rho,u,pi);

}

}  // namespace olb

#endif
