/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef CPUGPU_DATA_EXCHANGE_H
#define CPUGPU_DATA_EXCHANGE_H

#include "core/cudaErrorHandler.h"
#include "core/blockLattice2D.h"
#include "communication/readWriteFunctorsGPU.h"
#include <vector>


namespace olb {
////////////////////////////////////////////////////////////Transfer Object///////////////////////////////////////
#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice,typename DataType>
class CPUGPUTransferBase {

	public:

		CPUGPUTransferBase() = delete;
		CPUGPUTransferBase(size_t numberOfCells):
			 _numberOfCells(numberOfCells)
		{
			_allocateMemory();
		}

		CPUGPUTransferBase(CPUGPUTransferBase const & source):
			_numberOfCells(source._numberOfCells)
		{
			_allocateMemory();
			HANDLE_ERROR(cudaMemcpy(&_gpuData,&source._gpuData,sizeof(DataType) * source._numberOfCells,cudaMemcpyDeviceToDevice));
			HANDLE_ERROR(cudaMemcpy(&_gpuCellIndices,&source._gpuCellIndices,sizeof(size_t) * source._numberOfCells,cudaMemcpyDeviceToDevice));
			std::memcpy(_cpuData,source._cpuData,sizeof(DataType) * source._numberOfCells);
			std::memcpy(_cpuCellIndices,source._cpuCellIndices,sizeof(size_t) * source._numberOfCells);
		}

		CPUGPUTransferBase& operator= (CPUGPUTransferBase source)
		{
			if (this != source)
			{
				std::swap(*this,source);
			  return *this;
			}
			return *this;
		}

		~CPUGPUTransferBase()
		{
		    HANDLE_ERROR(cudaFree(_gpuData));
		    HANDLE_ERROR(cudaFree(_gpuCellIndices));
		    HANDLE_ERROR(cudaFreeHost(_cpuData));
		    HANDLE_ERROR(cudaFreeHost(_cpuCellIndices));
		}

		void transferDataToCPU()
		{
		    HANDLE_ERROR(cudaMemcpy(_cpuData, _gpuData, sizeof(DataType) * (_registerIndex), cudaMemcpyDeviceToHost));
		}

		void transferDataToGPU()
		{
//		    auto start = std::chrono::system_clock::now();
		    HANDLE_ERROR(cudaMemcpy(_gpuData, _cpuData, sizeof(DataType) * (_registerIndex), cudaMemcpyHostToDevice));
//		    auto end = std::chrono::system_clock::now();
//		    std::cout << "Memcopy to GPU took: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << " µs" << std::endl;
		}

		void transferIndexToGPU()
		{
		    HANDLE_ERROR(cudaMemcpy(_gpuCellIndices, _cpuCellIndices, sizeof(size_t) * (_registerIndex), cudaMemcpyHostToDevice));
		}
		const DataType getByLocalIndexCPU (size_t index) const
		{
			OLB_PRECONDITION(index < _numberOfCells);
			return _cpuData[index];
		}

		DataType& setByLocalIndexCPU (size_t index)
		{
			OLB_PRECONDITION(index < _numberOfCells);
			return _cpuData[index];
		}
	
		DataType* getRawPointerToGPUMemory ()
		{
			return _gpuData;
		}

		const size_t* getRawPointerToGPUCellIndices()
		{
			return _gpuCellIndices;
		}	

		void registerCell (size_t cellIndex)
		{
			OLB_PRECONDITION(_registerIndex < _numberOfCells);
			_cpuCellIndices[_registerIndex] = cellIndex;
			++_registerIndex;
		}

		size_t getNumberOfRegisteredCells()
		{
			return _registerIndex;
		}
	private:

		void _allocateMemory()
	    {
			if (_numberOfCells > 0)
			{
			// allocate GPU memory first
			HANDLE_ERROR(cudaMalloc(&_gpuData,sizeof(DataType) *  _numberOfCells));
			HANDLE_ERROR(cudaMalloc(&_gpuCellIndices,sizeof(size_t) *  _numberOfCells));

			// allocate CPU memory
			// _cpuData = new typename Functor<T,Lattice>::returnType[_memSize];
			// allocate pagelocked cpu memory for faster transfer
			HANDLE_ERROR(cudaMallocHost(&_cpuData,sizeof(DataType) * _numberOfCells));
			HANDLE_ERROR(cudaMallocHost(&_cpuCellIndices,sizeof(size_t) * _numberOfCells));
			}
		}

	private:

		size_t                              _numberOfCells=0;
		size_t                              _registerIndex=0;

		DataType*                           _gpuData=nullptr;
		DataType*                           _cpuData=nullptr;
		size_t*                             _cpuCellIndices=nullptr;
		size_t*                             _gpuCellIndices=nullptr;
};

#endif
};  // namespace olb
#endif
