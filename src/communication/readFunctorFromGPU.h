/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef READ_FUNCTOR_FROM_GPU_2D_H
#define READ_FUNCTOR_FROM_GPU_2D_H

#include "core/cudaErrorHandler.h"
#include "core/blockLattice2D.h"
#include "communication/CPUGPUDataExchange.h"
#include <vector>


namespace olb {
#ifdef ENABLE_CUDA
///////////////////////////////////////////////////////PURE ABSTRACT INTERFACE DEFINITION FOR READFUNCTORFROMGPU/////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class > class Functor>
class ReadFunctorFromGPU2D {

	public:

		ReadFunctorFromGPU2D() = default;

		virtual ~ReadFunctorFromGPU2D () = default;

		void transfer()
		{
		  _transfer_from_implementation();
		}

		void read()
		{
		  _read_from_implementation();
		}

		void readAndTransfer()
		{
      read();
		  transfer();
		}

		const typename Functor<T,Lattice>::returnType getByLocalIndex(int iX,int iY)
		{
			return _getByLocalIndex_from_implementation(iX,iY);
		}


	private:

		virtual const typename Functor<T,Lattice>::returnType _getByLocalIndex_from_implementation(int iX, int iY) = 0;
		virtual void _read_from_implementation() = 0;
		virtual void _transfer_from_implementation() =0;
};
////////////////////////////////////////////////////IMPLEMENTATION FOR A READFUNCTORFROMGPU BY RANGE/////////////////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class> class Functor>
class ReadFunctorByRangeFromGPU2D final: public ReadFunctorFromGPU2D<T,Lattice,Functor> {

	public:

		ReadFunctorByRangeFromGPU2D() = delete;

		ReadFunctorByRangeFromGPU2D(BlockLattice2D<T,Lattice>& blockLattice,const size_t iX0,const size_t iX1,const size_t iY0, const size_t iY1):ReadFunctorFromGPU2D<T,Lattice,Functor>(),
		  _dataTransfer(CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>{(iX1-iX0+1)*(iY1-iY0+1)}),
		  _blockLattice(blockLattice),
		  _iX0(iX0),
		  _iX1(iX1),
		  _iY0(iY0),
		  _iY1(iY1)
	    {
			_registerCellsByRange();
      _dataTransfer.transferIndexToGPU();
		}

		~ReadFunctorByRangeFromGPU2D() override = default; 

	private:

		const typename Functor<T,Lattice>::returnType _getByLocalIndex_from_implementation(int iX, int iY) override
		{
			return _dataTransfer.getByLocalIndexCPU(util::getCellIndex2D(iX,iY,(_iY1-_iY0+1)));
		}

		void _read_from_implementation() override
		{
		  _blockLattice.template readBlockLatticeByFunctor<Functor>(_dataTransfer.getRawPointerToGPUMemory(),_dataTransfer.getRawPointerToGPUCellIndices(),_dataTransfer.getNumberOfRegisteredCells());
		}
		
		void _transfer_from_implementation() override
		{
		  _dataTransfer.transferDataToCPU();
		}

		void _registerCellsByRange()
		{
			for (int iX=_iX0;iX<=_iX1;++iX)
			{
				for (int iY=_iY0;iY<=_iY1;++iY)
				{
				  _dataTransfer.registerCell(_blockLattice.getCellIndex(iX,iY));
				}
			}
		}


	private:

		CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>   _dataTransfer;
		BlockLattice2D<T,Lattice>&	                     	                      _blockLattice;
		size_t 							                                                    _iX0,_iX1,_iY0,_iY1;

};

///////////////////////////////////////////////////////PURE ABSTRACT INTERFACE DEFINITION FOR READFUNCTORFROMGPU/////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class > class Functor>
class ReadFunctorFromGPU3D {

    public:

        ReadFunctorFromGPU3D() = default;

        virtual ~ReadFunctorFromGPU3D () = default;

        void transfer()
        {
          _transfer_from_implementation();
        }

        void read()
        {
          _read_from_implementation();
        }

        void readAndTransfer()
        {
      read();
          transfer();
        }

        const typename Functor<T,Lattice>::returnType getByLocalIndex(int iX,int iY,int iZ)
        {
            return _getByLocalIndex_from_implementation(iX,iY,iZ);
        }

        const typename Functor<T,Lattice>::returnType getByGlobalIndex(int iX,int iY,int iZ)
        {
            return _getByGlobalIndex_from_implementation(iX,iY,iZ);
        }

        virtual CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>& getTransferObject() = 0;

    private:

        virtual const typename Functor<T,Lattice>::returnType _getByLocalIndex_from_implementation(int iX, int iY, int iZ) = 0;
        virtual const typename Functor<T,Lattice>::returnType _getByGlobalIndex_from_implementation(int iX, int iY, int iZ) = 0;
        virtual void _read_from_implementation() = 0;
        virtual void _transfer_from_implementation() =0;
};
////////////////////////////////////////////////////IMPLEMENTATION FOR A READFUNCTORFROMGPU BY RANGE/////////////////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class> class Functor>
class ReadFunctorByRangeFromGPU3D final: public ReadFunctorFromGPU3D<T,Lattice,Functor> {

    public:

        ReadFunctorByRangeFromGPU3D() = delete;

        ReadFunctorByRangeFromGPU3D(BlockLattice3D<T,Lattice>& blockLattice,const size_t iX0,const size_t iX1,
                const size_t iY0, const size_t iY1, const size_t iZ0, const size_t iZ1) :
          ReadFunctorFromGPU3D<T,Lattice,Functor>(),
          _dataTransfer(CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>{(iX1-iX0+1)*(iY1-iY0+1)*(iZ1-iZ0+1)}),
          _blockLattice(blockLattice),
          _iX0(iX0),
          _iX1(iX1),
          _iY0(iY0),
          _iY1(iY1),
          _iZ0(iZ0),
          _iZ1(iZ1)
        {
            _registerCellsByRange();
            _dataTransfer.transferIndexToGPU();
        }

        ~ReadFunctorByRangeFromGPU3D() override = default;

        CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>& getTransferObject() override
        {
            return _dataTransfer;
        }

    private:

        const typename Functor<T,Lattice>::returnType _getByLocalIndex_from_implementation(int iX, int iY, int iZ) override
        {
            return _dataTransfer.getByLocalIndexCPU(util::getCellIndex3D(iX,iY,iZ,(_iY1-_iY0+1),(_iZ1-_iZ0+1)));
        }

        const typename Functor<T,Lattice>::returnType _getByGlobalIndex_from_implementation(int iX, int iY, int iZ) override
        {
            return _dataTransfer.getByLocalIndexCPU(util::getCellIndex3D(iX-_iX0,iY-_iY0,iZ-_iZ0,(_iY1-_iY0+1),(_iZ1-_iZ0+1)));
        }

        void _read_from_implementation() override
        {
          _blockLattice.template readBlockLatticeByFunctor<Functor>(_dataTransfer.getRawPointerToGPUMemory(),_dataTransfer.getRawPointerToGPUCellIndices(),_dataTransfer.getNumberOfRegisteredCells());
        }

        void _transfer_from_implementation() override
        {
          _dataTransfer.transferDataToCPU();
        }

        void _registerCellsByRange()
        {
            for (int iX=_iX0;iX<=_iX1;++iX)
                for (int iY=_iY0;iY<=_iY1;++iY)
                    for(int iZ=_iZ0;iZ<=_iZ1;++iZ)
                        _dataTransfer.registerCell(_blockLattice.getCellIndex(iX,iY,iZ));
        }


    private:

        CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>   _dataTransfer;
        BlockLattice3D<T,Lattice>&                                              _blockLattice;
        size_t                                                                  _iX0,_iX1,_iY0,_iY1,_iZ0,_iZ1;

};


////////////////////////////////////////////////////IMPLEMENTATION FOR A READFUNCTORFROMGPU BY INDICATOR/////////////////////////
// template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class> class Functor>
// class ReadFunctorByIndicatorFromGPU2D : public ReadFunctorFromGPU2D<T,Lattice,Functor> {

	// public:

		// ReadFunctorByIndicatorFromGPU2D() = delete;

		// ReadFunctorByIndicatorFromGPU2D(BlockLattice2D<T,Lattice>& blockLattice,BlockGeometryStructure2D<T>& blockGeometryStructure, IndicatorF2D<T>& indicator):
			// ReadFunctorFromGPU2D<T,Lattice,Functor>(),
			// _indicator(indicator),
			// _blockGeometryStructure(blockGeometryStructure),
			// _dataTransfer(CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>(0)),
			// _blockLattice(blockLattice),
      // _cellIndices2D(std::vector<std::vector<int> >{})
		// {
			// _registerCellsByIndicator();
      // _dataTransfer = CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>(_numberOfCells);
			// _dataTransfer.transferIndexToGPU();
		// } 


	// private:

		// const typename Functor<T,Lattice>::returnType _getByLocalIndex_from_implementation(int iX, int iY) override
		// {
      // std::vector<std::vector<int>>::iterator it = std::find(_cellIndices2D.begin(), _cellIndices2D.end(), std::vector<int> {iX,iY});
      // if (it != _cellIndices2D.end() )
        // return _dataTransfer.getByLocalIndexCPU(std::distance(_cellIndices2D.begin(),it));
      // else
        // throw std::out_of_range ("Cell not found in the dataTransfer object");
		// }

		// void _read_from_implementation() override
		// {
			// _blockLattice.template readBlockLatticeByFunctor<Functor>(_dataTransfer.getRawPointerToGPUMemory(),_dataTransfer.getRawPointerToGPUCellIndices(),_dataTransfer.getNumberOfRegisteredCells());
		// }

		// void _transfer_from_implementation() override
		// {
			// _dataTransfer.transferDataToCPU();
		// }

		// void _registerCellsByIndicator()
		// {
			// Vector<T,2> tmp;
      // bool indicatorOutput[2]={false,false};
      // T physRange[2]={0.0,0.0};
			// // get min and max of lattice range of indicator
			// T min[2]={0.0,0.0};
			// T max[2]={0.0,0.0};

			// tmp = _indicator.getMin();
			// min[0] = tmp[0];
			// min[1] = tmp[1];

			// tmp = _indicator.getMax();
			// max[0] = tmp[0];
			// max[1] = tmp[1];

			// // get lattice coordinates of min and max
			// int minL[2]={0,0};
			// int maxL[2]={0,0};

			// // if not inside blockgeometry -> set on origin (cell 0,0)
			// if (!_blockGeometryStructure.getLatticeR(minL,min))
			// {
				// minL[0] = 0;
				// minL[1] = 0;
			// }

				// // if not inside blockgeometry -> set on extend (cell Nx,Ny)
			// if (!_blockGeometryStructure.getLatticeR(maxL,max))
			// {
				// maxL[0] = _blockGeometryStructure.getNx();
				// maxL[1] = _blockGeometryStructure.getNy();
			// }	



			// for(int iX=minL[0];iX<=maxL[0];++iX)
			// {
				// for (int iY=minL[1];iY<=maxL[1];++iY)
					// {
            // _blockGeometryStructure.getPhysR(physRange,iX,iY);
            // if (_indicator(indicatorOutput,physRange))
            // {
							// _dataTransfer.registerCell(_blockLattice.getCellIndex(iX,iY));
              // _cellIndices2D.push_back(std::vector<int> {iX,iY});
							// ++_numberOfCells;
            // }
          // }
			// }
		// }

	// private:

		// size_t                                                                  _numberOfCells=0;

		// IndicatorF2D<T>&                                                        _indicator;
		// BlockGeometryStructure2D<T>&                                            _blockGeometryStructure;
		// CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>   _dataTransfer;
		// BlockLattice2D<T,Lattice>&	                     	                      _blockLattice;
    // std::vector<std::vector<int>>                                           _cellIndices2D;
// };
#endif // end of CUDA
}; // end of namespace
#endif // end of includeguard
