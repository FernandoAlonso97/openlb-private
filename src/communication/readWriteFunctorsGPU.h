
/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef READ_WRITE_FUNCTORS_GPU_H
#define READ_WRITE_FUNCTORS_GPU_H

#include "core/cudaErrorHandler.h"
#include "core/blockLattice2D.h"
#include <vector>


namespace olb {
//////////////////////////////////////////////////////StackArray: Helper class in order to return arrays in functors
template<typename T, int size>
class StackArray final {

  private:
    T _data[size];

  public:

	StackArray() = default;

	StackArray(StackArray const & ) = default;

	StackArray(StackArray &&) = default;

	StackArray& operator= (StackArray const &) = default;

	StackArray& operator= (StackArray &&) = default;

  OPENLB_HOST_DEVICE
    T operator[] (int index) const
    {
      OLB_PRECONDITION(index<size);
      return _data[index];
    }

    OPENLB_HOST_DEVICE
    T& operator[] (int index)
      {
       OLB_PRECONDITION(index<size);
	     return _data[index];	
      }

};

template<typename T, int size>
OPENLB_HOST_DEVICE
StackArray<T,size> operator+ (StackArray<T,size> const &a,StackArray<T,size> const &b)
{
  StackArray<T,size> temp{};
  for(int i=0;i<size;++i)
  {
    temp[i] = a[i] + b[i];
  }
  return temp;
}

template<typename T, int size>
OPENLB_HOST_DEVICE
StackArray<T,size> operator- (StackArray<T,size> const &a,StackArray<T,size> const &b)
{
  StackArray<T,size> temp{};
  for(int i=0;i<size;++i)
  {
    temp[i] = a[i] - b[i];
  }
  return temp;
}

///////////////////////////////////////////////Definition of Functors
// ALL FUNCTORS HAVE TO IMPLEMENT THE FOLLOWING CONSTRUCT IN ORDER TO BE USABLE WITH THE NEW CONCEPT
//
// template<typename T, template<typename U> class Lattice>
// class ExampleFunctor {
// public:
// typedef StackArray<T,3> returnType; // change "3" to number of data entries that are returned by the functor
//
// OPENLB_HOST_DEVICE
// static returnType readFromField( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
// {
//   returnType tmp;
//   /* do calculation and write into tmp */
//   return tmp;
// }
// OPENLB_HOST_DEVICE
// static void writeToField( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const returnType& input)
// {
//  /* do calculation and write cellIndex to cellData */
// }
//
// };
//
//////////////////////////////////////////////////////////////VelocityFunctor/////////////////////////////////////////
template<typename T, template<typename U> class Lattice>
class VelocityFunctor {
public:
typedef StackArray<T,Lattice<T>::d> returnType; // change "3" to number of data entries that are returned by the functor

OPENLB_HOST_DEVICE
static returnType readFromField( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
{
  returnType tmp;
  for (int i=0;i<Lattice<T>::d;++i) //extract velocity
    tmp[i] = cellData[(Lattice<T>::uIndex+i)][cellIndex];
  return tmp;
}

OPENLB_HOST_DEVICE
static void writeToField( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const returnType& input)
{
  for ( int i=0;i<Lattice<T>::d;++i) //write velocity
    cellData[Lattice<T>::uIndex+i][cellIndex] = input[i];
}
};

//////////////////////////////////////////////////////////////ForceFunctor/////////////////////////////////////////
template<typename T, template<typename U> class Lattice>
class ForceFunctor {
public:
typedef StackArray<T,Lattice<T>::d> returnType; // change "3" to number of data entries that are returned by the functor

OPENLB_HOST_DEVICE
static returnType readFromField( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
{
  returnType tmp;
  for (int i=0;i<Lattice<T>::d;++i) // extract force
    tmp[i] = cellData[(Lattice<T>::forceIndex+i)][cellIndex];
  return tmp;
}

OPENLB_HOST_DEVICE
static void writeToField( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const returnType& input)
{
  for ( int i=0;i<Lattice<T>::d;++i) // write force
    cellData[Lattice<T>::forceIndex+i][cellIndex] = input[i];
}
};

}; // end of namespace
#endif // end of includeguard

