/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef WRITE_FUNCTOR_TO_GPU_2D_H
#define WRITE_FUNCTOR_TO_GPU_2D_H

#include "core/cudaErrorHandler.h"
#include "core/blockLattice2D.h"
#include "communication/CPUGPUDataExchange.h"
#include <vector>


namespace olb {
#ifdef ENABLE_CUDA
///////////////////////////////////////////////////////PURE ABSTRACT INTERFACE DEFINITION FOR WRITEFUNCTORTOGPU/////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class > class Functor>
class WriteFunctorToGPU2D {

	public:

		WriteFunctorToGPU2D() = default;

		virtual ~WriteFunctorToGPU2D () = default;

		void transfer()
		{
		  _transfer_from_implementation();
		}

		void write()
		{
		  _write_from_implementation();
		}

		void transferAndWrite()
		{
		  transfer();
		  write();
		}

		typename Functor<T,Lattice>::returnType& setByLocalIndex(int iX,int iY)
		{
			return _setByLocalIndex_from_implementation(iX,iY);
		}

	private:

		virtual typename Functor<T,Lattice>::returnType& _setByLocalIndex_from_implementation(int iX, int iY) = 0;
		virtual void _write_from_implementation() = 0;
		virtual void _transfer_from_implementation() =0;
};


////////////////////////////////////////////////////IMPLEMENTATION FOR A WRITEFUNCTORTOGPU BY RANGE/////////////////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class> class Functor>
class WriteFunctorByRangeToGPU2D final: public WriteFunctorToGPU2D<T,Lattice,Functor> {

	public:

		WriteFunctorByRangeToGPU2D() = delete;

		WriteFunctorByRangeToGPU2D(BlockLattice2D<T,Lattice>& blockLattice,const size_t iX0,const size_t iX1,const size_t iY0, const size_t iY1):WriteFunctorToGPU2D<T,Lattice,Functor>(),
		  _dataTransfer(CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>{(iX1-iX0+1)*(iY1-iY0+1)}),
		  _blockLattice(blockLattice),
		  _iX0(iX0),
		  _iX1(iX1),
		  _iY0(iY0),
		  _iY1(iY1)
	    {
			_registerCellsByRange();
		  _dataTransfer.transferIndexToGPU();
		}

		~WriteFunctorByRangeToGPU2D() override = default; 

	private:

		typename Functor<T,Lattice>::returnType& _setByLocalIndex_from_implementation(int iX,int iY) override
		{
			return _dataTransfer.setByLocalIndexCPU(util::getCellIndex2D(iX,iY,(_iY1-_iY0+1)));
		}

		void _write_from_implementation() override
		{
		  _blockLattice.template writeBlockLatticeByFunctor<Functor>(_dataTransfer.getRawPointerToGPUMemory(),_dataTransfer.getRawPointerToGPUCellIndices(),_dataTransfer.getNumberOfRegisteredCells());
		}
		
		void _transfer_from_implementation() override
		{
		  _dataTransfer.transferDataToGPU();
		}

		void _registerCellsByRange()
		{
			for (int iX=_iX0;iX<=_iX1;++iX)
			{
				for (int iY=_iY0;iY<=_iY1;++iY)
				{
				  _dataTransfer.registerCell(_blockLattice.getCellIndex(iX,iY));
				}
			}
		}


	private:

		CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>   _dataTransfer;
		BlockLattice2D<T,Lattice>&	                     	                      _blockLattice;
		size_t 							                                                    _iX0,_iX1,_iY0,_iY1;

};

///////////////////////////////////////////////////////PURE ABSTRACT INTERFACE DEFINITION FOR WRITEFUNCTORTOGPU/////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class > class Functor>
class WriteFunctorToGPU3D {

    public:

        WriteFunctorToGPU3D() = default;

        virtual ~WriteFunctorToGPU3D () = default;

        void transfer()
        {
          _transfer_from_implementation();
        }

        void write()
        {
          _write_from_implementation();
        }

        void transferAndWrite()
        {
          transfer();
          write();
        }

        typename Functor<T,Lattice>::returnType& setByLocalIndex(int iX,int iY,int iZ)
        {
            return _setByLocalIndex_from_implementation(iX,iY,iZ);
        }

        typename Functor<T,Lattice>::returnType& setByGlobalIndex(int iX,int iY,int iZ)
        {
            return _setByGlobalIndex_from_implementation(iX,iY,iZ);
        }

    private:

        virtual typename Functor<T,Lattice>::returnType& _setByLocalIndex_from_implementation(int iX, int iY, int iZ) = 0;
        virtual typename Functor<T,Lattice>::returnType& _setByGlobalIndex_from_implementation(int iX, int iY, int iZ) = 0;
        virtual void _write_from_implementation() = 0;
        virtual void _transfer_from_implementation() =0;
};


////////////////////////////////////////////////////IMPLEMENTATION FOR A WRITEFUNCTORTOGPU BY RANGE/////////////////////////
template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class> class Functor>
class WriteFunctorByRangeToGPU3D final: public WriteFunctorToGPU3D<T,Lattice,Functor> {

    public:

        WriteFunctorByRangeToGPU3D() = delete;

        WriteFunctorByRangeToGPU3D(BlockLattice3D<T,Lattice>& blockLattice,const size_t iX0,const size_t iX1,
                const size_t iY0, const size_t iY1, const size_t iZ0, const size_t iZ1):
            WriteFunctorToGPU3D<T,Lattice,Functor>(),
          _dataTransfer(CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>{(iX1-iX0+1)*(iY1-iY0+1)*(iZ1-iZ0+1)}),
          _blockLattice(blockLattice),
          _iX0(iX0),
          _iX1(iX1),
          _iY0(iY0),
          _iY1(iY1),
          _iZ0(iZ0),
          _iZ1(iZ1)
        {
            _registerCellsByRange();
            _dataTransfer.transferIndexToGPU();
        }

        ~WriteFunctorByRangeToGPU3D() override = default;

    private:

        typename Functor<T,Lattice>::returnType& _setByLocalIndex_from_implementation(int iX,int iY,int iZ) override
        {
            return _dataTransfer.setByLocalIndexCPU(util::getCellIndex3D(iX,iY,iZ,(_iY1-_iY0+1),(_iZ1-_iZ0+1)));
        }


        typename Functor<T,Lattice>::returnType& _setByGlobalIndex_from_implementation(int iX, int iY, int iZ) override
        {
            return _dataTransfer.setByLocalIndexCPU(util::getCellIndex3D(iX-_iX0,iY-_iY0,iZ-_iZ0,(_iY1-_iY0+1),(_iZ1-_iZ0+1)));
        }

        void _write_from_implementation() override
        {
          _blockLattice.template writeBlockLatticeByFunctor<Functor>(_dataTransfer.getRawPointerToGPUMemory(),_dataTransfer.getRawPointerToGPUCellIndices(),_dataTransfer.getNumberOfRegisteredCells());
        }

        void _transfer_from_implementation() override
        {
          _dataTransfer.transferDataToGPU();
        }

        void _registerCellsByRange()
        {
            for (int iX=_iX0;iX<=_iX1;++iX)
                for (int iY=_iY0;iY<=_iY1;++iY)
                    for(int iZ=_iZ0;iZ<=_iZ1;++iZ)
                        _dataTransfer.registerCell(_blockLattice.getCellIndex(iX,iY,iZ));
        }


    private:

        CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>   _dataTransfer;
        BlockLattice3D<T,Lattice>&                                              _blockLattice;
        size_t                                                                  _iX0,_iX1,_iY0,_iY1,_iZ0,_iZ1;

};

////////////////////////////////////////////////////IMPLEMENTATION FOR A WRITEFUNCTORTOGPU BY FUCTOR/////////////////////////
// template<typename T, template<typename U> class Lattice, template<typename ,template <typename > class> class Functor>
// class WriteFunctorByIndicatorToGPU2D : public WriteFunctorToGPU2D<T,Lattice,Functor> {

	// public:

		// WriteFunctorByIndicatorToGPU2D() = delete;

		// WriteFunctorByIndicatorToGPU2D(BlockLattice2D<T,Lattice>& blockLattice,BlockGeometryStructure2D<T>& blockGeometryStructure, IndicatorF2D<T>& indicator):
			// WriteFunctorToGPU2D<T,Lattice,Functor>(),
			// _indicator(indicator),
			// _blockGeometryStructure(blockGeometryStructure),
			// _dataTransfer(CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>(0)),
			// _blockLattice(blockLattice),
      // _cellIndices2D(std::vector<std::vector<int> >{})
		// {
			// _registerCellsByIndicator();
      // _dataTransfer = CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>(_numberOfCells);
			// _dataTransfer.transferIndexToGPU();
		// } 


	// private:

		// typename Functor<T,Lattice>::returnType& _setByLocalIndex_from_implementation(int iX,int iY) override
		// {
      // std::vector<std::vector<int>>::iterator it = std::find(_cellIndices2D.begin(), _cellIndices2D.end(), std::vector<int> {iX,iY});
      // if (it != _cellIndices2D.end() )
        // return _dataTransfer.setByLocalIndexCPU(std::distance(_cellIndices2D.begin(),it));
      // else
        // throw std::out_of_range ("Cell not found in the dataTransfer object");
		// }

		// void _write_from_implementation() const override
		// {
			// _blockLattice.template writeBlockLatticeByFunctor<Functor>(_dataTransfer.getRawPointerToGPUMemory(),_dataTransfer.getRawPointerToGPUCellIndices(),_dataTransfer.getNumberOfRegisteredCells());
		// }

		// void _transfer_from_implementation() override
		// {
			// _dataTransfer.transferDataToGPU();
		// }

		// void _registerCellsByIndicator()
		// {
			// Vector<T,2> tmp;
      // bool indicatorOutput[2]={false,false};
      // T physRange[2]={0.0,0.0};
			// // get min and max of lattice range of indicator
			// T min[2]={0.0,0.0};
			// T max[2]={0.0,0.0};

			// tmp = _indicator.getMin();
			// min[0] = tmp[0];
			// min[1] = tmp[1];

			// tmp = _indicator.getMax();
			// max[0] = tmp[0];
			// max[1] = tmp[1];

			// // get lattice coordinates of min and max
			// int minL[2]={0,0};
			// int maxL[2]={0,0};

			// // if not inside blockgeometry -> set on origin (cell 0,0)
			// if (!_blockGeometryStructure.getLatticeR(minL,min))
			// {
				// minL[0] = 0;
				// minL[1] = 0;
			// }

				// // if not inside blockgeometry -> set on extend (cell Nx,Ny)
			// if (!_blockGeometryStructure.getLatticeR(maxL,max))
			// {
				// maxL[0] = _blockGeometryStructure.getNx();
				// maxL[1] = _blockGeometryStructure.getNy();
			// }	



			// for(int iX=minL[0];iX<=maxL[0];++iX)
			// {
				// for (int iY=minL[1];iY<=maxL[1];++iY)
					// {
            // _blockGeometryStructure.getPhysR(physRange,iX,iY);
            // if (_indicator(indicatorOutput,physRange))
            // {
							// _dataTransfer.registerCell(_blockLattice.getCellIndex(iX,iY));
              // _cellIndices2D.push_back(std::vector<int> {iX,iY});
							// ++_numberOfCells;
            // }
          // }
			// }
		// }

	// private:

		// size_t                                                                  _numberOfCells=0;

		// IndicatorF2D<T>&                                                        _indicator;
		// BlockGeometryStructure2D<T>&                                            _blockGeometryStructure;
		// CPUGPUTransferBase<T,Lattice,typename Functor<T,Lattice>::returnType>   _dataTransfer;
		// BlockLattice2D<T,Lattice>&	                     	                      _blockLattice;
    // std::vector<std::vector<int>>                                           _cellIndices2D;

// };

#endif // end of CUDA
}; // end of namespace
#endif // includeguard

