/*
 * readExternalField.hh
 *
 *  Created on: Feb 12, 2020
 *      Author: Bastian Horvat
 */

/* This file allows the read and storage of a .szplt file in a vector. tecio library is required */

#include "/media/administrator/HTMWTUM/Privat/BHorvat/HeliOW/tecIO/teciosrc/TECIO.h"

#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <stdexcept>
#include <vector>
#include <cmath>
#include <array>

enum class variablesRead{
	coordX = 1,
	coordY,
	coordZ,
	velX,
	velY,
	velZ
};

enum class variablesWrite{
	coordX = 1,
	coordY = 2,
	coordZ = 3,
};

template<typename T>
void rotation(T vector[3], float phi, float theta, float psi, bool absvalue = false, bool round = false){

	if(not round)
	{
		T rotatedVector[3];
		rotatedVector[0] = cos(theta)*cos(psi)*vector[0] + cos(theta)*sin(psi)*vector[1] - sin(theta)*vector[2];
		rotatedVector[1] = (sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi))*vector[0]
				+ (sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi))*vector[1] + (sin(phi)*cos(theta))*vector[2];
		rotatedVector[2] = (cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi))*vector[0]
				+ (cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi))*vector[1] + (cos(phi)*cos(theta))*vector[2];
		vector[0] = rotatedVector[0];
		vector[1] = rotatedVector[1];
		vector[2] = rotatedVector[2];
	}
	else
	{
		double rotatedVector[3];
		rotatedVector[0] = cos(theta)*cos(psi)*vector[0] + cos(theta)*sin(psi)*vector[1] - sin(theta)*vector[2];
		rotatedVector[1] = (sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi))*vector[0]
				+ (sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi))*vector[1] + (sin(phi)*cos(theta))*vector[2];
		rotatedVector[2] = (cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi))*vector[0]
				+ (cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi))*vector[1] + (cos(phi)*cos(theta))*vector[2];
		vector[0] = std::round(rotatedVector[0]);
		vector[1] = std::round(rotatedVector[1]);
		vector[2] = std::round(rotatedVector[2]);
	}

	if(absvalue)
	{
		vector[0] = std::abs(vector[0]);
		vector[1] = std::abs(vector[1]);
		vector[2] = std::abs(vector[2]);
	}
}

template<typename T>
void writePreamble(const std::string& fullName, size_t nx, size_t ny, size_t nz,
                                   T originX, T originY, T originZ)
{
    std::ofstream fout(fullName.c_str());
    if (!fout) {
      std::cout << "Error: could not open " << fullName << std::endl;
    }
    T spacing = T(1);
    if (nx>0) {
      spacing = T(1)/(T)nx;
    }

    fout << "<?xml version=\"1.0\"?>\n";
    fout << "<VTKFile type=\"ImageData\" version=\"0.1\" "
         << "byte_order=\"LittleEndian\">\n";
    fout << "<ImageData WholeExtent=\""
         << "0" <<" "<< nx <<" "
         << "0" <<" "<< ny <<" "
         << "0" <<" "<< nz
         << "\" Origin=\"" << originX << " " << originY << " " << originZ
         << "\" Spacing=\"" << spacing << " " << spacing << " " << spacing << "\">\n";

    fout << "<Piece Extent=\""
         << 0 <<" "<< nx <<" "
         << 0 <<" "<< ny <<" "
         << 0 <<" "<< nz <<"\">\n";

    fout << "<PointData>\n";
    fout.close();
}

void closePreamble(const std::string& fullNamePiece)
{
    std::ofstream fout(fullNamePiece.c_str(), std::ios::app );
    if (!fout) {
      std::cout << "Error: could not open " << fullNamePiece << std::endl;
    }
    fout << "</PointData>\n";
    fout << "</Piece>\n";
    fout << "</ImageData>\n";
    fout << "</VTKFile>\n";
    fout.close();
}

template<typename T>
void writeRawData(const std::string& fullNameVti, std::string const & variableName, unsigned int const targetDim,
                                       std::array<int64_t,3> extent, T** const values)
{
std::ofstream fout(fullNameVti.c_str(), std::ios::app);
  if (!fout) {
    std::cout << "Error: could not open " << fullNameVti << std::endl;
  }

  fout << "<DataArray " ;
  if (targetDim == 1) {
    fout << "type=\"Float32\" Name=\"" << variableName << "\">\n";
  } else {
    fout << "type=\"Float32\" Name=\"" << variableName << "\" "
         << "NumberOfComponents=\"" << targetDim << "\">\n";
  }

  for (size_t iZ = 0; iZ < extent[2]; ++iZ)
	  for (size_t iY = 0; iY < extent[1]; ++iY)
		  for (size_t iX = 0; iX < extent[0]; ++iX)
			for (int iDim = 0; iDim < targetDim; ++iDim) {
				size_t iIndex = iX*extent[2]*extent[1] + iY*extent[2] + iZ;
				fout << values[iDim][iIndex] << " ";
			}

  fout << "\n</DataArray>\n";

  fout.close();
}



// We retrieve the return value from each tec call, but ignore it. Calls return 0 for success, non-zero for failure.
template<typename T>
T** readExternalField(std::string fileName, float const rotateAngles[3], std::vector<int64_t>& extent, const T conversionVelocity)
{
    try
    {
        // Open a .szplt file for reading with TECIO
        void* inputFileHandle = NULL;
        int i = tecFileReaderOpen(fileName.c_str(), &inputFileHandle);

        // Read the characteristics of the data set
        char* dataSetTitle = NULL;
        i = tecDataSetGetTitle(inputFileHandle, &dataSetTitle);

        int32_t numVars;
        i = tecDataSetGetNumVars(inputFileHandle, &numVars);
        std::ostringstream outputStream;
        for (int32_t var = 1; var <= numVars; ++var)
        {
            char* name = NULL;
            i = tecVarGetName(inputFileHandle, var, &name);
            outputStream << name;
            if (var < numVars)
                outputStream << ',';
            tecStringFree(&name);
        }
        outputStream << std::endl;


        int32_t fileType;
        i = tecFileGetType(inputFileHandle, &fileType);

        int32_t numZones;
        i = tecDataSetGetNumZones(inputFileHandle, &numZones);

        int32_t isDouble = 0;
        if(numZones != 1)
        {
        	std::string error = std::to_string(numZones) + " zones detected! Only single zone files supported!";
        	throw std::runtime_error(error);
        }

        if (numZones > 0)
        {
            int32_t type;
            i = tecZoneVarGetType(inputFileHandle, 1, 1, &type);
            if (type == FieldDataType_Double)
                isDouble = 1;
        }

//        for (int32_t inputZone = 1; inputZone <= numZones; ++inputZone)
//        {
//            // Retrieve zone characteristics
        int32_t inputZone = numZones;
		int32_t zoneType;
		i = tecZoneGetType(inputFileHandle, inputZone, &zoneType);
		if (zoneType == 6 || zoneType == 7)
			throw std::runtime_error("Unsupported zone type.");
//
//            char* zoneTitle = NULL;
//            i = tecZoneGetTitle(inputFileHandle, inputZone, &zoneTitle);
//
		int64_t maxCount[3];
		i = tecZoneGetIJK(inputFileHandle, inputZone, &maxCount[0], &maxCount[1], &maxCount[2]);

		std::vector<int32_t> varTypes(numVars,0);
		outputStream << "Var type: ";
		for (int32_t var = 1; var <= numVars; ++var)
		{
			i = tecZoneVarGetType(inputFileHandle, inputZone, var, &varTypes[var - 1]);
			outputStream << varTypes[var-1] << ",";
		}
		outputStream << std::endl;

		double solutionTime;
		i = tecZoneGetSolutionTime(inputFileHandle, inputZone, &solutionTime);

		outputStream << "Solution time: " << solutionTime << std::endl;

		int64_t dataSize = maxCount[0]*maxCount[1]*maxCount[2];
		int64_t chunkSize = dataSize;
		int64_t numChunks = dataSize/chunkSize+1;

		T** velocityField;
		velocityField = new T*[3];
		for(int32_t iDim = 0; iDim < 3; ++iDim)
			velocityField[iDim] = new T[dataSize];

		std::vector<std::vector<float>> coordinates(3);
		std::vector<std::vector<float>> velocities(3);

		for(auto & iCoord : coordinates)
			iCoord.resize(chunkSize);

		for(auto & iVel : velocities)
			iVel.resize(chunkSize);

		std::vector<size_t> cellX(dataSize);
		std::vector<size_t> cellZ(dataSize);
		std::vector<size_t> cellY(dataSize);


		float minVal[3], maxVal[3], delta[3];

		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX), 1, 1, &minVal[0]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX), dataSize, 1, &maxVal[0]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX), 2, 1, &delta[0]);

		delta[0] = std::abs(delta[0]-minVal[0]);

		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY), 1, 1, &minVal[1]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY), dataSize, 1, &maxVal[1]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY), maxCount[0]+1, 1, &delta[1]);

		delta[1] = std::abs(delta[1]-minVal[1]);

		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ), 1, 1, &minVal[2]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ), dataSize, 1, &maxVal[2]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ), (maxCount[0]+1)*maxCount[1]+1, 1, &delta[2]);

		delta[2] = std::abs(delta[2]-minVal[2]);

		outputStream << "data points: " << dataSize << " (" << maxCount[0] << "," << maxCount[1] << "," << maxCount[2] << ")" << std::endl;
		rotation(maxCount,rotateAngles[0],rotateAngles[1],rotateAngles[2],true,true);
		outputStream << "data points (rotated): " << dataSize << " (" << maxCount[0] << "," << maxCount[1] << "," << maxCount[2] << ")" << std::endl;

		extent[0] = maxCount[0];
		extent[1] = maxCount[1];
		extent[2] = maxCount[2];

		outputStream << "minVal[0],maxVal[0],delta[0]: " << minVal[0] << "," << maxVal[0] << "," << delta[0] << std::endl;
		outputStream << "minVal[1],maxVal[1],delta[1]: " << minVal[1] << "," << maxVal[1] << "," << delta[1] << std::endl;
		outputStream << "minVal[2],maxVal[2],delta[2]: " << minVal[2] << "," << maxVal[2] << "," << delta[2] << std::endl;

		rotation(delta,rotateAngles[0],rotateAngles[1],rotateAngles[2],true);
		rotation(minVal,rotateAngles[0],rotateAngles[1],rotateAngles[2]);
		rotation(maxVal,rotateAngles[0],rotateAngles[1],rotateAngles[2]);

		for(unsigned int iDim = 0; iDim < 3; ++iDim)
		{
			if(maxVal[iDim] < minVal[iDim])
				std::swap(maxVal[iDim],minVal[iDim]);
		}

		outputStream << "minVal[0],maxVal[0],delta[0]: " << minVal[0] << "," << maxVal[0] << "," << delta[0] << std::endl;
		outputStream << "minVal[1],maxVal[1],delta[1]: " << minVal[1] << "," << maxVal[1] << "," << delta[1] << std::endl;
		outputStream << "minVal[2],maxVal[2],delta[2]: " << minVal[2] << "," << maxVal[2] << "," << delta[2] << std::endl;

		// Retrieve zone data and send to the output file
		for(int32_t iChunk = 0; iChunk < numChunks; ++iChunk)
		{
			int64_t index = iChunk*chunkSize+1;
			if((iChunk+1)*chunkSize > dataSize)
			{
				chunkSize = dataSize - iChunk*chunkSize;
				if(chunkSize == 0)
					break;
			}


			outputStream << "Chunk: " << iChunk << ", index: " << index  << ", chunk size: " << chunkSize << std::endl;

			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX),
					index, chunkSize, coordinates[0].data());
			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY),
					index, chunkSize, coordinates[1].data());
			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ),
					index, chunkSize, coordinates[2].data());

			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::velX),
					index, chunkSize, velocities[0].data());
			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::velY),
					index, chunkSize, velocities[1].data());
			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::velZ),
					index, chunkSize, velocities[2].data());

			size_t cellX, cellY, cellZ;
			T coordinate[3];
			T velocity[3];

			for(unsigned int iValue = 0; iValue < chunkSize; ++iValue)
			{
				coordinate[0] = coordinates[0][iValue];
				coordinate[1] = coordinates[1][iValue];
				coordinate[2] = coordinates[2][iValue];

				rotation(coordinate,rotateAngles[0],rotateAngles[1],rotateAngles[2]);

				coordinate[0] -= minVal[0];
				if(std::signbit(static_cast<float>(variablesWrite::coordX)))
					coordinate[0] = -(coordinate[0]-(maxVal[0]-minVal[0]));

				coordinate[1] -= minVal[1];
				if(std::signbit(static_cast<float>(variablesWrite::coordY)))
					coordinate[1] = -(coordinate[1]-(maxVal[1]-minVal[1]));

				coordinate[2] -= minVal[2];
				if(std::signbit(static_cast<float>(variablesWrite::coordZ)))
					coordinate[2] = -(coordinate[2]-(maxVal[2]-minVal[2]));


				cellX = static_cast<size_t>(std::round(coordinate[0]/delta[0]));
				cellY = static_cast<size_t>(std::round(coordinate[1]/delta[1]));
				cellZ = static_cast<size_t>(std::round(coordinate[2]/delta[2]));

				size_t fieldIndex = cellX*maxCount[1]*maxCount[2]+cellY*maxCount[2]+cellZ;
				if(fieldIndex > dataSize)
				{
					outputStream << cellX << "," << cellY << "," << cellZ << std::endl;
					outputStream << coordinates[0][iValue] << "," << coordinates[1][iValue] << "," << coordinates[2][iValue] << std::endl;
					outputStream << coordinate[0] << "," << coordinate[1] << "," << coordinate[2] << std::endl;
					break;
				}

				velocity[0] = velocities[0][iValue];
                velocity[1] = velocities[1][iValue];
				velocity[2] = velocities[2][iValue];

				rotation(velocity,rotateAngles[0],rotateAngles[1],rotateAngles[2]);

				velocityField[0][fieldIndex] = velocity[0]/conversionVelocity;
				velocityField[1][fieldIndex] = velocity[1]/conversionVelocity;
				velocityField[2][fieldIndex] = velocity[2]/conversionVelocity;

			}

			outputStream << std::endl;
		}


        // Close file
        i = tecFileReaderClose(&inputFileHandle);

        std::cout << outputStream.str();

		return velocityField;

    }
    catch (std::runtime_error const& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        return nullptr;
    }
}

// We retrieve the return value from each tec call, but ignore it. Calls return 0 for success, non-zero for failure.
bool* readFluidMask(std::string fileName, float const rotateAngles[3])
{
    try
    {
    	size_t count = 0;
        // Open a .szplt file for reading with TECIO
        void* inputFileHandle = NULL;
        int i = tecFileReaderOpen(fileName.c_str(), &inputFileHandle);

        // Read the characteristics of the data set
        char* dataSetTitle = NULL;
        i = tecDataSetGetTitle(inputFileHandle, &dataSetTitle);

        int32_t numVars;
        i = tecDataSetGetNumVars(inputFileHandle, &numVars);
        std::ostringstream outputStream;
        for (int32_t var = 1; var <= numVars; ++var)
        {
            char* name = NULL;
            i = tecVarGetName(inputFileHandle, var, &name);
            outputStream << name;
            if (var < numVars)
                outputStream << ',';
            tecStringFree(&name);
        }
        outputStream << std::endl;


        int32_t fileType;
        i = tecFileGetType(inputFileHandle, &fileType);

        int32_t numZones;
        i = tecDataSetGetNumZones(inputFileHandle, &numZones);

        int32_t isDouble = 0;
        if(numZones != 1)
        {
        	std::string error = std::to_string(numZones) + " zones detected! Only single zone files supported!";
        	throw std::runtime_error(error);
        }

        if (numZones > 0)
        {
            int32_t type;
            i = tecZoneVarGetType(inputFileHandle, 1, 1, &type);
            if (type == FieldDataType_Double)
                isDouble = 1;
        }

//        for (int32_t inputZone = 1; inputZone <= numZones; ++inputZone)
//        {
//            // Retrieve zone characteristics
        int32_t inputZone = numZones;
		int32_t zoneType;
		i = tecZoneGetType(inputFileHandle, inputZone, &zoneType);
		if (zoneType == 6 || zoneType == 7)
			throw std::runtime_error("Unsupported zone type.");
//
//            char* zoneTitle = NULL;
//            i = tecZoneGetTitle(inputFileHandle, inputZone, &zoneTitle);
//
		int64_t maxCount[3];
		i = tecZoneGetIJK(inputFileHandle, inputZone, &maxCount[0], &maxCount[1], &maxCount[2]);

		std::vector<int32_t> varTypes(numVars,0);
		outputStream << "Var type: ";
		for (int32_t var = 1; var <= numVars; ++var)
		{
			i = tecZoneVarGetType(inputFileHandle, inputZone, var, &varTypes[var - 1]);
			outputStream << varTypes[var-1] << ",";
		}
		outputStream << std::endl;

		double solutionTime;
		i = tecZoneGetSolutionTime(inputFileHandle, inputZone, &solutionTime);

		outputStream << "Solution time: " << solutionTime << std::endl;

		int64_t dataSize = maxCount[0]*maxCount[1]*maxCount[2];
		int64_t chunkSize = dataSize;
		int64_t numChunks = dataSize/chunkSize+1;

		bool* fluidMask;
		fluidMask = new bool[dataSize];

		std::vector<std::vector<float>> coordinates(3);
		std::vector<double> fluidMaskRead(chunkSize);

		for(auto & iCoord : coordinates)
			iCoord.resize(chunkSize);

		std::vector<size_t> cellX(dataSize);
		std::vector<size_t> cellZ(dataSize);
		std::vector<size_t> cellY(dataSize);


		float minVal[3], maxVal[3], delta[3];
		std::vector<int64_t> extent(3);

		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX), 1, 1, &minVal[0]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX), dataSize, 1, &maxVal[0]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX), 2, 1, &delta[0]);

		delta[0] = std::abs(delta[0]-minVal[0]);

		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY), 1, 1, &minVal[1]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY), dataSize, 1, &maxVal[1]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY), maxCount[0]+1, 1, &delta[1]);

		delta[1] = std::abs(delta[1]-minVal[1]);

		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ), 1, 1, &minVal[2]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ), dataSize, 1, &maxVal[2]);
		tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ), (maxCount[0]+1)*maxCount[1]+1, 1, &delta[2]);

		delta[2] = std::abs(delta[2]-minVal[2]);

		outputStream << "data points: " << dataSize << " (" << maxCount[0] << "," << maxCount[1] << "," << maxCount[2] << ")" << std::endl;
		rotation(maxCount,rotateAngles[0],rotateAngles[1],rotateAngles[2],true,true);
		outputStream << "data points (rotated): " << dataSize << " (" << maxCount[0] << "," << maxCount[1] << "," << maxCount[2] << ")" << std::endl;

		extent[0] = maxCount[0];
		extent[1] = maxCount[1];
		extent[2] = maxCount[2];

		outputStream << "minVal[0],maxVal[0],delta[0]: " << minVal[0] << "," << maxVal[0] << "," << delta[0] << std::endl;
		outputStream << "minVal[1],maxVal[1],delta[1]: " << minVal[1] << "," << maxVal[1] << "," << delta[1] << std::endl;
		outputStream << "minVal[2],maxVal[2],delta[2]: " << minVal[2] << "," << maxVal[2] << "," << delta[2] << std::endl;

		rotation(delta,rotateAngles[0],rotateAngles[1],rotateAngles[2],true);
		rotation(minVal,rotateAngles[0],rotateAngles[1],rotateAngles[2]);
		rotation(maxVal,rotateAngles[0],rotateAngles[1],rotateAngles[2]);

		for(unsigned int iDim = 0; iDim < 3; ++iDim)
		{
			if(maxVal[iDim] < minVal[iDim])
				std::swap(maxVal[iDim],minVal[iDim]);
		}

		outputStream << "minVal[0],maxVal[0],delta[0]: " << minVal[0] << "," << maxVal[0] << "," << delta[0] << std::endl;
		outputStream << "minVal[1],maxVal[1],delta[1]: " << minVal[1] << "," << maxVal[1] << "," << delta[1] << std::endl;
		outputStream << "minVal[2],maxVal[2],delta[2]: " << minVal[2] << "," << maxVal[2] << "," << delta[2] << std::endl;

		// Retrieve zone data and send to the output file
		for(int32_t iChunk = 0; iChunk < numChunks; ++iChunk)
		{
			int64_t index = iChunk*chunkSize+1;
			if((iChunk+1)*chunkSize > dataSize)
			{
				chunkSize = dataSize - iChunk*chunkSize;
				if(chunkSize == 0)
					break;
			}


			outputStream << "Chunk: " << iChunk << ", index: " << index  << ", chunk size: " << chunkSize << std::endl;

			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordX),
					index, chunkSize, coordinates[0].data());
			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordY),
					index, chunkSize, coordinates[1].data());
			tecZoneVarGetFloatValues(inputFileHandle, inputZone, static_cast<int32_t>(variablesRead::coordZ),
					index, chunkSize, coordinates[2].data());

			tecZoneVarGetDoubleValues(inputFileHandle, inputZone, 4,
					index, chunkSize, fluidMaskRead.data() );

			size_t cellX, cellY, cellZ;
			T coordinate[3];

			for(unsigned int iValue = 0; iValue < chunkSize; ++iValue)
			{
				coordinate[0] = coordinates[0][iValue];
				coordinate[1] = coordinates[1][iValue];
				coordinate[2] = coordinates[2][iValue];

				rotation(coordinate,rotateAngles[0],rotateAngles[1],rotateAngles[2]);

				coordinate[0] -= minVal[0];
				if(std::signbit(static_cast<float>(variablesWrite::coordX)))
					coordinate[0] = -(coordinate[0]-(maxVal[0]-minVal[0]));

				coordinate[1] -= minVal[1];
				if(std::signbit(static_cast<float>(variablesWrite::coordY)))
					coordinate[1] = -(coordinate[1]-(maxVal[1]-minVal[1]));

				coordinate[2] -= minVal[2];
				if(std::signbit(static_cast<float>(variablesWrite::coordZ)))
					coordinate[2] = -(coordinate[2]-(maxVal[2]-minVal[2]));


				cellX = static_cast<size_t>(std::round(coordinate[0]/delta[0]));
				cellY = static_cast<size_t>(std::round(coordinate[1]/delta[1]));
				cellZ = static_cast<size_t>(std::round(coordinate[2]/delta[2]));

				size_t fieldIndex = cellX*maxCount[1]*maxCount[2]+cellY*maxCount[2]+cellZ;
				if(fieldIndex > dataSize)
				{
					outputStream << cellX << "," << cellY << "," << cellZ << std::endl;
					outputStream << coordinates[0][iValue] << "," << coordinates[1][iValue] << "," << coordinates[2][iValue] << std::endl;
					outputStream << coordinate[0] << "," << coordinate[1] << "," << coordinate[2] << std::endl;
					break;
				}

				if(fluidMaskRead[iValue] == 0)
					++count;
				fluidMask[fieldIndex] = static_cast<bool>(fluidMaskRead[iValue]);

			}

			outputStream << std::endl;
		}


        // Close file
        i = tecFileReaderClose(&inputFileHandle);

        writePreamble("fluidMask.vti",maxCount[0]-1,maxCount[1]-1,maxCount[2]-1,0,0,0);
        writeRawData("fluidMask.vti","fluidMask",1,{maxCount[0],maxCount[1],maxCount[2]},&fluidMask);
        closePreamble("fluidMask.vti");

        std::cout << outputStream.str();
        std::cout << "Wall cells: " << count << std::endl;

		return fluidMask;

    }
    catch (std::runtime_error const& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        return nullptr;
    }
}





