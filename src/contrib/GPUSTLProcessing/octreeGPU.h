#ifndef OCTREE_GPU_H
#define OCTREE_GPU_H

#define THRUST_IGNORE_DEPRECATED_CPP_DIALECT //suppress compiler warnings about C++11 deprecated
#define CUB_IGNORE_DEPRECATED_CPP_DIALECT

#include "io/octree.h"
#include "io/octree.hh"
#include "contrib/MemorySpace/memory_spaces.h"
#include "core/vector.h"
#include "core/affineTransform.h"
#include <iostream>
#include "core/config.h"
#include "contrib/domainDecomposition/subDomainInformation.h"
#include "core/util.h"

#include "thrust/gather.h"
#include "thrust/iterator/counting_iterator.h"
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/functional.h>
#include <thrust/copy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sequence.h>
#include <chrono>

template <typename T, template <typename> class Lattice>
struct CoordinateTransformIterator : public thrust::unary_function<size_t, size_t> {

    CoordinateTransformIterator(SubDomainInformation<T,Lattice<T>> localSubDomain, size_t localGlobalOrigin[3], size_t localRegionSize[3]) :
        _localSubDomain(localSubDomain) {
        
        for (int iD = 0; iD < 3; iD++) {
            _localGlobalOrigin[iD] = localGlobalOrigin[iD];
            _localRegionSize[iD] = localRegionSize[iD];
        }
    }

    OPENLB_HOST_DEVICE
    size_t operator()(size_t input) {
        size_t indices[3];
        util::getCellIndices3D(input, _localRegionSize[1], _localRegionSize[2], indices);

        for (int iD = 0; iD < 3; iD++)
            indices[iD] += _localGlobalOrigin[iD];

        Index3D localIndex(0,0,0,_localSubDomain.localSubDomain);
        _localSubDomain.isLocal(indices[0], indices[1], indices[2], localIndex);

        return util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], _localSubDomain.localGridSize()[1], _localSubDomain.localGridSize()[2]);
    }

    size_t _localGlobalOrigin[3];
    size_t _localRegionSize[3];
    SubDomainInformation<T,Lattice<T>> _localSubDomain;

};

template <typename T>
OPENLB_HOST_DEVICE
Vec3<T> intersectRayNode(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords, size_t octreeID, 
                         T baseRadius, const size_t * const OPENLB_RESTRICT depth,
                         Vec3<T> pt, Vec3<T> dir) {
    T t = T();
    T d = T();
    Vec3<T> s = Vec3<T>((T)0.0,(T)0.0,(T)0.0);
    T _radius = baseRadius * pow((T)0.5, (T)depth[octreeID]);

    if (dir(0) > (T)0.0) {
        d = centerCoords[0][octreeID] + _radius;
        t = (d-pt(0))/dir(0);
        s = dir*t;
        s += pt;
        if (fabs(s(1)-centerCoords[1][octreeID]) < _radius && fabs(s(2)-centerCoords[2][octreeID]) < _radius)
            return s;
    }
    else if (dir(0) < (T)0.0) {
        d = centerCoords[0][octreeID] - _radius;
        t = (d-pt(0))/dir(0);
        s = dir*t;
        s += pt;
        if (fabs(s(1)-centerCoords[1][octreeID]) < _radius && fabs(s(2)-centerCoords[2][octreeID]) < _radius)
            return s;
    }

    if (dir(1) > (T)0.0) {
        d = centerCoords[1][octreeID] + _radius;
        t = (d-pt(1))/dir(1);
        s = dir*t;
        s += pt;
        if (fabs(s(0)-centerCoords[0][octreeID]) < _radius && fabs(s(2)-centerCoords[2][octreeID]) < _radius)
            return s;
    }
    else if (dir(1) < (T)0.0) {
        d = centerCoords[1][octreeID] - _radius;
        t = (d-pt(1))/dir(1);
        s = dir*t;
        s += pt;
        if (fabs(s(0)-centerCoords[0][octreeID]) < _radius && fabs(s(2)-centerCoords[2][octreeID]) < _radius)
            return s;
    }

    if (dir(2) > (T)0.0) {
        d = centerCoords[2][octreeID] + _radius;
        t = (d-pt(2))/dir(2);
        s = dir*t;
        s += pt;
        if (fabs(s(0)-centerCoords[0][octreeID]) < _radius && fabs(s(1)-centerCoords[1][octreeID]) < _radius)
            return s;
    }
    else if (dir(2) < (T)0.0) {
        d = centerCoords[2][octreeID] - _radius;
        t = (d-pt(2))/dir(2);
        s = dir*t;
        s += pt;
        if (fabs(s(0)-centerCoords[0][octreeID]) < _radius && fabs(s(1)-centerCoords[1][octreeID]) < _radius)
            return s;
    }

    return s;
}

template <typename T>
OPENLB_HOST_DEVICE
bool findOctree(Vec3<T> point,
                const T baseRadius, 
                const size_t * const OPENLB_RESTRICT * const OPENLB_RESTRICT childIndices,
                const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords, 
                const char * const OPENLB_RESTRICT isLeaf, size_t& octreeID) {

    if (centerCoords[0][0] - baseRadius < point(0) && centerCoords[0][0] + baseRadius > point(0) &&
        centerCoords[1][0] - baseRadius < point(1) && centerCoords[1][0] + baseRadius > point(1) &&
        centerCoords[2][0] - baseRadius < point(2) && centerCoords[2][0] + baseRadius > point(2)) {
        //We are in the parent octree

        octreeID = 0; //start in root octree
        while (!isLeaf[octreeID]) {
            bool leftBranch = point(0) < centerCoords[0][octreeID];
            bool bottomBranch = point(1) < centerCoords[1][octreeID];
            bool frontBranch = point(2) < centerCoords[2][octreeID];

            if (leftBranch && bottomBranch && frontBranch)
                octreeID = childIndices[2][octreeID];
            else if (leftBranch && bottomBranch && !frontBranch)
                octreeID = childIndices[0][octreeID];
            else if (leftBranch && !bottomBranch && frontBranch)
                octreeID = childIndices[6][octreeID];
            else if (leftBranch && !bottomBranch && !frontBranch)
                octreeID = childIndices[4][octreeID];
            else if (!leftBranch && bottomBranch && frontBranch)
                octreeID = childIndices[3][octreeID];
            else if (!leftBranch && bottomBranch && !frontBranch)
                octreeID = childIndices[1][octreeID];
            else if (!leftBranch && !bottomBranch && frontBranch)
                octreeID = childIndices[7][octreeID];
            else if (!leftBranch && !bottomBranch && !frontBranch)
                octreeID = childIndices[5][octreeID];
        }
        return true;
    }
    else
        return false;
}

template <typename T>
OPENLB_HOST_DEVICE
bool testRayIntersect(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT triangles,
                      const size_t triIndex, const Vec3<T> pt, const Vec3<T> dir, Vec3<T>& q, T& alpha, T rad = (T)0.0) {

    const T epsilon = 1E-15;
    T rn = (T)0;
    Vec3<T> normal(triangles[9][triIndex], triangles[10][triIndex], triangles[11][triIndex]);
    Vec3<T> testPt = (normal*rad);
    testPt += pt;

    for (int iD = 0; iD < 3; iD++) 
        rn += dir(iD) * normal(iD);

    if (fabs(rn) < epsilon)
        return false;

    alpha = triangles[18][triIndex] - testPt(0)*normal(0) - testPt(1)*normal(1) - testPt(2)*normal(2);
    alpha /= rn;

    if (alpha < -epsilon)
        return false;

    for (int i = 0; i < 3; i++)
        q(i) = testPt(i) + alpha * dir(i);
    
    double beta = triangles[19][triIndex]; //forcing double precision as per stlreader, not sure if necessary
    for (int i = 0; i < 3; i++)
        beta += triangles[12+i][triIndex] * q(i);

    if (beta < -epsilon)
        return false;

    double gamma = triangles[20][triIndex];
    for (int i = 0; i < 3; i++)
        gamma += triangles[15+i][triIndex] * q(i);
    
    if (gamma < -epsilon)
        return false;

    if ((T)1.0 - beta - gamma < -epsilon)
        return false;

    return true;
}

template <typename T>
OPENLB_HOST_DEVICE
bool closestIntersection(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords, size_t octreeID,
                         const unsigned int * const OPENLB_RESTRICT * const OPENLB_RESTRICT octreeTriangles,
                         const size_t * const OPENLB_RESTRICT octreeNumTriangles,
                         const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT triangles,                         
                         Vec3<T> pt, Vec3<T> dir, Vec3<T>& q, T& a, T rad = (T)0.0) {
    a = 1E15; //close to infinity
    T alpha = T();
    Vec3<T> qtmp((T)0.0,(T)0.0,(T)0.0);
    bool found = false;

    for (int k = 0; k < octreeNumTriangles[octreeID]; k++) {
        size_t triIndex = octreeTriangles[octreeID][k];
        
        if (testRayIntersect(triangles, triIndex, pt, dir, qtmp, alpha)) {

            if (alpha < a) {
                a = alpha;
                q = qtmp;
                found = true;
            }
        }
    }

    return found;
}

template <typename T>
OPENLB_HOST_DEVICE
bool calcDistance(Vec3<T> origin,
            Vec3<T> direction,
            const T baseRadius, 
            const size_t * const OPENLB_RESTRICT depth,
            const size_t * const OPENLB_RESTRICT * const OPENLB_RESTRICT childIndices,
            const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords,
            const unsigned int * const OPENLB_RESTRICT * const OPENLB_RESTRICT octreeTriangles,
            const size_t * const OPENLB_RESTRICT octreeNumTriangles,
            const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT triangles,
            const char * const OPENLB_RESTRICT isLeaf, T& distance) {

    const T step = 0.5/10000.0;
    const int maxTries = 10;

    T directionNorm = sqrt(direction(0)*direction(0) + direction(1)*direction(1) + direction(2)*direction(2));
    direction(0) /= directionNorm;
    direction(1) /= directionNorm;
    direction(2) /= directionNorm;

    Vec3<T> pt = origin + direction*step;
    Vec3<T> q((T)0.0,(T)0.0,(T)0.0);
    T a = (T)0.0;

    for (int i = 0; i < maxTries; i++) {
        size_t octreeID;
        bool success = findOctree(pt, baseRadius, childIndices, centerCoords, isLeaf, octreeID);
        if (!success)
            return false;

        if (closestIntersection(centerCoords, octreeID, octreeTriangles, octreeNumTriangles, triangles, origin, direction, q, a)) {
            Vec3<T> vec = q - origin;
            distance = sqrt(vec(0)*vec(0) + vec(1)*vec(1) + vec(2)*vec(2));
            return true;
        }
        else {
            Vec3<T> s = intersectRayNode(centerCoords, octreeID, baseRadius, depth, pt, direction);
            pt = s + direction*step;
        }
    }    

    return false;
}

template <typename T>
OPENLB_HOST_DEVICE
bool pointIsInside(Vec3<T> point, const T baseRadius, 
                    const size_t * const OPENLB_RESTRICT * const OPENLB_RESTRICT childIndices,
                    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords, 
                    const char * const OPENLB_RESTRICT isLeaf, 
                    const char * const OPENLB_RESTRICT isInside) {

    size_t octreeID;
    if (findOctree(point, baseRadius, childIndices, centerCoords, isLeaf, octreeID))
        return isInside[octreeID];
    else
        return false;
}

#ifdef __CUDACC__
template <typename T, template <typename> class Lattice>
__global__ void stlInsideKernel(SubDomainInformation<T,Lattice<T>> localSubDomain, size_t startX, size_t startY, size_t startZ, size_t nx, size_t ny, size_t nz, 
                                AffineTransform<T,3> transform, 
                                T baseRadius,
                                const size_t * const OPENLB_RESTRICT * const OPENLB_RESTRICT childIndices,
                                const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords,
                                const char * const OPENLB_RESTRICT isLeaf,
                                const char * const OPENLB_RESTRICT isInside,
                                char * const OPENLB_RESTRICT checkRegion,
                                char * const OPENLB_RESTRICT formerCheckRegion,
                                char * const OPENLB_RESTRICT formerBBPointsRegion) {
                                    
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= nx*ny*nz)
        return;

    size_t globalIndices[3];
    util::getCellIndices3D(threadIndex, ny, nz, globalIndices);
    globalIndices[0] += startX;
    globalIndices[1] += startY;
    globalIndices[2] += startZ;
    Vec3<T> point((T)globalIndices[0], (T)globalIndices[1], (T)globalIndices[2]);
    transformInPlace<T>(point, transform);
    
    bool inside = pointIsInside(point, baseRadius, childIndices, centerCoords,
                                    isLeaf, isInside);
    checkRegion[threadIndex] = inside;

    // if (inside)
    //     printf("Inside!\n");

    Index3D localIndex;
    if (localSubDomain.isLocal(globalIndices[0], globalIndices[1], globalIndices[2], localIndex) && formerCheckRegion[threadIndex] && !inside)
        formerBBPointsRegion[threadIndex] = true;
}

template <typename T, template <typename> class Lattice>
__global__ void stlBoundaryKernel(SubDomainInformation<T,Lattice<T>> localSubDomain, size_t startX, size_t startY, size_t startZ, size_t nx, size_t ny, size_t nz, 
                                AffineTransform<T,3> transform, 
                                T baseRadius,
                                const size_t * const OPENLB_RESTRICT * const OPENLB_RESTRICT childIndices,
                                const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords,
                                const char * const OPENLB_RESTRICT isLeaf,
                                const char * const OPENLB_RESTRICT isInside,
                                char * const OPENLB_RESTRICT checkRegion) {
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= nx*ny*nz)
        return;

    size_t globalIndices[3];
    util::getCellIndices3D(threadIndex, ny, nz, globalIndices);
    globalIndices[0] += startX;
    globalIndices[1] += startY;
    globalIndices[2] += startZ;
    Vec3<T> originalPoint((T)globalIndices[0], (T)globalIndices[1], (T)globalIndices[2]);
    Vec3<T> point = originalPoint;
    transformInPlace<T>(point, transform);

    bool inside = pointIsInside(point, baseRadius, childIndices, centerCoords,
                                    isLeaf, isInside);
    
    if (!inside) {
        bool neighborInside = false;
        for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
            Vec3<T> neighborPoint = originalPoint;
            neighborPoint(0) += (T) Lattice<T>::c(iPop, 0);
            neighborPoint(1) += (T) Lattice<T>::c(iPop, 1);
            neighborPoint(2) += (T) Lattice<T>::c(iPop, 2);
            transformInPlace<T>(neighborPoint, transform);

            neighborInside = pointIsInside(neighborPoint, baseRadius, childIndices, centerCoords,
                                    isLeaf, isInside);
            if (neighborInside)
                break;
        }
        if (neighborInside)
            checkRegion[threadIndex] = true;
    }
    
}

template <typename T, template <typename> class Lattice>
__global__ void stlSharedMemoryBoundaryKernel(SubDomainInformation<T,Lattice<T>> localSubDomain, size_t startX, size_t startY, size_t startZ, size_t nx, size_t ny, size_t nz, 
                                              char * const OPENLB_RESTRICT insideCheckRegion,
                                              char * const OPENLB_RESTRICT boundaryCheckRegion, size_t numBlocksY, size_t numBlocksZ) {
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    // const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
    //                 + blockIndex * blockDim.x * blockDim.y * blockDim.z; // unused in this kernel
    const size_t threadIndexInBlock = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y;

    const size_t BLOCK_SIZE_X = 8;
    const size_t BLOCK_SIZE_Y = 4;
    const size_t BLOCK_SIZE_Z = 32;

    __shared__ bool insideShared[BLOCK_SIZE_X*BLOCK_SIZE_Y*BLOCK_SIZE_Z];

    size_t blockIndices[3];
    util::getCellIndices3D(blockIndex, numBlocksY, numBlocksZ, blockIndices);

    size_t blockOrigin[3] = {blockIndices[0]*BLOCK_SIZE_X, blockIndices[1]*BLOCK_SIZE_Y, blockIndices[2]*BLOCK_SIZE_Z};

    size_t checkRegionIndices[3];
    util::getCellIndices3D(threadIndexInBlock, BLOCK_SIZE_Y, BLOCK_SIZE_Z, checkRegionIndices);

    for (int iD = 0; iD < 3; iD++)        
        checkRegionIndices[iD] += blockOrigin[iD];

    if (checkRegionIndices[0] < nx && checkRegionIndices[1] < ny && checkRegionIndices[2] < nz) {
        insideShared[threadIndexInBlock] = insideCheckRegion[util::getCellIndex3D(checkRegionIndices[0], checkRegionIndices[1], checkRegionIndices[2], ny, nz)];
    }
    else
        insideShared[threadIndexInBlock] = false;

    __syncthreads();

    Index3D localIndex;
    if (localSubDomain.isLocal(checkRegionIndices[0] + startX, checkRegionIndices[1] + startY, checkRegionIndices[2] + startZ, localIndex) && !insideShared[threadIndexInBlock]) {
        bool neighborInside = false;
        for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
            long neighborIndices[3] = {(long)checkRegionIndices[0] + (long)Lattice<T>::c(iPop, 0), (long)checkRegionIndices[1] + (long)Lattice<T>::c(iPop, 1), (long)checkRegionIndices[2] + (long)Lattice<T>::c(iPop, 2)};
            
            if (neighborIndices[0] < 0 || neighborIndices[0] >= nx || neighborIndices[1] < 0 || neighborIndices[1] >= ny || neighborIndices[2] < 0 || neighborIndices[2] >= nz)
                continue;

            if (neighborIndices[0] >= blockOrigin[0] && neighborIndices[0] < blockOrigin[0] + BLOCK_SIZE_X && 
                neighborIndices[1] >= blockOrigin[1] && neighborIndices[1] < blockOrigin[1] + BLOCK_SIZE_Y && 
                neighborIndices[2] >= blockOrigin[2] && neighborIndices[2] < blockOrigin[2] + BLOCK_SIZE_Z) {

                neighborInside = insideShared[util::getCellIndex3D(neighborIndices[0]-blockOrigin[0], neighborIndices[1]-blockOrigin[1], neighborIndices[2]-blockOrigin[2], BLOCK_SIZE_Y, BLOCK_SIZE_Z)];
            }
            else 
                neighborInside = insideCheckRegion[util::getCellIndex3D(neighborIndices[0], neighborIndices[1], neighborIndices[2], ny, nz)];

            if (neighborInside)
                break;            
        }

        if (neighborInside) {
            boundaryCheckRegion[util::getCellIndex3D(checkRegionIndices[0], checkRegionIndices[1], checkRegionIndices[2], ny, nz)] = true;
        }
    }
  
}

template <typename T, template <typename> class Lattice>
__global__ void stlBoundaryDistanceKernel(SubDomainInformation<T,Lattice<T>> localSubDomain, size_t startX, size_t startY, size_t startZ, size_t nx, size_t ny, size_t nz, 
                                            AffineTransform<T,3> transform, 
                                            Vec3<T> bodyVelocity,
                                            Vec3<T> bodyAngularVelocity,
                                            T baseRadius,
                                            const size_t * const OPENLB_RESTRICT depth,
                                            const size_t * const OPENLB_RESTRICT * const OPENLB_RESTRICT childIndices,
                                            const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT centerCoords,
                                            const char * const OPENLB_RESTRICT isLeaf,
                                            const char * const OPENLB_RESTRICT isInside,
                                            const unsigned int * const OPENLB_RESTRICT * const OPENLB_RESTRICT octreeTriangles,
                                            const size_t * const OPENLB_RESTRICT octreeNumTriangles,
                                            const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT triangles,
                                            size_t * boundaryPoints,
                                            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData,
                                            T omega,
                                            size_t length) {
    const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= length)
        return;

    size_t localCellIndex = boundaryPoints[threadIndex];

    size_t localIndices[3];
    util::getCellIndices3D(localCellIndex, localSubDomain.localGridSize()[1], localSubDomain.localGridSize()[2], localIndices);
    Index3D localIndex(localIndices[0], localIndices[1], localIndices[2], localSubDomain.localSubDomain);
    Index3D globalIndex = localSubDomain.getGlobalIndex(localIndex);
    Vec3<T> originalPoint((T)globalIndex[0], (T)globalIndex[1], (T)globalIndex[2]);
    Vec3<T> point = originalPoint;
    transformInPlace<T>(point, transform);

    AffineTransform<T,3> invTransform;
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            invTransform.m(i,j) = transform.m(j,i); //transpose

    Vec3<T> invTranslate(-transform.m(0,3), -transform.m(1,3), -transform.m(2,3));
    invTransform.applyToRight(invTranslate);

    Vec3<T> origin((T)0.0,(T)0.0,(T)0.0);
    transformInPlace<T>(origin, invTransform);

    Vec3<T> rotationArm((T)0.0,(T)0.0,(T)0.0);;
    for (int iD = 0; iD < 3; iD++)
        rotationArm(iD) = originalPoint(iD) - origin(iD);

    Vec3<T> angularVelocityComponent(bodyAngularVelocity(1)*rotationArm(2)-bodyAngularVelocity(2)*rotationArm(1), -bodyAngularVelocity(0)*rotationArm(2)+bodyAngularVelocity(2)*rotationArm(0), bodyAngularVelocity(0)*rotationArm(1)-bodyAngularVelocity(1)*rotationArm(0));
    
    postProcData[0][threadIndex] = omega; //omega position
    postProcData[1][threadIndex] = (T)0.0; //following four values are surface normal positions, which aren't used for regular boundary
    postProcData[2][threadIndex] = (T)0.0;
    postProcData[3][threadIndex] = (T)0.0;
    postProcData[4][threadIndex] = (T)0.0;
    
    postProcData[8][threadIndex] = bodyVelocity(0) + angularVelocityComponent(0); //following three values are wall velocity
    postProcData[9][threadIndex] = bodyVelocity(1) + angularVelocityComponent(1);
    postProcData[10][threadIndex] = bodyVelocity(2) + angularVelocityComponent(2);

    postProcData[11][threadIndex] = (T)-1.0; //zero population index
    
    bool xNegInside = false, xPosInside = false, yNegInside = false, yPosInside = false, zNegInside = false, zPosInside = false;
    for (int iPop = 1; iPop < Lattice<T>::q; iPop++) {
        Vec3<T> neighborPoint = originalPoint;
        neighborPoint(0) += (T) Lattice<T>::c(iPop, 0);
        neighborPoint(1) += (T) Lattice<T>::c(iPop, 1);
        neighborPoint(2) += (T) Lattice<T>::c(iPop, 2);
        transformInPlace<T>(neighborPoint, transform);

        bool neighborInside = pointIsInside(neighborPoint, baseRadius, childIndices, centerCoords,
                                isLeaf, isInside);

        if (neighborInside) {
            switch (iPop) { //reference latticeDescriptors.h for the directions, it's just hardcoded here
            case 1:
                xNegInside = true;
                break;
            case 10:
                xPosInside = true;
                break;
            case 2:
                yNegInside = true;
                break;
            case 11:
                yPosInside = true;
                break;
            case 3:
                zNegInside = true;
                break;
            case 12:
                zPosInside = true;
                break;
            default:
                break;
            }

            //calc distance
            T distance = 0.0;
            Vec3<T> direction = neighborPoint - point;

            bool success = calcDistance(point, direction, baseRadius, depth, childIndices, centerCoords, 
                                        octreeTriangles, octreeNumTriangles, triangles, isLeaf, distance);

            distance /= sqrt((T)Lattice<T>::c(iPop, 0)*(T)Lattice<T>::c(iPop, 0) + (T)Lattice<T>::c(iPop, 1)*(T)Lattice<T>::c(iPop, 1) + (T)Lattice<T>::c(iPop, 2)*(T)Lattice<T>::c(iPop, 2));

            if (!success || distance > 1.25 || distance < 0.0) { 
                //try inverse direction
                Vec3<T> invDirection = direction*(T)-1;
                success = calcDistance(point, invDirection, baseRadius, depth, childIndices, centerCoords, 
                                        octreeTriangles, octreeNumTriangles, triangles, isLeaf, distance);

                distance /= (T)-1.0*sqrt((T)Lattice<T>::c(iPop, 0)*(T)Lattice<T>::c(iPop, 0) + (T)Lattice<T>::c(iPop, 1)*(T)Lattice<T>::c(iPop, 1) + (T)Lattice<T>::c(iPop, 2)*(T)Lattice<T>::c(iPop, 2));

                if (!success || distance < -0.5 || distance > 0.0)
                    distance = (T)0.0; //default in case distance measurement fails
            }

            postProcData[11+Lattice<T>::opposite(iPop)][threadIndex] = distance;
        }
        else
            postProcData[11+Lattice<T>::opposite(iPop)][threadIndex] = (T)-1.0;
    }

    if (xNegInside && xPosInside) //bad!
      postProcData[5][threadIndex] = 0.0;
    else if (xNegInside)
      postProcData[5][threadIndex] = 1.0;
    else if (xPosInside)
      postProcData[5][threadIndex] = -1.0;
    else
      postProcData[5][threadIndex] = 1.0;

    if (yNegInside && yPosInside) //bad!
      postProcData[6][threadIndex] = 0.0;
    else if (yNegInside)
      postProcData[6][threadIndex] = 1.0;
    else if (yPosInside)
      postProcData[6][threadIndex] = -1.0;
    else
      postProcData[6][threadIndex] = 1.0;

    if (zNegInside && zPosInside) //bad!
      postProcData[7][threadIndex] = 0.0;
    else if (zNegInside)
      postProcData[7][threadIndex] = 1.0;
    else if (zPosInside)
      postProcData[7][threadIndex] = -1.0;
    else
      postProcData[7][threadIndex] = 1.0;
}
#endif

template <typename T, template <typename> class Lattice>
class OctreeGPU {

public:
    template <typename S>
    OctreeGPU(Octree<S>* octreeCPU, SubDomainInformation<T,Lattice<T>> localSubDomain, SubDomainInformation<T,Lattice<T>> refSubDomain, size_t globalOrigin[3], size_t globalExtend[3]) :
    _localSubDomain(localSubDomain),
    _refSubDomain(refSubDomain),
    numOctrees(octreeCPU->getCount()),
    maxDepth(octreeCPU->getMaxdepth()),
    baseRadius(octreeCPU->getRadius()),
    centerCoordsData(numOctrees*3),
    centerCoords(3),
    depth(numOctrees),
    isLeaf(numOctrees),
    boundaryNode(numOctrees),
    isInside(numOctrees),
    parentIndex(numOctrees),
    childIndicesData(numOctrees*8),
    childIndices(8),
    insideCheckRegion(0),
    formerInsideCheckRegion(0),
    formerBBPointsRegion(0),
    boundaryCheckRegion(0),
    regionLength(1),
    octreeTriangles(numOctrees),
    octreeNumTriangles(numOctrees),
    octreeTriangleIDs(numOctrees),
    numTriangles(octreeCPU->getMesh()->triangleSize()),
    trianglesData(numTriangles*21),
    triangles(21)
    {
        for (int iD = 0; iD < 3; iD++) {
            _fullGlobalOrigin[iD] = globalOrigin[iD];
            _fullGlobalExtend[iD] = globalExtend[iD];
        }

        Index3D localIndexStart;
        Index3D localIndexEnd;
        hasRegion = _localSubDomain.isRegionLocal(globalOrigin[0], globalOrigin[1], globalOrigin[2], globalExtend[0], globalExtend[1], globalExtend[2], localIndexStart, localIndexEnd);

        if (!hasRegion)
            return;

        Index3D globalIndexStart = _localSubDomain.getGlobalIndex(localIndexStart);
        long long globalIndexStartArray[3] = {globalIndexStart[0], globalIndexStart[1], globalIndexStart[2]};

        Index3D globalIndexEnd = _localSubDomain.getGlobalIndex(localIndexEnd);
        long long globalIndexEndArray[3] = {globalIndexEnd[0], globalIndexEnd[1], globalIndexEnd[2]};

        const long long OVERLAP_REGION = 2;

        for (int iD = 0; iD < 3; iD++) {
            globalIndexStartArray[iD] = max(globalIndexStartArray[iD]-OVERLAP_REGION, (long long) _fullGlobalOrigin[iD]);
            globalIndexEndArray[iD] = min(globalIndexEndArray[iD]+OVERLAP_REGION, (long long) _fullGlobalExtend[iD]);
        }

        for (int iD = 0; iD < 3; iD++) {
            _localGlobalOrigin[iD] = (size_t)globalIndexStartArray[iD];
            _localRegionSize[iD] = (size_t)globalIndexEndArray[iD]-(size_t)globalIndexStartArray[iD]+1;
            regionLength *= _localRegionSize[iD];
        }
        
        insideCheckRegion.resize(regionLength);
        formerInsideCheckRegion.resize(regionLength);
        formerBBPointsRegion.resize(regionLength);
        boundaryCheckRegion.resize(regionLength);
        
        for (int iD = 0; iD < 3; iD++)
            centerCoords[iD] = centerCoordsData.get() + numOctrees*iD;
        
        for (int iC = 0; iC < 8; iC++)
            childIndices[iC] = childIndicesData.get() + numOctrees*iC;

        std::vector<T> cpuCenterCoordsData(numOctrees*3);
        std::vector<size_t> cpuDepth(numOctrees);
        std::vector<char> cpuIsLeaf(numOctrees);
        std::vector<char> cpuBoundaryNode(numOctrees);
        std::vector<char> cpuIsInside(numOctrees);
        std::vector<size_t> cpuParentIndex(numOctrees);
        std::vector<size_t> cpuChildIndicesData(numOctrees*8);
        std::vector<size_t> cpuOctreeNumTriangles(numOctrees);
        std::vector<unsigned int> cpuOctreeTriangleIDs;

        collectOctreeData(octreeCPU, cpuCenterCoordsData, cpuDepth, cpuIsLeaf, cpuBoundaryNode, cpuIsInside, cpuParentIndex, cpuChildIndicesData, cpuOctreeNumTriangles, cpuOctreeTriangleIDs, maxDepth);

        // cuda memcpy
        #ifdef __CUDACC__
        cudaMemcpy(centerCoordsData.get(), &cpuCenterCoordsData[0], sizeof(T)*cpuCenterCoordsData.size(), cudaMemcpyDefault);
        cudaMemcpy(depth.get(), &cpuDepth[0], sizeof(size_t)*cpuDepth.size(), cudaMemcpyDefault);
        cudaMemcpy(isLeaf.get(), &cpuIsLeaf[0], sizeof(char)*cpuIsLeaf.size(), cudaMemcpyDefault);
        cudaMemcpy(boundaryNode.get(), &cpuBoundaryNode[0], sizeof(char)*cpuBoundaryNode.size(), cudaMemcpyDefault);
        cudaMemcpy(isInside.get(), &cpuIsInside[0], sizeof(char)*cpuIsInside.size(), cudaMemcpyDefault);
        cudaMemcpy(parentIndex.get(), &cpuParentIndex[0], sizeof(size_t)*cpuParentIndex.size(), cudaMemcpyDefault);
        cudaMemcpy(childIndicesData.get(), &cpuChildIndicesData[0], sizeof(size_t)*cpuChildIndicesData.size(), cudaMemcpyDefault);
        cudaMemset(insideCheckRegion.get(), 0, sizeof(char)*regionLength);
        cudaMemset(formerInsideCheckRegion.get(), 0, sizeof(char)*regionLength);
        cudaMemset(formerBBPointsRegion.get(), 0, sizeof(char)*regionLength);
        cudaMemset(boundaryCheckRegion.get(), 0, sizeof(char)*regionLength);

        cudaMemcpy(octreeNumTriangles.get(), &cpuOctreeNumTriangles[0], sizeof(size_t)*cpuOctreeNumTriangles.size(), cudaMemcpyDefault);
       
        octreeTriangleIDs.resize(cpuOctreeTriangleIDs.size());
        cudaMemcpy(octreeTriangleIDs.get(), &cpuOctreeTriangleIDs[0], sizeof(unsigned int)*cpuOctreeTriangleIDs.size(), cudaMemcpyDefault);
        
        size_t pointerIncrement = 0;
        for (int i = 0; i < numOctrees; i++) {
            octreeTriangles[i] = octreeTriangleIDs.get() + pointerIncrement;
            pointerIncrement += cpuOctreeNumTriangles.at(i);
        }
        #endif

        std::vector<T> cpuTrianglesData(numTriangles*21);
        for (int i = 0; i < numTriangles; i++) {
            STLtriangle<S>  curTri = octreeCPU->getMesh()->getTri(i);

            cpuTrianglesData[numTriangles*0+i] = (T)curTri.point[0].r[0];
            cpuTrianglesData[numTriangles*1+i] = (T)curTri.point[0].r[1];
            cpuTrianglesData[numTriangles*2+i] = (T)curTri.point[0].r[2];

            cpuTrianglesData[numTriangles*3+i] = (T)curTri.point[1].r[0];
            cpuTrianglesData[numTriangles*4+i] = (T)curTri.point[1].r[1];
            cpuTrianglesData[numTriangles*5+i] = (T)curTri.point[1].r[2];

            cpuTrianglesData[numTriangles*6+i] = (T)curTri.point[2].r[0];
            cpuTrianglesData[numTriangles*7+i] = (T)curTri.point[2].r[1];
            cpuTrianglesData[numTriangles*8+i] = (T)curTri.point[2].r[2];

            cpuTrianglesData[numTriangles*9+i] = (T)curTri.normal[0];
            cpuTrianglesData[numTriangles*10+i] = (T)curTri.normal[1];
            cpuTrianglesData[numTriangles*11+i] = (T)curTri.normal[2];

            cpuTrianglesData[numTriangles*12+i] = curTri.uBeta[0];
            cpuTrianglesData[numTriangles*13+i] = curTri.uBeta[1];
            cpuTrianglesData[numTriangles*14+i] = curTri.uBeta[2];

            cpuTrianglesData[numTriangles*15+i] = curTri.uGamma[0];
            cpuTrianglesData[numTriangles*16+i] = curTri.uGamma[1];
            cpuTrianglesData[numTriangles*17+i] = curTri.uGamma[2];

            cpuTrianglesData[numTriangles*18+i] = curTri.d;
            cpuTrianglesData[numTriangles*19+i] = curTri.kBeta;
            cpuTrianglesData[numTriangles*20+i] = curTri.kGamma;
        }

        for (int i = 0; i < 21; i++) {
            triangles[i] = trianglesData.get() + numTriangles*i;
        } 

        //cuda memcpy triangles
        #ifdef __CUDACC__
        cudaMemcpy(trianglesData.get(), &cpuTrianglesData[0], sizeof(T)*cpuTrianglesData.size(), cudaMemcpyDefault);
        #endif
    }

    size_t stlInside(AffineTransform<T,3> transform, size_t * formerBBPoints) {
        if (!hasRegion)
            return 0;

        #ifdef __CUDACC__
        
        auto formerBBPointsDevicePointer  = thrust::device_pointer_cast(formerBBPoints);
        auto formerBBPointsRegionDevicePointer = thrust::device_pointer_cast(formerBBPointsRegion.get());

        stlInsideKernel<T,Lattice><<<regionLength/256+1, 256>>>(_localSubDomain, 
                                    _localGlobalOrigin[0], _localGlobalOrigin[1], _localGlobalOrigin[2],
                                    _localRegionSize[0], _localRegionSize[1], _localRegionSize[2],
                                    transform, baseRadius, &childIndices[0], &centerCoords[0], isLeaf.get(), isInside.get(), insideCheckRegion.get(), formerInsideCheckRegion.get(), formerBBPointsRegion.get());

        cudaDeviceSynchronize();
        CudaHandleError(cudaPeekAtLastError(), "octree", 814);

        auto it = thrust::copy_if(thrust::make_transform_iterator(thrust::make_counting_iterator<size_t>(0), CoordinateTransformIterator<T,Lattice>(_localSubDomain, _localGlobalOrigin, _localRegionSize)), 
                        thrust::make_transform_iterator(thrust::make_counting_iterator<size_t>(regionLength), CoordinateTransformIterator<T,Lattice>(_localSubDomain, _localGlobalOrigin, _localRegionSize)), 
                        formerBBPointsRegionDevicePointer, formerBBPointsDevicePointer, thrust::placeholders::_1);
        cudaDeviceSynchronize();

        size_t numFormerBBPoints = it - formerBBPointsDevicePointer;
        
        cudaMemset(formerBBPointsRegion.get(), 0, sizeof(char)*regionLength);

        return numFormerBBPoints;
        #endif       
    }

    size_t stlBoundary(AffineTransform<T,3> transform, size_t * deviceBoundaryIndices, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData, T omega,
                        Vec3<T> bodyVelocity, Vec3<T> bodyAngularVelocity) {
        if (!hasRegion)
            return 0;

        auto indicesDevicePointer = thrust::device_pointer_cast(deviceBoundaryIndices);
        auto checkRegionDevicePointer = thrust::device_pointer_cast(boundaryCheckRegion.get());
        
        size_t numBlocksX = _localRegionSize[0]/8 + 1;
        size_t numBlocksY = _localRegionSize[1]/4 + 1;
        size_t numBlocksZ = _localRegionSize[2]/32 + 1;
        
        #ifdef __CUDACC__
        stlSharedMemoryBoundaryKernel<T,Lattice><<<numBlocksX*numBlocksY*numBlocksZ, 8*4*32>>>(_localSubDomain, _localGlobalOrigin[0], _localGlobalOrigin[1], _localGlobalOrigin[2],
                                                                       _localRegionSize[0], _localRegionSize[1], _localRegionSize[2],
                                                                       insideCheckRegion.get(), boundaryCheckRegion.get(), numBlocksY, numBlocksZ);
        #endif
        cudaDeviceSynchronize();
        CudaHandleError(cudaPeekAtLastError(), "octree", 846);

        auto it = thrust::copy_if(thrust::make_transform_iterator(thrust::make_counting_iterator<size_t>(0), CoordinateTransformIterator<T,Lattice>(_localSubDomain, _localGlobalOrigin, _localRegionSize)), 
                                  thrust::make_transform_iterator(thrust::make_counting_iterator<size_t>(regionLength), CoordinateTransformIterator<T,Lattice>(_localSubDomain, _localGlobalOrigin, _localRegionSize)), 
                                  checkRegionDevicePointer, indicesDevicePointer, thrust::placeholders::_1);
        cudaDeviceSynchronize();
        
        size_t numBoundaryCells = it-indicesDevicePointer;
        cudaMemset(boundaryCheckRegion.get(), 0, sizeof(char)*regionLength);
        
        #ifdef __CUDACC__
        if (numBoundaryCells) {
            stlBoundaryDistanceKernel<T,Lattice><<<numBoundaryCells/256+1, 256>>>(_localSubDomain, _localGlobalOrigin[0], _localGlobalOrigin[1], _localGlobalOrigin[2], _localRegionSize[0], _localRegionSize[1], _localRegionSize[2],
                                        transform, bodyVelocity, bodyAngularVelocity, baseRadius, depth.get(), &childIndices[0], &centerCoords[0], isLeaf.get(), isInside.get(), &octreeTriangles[0], octreeNumTriangles.get(), &triangles[0],
                                        deviceBoundaryIndices, postProcData, omega, numBoundaryCells);
            cudaDeviceSynchronize();
            CudaHandleError(cudaPeekAtLastError(), "octree", 861);
        }
        #endif

        std::swap(insideCheckRegion, formerInsideCheckRegion);

        return numBoundaryCells;
    }

private:

    template <typename S>
    void collectOctreeData(Octree<S>* octreeCPU, std::vector<T>& cpuCenterCoordsData, std::vector<size_t>& cpuDepth, std::vector<char>& cpuIsLeaf, 
                            std::vector<char>& cpuBoundaryNode, std::vector<char>& cpuIsInside, std::vector<size_t>& cpuParentIndex, std::vector<size_t>& cpuChildIndices, 
                            std::vector<size_t>& cpuOctreeNumTriangles, std::vector<unsigned int>& cpuOctreeTriangleIDs, 
                            size_t parentMaxDepth) {

        Vector<S,3> center = octreeCPU->getCenter();
        size_t octreeID = octreeCPU->getID();

        for (int iD = 0; iD < 3; iD++)
            cpuCenterCoordsData.at(numOctrees*iD + octreeID) = (T) center[iD];

        cpuDepth.at(octreeID) = parentMaxDepth - octreeCPU->getMaxdepth();
        cpuIsLeaf.at(octreeID) = octreeCPU->isLeaf();

        cpuBoundaryNode.at(octreeID) = octreeCPU->getBoundaryNode();
        cpuIsInside.at(octreeID) = octreeCPU->getInside();

        if (octreeCPU->getParent())
            cpuParentIndex.at(octreeID) = octreeCPU->getParent()->getID();
        else 
            cpuParentIndex.at(octreeID) = 0;

        if (!octreeCPU->isLeaf())
            for (int iC = 0; iC < 8; iC++) {
                cpuChildIndices.at(numOctrees*iC + octreeID) = octreeCPU->getChild(iC)->getID();
            }
        else
            for (int iC = 0; iC < 8; iC++) {
                cpuChildIndices.at(numOctrees*iC + octreeID) = 0;
            }

        cpuOctreeNumTriangles.at(octreeID) = octreeCPU->getTriangles().size();
        cpuOctreeTriangleIDs.insert(cpuOctreeTriangleIDs.end(), octreeCPU->getTriangles().begin(), octreeCPU->getTriangles().end());

        //Recursion
        if (!octreeCPU->isLeaf()) {
            for (int iC = 0; iC < 8; iC++)
                collectOctreeData(octreeCPU->getChild(iC), cpuCenterCoordsData, cpuDepth, cpuIsLeaf, cpuBoundaryNode, cpuIsInside, cpuParentIndex, cpuChildIndices, 
                                        cpuOctreeNumTriangles, cpuOctreeTriangleIDs, parentMaxDepth);
        }
    }

private:
    bool hasRegion = true;
    size_t _fullGlobalOrigin[Lattice<T>::d];
    size_t _fullGlobalExtend[Lattice<T>::d];
    SubDomainInformation<T,Lattice<T>> _localSubDomain;
    SubDomainInformation<T,Lattice<T>> _refSubDomain;
    size_t _localGlobalOrigin[Lattice<T>::d];
    size_t _localRegionSize[Lattice<T>::d];
    size_t regionLength;
    size_t numOctrees;
    size_t maxDepth;
    T baseRadius;
    memory_space::CudaDeviceHeap<T> centerCoordsData;
    memory_space::CudaUnified<T*> centerCoords;
    memory_space::CudaDeviceHeap<size_t> depth;
    memory_space::CudaDeviceHeap<char> isLeaf;
    memory_space::CudaDeviceHeap<char> boundaryNode;
    memory_space::CudaDeviceHeap<char> isInside;
    memory_space::CudaDeviceHeap<size_t> parentIndex;
    memory_space::CudaDeviceHeap<size_t> childIndicesData;
    memory_space::CudaUnified<size_t*> childIndices;
    memory_space::CudaUnified<unsigned int*> octreeTriangles;
    memory_space::CudaDeviceHeap<size_t> octreeNumTriangles;
    memory_space::CudaDeviceHeap<unsigned int> octreeTriangleIDs;

    size_t numTriangles;
    memory_space::CudaDeviceHeap<T> trianglesData;
    memory_space::CudaUnified<T*> triangles;
    
    memory_space::CudaDeviceHeap<char> insideCheckRegion;
    memory_space::CudaDeviceHeap<char> formerInsideCheckRegion;
    memory_space::CudaDeviceHeap<char> formerBBPointsRegion;
    memory_space::CudaDeviceHeap<char> boundaryCheckRegion;
};

#endif