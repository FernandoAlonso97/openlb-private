#ifndef MEMORY_SPACE_H
#define MEMORY_SPACE_H

#ifdef __CUDACC__
#define HOST_EXECUTION_SPACE __host__
#define DEVICE_EXECUTION_SPACE __device__
#define ALL_EXECUTION_SPACES __host__ __device__
#else
#define HOST_EXECUTION_SPACE
#define DEVICE_EXECUTION_SPACE
#define ALL_EXECUTION_SPACES 
#endif


#include <memory>

namespace memory_space{

struct HostSpaceTag{};

struct CudaDeviceSpaceTag{};


/////////////////////// Host
template<typename T>
class HostHeap : public HostSpaceTag {

  public:

    using ReturnType = T&;
    using ConstReturnType = T const &;

    HostHeap() = default;

    HOST_EXECUTION_SPACE
    explicit HostHeap(std::size_t size):size_{size}
    {
      resize(size);
    } 

    HostHeap(HostHeap const &) = default;

    HostHeap& operator= (HostHeap const &) = default;

    HostHeap(HostHeap&&) = default;

    HostHeap& operator= (HostHeap&&) = default;

    void resize(std::size_t size)
    {
      //assert(size>0 && "no construction with size 0 allowed");
      if (size == 0)
        data_ = std::shared_ptr<T>();

      else
        data_= std::shared_ptr<T>(new T[size], std::default_delete<T[]>());
      size_ = size;
    }

    HOST_EXECUTION_SPACE
    ReturnType operator[] (std::size_t index)
    {
      // assert(index<size_ && "index to big write");
      return data_.get()[index];
    }

    HOST_EXECUTION_SPACE
    ConstReturnType operator[] (std::size_t index) const
    {
      // assert(index<size_ && "index to big read ");
      return data_.get()[index];
    }
  
    HOST_EXECUTION_SPACE
    constexpr std::size_t size () const
    {
      return size_;
    }

    HOST_EXECUTION_SPACE
    explicit operator bool() const noexcept
    {
      return static_cast<bool>(data_);
    }
      

    static constexpr bool isDeviceAccessable()
    {
      return false;
    }

    static constexpr bool isHostAccessable()
    {
      return true;
    }

    T* get() const
    {
      return data_.get();
    }

  private:

    std::shared_ptr<T> data_ = nullptr;
    std::size_t size_ = {0};
};

template<typename T>
std::ostream& operator<< (std::ostream& stream, const HostHeap<T>& mem)
{
  if (&mem[0] != nullptr)
    stream << "Valid HostHeap with size " << mem.size() << " at ptr_addr " <<mem.get() << "\n";
  else
    stream << "INVALID HostHeap with size " << mem.size() << "\n";

  return stream;
}

#ifdef ENABLE_CUDA
template<typename T>
T* cMallocHost(std::size_t count)
{
  T* tmp {nullptr};
  cudaMallocHost(&tmp,sizeof(T)*count);
  return tmp; 
}

template<typename T>
T* cMalloc(std::size_t count)
{
  T* tmp {nullptr};
  cudaMalloc(&tmp,sizeof(T)*count);
  return tmp;
}

template<typename T>
T* cMallocManaged(std::size_t count)
{
  T* tmp {nullptr};
  cudaMallocManaged(&tmp,sizeof(T)*count);
  return tmp;
}

template<typename T>
void cFree( T* ptr )
{
   cudaFree(ptr);
}

template<typename T>
class CudaHostPageLocked : public HostSpaceTag {

  public:

    using ReturnType = T&;
    using ConstReturnType = T const &;

    CudaHostPageLocked() = default;

    HOST_EXECUTION_SPACE
    explicit CudaHostPageLocked(std::size_t size)
    {
      resize(size);
    }

//    HOST_EXECUTION_SPACE
    CudaHostPageLocked(CudaHostPageLocked const &) = default;

//    HOST_EXECUTION_SPACE
    CudaHostPageLocked& operator= (CudaHostPageLocked const &) = default;

    HOST_EXECUTION_SPACE
    ~CudaHostPageLocked()
    {}

    void resize(std::size_t size)
    {
      if (size==0)
        data_ = std::shared_ptr<T>();
      else
        data_ = std::shared_ptr<T>(cMallocHost<T>(size),cFree<T>);
      size_= size; 
    }

    HOST_EXECUTION_SPACE
    ReturnType operator[] (std::size_t index)
    {
      return data_.get()[index];
    }

    HOST_EXECUTION_SPACE
    ConstReturnType operator[] (std::size_t index) const
    {
      return data_.get()[index];
    }

    HOST_EXECUTION_SPACE
    explicit operator bool() const noexcept
    {
      return static_cast<bool>(data_);
    }
  
    HOST_EXECUTION_SPACE
    constexpr std::size_t size () const
    {
      return size_;
    }

    static constexpr bool isDeviceAccessable()
    {
      return false;
    }

    static constexpr bool isHostAccessable()
    {
      return true;
    }

  private:

    std::shared_ptr<T> data_ = {nullptr};
    std::size_t size_{0};

};


////////////////////// CudaDevice
template<typename T>
class CudaDeviceHeap : public CudaDeviceSpaceTag {

  public:

    using ReturnType = T&;
    using ConstReturnType = T const &;

    CudaDeviceHeap() = default;

    HOST_EXECUTION_SPACE
    explicit CudaDeviceHeap(std::size_t size)
    {
      resize(size);
    }

//    HOST_EXECUTION_SPACE
    CudaDeviceHeap(CudaDeviceHeap const &) = default;

//    HOST_EXECUTION_SPACE
    CudaDeviceHeap& operator= (CudaDeviceHeap const &) = default;

    HOST_EXECUTION_SPACE
    ~CudaDeviceHeap() {}

    void resize(std::size_t size)
    {
      if (size==0)
      {
        data_ = std::shared_ptr<T>();
        dataDevice_ = data_.get();
      }
      
      else
      {  
        data_ = std::shared_ptr<T>(cMalloc<T>(size),cFree<T>);
        dataDevice_ = data_.get();
      }
      size_= size; 
    }

    DEVICE_EXECUTION_SPACE
    ReturnType operator[] (std::size_t index)
    {
      return dataDevice_[index];
    }

    DEVICE_EXECUTION_SPACE
    ConstReturnType operator[] (std::size_t index) const
    {
      return dataDevice_[index];
    }
  
    DEVICE_EXECUTION_SPACE 
    constexpr std::size_t size () const
    {
      return size_;
    }

    HOST_EXECUTION_SPACE
    explicit operator bool() const noexcept
    {
      return static_cast<bool>(data_);
    }

    static constexpr bool isDeviceAccessable()
    {
      return true;
    }

    static constexpr bool isHostAccessable()
    {
      return false;
    }

    ALL_EXECUTION_SPACES
    T* get() const 
    {
      return dataDevice_;
    }

    void set(T* dest)
    {
      dataDevice_ = dest;
      // data_.reset(dest);
    } 

    cudaIpcMemHandle_t* getMemHandle()
    {
      return &memHandle;
    }
    
    void** getDevPtrLoc()
    {
      return (void**) &dataDevice_;
    }
  private:

    std::shared_ptr<T> data_       = {nullptr};
    T*                 dataDevice_ = {nullptr};
    std::size_t size_{0};
    cudaIpcMemHandle_t memHandle;

};

template<typename T>
std::ostream& operator<< (std::ostream& stream, const CudaDeviceHeap<T>& mem)
{
  if (mem)
    stream << "Valid CudaDeviceHeap with size " << mem.size() << "\n";
  else
    stream << "INVALID CudaDeviceHeap with size " << mem.size() << "\n";

  return stream;
}

template<typename T>
class CudaUnified : public CudaDeviceSpaceTag, public HostSpaceTag {

  public:

    using ReturnType = T&;
    using ConstReturnType = T const &;

    CudaUnified() = default;

    HOST_EXECUTION_SPACE
    explicit CudaUnified(std::size_t size)
    {
      resize(size);
    }

//    HOST_EXECUTION_SPACE
    CudaUnified(CudaUnified const &) = default;

//    HOST_EXECUTION_SPACE
    CudaUnified& operator= (CudaUnified const &) = default;

    HOST_EXECUTION_SPACE
    ~CudaUnified() {}

    void resize(std::size_t size)
    {
      if (size==0)
      {
        data_=std::shared_ptr<T>();
        dataDevice_ = data_.get();
      }
      else
      {
        data_ = std::shared_ptr<T>(cMallocManaged<T>(size),cFree<T>);
        dataDevice_ = data_.get();
      }
      size_= size; 
    }


    ALL_EXECUTION_SPACES
    ReturnType operator[] (std::size_t index)
    {
#ifdef __CUDA_ARCH__
      return dataDevice_[index];
#else
      return data_.get()[index];
#endif
    }

    ALL_EXECUTION_SPACES
    ConstReturnType operator[] (std::size_t index) const
    {
#ifdef __CUDA_ARCH__
      return dataDevice_[index];
#else
      return data_.get()[index];
#endif
    }

    // HOST_EXECUTION_SPACE
    // ReturnType operator[] (std::size_t index)
    // {
      // return data_.get()[index];
    // }

    // HOST_EXECUTION_SPACE
    // ConstReturnType operator[] (std::size_t index) const
    // {
      // return data_.get()[index];
    // }
  
    DEVICE_EXECUTION_SPACE
    constexpr std::size_t size () const
    {
      return size_;
    }

    static constexpr bool isDeviceAccessable()
    {
      return true;
    }

    static constexpr bool isHostAccessable()
    {
      return true;
    }
  private:

    std::shared_ptr<T> data_       = {nullptr};
    T*                 dataDevice_ = {nullptr};
    std::size_t size_{0};

};
#endif

// copy functions
#ifdef ENABLE_CUDA
template<typename T>
HostHeap<T> transferToHostHeap (CudaDeviceHeap<T> const & other)
{
  HostHeap<T> tmp(other.size());
  HANDLE_ERROR(cudaMemcpy(tmp.get(),other.get(),other.size()*sizeof(T),cudaMemcpyDeviceToHost));
  return tmp;
}

template<typename T>
CudaDeviceHeap<T> transferToDeviceHeap (HostHeap<T> const & other)
{
  CudaDeviceHeap<T> tmp(other.size());
  HANDLE_ERROR(cudaMemcpy(tmp.get(),other.get(),other.size()*sizeof(T),cudaMemcpyHostToDevice));
  return tmp;
}
#endif
// class CudaDeviceConstant : public CudaDeviceSpaceTag {

// };

// class CudaDeviceTexture : public CudaDeviceTexture {

// };


// ////////////////////// Unified
// class CudaUnifiedHeap : public CudaDeviceSpaceTag, public HostSpaceTag {

// };

// ///////////////////// Synced
// class CudaSyncedHeap : public CudaDeviceSpaceTag, public HostSpaceTag {

// };

// template<typename MemSpace,
         // typename std::enable_if<std::is_base_of<HostSpace,MemSpace>::value,int>::type = 0>
// constexpr bool isInHost






// template<typename MemorySpaceA, typename MemorySpaceB>
// constexpr bool isAvailableIn()
// {
  // return false;
// }
}
#endif
