/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARIES_H
#define ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARIES_H


#include "advectionDiffusionWithSourceLatticeDescriptors.h"
#include "advectionDiffusionWithSourceDynamics.h"
#include "dynamics/dynamics.h"

namespace olb {

/**
* This class computes the Advection Diffusion BC with general dynamics.
*/
//===================================================================================
//================= AdvectionDiffusionWithSourceDynamcison Flat Boundaries =========
//===================================================================================

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
class AdvectionDiffusionWithSourceBoundariesDynamics : public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  AdvectionDiffusionWithSourceBoundariesDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual AdvectionDiffusionWithSourceBoundariesDynamics<T, Lattice, Dynamics, direction, orientation>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};

//===================================================================================
//================= AdvectionDiffusionWithSourceDynamcison Flat Boundaries =========
//===================================================================================

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
class AdvectionDiffusionWithSourceRLBboundariesDynamics :
  public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  AdvectionDiffusionWithSourceRLBboundariesDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual AdvectionDiffusionWithSourceRLBboundariesDynamics<T, Lattice, Dynamics, direction, orientation>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};

//===================================================================================
//================= AdvectionDiffusionWithSourceDynamcis On Edges =========
//===================================================================================

template<typename T, template<typename U> class Lattice, typename Dynamics, int plane, int normal1, int normal2>
class AdvectionDiffusionWithSourceEdgesDynamics : public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  AdvectionDiffusionWithSourceEdgesDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual AdvectionDiffusionWithSourceEdgesDynamics<T, Lattice, Dynamics, plane, normal1, normal2>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};


//===================================================================================
//================= AdvectionDiffusionWithSourceDynamics on  Corners for 2D Boundaries =========
//===================================================================================

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal>
class AdvectionDiffusionWithSourceCornerDynamics2D : public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  AdvectionDiffusionWithSourceCornerDynamics2D(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual AdvectionDiffusionWithSourceCornerDynamics2D<T, Lattice, Dynamics, xNormal, yNormal>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};

//===================================================================================
//================= AdvectionDiffusionWithSourceDynamics on  Corners for 3D Boundaries =========
//===================================================================================

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal, int zNormal>
class AdvectionDiffusionWithSourceCornerDynamics3D : public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  AdvectionDiffusionWithSourceCornerDynamics3D(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual AdvectionDiffusionWithSourceCornerDynamics3D<T, Lattice, Dynamics, xNormal, yNormal, zNormal>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};



}  // namespace olb

#endif
