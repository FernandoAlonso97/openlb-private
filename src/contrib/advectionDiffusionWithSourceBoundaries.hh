/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARIES_HH
#define ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARIES_HH

#include "advectionDiffusionWithSourceBoundaries.h"
#include "advectionDiffusionWithSourceLatticeDescriptors.h"
#include "core/util.h"
#include "dynamics/utilAdvectionDiffusion.h"
#include "advectionDiffusionWithSourceLbHelpers.h"

namespace olb {

using namespace descriptors;

//=====================================================================================
//==================== For regularized Advection Diffusion Boundary Condition
//=====================================================================================


// For flat Walls

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>::AdvectionDiffusionWithSourceBoundariesDynamics(
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    boundaryDynamics(omega_, momenta_)
{
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>* AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice, Dynamics, direction, orientation>::clone() const
{
  return new AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
T AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>::
collide(CellView<T,Lattice>& cell,LatticeStatistics<T>& statistics)
{
  typedef Lattice<T> L;
  typedef advectionDiffusionWithSourceLbHelpers<T,Lattice> lbH;

  T confTensor = this->momenta.computeRho(cell);

  int missingNormal = 0;
  std::vector<int> missingDiagonal = util::subIndexOutgoing<L,direction,orientation>();
  std::vector<int> knownIndexes   = util::remainingIndexes<L>(missingDiagonal);
  // here I know all missing and non missing f_i
  for (unsigned iPop = 0; iPop < missingDiagonal.size(); ++iPop) {
    int numOfNonNullComp = 0;
    for (int iDim = 0; iDim < L::d; ++iDim) {
      numOfNonNullComp += abs(L::c[missingDiagonal[iPop]][iDim]);
    }

    if (numOfNonNullComp == 1) {
      missingNormal = missingDiagonal[iPop];
      missingDiagonal.erase(missingDiagonal.begin()+iPop);
      break;
    }
  }

  T sum = T();
  for (unsigned iPop = 0; iPop < knownIndexes.size(); ++iPop) {
    sum += cell[knownIndexes[iPop]];
  }
  cell[missingNormal] = confTensor - sum -(T)1;

  // Once all the f_i are known, I can call the collision for the Regularized Model.
  boundaryDynamics.collide(cell, statistics);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
T AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>::getOmega() const
{
  return boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,Dynamics,direction,orientation>::setOmega(T omega_)
{
  boundaryDynamics.setOmega(omega_);
}


//=====================================================================================
//==================== For Mass conserving Advection Diffusion Boundary Condition
//=====================================================================================


// For flat Walls

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>::
AdvectionDiffusionWithSourceRLBboundariesDynamics(T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    boundaryDynamics(omega_, momenta_)
{
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>* AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice, Dynamics, direction, orientation>::
clone() const
{
  return new AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
T AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>::
computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>::
collide(CellView<T,Lattice>& cell,LatticeStatistics<T>& statistics)
{
  typedef Lattice<T> L;
  typedef advectionDiffusionWithSourceLbHelpers<T,Lattice> lbH;

  T confTensor = this->momenta.computeRho(cell);

  int missingNormal = 0;
  std::vector<int> missingDiagonal = util::subIndexOutgoing<L,direction,orientation>();
  std::vector<int> knownIndexes   = util::remainingIndexes<L>(missingDiagonal);
  // here I know all missing and non missing f_i
  for (unsigned iPop = 0; iPop < missingDiagonal.size(); ++iPop) {
    int numOfNonNullComp = 0;
    for (int iDim = 0; iDim < L::d; ++iDim) {
      numOfNonNullComp += abs(L::c[missingDiagonal[iPop]][iDim]);
    }

    if (numOfNonNullComp == 1) {
      missingNormal = missingDiagonal[iPop];
      missingDiagonal.erase(missingDiagonal.begin()+iPop);
      break;
    }
  }

  T *u = cell.getExternal(L::ExternalField::velocityBeginsAt);
  int iOpp = util::opposite<L>(missingNormal);
  cell[missingNormal] = computeEquilibrium(missingNormal, confTensor, u, T()) -
                        (cell[iOpp] - computeEquilibrium(iOpp, confTensor, u, T()));

  // Once all the f_i are known, I can call the collision for the Regularized Model.
  boundaryDynamics.collide(cell, statistics);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
T AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>::getOmega() const
{
  return boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,Dynamics,direction,orientation>::setOmega(T omega_)
{
  boundaryDynamics.setOmega(omega_);
}

//=================================================================
// For 2D Corners with regularized Dynamic ==============================================
//=================================================================
template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal>
AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>::AdvectionDiffusionWithSourceCornerDynamics2D(
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    boundaryDynamics(omega_, momenta_)
{
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal>
AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>* AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice, Dynamics, xNormal,yNormal>::clone() const
{
  return new AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics,  int xNormal, int yNormal>
T AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice, typename Dynamics,  int xNormal, int yNormal>
void AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>::collide(CellView<T,Lattice>& cell,LatticeStatistics<T>& statistics)
{
  typedef Lattice<T> L;
  typedef advectionDiffusionWithSourceLbHelpers<T,Lattice> lbH;

  T confTensor = this->momenta.computeRho(cell);
  T* u = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  // I need to get Missing information on the corners !!!!
  std::vector<int> unknownIndexes = utilAdvDiff::subIndexOutgoing2DonCorners<L,xNormal,yNormal>();
  // here I know all missing and non missing f_i


  // The collision procedure for D2Q5 and D3Q7 lattice is the same ...
  // Given the rule f_i_neq = -f_opposite(i)_neq
  // I have the right number of equations for the number of unknowns using these lattices

  for (unsigned iPop = 0; iPop < unknownIndexes.size(); ++iPop) {
    cell[unknownIndexes[iPop]] = lbH::equilibrium(unknownIndexes[iPop], confTensor, u)
                                 -(cell[util::opposite<L>(unknownIndexes[iPop])]
                                   - lbH::equilibrium(util::opposite<L>(unknownIndexes[iPop]), confTensor, u) ) ;
  }

  // Once all the f_i are known, I can call the collision for the Regularized Model.
  boundaryDynamics.collide(cell, statistics);

}

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal>
T AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>::getOmega() const
{
  return boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal>
void AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,Dynamics,xNormal,yNormal>::setOmega(T omega_)
{
  boundaryDynamics.setOmega(omega_);
}



//=================================================================
// For 3D Corners with regularized Dynamic ==============================================
//=================================================================
template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal, int zNormal>
AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal,zNormal>::AdvectionDiffusionWithSourceCornerDynamics3D(
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    boundaryDynamics(omega_, momenta_)
{
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal, int zNormal>
AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal, zNormal>* AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice, Dynamics, xNormal,yNormal,zNormal>::clone() const
{
  return new AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal,zNormal>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics,  int xNormal, int yNormal, int zNormal>
T AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal,zNormal>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice, typename Dynamics,  int xNormal, int yNormal, int zNormal>
void AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal,zNormal>::collide(CellView<T,Lattice>& cell,LatticeStatistics<T>& statistics)
{
  typedef Lattice<T> L;
  typedef advectionDiffusionWithSourceLbHelpers<T,Lattice> lbH;

  T confTensor = this->momenta.computeRho(cell);
  T* u = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  // I need to get Missing information on the corners !!!!
  std::vector<int> unknownIndexes = utilAdvDiff::subIndexOutgoing3DonCorners<L,xNormal,yNormal,zNormal>();
  // here I know all missing and non missing f_i


  // The collision procedure for D2Q5 and D3Q7 lattice is the same ...
  // Given the rule f_i_neq = -f_opposite(i)_neq
  // I have the right number of equations for the number of unknowns using these lattices

  for (unsigned iPop = 0; iPop < unknownIndexes.size(); ++iPop) {
    cell[unknownIndexes[iPop]] = lbH::equilibrium(unknownIndexes[iPop], confTensor, u)
                                 -(cell[util::opposite<L>(unknownIndexes[iPop])]
                                   - lbH::equilibrium(util::opposite<L>(unknownIndexes[iPop]), confTensor, u) ) ;
  }

  // Once all the f_i are known, I can call the collision for the Regularized Model.
  boundaryDynamics.collide(cell, statistics);

}

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal, int zNormal>
T AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal,zNormal>::getOmega() const
{
  return boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int xNormal, int yNormal, int zNormal>
void AdvectionDiffusionWithSourceCornerDynamics3D<T,Lattice,Dynamics,xNormal,yNormal,zNormal>::setOmega(T omega_)
{
  boundaryDynamics.setOmega(omega_);
}

//=================================================================
// For 3D Edges with regularized Dynamic ==============================================
//=================================================================
template<typename T, template<typename U> class Lattice, typename Dynamics, int plane, int normal1, int normal2>
AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>::AdvectionDiffusionWithSourceEdgesDynamics(
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    boundaryDynamics(omega_, momenta_)
{
}

template<typename T, template<typename U> class Lattice, typename Dynamics,  int plane, int normal1, int normal2>
AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>* AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice, Dynamics,plane,normal1, normal2>::clone() const
{
  return new AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int plane, int normal1, int normal2>
T AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice, typename Dynamics,  int plane, int normal1, int normal2>
void AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>::collide(CellView<T,Lattice>& cell,LatticeStatistics<T>& statistics)
{
  typedef Lattice<T> L;
  typedef advectionDiffusionWithSourceLbHelpers<T,Lattice> lbH;

  T confTensor = this->momenta.computeRho(cell);
  T* u = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  // I need to get Missing information on the corners !!!!
  std::vector<int> unknownIndexes = utilAdvDiff::subIndexOutgoing3DonEdges<L,plane,normal1, normal2>();
  // here I know all missing and non missing f_i


  // The collision procedure for D2Q5 and D3Q7 lattice is the same ...
  // Given the rule f_i_neq = -f_opposite(i)_neq
  // I have the right number of equations for the number of unknowns using these lattices

  for (unsigned iPop = 0; iPop < unknownIndexes.size(); ++iPop) {
    cell[unknownIndexes[iPop]] = lbH::equilibrium(unknownIndexes[iPop], confTensor, u)
                                 -(cell[util::opposite<L>(unknownIndexes[iPop])]
                                   - lbH::equilibrium(util::opposite<L>(unknownIndexes[iPop]), confTensor, u) ) ;
  }

  // Once all the f_i are known, I can call the collision for the Regularized Model.
  boundaryDynamics.collide(cell, statistics);

}

template<typename T, template<typename U> class Lattice, typename Dynamics, int plane, int normal1, int normal2>
T AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>::getOmega() const
{
  return boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int plane, int normal1, int normal2>
void AdvectionDiffusionWithSourceEdgesDynamics<T,Lattice,Dynamics,plane,normal1, normal2>::setOmega(T omega_)
{
  boundaryDynamics.setOmega(omega_);
}




}  // namespace olb




#endif
