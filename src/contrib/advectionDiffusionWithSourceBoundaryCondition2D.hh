/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas, Andrea Parmigiani
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A helper for initialising 2D boundaries -- generic implementation.
 */
#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_CONDITION_2D_HH
#define ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_CONDITION_2D_HH

#include "advectionDiffusionWithSourceBoundaries.h"
#include "advectionDiffusionWithSourceBoundaries.hh"
#include "advectionDiffusionWithSourceBoundaryCondition2D.h"
#include "advectionDiffusionWithSourceBoundaryInstantiator2D.h"
#include "advectionDiffusionWithSourceBoundaryPostProcessor2D.h"
#include "advectionDiffusionWithSourceBoundaryPostProcessor2D.hh"


namespace olb {
template<typename T, template<typename U> class Lattice, class MixinDynamics>
class LocalAdvectionDiffusionWithSourceBoundaryManager2D {
public:
  template<int direction, int orientation> static Momenta*
  getConfTensorBoundaryMomenta();
  template<int direction, int orientation> static Dynamics<T,Lattice>*
  getConfTensorBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static PostProcessorGenerator2D<T,Lattice>*
  getConfTensorBoundaryProcessor(int x0, int x1, int y0, int y1);

  template<int xNormal, int yNormal> static Momenta*
  getConfTensorCornerMomenta();
  template<int xNormal, int yNormal> static Dynamics<T,Lattice>*
  getConfTensorCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal> static PostProcessorGenerator2D<T,Lattice>*
  getConfTensorCornerProcessor(int x, int y);

};

template<typename T, template<typename U> class Lattice, class MixinDynamics>
class LocalRLBadvectionDiffusionWithSourceBoundaryManager2D {
public:
  template<int direction, int orientation> static Momenta*
  getConfTensorBoundaryMomenta();
  template<int direction, int orientation> static Dynamics<T,Lattice>*
  getConfTensorBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static PostProcessorGenerator2D<T,Lattice>*
  getConfTensorBoundaryProcessor(int x0, int x1, int y0, int y1);

  template<int xNormal, int yNormal> static Momenta*
  getConfTensorCornerMomenta();
  template<int xNormal, int yNormal> static Dynamics<T,Lattice>*
  getConfTensorCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal> static PostProcessorGenerator2D<T,Lattice>*
  getConfTensorCornerProcessor(int x, int y);

};

template<typename T, template<typename U> class Lattice, class MixinDynamics>
class InterpAdvectionDiffusionWithSourceBoundaryManager2D {
public:
  template<int direction, int orientation> static Momenta*
  getConfTensorBoundaryMomenta();
  template<int direction, int orientation> static Dynamics<T,Lattice>*
  getConfTensorBoundaryDynamics(T omega, Momenta& momenta);
  template<int direction, int orientation> static PostProcessorGenerator2D<T,Lattice>*
  getConfTensorBoundaryProcessor(int x0, int x1, int y0, int y1);

  template<int xNormal, int yNormal> static Momenta*
  getConfTensorCornerMomenta();
  template<int xNormal, int yNormal> static Dynamics<T,Lattice>*
  getConfTensorCornerDynamics(T omega, Momenta& momenta);
  template<int xNormal, int yNormal> static PostProcessorGenerator2D<T,Lattice>*
  getConfTensorCornerProcessor(int x, int y);

};


////////// LocalAdvectionDiffusionWithSourceBoundaryManager2D /////////////////////////////////////////

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
Momenta*
LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::getConfTensorBoundaryMomenta()
{
  return new EquilibriumBM<T,Lattice>;
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
Dynamics<T,Lattice>* LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryDynamics(T omega, Momenta& momenta)
{
  return new
         AdvectionDiffusionWithSourceBoundariesDynamics<T,Lattice,
         MixinDynamics,direction,orientation>(omega, momenta);
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
PostProcessorGenerator2D<T,Lattice>*
LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryProcessor(int x0, int x1, int y0, int y1)
{
  return 0;
}

//==================  Corners ================================

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
Momenta*
LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerMomenta()
{
  return new EquilibriumBM<T,Lattice>;
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
Dynamics<T,Lattice>* LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerDynamics(T omega, Momenta& momenta)
{
  return new
         AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,
         MixinDynamics,xNormal,yNormal>(omega, momenta);
}



template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
PostProcessorGenerator2D<T,Lattice>*
LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerProcessor(int x, int y)
{
  return 0;
}

////////// LocalRLBadvectionDiffusionWithSourceBoundaryManager2D /////////////////////

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
Momenta*
LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryMomenta()
{
  return new EquilibriumBM<T,Lattice>;
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
Dynamics<T,Lattice>* LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryDynamics(T omega, Momenta& momenta)
{
  return new
         AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,
         MixinDynamics,direction,orientation>(omega, momenta);
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
PostProcessorGenerator2D<T,Lattice>*
LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryProcessor(int x0, int x1, int y0, int y1)
{
  return 0;
}

//==================  Corners ================================

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
Momenta*
LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerMomenta()
{
  return new EquilibriumBM<T,Lattice>;
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
Dynamics<T,Lattice>* LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerDynamics(T omega, Momenta& momenta)
{
  return new
         AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,
         MixinDynamics,xNormal,yNormal>(omega, momenta);
}



template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
PostProcessorGenerator2D<T,Lattice>*
LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerProcessor(int x, int y)
{
  return 0;
}



////////// InterpAdvectionDiffusionWithSourceBoundaryManager2D /////////////////////////////////////////

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
Momenta*
InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::getConfTensorBoundaryMomenta()
{
  return new EquilibriumBM<T,Lattice>;
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
Dynamics<T,Lattice>* InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryDynamics(T omega, Momenta& momenta)
{
  return new
         AdvectionDiffusionWithSourceRLBboundariesDynamics<T,Lattice,
         MixinDynamics,direction,orientation>(omega, momenta);
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int direction, int orientation>
PostProcessorGenerator2D<T,Lattice>*
InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorBoundaryProcessor(int x0, int x1, int y0, int y1)
{
  return new
         AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>(x0, x1,y0,y1);
}

//==================  Corners ================================

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
Momenta*
InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerMomenta()
{
  return new EquilibriumBM<T,Lattice>;
}

template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
Dynamics<T,Lattice>* InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerDynamics(T omega, Momenta& momenta)
{
  return new
         AdvectionDiffusionWithSourceCornerDynamics2D<T,Lattice,
         MixinDynamics,xNormal,yNormal>(omega, momenta);
}



template<typename T, template<typename U> class Lattice, class MixinDynamics>
template<int xNormal, int yNormal>
PostProcessorGenerator2D<T,Lattice>*
InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice,MixinDynamics>::
getConfTensorCornerProcessor(int x, int y)
{
  return new
         AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D<T,
         Lattice,xNormal,yNormal>(x,y);
}

////////// Factory functions //////////////////////////////////////////////////

template<typename T, template<typename U> class Lattice, typename MixinDynamics>
OnLatticeAdvectionDiffusionWithSourceBoundaryCondition2D<T,Lattice>*
createLocalAdvectionDiffusionWithSourceBoundaryCondition2D(BlockLatticeStructure2D<T,Lattice>& block)
{
  return new AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T, Lattice,
         LocalAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice, MixinDynamics> > (block);
}

template<typename T, template<typename U> class Lattice, typename MixinDynamics>
OnLatticeAdvectionDiffusionWithSourceBoundaryCondition2D<T,Lattice>*
createLocalRLBadvectionDiffusionWithSourceBoundaryCondition2D(BlockLatticeStructure2D<T,Lattice>& block)
{
  return new AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T, Lattice,
         LocalRLBadvectionDiffusionWithSourceBoundaryManager2D<T,Lattice, MixinDynamics> > (block);
}

template<typename T, template<typename U> class Lattice, typename MixinDynamics>
OnLatticeAdvectionDiffusionWithSourceBoundaryCondition2D<T,Lattice>*
createInterpAdvectionDiffusionWithSourceBoundaryCondition2D(BlockLatticeStructure2D<T,Lattice>& block)
{
  return new AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T, Lattice,
         InterpAdvectionDiffusionWithSourceBoundaryManager2D<T,Lattice, MixinDynamics> > (block);
}



}  // namespace olb

#endif
