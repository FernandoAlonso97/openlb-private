/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file A helper for initialising 2D boundaries -- header file.  */
#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_INSTANTIATOR_2D_H
#define ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_INSTANTIATOR_2D_H

#include "advectionDiffusionWithSourceBoundaryCondition2D.h"
#include "advectionDiffusionWithSourceBoundaryCondition2D.hh"

namespace olb {

template<typename T, template<typename U> class Lattice, class BoundaryManager>
class AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D : public OnLatticeAdvectionDiffusionWithSourceBoundaryCondition2D<T,Lattice> {
public:
  AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D( BlockLatticeStructure2D<T,Lattice>& block_ );
  ~AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D();

  void addConfTensorBoundary0N(int x0, int x1, int y0, int y1, T omega);
  void addConfTensorBoundary0P(int x0, int x1, int y0, int y1, T omega);
  void addConfTensorBoundary1N(int x0, int x1, int y0, int y1, T omega);
  void addConfTensorBoundary1P(int x0, int x1, int y0, int y1, T omega);

  void addConfTensorCornerNN(int x, int y, T omega);
  void addConfTensorCornerNP(int x, int y, T omega);
  void addConfTensorCornerPN(int x, int y, T omega);
  void addConfTensorCornerPP(int x, int y, T omega);

  virtual BlockLatticeStructure2D<T,Lattice>& getBlock();
  virtual BlockLatticeStructure2D<T,Lattice> const& getBlock() const;
private:
  template<int direction, int orientation>
  void addConfTensorBoundary(int x0, int x1, int y0, int y1, T omega);
  template<int normalX, int normalY>
  void addConfTensorCorner(int x, int y, T omega);
private:
  BlockLatticeStructure2D<T,Lattice>& block;
  std::vector<Momenta*>  momentaVector;
  std::vector<Dynamics<T,Lattice>*> dynamicsVector;
};

///////// class AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D ////////////////////////

template<typename T, template<typename U> class Lattice, class BoundaryManager>
AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D (
  BlockLatticeStructure2D<T,Lattice>& block_)
  : block(block_)
{ }

template<typename T, template<typename U> class Lattice, class BoundaryManager>
AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::~AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D()
{
  for (unsigned iDynamics=0; iDynamics<dynamicsVector.size(); ++iDynamics) {
    delete dynamicsVector[iDynamics];
  }
  for (unsigned iMomenta=0; iMomenta<dynamicsVector.size(); ++iMomenta) {
    delete momentaVector[iMomenta];
  }
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int direction, int orientation>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorBoundary(int x0, int x1, int y0, int y1, T omega)
{
  OLB_PRECONDITION(x0==x1 || y0==y1);

  for (int iX=x0; iX<=x1; ++iX) {
    for (int iY=y0; iY<=y1; ++iY) {
      Momenta* momenta
        = BoundaryManager::template getConfTensorBoundaryMomenta<direction,orientation>();
      Dynamics<T,Lattice>* dynamics
        = BoundaryManager::template getConfTensorBoundaryDynamics<direction,orientation>(omega, *momenta);
      this->getBlock().defineDynamics(iX,iX,iY,iY, dynamics);
      momentaVector.push_back(momenta);
      dynamicsVector.push_back(dynamics);
    }
  }

  PostProcessorGenerator2D<T,Lattice>* postProcessor
    = BoundaryManager::template getConfTensorBoundaryProcessor<direction,orientation>(x0,x1, y0,y1);
  if (postProcessor) {
    this->getBlock().addPostProcessor(*postProcessor);
  }
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
template<int xNormal, int yNormal>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorCorner(int x, int y, T omega)
{
  Momenta* momenta
    = BoundaryManager::template getConfTensorCornerMomenta<xNormal,yNormal>();
  Dynamics<T,Lattice>* dynamics
    = BoundaryManager::template getConfTensorCornerDynamics<xNormal,yNormal>(omega, *momenta);

  this->getBlock().defineDynamics(x,x,y,y, dynamics);

  PostProcessorGenerator2D<T,Lattice>* postProcessor
    = BoundaryManager::template getConfTensorCornerProcessor<xNormal,yNormal>(x, y);
  if (postProcessor) {
    this->getBlock().addPostProcessor(*postProcessor);
  }
}



template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorBoundary0N(int x0, int x1, int y0, int y1, T omega)
{
  addConfTensorBoundary<0,-1>(x0,x1,y0,y1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorBoundary0P(int x0, int x1, int y0, int y1, T omega)
{
  addConfTensorBoundary<0,1>(x0,x1,y0,y1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorBoundary1N(int x0, int x1, int y0, int y1, T omega)
{
  addConfTensorBoundary<1,-1>(x0,x1,y0,y1, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorBoundary1P(int x0, int x1, int y0, int y1, T omega)
{
  addConfTensorBoundary<1,1>(x0,x1,y0,y1, omega);
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorCornerNN(int x, int y, T omega)
{
  addConfTensorCorner<-1,-1>(x,y, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorCornerNP(int x, int y, T omega)
{
  addConfTensorCorner<-1,1>(x,y, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorCornerPN(int x, int y, T omega)
{
  addConfTensorCorner<1,-1>(x,y, omega);
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
void AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::
addConfTensorCornerPP(int x, int y, T omega)
{
  addConfTensorCorner<1,1>(x,y, omega);
}


template<typename T, template<typename U> class Lattice, class BoundaryManager>
BlockLatticeStructure2D<T,Lattice>& AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::getBlock()
{
  return block;
}

template<typename T, template<typename U> class Lattice, class BoundaryManager>
BlockLatticeStructure2D<T,Lattice> const& AdvectionDiffusionWithSourceBoundaryConditionInstantiator2D<T,Lattice,BoundaryManager>::getBlock() const
{
  return block;
}

}


#endif
