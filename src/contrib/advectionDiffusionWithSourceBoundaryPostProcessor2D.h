/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN, Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_POST_PROCESSOR_2D_H
#define ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_POST_PROCESSOR_2D_H

#include "core/postProcessing.h"
#include "boundary/momentaOnBoundaries.h"
#include "core/blockLattice2D.h"

namespace olb {

/**
* This class computes the skordos BC
* on a flat wall in 2D but with a limited number of terms added to the
* equilibrium distributions (i.e. only the Q_i : Pi term)
*/
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class AdvectionDiffusionWithSourceFlatBoundaryProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  AdvectionDiffusionWithSourceFlatBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain ( BlockLattice2D<T,Lattice>& blockLattice,
                                  int x0_, int x1_, int y0_, int y1_ );

private:
  int x0, x1, y0, y1;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D : public PostProcessorGenerator2D<T,Lattice> {
public:
  AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_);
  virtual PostProcessor2D<T,Lattice>* generate() const;
  virtual PostProcessorGenerator2D<T,Lattice>*  clone() const;
};

/**
* This class computes the skordos BC
* on a corner in 2D but with a limited number of terms added to the
* equilibrium distributions (i.e. only the Q_i : Pi term)
*/
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class AdvectionDiffusionWithSourceCornerBoundaryProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  AdvectionDiffusionWithSourceCornerBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_);
  virtual int extent() const
  {
    return 3;
  }
  virtual int extent(int whichDirection) const
  {
    return 3;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain ( BlockLattice2D<T,Lattice>& blockLattice,
                                  int x0_, int x1_, int y0_, int y1_ );

private:
  int x0, x1, y0, y1;
};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D : public PostProcessorGenerator2D<T,Lattice> {
public:
  AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D(int x_, int y_);
  virtual PostProcessor2D<T,Lattice>* generate() const;
  virtual PostProcessorGenerator2D<T,Lattice>*  clone() const;
};


}

#endif
