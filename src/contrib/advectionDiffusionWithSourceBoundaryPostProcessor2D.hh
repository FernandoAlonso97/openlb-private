/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN, Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_POST_PROCESSOR_2D_HH
#define ADVECTION_DIFFUSION_WITH_SOURCE_BOUNDARY_POST_PROCESSOR_2D_HH

#include "boundary/boundaryPostProcessors2D.h"
#include "contrib/advectionDiffusionWithSourceBoundaryPostProcessor2D.h"
#include "contrib/extrapolation.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "dynamics/lbHelpers.h"

namespace olb {

///////////  StraightFdBoundaryProcessor2D ///////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction, int orientation>
AdvectionDiffusionWithSourceFlatBoundaryProcessor2D<T,Lattice,direction,orientation>::
AdvectionDiffusionWithSourceFlatBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_)
{
  OLB_PRECONDITION(x0==x1 || y0==y1);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void AdvectionDiffusionWithSourceFlatBoundaryProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_)
{
  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    int iX;

#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        //        std::cout << iX+(direction==0 ? (-orientation):0) << ", " << iY+(direction==1 ? (-orientation):0) << std::endl;
        const T A1 = blockLattice.get(iX+(direction==0 ? (-orientation):0),
                                      iY+(direction==1 ? (-orientation):0) ).computeRho();
        const T A2 = blockLattice.get(iX+(direction==0 ? (-2*orientation):0),
                                      iY+(direction==1 ? (-2*orientation):0) ).computeRho();
        const T A3 = blockLattice.get(iX+(direction==0 ? (-3*orientation):0),
                                      iY+(direction==1 ? (-3*orientation):0) ).computeRho();
        //        const T A4 = blockLattice.get(iX+(direction==0 ? (-4*orientation):0),
        //                        iY+(direction==1 ? (-4*orientation):0) ).computeRho();
        //        const T A5 = blockLattice.get(iX+(direction==0 ? (-5*orientation):0),
        //                        iY+(direction==1 ? (-5*orientation):0) ).computeRho();

        T A0 = fd::boundaryExtrapolation(A1,A2,A3);
        //        T A0 = fd::boundaryExtrapolation4th(A1,A2,A3,A4,A5);
        //        T A0 = A1;
        //        std::cout << iX << ", " << iY << ", " << A5 << ", " << A4 << ", " << A3 << ", ";
        //        std::cout << A2 << ", " << A1 << ", " << A0 << std::endl;

        blockLattice.get(iX,iY).defineRho(A0);

      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
void AdvectionDiffusionWithSourceFlatBoundaryProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

////////  AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int direction,int orientation>
AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D<T,Lattice, direction,orientation>::
AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_)
  : PostProcessorGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_)
{ }

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor2D<T,Lattice>*
AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::generate() const
{
  return new AdvectionDiffusionWithSourceFlatBoundaryProcessor2D<T,Lattice,direction,orientation>
         ( this->x0, this->x1, this->y0, this->y1);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessorGenerator2D<T,Lattice>*
AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new AdvectionDiffusionWithSourceFlatBoundaryProcessorGenerator2D<T,Lattice,direction,orientation>
         (this->x0, this->x1, this->y0, this->y1);
}


///////////  StraightFdBoundaryProcessor2D ///////////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
AdvectionDiffusionWithSourceCornerBoundaryProcessor2D<T,Lattice,xNormal,yNormal>::
AdvectionDiffusionWithSourceCornerBoundaryProcessor2D(int x0_, int x1_, int y0_, int y1_)
  : x0(x0_), x1(x1_), y0(y0_), y1(y1_)
{
  OLB_PRECONDITION(x0==x1 && y0==y1);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void AdvectionDiffusionWithSourceCornerBoundaryProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice, int x0_, int x1_, int y0_, int y1_)
{
  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    int iX;

#ifdef PARALLEL_MODE_OMP
    #pragma omp parallel for
#endif
    for (iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        const T A1_1 = blockLattice.get(iX - xNormal,  iY - yNormal).computeRho();
        const T A1_2 = blockLattice.get(iX - xNormal,  iY - 2*yNormal).computeRho();
        const T A2_1 = blockLattice.get(iX - 2*xNormal,iY - yNormal).computeRho();
        const T A2_2 = blockLattice.get(iX - 2*xNormal,iY - 2*yNormal).computeRho();
        const T A3_1 = blockLattice.get(iX - 3*xNormal,iY - yNormal).computeRho();
        const T A1_3 = blockLattice.get(iX - xNormal,  iY - 3*yNormal).computeRho();


        //        T A0 = A1_1;
        T A0 = fd::cornerBoundaryExtrapolation(A1_1,A2_1,A1_2,A2_2,A3_1,A1_3);
        //        std::cout << iX << ", " << iY << ", " << A1_3 << ", " << A3_1 << ", " << A2_2 << ", ";
        //        std::cout << A1_2 << ", " << A2_1 << ", " << A1_1 << ", " << A0 << std::endl;

        blockLattice.get(iX,iY).defineRho(A0);

      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void AdvectionDiffusionWithSourceCornerBoundaryProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

////////  AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D ////////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D<T,Lattice,xNormal,yNormal>::
AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D(int x_, int y_)
  : PostProcessorGenerator2D<T,Lattice>(x_, x_, y_, y_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>*
AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D<T,Lattice,xNormal,yNormal>::generate() const
{
  return new AdvectionDiffusionWithSourceCornerBoundaryProcessor2D<T,Lattice,xNormal,yNormal>
         ( this->x0, this->x1, this->y0, this->y1);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessorGenerator2D<T,Lattice>*
AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D<T,Lattice,xNormal,yNormal>::clone() const
{
  return new AdvectionDiffusionWithSourceCornerBoundaryProcessorGenerator2D<T,Lattice,xNormal,yNormal>
         (this->x0, this->y0);
}

}  // namespace olb

#endif
