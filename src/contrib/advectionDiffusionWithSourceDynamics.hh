/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- generic implementation.
 */
#ifndef ADVECTION_DIFFUSION_WITH_SOURCE_DYNAMICS_HH
#define ADVECTION_DIFFUSION_WITH_SOURCE_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "advectionDiffusionWithSourceDynamics.h"
#include "advectionDiffusionWithSourceLbHelpers.h"

namespace olb {


////////////////////// Class AdvectionDiffusionRLBdynamics //////////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */

//==================================================================//
//============= BGK Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class Lattice>
AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>::AdvectionDiffusionWithSourceBGKdynamics (
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>* AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>::clone() const
{
  return new AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice>
void AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>::collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics )
{
  T confTensor = this->momenta.computeRho(cell);

  const T* u = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  const T* source = cell.getExternal(Lattice<T>::ExternalField::scalarBeginsAt);

  T uSqr = advectionDiffusionWithSourceLbHelpers<T,Lattice>::
           bgkCollision(cell, confTensor, u, source[0], omega);

  statistics.incrementStats(confTensor, uSqr);
}

template<typename T, template<typename U> class Lattice>
T AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void AdvectionDiffusionWithSourceBGKdynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}

//==================================================================//
//============= Regularized Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class Lattice>
AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>::AdvectionDiffusionWithSourceRLBdynamics (
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>*
AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>::clone() const
{
  return new AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>::
computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return advectionDiffusionWithSourceLbHelpers<T,Lattice>::equilibrium(iPop, rho, u);
}


template<typename T, template<typename U> class Lattice>
void AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>::
collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;
  T confTensor = this->momenta.computeRho(cell);

  const T* u = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  const T* source = cell.getExternal(Lattice<T>::ExternalField::scalarBeginsAt);
  T* aU = cell.getExternal(Lattice<T>::ExternalField::vector_t1BeginsAt);

  T dt_aU[L::d];
  for (int iD = 0; iD < L::d; ++iD) {
    dt_aU[iD] = confTensor * u[iD] - aU[iD];
  }

  T uSqr = advectionDiffusionWithSourceLbHelpers<T,Lattice>::
           rlbCollision(cell, confTensor, u, source[0], dt_aU, omega);
  //  T uSqr = advectionDiffusionWithSourceLbHelpers<T,Lattice>::
  //             rlbCollision(cell, confTensor, u, source[0], omega);

  for (int iD = 0; iD < L::d; ++iD) {
    aU[iD] = confTensor;
  }

  statistics.incrementStats(confTensor, uSqr);
}

template<typename T, template<typename U> class Lattice>
T AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void AdvectionDiffusionWithSourceRLBdynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}


} // namespace olb













#endif
