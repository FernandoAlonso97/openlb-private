/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- header file.
 */
#ifndef APPROX_ENTROPIC_LB_DYNAMICS_H
#define APPROX_ENTROPIC_LB_DYNAMICS_H

#include "dynamics/dynamics.h"

namespace olb {

template<typename T, template<typename U> class Lattice> class CellView;


/// Implementation of the entropic collision step
template<typename T, template<typename U> class Lattice>
class ApproxEntropicDynamics : public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  ApproxEntropicDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual ApproxEntropicDynamics<T,Lattice>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  T omega;  ///< relaxation parameter
};

/// Implementation of the forced entropic collision step
template<typename T, template<typename U> class Lattice>
class ForcedApproxEntropicDynamics : public BasicDynamics<T,Lattice> {
public:
  /// Constructor
  ForcedApproxEntropicDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual ForcedApproxEntropicDynamics<T,Lattice>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  T omega;  ///< relaxation parameter

  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;
};

}

#endif
