/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- generic implementation.
 */
#ifndef APPROX_ENTROPIC_LB_DYNAMICS_HH
#define APPROX_ENTROPIC_LB_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "dynamics/lbHelpers.h"
#include "approxEntropicDynamics.h"
#include "dynamics/entropicLbHelpers.h"
#include "entropicLatticeDescriptors.h"
#include "approxEntropicLbHelpers.h"

namespace olb {

//==============================================================================//
/////////////////////////// Class ApproxEntropicDynamics ///////////////////////////////
//==============================================================================//
/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice>
ApproxEntropicDynamics<T,Lattice>::ApproxEntropicDynamics (
  T omega_, Momenta& momenta_ )
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
ApproxEntropicDynamics<T,Lattice>* ApproxEntropicDynamics<T,Lattice>::clone() const
{
  return new ApproxEntropicDynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T ApproxEntropicDynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return entropicLbHelpers<T,Lattice>::equilibrium(iPop,rho,u);
}

template<typename T, template<typename U> class Lattice>
void ApproxEntropicDynamics<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;
  typedef entropicLbHelpers<T,Lattice> eLbH;

  T rho, u[Lattice<T>::d], pi[util::TensorVal<Lattice<T> >::n];
  this->momenta.computeAllMomenta(cell, rho, u, pi);
  T uSqr = util::normSqr<T,L::d>(u);

  T *alpha = cell.getExternal(Lattice<T>::ExternalField::scalarBeginsAt);
  alpha[0] = approxEntropicLbHelpers<T,Lattice>::alphaFromPi2(rho,pi);

  T omegaTot = omega / 2.0 * alpha[0];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] *= (T)1-omegaTot;
    cell[iPop] += omegaTot * eLbH::equilibrium(iPop,rho,u);
  }

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice>
T ApproxEntropicDynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void ApproxEntropicDynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}

//====================================================================//
//////////////////// Class ForcedApproxEntropicDynamics //////////////////////
//====================================================================//

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 */
template<typename T, template<typename U> class Lattice>
ForcedApproxEntropicDynamics<T,Lattice>::ForcedApproxEntropicDynamics (
  T omega_, Momenta& momenta_ )
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
ForcedApproxEntropicDynamics<T,Lattice>* ForcedApproxEntropicDynamics<T,Lattice>::clone() const
{
  return new ForcedApproxEntropicDynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T ForcedApproxEntropicDynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return entropicLbHelpers<T,Lattice>::equilibrium(iPop,rho,u);
}


template<typename T, template<typename U> class Lattice>
void ForcedApproxEntropicDynamics<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;
  typedef entropicLbHelpers<T,Lattice> eLbH;

  T rho, u[Lattice<T>::d], pi[util::TensorVal<Lattice<T> >::n];
  this->momenta.computeRhoU(cell, rho, u);

  T* force = cell.getExternal(forceBeginsAt);
  for (int iDim=0; iDim<Lattice<T>::d; ++iDim) {
    u[iDim] += force[iDim] / (T)2.;
  }
  T uSqr = util::normSqr<T,L::d>(u);
  lbHelpers<T,Lattice>::computeStress (cell, rho, u, pi);

  T *alpha = cell.getExternal(Lattice<T>::ExternalField::scalarBeginsAt);
  alpha[0] = approxEntropicLbHelpers<T,Lattice>::alphaFromPi(rho,pi);
  T omegaTot = omega / 2.0 * alpha[0];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] *= (T)1-omegaTot;
    cell[iPop] += omegaTot * eLbH::equilibrium(iPop,rho,u);
  }
  lbHelpers<T,Lattice>::addExternalForce(cell, u, omegaTot);

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice>
T ForcedApproxEntropicDynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void ForcedApproxEntropicDynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}

}

#endif

