/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- header file.
 */
#ifndef APPROX_ENTROPIC_LB_HELPERS_H
#define APPROX_ENTROPIC_LB_HELPERS_H

#include <cmath>

namespace olb {

template<typename T, template<typename U> class Lattice>
struct approxEntropicLbHelpers {
  /// Computation of equilibrium distribution
  static T alphaMa1( const T fNeq[Lattice<T>::q], const T &rho, const T u[Lattice<T>::d])
  {
    typedef Lattice<T> L;

    T A = T();
    T B = T();
    for (int iPop = 0; iPop < L::q; ++iPop) {
      T f2 = fNeq[iPop]*fNeq[iPop];
      T f3 = fNeq[iPop]*fNeq[iPop]*fNeq[iPop];
      A += f2/L::t[iPop];
      B += f3/(L::t[iPop]*L::t[iPop]);
    }

    const T epsilon = std::numeric_limits<T>::epsilon();

    T alpha = (T)2;
    if (fabs(A) > (T)100*epsilon) {
      return alpha -B/((T)3*rho);
    } else {
      return alpha;
    }
  }

  /// Computation of equilibrium distribution
  static T alphaMa2( const T fNeq[Lattice<T>::q], const T &rho, const T u[Lattice<T>::d])
  {
    typedef Lattice<T> L;
    const T invCs = sqrt(L::invCs2);

    T A = T();
    T B = T();
    T C = T();
    T E = T();
    T F = T();

    for (int iPop = 0; iPop < L::q; ++iPop) {
      T c_uCs = T();
      for (int iD = 0; iD < L::d; ++iD) {
        c_uCs += L::c(iPop)[iD] * u[iD] * invCs;
      }

      T f2 = fNeq[iPop]*fNeq[iPop];
      T f3 = fNeq[iPop]*fNeq[iPop]*fNeq[iPop];
      A += f2/L::t[iPop];
      B += f3/(L::t[iPop]*L::t[iPop]);
      C += c_uCs*f2/L::t[iPop];
      E += f2*c_uCs*c_uCs/L::t[iPop];
      F += c_uCs*f3/(L::t[iPop]*L::t[iPop]);
    }

    const T epsilon = std::numeric_limits<T>::epsilon();

    T alpha = (T)2;
    if (fabs(A) > (T)100*epsilon) {
      return alpha + (-B/((T)3*rho) + (T)2/((T)3*rho)*F*invCs)/A +
             (B*B/((T)9*rho*rho) + B*E*L::invCs2/((T)6*rho) - B*C*invCs/((T)3*rho))/(A*A);
    } else {
      return alpha;
    }
  }

  /// Computation of equilibrium distribution
  static T alphaMa2fNeq(const T &rho, const T u[Lattice<T>::d], const T pi[])
  {
    typedef Lattice<T> L;
    const T invCs = sqrt(L::invCs2);

    T A = T();
    T B = T();
    T C = T();
    T E = T();
    T F = T();

    T fNeq[Lattice<T>::q];
    for (int iPop = 0; iPop < L::q; ++iPop) {
      fNeq[iPop] = firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop,pi);
      T c_uCs = T();
      for (int iD = 0; iD < L::d; ++iD) {
        c_uCs += L::c(iPop)[iD] * u[iD] * invCs;
      }

      T f2 = fNeq[iPop]*fNeq[iPop];
      T f3 = fNeq[iPop]*fNeq[iPop]*fNeq[iPop];
      A += f2/L::t[iPop];
      B += f3/(L::t[iPop]*L::t[iPop]);
      C += c_uCs*f2/L::t[iPop];
      E += f2*c_uCs*c_uCs/L::t[iPop];
      F += c_uCs*f3/(L::t[iPop]*L::t[iPop]);
    }

    const T epsilon = std::numeric_limits<T>::epsilon();

    T alpha = (T)2;
    if (fabs(A) > (T)100*epsilon) {
      return alpha + (-B/((T)3*rho) + (T)2/((T)3*rho)*F*invCs)/A +
             (B*B/((T)9*rho*rho) + B*E*L::invCs2/((T)6*rho) - B*C*invCs/((T)3*rho))/(A*A);
    } else {
      return alpha;
    }
  }

  /// Computation of equilibrium distribution
  static T alphaMa3( const T fNeq[Lattice<T>::q], const T &rho, const T u[Lattice<T>::d])
  {
    typedef Lattice<T> L;
    const T invCs = sqrt(L::invCs2);

    T Ma2 = T();
    for (int iD = 0; iD < L::d; ++iD) {
      Ma2 += (u[iD] * invCs) * (u[iD] * invCs);
    }

    T A = T();
    T B = T();
    T C = T();
    T E = T();
    T F = T();
    T G = T();

    for (int iPop = 0; iPop < L::q; ++iPop) {
      T c_uCs = T();
      for (int iD = 0; iD < L::d; ++iD) {
        c_uCs += L::c(iPop)[iD] * u[iD] * invCs;
      }

      T f2 = fNeq[iPop]*fNeq[iPop];
      T f3 = fNeq[iPop]*fNeq[iPop]*fNeq[iPop];

      A += f2/L::t[iPop];
      B += f3/(L::t[iPop]*L::t[iPop]);
      C += c_uCs*f2/L::t[iPop];
      E += f2*c_uCs*c_uCs/L::t[iPop];
      F += c_uCs*f3/(L::t[iPop]*L::t[iPop]);
      G += f3*c_uCs*c_uCs/(L::t[iPop]*L::t[iPop]);
    }

    const T epsilon = std::numeric_limits<T>::epsilon();

    T alpha = (T)2;
    if (fabs(A) > (T)100*epsilon) {
      return alpha + (-B/((T)3*rho) + (T)2*F*invCs/((T)3*rho) -
                      (T)2*G*L::invCs2/((T)3*rho) - Ma2*B/((T)6*rho))/A +
             (B*B/((T)9*rho*rho) - E*F*L::invCs2*invCs/((T)3*rho) +
              (T)2*C*F*L::invCs2/((T)3*rho) - (T)4*B*F*invCs/((T)9*rho*rho) +
              B*E*L::invCs2/((T)6*rho) - B*C*invCs/((T)3*rho))/(A*A) +
             (B*C*E*L::invCs2*invCs/((T)3*rho) - B*B*E*L::invCs2/((T)9*rho*rho) +
              (T)2*B*B*C*invCs/((T)9*rho*rho) - B*C*C*L::invCs2/((T)3*rho) -
              B*B*B/((T)16*rho*rho*rho))/(A*A*A);
    } else {
      return alpha;
    }


  }

  /// Computation of equilibrium distribution
  static T alphaFromPi( const T rho, const T pi[])
  {
    typedef Lattice<T> L;
    typedef firstOrderLbHelpers<T,Lattice> fLbH;

    T A = T();
    T B = T();
    for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
      T fNeq = fLbH::fromPiToFneq(iPop,pi);
      A += fNeq*fNeq/L::t[iPop];
      B += fNeq*fNeq*fNeq/(L::t[iPop]*L::t[iPop]);
    }

    const T epsilon = std::numeric_limits<T>::epsilon();
    if (fabs(A) > epsilon*(T)100) {
      //             std::cout << B << ", " << A << ", " << B/A << "\n";
      return (T)2-B/((T)3*A*rho);
    } else {
      return (T)2;
    }
  }

  /// Computation of equilibrium distribution
  static T alphaFromPi2( const T rho, const T pi[])
  {
    typedef Lattice<T> L;
    typedef firstOrderLbHelpers<T,Lattice> fLbH;

    T stress[L::d][L::d];

    int iPi = 0;
    for (int iAlpha=0; iAlpha < Lattice<T>::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta < Lattice<T>::d; ++iBeta) {
        stress[iAlpha][iBeta] = pi[iPi];
        if (iAlpha!=iBeta) {
          stress[iBeta][iAlpha] = pi[iPi];
        }
        ++iPi;
      }
    }

    T A = T();
    T B = T();
    for (int iAlpha=0; iAlpha < Lattice<T>::d; ++iAlpha) {
      for (int iBeta=0; iBeta < Lattice<T>::d; ++iBeta) {
        A += stress[iAlpha][iBeta]*stress[iAlpha][iBeta];
        for (int iGamma=0; iGamma < Lattice<T>::d; ++iGamma) {
          B += stress[iAlpha][iBeta]*stress[iBeta][iGamma]*stress[iGamma][iAlpha];
        }
      }
    }
    const T epsilon = std::numeric_limits<T>::epsilon();
    if (fabs(A) > epsilon*(T)100) {
      //             std::cout << B << ", " << A << ", " << B/A << "\n";
      return (T)2-B/((T)3*A*rho);
    } else {
      return (T)2;
    }
  }

  /// Computation of equilibrium distribution
  static T alphaFromFneq( CellView<T,Lattice> &cell, const T rho, const T u[])
  {
    typedef Lattice<T> L;
    typedef firstOrderLbHelpers<T,Lattice> fLbH;

    T A = T();
    T B = T();
    for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
      T fNeq = cell[iPop] -
               entropicLbHelpers<T,Lattice>::equilibrium(iPop,rho,u);
      A += fNeq*fNeq/L::t[iPop];
      B += fNeq*fNeq*fNeq/(L::t[iPop]*L::t[iPop]);
    }

    const T epsilon = std::numeric_limits<T>::epsilon();
    if (fabs(A) > epsilon*(T)100) {
      //             std::cout << B << ", " << A << ", " << B/A << "\n";
      return (T)2-B/((T)3*A*rho);
    } else {
      return (T)2;
    }
  }
};

}


#endif
