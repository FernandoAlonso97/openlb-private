/*  Copyright (C) 2015 Sebastian Schmidt, Patrick Nathen
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef BLOCK_WALL_FUNCTION_F_3D_H
#define BLOCK_WALL_FUNCTION_F_3D_H

#include "functors/lattice/blockLatticeLocalF3D.h"

namespace olb {


enum class UWallProfile : char {MUSKER, TBLE};
enum class fNeqCalcType : char {PI, GRADU};


/// Functor that computes the Populations for boundary cells
template <typename T, template <typename U> class DESCRIPTOR>
class BlockWallFunctionF3D : public BlockLatticeF3D<T, DESCRIPTOR> {
private:
  SuperLattice3D<T, DESCRIPTOR>& _sLattice;
  BlockGeometryStructure3D<T>& _bGeometry;
  IndicatorF3D<T>& _wallShape;
  const LBconverter<T>& _converter;
  UWallProfile profile_type;
  fNeqCalcType calc_type;
  T rel_interval_size;
  T abs_interval_size;
  BlockLatticePhysVelocity3D<T, DESCRIPTOR> _velocity;
  BlockLatticePhysPressure3D<T, DESCRIPTOR> _pressure;

  /// compute the stress tensor
  void computePiStraightBoundary(CellView<T,DESCRIPTOR> const& cell, T rho,
                                 const T u[DESCRIPTOR<T>::d],
                                 T Pi[util::TensorVal<DESCRIPTOR<T> >::n],
                                 std::vector<T> normal);

  /// compute Pi for curved Boundaries by extrapolating from the bulk
  void computePiCurvedBoundary(const int index[], std::vector<T>normal,
                               T Pi[util::TensorVal<DESCRIPTOR<T> >::n]);

  /// gradU from boundary profile
  T computeBoundaryProfile(T distance, T tau_w, T rho, T dp_dx);

  /// compute fneq using the velocity gradient (mostly FD) on lattice directions
  std::vector<T> computeFneqStraightBoundary(const int input[], T distance,
      T tau_w, T rho, std::vector<T> u,
      T dp_dx, std::vector<T> normal,
      std::vector<T> e_x_loc);

  /// compute fneq using the velocity gradient (FD)
  std::vector<T> computeFneqCurvedBoundary(const int input[], T distance,
      T tau_w, T rho, std::vector<T> u,
      T dp_dx, std::vector<T> normal,
      std::vector<T> physR);

  /// compute a approximated normal vector and normal distance of a boundary node
  std::vector<T> computeNormalAndDist(const int index[], std::vector<T> physR);

  /// compute Density of boundary cells for straight walls
  T computeRhoStraightBoundary(CellView<T, DESCRIPTOR>& cell, std::vector<T> normal,
                               T u_norm=0);

  /// compute Density of boundary cells for curved walls
  T computeRhoCurvedBoundary(const int index[], std::vector<T> normal);

public:
  BlockWallFunctionF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                       BlockLatticeStructure3D<T, DESCRIPTOR>& bLattice,
                       BlockGeometryStructure3D<T>& bGeometry,
                       IndicatorF3D<T>& wallShape,
                       LBconverter<T> const& converter,
                       UWallProfile pr_type, fNeqCalcType c_type,
                       T rel_search_size, T abs_search_size)
    : BlockLatticeF3D<T, DESCRIPTOR>(bLattice, DESCRIPTOR<T>::q),
      _sLattice(sLattice),
      _bGeometry(bGeometry),
      _wallShape(wallShape),
      _converter(converter),
      profile_type(pr_type),
      calc_type(c_type),
      rel_interval_size(rel_search_size),
      abs_interval_size(abs_search_size),
      _velocity(bLattice, converter),
      _pressure(bLattice, converter)
  {
    this->getName() = "WallFunctionF3D";
  }

  /// define Populations
  bool operator() (T output[], const int input[]);
};

} // end namespace olb

#endif
