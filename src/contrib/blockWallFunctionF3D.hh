/*  Copyright (C) 2015 Patrick Nathen, Sebastian Schmidt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef BLOCK_WALL_FUNCTION_F_3D_HH
#define BLOCK_WALL_FUNCTION_F_3D_HH

#include <vector>
#include <iostream>

#include "core/blockLatticeStructure3D.h"
#include "core/cell.h"
#include "dynamics/latticeDescriptors.h"
//#include "boundaryDescriptors3D.h"
#include "dynamics/lbHelpers.h"
#include "dynamics/mrtHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"
#include "core/util.h"
#include "core/blockLattice3D.h"
#include "core/finiteDifference.h"
#include "utilities/vectorHelpers.h"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/superLatticeLocalF3D.h"
#include "blockWallFunctionF3D.h"
#include "functors/analytical/interpolationF3D.h"
#include "utilities/benchmarkUtil.h"

namespace olb {

template <typename T, typename S>
class Musker : public AnalyticalF1D<T,S> {
private:
  T _nu;
  T _y;
  T _rho;
public:
  Musker(T nu, T y, T rho);
  bool operator() (T output[], const S tau_w[]);
};

template <typename T, typename S>
Musker<T,S>::Musker(T nu, T y, T rho) : AnalyticalF1D<T,S>(1), _nu(nu), _y(y),_rho(rho)
{
  this->getName() = "Musker";
}

template <typename T, typename S>
bool Musker<T,S>::operator()(T output[], const S tau_w[])
{
  T y_plus = _y*sqrt(tau_w[0]/_rho)/_nu;

  T a = 5.424;
  T b = 0.119760479041916168;
  T c = 0.488023952095808383;
  T d = 0.434;
  T e = 3.50727901936264842;

  output[0] = sqrt(tau_w[0]/_rho)*(a*atan(b*y_plus - c) +
                                   d*log(pow(y_plus+10.6, 9.6)/pow(pow(y_plus, 2) - 8.15*y_plus + 86, 2)) - e);

  return true;
}

template <typename T, typename S>
class tble_duplus_dyplus : public AnalyticalF1D<T,S> {
private:
  T _F_plus;
  T _kappa;
  T _A_plus;
  int _sign1;
  int _sign2;
public:
  tble_duplus_dyplus(T F_plus, T kappa, T A_plus, int sign1, int sign2);
  bool operator() (T output[], const S y_plus[]);
};

template <typename T, typename S>
tble_duplus_dyplus<T,S>::tble_duplus_dyplus(T F_plus, T kappa, T A_plus, int sign1, int sign2) : AnalyticalF1D<T,S>(1), _F_plus(F_plus), _kappa(kappa),
  _A_plus(A_plus), _sign1(sign1), _sign2(sign2)
{
  this->getName() = "tble_duplus_dyplus";
}

template <typename T, typename S>
bool tble_duplus_dyplus<T,S>::operator()(T output[], const S y_plus[])
{
  T V_Aplus = 1 - exp(-y_plus[0]/_A_plus);

  T Lm2plus = pow((_kappa * y_plus[0] * V_Aplus), 2);
  T a = _F_plus * y_plus[0] + _sign2;
  T Delta = 1 + 4 * _sign1 * Lm2plus * a;

  output[0] = 2*a/(1+sqrt(Delta));

  return true;
}



template <typename T, typename S>
class tble_u : public AnalyticalF1D<T,S> {
private:
  T _y;
  T _rho;
  T _nu;
  T _dp_dx;
public:
  tble_u(T y, T rho, T nu, T dp_dx);
  bool operator() (T output[], const S tau_w[]);
};

template <typename T, typename S>
tble_u<T,S>::tble_u(T y, T rho, T nu, T dp_dx) : AnalyticalF1D<T,S>(1), _y(y), _rho(rho),
  _nu(nu), _dp_dx(dp_dx)
{
  this->getName() = "tble_u";
}

template <typename T, typename S>
bool tble_u<T,S>::operator()(T output[], const S tau_w[])
{
  int n = 16;
  T u_tau = sqrt(fabs(tau_w[0])/_rho);
  T y_plus = _y * u_tau/_nu;
  T F_plus = _dp_dx * _nu/(pow(u_tau, 3)*_rho);

  T kappa = 0.384;
  T A_plus = 26.0;

  bool sign_change = false;
  int sign1 = 1;
  int sign2 = util::sign<T>(tau_w[0]);
  if (sign2==1 and F_plus<0 and (F_plus*y_plus+sign2)<0) {
    sign1 = -1;
    sign_change = true;
  } else if (sign2==-1 and F_plus>0 and (F_plus*y_plus+sign2)>0) {
    sign_change = true;
  } else if (sign2==-1 and F_plus>0 and (F_plus*y_plus+sign2)<0) {
    sign1 = -1;
  } else if (sign2==-1 and F_plus<0 and (F_plus*y_plus+sign2)<0) {
    sign1 = -1;
  } else if (F_plus==0) {
    sign1 = sign2;
  }


  tble_duplus_dyplus<T,T>tble_duplus_dyplus_(F_plus, kappa, A_plus, sign1, sign2);

  // sign1 changes at y_plus = -sign2/F_plus
  if (sign_change) {
    // 2 integrations
    T y_signchange = -sign2/F_plus;
    // distribute points equally over both integrations
    int n1 = n*y_signchange/y_plus;
//TODO checK!
    TrapezRuleInt1D<T> integral1(tble_duplus_dyplus_);
    T result = integral1.integrate(y_signchange, y_plus, (n-n1)-1);

    tble_duplus_dyplus<T,T>tble_duplus_dyplus_2(F_plus, kappa, A_plus, sign2, sign2);
    TrapezRuleInt1D<T> integral2(tble_duplus_dyplus_2);
    result += integral2.integrate(0, y_signchange, n1-1);
    output[0] = u_tau*result;
  }
  TrapezRuleInt1D<T> integral(tble_duplus_dyplus_);
  output[0] = u_tau * integral.integrate(0, y_plus, n-1);

  return true;
}






//template <typename T>
//struct tble_u_params {
//  T y, rho, nu, dp_dx;
//};
//
//
//// simplified turbulent boundary layer equation
//template <typename T>
//T tble_u(T tau_w, void* params)
//{
//  int n = 16;
//  struct tble_u_params<T>* p = (struct tble_u_params<T>*) params;
//  T u_tau = sqrt(fabs(tau_w)/p->rho);
//  T y_plus = p->y * u_tau/p->nu;
//  T F_plus = p->dp_dx * p->nu/(pow(u_tau, 3)*p->rho);
//
//  T kappa = 0.384;
//  T A_plus = 26.0;
//
//  bool sign_change = false;
//  int sign1 = 1;
//  int sign2 = util::sign<T>(tau_w);
//  if (sign2==1 and F_plus<0 and (F_plus*y_plus+sign2)<0) {
//    sign1 = -1;
//    sign_change = true;
//  } else if (sign2==-1 and F_plus>0 and (F_plus*y_plus+sign2)>0) {
//    sign_change = true;
//  } else if (sign2==-1 and F_plus>0 and (F_plus*y_plus+sign2)<0) {
//    sign1 = -1;
//  } else if (sign2==-1 and F_plus<0 and (F_plus*y_plus+sign2)<0) {
//    sign1 = -1;
//  } else if (F_plus==0) {
//    sign1 = sign2;
//  }
//
//
//  tble_duplus_dyplus<T,T>tble_duplus_dyplus_(F_plus, kappa, A_plus, sign1, sign2);
//
//  // sign1 changes at y_plus = -sign2/F_plus
//  if (sign_change) {
//    // 2 integrations
//    T y_signchange = -sign2/F_plus;
//    // distribute points equally over both integrations
//    int n1 = n*y_signchange/y_plus;
////TODO checK!
//    TrapezRuleInt1D<T> integral1(tble_duplus_dyplus_);
//    T result = integral1.integrate(y_signchange, y_plus, (n-n1)-1);
//
//    tble_duplus_dyplus<T,T>tble_duplus_dyplus_2(F_plus, kappa, A_plus, sign2, sign2);
//    TrapezRuleInt1D<T> integral2(tble_duplus_dyplus_2);
//    result += integral2.integrate(0, y_signchange, n1-1);
//    return u_tau*result;
//  }
//  TrapezRuleInt1D<T> integral(tble_duplus_dyplus_);
//  return u_tau * integral.integrate(0, y_plus, n-1);
//}


/// compute f^neq from grad u
template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> fromGradUToFneq(std::vector<T> gradU, T rho, T omega)
{
  std::vector<T> gradU_mid(6, T());
  gradU_mid[0] = gradU[0];
  gradU_mid[1] = (gradU[1] + gradU[3])/2;
  gradU_mid[2] = (gradU[2] + gradU[6])/2;
  gradU_mid[3] = gradU[4];
  gradU_mid[4] = (gradU[5] + gradU[7])/2;
  gradU_mid[5] = gradU[8];
  std::vector<T> fNeq(DESCRIPTOR<T>::q);
  for (int iQ=0; iQ<DESCRIPTOR<T>::q; ++iQ) {
    fNeq[iQ] = 0;
    int i = 0;
    // Iterate only over superior triangle + diagonal, and add
    // the elements under the diagonal by symmetry
    for (int iAlpha=0; iAlpha<3; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta<3; ++iBeta) {
        T toAdd = DESCRIPTOR<T>::c[iQ][iAlpha]*DESCRIPTOR<T>::c[iQ][iBeta];
        if (iAlpha==iBeta) {
          toAdd -= 1./DESCRIPTOR<T>::invCs2();
        } else {
          // multiply off-diagonal elements by 2 (Q tensor is symmetric)
          toAdd *= (T)2;
          toAdd *= gradU_mid[i];
          i++;
          fNeq[iQ] += toAdd;
        }
      }
      fNeq[iQ] *= -DESCRIPTOR<T>::t[iQ] * DESCRIPTOR<T>::invCs2() * rho / omega;
    }
  }
  return fNeq;
}


template <typename T, template <typename U> class DESCRIPTOR>
void BlockWallFunctionF3D<T, DESCRIPTOR>::computePiStraightBoundary(
  CellView<T,DESCRIPTOR> const& cell, T rho, const T u[DESCRIPTOR<T>::d],
  T Pi[util::TensorVal<DESCRIPTOR<T> >::n], std::vector<T> normal)
{
  // much like boundaryHelpers::computePi, but computes Pi^neq
  const T uSqr = util::normSqr<T, DESCRIPTOR<T>::d>(u);

  std::vector<int> onWallIndices;
  std::vector<int> normalIndices;
  for (int iQ=0; iQ<DESCRIPTOR<T>::q; ++iQ) {
    std::vector<T> xi_i(3);
    for (int iD=0; iD<3; ++iD) {
      xi_i[iD] = DESCRIPTOR<T>::c[iQ][iD];
    }
    T scalProdTMP = util::scalarProduct<T>(xi_i, normal);
    if (scalProdTMP==0) {
      onWallIndices.push_back(iQ);
    } else if (scalProdTMP>0) {
      normalIndices.push_back(iQ);
    }
  }

  T fNeq[DESCRIPTOR<T>::q];
  for (unsigned fIndex=0; fIndex<onWallIndices.size(); ++fIndex) {
    int iPop = onWallIndices[fIndex];
    fNeq[iPop] = cell[iPop] - lbHelpers<T,DESCRIPTOR>::equilibrium(iPop, rho, u, uSqr);
  }
  for (unsigned fIndex=0; fIndex<normalIndices.size(); ++fIndex) {
    int iPop = normalIndices[fIndex];
    if (iPop == 0) {
      fNeq[iPop] = T();
    } else {
      fNeq[iPop] = cell[iPop] - lbHelpers<T,DESCRIPTOR>::equilibrium(iPop, rho, u, uSqr);
    }
  }

  int iPi = 0;
  for (int iAlpha=0; iAlpha<DESCRIPTOR<T>::d; ++iAlpha) {
    for (int iBeta=iAlpha; iBeta<DESCRIPTOR<T>::d; ++iBeta) {
      Pi[iPi] = T();
      for (unsigned fIndex=0; fIndex<onWallIndices.size(); ++fIndex) {
        const int iPop = onWallIndices[fIndex];
        Pi[iPi] +=
          DESCRIPTOR<T>::c(iPop)[iAlpha]*DESCRIPTOR<T>::c(iPop)[iBeta]*fNeq[iPop];
        if (iAlpha == iBeta) {
          Pi[iPi] -= fNeq[iPop]/DESCRIPTOR<T>::invCs2();
        }
      }
      for (unsigned fIndex=0; fIndex<normalIndices.size(); ++fIndex) {
        const int iPop = normalIndices[fIndex];
        Pi[iPi] += (T)2 * DESCRIPTOR<T>::c(iPop)[iAlpha]*DESCRIPTOR<T>::c(iPop)[iBeta]*
                   fNeq[iPop];
        if (iAlpha == iBeta) {
          Pi[iPi] -= fNeq[iPop]/DESCRIPTOR<T>::invCs2();
        }
      }
      ++iPi;
    }
  }
}


// extrapolate Pi^neq from bulk
template<typename T, template<typename U> class DESCRIPTOR>
void BlockWallFunctionF3D<T, DESCRIPTOR>::computePiCurvedBoundary(
  const int index[], std::vector<T>normal,
  T Pi[util::TensorVal<DESCRIPTOR<T> >::n])
{
  // TODO: make sure not to run out of the blockLattice
  // for now, use at least 3 cells overlap
  int max_index = util::get_nearest_link<T, DESCRIPTOR>(normal);
  int dir_index = util::opposite<DESCRIPTOR<T> >(max_index);
  std::vector<T> dir(DESCRIPTOR<T>::c[dir_index], DESCRIPTOR<T>::c[dir_index]+3);
  int n = util::TensorVal<DESCRIPTOR<T> >::n;
  T* Pi0 = new T[n];
  T* Pi1 = new T[n];
  T* Pi2 = new T[n];
  T rho = 0;
  T* u = new T[DESCRIPTOR<T>::d];
  CellView<T, DESCRIPTOR>& cell0 = this->_blockLattice.get(index[0]+3*(int)dir[0],
                              index[1]+3*(int)dir[1],
                              index[2]+3*(int)dir[2]);
  CellView<T, DESCRIPTOR>& cell1 = this->_blockLattice.get(index[0]+2*(int)dir[0],
                              index[1]+2*(int)dir[1],
                              index[2]+2*(int)dir[2]);
  CellView<T, DESCRIPTOR>& cell2 = this->_blockLattice.get(index[0]+1*(int)dir[0],
                              index[1]+1*(int)dir[1],
                              index[2]+1*(int)dir[2]);
  lbHelpers<T, DESCRIPTOR>::computeAllMomenta(cell0, rho, u, Pi0);
  lbHelpers<T, DESCRIPTOR>::computeAllMomenta(cell1, rho, u, Pi1);
  lbHelpers<T, DESCRIPTOR>::computeAllMomenta(cell2, rho, u, Pi2);

  for (int iN=0; iN<n; ++iN) {
    //TODO Implemetation
    T boundaryGradient = fd::boundaryGradient(Pi2[iN], Pi1[iN], Pi0[iN]);
    if (boundaryGradient > 0){
      Pi[iN] = boundaryGradient - Pi2[iN];
    } else {
      Pi[iN] = Pi2[iN] - boundaryGradient;
    }
//    Pi[iN] = fd::extrapolate2_equalSpacing<T>(Pi0[iN], Pi1[iN], Pi2[iN]);
  }

  delete[] Pi0;
  delete[] Pi1;
  delete[] Pi2;
  delete[] u;
}


template <typename T, template <typename U> class DESCRIPTOR>
T BlockWallFunctionF3D<T, DESCRIPTOR>::computeBoundaryProfile(
  T distance, T tau_w, T rho, T dp_dx)
{
  T gradU = 0;
  T tau_w_[1];
  tau_w_[0]=tau_w;
  T dx = _converter.getLatticeL();
  if (profile_type == UWallProfile::MUSKER) {
    T y_ = distance - 0.001*dx;
    T nu = _converter.getCharNu();
    Musker<T,T> musker(y_, rho, nu);
    T val1[1];
    musker(val1,tau_w_);
    y_ = distance + 0.001*dx;
    Musker<T,T> musker2(y_, rho, nu);
    T val2[1];
    musker2(val2,tau_w_);
    gradU = (val2-val1)/(.002*dx);
  } else if (profile_type == UWallProfile::TBLE) {
    T nu = _converter.getCharNu();
    T u_tau = sqrt(fabs(tau_w)/rho);
    T y_plus = distance*u_tau/nu;
    T F_plus = dp_dx*nu/(pow(u_tau, 3)*rho);

    T kappa = 0.384;
    T A_plus = 26.0;

    int sign1 = 1;
    int sign2 = util::sign<T>(tau_w);
    if (sign2==1 and F_plus<0 and (F_plus*y_plus+sign2)<0) {
      sign1 = -1;
    } else if (sign2==-1 and F_plus>0 and (F_plus*y_plus+sign2)<0) {
      sign1 = -1;
    } else if (sign2==-1 and F_plus<0 and (F_plus*y_plus+sign2)<0) {
      sign1 = -1;
    } else if (F_plus==0) {
      sign1 = sign2;
    }
    tble_duplus_dyplus<T,T>tble_duplus_dyplus_(F_plus, kappa, A_plus, sign1, sign2);
    T output[1];
    T tmpTau_w_[1];
    tmpTau_w_[0]=tau_w;
    tble_duplus_dyplus_(output, tmpTau_w_);
    gradU = u_tau*output[0];

  }
  return gradU;
}


// compute f^neq using grad(u)
template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockWallFunctionF3D<T, DESCRIPTOR>::computeFneqStraightBoundary(
  const int input[], T distance, T tau_w, T rho, std::vector<T> u,
  T dp_dx, std::vector<T> normal, std::vector<T> e_x_loc)
{
  std::vector<T> gradU(9, T());
  T dx = _converter.getLatticeL();

  T u_xminus[3], u_yminus[3], u_zminus[3];
  this->_blockLattice.get(input[0]-1, input[1], input[2]).computeU(u_xminus);
  this->_blockLattice.get(input[0], input[1]-1, input[2]).computeU(u_yminus);
  this->_blockLattice.get(input[0], input[1], input[2]-1).computeU(u_zminus);

  // NOTE: boundary gradient points in local x-direction
  // => project onto global directions
  T boundary_gradient = computeBoundaryProfile(distance, tau_w, rho, dp_dx);

  // calculate gradU_xx
  if (not(util::aligned_to_x<T>(normal))) {
    gradU[0] = (u[0]-u_xminus[0])/dx;
  }

  // calculate gradU_xy
  if (util::aligned_to_z<T>(normal)) {
    gradU[1] = (u[1]-u_xminus[1])/dx;
  } else if (util::aligned_to_x<T>(normal)) {
    gradU[1] = -normal[0]*e_x_loc[1]*boundary_gradient;
  } else { // normal aligned to y
    gradU[1] = 0;
  }

  // calculate gradU_xz
  if (util::aligned_to_y<T>(normal)) {
    gradU[2] = (u[2]-u_xminus[2])/dx;
  } else if (util::aligned_to_x<T>(normal)) {
    gradU[2] = -normal[0]*e_x_loc[2]*boundary_gradient;
  } else { // normal aligned to z
    gradU[2] = 0;
  }

  // calculate gradU_yx
  if (util::aligned_to_z<T>(normal)) {
    gradU[3] = (u[0]-u_yminus[0])/dx;
  } else if (util::aligned_to_x<T>(normal)) {
    gradU[1] = 0;
  } else { // normal aligned to y
    gradU[1] = -normal[1]*e_x_loc[0]*boundary_gradient;
  }

  // calculate gradU_yy
  if (not(util::aligned_to_y<T>(normal))) {
    gradU[4] = (u[1] - u_yminus[1])/dx;
  }

  // calculate gradU_yz
  if (util::aligned_to_x<T>(normal)) {
    gradU[5]= (u[2] - u_yminus[2])/dx;
  } else if (util::aligned_to_y<T>(normal)) {
    gradU[5] = -normal[1]*e_x_loc[2]*boundary_gradient;
  } else { // normal aligned to z
    gradU[5] = 0;
  }

  // calculate gradU_zx
  if (util::aligned_to_y<T>(normal)) {
    gradU[6]= (u[0] - u_zminus[0])/dx;
  } else if (util::aligned_to_z<T>(normal)) {
    gradU[6] = -normal[2]*e_x_loc[0]*boundary_gradient;
  } else { // normal aligned to x
    gradU[6] = 0;
  }

  // calculate gradU_zy
  if (util::aligned_to_x<T>(normal)) {
    gradU[7]= (u[1] - u_zminus[1])/dx;
  } else if (util::aligned_to_z<T>(normal)) {
    gradU[7] = -normal[2]*e_x_loc[1]*boundary_gradient;
  } else { // normal aligned to y
    gradU[7] = 0;
  }

  // calculate gradU_zz
  if (not(util::aligned_to_z<T>(normal))) {
    gradU[8] = (u[2]-u_zminus[2])/dx;
  }

  return fromGradUToFneq<T, DESCRIPTOR>(gradU, rho, _blockLattice.getDynamics(input[0], input[1], input[2])->getOmega());
}


template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockWallFunctionF3D<T, DESCRIPTOR>::computeFneqCurvedBoundary(
  const int input[], T distance, T tau_w, T rho, std::vector<T> u,
  T dp_dx, std::vector<T> normal, std::vector<T> physR)
{
  std::vector<T> gradU(9, T());
  T dx = _converter.getLatticeL();

  // calculate gradU_xx, gradU_xy and gradU_xz
  int x_dir = (normal[0] != 0 ? util::sign<T>(normal[0]) : 1);
  T u_0[3];
  this->_blockLattice.get(input[0]-x_dir, input[1], input[2]).computeU(u_0);
  // check if the nex cell in x is a wall
  if (dynamic_cast<NoDynamics<T, DESCRIPTOR>*>
      (this->_blockLattice.getDynamics(input[0]+x_dir, input[1], input[2]))) {
    // get gradient from Lagrange interpolation
    std::vector<T> u_wall(3, T());
    T dist;
    _wallShape.distance(dist, physR, std::vector<T> {(T)x_dir, 0, 0});
    // x_0 at -dx, cell at 0, and wall at distance
    T l20 = -dist/(dx*(dx+dist));
    T l21 = (dx-dist)/(-dx*dist);
    T l22 = dx/(dist*(dist+dx));
    for (int i=0; i<3; ++i) {
      // don't forget to take the direction into account
      gradU[i] = x_dir*(l20*u_0[i] + l21*u[i] + l22*u_wall[i]);
    }
  } else {
    // simple finite difference
    for (int i=0; i<3; ++i) {
      // don't forget to take the direction into account
      gradU[i] = x_dir*(u[i]-u_0[i])/dx;
    }
  }

  // calculate gradU_yy and gradU_yz
  int y_dir = (normal[1] != 0 ? util::sign<T>(normal[1]) : 1);
  this->_blockLattice.get(input[0], input[1]-y_dir, input[2]).computeU(u_0);
  if (dynamic_cast<NoDynamics<T, DESCRIPTOR>*>
      (this->_blockLattice.getDynamics(input[0], input[1]+y_dir, input[2]))) {
    // get gradient from Lagrange interpolation
    std::vector<T> u_wall(3, T());
    T dist;
    _wallShape.distance(dist, physR, std::vector<T> {0, (T)y_dir, 0});
    // x_0 at -dx, cell at 0, and wall at distance
    T l20 = -dist/(dx*(dx+dist));
    T l21 = (dx-dist)/(-dx*dist);
    T l22 = dx/(dist*(dist+dx));
    for (int i=0; i<3; ++i) {
      // don't forget to take the direction into account
      gradU[i+3] = y_dir*(l20*u_0[i] + l21*u[i] + l22*u_wall[i]);
    }
  } else {
    // simple finite difference
    for (int i=0; i<3; ++i) {
      // don't forget to take the direction into account
      gradU[i+3] = y_dir*(u[i]-u_0[i])/dx;
    }
  }

  // calculate gradU_zz
  int z_dir = (normal[2] != 0 ? util::sign<T>(normal[2]) : 1);
  this->_blockLattice.get(input[0], input[1], input[2]-z_dir).computeU(u_0);
  if (dynamic_cast<NoDynamics<T, DESCRIPTOR>*>
      (this->_blockLattice.getDynamics(input[0], input[1], input[2]+z_dir))) {
    // get gradient from Lagrange interpolation
    std::vector<T> u_wall(3, T());
    T dist;
    _wallShape.distance(dist, physR, std::vector<T> {0, 0, (T)z_dir});
    // x_0 at -dx, cell at 0, and wall at distance
    T l20 = -dist/(dx*(dx+dist));
    T l21 = (dx-dist)/(-dx*dist);
    T l22 = dx/(dist*(dist+dx));
    for (int i=0; i<3; ++i) {
      // don't forget to take the direction into account
      gradU[i+6] = z_dir*(l20*u_0[i] + l21*u[i] + l22*u_wall[i]);
    }
  } else {
    for (int i=0; i<3; ++i) {
      // don't forget to take the direction into account
      gradU[i+6] = z_dir*(u[i]-u_0[i])/dx;
    }
  }

  T omega = this->_blockLattice.getDynamics(input[0], input[1], input[2])->getOmega();

  return fromGradUToFneq<T, DESCRIPTOR>(gradU, rho, omega);
}


template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockWallFunctionF3D<T, DESCRIPTOR>::computeNormalAndDist(
  const int index[], std::vector<T> physR)
{
  std::vector<T> normal_and_dist;
  std::vector<T> normal(3);
  T distance;
  for (int iQ=1; iQ<DESCRIPTOR<T>::q; ++iQ) {
    std::vector<T> c_i(DESCRIPTOR<T>::c[iQ], DESCRIPTOR<T>::c[iQ]+3);
    int iX = index[0] + int(c_i[0]);
    int iY = index[1] + int(c_i[1]);
    int iZ = index[2] + int(c_i[2]);
    this->_blockLattice.get(iX, iY, iZ);
    if (dynamic_cast<NoDynamics<T, DESCRIPTOR>*>
        (this->_blockLattice.getDynamics(iX, iY, iZ))) {
      if (_wallShape.distance(distance, physR, c_i)) {
        if (distance==0) {
          normal = {1.0, 0.0, 0.0};
          break;
        }
        normal[0] = normal[0] + c_i[0]*(1/(distance*util::norm<T>(c_i)));
        normal[1] = normal[1] + c_i[1]*(1/(distance*util::norm<T>(c_i)));
        normal[2] = normal[2] + c_i[2]*(1/(distance*util::norm<T>(c_i)));
      }
    }
  }
  if (util::nearZero<T>(normal[0])) {
    normal[0] = 0;
  }
  if (util::nearZero<T>(normal[1])) {
    normal[1] = 0;
  }
  if (util::nearZero<T>(normal[2])) {
    normal[2] = 0;
  }

  if (normal[0]==0. && normal[1]==0. && normal[2]==0.){
  } else {

    normal_and_dist = util::normalize<T>(normal);
  }
  normal_and_dist.resize(4);
  if (_wallShape.distance(distance, physR, normal)) {
    normal_and_dist[3] = distance;
  } else {
//    std::cerr << "WARNING AT " << physR[0] << ", " << physR[1] << ", " << physR[2] << std::endl;
//    std::cerr << "Normal found, but does not seem to point to the walls" << std::endl;
//    std::cerr << "Using 0.5*dx as distance" << std::endl;
    normal_and_dist[3] = 0.5*_converter.getLatticeL();
  }
  return normal_and_dist;
}


// local computation of rho assuming bounce back
template <typename T, template <typename U> class DESCRIPTOR>
T BlockWallFunctionF3D<T, DESCRIPTOR>::computeRhoStraightBoundary(
  CellView<T, DESCRIPTOR>& cell, std::vector<T> normal, T u_norm)
{
  T rho_0 = 0.0;
  T rho_plus = 0.0;
  for (int iQ=0; iQ<DESCRIPTOR<T>::q; ++iQ) {
    std::vector<T> c_i(DESCRIPTOR<T>::c[iQ], DESCRIPTOR<T>::c[iQ]+3);
    T scalProdTMP = util::scalarProduct<T>(c_i, normal);
    if (scalProdTMP==0) {
      rho_0 += cell[iQ];
    } else if (scalProdTMP>0) {
      rho_plus += cell[iQ];
    }
  }
  T rho_bc = (1 + 2*rho_plus+rho_0)/(1+u_norm);
  return rho_bc;
}


// extrapolation of rho from bulk
template <typename T, template <typename U> class DESCRIPTOR>
T BlockWallFunctionF3D<T, DESCRIPTOR>::computeRhoCurvedBoundary(
  const int index[], std::vector<T> normal)
{
  // find the link with the smallest angle to the normal
  int max_index = util::get_nearest_link<T, DESCRIPTOR>(normal);
  // extrapolation with lagrange polynom (3 points and equally spaced)
  int dir_index = util::opposite<DESCRIPTOR<T> >(max_index);
  std::vector<T> dir(DESCRIPTOR<T>::c[dir_index], DESCRIPTOR<T>::c[dir_index]+3);
  T rho_0 = this->_blockLattice.get(index[0]+3*(int)dir[0], index[1]+3*(int)dir[1],
                                    index[2]+3*(int)dir[2]).computeRho();
  T rho_1 = this->_blockLattice.get(index[0]+2*(int)dir[0], index[1]+2*(int)dir[1],
                                    index[2]+2*(int)dir[2]).computeRho();
  T rho_2 = this->_blockLattice.get(index[0]+1*(int)dir[0], index[1]+1*(int)dir[1],
                                    index[2]+1*(int)dir[2]).computeRho();
  //TODO Implementation
//  return fd::extrapolate2_equalSpacing<T>(rho_0, rho_1, rho_2);
  T boundaryGradient = fd::boundaryGradient(rho_2, rho_1, rho_0);
  if (boundaryGradient > 0){
    return boundaryGradient - rho_2;
  } else {
    return rho_2 - boundaryGradient;
  }
}


template <typename T, template <typename U> class DESCRIPTOR>
bool BlockWallFunctionF3D<T, DESCRIPTOR>::operator() (T output[], const int input[])
{
  std::vector<T> u1_vec(3);
  T u1 = 0;
  T tau_w[1];
  tau_w[0] =0.;
  T dp_dx = 0;

  CellView<T, DESCRIPTOR> cell = this->_blockLattice.get(input[0], input[1], input[2]);
  //std::vector<T> physCoord = this->_bGeometry.getPhysR(input[0], input[1], input[2]);
  T physCoord_tmp[3];
  this->_bGeometry.getPhysR(physCoord_tmp, input[0], input[1], input[2]);

  std::vector<T> physCoord(3,T());
  physCoord[0]=physCoord_tmp[0];
  physCoord[1]=physCoord_tmp[1];
  physCoord[2]=physCoord_tmp[2];

  T* tau_w_guess = cell.getExternal(DESCRIPTOR<T>::ExternalField::tauWIsAt);

  // NOTE: STEP 0
  // get normal vector - has to be normalized
  std::vector<T> normal = computeNormalAndDist(input, physCoord);
  T y_1 = normal.back();
  normal.pop_back();
  T y_2 = y_1 + _converter.getLatticeL();
  // NOTE: STEP 2
  // compute u_2
  std::vector<T> u2_vec;
  if (util::aligned_to_grid<T>(normal)) {
    int inp_[3];
    T tmpOutput[3];
    inp_[0]=input[0]-(int)normal[0];
    inp_[1]=input[1]-(int)normal[1];
    inp_[2]=input[2]-(int)normal[2];
    _velocity(tmpOutput,inp_);
    u2_vec.push_back(tmpOutput[0]);
    u2_vec.push_back(tmpOutput[1]);
    u2_vec.push_back(tmpOutput[2]);
    //  u2_vec = _velocity(input - std::vector<int>{(int)normal[0], (int)normal[1],
    //   (int)normal[2]});
  } else {
    SuperLatticePhysVelocity3D<T, DESCRIPTOR> superL_velocity(_sLattice, _converter);
    AnalyticalFfromSuperF3D<T, T> interpolate_u(superL_velocity, false);
    T tmpOutput[3];
    T physcoordcorr[3];
    physcoordcorr[0]=physCoord[0] - normal[0]*_converter.getLatticeL();
    physcoordcorr[1]=physCoord[0] - normal[1]*_converter.getLatticeL();
    physcoordcorr[2]=physCoord[0] - normal[2]*_converter.getLatticeL();
    interpolate_u(tmpOutput,physcoordcorr);

    u2_vec.push_back(tmpOutput[0]);
    u2_vec.push_back(tmpOutput[1]);
    u2_vec.push_back(tmpOutput[2]);
  }
  // NOTE: STEP 3
  // get local basis vector
  std::vector<T> e_x_loc(3);
  std::vector<T> u_2_vec_normal(3,T());

  u_2_vec_normal[0]=u2_vec[0]-util::scalarProduct<T>(u2_vec, normal)*normal[0];
  u_2_vec_normal[1]=u2_vec[1]-util::scalarProduct<T>(u2_vec, normal)*normal[1];
  u_2_vec_normal[2]=u2_vec[2]-util::scalarProduct<T>(u2_vec, normal)*normal[2];

  std::vector<T> tmp = (u_2_vec_normal);
  T tmp_norm = util::norm<T>(tmp);
  // NOTE: STEP 4
  // compute density
  T rho_bc = 0;
  if (util::aligned_to_grid<T>(normal)) {
    rho_bc = computeRhoStraightBoundary(cell, normal);
  } else {
    rho_bc = computeRhoCurvedBoundary(input, normal);
  }
  T rho_2;
  if (util::aligned_to_grid<T>(normal)) {
    rho_2 = this->_blockLattice.get(input[0]-(int)normal[0],
                                    input[1]-(int)normal[1],
                                    input[2]-(int)normal[2]).computeRho();
  } else {
    SuperLatticeDensity3D<T, DESCRIPTOR> density(this->_sLattice);
    AnalyticalFfromSuperF3D<T, T> interpolate_rho(density, false);
    std::vector<T>normalconv(3,T());
    T tmpOutput[3];
    T physcoordcorr[3];
    physcoordcorr[0]=physCoord[0]-normal[0]*_converter.getLatticeL();
    physcoordcorr[1]=physCoord[1]-normal[1]*_converter.getLatticeL();
    physcoordcorr[2]=physCoord[2]-normal[2]*_converter.getLatticeL();
    interpolate_rho(tmpOutput,physcoordcorr);
    rho_2 = _converter.physRho(tmpOutput[0]);
  }
  // NOTE: STEP 5
  // compute scalar u2
  T u2;
  if (tmp_norm != 0) {
    e_x_loc[0] = tmp[0]*(1/tmp_norm);
    e_x_loc[1] = tmp[1]*(1/tmp_norm);
    e_x_loc[2] = tmp[2]*(1/tmp_norm);
    u2 = util::scalarProduct<T>(u2_vec, e_x_loc);
  } else {
    e_x_loc = {1.0, 0.0, 0.0};
    u2 = 0;
  }

  // NOTE: STEP 6+7
  // calculate the search interval
  T halfsize = 0.5 * std::max<T>(abs_interval_size, *tau_w_guess*rel_interval_size);

  if (profile_type == UWallProfile::MUSKER) {
    // u1 can only be 0 if u2==0 for musker profile
    if (u2 == 0) {
      u1 = 0.0;
    } else {
      T nu = _converter.getCharNu();
      Musker<T,T> musker(y_2, rho_2, nu);
      Newton1D<T> newton(musker,u2);
      tau_w[0] = newton.solve(*tau_w_guess,false);

      // compute u1

      Musker<T,T> musker2(y_1, rho_bc, nu);
      T tmpu1[1];
      musker2(tmpu1,tau_w);
      u1 = tmpu1[0];

    }
  } else if (profile_type == UWallProfile::TBLE) {
    /*
    // pressure gradient
    if (util::aligned_to_x<T>(e_x_loc))
    {
      T rho_prev = this->_blockLattice.get(input[0]-1, input[1], input[2]).computeRho();
      T p_prev = _converter.physPressureFromRho(rho_prev);
      T p = _converter.physPressureFromRho(rho_bc);
      dp_dx = e_x_loc[0]*(p-p_prev)/_converter.getLatticeL();
    }
    else if (util::aligned_to_y<T>(e_x_loc))
    {
      T rho_prev = this->_blockLattice.get(input[0], input[1]-1, input[2]).computeRho();
      T p_prev = _converter.physPressureFromRho(rho_prev);
      T p = _converter.physPressureFromRho(rho_bc);
      dp_dx = e_x_loc[1]*(p-p_prev)/_converter.getLatticeL();
    }
    else if (util::aligned_to_z<T>(e_x_loc))
    {
      T rho_prev = this->_blockLattice.get(input[0], input[1], input[2]-1).computeRho();
      T p_prev = _converter.physPressureFromRho(rho_prev);
      T p = _converter.physPressureFromRho(rho_bc);
      dp_dx = e_x_loc[2]*(p-p_prev)/_converter.getLatticeL();
    }
    else
    {
      T dx = _converter.getLatticeL();
      T p = _converter.physPressureFromRho(rho_bc);
      if (input[0]<=1 and input[2]>1)
      {
        T rho_nextx = this->_blockLattice.get(input[0]+1, input[1], input[2]).computeRho();
        T p_nextx = _converter.physPressureFromRho(rho_nextx);
        T rho_prevz = this->_blockLattice.get(input[0], input[1], input[2]-1).computeRho();
        T p_prevz = _converter.physPressureFromRho(rho_prevz);
        T p_next = e_x_loc[0]*(p_nextx-p) + e_x_loc[2]*(p-p_prevz);
        dp_dx = (p_next-p)/dx;
      }
      else if (input[0]>1 and input[2]<=1)
      {
        T rho_prevx = this->_blockLattice.get(input[0]-1, input[1], input[2]).computeRho();
        T p_prevx = _converter.physPressureFromRho(rho_prevx);
        T rho_nextz = this->_blockLattice.get(input[0], input[1], input[2]+1).computeRho();
        T p_nextz = _converter.physPressureFromRho(rho_nextz);
        T p_next = e_x_loc[0]*(p-p_prevx) + e_x_loc[2]*(p_nextz-p);
        dp_dx = (p_next-p)/dx;
      }
      else if (input[0]<=1 and input[2]<=1)
      {
        T rho_nextx = this->_blockLattice.get(input[0]+1, input[1], input[2]).computeRho();
        T p_nextx = _converter.physPressureFromRho(rho_nextx);
        T rho_nextx2 = this->_blockLattice.get(input[0]+2, input[1], input[2]).computeRho();
        T p_nextx2 = _converter.physPressureFromRho(rho_nextx2);
        T rho_nextz = this->_blockLattice.get(input[0], input[1], input[2]+1).computeRho();
        T p_nextz = _converter.physPressureFromRho(rho_nextz);
        T rho_nextz2 = this->_blockLattice.get(input[0], input[1], input[2]+2).computeRho();
        T p_nextz2 = _converter.physPressureFromRho(rho_nextz2);
        T p_next = e_x_loc[0]*(-p_nextx2 + 4*p_nextx-3*p)/2 + e_x_loc[2]*(-p_nextz2 + 4*p_nextz-3*p)/2;
        dp_dx = (p_next-p)/dx;
      }
      else
      {
        T rho_prevx = this->_blockLattice.get(input[0]-1, input[1], input[2]).computeRho();
        T p_prevx = _converter.physPressureFromRho(rho_prevx);
        T rho_prevz = this->_blockLattice.get(input[0], input[1], input[2]-1).computeRho();
        T p_prevz = _converter.physPressureFromRho(rho_prevz);
        T p_next = e_x_loc[0]*(p-p_prevx) + e_x_loc[2]*(p-p_prevz);
        dp_dx = (p_next-p)/dx;
      }
    }

    if (util::nearZero<T>(dp_dx))
    {
      dp_dx = 0;
    }
    */
    dp_dx = 0;

    T nu = _converter.getCharNu();
    //struct tble_u_params<T> params = {y_2, rho_2, nu, dp_dx};
    tble_u<T,T> tble (y_2, rho_2, nu, dp_dx);



    Newton1D<T> newton(tble,u2);
    tau_w[0] = newton.solve(*tau_w_guess,false);

    // compute u1

    tble_u<T,T> tble2 (y_1, rho_bc, nu, dp_dx);
    T tmpu1[1];
    tble2(tmpu1,tau_w);
    u1 = tmpu1[0];
  }

  // save tau_w for next step
  cell.defineExternalField(DESCRIPTOR<T>::ExternalField::tauWIsAt,
                           DESCRIPTOR<T>::ExternalField::sizeOfTauW,
                           &tau_w[0]);

  // NOTE: STEP 7
  // compute velocity at boundary
  u1_vec[0] = e_x_loc[0] * u1;
  u1_vec[1] = e_x_loc[1] * u1;
  u1_vec[2] = e_x_loc[2] * u1;

  // TODO: check if force is existing
  int sizeOfForce = 3;//DESCRIPTOR<T>::ExternalField::sizeOfForce;
  std::vector<T> g(sizeOfForce, T());
  for (int iD=0; iD<sizeOfForce; ++iD) {
    g[iD] = *(cell.getExternal(DESCRIPTOR<T>::ExternalField::forceBeginsAt+iD));
  }


  g[0]*=0.5;
  g[1]*=0.5;
  g[2]*=0.5;
  std::vector<T> u_bc(3, T());
  u_bc[0]=u1_vec[0] - g[0];
  u_bc[0]=u1_vec[1] - g[1];
  u_bc[0]=u1_vec[2] - g[2];

  // NOTE: STEP 8+9
  // compute Populations
  std::vector<T> f(DESCRIPTOR<T>::q, T());
  std::vector<T> fNeq;

  // stress tensor
  T* u_bc_array = u_bc.data();
  if (calc_type == fNeqCalcType::PI) {
    // NOTE: compute Pi
    T* P_1 = new T[util::TensorVal<DESCRIPTOR<T> >::n];
    if (util::aligned_to_grid<T>(normal)) {
      computePiStraightBoundary(cell, rho_bc, u_bc_array, P_1, normal);
    } else {
      computePiCurvedBoundary(input, normal, P_1);
    }
    for (int iQ=0; iQ<DESCRIPTOR<T>::q; ++iQ) {
      T fNeq_i = firstOrderLbHelpers<T, DESCRIPTOR>::fromPiToFneq(iQ, P_1);
      T feq_i = lbHelpers<T, DESCRIPTOR>::equilibrium(iQ, rho_bc, u_bc_array,
                util::normSqr<T, 3>(u_bc_array));
      f[iQ] = feq_i + fNeq_i;
    }
    delete[] P_1;
  } else if (calc_type == fNeqCalcType::GRADU) {
    if (util::aligned_to_grid<T>(normal)) {
      fNeq = computeFneqStraightBoundary(input, y_1, tau_w[0], rho_bc, u_bc,
                                         dp_dx, normal, e_x_loc);
    } else {
      fNeq = computeFneqCurvedBoundary(input, y_1, tau_w[0], rho_bc, u_bc,
                                       dp_dx, normal, physCoord);
    }
    for (int iQ=0; iQ<DESCRIPTOR<T>::q; ++iQ) {
      T fNeq_i = fNeq[iQ];
      T feq_i = lbHelpers<T, DESCRIPTOR>::equilibrium(iQ, rho_bc, u_bc_array,
                util::normSqr<T, 3>(u_bc_array));
      f[iQ] = feq_i + fNeq_i;
    }
  }
  for (int i=0;i<f.size();i++){
    output[i] = f[i];
  }
  return true;
}

} // end namespace olb

#endif
