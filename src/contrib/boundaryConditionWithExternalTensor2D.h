/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file A helper for initialising 2D boundaries -- header file.  */

#ifndef BOUNDARY_CONDITION_WITH_EXTERNAL_TENSOR_2D_H
#define BOUNDARY_CONDITION_WITH_EXTERNAL_TENSOR_2D_H

#include "core/blockLatticeStructure2D.h"
#include "boundary/momentaOnBoundaries2D.h"
#include "boundary/boundaryPostProcessors2D.h"
#include "dynamics/dynamics.h"
#include "boundary/boundaryCondition2D.h"

namespace olb {

////////// Factory functions //////////////////////////////////////////////////

template<typename T, template<typename U> class Lattice, typename MixinDynamics>
OnLatticeBoundaryCondition2D<T,Lattice>*
createLocalBoundaryConditionWithExternalTensor2D(BlockLatticeStructure2D<T,Lattice>& block);

template<typename T, template<typename U> class Lattice>
OnLatticeBoundaryCondition2D<T,Lattice>*
createLocalBoundaryConditionWithExternalTensor2D(BlockLatticeStructure2D<T,Lattice>& block)
{
  return createLocalBoundaryConditionWithExternalTensor2D<T,Lattice,RLBdynamics<T,Lattice> >(block);
}

}

#endif
