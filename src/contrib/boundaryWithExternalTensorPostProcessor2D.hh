/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef BOUNDARY_WITH_EXTERNAL_TENSOR_2D_HH
#define BOUNDARY_WITH_EXTERNAL_TENSOR_2D_HH

#include "contrib/boundaryWithExternalTensorPostProcessor2D.h"
#include "core/finiteDifference2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "dynamics/lbHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"

namespace olb {
/////////// OuterVelocityWithExternalTensorCornerProcessor2D /////////////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
OuterVelocityWithExternalTensorCornerProcessor2D<T, Lattice, xNormal, yNormal>::
OuterVelocityWithExternalTensorCornerProcessor2D(int x_, int y_)
  : x(x_), y(y_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
void OuterVelocityWithExternalTensorCornerProcessor2D<T, Lattice, xNormal, yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  using namespace olb::util::tensorIndices2D;

  T rho10 = blockLattice.get(x-1*xNormal, y-0*yNormal).computeRho();
  T rho01 = blockLattice.get(x-0*xNormal, y-1*yNormal).computeRho();

  T rho20 = blockLattice.get(x-2*xNormal, y-0*yNormal).computeRho();
  T rho02 = blockLattice.get(x-0*xNormal, y-2*yNormal).computeRho();

  T rho = (T)2/(T)3*(rho01+rho10) - (T)1/(T)6*(rho02+rho20);

  T dx_u[Lattice<T>::d], dy_u[Lattice<T>::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::interpolateVector(dy_u, blockLattice, x,y);
  T dx_ux = dx_u[0];
  T dy_ux = dy_u[0];
  T dx_uy = dx_u[1];
  T dy_uy = dy_u[1];

  CellView<T,Lattice>& cell = blockLattice.get(x,y);

  Dynamics<T,Lattice>* dynamics = blockLattice.getDynamics(x, y);
  T omega = dynamics -> getOmega();

  T *tau = cell.getExternal(Lattice<T>::ExternalField::tensorBeginsAt);

  T sToPi = - rho / Lattice<T>::invCs2() / omega;
  T pi[util::TensorVal<Lattice<T> >::n];
  pi[xx] = (T)2 * dx_ux * sToPi - tau[xx];
  pi[yy] = (T)2 * dy_uy* sToPi - tau[yy];
  pi[xy] = (dx_uy + dy_ux) * sToPi - tau[xy];

  // Computation of the particle distribution functions
  // according to the regularized formula
  T u[Lattice<T>::d];
  blockLattice.get(x,y).computeU(u);

  T uSqr = util::normSqr<T,2>(u);
  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] =
      dynamics -> computeEquilibrium(iPop,rho,u,uSqr) +
      firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
void OuterVelocityWithExternalTensorCornerProcessor2D<T, Lattice, xNormal, yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_ )
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}


////////  OuterVelocityWithExternalTensorCornerProcessorGenerator2D ////////////////////////////

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
OuterVelocityWithExternalTensorCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::
OuterVelocityWithExternalTensorCornerProcessorGenerator2D(int x_, int y_)
  : PostProcessorGenerator2D<T,Lattice>(x_, x_, y_, y_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
PostProcessor2D<T,Lattice>*
OuterVelocityWithExternalTensorCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::generate() const
{
  return new OuterVelocityWithExternalTensorCornerProcessor2D<T, Lattice, xNormal, yNormal>
         ( this->x0, this->y0);
}

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
PostProcessorGenerator2D<T,Lattice>*
OuterVelocityWithExternalTensorCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>::
clone() const
{
  return new OuterVelocityWithExternalTensorCornerProcessorGenerator2D<T, Lattice, xNormal, yNormal>
         ( this->x0, this->y0);
}


}  // namespace olb

#endif
