/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef CHECKPOINT_F_H
#define CHECKPOINT_F_H


#include<vector>    // for generic i/o
#include<string>     // for lpnorm

#include "functors/analytical/analyticalF.h"
#include "core/superLattice3D.h"
#include "geometry/superGeometry3D.h"
#include "functors/lattice/superLatticeLocalF3D.h"
#include "dynamics/lbHelpers.h"  // for computation of lattice rho and velocity

#include "core/blockLattice3D.h"

namespace olb {


/// a class used to write and read checkpoint files in xml style
template <typename T,typename S, template <typename U> class DESCRIPTOR >
class checkpointF3D : public AnalyticalF3D<T,S> {
protected:
  SuperLatticeFpop3D<T, DESCRIPTOR>                 _f_pop;
  std::vector<T>                                    _return_of_f_pop;

  CuboidGeometry3D<T>&                              _cuboidGeometry;
  SuperGeometry3D<T>&                               _sGeometry;
  LoadBalancer<T>&                                  _loadBalancer;

  std::vector<std::vector<int> >                    _cells_on_cpu;
  std::vector<T>                                    _physOrigin_cpu;
  std::vector<T>                                    _physExtend_cpu;

  std::vector<std::vector<T> >                      _Pop_checkpoint;
  std::vector<std::vector<T> >                      _PhysPoints_checkpoint;

  std::vector<T>                                    _min_physRange_checkpoint_cpu;
  std::vector<T>                                    _max_physRange_checkpoint_cpu;

  std::vector<T>                                                 _physPoint;
  std::vector<int>                                               _latticePoint;

  std::vector<T>                                    _phys_Point_communication;
  std::vector<T>                                    _pop_communication;

  Vector<T,3>                                    _physOrigin;
  Vector<T,3>                                    _physExtend;

  std::vector<T>                                    _last_call_input;
  std::vector<T>                                    _result;

  int                                               _size;
  T                                                 _latticeL;
  T                                                 _latticeL_checkpoint;

  int                                               _material_no;
  int                                               _total_cells;
  int                                               _cellpoints_in_binaries;
  int                                               _callcount;

  typename std::vector<std::vector<T> >::iterator _itLastHit_000;
  typename std::vector<std::vector<T> >::iterator _itLastHit_100;
  typename std::vector<std::vector<T> >::iterator _itLastHit_010;
  typename std::vector<std::vector<T> >::iterator _itLastHit_001;
  typename std::vector<std::vector<T> >::iterator _itLastHit_110;
  typename std::vector<std::vector<T> >::iterator _itLastHit_011;
  typename std::vector<std::vector<T> >::iterator _itLastHit_101;
  typename std::vector<std::vector<T> >::iterator _itLastHit_111;

  int                                               _index_itLastHit_000_lastCall;
  bool                                              _was_reset_x;
  bool                                              _was_reset_y;

  std::vector<T>                                    _compare_to_input;
  std::vector<T>                                    _input;

  bool                                              _firstrun_operator;

  int                                               _hitcount_x;
  int                                               _hitcount_y;

  int                               i;
public:
  std::vector<T>                    min_input;
  std::vector<T>                    max_input;

  mutable OstreamManager                            clout;


public:
  checkpointF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,CuboidGeometry3D<T>& cuboidGeometry, SuperGeometry3D<T>& sGeometry, LBconverter<T>& converter , int material_no);

  void write(const std::string& filename, int iT);

  void read(const std::string& filename, int iT);

  bool operator() (T output[], const S input[]);



private:

  void reInit();
  /// bcast to all cpus
  void _bcastToAll();

  void _writeBinary(const std::string& filename, int iT);

  void _readBinary_and_bcast(const std::string& filename);

  bool _isStartingPoint( std::vector<T> physPointInVec );

  bool _isAlmostPhysPoint( std::vector<T> physPointInVec );

};
} // end namespace olb

#endif
