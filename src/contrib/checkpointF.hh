/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef CHECKPOINT_F_HH
#define CHECKPOINT_F_HH

#include <vector>    // for generic i/o
#include <algorithm> // for finding the value in a vector
#include <sstream>
#include <stdio.h>

#include "contrib/checkpointF.h"
#include "core/superLattice3D.h"
#include "functors/lattice/superLatticeLocalF3D.h"
#include "dynamics/lbHelpers.h"  // for computation of lattice rho and velocity
#include "external/tinyxml/tinyxml.h"


namespace olb {


///
template <typename T,typename S, template <typename U> class DESCRIPTOR >
checkpointF3D<T,S,DESCRIPTOR>::checkpointF3D( SuperLattice3D<T, DESCRIPTOR>& sLattice,CuboidGeometry3D<T>& cuboidGeometry, SuperGeometry3D<T>& sGeometry, LBconverter<T>& converter, int material_no)
  :
  AnalyticalF3D<T,T>(DESCRIPTOR<T>::q),
  _f_pop(sLattice),
  _return_of_f_pop(std::vector<T> (_f_pop.getTargetDim(),T())),
  _cuboidGeometry(cuboidGeometry),
  _sGeometry(sGeometry),
  _loadBalancer(sLattice.getLoadBalancer()),

  _physOrigin_cpu(std::vector<T> (3,T())),
  _physExtend_cpu(std::vector<T> (3,T())),

  _Pop_checkpoint(std::vector<std::vector<T> > (0,std::vector<T> (DESCRIPTOR<T>::q,T()))),
  _PhysPoints_checkpoint(std::vector<std::vector<T> > (0,std::vector<T> (DESCRIPTOR<T>::d,T()))),

  _min_physRange_checkpoint_cpu(std::vector<T> (DESCRIPTOR<T>::d,T())),
  _max_physRange_checkpoint_cpu(std::vector<T> (DESCRIPTOR<T>::d,T())),

  _physPoint(std::vector<T> (3,T())),
  _latticePoint(std::vector<int> (4,0)),

  _phys_Point_communication(std::vector<T> (DESCRIPTOR<T>::d,T())),
  _pop_communication(std::vector<T> (DESCRIPTOR<T>::q,T())),

  _physOrigin(std::vector<T> (3,T())),
  _physExtend(std::vector<T> (3,T())),

  _last_call_input(std::vector<T> (3,-100000000000)),
  _result(std::vector<T> (DESCRIPTOR<T>::q,T())),

  _latticeL(converter.getLatticeL()),
  _latticeL_checkpoint(0),

  _material_no(material_no),
  _total_cells(0),
  _cellpoints_in_binaries(0),
  _callcount(0),

  _itLastHit_000(_PhysPoints_checkpoint.begin()),
  _index_itLastHit_000_lastCall(0),
  _was_reset_x(true),
  _was_reset_y(true),

  _compare_to_input(std::vector<T> (3,T())),

  _firstrun_operator(true),
  _hitcount_x(0),
  _hitcount_y(0),

  i(0),
  min_input(std::vector<T> (3,100000)),
  max_input(std::vector<T> (3,T())),

  clout(std::cout,"CheckpointF3D")


{
  reInit();

  this->getName() = "checkpointF";
}

// TODO Operator is called by the function definePopulations
template <typename T,typename S, template <typename U> class DESCRIPTOR>
bool checkpointF3D<T,S,DESCRIPTOR>::operator() (T output[], const S input[])
{
  // TODO Divided by latticeL and multiplied with latticeL
  std::vector<int> tmp (3,0);
  std::vector<T> input_tmp (3,0);
  for (int j=0; j<3; ++j) {
    tmp[j] = (int)round((input[j]/_latticeL));
    input_tmp[j] = tmp[j]*_latticeL;
  }

  /// for speeding up the code (it will be called as many times as it has dimensions for the same point)
  /// as calling the same routine for every component in the vector is not very economic, this funktor will safe the last result and the last point it was asked, to minimize the effort
 if (input_tmp[0] == _last_call_input[0] && input_tmp[1] == _last_call_input[1] && input_tmp[2] == _last_call_input[2]) {
   for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
    output[pop] = _result[pop];
   }
    return true;
  }

  /// ############################################# SAME GRID -> NO INTERPOLATION NEEDED #################################################
  /// check if latticeL and latticeL_checkpoint are the same, then no interpolation will be needed
  if (_latticeL == _latticeL_checkpoint) {
    int index = -1;
    /// this is written for each coordinate direction sequentially although this may not be very efficient
    /// x
    if (input_tmp[0]>=(_min_physRange_checkpoint_cpu[0]-0.01*_latticeL) && input_tmp[0]<=(_max_physRange_checkpoint_cpu[0])+0.01*_latticeL) {
      /// y
      if (input_tmp[1]>=(_min_physRange_checkpoint_cpu[1]-0.01*_latticeL) && input_tmp[1]<=(_max_physRange_checkpoint_cpu[1])+0.01*_latticeL) {
        /// z
        if (input_tmp[2]>=(_min_physRange_checkpoint_cpu[2]-0.01*_latticeL) && input_tmp[2]<=(_max_physRange_checkpoint_cpu[2])+0.01*_latticeL) {
          ++_callcount;
          if ((_callcount)%10000 == 0) {
            cout << "Read " << _callcount << " cells of " << _cellpoints_in_binaries << " cells read" << endl;
          }

          /// search point in chechpoint values
          if (_firstrun_operator) {
            _itLastHit_000 = _PhysPoints_checkpoint.begin();
            _firstrun_operator = false;
          }
          _input = input_tmp;
          _itLastHit_000 = std::find_if(_itLastHit_000 , _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));

          ///not found since lasthit
          if (_itLastHit_000 == _PhysPoints_checkpoint.end()) {
            _itLastHit_000 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));

          }
          index = _itLastHit_000 - _PhysPoints_checkpoint.begin();

          ///increment itLast Hit for 1 as guess that it will hit on first test
          ++_itLastHit_000;

          if (index < _PhysPoints_checkpoint.size()) {
            /// return pop_checkpoint at this index
            _result = _Pop_checkpoint[index];
            _last_call_input = input_tmp;
            for (int pop=0; pop<DESCRIPTOR<T>::q; ++pop) {
              _result[pop] = _Pop_checkpoint[index][pop] - DESCRIPTOR<T>::t[pop];
            }
            for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
             output[pop] = _result[pop];
            }
            return true;


          } else {
            for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
             output[pop] = 0.;
            }
            return true;
          }
        }
      }
    }
    for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
     output[pop] = 0.;
    }
    return true;
  }

  // TODO Short Description How the interpolation works: trilinear,... Waht is hitcount, itLastHit etc. ?
  /// ############################################# FINER GRID -> INTERPOLATION NEEDED #################################################
  if (_latticeL < _latticeL_checkpoint) {
    /// this is written for each coordinate direction sequentially although this may not be very efficient
    /// x
    if (input_tmp[0]>=(_min_physRange_checkpoint_cpu[0]) && input_tmp[0]<=(_max_physRange_checkpoint_cpu[0])) {
      /// y
      if (input_tmp[1]>=(_min_physRange_checkpoint_cpu[1]) && input_tmp[1]<=(_max_physRange_checkpoint_cpu[1])) {
        /// z
        if (input_tmp[2]>=(_min_physRange_checkpoint_cpu[2]) && input_tmp[2]<=(_max_physRange_checkpoint_cpu[2])) {
          if (_firstrun_operator == false) {
            //cout << "inside speedup ################################" << endl;
            if ( input_tmp[0] != _last_call_input[0] && input_tmp[0] >= _PhysPoints_checkpoint[_itLastHit_000-_PhysPoints_checkpoint.begin()][0] && input_tmp[0] < _PhysPoints_checkpoint[_itLastHit_000-_PhysPoints_checkpoint.begin()][0] + _latticeL_checkpoint) {
              if (_hitcount_x <= _itLastHit_000 - _PhysPoints_checkpoint.begin()) {
                std::advance(_itLastHit_000, -(_hitcount_x));
                std::advance(_itLastHit_100, -(_hitcount_x));
                std::advance(_itLastHit_010, -(_hitcount_x));
                std::advance(_itLastHit_001, -(_hitcount_x));
                std::advance(_itLastHit_110, -(_hitcount_x));
                std::advance(_itLastHit_011, -(_hitcount_x));
                std::advance(_itLastHit_101, -(_hitcount_x));
                std::advance(_itLastHit_111, -(_hitcount_x));
              } else {
                _itLastHit_000 = _PhysPoints_checkpoint.begin();
                _itLastHit_100 = _PhysPoints_checkpoint.begin();
                _itLastHit_010 = _PhysPoints_checkpoint.begin();
                _itLastHit_001 = _PhysPoints_checkpoint.begin();
                _itLastHit_110 = _PhysPoints_checkpoint.begin();
                _itLastHit_011 = _PhysPoints_checkpoint.begin();
                _itLastHit_101 = _PhysPoints_checkpoint.begin();
                _itLastHit_111 = _PhysPoints_checkpoint.begin();

              }
              _hitcount_x = -1;
              _hitcount_y = -1;
            } else {
              if (input_tmp[1] != _last_call_input[1] && input_tmp[1] >= _PhysPoints_checkpoint[_itLastHit_000-_PhysPoints_checkpoint.begin()][1] && input_tmp[1] < _PhysPoints_checkpoint[_itLastHit_000-_PhysPoints_checkpoint.begin()][1] + _latticeL_checkpoint) {
                std::advance(_itLastHit_000, -(_hitcount_y));
                std::advance(_itLastHit_100, -(_hitcount_y));
                std::advance(_itLastHit_010, -(_hitcount_y));
                std::advance(_itLastHit_001, -(_hitcount_y));
                std::advance(_itLastHit_110, -(_hitcount_y));
                std::advance(_itLastHit_011, -(_hitcount_y));
                std::advance(_itLastHit_101, -(_hitcount_y));
                std::advance(_itLastHit_111, -(_hitcount_y));
                _hitcount_y = -1;
              }
            }
          }

          if (_firstrun_operator) {
            _itLastHit_000 = _PhysPoints_checkpoint.begin();
            _itLastHit_100 = _PhysPoints_checkpoint.begin();
            _itLastHit_010 = _PhysPoints_checkpoint.begin();
            _itLastHit_001 = _PhysPoints_checkpoint.begin();
            _itLastHit_110 = _PhysPoints_checkpoint.begin();
            _itLastHit_011 = _PhysPoints_checkpoint.begin();
            _itLastHit_101 = _PhysPoints_checkpoint.begin();
            _itLastHit_111 = _PhysPoints_checkpoint.begin();
            _firstrun_operator = false;
          }
          ///setting compare to vector to input point of operator
          _compare_to_input = input_tmp;
          _itLastHit_000 = std::find_if(_itLastHit_000 , _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isStartingPoint), this));

          /// not found, searching from start
          if (_itLastHit_000 == _PhysPoints_checkpoint.end()) {
            _itLastHit_000 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isStartingPoint), this));
          }
          /// if found -> search neighbours for interpolation
          if (_itLastHit_000 != _PhysPoints_checkpoint.end()) {
            if (_index_itLastHit_000_lastCall != _itLastHit_000 - _PhysPoints_checkpoint.begin()) {
              if (input_tmp[0] != _last_call_input[0]) {
                _hitcount_x = 0;
              } else {
                if (!(input_tmp[1] != _last_call_input[1] && input_tmp[1] >= _PhysPoints_checkpoint[_itLastHit_000-_PhysPoints_checkpoint.begin()][1] && input_tmp[1] < _PhysPoints_checkpoint[_itLastHit_000-_PhysPoints_checkpoint.begin()][1] + _latticeL_checkpoint)) {
                  ++_hitcount_x;
                }
              }
              ++_hitcount_y;
            }

            ///1 0 0
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[0] = _input[0] + _latticeL_checkpoint;
            _itLastHit_100 = std::find_if(_itLastHit_100, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_100 == _PhysPoints_checkpoint.end()) {
              _itLastHit_100 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            ///0 1 0
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[1] = _input[1] + _latticeL_checkpoint;
            _itLastHit_010 = std::find_if(_itLastHit_010, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_010 == _PhysPoints_checkpoint.end()) {
              _itLastHit_010 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            ///0 0 1
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[2] = _input[2] + _latticeL_checkpoint;
            _itLastHit_001 = std::find_if(_itLastHit_001, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_001 == _PhysPoints_checkpoint.end()) {
              _itLastHit_001 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            ///1 1 0
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[0] = _input[0] + _latticeL_checkpoint;
            _input[1] = _input[1] + _latticeL_checkpoint;
            _itLastHit_110 = std::find_if(_itLastHit_110, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_110 == _PhysPoints_checkpoint.end()) {
              _itLastHit_110 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            ///0 1 1
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[1] = _input[1] + _latticeL_checkpoint;
            _input[2] = _input[2] + _latticeL_checkpoint;
            _itLastHit_011 = std::find_if(_itLastHit_011, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_011 == _PhysPoints_checkpoint.end()) {
              _itLastHit_011 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            ///1 0 1
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[0] = _input[0] + _latticeL_checkpoint;
            _input[2] = _input[2] + _latticeL_checkpoint;
            _itLastHit_101 = std::find_if(_itLastHit_101, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_101 == _PhysPoints_checkpoint.end()) {
              _itLastHit_101 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            ///1 1 1
            _input = _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()];
            _input[0] = _input[0] + _latticeL_checkpoint;
            _input[1] = _input[1] + _latticeL_checkpoint;
            _input[2] = _input[2] + _latticeL_checkpoint;
            _itLastHit_111 = std::find_if(_itLastHit_111, _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            /// not found, searching from start
            if (_itLastHit_111 == _PhysPoints_checkpoint.end()) {
              _itLastHit_111 = std::find_if(_PhysPoints_checkpoint.begin(), _PhysPoints_checkpoint.end(), bind1st(mem_fun(&checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint), this));
            }

            /// check if all neighbours are found
            if (_itLastHit_100 != _PhysPoints_checkpoint.end() && _itLastHit_010 != _PhysPoints_checkpoint.end() && _itLastHit_001 != _PhysPoints_checkpoint.end() && _itLastHit_110 != _PhysPoints_checkpoint.end() && _itLastHit_011 != _PhysPoints_checkpoint.end() && _itLastHit_101 != _PhysPoints_checkpoint.end() && _itLastHit_111 != _PhysPoints_checkpoint.end()) {

              /// interpolate populations 1. calculate weights
              T z_0 = (_latticeL_checkpoint - (input_tmp[2] - _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][2])) / _latticeL_checkpoint;
              T z_1 = (input_tmp[2] - _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][2]) / _latticeL_checkpoint;

              T y_0 = (_latticeL_checkpoint - (input_tmp[1] - _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][1])) / _latticeL_checkpoint;
              T y_1 = (input_tmp[1] - _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][1]) / _latticeL_checkpoint;

              T x_0 = (_latticeL_checkpoint - (input_tmp[0] - _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][0])) / _latticeL_checkpoint;
              T x_1 = (input_tmp[0] - _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][0]) / _latticeL_checkpoint;

              /// do interpolation of populations
              for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {

                _result[pop] =( z_0*y_0*x_0* _Pop_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][pop] + z_0*y_0*x_1* _Pop_checkpoint[_itLastHit_100 - _PhysPoints_checkpoint.begin()][pop]
                                + z_0*y_1*x_0* _Pop_checkpoint[_itLastHit_010 - _PhysPoints_checkpoint.begin()][pop] + z_0*y_1*x_1* _Pop_checkpoint[_itLastHit_110 - _PhysPoints_checkpoint.begin()][pop]
                                + z_1*y_0*x_0* _Pop_checkpoint[_itLastHit_001 - _PhysPoints_checkpoint.begin()][pop] + z_1*y_0*x_1* _Pop_checkpoint[_itLastHit_101 - _PhysPoints_checkpoint.begin()][pop]
                                + z_1*y_1*x_0* _Pop_checkpoint[_itLastHit_011 - _PhysPoints_checkpoint.begin()][pop] + z_1*y_1*x_1* _Pop_checkpoint[_itLastHit_111 - _PhysPoints_checkpoint.begin()][pop]) - DESCRIPTOR<T>::t[pop];
              }

              _last_call_input = input_tmp;
              _index_itLastHit_000_lastCall = _itLastHit_000 - _PhysPoints_checkpoint.begin();
              for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
               output[pop] = _result[pop];
              }
              return true;
            }

            else {
              /// notity user something went wrong, the neighbours can't be found
              cout << "WARNING: Neighbours can't be found!" << endl;
              cout << "INPUT " << input_tmp[0] << " " << input_tmp[1] << " " << input_tmp[2] << endl;
              cout << "PhysBasepoint " << _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][0] << " " << _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][1] << " " << _PhysPoints_checkpoint[_itLastHit_000 - _PhysPoints_checkpoint.begin()][2] << endl;
            }

          }

          else {
            /// notity user something went wrong, the point can't be found
            cout << "WARNING: Point can't  be found!" << endl;
            cout << "INPUT " << input_tmp[0] << " " << input_tmp[1] << " " << input_tmp[2] << endl;
          }
          std::cout <<"index" << _itLastHit_000-_PhysPoints_checkpoint.begin() << endl;
        }
      }
    }
    // TODO Why do we set the populations to zero?
    for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
     output[pop] = 0.;
    }
    return true;

  }
  for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
   output[pop] = 0.;
  }
  return true;
}
// TODO Short Description whyand when we need reinit?
template <typename T,typename S, template <typename U> class DESCRIPTOR>
void checkpointF3D<T,S,DESCRIPTOR>::reInit()
{
  _cells_on_cpu.clear();
  ///extracting pyhsical origin from mothercuboid
  _physOrigin = _cuboidGeometry.getMotherCuboid().getOrigin();
  Vector<int,3> cuboidLength (0.,0.,0.);
  cuboidLength = _cuboidGeometry.getMotherCuboid().getExtend();
  T delta = 0;
  delta = _cuboidGeometry.getMotherCuboid().getDeltaR();
  for (int i=0; i< 3; ++i) {
    _physExtend[i] = cuboidLength[i] * delta;
  }

  bool firsthit = true;
  for (_physPoint[0]= _physOrigin[0]; _physPoint[0] <= _physExtend[0]; _physPoint[0] += _latticeL ) {

    for (_physPoint[1] = _physOrigin[1]; _physPoint[1] <= _physExtend[1]; _physPoint[1] += _latticeL ) {

      for (_physPoint[2] = _physOrigin[2]; _physPoint[2] <= _physExtend[2]; _physPoint[2] += _latticeL ) {

        ///getting latticePoint and respecting latticecuboid
        T physPoint[3]{0.,0.,0.};
        physPoint[0]=_physPoint[0];
        physPoint[1]=_physPoint[1];
        physPoint[2]=_physPoint[2];
        int latticePoint[4]{0,0,0,0};
        latticePoint[0]=_latticePoint[0];
        latticePoint[1]=_latticePoint[1];
        latticePoint[2]=_latticePoint[2];
        latticePoint[3]=_latticePoint[3];

        _f_pop.getSuperLattice().getCuboidGeometry().getLatticeR(latticePoint,physPoint);


        _latticePoint[0]=latticePoint[0];
        _latticePoint[1]=latticePoint[1];
        _latticePoint[2]=latticePoint[2];
        _latticePoint[3]=latticePoint[3];

        _physPoint[0]=physPoint[0];
        _physPoint[1]=physPoint[1];
        _physPoint[2]=physPoint[2];

        ///testing if the latticecuboid in which the lattice cell is contained is on the acual cpu, if so the return of the functor is added to the sum
        if ( _loadBalancer.rank(_latticePoint[0]) == singleton::mpi().getRank() ) {
          std::vector<int> latticePoint_tmp(4,0);
          latticePoint_tmp[0]=_latticePoint[0];
          latticePoint_tmp[1]=_latticePoint[1];
          latticePoint_tmp[2]=_latticePoint[2];
          latticePoint_tmp[3]=_latticePoint[3];

          _cells_on_cpu.push_back (latticePoint_tmp);

          /// determining physical range of cpu by maxima and minima of physical range on this cpu. WARNING: this is only valid if there are only cuboids on the cpus
          if (firsthit == true) {
            /// if the algortithm hits the first cell on the cpu, the values are set. for all other hits it will be decided if this range must be extended or not.
            _physOrigin_cpu = _physPoint;
            _physExtend_cpu = _physPoint;
            firsthit = false;
          }
          ///non firsthit, -> comparison and decision if the pyhsical range as to be extended.
          else {

            /// TODO: this is inefficient at this position because not every run all components of physPoint are changed (see for loop). but at the moment I don't care ...
            _physOrigin_cpu[0] = min(_physOrigin_cpu[0],_physPoint[0]);
            _physOrigin_cpu[1] = min(_physOrigin_cpu[1],_physPoint[1]);
            _physOrigin_cpu[2] = min(_physOrigin_cpu[2],_physPoint[2]);

            _physExtend_cpu[0] = max(_physExtend_cpu[0],_physPoint[0]);
            _physExtend_cpu[1] = max(_physExtend_cpu[1],_physPoint[1]);
            _physExtend_cpu[2] = max(_physExtend_cpu[2],_physPoint[2]);

          }
          min_input[0] = min(min_input[0], _physPoint[0]);
          min_input[1] = min(min_input[1], _physPoint[1]);
          min_input[2] = min(min_input[2], _physPoint[2]);

        }

      }
    }
  }
  if (singleton::mpi().isMainProcessor()) {
    std::cout << "physOrigin " << _physOrigin_cpu[0] << " " << _physOrigin_cpu[1] << " " << _physOrigin_cpu[2] << endl;
    std::cout << "physextend " << _physExtend_cpu[0] << " " << _physExtend_cpu[1] << " " << _physExtend_cpu[2] << endl;
  }
}

//saves the population information for every block
template <typename T,typename S, template <typename U> class DESCRIPTOR>
void checkpointF3D<T,S,DESCRIPTOR>::write( const std::string& filename, int iT)
{

  /// writing xml File to store header like information (to identify and group bin files). As it is only needed once -> let the main do it
  if (singleton::mpi().isMainProcessor()) {
    ofstream ofile;
    stringstream fname ;
    fname << filename  << "_iT" << iT << "_mat" << _material_no << ".xml" ;
    /// notify user
    cout << "Writing checkpoint " << fname.str() << " ..." << endl;

    ofile.open(fname.str().c_str());
    if ( ofile.is_open() ) {
      //ofile << std::setprecision( std::numeric_limits<double>::max() );
      ofile << "<?xml version='1.0'?>" << endl;
      ofile << "\n" << endl;
      /// writing global information about checkpoint
      ofile << "<checkpoint Timestep=\"" << iT << "\" material=\"" << _material_no << "\" Range_X=\"" << (_physExtend[0]-_physOrigin[0])/_latticeL <<
            "\" Range_Y=\"" << (_physExtend[1]-_physOrigin[1])/_latticeL <<  "\" Range_Z=\"" << (_physExtend[2]-_physOrigin[2])/_latticeL << "\" LatticeLength=\"" << _latticeL << "\" >" << endl;
      /// writing structure of the binary data
      ofile << "<structure>" << endl;
      ofile << "<binarydata type=\"double\" name=\"Phys_X\" length=\"1\" />" << endl;
      ofile << "<binarydata type=\"double\" name=\"Phys_Y\" length=\"1\" />" << endl;
      ofile << "<binarydata type=\"double\" name=\"Phys_Z\" length=\"1\" />" << endl;
      ofile << "<binarydata type=\"double\" name=\"Populations\" length=\"19\" />" << endl;
      ofile << "</structure>" << endl;

      /// order main cpu to write its data to file
      this->_writeBinary(filename,iT);

      ofile << "<cellpoints_in_binaries sum=\"" << _total_cells << "\" />" << endl;
      ofile << "<cpus root=\"" << singleton::mpi().bossId() << "\" size=\"" << singleton::mpi().getSize() << "\" />" << endl;
      ofile << "<binary_filenames>" << endl;
      ofile << "<file name=\"" << filename  << "_iT" << iT << "_mat" << _material_no << "_cpu" << singleton::mpi().getRank() << "_LatticeL" << ".bin" <<"\" />" << endl;
      for (int i=0; i<singleton::mpi().getSize(); ++i) {
        ofile << "<file name=\"" << filename  << "_iT" << iT << "_mat" << _material_no << "_cpu" << i << ".bin" << "\" />" << endl;
      }
      ofile << "</binary_filenames>" << endl;
      ofile << "</checkpoint>" << endl;

      ofile.close();
    } else {
      cout << "ERROR OPENING FILE TO WRITE CHECKPOINT" << endl;
    }

    /// writing lattice L to file
    stringstream fname_latticeL ;
    fname_latticeL << filename  << "_iT" << iT << "_mat" << _material_no << "_cpu" << singleton::mpi().getRank() << "_LatticeL" << ".bin" ;

    /// Using c style file output because this seems to be the fastest way
    FILE* pFile;
    pFile = fopen(fname_latticeL.str().c_str(), "wb");

    fwrite(&_latticeL, sizeof(T), 1, pFile);

    fclose(pFile);

    /// notify user that writing of checkpoint is finished
    cout << "Writing checkpoint " << fname.str() << " ... OK" << endl;

  } else {
    /// order all non main cpus to write their data to files
    this->_writeBinary(filename,iT);
  }



}
// TODO Short Description
template <typename T,typename S, template <typename U> class DESCRIPTOR>
void checkpointF3D<T,S,DESCRIPTOR>::read(const std::string& filename, int iT)
{
  /// main cpu loads xml file, then all binary files and broadcasts the values to all cpus. these check which points they need and safe them.
  if (singleton::mpi().isMainProcessor()) {

    /// loading xml file
    stringstream fname;
    fname << filename  << "_iT" << iT << "_mat" << _material_no << ".xml" ;
    /// notify user
    cout << "Reading checkpoint " << fname.str() << " ..." << endl;

    /// open xml document
    TiXmlDocument input(fname.str().c_str());

    ///create namelist for filenames
    std::vector<std::string> filenames (0, std::string() );

    ///loading file
    bool loaded = input.LoadFile();
    if (loaded) {

      TiXmlHandle hDoc(&input);
      TiXmlElement* pElem;
      TiXmlAttribute* pAttrib;
      TiXmlHandle hRoot(0);

      /// get pointer to main element named checkpoint
      pElem=hDoc.FirstChildElement( "checkpoint" ).Element();
      hRoot=TiXmlHandle(pElem);

      /// checking material no
      int matno_file = 0;
      pElem->Attribute("material", &matno_file );
      /// raise error if materialno is not the one of the funktor
      if (matno_file != _material_no) {
        cout << "Wrong material number in checkpoint file" << endl;
        exit(4);
      }
      ///extracting cellpoints_in_binaries for other cpus to know how many values need to be communicated
      pElem = hDoc.FirstChildElement( "checkpoint" ).FirstChild( "cellpoints_in_binaries" ).Element();

      pElem->Attribute("sum", &_cellpoints_in_binaries );

      /// extracting no of binary files by size of cpus
      int no_cpus_file = 0;
      pElem=hRoot.FirstChild( "cpus" ).Element();
      pElem->Attribute("size", &no_cpus_file);

      /// extracting filenames of binary files from binary_filenames in xml
      hRoot = hRoot.FirstChild( "binary_filenames" );
      filenames = std::vector<std::string> (no_cpus_file+1 , std::string() );
      pElem = hRoot.FirstChild( "file" ).Element();
      /// if element found, go on
      if (pElem) {

        /// loop over all elements
        int filecount = 0;
        while ( pElem ) {

          ///get attribute
          pAttrib = pElem->FirstAttribute();
          ///get filename of binary
          filenames[filecount] = pAttrib->Value();

          ///get next element
          pElem=pElem->NextSiblingElement();
          ++filecount;
        }
      }

    } else {
      cout << "WARNING: Could not load checkpoint file ... Guess: wrong material number or missing files" << endl;
      exit(3);

    }

    /// loading binary file with latticeL
    stringstream fname_latticeL ;
    fname_latticeL << filename  << "_iT" << iT << "_mat" << _material_no << "_cpu" << singleton::mpi().getRank() << "_LatticeL" << ".bin" ;

    ///opening file
    int size;
    FILE* pFile;
    pFile = fopen(fname_latticeL.str().c_str(), "rb");

    /// reading lattice L_
    size = fread(&_latticeL_checkpoint, sizeof(T), 1 , pFile);
    if (size > 1 || size < 1 ) {
      cout << "Error reading Lattice Length from binary file" << endl;
      exit(5);
    }
    fclose(pFile);

    /// finished loading of the xml file, starting loading of binary files and sending
    /// bcasting latticelength in checkpoint
    singleton::mpi().bCast(& _latticeL_checkpoint, 1 , singleton::mpi().bossId() , MPI_COMM_WORLD );

    ///broadcasting no of cells in checkpoint file (_cellpoints_in_binaries)
    singleton::mpi().bCast(& _cellpoints_in_binaries, 1 , singleton::mpi().bossId() , MPI_COMM_WORLD );

    /// loop over all filenames specified in xml file
    /// NOTE: only main cpu reads files, the rest is broadcasted
    for (int f=1; f<filenames.size(); ++f) {

      this->_readBinary_and_bcast( filenames[f] );

    }

    /// notify user
    if (_physOrigin_cpu[0] >= _min_physRange_checkpoint_cpu[0] && _physOrigin_cpu[1] >= _min_physRange_checkpoint_cpu[1] && _physOrigin_cpu[2] >= _min_physRange_checkpoint_cpu[2] && _physExtend_cpu[0] <= _max_physRange_checkpoint_cpu[0] && _physExtend_cpu[1] <= _max_physRange_checkpoint_cpu[1] && _physExtend_cpu[2] <= _max_physRange_checkpoint_cpu[2] ) {
      cout << "Reading checkpoint " << fname.str() << " ... OK" << endl;
    } else {
      cout << "[CheckpointF3D] WARNING: Checkpoint File does not contain the needed physical Range" << endl;
    }

  }
  ///######################################non main cpus############################
  else {
    /// catchin bcast for _latticeL_checkpoint
    singleton::mpi().bCast(& _latticeL_checkpoint, 1 , singleton::mpi().bossId() , MPI_COMM_WORLD );

    ///recieving site of cells in checkpoint
    singleton::mpi().bCast(& _cellpoints_in_binaries, 1 , singleton::mpi().bossId() , MPI_COMM_WORLD );

    ///catch all the bcasts from main (no = _cellpoints_in_binaries)
    for (int cell=0; cell<_cellpoints_in_binaries; ++cell) {
      this->_bcastToAll();
    }
  }
}
///####################################PRIVATE###############################################

// TODO Correct Description?
//MPI communication over all blocks: Mapping of the physical coordinates to every block
template <typename T,typename S, template <typename U> class DESCRIPTOR>
void checkpointF3D<T,S,DESCRIPTOR>::_bcastToAll ()
{

  ///bcasting both comm variables to all cpus
  singleton::mpi().bCast(& _pop_communication[0], DESCRIPTOR<T>::q , singleton::mpi().bossId() , MPI_COMM_WORLD );

  singleton::mpi().bCast(& _phys_Point_communication[0], DESCRIPTOR<T>::d, singleton::mpi().bossId() , MPI_COMM_WORLD );


  /// decide whether the point and the pop should be stored on the acual cpu or not
  /// this is written for each coordinate direction sequentially although this may not be very efficient
  /// x
  if (_phys_Point_communication[0]>=(_physOrigin_cpu[0]-3*_latticeL_checkpoint) && _phys_Point_communication[0]<=(_physExtend_cpu[0]+3*_latticeL_checkpoint)) {
    /// y
    if (_phys_Point_communication[1]>=(_physOrigin_cpu[1]-3*_latticeL_checkpoint) && _phys_Point_communication[1]<=(_physExtend_cpu[1]+3*_latticeL_checkpoint)) {
      /// z
      if (_phys_Point_communication[2]>=(_physOrigin_cpu[2]-3*_latticeL_checkpoint) && _phys_Point_communication[2]<=(_physExtend_cpu[2]+3*_latticeL_checkpoint)) {

        /// point is needed on the actual cpu -> store it in the vector
        _PhysPoints_checkpoint.push_back(_phys_Point_communication);
        _Pop_checkpoint.push_back(_pop_communication);

        /// get range of data that is read in from checkpoint on every cpu by finding the maxima and minima
        if (_PhysPoints_checkpoint.size() == 1) { // firstrun

          _min_physRange_checkpoint_cpu = _phys_Point_communication;
          _max_physRange_checkpoint_cpu = _phys_Point_communication;

        }
        for (int coordinate = 0; coordinate < DESCRIPTOR<T>::d; ++ coordinate) {

          _min_physRange_checkpoint_cpu[coordinate] = min(_min_physRange_checkpoint_cpu[coordinate], _phys_Point_communication[coordinate]);
          _max_physRange_checkpoint_cpu[coordinate] = max(_max_physRange_checkpoint_cpu[coordinate], _phys_Point_communication[coordinate]);
        }
      }
    }
  }

}
// TODO Short Description
template <typename T,typename S, template <typename U> class DESCRIPTOR>
void checkpointF3D<T,S,DESCRIPTOR>::_writeBinary( const std::string& filename, int iT)
{

  /// create filename with an additon of cpu at the end
  stringstream fname ;
  fname << filename  << "_iT" << iT << "_mat" << _material_no << "_cpu" << singleton::mpi().getRank() << ".bin" ;

  /// Using c style file output because this seems to be the fastest way
  FILE* pFile;
  pFile = fopen(fname.str().c_str(), "wb");

  unsigned int no_T_written = 0;
  /// loop over all cells on this cpu
  for (int i=0; i<_cells_on_cpu.size(); ++i) {
    /// getting Information about the point
    T physPoint[3]{0.,0.,0.};
    physPoint[0]=_physPoint[0];
    physPoint[1]=_physPoint[1];
    physPoint[2]=_physPoint[2];
    int latticePoint[4]{0,0,0,0};
    latticePoint[0]=_latticePoint[0];
    latticePoint[1]=_latticePoint[1];
    latticePoint[2]=_latticePoint[2];
    latticePoint[3]=_latticePoint[3];

    _f_pop.getSuperLattice().getCuboidGeometry().getPhysR(physPoint,latticePoint);

    _latticePoint[0]=latticePoint[0];
    _latticePoint[1]=latticePoint[1];
    _latticePoint[2]=latticePoint[2];
    _latticePoint[3]=latticePoint[3];

    _physPoint[0]=physPoint[0];
    _physPoint[1]=physPoint[1];
    _physPoint[2]=physPoint[2];


    T return_of_f_pop[DESCRIPTOR<T>::q];
    for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
      return_of_f_pop[pop] = 0.;
    }

    int cells_on_cpu[4];
    for (int j = 0 ; j < _cells_on_cpu[i].size() ; j++) {
      cells_on_cpu[j] = _cells_on_cpu[i][j];
    }
    _f_pop(return_of_f_pop, cells_on_cpu);

    for (int pop = 0 ; pop < DESCRIPTOR<T>::q ; ++pop) {
      _return_of_f_pop[pop] = return_of_f_pop[pop];
    }

    ///writing physical point
    no_T_written = fwrite(&_physPoint[0], sizeof(T), _physPoint.size(), pFile);
    /// checking if the write could be done
    if (no_T_written != _physPoint.size()) {
      fputs ("Writing Error in CheckpointF3D" , stderr);
      exit(1);
    }

    ///writing populations
    no_T_written = fwrite(&_return_of_f_pop[0], sizeof(T), _return_of_f_pop.size(), pFile);
    ///checking write
    if (no_T_written != _return_of_f_pop.size()) {
      fputs ("Writing Error in CheckpointF3D in file" , stderr);
      exit(2);
    }

  }
  fclose(pFile);

  /// reducing no of points written to main cpu
  void reduce(T& sendVal, T& recVal, MPI_Op, int root =0, MPI_Comm = MPI_COMM_WORLD);
}
// TODO Short Description
template <typename T,typename S, template <typename U> class DESCRIPTOR>
void checkpointF3D<T,S,DESCRIPTOR>::_readBinary_and_bcast( const std::string& filename)
{

  ///generating Filename
  stringstream fname ;
  fname << filename ;

  ///opening file
  FILE* pFile;
  pFile = fopen(fname.str().c_str(), "rb");

  ///get size of vectors so check if reading was correct
  unsigned int size_phys_point_read = DESCRIPTOR<T>::d;
  unsigned int size_pop_read = DESCRIPTOR<T>::q;

  while (size_phys_point_read == DESCRIPTOR<T>::d && size_pop_read == DESCRIPTOR<T>::q) {

    ///reading physical point
    size_phys_point_read = fread(&_phys_Point_communication[0], sizeof(T), DESCRIPTOR<T>::d, pFile);
    ///adjusting physical range because (0,0,0) is allways at the same spot which leads to a shift if the latticeL is not the same
    if (_latticeL < _latticeL_checkpoint) {
      for (int coordinate=0; coordinate< DESCRIPTOR<T>::d ; ++ coordinate) {
        _phys_Point_communication[coordinate] -= 0.25*(_latticeL);
      }
    } else if (_latticeL > _latticeL_checkpoint) {
      cout << "WARNING: Lattice Length is greater than Lattice Length in Checkpoint. This is not implemented" << endl;
      exit(11);
    }
    ///reading populations
    size_pop_read = fread(&_pop_communication[0], sizeof(T), DESCRIPTOR<T>::q, pFile);

    ///if value could be read sucessfully
    if (size_phys_point_read == DESCRIPTOR<T>::d && size_pop_read == DESCRIPTOR<T>::q) {
      this->_bcastToAll();
    }

  }
  fclose(pFile);
}

// TODO I dont understand this comment
/// Unary Predicate for find_if
template <typename T,typename S, template <typename U> class DESCRIPTOR >
bool checkpointF3D<T,S,DESCRIPTOR>::_isStartingPoint( std::vector<T> physPointInVec )
{
  bool found = true;
  for (int i = 0 ; i<DESCRIPTOR<T>::d ; ++i) {

    if (_compare_to_input[i] >= physPointInVec[i]/*-0.1*_latticeL*/ && _compare_to_input[i] < physPointInVec[i]+_latticeL_checkpoint/*+0.1*_latticeL*/ ) {

      found = (found && true);

    } else {

      found = (found && false);

    }
  }
  return found;
}
// TODO I dont understand this comment
/// Unary Predicate for find_if
template <typename T,typename S, template <typename U> class DESCRIPTOR >
bool checkpointF3D<T,S,DESCRIPTOR>::_isAlmostPhysPoint( std::vector<T> physPointInVec )
{
  bool found = true;
  for (int i = 0 ; i<DESCRIPTOR<T>::d ; ++i) {

    if (physPointInVec[i] <= (_input[i]+0.01*_latticeL) && physPointInVec[i] >= (_input[i]-0.01*_latticeL) ) {

      found = (found && true);

    } else {

      found = (found && false);

    }
  }
  return found;
}



} // end namespace olb

#endif
