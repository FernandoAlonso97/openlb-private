#ifndef NETWORKDATASTRUCTURES_H
#define NETWORKDATASTRUCTURES_H

template<unsigned int RESOLUTIONX, unsigned int RESOLUTIONY, unsigned int RESOLUTIONZ>
struct FluidMaskData {
    bool fluidMask[RESOLUTIONX*RESOLUTIONY*RESOLUTIONZ];
    FluidMaskData()
    {
        memset(fluidMask,0,sizeof(bool)*RESOLUTIONX*RESOLUTIONY*RESOLUTIONZ);
    }
};

struct GensimDataVis {

    GensimDataVis()
    {
        memset(attitudes,0,sizeof(float)*3);
        memset(rotatorics,0,sizeof(float)*3);
        memset(velocities,0,sizeof(float)*3);
        memset(positions,0,sizeof(float)*3);
    }

    float attitudes[3];
    float rotatorics[3];
    float velocities[3];
    float positions[3];
    int   simulationStatus{};

    template<class GensimReceiveData>
    GensimDataVis & operator = (GensimReceiveData const & gensimData)
    {
        for(unsigned int iDim = 0; iDim < 3; ++iDim)
        {
            attitudes[iDim] = gensimData.attitudes[iDim];
            rotatorics[iDim] = gensimData.rotatorics[iDim];
            velocities[iDim] = gensimData.velocities[iDim];
            positions[iDim] = gensimData.positions[iDim];
        }

        return *this;
    }

    void setTrimState()
    {
        for(unsigned int iDim = 0; iDim < 3; ++iDim)
        {
            velocities[iDim] = 0;
            positions[iDim] = 0;
        }

        simulationStatus = 99;
    }
};

typedef struct VelocityAtRotorsLinear{

	int					timestep = 0;
	
	float				velocityMainMean[3] = {0,0,0};
	float				velocityMainSin[3] = {0,0,0};
	float				velocityMainCos[3] = {0,0,0};

	float				velocityTailMean[3] = {0,0,0};
	float				velocityTailSin[3] = {0,0,0};
	float				velocityTailCos[3] = {0,0,0};

} VelocityAtRotorsLinear;

template <unsigned int NoHarmonicsRadial, unsigned int NoHarmonicsAzimuth>
struct VelocityAtRotorsTwoHarmonic {

            int                 timeStep;
            int                 noHarmonicsRadial = NoHarmonicsRadial;
            int                 noHarmonicsAzimuth = NoHarmonicsAzimuth;

            float               ccX[25*9];
            float               csX[25*9];
            float               scX[25*9];
            float               ssX[25*9];

            float               ccY[25*9];
			float               csY[25*9];
			float               scY[25*9];
			float               ssY[25*9];

            float               ccZ[25*9];
			float               csZ[25*9];
			float               scZ[25*9];
			float               ssZ[25*9];


            float               scalingFactor;
            float               maxVelocity;
            float               minVelocity;
            float               bounds[2];

            constexpr unsigned int getIndex(unsigned int iRad,unsigned int iAz) const {
                 return iRad*noHarmonicsAzimuth+iAz;
             }
};

struct VelocityAtLiftingLine {

            int                 timeStep;

            float               r[200];
            float               vx[200*10];
            float               vy[200*10];
            float               vz[200*10];

};

template <unsigned int NoHarmonics>
struct VelocityAtRotorsTwoHarmonicCartesian {

            int                 timeStep;
            int                 noHarmonics = NoHarmonics;

            float               cc[25*9];
            float               cs[25*9];
            float               sc[25*9];
            float               ss[25*9];
            float               scalingFactor;

            constexpr unsigned int getIndex(unsigned int iX,unsigned int iY) const {
                 return iX*NoHarmonics+iY;
             }
};

typedef struct ThrustAtRotorsLinear {

		int					timestep;
		int					simulationStatus;
		float				dt,dphi;
		float				AbsThrust;

		float				positions[3];
		float				attitudes[3];
		float				velocities[3];
		float				rotatorics[3];

		bool				mainIsRunning;
		float				presMainMean;
		float				presMainSin;
		float				presMainCos;

		bool				tailIsRunning;
		float				presTailMean;
		float				presTailSin;
		float				presTailCos;

}ThrustAtRotorsLinear;

template<unsigned int Harmonicscount>
struct VelocityAtRotorsHarmonic{

	int					timestep;
	
	float				velocityMainMean[3];
	float				velocityMainSin[3][Harmonicscount];
	float				velocityMainCos[3][Harmonicscount];

	float				velocityTailMean[3];
	float				velocityTailSin[3][Harmonicscount];
	float				velocityTailCos[3][Harmonicscount];


};

template<unsigned int Harmonicscount>
struct ThrustAtRotorsHarmonic{

		int					timestep = {0};
		int					simulationStatus = {0};
		float				dt = {0};
    float       dphi = {0};
		float				AbsThrust[3] = {0,0,0};

		float				positions[3] = {0,0,0};
		float				attitudes[3] = {0,0,0};
		float				velocities[3] = {0,0,0};
		float				rotatorics[3] = {0,0,0};

		bool				mainIsRunning = false;
		float				presMainMeanX = {0};
		float				presMainSinX[Harmonicscount];
		float				presMainCosX[Harmonicscount];

		float				presMainMeanY = {0};
		float				presMainSinY[Harmonicscount];
		float				presMainCosY[Harmonicscount];

		float				presMainMeanZ = {0};
		float				presMainSinZ[Harmonicscount];
		float				presMainCosZ[Harmonicscount];

		bool				tailIsRunning = false;
		float				presTailMeanX = {0};
		float				presTailSinX[Harmonicscount];
		float				presTailCosX[Harmonicscount];

		float				presTailMeanY = {0};
		float				presTailSinY[Harmonicscount];
		float				presTailCosY[Harmonicscount];

		float				presTailMeanZ = {0};
		float				presTailSinZ[Harmonicscount];
		float				presTailCosZ[Harmonicscount];

};

struct ThrustAtRotorsTwoHarmonic {

    int                 simulationStatus;
    float               dt,dphi;
    float               AbsThrust[3];

    unsigned int        noHarmonicsRadial;
    unsigned int        noHarmonicsAzimuth;

    float               positions[3];
    float               attitudes[3];
    float               velocities[3];
    float               rotatorics[3];

    float               cc[25*9];
    float               cs[25*9];
    float               sc[25*9];
    float               ss[25*9];

    OPENLB_HOST_DEVICE
    constexpr unsigned int getIndex(unsigned int const iRad,unsigned int const iAz) const {
         return iRad*noHarmonicsAzimuth+iAz;
    }
};

struct ThrustAtRotorsBesselHarmonic {

    int                 simulationStatus;
    float               dt,dphi;
    float               AbsThrust[3];

    unsigned int        noHarmonicsRadial;
    unsigned int        noHarmonicsAzimuth;

    float               positions[3];
    float               attitudes[3];
    float               velocities[3];
    float               rotatorics[3];

    float               c[25*9];
    float               s[25*9];

	float               roots[25];

    OPENLB_HOST_DEVICE
    constexpr unsigned int getIndex(unsigned int const iRad,unsigned int const iAz) const {
         return iRad*noHarmonicsAzimuth+iAz;
    }
};


struct LiftingLine {
    int                 simulationStatus;
    float               dt,dphi;
    float               AbsThrust[3];

	unsigned int        noHarmonicsRadial;
//    float               c[30*10];
//    float               s[30*10];

    float               t[180*2];
    float               r[180*2];

    float               positions[3];
    float               attitudes[3];
    float               velocities[3];
    float               rotatorics[3];

	double c_[10];
	double ar_[10];
	double az_[10];
	unsigned int nBlades_;

	OPENLB_HOST_DEVICE
	static constexpr unsigned int getIndex(unsigned int const iBlade, unsigned int const iHarm) {
		return iBlade*30+iHarm;
	}

	OPENLB_HOST_DEVICE
	static constexpr unsigned int getIndexT(unsigned int const iBlade, unsigned int const iPos) {
		return iBlade*180+iPos;
	}

	OPENLB_HOST_DEVICE
	const float* begin(unsigned int const iBlade) const {
		return r+(180*iBlade);
	}

	OPENLB_HOST_DEVICE
	const float* end(unsigned int const iBlade) const {
		return r+180*(iBlade+1);
	}
};



template<unsigned int Resolution>
struct VelocityAtRotorsLocal{

	int					timestep;
	
	float				velocityMainU[Resolution/2][Resolution/2];
	float				velocityMainV[Resolution/2][Resolution/2];
	float				velocityMainW[Resolution/2][Resolution/2];

	float				velocityMainMean;
	int					cellcountMR;

	float				velocityTailMean[3];
	float				velocityTailSin[3];
	float				velocityTailCos[3];

	int					cellcountTR;

	float				inducedPower[3];
	float				inflowVelocities[3];

};

template<unsigned int Harmonicscount>
struct ThrustAtRotorsLocal{

		int					timestep;
		int					simulationStatus;
		float				dt,dphi;
		float				AbsThrust[3];
//		int					azimuthPos;

		float				positions[3];
		float				attitudes[3];
		float				velocities[3];
		float				rotatorics[3];

		bool				mainIsRunning;
		float				presMainMeanX[360*15];
		float				presMainMeanY[360*15];
		float				presMainMeanZ[360*15];


		bool				tailIsRunning;
		float				presTailMeanX;
		float				presTailSinX[Harmonicscount];
		float				presTailCosX[Harmonicscount];

		float				presTailMeanY;
		float				presTailSinY[Harmonicscount];
		float				presTailCosY[Harmonicscount];

		float				presTailMeanZ;
		float				presTailSinZ[Harmonicscount];
		float				presTailCosZ[Harmonicscount];

		void print()
		{
		    std::cout << "=================================" << std::endl;
		    std::cout << "Timestep: " << timestep << ", dphi: " << dphi << ", dt: " << dt << std::endl;
		    std::cout << "Main is running: " << mainIsRunning << std::endl;
		    std::cout << "Main rotor thrust: " << AbsThrust[0] << "," << AbsThrust[1] << "," << AbsThrust[2] << std::endl;
		    std::cout << "Tail is running: " << tailIsRunning << std::endl;
		    std::cout << "Rotorcraft is at [" << positions[0] << "," << positions[1] << "," << positions[2] << std::endl;
		    std::cout << "Rotorcraft orientation is: " << attitudes[0] << "," << attitudes[1] << "," << attitudes[2] << std::endl;
		    std::cout << "Pressure Z: " << std::endl;
		    for(unsigned int iBladeElement = 0; iBladeElement<15; ++iBladeElement)
		    {
		        std::cout << presMainMeanZ[iBladeElement] << ",";
		    }
		    std::cout << std::endl;
		    std::cout << "=================================" << std::endl;
		}

};

template<unsigned int PsiSteps>
struct IndividualBladeValues{

	float					psiPositions[PsiSteps+1];
	
	float					forcesX[PsiSteps*15];
	float					forcesY[PsiSteps*15];
	float					forcesZ[PsiSteps*15];

	float					absForceX[PsiSteps];
	float					absForceY[PsiSteps];
	float					absForceZ[PsiSteps];

};

template<unsigned int Bladecount>
struct ThrustAtIndividualBlades{

		int					timestep;
		int					simulationStatus;
		float				dt,dphi;
		float				AbsThrust[3];
//		int					azimuthPos;

		float				positions[3];
		float				attitudes[3];
		float				velocities[3];
		float				rotatorics[3];

		bool				mainIsRunning;
//		IndividualBladeValues	blades[Bladecount];

		bool				tailIsRunning;

};

typedef struct ThrustAtRotorsConstant {

		int					timestep;
		int					simulationStatus;
		float				dt,dphi;
		float				AbsThrust;

		float				positions[3];
		float				attitudes[3];
		float				velocities[3];
		float				rotatorics[3];

		bool				mainIsRunning;
		float				presMainMean;
		float				presMainSin;
		float				presMainCos;

		bool				tailIsRunning;
		float				presTailMean;
		float				presTailSin;
		float				presTailCos;

}ThrustAtRotorsConstant;

#endif
