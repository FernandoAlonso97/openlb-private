/*
 * couplingCore.h
 *
 *  Created on: Jul 2, 2019
 *      Author: ga69kiq
 */

#ifndef SRC_CONTRIB_COUPLING_COUPLINGCORE_H_
#define SRC_CONTRIB_COUPLING_COUPLINGCORE_H_

#include "coupling_Policies.h"
#include "coupling_Policies.hh"

template<typename T, class WritePolicy, class ReadPolicy>
class Coupling final{
public:
    typedef typename WritePolicy::ReceiveDataType ReceiveDataType;
    typedef typename ReadPolicy::SendDataType SendDataType;
    typedef CouplingData<T, typename WritePolicy::DataType, typename ReadPolicy::DataType> CouplingDataType;

    template<typename...Params>
    static void writeReceiveData(Params&& (...parameter) )
    {
        WritePolicy::writeReceiveData(std::forward<Params>(parameter)...);
    }

    template<typename...Params>
    static void readSendData(Params&& (...parameter) )
    {
        ReadPolicy::readSendData(std::forward<Params>(parameter)...);
    }

    Coupling() = delete;
};

#endif /* SRC_CONTRIB_COUPLING_COUPLINGCORE_H_ */
