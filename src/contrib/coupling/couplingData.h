/*
 * couplingData.h
 *
 *  Created on: Jul 2, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CONTRIB_COUPLING_COUPLINGDATA_H_
#define SRC_CONTRIB_COUPLING_COUPLINGDATA_H_

#include "core/config.h"
#include "rotor.h"

#include "cublas_v2.h"

template<typename T, typename WriteData, typename ReadData>
class CouplingData {
public:
    template<class InitType>
    CouplingData(InitType& initData);

    template<class InitTypeWrite, class InitTypeRead>
    CouplingData(InitTypeWrite& initDataWrite, InitTypeRead& initDataRead);

    WriteData& getWriteData();

    ReadData& getReadData();

private:
    WriteData writeData;
    ReadData readData;
};

struct NoData {
    template<typename AnyType>
    NoData(AnyType& data){}
    NoData(){}
};

template<typename T>
struct RelaxationData {
    RelaxationData(T const relaxationFactor):
    relaxationFactor_(relaxationFactor){}

    T const relaxationFactor_;
};

template<typename T>
struct LiftingLineData {
	LiftingLineData(T const R, T const dx):
	nCell_(R/dx),
	n_max(std::max(0.09*dx/R,3.)),
	eps2_(dx*n_min),
	cellSize_(dx*dx) {	};

	OPENLB_HOST_DEVICE
	T eps(LiftingLine lifLine, T rPosition, unsigned int iBlade) const;

	OPENLB_HOST_DEVICE
	T eps(float const c, float const ar, T rPosition) const;

	T const n_max = 3.2;
	T const n_min = 2.;
	T const nCell_;
	T const eps2_;
	T const cellSize_;
};

template<typename T>
struct VelocitiesAtRotorLinearVectors {
    VelocitiesAtRotorLinearVectors(RotorCellData<T>& rotorCellData);

    OPENLB_HOST_DEVICE
    ~VelocitiesAtRotorLinearVectors();

    T** velocityMainMean;
    T** velocityMainSin;
    T** velocityMainCos;


    T* rotorCellFactorLinearCos;
    T* rotorCellFactorLinearSin;
};

template<typename T>
struct VelocityAtRotorsTwoHarmonicVectors {

    VelocityAtRotorsTwoHarmonicVectors(std::tuple<Rotor<T>, SquareCellData<T>,unsigned int, unsigned int,
            T, T, unsigned int, unsigned int, bool> const data);

    OPENLB_HOST_DEVICE
    ~VelocityAtRotorsTwoHarmonicVectors();

    enum Coefficent {cc = 0, cs, sc, ss};

    const unsigned int noHarmonicsRadial_;
    const unsigned int noHarmonicsAzimuth_;
    const T scalingFactor_;
    const bool getInterpInflow_;

    float* interpolationMatrix = nullptr;
    float* gridInterpMatrix = nullptr;
    float* inflowVector = nullptr;
    float* inflowInterpVector = nullptr;
    std::vector<float> h_inflowInterpVector;
    std::vector<float> h_coefficients;
    float* d_coefficients = nullptr;

#ifdef ENABLE_CUDA
    cublasHandle_t _handle;
#endif

    OPENLB_HOST_DEVICE
    constexpr unsigned int getIndex(unsigned int iRad,unsigned int iAz) {
        return iRad*noHarmonicsAzimuth_+iAz;
    }

    OPENLB_HOST_DEVICE
    constexpr unsigned int getIndex(unsigned int iRad,unsigned int iAz, unsigned int c) {
        return iRad*noHarmonicsAzimuth_*4+iAz*4+c;
    }

    OPENLB_HOST_DEVICE
    constexpr unsigned int getNoR() { return noHarmonicsRadial_; }

    OPENLB_HOST_DEVICE
    constexpr unsigned int getNoAz() { return noHarmonicsAzimuth_; }

    std::array<float,2> harmRadial(unsigned int iHarm, float rPos) {
        auto norm = iHarm == 0 ? 2*M_PI : M_PI;
        return {static_cast<float>(cos(rPos*iHarm*2*M_PI)/norm),static_cast<float>(sin(rPos*iHarm*2*M_PI)/norm)};
    }

    std::array<float,2> harmAz(unsigned int iHarm, float azPos) {
        auto norm = iHarm == 0 ? 1. : 0.5;
        return {static_cast<float>(cos(azPos*iHarm)/norm),static_cast<float>(sin(azPos*iHarm)/norm)};
    }
};

template<typename T>
struct VelocityAtRotorsTwoHarmonicCartesianVectors {
    VelocityAtRotorsTwoHarmonicCartesianVectors(std::tuple<SquareCellData<T>,unsigned int, T> data);

    OPENLB_HOST_DEVICE
    ~VelocityAtRotorsTwoHarmonicCartesianVectors();

    const unsigned int noHarmonics_;
    const T scalingFactor_;

    T** velocityCosCos;
    T** velocityCosSin;
    T** velocitySinCos;
    T** velocitySinSin;


    T** rotorCellFactorCosCos;
    T** rotorCellFactorCosSin;
    T** rotorCellFactorSinCos;
    T** rotorCellFactorSinSin;

    OPENLB_HOST_DEVICE
    constexpr unsigned int getIndex(unsigned int iX,unsigned int iY) {
        return iX*noHarmonics_+iY;
    }

    OPENLB_HOST_DEVICE
    constexpr unsigned int getNoHarmonics() { return noHarmonics_; }
};

#endif /* SRC_CONTRIB_COUPLING_COUPLINGDATA_H_ */
