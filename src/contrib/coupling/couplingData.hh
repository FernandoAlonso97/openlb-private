/*
 * couplingData.hh
 *
 *  Created on: Jul 2, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CONTRIB_COUPLING_COUPLINGDATA_HH_
#define SRC_CONTRIB_COUPLING_COUPLINGDATA_HH_

#include "core/cudaErrorHandler.h"
#include "rotor.hh"
#include "couplingData.h"
#include "interpolationKernel.h"
#include "core/util.h"
#include "contrib/cudaHelper/cublasHelper.h"

#include "cublas_v2.h"

template <typename RadialVal, typename AzimuthVal>
std::vector<float> getCellCoefficient(RadialVal radVal, AzimuthVal psiVal, float dA) {
    std::vector<float> cellCoeffs;
    for(auto iRadVal : radVal) {
        for(auto iPsiVal: psiVal) {
            cellCoeffs.push_back(iRadVal*iPsiVal*dA);
        }
    }
    return cellCoeffs;
}

template<typename T, typename WriteData, typename ReadData>
template<class InitTypeWrite, class InitTypeRead>
CouplingData<T,WriteData,ReadData>::CouplingData(InitTypeWrite& initDataWrite, InitTypeRead& initDataRead):
    writeData(initDataWrite),
    readData(initDataRead)
{}

template<typename T, typename WriteData, typename ReadData>
template<class InitType>
CouplingData<T,WriteData,ReadData>::CouplingData(InitType& initData):
    writeData(initData),
    readData(initData)
{}

template<typename T, typename WriteData, typename ReadData>
WriteData& CouplingData<T,WriteData,ReadData>::getWriteData()
{
    return writeData;
}

template<typename T, typename WriteData, typename ReadData>
ReadData& CouplingData<T,WriteData,ReadData>::getReadData()
{
    return readData;
}

template<typename T>
T LiftingLineData<T>::eps(LiftingLine lifLine, T rPosition, unsigned int iBlade) const {
	return eps(lifLine.c_[iBlade],lifLine.ar_[iBlade],rPosition);
}

template<typename T>
T LiftingLineData<T>::eps(float const c, float const ar, T rPosition) const {
	if(rPosition > 1.0000)
		return 0;

	auto const r = rPosition - 0.5;
	auto const ncell = nCell_;
	auto const c0 = 4/M_PI * c;
	auto const cstar = c0*std::sqrt(1-(2*r)*(2*r));


	auto const eps = 0.25*(n_max*1./ncell)*M_PI*ar*cstar;
	if(eps > eps2_)
		return eps;
	else
		return eps2_;
}

template<typename T>
VelocitiesAtRotorLinearVectors<T>::VelocitiesAtRotorLinearVectors(RotorCellData<T>& rotorCellData)
{
#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMallocManaged(&velocityMainMean,sizeof(T*)*3));
    HANDLE_ERROR(cudaMallocManaged(&velocityMainSin,sizeof(T*)*3));
    HANDLE_ERROR(cudaMallocManaged(&velocityMainCos,sizeof(T*)*3));

    for(unsigned int iDim = 0; iDim < 3; ++iDim)
    {
        HANDLE_ERROR(cudaMalloc(&velocityMainMean[iDim], sizeof(T)*rotorCellData._numberRotorCells));
        HANDLE_ERROR(cudaMalloc(&velocityMainSin[iDim], sizeof(T)*rotorCellData._numberRotorCells));
        HANDLE_ERROR(cudaMalloc(&velocityMainCos[iDim], sizeof(T)*rotorCellData._numberRotorCells));

        HANDLE_ERROR(cudaMemset(velocityMainMean[iDim],0,sizeof(T)*rotorCellData._numberRotorCells));
        HANDLE_ERROR(cudaMemset(velocityMainSin[iDim],0,sizeof(T)*rotorCellData._numberRotorCells));
        HANDLE_ERROR(cudaMemset(velocityMainCos[iDim],0,sizeof(T)*rotorCellData._numberRotorCells));
    }

    HANDLE_ERROR(cudaMalloc(&rotorCellFactorLinearCos, sizeof(T)*rotorCellData._numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&rotorCellFactorLinearSin, sizeof(T)*rotorCellData._numberRotorCells));
#else
    velocityMainMean = new T*[3];
    velocityMainSin  = new T*[3];
    velocityMainCos  = new T*[3];

    for(unsigned int iDim = 0; iDim < 3; ++iDim)
    {
        velocityMainMean[iDim] = new T[rotorCellData._numberRotorCells];
        velocityMainSin[iDim]  = new T[rotorCellData._numberRotorCells];
        velocityMainCos[iDim]  = new T[rotorCellData._numberRotorCells];

        memset(velocityMainMean[iDim],0,sizeof(T)*rotorCellData._numberRotorCells);
        memset(velocityMainSin[iDim],0,sizeof(T)*rotorCellData._numberRotorCells);
        memset(velocityMainCos[iDim],0,sizeof(T)*rotorCellData._numberRotorCells);
    }
#endif
    T* factorCos = new T[rotorCellData._numberRotorCells];
    T* factorSin = new T[rotorCellData._numberRotorCells];

    for(unsigned int iRotorCell = 0; iRotorCell < rotorCellData._numberRotorCells; ++iRotorCell)
    {
        factorCos[iRotorCell] = std::sin(rotorCellData._cellPositionAzimuthCPU[iRotorCell])
        *rotorCellData._cellPositionRadialCPU[iRotorCell]/4.;
        factorSin[iRotorCell] = std::cos(rotorCellData._cellPositionAzimuthCPU[iRotorCell])
        *rotorCellData._cellPositionRadialCPU[iRotorCell]/4.;
    }
#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMemcpy(rotorCellFactorLinearCos,factorCos,sizeof(T)*rotorCellData._numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(rotorCellFactorLinearSin,factorSin,sizeof(T)*rotorCellData._numberRotorCells,cudaMemcpyHostToDevice));
    delete [] factorCos;
    delete [] factorSin;
#else
    rotorCellFactorLinearCos = factorCos;
    rotorCellFactorLinearSin = factorSin;
#endif
}

template<typename T>
VelocitiesAtRotorLinearVectors<T>::~VelocitiesAtRotorLinearVectors()
{
#ifdef ENABLE_CUDA
#ifndef __CUDACC__
    for(unsigned int iDim = 0; iDim < 3; ++iDim)
    {
        HANDLE_ERROR(cudaFree(velocityMainMean[iDim]));
        HANDLE_ERROR(cudaFree(velocityMainSin[iDim]));
        HANDLE_ERROR(cudaFree(velocityMainCos[iDim]));
    }


    HANDLE_ERROR(cudaFree(velocityMainMean));
    HANDLE_ERROR(cudaFree(velocityMainSin));
    HANDLE_ERROR(cudaFree(velocityMainCos));

    HANDLE_ERROR(cudaFree(rotorCellFactorLinearCos));
    HANDLE_ERROR(cudaFree(rotorCellFactorLinearSin));

#endif
#else
    for(unsigned int iDim = 0; iDim < 3; ++iDim)
    {
        delete [] velocityMainMean[iDim];
        delete [] velocityMainSin[iDim];
        delete [] velocityMainCos[iDim];
    }

    delete [] velocityMainMean;
    delete [] velocityMainSin;
    delete [] velocityMainCos;

    delete [] rotorCellFactorLinearCos;
    delete [] rotorCellFactorLinearSin;
#endif
}


template<typename T>
VelocityAtRotorsTwoHarmonicVectors<T>::VelocityAtRotorsTwoHarmonicVectors(std::tuple<Rotor<T>, SquareCellData<T>,unsigned int, unsigned int,
        T, T, unsigned int, unsigned int, bool> const data):
        noHarmonicsRadial_(std::get<2>(data)),
        noHarmonicsAzimuth_(std::get<3>(data)),
        scalingFactor_(std::get<4>(data)),
		getInterpInflow_(std::get<8>(data))
{
    SquareCellData<T> rotorCellData = std::get<1>(data);
    Rotor<T> rotor = std::get<0>(data);
    size_t nCells = rotorCellData._numberRotorCells;
    auto center = rotor.getCenter();
    T deltaX = std::get<5>(data);
    size_t nCellX = rotor.getXLimits()[1]-rotor.getXLimits()[0] + 1;
    size_t nCellY = rotor.getYLimits()[1]-rotor.getYLimits()[0] + 1;

    std::cout << "cells X: " << nCellX << std::endl;
    std::cout << "cells Y: " << nCellX << std::endl;

    unsigned int nAzimuthPositions = std::get<7>(data);
    unsigned int nRadialPositons = std::get<6>(data);

    unsigned int nCoeffs = noHarmonicsRadial_*noHarmonicsAzimuth_*4;

    std::vector<float> radialPositions(nRadialPositons);
    std::vector<float> azimuthPositions(nAzimuthPositions);
    std::vector<float> xIndexInterp(radialPositions.size()*azimuthPositions.size());
    std::vector<float> yIndexInterp(radialPositions.size()*azimuthPositions.size());
    std::vector<float> rCoordsInterp(radialPositions.size()*azimuthPositions.size());
    std::vector<float> psiCoordsInterp(radialPositions.size()*azimuthPositions.size());

    std::set<size_t> indices;
    std::set<T> xIndices;
    std::set<T> yIndices;
    std::set<unsigned int> xIndicesInt;
    std::set<unsigned int> yIndicesInt;

#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMalloc(reinterpret_cast<void **>(&inflowVector), nCells * sizeof(float)));
    HANDLE_ERROR(cudaMalloc(reinterpret_cast<void **>(&interpolationMatrix), nCells*nCoeffs*sizeof(float)));
    HANDLE_ERROR(cudaMalloc(reinterpret_cast<void **>(&d_coefficients), nCoeffs*sizeof(float)));

    if(getInterpInflow_) {
		HANDLE_ERROR(cudaMalloc(reinterpret_cast<void **>(&inflowInterpVector), nAzimuthPositions*nRadialPositons*sizeof(float)));
		HANDLE_ERROR(cudaMalloc(reinterpret_cast<void **>(&gridInterpMatrix), nAzimuthPositions*nRadialPositons*nCells*sizeof(float)));
    }

    HANDLE_STATUS(cublasCreate(&_handle));
    HANDLE_STATUS(cublasSetMathMode(_handle, CUBLAS_TENSOR_OP_MATH));
#else
    assert(false);
#endif
    h_coefficients.resize(noHarmonicsRadial_*noHarmonicsAzimuth_*4);
    h_inflowInterpVector.resize(nAzimuthPositions*nRadialPositons);

    for(unsigned int iR = 0; iR < radialPositions.size(); ++iR)
        radialPositions[iR] = 0.+iR*1./(nRadialPositons-1);

    for(unsigned int iPsi = 0; iPsi < azimuthPositions.size(); ++iPsi)
        azimuthPositions[iPsi] = 0.+iPsi*2*M_PI/(nAzimuthPositions);

    T drdpsi = (radialPositions[1]-radialPositions[0])*(azimuthPositions[1]-azimuthPositions[0]);

    for(unsigned int iPsi = 0; iPsi < nAzimuthPositions; ++iPsi) {
        for(unsigned int iRad = 0; iRad < nRadialPositons; ++iRad) {
            xIndexInterp[olb::util::getCellIndex2D(iRad,iPsi,nAzimuthPositions)]       = (-radialPositions[iRad] * std::cos(azimuthPositions[iPsi]))*rotor.getRadiusUndim()+rotor.getCenter()[0]-rotor.getXLimits()[0];
            yIndexInterp[olb::util::getCellIndex2D(iRad,iPsi,nAzimuthPositions)]       = ( radialPositions[iRad] * std::sin(azimuthPositions[iPsi]))*rotor.getRadiusUndim()+rotor.getCenter()[1]-rotor.getYLimits()[0];
            rCoordsInterp[olb::util::getCellIndex2D(iRad,iPsi,nAzimuthPositions)]      =   radialPositions[iRad];
            psiCoordsInterp[olb::util::getCellIndex2D(iRad,iPsi,nAzimuthPositions)]    =   azimuthPositions[iPsi];
        }
    }

    float* h_interpolationMatrix = new float[nCells*nCoeffs*sizeof(float)] {};

    std::ofstream matrix("matrix.txt");

//#pragma omp parallel for
    for(unsigned int iHarmRad = 0; iHarmRad < noHarmonicsRadial_; ++iHarmRad) {
        for(unsigned int iHarmPsi = 0; iHarmPsi < noHarmonicsAzimuth_; ++iHarmPsi) {
            for(unsigned int iCellInt = 0; iCellInt < xIndexInterp.size(); ++iCellInt) {
                float r = rCoordsInterp[iCellInt];
                float psi = psiCoordsInterp[iCellInt];
                auto values = bilinearKernel(xIndexInterp[iCellInt],yIndexInterp[iCellInt]);
                auto cellFactors = getCellCoefficient(harmRadial(iHarmRad,r),harmAz(iHarmPsi,psi),drdpsi);

                for(unsigned int ii = 0; ii < cellFactors.size(); ++ii) {
                    for(auto iVal : values) {
                        unsigned int harmIndex = getIndex(iHarmRad,iHarmPsi,ii);

                        size_t index = olb::util::indexColumnMaj(static_cast<size_t>(nCoeffs),static_cast<size_t>(nCells),harmIndex,olb::util::getCellIndex2D(std::get<0>(iVal),std::get<1>(iVal),nCellY));
                        h_interpolationMatrix[index] += std::get<2>(iVal)*cellFactors[ii];
                    }

                }
            }
        }
    }

    if(getInterpInflow_){
		float* h_gridInterpMatrix = new float[nRadialPositons*nAzimuthPositions*nCells*sizeof(float)] {};
		for(unsigned int iCellInt = 0; iCellInt < xIndexInterp.size(); ++iCellInt) {
			float r = rCoordsInterp[iCellInt];
			float psi = psiCoordsInterp[iCellInt];
			auto values = bilinearKernel(xIndexInterp[iCellInt],yIndexInterp[iCellInt]);
				for(auto iVal : values) {
					size_t index = olb::util::indexColumnMaj(nRadialPositons*nAzimuthPositions,nCells,iCellInt,olb::util::getCellIndex2D(std::get<0>(iVal),std::get<1>(iVal),nCellY));
					h_gridInterpMatrix[index] = std::get<2>(iVal);
				}
		}
#ifdef ENABLE_CUDA
		if(getInterpInflow_)
			HANDLE_ERROR(cudaMemcpy(gridInterpMatrix,h_gridInterpMatrix,sizeof(float)*nRadialPositons*nAzimuthPositions*nCells,cudaMemcpyHostToDevice));
#endif
    	delete [] h_gridInterpMatrix;
	}

#ifdef ENABLE_CUDA
    HANDLE_STATUS(cublasSetVector(nCells*nCoeffs,sizeof(float),h_interpolationMatrix,1,interpolationMatrix,1));
    // HANDLE_STATUS(cublasSetVector(nRadialPositons*nAzimuthPositions*nCells,sizeof(float),h_gridInterpMatrix,1,gridInterpMatrix,1));
#endif
    delete [] h_interpolationMatrix;

}

template<typename T>
VelocityAtRotorsTwoHarmonicVectors<T>::~VelocityAtRotorsTwoHarmonicVectors()
{
#ifdef ENABLE_CUDA
#ifndef __CUDACC__
    HANDLE_ERROR(cudaFree(inflowVector));
    HANDLE_ERROR(cudaFree(interpolationMatrix));
    HANDLE_ERROR(cudaFree(gridInterpMatrix));
    HANDLE_ERROR(cudaFree(inflowInterpVector));
    HANDLE_ERROR(cudaFree(d_coefficients));
#endif
#else
#endif
}

template<typename T>
VelocityAtRotorsTwoHarmonicCartesianVectors<T>::VelocityAtRotorsTwoHarmonicCartesianVectors(std::tuple<SquareCellData<T>,unsigned int, T> data):
        noHarmonics_(std::get<1>(data)),
        scalingFactor_(std::get<2>(data))
{
    std::array<unsigned int,4> limits;
    SquareCellData<T> squareCellData = std::get<0>(data);
    unsigned int numberCells = squareCellData._numberRotorCells;

#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMallocManaged(&velocityCosCos,sizeof(T*)*3));
    HANDLE_ERROR(cudaMallocManaged(&velocityCosSin,sizeof(T*)*3));
    HANDLE_ERROR(cudaMallocManaged(&velocitySinCos,sizeof(T*)*3));
    HANDLE_ERROR(cudaMallocManaged(&velocitySinSin,sizeof(T*)*3));

    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii)
    {
        HANDLE_ERROR(cudaMalloc(&velocityCosCos[ii], sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMalloc(&velocityCosSin[ii], sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMalloc(&velocitySinCos[ii], sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMalloc(&velocitySinSin[ii], sizeof(T)*numberCells));

        HANDLE_ERROR(cudaMemset(velocityCosCos[ii],0,sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMemset(velocityCosSin[ii],0,sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMemset(velocitySinCos[ii],0,sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMemset(velocitySinSin[ii],0,sizeof(T)*numberCells));
    }

    HANDLE_ERROR(cudaMallocManaged(&rotorCellFactorCosCos, sizeof(T*)*noHarmonics_*noHarmonics_));
    HANDLE_ERROR(cudaMallocManaged(&rotorCellFactorCosSin, sizeof(T*)*noHarmonics_*noHarmonics_));
    HANDLE_ERROR(cudaMallocManaged(&rotorCellFactorSinCos, sizeof(T*)*noHarmonics_*noHarmonics_));
    HANDLE_ERROR(cudaMallocManaged(&rotorCellFactorSinSin, sizeof(T*)*noHarmonics_*noHarmonics_));

    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii)
    {
        HANDLE_ERROR(cudaMalloc(&rotorCellFactorCosCos[ii], sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMalloc(&rotorCellFactorCosSin[ii], sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMalloc(&rotorCellFactorSinCos[ii], sizeof(T)*numberCells));
        HANDLE_ERROR(cudaMalloc(&rotorCellFactorSinSin[ii], sizeof(T)*numberCells));

    }
#else
    velocityCosCos = new T*[3];
    velocityCosSin  = new T*[3];
    velocitySinCos  = new T*[3];
    velocitySinSin  = new T*[3];

    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii)
    {
        velocityCosCos[ii] = new T[numberCells];
        velocityCosSin[ii]  = new T[numberCells];
        velocitySinCos[ii]  = new T[numberCells];
        velocitySinSin[ii]  = new T[numberCells];

        memset(velocityCosCos[ii],0,sizeof(T)*numberCells);
        memset(velocityCosSin[ii],0,sizeof(T)*numberCells);
        memset(velocitySinCos[ii],0,sizeof(T)*numberCells);
        memset(velocitySinSin[ii],0,sizeof(T)*numberCells);
    }
#endif
    T** factorCosCos = new T*[noHarmonics_*noHarmonics_];
    T** factorCosSin = new T*[noHarmonics_*noHarmonics_];
    T** factorSinCos = new T*[noHarmonics_*noHarmonics_];
    T** factorSinSin = new T*[noHarmonics_*noHarmonics_];

    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii)
    {
        factorCosCos[ii]  = new T[numberCells];
        factorCosSin[ii]  = new T[numberCells];
        factorSinCos[ii]  = new T[numberCells];
        factorSinSin[ii]  = new T[numberCells];
    }

    std::ofstream cellFactors("cellFactors.txt");
    cellFactors << "cc,cs,sc,ss,iX,iY,index,globalIndex" << std::endl;

    std::ofstream cellInfo("cellInfo.txt");
    cellInfo << "index,x,y,dA,globalIndex" << std::endl;


    for(unsigned int iRotorCell = 0; iRotorCell < numberCells; ++iRotorCell) {
        for(unsigned int iX = 0; iX < noHarmonics_; ++iX) {
            for(unsigned int iY = 0; iY < noHarmonics_; ++iY) {

                T norm = 1./squareCellData._cellSize;
                if(iX == 0)
                    norm *= 2;
                if(iY == 0)
                    norm *= 2;

                const unsigned int index = getIndex(iX,iY);

                factorCosCos[index][iRotorCell] = std::cos(iX*M_PI*squareCellData._cellPosition1CPU[iRotorCell])*
                        std::cos(iY*M_PI*squareCellData._cellPosition2CPU[iRotorCell])/norm;
                factorCosSin[index][iRotorCell] = std::cos(iX*M_PI*squareCellData._cellPosition1CPU[iRotorCell])*
                        std::sin(iY*M_PI*squareCellData._cellPosition2CPU[iRotorCell])/norm;
                factorSinCos[index][iRotorCell] = std::sin(iX*M_PI*squareCellData._cellPosition1CPU[iRotorCell])*
                        std::cos(iY*M_PI*squareCellData._cellPosition2CPU[iRotorCell])/norm;
                factorSinSin[index][iRotorCell] = std::sin(iX*M_PI*squareCellData._cellPosition1CPU[iRotorCell])*
                        std::sin(iY*M_PI*squareCellData._cellPosition2CPU[iRotorCell])/norm;

                cellFactors << factorCosCos[index][iRotorCell] << "," << factorCosSin[index][iRotorCell] << "," << factorSinCos[index][iRotorCell] <<
                        "," << factorSinSin[index][iRotorCell] << "," << iX << "," << iY << "," << iRotorCell << "," << squareCellData._cellIndexCPU[iRotorCell] << std::endl;
            }
        }
        cellInfo << iRotorCell << "," << squareCellData._cellPosition1CPU[iRotorCell] << "," << squareCellData._cellPosition2CPU[iRotorCell] <<
                "," << squareCellData._cellSize << "," << squareCellData._cellIndexCPU[iRotorCell] << std::endl;
    }
#ifdef ENABLE_CUDA

    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii) {
        HANDLE_ERROR(cudaMemcpy(rotorCellFactorCosCos[ii],factorCosCos[ii],sizeof(T)*numberCells,cudaMemcpyHostToDevice));
        HANDLE_ERROR(cudaMemcpy(rotorCellFactorCosSin[ii],factorCosSin[ii],sizeof(T)*numberCells,cudaMemcpyHostToDevice));
        HANDLE_ERROR(cudaMemcpy(rotorCellFactorSinCos[ii],factorSinCos[ii],sizeof(T)*numberCells,cudaMemcpyHostToDevice));
        HANDLE_ERROR(cudaMemcpy(rotorCellFactorSinSin[ii],factorSinSin[ii],sizeof(T)*numberCells,cudaMemcpyHostToDevice));
    }

    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii) {
        delete [] factorCosCos[ii];
        delete [] factorCosSin[ii];
        delete [] factorSinCos[ii];
        delete [] factorSinSin[ii];
    }

    delete [] factorCosCos;
    delete [] factorCosSin;
    delete [] factorSinCos;
    delete [] factorSinSin;
#else
    rotorCellFactorCosCos = factorCosCos;
    rotorCellFactorCosSin = factorCosSin;
    rotorCellFactorSinCos = factorSinCos;
    rotorCellFactorSinSin = factorSinSin;
#endif
}

template<typename T>
VelocityAtRotorsTwoHarmonicCartesianVectors<T>::~VelocityAtRotorsTwoHarmonicCartesianVectors()
{
#ifndef __CUDACC__
#ifdef ENABLE_CUDA
    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii)
    {
        HANDLE_ERROR(cudaFree(velocityCosCos[ii]));
        HANDLE_ERROR(cudaFree(velocityCosSin[ii]));
        HANDLE_ERROR(cudaFree(velocitySinCos[ii]));
        HANDLE_ERROR(cudaFree(velocitySinSin[ii]));

        HANDLE_ERROR(cudaFree(rotorCellFactorCosCos[ii]));
        HANDLE_ERROR(cudaFree(rotorCellFactorCosSin[ii]));
        HANDLE_ERROR(cudaFree(rotorCellFactorSinCos[ii]));
        HANDLE_ERROR(cudaFree(rotorCellFactorSinSin[ii]));
    }

    HANDLE_ERROR(cudaFree(velocityCosCos));
    HANDLE_ERROR(cudaFree(velocityCosSin));
    HANDLE_ERROR(cudaFree(velocitySinCos));
    HANDLE_ERROR(cudaFree(velocitySinSin));

    HANDLE_ERROR(cudaFree(rotorCellFactorCosCos));
    HANDLE_ERROR(cudaFree(rotorCellFactorCosSin));
    HANDLE_ERROR(cudaFree(rotorCellFactorSinCos));
    HANDLE_ERROR(cudaFree(rotorCellFactorSinSin));

#else
    for(unsigned int ii = 0; ii < noHarmonics_*noHarmonics_; ++ii)
    {
        delete [] velocityCosCos[ii];
        delete [] velocityCosSin[ii];
        delete [] velocitySinCos[ii];
        delete [] velocitySinSin[ii];

        delete [] rotorCellFactorCosCos[ii];
        delete [] rotorCellFactorCosSin[ii];
        delete [] rotorCellFactorSinCos[ii];
        delete [] rotorCellFactorSinSin[ii];
    }

    delete [] velocityCosCos;
    delete [] velocityCosSin;
    delete [] velocitySinCos;
    delete [] velocitySinSin;

    delete [] rotorCellFactorCosCos;
    delete [] rotorCellFactorCosSin;
    delete [] rotorCellFactorSinCos;
    delete [] rotorCellFactorSinSin;
#endif
#endif
}


#endif /* SRC_CONTRIB_COUPLING_COUPLINGDATA_HH_ */


