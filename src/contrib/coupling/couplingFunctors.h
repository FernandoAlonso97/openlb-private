#ifndef COUPLING_FUNCTORS_H
#define COUOLING_FUNCTORS_H


#include "contrib/MemorySpace/memory_spaces.h"
#include "core/util.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "mpi.h"

namespace olb{

VelocityAtRotorsLinear reduceVelocity (VelocityAtRotorsLinear& vel, int masterRank)
{
  VelocityAtRotorsLinear tmp;
    MPI_Reduce(&(vel.velocityMainMean[0]),&(tmp.velocityMainMean[0]),3,MPI_FLOAT,MPI_SUM,masterRank,MPI_COMM_WORLD);
    MPI_Reduce(&(vel.velocityMainSin[0]),&(tmp.velocityMainSin[0]),3,MPI_FLOAT,MPI_SUM,masterRank,MPI_COMM_WORLD);
    MPI_Reduce(&(vel.velocityMainCos[0]),&(tmp.velocityMainCos[0]),3,MPI_FLOAT,MPI_SUM,masterRank,MPI_COMM_WORLD);

    MPI_Reduce(&(vel.velocityTailMean[0]),&(tmp.velocityTailMean[0]),3,MPI_FLOAT,MPI_SUM,masterRank,MPI_COMM_WORLD);
    MPI_Reduce(&(vel.velocityTailSin[0]),&(tmp.velocityTailSin[0]),3,MPI_FLOAT,MPI_SUM,masterRank,MPI_COMM_WORLD);
    MPI_Reduce(&(vel.velocityTailCos[0]),&(tmp.velocityTailCos[0]),3,MPI_FLOAT,MPI_SUM,masterRank,MPI_COMM_WORLD);

    return tmp;
}

template<typename T>
OPENLB_HOST_DEVICE
bool isInsideHollowCircleXY (std::vector<T> center, T outerRadius,T innerRadius,T X,T Y)
{
      T radialPositionUndim = std::sqrt(std::pow(X-center[0],2)+std::pow(Y-center[1],2))/outerRadius;
      if (radialPositionUndim >= innerRadius/outerRadius && radialPositionUndim <=1)
      {
        return true;
      } 
      else 
      {
        return false;
      }

}

//S: identify cells inside a rotor, O: fkt called is free funct. L: no inheritance, I: has specialized interface D: no dependencies 
template<typename T>
class RotorDiskSelectFunctor{

  public:

    RotorDiskSelectFunctor() = delete;

    RotorDiskSelectFunctor(std::vector<T> globalCenter, T globalOuterRadius, T globalInnerRadius):
      globalCenter_(std::move(globalCenter)),
      globalOuterRadius_(globalOuterRadius),
      globalInnerRadius_(globalInnerRadius)
    {}

OPENLB_HOST_DEVICE
    bool operator() (T iX, T iY)
    {
      return isInsideHollowCircleXY (globalCenter_,globalOuterRadius_,globalInnerRadius_, iX,iY);
    }

  private:
      std::vector<T> globalCenter_ = std::vector<T>(3);
      T globalOuterRadius_ = 0;
      T globalInnerRadius_ = 0;

};

//S: check if Z value matches,O: just to simple so be extensible->rewrite in case, L: no inheritance,I:helps segregation when dim of input changes,D:allows any callable type that can be copied
template<typename T, typename Callable>
class SelectXYPlane {
  public:
    SelectXYPlane() = delete;

    SelectXYPlane(T ZValue, Callable call):zValue(ZValue),callable(call) {}

OPENLB_HOST_DEVICE
    bool operator() (T X, T Y, T Z)
    {
      if (Z == zValue)
      {
        return callable(X,Y);
      }
      else
        return false;
    }

  private:
    T zValue = 0;
    Callable callable;

};

//S selects only local cells, O:just a hull for isLocal call, L: no inheritance,I segregates interface actually,D: relies on an implementation -> SubDomainInformation should be refactored
template<typename T, typename Lattice>
class CheckLocalityF{

  public:
    CheckLocalityF() = delete;

    CheckLocalityF(SubDomainInformation<T,Lattice> localSubDomain): localSubDomain_(localSubDomain) {}

OPENLB_HOST_DEVICE
    bool operator() (T iX, T iY, T iZ) const
    {
      Index3D localIndex;
      return localSubDomain_.isLocal(iX,iY,iZ, localIndex);
    }

  private:
    SubDomainInformation<T,Lattice> const localSubDomain_;
};

//S:implements && operator for functors. O:can be called recursively, can be extended by overloading && L: no inh I:only forwards to other interfaces of same type. D: depends only on callables(must be copiable) via templates.
template<typename CallA, typename CallB>
class AandBF{

  public:
    AandBF() = delete;

    AandBF(CallA calla, CallB callb): calla_(calla),callb_(callb) {}

OPENLB_HOST_DEVICE
    bool operator() (T iX, T iY, T iZ)
    {
      return calla_(iX,iY,iZ) && callb_(iX,iY,iZ);
    }

  private:
      CallA calla_;
      CallB callb_;
};

// template<typename CallA, typename CallB>
// OPENLB_HOST_DEVICE
// AandBF<CallA,CallB> operator && (CallA calla, CallB callb)
// {
   // return AandBF<CallA,CallB>(calla,callb);
// }

//S: returns localIndex from isLocal of SubdomainInfo, O:non-final. L: no inh I:segregates Interfaces of isLocal D:depends on impl of SubDomainInfo and SpaceFillingCurve->this should be changed into a separate callable
template<typename T, typename Lattice>
class LocalIndexFromSubDomainF{
  
  public:
    LocalIndexFromSubDomainF() = delete;

    LocalIndexFromSubDomainF(SubDomainInformation<T,Lattice> localSubDomain): localSubDomain_(localSubDomain) {}

OPENLB_HOST_DEVICE
    std::size_t operator() (T iX, T iY,T iZ) const
    {
      Index3D localIndex;
      localSubDomain_.isLocal(iX,iY,iZ,localIndex);
      return util::getCellIndex3D(localIndex[0],localIndex[1],localIndex[2],localSubDomain_.localGridSize()[1],localSubDomain_.localGridSize()[2]);
    }

  private:
    SubDomainInformation<T,Lattice> const localSubDomain_;

};

template<typename Memory, typename T, typename Lattice, typename Selector, typename Callable>
Memory globalDomainLoop (SubDomainInformation<T,Lattice> const globalDomain,Selector selector,Callable call)
{
  size_t size=0;
  for (size_t iX= globalDomain.globalIndexStart[0];iX<globalDomain.globalIndexEnd[0];++iX)
    for (size_t iY= globalDomain.globalIndexStart[1];iY<globalDomain.globalIndexEnd[1];++iY)
      for (size_t iZ= globalDomain.globalIndexStart[2];iZ<globalDomain.globalIndexEnd[2];++iZ)
      {
        if (selector(iX,iY,iZ))
         ++ size; 
      }

  Memory mem(size);

  size_t idx=0;
  for (size_t iX= globalDomain.globalIndexStart[0];iX<globalDomain.globalIndexEnd[0];++iX)
    for (size_t iY= globalDomain.globalIndexStart[1];iY<globalDomain.globalIndexEnd[1];++iY)
      for (size_t iZ= globalDomain.globalIndexStart[2];iZ<globalDomain.globalIndexEnd[2];++iZ)
      {
        if (selector(iX,iY,iZ))
        {
           mem[idx] = call(iX,iY,iZ);
           ++idx; 
        }
      }
  return mem;
}

template<typename T,typename Callable,typename Memory>
class UseBufferedIndicesF {

  public:
    UseBufferedIndicesF() = delete;

    UseBufferedIndicesF(Callable call,Memory memory): callable(call),indices_(std::move(memory)) {}

    template<typename ... ARGS>
OPENLB_HOST_DEVICE
    typename Callable::ReturnType
    operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex,ARGS ... args)
    {
      size_t cellIndex = indices_[threadIndex];
      return callable(cellData,cellIndex,std::forward<ARGS>(args)...);
    }

  private:
    Callable callable;
    Memory   indices_;
};

template<typename T, class Lattice>
OPENLB_HOST_DEVICE
T computeRotorAzimuth(T const * globalRotorCenter,SubDomainInformation<T,Lattice> const subDomInfo, size_t const localCellIndex)
{
  size_t indices[3];
  util::getCellIndices3D(localCellIndex,subDomInfo.localGridSize()[1],subDomInfo.localGridSize()[2],indices);
  Index3D localIndex(indices[0],indices[1],indices[2],subDomInfo.localSubDomain);
  Index3D globalIndex = subDomInfo.getGlobalIndex(localIndex);
#ifdef ENABLE_CUDA
  return atan2(static_cast<T>(globalIndex[0])-globalRotorCenter[0],static_cast<T>(globalIndex[1])-globalRotorCenter[1]);
#else
  return std::atan2(static_cast<T>(globalIndex[0]-globalRotorCenter[0],static_cast<T>(globalIndex[1])-globalRotorCenter[1]));
#endif
}

//S: calc Thrust per cell and write it->2things O:not open for extension,as it does not use a return value L:no inheritance I:write and calculation values mixed in interface D:depends on implementations, as it has an i do interface
template<typename T,typename Descriptor>
class WriteHarmonicThrustFunctor{

  public:

    using ReturnType = void;

    WriteHarmonicThrustFunctor() = delete;

    WriteHarmonicThrustFunctor(std::vector<T> globalRotorCenter,SubDomainInformation<T,Descriptor> subDomInfo, T forceFactor): globalRotorCenter_{globalRotorCenter[0],globalRotorCenter[1],globalRotorCenter[2]},subDomInfo_(subDomInfo),forceFactor_(forceFactor) {}

    template<unsigned Harmonicscount>
OPENLB_HOST_DEVICE
    ReturnType operator()  (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, const size_t cellIndex,ThrustAtRotorsHarmonic<Harmonicscount> thrustRotor)
    {
      float azimuth =  computeRotorAzimuth(globalRotorCenter_,subDomInfo_,cellIndex);
      
      float thrustPerCell[3] = {0,0,0};

      for (unsigned int iHarmonics=0; iHarmonics<Harmonicscount; ++iHarmonics)
      {
        thrustPerCell[0] += std::cos((iHarmonics+1)*azimuth)*thrustRotor.presMainCosX[iHarmonics];
        thrustPerCell[0] += std::sin((iHarmonics+1)*azimuth)*thrustRotor.presMainSinX[iHarmonics];

        thrustPerCell[1] += std::cos((iHarmonics+1)*azimuth)*thrustRotor.presMainCosY[iHarmonics];
        thrustPerCell[1] += std::sin((iHarmonics+1)*azimuth)*thrustRotor.presMainSinY[iHarmonics];

        thrustPerCell[2] += std::cos((iHarmonics+1)*azimuth)*thrustRotor.presMainCosZ[iHarmonics];
        thrustPerCell[2] += std::sin((iHarmonics+1)*azimuth)*thrustRotor.presMainSinZ[iHarmonics];
      }

      thrustPerCell[0] += thrustRotor.presMainMeanX;
      thrustPerCell[1] += thrustRotor.presMainMeanY;
      thrustPerCell[2] += thrustRotor.presMainMeanZ;

      thrustPerCell[0] *= 1;//rotorCellWeight[threadIndex];
      thrustPerCell[1] *= 1;//rotorCellWeight[threadIndex];
      thrustPerCell[2] *= 1;//rotorCellWeight[threadIndex];


      cellData[Descriptor::forceIndex+0][cellIndex] = thrustPerCell[0]*forceFactor_;
      cellData[Descriptor::forceIndex+1][cellIndex] = thrustPerCell[1]*forceFactor_;
      cellData[Descriptor::forceIndex+2][cellIndex] = thrustPerCell[2]*forceFactor_;
    
    }

  private:

    T const globalRotorCenter_[3];
    SubDomainInformation<T,Descriptor> const subDomInfo_;
    T const forceFactor_;

};

template<typename T, class Lattice>
OPENLB_HOST_DEVICE
T computeDimlessRadius(T const * globalRotorCenter,SubDomainInformation<T,Lattice> const subDomInfo, size_t const localCellIndex,T const globalRadius)
{
  size_t indices[3];
  util::getCellIndices3D(localCellIndex,subDomInfo.localGridSize()[1],subDomInfo.localGridSize()[2],indices);
  Index3D localIndex(indices[0],indices[1],indices[2],subDomInfo.localSubDomain);
  Index3D globalIndex = subDomInfo.getGlobalIndex(localIndex);
#ifdef ENABLE_CUDA
  return sqrtf(powf(globalIndex[0]-globalRotorCenter[0],2)+powf(globalIndex[1]-globalRotorCenter[1],2))/globalRadius;
#else
  return std::sqrt(std::pow(globalIndex[0]-globalRotorCenter[0],2)+std::pow(globalIndex[1]-globalRotorCenter[1],2))/globalRadius;
#endif

}

//S:sums values into internal data structure. Provides access to the structure (should be done via return value).O: as no return value is used no extensibility. L: no inheritance I: okay, everything segregated D:i do interface is bad
template<typename T,typename Descriptor, template<typename U> class Memory>
class ReadLinearVelocityFunctor{
  public:

    using ReturnType = void;

    ReadLinearVelocityFunctor() = delete;

    ReadLinearVelocityFunctor(std::vector<T> globalRotorCenter,
        SubDomainInformation<T,Descriptor> subDomInfo,T velocityFactor,T globalRadius): globalRotorCenter_{globalRotorCenter[0],globalRotorCenter[1],globalRotorCenter[2]},subDomInfo_(subDomInfo),velocityFactor_(velocityFactor),globalRadius_(globalRadius),data_(Memory<VelocityAtRotorsLinear>(1))
    {} 

OPENLB_HOST_DEVICE
    ReturnType operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, const size_t cellIndex)
    {
      data_[0] = VelocityAtRotorsLinear();
      float azimuth =  computeRotorAzimuth(globalRotorCenter_,subDomInfo_,cellIndex);
      T radialPosition = computeDimlessRadius(globalRotorCenter_,subDomInfo_,cellIndex,globalRadius_);
      T velocity[3] = {0,0,0};
      for(unsigned int iDim = 0; iDim < Descriptor::d; ++iDim)
      {
          velocity[iDim] = cellData[Descriptor::uIndex+iDim][cellIndex]*velocityFactor_;
          atomicAdd(&(data_[0].velocityMainMean[iDim]),static_cast<float>(velocity[iDim]));
          atomicAdd(&(data_[0].velocityMainSin[iDim]),static_cast<float>(0.25*radialPosition*sin(azimuth)*velocity[iDim]));
          atomicAdd(&(data_[0].velocityMainCos[iDim]),static_cast<float>(0.25*radialPosition*cos(azimuth)*velocity[iDim]));
      }
    
    }

    Memory<VelocityAtRotorsLinear> getData()
    {
      return data_;
    }

  private:

    T const globalRotorCenter_[3];
    SubDomainInformation<T,Descriptor> const subDomInfo_;
    T const velocityFactor_;
    T const globalRadius_;
    Memory<VelocityAtRotorsLinear> data_;

};



} // eo Namespace
#endif
