/*
 * couplingImplementation.h
 *
 *  Created on: Jul 2, 2019
 *      Author: ga69kiq
 */

#ifndef SRC_CONTRIB_COUPLING_COUPLINGIMPLEMENTATION_H_
#define SRC_CONTRIB_COUPLING_COUPLINGIMPLEMENTATION_H_

#include "core/config.h"
#include "contrib/communication/NetworkDataStructures.h"
#include "couplingData.h"

#include "boost/math/special_functions/bessel.hpp"

#ifdef ENABLE_CUDA
#include <thrust/binary_search.h>
#include <thrust/distance.h>
#endif

#include <cmath>

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void writeRotorThrustImplementation(ThrustAtRotorsHarmonic<10> const & thrustRotor,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t threadIndex, T const forceFactor, size_t const * const rotorCellIndex,
        T const * const rotorCellPositionAzimuth, T const * const rotorCellWeight)
{
    size_t index = rotorCellIndex[threadIndex];
    float thrustPerCell[3] = {0,0,0};

    for (unsigned int iHarmonics=0; iHarmonics<10; ++iHarmonics)
    {
      thrustPerCell[0] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainCosX[iHarmonics];
      thrustPerCell[0] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainSinX[iHarmonics];

      thrustPerCell[1] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainCosY[iHarmonics];
      thrustPerCell[1] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainSinY[iHarmonics];

      thrustPerCell[2] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainCosZ[iHarmonics];
      thrustPerCell[2] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainSinZ[iHarmonics];
    }

    thrustPerCell[0] += thrustRotor.presMainMeanX;
    thrustPerCell[1] += thrustRotor.presMainMeanY;
    thrustPerCell[2] += thrustRotor.presMainMeanZ;

    thrustPerCell[0] *= rotorCellWeight[threadIndex];
    thrustPerCell[1] *= rotorCellWeight[threadIndex];
    thrustPerCell[2] *= rotorCellWeight[threadIndex];


    cellData[Descriptor::forceIndex+0][index] = thrustPerCell[0]*forceFactor;
    cellData[Descriptor::forceIndex+1][index] = thrustPerCell[1]*forceFactor;
    cellData[Descriptor::forceIndex+2][index] = thrustPerCell[2]*forceFactor;

}

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void writeRotorThrustImplementation(ThrustAtRotorsTwoHarmonic const & thrustRotor,
		T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		size_t threadIndex, T const forceFactor, size_t const * const rotorCellIndex,
		T const * const rotorCellPositionAzimuth, T const * const rotorCellPositionRadial, T const * const rotorCellWeight, T const cellSize)
{
	size_t index = rotorCellIndex[threadIndex];
	float thrustPerCell[3] = {0,0,0};

	for(unsigned int iRad = 0; iRad < thrustRotor.noHarmonicsRadial; ++iRad)
		for(unsigned int iAz = 0; iAz < thrustRotor.noHarmonicsAzimuth; ++iAz) {
			unsigned int index = thrustRotor.getIndex(iRad,iAz);
			const float radial = rotorCellPositionRadial[threadIndex];
			const float azimuth = rotorCellPositionAzimuth[threadIndex];

			thrustPerCell[2] += thrustRotor.cc[index]*std::cos(iRad*2*M_PI*radial)*std::cos(iAz*azimuth) +
								thrustRotor.cs[index]*std::cos(iRad*2*M_PI*radial)*std::sin(iAz*azimuth) +
								thrustRotor.sc[index]*std::sin(iRad*2*M_PI*radial)*std::cos(iAz*azimuth) +
								thrustRotor.ss[index]*std::sin(iRad*2*M_PI*radial)*std::sin(iAz*azimuth);
		}

	thrustPerCell[0] *= rotorCellWeight[threadIndex]*cellSize;
	thrustPerCell[1] *= rotorCellWeight[threadIndex]*cellSize;
	thrustPerCell[2] *= rotorCellWeight[threadIndex]*cellSize;

   cellData[Descriptor::forceIndex+0][index] = thrustPerCell[0]*forceFactor;
   cellData[Descriptor::forceIndex+1][index] = thrustPerCell[1]*forceFactor;
   cellData[Descriptor::forceIndex+2][index] = thrustPerCell[2]*forceFactor;

}

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void writeRotorThrustImplementation(ThrustAtRotorsBesselHarmonic const & thrustRotor,
		T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		size_t threadIndex, T const forceFactor, size_t const * const rotorCellIndex,
		T const * const rotorCellPositionAzimuth, T const * const rotorCellPositionRadial, T const * const rotorCellWeight, double const cellSize)
{
	size_t index = rotorCellIndex[threadIndex];
	float thrustPerCell[3] = {0,0,0};

	int alpha = 0;

	for(unsigned int iRad = 0; iRad < thrustRotor.noHarmonicsRadial; ++iRad)
		for(unsigned int iAz = 0; iAz < thrustRotor.noHarmonicsAzimuth; ++iAz) {
			unsigned int index = thrustRotor.getIndex(iRad,iAz);
			const float radial = rotorCellPositionRadial[threadIndex];
			const float azimuth = rotorCellPositionAzimuth[threadIndex];

			thrustPerCell[2] += thrustRotor.s[index]*jn(alpha,radial*thrustRotor.roots[iRad]*radial)*std::sin(azimuth*iAz)
					+ thrustRotor.c[index]*jn(alpha,thrustRotor.roots[iRad]*radial)*std::cos(azimuth*iAz);
			if(threadIndex == 100) {
				// printf("%f,%f,%u,%u\n",roots[iRad],jn(alpha,thrustRotor.roots[iRad]))
			}

		}

	thrustPerCell[0] *= rotorCellWeight[threadIndex]*cellSize;
	thrustPerCell[1] *= rotorCellWeight[threadIndex]*cellSize;
	thrustPerCell[2] *= rotorCellWeight[threadIndex]*cellSize;

   cellData[Descriptor::forceIndex+0][index] = thrustPerCell[0]*forceFactor;
   cellData[Descriptor::forceIndex+1][index] = thrustPerCell[1]*forceFactor;
   cellData[Descriptor::forceIndex+2][index] = thrustPerCell[2]*forceFactor;

}

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void writeRotorThrustImplementationRelaxed(ThrustAtRotorsHarmonic<10> const & thrustRotor,
        RelaxationData<T> const couplingData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t threadIndex, T const forceFactor, size_t const * const rotorCellIndex,
        T const * const rotorCellPositionAzimuth, T const * const rotorCellWeight)
{
    size_t index = rotorCellIndex[threadIndex];
    float thrustPerCell[3] = {0,0,0};

    for (unsigned int iHarmonics=0; iHarmonics<10; ++iHarmonics)
    {
      thrustPerCell[0] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainCosX[iHarmonics];
      thrustPerCell[0] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainSinX[iHarmonics];

      thrustPerCell[1] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainCosY[iHarmonics];
      thrustPerCell[1] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainSinY[iHarmonics];

      thrustPerCell[2] += std::cos((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainCosZ[iHarmonics];
      thrustPerCell[2] += std::sin((iHarmonics+1)*rotorCellPositionAzimuth[threadIndex])*thrustRotor.presMainSinZ[iHarmonics];
    }

    thrustPerCell[0] += thrustRotor.presMainMeanX;
    thrustPerCell[1] += thrustRotor.presMainMeanY;
    thrustPerCell[2] += thrustRotor.presMainMeanZ;

    for(unsigned int iDim = 0; iDim < Descriptor::d; ++iDim)
    {
        thrustPerCell[iDim] *= rotorCellWeight[threadIndex];
		if(cellData[Descriptor::forceIndex+iDim][index] != 0)
	        cellData[Descriptor::forceIndex+iDim][index] += couplingData.relaxationFactor_*
                (thrustPerCell[iDim]*forceFactor-cellData[Descriptor::forceIndex+iDim][index]);
		else
			cellData[Descriptor::forceIndex+iDim][index] = thrustPerCell[iDim]*forceFactor;
    }

}

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void writeRotorThrustImplementation(LiftingLine const & thrustRotor,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t threadIndex, T const forceFactor, size_t const * const rotorCellIndex,
        T const * const rotorCellPositionAzimuth, T const * const rotorCellPositionRadial, T const * const rotorCellWeight, LiftingLineData<T> const & lifLineData )
{

	size_t latIndex = rotorCellIndex[threadIndex];

	T az = rotorCellPositionAzimuth[threadIndex];
	T r = rotorCellPositionRadial[threadIndex];
	T weight = rotorCellWeight[threadIndex];

	T thrust = 0;

	for(unsigned int iBlade = 0; iBlade < thrustRotor.nBlades_; ++iBlade) {
		T distPsi = abs(az-thrustRotor.az_[iBlade]);
		if(distPsi > M_PI)
			distPsi = abs(distPsi-2*M_PI);

		T const sig = lifLineData.eps(thrustRotor,r,iBlade);
		if(sig < 1e-7)
			continue;

		T const sig2 = sig*sig;
		T arcl2 = distPsi*distPsi*r*r;

		auto const rStart = thrustRotor.begin(iBlade);
		auto const rEnd = thrustRotor.end(iBlade);
		auto rIter = rStart;

		int pos = 0;
		while(rIter != rEnd) {
			if(*rIter > static_cast<float>(r)) {
				pos = thrust::distance(rStart,rIter);
				break;
			}
			++rIter;
		}

//		for(unsigned int iRad = 0; iRad < thrustRotor.noHarmonicsRadial; ++iRad) {
//			unsigned int index = LiftingLine::getIndex(iBlade,iRad);
//			thrust +=  (thrustRotor.c[index]*std::cos(iRad*2*M_PI*r) +
//					   thrustRotor.s[index]*std::cos(iRad*2*M_PI*r)) *
//					   1/(sig*4.442882938)*std::exp(-0.5*(arcl2/sig2))*lifLineData.cellSize_*weight*forceFactor;
//		}


		auto indexT = LiftingLine::getIndexT(iBlade, pos);
		// thrust += thrustRotor.t[indexT]*1/(sig*4.442882938)*std::exp(-0.5*(arcl2/sig2))*lifLineData.cellSize_*weight*forceFactor;
		thrust += thrustRotor.t[indexT]*1/(sig*2.50662827463)*std::exp(-0.5*(arcl2/sig2))*lifLineData.cellSize_*weight*forceFactor;

	}

	cellData[Descriptor::forceIndex+2][latIndex] = 1.*(thrust-cellData[Descriptor::forceIndex+2][latIndex])+
		cellData[Descriptor::forceIndex+2][latIndex];
}

#ifdef ENABLE_CUDA

template<typename T, class Descriptor, typename ReceiveData, typename...Params>
__global__ void writeRotorThrust(ReceiveData const thrustRotor, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
         size_t const numRotorCells, T const forceFactor, Params...args)
{
    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= numRotorCells)
      return;

	writeRotorThrustImplementation<T,Descriptor>(thrustRotor, cellData, threadIndex, forceFactor, std::forward<Params>(args)...);
}

template<typename T, class Descriptor, typename ReceiveData, typename CouplingData, typename...Params>
__global__ void writeRotorThrustRelaxed(ReceiveData const thrustRotor, CouplingData const couplingData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
         size_t const numRotorCells, T const forceFactor, Params...args)
{
    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= numRotorCells)
      return;

    writeRotorThrustImplementationRelaxed<T,Descriptor>(thrustRotor, couplingData, cellData, threadIndex, forceFactor, std::forward<Params>(args)...);
}

#endif

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void readVelocitiesImplementation(VelocitiesAtRotorLinearVectors<T>& velocitiesRotor,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t const threadIndex, T const undimVelocity, size_t const * const rotorCellIndex, T const * const rotorCellWeight)
{
    size_t index = rotorCellIndex[threadIndex];
    T velocity[3];

    if(rotorCellWeight[threadIndex] == 1.)
    {
        for(unsigned int iDim = 0; iDim < Descriptor::d; ++iDim)
        {
            velocity[iDim] = cellData[Descriptor::uIndex+iDim][index]*undimVelocity;
            velocitiesRotor.velocityMainMean[iDim][threadIndex] = velocity[iDim];
            velocitiesRotor.velocityMainSin[iDim][threadIndex] = velocitiesRotor.rotorCellFactorLinearSin[threadIndex]*velocity[iDim];
            velocitiesRotor.velocityMainCos[iDim][threadIndex] = velocitiesRotor.rotorCellFactorLinearCos[threadIndex]*velocity[iDim];
        }
    }

}

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void readVelocitiesImplementation(float * const OPENLB_RESTRICT inflowVector,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t const threadIndex, T const undimVelocity, size_t const * const rotorCellIndex, unsigned int velocityPos)
{
    size_t index = rotorCellIndex[threadIndex];
    inflowVector[threadIndex] = cellData[Descriptor::uIndex+velocityPos][index]*undimVelocity;

}

template<typename T, class Descriptor>
OPENLB_HOST_DEVICE
void readVelocitiesImplementation(VelocityAtRotorsTwoHarmonicCartesianVectors<T>& velocitiesRotor,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t const threadIndex, T const undimVelocity, T const * const rotorCellWeight, size_t const * const rotorCellIndex)
{
    size_t index = rotorCellIndex[threadIndex];
    T velocity[3];

    if(rotorCellWeight[threadIndex] > 0) {
        velocity[2] = cellData[Descriptor::uIndex+2][index]*undimVelocity;
        for(unsigned int iX = 0; iX < velocitiesRotor.getNoHarmonics(); ++iX) {
            for(unsigned int iY = 0; iY < velocitiesRotor.getNoHarmonics(); ++iY) {

                int harmonicIndex = velocitiesRotor.getIndex(iX,iY);

                velocitiesRotor.velocityCosCos[harmonicIndex][threadIndex] = velocitiesRotor.rotorCellFactorCosCos[harmonicIndex][threadIndex]*velocity[2];
                velocitiesRotor.velocityCosSin[harmonicIndex][threadIndex] = velocitiesRotor.rotorCellFactorCosSin[harmonicIndex][threadIndex]*velocity[2];
                velocitiesRotor.velocitySinCos[harmonicIndex][threadIndex] = velocitiesRotor.rotorCellFactorSinCos[harmonicIndex][threadIndex]*velocity[2];
                velocitiesRotor.velocitySinSin[harmonicIndex][threadIndex] = velocitiesRotor.rotorCellFactorSinSin[harmonicIndex][threadIndex]*velocity[2];
            }
        }
    }

}

#ifdef ENABLE_CUDA

template<typename T, class Descriptor, typename SendDataVectors, typename...Params>
__global__ void readVelocities(SendDataVectors velocitiesRotor, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
         size_t const numRotorCells, T const undimVelocity, Params...args)
{
    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= numRotorCells)
      return;

    readVelocitiesImplementation<T,Descriptor>(velocitiesRotor, cellData, threadIndex, undimVelocity, std::forward<Params>(args)...);
}

#endif


#endif /* SRC_CONTRIB_COUPLING_COUPLINGIMPLEMENTATION_H_ */
