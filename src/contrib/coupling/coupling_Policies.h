/*
 * coupling_Policies.h
 *
 *  Created on: Jul 2, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CONTRIB_COUPLING_COUPLING_POLICIES_H_
#define SRC_CONTRIB_COUPLING_COUPLING_POLICIES_H_

#include "contrib/communication/NetworkDataStructures.h"
#include "couplingData.h"

template<typename T, class Descriptor>
struct HarmonicThrust {
    typedef ThrustAtRotorsHarmonic<10> ReceiveDataType;
    typedef NoData DataType;

    static void writeReceiveData(ReceiveDataType& dataReceive,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor>
struct TwoHarmonicThrust {
    typedef ThrustAtRotorsTwoHarmonic ReceiveDataType;
    typedef T DataType;

    static void writeReceiveData(ReceiveDataType& dataReceive,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor>
struct BesselHarmonicThrust {
    typedef ThrustAtRotorsBesselHarmonic ReceiveDataType;
    typedef T DataType;

    static void writeReceiveData(ReceiveDataType& dataReceive,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor>
struct LiftingLineThrust {
    typedef LiftingLine ReceiveDataType;
    typedef LiftingLineData<T> DataType;

    static void writeReceiveData(ReceiveDataType& dataReceive,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor>
struct HarmonicThrustRelaxed {
    typedef ThrustAtRotorsHarmonic<10> ReceiveDataType;
    typedef RelaxationData<T> DataType;

    static void writeReceiveData(ReceiveDataType& dataReceive,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor>
struct LinearVelocity {
    typedef VelocityAtRotorsLinear SendDataType;
    typedef VelocitiesAtRotorLinearVectors<T> DataType;
    static void readSendData(SendDataType& dataSend,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor, unsigned int NoHarmonicsRadial, unsigned int NoHarmonicsAzimuth>
struct TwoHarmonicVelocity {
    typedef VelocityAtRotorsTwoHarmonic<NoHarmonicsRadial,NoHarmonicsAzimuth> SendDataType;
    typedef VelocityAtRotorsTwoHarmonicVectors<T> DataType;
    static void readSendData(SendDataType& dataSend,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, SquareCellData<T>& rotorCellData, DataType& couplingData);
};

template<typename T, class Descriptor>
struct LiftingLineVelocity {
    typedef VelocityAtLiftingLine SendDataType;
    typedef T DataType;
};

template<typename T, class Descriptor, unsigned int NoHarmonics>
struct TwoHarmonicCartesianVelocity {
    typedef VelocityAtRotorsTwoHarmonicCartesian<NoHarmonics> SendDataType;
    typedef VelocityAtRotorsTwoHarmonicCartesianVectors<T> DataType;
    static void readSendData(SendDataType& dataSend,
            T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, SquareCellData<T>& rotorCellData, DataType& couplingData);
};

#endif /* SRC_CONTRIB_COUPLING_COUPLING_POLICIES_H_ */
