/*
 * coupling_Policies.hh
 *
 *  Created on: Jul 2, 2019
 *      Author: Bastian Horvat
 */
#ifndef SRC_CONTRIB_COUPLING_COUPLING_POLICIES_HH_
#define SRC_CONTRIB_COUPLING_COUPLING_POLICIES_HH_

#include "coupling_Policies.h"
#include "couplingData.hh"
#include "couplingImplementation.h"

#ifdef ENABLE_CUDA
#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <cuda_runtime_api.h>
#include "core/cudaErrorHandler.h"
#endif


template<typename T, class Descriptor>
void HarmonicThrust<T,Descriptor>::writeReceiveData(ReceiveDataType& dataReceive,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
    writeRotorThrust<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(dataReceive, cellData, rotorCellData._numberRotorCells, rotorCellData._undimThrust/rotorCellData._sumOfWeights,
            rotorCellData._cellIndexGPU, rotorCellData._cellPositionAzimuthGPU, rotorCellData._cellWeightGPU);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());
#endif
}

template<typename T, class Descriptor>
void HarmonicThrustRelaxed<T,Descriptor>::writeReceiveData(ReceiveDataType& dataReceive,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
    writeRotorThrustRelaxed<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(dataReceive, couplingData, cellData, rotorCellData._numberRotorCells, rotorCellData._undimThrust/rotorCellData._sumOfWeights,
            rotorCellData._cellIndexGPU, rotorCellData._cellPositionAzimuthGPU, rotorCellData._cellWeightGPU);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());
#endif
}

template<typename T, class Descriptor>
void TwoHarmonicThrust<T,Descriptor>::writeReceiveData(ReceiveDataType& dataReceive,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
    writeRotorThrust<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(dataReceive, cellData, rotorCellData._numberRotorCells, rotorCellData._undimThrust,
                rotorCellData._cellIndexGPU, rotorCellData._cellPositionAzimuthGPU, rotorCellData._cellPositionRadialGPU, rotorCellData._cellWeightGPU, couplingData);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());
#endif
    fflush(NULL);
}

template<typename T, class Descriptor>
void BesselHarmonicThrust<T,Descriptor>::writeReceiveData(ReceiveDataType& dataReceive,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
    writeRotorThrust<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(dataReceive, cellData, rotorCellData._numberRotorCells, rotorCellData._undimThrust,
                rotorCellData._cellIndexGPU, rotorCellData._cellPositionAzimuthGPU, rotorCellData._cellPositionRadialGPU, rotorCellData._cellWeightGPU, couplingData);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());
#endif
}

template<typename T, class Descriptor>
void LiftingLineThrust<T,Descriptor>::writeReceiveData(ReceiveDataType& dataReceive,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
	writeRotorThrust<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(dataReceive, cellData, rotorCellData._numberRotorCells, rotorCellData._undimThrust,
				rotorCellData._cellIndexGPU, rotorCellData._cellPositionAzimuthGPU, rotorCellData._cellPositionRadialGPU, rotorCellData._cellWeightGPU, couplingData);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());
#endif
}

template<typename T, class Descriptor>
void LinearVelocity<T,Descriptor>::readSendData(SendDataType& dataSend,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, RotorCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
    readVelocities<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(couplingData, cellData,
            rotorCellData._numberRotorCells, rotorCellData._undimVelocity, rotorCellData._cellIndexGPU, rotorCellData._cellWeightGPU);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());

    for(unsigned int iDim = 0; iDim < Descriptor::d; ++iDim)
    {
      dataSend.velocityMainMean[iDim] = thrust::reduce(thrust::device,
              &couplingData.velocityMainMean[iDim][0],&couplingData.velocityMainMean[iDim][rotorCellData._numberRotorCells-1]);
      dataSend.velocityMainSin[iDim] = thrust::reduce(thrust::device,&couplingData.velocityMainSin[iDim][0],
              &couplingData.velocityMainSin[iDim][rotorCellData._numberRotorCells-1]);
      dataSend.velocityMainCos[iDim] = thrust::reduce(thrust::device,&couplingData.velocityMainCos[iDim][0],
              &couplingData.velocityMainCos[iDim][rotorCellData._numberRotorCells-1]);
    }
#endif
}


template<typename T, class Descriptor, unsigned int NoHarmonicsRadial, unsigned int NoHarmonicsAzimuth>
void TwoHarmonicVelocity<T,Descriptor,NoHarmonicsRadial,NoHarmonicsAzimuth>::readSendData(SendDataType& dataSend,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, SquareCellData<T>& rotorCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
	for(unsigned int iDim = 0; iDim < Descriptor::d; ++iDim) {

		readVelocities<T,Descriptor><<<rotorCellData._numberRotorCells/256+1,256>>>(couplingData.inflowVector, cellData,
				rotorCellData._numberRotorCells, rotorCellData._undimVelocity, rotorCellData._cellIndexGPU, iDim);

		cudaDeviceSynchronize();
		HANDLE_ERROR(cudaGetLastError());

		float alpha = 1.0;
		float beta = 0.0;
		cublasStatus_t status = cublasSgemv(couplingData._handle, CUBLAS_OP_N, couplingData.h_coefficients.size(), rotorCellData._numberRotorCells, &alpha, couplingData.interpolationMatrix,
				couplingData.h_coefficients.size(), couplingData.inflowVector, 1, &beta, couplingData.d_coefficients, 1);
		cudaDeviceSynchronize();
		HANDLE_STATUS(status);
		HANDLE_STATUS(cublasGetVector(couplingData.h_coefficients.size(),sizeof(float),couplingData.d_coefficients,1,couplingData.h_coefficients.data(),1));


		if(couplingData.getInterpInflow_) {
			status = cublasSgemv(couplingData._handle, CUBLAS_OP_N, couplingData.h_inflowInterpVector.size(), rotorCellData._numberRotorCells, &alpha, couplingData.gridInterpMatrix,
						couplingData.h_inflowInterpVector.size(), couplingData.inflowVector, 1, &beta, couplingData.inflowInterpVector, 1);
			cudaDeviceSynchronize();
			HANDLE_STATUS(status);
			HANDLE_STATUS(cublasGetVector(couplingData.h_inflowInterpVector.size(),sizeof(float),couplingData.inflowInterpVector,1,couplingData.h_inflowInterpVector.data(),1));
		}


		for(unsigned int iRad = 0; iRad < NoHarmonicsRadial; ++iRad) {
			for(unsigned int iAz = 0; iAz < NoHarmonicsAzimuth; ++iAz) {
				unsigned indexLocal = couplingData.getIndex(iRad,iAz,0);
				unsigned indexSend = dataSend.getIndex(iRad,iAz);

				switch(iDim) {
				case 0:
					dataSend.ccX[indexSend] = couplingData.h_coefficients[indexLocal+0];
					dataSend.csX[indexSend] = couplingData.h_coefficients[indexLocal+1];
					dataSend.scX[indexSend] = couplingData.h_coefficients[indexLocal+2];
					dataSend.ssX[indexSend] = couplingData.h_coefficients[indexLocal+3];
					break;
				case 1:
					dataSend.ccY[indexSend] = couplingData.h_coefficients[indexLocal+0];
					dataSend.csY[indexSend] = couplingData.h_coefficients[indexLocal+1];
					dataSend.scY[indexSend] = couplingData.h_coefficients[indexLocal+2];
					dataSend.ssY[indexSend] = couplingData.h_coefficients[indexLocal+3];
					break;
				case 2:
					dataSend.ccZ[indexSend] = couplingData.h_coefficients[indexLocal+0];
					dataSend.csZ[indexSend] = couplingData.h_coefficients[indexLocal+1];
					dataSend.scZ[indexSend] = couplingData.h_coefficients[indexLocal+2];
					dataSend.ssZ[indexSend] = couplingData.h_coefficients[indexLocal+3];
					break;
				}
			}
		}
	}

    dataSend.scalingFactor = couplingData.scalingFactor_;
#endif

}

#ifdef ENABLE_CUDA

template<typename T, class Descriptor>
__global__ void debugKernel(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT velocity, size_t const numRotorCells,
        T const undimVelocity, T const * const rotorCellWeight, size_t const * const rotorCellIndex) {

    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= numRotorCells)
      return;

    size_t index = rotorCellIndex[threadIndex];

    if(rotorCellWeight[threadIndex] > 0) {
        velocity[threadIndex] = cellData[Descriptor::uIndex+2][index]*undimVelocity;
    }
    else
        velocity[threadIndex] = 0;
}

#endif

template<typename T, class Descriptor, unsigned int NoHarmonics>
void TwoHarmonicCartesianVelocity<T,Descriptor,NoHarmonics>::readSendData(SendDataType& dataSend,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, SquareCellData<T>& squareCellData, DataType& couplingData)
{
#ifdef ENABLE_CUDA
    readVelocities<T,Descriptor><<<squareCellData._numberRotorCells/256+1,256>>>(couplingData, cellData,
            squareCellData._numberRotorCells, squareCellData._undimVelocity, squareCellData._cellWeightGPU, squareCellData._cellIndexGPU);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());

    T* velocities;
    HANDLE_ERROR(cudaMallocManaged(&velocities,squareCellData._numberRotorCells*sizeof(T)));

    debugKernel<T,Descriptor><<<squareCellData._numberRotorCells/256+1,256>>>(cellData, velocities,
            squareCellData._numberRotorCells, squareCellData._undimVelocity, squareCellData._cellWeightGPU, squareCellData._cellIndexGPU);

    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());

    std::ofstream cellVelFact ("cellVFact.txt");
    std::ofstream cellFactGPU ("cellFactGPU.txt");
    cellVelFact << "index,iX,iY,cc,cs,sc,ss,globalIndex" << std::endl;
    cellFactGPU << "index,iX,iY,cc,cs,sc,ss,globalIndex" << std::endl;

    T* dcc = new T[squareCellData._numberRotorCells];
    T* dcs = new T[squareCellData._numberRotorCells];
    T* dsc = new T[squareCellData._numberRotorCells];
    T* dss = new T[squareCellData._numberRotorCells];

    size_t numCells = squareCellData._numberRotorCells;

    for(unsigned int iX = 0; iX < NoHarmonics; ++iX) {
        for(unsigned int iY = 0; iY < NoHarmonics; ++iY) {
            unsigned int index = couplingData.getIndex(iX,iY);
            cudaMemcpy(dcc,couplingData.velocityCosCos[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            cudaMemcpy(dcs,couplingData.velocityCosSin[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            cudaMemcpy(dsc,couplingData.velocitySinCos[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            cudaMemcpy(dss,couplingData.velocitySinSin[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            for(unsigned int iCell = 0; iCell < squareCellData._numberRotorCells; ++iCell) {
                cellVelFact << iCell << "," << iX << "," << iY << "," << dcc[iCell] << "," << dcs[iCell] << "," << dsc[iCell] << "," << dss[iCell] << "," << squareCellData._cellIndexCPU[iCell] << std::endl;
            }

            cudaMemcpy(dcc,couplingData.rotorCellFactorCosCos[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            cudaMemcpy(dcs,couplingData.rotorCellFactorCosSin[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            cudaMemcpy(dsc,couplingData.rotorCellFactorSinCos[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            cudaMemcpy(dss,couplingData.rotorCellFactorSinSin[index],numCells*sizeof(T),cudaMemcpyDeviceToHost);
            for(unsigned int iCell = 0; iCell < squareCellData._numberRotorCells; ++iCell) {
                cellFactGPU << iCell << "," << iX << "," << iY << "," << dcc[iCell] << "," << dcs[iCell] << "," << dsc[iCell] << "," << dss[iCell] << "," << squareCellData._cellIndexCPU[iCell] << std::endl;
            }
        }
    }

    delete [] dcc;
    delete [] dcs;
    delete [] dsc;
    delete [] dss;

    std::ofstream cellInfl ("cellInfl.txt");
    cellInfl << "index,infl,globalIndex" << std::endl;

    for(unsigned int iCell = 0; iCell < squareCellData._numberRotorCells; ++iCell) {
        cellInfl << iCell << "," << velocities[iCell] << "," << squareCellData._cellIndexCPU[iCell] << std::endl;
    }


    for(unsigned int iX = 0; iX < NoHarmonics; ++iX) {
        for(unsigned int iY = 0; iY < NoHarmonics; ++iY) {
            unsigned int index = couplingData.getIndex(iX,iY);

            dataSend.cc[index] = thrust::reduce(thrust::device,&couplingData.velocityCosCos[index][0],
                    &couplingData.velocityCosCos[index][squareCellData._numberRotorCells-1]);
            dataSend.cs[index] = thrust::reduce(thrust::device,&couplingData.velocityCosSin[index][0],
                    &couplingData.velocityCosSin[index][squareCellData._numberRotorCells-1]);
            dataSend.sc[index] = thrust::reduce(thrust::device,&couplingData.velocitySinCos[index][0],
                    &couplingData.velocitySinCos[index][squareCellData._numberRotorCells-1]);
            dataSend.ss[index] = thrust::reduce(thrust::device,&couplingData.velocitySinSin[index][0],
                    &couplingData.velocitySinSin[index][squareCellData._numberRotorCells-1]);

        }
    }

    dataSend.scalingFactor = couplingData.scalingFactor_;
#endif
}
#endif /* SRC_CONTRIB_COUPLING_COUPLING_POLICIES_HH_ */

