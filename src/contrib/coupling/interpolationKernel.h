/*
 * interpolationKernel.h
 *
 *  Created on: Mar 18, 2021
 *      Author: ga69kiq
 */

#include <cmath>
#include <array>
#include <tuple>

#ifndef SRC_CONTRIB_COUPLING_INTERPOLATIONKERNEL_H_
#define SRC_CONTRIB_COUPLING_INTERPOLATIONKERNEL_H_

std::array<std::tuple<unsigned int, unsigned int, float>,4> bilinearKernel(float indexX, float indexY) {
    std::array<float,4> coeffs;
    std::array<float,4> iX{std::floor(indexX),std::ceil(indexX),std::floor(indexX),std::ceil(indexX)};
    std::array<float,4> iY{std::floor(indexY),std::floor(indexY),std::ceil(indexY),std::ceil(indexY)};

    float cx1 = std::ceil(indexX) - indexX;
    float cx2 = 1 - cx1;
    float cy1 = std::ceil(indexY) - indexY;
    float cy2 = 1 - cy1;

    coeffs[0] = cx1*cy1;
    coeffs[1] = cx2*cy1;
    coeffs[2] = cx1*cy2;
    coeffs[3] = cx2*cy2;

    std::array<std::tuple<unsigned int, unsigned int, float>,4> tmp;
    for(unsigned int ii = 0; ii < tmp.size(); ++ii) {
        tmp[ii] = std::tuple<unsigned int, unsigned int, float>{iX[ii],iY[ii],coeffs[ii]};
    }

    return tmp;
}

#endif /* SRC_CONTRIB_COUPLING_INTERPOLATIONKERNEL_H_ */
