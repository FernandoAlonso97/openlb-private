/*
 * rotor.h
 *
 *  Created on: Jul 2, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CONTRIB_COUPLING_ROTOR_H_
#define SRC_CONTRIB_COUPLING_ROTOR_H_

#include <vector>
#include <cmath>
#include <iostream>
#include "core/cudaErrorHandler.h"

template<typename T>
class Rotor {
private:
    T _radiusUndim;
    T _rotorArea;
    T _hubradius;
    std::vector<int> _xLimits;
    std::vector<int> _yLimits;
    std::vector<int> _zLimits;
    std::vector<int> _rotationAxis;

    std::vector<T> _rotorCenter;
    size_t         _numberRotorCells;

public:
    Rotor(T const radius, T const hubRadius, std::vector<T> const rotorCenter, T const spacing);

    size_t getNumberRotorCells() const;
    T getRadiusUndim() const;
    T getRotorArea() const;
    T getHubRadius() const;

    std::vector<int> const & getXLimits() const;
    std::vector<int> const & getYLimits() const;
    std::vector<int> const & getZLimits() const;
    std::vector<T> const & getCenter() const;

};

template<typename T>
class RotorCellData
{
public:
    template<class Lattice, class Converter>
    RotorCellData(Rotor<T> const & rotor, Lattice const & lattice, Converter const & converter, bool const print = false);

    OPENLB_HOST_DEVICE
    ~RotorCellData();

public:
    size_t _numberRotorCells{};
    size_t _numberFullRotorCells{};
    T _undimThrust{};
    T _undimVelocity{};
    T _sumOfWeights{};

    T* _cellPositionAzimuthCPU{nullptr};
    T* _cellPositionRadialCPU{nullptr};
    T* _cellWeightCPU{nullptr};
    size_t* _cellIndexCPU{nullptr};

#ifdef ENABLE_CUDA
    T* _cellPositionAzimuthGPU{nullptr};
    T* _cellPositionRadialGPU{nullptr};
    T* _cellWeightGPU{nullptr};
    size_t* _cellIndexGPU{nullptr};
#endif
};

template<typename T>
class SquareCellData
{
public:
    template<class Lattice, class Converter>
    SquareCellData(Rotor<T> const & rotor, Lattice const & lattice, Converter const & converter, bool const print = false);

    OPENLB_HOST_DEVICE
    ~SquareCellData();

public:
    size_t _numberRotorCells{};
    T _undimThrust{};
    T _undimVelocity{};
    T _cellSize{};

    T* _cellPosition1CPU{nullptr};
    T* _cellPosition2CPU{nullptr};
    T* _cellWeightCPU{nullptr};
    size_t* _cellIndexCPU{nullptr};

#ifdef ENABLE_CUDA
    T* _cellPosition1GPU{nullptr};
    T* _cellPosition2GPU{nullptr};
    T* _cellWeightGPU{nullptr};
    size_t* _cellIndexGPU{nullptr};
#endif
};


#endif /* SRC_CONTRIB_COUPLING_ROTOR_H_ */
