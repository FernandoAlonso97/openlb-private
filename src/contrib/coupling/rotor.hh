/*
 * rotor.hh
 *
 *  Created on: Jul 2, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CONTRIB_COUPLING_ROTOR_HH_
#define SRC_CONTRIB_COUPLING_ROTOR_HH_

#include "rotor.h"
#include "core/util.h"

template<typename T>
Rotor<T>::Rotor(T const radius, T const hubRadius, std::vector<T> const rotorCenter, T const spacing):
    _radiusUndim{radius/spacing},
    _rotorArea{radius*radius*M_PI},
    _hubradius{hubRadius},
    _xLimits{std::vector<int>{static_cast<int>(std::ceil(rotorCenter[0]-_radiusUndim))-1,
            static_cast<int>(std::floor(rotorCenter[0]+_radiusUndim))+1}},
    _yLimits{std::vector<int>{static_cast<int>(std::ceil(rotorCenter[1]-_radiusUndim))-1,
            static_cast<int>(std::floor(rotorCenter[1]+_radiusUndim))+1}},
    _zLimits{std::vector<int>{static_cast<int>(rotorCenter[2]),static_cast<int>(rotorCenter[2])}},
    _rotorCenter{rotorCenter},
    _numberRotorCells{(_xLimits[1]-_xLimits[0]+1)*(_yLimits[1]-_yLimits[0]+1)}
    {
        if(not(_xLimits[0] == _xLimits[1] or _yLimits[0] == _yLimits[1] or _zLimits[0] == _zLimits[1]) )
                std::cout << "Rotor can only be in one cell layer" << std::endl;
        std::cout << "Rotorlimits x: " << _xLimits[0] << ":" << _xLimits[1] << std::endl;
        std::cout << "Rotorlimits y: " << _yLimits[0] << ":" << _yLimits[1] << std::endl;
        std::cout << "Rotor center: " << _rotorCenter[0] << "," << _rotorCenter[1] << "," << _rotorCenter[2] << std::endl;
    }

template<typename T>
size_t Rotor<T>::getNumberRotorCells() const
{
    return _numberRotorCells;
}

template<typename T>
T Rotor<T>::getRadiusUndim() const
{
    return _radiusUndim;
}

template<typename T>
T Rotor<T>::getRotorArea() const
{
    return _rotorArea;
}

template<typename T>
T Rotor<T>::getHubRadius() const
{
    return _hubradius;
}

template<typename T>
std::vector<int> const & Rotor<T>::getXLimits() const
{
    return _xLimits;
}

template<typename T>
std::vector<int> const & Rotor<T>::getYLimits() const
{
    return _yLimits;
}

template<typename T>
std::vector<int> const & Rotor<T>::getZLimits() const
{
    return _zLimits;
}

template<typename T>
std::vector<T> const & Rotor<T>::getCenter() const
{
    return _rotorCenter;
}

template<typename T>
template<class Lattice, class Converter>
RotorCellData<T>::RotorCellData(Rotor<T> const & rotor, Lattice const & lattice, Converter const & converter, bool const print):
    _numberRotorCells{rotor.getNumberRotorCells()},
    _undimThrust{ converter.getLatticeForce(1.) },
    _undimVelocity{converter.getPhysVelocity(1.)},
    _cellPositionAzimuthCPU{new T[_numberRotorCells]},
    _cellPositionRadialCPU{new T[_numberRotorCells]},
    _cellWeightCPU{new T[_numberRotorCells]},
    _cellIndexCPU{new size_t[_numberRotorCells]}
{
#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMalloc(&_cellPositionAzimuthGPU, sizeof(T)*_numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&_cellPositionRadialGPU, sizeof(T)*_numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&_cellWeightGPU, sizeof(T)*_numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&_cellIndexGPU, sizeof(size_t)*_numberRotorCells));
#endif

    std::vector<T> const & rotorCenter = rotor.getCenter();
    std::vector<int> const & xLimits = rotor.getXLimits();
    std::vector<int> const & yLimits = rotor.getYLimits();
    std::vector<int> const & zLimits = rotor.getZLimits();

    T weightAbs{0};
    T fullRotorCells{0};

    const T rotorRadiusUndim = rotor.getRadiusUndim();

    const size_t ny = lattice.getNy();
    const size_t nz = lattice.getNz();

    for(int iX = xLimits[0]; iX <= xLimits[1]; ++iX)
        for(int iY = yLimits[0]; iY <= yLimits[1]; ++iY)
        {
            int index = (iX-xLimits[0])*(yLimits[1]-yLimits[0]+1)+(iY-yLimits[0]);

            size_t cellIndex = olb::util::getCellIndex3D(iX,iY,rotorCenter[2],ny,nz);

            T radialPosition = std::sqrt(std::pow(iX-rotorCenter[0],2)+std::pow(iY-rotorCenter[1],2))/rotorRadiusUndim;
            _cellPositionRadialCPU[index] = radialPosition;
            _cellIndexCPU[index] = cellIndex;

            if(radialPosition < rotor.getHubRadius())
                _cellWeightCPU[index] = 0;
            else if(radialPosition <= 1-std::sqrt(0.5)/rotorRadiusUndim)
            {
                _cellWeightCPU[index] = 1;
                _numberFullRotorCells += 1;
            }
            else if(radialPosition <= 1+std::sqrt(0.5)/rotorRadiusUndim)
                _cellWeightCPU[index] = std::max((1+std::sqrt(0.5)/rotorRadiusUndim-radialPosition)/(std::sqrt(0.5)/rotorRadiusUndim),0.)*0.5;
            else
                _cellWeightCPU[index] = 0;

            T distanceX = static_cast<T>(iX)-rotorCenter[0];
            T distanceY = static_cast<T>(iY)-rotorCenter[1];
            _cellPositionAzimuthCPU[index] = (std::atan2(-distanceY,distanceX)+M_PI);
            weightAbs += _cellWeightCPU[index];
        }

    _sumOfWeights = weightAbs;
//    _undimThrust /= 1;
    _undimVelocity /= _numberFullRotorCells;

	if(print) {
    std::cout << "Sum of Weights is: " << weightAbs  << " (" << weightAbs*converter.getPhysDeltaX()*converter.getPhysDeltaX()
            << ")"<< std::endl;


    for(int iX = 0; iX < xLimits[1]-xLimits[0]+1; ++iX)
    {
        for(int iY = 0; iY < yLimits[1]-yLimits[0]+1; ++iY)
        {
            int index = iX*(yLimits[1]-yLimits[0]+1)+iY;
            std::cout << std::setprecision(2) << std::fixed << _cellPositionRadialCPU[index] << ",";
        }
        std::cout << std::endl;
    }

    std::cout << "=========" << std::endl;

    for(int iX = 0; iX < xLimits[1]-xLimits[0]+1; ++iX)
    {
        for(int iY = 0; iY < yLimits[1]-yLimits[0]+1; ++iY)
        {
            int index = iX*(yLimits[1]-yLimits[0]+1)+iY;
            std::cout << std::setprecision(2) << std::fixed << _cellWeightCPU[index] << ",";
        }
        std::cout << std::endl;
    }

    std::cout << "=========" << std::endl;

    for(int iX = xLimits[1]-xLimits[0]; iX >= 0; --iX)
    {
        for(int iY = 0; iY < yLimits[1]-yLimits[0]+1; ++iY)
        {
            int index = iX*(yLimits[1]-yLimits[0]+1)+iY;
            std::cout << std::setprecision(2) << std::fixed << _cellPositionAzimuthCPU[index] << ",";
        }
        std::cout << std::endl;
    }
	}

#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMemcpy(_cellPositionAzimuthGPU,_cellPositionAzimuthCPU,sizeof(T)*_numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(_cellPositionRadialGPU,_cellPositionRadialCPU,sizeof(T)*_numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(_cellWeightGPU,_cellWeightCPU,sizeof(T)*_numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(_cellIndexGPU,_cellIndexCPU,sizeof(size_t)*_numberRotorCells,cudaMemcpyHostToDevice));
#endif
}

template<typename T>
RotorCellData<T>::~RotorCellData()
{
//    delete [] _cellPositionAzimuthCPU;
//    delete [] _cellPositionRadialCPU;
//    delete [] _cellWeightCPU;
//    delete [] _cellIndexCPU;
//
//#ifdef ENABLE_CUDA
//    HANDLE_ERROR(cudaFree(_cellPositionAzimuthGPU));
//    HANDLE_ERROR(cudaFree(_cellPositionRadialGPU));
//    HANDLE_ERROR(cudaFree(_cellWeightGPU));
//    HANDLE_ERROR(cudaFree(_cellIndexGPU));
//#endif
}

template<typename T>
template<class Lattice, class Converter>
SquareCellData<T>::SquareCellData(Rotor<T> const & rotor, Lattice const & lattice, Converter const & converter, bool const print):
    _numberRotorCells{rotor.getNumberRotorCells()},
    _undimThrust{ converter.getLatticeForce(1.) },
    _undimVelocity{converter.getPhysVelocity(1.)},
    _cellSize{std::pow(1./rotor.getRadiusUndim(),2)},
    _cellPosition1CPU{new T[_numberRotorCells]},
    _cellPosition2CPU{new T[_numberRotorCells]},
    _cellWeightCPU{new T[_numberRotorCells]},
    _cellIndexCPU{new size_t[_numberRotorCells]}
{
#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMalloc(&_cellPosition1GPU, sizeof(T)*_numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&_cellPosition2GPU, sizeof(T)*_numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&_cellWeightGPU, sizeof(T)*_numberRotorCells));
    HANDLE_ERROR(cudaMalloc(&_cellIndexGPU, sizeof(size_t)*_numberRotorCells));
#endif

    std::vector<T> const & rotorCenter = rotor.getCenter();
    std::vector<int> const & xLimits = rotor.getXLimits();
    std::vector<int> const & yLimits = rotor.getYLimits();
    std::vector<int> const & zLimits = rotor.getZLimits();

    const T rotorRadiusUndim = rotor.getRadiusUndim();

    const size_t ny = lattice.getNy();
    const size_t nz = lattice.getNz();

    for(int iX = xLimits[0]; iX <= xLimits[1]; ++iX)
        for(int iY = yLimits[0]; iY <= yLimits[1]; ++iY) {
            int index = olb::util::getCellIndex2D(iX-xLimits[0],iY-yLimits[0],yLimits[1]-yLimits[0]+1);

            size_t cellIndex = olb::util::getCellIndex3D(iX,iY,rotorCenter[2],ny,nz);

            T xPosition = (iX-rotorCenter[0])/rotorRadiusUndim;
            T yPosition = (iY-rotorCenter[1])/rotorRadiusUndim;
            _cellPosition1CPU[index] = xPosition;
            _cellPosition2CPU[index] = yPosition;
            _cellIndexCPU[index] = cellIndex;

            if (xPosition <= 1. and xPosition >= -1 and yPosition <= 1 and yPosition >= -1)
                _cellWeightCPU[index] = 1.;
            else
                _cellWeightCPU[index] = 0.;

        }

	if(print) {
    for(int iX = 0; iX < xLimits[1]-xLimits[0]+1; ++iX) {
        for(int iY = 0; iY < yLimits[1]-yLimits[0]+1; ++iY) {
            int index = iX*(yLimits[1]-yLimits[0]+1)+iY;
            std::cout << std::setprecision(2) << std::fixed << _cellPosition1CPU[index] << ",";
        }
        std::cout << std::endl;
    }

    std::cout << "=========" << std::endl;

    for(int iX = xLimits[1]-xLimits[0]; iX >= 0; --iX) {
        for(int iY = 0; iY < yLimits[1]-yLimits[0]+1; ++iY) {
            int index = iX*(yLimits[1]-yLimits[0]+1)+iY;
            std::cout << std::setprecision(2) << std::fixed << _cellPosition2CPU[index] << ",";
        }
        std::cout << std::endl;
    }

    std::cout << "=========" << std::endl;

    for(int iX = xLimits[1]-xLimits[0]; iX >= 0; --iX) {
        for(int iY = 0; iY < yLimits[1]-yLimits[0]+1; ++iY) {
            int index = iX*(yLimits[1]-yLimits[0]+1)+iY;
            std::cout << std::setprecision(2) << std::fixed << _cellWeightCPU[index] << ",";
        }
        std::cout << std::endl;
    }

    std::cout << "=========" << std::endl;
	}

#ifdef ENABLE_CUDA
    HANDLE_ERROR(cudaMemcpy(_cellPosition1GPU,_cellPosition1CPU,sizeof(T)*_numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(_cellPosition2GPU,_cellPosition2CPU,sizeof(T)*_numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(_cellWeightGPU   ,_cellWeightCPU,sizeof(T)*_numberRotorCells,cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(_cellIndexGPU    ,_cellIndexCPU,sizeof(size_t)*_numberRotorCells,cudaMemcpyHostToDevice));
#endif
}

template<typename T>
SquareCellData<T>::~SquareCellData()
{
#ifdef ENABLE_CUDA
#ifndef __CUDACC__
    HANDLE_ERROR(cudaFree(_cellPosition1GPU));
    HANDLE_ERROR(cudaFree(_cellPosition2GPU));
    HANDLE_ERROR(cudaFree(_cellWeightGPU));
    HANDLE_ERROR(cudaFree(_cellIndexGPU));
#endif
#else
    delete [] _cellPosition1CPU;
    delete [] _cellPosition2CPU;
    delete [] _cellWeightCPU;
    delete [] _cellIndexCPU;
#endif
}

#endif /* SRC_CONTRIB_COUPLING_ROTOR_HH_ */
