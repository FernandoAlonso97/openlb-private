/*
 * cublasHelper.h
 *
 *  Created on: Mar 18, 2021
 *      Author: ga69kiq
 */

#ifndef SRC_CONTRIB_CUDAHELPER_CUBLASHELPER_H_
#define SRC_CONTRIB_CUDAHELPER_CUBLASHELPER_H_

#include "cublas_v2.h"
#include <cstddef>

#define HANDLE_STATUS( status ) ( CublasHandleStatus( status, __FILE__, __LINE__ ) )

inline static void CublasHandleStatus( cublasStatus_t status, const char *file, int line )
{
    if (status != CUBLAS_STATUS_SUCCESS) {


        printf( "Status %i in %s at line %d\n", status,
                file, line );
        switch(status) {
        case CUBLAS_STATUS_NOT_INITIALIZED:
            printf("CUBLAS_STATUS_NOT_INITIALIZED\n");
            break;
        case CUBLAS_STATUS_ALLOC_FAILED    :
            printf("CUBLAS_STATUS_ALLOC_FAILED\n");
            break;
        case CUBLAS_STATUS_INVALID_VALUE   :
            printf("CUBLAS_STATUS_INVALID_VALUE\n");
            break;
        case CUBLAS_STATUS_ARCH_MISMATCH   :
            printf("CUBLAS_STATUS_ARCH_MISMATCH\n");
            break;
        case CUBLAS_STATUS_MAPPING_ERROR   :
            printf("CUBLAS_STATUS_MAPPING_ERROR\n");
            break;
        case CUBLAS_STATUS_EXECUTION_FAILED:
            printf("CUBLAS_STATUS_EXECUTION_FAILED\n");
            break;
        case CUBLAS_STATUS_INTERNAL_ERROR  :
            printf("CUBLAS_STATUS_INTERNAL_ERROR\n");
            break;
        case CUBLAS_STATUS_NOT_SUPPORTED   :
            printf("CUBLAS_STATUS_NOT_SUPPORTED\n");
            break;
        case CUBLAS_STATUS_LICENSE_ERROR   :
            printf("CUBLAS_STATUS_LICENSE_ERROR\n");
            break;
        default:
            printf("Unknown status error\n");
            break;
        }
        exit( EXIT_FAILURE );
    }
}


void cudaSynchronize() {
#ifdef ENABLE_CUDA
		cudaDeviceSynchronize();
#endif
}

#ifdef ENABLE_CUDA
__device__ int getBlockIndex() {return blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;}
__device__ int getThreadIndex( size_t blockIndex ) {return threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y \
		                               + blockIndex * blockDim.x * blockDim.y * blockDim.z;}
#endif



#endif /* SRC_CONTRIB_CUDAHELPER_CUBLASHELPER_H_ */
