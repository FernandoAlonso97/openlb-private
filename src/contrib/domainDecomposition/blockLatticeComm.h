#ifndef BLOCK_LATTICE_COM_H
#define BLOCK_LATTICE_COM_h

#include "blockLattice3D.h"

namespace olb{


template<typename T,template<typename> class Lattice,class Blocklattice,template<typename> class Memory>
class BlockLatticeComm: public Blocklattice {

  public:
    BlockLatticeComm () = delete;

    template<typename ... ARGS>
    BlockLatticeComm(CommunicationDataHandler<T,Lattice<T>,Memory>&& commHandler, ARGS && ... args):Blocklattice{std::forward<ARGS>(args)...},commDataHandler_(std::move(commHandler)) {}

    CommunicationDataHandler<T,Lattice<T>,Memory>& getCommDataHandler ()
    {
      return commDataHandler_;
    }

  private:

    CommunicationDataHandler<T,Lattice<T>,Memory> commDataHandler_;

};


} // end of namespace
#endif
