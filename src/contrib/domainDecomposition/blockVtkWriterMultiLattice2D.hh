
#ifndef BLOCK_VTK_WRITER_MULTILATTICE_2D_HH
#define BLOCK_VTK_WRITER_MULTILATTICE_2D_HH

#include <fstream>
#include <iostream>
#include "core/singleton.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.h"
#include "io/base64.h"
#include "io/fileName.h"

namespace olb {

template<typename T,typename Lattice>
BlockVTKwriterMultiLattice2D<T,Lattice>::BlockVTKwriterMultiLattice2D( std::string name,
		DomainInformation<T,Lattice>& domainInfo, float scaling, bool binary , bool printGhostLayer)
  : clout( std::cout,"BlockVTKwriterMultiLattice2D" ), _name(name), _binary(binary),
	_printGhostLayer(printGhostLayer),_domainInfo(domainInfo),_scaling(scaling)
{
	if(_domainInfo.getLocalInfo().localSubDomain == 0) {
	  std::string fullNamePvd = singleton::directories().getVtkOutDir()
	  										   + _name + ".pvd";
	  _pvdFile = std::ofstream(fullNamePvd.c_str());

	  _pvdFile << "<?xml version=\"1.0\"?>\n";
	  _pvdFile << "<VTKFile type=\"Collection\" version=\"0.1\"\n";
	  _pvdFile << "         byte_order=\"LittleEndian\"\n";
	  _pvdFile << "         compressor=\"vtkZLibDataCompressor\">\n";
	  _pvdFile << "  <Collection>" << std::endl ;
	}
}

template<typename T,typename Lattice>
BlockVTKwriterMultiLattice2D<T,Lattice>::BlockVTKwriterMultiLattice2D( std::string name,
		DomainInformation<T,Lattice>& domainInfo, bool binary , bool printGhostLayer) :
		BlockVTKwriterMultiLattice2D(name, domainInfo, 1.0/domainInfo.getRefInfo().globalIndexEnd[0],
				binary, printGhostLayer) {}

template<typename T,typename Lattice>
std::string BlockVTKwriterMultiLattice2D<T,Lattice>::getName() {
  return _name;
}

template<typename T,typename Lattice>
BlockVTKwriterMultiLattice2D<T,Lattice>::~BlockVTKwriterMultiLattice2D ()
{
  clearAddedFunctors();
  if(_domainInfo.getLocalInfo().localSubDomain == 0)
	  finalize();
}

template<typename T, class Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::computeDomainStuff(SubDomainInformation<T,Lattice> const & subDomain,
		size_t* globalOrigin, size_t* globalExtend, std::array<size_t,Lattice::d>& nCells, std::array<long long int,
		Lattice::d>& localIXS, std::array<long long int,Lattice::d>& localIXE) {

	  unsigned ghostLayer[] = {0,0};
	  if (_printGhostLayer)
	    for (unsigned i=0;i<2;++i)
	    {
	      ghostLayer[i] = std::min(static_cast<unsigned>(1),subDomain.ghostLayer[i]);
	    }

	  if ( _pointerVec.empty() ) {
	    clout << "Error: Please add functor via addFunctor()";
	  } else {
	    // get first functor
	    auto it = _pointerVec.cbegin();

	    long long iXS[2];
	    long long iXE[2];

	   for (unsigned i=0;i<2;++i)
	   {
	    if (globalOrigin[i] >= subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = 0;
	      iXE[i] = 0;
	    }
	    else if (globalExtend[i] <= subDomain.globalIndexStart[i])
	    {
	      iXS[i] = 0;
	      iXE[i] = 0;
	    }

	    else if (globalOrigin[i] <= subDomain.globalIndexStart[i] && globalExtend[i] > subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = subDomain.globalIndexStart[i];
	      iXE[i] = subDomain.globalIndexEnd[i]+ghostLayer[i];
	    }

      else if (globalOrigin[i] <= subDomain.globalIndexStart[i] && globalExtend[i] == subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = subDomain.globalIndexStart[i];
	      iXE[i] = subDomain.globalIndexEnd[i];
	    }

	    else if (globalOrigin[i] <= subDomain.globalIndexStart[i] && globalExtend[i] < subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = subDomain.globalIndexStart[i];
	      iXE[i] = globalExtend[i];
	    }

	    else if (globalOrigin[i] > subDomain.globalIndexStart[i] && globalExtend[i] > subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = globalOrigin[i];
	      iXE[i] = subDomain.globalIndexEnd[i]+ghostLayer[i];
	    }

      else if (globalOrigin[i] > subDomain.globalIndexStart[i] && globalExtend[i] == subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = globalOrigin[i];
	      iXE[i] = subDomain.globalIndexEnd[i];
	    }

	    else if (globalOrigin[i] > subDomain.globalIndexStart[i] && globalExtend[i] < subDomain.globalIndexEnd[i])
	    {
	      iXS[i] = globalOrigin[i];
	      iXE[i] = globalExtend[i];
	    }
	    else
	    {
	      std::cout << "something went horribly wrong in blockVtkWriterMulitLattice3D" << std::endl;
	      exit(999);
	    }
	  }


	    nCells[0] = iXE[0]-iXS[0] > 0 ? iXE[0]-iXS[0] : 0;
	    nCells[1] = iXE[1]-iXS[1] > 0 ? iXE[1]-iXS[1] : 0;

	    for(unsigned int iDim = 0; iDim < Lattice::d; ++iDim) {
        localIXS[iDim] = iXS[iDim];
        localIXE[iDim] = iXE[iDim];
	    }
	  }
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::finalize() {
	_pvdFile << "  </Collection>\n";
	_pvdFile << "</VTKFile>" << std::endl;
	_pvdFile.close();
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::write( int iT , size_t* globalOrigin, size_t* globalExtend)
{
  size_t origin[2];
  size_t extend[2];
  if (!globalOrigin) {
    origin[0] = _domainInfo.refSubDomain.globalIndexStart[0];
    origin[1] = _domainInfo.refSubDomain.globalIndexStart[1];
    globalOrigin = origin;
  }
  if (!globalExtend) {
    extend[0] = _domainInfo.refSubDomain.globalIndexEnd[0];
    extend[1] = _domainInfo.refSubDomain.globalIndexEnd[1];
    globalExtend = extend;
  }

  unsigned ghostLayer[] = {0,0};
  if (_printGhostLayer)
    for (unsigned i=0;i<2;++i)
    {
      ghostLayer[i] = _domainInfo.getLocalInfo().ghostLayer[i];
    }

  if ( _pointerVec.empty() ) {
    clout << "Error: Please add functor via addFunctor()";
  } else {
    // get first functor
    auto it = _pointerVec.cbegin();

    // std::cout << "valüüüüüüüüüüüüüüs in writer start " << globalOrigin[0] <<" " << globalOrigin[1] << " " << globalOrigin[2] << " " <<globalExtend[0] << " " << globalExtend[1] << " " << globalExtend[2] << std::endl; 
    long long iXS[2];
    long long iXE[2];
    
   for (unsigned i=0;i<2;++i)
   {
    if (globalOrigin[i] >= _domainInfo.getLocalInfo().globalIndexEnd[i])
    {
      iXS[i] = 0.0;
      iXE[i] = 0.0;
    }
    else if (globalExtend[i] <= _domainInfo.getLocalInfo().globalIndexStart[i])
    {
      iXS[i] = 0.0;
      iXE[i] = 0.0;
    }

    else if (globalOrigin[i] <= _domainInfo.getLocalInfo().globalIndexStart[i] && globalExtend[i] >= _domainInfo.getLocalInfo().globalIndexEnd[i])
    {
      iXS[i] = _domainInfo.getLocalInfo().globalIndexStart[i]-ghostLayer[i];
      iXE[i] = _domainInfo.getLocalInfo().globalIndexEnd[i]+ghostLayer[i];
    }

    else if (globalOrigin[i] <= _domainInfo.getLocalInfo().globalIndexStart[i] && globalExtend[i] < _domainInfo.getLocalInfo().globalIndexEnd[i])
    {
      iXS[i] = _domainInfo.getLocalInfo().globalIndexStart[i]-ghostLayer[i];
      iXE[i] = globalExtend[i];
    }

    else if (globalOrigin[i] > _domainInfo.getLocalInfo().globalIndexStart[i] && globalExtend[i] >= _domainInfo.getLocalInfo().globalIndexEnd[i])
    {
      iXS[i] = globalOrigin[i];
      iXE[i] = _domainInfo.getLocalInfo().globalIndexEnd[i]+ghostLayer[i];
    }

    else if (globalOrigin[i] > _domainInfo.getLocalInfo().globalIndexStart[i] && globalExtend[i] < _domainInfo.getLocalInfo().globalIndexEnd[i])
    {
      iXS[i] = globalOrigin[i];
      iXE[i] = globalExtend[i];
    }
    else 
    {
      std::cout << "something went horribly wrong in blockVtkWriterMulitLattice2D" << std::endl;
      exit(999);
    }
  }


    size_t nx = iXE[0]-iXS[0]-1;//(**it).getBlockStructure().getNx() -1;
    size_t ny = iXE[1]-iXS[1]-1;//(**it).getBlockStructure().getNy() -1;
    // std::cout << "valüüüüüüüüüüüüüüs in writer " << iXS[0] <<" " << iXS[1] << " " << iXS[2] << " " << iXE[0] << " " << iXE[1] << " " << iXE[2] << std::endl; 

    std::string fullNameVti = singleton::directories().getVtkOutDir()
                              + createFileName( _name + "_GPU" + std::to_string(_domainInfo.getLocalInfo().localSubDomain), iT ) + ".vti";

    size_t localIXS[2] = {iXS[0]-_domainInfo.getLocalInfo().globalIndexStart[0]+_domainInfo.getLocalInfo().ghostLayer[0],
                            iXS[1]-_domainInfo.getLocalInfo().globalIndexStart[1]+_domainInfo.getLocalInfo().ghostLayer[1]};

    size_t localIXE[2] = {iXE[0]-_domainInfo.getLocalInfo().globalIndexStart[0]+_domainInfo.getLocalInfo().ghostLayer[0],
                            iXE[1]-_domainInfo.getLocalInfo().globalIndexStart[1]+_domainInfo.getLocalInfo().ghostLayer[1]};

    // std::cout << "valüüüüüüüüüüüüüüs in local " << localIXS[0] <<" " << localIXS[1] << " " << localIXS[2] << " " << localIXE[0] << " " << localIXE[1] << " " << localIXE[2] << std::endl; 

    if (nx > 0 && ny > 0 && _domainInfo.getLocalInfo().localHasGrid)
    {
    preamble( fullNameVti, nx,ny,iXS[0],iXS[1],0.0, _scaling );
    if ( _binary ) {
      // iterate on functors
      for ( auto functor = _pointerVec.cbegin(); functor != _pointerVec.cend(); ++functor) {
        writeRawDataBinary( fullNameVti, **functor, localIXS, localIXE);
      }
    } else {
      for ( auto functor = _pointerVec.cbegin(); functor != _pointerVec.cend(); ++functor) {
        writeRawData( fullNameVti, **functor, localIXS, localIXE);
      }
    }
    closePreamble( fullNameVti );
    }
  }
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::write( int iT , float timeStep, size_t* globalOrigin, size_t* globalExtend) {
	float position[2] = {0,0};
	write(iT, timeStep, position, globalOrigin, globalExtend);
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::write( int iT , float timeStep, float* position, size_t* globalOrigin, size_t* globalExtend)
{
  size_t origin[2];
  size_t extend[2];
  if (!globalOrigin) {
    origin[0] = _domainInfo.refSubDomain.globalIndexStart[0];
    origin[1] = _domainInfo.refSubDomain.globalIndexStart[1];
    globalOrigin = origin;
  }
  if (!globalExtend) {
    extend[0] = _domainInfo.refSubDomain.globalIndexEnd[0];
    extend[1] = _domainInfo.refSubDomain.globalIndexEnd[1];
    globalExtend = extend;
  }

  for (int i = 0; i < Lattice::d; i++) {
    globalExtend[i] = min(globalExtend[i], _domainInfo.refSubDomain.globalIndexEnd[i]);
  }

  auto const & subDomain = _domainInfo.getLocalInfo();

  // unsigned ghostLayer[] = {0,0}; //this part appears unused here
  // if (_printGhostLayer)
  //   for (unsigned i=0;i<2;++i)
  //   {
  //     ghostLayer[i] = _domainInfo.getLocalInfo().ghostLayer[i];
  //   }

  if(_domainInfo.localSubDomain == 0) {

	  std::string fullNamePvti = singleton::directories().getVtkOutDir()
								   + createFileName( _name, iT ) + ".pvti";

	  _pvdFile << "<DataSet timestep=\"" << timeStep << "\" part=\"0\"\n";
	  _pvdFile << " file=\"" << createFileName( _name, iT ) + ".pvti" << "\"/>" << std::endl;

	  std::ofstream fout(fullNamePvti.c_str());
	  if (!fout) {
		clout << "Error: could not open " << fullNamePvti << std::endl;
	  }

	  fout << "<?xml version=\"1.0\"?>\n";
	  fout << "<VTKFile type=\"PImageData\" version=\"0.1\" "
		   << "byte_order=\"LittleEndian\">\n";
	  fout << "<PImageData WholeExtent=\""
		   << globalOrigin[0] <<" "<< globalExtend[0]-1 <<" "
		   << globalOrigin[1] <<" "<< globalExtend[1]-1 <<" "
		   << 0 << " " << 0
		   << "\" GhostLevel= \"0\" "
		   << "Origin=\"" << position[0] << " " << position[1] << " " << 0
		   << "\" Spacing=\"" << _scaling << " " << _scaling << " " << _scaling << "\">\n";
	  fout << "<PPointData>\n";

	  for(auto func : _pointerVec) {
			fout << "<PDataArray " ;
			if (func->getTargetDim() == 1) {
			  fout << "type=\"Float32\" Name=\"" << func->getName() << "\" "
				   << "format=\"binary\" encoding=\"base64\"/>\n";
			} else {
			  fout << "type=\"Float32\" Name=\"" << func->getName() << "\" "
				   << "format=\"binary\" encoding=\"base64\" "
				   << "NumberOfComponents=\"" << func->getTargetDim() << "\"/>\n";
			}
	  }
	  fout << "</PPointData>\n";

	  for(unsigned int ii=0; ii<_domainInfo.noSubDomains; ++ii) {
		  std::array<size_t,Lattice::d> nCells;
		  std::array<long long int,Lattice::d> localIXE;
		  std::array<long long int,Lattice::d> localIXS;
		  computeDomainStuff(_domainInfo.subDomains[ii], globalOrigin, globalExtend, nCells, localIXS, localIXE);

      bool noPrint = false;
		  for(auto iCells : nCells)
			  if(iCells == 0 || iCells == 1) {
				  noPrint = true;
		    }
      if (noPrint) continue;

		  fout << "<Piece Extent=\"" << localIXS[0] << " " << localIXE[0]-1 << " " <<
				  localIXS[1] << " " << localIXE[1]-1  << " " <<
				  0 << " " << 0 << "\" " <<
				  "Source=\"" << createFileName( _name+ "_GPU" + std::to_string(ii), iT ) << ".vti\"/>\n";
		  
	  }
	  fout << "</PImageData>\n";
	  fout << "</VTKFile>\n";

	  fout.close();
  }


  if ( _pointerVec.empty() ) {
    clout << "Error: Please add functor via addFunctor()";
  } else {

   std::array<size_t,Lattice::d> nCells;
   std::array<long long int,Lattice::d> iXS;
   std::array<long long int,Lattice::d> iXE;
   computeDomainStuff(_domainInfo.getLocalInfo(), globalOrigin, globalExtend, nCells, iXS, iXE);


    size_t nx = std::max(iXE[0]-iXS[0], (long long int)1)-1;
    size_t ny = std::max(iXE[1]-iXS[1], (long long int)1)-1;

    size_t localIXS[2] = {iXS[0]-_domainInfo.getLocalInfo().globalIndexStart[0]+_domainInfo.getLocalInfo().ghostLayer[0],
                            iXS[1]-_domainInfo.getLocalInfo().globalIndexStart[1]+_domainInfo.getLocalInfo().ghostLayer[1]};

    size_t localIXE[2] = {iXE[0]-_domainInfo.getLocalInfo().globalIndexStart[0]+_domainInfo.getLocalInfo().ghostLayer[0],
                            iXE[1]-_domainInfo.getLocalInfo().globalIndexStart[1]+_domainInfo.getLocalInfo().ghostLayer[1]};


    std::string fullNameVti = singleton::directories().getVtkOutDir()
                              + createFileName( _name + "_GPU" + std::to_string(_domainInfo.getLocalInfo().localSubDomain), iT ) + ".vti";

    if (nx > 0 && ny > 0)
    {
    preamble( fullNameVti, iXS.data(), iXE.data(), position,_scaling);
    if ( _binary ) {
      // iterate on functors
      for ( auto functor = _pointerVec.cbegin(); functor != _pointerVec.cend(); ++functor) {
        writeRawDataBinary( fullNameVti, **functor, localIXS, localIXE);
      }
    } else {
      for ( auto functor = _pointerVec.cbegin(); functor != _pointerVec.cend(); ++functor) {
        writeRawData( fullNameVti, **functor, localIXS, localIXE);
      }
    }
    closePreamble( fullNameVti );
    }
  }
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::addFunctor(BlockF2D<T>& f)
{
  _pointerVec.push_back(&f);
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::clearAddedFunctors()
{
  _pointerVec.clear();
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::preamble(const std::string& fullName, size_t nx, size_t ny,
                                   T originX, T originY, T originZ,T scale)
{
    std::ofstream fout(fullName.c_str());
    if (!fout) {
      clout << "Error: could not open " << fullName << std::endl;
    }
    T spacing = scale;

    fout << "<?xml version=\"1.0\"?>\n";
    fout << "<VTKFile type=\"ImageData\" version=\"0.1\" "
         << "byte_order=\"LittleEndian\">\n";
    fout << "<ImageData WholeExtent=\""
         << "0" <<" "<< nx <<" "
         << "0" <<" "<< ny <<" "
         << "0" <<" "<< 0
         << "\" Origin=\"" << originX*spacing << " " << originY*spacing << " " << originZ*spacing
         << "\" Spacing=\"" << spacing << " " << spacing << " " << spacing << "\">\n";

    fout << "<Piece Extent=\""
         << 0 <<" "<< nx <<" "
         << 0 <<" "<< ny <<" "
         << 0 <<" "<< 0 <<"\">\n";

    fout << "<PointData>\n";
    fout.close();


    // std::cout << " ############### dimensions in preamble " << nx << " " << ny << " "<< nz << std::endl; 
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::preamble(const std::string& fullName,
		long long int* localIXS, long long int* localIXE, float* origin, T scale)
{
    std::ofstream fout(fullName.c_str());
    if (!fout) {
      clout << "Error: could not open " << fullName << std::endl;
    }
    T spacing = scale;

    fout << "<?xml version=\"1.0\"?>\n";
    fout << "<VTKFile type=\"ImageData\" version=\"0.1\" "
         << "byte_order=\"LittleEndian\">\n";
    fout << "<ImageData WholeExtent=\""
         << localIXS[0] <<" "<< localIXE[0]-1 <<" "
         << localIXS[1] <<" "<< localIXE[1]-1 <<" "
         << 0 <<" "<< 0
         << "\" Origin=\"" << origin[0] << " " << origin[1] << " " << 0
         << "\" Spacing=\"" << spacing << " " << spacing << " " << spacing << "\">\n";

    fout << "<Piece Extent=\""
         << localIXS[0] <<" "<< localIXE[0]-1 <<" "
         << localIXS[1] <<" "<< localIXE[1]-1 <<" "
         << 0 << " "<< 0 <<"\">\n";

    fout << "<PointData>\n";
    fout.close();


    // std::cout << " ############### dimensions in preamble " << nx << " " << ny << " "<< nz << std::endl;
}

template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::closePreamble(const std::string& fullNamePiece)
{
    std::ofstream fout(fullNamePiece.c_str(), std::ios::app );
    if (!fout) {
      clout << "Error: could not open " << fullNamePiece << std::endl;
    }
    fout << "</PointData>\n";
    fout << "</Piece>\n";
    fout << "</ImageData>\n";
    fout << "</VTKFile>\n";
    fout.close();
}



template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::writeRawData(const std::string& fullNameVti, BlockF2D<T>& f,
                                       size_t* iXS, size_t* iXE)
{
  std::ofstream fout(fullNameVti.c_str(), std::ios::app);
  if (!fout) {
    clout << "Error: could not open " << fullNameVti << std::endl;
  }

    fout << "<DataArray " ;
    if (f.getTargetDim() == 1) {
      fout << "type=\"Float32\" Name=\"" << f.getName() << "\">\n";
    } else {
      fout << "type=\"Float32\" Name=\"" << f.getName() << "\" "
           << "NumberOfComponents=\"" << f.getTargetDim() << "\">\n";
    }

  // size_t iX[3] = {0,0,0}; //appears unused
  T evaluated[f.getTargetDim()];
  for (int iDim = 0; iDim < f.getTargetDim(); ++iDim) {
    evaluated[iDim] = T();
  }
  for (size_t iY = iXS[1]; iY < iXE[1]; ++iY) {
    for (size_t iX = iXS[0]; iX < iXE[0]; ++iX) {
      int idx[2] = {static_cast<int>(iX),static_cast<int>(iY)};
      f(evaluated,idx);
      // std::cout << " wrote value " << iX <<" " << iY << " "<< iZ << std::endl;
      for (int iDim = 0; iDim < f.getTargetDim(); ++iDim) {
        fout << evaluated[iDim] << " ";
      }
    }
  }
  
    fout << "\n</DataArray>\n";

  fout.close();
}

//  uses base64 encoder to write binary output
//  first number is written by a seperate sizeEncoder
//  this number indicates how many numbers will be stored.
//  then dataEncoder will be called to write output.
//  !!code is fixed to float functor values!!
template<typename T,typename Lattice>
void BlockVTKwriterMultiLattice2D<T,Lattice>::writeRawDataBinary(const std::string& fullNameVti,
    BlockF2D<T>& f, size_t* iXS, size_t* iXE)
{
  // std::cout << "ääääää value in writeBin " << iXS[0] <<" " << iXS[1] <<" " << iXS[2] <<" " << iXE[0] <<" "<< iXE[1] <<" "<< iXE[2] << std::endl;    
  const char* fileName = fullNameVti.c_str();
  std::ofstream fout(fileName, std::ios::app);
  if (!fout) {
    clout << "Error: could not open " << fileName << std::endl;
  }

    fout << "<DataArray " ;
    if (f.getTargetDim() == 1) {
      fout << "type=\"Float32\" Name=\"" << f.getName() << "\" "
           << "format=\"binary\" encoding=\"base64\">\n";
    } else {
      fout << "type=\"Float32\" Name=\"" << f.getName() << "\" "
           << "format=\"binary\" encoding=\"base64\" "
           << "NumberOfComponents=\"" << f.getTargetDim() << "\">\n";
    }
  fout.close();

  std::ofstream ofstr( fileName, std::ios::out | std::ios::app | std::ios::binary );
  if (!ofstr) {
    clout << "Error: could not open " << fileName << std::endl;
  }

  size_t fullSize = f.getTargetDim() * (iXE[0]-iXS[0]) * (iXE[1]-iXS[1]);
  size_t binarySize = size_t( fullSize * sizeof(float) );
  // writes first number, which have to be the size(byte) of the following data
  Base64Encoder<unsigned int> sizeEncoder(ofstr, 1);
  unsigned int uintBinarySize = (unsigned int)binarySize;
  sizeEncoder.encode(&uintBinarySize, 1);
  //  write numbers from functor
  Base64Encoder<float>* dataEncoder = nullptr;
  dataEncoder = new Base64Encoder<float>( ofstr, fullSize );

  size_t iX[2] = {0,0};
  T evaluated[f.getTargetDim()];
  for (int iDim = 0; iDim < f.getTargetDim(); ++iDim) {
    evaluated[iDim] = T();
  }
  for (iX[1] = iXS[1]; iX[1] < iXE[1]; ++iX[1]) {
    for (iX[0] = iXS[0]; iX[0] < iXE[0]; ++iX[0]) {
      int idx[2] = {static_cast<int>(iX[0]),static_cast<int>(iX[1])};
      f(evaluated,idx);
      for (int iDim = 0; iDim<f.getTargetDim(); ++iDim) {
        const float evaluated2 = float( evaluated[iDim] );
        dataEncoder->encode( &evaluated2, 1 );
      }
    }
  }
  
  ofstr.close();

  std::ofstream foutt(fileName,  std::ios::out | std::ios::app);
  if (!foutt) {
    clout << "Error: could not open " << fileName << std::endl;
  }
  foutt << "\n</DataArray>\n";
  foutt.close();
  delete dataEncoder;
}


}  // namespace olb

#endif
