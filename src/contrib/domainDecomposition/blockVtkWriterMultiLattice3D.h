#ifndef BLOCK_VTK_WRITER_MULTILATTICE_3D_H
#define BLOCK_VTK_WRITER_MULTILATTICE_3D_H

#include "io/ostreamManager.h"
#include "functors/lattice/blockBaseF3D.h"

namespace olb {

template<typename T,typename Lattice>
class BlockVTKwriterMultiLattice3D {
public:
	BlockVTKwriterMultiLattice3D( std::string name, DomainInformation<T,Lattice>& domainInfo,
			  float scaling, bool binary=true, bool printGhostLayer = true );
  BlockVTKwriterMultiLattice3D( std::string name, DomainInformation<T,Lattice>& domainInfo,
		  bool binary=true, bool printGhostLayer = true );
  ~BlockVTKwriterMultiLattice3D();
  ///  method calls preamble(), pointData(), data() and coresponding
  ///  closing methods.
  ///  writes functors stored at pointerVec
  void write( int iT , size_t* globalOrigin = nullptr, size_t* globalExtend = nullptr);
  void write( int iT , float timeStep, size_t* globalOrigin = nullptr,size_t* globalExtend = nullptr);
  void write( int iT , float timeStep, float* position, size_t* globalOrigin = nullptr, size_t* globalExtend = nullptr);
  ///  put functor to _pointerVec
  ///  to simplify writing process of several functors
  void addFunctor( BlockF3D<T>& f );
  ///  to clear stored functors
  void clearAddedFunctors();
  void computeDomainStuff(SubDomainInformation<T,Lattice> const & subDomain, size_t* globalOrigin, size_t* globalExtend,
  		std::array<size_t,Lattice::d>& nCells, std::array<long long int,Lattice::d>& localIXS, std::array<long long int,Lattice::d>& localIXE);
  ///  writes <VTKFile .... >, <ImageData ... >, <Piece ... > and  <PointData Scalar="..." >
  std::string getName();
private:
  void preamble( const std::string& fullName, size_t nx, size_t ny, size_t nz,
                 T originX, T originY, T originZ, T scale );
  void preamble(const std::string& fullName,
		  long long int* localIXS, long long int* localIXE, float* origin, T scale);
  ///  writes </PointData>, </Piece>, </ImageData> and  </VTKFile>
  void closePreamble( const std::string& fullName );
  ///  writes data of given functor f as ASCII
  void writeRawData( const std::string& fullNameVti, BlockF3D<T>& f, size_t* iXS, size_t* iXE );
  ///  writes data of given functor f as binary
  void writeRawDataBinary( const std::string& fullNameVti, BlockF3D<T>& f,
                           size_t* iXS, size_t* iXE);
  void finalize();
private:
  mutable OstreamManager clout;
  ///  determines the name of .vti per iT
  std::string _name;
  ///  default is true, may be changed at constructor
  bool _binary;
  bool _printGhostLayer;
  ///  holds added functor, to simplify the use of write function
  std::vector< BlockF3D<T>* > _pointerVec;
  DomainInformation<T,Lattice>& _domainInfo;
  float _scaling;
  std::ofstream _pvdFile;
};

}  // namespace olb


#endif
