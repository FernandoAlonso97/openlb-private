#ifndef COMMUNICATION_H

#define COMMUNICATION_H

#include "contrib/domainDecomposition/cudaIPC.h"
// #include <stdlib.h> 
// #include "core/blockLatticeALE3D.h"
#include "core/blockLatticeALE3D.hh"
#include "contrib/domainDecomposition/domainDecomposition.h"
// #include "core/ALEutil.h"


namespace olb {

#ifdef ENABLE_CUDA
template<typename T>
__global__ void printKernel (T* data,long long * index, size_t gridSize,int domain,int population,int iPop)
{
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= gridSize)
    return;

  printf("Value %E %lu %i %i %i\n",data[threadIndex],index[threadIndex],domain,population,iPop);
}
#endif


template<typename T, class Lattice ,template<typename> class Memory>
class NumaSwapCommunication {

  public:

  NumaSwapCommunication() = delete;

  explicit NumaSwapCommunication(std::vector<CommunicationDataHandler<T,Lattice,Memory>>& commDataHandler):commDataHandler_(commDataHandler) {}

  void operator() ()
  {
    communicateSwap(commDataHandler_);
  }

  private:
    std::vector<CommunicationDataHandler<T,Lattice,Memory>>&  commDataHandler_;
};


#ifdef ENABLE_CUDA
std::ostream& operator<< (std::ostream& stream, const cudaIpcMemHandle_t& hdl)
{
  stream << reinterpret_cast<long long>(hdl.reserved);
  return stream;
}
#endif

void CheckMPICall(int result, MPI_Status status, int expectedElements, int myRank, std::ostream& report)
{
    if (result != MPI_SUCCESS)
      report << "Operation not successfull on rank " << myRank << "("
                  << result << ")" << std::endl;

    int count = 0;
    result = -1;
    result = MPI_Get_count(&status, MPI_BYTE, &count);
    if (result != MPI_SUCCESS)
      report << "Getting number of elements not successful on rank " << myRank << std::endl;
    if (count != expectedElements)
      report << "Received " << count << " elements on rank " << myRank << " expected " << expectedElements
          << std::endl;
    int flag = -1;
    MPI_Test_cancelled(&status, &flag);
    if (flag == true)
      report << "Operation was cancelled" << std::endl;
}

template<typename T,class Lattice>
class mpiCommunication {
  public:

    mpiCommunication() = delete;

    explicit mpiCommunication(CommunicationDataHandler<T,Lattice,memory_space::HostHeap>& commDataHandlerIn):commDataHandler(commDataHandlerIn)
  {}

    void operator() ()
    {
    

      for (unsigned neighbour=0;neighbour < Lattice::q; ++neighbour)
      {
        if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
        {
          for (unsigned iPop=0;iPop<Lattice::q;++iPop)
          {
            if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
            {
              int successIPC = -1;
              MPI_Status statusIPC;
              unsigned callIdOut = commDataHandler.domainInfo.getLocalInfo().idNeighbour[neighbour] + neighbour*100 + iPop * 10000;
              successIPC = MPI_Send(&commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData[0],
                                     commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.size(),MPI_DOUBLE,commDataHandler.domainInfo.getLocalInfo().idNeighbour[neighbour],callIdOut,MPI_COMM_WORLD);
#ifdef OLB_DEBUG
              CheckMPICall(successIPC,statusIPC,commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.size(),commDataHandler.domainInfo.getLocalInfo().localSubDomain,std::cout);
              std::cout << "MPI Send for indices " << neighbour << " " << iPop << " With tag " << callIdOut << std::endl;
#endif
            }
          }
        }

        
        if (commDataHandler.domainInfo.getLocalInfo().hasNeighbourIn[neighbour])
        {
          for (unsigned iPop=0;iPop<Lattice::q;++iPop)
          {
           if (commDataHandler.getCommunicationDataIn()[neighbour].popArrays[iPop].popData)
           {
              int successIPC = -1;
              MPI_Status statusIPC;
              unsigned callIdIn = commDataHandler.domainInfo.getLocalInfo().localSubDomain + neighbour*100 + iPop * 10000;
              successIPC = MPI_Recv(&commDataHandler.getCommunicationDataIn()[neighbour].popArrays[iPop].popData[0],
                                     commDataHandler.getCommunicationDataIn()[neighbour].popArrays[iPop].popData.size(),MPI_DOUBLE,commDataHandler.domainInfo.getLocalInfo().idNeighbourIn[neighbour],callIdIn,MPI_COMM_WORLD,&statusIPC);
#ifdef OLB_DEBUG
              CheckMPICall(successIPC,statusIPC,commDataHandler.getCommunicationDataIn()[neighbour].popArrays[iPop].popData.size(),commDataHandler.domainInfo.getLocalInfo().localSubDomain,std::cout);
              std::cout << "MPI Recv for indices " << neighbour << " " << iPop << " with tags " << callIdIn << std::endl;
#endif
           }
         }
       }
      }


  MPI_Barrier (MPI_COMM_WORLD);
    }
  private:
    CommunicationDataHandler<T,Lattice,memory_space::HostHeap>& commDataHandler;

};

#ifdef ENABLE_CUDA
template<typename T,class Lattice>
class ipcCommunication {

  public:
      
  ipcCommunication() = delete;

  explicit ipcCommunication(CommunicationDataHandler<T,Lattice,memory_space::CudaDeviceHeap>& commDataHandler):commDataHandler_(commDataHandler) 
  {


 // for (unsigned neighbour=0;neighbour < Lattice::q; ++neighbour)
  // {
      // for (unsigned iPop=0;iPop<Lattice::q;++iPop)
      // {
        // memHandlesOut[neighbour][iPop] = 0;
        // memHandlesIn[neighbour][iPop] = 0;
      // }
  // }
 

  // get outgoing ipcHandles
  
 for (unsigned neighbour=0;neighbour < Lattice::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
      for (unsigned iPop=0;iPop<Lattice::q+1+Lattice::d;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
         cudaIpcGetMemHandle(
          commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.getMemHandle(),
          commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.get()
          );
          // memHandlesOut[neighbour][iPop] = neighbour + 10*iPop;
#ifdef OLB_DEBUG
          std::cout << "CudaIPCMemHandle created " << " for neighbour " << neighbour << " and iPop " << iPop << " on domain " << commDataHandler.domainInfo.getLocalInfo().localSubDomain << std::endl;
#endif
       }
      }
    }
  }

  cudaDeviceSynchronize();
  MPI_Barrier (MPI_COMM_WORLD);

 for (unsigned neighbour=0;neighbour < Lattice::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
      for (unsigned iPop=0;iPop<Lattice::q+1+Lattice::d;++iPop)
      {
        if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
        {
          __attribute__ ((unused)) int successIPC = -1; //labelled as potentially unused to suppress warnings
          __attribute__ ((unused)) MPI_Status statusIPC; //labelled as potentially unused to suppress warnings
          unsigned callIdOut = commDataHandler.domainInfo.getLocalInfo().idNeighbour[neighbour] + neighbour*100 + iPop * 10000;
          successIPC = MPI_Send(commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.getMemHandle(),sizeof(cudaIpcMemHandle_t),MPI_BYTE,commDataHandler.domainInfo.getLocalInfo().idNeighbour[neighbour],callIdOut,MPI_COMM_WORLD);
#ifdef OLB_DEBUG
          CheckMPICall(successIPC,statusIPC,sizeof(cudaIpcMemHandle_t),commDataHandler.domainInfo.getLocalInfo().localSubDomain,std::cout);
          std::cout << "MPI Send for indices " << neighbour << " " << iPop << " With tag " << callIdOut << std::endl;
#endif
        }
      }
    }
  
    
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbourIn[neighbour])
    {
      for (unsigned iPop=0;iPop<Lattice::q+1+Lattice::d;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[neighbour].popArrays[iPop].popData)
       {
          __attribute__ ((unused)) int successIPC = -1; //labelled as potentially unused to suppress warnings
          __attribute__ ((unused)) MPI_Status statusIPC; //labelled as potentially unused to suppress warnings
          unsigned callIdIn = commDataHandler.domainInfo.getLocalInfo().localSubDomain + neighbour*100 + iPop * 10000;
          successIPC = MPI_Recv(commDataHandler.getCommunicationDataIn()[neighbour].popArrays[iPop].popData.getMemHandle(),sizeof(cudaIpcMemHandle_t),MPI_BYTE,commDataHandler.domainInfo.getLocalInfo().idNeighbourIn[neighbour],callIdIn,MPI_COMM_WORLD,&statusIPC);
#ifdef OLB_DEBUG
          CheckMPICall(successIPC,statusIPC,sizeof(int),commDataHandler.domainInfo.getLocalInfo().localSubDomain,std::cout);
          std::cout << "MPI Recv for indices " << neighbour << " " << iPop << " with tags " << callIdIn << std::endl;
#endif
       }
     }
   }
  }


  // open incoming ipcHandles
  cudaDeviceSynchronize();
  MPI_Barrier (MPI_COMM_WORLD);
    
for (unsigned population=0;population < Lattice::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbourIn[population])
    {
      for (unsigned iPop=0;iPop<Lattice::q+1+Lattice::d;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       { 
#ifdef OLB_DEBUG
          std::cout << "CudaIPCMemHandle opened "  << " for neighbour " << population << " and iPop " << iPop << " on domain " << commDataHandler.domainInfo.getLocalInfo().localSubDomain << std::endl;
#endif
         cudaIpcOpenMemHandle(commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData.getDevPtrLoc(),*(commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData.getMemHandle()),cudaIpcMemLazyEnablePeerAccess);
         // commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData.set(handlePtr);
       }
      }
     }
   }
  
  
  cudaDeviceSynchronize();
  MPI_Barrier (MPI_COMM_WORLD);

  }

  void operator() ()
  {
  }



  private:
   CommunicationDataHandler<T,Lattice,memory_space::CudaDeviceHeap>& commDataHandler_;

};
#endif
template<typename T, class Lattice ,template<typename> class Memory>
void communicateSwap(std::vector<CommunicationDataHandler<T,Lattice,Memory>>& commDataHandler)
{
  for (unsigned domain=0;domain<commDataHandler[0].domainInfo.getLocalInfo().noSubDomains;++domain)
  {
    for (unsigned direction=0;direction<Lattice::q;++direction)
    {
      if (commDataHandler[domain].domainInfo.getLocalInfo().hasNeighbour[direction])
      {
        // search corresponding neighbour
        
        CommunicationDataHandler<T,Lattice,Memory>* neighbour = nullptr;
        int nId = -1;
        for (unsigned i=0;i<commDataHandler[domain].domainInfo.getLocalInfo().noSubDomains;++i)
        {
          if (commDataHandler[i].domainInfo.getLocalInfo().localSubDomain == commDataHandler[domain].domainInfo.getLocalInfo().idNeighbour[direction])
          {
            neighbour = &commDataHandler[i];
            nId = commDataHandler[i].domainInfo.getLocalInfo().localSubDomain;
          }
        }

        if (neighbour != nullptr)
        {
          // std::cout << "for domain " << domain << " found neighbour " << nId << std::endl;
          // std::cout << "Base: \n" << commDataHandler[domain] << " \nNeighbour: \n" << *neighbour << std::endl; 
          for (unsigned pop=0;pop<Lattice::q;++pop)
          {
            
            // for (unsigned i=0;i<neighbour->getCommunicationDataIn()[direction].popArrays[pop].popData.size();++i)
              // std::cout << commDataHandler[domain].getCommunicationDataOut()[direction].popArrays[(pop)].popData[i] <<" ";
            // neighbour->getCommunicationDataIn()[direction].popArrays[pop].popData[i] = 5;

            if (commDataHandler[domain].getCommunicationDataOut()[direction].popArrays[pop].popData &&
                (*neighbour).getCommunicationDataIn()[direction].popArrays[pop].popData) 
            {
            // std::cout << "pre swapping values(OUT) of me " << commDataHandler[domain].domainInfo.getLocalInfo().localSubDomain  << " for pop " << pop <<  
            // " values " << commDataHandler[domain].getCommunicationDataOut()[direction].popArrays[(pop)].popData[34] 
            // << " to(IN) " << neighbour->domainInfo.getLocalInfo().localSubDomain <<  " for pop " << pop << " values " << 
            // neighbour->getCommunicationDataIn()[direction].popArrays[pop].popData[34] << std::endl;

            std::swap(neighbour->getCommunicationDataIn()[direction].popArrays[pop].popData,
            commDataHandler[domain].getCommunicationDataOut()[direction].popArrays[(pop)].popData);

            // std::cout << "post swapping values(OUT) of me " << commDataHandler[domain].domainInfo.getLocalInfo().localSubDomain  << 
            // " values " << commDataHandler[domain].getCommunicationDataOut()[direction].popArrays[(pop)].popData[34] 
            // << " to(IN) " << neighbour->domainInfo.getLocalInfo().localSubDomain << " which now has the values " << 
            // neighbour->getCommunicationDataIn()[direction].popArrays[pop].popData[34] << std::endl;
            }
          }
        }
        else
        {
          std::cout << "error: Neighbour domain not in commDataHandler list" << std::endl;
          exit(EXIT_FAILURE);
        }
    
      }
    }
  
  }
  
}

template<typename T,class Lattice>
class mpiWinFullCommunication{

  public:

    mpiWinFullCommunication() = delete;

    mpiWinFullCommunication(DomainInformation<T,Lattice>& domInfo,double** latticeData_even,double** latticeData_odd,double** latticeDataALE_even,double** latticeDataALE_odd) 
  {
    // create windows
    for (unsigned i=0;i<Lattice::dataSize;++i)
    {
      MPI_Win_create(latticeData_even[i],sizeof(MPI_DOUBLE)*domInfo.getLocalInfo().localGridSize()[0]*domInfo.getLocalInfo().localGridSize()[1]*domInfo.getLocalInfo().localGridSize()[2],sizeof(MPI_DOUBLE),MPI_INFO_NULL,MPI_COMM_WORLD,&windows_even[i]);

      MPI_Win_create(latticeData_odd[i],sizeof(MPI_DOUBLE)*domInfo.getLocalInfo().localGridSize()[0]*domInfo.getLocalInfo().localGridSize()[1]*domInfo.getLocalInfo().localGridSize()[2],sizeof(MPI_DOUBLE),MPI_INFO_NULL,MPI_COMM_WORLD,&windows_odd[i]);

      MPI_Win_create(latticeDataALE_even[i],sizeof(MPI_DOUBLE)*domInfo.getLocalInfo().localGridSize()[0]*domInfo.getLocalInfo().localGridSize()[1]*domInfo.getLocalInfo().localGridSize()[2],sizeof(MPI_DOUBLE),MPI_INFO_NULL,MPI_COMM_WORLD,&windowsALE_even[i]);

      MPI_Win_create(latticeDataALE_odd[i],sizeof(MPI_DOUBLE)*domInfo.getLocalInfo().localGridSize()[0]*domInfo.getLocalInfo().localGridSize()[1]*domInfo.getLocalInfo().localGridSize()[2],sizeof(MPI_DOUBLE),MPI_INFO_NULL,MPI_COMM_WORLD,&windowsALE_odd[i]);
    
    }
    MPI_Barrier (MPI_COMM_WORLD);


    for (unsigned i=0;i<Lattice::dataSize;++i)
    {
      windows_current[i] = &windows_even[i];
      windowsALE_current[i] = &windowsALE_even[i];
    }
  }

  ~mpiWinFullCommunication ()
  {
    MPI_Barrier (MPI_COMM_WORLD);
    for (unsigned i=0;i<Lattice::dataSize;++i)
    {
      MPI_Win_free (&windows_even[i]);
      MPI_Win_free (&windows_odd[i]);
      MPI_Win_free (&windowsALE_even[i]);
      MPI_Win_free (&windowsALE_odd[i]);
    }
  }   

  double get (size_t index,unsigned domain,unsigned iPop)
  {
    double value;

    int mpiRank; MPI_Comm_rank(MPI_COMM_WORLD,&mpiRank);
    if (domain != mpiRank && domain ==0)
    {
    std::cout << "accessing index " << index << " on domain " << domain << " from domain " <<  mpiRank << std::endl;
    }
    MPI_Win_lock(MPI_LOCK_SHARED,domain, 0, *windows_current[iPop]);
    MPI_Get(&value,1,MPI_DOUBLE,domain,index,1,MPI_DOUBLE,*windows_current[iPop]);
    MPI_Win_unlock(domain, *windows_current[iPop]);
    return value;
  }

  double getALE (size_t index,unsigned domain,unsigned iPop)
  {
    double value;

    // int mpiRank; MPI_Comm_rank(MPI_COMM_WORLD,&mpiRank);
    // std::cout << "ALE accessing index " << index << " on domain " << domain << " from domain " <<  mpiRank << std::endl;
    MPI_Win_lock(MPI_LOCK_SHARED,domain, 0, *windowsALE_current[iPop]);
    MPI_Get(&value,1,MPI_DOUBLE,domain,index,1,MPI_DOUBLE,*windowsALE_current[iPop]);
    MPI_Win_unlock(domain, *windowsALE_current[iPop]);
    return value;
  }

  void set (double value,size_t index,unsigned domain,unsigned iPop)
  {
    // int mpiRank; MPI_Comm_rank(MPI_COMM_WORLD,&mpiRank);
    // std::cout << "write accessing index " << index << " on domain " << domain << " from domain " <<  mpiRank << std::endl;
    MPI_Win_lock(MPI_LOCK_SHARED,domain, 0, *windows_current[iPop]);
    MPI_Put(&value,1,MPI_DOUBLE,domain,index,1,MPI_DOUBLE,*windows_current[iPop]);
    MPI_Win_unlock(domain, *windows_current[iPop]);
  }

  void setALE (double value,size_t index,unsigned domain,unsigned iPop)
  {
    // int mpiRank; MPI_Comm_rank(MPI_COMM_WORLD,&mpiRank);
    // std::cout << "ALE write accessing index " << index << " on domain " << domain << " from domain " <<  mpiRank << std::endl;
    MPI_Win_lock(MPI_LOCK_SHARED,domain, 0, *windowsALE_current[iPop]);
    MPI_Put(&value,1,MPI_DOUBLE,domain,index,1,MPI_DOUBLE,*windowsALE_current[iPop]);
    MPI_Win_unlock(domain, *windowsALE_current[iPop]);
  }

  void stream()
  {
        if (streamEven) {
            if (!aleEven) {
                for (unsigned i=0;i<Lattice::dataSize;++i)
                {
                  windowsALE_current[i] = &windowsALE_odd[i];
                  windows_current[i] = &windows_odd[i];
                }
            } else {
                for (unsigned i=0;i<Lattice::dataSize;++i)
                {
                  windowsALE_current[i] = &windows_odd[i];
                  windows_current[i] = &windowsALE_odd[i];
                }
            }
        } else {
            if (!aleEven) {
                for (unsigned i=0;i<Lattice::dataSize;++i)
                {
                  windowsALE_current[i] = &windowsALE_even[i];
                  windows_current[i] = &windows_even[i];
                }
            } else {
                for (unsigned i=0;i<Lattice::dataSize;++i)
                {
                  windowsALE_current[i] = &windows_even[i];
                  windows_current[i] = &windowsALE_even[i];
                }
            }
        ;}
        streamEven = !streamEven;
  }
  
  void moveMesh()
  {
    for (unsigned i=0;i<Lattice::dataSize;++i)
        std::swap(windows_current[i], windowsALE_current[i]);

        aleEven = !aleEven;
  }


  private:

    MPI_Win windows_even[Lattice::dataSize];
    MPI_Win windows_odd[Lattice::dataSize];

    MPI_Win windowsALE_even[Lattice::dataSize];
    MPI_Win windowsALE_odd[Lattice::dataSize];

    MPI_Win* windows_current[Lattice::dataSize];
    MPI_Win* windowsALE_current[Lattice::dataSize];

    bool streamEven = true;
    bool aleEven = false;
};

template<typename T,template<typename>class Lattice,class LatticeVersion>
mpiWinFullCommunication<T,Lattice<T>> createMPIWinFullCommunication (LatticeVersion& lattice,DomainInformation<T,Lattice<T>>& domainInfo)
{
  return mpiWinFullCommunication<T,Lattice<T>> {domainInfo,
                                                lattice.getCellBlockData().getFluidDataEven(),
                                                lattice.getCellBlockData().getFluidDataOdd(),
                                                lattice.getCellBlockData().getFluidDataALEEven(),
                                                lattice.getCellBlockData().getFluidDataALEOdd()
  }; 
}
//############################ Runtime functions #####################

template<typename Lattice,typename CommBuffer>  
void initalizeCommDataMultilatticeCPU(Lattice& lattice, CommBuffer& commBuffer)
{
  // for(unsigned i=0;i<noLattices;++i)
  // {
#ifdef OLB_DEBUG
  std::cout << "Starting calculation of indices for cells that are communicated" << std::endl;
#endif
    calculateCommunicationIndicesCPU(commBuffer,lattice); 
  // }
}


template<typename FluidDynamics, typename Lattice, typename CommBuffer,typename Callable>
void collideAndStreamMultilattice (Lattice& lattice, CommBuffer& commBuffer, Callable& communicator)
{
#ifdef OLB_DEBUG
  std::cout << "executing collision ...";
#endif
    lattice.template collide<FluidDynamics>();
#ifdef OLB_DEBUG
  std::cout << "done" << std::endl;
#endif
#ifdef OLB_DEBUG
  std::cout << "writing data to communication buffer ... ";
#endif
    writeToCommBufferFromLattice(commBuffer,lattice);
#ifdef OLB_DEBUG
  std::cout << "done" << std::endl;
#endif

  // communicate
#ifdef OLB_DEBUG
  std::cout << "calling communicator ... ";
#endif
  communicator();
#ifdef OLB_DEBUG
  std::cout << "done" << std::endl;
#endif


#ifdef OLB_DEBUG
  std::cout << "reading data from communication buffer ... ";
#endif
    readFromCommBufferToLattice(commBuffer,lattice);
#ifdef OLB_DEBUG
  std::cout << "done"<< std::endl;
#endif
#ifdef OLB_DEBUG
  std::cout << "executing streaming and postproc steps ... ";
#endif
    lattice.stream();
    lattice.postProcess();
    lattice.getStatistics().incrementTime();
#ifdef OLB_DEBUG
  std::cout << "done"<< std::endl;
#endif
}

#ifdef ENABLE_CUDA
template<typename Lattice,typename CommBuffer>  
void initalizeCommDataMultilatticeGPU(Lattice& lattice, CommBuffer& commBuffer)
{
  // for(unsigned i=0;i<noLattices;++i)
  // {
    calculateCommunicationIndicesGPU(commBuffer,lattice); 
  // }
}

template<typename FluidDynamics, typename Lattice, typename CommBuffer,typename Callable>
void collideAndStreamMultilatticeGPU (Lattice& lattice, CommBuffer& commBuffer, Callable& communicator)
{

  lattice.template fieldCollisionGPU<FluidDynamics>();
  lattice.boundaryCollisionGPU();
  cudaDeviceSynchronize();

  writeToCommBufferFromLatticeGPU(commBuffer,lattice);
   
  cudaDeviceSynchronize();
  MPI_Barrier (MPI_COMM_WORLD);
 
#ifdef OLB_DEBUG 
  cudaDeviceSynchronize();
  HANDLE_ERROR(cudaPeekAtLastError());
#endif
  // communicate
  communicator();
 
  readFromCommBufferToLatticeGPU(commBuffer,lattice);
  cudaDeviceSynchronize();
  lattice.streamGPU();
  cudaDeviceSynchronize();
#ifdef OLB_DEBUG 
  HANDLE_ERROR(cudaPeekAtLastError());
#endif

  lattice.postProcessGPU();
  cudaDeviceSynchronize();
#ifdef OLB_DEBUG 
  HANDLE_ERROR(cudaPeekAtLastError());
#endif

}

#endif

} // end of namespace
#endif
