#ifndef CUDA_IPC_H
#define CUDA_IPC_H

#ifdef ENABLE_CUDA

#include <mpi.h>
#include <mpi-ext.h>
#include <driver_types.h>
#include <cuda_runtime_api.h>
#include <iostream>
#include "core/cudaErrorHandler.h"
#include "mpiCommunication.h"


#define ENV_LOCAL_RANK "OMPI_COMM_WORLD_LOCAL_RANK"

int initIPC (/*unsigned noRanksRequiredForCase*/)
{
    // char * localRankStr = NULL;
    // int rank = 0;

    // // We extract the local rank initialization using an environment variable
    // if ((localRankStr = getenv(ENV_LOCAL_RANK)) != NULL) {
        // rank = atoi(localRankStr);
        // }
    
    // std::cout << std::endl << "=========" << std::endl << "Pre-init rank: " << rank << std::endl;
    // int device = rank!=1 ? rank%4 : rank+1;
    // std::cout << "setting GPU: " << device << " on rank " << rank << std::endl;
    


     // Initialize the MPI environment
     MPI_Init(NULL, NULL);
     // Find out rank, size
#ifdef OLB_DEBUG
     printf("Compile time check:\n");
#if defined(MPIX_CUDA_AWARE_SUPPORT) && MPIX_CUDA_AWARE_SUPPORT
     printf("This MPI library has CUDA-aware support.\n", MPIX_CUDA_AWARE_SUPPORT);
#elif defined(MPIX_CUDA_AWARE_SUPPORT) && !MPIX_CUDA_AWARE_SUPPORT
     printf("This MPI library does not have CUDA-aware support.\n");
#else
     printf("This MPI library cannot determine if there is CUDA-aware support.\n");
#endif /* MPIX_CUDA_AWARE_SUPPORT */

     printf("Run time check:\n");
#if defined(MPIX_CUDA_AWARE_SUPPORT)
     if (1 == MPIX_Query_cuda_support()) {
        printf("This MPI library has CUDA-aware support.\n");
     }
     else {
        printf("This MPI library does not have CUDA-aware support.\n");
     }
#else /* !defined(MPIX_CUDA_AWARE_SUPPORT) */
     printf("This MPI library cannot determine if there is CUDA-aware support.\n");
#endif /* MPIX_CUDA_AWARE_SUPPORT */
#endif

     int world_rank;
     MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
     int world_size;
     MPI_Comm_size(MPI_COMM_WORLD, &world_size);

#ifdef OLB_DEBUG
     std::cout << "World Rank: " << world_rank << std::endl;
     std::cout << "World Size: " << world_size << std::endl;
#endif

     // if (world_size != noRanksRequiredForCase ) {
         // fprintf(stderr, "noRanks not matching\n");
         // MPI_Abort(MPI_COMM_WORLD, 1);
     // }
#ifdef OLB_DEBUG
    std::cout << "Grabbing one GPU per process on process " << world_rank << " GPU no " << world_rank << std::endl;
#endif
    int devCount;
    HANDLE_ERROR(cudaGetDeviceCount(&devCount));
#ifdef OLB_DEBUG
    std::cout << "No available GPUs is " << devCount << std::endl;
#endif
    HANDLE_ERROR(cudaSetDevice(world_rank));
#ifdef OLB_DEBUG
    std::cout << "Grabbed" << std::endl;
#endif

     return world_rank;
}

void finalizeIPC()
{
    cudaDeviceSynchronize();
    MPI_Finalize();
}

#endif

#endif
