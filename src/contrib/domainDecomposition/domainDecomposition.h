#ifndef DOMAIN_DECOMPOSITION_H
#define DOMAIN_DECOMPOSITION_H

#include "contrib/MemorySpace/memory_spaces.h"
#include <iostream>
#include "contrib/domainDecomposition/subDomainInformation.h"

namespace olb {

inline int modulo(int a, int b) {
  if(b < 0) return modulo(-a, -b);
  const int result = a % b;
  return result >= 0 ? result : result + b;
}

template<typename T,class Lattice>
struct DomainInformation {
  
  DomainInformation() = delete;

  explicit DomainInformation(unsigned noSubDomainsIn, unsigned localSubDomainIn,SubDomainInformation<T,Lattice> refSubDomainIn, 
      unsigned (*subDomFktIn) (DomainInformation<T,Lattice>&,size_t,size_t,size_t),
      size_t (*cellIdxFktIn) (DomainInformation<T,Lattice>&,size_t,size_t,size_t),
      Index3D (*localIdxFktIn) (DomainInformation<T,Lattice>&,size_t,size_t,size_t)):
    localSubDomain{localSubDomainIn},noSubDomains{noSubDomainsIn},refSubDomain{refSubDomainIn},subDomFkt{subDomFktIn},cellIdxFkt{cellIdxFktIn},localIdxFkt{localIdxFktIn} 
  {
    subDomains = new SubDomainInformation<T,Lattice>[noSubDomainsIn];
  }

  DomainInformation(DomainInformation const & src)
  {
    refSubDomain = src.refSubDomain;
    localSubDomain = src.localSubDomain;
    noSubDomains = src.noSubDomains;

    subDomFkt = src.subDomFkt;
    cellIdxFkt = src.cellIdxFkt;
    localIdxFkt = src.localIdxFkt;

    subDomains = new SubDomainInformation<T,Lattice>[noSubDomains];

    for (unsigned i=0;i<noSubDomains;++i)
        subDomains[i] = src.subDomains[i];
  
  }
  DomainInformation& operator = (DomainInformation src)
  {
    std::swap(*this,src);
    return *this;
  }

  ~DomainInformation()
  {
    delete[] subDomains;
  }

  SubDomainInformation<T,Lattice> getLocalInfo() const
  {
    return subDomains[localSubDomain];
  }

  SubDomainInformation<T,Lattice> getRefInfo() const
  {
    return refSubDomain;
  }


  unsigned getSubDomainNo (size_t iX,size_t iY,size_t iZ)
  {
    return subDomFkt(*this,iX,iY,iZ);
  }

  size_t getCellIdx (size_t iX,size_t iY,size_t iZ)
  {
    return cellIdxFkt(*this,iX,iY,iZ);
  }

  Index3D getLocalIdx (size_t iX,size_t iY,size_t iZ)
  {
    return localIdxFkt(*this,iX,iY,iZ);
  }

  SubDomainInformation<T,Lattice>* subDomains;
  SubDomainInformation<T,Lattice>  refSubDomain;
  unsigned localSubDomain = 0;
  unsigned noSubDomains = 0;

  unsigned (*subDomFkt) (DomainInformation<T,Lattice>&,size_t,size_t,size_t);
  size_t (*cellIdxFkt) (DomainInformation<T,Lattice>&,size_t,size_t,size_t);
  Index3D (*localIdxFkt) (DomainInformation<T,Lattice>&,size_t,size_t,size_t);

};

template<typename T,class Lattice>
std::ostream& operator<< (std::ostream& stream, const DomainInformation<T,Lattice>& domainInfo)
{
  stream << "DomainInformation: \n";
  stream << "localSubdomain( "<< domainInfo.localSubDomain <<" ) \n";
  stream << "noSubdomains( "<< domainInfo.noSubDomains <<" ) \n";
  stream << "refSubDomain: \n";
  stream << domainInfo.refSubDomain << "\n";
  for (int i = 0; i < domainInfo.noSubDomains; i++ ) {
    stream << "DomainInformation SubDomain " << i << ": \n";
    stream << domainInfo.subDomains[i];
  } 

  stream << std::endl;
  return stream;
}

template<typename T,class Lattice>
const SubDomainInformation<T,Lattice> decomposeDomainAlongLongestCoord (size_t Nx, size_t Ny, size_t Nz, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{
  size_t maxDim = std::max(Nx,std::max(Ny,Nz));
  // unsigned index = (Nz>=Nx) ? 2 : ((Ny>=Nx) ? 1 : 0);
  //unsigned index = (Nx>=Nz) ? 0 : ((Ny>=Nz) ? 1 : 2);
  unsigned index = (Nx == maxDim) ? 0 : ((Ny == maxDim) ? 1 : 2);

#ifdef OLB_DEBUG
  std::cout << "Index on which to decompose: " << index << std::endl;

  std::cout << "Decomposition index is " << index << " (" << Nx << "," << Ny << "," << Nz << ") with max dim: " << maxDim << std::endl;
#endif

  assert("The longest dim mod noSubDomains does not equal 0 which is required" && maxDim%noSubDomains == 0);

  SubDomainInformation<T,Lattice> tmp {};
  tmp.globalIndexStart[0] = 0; 
  tmp.globalIndexStart[1] = 0; 
  tmp.globalIndexStart[2] = 0; 

  tmp.globalIndexEnd[0] = Nx; 
  tmp.globalIndexEnd[1] = Ny; 
  tmp.globalIndexEnd[2] = Nz; 

  tmp.ghostLayer[0] = ghostLayer[0];
  tmp.ghostLayer[1] = ghostLayer[1];
  tmp.ghostLayer[2] = ghostLayer[2];

  tmp.globalIndexStart[index] = localSubDomain * maxDim/noSubDomains;
  tmp.globalIndexEnd[index] = (localSubDomain+1) * maxDim/noSubDomains;

  tmp.localSubDomain = localSubDomain;
  tmp.noSubDomains = noSubDomains;

  if (noSubDomains > 1)
  { 
    int direction[3] = {0,0,0};
    direction[index] = 1;
    unsigned popIndex = getPopFromC<Lattice>(direction);

    if (localSubDomain > 0 and localSubDomain<noSubDomains-1)
    {
      tmp.hasNeighbour[popIndex] = true;
      tmp.idNeighbour[popIndex] = localSubDomain + 1;
      tmp.hasNeighbourIn[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbourIn[Lattice::opposite(popIndex)] = localSubDomain + 1;

      tmp.hasNeighbour[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbour[Lattice::opposite(popIndex)] = localSubDomain - 1;
      tmp.hasNeighbourIn[(popIndex)] = true;
      tmp.idNeighbourIn[(popIndex)] = localSubDomain - 1;
    }
    else if (localSubDomain == 0 )
    {
      tmp.hasNeighbour[popIndex] = true;
      tmp.idNeighbour[popIndex] = localSubDomain + 1;

      tmp.hasNeighbourIn[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbourIn[Lattice::opposite(popIndex)] = localSubDomain + 1;
    }
    else if (localSubDomain == noSubDomains-1)
    {
      tmp.hasNeighbour[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbour[Lattice::opposite(popIndex)] = localSubDomain - 1;

      tmp.hasNeighbourIn[(popIndex)] = true;
      tmp.idNeighbourIn[(popIndex)] = localSubDomain - 1;
    }
    else
      assert("subDomain given is outside the range of  [0,noSubDomains]" && false);
  }
  return tmp;
}

template<typename T,class Lattice>
const SubDomainInformation<T,Lattice> decomposeDomainApproximatelyAlongAxis (size_t Nx, size_t Ny, size_t Nz, unsigned axis, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{

#ifdef OLB_DEBUG
  std::cout << "Axis on which to decompose: " << axis << std::endl;
#endif
  size_t maxDim = axis == 0 ? Nx : (axis == 1 ? Ny : Nz);

  size_t remainder = maxDim % noSubDomains;
  size_t baseSize = (maxDim - remainder)/noSubDomains;

  SubDomainInformation<T,Lattice> tmp {};
  tmp.globalIndexStart[0] = 0; 
  tmp.globalIndexStart[1] = 0; 
  tmp.globalIndexStart[2] = 0; 

  tmp.globalIndexEnd[0] = Nx; 
  tmp.globalIndexEnd[1] = Ny; 
  tmp.globalIndexEnd[2] = Nz; 

  tmp.ghostLayer[0] = ghostLayer[0];
  tmp.ghostLayer[1] = ghostLayer[1];
  tmp.ghostLayer[2] = ghostLayer[2];

  tmp.globalIndexStart[axis] = localSubDomain * baseSize;
  tmp.globalIndexEnd[axis] = (localSubDomain+1) * baseSize;

  if (localSubDomain < remainder) {
    tmp.globalIndexStart[axis] += localSubDomain;
    tmp.globalIndexEnd[axis] += localSubDomain + 1;
  }
  else {
    tmp.globalIndexStart[axis] += remainder;
    tmp.globalIndexEnd[axis] += remainder;
  }

  tmp.localSubDomain = localSubDomain;
  tmp.noSubDomains = noSubDomains;

  if (noSubDomains > 1)
  { 
    int direction[3] = {0,0,0};
    direction[axis] = 1;
    unsigned popIndex = getPopFromC<Lattice>(direction);

    if (localSubDomain > 0 and localSubDomain<noSubDomains-1)
    {
      tmp.hasNeighbour[popIndex] = true;
      tmp.idNeighbour[popIndex] = localSubDomain + 1;
      tmp.hasNeighbourIn[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbourIn[Lattice::opposite(popIndex)] = localSubDomain + 1;

      tmp.hasNeighbour[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbour[Lattice::opposite(popIndex)] = localSubDomain - 1;
      tmp.hasNeighbourIn[(popIndex)] = true;
      tmp.idNeighbourIn[(popIndex)] = localSubDomain - 1;
    }
    else if (localSubDomain == 0 )
    {
      tmp.hasNeighbour[popIndex] = true;
      tmp.idNeighbour[popIndex] = localSubDomain + 1;

      tmp.hasNeighbourIn[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbourIn[Lattice::opposite(popIndex)] = localSubDomain + 1;
    }
    else if (localSubDomain == noSubDomains-1)
    {
      tmp.hasNeighbour[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbour[Lattice::opposite(popIndex)] = localSubDomain - 1;

      tmp.hasNeighbourIn[(popIndex)] = true;
      tmp.idNeighbourIn[(popIndex)] = localSubDomain - 1;
    }
    else
      assert("subDomain given is outside the range of  [0,noSubDomains]" && false);
  }
  return tmp;
}

template<typename T,class Lattice>
const SubDomainInformation<T,Lattice> decomposeDomainAtIndices (size_t Nx, size_t Ny, size_t Nz, size_t axis, std::vector<size_t> splitIndices, unsigned localSubDomain, unsigned ghostLayer[3])
{
  
  size_t noSubDomains = splitIndices.size()+1;

  size_t refDomainSize[3] = {Nx, Ny, Nz};

  

  SubDomainInformation<T,Lattice> tmp {};
  tmp.globalIndexStart[0] = 0; 
  tmp.globalIndexStart[1] = 0; 
  tmp.globalIndexStart[2] = 0; 

  tmp.globalIndexEnd[0] = Nx; 
  tmp.globalIndexEnd[1] = Ny; 
  tmp.globalIndexEnd[2] = Nz; 

  tmp.ghostLayer[0] = ghostLayer[0];
  tmp.ghostLayer[1] = ghostLayer[1];
  tmp.ghostLayer[2] = ghostLayer[2];

  splitIndices.push_back(tmp.globalIndexEnd[axis]);
  splitIndices.insert(splitIndices.begin(), 0);

  for (int index = 0; index < splitIndices.size(); index++) {
    if (splitIndices.at(index) > tmp.globalIndexEnd[axis])
      splitIndices[index] = tmp.globalIndexEnd[axis];
  }

  tmp.globalIndexStart[axis] = splitIndices.at(localSubDomain);
  tmp.globalIndexEnd[axis] = splitIndices.at(localSubDomain+1);

  tmp.localSubDomain = localSubDomain;
  tmp.noSubDomains = noSubDomains;

  if (tmp.globalIndexEnd[axis] == 0 || tmp.globalIndexStart[axis] >= tmp.globalIndexEnd[axis]) {
    tmp.localHasGrid = false;

    tmp.ghostLayer[0] = 0;
    tmp.ghostLayer[1] = 0;
    tmp.ghostLayer[2] = 0;

    tmp.globalIndexStart[0] = 0;
    tmp.globalIndexStart[1] = 0;
    tmp.globalIndexStart[2] = 0;
    
    tmp.globalIndexEnd[0] = 1;
    tmp.globalIndexEnd[1] = 1;
    tmp.globalIndexEnd[2] = 1;    
  }

  if (noSubDomains > 1 && tmp.localHasGrid)
  { 
    int direction[3] = {0,0,0};
    direction[axis] = 1;
    unsigned popIndex = getPopFromC<Lattice>(direction);

    if (tmp.globalIndexStart[axis] > 0 && tmp.globalIndexEnd[axis] < refDomainSize[axis])
    {
      tmp.hasNeighbour[popIndex] = true;
      tmp.idNeighbour[popIndex] = localSubDomain + 1;
      tmp.hasNeighbourIn[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbourIn[Lattice::opposite(popIndex)] = localSubDomain + 1;

      tmp.hasNeighbour[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbour[Lattice::opposite(popIndex)] = localSubDomain - 1;
      tmp.hasNeighbourIn[(popIndex)] = true;
      tmp.idNeighbourIn[(popIndex)] = localSubDomain - 1;
    }
    else if (tmp.globalIndexStart[axis] == 0 )
    {
      if (tmp.globalIndexEnd[axis] < refDomainSize[axis]) {
        tmp.hasNeighbour[popIndex] = true;
        tmp.idNeighbour[popIndex] = localSubDomain + 1;

        tmp.hasNeighbourIn[Lattice::opposite(popIndex)] = true;
        tmp.idNeighbourIn[Lattice::opposite(popIndex)] = localSubDomain + 1;
      }
    }
    else if (tmp.globalIndexEnd[axis] == refDomainSize[axis])
    {
      tmp.hasNeighbour[Lattice::opposite(popIndex)] = true;
      tmp.idNeighbour[Lattice::opposite(popIndex)] = localSubDomain - 1;

      tmp.hasNeighbourIn[(popIndex)] = true;
      tmp.idNeighbourIn[(popIndex)] = localSubDomain - 1;
    }
    else
      assert("subDomain given is outside the range of  [0,noSubDomains]" && false);
  }
  return tmp;
}

template<typename T,typename Lattice>
unsigned getSubDIdxAlongX (DomainInformation<T,Lattice>& domainInfo,size_t iX,size_t iY, size_t iZ)
{
  // asuming even distribution of cells to domains (eg. noCellsX%noDomains = 0)
  return iX/(domainInfo.getLocalInfo().coreGridSize()[0]);
}

template<typename T,typename Lattice>
unsigned generalizedGetSubD(DomainInformation<T,Lattice>& domainInfo,size_t iX,size_t iY, size_t iZ)
{
  for (unsigned subDomainID = 0; subDomainID < domainInfo.noSubDomains; subDomainID++) {
    Index3D localIndex;
    if (domainInfo.subDomains[subDomainID].isLocal(iX,iY,iZ,localIndex)) {
      return subDomainID;
    }
  }
  throw std::runtime_error("generalizedGetSubD broken");
  return 0; //shouldn't ever get here!
}

template<typename T,typename Lattice>
size_t getCellIdxAlongX (DomainInformation<T,Lattice>& domInfo,size_t iX,size_t iY, size_t iZ)
{
  // asuming even distribution of cells to domains (eg. noCellsX%noDomains = 0)
  return util::getCellIndex3D(modulo(iX+domInfo.getLocalInfo().ghostLayer[0],(domInfo.getLocalInfo().localGridSize()[0])),iY+domInfo.getLocalInfo().ghostLayer[1],iZ+domInfo.getLocalInfo().ghostLayer[2],domInfo.getLocalInfo().localGridSize()[1],domInfo.getLocalInfo().localGridSize()[2]);
}

template<typename T,typename Lattice>
size_t generalizedGetCellIdx(DomainInformation<T,Lattice>& domainInfo,size_t iX,size_t iY, size_t iZ)
{
  for (unsigned subDomainID = 0; subDomainID < domainInfo.noSubDomains; subDomainID++) {
    Index3D localIndex;
    if (domainInfo.subDomains[subDomainID].isLocal(iX,iY,iZ,localIndex)) {
      if (Lattice::d == 2)
        return util::getCellIndex2D(localIndex[0], localIndex[1], domainInfo.subDomains[subDomainID].localGridSize()[1]);
      else
        return util::getCellIndex3D(localIndex[0], localIndex[1], localIndex[2], domainInfo.subDomains[subDomainID].localGridSize()[1], domainInfo.subDomains[subDomainID].localGridSize()[2]);
    }
  }
  throw std::runtime_error("generalizedGetCellIdx broken");
  return 0; //shouldn't ever get here!
}

template<typename T,typename Lattice>
Index3D getLocalIdxAlongX (DomainInformation<T,Lattice>& domInfo,size_t iX,size_t iY, size_t iZ)
{
  // asuming even distribution of cells to domains (eg. noCellsX%noDomains = 0)
  return Index3D{iX-domInfo.getLocalInfo().globalIndexStart[0]+domInfo.getLocalInfo().ghostLayer[0],iY+domInfo.getLocalInfo().ghostLayer[1],iZ+domInfo.getLocalInfo().ghostLayer[2],domInfo.getLocalInfo().localSubDomain};
}

template<typename T,typename Lattice>
Index3D generalizedGetLocalIdx(DomainInformation<T,Lattice>& domainInfo,size_t iX,size_t iY, size_t iZ)
{
  Index3D localIndex;
  for (unsigned subDomainID = 0; subDomainID < domainInfo.noSubDomains; subDomainID++) {
    if (domainInfo.subDomains[subDomainID].isLocal(iX,iY,iZ,localIndex)) {
      return localIndex;
    }
  }
  throw std::runtime_error("generalizedGetLocalIdx broken");
  return localIndex; //shouldn't ever get here!
}

template<typename T,typename Lattice>
unsigned getSubDIdxAlongZ (DomainInformation<T,Lattice>& domainInfo,size_t iX,size_t iY, size_t iZ)
{
  // asuming even distribution of cells to domains (eg. noCellsZ%noDomains = 0)
  return iZ/(domainInfo.getLocalInfo().coreGridSize()[2]);
}

template<typename T,typename Lattice>
size_t getCellIdxAlongZ (DomainInformation<T,Lattice>& domInfo,size_t iX,size_t iY, size_t iZ)
{
  // asuming even distribution of cells to domains (eg. noCellsZ%noDomains = 0)
  return util::getCellIndex3D(iX+domInfo.getLocalInfo().ghostLayer[0],iY+domInfo.getLocalInfo().ghostLayer[1],modulo(iZ+domInfo.getLocalInfo().ghostLayer[2],(domInfo.getLocalInfo().localGridSize()[2])),domInfo.getLocalInfo().localGridSize()[1],domInfo.getLocalInfo().localGridSize()[2]);
}

template<typename T,typename Lattice>
Index3D getLocalIdxAlongZ (DomainInformation<T,Lattice>& domInfo,size_t iX,size_t iY, size_t iZ)
{
  // asuming even distribution of cells to domains (eg. noCellsZ%noDomains = 0)
  return Index3D{iX+domInfo.getLocalInfo().ghostLayer[0],iY+domInfo.getLocalInfo().ghostLayer[1],iZ-domInfo.getLocalInfo().globalIndexStart[2]+domInfo.getLocalInfo().ghostLayer[2],domInfo.getLocalInfo().localSubDomain};
}

template<typename T,class Lattice>
const SubDomainInformation<T,Lattice> decomposeDomainAlongLongestCoord (const SubDomainInformation<T,Lattice>& refSubDomain, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{
  return decomposeDomainAlongLongestCoord<T,Lattice> (static_cast<size_t>(refSubDomain.globalIndexEnd[0]),static_cast<size_t>(refSubDomain.globalIndexEnd[1]),static_cast<size_t>(refSubDomain.globalIndexEnd[2]),localSubDomain,noSubDomains,ghostLayer);
}

template<typename T,class Lattice>
const SubDomainInformation<T,Lattice> decomposeDomainAtIndices (const SubDomainInformation<T,Lattice>& refSubDomain, size_t axis, std::vector<size_t> splitIndices, unsigned localSubDomain, unsigned ghostLayer[3])
{
  return decomposeDomainAtIndices<T,Lattice> (static_cast<size_t>(refSubDomain.globalIndexEnd[0]),static_cast<size_t>(refSubDomain.globalIndexEnd[1]),static_cast<size_t>(refSubDomain.globalIndexEnd[2]), axis, splitIndices, localSubDomain, ghostLayer);
}

template<typename T,class Lattice>
const SubDomainInformation<T,Lattice> decomposeDomainApproximatelyAlongAxis (const SubDomainInformation<T,Lattice>& refSubDomain, size_t axis, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{
  return decomposeDomainApproximatelyAlongAxis<T,Lattice> (static_cast<size_t>(refSubDomain.globalIndexEnd[0]),static_cast<size_t>(refSubDomain.globalIndexEnd[1]),static_cast<size_t>(refSubDomain.globalIndexEnd[2]), axis, localSubDomain, noSubDomains, ghostLayer);
}


template<typename T,class Lattice>
const DomainInformation<T,Lattice> decomposeDomainAlongX (const SubDomainInformation<T,Lattice>& refSubDomain, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{
  DomainInformation<T,Lattice> tmp{noSubDomains,localSubDomain,refSubDomain,getSubDIdxAlongX,getCellIdxAlongX,getLocalIdxAlongX};
  for (unsigned dom=0;dom<noSubDomains;++dom)
  {
    tmp.subDomains[dom] = decomposeDomainAlongLongestCoord(refSubDomain,dom,noSubDomains,ghostLayer);
  }
  return tmp;
}

template<typename T,class Lattice>
const DomainInformation<T,Lattice> domainInfoDecomposeDomainAtIndices (const SubDomainInformation<T,Lattice>& refSubDomain, unsigned axis, std::vector<size_t> splitIndices, unsigned localSubDomain, unsigned ghostLayer[3])
{
  DomainInformation<T,Lattice> tmp{static_cast<unsigned int>(splitIndices.size()+1),localSubDomain,refSubDomain,generalizedGetSubD,generalizedGetCellIdx,generalizedGetLocalIdx};
  for (unsigned dom=0;dom< (splitIndices.size()+1) ;++dom)
  {
    tmp.subDomains[dom] = decomposeDomainAtIndices(refSubDomain, axis, splitIndices, dom, ghostLayer);
  }
  return tmp;
}

template<typename T,class Lattice>
const DomainInformation<T,Lattice> domainInfoDecomposeDomainApproximatelyAlongAxis (const SubDomainInformation<T,Lattice>& refSubDomain, unsigned axis, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{
  DomainInformation<T,Lattice> tmp{noSubDomains,localSubDomain,refSubDomain,generalizedGetSubD,generalizedGetCellIdx,generalizedGetLocalIdx};
  for (unsigned dom=0;dom<noSubDomains;++dom)
  {
    tmp.subDomains[dom] = decomposeDomainApproximatelyAlongAxis(refSubDomain,axis,dom,noSubDomains,ghostLayer);
  }
  return tmp;
}

template<typename T,class Lattice>
const DomainInformation<T,Lattice> decomposeDomainAlongZ (const SubDomainInformation<T,Lattice>& refSubDomain, unsigned localSubDomain, unsigned noSubDomains, unsigned ghostLayer[3])
{
  DomainInformation<T,Lattice> tmp{noSubDomains,localSubDomain,refSubDomain,getSubDIdxAlongZ,getCellIdxAlongZ,getLocalIdxAlongZ};
  for (unsigned dom=0;dom<noSubDomains;++dom)
  {
    tmp.subDomains[dom] = decomposeDomainAlongLongestCoord(refSubDomain,0,dom,noSubDomains,ghostLayer);
  }
  return tmp;
}

template<typename T>
class PopArray{
  
  public:
  T popData;
  unsigned popIndex=0;
#ifdef ENABLE_CUDA
  cudaStream_t stream;
  // PopArray() {cudaStreamCreateWithFlags(&stream,cudaStreamNonBlocking);}
  PopArray() {HANDLE_ERROR(cudaStreamCreate(&stream));}

  // ~PopArray() {cudaStreamDestroy(stream);}
#endif
  // ~PopArray () {std::cout << "deleting Pop array with index" << popIndex << std::endl;}
};

template<typename T>
struct IndexArray{

  T indexData;
  unsigned popIndex=0;
  size_t noIndices=0;

};

template<typename T>
std::ostream& operator<< (std::ostream& stream, const PopArray<T>& pop)
{
  stream << "PopArray for popIndex " << pop.popIndex << " with properties " << pop.popData; 
  return stream;
}

template<typename T>
std::ostream& operator<< (std::ostream& stream, const IndexArray<T>& arr)
{
  stream << "IndexArray for popIndex " << arr.popIndex << " with properties " << arr.indexData; 
  return stream;
}

template<typename T, class Lattice,template<typename> class Memory>
struct CommunicationData {

  Index3D size;
  PopArray<Memory<T>> popArrays[Lattice::dataSize]; // buffer of populations, as it is not known at this time which and how many populations are required
  IndexArray<Memory<long long>> indexArrays[Lattice::dataSize];
  // ~CommunicationData () {std::cout << "deleting communicationData with size" << size << std::endl;}
};


template<typename T,class Lattice,template<typename> class Memory>
class CommunicationDataHandler {

  public:

  CommunicationDataHandler() = delete;
  CommunicationDataHandler(const DomainInformation<T,Lattice>& sub):domainInfo{sub} {}

  ~CommunicationDataHandler() = default;

  CommunicationData<T,Lattice,Memory>* getCommunicationDataOut()
  {
    return &buffersOut_[0];
  }

  const CommunicationData<T,Lattice,Memory>* getCommunicationDataOut() const
  {
    return &buffersOut_[0];
  }

  CommunicationData<T,Lattice,Memory>* getCommunicationDataIn()
  {
    return &buffersIn_[0];
  }

  const CommunicationData<T,Lattice,Memory>* getCommunicationDataIn() const
  {
    return &buffersIn_[0];
  }

  private:

    CommunicationData<T,Lattice,Memory> buffersOut_[Lattice::q]; // one buffer for each potential neighbour
    CommunicationData<T,Lattice,Memory> buffersIn_[Lattice::q]; // one buffer for each potential neighbour

  public:
    const DomainInformation<T,Lattice> domainInfo;

};

template<typename T,class Lattice,template<typename> class Memory>
std::ostream& operator<< (std::ostream& stream, const CommunicationDataHandler<T,Lattice,Memory>& commDataHandler)
{
  stream << "CommunicationDataHandlerInformation: \n";
  stream << "based on SubdomainInfo: \n";
  stream << "[ \n";
  stream << commDataHandler.domainInfo.getLocalInfo();
  stream << "] \n";

  for (unsigned i=0;i<Lattice::q;++i)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[i])
    {
      stream << "Neighbour direction: " << Lattice::c(i,0) << " " << Lattice::c(i,1) << " " << Lattice::c(i,2) << "\n";
      stream << "Size of OUT interface: " << commDataHandler.getCommunicationDataOut()[i].size;

    stream << "Interface data: \n";
    for (unsigned j=0;j<Lattice::q+1+Lattice::d;++j)
    {
      if (commDataHandler.getCommunicationDataOut()[i].popArrays[j].popData)
      {
        stream << "direction popData " << j << " " << commDataHandler.getCommunicationDataOut()[i].popArrays[j];
        stream << "direction indexData " << j << " " << commDataHandler.getCommunicationDataOut()[i].indexArrays[j];
      }
    } 
    }
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice::opposite(i)])
    {
      stream << "Neighbour direction: " << Lattice::c(i,0) << " " << Lattice::c(i,1) << " " << Lattice::c(i,2) << "\n";
      stream << "Size of IN interface: " << commDataHandler.getCommunicationDataIn()[i].size;

    stream << "Interface data: \n";
    for (unsigned j=0;j<Lattice::q+1+Lattice::d;++j)
    {
      if (commDataHandler.getCommunicationDataIn()[i].popArrays[j].popData)
      {
        stream << " direction popData " << j << " " << commDataHandler.getCommunicationDataIn()[i].popArrays[j];
        stream << " direction indexData " << j << " " << commDataHandler.getCommunicationDataIn()[i].popArrays[j];
      }
    }
    } 
  }
  return stream;
}

template<typename Lattice>
unsigned getPermutationNumber (int direction[3])
{
  unsigned zeroCount=0;
  for (unsigned i=0;i<Lattice::d;++i)
  {
    if (direction[i] == 0)
      ++zeroCount;
  }
  return pow(3,zeroCount);
}

template<typename T,class Lattice,template<typename> class Memory>
void allocateMemoryArrays (CommunicationData<T,Lattice,Memory>& data,unsigned startIdx,unsigned endIdx,unsigned size[3])
{
   for (unsigned j=startIdx;j<endIdx;++j)
   {
     data.popArrays[j].popIndex = j;  
     data.indexArrays[j].popIndex = j;  
     data.popArrays[j].popData = Memory<T>{size[0]*size[1]*size[2]};  
     data.indexArrays[j].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
    }
}

template<typename T,class Lattice,template<typename> class Memory>
void allocateCommunicationData (const SubDomainInformation<T,Lattice>& subDomainInfo,CommunicationData<T,Lattice,Memory>* data,unsigned iPop)
{
      unsigned abs_val=0;
      int direction[3] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      unsigned indices_neq_zero[3] = {std::numeric_limits<unsigned>::max(),std::numeric_limits<unsigned>::max(),std::numeric_limits<unsigned>::max()};
      unsigned indices_eq_zero[3] = {std::numeric_limits<unsigned>::max(),std::numeric_limits<unsigned>::max(),std::numeric_limits<unsigned>::max()};
      // unsigned count = 0; //appears to be unused
      unsigned count_neq = 0;
      unsigned count_eq = 0;
      for(unsigned dim=0;dim<Lattice::d;++dim)
      {
        abs_val += abs(Lattice::c(iPop,dim));
        direction[dim] =  Lattice::c(iPop,dim);
        if (Lattice::c(iPop,dim) != 0)
        {
          indices_neq_zero[count_neq] = dim;
          ++count_neq;
        }
        else
        {
          indices_eq_zero[count_eq] =  dim;
          ++count_eq;
        }
      }

      unsigned iPopStart;
      unsigned iPopEnd;
      bool directCommunication=false;

      if (subDomainInfo.ghostLayer[0] == 0 and subDomainInfo.ghostLayer[1] == 0 and subDomainInfo.ghostLayer[2] == 0) // no ghostLayers, thus direct communication
      {
        iPopStart=iPop;
        iPopEnd=iPop+1; 
        directCommunication=true;
        
      }
      else // ghostLayer communication -> full cells in the communication buffers
      {
        iPopStart = 0;
        iPopEnd = Lattice::dataSize;
        directCommunication=false;
      }

      unsigned size[3] = {0,0,0};

      switch(abs_val)
      {
        case Lattice::d: // ecke
          

          if (directCommunication)
          {  
            size[0] = 1;
            size[1] = 1;
            size[2] = 1;
          }
          else
          {
            size[0] = subDomainInfo.ghostLayer[0];
            size[1] = subDomainInfo.ghostLayer[1];
            size[2] = subDomainInfo.ghostLayer[2];
          }

          data[iPop].size.index[0] = size[0];
          data[iPop].size.index[1] = size[1];
          data[iPop].size.index[2] = size[2];


          allocateMemoryArrays(data[iPop],iPopStart,iPopEnd,size);
          // for (unsigned j=iPopStart;j<iPopEnd;++j)
          // {
            // data[iPop].popArrays[j].popIndex = j;  
            // data[iPop].indexArrays[j].popIndex = j;  
            // data[iPop].popArrays[j].popData = Memory<T>{size[0]*size[1]*size[2]};  
            // data[iPop].indexArrays[j].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
          // }
          // for (unsigned data=Lattice::q;data<Lattice::dataSize;++data)
          // {
            // data[iPop].popArrays[data].popIndex = data;
            // data[iPop].indexArrays[data].popIndex = data;
            // data[iPop].popArrays[data].popData = Memory<T>{size[0]*size[1]*size[2]};
            // data[iPop].indexArrays[data].indexData = Memory<long long>{size[0]*size[1]*size[2]};
          // }
          break;
/////////////////////////////////////////////////////////////////////////////////////////////////////
        case Lattice::d-1: // kante

          if (directCommunication)
          {
          for(unsigned i=1;i<Lattice::q;++i)
          {
          if (Lattice::d == 2) // 2D
          {
           if (Lattice::c(i,indices_neq_zero[0]) == direction[indices_neq_zero[0]])
           {
             if (Lattice::c(i,0) == direction[0] and Lattice::c(i,1) == direction[1]) // full face
             {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
              size[1] = 1; //
              size[2] = 1;
             }
             else 
             {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]-1; // diagonals over side
              size[1] = 1; //
              size[2] = 1;
             }

             // if(!directCommunication)
             // {
              // size[0] *= subDomainInfo.ghostLayer[0]; 
              // size[1] *= subDomainInfo.ghostLayer[1]; 
              // size[2] *= subDomainInfo.ghostLayer[2]; 
             // }

            data[iPop].size.index[indices_eq_zero[0]] = size[0]; //
            data[iPop].size.index[indices_neq_zero[0]] = size[1]; //
            data[iPop].size.index[2] = size[2]; //

            allocateMemoryArrays(data[iPop],i,i+1,size);


              // data[iPop].popArrays[i].popIndex = i;  
              // data[iPop].indexArrays[i].popIndex = i;  
              // data[iPop].popArrays[i].popData = Memory<T>{size[0]*size[1]*size[2]};  
              // data[iPop].indexArrays[i].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
           }
          
          }
          if (Lattice::d == 3) // 3d
          {
            if (Lattice::c(i,indices_neq_zero[0]) == direction[indices_neq_zero[0]] and Lattice::c(i,indices_neq_zero[1]) == direction[indices_neq_zero[1]])
            {
             if (Lattice::c(i,0) == direction[0] and Lattice::c(i,1) == direction[1] and Lattice::c(i,2) == direction[2]) // full face
             {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
              size[1] = 1;
              size[2] = 1;
             }
             else
             {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]-1; //
              size[1] = 1;
              size[2] = 1;
             }
            data[iPop].size.index[indices_eq_zero[0]] = size[0]; //
            data[iPop].size.index[indices_neq_zero[0]] = size[1]; //
            data[iPop].size.index[indices_neq_zero[1]] = size[2]; //

            allocateMemoryArrays(data[iPop],i,i+1,size);
            // data[iPop].popArrays[i].popIndex = i;  
            // data[iPop].indexArrays[i].popIndex = i;  
            // data[iPop].popArrays[i].popData = Memory<T>{size[0]*size[1]*size[2]};  
            // data[iPop].indexArrays[i].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
            }
          }
          }
          }
          else
          {
          if (Lattice::d == 2) // 2d
          {
           size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]];
           size[1] = subDomainInfo.ghostLayer[indices_neq_zero[0]];
           size[2] = 1;
          
           data[iPop].size.index[indices_eq_zero[0]] = size[0]; //
           data[iPop].size.index[indices_neq_zero[0]] = size[1]; //
           data[iPop].size.index[2] = size[2]; //

           allocateMemoryArrays(data[iPop],iPopStart,iPopEnd,size);

            // for (unsigned j=iPopStart;j<iPopEnd;++j)
            // {
              // data[iPop].popArrays[j].popIndex = j;  
              // data[iPop].indexArrays[j].popIndex = j;  
              // data[iPop].popArrays[j].popData = Memory<T>{size[0]*size[1]*size[2]};  
              // data[iPop].indexArrays[j].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
            // }

          
          }
          if (Lattice::d == 3) // 3d
          {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
              size[1] = subDomainInfo.ghostLayer[indices_neq_zero[0]];
              size[2] = subDomainInfo.ghostLayer[indices_neq_zero[1]];

            data[iPop].size.index[indices_eq_zero[0]] = size[0]; //
            data[iPop].size.index[indices_neq_zero[0]] = size[1]; //
            data[iPop].size.index[indices_neq_zero[1]] = size[2]; //

            allocateMemoryArrays(data[iPop],iPopStart,iPopEnd,size);
            // for (unsigned j=iPopStart;j<iPopEnd;++j)
            // {
            // data[iPop].popArrays[j].popIndex = j;  
            // data[iPop].indexArrays[j].popIndex = j;  
            // data[iPop].popArrays[j].popData = Memory<T>{size[0]*size[1]*size[2]};  
            // data[iPop].indexArrays[j].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
            // }
          }
          }

          break;

        case Lattice::d-2: // fläche // not available in 2d

          if (directCommunication)
          {
          for(unsigned i=1;i<Lattice::q;++i)
          {

           if (Lattice::c(i,indices_neq_zero[0]) == direction[indices_neq_zero[0]])
           {
             if (Lattice::c(i,0) == direction[0] and Lattice::c(i,1) == direction[1] and Lattice::c(i,2) == direction[2]) // full face
             {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
              size[1] = subDomainInfo.localGridSize().index[indices_eq_zero[1]];
              size[2] = 1;
             }
             else
             {

              unsigned abs_val_local = 0;
                for(unsigned dim=0;dim<Lattice::d;++dim)
                {
                  abs_val_local += abs(Lattice::c(i,dim));
                }

                // this is not optimal ... 
              if (abs_val_local == 2) //pop inside face
              {
                  size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
                  size[1] = subDomainInfo.localGridSize().index[indices_eq_zero[1]];
                  size[2] = 1;
                  
              } 
              if (abs_val_local == 3) // pop diagonal outside
              {
                  size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
                  size[1] = subDomainInfo.localGridSize().index[indices_eq_zero[1]];
                  size[2] = 1;
              
              }
            }
            data[iPop].size.index[indices_eq_zero[0]] = size[0]; //
            data[iPop].size.index[indices_eq_zero[1]] = size[1]; //
            data[iPop].size.index[indices_neq_zero[0]] = size[2]; //

            allocateMemoryArrays(data[iPop],i,i+1,size);
            // data[iPop].popArrays[i].popIndex = i;  
            // data[iPop].indexArrays[i].popIndex = i;  
            // data[iPop].popArrays[i].popData = Memory<T>{size[0]*size[1]*size[2]};  
            // data[iPop].indexArrays[i].indexData = Memory<long long>{size[0]*size[1]*size[2]};  

           }
     
          }
          }
          else
          {
              size[0] = subDomainInfo.localGridSize().index[indices_eq_zero[0]]; //
              size[1] = subDomainInfo.localGridSize().index[indices_eq_zero[1]];
              size[2] = subDomainInfo.ghostLayer[indices_neq_zero[0]];
          
            data[iPop].size.index[indices_eq_zero[0]] = size[0]; //
            data[iPop].size.index[indices_eq_zero[1]] = size[1]; //
            data[iPop].size.index[indices_neq_zero[0]] = size[2]; //

            allocateMemoryArrays(data[iPop],iPopStart,iPopEnd,size);
            // for (unsigned j=iPopStart;j<iPopEnd;++j)
            // {
            // data[iPop].popArrays[j].popIndex = j;  
            // data[iPop].indexArrays[j].popIndex = j;  
            // data[iPop].popArrays[j].popData = Memory<T>{size[0]*size[1]*size[2]};  
            // data[iPop].indexArrays[j].indexData = Memory<long long>{size[0]*size[1]*size[2]};  
            // }
          }

          break;
  
    }


}

template<template<typename> class Memory,typename T, class Lattice>
CommunicationDataHandler<T,Lattice,Memory> createCommunicationDataHandler (const DomainInformation<T,Lattice>& DomainInfo)
{
  CommunicationDataHandler<T,Lattice,Memory> tmp{DomainInfo};
// #ifdef OLD_DEBUG
  // std::cout << tmp<<std::endl;
// #endif


  for(unsigned iPop=0;iPop<Lattice::q;++iPop)
  {
    if (DomainInfo.getLocalInfo().hasNeighbour[iPop])
    {
#ifdef OLB_DEBUG
      std::cout << "allocating outgoing data for neighbour direction along iPop " << iPop << " and incoming data for neighbour direction along iPop " << Lattice::opposite(iPop)<<std::endl;
#endif
      allocateCommunicationData(DomainInfo.getLocalInfo(),tmp.getCommunicationDataOut(),iPop);
      allocateCommunicationData(DomainInfo.getLocalInfo(),tmp.getCommunicationDataIn(),Lattice::opposite(iPop));
    }
  }

// #ifdef OLB_DEBUG
// std::cout << "#########################################" << std::endl;
// std::cout << "#########################################" << std::endl;
// std::cout << "#########################################" << std::endl;
// std::cout << "#########################################" << std::endl;
// #endif 

  return tmp;

}

template<typename T, template<typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesRead (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
  
  for (unsigned population=0;population < Lattice<T>::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice<T>::opposite(population)])
    {
    
      int direction[3] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      for(unsigned dim=0;dim<Lattice<T>::d;++dim)
      {
        direction[dim] =  Lattice<T>::c(Lattice<T>::opposite(population),dim);
      }
   
     int iXS = std::numeric_limits<int>::max();
     int iYS = std::numeric_limits<int>::max();
     int iZS = std::numeric_limits<int>::max();
     int iXE = std::numeric_limits<int>::max();
     int iYE = std::numeric_limits<int>::max();
     int iZE = std::numeric_limits<int>::max();

      bool directCommunication=false;
      unsigned ghostLayer[] = {0,0,0};

      if (commDataHandler.domainInfo.getLocalInfo().ghostLayer[0] == 0 and commDataHandler.domainInfo.getLocalInfo().ghostLayer[1] == 0 and commDataHandler.domainInfo.getLocalInfo().ghostLayer[2] == 0) // no ghostLayers, thus direct communication
      {
        directCommunication = true;
      }
      else 
      {
        ghostLayer[0] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[0]-1;
        ghostLayer[1] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[1]-1;
        ghostLayer[2] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[2]-1;
      }

      auto localInfo = commDataHandler.domainInfo.getLocalInfo();

     switch (direction[0]) 
     {
      case 1:
        // iXS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1 + ghostLayer[0]; 
        // iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        iXS = localInfo.coreGridEnd()[0]; 
        iXE = iXS + ghostLayer[0]; 
        break;

      case 0:
        iXS = 0; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case -1:
        // iXS = 0;
        // iXE = ghostLayer[0];
        iXS = 0;
        iXE = ghostLayer[0];
        break;
     }
     switch (direction[1]) 
     {
      case 1:
        // iYS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1 - ghostLayer[1]; 
        // iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        iYS = localInfo.coreGridEnd()[1]; 
        iYE = iYS + ghostLayer[1]; 
        break;

      case 0:
        iYS = 0; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case -1:
        iYS = 0;
        iYE = ghostLayer[1];
        break;
     }
     switch (direction[2]) 
     {
      case 1:
        // iZS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1 - ghostLayer[2]; 
        // iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        iZS = localInfo.coreGridEnd()[2]; 
        iZE = iZS + ghostLayer[2]; 
        break;

      case 0:
        iZS = 0; 
        iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        break;

      case -1:
        iZS = 0;
        iZE = ghostLayer[2];
        break;
     }


      if (directCommunication)
      {
      for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS; int iZSS=iZS;
         int iXEE=iXE; int iYEE=iYE; int iZEE=iZE;
         int latticeOffset[3] = {direction[0] + Lattice<T>::c(iPop,0),direction[1] + Lattice<T>::c(iPop,1),direction[2] + Lattice<T>::c(iPop,2)};
         latticeOffset[0] <0 ? iXSS-=latticeOffset[0] : iXEE-=latticeOffset[0];
         latticeOffset[1] <0 ? iYSS-=latticeOffset[1] : iYEE-=latticeOffset[1];
         latticeOffset[2] <0 ? iZSS-=latticeOffset[2] : iZEE-=latticeOffset[2];

         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
            for (int iZ = iZSS; iZ<=iZEE;++iZ)
            {
// #ifdef OLB_DEBUG
              // std::cout << "writing to indexData at position " << iterator << std::endl;
// #endif
              commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX-Lattice<T>::c(population,0),iY-Lattice<T>::c(population,1),iZ-Lattice<T>::c(population,2));
              ++iterator;
            }
         commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices=iterator;
// #ifdef OLB_DEBUG
         // std::cout << "indexArray for neighbour " << population << " filled with " << iterator << " indices" << std::endl;
// #endif
       }
      }
      }
      else
      {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS; int iZSS=iZS;
         int iXEE=iXE; int iYEE=iYE; int iZEE=iZE;
         // std::cout << "indices " << iXSS <<" "<< iXEE <<" "<<iYSS << " " << iYEE << " " << iZSS<< " " << iZEE<<std::endl; 
         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
            for (int iZ = iZSS; iZ<=iZEE;++iZ)
            {
// #ifdef OLB_DEBUG
              // std::cout << "writing to indexData at position " << iterator << std::endl;
// #endif
              commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX,iY,iZ);
              // std::cout << "adding cell to dataIn " << iX << " " << iY << " " << iZ << " on subdomain " << commDataHandler.domainInfo.getLocalInfo().localSubDomain << " with neighbour direction " << population << " for iPop" << iPop<< std::endl;
              ++iterator;
            }
         commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices=iterator;
// #ifdef OLB_DEBUG
         // std::cout << "indexArray for neighbour " << population << " filled with " << iterator << " indices" << std::endl;
// #endif
       }
      }
     }
    }
  }
}

//same function but for 2D
template<typename T, template<typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesRead (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice2D<T,Lattice>& lattice)
{
  
  for (unsigned population=0;population < Lattice<T>::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice<T>::opposite(population)])
    {
    
      int direction[2] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      for(unsigned dim=0;dim<Lattice<T>::d;++dim)
      {
        direction[dim] =  Lattice<T>::c(Lattice<T>::opposite(population),dim);
      }
   
     int iXS = std::numeric_limits<int>::max();
     int iYS = std::numeric_limits<int>::max();
     int iXE = std::numeric_limits<int>::max();
     int iYE = std::numeric_limits<int>::max();

      bool directCommunication=false;
      unsigned ghostLayer[] = {0,0};

      if (commDataHandler.domainInfo.getLocalInfo().ghostLayer[0] == 0 and commDataHandler.domainInfo.getLocalInfo().ghostLayer[1] == 0) // no ghostLayers, thus direct communication
      {
        directCommunication = true;
      }
      else 
      {
        ghostLayer[0] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[0]-1;
        ghostLayer[1] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[1]-1;
      }

      auto localInfo = commDataHandler.domainInfo.getLocalInfo();

     switch (direction[0]) 
     {
      case 1:
        iXS = localInfo.coreGridEnd()[0]; 
        iXE = iXS + ghostLayer[0]; 
        break;

      case 0:
        iXS = 0; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case -1:
        iXS = 0;
        iXE = ghostLayer[0];
        break;
     }
     switch (direction[1]) 
     {
      case 1:
        iYS = localInfo.coreGridEnd()[1]; 
        iYE = iYS + ghostLayer[1]; 
        break;

      case 0:
        iYS = 0; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case -1:
        iYS = 0;
        iYE = ghostLayer[1];
        break;
     }

      if (directCommunication)
      {
      for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS;
         int iXEE=iXE; int iYEE=iYE;
         int latticeOffset[2] = {direction[0] + Lattice<T>::c(iPop,0), direction[1] + Lattice<T>::c(iPop,1)};
         latticeOffset[0] <0 ? iXSS-=latticeOffset[0] : iXEE-=latticeOffset[0];
         latticeOffset[1] <0 ? iYSS-=latticeOffset[1] : iYEE-=latticeOffset[1];

         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
          {
            commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX-Lattice<T>::c(population,0),iY-Lattice<T>::c(population,1));
            ++iterator;
          }
         commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices=iterator;
// #ifdef OLB_DEBUG
         // std::cout << "indexArray for neighbour " << population << " filled with " << iterator << " indices" << std::endl;
// #endif
       }
      }
      }
      else
      {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS;
         int iXEE=iXE; int iYEE=iYE;
         // std::cout << "indices " << iXSS <<" "<< iXEE <<" "<<iYSS << " " << iYEE << " " << iZSS<< " " << iZEE<<std::endl; 
         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
          {

            commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX,iY);
            ++iterator;
          }
         commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices=iterator;

       }
      }
     }
    }
  }
}

template<typename T,template <typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesWrite (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
 for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
    
      int direction[3] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      for(unsigned dim=0;dim<Lattice<T>::d;++dim)
      {
        direction[dim] =  Lattice<T>::c(neighbour,dim);
      }
   
     int iXS = std::numeric_limits<int>::max();
     int iYS = std::numeric_limits<int>::max();
     int iZS = std::numeric_limits<int>::max();
     int iXE = std::numeric_limits<int>::max();
     int iYE = std::numeric_limits<int>::max();
     int iZE = std::numeric_limits<int>::max();

      bool directCommunication=false;
      unsigned ghostLayer[] = {0,0,0};

      if (commDataHandler.domainInfo.getLocalInfo().ghostLayer[0] == 0 and commDataHandler.domainInfo.getLocalInfo().ghostLayer[1] == 0 and commDataHandler.domainInfo.getLocalInfo().ghostLayer[2] == 0) // no ghostLayers, thus direct communication
      {
        directCommunication = true;
      }
      else 
      {
        ghostLayer[0] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[0]-1;
        ghostLayer[1] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[1]-1;
        ghostLayer[2] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[2]-1;
      }

     auto localInfo = commDataHandler.domainInfo.getLocalInfo();
     switch (direction[0]) 
     {
      case 1:
        iXS = localInfo.coreGridEnd()[0]-1-ghostLayer[0]; 
        iXE = iXS + ghostLayer[0]; 
        break;

      case 0:
        iXS = 0; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case -1:
        iXS = localInfo.coreGridStart()[0];//0;
        iXE = iXS + ghostLayer[0];
        break;
     }
     switch (direction[1]) 
     {
      case 1:
        iYS = localInfo.coreGridEnd()[1]-1-ghostLayer[1]; 
        iYE = iYS + ghostLayer[1]; 
        break;

      case 0:
        iYS = 0; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case -1:
        iYS = localInfo.coreGridStart()[1];
        iYE = iYS + ghostLayer[1];
        break;
     }
     switch (direction[2]) 
     {
      case 1:
        iZS = localInfo.coreGridEnd()[2]-1-ghostLayer[2]; 
        iZE = iZS + ghostLayer[2]; 
        break;

      case 0:
        iZS = 0; 
        iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        break;

      case -1:
        iZS = localInfo.coreGridStart()[2];
        iZE = iZS + ghostLayer[2];
        break;
     }

      if (directCommunication)
      {
      for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS; int iZSS=iZS;
         int iXEE=iXE; int iYEE=iYE; int iZEE=iZE;
         int latticeOffset[3] = {-direction[0] + Lattice<T>::c(iPop,0),-direction[1] + Lattice<T>::c(iPop,1),-direction[2] + Lattice<T>::c(iPop,2)};
         latticeOffset[0] <0 ? iXSS-=latticeOffset[0] : iXEE-=latticeOffset[0];
         latticeOffset[1] <0 ? iYSS-=latticeOffset[1] : iYEE-=latticeOffset[1];
         latticeOffset[2] <0 ? iZSS-=latticeOffset[2] : iZEE-=latticeOffset[2];

         unsigned iterator = 0;
         for (unsigned iX = iXSS; iX<=iXEE;++iX)
          for (unsigned iY = iYSS; iY<=iYEE;++iY)
            for (unsigned iZ = iZSS; iZ<=iZEE;++iZ)
            {
              commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX,iY,iZ);
               ++iterator;
            }
         commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices=iterator;
       }
      }
      }
      else
      {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS; int iZSS=iZS;
         int iXEE=iXE; int iYEE=iYE; int iZEE=iZE;
         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
            for (int iZ = iZSS; iZ<=iZEE;++iZ)
            {
              commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX,iY,iZ);
              ++iterator;
              // std::cout << "adding cell to dataOut " << iX << " " << iY << " " << iZ << " on subdomain " << commDataHandler.domainInfo.getLocalInfo().localSubDomain << " with neighbour direction " << neighbour << " for iPop" << iPop<< std::endl;
            }
         commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices=iterator;
       }
      
      }
      }
    }
  }
}

//same function but for 2D
template<typename T,template <typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesWrite (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice2D<T,Lattice>& lattice)
{
 for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
    
      int direction[2] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      for(unsigned dim=0;dim<Lattice<T>::d;++dim)
      {
        direction[dim] =  Lattice<T>::c(neighbour,dim);
      }
   
     int iXS = std::numeric_limits<int>::max();
     int iYS = std::numeric_limits<int>::max();
     int iXE = std::numeric_limits<int>::max();
     int iYE = std::numeric_limits<int>::max();

      bool directCommunication=false;
      unsigned ghostLayer[] = {0,0};

      if (commDataHandler.domainInfo.getLocalInfo().ghostLayer[0] == 0 and commDataHandler.domainInfo.getLocalInfo().ghostLayer[1] == 0) // no ghostLayers, thus direct communication
      {
        directCommunication = true;
      }
      else 
      {
        ghostLayer[0] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[0]-1;
        ghostLayer[1] = commDataHandler.domainInfo.getLocalInfo().ghostLayer[1]-1;
      }

     auto localInfo = commDataHandler.domainInfo.getLocalInfo();

     switch (direction[0]) 
     {
      case 1:
        iXS = localInfo.coreGridEnd()[0]-1-ghostLayer[0]; 
        iXE = iXS + ghostLayer[0]; 
        break;

      case 0:
        iXS = 0; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case -1:
        iXS = localInfo.coreGridStart()[0];
        iXE = iXS + ghostLayer[0];
        break;
     }
     switch (direction[1]) 
     {
      case 1:
        iYS = localInfo.coreGridEnd()[1]-1-ghostLayer[1]; 
        iYE = iYS + ghostLayer[1]; 
        break;

      case 0:
        iYS = 0; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case -1:
        iYS = localInfo.coreGridStart()[1];
        iYE = iYS + ghostLayer[1];
        break;
     }

      if (directCommunication)
      {
      for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS;
         int iXEE=iXE; int iYEE=iYE;
         int latticeOffset[3] = {-direction[0] + Lattice<T>::c(iPop,0), -direction[1] + Lattice<T>::c(iPop,1)};
         latticeOffset[0] <0 ? iXSS-=latticeOffset[0] : iXEE-=latticeOffset[0];
         latticeOffset[1] <0 ? iYSS-=latticeOffset[1] : iYEE-=latticeOffset[1];

         unsigned iterator = 0;
         for (unsigned iX = iXSS; iX<=iXEE;++iX)
          for (unsigned iY = iYSS; iY<=iYEE;++iY)
          {
            commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX,iY);
              ++iterator;
          }
         commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices=iterator;
       }
      }
      }
      else
      {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS;
         int iXEE=iXE; int iYEE=iYE;
         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
          {
            commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData[iterator] = lattice.getCellIndex(iX,iY);
            ++iterator;
            // std::cout << "adding cell to dataOut " << iX << " " << iY << " " << iZ << " on subdomain " << commDataHandler.domainInfo.getLocalInfo().localSubDomain << " with neighbour direction " << neighbour << " for iPop" << iPop<< std::endl;
          }
         commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices=iterator;
       }
      
      }
      }
    }
  }
}

template<typename T, template<typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesCPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
#ifdef OLB_DEBUG
  std::cout << "writing communication indices for read and write ... ";
#endif
  calculateCommunicationIndicesRead(commDataHandler,lattice);
  calculateCommunicationIndicesWrite(commDataHandler,lattice);
#ifdef OLB_DEBUG
  std::cout << "done" << std::endl;
#endif
}
//same function but for 2D
template<typename T, template<typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesCPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice2D<T,Lattice>& lattice)
{
#ifdef OLB_DEBUG
  std::cout << "writing communication indices for read and write ... ";
#endif
  calculateCommunicationIndicesRead(commDataHandler,lattice);
  calculateCommunicationIndicesWrite(commDataHandler,lattice);
#ifdef OLB_DEBUG
  std::cout << "done" << std::endl;
#endif
}

template<typename T,template<typename> class Lattice, template <typename> class Memory>
void readFromCommBufferToLattice (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
  for (unsigned population=0;population < Lattice<T>::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice<T>::opposite(population)])
    {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
        auto size=commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices;
        for (size_t index=0;index<size;++index)
        {
          if (iPop<Lattice<T>::q) // populations
          {
          lattice.getData()[Lattice<T>::opposite(iPop)][commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData[index]] = 
          commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData[index];
          }
          else //other data
          {
          lattice.getData()[iPop][commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData[index]] = 
          commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData[index];
          }

        }
       }
      }
    }
  }
}

template<typename T,template<typename> class Lattice, template <typename> class Memory>
void writeToCommBufferFromLattice (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
 for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
        auto size=commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices;
        for (size_t index=0;index<size;++index)
        {
          if(iPop<Lattice<T>::q) // population
          commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData[index] =
          lattice.getData()[Lattice<T>::opposite(iPop)][commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData[index]]; 
          else //other data
          {
          commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData[index] =
          lattice.getData()[iPop][commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData[index]];
          }
        }
       }
      }
    }
  }
}

#ifdef __CUDACC__


template<typename T,template<typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesGPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
  CommunicationDataHandler<T,Lattice<T>,memory_space::HostHeap> commDataHandlerCPU = createCommunicationDataHandler<memory_space::HostHeap>(commDataHandler.domainInfo);

  calculateCommunicationIndicesCPU(commDataHandlerCPU,lattice);


  for (unsigned population=0;population < Lattice<T>::q; ++population)
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
        if (commDataHandlerCPU.getCommunicationDataOut()[population].popArrays[iPop].popData)
        {
          auto size = commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].indexData.size();
          HANDLE_ERROR(cudaMemcpy(
                commDataHandler.getCommunicationDataOut()[population].indexArrays[iPop].indexData.get(),
                commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].indexData.get(),sizeof(long long)*size,
                cudaMemcpyHostToDevice));
          commDataHandler.getCommunicationDataOut()[population].indexArrays[iPop].noIndices = 
          commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].noIndices;
          // for (unsigned i=0;i<commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].noIndices;++i)
          // {
            // long long ind = commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].indexData[i];
            // size_t idx[3];
            // util::getCellIndices3D(ind,4,4,idx);
            // std::cout << " index " << ind << " equals lattice position " << idx[0] << " " << idx[1] << "  " << idx[2] << std::endl;
          // }
        } 
        if (commDataHandlerCPU.getCommunicationDataIn()[population].popArrays[iPop].popData)
        {
          auto size = commDataHandlerCPU.getCommunicationDataIn()[population].indexArrays[iPop].indexData.size();
          HANDLE_ERROR(cudaMemcpy(
                commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData.get(),
                commDataHandlerCPU.getCommunicationDataIn()[population].indexArrays[iPop].indexData.get(),sizeof(long long)*size,
                cudaMemcpyHostToDevice));
          commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices = 
          commDataHandlerCPU.getCommunicationDataIn()[population].indexArrays[iPop].noIndices;
        } 

      }
  cudaDeviceSynchronize();
}

//same function but for 2D
template<typename T,template<typename> class Lattice, template <typename> class Memory>
void calculateCommunicationIndicesGPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice2D<T,Lattice>& lattice)
{
  CommunicationDataHandler<T,Lattice<T>,memory_space::HostHeap> commDataHandlerCPU = createCommunicationDataHandler<memory_space::HostHeap>(commDataHandler.domainInfo);

  calculateCommunicationIndicesCPU(commDataHandlerCPU,lattice);

  for (unsigned population=0;population < Lattice<T>::q; ++population)
      for (unsigned iPop=0;iPop<Lattice<T>::q+1+Lattice<T>::d;++iPop)
      {
        if (commDataHandlerCPU.getCommunicationDataOut()[population].popArrays[iPop].popData)
        {
          auto size = commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].indexData.size();
          HANDLE_ERROR(cudaMemcpy(
                commDataHandler.getCommunicationDataOut()[population].indexArrays[iPop].indexData.get(),
                commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].indexData.get(),sizeof(long long)*size,
                cudaMemcpyHostToDevice));
          commDataHandler.getCommunicationDataOut()[population].indexArrays[iPop].noIndices = 
          commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].noIndices;
          // for (unsigned i=0;i<commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].noIndices;++i)
          // {
            // long long ind = commDataHandlerCPU.getCommunicationDataOut()[population].indexArrays[iPop].indexData[i];
            // size_t idx[3];
            // util::getCellIndices3D(ind,4,4,idx);
            // std::cout << " index " << ind << " equals lattice position " << idx[0] << " " << idx[1] << "  " << idx[2] << std::endl;
          // }
        } 
        if (commDataHandlerCPU.getCommunicationDataIn()[population].popArrays[iPop].popData)
        {
          auto size = commDataHandlerCPU.getCommunicationDataIn()[population].indexArrays[iPop].indexData.size();
          HANDLE_ERROR(cudaMemcpy(
                commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData.get(),
                commDataHandlerCPU.getCommunicationDataIn()[population].indexArrays[iPop].indexData.get(),sizeof(long long)*size,
                cudaMemcpyHostToDevice));
          commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices = 
          commDataHandlerCPU.getCommunicationDataIn()[population].indexArrays[iPop].noIndices;
        } 

      }
  cudaDeviceSynchronize();
}

template<typename T>
__global__ void readFromCommBufferToLatticeKernel (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, T* const OPENLB_RESTRICT interfaceData, long long * const OPENLB_RESTRICT indices,unsigned iPop, size_t gridSize)
{
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= gridSize)
    return;
  cellData[iPop][indices[threadIndex]] = interfaceData[threadIndex];
  // size_t cellIndex[3];
  // util::getCellIndices3D(indices[threadIndex],4,4,cellIndex);
  // printf("readValue %E  to index %lu %lu %lu \n",cellData[iPop][indices[threadIndex]],cellIndex[0],cellIndex[1],cellIndex[2]);
}

template<typename T>
__global__ void writeToCommBufferFromLatticeKernel (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, T* const OPENLB_RESTRICT interfaceData, long long * const OPENLB_RESTRICT indices,unsigned iPop, size_t gridSize)
{
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= gridSize)
    return;
  interfaceData[threadIndex] =cellData[iPop][indices[threadIndex]];
  // size_t cellIndex[3];
  // util::getCellIndices3D(indices[threadIndex],4,4,cellIndex);
  // printf("wroteValue %E  from index %lu %lu %lu %lli \n",cellData[iPop][indices[threadIndex]],cellIndex[0],cellIndex[1],cellIndex[2],indices[threadIndex]);
}


template<typename T,template<typename> class Lattice, template <typename> class Memory>
void readFromCommBufferToLatticeGPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
  for (unsigned population=0;population < Lattice<T>::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice<T>::opposite(population)])
    {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
        auto size=commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices;
        T* interfaceData = commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData.get();
        long long* indices = commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData.get();
        dim3 threadsPerBlock(256,1,1);
        dim3 numBlocks( size/256 +1 ,1,1);
        if (iPop < Lattice<T>::q)
          readFromCommBufferToLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,Lattice<T>::opposite(iPop),size);
        else
          readFromCommBufferToLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,iPop,size);
      }
     }
    }
  }
}
//same function but for 2D
template<typename T,template<typename> class Lattice, template <typename> class Memory>
void readFromCommBufferToLatticeGPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice2D<T,Lattice>& lattice)
{
  for (unsigned population=0;population < Lattice<T>::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice<T>::opposite(population)])
    {
      for (unsigned iPop=0;iPop<Lattice<T>::q+1+Lattice<T>::d;++iPop)
      {
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
        auto size=commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].noIndices;
        T* interfaceData = commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData.get();
        long long* indices = commDataHandler.getCommunicationDataIn()[population].indexArrays[iPop].indexData.get();
        dim3 threadsPerBlock(256,1,1);
        dim3 numBlocks( size/256 +1 ,1,1);
        if (iPop < Lattice<T>::q)
          readFromCommBufferToLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,Lattice<T>::opposite(iPop),size);
        else
          readFromCommBufferToLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,iPop,size);
    
      }
     }
    }
  }
}

template<typename T,template<typename> class Lattice, template <typename> class Memory>
void writeToCommBufferFromLatticeGPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice3D<T,Lattice>& lattice)
{
 for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
      for (unsigned iPop=0;iPop<Lattice<T>::dataSize;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
        auto size=commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices;
        T* interfaceData = commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.get();
        long long* indices = commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData.get();
        dim3 threadsPerBlock(256,1,1);
        dim3 numBlocks( size/256 +1 ,1,1);
        if (iPop < Lattice<T>::q)
          writeToCommBufferFromLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,Lattice<T>::opposite(iPop),size);
        else
          writeToCommBufferFromLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,iPop,size);
       }
      }
    }
  }
}

//same function but for 2D
template<typename T,template<typename> class Lattice, template <typename> class Memory>
void writeToCommBufferFromLatticeGPU (CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler, BlockLattice2D<T,Lattice>& lattice)
{
 for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
      for (unsigned iPop=0;iPop<Lattice<T>::q+1+Lattice<T>::d;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
        auto size=commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].noIndices;
        T* interfaceData = commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData.get();
        long long* indices = commDataHandler.getCommunicationDataOut()[neighbour].indexArrays[iPop].indexData.get();
        dim3 threadsPerBlock(256,1,1);
        dim3 numBlocks( size/256 +1 ,1,1);
        if (iPop < Lattice<T>::q)
          writeToCommBufferFromLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,Lattice<T>::opposite(iPop),size);
        else
          writeToCommBufferFromLatticeKernel<<<numBlocks,threadsPerBlock,0,commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].stream>>>(lattice.getDataGPU(),interfaceData,indices,iPop,size);
       }
      }
    }
  }
}
#endif

//Forward declarations for the initGhostLayer function
template<typename T, template<typename U> class Lattice>
class GhostDynamics;

namespace instances {
  template <typename T, template<typename U> class Lattice> GhostDynamics<T,Lattice>& getGhostDynamics();
}

template<typename T,template<typename> class Lattice>
void initGhostLayer(SubDomainInformation<T,Lattice<T>> subDomInfo, BlockLattice3D<T,Lattice>& lattice)
{
 
  if (subDomInfo.ghostLayer[0]) {
      lattice.defineDynamics(0, subDomInfo.ghostLayer[0]-1, 0, subDomInfo.localGridSize()[1]-1, 0, subDomInfo.localGridSize()[2]-1, &instances::getGhostDynamics<T,Lattice>());
      lattice.defineDynamics(subDomInfo.localGridSize()[0]-subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1, 0, subDomInfo.localGridSize()[1]-1, 0, subDomInfo.localGridSize()[2]-1, &instances::getGhostDynamics<T,Lattice>());
  }
  if (subDomInfo.ghostLayer[1]) {
      lattice.defineDynamics(subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1-subDomInfo.ghostLayer[0], 0, subDomInfo.ghostLayer[1]-1, 0, subDomInfo.localGridSize()[2]-1, &instances::getGhostDynamics<T,Lattice>());
      lattice.defineDynamics(subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1-subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[1]-subDomInfo.ghostLayer[1], subDomInfo.localGridSize()[1]-1, 0, subDomInfo.localGridSize()[2]-1, &instances::getGhostDynamics<T,Lattice>());
  }
  if (subDomInfo.ghostLayer[2]) {
      lattice.defineDynamics(subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1-subDomInfo.ghostLayer[0], subDomInfo.ghostLayer[1], subDomInfo.localGridSize()[1]-1-subDomInfo.ghostLayer[1], 0, subDomInfo.ghostLayer[2]-1, &instances::getGhostDynamics<T,Lattice>());
      lattice.defineDynamics(subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1-subDomInfo.ghostLayer[0], subDomInfo.ghostLayer[1], subDomInfo.localGridSize()[1]-1-subDomInfo.ghostLayer[1], subDomInfo.localGridSize()[2]-subDomInfo.ghostLayer[2], subDomInfo.localGridSize()[2]-1, &instances::getGhostDynamics<T,Lattice>());
  }

}

template<typename T,template<typename> class Lattice>
void initGhostLayer(SubDomainInformation<T,Lattice<T>> subDomInfo, BlockLattice2D<T,Lattice>& lattice)
{
  if (subDomInfo.ghostLayer[0]) {
      lattice.defineDynamics(0, subDomInfo.ghostLayer[0]-1, 0, subDomInfo.localGridSize()[1]-1, &instances::getGhostDynamics<T,Lattice>());
      lattice.defineDynamics(subDomInfo.localGridSize()[0]-subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1, 0, subDomInfo.localGridSize()[1]-1, &instances::getGhostDynamics<T,Lattice>());
  }
  if (subDomInfo.ghostLayer[1]) {
      lattice.defineDynamics(subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1-subDomInfo.ghostLayer[0], 0, subDomInfo.ghostLayer[1]-1, &instances::getGhostDynamics<T,Lattice>());
      lattice.defineDynamics(subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[0]-1-subDomInfo.ghostLayer[0], subDomInfo.localGridSize()[1]-subDomInfo.ghostLayer[1], subDomInfo.localGridSize()[1]-1, &instances::getGhostDynamics<T,Lattice>());
  }
}

} // end of namespace
#endif
