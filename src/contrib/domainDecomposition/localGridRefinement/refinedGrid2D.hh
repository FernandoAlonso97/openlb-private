#ifndef REFINED_GRID_2D_HH
#define REFINED_GRID_2D_HH

#include "refinedGrid2D.h"

namespace olb
{

template <typename T, template <typename U> class Lattice> class BlockLattice2D;
template <typename T, class Lattice> class SubDomainInformation;
template <typename T, template <typename U> class Lattice> class UnitConverter;

template <typename T, template <typename U> class Lattice>
RefinedGrid2D<T, Lattice>::RefinedGrid2D(size_t nx, size_t ny, Dynamics<T, Lattice> *dynamics, T omega, bool isParent, size_t offsetX, size_t offsetY, size_t parentGlobalOffsetX, size_t parentGlobalOffsetY, unsigned interpLayerThickness)
    : _fluidDynamicsPointer(dynamics), _omega(omega), _childOmega(2 * _omega / (4 - _omega)), _baseDimensions{nx, ny}, _baseCoarseDimensions{(nx + 1) / 2, (ny + 1) / 2}, _offset{offsetX, offsetY},
      _globalOffset{parentGlobalOffsetX * 2 + offsetX * 2, parentGlobalOffsetY * 2 + offsetY * 2}, _isParent(isParent), _interpLayerThickness(interpLayerThickness)
{
  unsigned tempGhostLayer[3] = {0, 0, 0};
  if (isParent)
    _interpLayerThickness = 0;

  _refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>(nx + 2 * _interpLayerThickness, ny + 2 * _interpLayerThickness, (size_t)0, 0u, 1u, tempGhostLayer);
  _localSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>(nx + 2 * _interpLayerThickness, ny + 2 * _interpLayerThickness, (size_t)0, 0u, 1u, tempGhostLayer);
  std::vector<size_t> noIndices = {};
  _domainInfo = std::shared_ptr<DomainInformation<T, Lattice<T>>>(new DomainInformation<T, Lattice<T>>(domainInfoDecomposeDomainAtIndices(_refSubDomain, 0, noIndices, 0u, tempGhostLayer)));
}

template <typename T, template <typename U> class Lattice>
UnitConverter<T,Lattice> RefinedGrid2D<T,Lattice>::getConverterForRefinementLevel(UnitConverter<T,Lattice> baseConverter, int refinementLevel) {
  return UnitConverter<T,Lattice>(baseConverter.getConversionFactorLength()*pow(0.5, refinementLevel), 
                                  baseConverter.getConversionFactorTime()*pow(0.5, refinementLevel),
                                  baseConverter.getCharPhysLength()*pow(0.5, refinementLevel),
                                  baseConverter.getCharPhysVelocity(),
                                  baseConverter.getPhysViscosity(),
                                  baseConverter.getPhysDensity());
}

template <typename T, template <typename U> class Lattice>
T RefinedGrid2D<T,Lattice>::getOmegaForRefinementLevel(T baseOmega, int refinementLevel) {
  T refinedOmega = baseOmega;

  for (int i = 0; i < refinementLevel; i++) {
    refinedOmega = 2*refinedOmega/(4-refinedOmega);
  }

  return refinedOmega;
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T,Lattice>::getTopLevelParentIndices(size_t iX, size_t iY, T topLevelIndices[2]) {
  topLevelIndices[0] = (T)(iX - _interpLayerThickness + _globalOffset[0]) * pow(0.5, _refinementLevel);
  topLevelIndices[1] = (T)(iY - _interpLayerThickness + _globalOffset[1]) * pow(0.5, _refinementLevel);
}

template <typename T, template <typename U> class Lattice>
int RefinedGrid2D<T, Lattice>::calculateTotalLatticeUnits() {
  int totalWorkload = 0;
  for (size_t i = 0; i < _refSubDomain.coreGridSize()[0]; i++)
  {
    size_t currentWorkload = assessWorkloadAt(0, i);
    totalWorkload += currentWorkload;
  }
  return totalWorkload;
}

template <typename T, template <typename U> class Lattice>
size_t RefinedGrid2D<T, Lattice>::addChild(Dynamics<T, Lattice> *childDynamics, size_t nx, size_t ny, size_t offsetX, size_t offsetY, unsigned interpLayerThickness)
{
  RefinedGrid2D<T, Lattice> child(2 * nx - 1, 2 * ny - 1, childDynamics, _childOmega, false, offsetX, offsetY, _globalOffset[0], _globalOffset[1], 2 * interpLayerThickness);
  child._refinementLevel = _refinementLevel + 1;
  child._childID = children.size();
  child._parentID = _childID;
  children.push_back(child);
  return child._childID;
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::refinedDecomposeDomainAtIndices(std::vector<size_t> splitIndices, unsigned axis, unsigned localSubDomain, unsigned ghostLayer[3])
{
  if (ghostLayer[axis] < 2)
    std::cout << "WARNING: Ghost layer along decomposition axis should generally be at least 2 for local grid refinement to work properly." << std::endl;
  std::vector<size_t> processedSplitIndices;
  std::vector<size_t> childSplitIndices;
  for (int i = 0; i < splitIndices.size(); i++)
  {
    size_t currentIndex = splitIndices.at(i);

    if (currentIndex < _globalOffset[axis] - _interpLayerThickness)
      processedSplitIndices.push_back(0);
    else if (currentIndex >= _globalOffset[axis] - _interpLayerThickness && currentIndex < _globalOffset[axis] + _baseDimensions[axis] + _interpLayerThickness)
      processedSplitIndices.push_back(currentIndex + _interpLayerThickness - _globalOffset[axis]);
    else
      processedSplitIndices.push_back(_globalOffset[axis] + _baseDimensions[axis] + _interpLayerThickness);

    childSplitIndices.push_back(currentIndex * 2);
  }
  _refSubDomain = decomposeDomainAlongLongestCoord<T, Lattice<T>>(_baseDimensions[0] + 2 * _interpLayerThickness, _baseDimensions[1] + 2 * _interpLayerThickness, (size_t)0, 0u, 1u, ghostLayer);
  _domainInfo = std::shared_ptr<DomainInformation<T, Lattice<T>>>(new DomainInformation<T, Lattice<T>>(domainInfoDecomposeDomainAtIndices(_refSubDomain, axis, processedSplitIndices, localSubDomain, ghostLayer)));
  _localSubDomain = _domainInfo->getLocalInfo();
  for (int child = 0; child < children.size(); child++)
  {
    children[child].refinedDecomposeDomainAtIndices(childSplitIndices, axis, localSubDomain, ghostLayer);
  }
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::refinedDecomposeDomainEvenlyAlongAxis(unsigned localSubDomain, unsigned axis, unsigned noSubDomains, unsigned ghostLayer[3], bool accountForCutouts)
{
  if (ghostLayer[axis] < 2)
    std::cout << "WARNING: Ghost layer along decomposition axis should generally be at least 2 for local grid refinement to work properly." << std::endl;
  if (!accountForCutouts)
  {
    _refSubDomain = decomposeDomainApproximatelyAlongAxis<T, Lattice<T>>(_baseDimensions[0] + 2 * _interpLayerThickness, _baseDimensions[1] + 2 * _interpLayerThickness, (size_t)0, 0, 0u, 1u, ghostLayer);
    _domainInfo = std::shared_ptr<DomainInformation<T, Lattice<T>>>(new DomainInformation<T, Lattice<T>>(domainInfoDecomposeDomainApproximatelyAlongAxis(_refSubDomain, axis, localSubDomain, noSubDomains, ghostLayer)));
    _localSubDomain = _domainInfo->getLocalInfo();
  }
  else
  {
    size_t totalWorkload = 0;
    for (size_t i = 0; i < _refSubDomain.coreGridSize()[axis]; i++)
    {
      size_t currentWorkload = nonRecursiveAssessWorkloadAt(axis, i);
      totalWorkload += currentWorkload;
    }
    size_t desiredWorkChunk = totalWorkload / noSubDomains;
    std::vector<size_t> splitIndices;

    size_t runningTotalWorkload = 0;
    for (size_t i = 0; i < _refSubDomain.coreGridSize()[axis]; i++)
    {
      if (splitIndices.size() == noSubDomains - 1)
        break;
      size_t currentWorkload = nonRecursiveAssessWorkloadAt(axis, i);
      runningTotalWorkload += currentWorkload;
      if (runningTotalWorkload >= desiredWorkChunk * (splitIndices.size() + 1))
      {
        splitIndices.push_back(i);
      }
    }
    _refSubDomain = decomposeDomainApproximatelyAlongAxis<T, Lattice<T>>(_baseDimensions[0] + 2 * _interpLayerThickness, _baseDimensions[1] + 2 * _interpLayerThickness, (size_t)0, 0, 0u, 1u, ghostLayer);
    _domainInfo = std::shared_ptr<DomainInformation<T, Lattice<T>>>(new DomainInformation<T, Lattice<T>>(domainInfoDecomposeDomainAtIndices(_refSubDomain, axis, splitIndices, localSubDomain, ghostLayer)));
    _localSubDomain = _domainInfo->getLocalInfo();
  }
  for (int child = 0; child < children.size(); child++)
  {
    children[child].refinedDecomposeDomainEvenlyAlongAxis(localSubDomain, axis, noSubDomains, ghostLayer, accountForCutouts);
  }
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::setupRefinementCPU()
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].setupRefinementCPU();
  }
  _latticePtr = std::make_shared<BlockLattice2D<T, Lattice>>(_localSubDomain, _fluidDynamicsPointer);
  calculateInterpolationIndices();

  if (C2FOutBufferIndicesC.size() > 0)
  {
    C2FOutBuffer = std::make_shared<CellBlockData<T, Lattice>>(C2FOutBufferIndicesC.size(), 1);
    C2FOutBuffers.push_back(C2FOutBuffer->getCellData());
  }
  else
  {
    C2FOutBuffers.push_back(nullptr);
  }

  if (F2COutBufferIndicesF.size() > 0)
  {
    F2COutBuffer = std::make_shared<CellBlockData<T, Lattice>>(F2COutBufferIndicesF.size(), 1);
    F2COutBuffers.push_back(F2COutBuffer->getCellData());
  }
  else
  {
    F2COutBuffers.push_back(nullptr);
  }
  if (C2FOutBufferIndicesInterpNonLocal.size() > 0)
  {
    C2FOutBufferInterpNonLocal = std::make_shared<CellBlockData<T, Lattice>>(C2FOutBufferIndicesInterpNonLocal.size(), 1);
    C2FInterpNonLocalOutBuffers.push_back(C2FOutBufferInterpNonLocal->getCellData());
  }
  else
  {
    C2FInterpNonLocalOutBuffers.push_back(nullptr);
  }

  if (F2FOutBufferIndicesF.size() > 0)
  {
    F2FOutBuffer = std::make_shared<CellBlockData<T, Lattice>>(F2FOutBufferIndicesF.size(), 1);
    F2FOutBuffers.push_back(F2FOutBuffer->getCellData());
  }
  else
  {
    F2FOutBuffers.push_back(nullptr);
  }

  if (C2FindicesCCoarseDirect.size() > 0)
    C2FinterpLayer = std::make_shared<CellBlockData<T, Lattice>>(C2FindicesCCoarseDirect.size(), 1);

  for (int child = 0; child < children.size(); child++)
  {
    childCellDatasEven.push_back(children[child]._latticePtr->cellData->getCellData());
    childCellDatasOdd.push_back(children[child]._latticePtr->cellData->getCellDataPrevious());
    childNYs.push_back(children[child]._localSubDomain.localGridSize()[1]);
  }
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::setupRefinementGPU()
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].setupRefinementGPU();
  }
  _latticePtr = std::make_shared<BlockLattice2D<T, Lattice>>(_localSubDomain, _fluidDynamicsPointer);
  calculateInterpolationIndices();

  for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
  {

    if (currentSubDomain == _localSubDomain.localSubDomain)
    {
      if (C2FOutBufferIndicesC.size() > 0)
      {
        C2FOutBuffer = std::make_shared<CellBlockData<T, Lattice>>(C2FOutBufferIndicesC.size(), 1);

        for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
        {
          //remove offsets - the offset pointers interfere with cudaMemHandles
          C2FOutBuffer->gpuGetFluidData()[iPop] = C2FOutBuffer->gpuGetNonOffsetFluidData()[iPop];
        }
      }

      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        T *currentDataVector = C2FOutBuffer ? C2FOutBuffer->gpuGetFluidData()[iPop] : nullptr;
        gpuC2FOutBufferDataVector.push_back(currentDataVector);

        //Mpi send the currentdatavector
        for (int destSubDomain = 0; destSubDomain < _localSubDomain.noSubDomains; destSubDomain++)
        {
          if (destSubDomain == _localSubDomain.localSubDomain)
            continue; // don't need to send if the target is ourself

          cudaIpcMemHandle_t handle;
          if (currentDataVector)
          {
            HANDLE_ERROR(cudaIpcGetMemHandle(&handle, currentDataVector));
            int hasVector = 1;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
            MPI_Send(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, destSubDomain, iPop, MPI_COMM_WORLD);
          }
          else
          {
            int hasVector = 0;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
          }
        }
      }
    }
    else
    {
      //mpi recv the currentdatavector
      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        int hasVector;
        T *currentDataVector = nullptr;
        MPI_Status status;
        MPI_Recv(&hasVector, 1, MPI_INT, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
        if (hasVector)
        {
          cudaIpcMemHandle_t handle;
          MPI_Recv(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
          HANDLE_ERROR(cudaIpcOpenMemHandle((void **)&currentDataVector, handle, cudaIpcMemLazyEnablePeerAccess));
          cudaDeviceSynchronize();
        }
        gpuC2FOutBufferDataVector.push_back(currentDataVector);
      }
    }
  }

  for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
  {
    if (currentSubDomain == _localSubDomain.localSubDomain)
    {
      if (F2COutBufferIndicesF.size() > 0)
      {
        F2COutBuffer = std::make_shared<CellBlockData<T, Lattice>>(F2COutBufferIndicesF.size(), 1);

        for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
        {
          //remove offsets - the offset pointers interfere with cudaMemHandles
          F2COutBuffer->gpuGetFluidData()[iPop] = F2COutBuffer->gpuGetNonOffsetFluidData()[iPop];
        }
      }

      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        T *currentDataVector = F2COutBuffer ? F2COutBuffer->gpuGetFluidData()[iPop] : nullptr;
        gpuF2COutBufferDataVector.push_back(currentDataVector);

        //Mpi send the currentdatavector
        for (int destSubDomain = 0; destSubDomain < _localSubDomain.noSubDomains; destSubDomain++)
        {
          if (destSubDomain == _localSubDomain.localSubDomain)
            continue; // don't need to send if the target is ourself

          cudaIpcMemHandle_t handle;
          if (currentDataVector)
          {
            HANDLE_ERROR(cudaIpcGetMemHandle(&handle, currentDataVector));
            int hasVector = 1;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
            MPI_Send(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, destSubDomain, iPop, MPI_COMM_WORLD);
          }
          else
          {
            int hasVector = 0;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
          }
        }
      }
    }
    else
    {
      //mpi recv here
      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        int hasVector;
        T *currentDataVector = nullptr;
        MPI_Status status;
        MPI_Recv(&hasVector, 1, MPI_INT, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
        if (hasVector)
        {
          cudaIpcMemHandle_t handle;
          MPI_Recv(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
          HANDLE_ERROR(cudaIpcOpenMemHandle((void **)&currentDataVector, handle, cudaIpcMemLazyEnablePeerAccess));
          cudaDeviceSynchronize();
        }
        gpuF2COutBufferDataVector.push_back(currentDataVector);
      }
    }
  }

  for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
  {
    if (currentSubDomain == _localSubDomain.localSubDomain)
    {
      if (C2FOutBufferIndicesInterpNonLocal.size() > 0)
      {
        C2FOutBufferInterpNonLocal = std::make_shared<CellBlockData<T, Lattice>>(C2FOutBufferIndicesInterpNonLocal.size(), 1);

        for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
        {
          //remove offsets - the offset pointers interfere with cudaMemHandles
          C2FOutBufferInterpNonLocal->gpuGetFluidData()[iPop] = C2FOutBufferInterpNonLocal->gpuGetNonOffsetFluidData()[iPop];
        }
      }

      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        T *currentDataVector = C2FOutBufferInterpNonLocal ? C2FOutBufferInterpNonLocal->gpuGetFluidData()[iPop] : nullptr;
        gpuC2FInterpNonLocalOutBufferDataVector.push_back(currentDataVector);

        //Mpi send the currentdatavector
        for (int destSubDomain = 0; destSubDomain < _localSubDomain.noSubDomains; destSubDomain++)
        {
          if (destSubDomain == _localSubDomain.localSubDomain)
            continue; // don't need to send if the target is ourself

          cudaIpcMemHandle_t handle;
          if (currentDataVector)
          {
            HANDLE_ERROR(cudaIpcGetMemHandle(&handle, currentDataVector));
            int hasVector = 1;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
            MPI_Send(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, destSubDomain, iPop, MPI_COMM_WORLD);
          }
          else
          {
            int hasVector = 0;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
          }
        }
      }
    }
    else
    {
      //mpi recv here
      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        int hasVector;
        T *currentDataVector = nullptr;
        MPI_Status status;
        MPI_Recv(&hasVector, 1, MPI_INT, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
        if (hasVector)
        {
          cudaIpcMemHandle_t handle;
          MPI_Recv(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
          HANDLE_ERROR(cudaIpcOpenMemHandle((void **)&currentDataVector, handle, cudaIpcMemLazyEnablePeerAccess));
          cudaDeviceSynchronize();
        }
        gpuC2FInterpNonLocalOutBufferDataVector.push_back(currentDataVector);
      }
    }
  }

  for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
  {
    if (currentSubDomain == _localSubDomain.localSubDomain)
    {
      if (F2FOutBufferIndicesF.size() > 0)
      {
        F2FOutBuffer = std::make_shared<CellBlockData<T, Lattice>>(F2FOutBufferIndicesF.size(), 1);

        for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
        {
          //remove offsets - the offset pointers interfere with cudaMemHandles
          F2FOutBuffer->gpuGetFluidData()[iPop] = F2FOutBuffer->gpuGetNonOffsetFluidData()[iPop];
        }
      }

      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        T *currentDataVector = F2FOutBuffer ? F2FOutBuffer->gpuGetFluidData()[iPop] : nullptr;
        gpuF2FOutBufferDataVector.push_back(currentDataVector);

        //Mpi send the currentdatavector
        for (int destSubDomain = 0; destSubDomain < _localSubDomain.noSubDomains; destSubDomain++)
        {
          if (destSubDomain == _localSubDomain.localSubDomain)
            continue; // don't need to send if the target is ourself

          cudaIpcMemHandle_t handle;
          if (currentDataVector)
          {
            HANDLE_ERROR(cudaIpcGetMemHandle(&handle, currentDataVector));
            int hasVector = 1;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
            MPI_Send(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, destSubDomain, iPop, MPI_COMM_WORLD);
          }
          else
          {
            int hasVector = 0;
            MPI_Send(&hasVector, 1, MPI_INT, destSubDomain, iPop, MPI_COMM_WORLD);
          }
        }
      }
    }
    else
    {
      //mpi recv here
      for (int iPop = 0; iPop < Lattice<T>::dataSize; iPop++)
      {
        int hasVector;
        T *currentDataVector = nullptr;
        MPI_Status status;
        MPI_Recv(&hasVector, 1, MPI_INT, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
        if (hasVector)
        {
          cudaIpcMemHandle_t handle;
          MPI_Recv(&handle, sizeof(cudaIpcMemHandle_t), MPI_BYTE, currentSubDomain, iPop, MPI_COMM_WORLD, &status);
          HANDLE_ERROR(cudaIpcOpenMemHandle((void **)&currentDataVector, handle, cudaIpcMemLazyEnablePeerAccess));
          cudaDeviceSynchronize();
        }
        gpuF2FOutBufferDataVector.push_back(currentDataVector);
      }
    }
  }
  MPI_Barrier(MPI_COMM_WORLD);

  if (C2FindicesCCoarseDirect.size() > 0)
  {
    C2FinterpLayer = std::make_shared<CellBlockData<T, Lattice>>(C2FindicesCCoarseDirect.size(), 1);
  }

#ifdef __CUDACC__
  for (int child = 0; child < children.size(); child++)
  {
    gpuChildCellDatasEvenVector.push_back(children[child]._latticePtr->cellData->gpuGetFluidData());
    gpuChildCellDatasOddVector.push_back(children[child]._latticePtr->cellData->gpuGetFluidDataPrevious());
    childNYs.push_back(children[child]._latticePtr->getNy());
  }

  gpuF2CSize = F2CindicesF.size();
  if (gpuF2CSize > 0)
  {
    gpuF2CindicesF = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuF2CSize);
    gpuF2CindicesC = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuF2CSize);
    gpuF2CGridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuF2CSize);

    HANDLE_ERROR(cudaMemcpy(gpuF2CindicesF->get(), &F2CindicesF[0], sizeof(size_t) * gpuF2CSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2CindicesC->get(), &F2CindicesC[0], sizeof(size_t) * gpuF2CSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2CGridIDs->get(), &F2CGridIDs[0], sizeof(int) * gpuF2CSize, cudaMemcpyHostToDevice));
  }

  gpuF2COutBufferSize = F2COutBufferIndicesF.size();
  if (gpuF2COutBufferSize > 0)
  {
    gpuF2COutBufferIndicesF = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuF2COutBufferSize);
    gpuF2COutBufferChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuF2COutBufferSize);

    HANDLE_ERROR(cudaMemcpy(gpuF2COutBufferIndicesF->get(), &F2COutBufferIndicesF[0], sizeof(size_t) * gpuF2COutBufferSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2COutBufferChildIDs->get(), &F2COutBufferChildIDs[0], sizeof(int) * gpuF2COutBufferSize, cudaMemcpyHostToDevice));

    gpuF2COutBufferLoadIndicesF = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(F2COutBufferLoadIndicesF.size());
    gpuF2COutBufferLoadChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(F2COutBufferLoadChildIDs.size());
    gpuF2COutBufferSharedIndicesF = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(F2COutBufferSharedIndicesF.size());

    HANDLE_ERROR(cudaMemcpy(gpuF2COutBufferLoadIndicesF->get(), &F2COutBufferLoadIndicesF[0], sizeof(size_t) * F2COutBufferLoadIndicesF.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2COutBufferLoadChildIDs->get(), &F2COutBufferLoadChildIDs[0], sizeof(int) * F2COutBufferLoadChildIDs.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2COutBufferSharedIndicesF->get(), &F2COutBufferSharedIndicesF[0], sizeof(size_t) * F2COutBufferSharedIndicesF.size(), cudaMemcpyHostToDevice));
  }

  if (gpuF2COutBufferDataVector.size() > 0)
  {
    gpuF2COutBufferData = std::make_shared<memory_space::CudaDeviceHeap<T *>>(gpuF2COutBufferDataVector.size());

    for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
    {
      T **currentOutBufferAddress = gpuF2COutBufferData->get();
      currentOutBufferAddress += currentSubDomain * Lattice<T>::dataSize;
      gpuF2COutBuffersVector.push_back(currentOutBufferAddress);
    }

    gpuF2COutBuffers = std::make_shared<memory_space::CudaDeviceHeap<T **>>(gpuF2COutBuffersVector.size());

    HANDLE_ERROR(cudaMemcpy(gpuF2COutBufferData->get(), &gpuF2COutBufferDataVector[0], sizeof(T *) * gpuF2COutBufferDataVector.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2COutBuffers->get(), &gpuF2COutBuffersVector[0], sizeof(T **) * gpuF2COutBuffersVector.size(), cudaMemcpyHostToDevice));
  }

  gpuF2FSize = F2FindicesSource.size();
  if (gpuF2FSize > 0)
  {
    gpuF2FindicesSource = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuF2FSize);
    gpuF2FindicesDest = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuF2FSize);
    gpuF2FSourceGridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuF2FSize);
    gpuF2FDestChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuF2FSize);

    HANDLE_ERROR(cudaMemcpy(gpuF2FindicesSource->get(), &F2FindicesSource[0], sizeof(size_t) * gpuF2FSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2FindicesDest->get(), &F2FindicesDest[0], sizeof(size_t) * gpuF2FSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2FSourceGridIDs->get(), &F2FSourceGridIDs[0], sizeof(int) * gpuF2FSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2FDestChildIDs->get(), &F2FDestChildIDs[0], sizeof(int) * gpuF2FSize, cudaMemcpyHostToDevice));
  }

  gpuC2FCoarseDirectSize = C2FindicesFCoarseDirect.size();
  if (gpuC2FCoarseDirectSize > 0)
  {
    gpuC2FindicesFCoarseDirect = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FCoarseDirectSize);
    gpuC2FindicesCCoarseDirect = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FCoarseDirectSize);
    gpuC2FCoarseDirectFineChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FCoarseDirectSize);
    gpuC2FCoarseDirectCoarseGridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FCoarseDirectSize);

    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFCoarseDirect->get(), &C2FindicesFCoarseDirect[0], sizeof(size_t) * gpuC2FCoarseDirectSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesCCoarseDirect->get(), &C2FindicesCCoarseDirect[0], sizeof(size_t) * gpuC2FCoarseDirectSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FCoarseDirectFineChildIDs->get(), &C2FCoarseDirectFineChildIDs[0], sizeof(int) * gpuC2FCoarseDirectSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FCoarseDirectCoarseGridIDs->get(), &C2FCoarseDirectCoarseGridIDs[0], sizeof(int) * gpuC2FCoarseDirectSize, cudaMemcpyHostToDevice));
  }

  gpuC2FOutBufferSize = C2FOutBufferIndicesC.size();
  if (gpuC2FOutBufferSize > 0)
  {
    gpuC2FOutBufferIndicesC = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FOutBufferSize);

    HANDLE_ERROR(cudaMemcpy(gpuC2FOutBufferIndicesC->get(), &C2FOutBufferIndicesC[0], sizeof(size_t) * gpuC2FOutBufferSize, cudaMemcpyHostToDevice));
  }

  if (gpuC2FOutBufferDataVector.size() > 0)
  {
    gpuC2FOutBufferData = std::make_shared<memory_space::CudaDeviceHeap<T *>>(gpuC2FOutBufferDataVector.size());

    for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
    {
      T **currentOutBufferAddress = gpuC2FOutBufferData->get();
      currentOutBufferAddress += currentSubDomain * Lattice<T>::dataSize;
      gpuC2FOutBuffersVector.push_back(currentOutBufferAddress);
    }

    gpuC2FOutBuffers = std::make_shared<memory_space::CudaDeviceHeap<T **>>(gpuC2FOutBuffersVector.size());

    HANDLE_ERROR(cudaMemcpy(gpuC2FOutBufferData->get(), &gpuC2FOutBufferDataVector[0], sizeof(T *) * gpuC2FOutBufferDataVector.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FOutBuffers->get(), &gpuC2FOutBuffersVector[0], sizeof(T **) * gpuC2FOutBuffersVector.size(), cudaMemcpyHostToDevice));
  }

  gpuF2FOutBufferSize = F2FOutBufferIndicesF.size();
  if (gpuF2FOutBufferSize > 0)
  {
    gpuF2FOutBufferIndicesF = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuF2FOutBufferSize);
    gpuF2FOutBufferChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuF2FOutBufferSize);

    HANDLE_ERROR(cudaMemcpy(gpuF2FOutBufferIndicesF->get(), &F2FOutBufferIndicesF[0], sizeof(size_t) * gpuF2FOutBufferSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2FOutBufferChildIDs->get(), &F2FOutBufferChildIDs[0], sizeof(int) * gpuF2FOutBufferSize, cudaMemcpyHostToDevice));
  }

  if (gpuF2FOutBufferDataVector.size() > 0)
  {
    gpuF2FOutBufferData = std::make_shared<memory_space::CudaDeviceHeap<T *>>(gpuF2FOutBufferDataVector.size());

    for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
    {
      T **currentOutBufferAddress = gpuF2FOutBufferData->get();
      currentOutBufferAddress += currentSubDomain * Lattice<T>::dataSize;
      gpuF2FOutBuffersVector.push_back(currentOutBufferAddress);
    }

    gpuF2FOutBuffers = std::make_shared<memory_space::CudaDeviceHeap<T **>>(gpuF2FOutBuffersVector.size());

    HANDLE_ERROR(cudaMemcpy(gpuF2FOutBufferData->get(), &gpuF2FOutBufferDataVector[0], sizeof(T *) * gpuF2FOutBufferDataVector.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuF2FOutBuffers->get(), &gpuF2FOutBuffersVector[0], sizeof(T **) * gpuF2FOutBuffersVector.size(), cudaMemcpyHostToDevice));
  }

  gpuC2FInterpSize = C2FindicesFInterp.size();
  if (gpuC2FInterpSize > 0)
  {
    gpuC2FindicesFInterp = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpSize);
    gpuC2FInterpChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpSize);
    gpuC2FInterpAxis = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpSize);
    gpuC2FInterpType = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpSize);

    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterp->get(), &C2FindicesFInterp[0], sizeof(size_t) * gpuC2FInterpSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FInterpChildIDs->get(), &C2FInterpChildIDs[0], sizeof(int) * gpuC2FInterpSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FInterpAxis->get(), &C2FInterpAxis[0], sizeof(int) * gpuC2FInterpSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FInterpType->get(), &C2FInterpType[0], sizeof(int) * gpuC2FInterpSize, cudaMemcpyHostToDevice));
  }

  gpuC2FInterpNonLocalSize = C2FindicesFInterpNonLocal.size();
  if (gpuC2FInterpNonLocalSize > 0)
  {
    gpuC2FindicesFInterpNonLocal = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpNonLocalSize);
    gpuC2FInterpNonLocalChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor1 = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor2 = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor3 = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor4 = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor1GridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor2GridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor3GridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpNonLocalSize);
    gpuC2FindicesFInterpNonLocalNeighbor4GridIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpNonLocalSize);

    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocal->get(), &C2FindicesFInterpNonLocal[0], sizeof(size_t) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FInterpNonLocalChildIDs->get(), &C2FInterpNonLocalChildIDs[0], sizeof(int) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor1->get(), &C2FindicesFInterpNonLocalNeighbor1[0], sizeof(size_t) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor2->get(), &C2FindicesFInterpNonLocalNeighbor2[0], sizeof(size_t) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor3->get(), &C2FindicesFInterpNonLocalNeighbor3[0], sizeof(size_t) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor4->get(), &C2FindicesFInterpNonLocalNeighbor4[0], sizeof(size_t) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor1GridIDs->get(), &C2FindicesFInterpNonLocalNeighbor1GridIDs[0], sizeof(int) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor2GridIDs->get(), &C2FindicesFInterpNonLocalNeighbor2GridIDs[0], sizeof(int) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor3GridIDs->get(), &C2FindicesFInterpNonLocalNeighbor3GridIDs[0], sizeof(int) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FindicesFInterpNonLocalNeighbor4GridIDs->get(), &C2FindicesFInterpNonLocalNeighbor4GridIDs[0], sizeof(int) * gpuC2FInterpNonLocalSize, cudaMemcpyHostToDevice));
  }

  gpuC2FInterpNonLocalOutBuffersSize = C2FOutBufferIndicesInterpNonLocal.size();
  if (gpuC2FInterpNonLocalSize > 0)
  {
    gpuC2FOutBufferIndicesInterpNonLocal = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(gpuC2FInterpNonLocalOutBuffersSize);
    gpuC2FOutBufferIndicesInterpNonLocalChildIDs = std::make_shared<memory_space::CudaDeviceHeap<int>>(gpuC2FInterpNonLocalOutBuffersSize);

    HANDLE_ERROR(cudaMemcpy(gpuC2FOutBufferIndicesInterpNonLocal->get(), &C2FOutBufferIndicesInterpNonLocal[0], sizeof(size_t) * gpuC2FInterpNonLocalOutBuffersSize, cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FOutBufferIndicesInterpNonLocalChildIDs->get(), &C2FOutBufferIndicesInterpNonLocalChildIDs[0], sizeof(int) * gpuC2FInterpNonLocalOutBuffersSize, cudaMemcpyHostToDevice));
  }

  if (gpuC2FInterpNonLocalOutBufferDataVector.size() > 0)
  {
    gpuC2FInterpNonLocalOutBufferData = std::make_shared<memory_space::CudaDeviceHeap<T *>>(gpuC2FInterpNonLocalOutBufferDataVector.size());

    for (int currentSubDomain = 0; currentSubDomain < _localSubDomain.noSubDomains; currentSubDomain++)
    {
      T **currentOutBufferAddress = gpuC2FInterpNonLocalOutBufferData->get();
      currentOutBufferAddress += currentSubDomain * Lattice<T>::dataSize;
      gpuC2FInterpNonLocalOutBuffersVector.push_back(currentOutBufferAddress);
    }

    gpuC2FInterpNonLocalOutBuffers = std::make_shared<memory_space::CudaDeviceHeap<T **>>(gpuC2FInterpNonLocalOutBuffersVector.size());

    HANDLE_ERROR(cudaMemcpy(gpuC2FInterpNonLocalOutBufferData->get(), &gpuC2FInterpNonLocalOutBufferDataVector[0], sizeof(T *) * gpuC2FInterpNonLocalOutBufferDataVector.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuC2FInterpNonLocalOutBuffers->get(), &gpuC2FInterpNonLocalOutBuffersVector[0], sizeof(T **) * gpuC2FInterpNonLocalOutBuffersVector.size(), cudaMemcpyHostToDevice));
  }

  if (children.size() > 0)
  {
    gpuChildCellDatasEven = std::make_shared<memory_space::CudaDeviceHeap<T **>>(children.size());
    gpuChildCellDatasOdd = std::make_shared<memory_space::CudaDeviceHeap<T **>>(children.size());
    gpuChildNYs = std::make_shared<memory_space::CudaDeviceHeap<size_t>>(children.size());

    HANDLE_ERROR(cudaMemcpy(gpuChildCellDatasEven->get(), &gpuChildCellDatasEvenVector[0], sizeof(T **) * children.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuChildCellDatasOdd->get(), &gpuChildCellDatasOddVector[0], sizeof(T **) * children.size(), cudaMemcpyHostToDevice));
    HANDLE_ERROR(cudaMemcpy(gpuChildNYs->get(), &childNYs[0], sizeof(size_t) * children.size(), cudaMemcpyHostToDevice));
  }
  cudaDeviceSynchronize();

  //init cuda streams
  for (int i = 0; i < numInterps + 1; i++)
  {
    cudaStreamCreate(&refinementStreams[i]);
  }
#endif
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::setupMultilatticeGPU()
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].setupMultilatticeGPU();
  }

  _commDataHandlerPtr = std::make_shared<CommunicationDataHandler<T, Lattice<T>, memory_space::CudaDeviceHeap>>(createCommunicationDataHandler<memory_space::CudaDeviceHeap>(*_domainInfo));
  initalizeCommDataMultilatticeGPU(*_latticePtr, *_commDataHandlerPtr);
  _communicationPtr = std::make_shared<ipcCommunication<T, Lattice<T>>>(*_commDataHandlerPtr);
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::applyMasks()
{
  //recursion
  for (int child = 0; child < children.size(); child++)
  {
    children[child].applyMasks();
  }

  //apply fluid mask where its necessary
  for (int child = 0; child < children.size(); child++)
  {
    size_t fluidMaskLeft = _interpLayerThickness + children[child]._offset[0] + 1;
    size_t fluidMaskRight = _interpLayerThickness + children[child]._offset[0] + children[child]._baseCoarseDimensions[0] - 2;

    size_t fluidMaskBottom = _interpLayerThickness + children[child]._offset[1] + 1;
    size_t fluidMaskTop = _interpLayerThickness + children[child]._offset[1] + children[child]._baseCoarseDimensions[1] - 2;

    for (int iX = fluidMaskLeft; iX <= fluidMaskRight; iX++)
      for (int iY = fluidMaskBottom; iY <= fluidMaskTop; iY++)
      {
        Index3D localIndex;
        if (_localSubDomain.isLocal(iX, iY, 0, localIndex))
        {
          _latticePtr->defineDynamics(localIndex[0], localIndex[0], localIndex[1], localIndex[1], &instances::getBounceBack<T, Lattice>());
        }
      }

    //check border of current grid; if interior point, need to apply fluid mask
    for (int interpIndex = 0; interpIndex < refinementutil::getF2CCoarseLength2D(children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1]); interpIndex++)
    {

      size_t coarseIndices[2];
      refinementutil::getF2CCoarseIndices2D(interpIndex, children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1], children[child]._offset[0], children[child]._offset[1], _interpLayerThickness, coarseIndices);
      bool interior = checkCoarsePointIsInterior(coarseIndices, child);

      if (interior)
      {
        Index3D localIndex;
        if (_localSubDomain.isLocal(coarseIndices[0], coarseIndices[1], 0, localIndex))
        {
          _latticePtr->defineDynamics(localIndex[0], localIndex[0], localIndex[1], localIndex[1], &instances::getBounceBack<T, Lattice>());
        }
      }
    }
  }
  
  initGhostLayer(_localSubDomain, *_latticePtr);
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::initDataArrays()
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].initDataArrays();
  }

  _latticePtr->initDataArrays();
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::iniEquilibrium(T rho, T vel[2])
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].iniEquilibrium(rho, vel);
  }

  for (size_t iX = 0; iX < _localSubDomain.localGridSize()[0]; iX++)
    for (size_t iY = 0; iY < _localSubDomain.localGridSize()[1]; iY++)
    {
      // iniEquilibrium
      _latticePtr->defineRhoU(iX, iX, iY, iY, rho, vel);
      _latticePtr->iniEquilibrium(iX, iX, iY, iY, rho, vel);
    }
}

template <typename T, template <typename U> class Lattice>
BlockLattice2D<T, Lattice> *RefinedGrid2D<T, Lattice>::getLatticePointer()
{
  return _latticePtr.get();
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::copyLatticesToCPU()
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].copyLatticesToCPU();
  }
  _latticePtr->copyDataToCPU();
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::copyLatticesToGPU()
{
  for (int child = 0; child < children.size(); child++)
  {
    children[child].copyLatticesToGPU();
  }
  _latticePtr->copyDataToGPU();
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T, Lattice>::calculateInterpolationIndices()
{
  std::vector<size_t> F2CincomingOffsets(_localSubDomain.noSubDomains, 0);
  std::vector<size_t> C2FincomingOffsets(_localSubDomain.noSubDomains, 0);
  std::vector<size_t> C2FinterpOffsets(_localSubDomain.noSubDomains, 0);
  std::vector<size_t> F2FincomingOffsets(_localSubDomain.noSubDomains, 0);

  int thisSubDomain = _localSubDomain.localSubDomain;

  std::vector<size_t> previousChildNonLocalOutBufferLengths(_localSubDomain.noSubDomains); // used for calculating nonlocal interpolation indices
  for (int child = 0; child < children.size(); child++)
  {
    //set up F2C indices
    for (int interpIndex = 0; interpIndex < refinementutil::getF2CCoarseLength2D(children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1]); interpIndex++)
    {

      size_t coarseIndices[2];
      size_t fineIndices[2];
      refinementutil::getF2CCoarseIndices2D(interpIndex, children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1], children[child]._offset[0], children[child]._offset[1], _interpLayerThickness, coarseIndices);
      refinementutil::getFineIndicesFromCoarseIndices2D(coarseIndices[0], coarseIndices[1], children[child]._offset[0], children[child]._offset[1], children[child]._interpLayerThickness, _interpLayerThickness, fineIndices);

      size_t fineSubDomainID = children[child]._domainInfo->getSubDomainNo(fineIndices[0], fineIndices[1], 0);
      size_t coarseSubDomainID = _domainInfo->getSubDomainNo(coarseIndices[0], coarseIndices[1], 0);
      size_t coarseIndex = _domainInfo->getCellIdx(coarseIndices[0], coarseIndices[1], 0);
      size_t fineIndex = children[child]._domainInfo->getCellIdx(fineIndices[0], fineIndices[1], 0);

      //following checks are in case we have neighboring grids
      bool interior = checkCoarsePointIsInterior(coarseIndices, child);

      if (interior)
      {
        // std::cout << "F2C index " << interpIndex << " at coarse location (" << coarseIndices[0] << ", " << coarseIndices[1] << ") is interior. continuing" << std::endl;
        continue;
      }

      Index3D localIndex;
      if (coarseSubDomainID == thisSubDomain)
      {
        F2CindicesC.push_back(coarseIndex);
        F2CindicesF.push_back(F2CincomingOffsets[fineSubDomainID]);
        F2CGridIDs.push_back(fineSubDomainID);
      }
      else if (_domainInfo->getLocalInfo().isLocalToGhostLayer(coarseIndices[0], coarseIndices[1], 0, localIndex)) {
        F2CindicesC.push_back(util::getCellIndex2D(localIndex[0], localIndex[1], _domainInfo->getLocalInfo().localGridSize()[1]));
        F2CindicesF.push_back(F2CincomingOffsets[fineSubDomainID]);
        F2CGridIDs.push_back(fineSubDomainID);
      }
      F2CincomingOffsets[fineSubDomainID]++;
      if (fineSubDomainID == thisSubDomain)
      {
        F2COutBufferIndicesF.push_back(fineIndex);
        F2COutBufferChildIDs.push_back(child);
      }
    }

    //set up C2F indices, and also F2F direct indices

    //on these cells, fine cells are coincident with coarse cells.
    for (int interpIndex = 0; interpIndex < refinementutil::getC2FCoarseLength2D(children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1], children[child]._interpLayerThickness); interpIndex++)
    {
      size_t coarseIndices[2];
      size_t fineIndices[2];
      refinementutil::getC2FCoarseIndices2D(interpIndex, children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1], children[child]._offset[0], children[child]._offset[1], children[child]._interpLayerThickness, _interpLayerThickness, coarseIndices);
      refinementutil::getFineIndicesFromCoarseIndices2D(coarseIndices[0], coarseIndices[1], children[child]._offset[0], children[child]._offset[1], children[child]._interpLayerThickness, _interpLayerThickness, fineIndices);

      size_t fineSubDomainID = children[child]._domainInfo->getSubDomainNo(fineIndices[0], fineIndices[1], 0);
      size_t coarseSubDomainID = _domainInfo->getSubDomainNo(coarseIndices[0], coarseIndices[1], 0);
      size_t coarseIndex = _domainInfo->getCellIdx(coarseIndices[0], coarseIndices[1], 0);
      size_t fineIndex = children[child]._domainInfo->getCellIdx(fineIndices[0], fineIndices[1], 0);

      //check for direct communication F2F
      bool F2FDirect = processF2F(child, fineIndices, F2FindicesSource, F2FSourceGridIDs, F2FindicesDest, F2FDestChildIDs, F2FincomingOffsets, F2FOutBufferIndicesF, F2FOutBufferChildIDs);
      if (F2FDirect)
        continue;

      if (fineSubDomainID == thisSubDomain)
      {
        C2FindicesFCoarseDirect.push_back(fineIndex);
        C2FindicesCCoarseDirect.push_back(C2FincomingOffsets[coarseSubDomainID]);
        C2FCoarseDirectFineChildIDs.push_back(child);
        C2FCoarseDirectCoarseGridIDs.push_back(coarseSubDomainID);
      }
      C2FincomingOffsets[coarseSubDomainID]++;
      if (coarseSubDomainID == thisSubDomain)
      {
        C2FOutBufferIndicesC.push_back(coarseIndex);
      }
    }

    //on these cells, fine cells are in between coarse cells, so an interp is needed.
    std::vector<std::vector<size_t>> thisChildNonLocalOutBuffers(_localSubDomain.noSubDomains);
    for (int interpIndex = 0; interpIndex < refinementutil::getC2FFineLength2D(children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1], children[child]._interpLayerThickness); interpIndex++)
    {
      size_t fineIndices[2];
      refinementutil::getC2FFineIndices2D(interpIndex, children[child]._baseCoarseDimensions[0], children[child]._baseCoarseDimensions[1], children[child]._interpLayerThickness, fineIndices);

      bool xEven = fineIndices[0] % 2 == 0;
      bool yEven = fineIndices[1] % 2 == 0;

      if (xEven && yEven)
        continue; // these points are coincident so we need to skip them.

      //check for direct communication F2F
      bool F2FDirect = processF2F(child, fineIndices, F2FindicesSource, F2FSourceGridIDs, F2FindicesDest, F2FDestChildIDs, F2FincomingOffsets, F2FOutBufferIndicesF, F2FOutBufferChildIDs);
      if (F2FDirect)
        continue;

      Index3D fineLocalIndex;
      //X Single Interp
      if (!xEven && yEven) { 
        if (fineIndices[0] == 1) { //negative corner
          if (children[child]._localSubDomain.isLocal(fineIndices[0], fineIndices[1], 0, fineLocalIndex)) {
            size_t fineIndex = util::getCellIndex2D(fineLocalIndex[0], fineLocalIndex[1], children[child]._domainInfo->subDomains[thisSubDomain].localGridSize()[1]);
            C2FindicesFInterp.push_back(fineIndex);
            C2FInterpChildIDs.push_back(child);
            C2FInterpAxis.push_back(0);
            C2FInterpType.push_back(-1);
          }
        }
        else if (fineIndices[0] == children[child]._refSubDomain.globalIndexEnd[0] - 1 - 1) { //positive corner
          if (children[child]._localSubDomain.isLocal(fineIndices[0], fineIndices[1], 0, fineLocalIndex)) {
            size_t fineIndex = util::getCellIndex2D(fineLocalIndex[0], fineLocalIndex[1], children[child]._domainInfo->subDomains[thisSubDomain].localGridSize()[1]);
            C2FindicesFInterp.push_back(fineIndex);
            C2FInterpChildIDs.push_back(child);
            C2FInterpAxis.push_back(0);
            C2FInterpType.push_back(1);
          }
        }
        else { //standard
          processStandardInterp(0, fineIndices, child, C2FindicesFInterp, C2FInterpChildIDs, C2FInterpAxis, C2FInterpType, 
                                                                                               C2FindicesFInterpNonLocal, C2FInterpNonLocalChildIDs, C2FindicesFInterpNonLocalNeighbor1, C2FindicesFInterpNonLocalNeighbor1GridIDs,
                                                                                                                                                     C2FindicesFInterpNonLocalNeighbor2, C2FindicesFInterpNonLocalNeighbor2GridIDs,
                                                                                                                                                     C2FindicesFInterpNonLocalNeighbor3, C2FindicesFInterpNonLocalNeighbor3GridIDs,
                                                                                                                                                     C2FindicesFInterpNonLocalNeighbor4, C2FindicesFInterpNonLocalNeighbor4GridIDs,
                                                                                                                                                     C2FinterpOffsets, thisChildNonLocalOutBuffers, previousChildNonLocalOutBufferLengths);
        }
      }
      //Y Single Interp
      if (xEven && !yEven) { 
        if (fineIndices[1] == 1) { //negative corner
          if (children[child]._localSubDomain.isLocal(fineIndices[0], fineIndices[1], 0, fineLocalIndex)) {
            size_t fineIndex = util::getCellIndex2D(fineLocalIndex[0], fineLocalIndex[1], children[child]._domainInfo->subDomains[thisSubDomain].localGridSize()[1]);
            C2FindicesFInterp.push_back(fineIndex);
            C2FInterpChildIDs.push_back(child);
            C2FInterpAxis.push_back(1);
            C2FInterpType.push_back(-1);
          }
        }
        else if (fineIndices[1] == children[child]._refSubDomain.globalIndexEnd[1] - 1 - 1) { //positive corner
          if (children[child]._localSubDomain.isLocal(fineIndices[0], fineIndices[1], 0, fineLocalIndex)) {
            size_t fineIndex = util::getCellIndex2D(fineLocalIndex[0], fineLocalIndex[1], children[child]._domainInfo->subDomains[thisSubDomain].localGridSize()[1]);
            C2FindicesFInterp.push_back(fineIndex);
            C2FInterpChildIDs.push_back(child);
            C2FInterpAxis.push_back(1);
            C2FInterpType.push_back(1);
          }
        }
        else { //standard

          processStandardInterp(1, fineIndices, child, C2FindicesFInterp, C2FInterpChildIDs, C2FInterpAxis, C2FInterpType, 
                                                                                               C2FindicesFInterpNonLocal, C2FInterpNonLocalChildIDs, C2FindicesFInterpNonLocalNeighbor1, C2FindicesFInterpNonLocalNeighbor1GridIDs,
                                                                                                                                                     C2FindicesFInterpNonLocalNeighbor2, C2FindicesFInterpNonLocalNeighbor2GridIDs,
                                                                                                                                                     C2FindicesFInterpNonLocalNeighbor3, C2FindicesFInterpNonLocalNeighbor3GridIDs,
                                                                                                                                                     C2FindicesFInterpNonLocalNeighbor4, C2FindicesFInterpNonLocalNeighbor4GridIDs,
                                                                                                                                                     C2FinterpOffsets, thisChildNonLocalOutBuffers, previousChildNonLocalOutBufferLengths);
        }
      }
    }

    //add this GPUs' nonlocal interp buffers to the relevant vector
    for (int index = 0; index < thisChildNonLocalOutBuffers.at(thisSubDomain).size(); index++)
    {
      C2FOutBufferIndicesInterpNonLocal.push_back(thisChildNonLocalOutBuffers.at(thisSubDomain).at(index));
      C2FOutBufferIndicesInterpNonLocalChildIDs.push_back(child);
    }
    for (int grid = 0; grid < _localSubDomain.noSubDomains; grid++)
    {
      previousChildNonLocalOutBufferLengths.at(grid) += thisChildNonLocalOutBuffers.at(grid).size();
    }
  }

  //shared memory F2C outbuffer write
  processSharedF2COutBufferWrite();
}

template <typename T, template <typename U> class Lattice>
bool RefinedGrid2D<T,Lattice>::checkCoarsePointIsInterior(size_t coarseIndices[2], int child) {
  bool interior = false;
  for (int sChild = 0; sChild < children.size(); sChild++)
  { // loop over children at higher index values
    if (sChild == child)
      continue;

    if (refinementutil::isCoarsePointContainedInFineCoreArea2D(coarseIndices[0], coarseIndices[1],
                                                               children[sChild]._baseCoarseDimensions[0], children[sChild]._baseCoarseDimensions[1], children[sChild]._offset[0], children[sChild]._offset[1], _interpLayerThickness))
    { // if this point is shared

      // std::cout << "F2C index " << interpIndex << " at coarse location (" << coarseIndices[0] << ", " << coarseIndices[1] << ") is shared with child " << sChild
      //           << ". Checking neighbors to see if this is an interior point." << std::endl;

      interior = true; // assume the point is interior for now.
      for (int dir = 1; dir < Lattice<T>::q; dir++)
      { // loop over neighbors
        size_t currentNeighborIndices[2];
        for (int iD = 0; iD < Lattice<T>::d; iD++)
        {
          currentNeighborIndices[iD] = coarseIndices[iD] + Lattice<T>::c(dir, iD);
        }

        bool currentNeighborInterior = false;
        for (int iChild = 0; iChild < children.size(); iChild++)
        {
          if (refinementutil::isCoarsePointContainedInFineCoreArea2D(currentNeighborIndices[0], currentNeighborIndices[1],
                                                                     children[iChild]._baseCoarseDimensions[0], children[iChild]._baseCoarseDimensions[1], children[iChild]._offset[0], children[iChild]._offset[1], _interpLayerThickness))
          {

            currentNeighborInterior = true;
            break;
          }
        }

        if (!currentNeighborInterior)
        {
          interior = false;
          break;
        }
      }

      break;
    }
  }
  return interior;
}

template <typename T, template <typename U> class Lattice>
bool RefinedGrid2D<T,Lattice>::processF2F(int child, size_t fineIndices[2], std::vector<size_t>& sourceIndices, std::vector<int>& sourceGridIDs, std::vector<size_t>& destIndices, std::vector<int>& destChildIDs, 
                                          std::vector<size_t>& incomingOffsets, std::vector<size_t>& outBuffer, std::vector<int>& outBufferChildIDs) {
  bool F2FDirect = false;
  int thisSubDomain = _localSubDomain.localSubDomain;

  for (int iChild = 0; iChild < children.size(); iChild++)
  {
    size_t compIndices[2];
    if (refinementutil::isFinePointContainedInFineCoreArea2D(fineIndices[0], fineIndices[1], children[child]._offset[0], children[child]._offset[1],
                                                              children[iChild]._baseCoarseDimensions[0], children[iChild]._baseCoarseDimensions[1], children[iChild]._offset[0], children[iChild]._offset[1], children[child]._interpLayerThickness, children[iChild]._interpLayerThickness, compIndices))
    {

      F2FDirect = true;

      // std::cout << "Point (" << fineIndices[0] <<", " << fineIndices[1] <<") on child " << child << " can directly communicate with point (" <<
      //      compIndices[0] <<", " << compIndices[1] <<") on child " << iChild << std::endl;

      size_t destSubDomainID = children[child]._domainInfo->getSubDomainNo(fineIndices[0], fineIndices[1], 0);
      size_t sourceSubDomainID = children[iChild]._domainInfo->getSubDomainNo(compIndices[0], compIndices[1], 0);
      size_t destIndex = children[child]._domainInfo->getCellIdx(fineIndices[0], fineIndices[1], 0);
      size_t sourceIndex = children[iChild]._domainInfo->getCellIdx(compIndices[0], compIndices[1], 0);

      if (destSubDomainID == thisSubDomain)
      {
        sourceIndices.push_back(incomingOffsets[sourceSubDomainID]);
        destIndices.push_back(destIndex);
        destChildIDs.push_back(child);
        sourceGridIDs.push_back(sourceSubDomainID);
      }
      incomingOffsets[sourceSubDomainID]++;
      if (sourceSubDomainID == thisSubDomain)
      {
        outBuffer.push_back(sourceIndex);
        outBufferChildIDs.push_back(iChild);
      }
      break;
    }
  }
  return F2FDirect;
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T,Lattice>::processStandardInterp(int axis, size_t fineIndices[2], size_t child, std::vector<size_t>& localInterp, std::vector<int>& localChildIDs, std::vector<int>& localAxis, std::vector<int>& localType,
                                                                    std::vector<size_t>& nonlocalInterp, std::vector<int>& nonlocalChildIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor1, std::vector<int>& nonlocalNeighbor1GridIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor2, std::vector<int>& nonlocalNeighbor2GridIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor3, std::vector<int>& nonlocalNeighbor3GridIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor4, std::vector<int>& nonlocalNeighbor4GridIDs, 
                                                                    std::vector<size_t>& interpOffsets, std::vector<std::vector<size_t>>& nonlocalOutBuffers, std::vector<size_t>& previousChildNonLocalOutBufferLengths) {
  int thisSubDomain = _localSubDomain.localSubDomain;

  size_t neighbor1Indices[2] = {fineIndices[0], fineIndices[1]};
  size_t neighbor2Indices[2] = {fineIndices[0], fineIndices[1]};
  size_t neighbor3Indices[2] = {fineIndices[0], fineIndices[1]};
  size_t neighbor4Indices[2] = {fineIndices[0], fineIndices[1]};

  neighbor1Indices[axis] -= 3;
  neighbor2Indices[axis] -= 1;
  neighbor3Indices[axis] += 1;
  neighbor4Indices[axis] += 3;

  size_t interpPointSubDomainID = children[child]._domainInfo->getSubDomainNo(fineIndices[0], fineIndices[1], 0);
  size_t neighbor1SubDomainID = children[child]._domainInfo->getSubDomainNo(neighbor1Indices[0], neighbor1Indices[1], 0);
  size_t neighbor2SubDomainID = children[child]._domainInfo->getSubDomainNo(neighbor2Indices[0], neighbor2Indices[1], 0);
  size_t neighbor3SubDomainID = children[child]._domainInfo->getSubDomainNo(neighbor3Indices[0], neighbor3Indices[1], 0);
  size_t neighbor4SubDomainID = children[child]._domainInfo->getSubDomainNo(neighbor4Indices[0], neighbor4Indices[1], 0);

  size_t interpPointCellIndex = children[child]._domainInfo->getCellIdx(fineIndices[0], fineIndices[1], 0);
  size_t neighbor1CellIndex = children[child]._domainInfo->getCellIdx(neighbor1Indices[0], neighbor1Indices[1], 0);
  size_t neighbor2CellIndex = children[child]._domainInfo->getCellIdx(neighbor2Indices[0], neighbor2Indices[1], 0);
  size_t neighbor3CellIndex = children[child]._domainInfo->getCellIdx(neighbor3Indices[0], neighbor3Indices[1], 0);
  size_t neighbor4CellIndex = children[child]._domainInfo->getCellIdx(neighbor4Indices[0], neighbor4Indices[1], 0);

  bool loadNeighbor1 = true;
  bool loadNeighbor2 = true;
  bool loadNeighbor3 = true;
  bool loadNeighbor4 = true;

  if (interpPointSubDomainID == thisSubDomain && neighbor1SubDomainID == thisSubDomain && neighbor2SubDomainID == thisSubDomain && neighbor3SubDomainID == thisSubDomain && neighbor4SubDomainID == thisSubDomain)
  { //local interpolation on this GPU
    localInterp.push_back(interpPointCellIndex);
    localChildIDs.push_back(child);
    localAxis.push_back(axis);
    localType.push_back(0);
  }
  else if (interpPointSubDomainID == neighbor1SubDomainID && interpPointSubDomainID == neighbor2SubDomainID && interpPointSubDomainID == neighbor3SubDomainID && interpPointSubDomainID == neighbor4SubDomainID)
  { //local interpolation on another GPU
    //do nothing
  }
  else
  {
    //potential nonlocal interpolations
    if (interpPointSubDomainID == thisSubDomain)
    {
      nonlocalInterp.push_back(interpPointCellIndex);
      nonlocalChildIDs.push_back(child);

      auto neighbor1Itr = std::find(nonlocalOutBuffers.at(neighbor1SubDomainID).begin(), nonlocalOutBuffers.at(neighbor1SubDomainID).end(), neighbor1CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor1Itr != nonlocalOutBuffers.at(neighbor1SubDomainID).cend())
      {
        size_t indexOffset = std::distance(nonlocalOutBuffers.at(neighbor1SubDomainID).begin(), neighbor1Itr) + previousChildNonLocalOutBufferLengths.at(neighbor1SubDomainID);
        nonlocalNeighbor1.push_back(indexOffset);
        nonlocalNeighbor1GridIDs.push_back(neighbor1SubDomainID);
        loadNeighbor1 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        nonlocalNeighbor1.push_back(interpOffsets[neighbor1SubDomainID]);
        nonlocalNeighbor1GridIDs.push_back(neighbor1SubDomainID);
        interpOffsets[neighbor1SubDomainID]++;
      }

      auto neighbor2Itr = std::find(nonlocalOutBuffers.at(neighbor2SubDomainID).begin(), nonlocalOutBuffers.at(neighbor2SubDomainID).end(), neighbor2CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor2Itr != nonlocalOutBuffers.at(neighbor2SubDomainID).cend())
      {
        size_t indexOffset = std::distance(nonlocalOutBuffers.at(neighbor2SubDomainID).begin(), neighbor2Itr) + previousChildNonLocalOutBufferLengths.at(neighbor2SubDomainID);
        nonlocalNeighbor2.push_back(indexOffset);
        nonlocalNeighbor2GridIDs.push_back(neighbor2SubDomainID);
        loadNeighbor2 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        nonlocalNeighbor2.push_back(interpOffsets[neighbor2SubDomainID]);
        nonlocalNeighbor2GridIDs.push_back(neighbor2SubDomainID);
        interpOffsets[neighbor2SubDomainID]++;
      }

      auto neighbor3Itr = std::find(nonlocalOutBuffers.at(neighbor3SubDomainID).begin(), nonlocalOutBuffers.at(neighbor3SubDomainID).end(), neighbor3CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor3Itr != nonlocalOutBuffers.at(neighbor3SubDomainID).cend())
      {
        size_t indexOffset = std::distance(nonlocalOutBuffers.at(neighbor3SubDomainID).begin(), neighbor3Itr) + previousChildNonLocalOutBufferLengths.at(neighbor3SubDomainID);
        nonlocalNeighbor3.push_back(indexOffset);
        nonlocalNeighbor3GridIDs.push_back(neighbor3SubDomainID);
        loadNeighbor3 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        nonlocalNeighbor3.push_back(interpOffsets[neighbor3SubDomainID]);
        nonlocalNeighbor3GridIDs.push_back(neighbor3SubDomainID);
        interpOffsets[neighbor3SubDomainID]++;
      }

      auto neighbor4Itr = std::find(nonlocalOutBuffers.at(neighbor4SubDomainID).begin(), nonlocalOutBuffers.at(neighbor4SubDomainID).end(), neighbor4CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor4Itr != nonlocalOutBuffers.at(neighbor4SubDomainID).cend())
      {
        size_t indexOffset = std::distance(nonlocalOutBuffers.at(neighbor4SubDomainID).begin(), neighbor4Itr) + previousChildNonLocalOutBufferLengths.at(neighbor4SubDomainID);
        nonlocalNeighbor4.push_back(indexOffset);
        nonlocalNeighbor4GridIDs.push_back(neighbor4SubDomainID);
        loadNeighbor4 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        nonlocalNeighbor4.push_back(interpOffsets[neighbor4SubDomainID]);
        nonlocalNeighbor4GridIDs.push_back(neighbor4SubDomainID);
        interpOffsets[neighbor4SubDomainID]++;
      }
    }
    else
    {
      auto neighbor1Itr = std::find(nonlocalOutBuffers.at(neighbor1SubDomainID).begin(), nonlocalOutBuffers.at(neighbor1SubDomainID).end(), neighbor1CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor1Itr != nonlocalOutBuffers.at(neighbor1SubDomainID).cend())
      {
        loadNeighbor1 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        interpOffsets[neighbor1SubDomainID]++;
      }

      auto neighbor2Itr = std::find(nonlocalOutBuffers.at(neighbor2SubDomainID).begin(), nonlocalOutBuffers.at(neighbor2SubDomainID).end(), neighbor2CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor2Itr != nonlocalOutBuffers.at(neighbor2SubDomainID).cend())
      {
        loadNeighbor2 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        interpOffsets[neighbor2SubDomainID]++;
      }

      auto neighbor3Itr = std::find(nonlocalOutBuffers.at(neighbor3SubDomainID).begin(), nonlocalOutBuffers.at(neighbor3SubDomainID).end(), neighbor3CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor3Itr != nonlocalOutBuffers.at(neighbor3SubDomainID).cend())
      {
        loadNeighbor3 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        interpOffsets[neighbor3SubDomainID]++;
      }

      auto neighbor4Itr = std::find(nonlocalOutBuffers.at(neighbor4SubDomainID).begin(), nonlocalOutBuffers.at(neighbor4SubDomainID).end(), neighbor4CellIndex); //check if a previous nonlocal interpolation has already loaded this neighbor point
      if (neighbor4Itr != nonlocalOutBuffers.at(neighbor4SubDomainID).cend())
      {
        loadNeighbor4 = false;
      }
      else
      { // if not, add the point to the list to be loaded
        interpOffsets[neighbor4SubDomainID]++;
      }
    }

    if (loadNeighbor1)
      nonlocalOutBuffers.at(neighbor1SubDomainID).push_back(neighbor1CellIndex);
    if (loadNeighbor2)
      nonlocalOutBuffers.at(neighbor2SubDomainID).push_back(neighbor2CellIndex);
    if (loadNeighbor3)
      nonlocalOutBuffers.at(neighbor3SubDomainID).push_back(neighbor3CellIndex);
    if (loadNeighbor4)
      nonlocalOutBuffers.at(neighbor4SubDomainID).push_back(neighbor4CellIndex);
  }
}

template <typename T, template <typename U> class Lattice>
void RefinedGrid2D<T,Lattice>::processSharedF2COutBufferWrite() {
  const size_t F2CBlockSize = 128;
  F2COutBufferSharedIndicesF.resize(F2COutBufferIndicesF.size()*Lattice<T>::q);

  for (size_t i = 0; i < F2COutBufferIndicesF.size(); i += F2CBlockSize) {
    std::vector<std::set<size_t>> thisBlockLoads;
    thisBlockLoads.resize(children.size());

    for (int j = i; j < min(i+F2CBlockSize, F2COutBufferIndicesF.size()); j++) {
      int currentChild = F2COutBufferChildIDs.at(j);
      size_t currentF2CIndex = F2COutBufferIndicesF.at(j);
      size_t currentF2CIndices[Lattice<T>::d];
      util::getCellIndices2D(currentF2CIndex, children[currentChild]._domainInfo->getLocalInfo().localGridSize()[1], currentF2CIndices);
      for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
        size_t neighborF2CIndices[Lattice<T>::d];
        for (int iD = 0; iD < Lattice<T>::d; iD++) {
          neighborF2CIndices[iD] = currentF2CIndices[iD] + Lattice<T>::c(iPop, iD);
        }
        size_t neighborF2CIndex = util::getCellIndex2D(neighborF2CIndices[0], neighborF2CIndices[1], children[currentChild]._domainInfo->getLocalInfo().localGridSize()[1]);
        thisBlockLoads.at(currentChild).insert(neighborF2CIndex);
      }
    }

    size_t naiveLoads = (min(i+F2CBlockSize, F2COutBufferIndicesF.size())-i)*Lattice<T>::q;
    size_t sharedLoads = 0;
    size_t currentBlockIndex = i/F2CBlockSize;
    for (auto& currentSet : thisBlockLoads)
      sharedLoads += currentSet.size();
    
    // std::cout << "Naive loads for block " << currentBlockIndex << ": " << naiveLoads << std::endl;
    // std::cout << "Shared loads for block " << currentBlockIndex << ": " << sharedLoads << std::endl;

    size_t numF2CThreads = (F2COutBufferIndicesF.size()/F2CBlockSize + 1)*F2CBlockSize;
    size_t numThreadStrides = max((int)sharedLoads/(int)F2CBlockSize + 1, 7);

    if (F2COutBufferLoadIndicesF.size() < numF2CThreads*numThreadStrides) {
      F2COutBufferLoadIndicesF.resize(numF2CThreads*numThreadStrides);
      F2COutBufferLoadChildIDs.resize(numF2CThreads*numThreadStrides);
    }

    size_t currentLoadIndex = 0;
    size_t currentChild = 0;
    
    for (auto& currentSet : thisBlockLoads) {
      for (size_t index : currentSet) {
        size_t currentThreadIndex = currentLoadIndex % F2CBlockSize;
        size_t currentThreadStrideIndex = currentLoadIndex / F2CBlockSize;

        F2COutBufferLoadIndicesF.at(numF2CThreads*currentThreadStrideIndex + currentBlockIndex*F2CBlockSize + currentThreadIndex) = index;
        F2COutBufferLoadChildIDs.at(numF2CThreads*currentThreadStrideIndex + currentBlockIndex*F2CBlockSize + currentThreadIndex) = currentChild;

        currentLoadIndex++;
      }
      currentChild++;
    }

    F2COutBufferLoadsPerThread = std::max(F2COutBufferLoadsPerThread, numThreadStrides);
    // std::cout << F2COutBufferLoadsPerThread << " " << numThreadStrides << std::endl;
    
    for (int j = i; j < min(i+F2CBlockSize, F2COutBufferIndicesF.size()); j++) {
      int currentChild = F2COutBufferChildIDs.at(j);
      size_t currentF2CIndex = F2COutBufferIndicesF.at(j);
      size_t currentF2CIndices[Lattice<T>::d];
      util::getCellIndices2D(currentF2CIndex, children[currentChild]._domainInfo->getLocalInfo().localGridSize()[1], currentF2CIndices);
      for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
        size_t neighborF2CIndices[Lattice<T>::d];
        for (int iD = 0; iD < Lattice<T>::d; iD++) {
          neighborF2CIndices[iD] = currentF2CIndices[iD] + Lattice<T>::c(iPop, iD);
        }
        size_t neighborF2CIndex = util::getCellIndex2D(neighborF2CIndices[0], neighborF2CIndices[1], children[currentChild]._domainInfo->getLocalInfo().localGridSize()[1]);

        size_t neighborSharedF2CIndex = 0;
        
        for (int previousChild = 0; previousChild < currentChild; previousChild++)
          neighborSharedF2CIndex += thisBlockLoads.at(previousChild).size();
        
        neighborSharedF2CIndex += std::distance(thisBlockLoads.at(currentChild).begin(), thisBlockLoads.at(currentChild).find(neighborF2CIndex));

        F2COutBufferSharedIndicesF.at(iPop*F2COutBufferIndicesF.size() + j) = neighborSharedF2CIndex;
      }
    }
  }
  if (F2COutBufferLoadsPerThread > 7)
    std::cout << "WARNING! Loads per thread greater than 15. Shared memory F2C will likely fail..." << std::endl;
}

template <typename T, template <typename U> class Lattice>
size_t RefinedGrid2D<T, Lattice>::assessWorkloadAt(int axis, size_t axisCoord)
{
  std::vector<std::array<size_t, 3>> cutOutLocations;

  for (int child = 0; child < children.size(); child++)
  {
    std::array<size_t, 3> currentCutOut = {_globalOffset[axis] + children[child]._offset[axis] + 1, _globalOffset[axis] + children[child]._offset[axis] + children[child]._baseCoarseDimensions[axis] - 2, children[child]._baseCoarseDimensions[(axis + 1) % 2] - 2};
    cutOutLocations.push_back(currentCutOut);
  }

  if (axisCoord < _globalOffset[axis] - _interpLayerThickness)
  {
    return 0;
  }
  else if (axisCoord > _globalOffset[axis] + _baseDimensions[axis] - 1 + _interpLayerThickness)
  {
    return 0;
  }
  else
  {
    size_t baseWorkAmount = _baseDimensions[(axis + 1) % 2] + 2 * _interpLayerThickness;

    //remove cutouts for child grids
    for (int i = 0; i < cutOutLocations.size(); i++)
    {
      if (axisCoord >= cutOutLocations.at(i).at(0) && axisCoord <= cutOutLocations.at(i).at(1))
        baseWorkAmount -= cutOutLocations.at(i).at(2);
    }

    //add workload of children
    for (int child = 0; child < children.size(); child++)
    {
      size_t currentChildWorkAmount = children[child].assessWorkloadAt(axis, axisCoord * 2) + children[child].assessWorkloadAt(axis, axisCoord * 2 + 1);
      currentChildWorkAmount *= 2; //children have half the timestep, so double the work
      baseWorkAmount += currentChildWorkAmount;
    }
    return baseWorkAmount;
  }
}

template <typename T, template <typename U> class Lattice>
size_t RefinedGrid2D<T, Lattice>::nonRecursiveAssessWorkloadAt(int axis, size_t axisCoord)
{
  std::vector<std::array<size_t, 3>> cutOutLocations;

  for (int child = 0; child < children.size(); child++)
  {
    std::array<size_t, 3> currentCutOut = {_interpLayerThickness + children[child]._offset[axis] + 1, _interpLayerThickness + children[child]._offset[axis] + children[child]._baseCoarseDimensions[axis] - 2, children[child]._baseCoarseDimensions[(axis + 1) % 2] - 2};
    cutOutLocations.push_back(currentCutOut);
  }

  size_t baseWorkAmount = _refSubDomain.coreGridSize()[(axis + 1) % 2];

  //remove cutouts for child grids
  for (int i = 0; i < cutOutLocations.size(); i++)
  {
    if (axisCoord >= cutOutLocations.at(i).at(0) && axisCoord <= cutOutLocations.at(i).at(1))
      baseWorkAmount -= cutOutLocations.at(i).at(2);
  }
  return baseWorkAmount;
}

template <typename T, template <typename U> class Lattice>
template <typename FluidDynamics>
void RefinedGrid2D<T, Lattice>::collideAndStream()
{
  //this function is recursive. The base case is to simply collide and stream the lattice.
  if (children.size() == 0)
  {
    _latticePtr->template collideAndStream<FluidDynamics>();
  }
  else
  {
    if (firstCollideAndStream) {
      //Step 1: write to C2F out buffer
      for (size_t threadIndex = 0; threadIndex < C2FOutBufferIndicesC.size(); threadIndex++)
        refinementutil::C2FWriteToOutBuffer2D<T, Lattice>(_latticePtr->getData(), &C2FOutBufferIndicesC[0], C2FOutBuffer->getCellData(), _omega, _childOmega, threadIndex);

      firstCollideAndStream = false;
    }

    //Step 2: write C2F from out buffers to interp layer
    for (size_t threadIndex = 0; threadIndex < C2FindicesCCoarseDirect.size(); threadIndex++)
      refinementutil::copyC2FCoarseToInterpLayer2D<T, Lattice>(&C2FOutBuffers[0], &C2FindicesCCoarseDirect[0], &C2FCoarseDirectCoarseGridIDs[0], C2FinterpLayer->getCellData(), threadIndex);

    //Step 3: collide and stream coarse and fine lattices
    _latticePtr->template collideAndStream<FluidDynamics>();
    for (size_t child = 0; child < children.size(); child++)
      children[child].template collideAndStream<FluidDynamics>();

    //Step 4: write to C2F out buffer again
    for (size_t threadIndex = 0; threadIndex < C2FOutBufferIndicesC.size(); threadIndex++)
      refinementutil::C2FWriteToOutBuffer2D<T, Lattice>(_latticePtr->getData(), &C2FOutBufferIndicesC[0], C2FOutBuffer->getCellData(), _omega, _childOmega, threadIndex);

    //Step 4.5: write to F2F out buffer
    for (size_t threadIndex = 0; threadIndex < F2FOutBufferIndicesF.size(); threadIndex++)
      refinementutil::F2FWriteToOutBuffer2D<T, Lattice>(&childCellDatasOdd[0], &F2FOutBufferIndicesF[0], &F2FOutBufferChildIDs[0], F2FOutBuffer->getCellData(), threadIndex);

    //Step 5: calculate time interpolated C2F
    for (size_t threadIndex = 0; threadIndex < C2FindicesCCoarseDirect.size(); threadIndex++)
      refinementutil::calculateTimeInterpolatedC2FInterpLayer2D<T, Lattice>(&C2FOutBuffers[0], &C2FindicesCCoarseDirect[0], &C2FCoarseDirectCoarseGridIDs[0], C2FinterpLayer->getCellData(), threadIndex);

    //Step 5.5: F2F
    for (size_t threadIndex = 0; threadIndex < F2FindicesSource.size(); threadIndex++)
      refinementutil::copyF2F2D<T, Lattice>(&childCellDatasOdd[0], &F2FindicesDest[0], &F2FDestChildIDs[0], &F2FOutBuffers[0], &F2FindicesSource[0], &F2FSourceGridIDs[0], threadIndex);

    //Step 6: C2F from interp layer
    for (size_t threadIndex = 0; threadIndex < C2FindicesCCoarseDirect.size(); threadIndex++)
      refinementutil::copyC2FCoarseDirectFromInterpLayer2D<T, Lattice>(C2FinterpLayer->getCellData(), &childCellDatasOdd[0], &C2FindicesFCoarseDirect[0], &C2FCoarseDirectFineChildIDs[0], threadIndex);

    //Step 7: Write to C2F interp out buffer
    for (size_t threadIndex = 0; threadIndex < C2FOutBufferIndicesInterpNonLocal.size(); threadIndex++)
      refinementutil::C2FInterpWriteToOutBuffer2D<T, Lattice>(&childCellDatasOdd[0], &C2FOutBufferIndicesInterpNonLocal[0], &C2FOutBufferIndicesInterpNonLocalChildIDs[0], C2FOutBufferInterpNonLocal->getCellData(), threadIndex);

    //Step 8: C2F interpolated points
    for (size_t threadIndex = 0; threadIndex < C2FindicesFInterp.size(); threadIndex++)
      refinementutil::C2FGeneralInterp2D<T, Lattice>(&childCellDatasOdd[0], &C2FindicesFInterp[0], &C2FInterpChildIDs[0], &C2FInterpAxis[0], &C2FInterpType[0], &childNYs[0], threadIndex);

    for (size_t threadIndex = 0; threadIndex < C2FindicesFInterpNonLocal.size(); threadIndex++)
      refinementutil::C2FNonLocalInterp2D<T, Lattice>(&childCellDatasOdd[0], &C2FindicesFInterpNonLocal[0], &C2FInterpNonLocalChildIDs[0], &C2FInterpNonLocalOutBuffers[0], &C2FindicesFInterpNonLocalNeighbor1[0], &C2FindicesFInterpNonLocalNeighbor1GridIDs[0], &C2FindicesFInterpNonLocalNeighbor2[0], &C2FindicesFInterpNonLocalNeighbor2GridIDs[0],
                                                      &C2FindicesFInterpNonLocalNeighbor3[0], &C2FindicesFInterpNonLocalNeighbor3GridIDs[0], &C2FindicesFInterpNonLocalNeighbor4[0], &C2FindicesFInterpNonLocalNeighbor4GridIDs[0], threadIndex);

    //Step 9: collide and stream fine lattices
    for (size_t child = 0; child < children.size(); child++)
      children[child].template collideAndStream<FluidDynamics>();

    //Step 10: Write to F2C out buffer
    for (size_t threadIndex = 0; threadIndex < F2COutBufferIndicesF.size(); threadIndex++)
      refinementutil::F2CWriteToOutBuffer2D<T, Lattice>(&childCellDatasEven[0], &F2COutBufferIndicesF[0], &F2COutBufferChildIDs[0], F2COutBuffer->getCellData(), _omega, _childOmega, &childNYs[0], threadIndex);

    //Step 10.5: Write to F2F out buffer
    for (size_t threadIndex = 0; threadIndex < F2FOutBufferIndicesF.size(); threadIndex++)
      refinementutil::F2FWriteToOutBuffer2D<T, Lattice>(&childCellDatasEven[0], &F2FOutBufferIndicesF[0], &F2FOutBufferChildIDs[0], F2FOutBuffer->getCellData(), threadIndex);

    //Step 11: F2C
    for (size_t threadIndex = 0; threadIndex < F2CindicesC.size(); threadIndex++)
      refinementutil::copyF2C2D<T, Lattice>(_latticePtr->getData(), &F2CindicesC[0], &F2COutBuffers[0], &F2CindicesF[0], &F2CGridIDs[0], threadIndex);

    //Step 11.5: F2F
    for (size_t threadIndex = 0; threadIndex < F2FindicesSource.size(); threadIndex++)
      refinementutil::copyF2F2D<T, Lattice>(&childCellDatasEven[0], &F2FindicesDest[0], &F2FDestChildIDs[0], &F2FOutBuffers[0], &F2FindicesSource[0], &F2FSourceGridIDs[0], threadIndex);

    //Step 12: C2F
    for (size_t threadIndex = 0; threadIndex < C2FindicesCCoarseDirect.size(); threadIndex++) //update for multiple children!!
      refinementutil::copyC2FCoarseDirect2D<T, Lattice>(&C2FOutBuffers[0], &C2FindicesCCoarseDirect[0], &C2FCoarseDirectCoarseGridIDs[0], &childCellDatasEven[0], &C2FindicesFCoarseDirect[0], &C2FCoarseDirectFineChildIDs[0], threadIndex);

    //Step 13: Write to C2F interp out buffer
    for (size_t threadIndex = 0; threadIndex < C2FOutBufferIndicesInterpNonLocal.size(); threadIndex++)
      refinementutil::C2FInterpWriteToOutBuffer2D<T, Lattice>(&childCellDatasEven[0], &C2FOutBufferIndicesInterpNonLocal[0], &C2FOutBufferIndicesInterpNonLocalChildIDs[0], C2FOutBufferInterpNonLocal->getCellData(), threadIndex);

    //Step 14: C2F interpolated points
    for (size_t threadIndex = 0; threadIndex < C2FindicesFInterp.size(); threadIndex++)
      refinementutil::C2FGeneralInterp2D<T, Lattice>(&childCellDatasEven[0], &C2FindicesFInterp[0], &C2FInterpChildIDs[0], &C2FInterpAxis[0], &C2FInterpType[0], &childNYs[0], threadIndex);

    for (size_t threadIndex = 0; threadIndex < C2FindicesFInterpNonLocal.size(); threadIndex++)
      refinementutil::C2FNonLocalInterp2D<T, Lattice>(&childCellDatasEven[0], &C2FindicesFInterpNonLocal[0], &C2FInterpNonLocalChildIDs[0], &C2FInterpNonLocalOutBuffers[0], &C2FindicesFInterpNonLocalNeighbor1[0], &C2FindicesFInterpNonLocalNeighbor1GridIDs[0], &C2FindicesFInterpNonLocalNeighbor2[0], &C2FindicesFInterpNonLocalNeighbor2GridIDs[0],
                                                      &C2FindicesFInterpNonLocalNeighbor3[0], &C2FindicesFInterpNonLocalNeighbor3GridIDs[0], &C2FindicesFInterpNonLocalNeighbor4[0], &C2FindicesFInterpNonLocalNeighbor4GridIDs[0], threadIndex);
  }
}

template <typename T, template <typename U> class Lattice>
template <typename FluidDynamics>
void RefinedGrid2D<T, Lattice>::collideAndStreamGPU()
{
#ifdef __CUDACC__
  //this function is recursive. The base case is to simply collide and stream the lattice.
  if (children.size() == 0)
  {
    _latticePtr->template collideAndStreamGPU<FluidDynamics>();
  }
  else
  {
    if (firstCollideAndStream) {
      if (gpuC2FOutBufferSize > 0)
      {
        dim3 threadsPerBlock(256, 1, 1);
        dim3 numBlocks(gpuC2FOutBufferSize / 256 + 1, 1, 1);
        refinementutil::C2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(_latticePtr->getDataGPU(), gpuC2FOutBufferIndicesC->get(), C2FOutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuC2FOutBufferSize);
      }

      firstCollideAndStream = false;
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::copyC2FCoarseToInterpLayerKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuC2FOutBuffers->get(), gpuC2FindicesCCoarseDirect->get(), gpuC2FCoarseDirectCoarseGridIDs->get(), C2FinterpLayer->gpuGetFluidData(), gpuC2FCoarseDirectSize);
    }

    cudaDeviceSynchronize();
    for (size_t child = 0; child < children.size(); child++)
      children[child].template collideAndStreamGPU<FluidDynamics>();
    _latticePtr->template collideAndStreamGPU<FluidDynamics>();
    cudaDeviceSynchronize(); //sync device after Async collide and streams

    if (gpuC2FOutBufferSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FOutBufferSize / 256 + 1, 1, 1);
      refinementutil::C2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(_latticePtr->getDataGPU(), gpuC2FOutBufferIndicesC->get(), C2FOutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuC2FOutBufferSize);
    }

    if (gpuF2FOutBufferSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FOutBufferSize / 256 + 1, 1, 1);
      refinementutil::F2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasOdd->get(), gpuF2FOutBufferIndicesF->get(), gpuF2FOutBufferChildIDs->get(), F2FOutBuffer->gpuGetFluidData(), gpuF2FOutBufferSize);
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::calculateTimeInterpolatedC2FInterpLayerKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuC2FOutBuffers->get(), gpuC2FindicesCCoarseDirect->get(), gpuC2FCoarseDirectCoarseGridIDs->get(), C2FinterpLayer->gpuGetFluidData(), gpuC2FCoarseDirectSize);
    }

    if (gpuF2FSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FSize / 256 + 1, 1, 1);
      refinementutil::copyF2FKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasOdd->get(), gpuF2FindicesDest->get(), gpuF2FDestChildIDs->get(), gpuF2FOutBuffers->get(), gpuF2FindicesSource->get(), gpuF2FSourceGridIDs->get(), gpuF2FSize);
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::copyC2FCoarseDirectFromInterpLayerKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(C2FinterpLayer->gpuGetFluidData(), gpuChildCellDatasOdd->get(), gpuC2FindicesFCoarseDirect->get(), gpuC2FCoarseDirectFineChildIDs->get(), gpuC2FCoarseDirectSize);
    }

    if (gpuC2FInterpNonLocalOutBuffersSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalOutBuffersSize / 256 + 1, 1, 1);
      refinementutil::C2FInterpWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasOdd->get(), gpuC2FOutBufferIndicesInterpNonLocal->get(), gpuC2FOutBufferIndicesInterpNonLocalChildIDs->get(), C2FOutBufferInterpNonLocal->gpuGetFluidData(), gpuC2FInterpNonLocalOutBuffersSize);
    }
    cudaDeviceSynchronize();

    if (gpuC2FInterpSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpSize / 256 + 1, 1, 1);
      refinementutil::C2FGeneralInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasOdd->get(), gpuC2FindicesFInterp->get(), gpuC2FInterpChildIDs->get(), gpuC2FInterpAxis->get(), gpuC2FInterpType->get(), gpuChildNYs->get(), gpuC2FInterpSize);
    }

    if (gpuC2FInterpNonLocalSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalSize / 256 + 1, 1, 1);
      refinementutil::C2FNonLocalInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[7]>>>(gpuChildCellDatasOdd->get(), gpuC2FindicesFInterpNonLocal->get(), gpuC2FInterpNonLocalChildIDs->get(), gpuC2FInterpNonLocalOutBuffers->get(), gpuC2FindicesFInterpNonLocalNeighbor1->get(), gpuC2FindicesFInterpNonLocalNeighbor1GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor2->get(), gpuC2FindicesFInterpNonLocalNeighbor2GridIDs->get(),
                                                                                                                      gpuC2FindicesFInterpNonLocalNeighbor3->get(), gpuC2FindicesFInterpNonLocalNeighbor3GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor4->get(), gpuC2FindicesFInterpNonLocalNeighbor4GridIDs->get(), gpuC2FInterpNonLocalSize);
    }
    cudaDeviceSynchronize();
    for (size_t child = 0; child < children.size(); child++)
      children[child].template collideAndStreamGPU<FluidDynamics>();
    cudaDeviceSynchronize();

    if (gpuF2COutBufferSize > 0)
    {
      // dim3 threadsPerBlock(256, 1, 1);
      // dim3 numBlocks(gpuF2COutBufferSize / 256 + 1, 1, 1);
      // refinementutil::F2CWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasEven->get(), gpuF2COutBufferIndicesF->get(), gpuF2COutBufferChildIDs->get(), F2COutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuChildNYs->get(), gpuF2COutBufferSize);

      dim3 threadsPerBlock(128, 1, 1);
      dim3 numBlocks(gpuF2COutBufferSize / 128 + 1, 1, 1);
      refinementutil::F2CSharedWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasEven->get(), gpuF2COutBufferLoadIndicesF->get(), gpuF2COutBufferLoadChildIDs->get(), F2COutBufferLoadsPerThread, gpuF2COutBufferSharedIndicesF->get(), 
                                                                                                                            F2COutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuF2COutBufferSize);
    }

    if (gpuF2FOutBufferSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FOutBufferSize / 256 + 1, 1, 1);
      refinementutil::F2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasEven->get(), gpuF2FOutBufferIndicesF->get(), gpuF2FOutBufferChildIDs->get(), F2FOutBuffer->gpuGetFluidData(), gpuF2FOutBufferSize);
    }

    if (gpuF2CSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2CSize / 256 + 1, 1, 1);
      refinementutil::copyF2CKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(_latticePtr->getDataGPU(), gpuF2CindicesC->get(), gpuF2COutBuffers->get(), gpuF2CindicesF->get(), gpuF2CGridIDs->get(), gpuF2CSize);
    }

    if (gpuF2FSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FSize / 256 + 1, 1, 1);
      refinementutil::copyF2FKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasEven->get(), gpuF2FindicesDest->get(), gpuF2FDestChildIDs->get(), gpuF2FOutBuffers->get(), gpuF2FindicesSource->get(), gpuF2FSourceGridIDs->get(), gpuF2FSize);
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::copyC2FCoarseDirectKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuC2FOutBuffers->get(), gpuC2FindicesCCoarseDirect->get(), gpuC2FCoarseDirectCoarseGridIDs->get(), gpuChildCellDatasEven->get(), gpuC2FindicesFCoarseDirect->get(), gpuC2FCoarseDirectFineChildIDs->get(), gpuC2FCoarseDirectSize);
    }

    if (gpuC2FInterpNonLocalOutBuffersSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalOutBuffersSize / 256 + 1, 1, 1);
      refinementutil::C2FInterpWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasEven->get(), gpuC2FOutBufferIndicesInterpNonLocal->get(), gpuC2FOutBufferIndicesInterpNonLocalChildIDs->get(), C2FOutBufferInterpNonLocal->gpuGetFluidData(), gpuC2FInterpNonLocalOutBuffersSize);
    }

    cudaDeviceSynchronize();

    if (gpuC2FInterpSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpSize / 256 + 1, 1, 1);
      refinementutil::C2FGeneralInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasEven->get(), gpuC2FindicesFInterp->get(), gpuC2FInterpChildIDs->get(), gpuC2FInterpAxis->get(), gpuC2FInterpType->get(), gpuChildNYs->get(), gpuC2FInterpSize);
    }

    if (gpuC2FInterpNonLocalSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalSize / 256 + 1, 1, 1);
      refinementutil::C2FNonLocalInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[7]>>>(gpuChildCellDatasEven->get(), gpuC2FindicesFInterpNonLocal->get(), gpuC2FInterpNonLocalChildIDs->get(), gpuC2FInterpNonLocalOutBuffers->get(), gpuC2FindicesFInterpNonLocalNeighbor1->get(), gpuC2FindicesFInterpNonLocalNeighbor1GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor2->get(), gpuC2FindicesFInterpNonLocalNeighbor2GridIDs->get(),
                                                                                                                      gpuC2FindicesFInterpNonLocalNeighbor3->get(), gpuC2FindicesFInterpNonLocalNeighbor3GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor4->get(), gpuC2FindicesFInterpNonLocalNeighbor4GridIDs->get(), gpuC2FInterpNonLocalSize);
    }
    cudaDeviceSynchronize();
  }
#endif
}

template <typename T, template <typename U> class Lattice>
template <typename FluidDynamics>
void RefinedGrid2D<T, Lattice>::collideAndStreamMultilatticeGPU()
{
#ifdef __CUDACC__
  if (children.size() == 0)
  {
    olb::collideAndStreamMultilatticeGPU<FluidDynamics>(*_latticePtr, *_commDataHandlerPtr, *_communicationPtr);
  }
  else
  {
    if (firstCollideAndStream) {
      if (gpuC2FOutBufferSize > 0)
      {
        dim3 threadsPerBlock(256, 1, 1);
        dim3 numBlocks(gpuC2FOutBufferSize / 256 + 1, 1, 1);
        refinementutil::C2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(_latticePtr->getDataGPU(), gpuC2FOutBufferIndicesC->get(), C2FOutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuC2FOutBufferSize);
      }

      firstCollideAndStream = false;

      cudaDeviceSynchronize();
      MPI_Barrier(MPI_COMM_WORLD);
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::copyC2FCoarseToInterpLayerKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuC2FOutBuffers->get(), gpuC2FindicesCCoarseDirect->get(), gpuC2FCoarseDirectCoarseGridIDs->get(), C2FinterpLayer->gpuGetFluidData(), gpuC2FCoarseDirectSize);
    }

    for (size_t child = 0; child < children.size(); child++)
    {
      children[child].template collideAndStreamMultilatticeGPU<FluidDynamics>();
    }
    olb::collideAndStreamMultilatticeGPU<FluidDynamics>(*_latticePtr, *_commDataHandlerPtr, *_communicationPtr);
    MPI_Barrier(MPI_COMM_WORLD);

    if (gpuC2FOutBufferSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FOutBufferSize / 256 + 1, 1, 1);
      refinementutil::C2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(_latticePtr->getDataGPU(), gpuC2FOutBufferIndicesC->get(), C2FOutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuC2FOutBufferSize);
    }

    if (gpuF2FOutBufferSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FOutBufferSize / 256 + 1, 1, 1);
      refinementutil::F2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasOdd->get(), gpuF2FOutBufferIndicesF->get(), gpuF2FOutBufferChildIDs->get(), F2FOutBuffer->gpuGetFluidData(), gpuF2FOutBufferSize);
    }

    cudaDeviceSynchronize();
    MPI_Barrier(MPI_COMM_WORLD);

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::calculateTimeInterpolatedC2FInterpLayerKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuC2FOutBuffers->get(), gpuC2FindicesCCoarseDirect->get(), gpuC2FCoarseDirectCoarseGridIDs->get(), C2FinterpLayer->gpuGetFluidData(), gpuC2FCoarseDirectSize);
    }

    if (gpuF2FSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FSize / 256 + 1, 1, 1);
      refinementutil::copyF2FKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasOdd->get(), gpuF2FindicesDest->get(), gpuF2FDestChildIDs->get(), gpuF2FOutBuffers->get(), gpuF2FindicesSource->get(), gpuF2FSourceGridIDs->get(), gpuF2FSize);
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::copyC2FCoarseDirectFromInterpLayerKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(C2FinterpLayer->gpuGetFluidData(), gpuChildCellDatasOdd->get(), gpuC2FindicesFCoarseDirect->get(), gpuC2FCoarseDirectFineChildIDs->get(), gpuC2FCoarseDirectSize);
    }

    cudaDeviceSynchronize();

    if (gpuC2FInterpNonLocalOutBuffersSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalOutBuffersSize / 256 + 1, 1, 1);
      refinementutil::C2FInterpWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasOdd->get(), gpuC2FOutBufferIndicesInterpNonLocal->get(), gpuC2FOutBufferIndicesInterpNonLocalChildIDs->get(), C2FOutBufferInterpNonLocal->gpuGetFluidData(), gpuC2FInterpNonLocalOutBuffersSize);
    }
    cudaDeviceSynchronize();
    MPI_Barrier(MPI_COMM_WORLD);

    // interp steps on different streams
    if (gpuC2FInterpSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpSize / 256 + 1, 1, 1);
      refinementutil::C2FGeneralInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasOdd->get(), gpuC2FindicesFInterp->get(), gpuC2FInterpChildIDs->get(), gpuC2FInterpAxis->get(), gpuC2FInterpType->get(), gpuChildNYs->get(), gpuC2FInterpSize);
    }

    if (gpuC2FInterpNonLocalSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalSize / 256 + 1, 1, 1);
      refinementutil::C2FNonLocalInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[7]>>>(gpuChildCellDatasOdd->get(), gpuC2FindicesFInterpNonLocal->get(), gpuC2FInterpNonLocalChildIDs->get(), gpuC2FInterpNonLocalOutBuffers->get(), gpuC2FindicesFInterpNonLocalNeighbor1->get(), gpuC2FindicesFInterpNonLocalNeighbor1GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor2->get(), gpuC2FindicesFInterpNonLocalNeighbor2GridIDs->get(),
                                                                                                                      gpuC2FindicesFInterpNonLocalNeighbor3->get(), gpuC2FindicesFInterpNonLocalNeighbor3GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor4->get(), gpuC2FindicesFInterpNonLocalNeighbor4GridIDs->get(), gpuC2FInterpNonLocalSize);
    }

    cudaDeviceSynchronize();
    for (size_t child = 0; child < children.size(); child++)
    {
      children[child].template collideAndStreamMultilatticeGPU<FluidDynamics>();
    }

    if (gpuF2COutBufferSize > 0)
    {
      // dim3 threadsPerBlock(256, 1, 1);
      // dim3 numBlocks(gpuF2COutBufferSize / 256 + 1, 1, 1);
      // refinementutil::F2CWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasEven->get(), gpuF2COutBufferIndicesF->get(), gpuF2COutBufferChildIDs->get(), F2COutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuChildNYs->get(), gpuF2COutBufferSize);

      dim3 threadsPerBlock(128, 1, 1);
      dim3 numBlocks(gpuF2COutBufferSize / 128 + 1, 1, 1);
      refinementutil::F2CSharedWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasEven->get(), gpuF2COutBufferLoadIndicesF->get(), gpuF2COutBufferLoadChildIDs->get(), F2COutBufferLoadsPerThread, gpuF2COutBufferSharedIndicesF->get(), 
                                                                                                                            F2COutBuffer->gpuGetFluidData(), _omega, _childOmega, gpuF2COutBufferSize);
    }

    if (gpuF2FOutBufferSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FOutBufferSize / 256 + 1, 1, 1);
      refinementutil::F2FWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasEven->get(), gpuF2FOutBufferIndicesF->get(), gpuF2FOutBufferChildIDs->get(), F2FOutBuffer->gpuGetFluidData(), gpuF2FOutBufferSize);
    }

    cudaDeviceSynchronize();
    MPI_Barrier(MPI_COMM_WORLD);

    if (gpuF2CSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2CSize / 256 + 1, 1, 1);
      refinementutil::copyF2CKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(_latticePtr->getDataGPU(), gpuF2CindicesC->get(), gpuF2COutBuffers->get(), gpuF2CindicesF->get(), gpuF2CGridIDs->get(), gpuF2CSize);
    }

    if (gpuF2FSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuF2FSize / 256 + 1, 1, 1);
      refinementutil::copyF2FKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasEven->get(), gpuF2FindicesDest->get(), gpuF2FDestChildIDs->get(), gpuF2FOutBuffers->get(), gpuF2FindicesSource->get(), gpuF2FSourceGridIDs->get(), gpuF2FSize);
    }

    if (gpuC2FCoarseDirectSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FCoarseDirectSize / 256 + 1, 1, 1);
      refinementutil::copyC2FCoarseDirectKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuC2FOutBuffers->get(), gpuC2FindicesCCoarseDirect->get(), gpuC2FCoarseDirectCoarseGridIDs->get(), gpuChildCellDatasEven->get(), gpuC2FindicesFCoarseDirect->get(), gpuC2FCoarseDirectFineChildIDs->get(), gpuC2FCoarseDirectSize);
    }

    cudaDeviceSynchronize();

    if (gpuC2FInterpNonLocalOutBuffersSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalOutBuffersSize / 256 + 1, 1, 1);
      refinementutil::C2FInterpWriteToOutBufferKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[0]>>>(gpuChildCellDatasEven->get(), gpuC2FOutBufferIndicesInterpNonLocal->get(), gpuC2FOutBufferIndicesInterpNonLocalChildIDs->get(), C2FOutBufferInterpNonLocal->gpuGetFluidData(), gpuC2FInterpNonLocalOutBuffersSize);
    }
    cudaDeviceSynchronize();
    MPI_Barrier(MPI_COMM_WORLD);

    if (gpuC2FInterpSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpSize / 256 + 1, 1, 1);
      refinementutil::C2FGeneralInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[1]>>>(gpuChildCellDatasEven->get(), gpuC2FindicesFInterp->get(), gpuC2FInterpChildIDs->get(), gpuC2FInterpAxis->get(), gpuC2FInterpType->get(), gpuChildNYs->get(), gpuC2FInterpSize);
    }

    if (gpuC2FInterpNonLocalSize > 0)
    {
      dim3 threadsPerBlock(256, 1, 1);
      dim3 numBlocks(gpuC2FInterpNonLocalSize / 256 + 1, 1, 1);
      refinementutil::C2FNonLocalInterpKernel2D<T, Lattice><<<numBlocks, threadsPerBlock, 0, refinementStreams[7]>>>(gpuChildCellDatasEven->get(), gpuC2FindicesFInterpNonLocal->get(), gpuC2FInterpNonLocalChildIDs->get(), gpuC2FInterpNonLocalOutBuffers->get(), gpuC2FindicesFInterpNonLocalNeighbor1->get(), gpuC2FindicesFInterpNonLocalNeighbor1GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor2->get(), gpuC2FindicesFInterpNonLocalNeighbor2GridIDs->get(),
                                                                                                                      gpuC2FindicesFInterpNonLocalNeighbor3->get(), gpuC2FindicesFInterpNonLocalNeighbor3GridIDs->get(), gpuC2FindicesFInterpNonLocalNeighbor4->get(), gpuC2FindicesFInterpNonLocalNeighbor4GridIDs->get(), gpuC2FInterpNonLocalSize);
    }

    cudaDeviceSynchronize();
    MPI_Barrier(MPI_COMM_WORLD);
  }
#endif
}

} //end namespace olb
#endif