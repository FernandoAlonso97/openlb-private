#ifndef REFINED_GRID_3D_H
#define REFINED_GRID_3D_H

#include "core/config.h"
#include "core/blockLattice3D.h"
#include "core/blockLattice3D.hh"
#include "dynamics/dynamics.h"
#include "dynamics/dynamics.hh"
#include "refinementUtil3D.h"
#include "contrib/domainDecomposition/mpiCommunication.h"
#include "contrib/MemorySpace/memory_spaces.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/communication.h"
#include "contrib/domainDecomposition/cudaIPC.h"
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

namespace olb {

template <typename T, template <typename U> class Lattice>
class RefinedGrid3D
{
public:
  RefinedGrid3D(size_t nx, size_t ny, size_t nz, Dynamics<T, Lattice> *dynamics, T omega, bool isParent = true, size_t offsetX = 0, size_t offsetY = 0, size_t offsetZ = 0, size_t parentGlobalOffsetX = 0, size_t parentGlobalOffsetY = 0, size_t parentGlobalOffsetZ = 0, unsigned interpLayerThickness = 2);

  size_t addChild(Dynamics<T,Lattice> * childDynamics, size_t nx, size_t ny, size_t nz, size_t offsetX, size_t offsetY, size_t offsetZ, unsigned interpLayerThickness = 1);

  void refinedDecomposeDomainAtIndices(std::vector<size_t> splitIndices, unsigned axis, unsigned localSubDomain, unsigned ghostLayer[3]);

  void refinedDecomposeDomainEvenlyAlongAxis(unsigned localSubDomain, unsigned axis, unsigned noSubDomains, unsigned ghostLayer[3], T cutOutWeight = 0.25, bool accountForCutouts = true);

  void setAdjacentToBoundary(bool iXMin, bool iXMax, bool iYMin, bool iYMax, bool iZMin, bool iZMax);

  void setupRefinementCPU();

  void setupRefinementGPU();

  void setupMultilatticeGPU();

  void applyMasks();

  void initDataArrays();

  void iniEquilibrium(T rho, T vel[3]);

  BlockLattice3D<T,Lattice>* getLatticePointer();

  void copyLatticesToCPU();

  void copyVizToCPU(bool copyRho, bool copyU, bool copyForce, bool copyFluidMask);

  void copyLatticesToGPU();

  size_t assessWorkloadAt(int axis, size_t axisCoord);

  size_t nonRecursiveAssessWorkloadAt(int axis, size_t axisCoord, T cutOutWeight);

  size_t calculateTotalLatticeUnits();

  template <typename FluidDynamics>
  void collideAndStream();

  template <typename FluidDynamics>
  void collideAndStreamGPU();

  template <typename FluidDynamics>
  void collideAndStreamMultilatticeGPU();

  static UnitConverter<T,Lattice> getConverterForRefinementLevel(UnitConverter<T,Lattice> baseConverter, int refinementLevel);

  static T getOmegaForRefinementLevel(T baseOmega, int refinementLevel);

  void getTopLevelParentIndices(size_t iX, size_t iY, size_t iZ, T topLevelIndices[3]);

  void registerPostCollideAndStreamFunctor(void (*functor)(int, std::vector<void*>), std::vector<void *> args);

private:
  void calculateInterpolationIndices();

  void processStandardInterp(int axis, size_t fineIndices[3], size_t child, std::vector<size_t>& localInterp, std::vector<int>& localChildIDs, std::vector<int>& localAxis, std::vector<int>& localType,
                                                                    std::vector<size_t>& nonlocalInterp, std::vector<int>& nonlocalChildIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor1, std::vector<int>& nonlocalNeighbor1GridIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor2, std::vector<int>& nonlocalNeighbor2GridIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor3, std::vector<int>& nonlocalNeighbor3GridIDs,
                                                                    std::vector<size_t>& nonlocalNeighbor4, std::vector<int>& nonlocalNeighbor4GridIDs, 
                                                                    std::vector<size_t>& interpOffsets, std::vector<std::vector<size_t>>& nonlocalOutBuffers, std::vector<size_t>& previousChildNonLocalOutBufferLengths);

  bool checkCoarsePointIsInterior(size_t coarseIndices[3], int child);

  bool processF2F(int child, size_t fineIndices[3], std::vector<size_t>& sourceIndices, std::vector<int>& sourceGridIDs, std::vector<size_t>& destIndices, std::vector<int>& destChildIDs, 
                                          std::vector<size_t>& incomingOffsets, std::vector<size_t>& outBuffer, std::vector<int>& outBufferChildIDs);

  void processSharedF2COutBufferWrite();
  
public:
  unsigned _interpLayerThickness = 2;
  bool _isParent = false;
  T _omega;
  T _childOmega;
  unsigned _refinementLevel = 0;
  size_t _offset[3] = {0, 0, 0}; //parent's units
  size_t _globalOffset[3] = {0, 0, 0}; //this units
  size_t _baseDimensions[3] = {0, 0, 0}; //this units
  size_t _baseCoarseDimensions[3] = {0, 0, 0}; //parent's units
  size_t _childID = 0;
  size_t _parentID = 0;
  std::vector<RefinedGrid3D<T, Lattice>> children;
  SubDomainInformation<T, Lattice<T>> _refSubDomain;
  SubDomainInformation<T, Lattice<T>> _localSubDomain;
  
  std::shared_ptr<DomainInformation<T, Lattice<T>>> _domainInfo = {nullptr};

  std::shared_ptr<BlockLattice3D<T, Lattice>> _latticePtr = {nullptr};
  std::shared_ptr<CommunicationDataHandler<T, Lattice<T>, memory_space::CudaDeviceHeap>> _commDataHandlerPtr = {nullptr};
  std::shared_ptr<ipcCommunication<T,Lattice<T>>> _communicationPtr = {nullptr};
  Dynamics<T,Lattice> * _fluidDynamicsPointer = nullptr;

  std::shared_ptr<CellBlockData<T,Lattice>> C2FinterpLayer = {nullptr};

  unsigned decompositionAxis = 0;
  bool firstCollideAndStream = true;

  int thisDomainTimeStep = 0;
  void (*postCollideAndStreamFunctor)(int, std::vector<void *>) = nullptr;
  std::vector<void *> postCollideAndStreamFunctorArgs;

  //used if we refine a region on the edge of a domain
  bool adjacentToBoundary[6] = {false, false, false, false, false, false}; //iXMin, iXMax, iYMin, iYMax, iZMin, iZMax

  std::vector<size_t> F2CindicesF;
  std::vector<size_t> F2CindicesC;
  std::vector<int> F2CGridIDs;

  std::vector<size_t> F2COutBufferIndicesF;
  std::vector<int> F2COutBufferChildIDs;
  std::shared_ptr<CellBlockData<T,Lattice>> F2COutBuffer = {nullptr};
  std::vector<T**> F2COutBuffers;
  std::vector<int> F2COutBufferDoNotFilter;

  std::vector<size_t> F2COutBufferLoadIndicesF;
  std::vector<int> F2COutBufferLoadChildIDs;
  size_t F2COutBufferLoadsPerThread = 0;
  std::vector<size_t> F2COutBufferSharedIndicesF;

  std::vector<size_t> F2FindicesSource;
  std::vector<size_t> F2FindicesDest;
  std::vector<int> F2FSourceGridIDs;
  std::vector<int> F2FDestChildIDs;

  std::vector<size_t> C2FindicesFCoarseDirect;
  std::vector<size_t> C2FindicesCCoarseDirect;
  std::vector<int> C2FCoarseDirectFineChildIDs;
  std::vector<int> C2FCoarseDirectCoarseGridIDs;

  std::vector<size_t> C2FOutBufferIndicesC;
  std::shared_ptr<CellBlockData<T,Lattice>> C2FOutBuffer = {nullptr};
  std::vector<T**> C2FOutBuffers;

  std::vector<size_t> F2FOutBufferIndicesF;
  std::vector<int> F2FOutBufferChildIDs;
  std::shared_ptr<CellBlockData<T,Lattice>> F2FOutBuffer = {nullptr};
  std::vector<T**> F2FOutBuffers;

  std::vector<size_t> C2FindicesFInterp;
  std::vector<int> C2FInterpChildIDs;
  std::vector<int> C2FInterpAxis;
  std::vector<int> C2FInterpType;

  std::vector<size_t> C2FindicesFDoubleInterp;
  std::vector<int> C2FDoubleInterpChildIDs;
  std::vector<int> C2FDoubleInterpAxis;
  std::vector<int> C2FDoubleInterpType;

  std::vector<size_t> C2FindicesFInterpNonLocal;
  std::vector<int> C2FInterpNonLocalChildIDs;
  std::vector<size_t> C2FindicesFInterpNonLocalNeighbor1;
  std::vector<size_t> C2FindicesFInterpNonLocalNeighbor2;
  std::vector<size_t> C2FindicesFInterpNonLocalNeighbor3;
  std::vector<size_t> C2FindicesFInterpNonLocalNeighbor4;
  std::vector<int> C2FindicesFInterpNonLocalNeighbor1GridIDs;
  std::vector<int> C2FindicesFInterpNonLocalNeighbor2GridIDs;
  std::vector<int> C2FindicesFInterpNonLocalNeighbor3GridIDs;
  std::vector<int> C2FindicesFInterpNonLocalNeighbor4GridIDs;

  std::vector<size_t> C2FOutBufferIndicesInterpNonLocal;
  std::vector<int> C2FOutBufferIndicesInterpNonLocalChildIDs;
  std::shared_ptr<CellBlockData<T,Lattice>> C2FOutBufferInterpNonLocal = {nullptr};
  std::vector<T**> C2FInterpNonLocalOutBuffers;

  std::vector<size_t> C2FindicesFDoubleInterpNonLocal;
  std::vector<int> C2FDoubleInterpNonLocalChildIDs;
  std::vector<size_t> C2FindicesFDoubleInterpNonLocalNeighbor1;
  std::vector<size_t> C2FindicesFDoubleInterpNonLocalNeighbor2;
  std::vector<size_t> C2FindicesFDoubleInterpNonLocalNeighbor3;
  std::vector<size_t> C2FindicesFDoubleInterpNonLocalNeighbor4;
  std::vector<int> C2FindicesFDoubleInterpNonLocalNeighbor1GridIDs;
  std::vector<int> C2FindicesFDoubleInterpNonLocalNeighbor2GridIDs;
  std::vector<int> C2FindicesFDoubleInterpNonLocalNeighbor3GridIDs;
  std::vector<int> C2FindicesFDoubleInterpNonLocalNeighbor4GridIDs;

  std::vector<size_t> C2FOutBufferIndicesDoubleInterpNonLocal;
  std::vector<int> C2FOutBufferIndicesDoubleInterpNonLocalChildIDs;
  std::shared_ptr<CellBlockData<T,Lattice>> C2FOutBufferDoubleInterpNonLocal = {nullptr};
  std::vector<T**> C2FDoubleInterpNonLocalOutBuffers;

  std::vector<T**> childCellDatasEven;
  std::vector<T**> childCellDatasOdd;
  std::vector<size_t> childNYs;
  std::vector<size_t> childNZs;

  #ifdef __CUDACC__

  static constexpr int numInterps = 10;
  cudaStream_t refinementStreams[1+numInterps];

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2CindicesF = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2CindicesC = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2CGridIDs = {nullptr};
  size_t gpuF2CSize;

  std::vector<T**> gpuF2COutBuffersVector;
  std::vector<T*> gpuF2COutBufferDataVector;
  std::shared_ptr<memory_space::CudaDeviceHeap<T*>> gpuF2COutBufferData = {nullptr};  
  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuF2COutBuffers = {nullptr};

  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2COutBufferChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2COutBufferIndicesF = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2COutBufferDoNotFilter = {nullptr};
  size_t gpuF2COutBufferSize;

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2COutBufferLoadIndicesF;
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2COutBufferLoadChildIDs;
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2COutBufferSharedIndicesF;

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2FindicesSource = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2FindicesDest = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2FSourceGridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2FDestChildIDs = {nullptr};
  size_t gpuF2FSize;

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFCoarseDirect = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesCCoarseDirect = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FCoarseDirectFineChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FCoarseDirectCoarseGridIDs = {nullptr};
  size_t gpuC2FCoarseDirectSize;
  
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FOutBufferIndicesC = {nullptr};
  size_t gpuC2FOutBufferSize;

  std::vector<T**> gpuF2FOutBuffersVector;
  std::vector<T*> gpuF2FOutBufferDataVector;
  std::shared_ptr<memory_space::CudaDeviceHeap<T*>> gpuF2FOutBufferData = {nullptr};  
  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuF2FOutBuffers = {nullptr};

  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuF2FOutBufferChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuF2FOutBufferIndicesF = {nullptr};
  size_t gpuF2FOutBufferSize;

  std::vector<T**> gpuC2FOutBuffersVector;
  std::vector<T*> gpuC2FOutBufferDataVector;
  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuC2FOutBuffers = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<T*>> gpuC2FOutBufferData = {nullptr};

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFInterp = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FInterpChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FInterpAxis = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FInterpType = {nullptr};
  size_t gpuC2FInterpSize;

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFDoubleInterp = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FDoubleInterpChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FDoubleInterpAxis = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FDoubleInterpType = {nullptr};
  size_t gpuC2FDoubleInterpSize;

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFInterpNonLocal = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FInterpNonLocalChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFInterpNonLocalNeighbor1 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFInterpNonLocalNeighbor2 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFInterpNonLocalNeighbor3 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFInterpNonLocalNeighbor4 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFInterpNonLocalNeighbor1GridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFInterpNonLocalNeighbor2GridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFInterpNonLocalNeighbor3GridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFInterpNonLocalNeighbor4GridIDs = {nullptr};
  size_t gpuC2FInterpNonLocalSize;

  std::vector<T**> gpuC2FInterpNonLocalOutBuffersVector;
  std::vector<T*> gpuC2FInterpNonLocalOutBufferDataVector;
  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuC2FInterpNonLocalOutBuffers = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<T*>> gpuC2FInterpNonLocalOutBufferData = {nullptr};

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FOutBufferIndicesInterpNonLocal = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FOutBufferIndicesInterpNonLocalChildIDs = {nullptr};
  size_t gpuC2FInterpNonLocalOutBuffersSize;

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFDoubleInterpNonLocal = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FDoubleInterpNonLocalChildIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFDoubleInterpNonLocalNeighbor1 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFDoubleInterpNonLocalNeighbor2 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFDoubleInterpNonLocalNeighbor3 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FindicesFDoubleInterpNonLocalNeighbor4 = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFDoubleInterpNonLocalNeighbor1GridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFDoubleInterpNonLocalNeighbor2GridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFDoubleInterpNonLocalNeighbor3GridIDs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FindicesFDoubleInterpNonLocalNeighbor4GridIDs = {nullptr};
  size_t gpuC2FDoubleInterpNonLocalSize;

  std::vector<T**> gpuC2FDoubleInterpNonLocalOutBuffersVector;
  std::vector<T*> gpuC2FDoubleInterpNonLocalOutBufferDataVector;
  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuC2FDoubleInterpNonLocalOutBuffers = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<T*>> gpuC2FDoubleInterpNonLocalOutBufferData = {nullptr};

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuC2FOutBufferIndicesDoubleInterpNonLocal = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<int>> gpuC2FOutBufferIndicesDoubleInterpNonLocalChildIDs = {nullptr};
  size_t gpuC2FDoubleInterpNonLocalOutBuffersSize;

  std::vector<T**> gpuChildCellDatasEvenVector;
  std::vector<T**> gpuChildCellDatasOddVector;

  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuChildCellDatasEven = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<T**>> gpuChildCellDatasOdd = {nullptr};

  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuChildNYs = {nullptr};
  std::shared_ptr<memory_space::CudaDeviceHeap<size_t>> gpuChildNZs = {nullptr};
  #endif
};

} //end namespace olb

#endif