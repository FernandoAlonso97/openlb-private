#ifndef REFINED_GRID_VTK_MANAGER_2D_H
#define REFINED_GRID_VTK_MANAGER_2D_H

#include "refinedGrid2D.h"
#include "refinementUtil2D.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.hh"
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

namespace olb {

template <typename T, template <typename U> class Lattice>
class RefinedGridVTKManager2D {

public:
  RefinedGridVTKManager2D(std::string name, const RefinedGrid2D<T,Lattice>& gridInfo, T baseScale = 1.0, bool binary = true, bool printGhostLayer = true, bool printInterpLayer = false);

  BlockVTKwriterMultiLattice2D<T,Lattice<T>>& getWriter();

  template <typename FunctorType, typename ...Args>
  void addFunctor(Args && ...args);

  void write(int iT, size_t * globalOrigin = nullptr, size_t* globalExtend = nullptr);

private:
  void calculateWriteBounds(size_t * localOrigin, size_t * localExtend, size_t * globalOrigin = nullptr, size_t* globalExtend = nullptr);

  void calculateAMRBlocks(int iT, std::vector<std::tuple<std::string, int, size_t, size_t, size_t, size_t>>& amrBlocks, int& maxRefinementLevel, size_t* globalOrigin, size_t* globalExtend);

public:
  std::shared_ptr<BlockVTKwriterMultiLattice2D<T,Lattice<T>>> vtkWriter = {nullptr};
  std::vector<std::shared_ptr<BlockF2D<T>>> functors;
  const RefinedGrid2D<T,Lattice>& _gridInfo;
  std::vector<RefinedGridVTKManager2D<T,Lattice>> childWriters;
  std::string _name;
  bool _binary;
  bool _printGhostLayer;
  bool _printInterpLayer; 
  T _baseScale;
  float _currentOffset[2];
};

}//end namespace olb

#endif