#ifndef REFINED_GRID_VTK_MANAGER_2D_HH
#define REFINED_GRID_VTK_MANAGER_2D_HH

#include "refinedGridVTKManager2D.h"
#include "refinedGrid2D.h"
#include "refinementUtil2D.h"
#include "contrib/domainDecomposition/domainDecomposition.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.h"
#include "contrib/domainDecomposition/blockVtkWriterMultiLattice2D.hh"
#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>

namespace olb
{

template <typename T, template <typename U> class Lattice>
RefinedGridVTKManager2D<T, Lattice>::RefinedGridVTKManager2D(std::string name, const RefinedGrid2D<T, Lattice> &gridInfo, T baseScale, bool binary, bool printGhostLayer, bool printInterpLayer)
    : _name(name), _gridInfo(gridInfo), _binary(binary), _printGhostLayer(printGhostLayer), _printInterpLayer(printInterpLayer), _baseScale(baseScale)
{
  std::string currentName = _name + "_RefinementLevel" + std::to_string(gridInfo._refinementLevel) + "_ChildID" + std::to_string(_gridInfo._childID) + "_ChildOf" + std::to_string(_gridInfo._parentID);

  T currentScale = _baseScale * pow(0.5, gridInfo._refinementLevel);

  _currentOffset[0] = currentScale*(_gridInfo._globalOffset[0] - _gridInfo._interpLayerThickness);
  _currentOffset[1] = currentScale*(_gridInfo._globalOffset[1] - _gridInfo._interpLayerThickness);

  vtkWriter = std::make_shared<BlockVTKwriterMultiLattice2D<T, Lattice<T>>>(currentName, *(_gridInfo._domainInfo), currentScale, _binary, _printGhostLayer);

  for (int child = 0; child < _gridInfo.children.size(); child++)
  {
    RefinedGridVTKManager2D<T, Lattice> test(_name, _gridInfo.children[child], _baseScale, _binary, _printGhostLayer, _printInterpLayer);
    childWriters.push_back(test);
  }
}

template <typename T, template <typename U> class Lattice>
BlockVTKwriterMultiLattice2D<T, Lattice<T>> &RefinedGridVTKManager2D<T, Lattice>::getWriter()
{
  return *vtkWriter;
}

template <typename T, template <typename U> class Lattice>
template <typename FunctorType, typename... Args>
void RefinedGridVTKManager2D<T, Lattice>::addFunctor(Args &&...args)
{
  std::shared_ptr<FunctorType> functor = std::make_shared<FunctorType>(*(_gridInfo._latticePtr), std::forward<Args>(args)...);
  vtkWriter->addFunctor(*functor);
  functors.push_back(functor);
  for (int child = 0; child < childWriters.size(); child++)
  {
    childWriters[child].template addFunctor<FunctorType>(std::forward<Args>(args)...);
  }
}

template <typename T, template <typename U> class Lattice>
void RefinedGridVTKManager2D<T, Lattice>::write(int iT, size_t *globalOrigin, size_t *globalExtend)
{
  size_t localOrigin[2];
  size_t localExtend[2];

  calculateWriteBounds(localOrigin, localExtend, globalOrigin, globalExtend);

  vtkWriter->write(iT, float(iT), _currentOffset, localOrigin, localExtend);
  
  for (int child = 0; child < childWriters.size(); child++)
  {
    childWriters[child].write(iT, globalOrigin, globalExtend);
  }

  if (_gridInfo._isParent && _gridInfo._localSubDomain.localSubDomain == 0) { //write the amr vtk
    std::string fullNameVthb = singleton::directories().getVtkOutDir()  + createFileName(_name, iT) + ".vthb";
    std::ofstream vthbFile(fullNameVthb.c_str());

    int maxRefinementLevel = 0;
    std::vector<std::tuple<std::string, int, size_t, size_t, size_t, size_t>> amrBlocks;

    calculateAMRBlocks(iT, amrBlocks, maxRefinementLevel, globalOrigin, globalExtend);

    vthbFile << "<VTKFile type=\"vtkOverlappingAMR\" version=\"1.1\" byte_order=\"LittleEndian\" header_type=\"UInt64\">\n";
    vthbFile << "  <vtkOverlappingAMR origin=\"0 0 0\" grid_description=\"XY\">\n";
    for (int level = 0; level <= maxRefinementLevel; level++) {
      T currentSpacing = _baseScale * pow(0.5, level);
      vthbFile << "   <Block level=\"" << level << "\" spacing=\"" << currentSpacing << " " << currentSpacing << " " << currentSpacing << "\">\n";

      int dataIndex = 0;
      for (const auto& amrBlock : amrBlocks) {
        if (std::get<1>(amrBlock) == level) {
          vthbFile << "      <DataSet index=\"" << dataIndex << "\" amr_box=\"" << std::get<2>(amrBlock) << " " << std::get<3>(amrBlock) << " " << std::get<4>(amrBlock) << " " << std::get<5>(amrBlock) << " " << 0 << " " << -1 << "\" file=\"" + std::get<0>(amrBlock) << "\"/>\n";
          dataIndex++;
        }
      }

      vthbFile << "   </Block>\n";
    }
    vthbFile << "  </vtkOverlappingAMR>\n";
    vthbFile << "</VTKFile>\n";
    vthbFile.close();
  }
}

template <typename T, template <typename U> class Lattice>
void RefinedGridVTKManager2D<T, Lattice>::calculateAMRBlocks(int iT, std::vector<std::tuple<std::string, int, size_t, size_t, size_t, size_t>>& amrBlocks, int& maxRefinementLevel, size_t* globalOrigin, size_t* globalExtend) {

  size_t localOrigin[2];
  size_t localExtend[2];

  calculateWriteBounds(localOrigin, localExtend, globalOrigin, globalExtend);

  for (int subDomainID = 0; subDomainID < _gridInfo._domainInfo->noSubDomains; subDomainID++) {
    std::array<size_t,Lattice<T>::d> nCells;
    std::array<long long int,Lattice<T>::d> localIXE;
    std::array<long long int,Lattice<T>::d> localIXS;
    vtkWriter->computeDomainStuff(_gridInfo._domainInfo->subDomains[subDomainID], localOrigin, localExtend, nCells, localIXS, localIXE);

    bool noPrint = false;
    for(auto iCells : nCells)
      if(iCells == 0 || iCells == 1) {
        noPrint = true;
      }
    if (noPrint) continue;

    std::string currentFileName = createFileName( vtkWriter->getName() + "_GPU" + std::to_string(subDomainID), iT ) + ".vti";

    std::tuple<std::string, int, size_t, size_t, size_t, size_t> currentAMRBlock = std::make_tuple(currentFileName, _gridInfo._refinementLevel, _gridInfo._globalOffset[0]+localIXS[0]-_gridInfo._interpLayerThickness,
                                                                                                                   _gridInfo._globalOffset[0]+localIXE[0]-2-_gridInfo._interpLayerThickness,
                                                                                                                   _gridInfo._globalOffset[1]+localIXS[1]-_gridInfo._interpLayerThickness,
                                                                                                                   _gridInfo._globalOffset[1]+localIXE[1]-2-_gridInfo._interpLayerThickness);

    amrBlocks.push_back(currentAMRBlock);
    maxRefinementLevel = max(maxRefinementLevel, _gridInfo._refinementLevel);

    // std::cout << "Current domain stuff: subDomainID " << subDomainID << ", x bounds " << _gridInfo._globalOffset[0]+localIXS[0]-_gridInfo._interpLayerThickness << ", " << _gridInfo._globalOffset[0]+localIXE[0]-2-_gridInfo._interpLayerThickness << "; y bounds " << _gridInfo._globalOffset[1]+localIXS[1]-_gridInfo._interpLayerThickness << ", " << _gridInfo._globalOffset[1]+localIXE[1]-2-_gridInfo._interpLayerThickness << std::endl;
  }

  for (int child = 0; child < childWriters.size(); child++) {
    childWriters[child].calculateAMRBlocks(iT, amrBlocks, maxRefinementLevel, globalOrigin, globalExtend);
  }
}


template <typename T, template <typename U> class Lattice>
void RefinedGridVTKManager2D<T, Lattice>::calculateWriteBounds(size_t * localOrigin, size_t * localExtend, size_t * globalOrigin, size_t* globalExtend) {

  if (globalOrigin && globalExtend)
  {
    long currentGlobalOrigin[2];
    long currentGlobalExtend[2];

    currentGlobalOrigin[0] = globalOrigin[0] * pow(2, _gridInfo._refinementLevel) - _gridInfo._globalOffset[0] + _gridInfo._interpLayerThickness;
    currentGlobalOrigin[1] = globalOrigin[1] * pow(2, _gridInfo._refinementLevel) - _gridInfo._globalOffset[1] + _gridInfo._interpLayerThickness;

    currentGlobalExtend[0] = globalExtend[0] * pow(2, _gridInfo._refinementLevel) - _gridInfo._globalOffset[0] + _gridInfo._interpLayerThickness - pow(2, _gridInfo._refinementLevel) + 1;
    currentGlobalExtend[1] = globalExtend[1] * pow(2, _gridInfo._refinementLevel) - _gridInfo._globalOffset[1] + _gridInfo._interpLayerThickness - pow(2, _gridInfo._refinementLevel) + 1;

    currentGlobalOrigin[0] = max(currentGlobalOrigin[0], 0l);
    currentGlobalOrigin[1] = max(currentGlobalOrigin[1], 0l);
    currentGlobalExtend[0] = max(currentGlobalExtend[0], 0l);
    currentGlobalExtend[1] = max(currentGlobalExtend[1], 0l);

    size_t currentGlobalOriginSizeT[2] = {static_cast<size_t>(currentGlobalOrigin[0]), static_cast<size_t>(currentGlobalOrigin[1])};
    size_t currentGlobalExtendSizeT[2] = {static_cast<size_t>(currentGlobalExtend[0]), static_cast<size_t>(currentGlobalExtend[1])};

    if (!_printInterpLayer) {
      currentGlobalOriginSizeT[0] = std::max(currentGlobalOriginSizeT[0], (size_t)_gridInfo._interpLayerThickness);
      currentGlobalOriginSizeT[1] = std::max(currentGlobalOriginSizeT[1], (size_t)_gridInfo._interpLayerThickness);

      currentGlobalExtendSizeT[0] = std::min(currentGlobalExtendSizeT[0], (size_t)_gridInfo._baseDimensions[0] + (size_t)_gridInfo._interpLayerThickness);
      currentGlobalExtendSizeT[1] = std::min(currentGlobalExtendSizeT[1], (size_t)_gridInfo._baseDimensions[1] + (size_t)_gridInfo._interpLayerThickness);
    }
    else {
      currentGlobalExtendSizeT[0] = std::min(currentGlobalExtendSizeT[0], (size_t)_gridInfo._baseDimensions[0] + (size_t)2*_gridInfo._interpLayerThickness);
      currentGlobalExtendSizeT[1] = std::min(currentGlobalExtendSizeT[1], (size_t)_gridInfo._baseDimensions[1] + (size_t)2*_gridInfo._interpLayerThickness);
    }

    localOrigin[0] = currentGlobalOriginSizeT[0];
    localOrigin[1] = currentGlobalOriginSizeT[1];

    localExtend[0] = currentGlobalExtendSizeT[0];
    localExtend[1] = currentGlobalExtendSizeT[1];
  }
  else
  {
    if (!_printInterpLayer) {
      size_t currentGlobalOriginSizeT[2] = {_gridInfo._interpLayerThickness, _gridInfo._interpLayerThickness};
      size_t currentGlobalExtendSizeT[2] = {_gridInfo._baseDimensions[0] + _gridInfo._interpLayerThickness, _gridInfo._baseDimensions[1] + _gridInfo._interpLayerThickness}; 

      localOrigin[0] = currentGlobalOriginSizeT[0];
      localOrigin[1] = currentGlobalOriginSizeT[1];

      localExtend[0] = currentGlobalExtendSizeT[0];
      localExtend[1] = currentGlobalExtendSizeT[1];
    }
    else {
      size_t currentGlobalOriginSizeT[2] = {0,0};
      size_t currentGlobalExtendSizeT[2] = {_gridInfo._baseDimensions[0] + 2*_gridInfo._interpLayerThickness, _gridInfo._baseDimensions[1] + 2*_gridInfo._interpLayerThickness}; 

      localOrigin[0] = currentGlobalOriginSizeT[0];
      localOrigin[1] = currentGlobalOriginSizeT[1];

      localExtend[0] = currentGlobalExtendSizeT[0];
      localExtend[1] = currentGlobalExtendSizeT[1];
    }
  }
}

} //end namespace olb
#endif