#ifndef REFINEMENT_UTIL_2D_H
#define REFINEMENT_UTIL_2D_H

#include "core/config.h"
#include <iostream>

namespace olb 
{

namespace refinementutil 
{

OPENLB_HOST_DEVICE
constexpr size_t getRefinedRegionXFineLength2D(size_t const refinedRegionXCoarse, size_t const refinedRegionYCoarse, size_t const refinedRegionGhostLayerFine) {
  return refinedRegionXCoarse*2 - 1 + 2*refinedRegionGhostLayerFine;
}
OPENLB_HOST_DEVICE
constexpr size_t getRefinedRegionYFineLength2D(size_t const refinedRegionXCoarse, size_t const refinedRegionYCoarse, size_t const refinedRegionGhostLayerFine) {
  return refinedRegionYCoarse*2 - 1 + 2*refinedRegionGhostLayerFine;
}
OPENLB_HOST_DEVICE
constexpr size_t getF2CFineLength2D(size_t const refinedRegionXCoarse, size_t const refinedRegionYCoarse) {
  return 2*(refinedRegionYCoarse*2-1) + 2*(refinedRegionXCoarse*2-1-2); 
}
OPENLB_HOST_DEVICE
constexpr size_t getF2CCoarseLength2D(size_t const refinedRegionXCoarse, size_t const refinedRegionYCoarse) {
  return 2*refinedRegionYCoarse + 2*(refinedRegionXCoarse-2);
}
OPENLB_HOST_DEVICE
constexpr size_t getC2FFineLength2D(size_t const refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionGhostLayerFine) {
  return 2*(2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)-1) + 2*(2*(refinedRegionXCoarse-2+2*refinedRegionGhostLayerFine/2)+1);
} 
OPENLB_HOST_DEVICE
constexpr size_t getC2FCoarseLength2D(size_t const refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionGhostLayerFine) {
  return 2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2) + 2*(refinedRegionXCoarse-2+2*refinedRegionGhostLayerFine/2);
}
OPENLB_HOST_DEVICE
void getF2CCoarseIndices2D(size_t index, size_t refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionXOffsetCoarse, size_t refinedRegionYOffsetCoarse, size_t parentRegionGhostLayerCoarse, size_t indices[2]) {
  if (index < refinedRegionYCoarse) {
    indices[0] = refinedRegionXOffsetCoarse;
    indices[1] = refinedRegionYOffsetCoarse + index;
  }
  else if (index < refinedRegionYCoarse + 2*(refinedRegionXCoarse-2)) {
    indices[0] = refinedRegionXOffsetCoarse + (index-refinedRegionYCoarse)/2 + 1;
    indices[1] = refinedRegionYOffsetCoarse + ((index-refinedRegionYCoarse) % 2)*(refinedRegionYCoarse-1);
  }
  else {
    indices[0] = refinedRegionXOffsetCoarse + refinedRegionXCoarse - 1;
    indices[1] = refinedRegionYOffsetCoarse + (index - refinedRegionYCoarse - 2*(refinedRegionXCoarse-2));
  }
  indices[0] += parentRegionGhostLayerCoarse;
  indices[1] += parentRegionGhostLayerCoarse;
}
OPENLB_HOST_DEVICE
void getF2CFineIndices2D(size_t index, size_t refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionGhostLayerFine, size_t indices[2]) {
  if (index < refinedRegionYCoarse*2-1) {
    indices[0] = refinedRegionGhostLayerFine;
    indices[1] = refinedRegionGhostLayerFine + index;
  }
  else if (index < refinedRegionYCoarse*2-1 + 2*(2*(refinedRegionXCoarse-2)+1)) {
    indices[0] = refinedRegionGhostLayerFine + (index-(refinedRegionYCoarse*2-1))/2 + 1;
    indices[1] = refinedRegionGhostLayerFine + ((index-(refinedRegionYCoarse*2-1)) % 2)*(refinedRegionYCoarse*2-2);
  }
  else {
    indices[0] = refinedRegionGhostLayerFine + refinedRegionXCoarse*2 - 2;
    indices[1] = refinedRegionGhostLayerFine + (index - (refinedRegionYCoarse*2-1) - 2*(2*(refinedRegionXCoarse-2)+1));
  }
}
OPENLB_HOST_DEVICE
void getC2FCoarseIndices2D(size_t index, size_t refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionXOffsetCoarse, size_t refinedRegionYOffsetCoarse, size_t refinedRegionGhostLayerFine, size_t parentRegionGhostLayerCoarse, size_t indices[2]) {
  if (index < refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2) {
    indices[0] = refinedRegionXOffsetCoarse - refinedRegionGhostLayerFine/2;
    indices[1] = refinedRegionYOffsetCoarse - refinedRegionGhostLayerFine/2 + index;
  }
  else if (index < refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2 + 2*(refinedRegionXCoarse-2+2*refinedRegionGhostLayerFine/2)) {
    indices[0] = refinedRegionXOffsetCoarse - refinedRegionGhostLayerFine/2 + (index - (refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2))/2 + 1;
    indices[1] = refinedRegionYOffsetCoarse - refinedRegionGhostLayerFine/2 + ((index - (refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)) % 2)*(refinedRegionYCoarse+refinedRegionGhostLayerFine-1);
  }
  else {
    indices[0] = refinedRegionXOffsetCoarse + refinedRegionXCoarse - 1 + refinedRegionGhostLayerFine/2;
    indices[1] = refinedRegionYOffsetCoarse - refinedRegionGhostLayerFine/2 + (index - (refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2) - 2*(refinedRegionXCoarse-2+2*refinedRegionGhostLayerFine/2));
  }

  indices[0] += parentRegionGhostLayerCoarse;
  indices[1] += parentRegionGhostLayerCoarse;
}
OPENLB_HOST_DEVICE
void getC2FFineIndices2D(size_t index, size_t refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionGhostLayerFine, size_t indices[2]) {
  if (index < 2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)-1) {
    indices[0] = 0;
    indices[1] = index;
  }
  else if  (index < 2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)-1 + 2*(2*(refinedRegionXCoarse-2+2*refinedRegionGhostLayerFine/2)+1)) {
    indices[0] = (index - (2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)-1))/2 + 1;
    indices[1] = ((index - (2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)-1)) % 2) * (refinedRegionYCoarse*2-1 + 2*refinedRegionGhostLayerFine - 1);
  }
  else {
    indices[0] = refinedRegionXCoarse*2-1+2*refinedRegionGhostLayerFine-1;
    indices[1] = (index - (2*(refinedRegionYCoarse+2*refinedRegionGhostLayerFine/2)-1) - 2*(2*(refinedRegionXCoarse-2+2*refinedRegionGhostLayerFine/2)+1));
  }
}
OPENLB_HOST_DEVICE
void getFineIndicesFromCoarseIndices2D(size_t coarseX, size_t coarseY, size_t refinedRegionXOffsetCoarse, size_t refinedRegionYOffsetCoarse, size_t refinedRegionGhostLayerFine, size_t parentRegionGhostLayerCoarse, size_t indices[2]) {
  indices[0] = ((int)coarseX - (int)parentRegionGhostLayerCoarse - (int)refinedRegionXOffsetCoarse)*2 + refinedRegionGhostLayerFine;
  indices[1] = ((int)coarseY - (int)parentRegionGhostLayerCoarse - (int)refinedRegionYOffsetCoarse)*2 + refinedRegionGhostLayerFine;
}

OPENLB_HOST_DEVICE
bool isCoarsePointContainedInFineCoreArea2D(size_t coarseX, size_t coarseY, size_t refinedRegionXCoarse, size_t refinedRegionYCoarse, size_t refinedRegionXOffsetCoarse, size_t refinedRegionYOffsetCoarse, size_t parentRegionGhostLayerCoarse) {
  return coarseX >= (refinedRegionXOffsetCoarse + parentRegionGhostLayerCoarse) && coarseX < (refinedRegionXOffsetCoarse + parentRegionGhostLayerCoarse + refinedRegionXCoarse) &&
         coarseY >= (refinedRegionYOffsetCoarse + parentRegionGhostLayerCoarse) && coarseY < (refinedRegionYOffsetCoarse + parentRegionGhostLayerCoarse + refinedRegionYCoarse);
}

OPENLB_HOST_DEVICE
bool isFinePointContainedInFineCoreArea2D(size_t fineX, size_t fineY, size_t refinedRegionXOffsetCoarse, size_t refinedRegionYOffsetCoarse, size_t compRefinedRegionXCoarse, size_t compRefinedRegionYCoarse, size_t compRefinedRegionXOffsetCoarse, size_t compRefinedRegionYOffsetCoarse, size_t refinedRegionGhostLayerFine, size_t compRefinedRegionGhostLayerFine, size_t compIndices[2]) {
  bool contained = 
      (fineX - refinedRegionGhostLayerFine + refinedRegionXOffsetCoarse*2) >= compRefinedRegionXOffsetCoarse*2 &&
      (fineX - refinedRegionGhostLayerFine + refinedRegionXOffsetCoarse*2) < ((compRefinedRegionXOffsetCoarse + compRefinedRegionXCoarse - 1)*2 + 1) &&
      (fineY - refinedRegionGhostLayerFine + refinedRegionYOffsetCoarse*2) >= compRefinedRegionYOffsetCoarse*2 &&
      (fineY - refinedRegionGhostLayerFine + refinedRegionYOffsetCoarse*2) < ((compRefinedRegionYOffsetCoarse + compRefinedRegionYCoarse - 1)*2 + 1);

  if (contained) {
    compIndices[0] = (fineX - refinedRegionGhostLayerFine + refinedRegionXOffsetCoarse*2) - compRefinedRegionXOffsetCoarse*2 + compRefinedRegionGhostLayerFine;
    compIndices[1] = (fineY - refinedRegionGhostLayerFine + refinedRegionYOffsetCoarse*2) - compRefinedRegionYOffsetCoarse*2 + compRefinedRegionGhostLayerFine;
  }

  return contained;
}

template <typename T, template <typename U> class Lattice> 
OPENLB_HOST_DEVICE
void copyC2FCoarseToInterpLayer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FOutBuffers, size_t * const OPENLB_RESTRICT coarseIndices, 
                                  int * const OPENLB_RESTRICT coarseGridIDs, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT interpLayer, size_t threadIndex) {

  for (int iPop = 0; iPop < Lattice<T>::q + 1 + Lattice<T>::d; iPop++) {
    interpLayer[iPop][threadIndex] = C2FOutBuffers[coarseGridIDs[threadIndex]][iPop][coarseIndices[threadIndex]];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void calculateTimeInterpolatedC2FInterpLayer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FOutBuffers, size_t * const OPENLB_RESTRICT coarseIndices, 
                                  int * const OPENLB_RESTRICT coarseGridIDs, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT interpLayer, size_t threadIndex) {
  for (int iPop = 0; iPop < Lattice<T>::q + 1 + Lattice<T>::d; iPop++) {
    interpLayer[iPop][threadIndex] += C2FOutBuffers[coarseGridIDs[threadIndex]][iPop][coarseIndices[threadIndex]];
    interpLayer[iPop][threadIndex] *= 0.5;
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void copyF2F2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineDestIndices, 
                          int * const OPENLB_RESTRICT fineDestChildIDs, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT F2FOutBuffers, size_t * fineSrcIndices, int * fineSrcGridIDs, size_t threadIndex) {

  for (int iPop = 0; iPop < Lattice<T>::q + 1 + Lattice<T>::d; iPop++) {
    fineCellData[fineDestChildIDs[threadIndex]][iPop][fineDestIndices[threadIndex]] = F2FOutBuffers[fineSrcGridIDs[threadIndex]][iPop][fineSrcIndices[threadIndex]];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void copyF2C2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT coarseCellData, size_t * const OPENLB_RESTRICT coarseIndices,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT F2COutBuffers, size_t * const OPENLB_RESTRICT fineIndices,
                          int * const OPENLB_RESTRICT fineGridIDs, size_t threadIndex) {
  
  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    coarseCellData[iPop][coarseIndices[threadIndex]] = F2COutBuffers[fineGridIDs[threadIndex]][iPop][fineIndices[threadIndex]];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void copyC2FCoarseDirect2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FOutBuffers, size_t * const OPENLB_RESTRICT coarseIndices, 
                          int * const OPENLB_RESTRICT coarseGridIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices,
                          int * const OPENLB_RESTRICT fineChildIDs, size_t threadIndex) {
  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]] = C2FOutBuffers[coarseGridIDs[threadIndex]][iPop][coarseIndices[threadIndex]];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void copyC2FCoarseDirectFromInterpLayer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT interpLayer, 
                                      T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices,
                                      int * const OPENLB_RESTRICT fineChildIDs, size_t threadIndex) {
                       
  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]] = interpLayer[iPop][threadIndex];
  }
}

template <typename T, template <typename U> class Lattice, unsigned interpAxis>
OPENLB_HOST_DEVICE
void C2FInterp2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineChildIDs,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t threadIndex) {

  size_t fineCoords[Lattice<T>::d];
  util::getCellIndices2D(fineIndices[threadIndex], fineNYs[fineChildIDs[threadIndex]], fineCoords);

  //cubic interpolation with two neighbors on each side
  size_t interpNeighborCoords1[Lattice<T>::d];
  size_t interpNeighborCoords2[Lattice<T>::d];
  size_t interpNeighborCoords3[Lattice<T>::d];
  size_t interpNeighborCoords4[Lattice<T>::d];

  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    interpNeighborCoords1[iD] = fineCoords[iD];
    interpNeighborCoords2[iD] = fineCoords[iD];
    interpNeighborCoords3[iD] = fineCoords[iD];
    interpNeighborCoords4[iD] = fineCoords[iD];
  }

  interpNeighborCoords1[interpAxis] -= 3;
  interpNeighborCoords2[interpAxis] -= 1;
  interpNeighborCoords3[interpAxis] += 1;
  interpNeighborCoords4[interpAxis] += 3;

  size_t interpNeighborIndex1 = util::getCellIndex2D(interpNeighborCoords1[0], interpNeighborCoords1[1], fineNYs[fineChildIDs[threadIndex]]);
  size_t interpNeighborIndex2 = util::getCellIndex2D(interpNeighborCoords2[0], interpNeighborCoords2[1], fineNYs[fineChildIDs[threadIndex]]);
  size_t interpNeighborIndex3 = util::getCellIndex2D(interpNeighborCoords3[0], interpNeighborCoords3[1], fineNYs[fineChildIDs[threadIndex]]);
  size_t interpNeighborIndex4 = util::getCellIndex2D(interpNeighborCoords4[0], interpNeighborCoords4[1], fineNYs[fineChildIDs[threadIndex]]);

  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]] = (9.0/16.0)*(fineCellData[fineChildIDs[threadIndex]][iPop][interpNeighborIndex2]+fineCellData[fineChildIDs[threadIndex]][iPop][interpNeighborIndex3])
                                                                              -(1.0/16.0)*(fineCellData[fineChildIDs[threadIndex]][iPop][interpNeighborIndex1]+fineCellData[fineChildIDs[threadIndex]][iPop][interpNeighborIndex4]);
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void C2FNonLocalInterp2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, 
                        size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FNonLocalOutBuffers,
                        size_t * const OPENLB_RESTRICT neighbor1Indices, int * const neighbor1GridIDs,
                        size_t * const OPENLB_RESTRICT neighbor2Indices, int * const neighbor2GridIDs,
                        size_t * const OPENLB_RESTRICT neighbor3Indices, int * const neighbor3GridIDs,
                        size_t * const OPENLB_RESTRICT neighbor4Indices, int * const neighbor4GridIDs,
                        size_t threadIndex) {

  //cubic interpolation with two neighbors on each side 
  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]] = (9.0/16.0)*(C2FNonLocalOutBuffers[neighbor2GridIDs[threadIndex]][iPop][neighbor2Indices[threadIndex]]+C2FNonLocalOutBuffers[neighbor3GridIDs[threadIndex]][iPop][neighbor3Indices[threadIndex]])
                                                                             -(1.0/16.0)*(C2FNonLocalOutBuffers[neighbor1GridIDs[threadIndex]][iPop][neighbor1Indices[threadIndex]]+C2FNonLocalOutBuffers[neighbor4GridIDs[threadIndex]][iPop][neighbor4Indices[threadIndex]]);
  }
}

template <typename T, template <typename U> class Lattice, unsigned interpAxis>
OPENLB_HOST_DEVICE
void C2FNegativeCornerInterp2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineGridIDs,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t threadIndex) {
  size_t fineCoords[Lattice<T>::d];
  util::getCellIndices2D(fineIndices[threadIndex], fineNYs[fineGridIDs[threadIndex]], fineCoords);

  //cubic interpolation with 1 neighbor on (-), 2 neighbors on (+)
  size_t interpNeighborCoords1[Lattice<T>::d];
  size_t interpNeighborCoords2[Lattice<T>::d];
  size_t interpNeighborCoords3[Lattice<T>::d];

  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    interpNeighborCoords1[iD] = fineCoords[iD];
    interpNeighborCoords2[iD] = fineCoords[iD];
    interpNeighborCoords3[iD] = fineCoords[iD];
  }

  interpNeighborCoords1[interpAxis] -= 1;
  interpNeighborCoords2[interpAxis] += 1;
  interpNeighborCoords3[interpAxis] += 3;

  size_t interpNeighborIndex1 = util::getCellIndex2D(interpNeighborCoords1[0], interpNeighborCoords1[1], fineNYs[fineGridIDs[threadIndex]]);
  size_t interpNeighborIndex2 = util::getCellIndex2D(interpNeighborCoords2[0], interpNeighborCoords2[1], fineNYs[fineGridIDs[threadIndex]]);
  size_t interpNeighborIndex3 = util::getCellIndex2D(interpNeighborCoords3[0], interpNeighborCoords3[1], fineNYs[fineGridIDs[threadIndex]]);
    
  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    fineCellData[fineGridIDs[threadIndex]][iPop][fineIndices[threadIndex]] = (3.0/8.0)*(fineCellData[fineGridIDs[threadIndex]][iPop][interpNeighborIndex1])
                                                 + (3.0/4.0)*(fineCellData[fineGridIDs[threadIndex]][iPop][interpNeighborIndex2])
                                                 - (1.0/8.0)*(fineCellData[fineGridIDs[threadIndex]][iPop][interpNeighborIndex3]);
  }
}

template <typename T, template <typename U> class Lattice, unsigned interpAxis>
OPENLB_HOST_DEVICE
void C2FPositiveCornerInterp2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineGridIDs,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t threadIndex) {
  size_t fineCoords[Lattice<T>::d];
  util::getCellIndices2D(fineIndices[threadIndex], fineNYs[fineGridIDs[threadIndex]], fineCoords);

  //cubic interpolation with 2 neighbors on (-), 1 neighbor on (+)
  size_t interpNeighborCoords1[Lattice<T>::d];
  size_t interpNeighborCoords2[Lattice<T>::d];
  size_t interpNeighborCoords3[Lattice<T>::d];

  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    interpNeighborCoords1[iD] = fineCoords[iD];
    interpNeighborCoords2[iD] = fineCoords[iD];
    interpNeighborCoords3[iD] = fineCoords[iD];
  }

  interpNeighborCoords1[interpAxis] -= 3;
  interpNeighborCoords2[interpAxis] -= 1;
  interpNeighborCoords3[interpAxis] += 1;

  size_t interpNeighborIndex1 = util::getCellIndex2D(interpNeighborCoords1[0], interpNeighborCoords1[1], fineNYs[fineGridIDs[threadIndex]]);
  size_t interpNeighborIndex2 = util::getCellIndex2D(interpNeighborCoords2[0], interpNeighborCoords2[1], fineNYs[fineGridIDs[threadIndex]]);
  size_t interpNeighborIndex3 = util::getCellIndex2D(interpNeighborCoords3[0], interpNeighborCoords3[1], fineNYs[fineGridIDs[threadIndex]]);

  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    fineCellData[fineGridIDs[threadIndex]][iPop][fineIndices[threadIndex]] = -(1.0/8.0)*(fineCellData[fineGridIDs[threadIndex]][iPop][interpNeighborIndex1])
                                                  + (3.0/4.0)*(fineCellData[fineGridIDs[threadIndex]][iPop][interpNeighborIndex2])
                                                  + (3.0/8.0)*(fineCellData[fineGridIDs[threadIndex]][iPop][interpNeighborIndex3]);
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void C2FGeneralInterp2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineChildIDs, int * const OPENLB_RESTRICT fineInterpAxis, int * const OPENLB_RESTRICT fineInterpType,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t threadIndex) {

  switch (fineInterpAxis[threadIndex]) {
    case 0:
      switch (fineInterpType[threadIndex]) {
        case -1:
          C2FNegativeCornerInterp2D<T,Lattice,0>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
          break;
        case 1:
          C2FPositiveCornerInterp2D<T,Lattice,0>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
          break;
        case 0:
          C2FInterp2D<T,Lattice,0>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
          break;
      }
      break;

    case 1:
      switch (fineInterpType[threadIndex]) {
        case -1:
          C2FNegativeCornerInterp2D<T,Lattice,1>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
          break;
        case 1:
          C2FPositiveCornerInterp2D<T,Lattice,1>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
          break;
        case 0:
          C2FInterp2D<T,Lattice,1>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
          break;
      }
      break;

  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void C2FWriteToOutBuffer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT coarseCellData, size_t * const OPENLB_RESTRICT coarseIndices,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer, T omegaCoarse, T omegaFine, size_t threadIndex) {

  T rho = coarseCellData[Lattice<T>::rhoIndex][coarseIndices[threadIndex]];
  T u[Lattice<T>::d];
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    u[iD] = coarseCellData[Lattice<T>::uIndex+iD][coarseIndices[threadIndex]];
  }

  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    T sourcePop = coarseCellData[iPop][coarseIndices[threadIndex]];
    T sourceFeq = lbHelpers<T, Lattice>::equilibrium(iPop, rho, u, uSqr);
    T sourceFneq = sourcePop - sourceFeq;
    outBuffer[iPop][threadIndex] = sourceFeq + sourceFneq*(omegaCoarse)/(2.0*omegaFine);
  }
  outBuffer[Lattice<T>::rhoIndex][threadIndex] = rho;

  for (int iD = 0; iD < Lattice<T>::d; iD++) {
     outBuffer[Lattice<T>::uIndex+iD][threadIndex] = u[iD];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void F2CWriteToOutBuffer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer, T omegaCoarse, T omegaFine, size_t * const OPENLB_RESTRICT fineNYs, size_t threadIndex) {

  size_t fineCoords[2];
  util::getCellIndices2D(fineIndices[threadIndex], fineNYs[fineChildIDs[threadIndex]], fineCoords);

  T rho = fineCellData[fineChildIDs[threadIndex]][Lattice<T>::rhoIndex][fineIndices[threadIndex]];
  T u[Lattice<T>::d];
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    u[iD] = fineCellData[fineChildIDs[threadIndex]][Lattice<T>::uIndex+iD][fineIndices[threadIndex]];
  }
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    T sourcePop = fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]];
    T sourceFeq = lbHelpers<T, Lattice>::equilibrium(iPop, rho, u, uSqr);
    T sourceFneq = sourcePop - sourceFeq;

    //box filter of Fneq
    T fNeqRunningSum = sourceFneq;
    for (int dir = 1; dir < Lattice<T>::q; dir++) {
      size_t currentFilterCoords[Lattice<T>::d];
      for (int iD = 0; iD < Lattice<T>::d; iD++) {
        currentFilterCoords[iD] = fineCoords[iD] + Lattice<T>::c(dir, iD);
      }
      size_t currentFilterIndex = util::getCellIndex2D(currentFilterCoords[0], currentFilterCoords[1], fineNYs[fineChildIDs[threadIndex]]);
      T filterRho = fineCellData[fineChildIDs[threadIndex]][Lattice<T>::rhoIndex][currentFilterIndex];
      T filterU[Lattice<T>::d];
      for (int iD = 0; iD < Lattice<T>::d; iD++) {
        filterU[iD] = fineCellData[fineChildIDs[threadIndex]][Lattice<T>::uIndex+iD][currentFilterIndex];
      }
      T filterUSqr = util::normSqr<T,Lattice<T>::d>(filterU);
      T filterPop = fineCellData[fineChildIDs[threadIndex]][iPop][currentFilterIndex];
      T filterFeq = lbHelpers<T,Lattice>::equilibrium(iPop, filterRho, filterU, filterUSqr);
      T filterFneq = filterPop-filterFeq;
      fNeqRunningSum += filterFneq;
    }

    T filteredFNeq = fNeqRunningSum / (T) Lattice<T>::q;
    outBuffer[iPop][threadIndex] = sourceFeq + filteredFNeq*2.0*omegaFine/omegaCoarse;
  }

  outBuffer[Lattice<T>::rhoIndex][threadIndex] = rho;
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    outBuffer[Lattice<T>::uIndex+iD][threadIndex] = u[iD];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void C2FInterpWriteToOutBuffer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer,  size_t threadIndex) {
  //direct copy operation
  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    outBuffer[iPop][threadIndex] = fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]];
  }
}

template <typename T, template <typename U> class Lattice>
OPENLB_HOST_DEVICE
void F2FWriteToOutBuffer2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer, size_t threadIndex) {

  for (int iPop = 0; iPop < Lattice<T>::q+1+Lattice<T>::d; iPop++) {
    outBuffer[iPop][threadIndex] = fineCellData[fineChildIDs[threadIndex]][iPop][fineIndices[threadIndex]];
  }
}


#ifdef __CUDACC__

template <typename T, template <typename U> class Lattice>
__global__ void copyC2FCoarseToInterpLayerKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FOutBuffers, size_t * const OPENLB_RESTRICT coarseIndices, 
                                  int * const OPENLB_RESTRICT coarseGridIDs, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT interpLayer, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  copyC2FCoarseToInterpLayer2D<T,Lattice>(C2FOutBuffers, coarseIndices, coarseGridIDs, interpLayer, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void calculateTimeInterpolatedC2FInterpLayerKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FOutBuffers, size_t * const OPENLB_RESTRICT coarseIndices, 
                                  int * const OPENLB_RESTRICT coarseGridIDs, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT interpLayer, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= gridSize)
    return;
  calculateTimeInterpolatedC2FInterpLayer2D<T,Lattice>(C2FOutBuffers, coarseIndices, coarseGridIDs, interpLayer, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void copyF2FKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineDestIndices, 
                          int * const OPENLB_RESTRICT fineDestChildIDs, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT F2FOutBuffers, size_t * fineSrcIndices, int * fineSrcGridIDs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  copyF2F2D<T,Lattice>(fineCellData, fineDestIndices, fineDestChildIDs, F2FOutBuffers, fineSrcIndices, fineSrcGridIDs, threadIndex);
}                            

template <typename T, template <typename U> class Lattice>
__global__ void copyF2CKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT coarseCellData, size_t * const OPENLB_RESTRICT coarseIndices,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT F2COutBuffers, size_t * const OPENLB_RESTRICT fineIndices,
                          int * const OPENLB_RESTRICT fineGridIDs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  copyF2C2D<T,Lattice>(coarseCellData, coarseIndices, F2COutBuffers, fineIndices, fineGridIDs, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void copyC2FCoarseDirectKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FOutBuffers, size_t * const OPENLB_RESTRICT coarseIndices, 
                          int * const OPENLB_RESTRICT coarseGridIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices,
                          int * const OPENLB_RESTRICT fineChildIDs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  copyC2FCoarseDirect2D<T,Lattice>(C2FOutBuffers, coarseIndices, coarseGridIDs, fineCellData, fineIndices, fineChildIDs, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void copyC2FCoarseDirectFromInterpLayerKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT interpLayer, 
                                      T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices,
                                      int * const OPENLB_RESTRICT fineChildIDs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  copyC2FCoarseDirectFromInterpLayer2D<T,Lattice>(interpLayer, fineCellData, fineIndices, fineChildIDs, threadIndex);
}

template <typename T, template <typename U> class Lattice, unsigned interpAxis>
__global__ void C2FInterpKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineChildIDs,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  C2FInterp2D<T,Lattice,interpAxis>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
}

template <typename T, template <typename U> class Lattice, unsigned interpAxis>
__global__ void C2FNegativeCornerInterpKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineChildIDs,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  C2FNegativeCornerInterp2D<T,Lattice,interpAxis>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
}

template <typename T, template <typename U> class Lattice, unsigned interpAxis>
__global__ void C2FPositiveCornerInterpKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineChildIDs,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  C2FPositiveCornerInterp2D<T,Lattice,interpAxis>(fineCellData, fineIndices, fineChildIDs, fineNYs, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void C2FGeneralInterpKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, 
                  int * const OPENLB_RESTRICT fineChildIDs, int * const OPENLB_RESTRICT fineInterpAxis, int * const OPENLB_RESTRICT fineInterpType,
                  size_t * const OPENLB_RESTRICT fineNYs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;            
  C2FGeneralInterp2D<T,Lattice>(fineCellData, fineIndices, fineChildIDs, fineInterpAxis, fineInterpType, fineNYs, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void C2FNonLocalInterpKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, 
                        size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT C2FNonLocalOutBuffers,
                        size_t * const OPENLB_RESTRICT neighbor1Indices, int * const neighbor1GridIDs,
                        size_t * const OPENLB_RESTRICT neighbor2Indices, int * const neighbor2GridIDs,
                        size_t * const OPENLB_RESTRICT neighbor3Indices, int * const neighbor3GridIDs,
                        size_t * const OPENLB_RESTRICT neighbor4Indices, int * const neighbor4GridIDs,
                        size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  C2FNonLocalInterp2D<T,Lattice>(fineCellData, fineIndices, fineChildIDs, C2FNonLocalOutBuffers, neighbor1Indices, neighbor1GridIDs, neighbor2Indices, neighbor2GridIDs, neighbor3Indices, neighbor3GridIDs, neighbor4Indices, neighbor4GridIDs, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void C2FWriteToOutBufferKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT coarseCellData, size_t * const OPENLB_RESTRICT coarseIndices,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer, T omegaCoarse, T omegaFine, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  C2FWriteToOutBuffer2D<T,Lattice>(coarseCellData, coarseIndices, outBuffer, omegaCoarse, omegaFine, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void F2CWriteToOutBufferKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer, T omegaCoarse, T omegaFine, size_t * const OPENLB_RESTRICT fineNYs, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  F2CWriteToOutBuffer2D<T,Lattice>(fineCellData, fineIndices, fineChildIDs, outBuffer, omegaCoarse, omegaFine, fineNYs, threadIndex);
}

template <typename T, template <typename U> class Lattice>
__global__ void F2CSharedWriteToOutBufferKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData,
                          size_t * const OPENLB_RESTRICT fineLoadIndices, int * const OPENLB_RESTRICT fineLoadChildIDs, size_t numLoadsPerThread,
                          size_t * const OPENLB_RESTRICT fineSharedIndices,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer, T omegaCoarse, T omegaFine, size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  const size_t threadIndexInBlock = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y;
  const size_t maxLoadsPerThread = 7;
  const size_t TPB = 128;
  const size_t numThreads = gridDim.x*gridDim.y*gridDim.z*TPB;

  __shared__ T sharedRho[TPB*maxLoadsPerThread];
  __shared__ T sharedU0[TPB*maxLoadsPerThread];
  __shared__ T sharedU1[TPB*maxLoadsPerThread];
  __shared__ T sharedPop[TPB*maxLoadsPerThread];

  for (int i = 0; i < numLoadsPerThread; i++) {
    sharedRho[TPB*i+threadIndexInBlock] = fineCellData[fineLoadChildIDs[numThreads*i+threadIndex]][Lattice<T>::rhoIndex][fineLoadIndices[numThreads*i+threadIndex]];
    sharedU0[TPB*i+threadIndexInBlock] = fineCellData[fineLoadChildIDs[numThreads*i+threadIndex]][Lattice<T>::uIndex][fineLoadIndices[numThreads*i+threadIndex]];
    sharedU1[TPB*i+threadIndexInBlock] = fineCellData[fineLoadChildIDs[numThreads*i+threadIndex]][Lattice<T>::uIndex+1][fineLoadIndices[numThreads*i+threadIndex]];
  }
  __syncthreads();

  T rho;
  T u[Lattice<T>::d];
  if (threadIndex < gridSize) {
    rho = sharedRho[fineSharedIndices[threadIndex]];
    u[0] = sharedU0[fineSharedIndices[threadIndex]];
    u[1] = sharedU1[fineSharedIndices[threadIndex]];

    outBuffer[Lattice<T>::rhoIndex][threadIndex] = rho;
    for (int iD = 0; iD < Lattice<T>::d; iD++) {
      outBuffer[Lattice<T>::uIndex+iD][threadIndex] = u[iD];
    }
  }
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);  

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    for (int i = 0; i < numLoadsPerThread; i++) {
      sharedPop[TPB*i+threadIndexInBlock] = fineCellData[fineLoadChildIDs[numThreads*i+threadIndex]][iPop][fineLoadIndices[numThreads*i+threadIndex]];
    }
    __syncthreads();

    
    if (threadIndex < gridSize) {
      T sourcePop = sharedPop[fineSharedIndices[threadIndex]];
      T sourceFeq = lbHelpers<T, Lattice>::equilibrium(iPop, rho, u, uSqr);
      T sourceFneq = sourcePop - sourceFeq;

      //box filter of Fneq
      T fNeqRunningSum = sourceFneq;
      for (int dir = 1; dir < Lattice<T>::q; dir++) {
        T filterRho = sharedRho[fineSharedIndices[gridSize*dir+threadIndex]];
        T filterU[Lattice<T>::d];
        filterU[0] = sharedU0[fineSharedIndices[gridSize*dir+threadIndex]];
        filterU[1] = sharedU1[fineSharedIndices[gridSize*dir+threadIndex]];
        T filterUSqr = util::normSqr<T,Lattice<T>::d>(filterU);
        T filterPop = sharedPop[fineSharedIndices[gridSize*dir+threadIndex]];
        T filterFeq = lbHelpers<T,Lattice>::equilibrium(iPop, filterRho, filterU, filterUSqr);
        T filterFneq = filterPop-filterFeq;
        fNeqRunningSum += filterFneq;
      }

      T filteredFNeq = fNeqRunningSum/(T) Lattice<T>::q;
      outBuffer[iPop][threadIndex] = sourceFeq + filteredFNeq*2.0*omegaFine/omegaCoarse;
    }
    __syncthreads();
  }

}

template <typename T, template <typename U> class Lattice>
__global__ void C2FInterpWriteToOutBufferKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer,  size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  C2FInterpWriteToOutBuffer2D<T,Lattice>(fineCellData, fineIndices, fineChildIDs, outBuffer, threadIndex);     
}

template <typename T, template <typename U> class Lattice>
__global__ void F2FWriteToOutBufferKernel2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT * const OPENLB_RESTRICT fineCellData, size_t * const OPENLB_RESTRICT fineIndices, int * const OPENLB_RESTRICT fineChildIDs,
                          T * const OPENLB_RESTRICT * const OPENLB_RESTRICT outBuffer,  size_t gridSize) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;
  if (threadIndex >= gridSize)
    return;
  F2FWriteToOutBuffer2D<T,Lattice>(fineCellData, fineIndices, fineChildIDs, outBuffer, threadIndex);     
}
#endif

} //end namespace refinementutil

}
#endif