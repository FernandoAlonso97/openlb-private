#ifndef MPI_COMMUNICATION_H
#define MPI_COMMUNICATION_H


#include <mpi.h>
#include <mpi-ext.h>
#include "mpiTemplateTypes.h"

int initMPI()
{
     // Initialize the MPI environment
     MPI_Init(NULL, NULL);

     int world_rank;
     MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

     return world_rank;
}

void finalizeMPI()
{
  MPI_Finalize();
}

int getNoRanks()
{
     int world_size;
     MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  return world_size; 
}

int getRank()
{
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  return world_rank;
}
#endif
