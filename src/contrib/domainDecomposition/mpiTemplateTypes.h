//See:
//https://github.com/motonacciu/mpp/blob/master/include/detail/type_traits.h
template <class RawType>
struct MPIType {
    typedef RawType element_type;

    static inline MPI_Datatype get_type(RawType&& raw);
};

#define PRIMITIVE(RawType, MpiType) \
	template<> \
	inline MPI_Datatype MPIType<RawType>::get_type(RawType&&) { \
		return MpiType; \
	}

PRIMITIVE(char, 				MPI_CHAR);
PRIMITIVE(wchar_t,				MPI_WCHAR);
PRIMITIVE(short, 				MPI_SHORT);
PRIMITIVE(int, 					MPI_INT);
PRIMITIVE(long, 				MPI_LONG);
PRIMITIVE(signed char, 			MPI_SIGNED_CHAR);
PRIMITIVE(unsigned char, 		MPI_UNSIGNED_CHAR);
PRIMITIVE(unsigned short, 		MPI_UNSIGNED_SHORT);
PRIMITIVE(unsigned int,			MPI_UNSIGNED);
PRIMITIVE(unsigned long,		MPI_UNSIGNED_LONG);
PRIMITIVE(unsigned long long,	MPI_UNSIGNED_LONG_LONG);

PRIMITIVE(float, 				MPI_FLOAT);
PRIMITIVE(double, 				MPI_DOUBLE);
PRIMITIVE(long double,			MPI_LONG_DOUBLE);

PRIMITIVE(std::complex<float>,		MPI_COMPLEX);
PRIMITIVE(std::complex<double>,		MPI_DOUBLE_COMPLEX);

#undef PRIMITIVE 