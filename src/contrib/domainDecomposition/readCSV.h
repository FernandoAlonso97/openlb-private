#ifndef READ_CSV_H
#define READ_CSV_H

#include <vector>
#include <string>
#include <istream>
#include <sstream>
#include <functional>
#include <algorithm>
#include <exception>

namespace olb{


  template<typename CharT>
  std::vector<std::string> readNextLine (std::istream& origin, CharT delimiter)
  {
    std::vector<std::string> result;
    std::string              line;
    std::getline(origin,line);

    if (line.empty())
      throw (std::out_of_range("File at End"));

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell,delimiter))
    {
      if(!cell.empty())
        result.push_back(cell);
    }
    return result;
  }


class ReadSconeData {

  public:
    ReadSconeData(std::string filename,unsigned baseskip):filename_(filename),filestream_(filename_)
  {
    if (filestream_.is_open())
    {
      std::cout << "File " + filename_ +" opened successful" << std::endl;
      for (unsigned i=0;i<baseskip;++i)
      {
        std::string line;
        std::getline(filestream_,line);
      }
    }
    else 
      throw (std::invalid_argument("Filename " + filename_ + " could not be opened"));
  }

    ReadSconeData() = delete;

    ReadSconeData(ReadSconeData const &) = default;


  std::vector<std::string> operator() ()
  {
      return readNextLine(filestream_,' ');
  }

  private:
      std::string filename_;
      std::ifstream filestream_;

};

template<typename Callable>
class ConvertStrToDouble {
  public:
   explicit ConvertStrToDouble (Callable& call):call_(call) {}
   
   ConvertStrToDouble () = delete; 

   std::vector<double> operator() () 
  {
    std::vector<std::string> result = call_();
    std::vector<double> ret(result.size());
    unsigned i = 0;
    std::generate(ret.begin(),ret.end(),[&result,&i](){return std::stod(result[i++]);});
    // for (auto it=result.begin();it!=result.end();++it )
    // {
      // ret[it-result.begin()]=(std::stod(*it));
    // }
    return ret;
  }

  private:
     Callable& call_;
};

template<typename T>
std::vector<T> transformToSI (std::vector<T> input, std::vector<T> const conversionFactors)
{
  std::transform(input.begin(),input.end(),conversionFactors.begin(),input.begin(),std::multiplies<T>{});
  return input;
}

template<typename Callable>
class ToSIUnits{

  public:
    explicit ToSIUnits (Callable& call,std::vector<double> conversionFactors):call_(call),conversionFactors_(conversionFactors) {}

    ToSIUnits() = delete;

    std::vector<double> operator() ()
    {
      return transformToSI(call_(),conversionFactors_);
    }

  private:
    Callable& call_;
    std::vector<double> const conversionFactors_;

};

template<typename T>
std::vector<T> interpolateSconeVals(std::vector<T> sconeValsPast,std::vector<T> const & sconeValsFuture, T timeNow)
{
      T timePast = sconeValsPast[0];
      T timeFuture = sconeValsFuture[0];
  std::transform(sconeValsPast.begin(),sconeValsPast.end(),sconeValsFuture.begin(),sconeValsPast.begin(),
      [timeNow,timePast,timeFuture](T& past,T const & future)
      {
        return past + (future-past)*((timeNow-timePast)/(timeFuture-timePast));
      }
      );
  return sconeValsPast;
}

} //eon
#endif
