#ifndef READ_VTI_H
#define READ_VTI_H

#include <algorithm>
#include "io/xmlReader.h"
#include "io/xmlReader.cpp"
#include "io/base64.h"
#include "core/util.h"


namespace olb {

template<typename T>
T** readvti (std::vector<std::string> cohesiveFileNames, std::string fieldName,std::vector<int>& globalExtend, std::vector<float>& spacing,double conversionFactor,unsigned dim=3)
{

  std::cout << "Reading ";
  std::for_each(cohesiveFileNames.begin(),cohesiveFileNames.end(),[](std::string& v){std::cout << v << ";";});
  std::cout << std::endl;

  globalExtend.resize(6);
  std::for_each(globalExtend.begin(),globalExtend.end(),[](int& v){v=0;});

  std::vector<XMLreader> xmlReaders{};
  for (unsigned i= 0;i< cohesiveFileNames.size();++i)
  {
    xmlReaders.emplace_back(XMLreader(cohesiveFileNames[i]));
  }

  std::vector<std::vector<int>> extend {cohesiveFileNames.size()};
  for (unsigned i=0;i<cohesiveFileNames.size();++i)
    extend[i] = std::vector<int>{0,0,0,0,0,0};

  for (unsigned i=0;i<xmlReaders.size();++i)
  {
    // Read WholeExtent (2 * _dim ints) from XML File and calculate _extent
    std::stringstream readExtent{xmlReaders[i]["ImageData"].getAttribute("WholeExtent")};
    int tmp{0};
    for (int j = 0; j < 2 * dim; ++j) {
      readExtent >> tmp;
      extend[i][j] = tmp;
    }

    extend[i][1] += 1;
    extend[i][3] += 1;
    extend[i][5] += 1;

    std::cout << "Extend ";
    std::for_each(extend[i].begin(),extend[i].end(),[](int& v){std::cout << v << " ";});
    std::cout << std::endl;
  }

  for (unsigned i=0;i<xmlReaders.size();++i)
  {
    globalExtend[0] = extend[0][0];
    globalExtend[1] += extend[i][1];
    globalExtend[2] = extend[i][2];
    globalExtend[3] = extend[i][3];
    globalExtend[4] = extend[i][4];
    globalExtend[5] = extend[i][5];
  }

  std::cout << "GlobalExtend ";
  std::for_each(globalExtend.begin(),globalExtend.end(),[](int& v){std::cout << v << " ";});
  std::cout << std::endl;

  // Read _delta
  spacing = std::vector<float>{0.0,0.0,0.0};
  std::stringstream stream_val_1(xmlReaders[0]["ImageData"].getAttribute("Spacing"));
  for (unsigned i=0;i<dim;++i)
  {
    stream_val_1 >> spacing[i];
  }

  std::cout << "Spacing ";
  std::for_each(spacing.begin(),spacing.end(),[](float& v){std::cout << v << " ";});
  std::cout << std::endl;


  // get dim 
  int fieldDim {1};
  for (auto& data : xmlReaders[0]["ImageData"])
  { 
    for (auto& dataArray : (*data)["PointData"])
    if (dataArray->getAttribute("Name") == fieldName && dataArray->getName() == "DataArray") 
    {
      if ("Attribute not found." != dataArray->getAttribute("NumberOfComponents"))
        fieldDim = std::stoi(dataArray->getAttribute("NumberOfComponents"));
    }
  }

  std::cout << "FieldDim " << fieldDim << std::endl;;

  //allocate
  T** retVal = nullptr;
  retVal = new T*[fieldDim];
  for (unsigned i=0;i<fieldDim;++i)
  {
    retVal[i] = new T[globalExtend[1]*globalExtend[3]*globalExtend[5]];
  }

  unsigned indexShift[dim] ={0,0,0};

  for (unsigned readerIndex = 0;readerIndex<xmlReaders.size();++readerIndex)
  {
  // Iterate through all <DataArray> tags and take the one with the given Name attribute
  for (auto & data : xmlReaders[readerIndex]["ImageData"]) {
    for (auto& dataArray : (*data)["PointData"])
    if (dataArray->getAttribute("Name") == fieldName && dataArray->getName() == "DataArray") {
      std::string data_str;
      if (dataArray->read(data_str)) {
        std::stringstream stream_val(data_str.substr(8));


        // unsigned int datasize;
        // stream_val >> datasize;
        // std::cout << "datasize in file " << datasize << std::endl;
        // stream_val.ignore(sizeof(unsigned int));
        // std::cout << stream_val.str() << " \n";
        Base64Decoder<float> decoder{stream_val,fieldDim*globalExtend[1]*globalExtend[3]*globalExtend[5]};

        float* data_float = new float[fieldDim*globalExtend[1]*globalExtend[3]*globalExtend[5]];
        decoder.decode(data_float,fieldDim*globalExtend[1]*globalExtend[3]*globalExtend[5]);
        std::cout << data_float[0] << " " << data_float[1] << " " << data_float[2] << " readerindex " << readerIndex <<  std::endl;
        // // std::cout << stream_val.str() << " \n";

        size_t index = 0;
        // Careful: respect ordering in VTI File
        for (int iz = 0; iz < extend[readerIndex][5]; iz++) {
          for (int iy = 0; iy < extend[readerIndex][3]; iy++) {
            for (int ix = indexShift[0]; ix < extend[readerIndex][1]+indexShift[0]; ix++) {
              // std::cout << "ix iy iz" << ix << " " << iy << " " << iz << " " << data_float[index] <<" "<< data_float[index+1] << data_float[index+2] << std::endl;
              for (int iSize=0; iSize < fieldDim; iSize++) {
                // write tmp into blockData
                // std::cout << data_float[index] << " " << static_cast<T>(conversionFactor*data_float[index]) << std::endl;
                retVal[iSize][util::getCellIndex3D(ix,iy,iz,globalExtend[3],globalExtend[5])] = conversionFactor * data_float[index];
                ++index;
              }
            }
          }
        }
        delete[] data_float;
      }
    }

    indexShift[0]+=extend[readerIndex][1];
  }
  }

  return retVal;
}

}
#endif
