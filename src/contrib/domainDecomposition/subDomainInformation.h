#ifndef SUBDOMAININFORMATION_H
#define SUBDOMAININFORMATION_H

namespace olb{


template<typename Lattice>
OPENLB_HOST_DEVICE
unsigned getPopFromC (int c[3])
{
  if (Lattice::d == 3) {
    for (unsigned i=0;i<Lattice::q;++i)
    {
        if(Lattice::c(i,0)==c[0] and 
          Lattice::c(i,1)==c[1] and 
          Lattice::c(i,2)==c[2] )
          return i;
    }
  }
  else if (Lattice::d == 2) {
    for (unsigned i=0;i<Lattice::q;++i)
    {
        if(Lattice::c(i,0)==c[0] and 
          Lattice::c(i,1)==c[1])
          return i;
    }
  }

  assert("direction not contained in lattice descriptor" && false);
  return 99;
}


struct Index3D {

  Index3D() = default;
  OPENLB_HOST_DEVICE
  Index3D(size_t ix, size_t iy, size_t iz,unsigned subDom):
    subDomain(subDom) 
    {index[0] = ix; index[1] = iy; index[2] = iz;}

  OPENLB_HOST_DEVICE
  int& operator[] (unsigned idx)
  {
    return index[idx];
  }

  int index[3] = {0,0,0};
  unsigned int subDomain = 0;

};

std::ostream& operator<< (std::ostream& stream, const Index3D& idx)
{
  stream << "Index: " << idx.index[0] << " " << idx.index[1] << " " << idx.index[2] << "\n";
  return stream;
}

template<typename T,class Lattice>
class SubDomainInformation {

  public:
    SubDomainInformation()
    {
      for (unsigned i=0;i<Lattice::q;++i)
      {
        hasNeighbour[i] = false;
        idNeighbour[i] = std::numeric_limits<unsigned>::max();
      }
    }


    OPENLB_HOST_DEVICE
    bool isLocal (size_t globalIx, size_t globalIy, size_t globalIz, Index3D& localIndex) const 
    {
      if (!localHasGrid)
        return false;
      if (Lattice::d == 3) {
        if(globalIx >= globalIndexStart[0] and globalIx < globalIndexEnd[0]) {
          if(globalIy >= globalIndexStart[1] and globalIy < globalIndexEnd[1]) {
            if(globalIz >= globalIndexStart[2] and globalIz < globalIndexEnd[2])
            {
              localIndex.index[0] = globalIx - globalIndexStart[0] + ghostLayer[0];
              localIndex.index[1] = globalIy - globalIndexStart[1] + ghostLayer[1];
              localIndex.index[2] = globalIz - globalIndexStart[2] + ghostLayer[2];
              localIndex.subDomain = localSubDomain;
              return true;
            }
          }
        }
      }
      else if (Lattice::d == 2) {
        if(globalIx >= globalIndexStart[0] and globalIx < globalIndexEnd[0]) {
          if(globalIy >= globalIndexStart[1] and globalIy < globalIndexEnd[1])
          {
            localIndex.index[0] = globalIx - globalIndexStart[0] + ghostLayer[0];
            localIndex.index[1] = globalIy - globalIndexStart[1] + ghostLayer[1];
            localIndex.index[2] = 0;
            localIndex.subDomain = localSubDomain;
            return true;
          }
        }
      }
      return false;
    }

    OPENLB_HOST_DEVICE
    bool isLocalToGhostLayer (size_t globalIx, size_t globalIy, size_t globalIz, Index3D& localIndex) const 
    {
      if (!localHasGrid)
        return false;
      if (Lattice::d == 3) {
        if(globalIx + ghostLayer[0] >= globalIndexStart[0] and globalIx < globalIndexEnd[0] + ghostLayer[0]) {
          if(globalIy + ghostLayer[1] >= globalIndexStart[1] and globalIy < globalIndexEnd[1] + ghostLayer[1]) {
            if(globalIz + ghostLayer[2] >= globalIndexStart[2] and globalIz < globalIndexEnd[2] + ghostLayer[2])
            {
              localIndex.index[0] = globalIx - globalIndexStart[0] + ghostLayer[0];
              localIndex.index[1] = globalIy - globalIndexStart[1] + ghostLayer[1];
              localIndex.index[2] = globalIz - globalIndexStart[2] + ghostLayer[2];
              localIndex.subDomain = localSubDomain;
              return true;
            }
          }
        }
      }
      else if (Lattice::d == 2) {
        if(globalIx + ghostLayer[0] >= globalIndexStart[0] and globalIx < globalIndexEnd[0] + ghostLayer[0]) {
          if(globalIy + ghostLayer[1] >= globalIndexStart[1] and globalIy < globalIndexEnd[1] + ghostLayer[1])
          {
            localIndex.index[0] = globalIx - globalIndexStart[0] + ghostLayer[0];
            localIndex.index[1] = globalIy - globalIndexStart[1] + ghostLayer[1];
            localIndex.index[2] = 0;
            localIndex.subDomain = localSubDomain;
            return true;
          }
        }
      }
      return false;
    }

    OPENLB_HOST_DEVICE
    bool isLocalToValidGhostLayer (size_t globalIx, size_t globalIy, size_t globalIz, Index3D& localIndex) const 
    {
      unsigned adjustedGhostLayer[3];
      for (int i = 0; i < 3; i++) {
        if (ghostLayer[i] == 0)
          adjustedGhostLayer[i] = 0;
        else
          adjustedGhostLayer[i] = ghostLayer[i]-1;
      }

      if (!localHasGrid)
        return false;
      if (Lattice::d == 3) {
        if(globalIx + adjustedGhostLayer[0] >= globalIndexStart[0] and globalIx < globalIndexEnd[0] + adjustedGhostLayer[0]) {
          if(globalIy + adjustedGhostLayer[1] >= globalIndexStart[1] and globalIy < globalIndexEnd[1] + adjustedGhostLayer[1]) {
            if(globalIz + adjustedGhostLayer[2] >= globalIndexStart[2] and globalIz < globalIndexEnd[2] + adjustedGhostLayer[2])
            {
              localIndex.index[0] = globalIx - globalIndexStart[0] + ghostLayer[0];
              localIndex.index[1] = globalIy - globalIndexStart[1] + ghostLayer[1];
              localIndex.index[2] = globalIz - globalIndexStart[2] + ghostLayer[2];
              localIndex.subDomain = localSubDomain;
              return true;
            }
          }
        }
      }
      else if (Lattice::d == 2) {
        if(globalIx + adjustedGhostLayer[0] >= globalIndexStart[0] and globalIx < globalIndexEnd[0] + adjustedGhostLayer[0]) {
          if(globalIy + adjustedGhostLayer[1] >= globalIndexStart[1] and globalIy < globalIndexEnd[1] + adjustedGhostLayer[1])
          {
            localIndex.index[0] = globalIx - globalIndexStart[0] + ghostLayer[0];
            localIndex.index[1] = globalIy - globalIndexStart[1] + ghostLayer[1];
            localIndex.index[2] = 0;
            localIndex.subDomain = localSubDomain;
            return true;
          }
        }
      }
      return false;
    }

    OPENLB_HOST_DEVICE
    bool isRegionLocal (size_t globalIxStart, size_t globalIyStart, size_t globalIzStart, size_t globalIxEnd, size_t globalIyEnd, size_t globalIzEnd, Index3D& localIndexStart, Index3D& localIndexEnd) const 
    {
      if (!localHasGrid)
        return false;

      bool startLocal = isLocal(globalIxStart, globalIyStart, globalIzStart, localIndexStart);
      bool endLocal = isLocal(globalIxEnd, globalIyEnd, globalIzEnd, localIndexEnd);

      if (Lattice::d == 3) {
        if ( startLocal || 
             endLocal   ||
            (globalIxStart < globalIndexStart[0] && globalIxEnd >= globalIndexEnd[0]) ||
            (globalIyStart < globalIndexStart[1] && globalIyEnd >= globalIndexEnd[1]) ||
            (globalIzStart < globalIndexStart[2] && globalIzEnd >= globalIndexEnd[2])  ) {

          localIndexStart.index[0] = max(globalIxStart, globalIndexStart[0]) - globalIndexStart[0] + ghostLayer[0];
          localIndexStart.index[1] = max(globalIyStart, globalIndexStart[1]) - globalIndexStart[1] + ghostLayer[1];
          localIndexStart.index[2] = max(globalIzStart, globalIndexStart[2]) - globalIndexStart[2] + ghostLayer[2];

          localIndexEnd.index[0] = min(globalIxEnd, globalIndexEnd[0]-1) - globalIndexStart[0] + ghostLayer[0];
          localIndexEnd.index[1] = min(globalIyEnd, globalIndexEnd[1]-1) - globalIndexStart[1] + ghostLayer[1];
          localIndexEnd.index[2] = min(globalIzEnd, globalIndexEnd[2]-1) - globalIndexStart[2] + ghostLayer[2];

          return true;
        }
      }
      else if (Lattice::d == 2) {
        if ( startLocal || 
             endLocal   ||
            (globalIxStart < globalIndexStart[0] && globalIxEnd >= globalIndexEnd[0]) ||
            (globalIyStart < globalIndexStart[1] && globalIyEnd >= globalIndexEnd[1]) ) {

          localIndexStart.index[0] = max(globalIxStart, globalIndexStart[0]) - globalIndexStart[0] + ghostLayer[0];
          localIndexStart.index[1] = max(globalIyStart, globalIndexStart[1]) - globalIndexStart[1] + ghostLayer[1];
          localIndexStart.index[2] = 0;

          localIndexEnd.index[0] = min(globalIxEnd, globalIndexEnd[0]-1) - globalIndexStart[0] + ghostLayer[0];
          localIndexEnd.index[1] = min(globalIyEnd, globalIndexEnd[1]-1) - globalIndexStart[1] + ghostLayer[1];
          localIndexEnd.index[2] = 0;

          return true;
        }
      }
      return false;
    }

    OPENLB_HOST_DEVICE
    bool isRegionLocalToValidGhostLayer (size_t globalIxStart, size_t globalIyStart, size_t globalIzStart, size_t globalIxEnd, size_t globalIyEnd, size_t globalIzEnd, Index3D& localIndexStart, Index3D& localIndexEnd) const 
    {
      if (!localHasGrid)
        return false;

      bool startLocal = isLocalToValidGhostLayer(globalIxStart, globalIyStart, globalIzStart, localIndexStart);
      bool endLocal = isLocalToValidGhostLayer(globalIxEnd, globalIyEnd, globalIzEnd, localIndexEnd);

      size_t adjustedGlobalIndexStart[3];
      size_t adjustedGlobalIndexEnd[3];
      for (int i = 0; i < 3; i++) {
        if (ghostLayer[i] == 0) {
          adjustedGlobalIndexStart[i] = globalIndexStart[i];
          adjustedGlobalIndexEnd[i] = globalIndexEnd[i];
        }
        else {
          adjustedGlobalIndexStart[i] = globalIndexStart[i] - ghostLayer[i] + 1;
          adjustedGlobalIndexEnd[i] = globalIndexEnd[i] + ghostLayer[i] - 1;

          if (globalIndexStart[i] == 0)
            adjustedGlobalIndexStart[i] = globalIndexStart[i];
        }
      }

      if (Lattice::d == 3) {
        if ( startLocal || 
             endLocal   ||
            (globalIxStart < adjustedGlobalIndexStart[0] && globalIxEnd >= adjustedGlobalIndexEnd[0]) ||
            (globalIyStart < adjustedGlobalIndexStart[1] && globalIyEnd >= adjustedGlobalIndexEnd[1]) ||
            (globalIzStart < adjustedGlobalIndexStart[2] && globalIzEnd >= adjustedGlobalIndexEnd[2])  ) {

          localIndexStart.index[0] = max(globalIxStart, adjustedGlobalIndexStart[0]) - globalIndexStart[0] + ghostLayer[0];
          localIndexStart.index[1] = max(globalIyStart, adjustedGlobalIndexStart[1]) - globalIndexStart[1] + ghostLayer[1];
          localIndexStart.index[2] = max(globalIzStart, adjustedGlobalIndexStart[2]) - globalIndexStart[2] + ghostLayer[2];

          localIndexEnd.index[0] = min(globalIxEnd, adjustedGlobalIndexEnd[0]-1) - globalIndexStart[0] + ghostLayer[0];
          localIndexEnd.index[1] = min(globalIyEnd, adjustedGlobalIndexEnd[1]-1) - globalIndexStart[1] + ghostLayer[1];
          localIndexEnd.index[2] = min(globalIzEnd, adjustedGlobalIndexEnd[2]-1) - globalIndexStart[2] + ghostLayer[2];

          return true;
        }
      }
      else if (Lattice::d == 2) {
        if ( startLocal || 
             endLocal   ||
            (globalIxStart < adjustedGlobalIndexStart[0] && globalIxEnd >= adjustedGlobalIndexEnd[0]) ||
            (globalIyStart < adjustedGlobalIndexStart[1] && globalIyEnd >= adjustedGlobalIndexEnd[1]) ) {

          localIndexStart.index[0] = max(globalIxStart, adjustedGlobalIndexStart[0]) - globalIndexStart[0] + ghostLayer[0];
          localIndexStart.index[1] = max(globalIyStart, adjustedGlobalIndexStart[1]) - globalIndexStart[1] + ghostLayer[1];
          localIndexStart.index[2] = 0;

          localIndexEnd.index[0] = min(globalIxEnd, adjustedGlobalIndexEnd[0]-1) - globalIndexStart[0] + ghostLayer[0];
          localIndexEnd.index[1] = min(globalIyEnd, adjustedGlobalIndexEnd[1]-1) - globalIndexStart[1] + ghostLayer[1];
          localIndexEnd.index[2] = 0;

          return true;
        }
      }
      return false;
    }

    OPENLB_HOST_DEVICE
    Index3D getGlobalIndex(Index3D localIndex) const
    {
      return Index3D{globalIndexStart[0]+localIndex[0]-ghostLayer[0],globalIndexStart[1]+localIndex[1]-ghostLayer[1],globalIndexStart[2]+localIndex[2]-ghostLayer[2],localSubDomain};
    }

    OPENLB_HOST_DEVICE
    Index3D localGridSize() const
    {
      return Index3D{globalIndexEnd[0]-globalIndexStart[0]+2*ghostLayer[0],globalIndexEnd[1]-globalIndexStart[1]+2*ghostLayer[1],globalIndexEnd[2]-globalIndexStart[2]+2*ghostLayer[2],localSubDomain};
    }
    
    OPENLB_HOST_DEVICE
    Index3D coreGridSize() const
    {
      return Index3D{globalIndexEnd[0]-globalIndexStart[0],globalIndexEnd[1]-globalIndexStart[1],globalIndexEnd[2]-globalIndexStart[2],localSubDomain};
    }

    OPENLB_HOST_DEVICE
   Index3D coreGridStart() const
   {
    return Index3D(ghostLayer[0],ghostLayer[1],ghostLayer[2],localSubDomain);
   }

    OPENLB_HOST_DEVICE
   Index3D coreGridEnd() const
   {
    return Index3D(coreGridSize()[0]+ghostLayer[0],coreGridSize()[1]+ghostLayer[1],coreGridSize()[2]+ghostLayer[2],localSubDomain);
   }

    OPENLB_HOST_DEVICE
    Index3D validGridStart() const
    {
      Index3D tmp(ghostLayer[0],ghostLayer[1],ghostLayer[2],localSubDomain);
      for (unsigned idx =0;idx<3;++idx)
      {
        int direction[3] = {0,0,0};
        direction[idx] = -1;
        if (hasNeighbour[getPopFromC<Lattice>(direction)])
        {
          tmp[idx]= ghostLayer[idx] ? 1 : tmp[idx];
        }
      }
      return tmp;
    }

    OPENLB_HOST_DEVICE
    Index3D validGridEnd() const
    {
      Index3D tmp(localGridSize()[0]-ghostLayer[0],localGridSize()[1]-ghostLayer[1],localGridSize()[2]-ghostLayer[2],localSubDomain);
      for (unsigned idx =0;idx<3;++idx)
      {
        int direction[3] = {0,0,0};
        direction[idx] = 1;
        if (hasNeighbour[getPopFromC<Lattice>(direction)])
        {
          tmp[idx]= ghostLayer[idx] ? localGridSize()[idx]-1 : tmp[idx];
        }
      }
      return tmp;
    }

  public:
    size_t globalIndexStart[3] = {0,0,0};
    size_t globalIndexEnd[3] = {0,0,0};
    unsigned ghostLayer[3] = {0,0,0};

    unsigned localSubDomain = 0;
    unsigned noSubDomains = 0;

    bool localHasGrid = true;

    bool hasNeighbour[(Lattice::q)];
    unsigned idNeighbour[(Lattice::q)];

    bool hasNeighbourIn[(Lattice::q)];
    unsigned idNeighbourIn[(Lattice::q)];
};

template<typename T,class Lattice>
std::ostream& operator<< (std::ostream& stream, const SubDomainInformation<T,Lattice>& subDomainInfo)
{
  stream << "SubdomainInformation: \n";
  stream << "globalStartIndex( "<< subDomainInfo.globalIndexStart[0] <<" " << subDomainInfo.globalIndexStart[1] << " " << subDomainInfo.globalIndexStart[2] << " ) \n";
  stream << "globalEndIndex( "<< subDomainInfo.globalIndexEnd[0] <<" " << subDomainInfo.globalIndexEnd[1] << " " << subDomainInfo.globalIndexEnd[2] << " ) \n";
  stream << "localSubdomain( "<< subDomainInfo.localSubDomain <<" ) \n";
  stream << "noSubdomains( "<< subDomainInfo.noSubDomains <<" ) \n";
  stream << "hasNeighbour( ";
  for (unsigned i=0;i<Lattice::q;++i)
  {
    stream << subDomainInfo.hasNeighbour[i] << " ";
  }
  stream << " ) \n";
  stream << "idNeighbour (";
  for (unsigned i=0;i<Lattice::q;++i)
  {
    stream << subDomainInfo.idNeighbour[i] << " ";
  }
  stream << " )" << std::endl;

  stream << "localGridSize: ";
  for (unsigned i=0;i<Lattice::d;++i)
  {
    stream << subDomainInfo.localGridSize().index[i] << " ";
  }
  stream << std::endl;

  stream << "ghostLayer: ";
  for (unsigned i=0;i<Lattice::d;++i)
  {
    stream << subDomainInfo.ghostLayer[i] << " ";
  }
  stream << std::endl;

  stream << "validGridStart: ";
  for (unsigned i=0;i<Lattice::d;++i)
  {
    stream << subDomainInfo.validGridStart()[i] << " ";
  }
  stream << std::endl;

  stream << "validGridEnd: ";
  for (unsigned i=0;i<Lattice::d;++i)
  {
    stream << subDomainInfo.validGridEnd()[i] << " ";
  }
  stream << std::endl;

  return stream;
}

} // eon

#endif
