/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- header file
 */
#ifndef ENTROPIC_LATTICE_DESCRIPTORS_H
#define ENTROPIC_LATTICE_DESCRIPTORS_H

#include <vector>
#include "dynamics/latticeDescriptors.h"
namespace olb {

/// Descriptors for the 2D and 3D lattices.
/** \warning Attention: The lattice directions must always be ordered in
 * such a way that c[i] = -c[i+(q-1)/2] for i=1..(q-1)/2, and c[0] = 0 must
 * be the rest velocity. Furthermore, the velocities c[i] for i=1..(q-1)/2
 * must verify
 *  - in 2D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *  - in 3D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *                       || (c[i][0]==0 && c[i][1]==0 && c[i][2]<0)
 * Otherwise some of the code will work erroneously, because the
 * aformentioned relations are taken as given to enable a few
 * optimizations.
*/
namespace descriptors {

struct ScalarExternalField {
  static const int numScalars = 1;
  static const int numSpecies = 1;
  static const int scalarBeginsAt = 0;
  static const int sizeOfScalar   = 1;
};

struct ScalarExternalFieldBase {
  typedef ScalarExternalField ExternalField;
};

struct ForcedEntropicExternalField2d {
  static const int numScalars = 3;
  static const int numSpecies = 2;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 2;
  static const int scalarBeginsAt = 2;
  static const int sizeOfScalar   = 1;
};

struct ForcedEntropicExternalField2dBase {
  typedef ForcedEntropicExternalField2d ExternalField;
};

struct ForcedEntropicExternalField3d {
  static const int numScalars = 4;
  static const int numSpecies = 2;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 3;
  static const int scalarBeginsAt = 3;
  static const int sizeOfScalar   = 1;
};

struct ForcedEntropicExternalField3dBase {
  typedef ForcedEntropicExternalField3d ExternalField;
};

/// D2Q9 lattice
template <typename T> struct EntropicD2Q9Descriptor
  : public D2Q9DescriptorBase<T>, public ScalarExternalFieldBase {
};


/// D3Q15 lattice
template <typename T> struct EntropicD3Q15Descriptor
  : public D3Q15DescriptorBase<T>, public ScalarExternalFieldBase {
};

/// D3Q19 lattice
template <typename T> struct EntropicD3Q19Descriptor
  : public D3Q19DescriptorBase<T>, public ScalarExternalFieldBase {
};

/// D3Q27 lattice
template <typename T> struct EntropicD3Q27Descriptor
  : public D3Q27DescriptorBase<T>, public ScalarExternalFieldBase {
};

/// D2Q9 lattice with force
template <typename T> struct ForcedEntropicD2Q9Descriptor
  : public D2Q9DescriptorBase<T>, public ForcedEntropicExternalField2dBase {
};

/// D3Q19 lattice with force
template <typename T> struct ForcedEntropicD3Q19Descriptor
  : public D3Q19DescriptorBase<T>, public ForcedEntropicExternalField3dBase {
};

}  // namespace descriptors

}  // namespace olb

#endif
