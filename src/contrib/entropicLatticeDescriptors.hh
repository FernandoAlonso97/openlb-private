/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- generic code
 */
#ifndef ENTROPIC_LATTICE_DESCRIPTORS_HH
#define ENTROPIC_LATTICE_DESCRIPTORS_HH

#include "entropicLatticeDescriptors.h"

namespace olb {
namespace descriptors {

// D2Q9 ////////////////////////////////////////////////////////////

template<typename T>
const int EntropicD2Q9Descriptor<T>::c
[EntropicD2Q9Descriptor<T>::q][EntropicD2Q9Descriptor<T>::d] = {
  { 0, 0},
  {-1, 1}, {-1, 0}, {-1,-1}, { 0,-1},
  { 1,-1}, { 1, 0}, { 1, 1}, { 0, 1}
};

template<typename T>
const T EntropicD2Q9Descriptor<T>::t[EntropicD2Q9Descriptor<T>::q] = {
  (T)4/(T)9, (T)1/(T)36, (T)1/(T)9, (T)1/(T)36, (T)1/(T)9,
  (T)1/(T)36, (T)1/(T)9, (T)1/(T)36, (T)1/(T)9
};

template<typename T>
const T EntropicD2Q9Descriptor<T>::invCs2 = (T)3;


// D3Q15 ///////////////////////////////////////////////////////////

template<typename T>
const int EntropicD3Q15Descriptor<T>::c
[EntropicD3Q15Descriptor<T>::q][EntropicD3Q15Descriptor<T>::d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-1,-1,-1}, {-1,-1, 1}, {-1, 1,-1}, {-1, 1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 1, 1, 1}, { 1, 1,-1}, { 1,-1, 1}, { 1,-1,-1},

};

template<typename T>
const T EntropicD3Q15Descriptor<T>::t[EntropicD3Q15Descriptor<T>::q] = {
  (T)2/(T)9,

  (T)1/(T)9, (T)1/(T)9, (T)1/(T)9,
  (T)1/(T)72, (T)1/(T)72, (T)1/(T)72, (T)1/(T)72,

  (T)1/(T)9, (T)1/(T)9, (T)1/(T)9,
  (T)1/(T)72, (T)1/(T)72, (T)1/(T)72, (T)1/(T)72
};

template<typename T>
const T EntropicD3Q15Descriptor<T>::invCs2 = (T)3;


// D3Q19 ///////////////////////////////////////////////////////////

template<typename T>
const int EntropicD3Q19Descriptor<T>::c
[EntropicD3Q19Descriptor<T>::q][EntropicD3Q19Descriptor<T>::d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
  {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
  { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1}
};

template<typename T>
const T EntropicD3Q19Descriptor<T>::t[EntropicD3Q19Descriptor<T>::q] = {
  (T)1/(T)3,

  (T)1/(T)18, (T)1/(T)18, (T)1/(T)18,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,

  (T)1/(T)18, (T)1/(T)18, (T)1/(T)18,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36
};

template<typename T>
const T EntropicD3Q19Descriptor<T>::invCs2 = (T)3;


// D3Q27 ///////////////////////////////////////////////////////////

template<typename T>
const int EntropicD3Q27Descriptor<T>::c
[EntropicD3Q27Descriptor<T>::q][EntropicD3Q27Descriptor<T>::d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
  {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},
  {-1,-1,-1}, {-1,-1, 1}, {-1, 1,-1}, {-1, 1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
  { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1},
  { 1, 1, 1}, { 1, 1,-1}, { 1,-1, 1}, { 1,-1,-1}
};

template<typename T>
const T EntropicD3Q27Descriptor<T>::t[EntropicD3Q27Descriptor<T>::q] = {
  (T)8/(T)27,

  (T)2/(T)27, (T)2/(T)27, (T)2/(T)27,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)216, (T)1/(T)216, (T)1/(T)216, (T)1/(T)216,

  (T)2/(T)27, (T)2/(T)27, (T)2/(T)27,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)216, (T)1/(T)216, (T)1/(T)216, (T)1/(T)216
};

template<typename T>
const T EntropicD3Q27Descriptor<T>::invCs2 = (T)3;


// D2Q9 with force //////////////////////////////////////////////////

template<typename T>
const int ForcedEntropicD2Q9Descriptor<T>::c
[ForcedEntropicD2Q9Descriptor<T>::q][ForcedEntropicD2Q9Descriptor<T>::d] = {
  { 0, 0},
  {-1, 1}, {-1, 0}, {-1,-1}, { 0,-1},
  { 1,-1}, { 1, 0}, { 1, 1}, { 0, 1}
};

template<typename T>
const T ForcedEntropicD2Q9Descriptor<T>::
t[ForcedEntropicD2Q9Descriptor<T>::q] = {
  (T)4/(T)9, (T)1/(T)36, (T)1/(T)9, (T)1/(T)36, (T)1/(T)9,
  (T)1/(T)36, (T)1/(T)9, (T)1/(T)36, (T)1/(T)9
};

template<typename T>
const T ForcedEntropicD2Q9Descriptor<T>::invCs2 = (T)3;


// D3Q19 with force ///////////////////////////////////////////////////

template<typename T>
const int ForcedEntropicD3Q19Descriptor<T>::c
[ForcedEntropicD3Q19Descriptor<T>::q][ForcedEntropicD3Q19Descriptor<T>::d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
  {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
  { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1}
};

template<typename T>
const T ForcedEntropicD3Q19Descriptor<T>::t[ForcedEntropicD3Q19Descriptor<T>::q] = {
  (T)1/(T)3,

  (T)1/(T)18, (T)1/(T)18, (T)1/(T)18,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,

  (T)1/(T)18, (T)1/(T)18, (T)1/(T)18,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,
  (T)1/(T)36, (T)1/(T)36, (T)1/(T)36
};

template<typename T>
const T ForcedEntropicD3Q19Descriptor<T>::invCs2 = (T)3;




}  // namespace descriptors

}  // namespace olb

#endif
