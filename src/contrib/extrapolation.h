/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef EXTRAPOLATION_H
#define EXTRAPOLATION_H

namespace olb {

namespace fd {

/// Second-order extrapolation
template<typename T>
T boundaryExtrapolation(T u_1, T u_2, T u_3)
{
  return (T)3*(u_1-u_2)+u_3;
}

/// Fourth-order extrapolation
template<typename T>
T boundaryExtrapolation4th(T u_1, T u_2, T u_3, T u_4, T u_5)
{
  return (T)6/(T)31*u_5-(T)55/(T)31*u_4-(T)210/(T)31*u_2+(T)130/(T)31*u_1+(T)160/(T)31*u_3;
}

/// Second-order corner extrapolation
template<typename T>
T cornerBoundaryExtrapolation(T u1_1,T u2_1,T u1_2,T u2_2,T u3_1,T u1_3)
{
  return u3_1+u2_2+u1_3+(T)6*u1_1-(T)4*u1_2-(T)4*u2_1;
}
}  // namespace fd

}  // namespace olb


#endif
