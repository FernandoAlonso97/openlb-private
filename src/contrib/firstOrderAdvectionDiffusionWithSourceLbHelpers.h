/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Specialized helper functions for advanced techniques around LB
 * implementations. They implement the physics of the first-order terms
 * of the Chapman-Enskog expansion and are useful whenever a transition
 * from hydrodynamical variables (rho, u) to kinetic variables (f) si to
 * be implemented. Additionally, they are used for the implementation of
 * the stable RLB dynamics.
 *
 * This file is all about efficiency. The generic
 * template code is specialized for commonly used Lattices, so that a
 * maximum performance can be taken out of each case.
 */
#ifndef FIRST_ORDER_ADVECTION_DIFFUSION_WITH_SOURCE_LB_HELPERS_H
#define FIRST_ORDER_ADVECTION_DIFFUSION_WITH_SOURCE_LB_HELPERS_H

#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include "core/cell.h"
#include "core/util.h"

namespace olb {

/// General first-order functions
template<typename T, template<typename U> class Lattice>
struct firstOrderAdevectionDiffusionWithSourceLbHelpers {

  static T fromJ1ToFneq (
    int iPop, const T j1[Lattice<T>::d] )
  {
    typedef Lattice<T> L;
    T fNeq = T();
    for (int iD=0; iD<L::d; ++iD) {
      fNeq += L::c(iPop)[iD] * j1[iD];
    }
    fNeq *= L::t[iPop] * L::invCs2;
    return fNeq;
  }

  static T fromGradRhoToFneq (
    int iPop, const T gradRho[Lattice<T>::d], T omega )
  {
    typedef Lattice<T> L;
    T fNeq = T();
    for (int iD=0; iD<L::d; ++iD) {
      fNeq += L::c(iPop)[iD] * gradRho[iD];
    }
    fNeq *= -L::t[iPop] / omega;
    return fNeq;
  }

};  // struct firstOrderAdevectionDiffusionWithSourceLbHelpers

/// General boundaries first-order functions
template<typename T, template<typename U> class Lattice, int direction, int orientation>
struct flatBoundariesAdevectionDiffusionWithSourceLbHelpers {

  /// interpolates from fneq at second order
  static T computeRhoFromFneq( CellView<T,Lattice> &cell, T omega, T rho1, T rho2 )
  {
    typedef Lattice<T> L;

    std::vector<int> missingDiagonal = util::subIndexOutgoing<L,direction,orientation>();
    int iOpp = util::opposite<L>(missingDiagonal[0]);

    T *u = cell.getExternal(L::ExternalField::velocityBeginsAt);
    T c_u = T();
    for (int iD = 0; iD < L::d; ++iD) {
      c_u += L::c[iOpp][iD] * u[iD];
    }

    T rho = ((T)4*rho1 - rho2 + (T)2*omega/L::t[iOpp] * (cell[iOpp] + L::t[iOpp])) /
            ((T)3+(T)2*omega*((T)1+L::invCs2*c_u));

    return rho;
  }

  /// interpolates from fneq at second order
  static T computeRhoFromJ1( CellView<T,Lattice> &cell, T omega, T rho1, T rho2,
                             std::vector<int> kI, std::vector<int> uI )
  {
    typedef Lattice<T> L;

    T j = T();
    for (unsigned iPop = 0; iPop < kI.size(); ++iPop) {
      j += L::c[kI[iPop]][direction] * (cell[kI[iPop]] + L::t[kI[iPop]]);
    }
    for (unsigned iPop = 0; iPop < uI.size(); ++iPop) {
      int iOpp = util::opposite<L>(uI[iPop]);
      j += L::c[iOpp][direction] * (cell[iOpp] + L::t[iOpp]);
    }

    T *u = cell.getExternal(L::ExternalField::velocityBeginsAt);
    T jStar = T();
    for (unsigned iPop = 0; iPop < kI.size(); ++iPop) {
      T c_u = T();
      for (int iD = 0; iD < L::d; ++iD) {
        c_u += L::c[kI[iPop]][iD] * u[iD];
      }
      jStar += L::c[kI[iPop]][direction] * L::t[kI[iPop]] *
               ((T)1 + c_u * L::invCs2);
    }
    for (unsigned iPop = 0; iPop < uI.size(); ++iPop) {
      int iOpp = util::opposite<L>(uI[iPop]);
      T c_u = T();
      for (int iD = 0; iD < L::d; ++iD) {
        c_u += L::c[iOpp][iD] * u[iD];
      }
      jStar += L::c[iOpp][direction] * L::t[iOpp] * ((T)1+L::invCs2 * c_u);
    }


    T rho = ((T)4*rho1 - rho2 +(T)2*orientation*omega*L::invCs2 * j ) /
            ((T)3+(T)2*orientation*omega*L::invCs2 * jStar);

    return rho;
  }

  /// interpolates from fneq at second order
  static T computeRhoCornerFromJ1( CellView<T,Lattice> &cell, T rho1, T rho2,
                                   std::vector<int> kI, std::vector<int> uI )
  {
    typedef Lattice<T> L;

    T *u = cell.getExternal(L::ExternalField::velocityBeginsAt);
    T j[L::d];
    for (int iA = 0; iA < L::d; ++iA) {
      j[iA] = T();
      for (unsigned iPop = 0; iPop < kI.size(); ++iPop) {
        j[iA] += L::c[kI[iPop]][iA] * (cell[kI[iPop]] + L::t[kI[iPop]]);
      }
      for (unsigned iPop = 0; iPop < uI.size(); ++iPop) {
        int iOpp = util::opposite<L>(uI[iPop]);
        j[iA] += L::c[iOpp][iA] * (cell[iOpp] + L::t[iOpp]);
      }
    }


    T jStar[L::d];
    for (int iA = 0; iA < L::d; ++iA) {
      jStar[iA] = T();
      for (unsigned iPop = 0; iPop < kI.size(); ++iPop) {
        T c_u = T();
        for (int iD = 0; iD < L::d; ++iD) {
          c_u += L::c[kI[iPop]][iD] * u[iD];
        }
        jStar[iA] += L::c[kI[iPop]][iA] * L::t[kI[iPop]] *
                     ((T)1 + c_u * L::invCs2);
      }
      for (unsigned iPop = 0; iPop < uI.size(); ++iPop) {
        int iOpp = util::opposite<L>(uI[iPop]);
        T c_u = T();
        for (int iD = 0; iD < L::d; ++iD) {
          c_u += L::c[iOpp][iD] * u[iD];
        }
        jStar[iA] += L::c[iOpp][iA] * L::t[iOpp] * ((T)1+L::invCs2 * c_u);
      }
    }

    T omega = cell.getDynamics()->getOmega();

    T rho = ((T)4*rho1 - rho2 +(T)2*omega*L::invCs2 * (direction*j[0] + orientation*j[1]) ) /
            ((T)3+(T)2*omega*L::invCs2 * (direction*jStar[0] + orientation*jStar[1]));

    return rho;
  }

  /// interpolates from fneq at first order
  static T computeRhoFromFneq( CellView<T,Lattice> &cell, T omega, T rho1)
  {
    typedef Lattice<T> L;

    std::vector<int> missingDiagonal = util::subIndexOutgoing<L,direction,orientation>();
    int iOpp = util::opposite<L>(missingDiagonal[0]);

    T *u = cell.getExternal(L::ExternalField::velocityBeginsAt);
    T c_u = T();
    for (int iD = 0; iD < L::d; ++iD) {
      c_u += L::c[iOpp][iD] * u[iD];
    }

    T rho = (rho1 + omega/L::t[iOpp] * (cell[iOpp] + L::t[iOpp])) /
            ((T)1+omega*((T)1+L::invCs2*c_u));

    return rho;
  }

};  // struct flatBoundariesAdevectionDiffusionWithSourceLbHelpers

}  // namespace olb

// The specialized code is directly included. That is because we never want
// it to be precompiled so that in both the precompiled and the
// "include-everything" version, the compiler can apply all the
// optimizations it wants.
// #include "firstOrderAdevectionDiffusionWithSourceLbHelpers2D.h"
// #include "firstOrderAdevectionDiffusionWithSourceLbHelpers3D.h"

#endif
