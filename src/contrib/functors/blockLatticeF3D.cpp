/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#include "blockLatticeF3D.h"
#include "blockLatticeF3D.hh"
#include "core/latticeDescriptors.h"

namespace olb {

template class LatticeF3D<double,descriptors::D3Q19Descriptor>;

// 3rd level classes: LatticeF3D
template class BlockLatticeF3D<double,descriptors::D3Q19Descriptor>;
template class BlockLatticeDensity3D<double,descriptors::D3Q19Descriptor>;
template class BlockLatticeVelocity3D<double,descriptors::D3Q19Descriptor>;
template class DataLatticeF3D<double,descriptors::D3Q19Descriptor>;
template class PhysBlockLatticeF3D<double,descriptors::D3Q19Descriptor>;

// wrapper classes
template class BlockLatticeVelocityFromAnalytical3D<double,descriptors::D3Q19Descriptor>;
template class BlockLatticeForceFromAnalytical3D<double,descriptors::D3Q19Descriptor>;
template class BlockLatticePressureFromAnalytical3D<double,descriptors::D3Q19Descriptor>;

}

