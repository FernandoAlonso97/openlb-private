/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef BLOCK_LATTICE_F_3D_H
#define BLOCK_LATTICE_F_3D_H

#include<vector>    // for generic i/o
#include<cmath>     // for lpnorm

#include "functors/genericF.h"
#include "functors/analytical/analyticalF.h"
#include "core/blockGeometry3D.h"
#include "core/blockLatticeStructure3D.h"
#include "core/units.h"

/** Note: Throughout the whole source code directory genericFunctions, the
 *  template parameters for i/o dimensions are:
 *           F: S^n -> T^m  (S=source, T=target)
 */

namespace olb {

////////////////////////////////////////////////////////////////////////////////
// 2nd level classes
// note: for LatticeFunctions the number indicates the SOURCE dimension,
//       target dim depends on return variable type, so std::vector<T> is used

/// represents discrete functions that are defined on a lattice
template < typename T, template <typename U> class DESCRIPTOR>
class LatticeF3D : public GenericF<T,T> {
protected:
  BlockLatticeStructure3D<T,DESCRIPTOR>* lattice;
  BlockGeometry3D* bg;
public:
  LatticeF3D(BlockLattice3D<T,DESCRIPTOR>* _lattice, BlockGeometry3D* bg) { };
  //virtual std::vector<T> operator() (T ix, T iy, T iz) = 0;
  //virtual std::vector<T> operator() () = 0;
  virtual std::vector<T> operator() (int ix, int iy, int iz) = 0;
};


////////////////////////////////////////////////////////////////////////////////
// 3rd level classes: LatticeF3D

/// represents all functors that operate on a Lattice in general,
/// e.g. getVelocity(), getForce(), getPressure()
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticeF3D : public LatticeF3D<T,DESCRIPTOR> {
protected:
  BlockLattice3D<T,DESCRIPTOR>* lattice;
public:
  BlockLatticeF3D(BlockLattice3D<T,DESCRIPTOR>* _lattice);
  virtual std::vector<T> operator() (int ix, int iy, int iz) = 0;
};


/// functor to get pointwise density on lattice
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticeDensity3D : public BlockLatticeF3D<T,DESCRIPTOR> {
public:
  BlockLatticeDensity3D(BlockLattice3D<T,DESCRIPTOR>* _lattice);
  std::vector<T> operator() (int ix, int iy, int iz);
};


/// functor to get pointwise velocity on lattice
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticeVelocity3D : public BlockLatticeF3D<T,DESCRIPTOR> {
public:
  BlockLatticeVelocity3D(BlockLattice3D<T,DESCRIPTOR>* _lattice);
  std::vector<T> operator() (int ix, int iy, int iz);
};

/*
/// functor to get pointwise velocity on lattice
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticeMeanEnstrophy3D : public BlockLatticeF3D<T,DESCRIPTOR> {
public:
  BlockLatticeMeanEnstrophy3D(BlockLattice3D<T,DESCRIPTOR>* _lattice)
    : BlockLatticeF3D<T,DESCRIPTOR>(_lattice) { }
  std::vector<T> operator() (int ix, int iy, int iz) {

    BlockLatticeVelocity3D<T,DESCRIPTOR> vel(this->lattice);

    int nx = this->lattice->getNx()-1;
    int ny = this->lattice->getNy()-1;
    int nz = this->lattice->getNz()-1;

  T enstrophy = T();
  for (int iX=0; iX<nx; ++iX) {
    for (int iY=0; iY<ny; ++iY) {
      for (int iZ=0; iZ<nz; ++iZ) {
        T dxuy = (
                   vel(iX+1,iY+1,iZ  )[1]
                   + vel(iX+1,iY  ,iZ  )[1]
                   + vel(iX+1,iY+1,iZ+1)[1]
                   + vel(iX+1,iY  ,iZ+1)[1]
                   - vel(iX  ,iY+1,iZ  )[1]
                   - vel(iX  ,iY  ,iZ  )[1]
                   - vel(iX  ,iY+1,iZ+1)[1]
                   - vel(iX  ,iY  ,iZ+1)[1] ) / (T)4;
        T dxuz = (
                   vel(iX+1,iY+1,iZ  )[2]
                   + vel(iX+1,iY  ,iZ  )[2]
                   + vel(iX+1,iY+1,iZ+1)[2]
                   + vel(iX+1,iY  ,iZ+1)[2]
                   - vel(iX  ,iY+1,iZ  )[2]
                   - vel(iX  ,iY  ,iZ  )[2]
                   - vel(iX  ,iY+1,iZ+1)[2]
                   - vel(iX  ,iY  ,iZ+1)[2] ) / (T)4;
        T dyux = (
                   vel(iX  ,iY+1,iZ  )[0]
                   + vel(iX+1,iY+1,iZ  )[0]
                   + vel(iX  ,iY+1,iZ+1)[0]
                   + vel(iX+1,iY+1,iZ+1)[0]
                   - vel(iX  ,iY  ,iZ  )[0]
                   - vel(iX+1,iY  ,iZ  )[0]
                   - vel(iX  ,iY  ,iZ+1)[0]
                   - vel(iX+1,iY  ,iZ+1)[0] ) / (T)4;
        T dyuz = (
                   vel(iX  ,iY+1,iZ  )[2]
                   + vel(iX+1,iY+1,iZ  )[2]
                   + vel(iX  ,iY+1,iZ+1)[2]
                   + vel(iX+1,iY+1,iZ+1)[2]
                   - vel(iX  ,iY  ,iZ  )[2]
                   - vel(iX+1,iY  ,iZ  )[2]
                   - vel(iX  ,iY  ,iZ+1)[2]
                   - vel(iX+1,iY  ,iZ+1)[2] ) / (T)4;
        T dzux = (
                   vel(iX  ,iY  ,iZ+1)[0]
                   + vel(iX+1,iY+1,iZ+1)[0]
                   + vel(iX  ,iY+1,iZ+1)[0]
                   + vel(iX+1,iY  ,iZ+1)[0]
                   - vel(iX  ,iY  ,iZ  )[0]
                   - vel(iX+1,iY  ,iZ  )[0]
                   - vel(iX  ,iY+1,iZ  )[0]
                   - vel(iX+1,iY+1,iZ  )[0] ) / (T)4;
        T dzuy = (
                   vel(iX  ,iY  ,iZ+1)[1]
                   + vel(iX+1,iY+1,iZ+1)[1]
                   + vel(iX  ,iY+1,iZ+1)[1]
                   + vel(iX+1,iY  ,iZ+1)[1]
                   - vel(iX  ,iY  ,iZ  )[1]
                   - vel(iX+1,iY  ,iZ  )[1]
                   - vel(iX  ,iY+1,iZ  )[1]
                   - vel(iX+1,iY+1,iZ  )[1] ) / (T)4;

        T omegaX = dyuz - dzuy;
        T omegaY = dzux - dxuz;
        T omegaZ = dxuy - dyux;

        enstrophy += omegaX*omegaX+omegaY*omegaY+omegaZ*omegaZ;
      }
    }
  }
  enstrophy /= (2*nx*ny*nz);

  std::vector<T> v(1);
  v[0] = enstrophy;
  return v;
  }
};

*/

/// a class enabling real pointwise copying of data into an own array/field
/// TODO: fill with code
template <typename T, template <typename U> class DESCRIPTOR>
class DataLatticeF3D : public LatticeF3D<T,DESCRIPTOR> {
private:
  T** dataField;
public:
  DataLatticeF3D() { } // initialize dataField
  ~DataLatticeF3D() { } // uninitialize dataField
  DataLatticeF3D(LatticeF3D<T,DESCRIPTOR>* rhs) { }
  std::vector<T> operator() (T ix, T iy, T iz);
};


/// a class used to convert analytical functions to lattice functions
/// input functions are interpreted as SI->SI units, the resulting lattice
/// function will map lattice->lattice units
template <typename T, template <typename U> class DESCRIPTOR>
class PhysBlockLatticeF3D : public LatticeF3D<T,DESCRIPTOR> {
protected:
  AnalyticalF3D<T,T>* fx;
  AnalyticalF3D<T,T>* fy;
  AnalyticalF3D<T,T>* fz;
  BlockGeometry3D* bg;
  LBconverter<T>* converter;
public:
  PhysBlockLatticeF3D(AnalyticalF3D<T,T>* _fx,
                      AnalyticalF3D<T,T>* _fy,
                      AnalyticalF3D<T,T>* _fz,
                      BlockGeometry3D* _bg,
                      LBconverter<T>* _converter);
  virtual std::vector<T> operator() (T ix, T iy, T iz) = 0;
};


/// wrapper class for conversion from analytical (SI units) to
/// velocity functions (LB units)
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticeVelocityFromAnalytical3D : public PhysBlockLatticeF3D<T,DESCRIPTOR> {
public:
  BlockLatticeVelocityFromAnalytical3D(AnalyticalF3D<T,T>* fx,
                                       AnalyticalF3D<T,T>* fy,
                                       AnalyticalF3D<T,T>* fz,
                                       BlockGeometry3D* bg,
                                       LBconverter<T>* converter);
  std::vector<T> operator() (T ix, T iy, T iz);
};


/// wrapper class for conversion from analytical (SI units) to
/// force functions (LB units)
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticeForceFromAnalytical3D : public PhysBlockLatticeF3D<T,DESCRIPTOR> {
public:
  BlockLatticeForceFromAnalytical3D(AnalyticalF3D<T,T>* f,
                                    BlockGeometry3D* bg,
                                    LBconverter<T>* converter);
  std::vector<T> operator() (T ix, T iy, T iz);
};


/// wrapper class for conversion from analytical to (SI units) pressure
/// functions (LB units)
template <typename T, template <typename U> class DESCRIPTOR>
class BlockLatticePressureFromAnalytical3D : public PhysBlockLatticeF3D<T,DESCRIPTOR> {
public:
  BlockLatticePressureFromAnalytical3D(AnalyticalF3D<T,T>* f,
                                       BlockGeometry3D* bg,
                                       LBconverter<T>* converter);
  std::vector<T> operator() (T ix, T iy, T iz);
};


} // end namespace olb

#endif
