/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef BLOCK_LATTICE_F_3D_HH
#define BLOCK_LATTICE_F_3D_HH

#include<vector>    // for generic i/o
#include<cmath>     // for lpnorm

#include "blockLatticeF3D.h"
#include "functors/genericF.h"
#include "functors/analytical/analyticalF.h"
#include "core/blockGeometry3D.h"
#include "core/blockLatticeStructure3D.h"
#include "core/units.h"

namespace olb {


// BlockLatticeF3D
template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeF3D<T,DESCRIPTOR>::BlockLatticeF3D(BlockLattice3D<T,DESCRIPTOR>* _lattice)
  : LatticeF3D<T,DESCRIPTOR>(_lattice,NULL), lattice(_lattice) { }



// BlockLatticeDensity3D
template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeDensity3D<T,DESCRIPTOR>::BlockLatticeDensity3D(BlockLattice3D<T,DESCRIPTOR>* _lattice)
  : BlockLatticeF3D<T,DESCRIPTOR>(_lattice) { }

template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticeDensity3D<T,DESCRIPTOR>::operator() (int ix, int iy, int iz)
{
  return std::vector<T>(1,BlockLatticeF3D<T,DESCRIPTOR>::lattice->get(ix, iy, iz).computeRho());
}



// BlockLatticeVelocity3D
template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeVelocity3D<T,DESCRIPTOR>::BlockLatticeVelocity3D(BlockLattice3D<T,DESCRIPTOR>* _lattice)
  : BlockLatticeF3D<T,DESCRIPTOR>(_lattice) { }

template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticeVelocity3D<T,DESCRIPTOR>::operator() (int ix, int iy, int iz)
{
  T u[3];
  BlockLatticeF3D<T,DESCRIPTOR>::lattice->get(ix,iy,iz).computeU(u);
  std::vector<T> v(3);
  v[0] = u[0];
  v[1] = u[1];
  v[2] = u[2];
  return v;
}



// DataLatticeF3D
template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> DataLatticeF3D<T,DESCRIPTOR>::operator() (T ix, T iy, T iz) { }



// PhysBlockLatticeF3D
template <typename T, template <typename U> class DESCRIPTOR>
PhysBlockLatticeF3D<T,DESCRIPTOR>::PhysBlockLatticeF3D(AnalyticalF3D<T,T>* _fx,
    AnalyticalF3D<T,T>* _fy,
    AnalyticalF3D<T,T>* _fz,
    BlockGeometry3D* _bg,
    LBconverter<T>* _converter)
  : LatticeF3D<T,DESCRIPTOR>(NULL,_bg), fx(_fx), fy(_fy), fz(_fz), bg(_bg), converter(_converter) { }



// BlockLatticeVelocityFromAnalytical3D
template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticeVelocityFromAnalytical3D<T,DESCRIPTOR>::operator() (T ix, T iy, T iz)
{
  // convert to physical coordinates
  T physX = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[0];
  T physY = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[1];
  T physZ = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[2];
  // call fx, fy with physical coordinates
  T resultX = PhysBlockLatticeF3D<T,DESCRIPTOR>::fx->operator()(physX, physY, physZ);
  T resultY = PhysBlockLatticeF3D<T,DESCRIPTOR>::fy->operator()(physX, physY, physZ);
  T resultZ = PhysBlockLatticeF3D<T,DESCRIPTOR>::fz->operator()(physX, physY, physZ);
  // return value in lattice boltzmann units
  std::vector<T> physResult(3);
  physResult[0] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticeVelocity(resultX);
  physResult[1] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticeVelocity(resultY);
  physResult[2] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticeVelocity(resultZ);
  return physResult;
}

template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeVelocityFromAnalytical3D<T,DESCRIPTOR>::BlockLatticeVelocityFromAnalytical3D(AnalyticalF3D<T,T>* fx, AnalyticalF3D<T,T>* fy, AnalyticalF3D<T,T>* fz, BlockGeometry3D* bg, LBconverter<T>* converter)
  : PhysBlockLatticeF3D<T,DESCRIPTOR>(fx, fy, fz, bg, converter) { }



// BlockLatticeForceFromAnalytical3D
template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeForceFromAnalytical3D<T,DESCRIPTOR>::BlockLatticeForceFromAnalytical3D(AnalyticalF3D<T,T>* f,
    BlockGeometry3D* bg,
    LBconverter<T>* converter)
  : PhysBlockLatticeF3D<T,DESCRIPTOR>(f, f, f, bg, converter) { }

template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticeForceFromAnalytical3D<T,DESCRIPTOR>::operator() (T ix, T iy, T iz)
{
  // convert to physical coordinates
  T physX = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[0];
  T physY = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[1];
  T physZ = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[2];
  // call fx, fy with physical coordinates
  T resultX = PhysBlockLatticeF3D<T,DESCRIPTOR>::fx->operator()(physX, physY, physZ);
  T resultY = PhysBlockLatticeF3D<T,DESCRIPTOR>::fy->operator()(physX, physY, physZ);
  T resultZ = PhysBlockLatticeF3D<T,DESCRIPTOR>::fz->operator()(physX, physY, physZ);
  // return value in lattice boltzmann units
  std::vector<T> physResult(3);
  physResult[0] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticeForce(resultX);
  physResult[1] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticeForce(resultY);
  physResult[2] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticeForce(resultZ);
  return physResult;
}


// BlockLatticePressureFromAnalytical3D
template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticePressureFromAnalytical3D<T,DESCRIPTOR>::BlockLatticePressureFromAnalytical3D(AnalyticalF3D<T,T>* f, BlockGeometry3D* bg, LBconverter<T>* converter)
  : PhysBlockLatticeF3D<T,DESCRIPTOR>(f, f, f, bg, converter) { }

template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticePressureFromAnalytical3D<T,DESCRIPTOR>::operator() (T ix, T iy, T iz)
{
  // convert to physical coordinates
  T physX = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[0];
  T physY = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[1];
  T physZ = PhysBlockLatticeF3D<T,DESCRIPTOR>::bg->physCoords(ix,iy,iz)[2];
  // call f with physical coordinates
  T result = PhysBlockLatticeF3D<T,DESCRIPTOR>::fx->operator()(physX, physY, physZ);
  // return value in lattice boltzmann units
  std::vector<T> physResult(1);
  physResult[0] = PhysBlockLatticeF3D<T,DESCRIPTOR>::converter->latticePressure(result);
  return physResult;
}


} // end namespace olb

#endif
