/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Patrick Nathen, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#include "functors/lattice/turbulentF3D.h"
#include "functors/lattice/turbulentF3D.hh"
#include "dynamics/latticeDescriptors.h"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeLocalF3D.hh"

namespace olb {

template class SuperLatticeYplus3D<double,descriptors::D3Q19Descriptor>;
template class SuperLatticeQCrit3D<double,descriptors::D3Q19Descriptor>;
template class SuperLatticeDynSmagoConst<double,descriptors::D3Q19Descriptor>;


template class BlockLatticeDynSmagoConst<double,descriptors::D3Q19Descriptor>;
template class BlockLatticeADMFilter<double,descriptors::D3Q19Descriptor>;


//template class BlockLatticeYplus3D<double,descriptors::D3Q19Descriptor>;
//template class BlockLatticeQCrit3D<double,descriptors::D3Q19Descriptor>;

}
