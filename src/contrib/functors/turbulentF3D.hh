/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Patrick Nathen, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef TURBULENT_F_3D_HH
#define TURBULENT_F_3D_HH

#include<vector>
#include<cmath>
#include<string>

#include "functors/lattice/turbulentF3D.h"
#include "functors/lattice/superLatticeBaseF3D.h"
#include "functors/lattice/superLatticeLocalF3D.h"
#include "contrib/viscoFiniteDifference.h"
#include "core/finiteDifference3D.h"
#include "core/superLattice3D.h"
#include "dynamics/lbHelpers.h"  // for computation of lattice rho and velocity
#include "io/stlReader.h"
#include "functors/lattice/blockLatticeLocalF3D.h"
#include "functors/lattice/blockLatticeBaseF3D.h"
#include "core/blockLattice3D.h"
#include "core/blockLatticeStructure3D.h"

/** To enable simulations in a rotating frame, the axis is set in the
  * constructor with axisPoint and axisDirection. The axisPoint can be the
  * coordinate of any point where the axis pass. The axisDirection has to,
  * be a normal. The pulse w is in rad/s. It determines the pulse speed by
  * its norm while the trigonometric or clockwise direction is determined by
  * its sign: when the axisDirection is pointing "toward you", a
  * positive pulse make it turn in the trigonometric way. It has to be noted
  * that putting both axisDirection into -axisDirection and w into -w yields
  * an exactly identical situation.
  */

namespace olb {


///////////////////////////// SuperLatticeYplus3D //////////////////////////////


template <typename T, template <typename U> class DESCRIPTOR>
SuperLatticeYplus3D<T,DESCRIPTOR>::SuperLatticeYplus3D(SuperLattice3D<T,DESCRIPTOR>& sLattice,
    const LBconverter<T>& converter, SuperGeometry3D<T>& superGeometry,
    STLreader<T>& stlReader, int material )
  : SuperLatticePhysF3D<T,DESCRIPTOR>(sLattice,converter,1),
    _superGeometry(superGeometry), _stlReader(stlReader), _material(material)
{
  this->_name = "yPlus";
}


template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> SuperLatticeYplus3D<T,DESCRIPTOR>::operator() (std::vector<int> input)
{
  int globIC = input[0];
  int locix = input[1];
  int lociy = input[2];
  int lociz = input[3];

  std::vector<T> output(1,T());

  if ( this->_sLattice.getLoadBalancer().rank(globIC) == singleton::mpi().getRank() ) { //if 1

    T normal[3] = {0,0,0};
    T counter = T();
    T dist;
    if (_superGeometry.getMaterial(input) == 1) { //if 2
      for (int iPop = 0; iPop < DESCRIPTOR<T>::q; iPop++) {
        if (_superGeometry.getMaterial(input[0],
                                       input[1]+DESCRIPTOR<T>::c(iPop)[0],
                                       input[2]+DESCRIPTOR<T>::c(iPop)[1],
                                       input[3]+DESCRIPTOR<T>::c(iPop)[2]) == _material) {
          counter++;
          normal[0] += DESCRIPTOR<T>::c(iPop)[0];
          normal[1] += DESCRIPTOR<T>::c(iPop)[1];
          normal[2] += DESCRIPTOR<T>::c(iPop)[2];
        }
      }
      if (counter != 0) { //if 3
        // get physical Coordinates at intersection
        std::vector<T> physR = _superGeometry.getCuboidGeometry().getPhysR(input);
        T voxelSize = _superGeometry.getDeltaR();
        // calculate distance to STL file
        if ( _stlReader.readDistanceCuboid(physR[0], physR[1], physR[2],
                                           voxelSize*normal[0]*1.5, voxelSize*normal[1]*1.5, voxelSize*normal[2]*1.5,
                                           this->_sLattice.getLoadBalancer().loc(globIC), dist)) {

          T norm = sqrt(normal[0]*normal[0]+normal[1]*normal[1]+normal[2]*normal[2]);
          if (norm != 0) {
            normal[0] /= norm;
            normal[1] /= norm;
            normal[2] /= norm;
          }

          // call stress at this point
          T rho;
          T u[3];
          T pi[6];
          this->_sLattice.getBlockLattice(this->_sLattice.getLoadBalancer().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho, u);
          this->_sLattice.getBlockLattice(this->_sLattice.getLoadBalancer().loc(globIC)).computeStress(locix, lociy, lociz, pi);

          //  Totel Stress projected from cell in normal direction on obstacle
          T Rx = pi[0]*normal[0]+pi[1]*normal[1]+pi[2]*normal[2];
          T Ry = pi[1]*normal[0]+pi[3]*normal[1]+pi[4]*normal[2];
          T Rz = pi[2]*normal[0]+pi[4]*normal[1]+pi[5]*normal[2];

          // Stress appearing as pressure in corresponding direction is calculated and substracted
          T R_res_pressure = normal[0]*pi[0]*normal[0]+ normal[0]*pi[1]*normal[1] + normal[0]*pi[2]*normal[2] +
                             normal[1]*pi[1]*normal[0]+ normal[1]*pi[3]*normal[1] + normal[1]*pi[4]*normal[2] +
                             normal[2]*pi[2]*normal[0]+ normal[2]*pi[4]*normal[1] + normal[2]*pi[5]*normal[2];

          Rx -= R_res_pressure *normal[0];
          Ry -= R_res_pressure *normal[1];
          Rz -= R_res_pressure *normal[2];

          T tau_wall = sqrt(Rx*Rx+Ry*Ry+Rz*Rz)*this->_converter.getLatticeNu();
          T u_tau = sqrt(tau_wall/rho);
          //y_plus
          output[0] = dist*u_tau/this->_converter.getCharNu();
        } // if 4
      }
    }
  }
  return output; // empty vector
} // operator()


///////////////////////////// SuperLatticeQCrit3D //////////////////////////////


template <typename T, template <typename U> class DESCRIPTOR>
SuperLatticeQCrit3D<T,DESCRIPTOR>::SuperLatticeQCrit3D(
  SuperLattice3D<T,DESCRIPTOR>& sLattice, LBconverter<T>& converter, SuperGeometry3D<T>& sg)
  : SuperLatticePhysF3D<T,DESCRIPTOR>(sLattice,converter,1), _superGeometry(sg)
{
  this->_name = "qCrit";
}


template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> SuperLatticeQCrit3D<T,DESCRIPTOR>::operator() (std::vector<int> input)
{
  int foundIC = (int)input[0];
  int locix = input[1];
  int lociy = input[2];
  int lociz = input[3];
  //int globIC = input[0];
  //int count;
  if ( this->_sLattice.getLoadBalancer().rank(foundIC) == singleton::mpi().getRank() ) {

    /// material fluid???
    if (_superGeometry.getMaterial(input[0], input[1]+1, input[2]+1, input[3]+1) == 1) {



      /////CORE LOOOOOOOP
      //T rho;
      T u_x_plus[3], u_x_minus[3], u_y_plus[3], u_y_minus[3],u_z_plus[3], u_z_minus[3];
      std::vector<T> pos(3, 0.);

      //T loc_pos[3];



      // pos[0] = this->_converter->latticeLength(input[1] - _superGeometry.getPositionX());
      // pos[1] = this->_converter->latticeLength(input[2] - _superGeometry.getPositionY());
      // pos[2] = this->_converter->latticeLength(input[3] - _superGeometry.getPositionZ());

      //      Cell<T, DESCRIPTOR> cell;
      // T x0 = floor(pos[0]);
      // T y0 = floor(pos[1]);
      // T z0 = floor(pos[2]);

      //BlockLattice
      // loc_pos[0] = x0 -globX + this->_sLattice.getOverlap()*
      //     this->_sLattice.get_cGeometry().get(foundIC).getDeltaR();
      // loc_pos[1] = y0 - globY + this->_sLattice.getOverlap()*
      //     this->_sLattice.get_cGeometry().get(foundIC).getDeltaR();
      // loc_pos[2] = z0 - globZ + this->_sLattice.getOverlap()*
      //     this->_sLattice.get_cGeometry().get(foundIC).getDeltaR();

      foundIC = this->_sLattice.getLoadBalancer().loc(foundIC);
      //std::cout << "[SuperLatticePhysVelocityInterp3D] "<< " " <<"locx: "<<globX<<" locy: "<<globY<<" locz: "<<globZ << std::endl;

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(foundIC)).get(loc_pos[0]+1, loc_pos[1], loc_pos[2]).computeRhoU(rho,u_x_plus);
      // //this->_sLattice.getExtendedBlockLattice(foundIC).get(loc_pos[0]  , loc_pos[1], loc_pos[2]).computeU(u_x);
      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(foundIC)).get(loc_pos[0]+1, loc_pos[1], loc_pos[2]).computeRhoU(rho,u_x_minus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(foundIC)).get(loc_pos[0], loc_pos[1]+1, loc_pos[2]).computeRhoU(rho,u_y_plus);
      // //this->_sLattice.getExtendedBlockLattice(foundIC).get(loc_pos[0]  , loc_pos[1]  , loc_pos[2]).computeU(loc_vel[1]);
      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(foundIC)).get(loc_pos[0], loc_pos[1]-1, loc_pos[2]).computeRhoU(rho,u_y_minus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(foundIC)).get(loc_pos[0], loc_pos[1], loc_pos[2]+1).computeRhoU(rho,u_z_plus);
      // //this->_sLattice.getExtendedBlockLattice(foundIC).get(loc_pos[0]  , loc_pos[1]  , loc_pos[2]  ).computeU(u_y_plus);
      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(foundIC)).get(loc_pos[0], loc_pos[1], loc_pos[2]-1).computeRhoU(rho,u_z_minus);

      std::cout <<"dafuq x ?????: " <<locix<<std::endl;
      this->_sLattice.getExtendedBlockLattice(foundIC).get(locix+this->_sLattice.getOverlap()+1,   lociy  ,  lociz  ).computeU(u_x_plus);
      //this->_sLattice.getExtendedBlockLattice(foundIC).get(loc_pos[0]  ,   loc_pos[1]  ,  loc_pos[2]  ).computeU(u_x_plus[0]);
      this->_sLattice.getExtendedBlockLattice(foundIC).get(locix+this->_sLattice.getOverlap()-1,   lociy  ,  lociz  ).computeU(u_x_minus);

      this->_sLattice.getExtendedBlockLattice(foundIC).get(locix  ,   lociy+this->_sLattice.getOverlap()+1,  lociz  ).computeU(u_y_plus);
      //this->_sLattice.getExtendedBlockLattice(foundIC).get(loc_pos[0]  ,   loc_pos[1]  ,  loc_pos[2]  ).computeU(loc_vel[4]);
      this->_sLattice.getExtendedBlockLattice(foundIC).get(locix  ,   lociy+this->_sLattice.getOverlap()-1,  lociz  ).computeU(u_y_minus);

      this->_sLattice.getExtendedBlockLattice(foundIC).get(locix  ,   lociy  ,  lociz+this->_sLattice.getOverlap()+1).computeU(u_z_plus);
      //this->_sLattice.getExtendedBlockLattice(foundIC).get(loc_pos[0]  ,   loc_pos[1],  loc_pos[2]  ).computeU(u_y_plus);
      this->_sLattice.getExtendedBlockLattice(foundIC).get(locix  ,   lociy  ,  lociz+this->_sLattice.getOverlap()-1).computeU(u_z_minus);
      std::cout <<"dafuq x ?????: " <<lociy<<std::endl;

      //           int globX_pos = (int)cGeometry.get(load.glob(globIC)).get_globPosX()  + 1;
      //           int globX     = (int)cGeometry.get(load.glob(globIC)).get_globPosX()     ;
      //           int globX_neg = (int)cGeometry.get(load.glob(globIC)).get_globPosX()  - 1;

      //           int globY_pos = (int)cGeometry.get(load.glob(globIC)).get_globPosY()   + 1;
      //           int globY     = (int)cGeometry.get(load.glob(globIC)).get_globPosY()      ;
      //           int globY_neg = (int)cGeometry.get(load.glob(globIC)).get_globPosY()   - 1;

      //           int globZ_pos = (int)cGeometry.get(load.glob(globIC)).get_globPosZ()   + 1;
      //           int globZ     = (int)cGeometry.get(load.glob(globIC)).get_globPosZ()      ;
      //           int globZ_neg = (int)cGeometry.get(load.glob(globIC)).get_globPosZ()   - 1;

      // int globX_pos = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosX() + locix + 1;
      // int globX     = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosX() + locix    ;
      // int globX_neg = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosX() + locix - 1;

      // int globY_pos = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosY() + lociy + 1;
      // int globY     = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosY() + lociy    ;
      // int globY_neg = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosY() + lociy - 1;

      // int globZ_pos = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosZ() + lociz + 1;
      // int globZ     = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosZ() + lociz    ;
      // int globZ_neg = (int)this->_sLattice.get_cGeometry().get(globIC).get_globPosZ() + lociz - 1;


      //    //
      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(globX_pos, globY, globZ).computeRhoU(rho,u_x_plus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(globX_neg, globY, globZ).computeRhoU(rho,u_x_minus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(globX, globY_pos, globZ).computeRhoU(rho,u_y_plus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(globX, globY_neg, globZ).computeRhoU(rho,u_y_minus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(globX, globY, globZ_pos).computeRhoU(rho,u_z_plus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(globX, globY, globZ_neg).computeRhoU(rho,u_z_minus);




      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho,u_x_plus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho,u_x_minus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho,u_y_plus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho,u_y_minus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho,u_z_plus);

      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeRhoU(rho,u_z_minus);

      T dUdX = (this->_converter.physVelocity(u_x_plus[0]-u_x_minus[0]))/(2*this->_converter.getDeltaX());
      T dUdY = (this->_converter.physVelocity(u_y_plus[0]-u_y_minus[0]))/(2*this->_converter.getDeltaX());
      T dUdZ = (this->_converter.physVelocity(u_z_plus[0]-u_z_minus[0]))/(2*this->_converter.getDeltaX());

      T dVdX = (this->_converter.physVelocity(u_x_plus[1]-u_x_minus[1]))/(2*this->_converter.getDeltaX());
      T dVdY = (this->_converter.physVelocity(u_y_plus[1]-u_y_minus[1]))/(2*this->_converter.getDeltaX());
      T dVdZ = (this->_converter.physVelocity(u_z_plus[1]-u_z_minus[1]))/(2*this->_converter.getDeltaX());

      T dWdX = (this->_converter.physVelocity(u_x_plus[2]-u_x_minus[2]))/(2*this->_converter.getDeltaX());
      T dWdY = (this->_converter.physVelocity(u_y_plus[2]-u_y_minus[2]))/(2*this->_converter.getDeltaX());
      T dWdZ = (this->_converter.physVelocity(u_z_plus[2]-u_z_minus[2]))/(2*this->_converter.getDeltaX());

      T qCrit = dUdX*dUdX - (dUdY*dVdX + dUdZ*dWdX) +
                dVdY*dVdY - (dVdX*dUdY + dVdZ*dWdY) +
                dWdZ*dWdZ - (dWdX*dUdZ + dWdY*dVdZ);


      // local coords are given, fetch local cell and compute value(s)
      // T rho, uTemp[DESCRIPTOR<T>::d], pi[util::TensorVal<DESCRIPTOR<T> >::n];
      // this->_sLattice.getBlockLattice(this->_sLattice.get_load().loc(globIC)).get(locix, lociy, lociz).computeAllMomenta(rho, uTemp, pi);

      // T PiNeqNormSqr = pi[0]*pi[0] + 2.*pi[1]*pi[1] + pi[2]*pi[2];
      // if (util::TensorVal<DESCRIPTOR<T> >::n == 6)
      //   PiNeqNormSqr += pi[2]*pi[2] + pi[3]*pi[3] + 2.*pi[4]*pi[4] +pi[5]*pi[5];

      // T nuLattice = converter->getLatticeNu();
      // T omega = converter->getOmega();
      // T finalResult = PiNeqNormSqr*nuLattice*pow(omega*DESCRIPTOR<T>::invCs2(),2)/rho/2.;

      return std::vector<T>(1,-0.5*qCrit);
    } else { // if 3
      std::vector<T> output;
      output.push_back(T());
      return output;
    }
  }

  else {
    return std::vector<T>(); // empty vector
  }
} // end SuperLatticeQCrit3D::operator()




///////////////////////////// SuperLatticeDynSmagoConst //////////////////////////////


template <typename T, template <typename U> class DESCRIPTOR>
SuperLatticeDynSmagoConst<T,DESCRIPTOR>::SuperLatticeDynSmagoConst
(SuperLattice3D<T,DESCRIPTOR>& sLattice, LBconverter<T>& converter, SuperGeometry3D<T>& sg)
  : SuperLatticePhysF3D<T,DESCRIPTOR>(sLattice,converter,1), _superGeometry(sg)
{
  this->_name = "dynSmago";
}



template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> SuperLatticeDynSmagoConst<T,DESCRIPTOR>::operator() (std::vector<int> input)
{
  int foundIC = (int)input[0];
  int locix = input[1];
  int lociy = input[2];
  int lociz = input[3];

  //int count;
  if ( this->_sLattice.getLoadBalancer().rank(foundIC) == singleton::mpi().getRank() ) {

    int globX = locix;
    int globY = lociy;
    int globZ = lociz;
    /// material fluid???
    if (_superGeometry.getMaterial(input) == 1) {

      SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity(this->_sLattice, this->_converter);

      T shear_test[util::TensorVal<DESCRIPTOR<T> >::n];
      T u[3], U_ij[util::TensorVal<DESCRIPTOR<T> >::n];;
      T S_ij[util::TensorVal<DESCRIPTOR<T> >::n];

      //int locIC = cuboidGeometry.get_iC(globX+100,iY,iZ, sLattice.getOverlap());
      //cout << "iC:" << locIC <<endl;
      //first block
      std::vector<int> u_ppp(4,int());
      u_ppp[0]=foundIC, u_ppp[1]=globX+1, u_ppp[2]=globY+1, u_ppp[3]=globZ+1;
      std::vector<int> u_0pp(4,int());
      u_0pp[0]=foundIC, u_0pp[1]=globX,   u_0pp[2]=globY+1, u_0pp[3]=globZ+1;
      std::vector<int> u_npp(4,int());
      u_0pp[0]=foundIC, u_0pp[1]=globX-1, u_0pp[2]=globY+1, u_0pp[3]=globZ+1;
      //
      std::vector<int> u_p0p(4,int());
      u_p0p[0]=foundIC, u_p0p[1]=globX+1, u_ppp[2]=globY, u_p0p[3]=globZ+1;
      std::vector<int> u_00p(4,int());
      u_00p[0]=foundIC, u_00p[1]=globX,   u_00p[2]=globY, u_00p[3]=globZ+1;
      std::vector<int> u_n0p(4,int());
      u_n0p[0]=foundIC, u_n0p[1]=globX-1, u_n0p[2]=globY+1, u_n0p[3]=globZ+1;
      //
      std::vector<int> u_pnp(4,int());
      u_pnp[0]=foundIC, u_pnp[1]=globX+1, u_pnp[2]=globY-1, u_pnp[3]=globZ+1;
      std::vector<int> u_0np(4,int());
      u_0np[0]=foundIC, u_0np[1]=globX,   u_0np[2]=globY-1, u_0np[3]=globZ+1;
      std::vector<int> u_nnp(4,int());
      u_nnp[0]=foundIC, u_nnp[1]=globX-1, u_nnp[2]=globY-1, u_nnp[3]=globZ+1;
      //second block
      std::vector<int> u_pp0(4,int());
      u_pp0[0]=foundIC, u_pp0[1]=globX+1, u_pp0[2]=globY+1, u_pp0[3]=globZ;
      std::vector<int> u_0p0(4,int());
      u_0p0[0]=foundIC, u_0p0[1]=globX,   u_0p0[2]=globY+1, u_0p0[3]=globZ;
      std::vector<int> u_np0(4,int());
      u_np0[0]=foundIC, u_np0[1]=globX-1, u_np0[2]=globY+1, u_np0[3]=globZ;
      //
      std::vector<int> u_p00(4,int());
      u_p00[0]=foundIC, u_p00[1]=globX+1, u_p00[2]=globY, u_p00[3]=globZ;
      std::vector<int> u_000(4,int());
      u_000[0]=foundIC, u_000[1]=globX,   u_000[2]=globY, u_000[3]=globZ;
      std::vector<int> u_n00(4,int());
      u_n00[0]=foundIC, u_n00[1]=globX-1, u_n00[2]=globY, u_n00[3]=globZ;
      //
      std::vector<int> u_pn0(4,int());
      u_pn0[0]=foundIC, u_pn0[1]=globX+1, u_pn0[2]=globY-1, u_pn0[3]=globZ;
      std::vector<int> u_0n0(4,int());
      u_0n0[0]=foundIC, u_0n0[1]=globX,   u_0n0[2]=globY-1, u_0n0[3]=globZ;
      std::vector<int> u_nn0(4,int());
      u_nn0[0]=foundIC, u_nn0[1]=globX-1, u_nn0[2]=globY-1, u_nn0[3]=globZ;
      //third block
      std::vector<int> u_ppn(4,int());
      u_ppn[0]=foundIC, u_ppn[1]=globX+1, u_ppn[2]=globY+1, u_ppn[3]=globZ-1;
      std::vector<int> u_0pn(4,int());
      u_0pn[0]=foundIC, u_0pn[1]=globX,   u_0pn[2]=globY+1, u_0pn[3]=globZ-1;
      std::vector<int> u_npn(4,int());
      u_npn[0]=foundIC, u_npn[1]=globX-1, u_npn[2]=globY+1, u_npn[3]=globZ-1;
      //
      std::vector<int> u_p0n(4,int());
      u_p0n[0]=foundIC, u_p0n[1]=globX+1, u_p0n[2]=globY, u_p0n[3]=globZ-1;
      std::vector<int> u_00n(4,int());
      u_00n[0]=foundIC, u_00n[1]=globX,   u_00n[2]=globY, u_00n[3]=globZ-1;
      std::vector<int> u_n0n(4,int());
      u_n0n[0]=foundIC, u_n0n[1]=globX-1, u_n0n[2]=globY, u_n0n[3]=globZ-1;
      //
      std::vector<int> u_pnn(4,int());
      u_pnn[0]=foundIC, u_pnn[1]=globX+1, u_pnn[2]=globY-1, u_pnn[3]=globZ-1;
      std::vector<int> u_0nn(4,int());
      u_0nn[0]=foundIC, u_0nn[1]=globX,   u_0nn[2]=globY-1, u_0nn[3]=globZ-1;
      std::vector<int> u_nnn(4,int());
      u_nnn[0]=foundIC, u_nnn[1]=globX-1, u_nnn[2]=globY-1, u_nnn[3]=globZ-1;



      //Testfilter for U
      for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
        u[i]=(( (velocity(u_ppp)[i] + 2*velocity(u_0pp)[i] + velocity(u_npp)[i]   )
                +2*(velocity(u_p0p)[i] + 2*velocity(u_00p)[i] + velocity(u_n0p)[i])
                +(velocity(u_pnp)[i] + 2*velocity(u_0np)[i] + velocity(u_nnp)[i]))*0.25+

              2*(( velocity(u_pp0)[i] + 2*velocity(u_0p0)[i] + velocity(u_np0)[i]   )
                 +2*(velocity(u_p00)[i] + 2*velocity(u_000)[i] + velocity(u_n00)[i])
                 +(velocity(u_pn0)[i] + 2*velocity(u_0n0)[i] + velocity(u_nn0)[i]))*0.25+

              ((velocity(u_ppn)[i] + 2*velocity(u_0pn)[i] + velocity(u_npn)[i])
               +2*(velocity(u_p0n)[i] + 2*velocity(u_00n)[i] + velocity(u_n0n)[i])
               +(velocity(u_pnn)[i] + 2*velocity(u_0nn)[i] + velocity(u_nnn)[i]))*0.25)*0.25;
      }

      //U_ij Testfilter

      U_ij[0]=(((velocity(u_ppp)[0]*velocity(u_ppp)[0] + 2*velocity(u_0pp)[0]*velocity(u_0pp)[0] + velocity(u_npp)[0]*velocity(u_0pp)[0])*0.25
                +2*(velocity(u_p0p)[0]*velocity(u_p0p)[0] + 2*velocity(u_00p)[0]*velocity(u_00p)[0] + velocity(u_n0p)[0]*velocity(u_00p)[0])*0.25
                +(velocity(u_pnp)[0]*velocity(u_pnp)[0] + 2*velocity(u_0np)[0]*velocity(u_0np)[0] + velocity(u_nnp)[0]*velocity(u_0np)[0])*0.25)*0.25+

               2*(( velocity(u_pp0)[0]*velocity(u_pp0)[0] + 2*velocity(u_0p0)[0]*velocity(u_0p0)[0] + velocity(u_np0)[0]*velocity(u_0p0)[0])*0.25
                  +2*(velocity(u_p00)[0]*velocity(u_p00)[0] + 2*velocity(u_000)[0]*velocity(u_000)[0] + velocity(u_n00)[0]*velocity(u_000)[0])*0.25
                  +(velocity(u_pn0)[0]*velocity(u_pn0)[0] + 2*velocity(u_0n0)[0]*velocity(u_0n0)[0] + velocity(u_nn0)[0]*velocity(u_0n0)[0])*0.25*0.25)*0.25+

               ((velocity(u_ppn)[0]*velocity(u_ppn)[0] + 2*velocity(u_0pn)[0]*velocity(u_0pn)[0] + velocity(u_npn)[0]*velocity(u_0pn)[0])*0.25
                +2*(velocity(u_p0n)[0]*velocity(u_p0n)[0] + 2*velocity(u_00n)[0]*velocity(u_00n)[0] + velocity(u_n0n)[0]*velocity(u_00n)[0])*0.25
                +(velocity(u_pnn)[0]*velocity(u_pnn)[0] + 2*velocity(u_0nn)[0]*velocity(u_0nn)[0] + velocity(u_nnn)[0]*velocity(u_0nn)[0])*0.25)*0.25);

      U_ij[1]=(((velocity(u_ppp)[0]*velocity(u_ppp)[1] + 2*velocity(u_0pp)[0]*velocity(u_0pp)[1] + velocity(u_npp)[0]*velocity(u_0pp)[1])*0.25
                +2*(velocity(u_p0p)[0]*velocity(u_p0p)[1] + 2*velocity(u_00p)[0]*velocity(u_00p)[1] + velocity(u_n0p)[0]*velocity(u_00p)[1])*0.25
                +(velocity(u_pnp)[0]*velocity(u_pnp)[1] + 2*velocity(u_0np)[0]*velocity(u_0np)[1] + velocity(u_nnp)[0]*velocity(u_0np)[1])*0.25)*0.25+

               2*(( velocity(u_pp0)[0]*velocity(u_pp0)[1] + 2*velocity(u_0p0)[0]*velocity(u_0p0)[1] + velocity(u_np0)[0]*velocity(u_0p0)[1])*0.25
                  +2*(velocity(u_p00)[0]*velocity(u_p00)[1] + 2*velocity(u_000)[0]*velocity(u_000)[1] + velocity(u_n00)[0]*velocity(u_000)[1])*0.25
                  +(velocity(u_pn0)[0]*velocity(u_pn0)[1] + 2*velocity(u_0n0)[0]*velocity(u_0n0)[1] + velocity(u_nn0)[0]*velocity(u_0n0)[1])*0.25)*0.25+

               ((velocity(u_ppn)[0]*velocity(u_ppn)[1] + 2*velocity(u_0pn)[0]*velocity(u_0pn)[1] + velocity(u_npn)[0]*velocity(u_0pn)[1])*0.25
                +2*(velocity(u_p0n)[0]*velocity(u_p0n)[1] + 2*velocity(u_00n)[0]*velocity(u_00n)[1] + velocity(u_n0n)[0]*velocity(u_00n)[1])*0.25
                +(velocity(u_pnn)[0]*velocity(u_pnn)[1] + 2*velocity(u_0nn)[0]*velocity(u_0nn)[1] + velocity(u_nnn)[0]*velocity(u_0nn)[1])*0.25)*0.25);

      U_ij[2]=(((velocity(u_ppp)[0]*velocity(u_ppp)[2] + 2*velocity(u_0pp)[0]*velocity(u_0pp)[2] + velocity(u_npp)[0]*velocity(u_0pp)[2])*0.25
                +2*(velocity(u_p0p)[0]*velocity(u_p0p)[2] + 2*velocity(u_00p)[0]*velocity(u_00p)[2] + velocity(u_n0p)[0]*velocity(u_00p)[2])*0.25
                +(velocity(u_pnp)[0]*velocity(u_pnp)[2] + 2*velocity(u_0np)[0]*velocity(u_0np)[2] + velocity(u_nnp)[0]*velocity(u_0np)[2])*0.25)*0.25+

               2*(( velocity(u_pp0)[0]*velocity(u_pp0)[2] + 2*velocity(u_0p0)[0]*velocity(u_0p0)[2] + velocity(u_np0)[0]*velocity(u_0p0)[2])*0.25
                  +2*(velocity(u_p00)[0]*velocity(u_p00)[2] + 2*velocity(u_000)[0]*velocity(u_000)[2] + velocity(u_n00)[0]*velocity(u_000)[2])*0.25
                  +(velocity(u_pn0)[0]*velocity(u_pn0)[2] + 2*velocity(u_0n0)[0]*velocity(u_0n0)[2] + velocity(u_nn0)[0]*velocity(u_0n0)[2])*0.25)*0.25+

               ((velocity(u_ppn)[0]*velocity(u_ppn)[2] + 2*velocity(u_0pn)[0]*velocity(u_0pn)[2] + velocity(u_npn)[0]*velocity(u_0pn)[2])*0.25
                +2*(velocity(u_p0n)[0]*velocity(u_p0n)[2] + 2*velocity(u_00n)[0]*velocity(u_00n)[2] + velocity(u_n0n)[0]*velocity(u_00n)[2])*0.25
                +(velocity(u_pnn)[0]*velocity(u_pnn)[2] + 2*velocity(u_0nn)[0]*velocity(u_0nn)[2] + velocity(u_nnn)[0]*velocity(u_0nn)[2])*0.25)*0.25);

      U_ij[3]=(((velocity(u_ppp)[1]*velocity(u_ppp)[1] + 2*velocity(u_0pp)[1]*velocity(u_0pp)[1] + velocity(u_npp)[1]*velocity(u_0pp)[1])*0.25
                +2*(velocity(u_p0p)[1]*velocity(u_p0p)[1] + 2*velocity(u_00p)[1]*velocity(u_00p)[1] + velocity(u_n0p)[1]*velocity(u_00p)[1])*0.25
                +(velocity(u_pnp)[1]*velocity(u_pnp)[1] + 2*velocity(u_0np)[1]*velocity(u_0np)[1] + velocity(u_nnp)[1]*velocity(u_0np)[1])*0.25)*0.25+

               2*(( velocity(u_pp0)[1]*velocity(u_pp0)[1] + 2*velocity(u_0p0)[1]*velocity(u_0p0)[1] + velocity(u_np0)[1]*velocity(u_0p0)[1])*0.25
                  +2*(velocity(u_p00)[1]*velocity(u_p00)[1] + 2*velocity(u_000)[1]*velocity(u_000)[1] + velocity(u_n00)[1]*velocity(u_000)[1])*0.25
                  +(velocity(u_pn0)[1]*velocity(u_pn0)[1] + 2*velocity(u_0n0)[1]*velocity(u_0n0)[1] + velocity(u_nn0)[1]*velocity(u_0n0)[1])*0.25)*0.25+

               ((velocity(u_ppn)[1]*velocity(u_ppn)[1] + 2*velocity(u_0pn)[1]*velocity(u_0pn)[1] + velocity(u_npn)[1]*velocity(u_0pn)[1])*0.25
                +2*(velocity(u_p0n)[1]*velocity(u_p0n)[1] + 2*velocity(u_00n)[1]*velocity(u_00n)[1] + velocity(u_n0n)[1]*velocity(u_00n)[1])*0.25
                +(velocity(u_pnn)[1]*velocity(u_pnn)[1] + 2*velocity(u_0nn)[1]*velocity(u_0nn)[1] + velocity(u_nnn)[1]*velocity(u_0nn)[1])*0.25)*0.25);

      U_ij[4]=(((velocity(u_ppp)[1]*velocity(u_ppp)[2] + 2*velocity(u_0pp)[1]*velocity(u_0pp)[2] + velocity(u_npp)[1]*velocity(u_0pp)[2])*0.25
                +2*(velocity(u_p0p)[1]*velocity(u_p0p)[2] + 2*velocity(u_00p)[1]*velocity(u_00p)[2] + velocity(u_n0p)[1]*velocity(u_00p)[2])*0.25
                +(velocity(u_pnp)[1]*velocity(u_pnp)[2] + 2*velocity(u_0np)[1]*velocity(u_0np)[2] + velocity(u_nnp)[1]*velocity(u_0np)[2])*0.25)*0.25+

               2*(( velocity(u_pp0)[1]*velocity(u_pp0)[2] + 2*velocity(u_0p0)[1]*velocity(u_0p0)[2] + velocity(u_np0)[1]*velocity(u_0p0)[2])*0.25
                  +2*(velocity(u_p00)[1]*velocity(u_p00)[2] + 2*velocity(u_000)[1]*velocity(u_000)[2] + velocity(u_n00)[1]*velocity(u_000)[2])*0.25
                  +(velocity(u_pn0)[1]*velocity(u_pn0)[2] + 2*velocity(u_0n0)[1]*velocity(u_0n0)[2] + velocity(u_nn0)[1]*velocity(u_0n0)[2])*0.25)*0.25+

               ((velocity(u_ppn)[1]*velocity(u_ppn)[2] + 2*velocity(u_0pn)[1]*velocity(u_0pn)[2] + velocity(u_npn)[1]*velocity(u_0pn)[2])*0.25
                +2*(velocity(u_p0n)[1]*velocity(u_p0n)[2] + 2*velocity(u_00n)[1]*velocity(u_00n)[2] + velocity(u_n0n)[1]*velocity(u_00n)[2])*0.25
                +(velocity(u_pnn)[1]*velocity(u_pnn)[2] + 2*velocity(u_0nn)[1]*velocity(u_0nn)[2] + velocity(u_nnn)[1]*velocity(u_0nn)[2])*0.25)*0.25);

      U_ij[5]=(((velocity(u_ppp)[2]*velocity(u_ppp)[2] + 2*velocity(u_0pp)[2]*velocity(u_0pp)[2] + velocity(u_npp)[2]*velocity(u_0pp)[2])*0.25
                +2*(velocity(u_p0p)[2]*velocity(u_p0p)[2] + 2*velocity(u_00p)[2]*velocity(u_00p)[2] + velocity(u_n0p)[2]*velocity(u_00p)[2])*0.25
                +(velocity(u_pnp)[2]*velocity(u_pnp)[2] + 2*velocity(u_0np)[2]*velocity(u_0np)[2] + velocity(u_nnp)[2]*velocity(u_0np)[2])*0.25)*0.25+

               2*(( velocity(u_pp0)[2]*velocity(u_pp0)[2] + 2*velocity(u_0p0)[2]*velocity(u_0p0)[2] + velocity(u_np0)[2]*velocity(u_0p0)[2])*0.25
                  +2*(velocity(u_p00)[2]*velocity(u_p00)[2] + 2*velocity(u_000)[2]*velocity(u_000)[2] + velocity(u_n00)[2]*velocity(u_000)[2])*0.25
                  +(velocity(u_pn0)[2]*velocity(u_pn0)[2] + 2*velocity(u_0n0)[2]*velocity(u_0n0)[2] + velocity(u_nn0)[2]*velocity(u_0n0)[2])*0.25)*0.25+

               ((velocity(u_ppn)[2]*velocity(u_ppn)[2] + 2*velocity(u_0pn)[2]*velocity(u_0pn)[2] + velocity(u_npn)[2]*velocity(u_0pn)[2])*0.25
                +2*(velocity(u_p0n)[2]*velocity(u_p0n)[2] + 2*velocity(u_00n)[2]*velocity(u_00n)[2] + velocity(u_n0n)[2]*velocity(u_00n)[2])*0.25
                +(velocity(u_pnn)[2]*velocity(u_pnn)[2] + 2*velocity(u_0nn)[2]*velocity(u_0nn)[2] + velocity(u_nnn)[2]*velocity(u_0nn)[2])*0.25)*0.25);

      //////////////////////////////BUG FIX SECOND LAYER/////////////////////////////////
      /// Shear rate with test filtered velocities

      T dx=this->_converter.getDeltaX();

      // T uXp[3], uXn[3], uYp[3], uYn[3], uZp[3], uZn[3];



      shear_test[0] = 1./dx*(fd::centralGradient(velocity(u_p00)[0],velocity(u_n00)[0])+fd::centralGradient(velocity(u_p00)[0],velocity(u_n00)[0]));//dudx+dudx
      shear_test[1] = 1./dx*(fd::centralGradient(velocity(u_0p0)[0],velocity(u_0n0)[0])+fd::centralGradient(velocity(u_p00)[1],velocity(u_n00)[1]));//dudy+dvdx
      shear_test[2] = 1./dx*(fd::centralGradient(velocity(u_00p)[0],velocity(u_00n)[0])+fd::centralGradient(velocity(u_p00)[2],velocity(u_n00)[2]));//dudz+dwdx

      shear_test[3] = 1./dx*(fd::centralGradient(velocity(u_0p0)[1],velocity(u_0n0)[1])+fd::centralGradient(velocity(u_0p0)[1],velocity(u_0n0)[1]));//dvdy+dvdy
      shear_test[4] = 1./dx*(fd::centralGradient(velocity(u_00p)[1],velocity(u_00n)[1])+fd::centralGradient(velocity(u_0p0)[2],velocity(u_0n0)[2]));//dvdz+dwdy
      shear_test[5] = 1./dx*(fd::centralGradient(velocity(u_00p)[2],velocity(u_00n)[2])+fd::centralGradient(velocity(u_00p)[2],velocity(u_00n)[2]));//dwdz+dwdz



      T PiNeqNormSqr = shear_test[0]*shear_test[0] + 2.0*shear_test[1]*shear_test[1] + shear_test[2]*shear_test[2];
      if (util::TensorVal<DESCRIPTOR<T> >::n == 6) {
        PiNeqNormSqr += shear_test[2]*shear_test[2] + shear_test[3]*shear_test[3] + 2*shear_test[4]*shear_test[4] +shear_test[5]*shear_test[5];
      }
      T PiNeqNorm    = sqrt(2*PiNeqNormSqr);
      for (int i = 0; i < util::TensorVal<DESCRIPTOR<T> >::n; ++i ) {
        shear_test[i] = shear_test[i]*PiNeqNorm;
      }


      // test filtered shear

      SuperLatticeStress3D<T, DESCRIPTOR> stress(this->_sLattice);
      //first block
      std::vector<int> S_ppp(4,int());
      S_ppp[0]=foundIC, S_ppp[1]=globX+1, S_ppp[2]=globY+1, S_ppp[3]=globZ+1;
      std::vector<int> S_0pp(4,int());
      S_0pp[0]=foundIC, S_0pp[1]=globX,   S_0pp[2]=globY+1, S_0pp[3]=globZ+1;
      std::vector<int> S_npp(4,int());
      S_0pp[0]=foundIC, S_0pp[1]=globX-1, S_0pp[2]=globY+1, S_0pp[3]=globZ+1;
      //
      std::vector<int> S_p0p(4,int());
      S_p0p[0]=foundIC, S_p0p[1]=globX+1, S_ppp[2]=globY, S_p0p[3]=globZ+1;
      std::vector<int> S_00p(4,int());
      S_00p[0]=foundIC, S_00p[1]=globX,   S_00p[2]=globY, S_00p[3]=globZ+1;
      std::vector<int> S_n0p(4,int());
      S_n0p[0]=foundIC, S_n0p[1]=globX-1, S_n0p[2]=globY+1, S_n0p[3]=globZ+1;
      //
      std::vector<int> S_pnp(4,int());
      S_pnp[0]=foundIC, S_pnp[1]=globX+1, S_pnp[2]=globY-1, S_pnp[3]=globZ+1;
      std::vector<int> S_0np(4,int());
      S_0np[0]=foundIC, S_0np[1]=globX,   S_0np[2]=globY-1, S_0np[3]=globZ+1;
      std::vector<int> S_nnp(4,int());
      S_nnp[0]=foundIC, S_nnp[1]=globX-1, S_nnp[2]=globY-1, S_nnp[3]=globZ+1;
      //second block
      std::vector<int> S_pp0(4,int());
      S_pp0[0]=foundIC, S_pp0[1]=globX+1, S_pp0[2]=globY+1, S_pp0[3]=globZ;
      std::vector<int> S_0p0(4,int());
      S_0p0[0]=foundIC, S_0p0[1]=globX,   S_0p0[2]=globY+1, S_0p0[3]=globZ;
      std::vector<int> S_np0(4,int());
      S_np0[0]=foundIC, S_np0[1]=globX-1, S_np0[2]=globY+1, S_np0[3]=globZ;
      //
      std::vector<int> S_p00(4,int());
      S_p00[0]=foundIC, S_p00[1]=globX+1, S_p00[2]=globY, S_p00[3]=globZ;
      std::vector<int> S_000(4,int());
      S_000[0]=foundIC, S_000[1]=globX,   S_000[2]=globY, S_000[3]=globZ;
      std::vector<int> S_n00(4,int());
      S_n00[0]=foundIC, S_n00[1]=globX-1, S_n00[2]=globY, S_n00[3]=globZ;
      //
      std::vector<int> S_pn0(4,int());
      S_pn0[0]=foundIC, S_pn0[1]=globX+1, S_pn0[2]=globY-1, S_pn0[3]=globZ;
      std::vector<int> S_0n0(4,int());
      S_0n0[0]=foundIC, S_0n0[1]=globX,   S_0n0[2]=globY-1, S_0n0[3]=globZ;
      std::vector<int> S_nn0(4,int());
      S_nn0[0]=foundIC, S_nn0[1]=globX-1, S_nn0[2]=globY-1, S_nn0[3]=globZ;
      //third block
      std::vector<int> S_ppn(4,int());
      S_ppn[0]=foundIC, S_ppn[1]=globX+1, S_ppn[2]=globY+1, S_ppn[3]=globZ-1;
      std::vector<int> S_0pn(4,int());
      S_0pn[0]=foundIC, S_0pn[1]=globX,   S_0pn[2]=globY+1, S_0pn[3]=globZ-1;
      std::vector<int> S_npn(4,int());
      S_npn[0]=foundIC, S_npn[1]=globX-1, S_npn[2]=globY+1, S_npn[3]=globZ-1;
      //
      std::vector<int> S_p0n(4,int());
      S_p0n[0]=foundIC, S_p0n[1]=globX+1, S_p0n[2]=globY, S_p0n[3]=globZ-1;
      std::vector<int> S_00n(4,int());
      S_00n[0]=foundIC, S_00n[1]=globX,   S_00n[2]=globY, S_00n[3]=globZ-1;
      std::vector<int> S_n0n(4,int());
      S_n0n[0]=foundIC, S_n0n[1]=globX-1, S_n0n[2]=globY, S_n0n[3]=globZ-1;
      //
      std::vector<int> S_pnn(4,int());
      S_pnn[0]=foundIC, S_pnn[1]=globX+1, S_pnn[2]=globY-1, S_pnn[3]=globZ-1;
      std::vector<int> S_0nn(4,int());
      S_0nn[0]=foundIC, S_0nn[1]=globX, S_0nn[2]=globY-1, S_0nn[3]=globZ-1;
      std::vector<int> S_nnn(4,int());
      S_nnn[0]=foundIC, S_nnn[1]=globX-1, S_nnn[2]=globY-1, S_nnn[3]=globZ-1;

      //first block

      T PiNeqNormSqr_ppp = sqrt(2*(stress(S_ppp)[0]*stress(S_ppp)[0] + 2.0*stress(S_ppp)[1]*stress(S_ppp)[1] + stress(S_ppp)[2]*stress(S_ppp)[2]
                                   +stress(S_ppp)[2]*stress(S_ppp)[2] + stress(S_ppp)[3]*stress(S_ppp)[3] + 2*stress(S_ppp)[4]*stress(S_ppp)[4] +stress(S_ppp)[5]*stress(S_ppp)[5]));

      T PiNeqNormSqr_0pp = sqrt(2*(stress(S_0pp)[0]*stress(S_0pp)[0] + 2.0*stress(S_0pp)[1]*stress(S_0pp)[1] + stress(S_0pp)[2]*stress(S_0pp)[2]
                                   +stress(S_0pp)[2]*stress(S_0pp)[2] + stress(S_0pp)[3]*stress(S_0pp)[3] + 2*stress(S_0pp)[4]*stress(S_0pp)[4] +stress(S_0pp)[5]*stress(S_0pp)[5]));

      T PiNeqNormSqr_npp = sqrt(2*(stress(S_npp)[0]*stress(S_npp)[0] + 2.0*stress(S_npp)[1]*stress(S_npp)[1] + stress(S_npp)[2]*stress(S_npp)[2]
                                   +stress(S_npp)[2]*stress(S_npp)[2] + stress(S_npp)[3]*stress(S_npp)[3] + 2*stress(S_npp)[4]*stress(S_npp)[4] +stress(S_npp)[5]*stress(S_npp)[5]));
      //

      T PiNeqNormSqr_p0p = sqrt(2*(stress(S_p0p)[0]*stress(S_p0p)[0] + 2.0*stress(S_p0p)[1]*stress(S_p0p)[1] + stress(S_p0p)[2]*stress(S_p0p)[2]
                                   +stress(S_p0p)[2]*stress(S_p0p)[2] + stress(S_p0p)[3]*stress(S_p0p)[3] + 2*stress(S_p0p)[4]*stress(S_p0p)[4] +stress(S_p0p)[5]*stress(S_p0p)[5]));

      T PiNeqNormSqr_00p = sqrt(2*(stress(S_00p)[0]*stress(S_00p)[0] + 2.0*stress(S_00p)[1]*stress(S_00p)[1] + stress(S_00p)[2]*stress(S_00p)[2]
                                   +stress(S_00p)[2]*stress(S_00p)[2] + stress(S_00p)[3]*stress(S_00p)[3] + 2*stress(S_00p)[4]*stress(S_00p)[4] +stress(S_00p)[5]*stress(S_00p)[5]));

      T PiNeqNormSqr_n0p = sqrt(2*(stress(S_n0p)[0]*stress(S_n0p)[0] + 2.0*stress(S_n0p)[1]*stress(S_n0p)[1] + stress(S_n0p)[2]*stress(S_n0p)[2]
                                   +stress(S_n0p)[2]*stress(S_n0p)[2] + stress(S_n0p)[3]*stress(S_n0p)[3] + 2*stress(S_n0p)[4]*stress(S_n0p)[4] +stress(S_n0p)[5]*stress(S_n0p)[5]));
      //

      T PiNeqNormSqr_pnp = sqrt(2*(stress(S_pnp)[0]*stress(S_pnp)[0] + 2.0*stress(S_pnp)[1]*stress(S_pnp)[1] + stress(S_pnp)[2]*stress(S_pnp)[2]
                                   +stress(S_pnp)[2]*stress(S_pnp)[2] + stress(S_pnp)[3]*stress(S_pnp)[3] + 2*stress(S_pnp)[4]*stress(S_pnp)[4] +stress(S_pnp)[5]*stress(S_pnp)[5]));

      T PiNeqNormSqr_0np = sqrt(2*(stress(S_0np)[0]*stress(S_0np)[0] + 2.0*stress(S_0np)[1]*stress(S_0np)[1] + stress(S_0np)[2]*stress(S_0np)[2]
                                   +stress(S_0np)[2]*stress(S_0np)[2] + stress(S_0np)[3]*stress(S_0np)[3] + 2*stress(S_0np)[4]*stress(S_0np)[4] +stress(S_0np)[5]*stress(S_0np)[5]));

      T PiNeqNormSqr_nnp = sqrt(2*(stress(S_nnp)[0]*stress(S_nnp)[0] + 2.0*stress(S_nnp)[1]*stress(S_nnp)[1] + stress(S_nnp)[2]*stress(S_nnp)[2]
                                   +stress(S_nnp)[2]*stress(S_nnp)[2] + stress(S_nnp)[3]*stress(S_nnp)[3] + 2*stress(S_nnp)[4]*stress(S_nnp)[4] +stress(S_nnp)[5]*stress(S_nnp)[5]));

      //second block

      T PiNeqNormSqr_pp0 = sqrt(2*(stress(S_pp0)[0]*stress(S_pp0)[0] + 2.0*stress(S_pp0)[1]*stress(S_pp0)[1] + stress(S_pp0)[2]*stress(S_pp0)[2]
                                   +stress(S_pp0)[2]*stress(S_pp0)[2] + stress(S_pp0)[3]*stress(S_pp0)[3] + 2*stress(S_pp0)[4]*stress(S_pp0)[4] +stress(S_pp0)[5]*stress(S_pp0)[5]));

      T PiNeqNormSqr_0p0 = sqrt(2*(stress(S_0p0)[0]*stress(S_0p0)[0] + 2.0*stress(S_0p0)[1]*stress(S_0p0)[1] + stress(S_0p0)[2]*stress(S_0p0)[2]
                                   +stress(S_0p0)[2]*stress(S_0p0)[2] + stress(S_0p0)[3]*stress(S_0p0)[3] + 2*stress(S_0p0)[4]*stress(S_0p0)[4] +stress(S_0p0)[5]*stress(S_0p0)[5]));

      T PiNeqNormSqr_np0 = sqrt(2*(stress(S_np0)[0]*stress(S_np0)[0] + 2.0*stress(S_np0)[1]*stress(S_np0)[1] + stress(S_np0)[2]*stress(S_np0)[2]
                                   +stress(S_np0)[2]*stress(S_np0)[2] + stress(S_np0)[3]*stress(S_np0)[3] + 2*stress(S_np0)[4]*stress(S_np0)[4] +stress(S_np0)[5]*stress(S_np0)[5]));
      //

      T PiNeqNormSqr_p00 = sqrt(2*(stress(S_p00)[0]*stress(S_p00)[0] + 2.0*stress(S_p00)[1]*stress(S_p00)[1] + stress(S_p00)[2]*stress(S_p00)[2]
                                   +stress(S_p00)[2]*stress(S_p00)[2] + stress(S_p00)[3]*stress(S_p00)[3] + 2*stress(S_p00)[4]*stress(S_p00)[4] +stress(S_p00)[5]*stress(S_p00)[5]));

      T PiNeqNormSqr_000 = sqrt(2*(stress(S_000)[0]*stress(S_000)[0] + 2.0*stress(S_000)[1]*stress(S_000)[1] + stress(S_000)[2]*stress(S_000)[2]
                                   +stress(S_000)[2]*stress(S_000)[2] + stress(S_000)[3]*stress(S_000)[3] + 2*stress(S_000)[4]*stress(S_000)[4] +stress(S_000)[5]*stress(S_000)[5]));

      T PiNeqNormSqr_n00 = sqrt(2*(stress(S_n00)[0]*stress(S_n00)[0] + 2.0*stress(S_n00)[1]*stress(S_n00)[1] + stress(S_n00)[2]*stress(S_n00)[2]
                                   +stress(S_n00)[2]*stress(S_n00)[2] + stress(S_n00)[3]*stress(S_n00)[3] + 2*stress(S_n00)[4]*stress(S_n00)[4] +stress(S_n00)[5]*stress(S_n00)[5]));
      //

      T PiNeqNormSqr_pn0 = sqrt(2*(stress(S_pn0)[0]*stress(S_pn0)[0] + 2.0*stress(S_pn0)[1]*stress(S_pn0)[1] + stress(S_pn0)[2]*stress(S_pn0)[2]
                                   +stress(S_pn0)[2]*stress(S_pn0)[2] + stress(S_pn0)[3]*stress(S_pn0)[3] + 2*stress(S_pn0)[4]*stress(S_pn0)[4] +stress(S_pn0)[5]*stress(S_pn0)[5]));

      T PiNeqNormSqr_0n0 = sqrt(2*(stress(S_0n0)[0]*stress(S_0n0)[0] + 2.0*stress(S_0n0)[1]*stress(S_0n0)[1] + stress(S_0n0)[2]*stress(S_0n0)[2]
                                   +stress(S_0n0)[2]*stress(S_0n0)[2] + stress(S_0n0)[3]*stress(S_0n0)[3] + 2*stress(S_0n0)[4]*stress(S_0n0)[4] +stress(S_0n0)[5]*stress(S_0n0)[5]));

      T PiNeqNormSqr_nn0 = sqrt(2*(stress(S_nn0)[0]*stress(S_nn0)[0] + 2.0*stress(S_nn0)[1]*stress(S_nn0)[1] + stress(S_nn0)[2]*stress(S_nn0)[2]
                                   +stress(S_nn0)[2]*stress(S_nn0)[2] + stress(S_nn0)[3]*stress(S_nn0)[3] + 2*stress(S_nn0)[4]*stress(S_nn0)[4] +stress(S_nn0)[5]*stress(S_nn0)[5]));

      //third block

      T PiNeqNormSqr_ppn = sqrt(2*(stress(S_ppn)[0]*stress(S_ppn)[0] + 2.0*stress(S_ppn)[1]*stress(S_ppn)[1] + stress(S_ppn)[2]*stress(S_ppn)[2]
                                   +stress(S_ppn)[2]*stress(S_ppn)[2] + stress(S_ppn)[3]*stress(S_ppn)[3] + 2*stress(S_ppn)[4]*stress(S_ppn)[4] +stress(S_ppn)[5]*stress(S_ppn)[5]));

      T PiNeqNormSqr_0pn = sqrt(2*(stress(S_0pn)[0]*stress(S_0pn)[0] + 2.0*stress(S_0pn)[1]*stress(S_0pn)[1] + stress(S_0pn)[2]*stress(S_0pn)[2]
                                   +stress(S_0pn)[2]*stress(S_0pn)[2] + stress(S_0pn)[3]*stress(S_0pn)[3] + 2*stress(S_0pn)[4]*stress(S_0pn)[4] +stress(S_0pn)[5]*stress(S_0pn)[5]));

      T PiNeqNormSqr_npn = sqrt(2*(stress(S_npn)[0]*stress(S_npn)[0] + 2.0*stress(S_npn)[1]*stress(S_npn)[1] + stress(S_npn)[2]*stress(S_npn)[2]
                                   +stress(S_npn)[2]*stress(S_npn)[2] + stress(S_npn)[3]*stress(S_npn)[3] + 2*stress(S_npn)[4]*stress(S_npn)[4] +stress(S_npn)[5]*stress(S_npn)[5]));
      //

      T PiNeqNormSqr_p0n = sqrt(2*(stress(S_p0n)[0]*stress(S_p0n)[0] + 2.0*stress(S_p0n)[1]*stress(S_p0n)[1] + stress(S_p0n)[2]*stress(S_p0n)[2]
                                   +stress(S_p0n)[2]*stress(S_p0n)[2] + stress(S_p0n)[3]*stress(S_p0n)[3] + 2*stress(S_p0n)[4]*stress(S_p0n)[4] +stress(S_p0n)[5]*stress(S_p0n)[5]));

      T PiNeqNormSqr_00n = sqrt(2*(stress(S_00n)[0]*stress(S_00n)[0] + 2.0*stress(S_00n)[1]*stress(S_00n)[1] + stress(S_00n)[2]*stress(S_00n)[2]
                                   +stress(S_00n)[2]*stress(S_00n)[2] + stress(S_00n)[3]*stress(S_00n)[3] + 2*stress(S_00n)[4]*stress(S_00n)[4] +stress(S_00n)[5]*stress(S_00n)[5]));

      T PiNeqNormSqr_n0n = sqrt(2*(stress(S_n0n)[0]*stress(S_n0n)[0] + 2.0*stress(S_n0n)[1]*stress(S_n0n)[1] + stress(S_n0n)[2]*stress(S_n0n)[2]
                                   +stress(S_n0n)[2]*stress(S_n0n)[2] + stress(S_n0n)[3]*stress(S_n0n)[3] + 2*stress(S_n0n)[4]*stress(S_n0n)[4] +stress(S_n0n)[5]*stress(S_n0n)[5]));
      //

      T PiNeqNormSqr_pnn = sqrt(2*(stress(S_pnn)[0]*stress(S_pnn)[0] + 2.0*stress(S_pnn)[1]*stress(S_pnn)[1] + stress(S_pnn)[2]*stress(S_pnn)[2]
                                   +stress(S_pnn)[2]*stress(S_pnn)[2] + stress(S_pnn)[3]*stress(S_pnn)[3] + 2*stress(S_pnn)[4]*stress(S_pnn)[4] +stress(S_pnn)[5]*stress(S_pnn)[5]));

      T PiNeqNormSqr_0nn = sqrt(2*(stress(S_0nn)[0]*stress(S_0nn)[0] + 2.0*stress(S_0nn)[1]*stress(S_0nn)[1] + stress(S_0nn)[2]*stress(S_0nn)[2]
                                   +stress(S_0nn)[2]*stress(S_0nn)[2] + stress(S_0nn)[3]*stress(S_0nn)[3] + 2*stress(S_0nn)[4]*stress(S_0nn)[4] +stress(S_0nn)[5]*stress(S_0nn)[5]));

      T PiNeqNormSqr_nnn = sqrt(2*(stress(S_nnn)[0]*stress(S_nnn)[0] + 2.0*stress(S_nnn)[1]*stress(S_nnn)[1] + stress(S_nnn)[2]*stress(S_nnn)[2]
                                   +stress(S_nnn)[2]*stress(S_nnn)[2] + stress(S_nnn)[3]*stress(S_nnn)[3] + 2*stress(S_nnn)[4]*stress(S_nnn)[4] +stress(S_nnn)[5]*stress(S_nnn)[5]));



      for (int i = 0; i < util::TensorVal<DESCRIPTOR<T> >::n; ++i ) {

        S_ij[i] = (( (stress(S_ppp)[i]*PiNeqNormSqr_ppp + 2*stress(S_0pp)[i]*PiNeqNormSqr_0pp + stress(S_npp)[i]*PiNeqNormSqr_npp)*0.25
                     +2*(stress(S_p0p)[i]*PiNeqNormSqr_p0p + 2*stress(S_00p)[i]*PiNeqNormSqr_00p + stress(S_n0p)[i]*PiNeqNormSqr_n0p)*0.25
                     +(stress(S_pnp)[i]*PiNeqNormSqr_pnp + 2*stress(S_0np)[i]*PiNeqNormSqr_0np + stress(S_nnp)[i]*PiNeqNormSqr_nnp)*0.25)*0.25+

                   2*(( stress(S_pp0)[i]*PiNeqNormSqr_pp0 + 2*stress(S_0p0)[i]*PiNeqNormSqr_0p0 + stress(S_np0)[i]*PiNeqNormSqr_np0)*0.25
                      +2*(stress(S_p00)[i]*PiNeqNormSqr_p00 + 2*stress(S_000)[i]*PiNeqNormSqr_000 + stress(S_n00)[i]*PiNeqNormSqr_n00)*0.25
                      +(stress(S_pn0)[i]*PiNeqNormSqr_pn0 + 2*stress(S_0n0)[i]*PiNeqNormSqr_0n0 + stress(S_nn0)[i]*PiNeqNormSqr_nn0)*0.25)*0.25+

                   ((stress(S_ppn)[i]*PiNeqNormSqr_ppn + 2*stress(S_0pn)[i]*PiNeqNormSqr_0pn + stress(S_npn)[i]*PiNeqNormSqr_npn)*0.25
                    +2*(stress(S_p0n)[i]*PiNeqNormSqr_p0n + 2*stress(S_00n)[i]*PiNeqNormSqr_00n + stress(S_n0n)[i]*PiNeqNormSqr_n0n)*0.25
                    +(stress(S_pnn)[i]*PiNeqNormSqr_pnn + 2*stress(S_0nn)[i]*PiNeqNormSqr_0nn + stress(S_nnn)[i]*PiNeqNormSqr_nnn)*0.25)*0.25);
      }


      //actual smagoConst evaluation
      T M_ij[3][3];
      M_ij[0][0] = (2*dx*2*dx)*shear_test[0]-dx*dx*S_ij[0];
      M_ij[0][1] = (2*dx*2*dx)*shear_test[1]-dx*dx*S_ij[1];
      M_ij[0][2] = (2*dx*2*dx)*shear_test[2]-dx*dx*S_ij[2];
      M_ij[1][0] = (2*dx*2*dx)*shear_test[1]-dx*dx*S_ij[1];
      M_ij[1][1] = (2*dx*2*dx)*shear_test[3]-dx*dx*S_ij[3];
      M_ij[1][2] = (2*dx*2*dx)*shear_test[4]-dx*dx*S_ij[4];
      M_ij[2][0] = (2*dx*2*dx)*shear_test[2]-dx*dx*S_ij[2];
      M_ij[2][1] = (2*dx*2*dx)*shear_test[4]-dx*dx*S_ij[4];
      M_ij[2][2] = (2*dx*2*dx)*shear_test[5]-dx*dx*S_ij[5];


      T L_ij[3][3];
      L_ij[0][0] = U_ij[0]-(u[0]*u[0]);
      L_ij[0][1] = U_ij[1]-(u[0]*u[1]);
      L_ij[0][2] = U_ij[2]-(u[0]*u[2]);
      L_ij[1][0] = U_ij[1]-(u[0]*u[1]);
      L_ij[1][1] = U_ij[3]-(u[1]*u[1]);
      L_ij[1][2] = U_ij[4]-(u[1]*u[2]);
      L_ij[2][0] = U_ij[2]-(u[0]*u[2]);
      L_ij[2][1] = U_ij[4]-(u[1]*u[2]);
      L_ij[2][2] = U_ij[5]-(u[2]*u[2]);

      T smagoConst = 0;
      T a = 0;
      for (int ix = 0; ix < DESCRIPTOR<T>::d; ++ix) {
        for (int iy = 0; iy < DESCRIPTOR<T>::d; ++iy) {
          a += L_ij[ix][iy]*M_ij[ix][iy];
        }
      }
      T b=0;
      for (int ix = 0; ix < DESCRIPTOR<T>::d; ++ix) {
        for (int iy = 0; iy < DESCRIPTOR<T>::d; ++iy) {
          b += M_ij[ix][iy]*M_ij[ix][iy];
        }
      }
      //       // computing the dynamic smagorinksy variable
      // make sure, the is no division by zero
      if (b!=0) {
        smagoConst = a/(-2*b);
      } else {
        smagoConst = 0.01;
      }
      // clipping
      if (smagoConst<0) {
        smagoConst=0;
      }
      if (smagoConst > 1) {
        smagoConst =1;
      }




      return std::vector<T>(1,smagoConst);
    } else { // if 3
      std::vector<T> output;
      output.push_back(T());
      return output;
    }
  }

  else {
    return std::vector<T>(); // empty vector
  }
} // end SuperLatticeDynSMagoConst::operator()


/////////////////BlockLattice Smagorinsky//////////////////////////////


template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeDynSmagoConst<T,DESCRIPTOR>::BlockLatticeDynSmagoConst
(BlockLatticeStructure3D<T,DESCRIPTOR>& blockLattice, const LBconverter<T>& converter,
 Dynamics<T, DESCRIPTOR>& bulkDynamics)//, int _iX, int _iY, int _iZ, Dynamics<T, DESCRIPTOR>& _bulkDynamics, Cell<T, DESCRIPTOR> _cell)
  : BlockLatticePhysF3D<T,DESCRIPTOR>(blockLattice,converter,3),
    _bulkDynamics(bulkDynamics)//, cell(_cell), iX(_iX), iY(_iY), iZ(_iZ)
{
  this->_name = "blockDynamicSmago";
}

template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticeDynSmagoConst<T,DESCRIPTOR>::operator() (std::vector<int> input)
{

  // T rho, u[3];
  // this->_blockLattice.get( input[0], input[1], input[2] ).computeRhoU(rho,u);
  BlockLatticePhysVelocity3D<T, DESCRIPTOR> velocity(this->_blockLattice, this->_converter);

  // int foundIC = (int)input[0];
  int globX = input[0];
  int globY = input[1];
  int globZ = input[2];

  // u[0]=this->_converter.physVelocity(u[0]);
  // u[1]=this->_converter.physVelocity(u[1]);
  // u[2]=this->_converter.physVelocity(u[2]);


  T shear_test[util::TensorVal<DESCRIPTOR<T> >::n];
  T u[3], U_ij[util::TensorVal<DESCRIPTOR<T> >::n];;
  T S_ij[util::TensorVal<DESCRIPTOR<T> >::n];

  //int locIC = cuboidGeometry.get_iC(globX+100,iY,iZ, sLattice.getOverlap());
  //first block

  std::vector<int> u_ppp(3,int());
  u_ppp[0]=globX+1, u_ppp[1]=globY+1, u_ppp[2]=globZ+1;
  std::vector<int> u_0pp(3,int());
  u_0pp[0]=globX,   u_0pp[1]=globY+1, u_0pp[2]=globZ+1;
  std::vector<int> u_npp(3,int());
  u_0pp[0]=globX-1, u_0pp[1]=globY+1, u_0pp[2]=globZ+1;
  //
  std::vector<int> u_p0p(3,int());
  u_p0p[0]=globX+1, u_ppp[1]=globY, u_p0p[2]=globZ+1;
  std::vector<int> u_00p(3,int());
  u_00p[0]=globX,   u_00p[1]=globY, u_00p[2]=globZ+1;
  std::vector<int> u_n0p(3,int());
  u_n0p[0]=globX-1, u_n0p[1]=globY+1, u_n0p[2]=globZ+1;
  //
  std::vector<int> u_pnp(3,int());
  u_pnp[0]=globX+1, u_pnp[1]=globY-1, u_pnp[2]=globZ+1;
  std::vector<int> u_0np(3,int());
  u_0np[0]=globX,   u_0np[1]=globY-1, u_0np[2]=globZ+1;
  std::vector<int> u_nnp(3,int());
  u_nnp[0]=globX-1, u_nnp[1]=globY-1, u_nnp[2]=globZ+1;
  //second block
  std::vector<int> u_pp0(3,int());
  u_pp0[0]=globX+1, u_pp0[1]=globY+1, u_pp0[2]=globZ;
  std::vector<int> u_0p0(3,int());
  u_0p0[0]=globX,   u_0p0[1]=globY+1, u_0p0[2]=globZ;
  std::vector<int> u_np0(3,int());
  u_np0[0]=globX-1, u_np0[1]=globY+1, u_np0[2]=globZ;
  //
  std::vector<int> u_p00(3,int());
  u_p00[0]=globX+1, u_p00[1]=globY, u_p00[2]=globZ;
  std::vector<int> u_000(3,int());
  u_000[0]=globX,   u_000[1]=globY, u_000[2]=globZ;
  std::vector<int> u_n00(3,int());
  u_n00[0]=globX-1, u_n00[1]=globY, u_n00[2]=globZ;
  //
  std::vector<int> u_pn0(3,int());
  u_pn0[0]=globX+1, u_pn0[1]=globY-1, u_pn0[2]=globZ;
  std::vector<int> u_0n0(3,int());
  u_0n0[0]=globX,   u_0n0[1]=globY-1, u_0n0[2]=globZ;
  std::vector<int> u_nn0(3,int());
  u_nn0[0]=globX-1, u_nn0[1]=globY-1, u_nn0[2]=globZ;
  //third block
  std::vector<int> u_ppn(3,int());
  u_ppn[0]=globX+1, u_ppn[1]=globY+1, u_ppn[2]=globZ-1;
  std::vector<int> u_0pn(3,int());
  u_0pn[0]=globX,   u_0pn[1]=globY+1, u_0pn[2]=globZ-1;
  std::vector<int> u_npn(3,int());
  u_npn[0]=globX-1, u_npn[1]=globY+1, u_npn[2]=globZ-1;
  //
  std::vector<int> u_p0n(3,int());
  u_p0n[0]=globX+1, u_p0n[1]=globY, u_p0n[2]=globZ-1;
  std::vector<int> u_00n(3,int());
  u_00n[0]=globX,   u_00n[1]=globY, u_00n[2]=globZ-1;
  std::vector<int> u_n0n(3,int());
  u_n0n[0]=globX-1, u_n0n[1]=globY, u_n0n[2]=globZ-1;
  //
  std::vector<int> u_pnn(3,int());
  u_pnn[0]=globX+1, u_pnn[1]=globY-1, u_pnn[2]=globZ-1;
  std::vector<int> u_0nn(3,int());
  u_0nn[0]=globX,   u_0nn[1]=globY-1, u_0nn[2]=globZ-1;
  std::vector<int> u_nnn(3,int());
  u_nnn[0]=globX-1, u_nnn[1]=globY-1, u_nnn[2]=globZ-1;



  // std::vector<int> v_ppp(3,int());
  //   v_ppp[0]=32, v_ppp[1]=32, v_ppp[2]=32;


  //Testfilter for U
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u[i]=(( (velocity(u_ppp)[i] + 2*velocity(u_0pp)[i] + velocity(u_npp)[i])*0.25
            +2*(velocity(u_p0p)[i] + 2*velocity(u_00p)[i] + velocity(u_n0p)[i])*0.25
            +(velocity(u_pnp)[i] + 2*velocity(u_0np)[i] + velocity(u_nnp)[i])*0.25)*0.25+

          2*((velocity(u_pp0)[i] + 2*velocity(u_0p0)[i] + velocity(u_np0)[i])*0.25
             +2*(velocity(u_p00)[i] + 2*velocity(u_000)[i] + velocity(u_n00)[i])*0.25
             +(velocity(u_pn0)[i] + 2*velocity(u_0n0)[i] + velocity(u_nn0)[i])*0.25)*0.25+

          ((velocity(u_ppn)[i] + 2*velocity(u_0pn)[i] + velocity(u_npn)[i])*0.25
           +2*(velocity(u_p0n)[i] + 2*velocity(u_00n)[i] + velocity(u_n0n)[i])*0.25
           +(velocity(u_pnn)[i] + 2*velocity(u_0nn)[i] + velocity(u_nnn)[i])*0.25)*0.25);
  }



  //U_ij Testfilter

  U_ij[0]=(( (velocity(u_ppp)[0]*velocity(u_ppp)[0] + 2*velocity(u_0pp)[0]*velocity(u_0pp)[0] + velocity(u_npp)[0]*velocity(u_0pp)[0])*0.25
             +2*(velocity(u_p0p)[0]*velocity(u_p0p)[0] + 2*velocity(u_00p)[0]*velocity(u_00p)[0] + velocity(u_n0p)[0]*velocity(u_00p)[0])*0.25
             +(velocity(u_pnp)[0]*velocity(u_pnp)[0] + 2*velocity(u_0np)[0]*velocity(u_0np)[0] + velocity(u_nnp)[0]*velocity(u_0np)[0])*0.25)*0.25+

           2*((velocity(u_pp0)[0]*velocity(u_pp0)[0] + 2*velocity(u_0p0)[0]*velocity(u_0p0)[0] + velocity(u_np0)[0]*velocity(u_0p0)[0])*0.25
              +2*(velocity(u_p00)[0]*velocity(u_p00)[0] + 2*velocity(u_000)[0]*velocity(u_000)[0] + velocity(u_n00)[0]*velocity(u_000)[0])*0.25
              +(velocity(u_pn0)[0]*velocity(u_pn0)[0] + 2*velocity(u_0n0)[0]*velocity(u_0n0)[0] + velocity(u_nn0)[0]*velocity(u_0n0)[0])*0.25*0.25)*0.25+

           ( (velocity(u_ppn)[0]*velocity(u_ppn)[0] + 2*velocity(u_0pn)[0]*velocity(u_0pn)[0] + velocity(u_npn)[0]*velocity(u_0pn)[0])*0.25
             +2*(velocity(u_p0n)[0]*velocity(u_p0n)[0] + 2*velocity(u_00n)[0]*velocity(u_00n)[0] + velocity(u_n0n)[0]*velocity(u_00n)[0])*0.25
             +(velocity(u_pnn)[0]*velocity(u_pnn)[0] + 2*velocity(u_0nn)[0]*velocity(u_0nn)[0] + velocity(u_nnn)[0]*velocity(u_0nn)[0])*0.25)*0.25);

  U_ij[1]=(( (velocity(u_ppp)[0]*velocity(u_ppp)[1] + 2*velocity(u_0pp)[0]*velocity(u_0pp)[1] + velocity(u_npp)[0]*velocity(u_0pp)[1])*0.25
             +2*(velocity(u_p0p)[0]*velocity(u_p0p)[1] + 2*velocity(u_00p)[0]*velocity(u_00p)[1] + velocity(u_n0p)[0]*velocity(u_00p)[1])*0.25
             +(velocity(u_pnp)[0]*velocity(u_pnp)[1] + 2*velocity(u_0np)[0]*velocity(u_0np)[1] + velocity(u_nnp)[0]*velocity(u_0np)[1])*0.25)*0.25+

           2*( (velocity(u_pp0)[0]*velocity(u_pp0)[1] + 2*velocity(u_0p0)[0]*velocity(u_0p0)[1] + velocity(u_np0)[0]*velocity(u_0p0)[1])*0.25
               +2*(velocity(u_p00)[0]*velocity(u_p00)[1] + 2*velocity(u_000)[0]*velocity(u_000)[1] + velocity(u_n00)[0]*velocity(u_000)[1])*0.25
               +(velocity(u_pn0)[0]*velocity(u_pn0)[1] + 2*velocity(u_0n0)[0]*velocity(u_0n0)[1] + velocity(u_nn0)[0]*velocity(u_0n0)[1])*0.25)*0.25+

           ( (velocity(u_ppn)[0]*velocity(u_ppn)[1] + 2*velocity(u_0pn)[0]*velocity(u_0pn)[1] + velocity(u_npn)[0]*velocity(u_0pn)[1])*0.25
             +2*(velocity(u_p0n)[0]*velocity(u_p0n)[1] + 2*velocity(u_00n)[0]*velocity(u_00n)[1] + velocity(u_n0n)[0]*velocity(u_00n)[1])*0.25
             +(velocity(u_pnn)[0]*velocity(u_pnn)[1] + 2*velocity(u_0nn)[0]*velocity(u_0nn)[1] + velocity(u_nnn)[0]*velocity(u_0nn)[1])*0.25)*0.25);

  U_ij[2]=(( (velocity(u_ppp)[0]*velocity(u_ppp)[2] + 2*velocity(u_0pp)[0]*velocity(u_0pp)[2] + velocity(u_npp)[0]*velocity(u_0pp)[2])*0.25
             +2*(velocity(u_p0p)[0]*velocity(u_p0p)[2] + 2*velocity(u_00p)[0]*velocity(u_00p)[2] + velocity(u_n0p)[0]*velocity(u_00p)[2])*0.25
             +(velocity(u_pnp)[0]*velocity(u_pnp)[2] + 2*velocity(u_0np)[0]*velocity(u_0np)[2] + velocity(u_nnp)[0]*velocity(u_0np)[2])*0.25)*0.25+

           2*( (velocity(u_pp0)[0]*velocity(u_pp0)[2] + 2*velocity(u_0p0)[0]*velocity(u_0p0)[2] + velocity(u_np0)[0]*velocity(u_0p0)[2])*0.25
               +2*(velocity(u_p00)[0]*velocity(u_p00)[2] + 2*velocity(u_000)[0]*velocity(u_000)[2] + velocity(u_n00)[0]*velocity(u_000)[2])*0.25
               +(velocity(u_pn0)[0]*velocity(u_pn0)[2] + 2*velocity(u_0n0)[0]*velocity(u_0n0)[2] + velocity(u_nn0)[0]*velocity(u_0n0)[2])*0.25)*0.25+

           ( (velocity(u_ppn)[0]*velocity(u_ppn)[2] + 2*velocity(u_0pn)[0]*velocity(u_0pn)[2] + velocity(u_npn)[0]*velocity(u_0pn)[2])*0.25
             +2*(velocity(u_p0n)[0]*velocity(u_p0n)[2] + 2*velocity(u_00n)[0]*velocity(u_00n)[2] + velocity(u_n0n)[0]*velocity(u_00n)[2])*0.25
             +(velocity(u_pnn)[0]*velocity(u_pnn)[2] + 2*velocity(u_0nn)[0]*velocity(u_0nn)[2] + velocity(u_nnn)[0]*velocity(u_0nn)[2])*0.25)*0.25);

  U_ij[3]=(( (velocity(u_ppp)[1]*velocity(u_ppp)[1] + 2*velocity(u_0pp)[1]*velocity(u_0pp)[1] + velocity(u_npp)[1]*velocity(u_0pp)[1])*0.25
             +2*(velocity(u_p0p)[1]*velocity(u_p0p)[1] + 2*velocity(u_00p)[1]*velocity(u_00p)[1] + velocity(u_n0p)[1]*velocity(u_00p)[1])*0.25
             +(velocity(u_pnp)[1]*velocity(u_pnp)[1] + 2*velocity(u_0np)[1]*velocity(u_0np)[1] + velocity(u_nnp)[1]*velocity(u_0np)[1])*0.25)*0.25+

           2*( (velocity(u_pp0)[1]*velocity(u_pp0)[1] + 2*velocity(u_0p0)[1]*velocity(u_0p0)[1] + velocity(u_np0)[1]*velocity(u_0p0)[1])*0.25
               +2*(velocity(u_p00)[1]*velocity(u_p00)[1] + 2*velocity(u_000)[1]*velocity(u_000)[1] + velocity(u_n00)[1]*velocity(u_000)[1])*0.25
               +(velocity(u_pn0)[1]*velocity(u_pn0)[1] + 2*velocity(u_0n0)[1]*velocity(u_0n0)[1] + velocity(u_nn0)[1]*velocity(u_0n0)[1])*0.25)*0.25+

           ( (velocity(u_ppn)[1]*velocity(u_ppn)[1] + 2*velocity(u_0pn)[1]*velocity(u_0pn)[1] + velocity(u_npn)[1]*velocity(u_0pn)[1])*0.25
             +2*(velocity(u_p0n)[1]*velocity(u_p0n)[1] + 2*velocity(u_00n)[1]*velocity(u_00n)[1] + velocity(u_n0n)[1]*velocity(u_00n)[1])*0.25
             +(velocity(u_pnn)[1]*velocity(u_pnn)[1] + 2*velocity(u_0nn)[1]*velocity(u_0nn)[1] + velocity(u_nnn)[1]*velocity(u_0nn)[1])*0.25)*0.25);

  U_ij[4]=(( (velocity(u_ppp)[1]*velocity(u_ppp)[2] + 2*velocity(u_0pp)[1]*velocity(u_0pp)[2] + velocity(u_npp)[1]*velocity(u_0pp)[2])*0.25
             +2*(velocity(u_p0p)[1]*velocity(u_p0p)[2] + 2*velocity(u_00p)[1]*velocity(u_00p)[2] + velocity(u_n0p)[1]*velocity(u_00p)[2])*0.25
             +(velocity(u_pnp)[1]*velocity(u_pnp)[2] + 2*velocity(u_0np)[1]*velocity(u_0np)[2] + velocity(u_nnp)[1]*velocity(u_0np)[2])*0.25)*0.25+

           2*( (velocity(u_pp0)[1]*velocity(u_pp0)[2] + 2*velocity(u_0p0)[1]*velocity(u_0p0)[2] + velocity(u_np0)[1]*velocity(u_0p0)[2])*0.25
               +2*(velocity(u_p00)[1]*velocity(u_p00)[2] + 2*velocity(u_000)[1]*velocity(u_000)[2] + velocity(u_n00)[1]*velocity(u_000)[2])*0.25
               +(velocity(u_pn0)[1]*velocity(u_pn0)[2] + 2*velocity(u_0n0)[1]*velocity(u_0n0)[2] + velocity(u_nn0)[1]*velocity(u_0n0)[2])*0.25)*0.25+

           ( (velocity(u_ppn)[1]*velocity(u_ppn)[2] + 2*velocity(u_0pn)[1]*velocity(u_0pn)[2] + velocity(u_npn)[1]*velocity(u_0pn)[2])*0.25
             +2*(velocity(u_p0n)[1]*velocity(u_p0n)[2] + 2*velocity(u_00n)[1]*velocity(u_00n)[2] + velocity(u_n0n)[1]*velocity(u_00n)[2])*0.25
             +(velocity(u_pnn)[1]*velocity(u_pnn)[2] + 2*velocity(u_0nn)[1]*velocity(u_0nn)[2] + velocity(u_nnn)[1]*velocity(u_0nn)[2])*0.25)*0.25);

  U_ij[5]=(( (velocity(u_ppp)[2]*velocity(u_ppp)[2] + 2*velocity(u_0pp)[2]*velocity(u_0pp)[2] + velocity(u_npp)[2]*velocity(u_0pp)[2])*0.25
             +2*(velocity(u_p0p)[2]*velocity(u_p0p)[2] + 2*velocity(u_00p)[2]*velocity(u_00p)[2] + velocity(u_n0p)[2]*velocity(u_00p)[2])*0.25
             +(velocity(u_pnp)[2]*velocity(u_pnp)[2] + 2*velocity(u_0np)[2]*velocity(u_0np)[2] + velocity(u_nnp)[2]*velocity(u_0np)[2])*0.25)*0.25+

           2*( (velocity(u_pp0)[2]*velocity(u_pp0)[2] + 2*velocity(u_0p0)[2]*velocity(u_0p0)[2] + velocity(u_np0)[2]*velocity(u_0p0)[2])*0.25
               +2*(velocity(u_p00)[2]*velocity(u_p00)[2] + 2*velocity(u_000)[2]*velocity(u_000)[2] + velocity(u_n00)[2]*velocity(u_000)[2])*0.25
               +(velocity(u_pn0)[2]*velocity(u_pn0)[2] + 2*velocity(u_0n0)[2]*velocity(u_0n0)[2] + velocity(u_nn0)[2]*velocity(u_0n0)[2])*0.25)*0.25+

           ( (velocity(u_ppn)[2]*velocity(u_ppn)[2] + 2*velocity(u_0pn)[2]*velocity(u_0pn)[2] + velocity(u_npn)[2]*velocity(u_0pn)[2])*0.25
             +2*(velocity(u_p0n)[2]*velocity(u_p0n)[2] + 2*velocity(u_00n)[2]*velocity(u_00n)[2] + velocity(u_n0n)[2]*velocity(u_00n)[2])*0.25
             +(velocity(u_pnn)[2]*velocity(u_pnn)[2] + 2*velocity(u_0nn)[2]*velocity(u_0nn)[2] + velocity(u_nnn)[2]*velocity(u_0nn)[2])*0.25)*0.25);

  //////////////////////////////BUG FIX SECOND LAYER/////////////////////////////////
  /// Shear rate with test filtered velocities

  ///////////////////////////positive x ///////////////////////////
  std::vector<int> u_2ppp(3,int());
  u_ppp[0]=globX+2, u_ppp[1]=globY+1, u_ppp[2]=globZ+1;
  std::vector<int> u_2p0p(3,int());
  u_p0p[0]=globX+2, u_ppp[1]=globY, u_p0p[2]=globZ+1;
  std::vector<int> u_2pnp(3,int());
  u_pnp[0]=globX+2, u_pnp[1]=globY-1, u_pnp[2]=globZ+1;

  std::vector<int> u_2pp0(3,int());
  u_pp0[0]=globX+2, u_pp0[1]=globY+1, u_pp0[2]=globZ;
  std::vector<int> u_2p00(3,int());
  u_p00[0]=globX+2, u_p00[1]=globY, u_p00[2]=globZ;
  std::vector<int> u_2pn0(3,int());
  u_pn0[0]=globX+2, u_pn0[1]=globY-1, u_pn0[2]=globZ;

  std::vector<int> u_2ppn(3,int());
  u_ppn[0]=globX+2, u_ppn[1]=globY+1, u_ppn[2]=globZ-1;
  std::vector<int> u_2p0n(3,int());
  u_p0n[0]=globX+2, u_p0n[1]=globY, u_p0n[2]=globZ-1;
  std::vector<int> u_2pnn(3,int());
  u_pnn[0]=globX+2, u_pnn[1]=globY-1, u_pnn[2]=globZ-1;

  ///////////////////////////negative x ///////////////////////////
  std::vector<int> u_2npp(3,int());
  u_ppp[0]=globX-2, u_ppp[1]=globY+1, u_ppp[2]=globZ+1;
  std::vector<int> u_2n0p(3,int());
  u_p0p[0]=globX-2, u_ppp[1]=globY, u_p0p[2]=globZ+1;
  std::vector<int> u_2nnp(3,int());
  u_pnp[0]=globX-2, u_pnp[1]=globY-1, u_pnp[2]=globZ+1;

  std::vector<int> u_2np0(3,int());
  u_pp0[0]=globX-2, u_pp0[1]=globY+1, u_pp0[2]=globZ;
  std::vector<int> u_2n00(3,int());
  u_p00[0]=globX-2, u_p00[1]=globY, u_p00[2]=globZ;
  std::vector<int> u_2nn0(3,int());
  u_pn0[0]=globX-2, u_pn0[1]=globY-1, u_pn0[2]=globZ;

  std::vector<int> u_2npn(3,int());
  u_ppn[0]=globX-2, u_ppn[1]=globY+1, u_ppn[2]=globZ-1;
  std::vector<int> u_2n0n(3,int());
  u_p0n[0]=globX-2, u_p0n[1]=globY, u_p0n[2]=globZ-1;
  std::vector<int> u_2nnn(3,int());
  u_pnn[0]=globX-2, u_pnn[1]=globY-1, u_pnn[2]=globZ-1;

  ///////////////////////////positive y ///////////////////////////
  std::vector<int> u_p2pp(3,int());
  u_ppp[0]=globX+1, u_ppp[1]=globY+2, u_ppp[2]=globZ+1;
  std::vector<int> u_02pp(3,int());
  u_0pp[0]=globX,   u_0pp[1]=globY+2, u_0pp[2]=globZ+1;
  std::vector<int> u_n2pp(3,int());
  u_0pp[0]=globX-1, u_0pp[1]=globY+2, u_0pp[2]=globZ+1;

  std::vector<int> u_p2p0(3,int());
  u_pp0[0]=globX+1, u_pp0[1]=globY+2, u_pp0[2]=globZ;
  std::vector<int> u_02p0(3,int());
  u_0p0[0]=globX,   u_0p0[1]=globY+2, u_0p0[2]=globZ;
  std::vector<int> u_n2p0(3,int());
  u_np0[0]=globX-1, u_np0[1]=globY+2, u_np0[2]=globZ;

  std::vector<int> u_p2pn(3,int());
  u_ppn[0]=globX+1, u_ppn[1]=globY+2, u_ppn[2]=globZ-1;
  std::vector<int> u_02pn(3,int());
  u_0pn[0]=globX,   u_0pn[1]=globY+2, u_0pn[2]=globZ-1;
  std::vector<int> u_n2pn(3,int());
  u_npn[0]=globX-1, u_npn[1]=globY+2, u_npn[2]=globZ-1;

  ///////////////////////////negative y ///////////////////////////
  std::vector<int> u_p2np(3,int());
  u_ppp[0]=globX+1, u_ppp[1]=globY-2, u_ppp[2]=globZ+1;
  std::vector<int> u_02np(3,int());
  u_0pp[0]=globX,   u_0pp[1]=globY-2, u_0pp[2]=globZ+1;
  std::vector<int> u_n2np(3,int());
  u_0pp[0]=globX-1, u_0pp[1]=globY-2, u_0pp[2]=globZ+1;

  std::vector<int> u_p2n0(3,int());
  u_pp0[0]=globX+1, u_pp0[1]=globY-2, u_pp0[2]=globZ;
  std::vector<int> u_02n0(3,int());
  u_0p0[0]=globX,   u_0p0[1]=globY-2, u_0p0[2]=globZ;
  std::vector<int> u_n2n0(3,int());
  u_np0[0]=globX-1, u_np0[1]=globY-2, u_np0[2]=globZ;

  std::vector<int> u_p2nn(3,int());
  u_ppn[0]=globX+1, u_ppn[1]=globY-2, u_ppn[2]=globZ-1;
  std::vector<int> u_02nn(3,int());
  u_0pn[0]=globX,   u_0pn[1]=globY-2, u_0pn[2]=globZ-1;
  std::vector<int> u_n2nn(3,int());
  u_npn[0]=globX-1, u_npn[1]=globY-2, u_npn[2]=globZ-1;

  ///////////////////////////positive z ///////////////////////////
  std::vector<int> u_pp2p(3,int());
  u_ppp[0]=globX+1, u_ppp[1]=globY+1, u_ppp[2]=globZ+2;
  std::vector<int> u_0p2p(3,int());
  u_0pp[0]=globX,   u_0pp[1]=globY+1, u_0pp[2]=globZ+2;
  std::vector<int> u_np2p(3,int());
  u_0pp[0]=globX-1, u_0pp[1]=globY+1, u_0pp[2]=globZ+2;
  //
  std::vector<int> u_p02p(3,int());
  u_p0p[0]=globX+1, u_ppp[1]=globY, u_p0p[2]=globZ+2;
  std::vector<int> u_002p(3,int());
  u_00p[0]=globX,   u_00p[1]=globY, u_00p[2]=globZ+2;
  std::vector<int> u_n02p(3,int());
  u_n0p[0]=globX-1, u_n0p[1]=globY+1, u_n0p[2]=globZ+2;
  //
  std::vector<int> u_pn2p(3,int());
  u_pnp[0]=globX+1, u_pnp[1]=globY-1, u_pnp[2]=globZ+2;
  std::vector<int> u_0n2p(3,int());
  u_0np[0]=globX,   u_0np[1]=globY-1, u_0np[2]=globZ+2;
  std::vector<int> u_nn2p(3,int());
  u_nnp[0]=globX-1, u_nnp[1]=globY-1, u_nnp[2]=globZ+2;

  ///////////////////////////negative z ///////////////////////////
  std::vector<int> u_pp2n(3,int());
  u_ppp[0]=globX+1, u_ppp[1]=globY+1, u_ppp[2]=globZ-2;
  std::vector<int> u_0p2n(3,int());
  u_0pp[0]=globX,   u_0pp[1]=globY+1, u_0pp[2]=globZ-2;
  std::vector<int> u_np2n(3,int());
  u_0pp[0]=globX-1, u_0pp[1]=globY+1, u_0pp[2]=globZ-2;
  //
  std::vector<int> u_p02n(3,int());
  u_p0p[0]=globX+1, u_ppp[1]=globY, u_p0p[2]=globZ-2;
  std::vector<int> u_002n(3,int());
  u_00p[0]=globX,   u_00p[1]=globY, u_00p[2]=globZ-2;
  std::vector<int> u_n02n(3,int());
  u_n0p[0]=globX-1, u_n0p[1]=globY+1, u_n0p[2]=globZ-2;
  //
  std::vector<int> u_pn2n(3,int());
  u_pnp[0]=globX+1, u_pnp[1]=globY-1, u_pnp[2]=globZ-2;
  std::vector<int> u_0n2n(3,int());
  u_0np[0]=globX,   u_0np[1]=globY-1, u_0np[2]=globZ-2;
  std::vector<int> u_nn2n(3,int());
  u_nnp[0]=globX-1, u_nnp[1]=globY-1, u_nnp[2]=globZ-2;


  ////////////////////////////////// second layer filtering for the velocities/////////////////////////////////////////////////
  T u_p00_fil[3], u_n00_fil[3], u_0p0_fil[3], u_0n0_fil[3], u_00p_fil[3], u_00n_fil[3];
  ///////////////////////////positive x ///////////////////////////
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u_p00_fil[i]=(( (velocity(u_2ppp)[i] + 2*velocity(u_ppp)[i] + velocity(u_0pp)[i])*0.25
                    +2*(velocity(u_2p0p)[i] + 2*velocity(u_p0p)[i] + velocity(u_00p)[i])*0.25
                    +(velocity(u_2pnp)[i] + 2*velocity(u_pnp)[i] + velocity(u_0np)[i])*0.25)*0.25+

                  2*((velocity(u_2pp0)[i] + 2*velocity(u_pp0)[i] + velocity(u_0p0)[i])*0.25
                     +2*(velocity(u_2p00)[i] + 2*velocity(u_p00)[i] + velocity(u_000)[i])*0.25
                     +(velocity(u_2pn0)[i] + 2*velocity(u_pn0)[i] + velocity(u_0n0)[i])*0.25)*0.25+

                  ((velocity(u_2ppn)[i] + 2*velocity(u_ppn)[i] + velocity(u_0pn)[i])*0.25
                   +2*(velocity(u_2p0n)[i] + 2*velocity(u_p0n)[i] + velocity(u_00n)[i])*0.25
                   +(velocity(u_2pnn)[i] + 2*velocity(u_pnn)[i] + velocity(u_0nn)[i])*0.25)*0.25);
  }


  ///////////////////////////negative x ///////////////////////////
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u_n00_fil[i]=(( (velocity(u_0pp)[i] + 2*velocity(u_npp)[i] + velocity(u_2npp)[i])*0.25
                    +2*(velocity(u_00p)[i] + 2*velocity(u_n0p)[i] + velocity(u_2n0p)[i])*0.25
                    +(velocity(u_0np)[i] + 2*velocity(u_nnp)[i] + velocity(u_2nnp)[i])*0.25)*0.25+

                  2*((velocity(u_0p0)[i] + 2*velocity(u_np0)[i] + velocity(u_2np0)[i])*0.25
                     +2*(velocity(u_000)[i] + 2*velocity(u_n00)[i] + velocity(u_2n00)[i])*0.25
                     +(velocity(u_0n0)[i] + 2*velocity(u_nn0)[i] + velocity(u_2nn0)[i])*0.25)*0.25+

                  ((velocity(u_0pn)[i] + 2*velocity(u_npn)[i] + velocity(u_2npn)[i])*0.25
                   +2*(velocity(u_00n)[i] + 2*velocity(u_n0n)[i] + velocity(u_2n0n)[i])*0.25
                   +(velocity(u_0nn)[i] + 2*velocity(u_nnn)[i] + velocity(u_2nnn)[i])*0.25)*0.25);
  }
  ///////////////////////////positive y ///////////////////////////
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u_0p0_fil[i]=(( (velocity(u_p2pp)[i] + 2*velocity(u_02pp)[i] + velocity(u_n2pp)[i])*0.25
                    +2*(velocity(u_ppp)[i] + 2*velocity(u_0pp)[i] + velocity(u_npp)[i])*0.25
                    +(velocity(u_p0p)[i] + 2*velocity(u_00p)[i] + velocity(u_n0p)[i])*0.25)*0.25+

                  2*((velocity(u_p2p0)[i] + 2*velocity(u_02p0)[i] + velocity(u_n2p0)[i])*0.25
                     +2*(velocity(u_pp0)[i] + 2*velocity(u_0p0)[i] + velocity(u_np0)[i])*0.25
                     +(velocity(u_p00)[i] + 2*velocity(u_000)[i] + velocity(u_n00)[i])*0.25)*0.25+

                  ((velocity(u_p2pn)[i] + 2*velocity(u_02pn)[i] + velocity(u_n2pn)[i])*0.25
                   +2*(velocity(u_ppn)[i] + 2*velocity(u_0pn)[i] + velocity(u_npn)[i])*0.25
                   +(velocity(u_p0n)[i] + 2*velocity(u_00n)[i] + velocity(u_n0n)[i])*0.25)*0.25);
  }


  ///////////////////////////negative y ///////////////////////////
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u_0n0_fil[i]=(( (velocity(u_p0p)[i] + 2*velocity(u_00p)[i] + velocity(u_n0p)[i])*0.25
                    +2*(velocity(u_pnp)[i] + 2*velocity(u_0np)[i] + velocity(u_nnp)[i])*0.25
                    +(velocity(u_p2np)[i] + 2*velocity(u_02np)[i] + velocity(u_n2np)[i])*0.25)*0.25+

                  2*((velocity(u_p00)[i] + 2*velocity(u_000)[i] + velocity(u_n00)[i])*0.25
                     +2*(velocity(u_pn0)[i] + 2*velocity(u_0n0)[i] + velocity(u_nn0)[i])*0.25
                     +(velocity(u_p2n0)[i] + 2*velocity(u_02n0)[i] + velocity(u_n2n0)[i])*0.25)*0.25+

                  ((velocity(u_p0n)[i] + 2*velocity(u_00n)[i] + velocity(u_n0n)[i])*0.25
                   +2*(velocity(u_pnn)[i] + 2*velocity(u_0nn)[i] + velocity(u_nnn)[i])*0.25
                   +(velocity(u_p2nn)[i] + 2*velocity(u_02nn)[i] + velocity(u_n2nn)[i])*0.25)*0.25);
  }

  ///////////////////////////positive z ///////////////////////////
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u_00p_fil[i]=(( (velocity(u_pp2p)[i] + 2*velocity(u_0p2p)[i] + velocity(u_np2p)[i])*0.25
                    +2*(velocity(u_p02p)[i] + 2*velocity(u_002p)[i] + velocity(u_n02p)[i])*0.25
                    +(velocity(u_pn2p)[i] + 2*velocity(u_0n2p)[i] + velocity(u_nn2p)[i])*0.25)*0.25+

                  2*((velocity(u_ppp)[i] + 2*velocity(u_0pp)[i] + velocity(u_npp)[i])*0.25
                     +2*(velocity(u_p0p)[i] + 2*velocity(u_00p)[i] + velocity(u_n0p)[i])*0.25
                     +(velocity(u_pnp)[i] + 2*velocity(u_0np)[i] + velocity(u_nnp)[i])*0.25)*0.25+

                  ((velocity(u_pp0)[i] + 2*velocity(u_0p0)[i] + velocity(u_np0)[i])*0.25
                   +2*(velocity(u_p00)[i] + 2*velocity(u_000)[i] + velocity(u_n00)[i])*0.25
                   +(velocity(u_pn0)[i] + 2*velocity(u_0n0)[i] + velocity(u_nn0)[i])*0.25)*0.25);
  }

  ///////////////////////////negative z ///////////////////////////
  for (int i=0; i<DESCRIPTOR<T>::d; ++i) {
    u_00n_fil[i]=(((velocity(u_pp0)[i] + 2*velocity(u_0p0)[i] + velocity(u_np0)[i])*0.25
                   +2*(velocity(u_p00)[i] + 2*velocity(u_000)[i] + velocity(u_n00)[i])*0.25
                   +(velocity(u_pn0)[i] + 2*velocity(u_0n0)[i] + velocity(u_nn0)[i])*0.25)*0.25+

                  2*((velocity(u_ppn)[i] + 2*velocity(u_0pn)[i] + velocity(u_npn)[i])*0.25
                     +2*(velocity(u_p0n)[i] + 2*velocity(u_00n)[i] + velocity(u_n0n)[i])*0.25
                     +(velocity(u_pnn)[i] + 2*velocity(u_0nn)[i] + velocity(u_nnn)[i])*0.25)*0.25+

                  ((velocity(u_pp2n)[i] + 2*velocity(u_0p2n)[i] + velocity(u_np2n)[i])*0.25
                   +2*(velocity(u_p02n)[i] + 2*velocity(u_002n)[i] + velocity(u_n02n)[i])*0.25
                   +(velocity(u_pn2n)[i] + 2*velocity(u_0n2n)[i] + velocity(u_nn2n)[i])*0.25)*0.25);
  }




  T dx=this->_converter.getDeltaX();


  ////////shear test with filtered velocities
  T shear_test_fil[util::TensorVal<DESCRIPTOR<T> >::n];

  shear_test_fil[0] = 1./2./dx*(fd::centralGradient(u_p00_fil[0], u_n00_fil[0]) + fd::centralGradient(u_p00_fil[0], u_n00_fil[0]));//dudx+dudx
  shear_test_fil[1] = 1./2./dx*(fd::centralGradient(u_0p0_fil[0], u_0n0_fil[0]) + fd::centralGradient(u_p00_fil[1], u_n00_fil[1]));//dudy+dvdx
  shear_test_fil[2] = 1./2./dx*(fd::centralGradient(u_00p_fil[0], u_00n_fil[0]) + fd::centralGradient(u_p00_fil[2], u_n00_fil[2]));//dudz+dwdx

  shear_test_fil[3] = 1./2./dx*(fd::centralGradient(u_0p0_fil[1], u_0n0_fil[1]) + fd::centralGradient(u_0p0_fil[1], u_0n0_fil[1]));//dvdy+dvdy
  shear_test_fil[4] = 1./2./dx*(fd::centralGradient(u_00p_fil[1], u_00n_fil[1]) + fd::centralGradient(u_0p0_fil[2], u_0n0_fil[2]));//dvdz+dwdy
  shear_test_fil[5] = 1./2./dx*(fd::centralGradient(u_00p_fil[2], u_00n_fil[2]) + fd::centralGradient(u_00p_fil[2], u_00n_fil[2]));//dwdz+dwdz

  ///////////////////////////////////////////////////////////////////////////////////////////////////



  ////////shear test with normal velocities
  shear_test[0] = 1./2./dx*(fd::centralGradient(velocity(u_p00)[0],velocity(u_n00)[0])+fd::centralGradient(velocity(u_p00)[0],velocity(u_n00)[0]));//dudx+dudx
  shear_test[1] = 1./2./dx*(fd::centralGradient(velocity(u_0p0)[0],velocity(u_0n0)[0])+fd::centralGradient(velocity(u_p00)[1],velocity(u_n00)[1]));//dudy+dvdx
  shear_test[2] = 1./2./dx*(fd::centralGradient(velocity(u_00p)[0],velocity(u_00n)[0])+fd::centralGradient(velocity(u_p00)[2],velocity(u_n00)[2]));//dudz+dwdx

  shear_test[3] = 1./2./dx*(fd::centralGradient(velocity(u_0p0)[1],velocity(u_0n0)[1])+fd::centralGradient(velocity(u_0p0)[1],velocity(u_0n0)[1]));//dvdy+dvdy
  shear_test[4] = 1./2./dx*(fd::centralGradient(velocity(u_00p)[1],velocity(u_00n)[1])+fd::centralGradient(velocity(u_0p0)[2],velocity(u_0n0)[2]));//dvdz+dwdy
  shear_test[5] = 1./2./dx*(fd::centralGradient(velocity(u_00p)[2],velocity(u_00n)[2])+fd::centralGradient(velocity(u_00p)[2],velocity(u_00n)[2]));//dwdz+dwdz

  // std::cout << "wat is mit S_ij .. behave yourself: " << shear_test[0]<<std::endl;


  T PiNeqNormSqr = shear_test[0]*shear_test[0] + 2.0*shear_test[1]*shear_test[1] + shear_test[2]*shear_test[2]

                   +shear_test[2]*shear_test[2] + shear_test[3]*shear_test[3] + 2*shear_test[4]*shear_test[4] +shear_test[5]*shear_test[5];
  T PiNeqNorm    = sqrt(2*PiNeqNormSqr);
  for (int i = 0; i < util::TensorVal<DESCRIPTOR<T> >::n; ++i ) {
    shear_test[i] = shear_test[i]*PiNeqNorm;
  }

  //     // test filtered shear

  // std::cout << "wat is mit S_ij .. behave yourseld: " <<PiNeqNormSqr_nnn<< " " <<stress(S_nnn)[0]*stress(S_nnn)[0]<< " " <<stress(S_nnn)[1]*stress(S_nnn)[1]<< " " <<stress(S_nnn)[2]*stress(S_nnn)[2]
  // << " " <<stress(S_nnn)[3]*stress(S_nnn)[3]<< " " <<stress(S_nnn)[4]*stress(S_nnn)[4]<< " " <<stress(S_nnn)[5]*stress(S_nnn)[5]<<
  // "                   " <<2*(stress(S_nnn)[0]*stress(S_nnn)[0] + 2.0*stress(S_nnn)[1]*stress(S_nnn)[1] + stress(S_nnn)[2]*stress(S_nnn)[2]
  //                       +stress(S_nnn)[2]*stress(S_nnn)[2] + stress(S_nnn)[3]*stress(S_nnn)[3] + 2*stress(S_nnn)[4]*stress(S_nnn)[4] +S_nnn[5]*S_nnn[5])<<std::endl;//M_ij[0][0] <<std::endl;

  BlockLatticeStress3D<T, DESCRIPTOR> stress(this->_blockLattice, this->_converter);
  //first block
  std::vector<int> S_ppp(3,int());
  S_ppp[0]=globX+1, S_ppp[1]=globY+1, S_ppp[2]=globZ+1;
  std::vector<int> S_0pp(3,int());
  S_0pp[0]=globX,   S_0pp[1]=globY+1, S_0pp[2]=globZ+1;
  std::vector<int> S_npp(3,int());
  S_0pp[0]=globX-1, S_0pp[1]=globY+1, S_0pp[2]=globZ+1;
  //
  std::vector<int> S_p0p(3,int());
  S_p0p[0]=globX+1, S_ppp[1]=globY,  S_p0p[2]=globZ+1;
  std::vector<int> S_00p(3,int());
  S_00p[0]=globX,   S_00p[1]=globY,  S_00p[2]=globZ+1;
  std::vector<int> S_n0p(3,int());
  S_n0p[0]=globX-1, S_n0p[1]=globY+1, S_n0p[2]=globZ+1;
  //
  std::vector<int> S_pnp(3,int());
  S_pnp[0]=globX+1, S_pnp[1]=globY-1, S_pnp[2]=globZ+1;
  std::vector<int> S_0np(3,int());
  S_0np[0]=globX,   S_0np[1]=globY-1, S_0np[2]=globZ+1;
  std::vector<int> S_nnp(3,int());
  S_nnp[0]=globX-1, S_nnp[1]=globY-1, S_nnp[2]=globZ+1;
  //second block
  std::vector<int> S_pp0(3,int());
  S_pp0[0]=globX+1, S_pp0[1]=globY+1, S_pp0[2]=globZ;
  std::vector<int> S_0p0(3,int());
  S_0p0[0]=globX,   S_0p0[1]=globY+1, S_0p0[2]=globZ;
  std::vector<int> S_np0(3,int());
  S_np0[0]=globX-1, S_np0[1]=globY+1, S_np0[2]=globZ;
  //
  std::vector<int> S_p00(3,int());
  S_p00[0]=globX+1, S_p00[1]=globY, S_p00[2]=globZ;
  std::vector<int> S_000(3,int());
  S_000[0]=globX,   S_000[1]=globY, S_000[2]=globZ;
  std::vector<int> S_n00(3,int());
  S_n00[0]=globX-1, S_n00[1]=globY, S_n00[2]=globZ;
  //
  std::vector<int> S_pn0(3,int());
  S_pn0[0]=globX+1, S_pn0[1]=globY-1, S_pn0[1]=globZ;
  std::vector<int> S_0n0(3,int());
  S_0n0[0]=globX,   S_0n0[1]=globY-1, S_0n0[1]=globZ;
  std::vector<int> S_nn0(3,int());
  S_nn0[0]=globX-1, S_nn0[1]=globY-1, S_nn0[1]=globZ;
  //third block
  std::vector<int> S_ppn(3,int());
  S_ppn[0]=globX+1, S_ppn[1]=globY+1, S_ppn[2]=globZ-1;
  std::vector<int> S_0pn(3,int());
  S_0pn[0]=globX,   S_0pn[1]=globY+1, S_0pn[2]=globZ-1;
  std::vector<int> S_npn(3,int());
  S_npn[0]=globX-1, S_npn[1]=globY+1, S_npn[2]=globZ-1;
  //
  std::vector<int> S_p0n(3,int());
  S_p0n[0]=globX+1, S_p0n[1]=globY, S_p0n[2]=globZ-1;
  std::vector<int> S_00n(3,int());
  S_00n[0]=globX,   S_00n[1]=globY, S_00n[2]=globZ-1;
  std::vector<int> S_n0n(3,int());
  S_n0n[0]=globX-1, S_n0n[1]=globY, S_n0n[2]=globZ-1;
  //
  std::vector<int> S_pnn(3,int());
  S_pnn[0]=globX+1, S_pnn[1]=globY-1, S_pnn[2]=globZ-1;
  std::vector<int> S_0nn(3,int());
  S_0nn[0]=globX, S_0nn[1]=globY-1,   S_0nn[2]=globZ-1;
  std::vector<int> S_nnn(3,int());
  S_nnn[0]=globX-1, S_nnn[1]=globY-1, S_nnn[2]=globZ-1;

  //first block

  T PiNeqNormSqr_ppp = sqrt(2*(stress(S_ppp)[0]*stress(S_ppp)[0] + 2.0*stress(S_ppp)[1]*stress(S_ppp)[1] + stress(S_ppp)[2]*stress(S_ppp)[2]
                               +stress(S_ppp)[2]*stress(S_ppp)[2] + stress(S_ppp)[3]*stress(S_ppp)[3] + 2*stress(S_ppp)[4]*stress(S_ppp)[4] +stress(S_ppp)[5]*stress(S_ppp)[5]));

  T PiNeqNormSqr_0pp = sqrt(2*(stress(S_0pp)[0]*stress(S_0pp)[0] + 2.0*stress(S_0pp)[1]*stress(S_0pp)[1] + stress(S_0pp)[2]*stress(S_0pp)[2]
                               +stress(S_0pp)[2]*stress(S_0pp)[2] + stress(S_0pp)[3]*stress(S_0pp)[3] + 2*stress(S_0pp)[4]*stress(S_0pp)[4] +stress(S_0pp)[5]*stress(S_0pp)[5]));

  T PiNeqNormSqr_npp = sqrt(2*(stress(S_npp)[0]*stress(S_npp)[0] + 2.0*stress(S_npp)[1]*stress(S_npp)[1] + stress(S_npp)[2]*stress(S_npp)[2]
                               +stress(S_npp)[2]*stress(S_npp)[2] + stress(S_npp)[3]*stress(S_npp)[3] + 2*stress(S_npp)[4]*stress(S_npp)[4] +stress(S_npp)[5]*stress(S_npp)[5]));
  //

  T PiNeqNormSqr_p0p = sqrt(2*(stress(S_p0p)[0]*stress(S_p0p)[0] + 2.0*stress(S_p0p)[1]*stress(S_p0p)[1] + stress(S_p0p)[2]*stress(S_p0p)[2]
                               +stress(S_p0p)[2]*stress(S_p0p)[2] + stress(S_p0p)[3]*stress(S_p0p)[3] + 2*stress(S_p0p)[4]*stress(S_p0p)[4] +stress(S_p0p)[5]*stress(S_p0p)[5]));

  T PiNeqNormSqr_00p = sqrt(2*(stress(S_00p)[0]*stress(S_00p)[0] + 2.0*stress(S_00p)[1]*stress(S_00p)[1] + stress(S_00p)[2]*stress(S_00p)[2]
                               +stress(S_00p)[2]*stress(S_00p)[2] + stress(S_00p)[3]*stress(S_00p)[3] + 2*stress(S_00p)[4]*stress(S_00p)[4] +stress(S_00p)[5]*stress(S_00p)[5]));

  T PiNeqNormSqr_n0p = sqrt(2*(stress(S_n0p)[0]*stress(S_n0p)[0] + 2.0*stress(S_n0p)[1]*stress(S_n0p)[1] + stress(S_n0p)[2]*stress(S_n0p)[2]
                               +stress(S_n0p)[2]*stress(S_n0p)[2] + stress(S_n0p)[3]*stress(S_n0p)[3] + 2*stress(S_n0p)[4]*stress(S_n0p)[4] +stress(S_n0p)[5]*stress(S_n0p)[5]));
  //

  T PiNeqNormSqr_pnp = sqrt(2*(stress(S_pnp)[0]*stress(S_pnp)[0] + 2.0*stress(S_pnp)[1]*stress(S_pnp)[1] + stress(S_pnp)[2]*stress(S_pnp)[2]
                               +stress(S_pnp)[2]*stress(S_pnp)[2] + stress(S_pnp)[3]*stress(S_pnp)[3] + 2*stress(S_pnp)[4]*stress(S_pnp)[4] +stress(S_pnp)[5]*stress(S_pnp)[5]));

  T PiNeqNormSqr_0np = sqrt(2*(stress(S_0np)[0]*stress(S_0np)[0] + 2.0*stress(S_0np)[1]*stress(S_0np)[1] + stress(S_0np)[2]*stress(S_0np)[2]
                               +stress(S_0np)[2]*stress(S_0np)[2] + stress(S_0np)[3]*stress(S_0np)[3] + 2*stress(S_0np)[4]*stress(S_0np)[4] +stress(S_0np)[5]*stress(S_0np)[5]));

  T PiNeqNormSqr_nnp = sqrt(2*(stress(S_nnp)[0]*stress(S_nnp)[0] + 2.0*stress(S_nnp)[1]*stress(S_nnp)[1] + stress(S_nnp)[2]*stress(S_nnp)[2]
                               +stress(S_nnp)[2]*stress(S_nnp)[2] + stress(S_nnp)[3]*stress(S_nnp)[3] + 2*stress(S_nnp)[4]*stress(S_nnp)[4] +stress(S_nnp)[5]*stress(S_nnp)[5]));

  //second block

  T PiNeqNormSqr_pp0 = sqrt(2*(stress(S_pp0)[0]*stress(S_pp0)[0] + 2.0*stress(S_pp0)[1]*stress(S_pp0)[1] + stress(S_pp0)[2]*stress(S_pp0)[2]
                               +stress(S_pp0)[2]*stress(S_pp0)[2] + stress(S_pp0)[3]*stress(S_pp0)[3] + 2*stress(S_pp0)[4]*stress(S_pp0)[4] +stress(S_pp0)[5]*stress(S_pp0)[5]));

  T PiNeqNormSqr_0p0 = sqrt(2*(stress(S_0p0)[0]*stress(S_0p0)[0] + 2.0*stress(S_0p0)[1]*stress(S_0p0)[1] + stress(S_0p0)[2]*stress(S_0p0)[2]
                               +stress(S_0p0)[2]*stress(S_0p0)[2] + stress(S_0p0)[3]*stress(S_0p0)[3] + 2*stress(S_0p0)[4]*stress(S_0p0)[4] +stress(S_0p0)[5]*stress(S_0p0)[5]));

  T PiNeqNormSqr_np0 = sqrt(2*(stress(S_np0)[0]*stress(S_np0)[0] + 2.0*stress(S_np0)[1]*stress(S_np0)[1] + stress(S_np0)[2]*stress(S_np0)[2]
                               +stress(S_np0)[2]*stress(S_np0)[2] + stress(S_np0)[3]*stress(S_np0)[3] + 2*stress(S_np0)[4]*stress(S_np0)[4] +stress(S_np0)[5]*stress(S_np0)[5]));
  //

  T PiNeqNormSqr_p00 = sqrt(2*(stress(S_p00)[0]*stress(S_p00)[0] + 2.0*stress(S_p00)[1]*stress(S_p00)[1] + stress(S_p00)[2]*stress(S_p00)[2]
                               +stress(S_p00)[2]*stress(S_p00)[2] + stress(S_p00)[3]*stress(S_p00)[3] + 2*stress(S_p00)[4]*stress(S_p00)[4] +stress(S_p00)[5]*stress(S_p00)[5]));

  T PiNeqNormSqr_000 = sqrt(2*(stress(S_000)[0]*stress(S_000)[0] + 2.0*stress(S_000)[1]*stress(S_000)[1] + stress(S_000)[2]*stress(S_000)[2]
                               +stress(S_000)[2]*stress(S_000)[2] + stress(S_000)[3]*stress(S_000)[3] + 2*stress(S_000)[4]*stress(S_000)[4] +stress(S_000)[5]*stress(S_000)[5]));

  T PiNeqNormSqr_n00 = sqrt(2*(stress(S_n00)[0]*stress(S_n00)[0] + 2.0*stress(S_n00)[1]*stress(S_n00)[1] + stress(S_n00)[2]*stress(S_n00)[2]
                               +stress(S_n00)[2]*stress(S_n00)[2] + stress(S_n00)[3]*stress(S_n00)[3] + 2*stress(S_n00)[4]*stress(S_n00)[4] +stress(S_n00)[5]*stress(S_n00)[5]));
  //

  T PiNeqNormSqr_pn0 = sqrt(2*(stress(S_pn0)[0]*stress(S_pn0)[0] + 2.0*stress(S_pn0)[1]*stress(S_pn0)[1] + stress(S_pn0)[2]*stress(S_pn0)[2]
                               +stress(S_pn0)[2]*stress(S_pn0)[2] + stress(S_pn0)[3]*stress(S_pn0)[3] + 2*stress(S_pn0)[4]*stress(S_pn0)[4] +stress(S_pn0)[5]*stress(S_pn0)[5]));

  T PiNeqNormSqr_0n0 = sqrt(2*(stress(S_0n0)[0]*stress(S_0n0)[0] + 2.0*stress(S_0n0)[1]*stress(S_0n0)[1] + stress(S_0n0)[2]*stress(S_0n0)[2]
                               +stress(S_0n0)[2]*stress(S_0n0)[2] + stress(S_0n0)[3]*stress(S_0n0)[3] + 2*stress(S_0n0)[4]*stress(S_0n0)[4] +stress(S_0n0)[5]*stress(S_0n0)[5]));

  T PiNeqNormSqr_nn0 = sqrt(2*(stress(S_nn0)[0]*stress(S_nn0)[0] + 2.0*stress(S_nn0)[1]*stress(S_nn0)[1] + stress(S_nn0)[2]*stress(S_nn0)[2]
                               +stress(S_nn0)[2]*stress(S_nn0)[2] + stress(S_nn0)[3]*stress(S_nn0)[3] + 2*stress(S_nn0)[4]*stress(S_nn0)[4] +stress(S_nn0)[5]*stress(S_nn0)[5]));

  //third block

  T PiNeqNormSqr_ppn = sqrt(2*(stress(S_ppn)[0]*stress(S_ppn)[0] + 2.0*stress(S_ppn)[1]*stress(S_ppn)[1] + stress(S_ppn)[2]*stress(S_ppn)[2]
                               +stress(S_ppn)[2]*stress(S_ppn)[2] + stress(S_ppn)[3]*stress(S_ppn)[3] + 2*stress(S_ppn)[4]*stress(S_ppn)[4] +stress(S_ppn)[5]*stress(S_ppn)[5]));

  T PiNeqNormSqr_0pn = sqrt(2*(stress(S_0pn)[0]*stress(S_0pn)[0] + 2.0*stress(S_0pn)[1]*stress(S_0pn)[1] + stress(S_0pn)[2]*stress(S_0pn)[2]
                               +stress(S_0pn)[2]*stress(S_0pn)[2] + stress(S_0pn)[3]*stress(S_0pn)[3] + 2*stress(S_0pn)[4]*stress(S_0pn)[4] +stress(S_0pn)[5]*stress(S_0pn)[5]));

  T PiNeqNormSqr_npn = sqrt(2*(stress(S_npn)[0]*stress(S_npn)[0] + 2.0*stress(S_npn)[1]*stress(S_npn)[1] + stress(S_npn)[2]*stress(S_npn)[2]
                               +stress(S_npn)[2]*stress(S_npn)[2] + stress(S_npn)[3]*stress(S_npn)[3] + 2*stress(S_npn)[4]*stress(S_npn)[4] +stress(S_npn)[5]*stress(S_npn)[5]));
  //

  T PiNeqNormSqr_p0n = sqrt(2*(stress(S_p0n)[0]*stress(S_p0n)[0] + 2.0*stress(S_p0n)[1]*stress(S_p0n)[1] + stress(S_p0n)[2]*stress(S_p0n)[2]
                               +stress(S_p0n)[2]*stress(S_p0n)[2] + stress(S_p0n)[3]*stress(S_p0n)[3] + 2*stress(S_p0n)[4]*stress(S_p0n)[4] +stress(S_p0n)[5]*stress(S_p0n)[5]));

  T PiNeqNormSqr_00n = sqrt(2*(stress(S_00n)[0]*stress(S_00n)[0] + 2.0*stress(S_00n)[1]*stress(S_00n)[1] + stress(S_00n)[2]*stress(S_00n)[2]
                               +stress(S_00n)[2]*stress(S_00n)[2] + stress(S_00n)[3]*stress(S_00n)[3] + 2*stress(S_00n)[4]*stress(S_00n)[4] +stress(S_00n)[5]*stress(S_00n)[5]));

  T PiNeqNormSqr_n0n = sqrt(2*(stress(S_n0n)[0]*stress(S_n0n)[0] + 2.0*stress(S_n0n)[1]*stress(S_n0n)[1] + stress(S_n0n)[2]*stress(S_n0n)[2]
                               +stress(S_n0n)[2]*stress(S_n0n)[2] + stress(S_n0n)[3]*stress(S_n0n)[3] + 2*stress(S_n0n)[4]*stress(S_n0n)[4] +stress(S_n0n)[5]*stress(S_n0n)[5]));
  //

  T PiNeqNormSqr_pnn = sqrt(2*(stress(S_pnn)[0]*stress(S_pnn)[0] + 2.0*stress(S_pnn)[1]*stress(S_pnn)[1] + stress(S_pnn)[2]*stress(S_pnn)[2]
                               +stress(S_pnn)[2]*stress(S_pnn)[2] + stress(S_pnn)[3]*stress(S_pnn)[3] + 2*stress(S_pnn)[4]*stress(S_pnn)[4] +stress(S_pnn)[5]*stress(S_pnn)[5]));

  T PiNeqNormSqr_0nn = sqrt(2*(stress(S_0nn)[0]*stress(S_0nn)[0] + 2.0*stress(S_0nn)[1]*stress(S_0nn)[1] + stress(S_0nn)[2]*stress(S_0nn)[2]
                               +stress(S_0nn)[2]*stress(S_0nn)[2] + stress(S_0nn)[3]*stress(S_0nn)[3] + 2*stress(S_0nn)[4]*stress(S_0nn)[4] +stress(S_0nn)[5]*stress(S_0nn)[5]));

  T PiNeqNormSqr_nnn = sqrt(2*(stress(S_nnn)[0]*stress(S_nnn)[0] + 2.0*stress(S_nnn)[1]*stress(S_nnn)[1] + stress(S_nnn)[2]*stress(S_nnn)[2]
                               +stress(S_nnn)[2]*stress(S_nnn)[2] + stress(S_nnn)[3]*stress(S_nnn)[3] + 2*stress(S_nnn)[4]*stress(S_nnn)[4] +stress(S_nnn)[5]*stress(S_nnn)[5]));


  // std::cout << "wat is mit S_ij aus den verteilungen: " <<PiNeqNormSqr_nnn<< " " <<stress(S_nnn)[0]*stress(S_nnn)[0]<< " " <<stress(S_nnn)[1]*stress(S_nnn)[1]<< " " <<stress(S_nnn)[2]*stress(S_nnn)[2]
  // << " " <<stress(S_nnn)[3]*stress(S_nnn)[3]<< " " <<stress(S_nnn)[4]*stress(S_nnn)[4]<< " " <<stress(S_nnn)[5]*stress(S_nnn)[5]<<
  // "                   " <<2*(stress(S_nnn)[0]*stress(S_nnn)[0] + 2.0*stress(S_nnn)[1]*stress(S_nnn)[1] + stress(S_nnn)[2]*stress(S_nnn)[2]
  //                       +stress(S_nnn)[2]*stress(S_nnn)[2] + stress(S_nnn)[3]*stress(S_nnn)[3] + 2*stress(S_nnn)[4]*stress(S_nnn)[4] +stress(S_nnn)[5]*stress(S_nnn)[5])<<std::endl;//M_ij[0][0] <<std::endl;







  for (int i = 0; i < util::TensorVal<DESCRIPTOR<T> >::n; ++i ) {
    S_ij[i] = (( (stress(S_ppp)[i]*PiNeqNormSqr_ppp + 2*stress(S_0pp)[i]*PiNeqNormSqr_0pp + stress(S_npp)[i]*PiNeqNormSqr_npp)*0.25
                 +2*(stress(S_p0p)[i]*PiNeqNormSqr_p0p + 2*stress(S_00p)[i]*PiNeqNormSqr_00p + stress(S_n0p)[i]*PiNeqNormSqr_n0p)*0.25
                 +(stress(S_pnp)[i]*PiNeqNormSqr_pnp + 2*stress(S_0np)[i]*PiNeqNormSqr_0np + stress(S_nnp)[i]*PiNeqNormSqr_nnp)*0.25)*0.25+

               2*(( stress(S_pp0)[i]*PiNeqNormSqr_pp0 + 2*stress(S_0p0)[i]*PiNeqNormSqr_0p0 + stress(S_np0)[i]*PiNeqNormSqr_np0)*0.25
                  +2*(stress(S_p00)[i]*PiNeqNormSqr_p00 + 2*stress(S_000)[i]*PiNeqNormSqr_000 + stress(S_n00)[i]*PiNeqNormSqr_n00)*0.25
                  +(stress(S_pn0)[i]*PiNeqNormSqr_pn0 + 2*stress(S_0n0)[i]*PiNeqNormSqr_0n0 + stress(S_nn0)[i]*PiNeqNormSqr_nn0)*0.25)*0.25+

               ((stress(S_ppn)[i]*PiNeqNormSqr_ppn + 2*stress(S_0pn)[i]*PiNeqNormSqr_0pn + stress(S_npn)[i]*PiNeqNormSqr_npn)*0.25
                +2*(stress(S_p0n)[i]*PiNeqNormSqr_p0n + 2*stress(S_00n)[i]*PiNeqNormSqr_00n + stress(S_n0n)[i]*PiNeqNormSqr_n0n)*0.25
                +(stress(S_pnn)[i]*PiNeqNormSqr_pnn + 2*stress(S_0nn)[i]*PiNeqNormSqr_0nn + stress(S_nnn)[i]*PiNeqNormSqr_nnn)*0.25)*0.25);
  }


  //actual smagoConst evaluation
  T M_ij[3][3];
  M_ij[0][0] = (2*dx*2*dx)*shear_test[0]-dx*dx*S_ij[0];
  M_ij[0][1] = (2*dx*2*dx)*shear_test[1]-dx*dx*S_ij[1];
  M_ij[0][2] = (2*dx*2*dx)*shear_test[2]-dx*dx*S_ij[2];
  M_ij[1][0] = (2*dx*2*dx)*shear_test[1]-dx*dx*S_ij[1];
  M_ij[1][1] = (2*dx*2*dx)*shear_test[3]-dx*dx*S_ij[3];
  M_ij[1][2] = (2*dx*2*dx)*shear_test[4]-dx*dx*S_ij[4];
  M_ij[2][0] = (2*dx*2*dx)*shear_test[2]-dx*dx*S_ij[2];
  M_ij[2][1] = (2*dx*2*dx)*shear_test[4]-dx*dx*S_ij[4];
  M_ij[2][2] = (2*dx*2*dx)*shear_test[5]-dx*dx*S_ij[5];



  // std::cout << "wat is mit M denn los a: " <<shear_test[0]<< " "<<S_ij[0]<<std::endl;//M_ij[0][0] <<std::endl;
  // std::cout << "wat is mit M denn los b: " <<shear_test[1]<< " "<<S_ij[1]<<std::endl;//M_ij[0][1] <<std::endl;
  // std::cout << "wat is mit M denn los c: " <<shear_test[2]<< " "<<S_ij[2]<<std::endl;//M_ij[0][2] <<std::endl;
  // std::cout << "wat is mit M denn los d: " <<shear_test[3]<< " "<<S_ij[3]<<std::endl;//M_ij[1][0] <<std::endl;
  // std::cout << "wat is mit M denn los e: " <<shear_test[4]<< " "<<S_ij[4]<<std::endl;//M_ij[1][1] <<std::endl;
  // std::cout << "wat is mit M denn los f: " <<shear_test[5]<< " "<<S_ij[5]<<std::endl;//M_ij[1][2] <<std::endl;
  // std::cout << "wat is mit M denn los g: " <<shear_test[1]<<st::endl;//M_ij[2][0] <<std::endl;
  // std::cout << "wat is mit M denn los h: " <<shear_test[2]<<st::endl;//M_ij[2][1] <<std::endl;
  // std::cout << "wat is mit M denn los i: " <<shear_test[1]<<st::endl;//M_ij[2][2] <<std::endl;
  T L_ij[3][3];
  L_ij[0][0] = U_ij[0]-(u[0]*u[0]);
  L_ij[0][1] = U_ij[1]-(u[0]*u[1]);
  L_ij[0][2] = U_ij[2]-(u[0]*u[2]);
  L_ij[1][0] = U_ij[1]-(u[0]*u[1]);
  L_ij[1][1] = U_ij[3]-(u[1]*u[1]);
  L_ij[1][2] = U_ij[4]-(u[1]*u[2]);
  L_ij[2][0] = U_ij[2]-(u[0]*u[2]);
  L_ij[2][1] = U_ij[4]-(u[1]*u[2]);
  L_ij[2][2] = U_ij[5]-(u[2]*u[2]);

  T smagoConst = 0;
  T a = 0;
  for (int ix = 0; ix < DESCRIPTOR<T>::d; ++ix) {
    for (int iy = 0; iy < DESCRIPTOR<T>::d; ++iy) {
      a += L_ij[ix][iy]*M_ij[ix][iy];
      // std::cout << "wat is mit L und M: " << L_ij[ix][iy]*M_ij[ix][iy] <<" L: "<< L_ij[ix][iy]<< " M: " << M_ij[ix][iy] <<std::endl;

    }
  }
  T b = 0;
  for (int ix = 0; ix < DESCRIPTOR<T>::d; ++ix) {
    for (int iy = 0; iy < DESCRIPTOR<T>::d; ++iy) {
      b += M_ij[ix][iy]*M_ij[ix][iy];
    }
  }
  //       // computing the dynamic smagorinksy variable
  // make sure, the is no division by zero
  if (b != 0) {
    smagoConst = a / (-2*b);
  } else {
    smagoConst = 0.01;
  }
  // clipping
  if (smagoConst < 0) {
    smagoConst = 0;
  }
  if (smagoConst > 1) {
    smagoConst = 1;
  }

  //   T* smagoIs = DESCRIPTOR<T>::ExternalField::smagoConstIsAt;

  //   *smagoIs = smagoConst;

  // std::cout << "wat is: " << smagoConst <<" a: "<< a<< " b: " << b <<std::endl;

  // bulkDynamics.defineExternalField(cell, 0, 1, smagoIs);//Const);
  std::vector<T> output(1, smagoConst); // first adress, last adress


  return output;
}






template <typename T, template <typename U> class DESCRIPTOR>
BlockLatticeADMFilter<T,DESCRIPTOR>::BlockLatticeADMFilter
(BlockLatticeStructure3D<T,DESCRIPTOR>& blockLattice, const LBconverter<T>& converter,
 Dynamics<T, DESCRIPTOR>& bulkDynamics)//, int _iX, int _iY, int _iZ, Dynamics<T, DESCRIPTOR>& _bulkDynamics, Cell<T, DESCRIPTOR> _cell)
  : BlockLatticePhysF3D<T,DESCRIPTOR>(blockLattice,converter,3), _bulkDynamics(bulkDynamics)//, cell(_cell), iX(_iX), iY(_iY), iZ(_iZ)
{
  this->_name = "blockADMFilter";
}

template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> BlockLatticeADMFilter<T,DESCRIPTOR>::operator() (std::vector<int> input)
{


  // T rho, u[3];
  // this->_blockLattice.get( input[0], input[1], input[2] ).computeRhoU(rho,u);

  // int foundIC = (int)input[0];
  int globX = input[0];
  int globY = input[1];
  int globZ = input[2];

  // u[0]=this->_converter.physVelocity(u[0]);
  // u[1]=this->_converter.physVelocity(u[1]);
  // u[2]=this->_converter.physVelocity(u[2]);



  T u_fil[3], rho_fil;

  std::vector<int> u_000(3,int());
  u_000[0]=globX, u_000[1]=globY, u_000[2]=globZ;

  ///x filter ini
  std::vector<int> u_p00(3,int());
  u_p00[0]=globX+1, u_p00[1]=globY, u_p00[2]=globZ;

  std::vector<int> u_2p00(3,int());
  u_2p00[0]=globX+2, u_2p00[1]=globY, u_2p00[2]=globZ;

  std::vector<int> u_n00(3,int());
  u_n00[0]=globX-1, u_n00[1]=globY, u_n00[2]=globZ;

  std::vector<int> u_2n00(3,int());
  u_2n00[0]=globX-2, u_2n00[1]=globY, u_2n00[2]=globZ;

  ///y filter ini
  std::vector<int> u_0p0(3,int());
  u_0p0[0]=globX, u_0p0[1]=globY+1, u_0p0[2]=globZ;

  std::vector<int> u_02p0(3,int());
  u_02p0[0]=globX, u_02p0[1]=globY+2, u_02p0[2]=globZ;

  std::vector<int> u_0n0(3,int());
  u_0n0[0]=globX, u_0n0[1]=globY-1, u_0n0[2]=globZ;

  std::vector<int> u_02n0(3,int());
  u_02n0[0]=globX, u_02n0[1]=globY-2, u_02n0[2]=globZ;

  ///z filter ini
  std::vector<int> u_00p(3,int());
  u_00p[0]=globX, u_00p[1]=globY, u_00p[2]=globZ+1;

  std::vector<int> u_002p(3,int());
  u_002p[0]=globX, u_002p[1]=globY, u_002p[2]=globZ+2;

  std::vector<int> u_00n(3,int());
  u_00n[0]=globX, u_00n[1]=globY, u_00n[2]=globZ-1;

  std::vector<int> u_002n(3,int());
  u_002n[0]=globX, u_002n[1]=globY, u_002n[2]=globZ-2;

  BlockLatticeDensity3D<T, DESCRIPTOR> density(this->_blockLattice);
  BlockLatticePhysVelocity3D<T, DESCRIPTOR> velocity(this->_blockLattice, this->_converter);

  T sigma = 0.005;

  T d_0 = 6. / 16.;
  T d_1 = -4. / 16.;
  T d_2 = 1. / 16.;



  rho_fil = density(u_000)[0]- sigma*(d_2*(density(u_2n00)[0]+density(u_02n0)[0]+density(u_002n)[0]) +
                                      d_1*(density(u_n00)[0]+density(u_0n0)[0]+density(u_00n)[0])+
                                      d_0*(density(u_000)[0]+density(u_000)[0]+density(u_000)[0])+
                                      d_1*(density(u_p00)[0]+density(u_0p0)[0]+density(u_00p)[0])+
                                      d_2*(density(u_2p00)[0]+density(u_02p0)[0]+density(u_002p)[0]) );



  for (int i = 0; i < 3; ++i ) {
    u_fil[i] = velocity(u_000)[i] - sigma*(d_2*(density(u_2n00)[0]*velocity(u_2n00)[i]+density(u_02n0)[0]*velocity(u_02n0)[i]+density(u_002n)[0]*velocity(u_002n)[i]) +
                                           d_1*(density(u_n00)[0]*velocity(u_n00)[i]+density(u_0n0)[0]*velocity(u_0n0)[i]+density(u_00n)[0]*velocity(u_00n)[i])+
                                           d_0*(density(u_000)[0]*velocity(u_000)[i]+density(u_000)[0]*velocity(u_000)[i]+density(u_000)[0]*velocity(u_000)[i])+
                                           d_1*(density(u_p00)[0]*velocity(u_p00)[i]+density(u_0p0)[0]*velocity(u_0p0)[i]+density(u_00p)[0]*velocity(u_00p)[i])+
                                           d_2*(density(u_2p00)[0]*velocity(u_2p00)[i]+density(u_02p0)[0]*velocity(u_02p0)[i]+density(u_002p)[0]*velocity(u_002p)[i]) );
  }
  OstreamManager clout(std::cout,"ADM in functor:");
  // clout <<"rhois stencil center: " <<density(u_000)[0]<<" rhois stencil center after filter: " <<rho_fil<< " velx stencil center: " << velocity(u_000)[0]<< " velx stencil center after filter: " << u_fil[0]<<std::endl;// " vely: " << u_fil[1]<< " velz: " << u_fil[2] << std::endl;

  // this->_blockLattice.defineExternalField(globX, globX, globY , globY, globZ, globZ, DESCRIPTOR<T>::ExternalField::filRhoIsAt,
  //                        1,
  //                        *rho_fil);

  // this->_blockLattice.defineExternalField(globX, globX, globY , globY, globZ, globZ, DESCRIPTOR<T>::ExternalField::localFilVelBeginsAt,
  //                         DESCRIPTOR<T>::ExternalField::sizeOfFilVel,
  //                         *u_fil[3]);
  std::vector<T> result(4,T());
  // std::vector<T> result(1,0.1);
  result[0] = rho_fil, result[1] = u_fil[0], result[2] = u_fil[1], result[3] = u_fil[2];
  return result;
}



template <typename T, template <typename U> class DESCRIPTOR>
SuperLatticeShearSmagoAv<T,DESCRIPTOR>::SuperLatticeShearSmagoAv
(SuperLattice3D<T,DESCRIPTOR>& sLattice, LBconverter<T>& converter, int iT)
  : SuperLatticePhysF3D<T,DESCRIPTOR>(sLattice,converter,1), _iT(iT)
{
  this->_name = "ShearSmagoAv";
}



template <typename T, template <typename U> class DESCRIPTOR>
std::vector<T> SuperLatticeShearSmagoAv<T,DESCRIPTOR>::operator() (std::vector<int> input)
{

  if ( this->_sLattice.getLoadBalancer().rank(input[0]) == singleton::mpi().getRank() ) {
    _iT++;
    SuperLatticeStress3D<T, DESCRIPTOR> stress(this->_sLattice);
    T PiNeqNormSqr = stress(input)[0]*stress(input)[0] + 2.*stress(input)[1]*stress(input)[1] + stress(input)[2]*stress(input)[2]
                     + stress(input)[2]*stress(input)[2] + stress(input)[3]*stress(input)[3]
                     + 2.*stress(input)[4]*stress(input)[4] + stress(input)[5]*stress(input)[5];
    T PiNeqNorm = sqrt(PiNeqNormSqr);

    int overlap = this->_sLattice.getOverlap();
    int avShearIsAt = DESCRIPTOR<T>::ExternalField::avShearIsAt;
    T* avShear =  this->_sLattice.getExtendedBlockLattice(this->_sLattice.getLoadBalancer().loc(input[0])).get(input[1]+overlap,input[2]+overlap,input[3]+overlap ).getExternal(avShearIsAt);
    *avShear = (*avShear*(_iT-1)+PiNeqNorm) / _iT;
    _iT--;
    return std::vector<T>(1,*avShear);
  } else {
    return std::vector<T>(); // empty vector
  }
}





} // end namespace olb
#endif


