/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Jonas Fietz, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef GRAPH_LOAD_BALANCER_H
#define GRAPH_LOAD_BALANCER_H

#include "complexGrids/mpiManager/mpiManager.h"
#include "complexGrids/cuboidStructure/cuboidGeometry3D.h"
#include "complexGrids/cuboidStructure/cuboidNeighbourhood3D.h"
#include "core/loadBalancer.h"
#include "io/ostreamManager.h"

namespace olb {

struct edge {
  int source;
  int target;
  int weight;
};

template<typename T>
class GraphLoadBalancer : public LoadBalancer<T> {
private:
  // Handles the MPI communication
#ifdef PARALLEL_MODE_MPI
  singleton::MpiNonBlockingHelper _mpiNbHelper;
#endif
  CuboidGeometry3D<T> _cGeometry;
  BlockGeometry3D<T> * const _blockGeometry;
  int _overlap;

public:
  GraphLoadBalancer(const CuboidGeometry3D<T> &cGeometry, BlockGeometry3D<T> * const blockGeometry = NULL, const double ratioFullEmpty=3.7, const int overlap=1);
};
}  // namespace olb

#endif
