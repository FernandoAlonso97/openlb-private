/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Jonas Fietz, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef GRAPH_LOAD_BALANCER_HH
#define GRAPH_LOAD_BALANCER_HH

#include <algorithm>
#include "core/cell.h"
#include "core/util.h"
#include <vector>
#include <map>
#include <math.h>
#include "external/boost/boost/shared_array.hpp"
#include "contrib/kaffpa/kaffpa_interface.h"
#include "graphLoadBalancer.h"
#include "complexGrids/mpiManager/mpiManager.h"

namespace olb {

template<typename T> GraphLoadBalancer<T>::GraphLoadBalancer(const CuboidGeometry3D<T> &cGeometry, BlockGeometry3D<T>  * const blockGeometry, const double ratioFullEmpty, const int overlap) : _cGeometry(cGeometry), _blockGeometry(blockGeometry), _overlap(overlap)
{
  int rank = 0;
  int size = 1;
  int nC = _cGeometry.getNc();
#ifdef PARALLEL_MODE_MPI
  rank = singleton::mpi().getRank();
  size = singleton::mpi().getSize();
#endif
  std::vector<Cell3D<T> > inCells;
  int xN, yN, zN;
  T globX, globY, globZ, delta;
  Cell3D<T> found;

  boost::shared_array<int> tempInCN(new int[nC]);

  std::vector<int> cuboidToThread(nC);
  // structures for CSR format for kaffpa
  std::vector<int> vwgt(nC+1); // node weights
  std::vector<int> xadj(nC+1);
  std::vector<int> adjcwgt;
  std::vector<int> adjncy;
  int partitionResult[nC];

  if (size == 1) {
    for (int i = 0; i < nC; ++i) {
      this->_glob.push_back(i);
      this->_loc[i] = i;
      this->_rank[i] = 0;
    };
    this->_size = nC;
    std::cout << "Cut size: 0" << std::endl;
    return;
  }
  if (rank == 0) {
    int edgeCount = 0;
    std::map<int, std::map<int, int> > edgeWeights;
    for ( int iC = 0; iC < nC; iC++) { // assemble neighbourhood information
      xN  = _cGeometry.get(iC).getNx();
      yN  = _cGeometry.get(iC).getNy();
      zN  = _cGeometry.get(iC).getNz();
      globX = _cGeometry.get(iC).get_globPosX();
      globY = _cGeometry.get(iC).get_globPosY();
      globZ = _cGeometry.get(iC).get_globPosZ();
      delta = _cGeometry.get(iC).getDeltaR();

      if (blockGeometry) {
        int fullCells = 0;
        for (int iX=globX; iX<globX + xN; iX++) {
          for (int iY=globY; iY<globY + yN; iY++) {
            for (int iZ=globZ; iZ<globZ + zN; iZ++) {
              if (_blockGeometry->getMaterial(iX, iY, iZ) != 0) {
                fullCells++;
              }
            }
          }
        }
        vwgt[iC] = (xN*yN*zN - fullCells) + (ratioFullEmpty * fullCells);
      } else {
        vwgt[iC] = xN*yN*zN;
      }

      for (int o = 1; o <= _overlap; o++) {
        for (int iX=0-o; iX<xN+o; iX++) {
          for (int iY=0-o; iY<yN+o; iY++) {
            for (int iZ=0-o; iZ<zN+o; iZ++) {
              if (iX == 0 - o || iX == xN + o -1 ||
                  iY == 0 - o || iY == yN + o -1 ||
                  iZ == 0 - o || iZ == zN + o -1 ) {

                T nextX = globX + iX*delta;
                T nextY = globY + iY*delta;
                T nextZ = globZ + iZ*delta;
                int _iC = _cGeometry.get_iC(nextX, nextY, nextZ);
                if (_iC != nC && _iC != iC) {
                  found.physR[0] = nextX;
                  found.physR[1] = nextY;
                  found.physR[2] = nextZ;
                  found.latticeR[0] = _iC;
                  inCells.push_back(found);
                }
              }
            }
          }
        }
      }
      for (int i = 0; i<nC; i++) {
        tempInCN[i]=0;
      }
      for (unsigned i = 0; i<inCells.size(); i++) {
        tempInCN[inCells[i].latticeR[0]]++;
      }

      std::map<int,int> localWeights;
      for (int i = 0; i < nC; i++) {

        if (tempInCN[i]) {
          localWeights[i] = tempInCN[i];
        } else {
          localWeights[i] = 0;
        }
      }
      inCells.clear();

      edgeWeights[iC] =  localWeights;
    }


    for (int i = 0; i < nC; i++) {
      for (int j = i+1; j < nC; j++) {
        edgeWeights[i][j] = edgeWeights[i][j] + edgeWeights[j][i];
        edgeWeights[j][i] = edgeWeights[i][j];
      }
    }

    for (int i = 0; i < nC; i++) {
      xadj[i] = edgeCount;
      for (int j = 0; j < nC; j++) {
        if (edgeWeights[i][j] != 0) {
          adjncy.push_back(j);
          adjcwgt.push_back(edgeWeights[i][j]);
          edgeCount++;
        }
      }
    }

    xadj[nC] = edgeCount; // End of the array in CSR

    double inbalance = 1.03;
    int edgecut;
#if 0
    std::cout << "vwgt" << std::endl;
    for (int i = 0; i < nC; i++)  {
      std::cout << vwgt[i] << std::endl;
    }
    std::cout << "xadj" << std::endl;
    for (int i = 0; i < nC+1; i++)  {
      std::cout << xadj[i] << std::endl;
    }
    std::cout << "adjncy" << std::endl;
    for (int i = 0; i <adjncy.size(); i++)  {
      std::cout << adjncy[i] << std::endl;
    }
    std::cout << "adjcwgt" << std::endl;
    for (int i = 0; i < adjcwgt.size(); i++)  {
      std::cout << adjcwgt[i] << std::endl;
    }

    std::cout << "nC" << nC << " size " << size << " inbalance " <<
              inbalance << std::endl;

    std::cout << "graph file " << std::endl;
    int j = 0;
    if (rank ==0) {
      std::cout << nC << " " << edgeCount << "11" << std::endl;
      for (int i =0; i < nC; i++) {
        std::cout << vwgt[i] << " ";
        for (; j<xadj[i+1]; j++) {
          std::cout << adjncy[j] << " " << adjcwgt[j] << " ";
        }
        std::cout << std::endl;
      }
    }

#endif
    kaffpa_strong(&nC, &vwgt.front(), &xadj.front(), &adjcwgt.front(),
                  &adjncy.front(), &size, &inbalance, false, &edgecut,
                  partitionResult);

    std::cout << "Cut size: " << edgecut << std::endl;

    int count = 0;
    for (int i = 0; i < nC; ++i) {
      if (partitionResult[i] == 0) {
        this->_glob.push_back(i);
        this->_loc[i] = count;
        count++;
      };
      this->_rank[i] = partitionResult[i];
      cuboidToThread[i] = partitionResult[i];
    }
    this->_size = count;
  }
  // Send all threads their number of cuboids

#ifdef PARALLEL_MODE_MPI
  if (rank == 0) {
    // Send all threads their respective cuboids
    _mpiNbHelper.free();
    _mpiNbHelper.allocate(size-1);
    for (int i = 1; i < size; i++) {
      singleton::mpi().iSend(&cuboidToThread.front(),
                             nC, i, &_mpiNbHelper.get_mpiRequest()[i-1], 0);
    }
    singleton::mpi().waitAll(_mpiNbHelper);
  } else {
    int *tmpCuboids = new int[nC];
    singleton::mpi().receive(tmpCuboids, nC, 0, 0);
    int count = 0;
    for (int i = 0; i < nC; ++i) {
      if (tmpCuboids[i] == rank) {
        this->_glob.push_back(i);
        this->_loc[i] = count;
        count++;
      };
      this->_rank[i] = tmpCuboids[i];
    }
    delete[] tmpCuboids;
    this->_size = count;
  }
#endif
#ifdef OLB_DEBUG
  this->print();
#endif
}
}  // namespace olb
#endif
