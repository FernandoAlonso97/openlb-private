/*
 * interpolation.h
 *
 *  Created on: Feb 4, 2022
 *      Author: bhorvat
 */

#ifndef SRC_CONTRIB_INTERPOLATION_INTERPOLATION_H_
#define SRC_CONTRIB_INTERPOLATION_INTERPOLATION_H_

#include"../MemorySpace/memory_spaces.h"

#include<cstddef>

#ifdef ENABLE_CUDA

#include <thrust/for_each.h>
#include <thrust/device_vector.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/tuple.h>

#endif

namespace olb {

namespace interpolate {

#ifdef ENABLE_CUDA
#define tupleGet thrust::get
#else
#define tupleGet std::get
#endif

#ifdef ENABLE_CUDA
template<class Functor, typename ...Args>
__global__ void applyKernel(Functor func, size_t size, Args...args) {

	int blockIdx = getBlockIndex();
	int threadIndex = getThreadIndex( blockIdx );
	if(threadIndex > static_cast<int>(size))
		return;

	func(threadIndex,args...);
}
#endif

struct BilinearKernel {

#ifdef ENABLE_CUDA
	using DataType = thrust::tuple<unsigned int, unsigned int, unsigned int, float>;
#else
	using DataType = std::tuple<unsigned int, unsigned int, unsigned int, float>;
#endif

#ifdef ENABLE_CUDA
	__host__ __device__
#endif
	void operator()(float indexX, float indexY, float indexZ,
			DataType* data) {
		float coeffs[4];
		float iX[4] = {std::floor(indexX),std::ceil(indexX),std::floor(indexX),std::ceil(indexX)};
		float iY[4] = {std::floor(indexY),std::floor(indexY),std::ceil(indexY),std::ceil(indexY)};

		float cx1 = std::ceil(indexX) - indexX;
		float cx2 = 1 - cx1;
		float cy1 = std::ceil(indexY) - indexY;
		float cy2 = 1 - cy1;

		coeffs[0] = cx1*cy1;
		coeffs[1] = cx2*cy1;
		coeffs[2] = cx1*cy2;
		coeffs[3] = cx2*cy2;

		for(unsigned int ii = 0; ii < 4; ++ii) {
			data[ii] = {iX[ii],iY[ii],indexZ,coeffs[ii]};
		}
	}
	static constexpr unsigned int size = 4;
};

struct GenerateCartCoords {

	GenerateCartCoords(float const scaling, float const offSetX,
			float const offSetY, float const offSetZ):
			scaling_(scaling),
			offSetX_(offSetX),
			offSetY_(offSetY),
			offSetZ_(offSetZ) {};
	template<typename Cont1, typename Cont2>
#ifdef ENABLE_CUDA
	__host__ __device__
#endif
	void operator()(Cont1& xInt, Cont1& yInt, Cont1& zInt, const Cont2& rInt, const Cont2& psiInt) {
		xInt = (-rInt * std::cos(psiInt))*scaling_+offSetX_;
		yInt = ( rInt * std::sin(psiInt))*scaling_+offSetY_;
		zInt = offSetZ_;
	}

	template<typename Cont1, typename Cont2>
#ifdef ENABLE_CUDA
	__host__ __device__
#endif
	void operator()(size_t const index, Cont1& xInt, Cont1& yInt, Cont1& zInt,
			const Cont2& rInt, const Cont2& psiInt) const {
		xInt = (-rInt[index] * cos(psiInt[index]))*scaling_+offSetX_;
		yInt = ( rInt[index] * sin(psiInt[index]))*scaling_+offSetY_;
		zInt = offSetZ_;

//		printf("%f,%f,%f,%f,%f,%f,%f\n",scaling_,offSetX_,offSetY_,offSetZ_,xInt,yInt,zInt);
	}

	float scaling_;
	float offSetX_;
	float offSetY_;
	float offSetZ_;
};

template<class InterpFunc>
struct Interpolation {

	using InterpType = InterpFunc;

	template<class FromIndexFunc, class Vector1, class Field>
#ifdef ENABLE_CUDA
	__host__ __device__
#endif
	void operator()(size_t const iRow, Vector1 & to,
			Field const & from, Vector1 const & xTo, Vector1 const & yTo,
			Vector1 const & zTo, FromIndexFunc const & indexFunc) const {
			typename InterpFunc::DataType data[InterpFunc::size];
			InterpFunc func;
			func(xTo[iRow],yTo[iRow],zTo[iRow],data);

			to[iRow] = 0;

			for(auto it = data; it<data+InterpFunc::size; ++it) {
				size_t const iCol = indexFunc(tupleGet<0>(*it),tupleGet<1>(*it),tupleGet<2>(*it));
				to[iRow] += tupleGet<3>(*it)*from[iCol];
			}
	}

	template<class FromIndexFunc, class Vector1, class Field, class Vector2>
#ifdef ENABLE_CUDA
	__host__ __device__
#endif
	void operator()(size_t const iRow, Vector1 & to,
			Field const & from, Vector2 const & xTo, Vector2 const & yTo,
			Vector2 const & zTo, FromIndexFunc const & indexFunc) const {
			typename InterpFunc::DataType data[InterpFunc::size];
			InterpFunc func;
			func(xTo,yTo,zTo,data);

			to[iRow] = 0;

			for(auto it = data; it<data+InterpFunc::size; ++it) {
				size_t const iCol = indexFunc(tupleGet<0>(*it),tupleGet<1>(*it),tupleGet<2>(*it));
				auto tmp = to[iRow];
				to[iRow] += tupleGet<3>(*it)*from[iCol];
//				printf("%lu,%lu,%lf,%f,%f,%f\n",iRow,iCol,from[iCol],tmp,to[iRow],tupleGet<3>(*it));
			}
	}

};

template<class InterpFunc, class CoordTransform>
struct InterpolationToNonCartesian {

	using InterpType = typename Interpolation<InterpFunc>::InterpType;

	template<class FromIndexFunc, class Vector, class Field, typename ...Args>
#ifdef ENABLE_CUDA
	__host__ __device__
#endif
	void operator()(size_t const iRow, CoordTransform const & transform, FromIndexFunc const & indexFunc,
			Vector & to, Field const & from, Args &&...coords) {
		float x,y,z;
		transform(iRow,x,y,z,coords...);
//		printf("%f,%f,%f\n",x,y,z);
		Interpolation<InterpFunc> func;
		func.template operator ()<FromIndexFunc,Vector,Field>(iRow,to,from,x,y,z,indexFunc);
	}


};

template<class InterpFunc, class IndexFunc, class Vector, class Field>
void interpolate(Vector & to, Field const & from, size_t nrows, Vector const & xTo, Vector const & yTo,
		Vector const & zTo, IndexFunc const & indexFunc) {
	Interpolation<InterpFunc> func;
#ifdef ENABLE_CUDA
	applyKernel<<<nrows/256+1,256>>>(func,nrows,to,from,xTo,yTo,zTo,indexFunc);
#else
	for(uint16_t iRow = 0; iRow < nrows; ++iRow)
		func.template operator ()<IndexFunc,Vector,Field>(iRow,to,from,xTo,yTo,zTo,indexFunc);

#endif
}

template<class MemSpace,class InterpFunc, class CoordTransform, class IndexFunc, class Vector,
class Field, typename ...Args>
void interpolate(CoordTransform const & transform, Vector & to,	Field const & from, size_t nrows,
		IndexFunc const & indexFunc, Args &&...coords) {
	InterpolationToNonCartesian<InterpFunc,CoordTransform> func;
	if(std::is_same<MemSpace,memory_space::CudaDeviceSpaceTag>::value) {
#ifdef ENABLE_CUDA
	applyKernel<<<nrows/256+1,256>>>(func,nrows,transform, indexFunc,
			to, from, coords...);
#endif
	}
	else {
	for(uint16_t iRow = 0; iRow < nrows; ++iRow)
		func.template operator ()<IndexFunc,Vector,Field>(iRow, transform, indexFunc,
				to, from, coords...);
	}

}

} // namespace interpolate
} // namespace olb



#endif /* SRC_CONTRIB_INTERPOLATION_INTERPOLATION_H_ */
