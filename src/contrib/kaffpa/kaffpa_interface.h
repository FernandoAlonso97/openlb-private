//
// Author: Christian Schulz <christian.schulz@kit.edu>
//

#ifndef KAFFPA_INTERFACE_RYEEZ6WJ
#define KAFFPA_INTERFACE_RYEEZ6WJ


//#include "metis.h"
typedef int idxtype;

#ifdef __cplusplus
extern "C"
{
#endif
void kaffpa_strong(int *n, idxtype* vwgt, idxtype *xadj, idxtype* adjcwgt, idxtype *adjncy, int *nparts, double *inbalance,  bool suppress_output, int *edgecut, idxtype *part);

void kaffpa_eco(int *n, idxtype* vwgt, idxtype *xadj, idxtype* adjcwgt, idxtype *adjncy,  int *nparts, double *inbalance,  bool suppress_output, int *edgecut, idxtype *part);

void kaffpa_fast(int *n, idxtype* vwgt, idxtype *xadj, idxtype* adjcwgt, idxtype *adjncy, int *nparts, double *inbalance,  bool suppress_output, int *edgecut, idxtype *part);
#ifdef __cplusplus
}
#endif

#endif /* end of include guard: KAFFPA_INTERFACE_RYEEZ6WJ */
