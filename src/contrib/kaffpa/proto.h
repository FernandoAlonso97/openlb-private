/*
 * Copyright 1997, Regents of the University of Minnesota
 *
 * proto.h
 *
 * This file contains header files
 *
 * Started 10/19/95
 * George
 *
 * $Id: proto.h,v 1.1 1998/11/27 17:59:28 karypis Exp $
 *
 */

/* balance.c */
void Balance2Way(CtrlType *, MetisGraphType *, int *, float);
void Bnd2WayBalance(CtrlType *, MetisGraphType *, int *);
void General2WayBalance(CtrlType *, MetisGraphType *, int *);

/* bucketsort.c */
void BucketSortKeysInc(int, int, idxtype *, idxtype *, idxtype *);

/* ccgraph.c */
void CreateCoarseGraph(CtrlType *, MetisGraphType *, int, idxtype *, idxtype *);
void CreateCoarseGraphNoMask(CtrlType *, MetisGraphType *, int, idxtype *, idxtype *);
void CreateCoarseGraph_NVW(CtrlType *, MetisGraphType *, int, idxtype *, idxtype *);
MetisGraphType *SetUpCoarseGraph(MetisGraphType *, int, int);
void ReAdjustMemory(MetisGraphType *, MetisGraphType *, int);

/* coarsen.c */
MetisGraphType *Coarsen2Way(CtrlType *, MetisGraphType *);

/* compress.c */
void CompressGraph(CtrlType *, MetisGraphType *, int, idxtype *, idxtype *, idxtype *, idxtype *);
void PruneGraph(CtrlType *, MetisGraphType *, int, idxtype *, idxtype *, idxtype *, float);

/* debug.c */
int ComputeCut(MetisGraphType *, idxtype *);
int CheckBnd(MetisGraphType *);
int CheckBnd2(MetisGraphType *);
int CheckNodeBnd(MetisGraphType *, int);
int CheckRInfo(RInfoType *);
int CheckNodePartitionParams(MetisGraphType *);
int IsSeparable(MetisGraphType *);

/* estmem.c */
void METIS_EstimateMemory(int *, idxtype *, idxtype *, int *, int *, int *);
void EstimateCFraction(int, idxtype *, idxtype *, float *, float *);
int ComputeCoarseGraphSize(int, idxtype *, idxtype *, int, idxtype *, idxtype *, idxtype *);

/* fm.c */
void FM_2WayEdgeRefine(CtrlType *, MetisGraphType *, int *, int);

/* fortran.c */
void Change2CNumbering(int, idxtype *, idxtype *);
void Change2FNumbering(int, idxtype *, idxtype *, idxtype *);
void Change2FNumbering2(int, idxtype *, idxtype *);
void Change2FNumberingOrder(int, idxtype *, idxtype *, idxtype *, idxtype *);
void ChangeMesh2CNumbering(int, idxtype *);
void ChangeMesh2FNumbering(int, idxtype *, int, idxtype *, idxtype *);
void ChangeMesh2FNumbering2(int, idxtype *, int, int, idxtype *, idxtype *);

/* frename.c */
void METIS_PARTGRAPHRECURSIVE(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphrecursive(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphrecursive_(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphrecursive__(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPARTGRAPHRECURSIVE(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphrecursive(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphrecursive_(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphrecursive__(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void METIS_PARTGRAPHKWAY(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphkway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphkway_(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphkway__(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPARTGRAPHKWAY(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphkway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphkway_(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphkway__(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void METIS_EDGEND(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_edgend(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_edgend_(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_edgend__(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_NODEND(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_nodend(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_nodend_(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_nodend__(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_NODEWND(int *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_nodewnd(int *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_nodewnd_(int *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_nodewnd__(int *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_PARTMESHNODAL(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void metis_partmeshnodal(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void metis_partmeshnodal_(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void metis_partmeshnodal__(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void METIS_PARTMESHDUAL(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void metis_partmeshdual(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void metis_partmeshdual_(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void metis_partmeshdual__(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void METIS_MESHTONODAL(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_meshtonodal(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_meshtonodal_(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_meshtonodal__(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_MESHTODUAL(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_meshtodual(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_meshtodual_(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void metis_meshtodual__(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_ESTIMATEMEMORY(int *, idxtype *, idxtype *, int *, int *, int *);
void metis_estimatememory(int *, idxtype *, idxtype *, int *, int *, int *);
void metis_estimatememory_(int *, idxtype *, idxtype *, int *, int *, int *);
void metis_estimatememory__(int *, idxtype *, idxtype *, int *, int *, int *);
void METIS_MCPARTGRAPHRECURSIVE(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_mcpartgraphrecursive(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_mcpartgraphrecursive_(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_mcpartgraphrecursive__(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_MCPARTGRAPHKWAY(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_mcpartgraphkway(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_mcpartgraphkway_(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_mcpartgraphkway__(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void METIS_PARTGRAPHVKWAY(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphvkway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphvkway_(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void metis_partgraphvkway__(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPARTGRAPHVKWAY(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphvkway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphvkway_(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void metis_wpartgraphvkway__(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);

/* graph.c */
void SetUpGraph(MetisGraphType *, int, int, int, idxtype *, idxtype *, idxtype *, idxtype *, int);
void SetUpGraphKway(MetisGraphType *, int, idxtype *, idxtype *);
void SetUpGraph2(MetisGraphType *, int, int, idxtype *, idxtype *, float *, idxtype *);
void VolSetUpGraph(MetisGraphType *, int, int, int, idxtype *, idxtype *, idxtype *, idxtype *, int);
void RandomizeGraph(MetisGraphType *);
int IsConnectedSubdomain(CtrlType *, MetisGraphType *, int, int);
int IsConnected(CtrlType *, MetisGraphType *, int);
int IsConnected2(MetisGraphType *, int);
int FindComponents(CtrlType *, MetisGraphType *, idxtype *, idxtype *);

/* initpart.c */
void Init2WayPartition(CtrlType *, MetisGraphType *, int *, float);
void InitSeparator(CtrlType *, MetisGraphType *, float);
void GrowBisection(CtrlType *, MetisGraphType *, int *, float);
void GrowBisectionNode(CtrlType *, MetisGraphType *, float);
void RandomBisection(CtrlType *, MetisGraphType *, int *, float);

/* kmetis.c */
void METIS_PartGraphKway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPartGraphKway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
int MlevelKWayPartitioning(CtrlType *, MetisGraphType *, int, idxtype *, float *, float);

/* kvmetis.c */
void METIS_PartGraphVKway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPartGraphVKway(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
int MlevelVolKWayPartitioning(CtrlType *, MetisGraphType *, int, idxtype *, float *, float);

/* kwayfm.c */
void Random_KWayEdgeRefine(CtrlType *, MetisGraphType *, int, float *, float, int, int);
void Greedy_KWayEdgeRefine(CtrlType *, MetisGraphType *, int, float *, float, int);
void Greedy_KWayEdgeBalance(CtrlType *, MetisGraphType *, int, float *, float, int);

/* kwayrefine.c */
void RefineKWay(CtrlType *, MetisGraphType *, MetisGraphType *, int, float *, float);
void AllocateKWayPartitionMemory(CtrlType *, MetisGraphType *, int);
void ComputeKWayPartitionParams(CtrlType *, MetisGraphType *, int);
void ProjectKWayPartition(CtrlType *, MetisGraphType *, int);
int IsBalanced(idxtype *, int, float *, float);
void ComputeKWayBoundary(CtrlType *, MetisGraphType *, int);
void ComputeKWayBalanceBoundary(CtrlType *, MetisGraphType *, int);

/* kwayvolfm.c */
void Random_KWayVolRefine(CtrlType *, MetisGraphType *, int, float *, float, int, int);
void Random_KWayVolRefineMConn(CtrlType *, MetisGraphType *, int, float *, float, int, int);
void Greedy_KWayVolBalance(CtrlType *, MetisGraphType *, int, float *, float, int);
void Greedy_KWayVolBalanceMConn(CtrlType *, MetisGraphType *, int, float *, float, int);
void KWayVolUpdate(CtrlType *, MetisGraphType *, int, int, int, idxtype *, idxtype *, idxtype *);
void ComputeKWayVolume(MetisGraphType *, int, idxtype *, idxtype *, idxtype *);
int ComputeVolume(MetisGraphType *, idxtype *);
void CheckVolKWayPartitionParams(CtrlType *, MetisGraphType *, int);
void ComputeVolSubDomainGraph(MetisGraphType *, int, idxtype *, idxtype *);
void EliminateVolSubDomainEdges(CtrlType *, MetisGraphType *, int, float *);
void EliminateVolComponents(CtrlType *, MetisGraphType *, int, float *, float);

/* kwayvolrefine.c */
void RefineVolKWay(CtrlType *, MetisGraphType *, MetisGraphType *, int, float *, float);
void AllocateVolKWayPartitionMemory(CtrlType *, MetisGraphType *, int);
void ComputeVolKWayPartitionParams(CtrlType *, MetisGraphType *, int);
void ComputeKWayVolGains(CtrlType *, MetisGraphType *, int);
void ProjectVolKWayPartition(CtrlType *, MetisGraphType *, int);
void ComputeVolKWayBoundary(CtrlType *, MetisGraphType *, int);
void ComputeVolKWayBalanceBoundary(CtrlType *, MetisGraphType *, int);

/* match.c */
void Match_RM(CtrlType *, MetisGraphType *);
void Match_RM_NVW(CtrlType *, MetisGraphType *);
void Match_HEM(CtrlType *, MetisGraphType *);
void Match_SHEM(CtrlType *, MetisGraphType *);

/* mbalance.c */
void MocBalance2Way(CtrlType *, MetisGraphType *, float *, float);
void MocGeneral2WayBalance(CtrlType *, MetisGraphType *, float *, float);

/* mbalance2.c */
void MocBalance2Way2(CtrlType *, MetisGraphType *, float *, float *);
void MocGeneral2WayBalance2(CtrlType *, MetisGraphType *, float *, float *);
void SelectQueue3(int, float *, float *, int *, int *, PQueueType [MAXNCON][2], float *);

/* mcoarsen.c */
MetisGraphType *MCCoarsen2Way(CtrlType *, MetisGraphType *);

/* memory.c */
void AllocateWorkSpace(CtrlType *, MetisGraphType *, int);
void FreeWorkSpace(CtrlType *, MetisGraphType *);
int WspaceAvail(CtrlType *);
idxtype *idxwspacemalloc(CtrlType *, int);
void idxwspacefree(CtrlType *, int);
float *fwspacemalloc(CtrlType *, int);
void fwspacefree(CtrlType *, int);
MetisGraphType *CreateGraph(void);
void InitGraph(MetisGraphType *);
void FreeGraph(MetisGraphType *);

/* mesh.c */
void METIS_MeshToDual(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_MeshToNodal(int *, int *, idxtype *, int *, int *, idxtype *, idxtype *);
void GENDUALMETIS(int, int, int, idxtype *, idxtype *, idxtype *adjncy);
void TRINODALMETIS(int, int, idxtype *, idxtype *, idxtype *adjncy);
void TETNODALMETIS(int, int, idxtype *, idxtype *, idxtype *adjncy);
void HEXNODALMETIS(int, int, idxtype *, idxtype *, idxtype *adjncy);
void QUADNODALMETIS(int, int, idxtype *, idxtype *, idxtype *adjncy);

/* meshpart.c */
void METIS_PartMeshNodal(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);
void METIS_PartMeshDual(int *, int *, idxtype *, int *, int *, int *, int *, idxtype *, idxtype *);

/* mfm.c */
void MocFM_2WayEdgeRefine(CtrlType *, MetisGraphType *, float *, int);
void SelectQueue(int, float *, float *, int *, int *, PQueueType [MAXNCON][2]);
int BetterBalance(int, float *, float *, float *);
float Compute2WayHLoadImbalance(int, float *, float *);
void Compute2WayHLoadImbalanceVec(int, float *, float *, float *);

/* mfm2.c */
void MocFM_2WayEdgeRefine2(CtrlType *, MetisGraphType *, float *, float *, int);
void SelectQueue2(int, float *, float *, int *, int *, PQueueType [MAXNCON][2], float *);
int IsBetter2wayBalance(int, float *, float *, float *);

/* mincover.o */
void MinCover(idxtype *, idxtype *, int, int, idxtype *, int *);
int MinCover_Augment(idxtype *, idxtype *, int, idxtype *, idxtype *, idxtype *, int);
void MinCover_Decompose(idxtype *, idxtype *, int, int, idxtype *, idxtype *, int *);
void MinCover_ColDFS(idxtype *, idxtype *, int, idxtype *, idxtype *, int);
void MinCover_RowDFS(idxtype *, idxtype *, int, idxtype *, idxtype *, int);

/* minitpart.c */
void MocInit2WayPartition(CtrlType *, MetisGraphType *, float *, float);
void MocGrowBisection(CtrlType *, MetisGraphType *, float *, float);
void MocRandomBisection(CtrlType *, MetisGraphType *, float *, float);
void MocInit2WayBalance(CtrlType *, MetisGraphType *, float *);
int SelectQueueoneWay(int, float *, float *, int, PQueueType [MAXNCON][2]);

/* minitpart2.c */
void MocInit2WayPartition2(CtrlType *, MetisGraphType *, float *, float *);
void MocGrowBisection2(CtrlType *, MetisGraphType *, float *, float *);
void MocGrowBisectionNew2(CtrlType *, MetisGraphType *, float *, float *);
void MocInit2WayBalance2(CtrlType *, MetisGraphType *, float *, float *);
int SelectQueueOneWay2(int, float *, PQueueType [MAXNCON][2], float *);

/* mkmetis.c */
void METIS_mCPartGraphKway(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
int MCMlevelKWayPartitioning(CtrlType *, MetisGraphType *, int, idxtype *, float *);

/* mkwayfmh.c */
void MCRandom_KWayEdgeRefineHorizontal(CtrlType *, MetisGraphType *, int, float *, int);
void MCGreedy_KWayEdgeBalanceHorizontal(CtrlType *, MetisGraphType *, int, float *, int);
int AreAllHVwgtsBelow(int, float, float *, float, float *, float *);
int AreAllHVwgtsAbove(int, float, float *, float, float *, float *);
void ComputeHKWayLoadImbalance(int, int, float *, float *);
int MocIsHBalanced(int, int, float *, float *);
int IsHBalanceBetterFT(int, int, float *, float *, float *, float *);
int IsHBalanceBetterTT(int, int, float *, float *, float *, float *);

/* mkwayrefine.c */
void MocRefineKWayHorizontal(CtrlType *, MetisGraphType *, MetisGraphType *, int, float *);
void MocAllocateKWayPartitionMemory(CtrlType *, MetisGraphType *, int);
void MocComputeKWayPartitionParams(CtrlType *, MetisGraphType *, int);
void MocProjectKWayPartition(CtrlType *, MetisGraphType *, int);
void MocComputeKWayBalanceBoundary(CtrlType *, MetisGraphType *, int);

/* mmatch.c */
void MCMatch_RM(CtrlType *, MetisGraphType *);
void MCMatch_HEM(CtrlType *, MetisGraphType *);
void MCMatch_SHEM(CtrlType *, MetisGraphType *);
void MCMatch_SHEBM(CtrlType *, MetisGraphType *, int);
void MCMatch_SBHEM(CtrlType *, MetisGraphType *, int);
float BetterVBalance(int, int, float *, float *, float *);
int AreAllVwgtsBelowFast(int, float *, float *, float);

/* mmd.c */
void genmmd(int, idxtype *, idxtype *, idxtype *, idxtype *, int , idxtype *, idxtype *, idxtype *, idxtype *, int, int *);
void mmdelm(int, idxtype *xadj, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, int, int);
int  mmdint(int, idxtype *xadj, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *);
void mmdnum(int, idxtype *, idxtype *, idxtype *);
void mmdupd(int, int, idxtype *, idxtype *, int, int *, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, int, int *tag);

/* mpmetis.c */
void METIS_mCPartGraphRecursive(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_mCHPartGraphRecursive(int *, int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void METIS_mCPartGraphRecursiveInternal(int *, int *, idxtype *, idxtype *, float *, idxtype *, int *, int *, int *, idxtype *);
void METIS_mCHPartGraphRecursiveInternal(int *, int *, idxtype *, idxtype *, float *, idxtype *, int *, float *, int *, int *, idxtype *);
int MCMlevelRecursiveBisection(CtrlType *, MetisGraphType *, int, idxtype *, float, int);
int MCHMlevelRecursiveBisection(CtrlType *, MetisGraphType *, int, idxtype *, float *, int);
void MCMlevelEdgeBisection(CtrlType *, MetisGraphType *, float *, float);
void MCHMlevelEdgeBisection(CtrlType *, MetisGraphType *, float *, float *);

/* mrefine.c */
void MocRefine2Way(CtrlType *, MetisGraphType *, MetisGraphType *, float *, float);
void MocAllocate2WayPartitionMemory(CtrlType *, MetisGraphType *);
void MocCompute2WayPartitionParams(CtrlType *, MetisGraphType *);
void MocProject2WayPartition(CtrlType *, MetisGraphType *);

/* mrefine2.c */
void MocRefine2Way2(CtrlType *, MetisGraphType *, MetisGraphType *, float *, float *);

/* mutil.c */
int AreAllVwgtsBelow(int, float, float *, float, float *, float);
int AreAnyVwgtsBelow(int, float, float *, float, float *, float);
int AreAllVwgtsAbove(int, float, float *, float, float *, float);
float ComputeLoadImbalance(int, int, float *, float *);
int AreAllBelow(int, float *, float *);

/* myqsort.c */
void iidxsort(int, idxtype *);
void iintsort(int, int *);
void ikeysort(int, KeyValueType *);
void ikeyvalsort(int, KeyValueType *);

/* ometis.c */
void METIS_EdgeND(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_NodeND(int *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void METIS_NodeWND(int *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *, idxtype *);
void MlevelNestedDissection(CtrlType *, MetisGraphType *, idxtype *, float, int);
void MlevelNestedDissectionCC(CtrlType *, MetisGraphType *, idxtype *, float, int);
void MlevelNodeBisectionMultiple(CtrlType *, MetisGraphType *, int *, float);
void MlevelNodeBisection(CtrlType *, MetisGraphType *, int *, float);
void SplitGraphOrder(CtrlType *, MetisGraphType *, MetisGraphType *, MetisGraphType *);
void MMDOrder(CtrlType *, MetisGraphType *, idxtype *, int);
int SplitGraphOrderCC(CtrlType *, MetisGraphType *, MetisGraphType *, int, idxtype *, idxtype *);

/* parmetis.c */
void METIS_PartGraphKway2(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPartGraphKway2(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
void METIS_NodeNDP(int, idxtype *, idxtype *, int, int *, idxtype *, idxtype *, idxtype *);
void MlevelNestedDissectionP(CtrlType *, MetisGraphType *, idxtype *, int, int, int, idxtype *);
void METIS_NodeComputeSeparator(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *);
void METIS_EdgeComputeSeparator(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, idxtype *);

/* pmetis.c */
void METIS_PartGraphRecursive(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, int *, int *, idxtype *);
void METIS_WPartGraphRecursive(int *, idxtype *, idxtype *, idxtype *, idxtype *, int *, int *, int *, float *, int *, int *, idxtype *);
int MlevelRecursiveBisection(CtrlType *, MetisGraphType *, int, idxtype *, float *, float, int);
void MlevelEdgeBisection(CtrlType *, MetisGraphType *, int *, float);
void SplitGraphPart(CtrlType *, MetisGraphType *, MetisGraphType *, MetisGraphType *);
void SetUpSplitGraph(MetisGraphType *, MetisGraphType *, int, int);

/* pqueue.c */
void PQueueInit(CtrlType *ctrl, PQueueType *, int, int);
void PQueueReset(PQueueType *);
void PQueueFree(CtrlType *ctrl, PQueueType *);
int PQueueGetSize(PQueueType *);
int PQueueInsert(PQueueType *, int, int);
int PQueueDelete(PQueueType *, int, int);
int PQueueUpdate(PQueueType *, int, int, int);
void PQueueUpdateUp(PQueueType *, int, int, int);
int PQueueGetMax(PQueueType *);
int PQueueSeeMax(PQueueType *);
int PQueueGetKey(PQueueType *);
int CheckHeap(PQueueType *);

/* refine.c */
void Refine2Way(CtrlType *, MetisGraphType *, MetisGraphType *, int *, float ubfactor);
void Allocate2WayPartitionMemory(CtrlType *, MetisGraphType *);
void Compute2WayPartitionParams(CtrlType *, MetisGraphType *);
void Project2WayPartition(CtrlType *, MetisGraphType *);

/* separator.c */
void ConstructSeparator(CtrlType *, MetisGraphType *, float);
void ConstructMinCoverSeparator0(CtrlType *, MetisGraphType *, float);
void ConstructMinCoverSeparator(CtrlType *, MetisGraphType *, float);

/* sfm.c */
void FM_2WayNodeRefine(CtrlType *, MetisGraphType *, float, int);
void FM_2WayNodeRefineEqWgt(CtrlType *, MetisGraphType *, int);
void FM_2WayNodeRefine_OneSided(CtrlType *, MetisGraphType *, float, int);
void FM_2WayNodeBalance(CtrlType *, MetisGraphType *, float);
int ComputeMaxNodeGain(int, idxtype *, idxtype *, idxtype *);

/* srefine.c */
void Refine2WayNode(CtrlType *, MetisGraphType *, MetisGraphType *, float);
void Allocate2WayNodePartitionMemory(CtrlType *, MetisGraphType *);
void Compute2WayNodePartitionParams(CtrlType *, MetisGraphType *);
void Project2WayNodePartition(CtrlType *, MetisGraphType *);

/* stat.c */
void ComputePartitionInfo(MetisGraphType *, int, idxtype *);
void ComputePartitionInfoBipartite(MetisGraphType *, int, idxtype *);
void ComputePartitionBalance(MetisGraphType *, int, idxtype *, float *);
float ComputeElementBalance(int, int, idxtype *);

/* subdomains.c */
void Random_KWayEdgeRefineMConn(CtrlType *, MetisGraphType *, int, float *, float, int, int);
void Greedy_KWayEdgeBalanceMConn(CtrlType *, MetisGraphType *, int, float *, float, int);
void PrintSubDomainGraph(MetisGraphType *, int, idxtype *);
void ComputeSubDomainGraph(MetisGraphType *, int, idxtype *, idxtype *);
void EliminateSubDomainEdges(CtrlType *, MetisGraphType *, int, float *);
void MoveGroupMConn(CtrlType *, MetisGraphType *, idxtype *, idxtype *, int, int, int, idxtype *);
void EliminateComponents(CtrlType *, MetisGraphType *, int, float *, float);
void MoveGroup(CtrlType *, MetisGraphType *, int, int, int, idxtype *, idxtype *);

/* timing.c */
void InitTimers(CtrlType *);
void PrintTimers(CtrlType *);
double seconds(void);

/* util.c */
void errexit(char *,...);
#ifndef DMALLOC
int *imalloc(int, char *);
idxtype *idxmalloc(int, char *);
float *fmalloc(int, char *);
int *ismalloc(int, int, char *);
idxtype *idxsmalloc(int, idxtype, char *);
void *GKmalloc(int, char *);
#endif
/*void GKfree(void **,...); */
int *iset(int n, int val, int *x);
idxtype *idxset(int n, idxtype val, idxtype *x);
float *sset(int n, float val, float *x);
int iamax(int, int *);
int idxamax(int, idxtype *);
int idxamax_strd(int, idxtype *, int);
int samax(int, float *);
int samax2(int, float *);
int idxamin(int, idxtype *);
int samin(int, float *);
int idxsum(int, idxtype *);
int idxsum_strd(int, idxtype *, int);
void idxadd(int, idxtype *, idxtype *);
int charsum(int, char *);
int isum(int, int *);
float ssum(int, float *);
float ssum_strd(int n, float *x, int);
void sscale(int n, float, float *x);
float snorm2(int, float *);
float sdot(int n, float *, float *);
void saxpy(int, float, float *, int, float *, int);
void RandomPermute(int, idxtype *, int);
//double drand48();
//void srand48(long);
int ispow2(int);
void InitRandom(int);
int log2i(int);










/***************************************************************
* Programs Directory
****************************************************************/

/* io.c */
void ReadGraph(MetisGraphType *, char *, int *);
void WritePartition(char *, idxtype *, int, int);
void WriteMeshPartition(char *, int, int, idxtype *, int, idxtype *);
void WritePermutation(char *, idxtype *, int);
int CheckGraph(MetisGraphType *);
idxtype *ReadMesh(char *, int *, int *, int *);
void WriteGraph(char *, int, idxtype *, idxtype *);

/* smbfactor.c */
void ComputeFillIn(MetisGraphType *, idxtype *);
idxtype ComputeFillIn2(MetisGraphType *, idxtype *);
int smbfct(int, idxtype *, idxtype *, idxtype *, idxtype *, idxtype *, int *, idxtype *, idxtype *, int *);


/***************************************************************
* Test Directory
****************************************************************/
void Test_PartGraph(int, idxtype *, idxtype *);
int VerifyPart(int, idxtype *, idxtype *, idxtype *, idxtype *, int, int, idxtype *);
int VerifyWPart(int, idxtype *, idxtype *, idxtype *, idxtype *, int, float *, int, idxtype *);
void Test_PartGraphV(int, idxtype *, idxtype *);
int VerifyPartV(int, idxtype *, idxtype *, idxtype *, idxtype *, int, int, idxtype *);
int VerifyWPartV(int, idxtype *, idxtype *, idxtype *, idxtype *, int, float *, int, idxtype *);
void Test_PartGraphmC(int, idxtype *, idxtype *);
int VerifyPartmC(int, int, idxtype *, idxtype *, idxtype *, idxtype *, int, float *, int, idxtype *);
void Test_ND(int, idxtype *, idxtype *);
int VerifyND(int, idxtype *, idxtype *);

