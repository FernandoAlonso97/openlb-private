/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Orestis Malaspinas, Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef KEEP_INCOMING_DYNAMICS_H
#define KEEP_INCOMING_DYNAMICS_H

#include "dynamics/dynamics.h"

namespace olb {

template<typename T>
struct StrainValue {
  static T value;
};

template<typename T>
T StrainValue<T>::value = T();

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta, int direction, int orientation>
class KeepIncomingDynamics : public BasicDynamics<T,Lattice,Momenta, KeepIncomingDynamics<T, Lattice, Dynamics, Momenta, direction, orientation>> {
public:
  /// Constructor
  KeepIncomingDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual KeepIncomingDynamics<T, Lattice, Dynamics, Momenta, direction, orientation>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta, int direction, int orientation>
class S1Dynamics : public BasicDynamics<T,Lattice,Momenta, S1Dynamics<T, Lattice, Dynamics, Momenta, direction, orientation>> {
public:
  /// Constructor
  S1Dynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual S1Dynamics<T, Lattice, Dynamics, Momenta, direction, orientation>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  Dynamics boundaryDynamics;
};

}


#endif
