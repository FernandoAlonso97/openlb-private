/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006,2007 Orestis Malaspinas and Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef KINETIC_DYNAMICS_HH
#define KINETIC_DYNAMICS_HH

#include "kineticDynamics.h"
#include "dynamics/latticeDescriptors.h"
#include "core/util.h"
#include "dynamics/lbHelpers.h"
#include <cmath>


namespace olb {

using namespace descriptors;


///////////// class KineticDynamics ////////////////////////////////////

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
KineticDynamics<T,Lattice,Dynamics,direction,orientation>::KineticDynamics (
  T omega_, Momenta& momenta_ )
  : BasicDynamics<T,Lattice>(momenta_),
    boundaryDynamics(omega_, momenta_)
{
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
KineticDynamics<T,Lattice,Dynamics,direction,orientation>* KineticDynamics<T,Lattice, Dynamics, direction, orientation>::clone() const
{
  return new KineticDynamics<T,Lattice,Dynamics,direction,orientation>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
T KineticDynamics<T,Lattice, Dynamics, direction, orientation>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void KineticDynamics<T,Lattice,Dynamics,direction,orientation>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef lbHelpers<T,Lattice> lbH;
  typedef Lattice<T> L;

  std::vector<int> missingIndexes = util::subIndexOutgoing<L,direction,orientation>();
  //     std::cout << "dir = " << direction << ", orien = " << orientation << std::endl;
  //     for (unsigned iPop = 0; iPop < missingIndexes.size(); ++iPop)
  //     {
  //         std::cout << missingIndexes[iPop] << ", ";
  //     }
  //     std::cout << std::endl;
  //     std::vector<int> knownIndexes;
  //     for (unsigned iPop = 0; iPop < missingIndexes.size(); ++iPop)
  //     {
  //         knownIndexes.push_back(util::opposite<Lattice<T> >(missingIndexes[iPop]));
  //     }
  T rho, u[L::d];
  this->momenta.computeRhoU(cell,rho,u);
  //     boundaryDynamics.collide(cell, statistics);
  //     T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(offLatticeVelocity);

  //     T sumEq = T();
  //     T sumF = T();
  //     T fEq[missingIndexes.size()];
  //     for (unsigned iPop = 0; iPop < missingIndexes.size(); ++iPop)
  //     {
  //         fEq[iPop] = computeEquilibrium(missingIndexes[iPop], rho, offLatticeVelocity, uSqr) +
  //                 Lattice<T>::t[missingIndexes[iPop]];
  // //         fEq[iPop] = computeEquilibrium(util::opposite<Lattice<T> >( missingIndexes[iPop]), rho, u, uSqr) +
  // //                 Lattice<T>::t[util::opposite<Lattice<T> >(missingIndexes[iPop])];
  //         sumEq += fEq[iPop];
  //         sumF += cell[util::opposite<Lattice<T> >(missingIndexes[iPop])] +
  //                 Lattice<T>::t[util::opposite<Lattice<T> >(missingIndexes[iPop])];
  //     }
  for (unsigned iPop = 0; iPop < missingIndexes.size(); ++iPop) {
    //         cell[missingIndexes[iPop]] = fEq[iPop] * sumF / sumEq -
    //                 Lattice<T>::t[missingIndexes[iPop]];
    cell[missingIndexes[iPop]] = computeEquilibrium( missingIndexes[iPop], rho, offLatticeVelocity, uSqr);
  }
  boundaryDynamics.collide(cell, statistics);

  //     T rhoTest, uTest[L::d];

  //     lbH::computeRhoU(cell,rhoTest,uTest);
  //     std::cout << rho - rhoTest << ", " << rho*u[0]-rhoTest*uTest[0] << ", ";
  //     std::cout << rho*u[1] - rhoTest*uTest[1] << std::endl;

  //     std::cout << rho << ", " << rhoTest << ", " << rho*u[0] << ", " << rhoTest*uTest[0] << ", ";
  //     std::cout << rho*u[1] << ", " << rhoTest*uTest[1] << std::endl;

}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
T KineticDynamics<T,Lattice,Dynamics,direction,orientation>::getOmega() const
{
  return boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void KineticDynamics<T,Lattice,Dynamics,direction,orientation>::setOmega(T omega_)
{
  boundaryDynamics.setOmega(omega_);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void KineticDynamics<T,Lattice,Dynamics,direction,orientation>::setOffLatticeVelocity(const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    offLatticeVelocity[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice, typename Dynamics, int direction, int orientation>
void KineticDynamics<T,Lattice,Dynamics,direction,orientation>::getOffLatticeVelocity(T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = offLatticeVelocity[iD];
  }
}


}  // namespace olb

#endif
