/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef MULTI_PHYSICS_OST_PROCESSOR_2D_H
#define MULTI_PHYSICS_OST_PROCESSOR_2D_H

#include "spatiallyExtendedObject.h"
#include "core/postProcessing.h"
#include "core/blockLattice3D.h"
#include <cmath>

using namespace boost;


namespace olb {

/**
* Multiphysics class for coupling between different lattices.
*/

template<typename T, template<typename U> class Lattice>
class MultiPhysicsPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  MultiPhysicsPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
                              std::vector<SpatiallyExtendedObject> &spBlock_);
  virtual int extent() const
  {
    return 1;
  }
  virtual int extent(int whichDirection) const
  {
    return 1;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
private:
  int x0, x1, y0, y1;

  std::vector<SpatiallyExtendedObject> &spBlock
};

template<typename T, template<typename U> class Lattice>
class MultiPhysicsPostProcessorGenerator2D
  : public PostProcessorGenerator2D<T,Lattice> {
public:
  MultiPhysicsPostProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_,
                                       std::vector<SpatiallyExtendedObject> &spBlock_);
  virtual PostProcessor2D<T,Lattice>* generate() const;
  virtual PostProcessorGenerator2D<T,Lattice>*  clone() const;
private:
  std::vector<SpatiallyExtendedObject> &spBlock_;
};


}

#endif
