/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef MULTI_PHYSICS_OST_PROCESSOR_2D_HH
#define MULTI_PHYSICS_OST_PROCESSOR_2D_HH

#include "core/latticeDescriptors.h"
#include "multiPhysicsPostProcessor2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"

using namespace boost;

namespace olb {

////////  MultiPhysicsPostProcessor2D ///////////////////////////////////

template<typename T, template<typename U> class Lattice>
MultiPhysicsPostProcessor2D <T,Lattice>::
MultiPhysicsPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
                            std::vector<SpatiallyExtendedObject> &spBlock_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_), spBlock(spBlock_)
{
}

template<typename T, template<typename U> class Lattice>
void MultiPhysicsPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  using namespace olb::util::tensorIndices2D;
  typedef lbHelpers<T,Lattice> lbH;
  enum {x,y};

  int newX0, newX1, newY0, newY1;
  int Lx = blockLattice.getNx();
  int Ly = blockLattice.getNy();
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {

        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void MultiPhysicsPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

////////  ExtendedFdPlaneBoundaryProcessorGenertor2D ///////////////////////////////

template<typename T, template<typename U> class Lattice>
MultiPhysicsPostProcessorGenerator2D<T,Lattice>::
MultiPhysicsPostProcessorGenerator2D(int x0_, int x1_, int y0_, int y1_,
                                     std::vector<SpatiallyExtendedObject> &spBlock_)
  : PostProcessorGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_), spBlock(spBlock_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>*
MultiPhysicsPostProcessorGenerator2D<T,Lattice>::generate() const
{
  return new MultiPhysicsPostProcessor2D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, this->spBlock);
}

template<typename T, template<typename U> class Lattice>
PostProcessorGenerator2D<T,Lattice>*
MultiPhysicsPostProcessorGenerator2D<T,Lattice>::clone() const
{
  return new MultiPhysicsPostProcessorGenerator2D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, this->spBlock);
}


}  // namespace olb

#endif
