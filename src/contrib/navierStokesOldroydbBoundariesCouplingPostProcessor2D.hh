/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef NAVIER_STOKES_OLDROYDB_BOUNDARIES_COUPLING_POST_PROCESSOR_2D_HH
#define NAVIER_STOKES_OLDROYDB_BOUNDARIES_COUPLING_POST_PROCESSOR_2D_HH

#include "core/latticeDescriptors.h"
#include "contrib/viscoelasticLatticeDescriptors.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include "contrib/navierStokesOldroydbCouplingPostProcessor2D.h"
#include "contrib/navierStokesOldroydbBoundariesCouplingPostProcessor2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "core/finiteDifference2D.h"
#include "contrib/viscoFiniteDifference2D.h"
#include "contrib/viscoHelpers.h"
#include "contrib/firstOrderAdvectionDiffusionWithSourceLbHelpers.h"
#include "contrib/advectionDiffusionWithSourceLbHelpers.h"


using namespace std;

namespace olb {


//=====================================================================================
//==  FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D =======
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        Axx_T1.push_back(AXXlattice->get(iX,iY).computeRho());
        Axy_T1.push_back(AXYlattice->get(iX,iY).computeRho());
        Ayy_T1.push_back(AYYlattice->get(iX,iY).computeRho());
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  //        std::cout << "direction = " << direction << ", orientation = " << orientation << ", iOpp = " << iOpp << ", missing = " << missingDiagonal[0] << endl;

  int newX0, newX1, newY0, newY1;
  unsigned iPos = 0;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        blockLattice.get(iX,iY).computeU(uXX);
        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    //    T error = T();

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T dx_u[L::d], dy_u[L::d];
        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        T gradU[L::d][L::d];
        gradU[x][x] = dx_u[x];
        gradU[x][y] = dx_u[y];
        gradU[y][x] = dy_u[x];
        gradU[y][y] = dy_u[y];

        computeA(A, Axx_T1[iPos], Axy_T1[iPos], Ayy_T1[iPos],gradU);
        Axx_T1[iPos] = A[xx];
        Axy_T1[iPos] = A[xy];
        Ayy_T1[iPos] = A[yy];

        //        error += abs(AXXlattice->get(iX,iY).computeRho() - A[xx]);
        //        error += abs(AYYlattice->get(iX,iY).computeRho() - A[yy]);
        //        error += abs(AXYlattice->get(iX,iY).computeRho() - A[xy]);


        //        std::cout << "(" << iX << ", " << iY << "), a_xx diff = " << AXXlattice->get(iX,iY).computeRho() - A[xx] << std::endl;
        //        std::cout << "(" << iX << ", " << iY << "), a_xy diff = " << AXYlattice->get(iX,iY).computeRho() - A[xy] << std::endl;
        //        std::cout << "(" << iX << ", " << iY << "), a_yy diff = " << AYYlattice->get(iX,iY).computeRho() - A[yy] << std::endl;

        AXXlattice->get(iX,iY).defineRho(A[xx]);
        AXYlattice->get(iX,iY).defineRho(A[xy]);
        AYYlattice->get(iX,iY).defineRho(A[yy]);

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        T source[util::TensorVal<Lattice<T> >::n];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];

        ++iPos;
      }
    }
    //    std::cout << error << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n],
                  T tau[util::TensorVal<Lattice<T> >::n],
                  T A[util::TensorVal<Lattice<T> >::n],
                  T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeA(T A[util::TensorVal<Lattice<T> >::n],
         T Axx_T1, T Axy_T1, T Ayy_T1,
         T gradU[Lattice<T>::d][Lattice<T>::d])
{
  enum {x, y};
  using namespace util::tensorIndices2D;

  A[xx] = Axx_T1 -(Axx_T1-(T)1)/lambda + (T)2 * (Axx_T1 * gradU[x][x] + Axy_T1*gradU[y][x]);
  A[yy] = Ayy_T1 -(Ayy_T1-(T)1)/lambda + (T)2 * (Ayy_T1 * gradU[y][y] + Axy_T1*gradU[x][y]);
  A[xy] = Axy_T1 - Axy_T1/lambda + Axy_T1 * (gradU[x][x] + gradU[y][y]) + Axx_T1 * gradU[x][y]
          + Ayy_T1 * gradU[y][x];
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatFdBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
FlatFdBoundaryNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor2D<T,Lattice>* FlatFdBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new FlatFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
LatticeCouplingGenerator2D<T,Lattice>*
FlatFdBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
clone() const
{
  return new FlatFdBoundaryNavierStokesOldroydbCouplingGenerator2D<
         T,Lattice,direction,orientation>(*this);
}

//=====================================================================================
//==============  CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), muP(muP_), partners(partners_)
{
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  Axx_T1 = AXXlattice->get(x,y).computeRho();
  Axy_T1 = AXYlattice->get(x,y).computeRho();
  Ayy_T1 = AYYlattice->get(x,y).computeRho();

}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  T *uXX = AXXlattice->get(x,y).getExternal(velOffset);

  // we have to store the values of the A_{alpha beta}u_{gamma}
  // in order to regularize the collision and remove
  // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
  T *axxU = AXXlattice->get(x,y).getExternal(aUoffset);
  T *axyU = AXYlattice->get(x,y).getExternal(aUoffset);
  T *ayyU = AYYlattice->get(x,y).getExternal(aUoffset);
  for (int iD = 0; iD < L::d; ++iD) {
    axxU[iD] *= uXX[iD];
    axyU[iD] *= uXX[iD];
    ayyU[iD] *= uXX[iD];
  }

  blockLattice.get(x,y).computeU(uXX);
  T *uXY = AXYlattice->get(x,y).getExternal(velOffset);
  T *uYY = AYYlattice->get(x,y).getExternal(velOffset);

  for (int iD = 0; iD < L::d; ++iD) {
    uXY[iD] = uXX[iD];
    uYY[iD] = uXX[iD];
  }

  // computation of the gradients of u
  // the non diagonal part must be
  // computed using finite difference
  // the diagonal using f_fneq.
  T dx_u[L::d], dy_u[L::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateVector(dy_u, blockLattice, x,y);

  T gradU[L::d][L::d];
  gradU[xCoord][xCoord] = dx_u[xCoord];
  gradU[xCoord][yCoord] = dx_u[yCoord];
  gradU[yCoord][xCoord] = dy_u[xCoord];
  gradU[yCoord][yCoord] = dy_u[yCoord];

  T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
  computeA(A, Axx_T1, Axy_T1, Ayy_T1,gradU);

  Axx_T1 = A[xx];
  Ayy_T1 = A[yy];
  Axy_T1 = A[xy];


  AXXlattice->get(x,y).defineRho(A[xx]);
  AXYlattice->get(x,y).defineRho(A[xy]);
  AYYlattice->get(x,y).defineRho(A[yy]);

  T *tau = blockLattice.get(x,y).getExternal(tauOffset);
  fromAtoTau(A, tau);

  T source[util::TensorVal<Lattice<T> >::n];
  computeSourceTerm(source, tau, A, gradU);

  T *rhs = AXXlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xx];
  rhs = AXYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xy];
  rhs = AYYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[yy];
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeA(T A[util::TensorVal<Lattice<T> >::n],
         T Axx_T1, T Axy_T1, T Ayy_T1,
         T gradU[Lattice<T>::d][Lattice<T>::d])
{
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  A[xx] = Axx_T1 -(Axx_T1-(T)1)/lambda + (T)2 * (Axx_T1 * gradU[xCoord][xCoord] + Axy_T1*gradU[yCoord][xCoord]);
  A[yy] = Ayy_T1 -(Ayy_T1-(T)1)/lambda + (T)2 * (Ayy_T1 * gradU[yCoord][yCoord] + Axy_T1*gradU[xCoord][yCoord]);
  A[xy] = Axy_T1 - Axy_T1/lambda + Axy_T1 * (gradU[xCoord][xCoord] + gradU[yCoord][yCoord])
          + Axx_T1 * gradU[xCoord][yCoord] + Ayy_T1 * gradU[yCoord][xCoord];
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerFdBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
CornerFdBoundaryNavierStokesOldroydbCouplingGenerator2D(int x_, int y_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>* CornerFdBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
generate (std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new
         CornerFdBoundaryNavierStokesOldroydbCouplingPostProcessor2D
         <T,Lattice,xNormal,yNormal>(this->x0,this->y0, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>* CornerFdBoundaryNavierStokesOldroydbCouplingGenerator2D
<T,Lattice,xNormal,yNormal>::clone() const
{
  return new
         CornerFdBoundaryNavierStokesOldroydbCouplingGenerator2D
         <T,Lattice,xNormal,yNormal>(*this);
}

}  // namespace olb

#endif

