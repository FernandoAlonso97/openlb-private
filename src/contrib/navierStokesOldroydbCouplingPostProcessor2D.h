/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef NAVIER_STOKES_OLDROYDB_COUPLING_POST_PROCESSOR_2D_H
#define NAVIER_STOKES_OLDROYDB_COUPLING_POST_PROCESSOR_2D_H

#include "core/spatiallyExtendedObject2D.h"
#include "core/postProcessing.h"
#include "core/blockLattice2D.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include <cmath>


namespace olb {

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
// This coupling must be necessarily be put on the Navier-Stokes lattice!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//======================================================================
// ========Bulk navier-stoke oldroyd-b coupling ====================//
//======================================================================
template<typename T, template<typename U> class Lattice>
class BulkNavierStokesOldroydbCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  BulkNavierStokesOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 1;
  }
  virtual int extent(int whichDirection) const
  {
    return 1;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

  void fromAtoTau(T A[util::TensorVal<Lattice<T> >::n],
                  T tau[util::TensorVal<Lattice<T> >::n]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n],
                         T tau[util::TensorVal<Lattice<T> >::n],
                         T A[util::TensorVal<Lattice<T> >::n],
                         T gradU[Lattice<T>::d][Lattice<T>::d]);
  // computes the source term for the Oldroyd-b constitutive equation
private:
  int x0, x1, y0, y1;
  T lambda, muP; // relaxation time, dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice>
class BulkNavierStokesOldroydbCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  BulkNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>*
  generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP; // relaxation time, dyn visc of polymer
};

//======================================================================
// ========flat boundary navier-stoke oldroyd-b coupling ===============
//======================================================================
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D(
    int x0_, int x1_, int y0_, int y1_, T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

private:
  template<int deriveDirection>
  void interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                            T velDeriv[Lattice<T>::d], int iX, int iY) const;
  void fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n],
                         T tau[util::TensorVal<Lattice<T> >::n],
                         T A[util::TensorVal<Lattice<T> >::n],
                         T gradU[Lattice<T>::d][Lattice<T>::d]);
private:
  int x0, x1, y0, y1;
  T lambda, muP; // relaxation time, dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class FlatBoundaryNavierStokesOldroydbCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  FlatBoundaryNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>*
  generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP; // relaxation time, dyn visc of polymer
};

//======================================================================
// =======flat interp boundary navier-stoke oldroyd-b coupling =========
//======================================================================
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D(
    int x0_, int x1_, int y0_, int y1_, T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

private:
  template<int deriveDirection>
  void interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                            T velDeriv[Lattice<T>::d], int iX, int iY) const;
  void fromAtoTau(T A[util::TensorVal<Lattice<T> >::n],
                  T tau[util::TensorVal<Lattice<T> >::n]);
  // converts the conformation tensor into tau
  void computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n],
                         T tau[util::TensorVal<Lattice<T> >::n],
                         T A[util::TensorVal<Lattice<T> >::n],
                         T gradU[Lattice<T>::d][Lattice<T>::d]);
  // computes the source term for the FENE-P constitutive equation
  T interpolateA(BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &block,
                 int iX, int iY);
private:
  int x0, x1, y0, y1;
  T lambda, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer
};

//======================================================================
// ========corner boundary navier-stoke oldroyd-b coupling =============//
//======================================================================
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x_, int y_,
      T lambda_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

  void fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d]);
  // computes the source term for the FENE-P constitutive equation
private:
  int x, y;
  T lambda, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class CornerBoundaryNavierStokesOldroydbCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  CornerBoundaryNavierStokesOldroydbCouplingGenerator2D(int x_, int y_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer
};

//======================================================================
// ========corner boundary navier-stoke oldroyd-b coupling =============//
//======================================================================
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x_, int y_,
      T lambda_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

  void fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d]);
  // computes the source term for the FENE-P constitutive equation
private:
  T interpolateA(BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &block);

  int x, y;
  T lambda, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D(int x_, int y_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer
};


} // old

#endif

