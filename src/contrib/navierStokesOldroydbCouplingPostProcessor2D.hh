/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef NAVIER_STOKES_OLDROYDB_COUPLING_POST_PROCESSOR_2D_HH
#define NAVIER_STOKES_OLDROYDB_COUPLING_POST_PROCESSOR_2D_HH

#include "core/latticeDescriptors.h"
#include "contrib/viscoelasticLatticeDescriptors.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include "contrib/navierStokesOldroydbCouplingPostProcessor2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "core/finiteDifference2D.h"
#include "contrib/viscoFiniteDifference2D.h"
#include "contrib/viscoHelpers.h"
#include "contrib/firstOrderAdvectionDiffusionWithSourceLbHelpers.h"
#include "contrib/advectionDiffusionWithSourceLbHelpers.h"

#include "contrib/extrapolation.h"


using namespace std;

namespace olb {

//=====================================================================================
//=====================================================================================
//==============  BulkNavierStokesOLDROYDBCouplingPostProcessor2D ===============
//=====================================================================================
//=====================================================================================

template<typename T, template<typename U> class Lattice>
BulkNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice>::
BulkNavierStokesOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
        A[xx] = AXXlattice->get(iX,iY).computeRho();
        A[xy] = AXYlattice->get(iX,iY).computeRho();
        A[yy] = AYYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        T gradU[L::d][L::d], rho, u[L::d], pi[util::TensorVal<Lattice<T> >::n];
        blockLattice.get(iX,iY).computeAllMomenta(rho,u,pi);
        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXX[iD] = u[iD];
          uXY[iD] = u[iD];
          uYY[iD] = u[iD];
        }

        //        T piToS = -blockLattice.getDynamics(iX,iY)->getOmega() *
        //            L::invCs2()/((T)2*rho);
        //        gradU[x][x] = piToS*(pi[xx] + tau[xx]);
        //        gradU[y][y] = piToS*(pi[yy] + tau[yy]);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T u_m1[L::d],u_p1[L::d];
        blockLattice.get(iX-1,iY).computeU(u_m1);
        blockLattice.get(iX+1,iY).computeU(u_p1);

        gradU[x][x] = (u_p1[0]-u_m1[0])/(T)2;
        gradU[x][y] = (u_p1[1]-u_m1[1])/(T)2;

        blockLattice.get(iX,iY-1).computeU(u_m1);
        blockLattice.get(iX,iY+1).computeU(u_p1);

        gradU[y][x] = (u_p1[0]-u_m1[0])/(T)2;
        gradU[y][y] = (u_p1[1]-u_m1[1])/(T)2;

        T source[3];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
BulkNavierStokesOldroydbCouplingGenerator2D<T,Lattice>::
BulkNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* BulkNavierStokesOldroydbCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new BulkNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* BulkNavierStokesOldroydbCouplingGenerator2D<T,Lattice>::clone() const
{
  return new BulkNavierStokesOldroydbCouplingGenerator2D<T,Lattice>(*this);
}


//=====================================================================================
//========  FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D =======
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        blockLattice.get(iX,iY).computeU(uXX);
        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[util::TensorVal<Lattice<T> >::n];
        A[xx] = AXXlattice->get(iX,iY).computeRho();
        A[yy] = AYYlattice->get(iX,iY).computeRho();
        A[xy] = AXYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T dx_u[L::d], dy_u[L::d];
        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        T gradU[L::d][L::d];
        gradU[x][x] = dx_u[x];
        gradU[x][y] = dx_u[y];
        gradU[y][x] = dy_u[x];
        gradU[y][y] = dy_u[y];

        T source[util::TensorVal<Lattice<T> >::n];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
FlatBoundaryNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor2D<T,Lattice>* FlatBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new FlatBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
LatticeCouplingGenerator2D<T,Lattice>* FlatBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new FlatBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>(*this);
}

//=====================================================================================
//==  FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D =======
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        blockLattice.get(iX,iY).computeU(uXX);
        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
        A[xx] = interpolateA(*AXXlattice,iX, iY);
        A[xy] = interpolateA(*AXYlattice,iX, iY);
        A[yy] = interpolateA(*AYYlattice,iX, iY);

        AXXlattice->get(iX,iY).defineRho(A[xx]);
        AXYlattice->get(iX,iY).defineRho(A[xy]);
        AYYlattice->get(iX,iY).defineRho(A[yy]);

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T dx_u[L::d], dy_u[L::d];
        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        T gradU[L::d][L::d];
        gradU[x][x] = dx_u[x];
        gradU[x][y] = dx_u[y];
        gradU[y][x] = dy_u[x];
        gradU[y][y] = dy_u[y];

        T source[util::TensorVal<Lattice<T> >::n];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];
      }
    }
    //    std::cout << error << std::endl;
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n],
                  T tau[util::TensorVal<Lattice<T> >::n],
                  T A[util::TensorVal<Lattice<T> >::n],
                  T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation>
T FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateA(BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &block,
             int iX, int iY)
{
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  typedef flatBoundariesAdevectionDiffusionWithSourceLbHelpers<
  T,AdvectionDiffusionWithSourceD2Q5Descriptor, direction, orientation> adH;

  CellView<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &cell = block.get(iX,iY);

  const T A1 = block.get(iX+(direction==0 ? (-orientation):0),
                         iY+(direction==1 ? (-orientation):0) ).computeRho();
  const T A2 = block.get(iX+(direction==0 ? (-2*orientation):0),
                         iY+(direction==1 ? (-2*orientation):0) ).computeRho();
  const T A3 = block.get(iX+(direction==0 ? (-3*orientation):0),
                         iY+(direction==1 ? (-3*orientation):0) ).computeRho();

  std::vector<int> uI = util::subIndexOutgoing<adL,direction,orientation>();
  std::vector<int> kI = util::remainingIndexes<adL>(uI);

  T A0 = fd::boundaryExtrapolation(A1,A2,A3);

  T omega = block.getDynamics(iX, iY)->getOmega();
  //  T A = adH::computeRhoFromFneq(cell, omega, A1);
  //  T A = adH::computeRhoFromFneq(cell, omega, A1, A2);
  T A = adH::computeRhoFromJ1(cell, omega, A1, A2, kI, uI);

  //  std::cout << (A0-A)/A0 << std::endl;

  return (A+A0)/(T)2;
  //  return A;
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor2D<T,Lattice>* FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new FlatInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
LatticeCouplingGenerator2D<T,Lattice>*
FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
clone() const
{
  return new FlatInterpBoundaryNavierStokesOldroydbCouplingGenerator2D<
         T,Lattice,direction,orientation>(*this);
}

//=====================================================================================
//==============  CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  T *uXX = AXXlattice->get(x,y).getExternal(velOffset);

  // we have to store the values of the A_{alpha beta}u_{gamma}
  // in order to regularize the collision and remove
  // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
  T *axxU = AXXlattice->get(x,y).getExternal(aUoffset);
  T *axyU = AXYlattice->get(x,y).getExternal(aUoffset);
  T *ayyU = AYYlattice->get(x,y).getExternal(aUoffset);
  for (int iD = 0; iD < L::d; ++iD) {
    axxU[iD] *= uXX[iD];
    axyU[iD] *= uXX[iD];
    ayyU[iD] *= uXX[iD];
  }

  blockLattice.get(x,y).computeU(uXX);
  T *uXY = AXYlattice->get(x,y).getExternal(velOffset);
  T *uYY = AYYlattice->get(x,y).getExternal(velOffset);

  for (int iD = 0; iD < L::d; ++iD) {
    uXY[iD] = uXX[iD];
    uYY[iD] = uXX[iD];
  }

  T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
  A[xx] = AXXlattice->get(x,y).computeRho();
  A[xy] = AXYlattice->get(x,y).computeRho();
  A[yy] = AYYlattice->get(x,y).computeRho();

  T *tau = blockLattice.get(x,y).getExternal(tauOffset);
  fromAtoTau(A, tau);

  // computation of the gradients of u
  // the non diagonal part must be
  // computed using finite difference
  // the diagonal using f_fneq.
  T dx_u[L::d], dy_u[L::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateVector(dy_u, blockLattice, x,y);

  T gradU[L::d][L::d];
  gradU[xCoord][xCoord] = dx_u[xCoord];
  gradU[xCoord][yCoord] = dx_u[yCoord];
  gradU[yCoord][xCoord] = dy_u[xCoord];
  gradU[yCoord][yCoord] = dy_u[yCoord];

  T source[util::TensorVal<Lattice<T> >::n];
  computeSourceTerm(source, tau, A, gradU);

  T *rhs = AXXlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xx];
  rhs = AXYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xy];
  rhs = AYYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[yy];
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n],
                  T tau[util::TensorVal<Lattice<T> >::n],
                  T A[util::TensorVal<Lattice<T> >::n],
                  T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
CornerBoundaryNavierStokesOldroydbCouplingGenerator2D(int x_, int y_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>* CornerBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
generate (std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new
         CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D
         <T,Lattice,xNormal,yNormal>(this->x0,this->y0, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>* CornerBoundaryNavierStokesOldroydbCouplingGenerator2D
<T,Lattice,xNormal,yNormal>::clone() const
{
  return new
         CornerBoundaryNavierStokesOldroydbCouplingGenerator2D
         <T,Lattice,xNormal,yNormal>(*this);
}

//=====================================================================================
//==============  CornerBoundaryNavierStokesOldroydbCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  T *uXX = AXXlattice->get(x,y).getExternal(velOffset);

  // we have to store the values of the A_{alpha beta}u_{gamma}
  // in order to regularize the collision and remove
  // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
  T *axxU = AXXlattice->get(x,y).getExternal(aUoffset);
  T *axyU = AXYlattice->get(x,y).getExternal(aUoffset);
  T *ayyU = AYYlattice->get(x,y).getExternal(aUoffset);
  for (int iD = 0; iD < L::d; ++iD) {
    axxU[iD] *= uXX[iD];
    axyU[iD] *= uXX[iD];
    ayyU[iD] *= uXX[iD];
  }

  blockLattice.get(x,y).computeU(uXX);
  T *uXY = AXYlattice->get(x,y).getExternal(velOffset);
  T *uYY = AYYlattice->get(x,y).getExternal(velOffset);

  for (int iD = 0; iD < L::d; ++iD) {
    uXY[iD] = uXX[iD];
    uYY[iD] = uXX[iD];
  }

  T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
  A[xx] = interpolateA(*AXXlattice);
  A[xy] = interpolateA(*AXYlattice);
  A[yy] = interpolateA(*AYYlattice);

  AXXlattice->get(x,y).defineRho(A[xx]);
  AXYlattice->get(x,y).defineRho(A[xy]);
  AYYlattice->get(x,y).defineRho(A[yy]);

  T *tau = blockLattice.get(x,y).getExternal(tauOffset);
  fromAtoTau(A, tau);

  // computation of the gradients of u
  // the non diagonal part must be
  // computed using finite difference
  // the diagonal using f_fneq.
  T dx_u[L::d], dy_u[L::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateVector(dy_u, blockLattice, x,y);

  T gradU[L::d][L::d];
  gradU[xCoord][xCoord] = dx_u[xCoord];
  gradU[xCoord][yCoord] = dx_u[yCoord];
  gradU[yCoord][xCoord] = dy_u[xCoord];
  gradU[yCoord][yCoord] = dy_u[yCoord];

  T source[util::TensorVal<Lattice<T> >::n];
  computeSourceTerm(source, tau, A, gradU);

  T *rhs = AXXlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xx];
  rhs = AXYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xy];
  rhs = AYYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[yy];
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
T CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
interpolateA(BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &block)
{
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  std::vector<int> uI = utilAdvDiff::subIndexOutgoing2DonCorners<adL,xNormal,yNormal>();
  std::vector<int> kI = util::remainingIndexes<adL>(uI);

  CellView<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &cell = block.get(x,y);
  T omega = block.getDynamics(x, y)->getOmega();

  //========= the interpolation using the mormals ==================
  //  typedef flatBoundariesAdevectionDiffusionWithSourceLbHelpers<
  //    T,AdvectionDiffusionWithSourceD2Q5Descriptor, 0, xNormal> adHx;
  //  typedef flatBoundariesAdevectionDiffusionWithSourceLbHelpers<
  //    T,AdvectionDiffusionWithSourceD2Q5Descriptor, 1, yNormal> adHy;
  //
  //  std::vector<T> aTemp;
  //
  //  T A1 = block.get(x - xNormal,  y).computeRho();
  //  T A2 = block.get(x - 2*xNormal,  y).computeRho();
  //  T A3 = block.get(x - 3*xNormal,  y).computeRho();
  //  aTemp.push_back(fd::boundaryExtrapolation(A1,A2,A3));
  //  aTemp.push_back(adHx::computeRhoFromJ1(cell, omega, A1, A2, kI, uI));
  //
  //  A1 = block.get(x, y - yNormal).computeRho();
  //  A2 = block.get(x, y - 2*yNormal).computeRho();
  //  A3 = block.get(x, y - 3*yNormal).computeRho();
  //  aTemp.push_back(fd::boundaryExtrapolation(A1,A2,A3));
  //  aTemp.push_back(adHy::computeRhoFromJ1(cell, omega, A1, A2, kI, uI));
  //
  //  T A = T();
  //  for (unsigned iPop = 0; iPop < aTemp.size(); ++iPop)
  //  {
  //    A += aTemp[iPop];
  //  }
  //  A /= (T)aTemp.size();

  //========= the interpolation using the diagonals ==================
  typedef flatBoundariesAdevectionDiffusionWithSourceLbHelpers<
  T,AdvectionDiffusionWithSourceD2Q5Descriptor, xNormal, yNormal> adHxy;

  T A1 = block.get(x - xNormal,  y - yNormal).computeRho();
  T A2 = block.get(x - 2*xNormal,  y - 2*yNormal).computeRho();
  T A3 = block.get(x - 3*xNormal,  y - 3*yNormal).computeRho();
  T A0 = fd::boundaryExtrapolation(A1,A2,A3);

  T A = adHxy::computeRhoCornerFromJ1(cell, omega, A1, A2, kI, uI);

  return (A+A0)/(T)2;
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D(int x_, int y_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>* CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
generate (std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new
         CornerInterpBoundaryNavierStokesOldroydbCouplingPostProcessor2D
         <T,Lattice,xNormal,yNormal>(this->x0,this->y0, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>* CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D
<T,Lattice,xNormal,yNormal>::clone() const
{
  return new
         CornerInterpBoundaryNavierStokesOldroydbCouplingGenerator2D
         <T,Lattice,xNormal,yNormal>(*this);
}

}  // namespace olb

#endif

