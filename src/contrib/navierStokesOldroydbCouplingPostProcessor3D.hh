/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef NAVIER_STOKES_OLDROYDB_COUPLING_POST_PROCESSOR_3D_HH
#define NAVIER_STOKES_OLDROYDB_COUPLING_POST_PROCESSOR_3D_HH

#include "core/latticeDescriptors.h"
#include "contrib/viscoelasticLatticeDescriptors.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include "contrib/navierStokesOldroydbCouplingPostProcessor3D.h"
#include "core/blockLattice3D.h"
#include "core/util.h"
#include "core/finiteDifference3D.h"
#include "contrib/viscoHelpers.h"
#include "contrib/firstOrderAdvectionDiffusionWithSourceLbHelpers.h"
#include "contrib/advectionDiffusionWithSourceLbHelpers.h"

#include "contrib/extrapolation.h"


using namespace std;

namespace olb {

//=====================================================================================
//=====================================================================================
//==============  BulkNavierStokesOLDROYDBCouplingPostProcessor3D ===============
//=====================================================================================
//=====================================================================================

template<typename T, template<typename U> class Lattice>
BulkNavierStokesOldroydbCouplingPostProcessor3D<T,Lattice>::
BulkNavierStokesOldroydbCouplingPostProcessor3D(int x0_, int x1_, int y0_, int y1_,
    int z0_, int z1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject3D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor3D<T,Lattice>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD3Q7Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices3D;
  enum {x, y, z};

  BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *>(partners[0]);
  BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *>(partners[1]);
  BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *AXZlattice =
    dynamic_cast<BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *>(partners[2]);
  BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *>(partners[3]);
  BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *AYZlattice =
    dynamic_cast<BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *>(partners[4]);
  BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *AZZlattice =
    dynamic_cast<BlockLattice3D<T,AdvectionDiffusionWithSourceD3Q7Descriptor> *>(partners[5]);

  int newX0, newX1, newY0, newY1, newZ0, newZ1;
  if ( util::intersect (
         x0, x1, y0, y1, z0, z1,
         x0_, x1_, y0_, y1_, z0_, z1_,
         newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          T *uXX = AXXlattice->get(iX,iY,iZ).getExternal(velOffset);

          // we have to store the values of the A_{alpha beta}u_{gamma}
          // in order to regularize the collision and remove
          // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
          T *axxU = AXXlattice->get(iX,iY,iZ).getExternal(aUoffset);
          T *axyU = AXYlattice->get(iX,iY,iZ).getExternal(aUoffset);
          T *axzU = AXZlattice->get(iX,iY,iZ).getExternal(aUoffset);
          T *ayyU = AYYlattice->get(iX,iY,iZ).getExternal(aUoffset);
          T *ayzU = AYZlattice->get(iX,iY,iZ).getExternal(aUoffset);
          T *azzU = AZZlattice->get(iX,iY,iZ).getExternal(aUoffset);
          for (int iD = 0; iD < L::d; ++iD) {
            axxU[iD] *= uXX[iD];
            axyU[iD] *= uXX[iD];
            axzU[iD] *= uXX[iD];
            ayyU[iD] *= uXX[iD];
            ayzU[iD] *= uXX[iD];
            azzU[iD] *= uXX[iD];
          }

          T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
          A[xx] = AXXlattice->get(iX,iY,iZ).computeRho();
          A[xy] = AXYlattice->get(iX,iY,iZ).computeRho();
          A[xz] = AXZlattice->get(iX,iY,iZ).computeRho();
          A[yy] = AYYlattice->get(iX,iY,iZ).computeRho();
          A[yz] = AYZlattice->get(iX,iY,iZ).computeRho();
          A[zz] = AZZlattice->get(iX,iY,iZ).computeRho();

          T *tau = blockLattice.get(iX,iY,iZ).getExternal(tauOffset);
          fromAtoTau(A, tau);

          T gradU[L::d][L::d], rho, u[L::d], pi[util::TensorVal<Lattice<T> >::n];
          blockLattice.get(iX,iY,iZ).computeAllMomenta(rho,u,pi);
          T *uXY = AXYlattice->get(iX,iY,iZ).getExternal(velOffset);
          T *uYY = AYYlattice->get(iX,iY,iZ).getExternal(velOffset);

          for (int iD = 0; iD < L::d; ++iD) {
            uXX[iD] = u[iD];
            uXY[iD] = u[iD];
            uYY[iD] = u[iD];
          }

          //          T piToS = -blockLattice.getDynamics(iX,iY,iZ)->getOmega() *
          //              L::invCs2()/((T)2*rho);
          //          gradU[x][x] = piToS*(pi[xx] + tau[xx]);
          //          gradU[y][y] = piToS*(pi[yy] + tau[yy]);

          // computation of the gradients of u
          // the non diagonal part must be
          // computed using finite difference
          // the diagonal using f_fneq.
          T u_m1[L::d],u_p1[L::d];
          blockLattice.get(iX-1,iY,iZ).computeU(u_m1);
          blockLattice.get(iX+1,iY,iZ).computeU(u_p1);

          gradU[x][x] = (u_p1[x]-u_m1[x])/(T)2;
          gradU[x][y] = (u_p1[y]-u_m1[y])/(T)2;
          gradU[x][z] = (u_p1[z]-u_m1[z])/(T)2;

          blockLattice.get(iX,iY-1,iZ).computeU(u_m1);
          blockLattice.get(iX,iY+1,iZ).computeU(u_p1);

          gradU[y][x] = (u_p1[x]-u_m1[x])/(T)2;
          gradU[y][y] = (u_p1[y]-u_m1[y])/(T)2;
          gradU[y][z] = (u_p1[z]-u_m1[z])/(T)2;

          blockLattice.get(iX,iY,iZ-1).computeU(u_m1);
          blockLattice.get(iX,iY,iZ+1).computeU(u_p1);

          gradU[z][x] = (u_p1[x]-u_m1[x])/(T)2;
          gradU[z][y] = (u_p1[y]-u_m1[y])/(T)2;
          gradU[z][z] = (u_p1[z]-u_m1[z])/(T)2;

          T source[util::TensorVal<Lattice<T> >::n];
          computeSourceTerm(source, tau, A, gradU);

          T *rhs = AXXlattice->get(iX,iY,iZ).getExternal(sourceOffset);
          rhs[0] = source[xx];
          rhs = AXYlattice->get(iX,iY,iZ).getExternal(sourceOffset);
          rhs[0] = source[xy];
          rhs = AXZlattice->get(iX,iY,iZ).getExternal(sourceOffset);
          rhs[0] = source[xz];
          rhs = AYYlattice->get(iX,iY,iZ).getExternal(sourceOffset);
          rhs[0] = source[yy];
          rhs = AYZlattice->get(iX,iY,iZ).getExternal(sourceOffset);
          rhs[0] = source[yz];
          rhs = AZZlattice->get(iX,iY,iZ).getExternal(sourceOffset);
          rhs[0] = source[zz];
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor3D<T,Lattice>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor3D<T,Lattice>::
fromAtoTau(T A[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n])
{
  viscoHelpers<T>::fromAtoTau3D(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesOldroydbCouplingPostProcessor3D<T,Lattice>::
computeSourceTerm(T source[util::TensorVal<Lattice<T> >::n], T tau[util::TensorVal<Lattice<T> >::n], T A[util::TensorVal<Lattice<T> >::n], T gradU[Lattice<T>::d][Lattice<T>::d])
{
  viscoHelpers<T>::computeSource3D(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
BulkNavierStokesOldroydbCouplingGenerator3D<T,Lattice>::
BulkNavierStokesOldroydbCouplingGenerator3D(int x0_, int x1_, int y0_, int y1_,
    int z0_, int z1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor3D<T,Lattice>* BulkNavierStokesOldroydbCouplingGenerator3D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject3D* > partners) const
{
  return new BulkNavierStokesOldroydbCouplingPostProcessor3D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1,this->z0,this->z1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator3D<T,Lattice>* BulkNavierStokesOldroydbCouplingGenerator3D<T,Lattice>::clone() const
{
  return new BulkNavierStokesOldroydbCouplingGenerator3D<T,Lattice>(*this);
}

}  // namespace olb

#endif

