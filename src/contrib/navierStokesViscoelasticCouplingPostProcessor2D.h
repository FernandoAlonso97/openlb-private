/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef NAVIER_STOKES_VISCOELASTIC_COUPLING_POST_PROCESSOR_2D_H
#define NAVIER_STOKES_VISCOELASTIC_COUPLING_POST_PROCESSOR_2D_H

#include "core/spatiallyExtendedObject2D.h"
#include "core/postProcessing.h"
#include "core/blockLattice2D.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include <cmath>


namespace olb {

/**
* Class for the coupling between a Navier-Stokes (NS) lattice and an
* Advection-Diffusion (AD) lattice.
*/

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
// This coupling must be necessarily be put on the Navier-Stokes lattice!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//======================================================================
// ========Bulk navier-stoke fene-p coupling ====================//
//======================================================================
template<typename T, template<typename U> class Lattice>
class BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T le2_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 1;
  }
  virtual int extent(int whichDirection) const
  {
    return 1;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

  void fromAtoTau(T A[3], T tau[3]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2]);
  // computes the source term for the FENE-P constitutive equation
private:
  int x0, x1, y0, y1;
  T lambda, le2, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice>
class BulkNavierStokesViscoelasticFENEPCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  BulkNavierStokesViscoelasticFENEPCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T le2_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, le2, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer
};

//======================================================================
// ========flat boundary navier-stoke fene-p coupling ====================//
//======================================================================
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T le2_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

private:
  template<int deriveDirection>
  void interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                            T velDeriv[Lattice<T>::d], int iX, int iY) const;
  void fromAtoTau(T A[3], T tau[3]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2]);
  // computes the source term for the FENE-P constitutive equation
private:
  int x0, x1, y0, y1;
  T lambda, le2, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T le2_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, le2, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer
};

//======================================================================
// ========corner boundary navier-stoke fene-p coupling ====================//
//======================================================================
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D :
  public LocalPostProcessor2D<T,Lattice> {
public:
  CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D(int x_, int y_,
      T lambda_, T le2_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);

  void fromAtoTau(T A[3], T tau[3]);  // converts the conformation tensor into tau
  void computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2]);
  // computes the source term for the FENE-P constitutive equation
private:
  int x, y;
  T lambda, le2, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D :
  public LatticeCouplingGenerator2D<T,Lattice> {
public:
  CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D(int x_, int y_,
      T lambda_, T le2_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, le2, muP; // relaxation time, max length of elongation,
  // dyn visc of polymer
};






}

#endif
