/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef NAVIER_STOKES_VISCOELASTIC_COUPLING_POST_PROCESSOR_2D_HH
#define NAVIER_STOKES_VISCOELASTIC_COUPLING_POST_PROCESSOR_2D_HH

#include "core/latticeDescriptors.h"
#include "contrib/viscoelasticLatticeDescriptors.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include "contrib/advectionDiffusionWithSourceLatticeDescriptors.h"
#include "contrib/navierStokesViscoelasticCouplingPostProcessor2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "core/finiteDifference2D.h"
#include "contrib/viscoFiniteDifference2D.h"
#include "contrib/viscoHelpers.h"
#include "contrib/advectionDiffusionWithSourceLbHelpers.h"


using namespace std;

namespace olb {

//=====================================================================================
//==============  BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice>
BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice>::
BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T le2_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), le2(le2_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };

  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    ScalarField2D<T> rho(blockLattice.getNx(),blockLattice.getNy());
    rho.construct();
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);
        blockLattice.get(iX,iY).computeRhoU(rho.get(iX,iY),uXX);
        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[3]; // the conformation tensor
        A[xx] = AXXlattice->get(iX,iY).computeRho();
        A[xy] = AXYlattice->get(iX,iY).computeRho();
        A[yy] = AYYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T *u_p1 = AXXlattice->get(iX+1,iY).getExternal(velOffset);
        T *u_m1 = AXXlattice->get(iX-1,iY).getExternal(velOffset);

        T gradU[2][2];
        gradU[x][y] = (u_p1[1]-u_m1[1])/(T)2;
        gradU[x][x] = (u_p1[0]-u_m1[0])/(T)2;

        u_p1 = AXXlattice->get(iX,iY+1).getExternal(velOffset);
        u_m1 = AXXlattice->get(iX,iY-1).getExternal(velOffset);
        gradU[y][x] = (u_p1[0]-u_m1[0])/(T)2;
        gradU[y][y] = (u_p1[1]-u_m1[1])/(T)2;

        T source[3];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];

      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda, le2);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
BulkNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice>::
BulkNavierStokesViscoelasticFENEPCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T le2_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), le2(le2_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* BulkNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new BulkNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1, lambda, le2, muP,partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* BulkNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice>::clone() const
{
  return new BulkNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice>(*this);
}


//=====================================================================================
//==============  FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>::
FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T le2_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), le2(le2_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    ScalarField2D<T> rho(blockLattice.getNx(),blockLattice.getNy());
    rho.construct();
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);
        blockLattice.get(iX,iY).computeRhoU(rho.get(iX,iY),uXX);
        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[3]; // the conformation tensor
        A[xx] = AXXlattice->get(iX,iY).computeRho();
        A[xy] = AXYlattice->get(iX,iY).computeRho();
        A[yy] = AYYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T dx_u[L::d], dy_u[L::d];
        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        T gradU[2][2];
        gradU[x][x] = dx_u[x];
        gradU[x][y] = dx_u[y];
        gradU[y][x] = dy_u[x];
        gradU[y][y] = dy_u[y];

        T source[3];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];

      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);

  //  fd::ViscoDirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  //      interpolateVector4th(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda, le2);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice,direction,orientation>::
FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T le2_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), le2(le2_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor2D<T,Lattice>* FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice,direction,orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new FlatBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,direction,orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, le2, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
LatticeCouplingGenerator2D<T,Lattice>* FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new FlatBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice,direction,orientation>(*this);
}

//=====================================================================================
//==============  CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T le2_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), le2(le2_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  T *uXX = AXXlattice->get(x,y).getExternal(velOffset);
  blockLattice.get(x,y).computeU(uXX);
  T *uXY = AXYlattice->get(x,y).getExternal(velOffset);
  T *uYY = AYYlattice->get(x,y).getExternal(velOffset);

  for (int iD = 0; iD < L::d; ++iD) {
    uXY[iD] = uXX[iD];
    uYY[iD] = uXX[iD];
  }

  T A[3]; // the conformation tensor
  A[xx] = AXXlattice->get(x,y).computeRho();
  A[xy] = AXYlattice->get(x,y).computeRho();
  A[yy] = AYYlattice->get(x,y).computeRho();

  T *tau = blockLattice.get(x,y).getExternal(tauOffset);
  fromAtoTau(A, tau);

  // computation of the gradients of u
  // the non diagonal part must be
  // computed using finite difference
  // the diagonal using f_fneq.
  T dx_u[L::d], dy_u[L::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::interpolateVector(dy_u, blockLattice, x,y);

  T gradU[2][2];
  gradU[xCoord][xCoord] = dx_u[xCoord];
  gradU[xCoord][yCoord] = dx_u[yCoord];
  gradU[yCoord][xCoord] = dy_u[xCoord];
  gradU[yCoord][yCoord] = dy_u[yCoord];

  T source[3];
  computeSourceTerm(source, tau, A, gradU);

  T *rhs = AXXlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xx];
  rhs = AXYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xy];
  rhs = AYYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[yy];
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda, le2);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D(int x_, int y_,
    T lambda_, T le2_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), le2(le2_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>* CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
generate (std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new
         CornerBoundaryNavierStokesViscoelasticFENEPCouplingPostProcessor2D
         <T,Lattice,xNormal,yNormal>(this->x0,this->y0, lambda, le2, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>* CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D
<T,Lattice,xNormal,yNormal>::clone() const
{
  return new
         CornerBoundaryNavierStokesViscoelasticFENEPCouplingGenerator2D
         <T,Lattice,xNormal,yNormal>(*this);
}


//=====================================================================================
//=====================================================================================
//==============  BulkNavierStokesViscoelasticOLDROYDBCouplingPostProcessor2D ===============
//=====================================================================================
//=====================================================================================

template<typename T, template<typename U> class Lattice>
BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice>::
BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[util::TensorVal<Lattice<T> >::n]; // the conformation tensor
        std::cout << AXXlattice->getDynamics(iX,iY)->getOmega() << std::endl;
        A[xx] = AXXlattice->get(iX,iY).computeRho();
        A[xy] = AXYlattice->get(iX,iY).computeRho();
        A[yy] = AYYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        T gradU[L::d][L::d], rho, u[L::d], pi[util::TensorVal<Lattice<T> >::n];
        blockLattice.get(iX,iY).computeAllMomenta(rho,u,pi);

        T piToS = -blockLattice.getDynamics(iX,iY)->getOmega() *
                  L::invCs2()/((T)2*rho);
        gradU[x][x] = piToS*(pi[xx] + tau[xx]);
        gradU[y][y] = piToS*(pi[yy] + tau[yy]);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T *u_p1 = AXXlattice->get(iX+1,iY).getExternal(velOffset);
        T *u_m1 = AXXlattice->get(iX-1,iY).getExternal(velOffset);

        gradU[x][y] = (u_p1[1]-u_m1[1])/(T)2;
        //        gradU[x][x] = (u_p1[0]-u_m1[0])/(T)2;

        u_p1 = AXXlattice->get(iX,iY+1).getExternal(velOffset);
        u_m1 = AXXlattice->get(iX,iY-1).getExternal(velOffset);
        gradU[y][x] = (u_p1[0]-u_m1[0])/(T)2;
        //        gradU[y][y] = (u_p1[1]-u_m1[1])/(T)2;

        T source[3];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice>
void BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
BulkNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice>::
BulkNavierStokesViscoelasticOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* BulkNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new BulkNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* BulkNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice>::clone() const
{
  return new BulkNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice>(*this);
}


//=====================================================================================
//==============  FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[3];
        A[xx] = AXXlattice->get(iX,iY).computeRho();
        A[yy] = AYYlattice->get(iX,iY).computeRho();
        A[xy] = AXYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T dx_u[L::d], dy_u[L::d];
        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        T gradU[2][2];
        gradU[x][x] = dx_u[x];
        gradU[x][y] = dx_u[y];
        gradU[y][x] = dy_u[x];
        gradU[y][y] = dy_u[y];

        T source[3];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
FlatBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor2D<T,Lattice>* FlatBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new FlatBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
LatticeCouplingGenerator2D<T,Lattice>* FlatBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new FlatBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>(*this);
}

//=====================================================================================
//==  FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D =======
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  using namespace util::tensorIndices2D;
  enum {x, y};

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  // computation of the missing f direction.
  std::vector<int> missingDiagonal = util::subIndexOutgoing<adL,direction,orientation>();
  int iOpp = util::opposite<adL>(missingDiagonal[0]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *uXX = AXXlattice->get(iX,iY).getExternal(velOffset);

        // we have to store the values of the A_{alpha beta}u_{gamma}
        // in order to regularize the collision and remove
        // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
        T *axxU = AXXlattice->get(iX,iY).getExternal(aUoffset);
        T *axyU = AXYlattice->get(iX,iY).getExternal(aUoffset);
        T *ayyU = AYYlattice->get(iX,iY).getExternal(aUoffset);
        for (int iD = 0; iD < L::d; ++iD) {
          axxU[iD] *= uXX[iD];
          axyU[iD] *= uXX[iD];
          ayyU[iD] *= uXX[iD];
        }

        T *uXY = AXYlattice->get(iX,iY).getExternal(velOffset);
        T *uYY = AYYlattice->get(iX,iY).getExternal(velOffset);

        for (int iD = 0; iD < L::d; ++iD) {
          uXY[iD] = uXX[iD];
          uYY[iD] = uXX[iD];
        }
      }
    }

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T A[3]; // the conformation tensor
        A[xx] = interpolateA(*AXXlattice,iX, iY, iOpp);
        A[xy] = interpolateA(*AXYlattice,iX, iY, iOpp);
        A[yy] = interpolateA(*AYYlattice,iX, iY, iOpp);

        //        std::cout << "(" << iX << ", " << iY << "), a_xx diff = " << AXXlattice->get(iX,iY).computeRho() - A[xx] << std::endl;

        AXXlattice->get(iX,iY).defineRho(A[xx]);
        AXYlattice->get(iX,iY).defineRho(A[xy]);
        AYYlattice->get(iX,iY).defineRho(A[yy]);

        //        A[xx] = AXXlattice->get(iX,iY).computeRho();
        //        A[yy] = AYYlattice->get(iX,iY).computeRho();
        //        A[xy] = AXYlattice->get(iX,iY).computeRho();

        T *tau = blockLattice.get(iX,iY).getExternal(tauOffset);
        fromAtoTau(A, tau);

        // computation of the gradients of u
        // the non diagonal part must be
        // computed using finite difference
        // the diagonal using f_fneq.
        T dx_u[L::d], dy_u[L::d];
        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        T gradU[2][2];
        gradU[x][x] = dx_u[x];
        gradU[x][y] = dx_u[y];
        gradU[y][x] = dy_u[x];
        gradU[y][y] = dy_u[y];

        T source[3];
        computeSourceTerm(source, tau, A, gradU);

        T *rhs = AXXlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xx];
        rhs = AXYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[xy];
        rhs = AYYlattice->get(iX,iY).getExternal(sourceOffset);
        rhs[0] = source[yy];
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::DirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}
template<typename T, template<typename U> class Lattice, int direction, int orientation>
T FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateA(BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &block,
             int iX, int iY, int iOpp)
{
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  CellView<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &cell = block.get(iX,iY);
  const T omega = block.getDynamics(iX, iY)->getOmega();
  const T A1 = block.get(iX+(direction==0 ? (-orientation):0),
                         iY+(direction==1 ? (-orientation):0) ).computeRho();
  const T A2 = block.get(iX+(direction==0 ? (-2*orientation):0),
                         iY+(direction==1 ? (-2*orientation):0) ).computeRho();

  T *u = cell.getExternal(adL::ExternalField::velocityBeginsAt);
  T c_u = T();
  for (int iD = 0; iD < adL::d; ++iD) {
    c_u += adL::c[iOpp][iD] * u[iD];
  }

  T A = ((T)4*A1 - A2 + (T)2*omega/adL::t[iOpp] * (cell[iOpp] + adL::t[iOpp])) /
        ((T)3+(T)2*omega*((T)1+adL::invCs2()*c_u));

  return A;
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction, int orientation>
FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::
FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
PostProcessor2D<T,Lattice>* FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,direction,orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
LatticeCouplingGenerator2D<T,Lattice>* FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>::clone() const
{
  return new FlatInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,direction,orientation>(*this);
}

//=====================================================================================
//==============  CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  T *uXX = AXXlattice->get(x,y).getExternal(velOffset);

  // we have to store the values of the A_{alpha beta}u_{gamma}
  // in order to regularize the collision and remove
  // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
  T *axxU = AXXlattice->get(x,y).getExternal(aUoffset);
  T *axyU = AXYlattice->get(x,y).getExternal(aUoffset);
  T *ayyU = AYYlattice->get(x,y).getExternal(aUoffset);
  for (int iD = 0; iD < L::d; ++iD) {
    axxU[iD] *= uXX[iD];
    axyU[iD] *= uXX[iD];
    ayyU[iD] *= uXX[iD];
  }

  blockLattice.get(x,y).computeU(uXX);
  T *uXY = AXYlattice->get(x,y).getExternal(velOffset);
  T *uYY = AYYlattice->get(x,y).getExternal(velOffset);

  for (int iD = 0; iD < L::d; ++iD) {
    uXY[iD] = uXX[iD];
    uYY[iD] = uXX[iD];
  }

  T A[3]; // the conformation tensor
  A[xx] = AXXlattice->get(x,y).computeRho();
  A[xy] = AXYlattice->get(x,y).computeRho();
  A[yy] = AYYlattice->get(x,y).computeRho();

  T *tau = blockLattice.get(x,y).getExternal(tauOffset);
  fromAtoTau(A, tau);

  // computation of the gradients of u
  // the non diagonal part must be
  // computed using finite difference
  // the diagonal using f_fneq.
  T dx_u[L::d], dy_u[L::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateVector(dy_u, blockLattice, x,y);

  T gradU[2][2];
  gradU[xCoord][xCoord] = dx_u[xCoord];
  gradU[xCoord][yCoord] = dx_u[yCoord];
  gradU[yCoord][xCoord] = dy_u[xCoord];
  gradU[yCoord][yCoord] = dy_u[yCoord];

  T source[3];
  computeSourceTerm(source, tau, A, gradU);

  T *rhs = AXXlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xx];
  rhs = AXYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xy];
  rhs = AYYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[yy];
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
CornerBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D(int x_, int y_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>* CornerBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
generate (std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new
         CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D
         <T,Lattice,xNormal,yNormal>(this->x0,this->y0, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>* CornerBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D
<T,Lattice,xNormal,yNormal>::clone() const
{
  return new
         CornerBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D
         <T,Lattice,xNormal,yNormal>(*this);
}

//=====================================================================================
//==============  CornerBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), muP(muP_), partners(partners_)
{  }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  enum {
    velOffset = adL::ExternalField::velocityBeginsAt,
    sourceOffset = adL::ExternalField::scalarBeginsAt,
    tauOffset = Lattice<T>::ExternalField::tensorBeginsAt,
    aUoffset = adL::ExternalField::vector_t1BeginsAt
  };
  enum {xCoord, yCoord};
  using namespace util::tensorIndices2D;

  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXXlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[0]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AXYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[1]);
  BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *AYYlattice =
    dynamic_cast<BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> *>(partners[2]);

  T *uXX = AXXlattice->get(x,y).getExternal(velOffset);

  // we have to store the values of the A_{alpha beta}u_{gamma}
  // in order to regularize the collision and remove
  // the d_t d_gamma (A_{alpha beta} u_{gamma}) term
  T *axxU = AXXlattice->get(x,y).getExternal(aUoffset);
  T *axyU = AXYlattice->get(x,y).getExternal(aUoffset);
  T *ayyU = AYYlattice->get(x,y).getExternal(aUoffset);
  for (int iD = 0; iD < L::d; ++iD) {
    axxU[iD] *= uXX[iD];
    axyU[iD] *= uXX[iD];
    ayyU[iD] *= uXX[iD];
  }

  blockLattice.get(x,y).computeU(uXX);
  T *uXY = AXYlattice->get(x,y).getExternal(velOffset);
  T *uYY = AYYlattice->get(x,y).getExternal(velOffset);

  for (int iD = 0; iD < L::d; ++iD) {
    uXY[iD] = uXX[iD];
    uYY[iD] = uXX[iD];
  }

  T A[3]; // the conformation tensor
  A[xx] = interpolateA(*AXXlattice);
  A[xy] = interpolateA(*AXYlattice);
  A[yy] = interpolateA(*AYYlattice);

  //  std::cout << "(" << x << ", " << y << "), a_xx diff = " << AXXlattice->get(x,y).computeRho() /*- A[xx] */<< std::endl;
  //  std::cout << "(" << x << ", " << y << "), a_xy diff = " << AXYlattice->get(x,y).computeRho() /*- A[xy]*/ << std::endl;
  //  std::cout << "(" << x << ", " << y << "), a_yy diff = " << AYYlattice->get(x,y).computeRho() /*- A[yy]*/ << std::endl;

  AXXlattice->get(x,y).defineRho(A[xx]);
  AYYlattice->get(x,y).defineRho(A[xy]);
  AYYlattice->get(x,y).defineRho(A[yy]);

  T *tau = blockLattice.get(x,y).getExternal(tauOffset);
  fromAtoTau(A, tau);

  // computation of the gradients of u
  // the non diagonal part must be
  // computed using finite difference
  // the diagonal using f_fneq.
  T dx_u[L::d], dy_u[L::d];
  fd::DirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateVector(dx_u, blockLattice, x,y);
  fd::DirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateVector(dy_u, blockLattice, x,y);

  T gradU[2][2];
  gradU[xCoord][xCoord] = dx_u[xCoord];
  gradU[xCoord][yCoord] = dx_u[yCoord];
  gradU[yCoord][xCoord] = dy_u[xCoord];
  gradU[yCoord][yCoord] = dy_u[yCoord];

  T source[3];
  computeSourceTerm(source, tau, A, gradU);

  T *rhs = AXXlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xx];
  rhs = AXYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[xy];
  rhs = AYYlattice->get(x,y).getExternal(sourceOffset);
  rhs[0] = source[yy];
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
fromAtoTau(T A[3], T tau[3])
{
  viscoHelpers<T>::fromAtoTau(A,tau,muP, lambda);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
computeSourceTerm(T source[3], T tau[3], T A[3], T gradU[2][2])
{
  viscoHelpers<T>::computeSource(source, tau, A, gradU, muP);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
T CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D<T,Lattice,xNormal,yNormal>::
interpolateA(BlockLattice2D<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &block)
{
  typedef AdvectionDiffusionWithSourceD2Q5Descriptor<T> adL;
  std::vector<int> unknownIndexes = utilAdvDiff::subIndexOutgoing2DonCorners<adL,xNormal,yNormal>();

  CellView<T,AdvectionDiffusionWithSourceD2Q5Descriptor> &cell = block.get(x,y);
  const T omega = block.getDynamics(x, y)->getOmega();

  std::vector<T> A1, A2;
  A1.push_back(block.get(x - xNormal,  y).computeRho());
  A2.push_back(block.get(x - 2*xNormal,y).computeRho());

  A1.push_back(block.get(x, y - yNormal).computeRho());
  A2.push_back(block.get(x, y - 2*yNormal).computeRho());

  T *u = cell.getExternal(adL::ExternalField::velocityBeginsAt);

  std::vector<T> aTemp, orient;
  orient.push_back(xNormal);
  orient.push_back(yNormal);
  //  std::cout << xNormal << ", " << yNormal << ", ";
  for (unsigned iPop = 0; iPop < unknownIndexes.size(); ++iPop) {
    int iOpp = util::opposite<adL>(unknownIndexes[iPop]);
    //    std::cout << iOpp << ", ";
    T c_u = T();
    for (int iD = 0; iD < adL::d; ++iD) {
      c_u += adL::c[iOpp][iD] * u[iD];
    }

    aTemp.push_back(((T)4*A1[iPop] - A2[iPop] + (T)2*omega/adL::t[iOpp] * (cell[iOpp] + adL::t[iOpp])) /
                    ((T)3 + (T)2*omega*((T)1+adL::invCs2()*c_u)));
  }
  //   std::cout << std::endl;

  T A = T();
  for (unsigned iPop = 0; iPop < aTemp.size(); ++iPop) {
    A += aTemp[iPop];
    //    std::cout << aTemp[iPop] << ", ";
  }
  //  std::cout << std::endl;
  A /= (T)aTemp.size();

  return A;
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D(int x_, int y_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>* CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D<T,Lattice,xNormal,yNormal>::
generate (std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new
         CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingPostProcessor2D
         <T,Lattice,xNormal,yNormal>(this->x0,this->y0, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>* CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D
<T,Lattice,xNormal,yNormal>::clone() const
{
  return new
         CornerInterpBoundaryNavierStokesViscoelasticOldroydbCouplingGenerator2D
         <T,Lattice,xNormal,yNormal>(*this);
}

}  // namespace olb

#endif

//  T d_AXX[2];
//  fd::DirectedGradients2D<T, AdvectionDiffusionWithSourceD2Q5Descriptor, 0, xNormal, true>::
//      interpolateScalar(d_AXX[0], *AXXlattice, x,y);
//  fd::DirectedGradients2D<T, AdvectionDiffusionWithSourceD2Q5Descriptor, 1, yNormal, true>::
//      interpolateScalar(d_AXX[1], *AXXlattice, x,y);
//
//  T d_AXY[2];
//  fd::DirectedGradients2D<T, AdvectionDiffusionWithSourceD2Q5Descriptor, 0, xNormal, true>::
//      interpolateScalar(d_AXY[0], *AXYlattice, x,y);
//  fd::DirectedGradients2D<T, AdvectionDiffusionWithSourceD2Q5Descriptor, 1, yNormal, true>::
//      interpolateScalar(d_AXY[1], *AXYlattice, x,y);
//
//  T d_AYY[2];
//  fd::DirectedGradients2D<T, AdvectionDiffusionWithSourceD2Q5Descriptor, 0, xNormal, true>::
//      interpolateScalar(d_AYY[0], *AYYlattice, x,y);
//  fd::DirectedGradients2D<T, AdvectionDiffusionWithSourceD2Q5Descriptor, 1, yNormal, true>::
//      interpolateScalar(d_AYY[1], *AYYlattice, x,y);
//
//  T omegaA = AXXlattice->getDynamics(x,y)->getOmega();
//
//  T fNeqXX[adL::q], fNeqXY[adL::q], fNeqYY[adL::q];
//  for (int iPop = 0; iPop < adL::q; ++iPop)
//  {
//    fNeqXX[iPop] = T();
//    fNeqXY[iPop] = T();
//    fNeqYY[iPop] = T();
//    T fact = -adL::t[iPop] / omegaA;
//    for (int iA = 0; iA < adL::d; ++iA)
//    {
//      fNeqXX[iPop] += adL::c(iPop)[iA] * d_AXX[iA];
//      fNeqXY[iPop] += adL::c(iPop)[iA] * d_AXY[iA];
//      fNeqYY[iPop] += adL::c(iPop)[iA] * d_AYY[iA];
//
//      for (int iB = 0; iB < adL::d; ++iB)
//      {
//        fNeqXX[iPop] += adL::invCs2() *
//            adL::c(iPop)[iA]*adL::c(iPop)[iB] *
//            (d_AXX[iA]*uXX[iB] + A[xx] * gradU[iA][iB]);
//        fNeqXY[iPop] += adL::invCs2() *
//            adL::c(iPop)[iA]*adL::c(iPop)[iB] *
//            (d_AXY[iA]*uXY[iB] + A[xy] * gradU[iA][iB]);
//        fNeqYY[iPop] += adL::invCs2() *
//            adL::c(iPop)[iA]*adL::c(iPop)[iB] *
//            (d_AYY[iA]*uYY[iB] + A[yy] * gradU[iA][iB]);
//        if (iA == iB)
//        {
//          fNeqXX[iPop] -= (d_AXX[iA]*uXX[iB] + A[xx] * gradU[iA][iB]);
//          fNeqXY[iPop] -= (d_AXY[iA]*uXY[iB] + A[xy] * gradU[iA][iB]);
//          fNeqYY[iPop] -= (d_AYY[iA]*uYY[iB] + A[yy] * gradU[iA][iB]);
//        }
//      }
//    }
//    fNeqXX[iPop] *= fact;
//    fNeqXY[iPop] *= fact;
//    fNeqYY[iPop] *= fact;
//  }
//
//  for (int iPop = 0; iPop < adL::q; ++iPop)
//  {
//    AXXlattice->get(x,y)[iPop] = advectionDiffusionWithSourceLbHelpers<T,Lattice>::
//        equilibrium(iPop, A[xx], uXX) + fNeqXX[iPop];
//    AXYlattice->get(x,y)[iPop] = advectionDiffusionWithSourceLbHelpers<T,Lattice>::
//        equilibrium(iPop, A[xy], uXY) +  fNeqXY[iPop];
//    AYYlattice->get(x,y)[iPop] = advectionDiffusionWithSourceLbHelpers<T,Lattice>::
//        equilibrium(iPop, A[yy], uYY) +  fNeqYY[iPop];
//  }
