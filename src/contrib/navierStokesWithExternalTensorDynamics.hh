/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- generic implementation.
 */
#ifndef NAVIER_STOKES_WITH_EXTERNAL_TENSOR_DYNAMICS_HH
#define NAVIER_STOKES_WITH_EXTERNAL_TENSOR_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "navierStokesWithExternalTensorDynamics.h"
#include "dynamics/lbHelpers.h"
#include "contrib/tensorLbHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"

namespace olb {


//////////////// Class AdvectionDiffusionWithExternalTensorBGKdynamics ////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */

//==================================================================//
//============= BGK Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class Lattice>
NavierStokesWithExternalTensorBGKdynamics<T,Lattice>::NavierStokesWithExternalTensorBGKdynamics (
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
NavierStokesWithExternalTensorBGKdynamics<T,Lattice>* NavierStokesWithExternalTensorBGKdynamics<T,Lattice>::clone() const
{
  return new NavierStokesWithExternalTensorBGKdynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T NavierStokesWithExternalTensorBGKdynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}


template<typename T, template<typename U> class Lattice>
void NavierStokesWithExternalTensorBGKdynamics<T,Lattice>::collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;

  T rho, u[Lattice<T>::d];
  this->momenta.computeRhoU(cell, rho, u);
  T uSqr = lbHelpers<T,Lattice>::
           bgkCollision(cell, rho, u, omega);

  T *tau = cell.getExternal(L::ExternalField::tensorBeginsAt);

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    int iTau = 0;
    T Qtau = T();
    for (int iA = 0; iA < Lattice<T>::d; ++iA) {
      for (int iB = iA; iB < Lattice<T>::d; ++iB) {
        Qtau += L::c(iPop)[iA]*L::c(iPop)[iB] * tau[iTau];
        if (iA == iB) {
          Qtau -= tau[iTau]/L::invCs2();
        }

        ++iTau;
      }
    }
    cell[iPop] -= L::t[iPop] * getOmega() * Qtau * L::invCs2() * L::invCs2() /(T)2;
  }

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice>
T NavierStokesWithExternalTensorBGKdynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void NavierStokesWithExternalTensorBGKdynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}

//////////////// Class AdvectionDiffusionWithExternalTensorRLBdynamics ////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */

//==================================================================//
//============= BGK Model for Advection diffusion===========//
//==================================================================//

template<typename T, template<typename U> class Lattice>
NavierStokesWithExternalTensorRLBdynamics<T,Lattice>::NavierStokesWithExternalTensorRLBdynamics (
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
NavierStokesWithExternalTensorRLBdynamics<T,Lattice>* NavierStokesWithExternalTensorRLBdynamics<T,Lattice>::clone() const
{
  return new NavierStokesWithExternalTensorRLBdynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T NavierStokesWithExternalTensorRLBdynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}


template<typename T, template<typename U> class Lattice>
void NavierStokesWithExternalTensorRLBdynamics<T,Lattice>::collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;

  T rho, u[Lattice<T>::d], pi[util::TensorVal<Lattice<T> >::n];
  this->momenta.computeAllMomenta(cell, rho, u, pi);
  T uSqr = rlbHelpers<T,Lattice>::
           rlbCollision(cell, rho, u, pi, omega);

  tensorLbHelpers<T,Lattice>::addExternalTensor(cell,omega);

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice>
T NavierStokesWithExternalTensorRLBdynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void NavierStokesWithExternalTensorRLBdynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}


} // namespace olb

#endif
