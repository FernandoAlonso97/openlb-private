/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- generic implementation.
 */
#ifndef OPT_ENTROPIC_LB_DYNAMICS_HH
#define OPT_ENTROPIC_LB_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "dynamics/lbHelpers.h"
#include "optEntropicDynamics.h"
#include "dynamics/entropicLbHelpers.h"
#include "entropicLatticeDescriptors.h"

namespace olb {

//==============================================================================//
/////////////////////////// Class OptEntropicDynamics ///////////////////////////////
//==============================================================================//
/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
OptEntropicDynamics<T,Lattice,Momenta>::OptEntropicDynamics (
  T omega_, Momenta& momenta_ )
  : BasicDynamics<T,Lattice,Momenta,OptEntropicDynamics<T, Lattice, Momenta>>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice, class Momenta>
OptEntropicDynamics<T,Lattice,Momenta>* OptEntropicDynamics<T,Lattice,Momenta>::clone() const
{
  return new OptEntropicDynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T OptEntropicDynamics<T,Lattice,Momenta>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return entropicLbHelpers<T,Lattice>::equilibrium(iPop,rho,u);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void OptEntropicDynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;
  typedef entropicLbHelpers<T,Lattice> eLbH;

  T rho, u[Lattice<T>::d];
  this->momenta.computeRhoU(cell, rho, u);
  T uSqr = util::normSqr<T,L::d>(u);

  T f[L::q], fEq[L::q], fNeq[L::q];
  for (int iPop = 0; iPop < L::q; ++iPop) {
    fEq[iPop]  = eLbH::equilibrium(iPop,rho,u);
    fNeq[iPop] = cell[iPop] - fEq[iPop];
    f[iPop]    = cell[iPop] + L::t[iPop];
    fEq[iPop] += L::t[iPop];
  }
  //==============================================================================//
  //============= Evaluation of alpha using a Newton Raphson algorithm ===========//
  //==============================================================================//

  T *alpha = cell.getExternal(Lattice<T>::ExternalField::scalarBeginsAt);
  bool converged = getAlpha(alpha[0],f,fNeq);
  if (!converged) {
    std::cout << "Newton-Raphson failed to converge.\n";
    exit(1);
  }

  OLB_ASSERT(converged,"Entropy growth failed to converge!");

  T omegaTot = omega / 2.0 * alpha[0];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] *= (T)1-omegaTot;
    cell[iPop] += omegaTot * (fEq[iPop]-L::t[iPop]);
  }

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T OptEntropicDynamics<T,Lattice,Momenta>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void OptEntropicDynamics<T,Lattice,Momenta>::setOmega(T omega_)
{
  omega = omega_;
}

template<typename T, template<typename U> class Lattice, class Momenta>
T OptEntropicDynamics<T,Lattice,Momenta>::computeEntropy(const T f[])
{
  typedef Lattice<T> L;
  T entropy = T();
  for (int iPop = 0; iPop < L::q; ++iPop) {
    OLB_ASSERT(f[iPop] > T(), "f[iPop] <= 0");
    entropy += f[iPop]*log(f[iPop]/L::t[iPop]);
  }

  return entropy;
}

template<typename T, template<typename U> class Lattice, class Momenta>
T OptEntropicDynamics<T,Lattice,Momenta>::computeEntropyGrowth(const T f[], const T fNeq[], const T &alpha)
{
  typedef Lattice<T> L;

  T fAlphaFneq[L::q];
  for (int iPop = 0; iPop < L::q; ++iPop) {
    fAlphaFneq[iPop] = f[iPop] - alpha*fNeq[iPop];
  }

  return computeEntropy(f) - computeEntropy(fAlphaFneq);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T OptEntropicDynamics<T,Lattice,Momenta>::computeEntropyGrowthDerivative(const T f[], const T fNeq[], const T &alpha)
{
  typedef Lattice<T> L;

  T entropyGrowthDerivative = T();
  for (int iPop = 0; iPop < L::q; ++iPop) {
    T tmp = f[iPop] - alpha*fNeq[iPop];
    OLB_ASSERT(tmp > T(), "f[iPop] - alpha*fNeq[iPop] <= 0");
    entropyGrowthDerivative += fNeq[iPop]*(log(tmp/L::t[iPop]));
  }

  return entropyGrowthDerivative;
}

template<typename T, template<typename U> class Lattice, class Momenta>
bool OptEntropicDynamics<T,Lattice,Momenta>::getAlpha(T &alpha, const T f[], const T fNeq[])
{
  const T epsilon = std::numeric_limits<T>::epsilon();

  T alphaGuess = T();
  const T var = 100.0;
  const T errorMax = epsilon*var;
  T error = 1.0;
  int count = 0;
  for (count = 0; count < 10000; ++count) {
    T entGrowth = computeEntropyGrowth(f,fNeq,alpha);
    T entGrowthDerivative = computeEntropyGrowthDerivative(f,fNeq,alpha);
    if ((error < errorMax) || (fabs(entGrowth) < var*epsilon)) {
      return true;
    }
    alphaGuess = alpha - entGrowth /
                 entGrowthDerivative;
    error = fabs(alpha-alphaGuess);
    alpha = alphaGuess;
  }
  return false;
}

//====================================================================//
//////////////////// Class ForcedOptEntropicDynamics //////////////////////
//====================================================================//

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 */
template<typename T, template<typename U> class Lattice, class Momenta>
ForcedOptEntropicDynamics<T,Lattice,Momenta>::ForcedOptEntropicDynamics (
  T omega_, Momenta& momenta_ )
  : BasicDynamics<T,Lattice,Momenta, ForcedOptEntropicDynamics<T, Lattice, Momenta>>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice, class Momenta>
ForcedOptEntropicDynamics<T,Lattice,Momenta>* ForcedOptEntropicDynamics<T,Lattice,Momenta>::clone() const
{
  return new ForcedOptEntropicDynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T ForcedOptEntropicDynamics<T,Lattice,Momenta>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return entropicLbHelpers<T,Lattice>::equilibrium(iPop,rho,u);
}


template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedOptEntropicDynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;
  typedef entropicLbHelpers<T,Lattice> eLbH;

  T rho, u[Lattice<T>::d];
  this->momenta.computeRhoU(cell, rho, u);
  T uSqr = util::normSqr<T,L::d>(u);

  T f[L::q], fEq[L::q], fNeq[L::q];
  for (int iPop = 0; iPop < L::q; ++iPop) {
    fEq[iPop]  = eLbH::equilibrium(iPop,rho,u);
    fNeq[iPop] = cell[iPop] - fEq[iPop];
    f[iPop]    = cell[iPop] + L::t[iPop];
    fEq[iPop] += L::t[iPop];
  }
  //==============================================================================//
  //============= Evaluation of alpha using a Newton Raphson algorithm ===========//
  //==============================================================================//

  T *alpha = cell.getExternal(Lattice<T>::ExternalField::scalarBeginsAt);
  bool converged = getAlpha(alpha[0],f,fNeq);
  if (!converged) {
    std::cout << "Newton-Raphson failed to converge.\n";
    exit(1);
  }

  OLB_ASSERT(converged,"Entropy growth failed to converge!");

  T* force = cell.getExternal(forceBeginsAt);
  for (int iDim=0; iDim<Lattice<T>::d; ++iDim) {
    u[iDim] += force[iDim] / (T)2.;
  }
  uSqr = util::normSqr<T,L::d>(u);
  T omegaTot = omega / 2.0 * alpha[0];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] *= (T)1-omegaTot;
    cell[iPop] += omegaTot * eLbH::equilibrium(iPop,rho,u);
  }
  lbHelpers<T,Lattice>::addExternalForce(cell, u, omegaTot);

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T ForcedOptEntropicDynamics<T,Lattice,Momenta>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedOptEntropicDynamics<T,Lattice,Momenta>::setOmega(T omega_)
{
  omega = omega_;
}

template<typename T, template<typename U> class Lattice, class Momenta>
T ForcedOptEntropicDynamics<T,Lattice,Momenta>::computeEntropy(const T f[])
{
  typedef Lattice<T> L;
  T entropy = T();
  for (int iPop = 0; iPop < L::q; ++iPop) {
    OLB_ASSERT(f[iPop] > T(), "f[iPop] <= 0");
    entropy += f[iPop]*log(f[iPop]/L::t[iPop]);
  }

  return entropy;
}

template<typename T, template<typename U> class Lattice, class Momenta>
T ForcedOptEntropicDynamics<T,Lattice,Momenta>::computeEntropyGrowth(const T f[], const T fNeq[], const T &alpha)
{
  typedef Lattice<T> L;

  T fAlphaFneq[L::q];
  for (int iPop = 0; iPop < L::q; ++iPop) {
    fAlphaFneq[iPop] = f[iPop] - alpha*fNeq[iPop];
  }

  return computeEntropy(f) - computeEntropy(fAlphaFneq);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T ForcedOptEntropicDynamics<T,Lattice,Momenta>::computeEntropyGrowthDerivative(const T f[], const T fNeq[], const T &alpha)
{
  typedef Lattice<T> L;

  T entropyGrowthDerivative = T();
  for (int iPop = 0; iPop < L::q; ++iPop) {
    T tmp = f[iPop] - alpha*fNeq[iPop];
    OLB_ASSERT(tmp > T(), "f[iPop] - alpha*fNeq[iPop] <= 0");
    entropyGrowthDerivative += fNeq[iPop]*log(tmp/L::t[iPop]);
  }

  return entropyGrowthDerivative;
}

template<typename T, template<typename U> class Lattice, class Momenta>
bool ForcedOptEntropicDynamics<T,Lattice,Momenta>::getAlpha(T &alpha, const T f[], const T fNeq[])
{
  const T epsilon = std::numeric_limits<T>::epsilon();

  T alphaGuess = T();
  const T var = 100.0;
  const T errorMax = epsilon*var;
  T error = 1.0;
  int count = 0;
  for (count = 0; count < 10000; ++count) {
    //         if (count != 0)
    //             std::cout << count << "\n";
    T entGrowth = computeEntropyGrowth(f,fNeq,alpha);
    T entGrowthDerivative = computeEntropyGrowthDerivative(f,fNeq,alpha);
    if ((error < errorMax) || (fabs(entGrowth) < var*epsilon)) {
      return true;
    }
    alphaGuess = alpha - entGrowth /
                 entGrowthDerivative;
    error = fabs(alpha-alphaGuess);
    alpha = alphaGuess;
  }
  return false;
}

}

#endif

