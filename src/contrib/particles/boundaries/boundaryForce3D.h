/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef BOUNDARYFORCE3D_H_
#define BOUNDARYFORCE3D_H_

#include <math.h>
#include "../sortAlgorithms/nanoflann_adaptor.hpp"
#include "../mesh/mesh.cpp"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class BoundaryForce3D : public Boundary3D<T, PARTICLETYPE> {
 public:
  BoundaryForce3D(STLreader<T>& stlReader, T latticeL, T dT);
  BoundaryForce3D(BoundaryForce3D<T, PARTICLETYPE>& f);
  virtual ~BoundaryForce3D() {
  }
  virtual void applyBoundary(
      typename std::deque<PARTICLETYPE<T> >::iterator& p,
      ParticleSystem3D<T, PARTICLETYPE>& psSys);
 private:
  bool testIntersection(const STLTriangle&, PARTICLETYPE<T>&, std::vector<T>&,
                        T&);
  Octree<T> *_tree;
  T _dT;
  std::vector<T> _oldPos;
  std::vector<T> _line;
  std::vector<unsigned int> locTris;
  std::set<unsigned int> allTris;
  T _latticeL;
};

template<typename T, template<typename U> class PARTICLETYPE>
BoundaryForce3D<T, PARTICLETYPE>::BoundaryForce3D(STLreader<T>& stlReader, T latticeL, T dT)
    : Boundary3D<T, PARTICLETYPE>(),
      _latticeL(latticeL), _dT(dT), _oldPos(3, T()), _line(3, T()) {
  _tree = stlReader.getTree();
}

template<typename T, template<typename U> class PARTICLETYPE>
void BoundaryForce3D<T, PARTICLETYPE>::applyBoundary(
    typename std::deque<PARTICLETYPE<T> >::iterator& p,
    ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  _line[0] = _dT * p->getVel()[0];
  _line[1] = _dT * p->getVel()[1];
  _line[2] = _dT * p->getVel()[2];

  _oldPos[0] = p->getPos()[0] - _line[0];
  _oldPos[1] = p->getPos()[1] - _line[1];
  _oldPos[2] = p->getPos()[2] - _line[2];

  std::vector<T> minPos(3, T()), maxPos(3, T());
  minPos[0] = std::min(_oldPos[0], p->getPos()[0]);
  maxPos[0] = std::max(_oldPos[0], p->getPos()[0]) + _latticeL;

  minPos[1] = std::min(_oldPos[1], p->getPos()[1]);
  maxPos[1] = std::max(_oldPos[1], p->getPos()[1]) + _latticeL;

  minPos[2] = std::min(_oldPos[2], p->getPos()[2]);
  maxPos[2] = std::max(_oldPos[2], p->getPos()[2]) + _latticeL;

//  std::vector<unsigned int> locTris;
//  std::set<unsigned int> allTris;
  while (minPos[0] < maxPos[0]) {
    while (minPos[1] < maxPos[1]) {
      while (minPos[2] < maxPos[2]) {
        locTris = _tree->find(minPos)->getTriangles();
        allTris.insert(locTris.begin(), locTris.end());
        minPos[2] +=_latticeL;
      }
      minPos[1] +=_latticeL;
    }
    minPos[0] +=_latticeL;
  }


  Vector<T, 3> q;
  T a = std::numeric_limits<T>::infinity();
  if (_tree->getMesh()->testRayIntersect(allTris, _oldPos, _line, q ,a)) {
    p->getPos()[0] = q[0];
    p->getPos()[1] = q[1];
    p->getPos()[2] = q[2];
    p->setActive(false);
  }

  locTris.clear();
  allTris.clear();

/*  if (p->getActive()) {
    std::vector<T> line(3, T()), q(3, T());
    std::vector<T> oldPos(3, T());
//    std::vector<T> pos = p->getPos();

      line[0] = -_dT * p->getVel()[0];
      line[1] = -_dT * p->getVel()[1];
      line[2] = -_dT * p->getVel()[2];

      oldPos[0] = p->getPos()[0] + line[0];
      oldPos[1] = p->getPos()[1] + line[1];
      oldPos[2] = p->getPos()[2] + line[2];

     T lineNorm2 = line[0] * line[0] + line[1] * line[1] + line[2] * line[2];

    std::vector<T> s(3, T());
    T a = std::numeric_limits<T>::infinity(), distance2 = 0;
    std::vector<T> pt = p->getPos();
    T step = 1. / 100. * _latticeL;

    STLtriangle<T> tri;
    Octree<T>* node = _tree->find(p->getPos());
    int it = 0;
    while (distance2 < lineNorm2 && it < 50) {
      it++;
      if (node->closestIntersection(p->getPos(), line, q, a) && a <= 1) {
        p->setActive(false);
        break;
      } else {
        node = _tree->find(pt);
        node->intersectRayNode(pt, line, s);
        pt[0] = s[0] + step * line[0];
        pt[1] = s[1] + step * line[1];
        pt[2] = s[2] + step * line[2];
        distance2 = std::pow(p->getPos()[0] - s[0], 2) + std::pow(p->getPos()[1] - s[1], 2)
            + std::pow(p->getPos()[2] - s[2], 2);
      }
    }
  }*/
}

}

#endif
