/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef BOUNDARYREFLECTION3D_H_
#define BOUNDARYREFLECTION3D_H_

#include <math.h>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class BoundaryReflection3D: public Boundary3D<T, PARTICLETYPE> {
public:
	BoundaryReflection3D(STLreader<T>& stlReader, T maxRadius, T dT);
	BoundaryReflection3D(BoundaryReflection3D<T, PARTICLETYPE>& f);
	virtual ~BoundaryReflection3D() {};
	virtual void applyBoundary(typename std::vector<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys);

private:
	bool intersectRaySphere(const std::vector<T>& pt, const typename std::vector<PARTICLETYPE<T> >::iterator& p, const std::vector<T>& oldPos, const std::vector<T>& pos, const std::vector<T>& dir, T& t);
	bool findIntersectionPoint(const typename std::vector<PARTICLETYPE<T> >::iterator& p, const std::vector<T>& oldPos, const std::vector<T>& pos, const STLtriangle<T>& tri, std::vector<T>& R, bool& qInside, T& alpha);
	bool computeReflection(std::vector<T>& oldPos, std::vector<T>& line, typename std::vector<PARTICLETYPE<T> >::iterator& p, std::vector<T>& q);
	Octree<T> *_tree;
	T _dT;
};

template<typename T, template<typename U> class PARTICLETYPE>
BoundaryReflection3D<T, PARTICLETYPE>::BoundaryReflection3D(STLreader<T>& stlReader, T maxRadius, T dT) :
		Boundary3D<T, PARTICLETYPE>(),
		_dT(dT) {
		_tree = stlReader.getTree();
}

template<typename T, template<typename U> class PARTICLETYPE>
void BoundaryReflection3D<T, PARTICLETYPE>::applyBoundary(typename std::vector<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys) {
	if(p->getActive()) {
//	  cout << "POS: " << p->getPos()[0] << " "<< p->getPos()[1] << " "<< p->getPos()[2] << " " << std::endl;
//	  cout << "VEL: " << p->getVel()[0] << " "<< p->getVel()[1] << " "<< p->getVel()[2] << " " << std::endl;
		std::vector<T> line = _dT * p->getVel(), q(3, T());
		std::vector<T> oldPos = p->getPos() - line;
		std::vector<T> pos = p->getPos();
		std::set<unsigned int> tris;
		bool qInside;
		bool qInsideFixed = false;
		bool foundQ = true;
		STLtriangle<T> triFixed;
		std::vector<T> qFixed(3, T());
		T alpha = T();
		int it = 0;

		while (foundQ && it < 100) {
	    T a = std::numeric_limits<T>::infinity();
	    _tree->trianglesOnLine(oldPos, pos, tris);
		  foundQ = false;
      for (auto& tri: tris) {
        STLtriangle<T> tria = _tree->getMesh()->getTri(tri);
        if (findIntersectionPoint(p, oldPos, pos, tria, q, qInside, alpha) && std::numeric_limits<T>::epsilon() < alpha && alpha < a) {
          a = alpha;
          qInsideFixed = qInside;
          qFixed = q;
          triFixed = tria;
        }
      }
//      cout << "A: " << a << std::endl;
      if (0.<a && a < 1.) {
        foundQ = true;
//        cout << "FoundQ: " << foundQ << std::endl;
//        cout << "Alpha: " << a << " qinside: " << qInsideFixed << " qFixed: " << qFixed[0] << " " << qFixed[1] << " " << qFixed[2] << std::endl;
        if (qInsideFixed) {
          T t = (triFixed.d - util::dotProduct3D(triFixed.normal,(p->getPos()+(p->getRad()*triFixed.normal)))); // /(util::dotProduct3D(tri.normal,tri.normal));
          p->setPos(p->getPos() + 2.*t*triFixed.normal);
          p->setVel(util::normalize(p->getPos() - qFixed + p->getRad()*triFixed.normal) * util::norm(p->getVel()));
        } else {
          T t = (triFixed.d - util::dotProduct3D(triFixed.normal,(p->getPos()+(p->getRad()*triFixed.normal)))); // /(util::dotProduct3D(tri.normal,tri.normal));
          p->setPos(p->getPos() + 2.*t*triFixed.normal);
          p->setVel(util::normalize(p->getPos() - qFixed + p->getRad()*triFixed.normal) * util::norm(p->getVel()));

        }
//        oldPos = q;
        pos = p->getPos();
//        cout << "NEWPOS: " << pos[0] << " "<< pos[1] << " "<< pos[2] << " " << std::endl;
      }
//      if (it>1) cout << "IT: " << it << std::endl;
      it++;
      if (it == 99) cout << "To many reflections" << std::endl;
		}
	}
}

template<typename T, template<typename U> class PARTICLETYPE>
bool BoundaryReflection3D<T, PARTICLETYPE>::findIntersectionPoint(const typename std::vector<PARTICLETYPE<T> >::iterator& p, const std::vector<T>& oldPos, const std::vector<T>& pos, const STLtriangle<T>& tri, std::vector<T>& q, bool& qInside, T& alpha) {
  T rn = 0.;
  qInside=true;
  std::vector<T> D = oldPos + p->getRad() * tri.normal;
  std::vector<T> dir = pos-oldPos;

//  T one = tri.d - util::dotProduct3D(D, tri.normal);
//  T two = tri.d - util::dotProduct3D(pos+ p->getRad() * tri.normal, tri.normal);
//  if(one*two < 0) cout << "SWAPPED SIDES! ==============================" << std::endl;
//  if(one*two > std::numeric_limits<T>::epsilon()) return false;

  for (int i = 0; i < 3; i++) {
    rn += dir[i]*tri.normal[i];
  }
  // Schnitttest Flugrichtung -> Ebene
  if (fabs(rn) < std::numeric_limits<T>::epsilon()) {
    return false;
  }
  alpha = tri.d;
  for (int i = 0; i < 3; i++) {
    alpha -= D[i]*tri.normal[i];
  }
  alpha /= rn;
//  cout << "Alpha: " << alpha << std::endl;
  // Abstand Partikel Ebene
  if (util::norm2(alpha*dir) > p->getRad()*p->getRad()) { // < -std::numeric_limits<T>::epsilon() || alpha > 1.) {
//    cout << "Und tschuess!" << std::endl;
    return false;
  }
  // Compute intersection Ray plane.
  for (int i = 0; i < 3; i++) {
    q[i] = D[i] + alpha*dir[i];
  }
  // Schnittpunkt q inside Triangle?
  double beta = tri.kBeta;
  for (int i = 0; i < 3; i++) {
    beta += tri.uBeta[i]*q[i];
  }
  if (beta < -10.*std::numeric_limits<T>::epsilon()) {
//    cout << "Beta: " << beta << std::endl;
    qInside =  false;
  }
  double gamma = tri.kGamma;
  for (int i = 0; i < 3; i++) {
    gamma += tri.uGamma[i]*q[i];
  }
  if (gamma < -10.*std::numeric_limits<T>::epsilon()) {
//    cout << "Gamma: " << beta << std::endl;
    qInside = false;
  }
  if (1. - beta - gamma < -20.*std::numeric_limits<T>::epsilon()) {
//    cout << "1. - beta - gamma: " << 1. - beta - gamma << std::endl;
    qInside = false;
  }
//  return qInside;
  if (qInside) {
//    cout << "RETURNING true;" << std::endl;
    return true;
  }
  q = tri.closestPtPointTriangle(q);
  return intersectRaySphere(q, p, oldPos, pos, dir, alpha);
}


/// Christer Ericson - Real Time Detection p177ff
template<typename T, template<typename U> class PARTICLETYPE>
bool BoundaryReflection3D<T, PARTICLETYPE>::intersectRaySphere(
    const std::vector<T>& pt,
    const typename std::vector<PARTICLETYPE<T> >::iterator& p,
    const std::vector<T>& oldPos,
    const std::vector<T>& pos,
    const std::vector<T>& dir,
    T& alpha) {
  T t = T();
  std::vector<T> m = pt - oldPos;
  std::vector<T> neg_dir(3, T(0));
  for (int i=0; i<3; i++) {
    neg_dir[i] = -dir[i];
  }
//  std::vector<T> dir_norm = util::normalize(vel);
  T dd = util::dotProduct3D(neg_dir, neg_dir);
  T b  = util::dotProduct3D(m, neg_dir) / dd;
  T c  = (util::dotProduct3D(m, m) - p->getRad()*p->getRad()) / dd;
//  cout << "C: " << c << " B: " << b << std::endl;
  // Exit if ray’s origin outside s (c > 0) and ray pointing away from s (b > 0)
  if (c > 0. && b > 0.) return false;
  T discr = b*b - c;
  // A negative discriminant corresponds to ray missing sphere
  if(discr < -std::numeric_limits<T>::epsilon()) return false;

  t = -b -std::sqrt(discr);
  // If t is negative, ray started inside sphere so clamp t to zero
//  cout << "T: " << t <<   std::endl;
   if (t < -std::numeric_limits<T>::epsilon() || t > 1.) return false;

//   cout << "T: " << t << " Alpha: " << alpha << std::endl;
 alpha = t;
        std::vector<T> spherePt = pt + t*neg_dir;
//        cout << "Pt on Sphere: " << spherePt[0] << " "<< spherePt[1] << " "<< spherePt[2] << " " << std::endl;
//        cout << "pt: " << pt[0] << " " << pt[1] << " " << pt[2] << std::endl;
//        cout << "Pos: " << pos[0] << " " <<   pos[1] << " " << pos[2] << " " << std::endl;
//        cout << "oldPos: " << oldPos[0] << " " <<   oldPos[1] << " " << oldPos[2] << " " << std::endl;
//        cout << "T: " << t << std::endl;
//
//        cout << "dist2: " << std::pow(spherePt[0]-pt[0], 2) +std::pow(spherePt[1]-pt[1], 2) +std::pow(spherePt[1]-pt[1], 2) << std::endl;
//        cout << "dir2: " << util::norm2(t*dir) << std::endl;
    return true;
}
}
#endif
