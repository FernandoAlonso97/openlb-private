/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Marie-Luise Maier, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef HertzBoundary3D_H_
#define HertzBoundary3D_H_

#include <set>
#include <math.h>
#include "functors/lattice/latticeFrameChangeF3D.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class HertzBoundary3D: public Boundary3D<T, PARTICLETYPE> {
private:
  CartesianToCylinder3D<T, T>& _car2cyl;
  T _length;
  T _radWire;
  T _delta;
  T _dT;
  SuperGeometry3D<T>& _sg;

public:
  HertzBoundary3D(CartesianToCylinder3D<T, T>& car2cyl, T length, T radWire,
    T delta, T dT, SuperGeometry3D<T>& sg);
  virtual ~HertzBoundary3D() {}
  virtual void applyBoundary(typename std::deque<PARTICLETYPE<T> >::iterator& p,
    ParticleSystem3D<T, PARTICLETYPE>& psSys);
};

template<typename T, template<typename U> class PARTICLETYPE>
HertzBoundary3D<T, PARTICLETYPE>::HertzBoundary3D(
  CartesianToCylinder3D<T, T>& car2cyl, T length, T radWire, T delta, T dT,
  SuperGeometry3D<T>& sg) :
  Boundary3D<T, PARTICLETYPE>(), _car2cyl(car2cyl), _radWire(radWire), _length(
    length), _delta(delta), _dT(dT), _sg(sg) {
}

template<typename T, template<typename U> class PARTICLETYPE>
void HertzBoundary3D<T, PARTICLETYPE>::applyBoundary(
  typename std::deque<PARTICLETYPE<T> >::iterator& p,
  ParticleSystem3D<T, PARTICLETYPE>& psSys) {

  T oldPos[3] = { T(), T(), T() };
  oldPos[0] = p->getPos()[0] - _dT * p->getVel()[0];
  oldPos[1] = p->getPos()[1] - _dT * p->getVel()[1];
  oldPos[2] = p->getPos()[2] - _dT * p->getVel()[2];

  Vector<T,3> line(3, T());
  line[0] = p->getPos()[0] - oldPos[0];
  line[1] = p->getPos()[1] - oldPos[1];
  line[2] = p->getPos()[2] - oldPos[2];

  Vector<T,3> relPosition;
  relPosition[0] = (p->getPos()[0] - _car2cyl.getCartesianOrigin()[0]);
  relPosition[1] = (p->getPos()[1] - _car2cyl.getCartesianOrigin()[1]);
  relPosition[2] = (p->getPos()[2] - _car2cyl.getCartesianOrigin()[2]);

  T tmp[3] = { T(), T(), T() };
  _car2cyl(tmp, &(p->getPos()[0]));
  T rad = tmp[0];
  T phi = tmp[1];

  T old[3] = { T(), T(), T() };
  _car2cyl(old, oldPos);

  Vector<T,3> normalAxis(_car2cyl.getAxisDirection());
  normalAxis.normalize();
  T dotProd = relPosition * normalAxis;

  // particle lies in cylinder
  if ((rad < _radWire + p->getRad() + _delta)
    && (T(0) <= dotProd && dotProd <= _length)) {

    T t = -(_radWire - rad) / (old[0] - rad) + 1;

    Vector<T, 3> intersection;
    intersection[0] = oldPos[0] + t*line[0];
    intersection[1] = oldPos[1] + t*line[1];
    intersection[2] = oldPos[2] + t*line[2];

    // todo add hertz force on cylinder surface

    //    std::vector<T> restF(3, T());
    //    restF[0] = p->getMass()
    //        * (p->getVel()[0] / _dT - line[0] * (1 - t) / _dT / _dT);
    //    restF[1] = p->getMass()
    //        * (p->getVel()[1] / _dT - line[1] * (1 - t) / _dT / _dT);
    //    restF[2] = p->getMass()
    //        * (p->getVel()[2] / _dT - line[2] * (1 - t) / _dT / _dT);

    p->getPos()[0] = intersection[0];
    p->getPos()[1] = intersection[1];
    p->getPos()[2] = intersection[2];

    p->getVel()[0] = T(0);
    p->getVel()[1] = T(0);
    p->getVel()[2] = T(0);

//    p->setActive(false);
  }
}

}
#endif /*HertzBoundary3D_H_*/

