/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef INDICATORTESTBOUNDARY3D_H_
#define INDICATORTESTBOUNDARY3D_H_

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class IndicatorTestBoundary3D: public Boundary3D<T, PARTICLETYPE> {
public:
  IndicatorTestBoundary3D(IndicatorF3D<T>& ind);
  IndicatorTestBoundary3D(IndicatorTestBoundary3D<T, PARTICLETYPE>& f);
  virtual ~IndicatorTestBoundary3D() {  }
  virtual void applyBoundary(typename std::vector<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys);
private:
  IndicatorF3D<T>& _ind;

};

template<typename T, template<typename U> class PARTICLETYPE>
IndicatorTestBoundary3D<T, PARTICLETYPE>::IndicatorTestBoundary3D(IndicatorF3D<T>& ind) : Boundary3D<T, PARTICLETYPE>(),  _ind(ind) {
}

template<typename T, template<typename U> class PARTICLETYPE>
void IndicatorTestBoundary3D<T, PARTICLETYPE>::applyBoundary(typename std::vector<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  if (_ind(p->getPos())[0])  {
    p = psSys._particles.erase(p);
    p--;
  }
}

}

#endif
