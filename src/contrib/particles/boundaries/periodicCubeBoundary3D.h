/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PERIODICCUBEBOUNDARY3D_H_
#define PERIODICCUBEBOUNDARY3D_H_

#include <math.h>
#include <vector>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class PeriodicCubeBoundary3D: public Boundary3D<T, PARTICLETYPE> {
public:
	PeriodicCubeBoundary3D(SuperGeometry3D<T> sg);
	PeriodicCubeBoundary3D(PeriodicCubeBoundary3D<T, PARTICLETYPE>& f);
	virtual ~PeriodicCubeBoundary3D() {	};
	virtual void applyBoundary(typename std::vector<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys);
private:
	//cube extents with origin (0,0,0)
	std::vector<T> extend;

};

template<typename T, template<typename U> class PARTICLETYPE>
PeriodicCubeBoundary3D<T, PARTICLETYPE>::PeriodicCubeBoundary3D(SuperGeometry3D<T> sg) : Boundary3D<T, PARTICLETYPE>() {
	extend = sg.getStatistics().getMaxPhysR(1);
}

template<typename T, template<typename U> class PARTICLETYPE>
void PeriodicCubeBoundary3D<T, PARTICLETYPE>::applyBoundary(typename std::vector<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys) {
		std::vector<T> pos = p->getPos();
		// check & calculation: new position
		for(int i=0;i<3;i++){
			if(abs(extend[i]/2 - pos[i])>extend[i]/2){
				if(pos[i]>extend[i]){
					pos[i] = pos[i]-extend[i];
				}
				else{
					pos[i] = extend[i]+pos[i];
				}
			}
		}
		p->setPos(pos);
}

}

#endif
