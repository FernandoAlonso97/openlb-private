/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef REMOVINGBOUNDARY3D_H_
#define REMOVINGBOUNDARY3D_H_

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class RemovingBoundary3D: public Boundary3D<T, PARTICLETYPE> {
public:
  RemovingBoundary3D(IndicatorF3D<T>& ind);
  RemovingBoundary3D(RemovingBoundary3D<T, PARTICLETYPE>& f);
  int getNoDeleted();
  virtual ~RemovingBoundary3D() { }
  virtual void applyBoundary(typename std::deque<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys);
private:
  IndicatorF3D<T>& _ind;
  static int noDeleted;
};

template<typename T, template<typename U> class PARTICLETYPE>
int RemovingBoundary3D<T, PARTICLETYPE>::noDeleted = 0;


template<typename T, template<typename U> class PARTICLETYPE>
RemovingBoundary3D<T, PARTICLETYPE>::RemovingBoundary3D(IndicatorF3D<T>& ind) : Boundary3D<T, PARTICLETYPE>(), _ind(ind) {
}

template<typename T, template<typename U> class PARTICLETYPE>
void RemovingBoundary3D<T, PARTICLETYPE>::applyBoundary(typename std::deque<PARTICLETYPE<T> >::iterator& p, ParticleSystem3D<T, PARTICLETYPE>& psSys) {
 bool foo[1];
 if (_ind(foo, &p->getPos()[0]) ) {
   p = psSys._particles.erase(p);
   --p;
   noDeleted++;
 }
}

template<typename T, template<typename U> class PARTICLETYPE>
int RemovingBoundary3D<T, PARTICLETYPE>::getNoDeleted() {
#ifdef PARALLEL_MODE_MPI
  int foo = noDeleted;
  singleton::mpi().reduceAndBcast(foo, MPI_SUM);
  return foo;
#else
  return noDeleted;
#endif
}



}

#endif
