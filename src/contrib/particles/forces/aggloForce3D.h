/*
 *  Copyright (C) 2015 Judith Kolbe, Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef AGGLOMERATION3D_H_
#define AGGLOMERATION3D_H_

#include "force3D.h"
//#include "../particleSystem3D.h"
//#include "../particleHelper.hh"
#include "../particleHelper.hh"
#include <cmath>


namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class AgglomerationEnnis3D: public Force3D<T, PARTICLETYPE> {

public:
	AgglomerationEnnis3D(T dT, T Kn, T Dn, T Ks, T Ds,
			T fric, T sRad, T vis, T dens, T stZ, T fh, T rh);

	virtual ~AgglomerationEnnis3D() {
	};

	void applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p, int pInt, ParticleSystem3D<T, PARTICLETYPE> &pSys);
private:
	T _dT, _Kn, _Dn, _Ks, _Ds, _fric, _sRad2, _vis, _dens, _stZ, _fh, _rh;
};

template<typename T, template<typename U> class PARTICLETYPE>
AgglomerationEnnis3D<T, PARTICLETYPE>::AgglomerationEnnis3D(T dT, T Kn, T Dn, T Ks, T Ds, T fric, T sRad,T vis, T dens, T stZ, T fh, T rh) :
		Force3D<T, PARTICLETYPE>(),_dT(dT), _Kn(Kn), _Dn(Dn), _Ks(Ks), _Ds(Ds), _fric(
				fric), _vis(vis), _dens(dens), _stZ(stZ), _fh(fh), _rh(rh) {
	_sRad2 = sRad * sRad;
}

template<typename T, template<typename U> class PARTICLETYPE>
void AgglomerationEnnis3D<T, PARTICLETYPE>::applyForce(
		typename std::vector<PARTICLETYPE<T> >::iterator p, int pInt,
		  ParticleSystem3D<T, PARTICLETYPE>& pSys) {

	OstreamManager clout(std::cout, "agglomerationProcess");

	std::vector<std::pair<size_t, T> > ret_matches;
	nanoflann::SearchParams params;
	params.sorted = false;

	const size_t nMatches = pSys._index.radiusSearch(&p->getPos()[0], _sRad2, ret_matches, params);
	for (int i = 0; i < nMatches; i++) {
		if (ret_matches[i].second != 0 && !pSys[ret_matches[i].first].getAggl()) {					//distance != 0

			T length = std::sqrt(ret_matches[i].second);											//distance from particle1 to particle2
			T delta = pSys[ret_matches[i].first].getRad() + p->getRad()	- length;					//contact/overlap

			if (delta > 0) {
				cout << "[agglomerationProcess] Collision start..."<< std::endl;
				std::vector<T> vel = pSys[ret_matches[i].first].getVel();
				std::vector<T> velp = p->getVel();
				std::vector<T> velR = vel-velp; //phelper::product<T>(velp, vel, 1., -1.);			//relative velocity of the two particles
				T velRabs2 = std::pow(velR[0], 2) + std::pow(velR[1], 2) + std::pow(velR[2], 2);
				cout << "[agglomerationProcess] rel. vel² = " << velRabs2 << endl;
				T rp = p->getRad();
				T r = pSys[ret_matches[i].first].getRad();
				T mRad = 1 / rp + 1 / r;

				if (velRabs2	< std::pow((1. + 1. / _stZ) * log(_fh / _rh) * 9. * _vis * mRad / (4. * _dens), 2)) {
					cout << "[agglomerationProcess] Agglomeration start..." << std::endl;



					T m = pSys[ret_matches[i].first].getMass();
					T mp = p->getMass();

					std::vector<T> pos = pSys[ret_matches[i].first].getPos();
					std::vector<T> posp = p->getPos();

					T massAgg = m + mp;
					T radAgg = pow(3. / (4. * M_PI) * massAgg / _dens, 1./3.);
					std::vector<T> velAgg(3, T());
					for (int j = 0; j < 3; j++) {
						velAgg[j] =_stZ* (m / massAgg * vel[j] + mp / massAgg * velp[j]);								//momentum conservation
					}

					std::vector<T> posAgg(3, T());
					for (int j = 0; j < 3; j++) {
						posAgg[j] = m / massAgg * pos[j] + mp / massAgg * posp[j];								//new position
					}

					/*
					 T absvel2 = pow(vel[0],2)+pow(vel[1],2)+pow(vel[2],2);
					 T absvelp2 = pow(velp[0],2)+pow(velp[1],2)+pow(velp[2],2);
					 T energydelta = m * mp /(2*(m+mp))*pow(sqrt(absvel2)-sqrt(absvelp2),2)*(1-pow(stZ,2));		//Energieverlust nur durch Stoß nicht durch Bindung
					 */

					pSys[ret_matches[i].first].setAggl(true);
					p->setMass(massAgg);
					p->setRad(radAgg);
					p->setVel(velAgg);
					p->setPos(posAgg);

					std::vector<T> frc(3,T());
					for(int j=0; j<3; j++) {
						frc[j] = massAgg/_dT * (velAgg[j] - velp[j]);
					}
					p->addForce(frc);

					cout << "[agglomerationProcess] Agglomeration finished" << endl;

				} else {
					T M = p->getMass()*pSys[ret_matches[i].first].getMass() / (p->getMass() + pSys[ret_matches[i].first].getMass());   //reduced mass
					std::vector<T> d(3,0.), s(3,0.), normal(3,0.), tangent(3,0.);
					d = phelper::product<T>(p->getPos(), pSys[ret_matches[i].first].getPos(), 1., -1.);			//vector from particle1 to particle2
					s = crossProduct3D<T>(crossProduct3D<T>(velR, normal), normal);								//tangent vector perpendicular to d

					if (d[0]*d[0]+d[1]*d[1]+d[2]*d[2] >0 ) {
						normal = normalize(d);
					} else print(normal);
					if (s[0]*s[0]+s[1]*s[1]+s[2]*s[2] >0 ) {
						tangent = normalize(s);
					}


					T velN = phelper::innerProduct<T>(velR, normal); 											//normal component of relative velocity
					T velS = phelper::innerProduct<T>(velR, tangent);											//missing term because angular velocity = 0

					T uS = velS*this->_converter.getDeltaT();
					T hS = -2.*M*_Ks*uS - 2*M*_Ds*velS;

					T forceN, forceT;
					forceN = 2.*M*_Kn*delta - 2.*M*_Dn*velN;
					if(hS*hS < _fric*_fric*forceN*forceN) {
						forceT = hS;
					} else forceT = _fric*forceN;


					std::vector<T> frc(3,0);
					for(int j=0; j<3; j++) {
						frc[j] += forceN*normal[j] + forceT*tangent[j];
					}
					p->addForce(frc);
				}
					//clout << "Springdashpot start..." << endl;
					//SpringDashpot3D<T, Particle3D> springDash ((converter, 1000, 10, 30, 10, 0.5, 0.001));		// welche werte?
					//springDash.applyForce(p,pSys);
					//clout << "Springdashpot finished" << endl;

			}
		}
	}
}

}



#endif
