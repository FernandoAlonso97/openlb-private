/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef BUOYANCYFORCERAYLEIGHTAYLOR3D_H_
#define BUOYANCYFORCERAYLEIGHTAYLOR3D_H_

#include <cmath>
#include "functors/lattice/superLatticeLocalF3D.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class BuoyancyForceRayleighTaylor3D: public Force3D<T, PARTICLETYPE> {

public:
  BuoyancyForceRayleighTaylor3D(LBconverter<T> const& converter, std::vector<T> direction,
      T psi = 0., T g = 9.81);

  virtual ~BuoyancyForceRayleighTaylor3D() { };

  void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
      ParticleSystem3D<T, PARTICLETYPE>& psSys);

private:
  LBconverter<T> const& _converter;
  std::vector<T> _direction;
  T _psi;
  T _g;
};

template<typename T, template<typename U> class PARTICLETYPE>
BuoyancyForceRayleighTaylor3D<T, PARTICLETYPE>::BuoyancyForceRayleighTaylor3D(
    LBconverter<T> const& converter, std::vector<T> direction, T psi, T g) :
    Force3D<T, PARTICLETYPE>(), _converter(converter), _direction(direction), _psi(psi),
    _g(g)
{
  T directionNorm = sqrt(
      pow(_direction[0], 2.) + pow(_direction[1], 2.) + pow(_direction[2], 2.));
  for (int i = 0; i < 3; ++i) {
    _direction[i] /= directionNorm;
  }
}

template<typename T, template<typename U> class PARTICLETYPE>
void BuoyancyForceRayleighTaylor3D< T, PARTICLETYPE>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& psSys)
{
  // weight force of fluid in similar volume like particle that replaces the fluid
  // acts in direction opposite to weight force of particle
  // _psi from application rayleighTaylor 0.436;
  T massEff = _psi * _converter.getCharRho() *4./3.*M_PI*std::pow(p->getRad(), 3);
  T latticeVol = _converter.getLatticeL()* _converter.getLatticeL()* _converter.getLatticeL();
  T factor = (4. / 3. * M_PI * std::pow(p->getRad(), 3)
             * _converter.getCharRho() + massEff);

  for (int j = 0; j < 3; ++j) {
    p->getForce()[j] -= factor * _g* _direction[j];
  }

}

}

#endif /* BUOYANCYFORCERAYLEIGHTAYLOR3D_H_ */
