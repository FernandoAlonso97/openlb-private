/*
 *  Copyright (C) 2015  Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef ContactTest3D_H_
#define ContactTest3D_H_

#include "force3D.h"
//#include "../particleSystem3D.h"
#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class ContactTest3D : public Force3D<T, PARTICLETYPE> {

 public:
  ContactTest3D();
  virtual ~ContactTest3D() {
  }
  ;
  void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                  ParticleSystem3D<T, PARTICLETYPE> &pSys);
  void reset() {
    contacts = 0;
    noContacts = 0;
  }
  long contacts;
  long noContacts;
};

template<typename T, template<typename U> class PARTICLETYPE>
ContactTest3D<T, PARTICLETYPE>::ContactTest3D()
    : Force3D<T, PARTICLETYPE>(),
      contacts(0), noContacts(0) {
}

template<typename T, template<typename U> class PARTICLETYPE>
void ContactTest3D<T, PARTICLETYPE>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& pSys) {

  std::vector<std::pair<size_t, T> > ret_matches;
  pSys.getContactDetection()->getMatches(pInt, ret_matches);
  typename std::vector<std::pair<size_t, T> >::iterator it =
      ret_matches.begin();

  for (; it != ret_matches.end(); ++it) {
    if (it->second != 0) {
      T rad2 = std::pow(pSys[it->first].getRad() + p->getRad(), 2);
      if (rad2 >= it->second) { // || nearZero(rad2 - it->second)) {
        contacts++;
      } else {
        noContacts++;
      }
    }
  }
}

}

#endif
