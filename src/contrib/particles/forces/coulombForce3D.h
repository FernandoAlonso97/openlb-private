///*  This file is part of the OpenLB library
// *
// *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
// *  E-mail contact: info@openlb.net
// *  The most recent release of OpenLB can be downloaded at
// *  <http://www.openlb.net/>
// *
// *  This program is free software; you can redistribute it and/or
// *  modify it under the terms of the GNU General Public License
// *  as published by the Free Software Foundation; either version 2
// *  of the License, or (at your option) any later version.
// *
// *  This program is distributed in the hope that it will be useful,
// *  but WITHOUT ANY WARRANTY; without even the implied warranty of
// *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *  GNU General Public License for more details.
// *
// *  You should have received a copy of the GNU General Public
// *  License along with this program; if not, write to the Free
// *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// *  Boston, MA  02110-1301, USA.
//*/
//
//#ifndef COULOMBFORCE3D_H_
//#define COULOMBFORCE3D_H_
//
//#define eps_0  8.854187817e-12
//
//namespace olb {
//
//template<typename T, template <typename U> class PARTICLETYPE>
//class ParticleSystem3D;
//
//template<typename T, template <typename U> class PARTICLETYPE>
//class CoulombForce3D : public Force3D<T, ElParticle3D> {
//public:
//  CoulombForce3D(ParticleSystem3D<T, PARTICLETYPE>&,const LBconverter<T>&,  T);
//  virtual ~CoulombForce3D() {};
//  void applyForce(const typename std::vector<PARTICLETYPE<T> >::iterator p);
//  T _k_e;
//};
//
//template<typename T, template <typename U> class PARTICLETYPE>
//CoulombForce3D<T, PARTICLETYPE>::CoulombForce3D(ParticleSystem3D<T, PARTICLETYPE>& psys,const LBconverter<T>& conv, T eps) :  Force3D<T, PARTICLETYPE>(psys, conv) {
//  _k_e = .25 * 1/(M_PI * eps_0 * eps);
//}
//
//template<typename T, template <typename U> class PARTICLETYPE>
//void CoulombForce3D<T, PARTICLETYPE>::applyForce(const typename std::vector<PARTICLETYPE<T> >::iterator p) {
//  std::vector<T> relPos(4,0.);
//  for (typename std::vector<PARTICLETYPE<T> >::iterator jt=p+1;
//       jt!=Force3D<T, PARTICLETYPE>::_psys->_particles.end(); ++jt) {
//    relPos[0] = p->_pos[0] - jt->_pos[0];
//    relPos[1] = p->_pos[1] - jt->_pos[1];
//    relPos[2] = p->_pos[2] - jt->_pos[2];
//    relPos[3] = pow(relPos[0], 2)+pow(relPos[1], 2)+pow(relPos[2], 2);
//    if (relPos[3] < 10*(p->_rad+jt->_rad)) {
//      if (relPos[3] == 0) relPos[3] = 1.e-100;
//      for (int i=0; i<3; ++i) {
//        T val =  _k_e * jt->_charge * p->_charge * relPos[i] / pow(relPos[3], 1.5);
//        p->_force[i] += val;
//        if(jt->_active) jt->_force[i] -= val;
//      }
//    }
//  }
//  for (typename std::vector<PARTICLETYPE<T> >::iterator jt=Force3D<T, PARTICLETYPE>::_psys->_shadowParticles.begin();
//       jt!=Force3D<T, PARTICLETYPE>::_psys->_shadowParticles.end(); ++jt) {
//    relPos[0] = p->_pos[0] - jt->_pos[0];
//    relPos[1] = p->_pos[1] - jt->_pos[1];
//    relPos[2] = p->_pos[2] - jt->_pos[2];
//    relPos[3] = pow(relPos[0], 2)+pow(relPos[1], 2)+pow(relPos[2], 2);
//    if (relPos[3] < 10*(p->_rad+jt->_rad)) {
//      if (relPos[3] == 0) relPos[3] = 1.e-100;
//      for (int i=0; i<3; ++i) {
//        p->_force[i] += _k_e * jt->_charge * p->_charge * relPos[i] / pow(relPos[3], 1.5);
//      }
//    }
//  }
//}
//
//
//
////#if PARALLEL_MODE_MPI
////  int size = 1;
////  size = singleton::mpi().getSize();
////
////  const int numInf = 5;
////
////  /* Communicate necessary information (position, charge, number) */
////  int globalNumOfParticles = Force3D<T, PARTICLETYPE>::_psys->globalNumOfParticles();
////  double* localPartInformation = new double[numInf*this->_psys->numOfParticles()];
////  int j=0;
////  for (typename std::vector<PARTICLETYPE<T> >::iterator it = this->_psys->_particles.begin(); it != this->_psys->_particles.end(); ++it, j+=numInf) {
////    localPartInformation[j] = it->_pos[0];
////    localPartInformation[j+1] = it->_pos[1];
////    localPartInformation[j+2] = it->_pos[2];
////    localPartInformation[j+3] = it->_charge;
////    localPartInformation[j+4] = it->_number;
////  }
////
////  int* localSizes = new int[size];
////  int* disps = new int[size];
////  int localNum = numInf*this->_psys->numOfParticles();
////
////  MPI_Allgather(&localNum, 1, MPI_INT, localSizes, 1, MPI_INT, MPI_COMM_WORLD);
////
////  double* particleInformation = new double[globalNumOfParticles*numInf];
////
//////  if(singleton::mpi().getRank() == 0) {
//////    std::cout << "localSize: ";
//////    for (int i=0; i<size; i++) {
//////      std::cout << localSizes[i]/5 << " ";
//////    }
//////    std::cout << std::endl;
//////  }
////
////  disps[0] = 0;
//////  if(singleton::mpi().getRank() == 0) std::cout << "disps: ";
////  for (int i=1; i<size; i++) {
////    disps[i] = disps[i-1] + localSizes[i-1];
//////    if(singleton::mpi().getRank() == 0) std::cout << disps[i]/5 << " ";
////  }
//////  if(singleton::mpi().getRank() == 0) std::cout << std::endl;
////
////  MPI_Allgatherv(&(localPartInformation[0]), localNum, MPI_DOUBLE,
////           &(particleInformation[0]),  localSizes, disps, MPI_DOUBLE, MPI_COMM_WORLD);
////  delete[] localSizes;
////  delete[] disps;
////  delete[] localPartInformation;
////
////  /* Calculate Force */
////  std::vector<T> relPos(4,0.);
////  for (int i=0; i<numInf*globalNumOfParticles; i+=numInf) {
////    if (p._number != particleInformation[i+4]) {
////      relPos[0] = p._pos[0] - particleInformation[i];
////      relPos[1] = p._pos[1] - particleInformation[i+1];
////      relPos[2] = p._pos[2] - particleInformation[i+2];
////      relPos[3] = sqrt(pow(relPos[0], 2)+pow(relPos[1], 2)+pow(relPos[2], 2) );
////      if (relPos[3] == 0) relPos[3] = 1.e-100;
////      for (int j=0; j<3; ++j) {
////        p._force[j] += _k_e * particleInformation[i+3] * p._charge * relPos[j] / pow(relPos[3], 3);
////      }
////    }
////  }
////  delete[] particleInformation;
////
////
////#else
////  std::vector<T> relPos(4,0.);
////  for (typename std::vector<PARTICLETYPE<T> >::iterator jt=it+1; jt!=Force3D<T, PARTICLETYPE>::_psys->_particles.end(); ++jt) {
////    relPos[0] = p._pos[0] - jt->_pos[0];
////    relPos[1] = p._pos[1] - jt->_pos[1];
////    relPos[2] = p._pos[2] - jt->_pos[2];
////    relPos[3] = sqrt(pow(relPos[0], 2)+pow(relPos[1], 2)+pow(relPos[2], 2) );
////    if (relPos[3] == 0) relPos[3] = 1.e-100;
////    for (int i=0; i<3; ++i) {
////      T val = _k_e * jt->_charge * p._charge * relPos[i] / pow(relPos[3], 3);
////      p._force[i] += val;
////      jt->_force[i] -= val;
////    }
////  }
////#endif
////}
//
//}
//#endif /* COULOMBFORCE_H_ */
