/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013-2016 Thomas Henn, Mathias J. Krause,
 *  Marie-Luise Maier
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef DENSITY_SINK_3D_H
#define DENSITY_SINK_3D_H

#include "functors/lattice/superLatticeLocalF3D.h"
#include "core/superLattice3D.h"
#include "particles/particleSystem3D.h"
#include "core/units.h"
#include "force3D.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
class DensitySink3D: public Force3D<T, PARTICLETYPE> {
public:
  DensitySink3D(SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel,
                SuperLattice3D<T, DESCRIPTOR>& lattice1,
                SuperLattice3D<T, DESCRIPTOR>& lattice2, LBconverter<T>& converter,
                T rate);
  DensitySink3D(DensitySink3D<T, PARTICLETYPE, DESCRIPTOR>&);
  virtual ~DensitySink3D() {
  }

  virtual void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                          ParticleSystem3D<T, PARTICLETYPE>& psSys);
private:
  SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& _getVel;
  SuperLattice3D<T, DESCRIPTOR>& _lattice1;
  SuperLattice3D<T, DESCRIPTOR>& _lattice2;
  LBconverter<T> _converter;
  T _rate;
  // TODO insert: maxLoad
};

}

#endif /* DENSITY_SINK_3D_H */
