/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef DENSITY_SINK_3D_HH
#define DENSITY_SINK_3D_HH

#include <cmath>
#include "densitySink3D.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
DensitySink3D<T, PARTICLETYPE, DESCRIPTOR>::DensitySink3D(
  SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel,
  SuperLattice3D<T, DESCRIPTOR>& lattice1,
  SuperLattice3D<T, DESCRIPTOR>& lattice2,
  LBconverter<T>& converter, T rate) :
  Force3D<T, PARTICLETYPE>(), _getVel(getVel),
  _lattice1(lattice1), _lattice2(lattice2), _converter(
    converter), _rate(rate)
{
}

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
DensitySink3D<T, PARTICLETYPE, DESCRIPTOR>::DensitySink3D(
  DensitySink3D<T, PARTICLETYPE, DESCRIPTOR>& f) :
  Force3D<T, PARTICLETYPE>(), _getVel(f._getVel),
  _lattice1(f._lattice1), _lattice2(f._lattice2), _converter(
    f.converter), _rate(f._rate)
{
}

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
void DensitySink3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
  typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
  ParticleSystem3D<T, PARTICLETYPE>& psSys)
{
  int intPos[3] = { 0 };
  T physIntPos[3] = { T() };

  // locIC indicates local cuboid number in actual thread
  int locIC = _lattice2.getLoadBalancer().loc(p->getCuboid());
  _lattice2.getCuboidGeometry().get(p->getCuboid()).getFloorLatticeR(intPos,
      &p->getPos()[0]);

  cout << "A intPos[0] " << intPos[0] << "intPos[1] " << intPos[1]
       << "intPos[2] " << intPos[2] << std::endl;

  T change1 = T();
  T change2 = T();
  // q = Geschw.richtungen, _lattice.getBlockLattice(iC).get(locX, locY, locZ)= Zelle von der Position locX,locY,locZ
  // _lattice.getBlockLattice(iC).get(locX, locY, locZ)[iPop]= Dichteverteilungsfkt an der Zelle in Richtung iPop= f_i
  for (int iPop = 1; iPop < DESCRIPTOR<T>::q; ++iPop) {
    change2 = ((_lattice2.getBlockLattice(locIC).get(intPos[0], intPos[1],
                intPos[2]).operator[](iPop) + DESCRIPTOR<T>::t[iPop]) * _rate)
              - DESCRIPTOR<T>::t[iPop];

    change1 = _lattice2.getBlockLattice(locIC).get(intPos[0], intPos[1], intPos[2]).operator[](
                iPop)-change2;

    _lattice2.getBlockLattice(locIC).get(intPos[0], intPos[1], intPos[2]).operator[](
      iPop) = change2;

    _lattice1.getBlockLattice(locIC).get(intPos[0], intPos[1], intPos[2]).operator[](
      iPop) += change1;
  }
}

}

#endif /* DENSITY_SINK_3D_HH */

