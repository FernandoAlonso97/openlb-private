/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Marie-Luise Maier, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

// To Do determine drag coefficient
// no explicit formulation

#ifndef DRAGFORCE_3D_HH
#define DRAGFORCE_3D_HH

#include<cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
class DragForce3D: public Force3D<T, PARTICLETYPE> {
public:
  /// Constructor, FluidVelocity, physicalTimeStep, physicalDynamicViscosity
  DragForce3D(SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel,
      T charRho, T cd);
  /// Constructor using values from converter
  DragForce3D(SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel,
      LBconverter<T> const& converter, T cd);
  /// Destructor
  virtual ~DragForce3D() {
  }
  virtual void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p,
      int pInt, ParticleSystem3D<T, PARTICLETYPE>& psSys);

private:
  SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& _getVel;
  T _charRho; // characteristic density
  T _cw;      // drag coefficient
  T _factor;
};

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
DragForce3D<T, PARTICLETYPE, DESCRIPTOR>::DragForce3D(
    SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel,
    T charRho, T cw) :
    Force3D<T, PARTICLETYPE>(), _getVel(getVel), _charRho(charRho),
    _cw(cw) {
  _factor = _cw*M_PI*_charRho/2.;
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
DragForce3D<T, PARTICLETYPE, DESCRIPTOR>::DragForce3D(
    SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel,
    LBconverter<T> const& converter, T cw) :
    Force3D<T, PARTICLETYPE>(), _getVel(getVel), _cw(cw) {
  _charRho = converter.getCharRho();
  _factor = _cw*M_PI*_charRho/2.;
}

/// 6 Pi r mu U_rel
template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
void DragForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  T fluidVel[3] = { 0., 0., 0. };

  _getVel(fluidVel, &p->getPos()[0], p->getCuboid());

  Vector<T,3> difference;
  difference[0] = fluidVel[0] - p->getVel()[0];
  difference[1] = fluidVel[1] - p->getVel()[1];
  difference[2] = fluidVel[2] - p->getVel()[2];

  normalize(difference);

  if (norm(difference) > 0) {
    p->getForce()[0] += _factor*p->getRad()*p->getRad()*pow(fluidVel[0] - p->getVel()[0], 2)/
        norm(difference)*difference[0];
    p->getForce()[1] += _factor*p->getRad()*p->getRad()*pow(fluidVel[1] - p->getVel()[1], 2)/
        norm(difference)*difference[1];
    p->getForce()[2] += _factor*p->getRad()*p->getRad()*pow(fluidVel[2] - p->getVel()[2], 2)/
        norm(difference)*difference[2];
  }
//  std::cout<<"dragF[0]=" << _factor*p->getRad()*p->getRad()*pow(fluidVel[0] - p->getVel()[0], 2)/
//      norm(difference)*difference[0] << std::endl;
}

}
#endif /* STOKESDRAGFORCE_3D_HH */
