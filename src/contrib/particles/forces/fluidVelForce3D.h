/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef FLUIDVELFORCE3D_H_
#define FLUIDVELFORCE3D_H_

#include <cmath>
#include <functional>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
class FluidVelForce3D : public Force3D<T, PARTICLETYPE> {
 public:
  FluidVelForce3D(SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel, T timestep);
  FluidVelForce3D(FluidVelForce3D<T, PARTICLETYPE, DESCRIPTOR>&);
  virtual ~FluidVelForce3D() {
    this->clout << "Destructing" << std::endl;
  }
  ;
  virtual void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, PARTICLETYPE>& psSys);

  SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& _getVel;
  T _timestep;
  clock_t time;

};

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
FluidVelForce3D<T, PARTICLETYPE, DESCRIPTOR>::FluidVelForce3D(
    SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel, T timestep)
    : Force3D<T, PARTICLETYPE>(),
      _getVel(getVel),
      _timestep(timestep) {
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
FluidVelForce3D<T, PARTICLETYPE, DESCRIPTOR>::FluidVelForce3D(
    FluidVelForce3D<T, PARTICLETYPE, DESCRIPTOR>& f)
    : Force3D<T, PARTICLETYPE>(f),
      _getVel(f._getVel),
      _timestep(_timestep) {
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
void FluidVelForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  T fluidVel[3] = {T()};
  //T x[3] = {p->getPos()[0], p->getPos()[1], p->getPos()[2]};
  std::vector<T> frc(3, T());
  //_getVel(fluidVel, x, p->getCuboid());
  _getVel(fluidVel, &p->getPos()[0], p->getCuboid());
  frc = p->getForce();
  for (int j = 0; j < 3; ++j) {
    frc[j] += (fluidVel[j] - p->getVel()[j]) * p->getMass() / _timestep;
  }
  p->setForce(frc);
  // cout << "fluidvel: " << fluidVel[0] << " "<< fluidVel[1] << " "<< fluidVel[2] << " " << std::endl;
}

}

#endif /* DRAGFORCE_H_ */
