/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#include "dragForce3D.h"
#include "rayleighDragForce3D.h"
#include "fluidVelForce3D.h"
#include "coulombForce3D.h"
#include "densitySink3D.h"
#include "springDashpot3D.h"
//#include "aggloForce3D.h"
#include "simpleVanDerWaals3D.h"
#include "vanDerWaals3D.h"
#include "integralDragForce3D.h"
#include "contactTestForce.h"

#include "magneticForce3D.h"
#include "interpMagForce3D.h"
#include "hertzMindlin3D.h"

#include "rotatingForce3D.h"
//#include "hertzMindlin3D_lindner.h"

#include "stokes2WayForce3D.h"
//#include "magnusForce3D.h"

#include "buoyancyForceRayleighTaylor3D.h"
#include "weightForceRayleighTaylor3D.h"
