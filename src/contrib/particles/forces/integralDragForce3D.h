/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef INTEGRALDRAGFORCE3D_H_
#define INTEGRALDRAGFORCE3D_H_

#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
class IntegralDragForce3D : public Force3D<T, PARTICLETYPE> {
 public:
  /// Converter, FluidVelocity, physicalTime, physicalDynamicViscosity
  IntegralDragForce3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, T eps = 1.1);
  IntegralDragForce3D(IntegralDragForce3D<T, PARTICLETYPE, DESCRIPTOR>& f);
  virtual ~IntegralDragForce3D() {
  }
  virtual void applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, PARTICLETYPE>& psSys);

 private:
  SuperLattice3D<T, DESCRIPTOR>& _sLattice;
  T minT[3], maxT[3];
  int min[3], max[3];
  T _eps2;
  T _eps;
};

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
IntegralDragForce3D<T, PARTICLETYPE, DESCRIPTOR>::IntegralDragForce3D(
    SuperLattice3D<T, DESCRIPTOR>& sLattice, T eps)
    : Force3D<T, PARTICLETYPE>(),
      _sLattice(sLattice),
      _eps2(eps*eps),
      _eps(eps){
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
IntegralDragForce3D<T, PARTICLETYPE, DESCRIPTOR>::IntegralDragForce3D(
    IntegralDragForce3D<T, PARTICLETYPE, DESCRIPTOR>& f)
    : Force3D<T, PARTICLETYPE>(f.conv),
      _sLattice(f._sLattice) {
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
void IntegralDragForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
    typename std::vector<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  std::vector<T> physR(3, T());
  T rad = p->getRad();
  T radEps = rad * _eps;
  minT[0] = p->getPos()[0] - radEps;
  minT[1] = p->getPos()[1] - radEps;
  minT[2] = p->getPos()[2] - radEps;
  maxT[0] = p->getPos()[0] + radEps;
  maxT[1] = p->getPos()[1] + radEps;
  maxT[2] = p->getPos()[2] + radEps;
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getLatticeR(min, minT);
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getLatticeR(max, maxT);
  min[0] += _sLattice.getOverlap();
  min[1] += _sLattice.getOverlap();
  min[2] += _sLattice.getOverlap();
  max[0] += _sLattice.getOverlap() + 1;
  max[1] += _sLattice.getOverlap() + 1;
  max[2] += _sLattice.getOverlap() + 1;
  T force[3] = {0};
  for (int iX = min[0]; iX < max[0]; ++iX) {
    for (int iY = min[1]; iY < max[1]; ++iY) {
      for (int iZ = min[2]; iZ < max[2]; ++iZ) {
        _sLattice.getCuboidGeometry().get(p->getCuboid()).getPhysR(&physR[0],
                        iX - _sLattice.getOverlap(),
                        iY - _sLattice.getOverlap(),
                        iZ - _sLattice.getOverlap());
        if (std::pow(physR[0] - p->getPos()[0], 2)
            + std::pow(physR[1] - p->getPos()[1], 2)
            + std::pow(physR[2] - p->getPos()[2], 2) < rad * rad * _eps2) {

          for (int iPop = 1; iPop < DESCRIPTOR<T>::q ; ++iPop) {
            const int* c = DESCRIPTOR<T>::c(iPop);
            _sLattice.getCuboidGeometry().get(p->getCuboid()).getPhysR(&physR[0], iX+c[0]-_sLattice.getOverlap(), iY+c[1]-_sLattice.getOverlap(), iZ+c[2]-_sLattice.getOverlap());
            if (std::pow(physR[0] - p->getPos()[0], 2)
                + std::pow(physR[1] - p->getPos()[1], 2)
                + std::pow(physR[2] - p->getPos()[2], 2) < rad * rad * _eps2) {
              T f = _sLattice.getExtendedBlockLattice(_sLattice.getLoadBalancer().loc(p->getCuboid())).get(iX + c[0], iY + c[1], iZ + c[2])[iPop];
               f += _sLattice.getExtendedBlockLattice(_sLattice.getLoadBalancer().loc(p->getCuboid())).get(iX, iY, iZ)[util::opposite<DESCRIPTOR<T> >(iPop)];
              force[0] -= c[0]*f;
              force[1] -= c[1]*f;
              force[2] -= c[2]*f;
            }
          }
        }
      }
    }
  }

  p->getForce()[0] += this->_converter.physForce(force[0]) / (2.*M_PI*std::pow(p->getRad(), 2));
  p->getForce()[1] += this->_converter.physForce(force[1]) / (2.*M_PI*std::pow(p->getRad(), 2));
  p->getForce()[2] += this->_converter.physForce(force[2]) / (2.*M_PI*std::pow(p->getRad(), 2));

  cout << "integralForce: " << this->_converter.physMasslessForce(force[0]) << " " << this->_converter.physMasslessForce(force[1]) << " " << this->_converter.physMasslessForce(force[2]) << " " << std::endl;

//  p->getForce()[0] += this->_converter.physMasslessForce(force[0]);
//  p->getForce()[1] += this->_converter.physMasslessForce(force[1]);
//  p->getForce()[2] += this->_converter.physMasslessForce(force[2]);
}


/// ############### TEMPLATE SPECIALISATION ###############

template<typename T, template<typename W> class DESCRIPTOR>
class IntegralDragForce3D<T, RotatingParticle3D, DESCRIPTOR> : public Force3D<T, RotatingParticle3D> {
 public:
  /// Converter, FluidVelocity, physicalTime, physicalDynamicViscosity
  IntegralDragForce3D(const LBconverter<T>& conv,
                      SuperLattice3D<T, DESCRIPTOR>& sLattice, T eps = 1.1);
  IntegralDragForce3D(IntegralDragForce3D<T, RotatingParticle3D, DESCRIPTOR>& f);
  virtual ~IntegralDragForce3D() {
  }
  virtual void applyForce(typename std::vector<RotatingParticle3D<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, RotatingParticle3D>& psSys);

 private:
  SuperLattice3D<T, DESCRIPTOR>& _sLattice;
  T minT[3], maxT[3];
  int min[3], max[3];
  T _eps2;
};

template<typename T, template<typename W> class DESCRIPTOR>
IntegralDragForce3D<T, RotatingParticle3D, DESCRIPTOR>::IntegralDragForce3D(
    const LBconverter<T>& conv, SuperLattice3D<T, DESCRIPTOR>& sLattice, T eps)
    : Force3D<T, RotatingParticle3D>(conv),
      _sLattice(sLattice),
      _eps2(eps*eps){
}

template<typename T, template<typename W> class DESCRIPTOR>
IntegralDragForce3D<T, RotatingParticle3D, DESCRIPTOR>::IntegralDragForce3D(
    IntegralDragForce3D<T, RotatingParticle3D, DESCRIPTOR>& f)
    : Force3D<T, RotatingParticle3D>(f.conv),
      _sLattice(f._sLattice) {
}

template<typename T, template<typename W> class DESCRIPTOR>
void IntegralDragForce3D<T, RotatingParticle3D, DESCRIPTOR>::applyForce(
    typename std::vector<RotatingParticle3D<T> >::iterator p, int pInt,
    ParticleSystem3D<T, RotatingParticle3D>& psSys) {
  T physR[3] = {0};
  T rad = p->getRad();
  minT[0] = p->getPos()[0] - rad;
  minT[1] = p->getPos()[1] - rad;
  minT[2] = p->getPos()[2] - rad;
  maxT[0] = p->getPos()[0] + rad;
  maxT[1] = p->getPos()[1] + rad;
  maxT[2] = p->getPos()[2] + rad;
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getLatticeR(min, minT);
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getLatticeR(max, maxT);
  min[0] += _sLattice.getOverlap();
  min[1] += _sLattice.getOverlap();
  min[2] += _sLattice.getOverlap();
  max[0] += _sLattice.getOverlap() + 1;
  max[1] += _sLattice.getOverlap() + 1;
  max[2] += _sLattice.getOverlap() + 1;
  T force[3] = {0};
  T torque[3] = {0};
  for (int iX = min[0]; iX < max[0]; ++iX) {
    for (int iY = min[1]; iY < max[1]; ++iY) {
      for (int iZ = min[2]; iZ < max[2]; ++iZ) {
        _sLattice.getCuboidGeometry().get(p->getCuboid()).getPhysR(physR, iX - _sLattice.getOverlap(),
                        iY - _sLattice.getOverlap(),
                        iZ - _sLattice.getOverlap());
        if (std::pow(physR[0] - p->getPos()[0], 2)
            + std::pow(physR[1] - p->getPos()[1], 2)
            + std::pow(physR[2] - p->getPos()[2], 2) < rad * rad * _eps2) {

          for (int iPop = 1; iPop < DESCRIPTOR<T>::q ; ++iPop) {
            const int* c = DESCRIPTOR<T>::c(iPop);
            _sLattice.getCuboidGeometry().get(p->getCuboid()).getPhysR(physR, iX+c[0]-_sLattice.getOverlap(), iY+c[1]-_sLattice.getOverlap(), iZ+c[2]-_sLattice.getOverlap());
            if (std::pow(physR[0] - p->getPos()[0], 2)
                + std::pow(physR[1] - p->getPos()[1], 2)
                + std::pow(physR[2] - p->getPos()[2], 2) < rad * rad * _eps2) {
              T f = _sLattice.getExtendedBlockLattice(_sLattice.getLoadBalancer().loc(p->getCuboid())).get(iX + c[0], iY + c[1], iZ + c[2])[iPop];
               f += _sLattice.getExtendedBlockLattice(_sLattice.getLoadBalancer().loc(p->getCuboid())).get(iX, iY, iZ)[util::opposite<DESCRIPTOR<T> >(iPop)];

              force[0] -= c[0]*f;
              force[1] -= c[1]*f;
              force[2] -= c[2]*f;
              torque[0] += (physR[1] - p->getPos()[1]) *c[2]*f - (physR[2] - p->getPos()[2]) *c[1]*f;
              torque[1] += (physR[2] - p->getPos()[2]) *c[0]*f - (physR[0] - p->getPos()[0]) *c[2]*f;
              torque[2] += (physR[0] - p->getPos()[0]) *c[1]*f - (physR[1] - p->getPos()[1]) *c[0]*f;
            }
          }
        }
      }
    }
  }

  p->getForce()[0] += this->_converter.physForce(force[0]) * M_PI*this->_converter.getLatticeL() * this->_converter.getLatticeL() * p->getRad()*p->getRad() * 2. / (this->_converter.getCharRho() * this->_converter.getCharU() * this->_converter.getCharU());
  p->getForce()[1] += this->_converter.physForce(force[1]) * M_PI*this->_converter.getLatticeL() * this->_converter.getLatticeL() * p->getRad()*p->getRad() * 2. / (this->_converter.getCharRho() * this->_converter.getCharU() * this->_converter.getCharU());
  p->getForce()[2] += this->_converter.physForce(force[2]) * M_PI*this->_converter.getLatticeL() * this->_converter.getLatticeL() * p->getRad()*p->getRad() * 2. / (this->_converter.getCharRho() * this->_converter.getCharU() * this->_converter.getCharU());
  p->getTorque()[0] = this->_converter.physForce(torque[0])*  this->_converter.physLength();
  p->getTorque()[1] = this->_converter.physForce(torque[1])*  this->_converter.physLength();
  p->getTorque()[2] = this->_converter.physForce(torque[2])*  this->_converter.physLength();

  cout << "integralForce: " <<this->_converter.physForce(force[0]) * M_PI*this->_converter.getLatticeL() * this->_converter.getLatticeL() * p->getRad()*p->getRad() * 2. / (this->_converter.getCharRho() * this->_converter.getCharU() * this->_converter.getCharU())
  << " " << this->_converter.physForce(force[1]) * M_PI*this->_converter.getLatticeL() * this->_converter.getLatticeL() * p->getRad()*p->getRad() * 2. / (this->_converter.getCharRho() * this->_converter.getCharU() * this->_converter.getCharU())
  << " " << this->_converter.physForce(force[2]) * M_PI*this->_converter.getLatticeL() * this->_converter.getLatticeL() * p->getRad()*p->getRad() * 2. / (this->_converter.getCharRho() * this->_converter.getCharU() * this->_converter.getCharU()) << " " << std::endl;


//  std::cout << "VEL: " << p->getVel()[0] << " " << p->getVel()[1] << " " << p->getVel()[2] << " " << std::endl;
//  std::cout << "TOR: " << p->getRad()*(p->getTorque()[2] - p->getTorque()[1]) << " " <<
//      p->getRad()*(p->getTorque()[2] - p->getTorque()[0]) << " " <<
//      p->getRad()*(p->getTorque()[1] - p->getTorque()[2]) << " " << std::endl;
}

}

#endif /* DRAGFORCE_H_ */
