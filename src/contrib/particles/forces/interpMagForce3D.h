/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2015 Marie-Luise Maier, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/// Magnetic field that creates magnetization in wire has to be orthogonal to the wire.
/// to calculate the magnetic force on particles around a cylinder
/// (J. Lindner et al. / Computers and Chemical Engineering 54 (2013) 111-121)

#ifndef INTERPMAGFORCE3D_H_
#define INTERPMAGFORCE3D_H_

#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
class InterpMagForce3D: public Force3D<T, PARTICLETYPE> {
public:
  InterpMagForce3D(T M_P, int directionField = 0, T scale = T(1));
  virtual ~InterpMagForce3D() { };

  virtual void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, PARTICLETYPE>& psSys);
private:
  T _M_P;
  T _scale;
  int _directionField;
};

template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
InterpMagForce3D<T, PARTICLETYPE, DESCRIPTOR>::InterpMagForce3D(T M_P,
    int directionField, T scale) :
  Force3D<T, PARTICLETYPE>(), _M_P(M_P), _scale(scale), _directionField(
    directionField)
{
  //  this->_name = "InterpMagForce3D";
}


template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
void InterpMagForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
  typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
  ParticleSystem3D<T, PARTICLETYPE>& pSys)
{

  std::vector<std::pair<size_t, T> > ret_matches;
  pSys.getContactDetection()->getMatches(pInt, ret_matches);

  const PARTICLETYPE<T>* p2 = NULL;

  typename std::vector<std::pair<size_t, T> >::iterator it =
    ret_matches.begin();

  for (; it != ret_matches.end(); it++) {

    if (it->second >= std::numeric_limits<T>::epsilon()) {   //distance != 0

      p2 = &pSys[it->first];

      /// check if particles do not overlap
//      if (pow(p2->getRad() + p->getRad(),2) < it->second) {
//      if ((p2->getRad() + p->getRad()) <= sqrt(it->second)) {

        T mu_0 = 4 * 3.14159265 * 1e-7;  // magnetic constant
        T mu_i = 4. / 3 * M_PI * pow(p->getRad(), 3) * _M_P; // magnetic moment of particle i
        T mu_j = 4. / 3 * M_PI * pow(p2->getRad(), 3) * _M_P;  // of particle j

        //vector from particle1 to particle2
        std::vector<T> t(3, 0.);
        t[0] = p2->getPos()[0] - p->getPos()[0];
        t[1] = p2->getPos()[1] - p->getPos()[1];
        t[2] = p2->getPos()[2] - p->getPos()[2];

        T dist = sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]); // distance p and p2

        t[0] = t[0] / dist;
        t[1] = t[1] / dist;
        t[2] = t[2] / dist;

        T factor = -3. / 4. * mu_0 * mu_i * mu_j    // *VZ
                   / M_PI / pow(dist, 4.);

        std::vector<T> frc(3, T());

        // ToDo: steady dipole direction!! should be changeable
        if (_directionField == 0) {
          // for field in x direction
          frc[0] = factor * (5 * t[0]*t[0] - 3) * t[0];
          frc[1] = factor * (5 * t[0]*t[0] - 1) * t[1];
          frc[2] = factor * (5 * t[0]*t[0] - 1) * t[2];
        }
        if (_directionField == 1) {
          // for field in y direction
          frc[0] = factor * (5 * t[1]*t[1] - 1) * t[0];
          frc[1] = factor * (5 * t[1]*t[1] - 3) * t[1];
          frc[2] = factor * (5 * t[1]*t[1] - 1) * t[2];
        }
        if (_directionField == 2) {
          // for field in z direction
          frc[0] = factor * (5 * t[2]*t[2] - 1) * t[0];
          frc[1] = factor * (5 * t[2]*t[2] - 1) * t[1];
          frc[2] = factor * (5 * t[2]*t[2] - 3) * t[2];
        }

        for (int j = 0; j < 3; j++) {
          p->getForce()[j] += frc[j] * _scale;
          std::cout << "InterpMagForce " << j << "= " << frc[j] * _scale
                    << std::endl;
        }
//      }
    }
  }
}

}
#endif // INTERPMAGFORCE3D_H_
