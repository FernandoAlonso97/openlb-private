/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2015 Marie-Luise Maier, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/// Magnetic field that creates magnetization in wire has to be orthogonal to the wire.
/// to calculate the magnetic force on particles around a cylinder
/// (J. Lindner et al. / Computers and Chemical Engineering 54 (2013) 111-121)
#ifndef INTERPMAGFORCEFORMAGP3D_H_
#define INTERPMAGFORCEFORMAGP3D_H_

#include "../../../particles/forces/force3D.h"
#include "../../../particles/particleSystem3D.h"

#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<
typename W> class DESCRIPTOR>
class InterpMagForceForMagP3D : public Force3D<T, MagneticParticle3D> {
public:
  InterpMagForceForMagP3D(T scale = T(1));
  InterpMagForceForMagP3D(InterpMagForceForMagP3D<T, DESCRIPTOR>& f);

  virtual ~InterpMagForceForMagP3D() {
  }
  ;
  virtual void applyForce(typename std::deque<MagneticParticle3D<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, MagneticParticle3D>& psSys);
  clock_t time;
private:
  T _scale;
  T _sRad2;
};

template<typename T, template<
typename W> class DESCRIPTOR>
InterpMagForceForMagP3D<T, DESCRIPTOR>::InterpMagForceForMagP3D(T scale)
  : Force3D<T, MagneticParticle3D>(),
    time(0),
    _scale(scale)
{
//  this->_name = "InterpMagForceForMagP3D";
}

template<typename T, template<
typename W> class DESCRIPTOR>
void InterpMagForceForMagP3D<T, DESCRIPTOR>::applyForce(
  typename std::deque<MagneticParticle3D<T> >::iterator p, int pInt,
  ParticleSystem3D<T, MagneticParticle3D>& pSys)
{

  std::vector < std::pair<size_t, T> > ret_matches;
  pSys.getDetection()->getMatches(pInt, ret_matches);

  const MagneticParticle3D<T>* p2 = NULL;
  typename std::vector<std::pair<size_t, T> >::iterator it =
    ret_matches.begin();

  Vector<T, 3> dMom_1 = Vector<T, 3>((p->getMoment()));
  if (dMom_1.norm() > std::numeric_limits<T>::epsilon()){
  T m_p1 = p->getMagnetisation();

  for (; it != ret_matches.end(); it++) {
    if (it->second >= std::numeric_limits < T > ::epsilon()) {   //distance != 0

      p2 = &pSys[it->first];

      if ((p2->getRad() + p->getRad()) <= std::sqrt(it->second)) {  //it->second != 0) {  //distance != 0

    	// get neighbour particle moment
    	Vector<T, 3> dMom_2((p2->getMoment()));
    	if (dMom_2.norm() > std::numeric_limits<T>::epsilon()){
    	T m_p2 = p2->getMagnetisation();

    	// given moment magnitudes as scale factors
    	T m_i_scaleFactor = dMom_1.norm();
    	T m_j_scaleFactor = dMom_2.norm();

        // normalised moment directions
    	Vector<T, 3> n_i(dMom_1);
    	n_i.normalize();
    	Vector<T, 3> n_j(dMom_2);
    	n_j.normalize();

    	// constants
        T mu_0 = 4*3.14159265e-7;  // magnetic constant
        T mu_i = 4./3*M_PI*pow(p->getRad(), 3)*m_p1 * m_i_scaleFactor; // magnetic moment of particle i
        T mu_j = 4./3*M_PI*pow(p2->getRad(), 3)*m_p2 * m_j_scaleFactor; // of particle j

        //vector from particle1 to particle2
        Vector<T, 3> r_ij;
        r_ij[0] = p->getPos()[0] - p2->getPos()[0];
        r_ij[1] = p->getPos()[1] - p2->getPos()[1];
        r_ij[2] = p->getPos()[2] - p2->getPos()[2];

        // distance from particle1 to particle2
        T r = r_ij.norm();

        // normalised direction vector
        Vector<T, 3> t_ij(r_ij);
        t_ij.normalize();

        // FORCE
        // F_ij = -[(3*mu_0*mu_i*mu_j)/(4*M_PI*r^4)][-(n_i x n_j)
        //         + 5(n_i * t_ij)(n_j * t_ij)t_ij - {(n_j * t_ij)n_i + (n_i * t_ij)n_j}]
        T scalar_termF = - (3 * mu_0 * mu_i * mu_j) / (4 * M_PI * std::pow(r, 4));
        Vector<T, 3> force = 5 * (n_i * t_ij) * (n_j * t_ij) * t_ij;
        force -= crossProduct3D(n_i, n_j);
        force -= (n_j * t_ij) * n_i;
        force -= (n_i * t_ij) * n_j;
        force *= scalar_termF;

        // TORQUE
        // T_ij = -[(mu_0*mu_i*mu_j)/(4*M_PI*r^3)][n_i x n_j - 3(n_j * t_ij)n_i x t_ij]
        T scalar_termT = - (mu_0 * mu_i * mu_j) / (4 * M_PI * std::pow(r, 3));
        Vector<T, 3> torque = crossProduct3D(n_i, n_j);
        torque -= crossProduct3D( 3 * (n_j * t_ij) * n_i, t_ij);
        torque *= scalar_termT;

        p->getTorque()[0]+= torque[0];
        p->getTorque()[1]+= torque[1];
        p->getTorque()[2]+= torque[2];
        p->getForce()[0] += force[0] * _scale;
        p->getForce()[1] += force[1] * _scale;
        p->getForce()[2] += force[2] * _scale;

      }
      }
    }
  }
  }
}

}

#endif // INTERPMAGFORCEFORMAGP3D_H_
