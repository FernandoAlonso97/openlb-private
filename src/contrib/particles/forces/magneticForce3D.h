/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2015 Marie-Luise Maier, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/// Magnetic field that creates magnetization in wire has to be orthogonal to the wire.
/// to calculate the magnetic force on particles around a cylinder
/// (J. Lindner et al. / Computers and Chemical Engineering 54 (2013) 111-121)
#ifndef MAGNETICFORCE3D_H_
#define MAGNETICFORCE3D_H_

//#include "force3D.h"
//#include "../particleSystem3D.h"
#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
class MagneticForce3D: public Force3D<T, PARTICLETYPE> {
public:
  /// Converter, FluidVelocity, physicalTime, physicalDynamicViscosity
  // Fm = mu0*4/3.*PI*radParticle^3*_Mp*radWire^2/r^3 *
  //   [radWire^2/r^2+cos(2*theta), sin(2*theta), 0]
  MagneticForce3D(AnalyticalF3D<T, T>& getMagForce,
                  T scale = T(1));
  MagneticForce3D(MagneticForce3D<T, PARTICLETYPE, DESCRIPTOR>& f);
  virtual ~MagneticForce3D() {
  }
  ;
  virtual void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, PARTICLETYPE>& psSys);
private:
  AnalyticalF3D<T, T>& _getMagForce;
  T _scale;
};

template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
MagneticForce3D<T, PARTICLETYPE, DESCRIPTOR>::MagneticForce3D(
  AnalyticalF3D<T, T>& getMagForce, T scale) :
  Force3D<T, PARTICLETYPE>(), _getMagForce(getMagForce), _scale(scale)
{
  //  this->_name = "magneticForce";
}
/*
template<typename T, template<typename U> class PARTICLETYPE, template<
  typename W> class DESCRIPTOR>
MagneticForce3D<T, PARTICLETYPE, DESCRIPTOR>::MagneticForce3D(
  MagneticForce3D<T, PARTICLETYPE, DESCRIPTOR>& f) :
  Force3D<T, PARTICLETYPE>(f.psys, f.conv)  //, _getMagForce(f.getMagForce)
{
  //  this->_name = "magneticForce";
}
*/
template<typename T, template<typename U> class PARTICLETYPE, template<
typename W> class DESCRIPTOR>
void MagneticForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
  typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
  ParticleSystem3D<T, PARTICLETYPE>& psSys)
{
  std::vector<T> force(3, T());
  force = p->getForce();

  T pos[3] = { T(), T(), T() };

  pos[0] = p->getPos()[0];
  pos[1] = p->getPos()[1];
  pos[2] = p->getPos()[2];

  T forceHelp[3] = { T(), T(), T() };
  _getMagForce(forceHelp, pos);

  for (int j = 0; j < 3; ++j) {
    p->getForce()[j] += forceHelp[j] * _scale;
//    std::cout << "magneticForce[ " << j << "]=" << forceHelp[j] * _scale << "; "
//        << std::endl;
  }
}

}

#endif // MAGNETICFORCE3D_H_
