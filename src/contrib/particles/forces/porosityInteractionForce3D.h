/*
 *  Copyright (C) 2017 Marie-Luise Maier
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef POROSITYINTERACTIONFORCE_H
#define POROSITYINTERACTIONFORCE_H

#include <cmath>
#include "particles/particleSystem3D.h"
#include "forces.h"
#include "core/units.h"

namespace olb {


template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
class PorosityInteractionForce3D: public Force3D<T, PARTICLETYPE> {

public:
  PorosityInteractionForce3D(LBconverter<T>& conv);
  virtual ~PorosityInteractionForce3D() {};
  void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                  ParticleSystem3D<T, PARTICLETYPE> &pSys);
  void computeForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                  ParticleSystem3D<T, PARTICLETYPE> &pSys, T force[3]);
private:
  LBconverter<T>& _conv;
};

}

#endif
//POROSITYINTERACTIONFORCE_H
