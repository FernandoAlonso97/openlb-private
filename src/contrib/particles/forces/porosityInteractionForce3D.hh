/*
 *  Copyright (C) 2017 Marie-Luise Maier
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef POROSITYINTERACTIONFORCE_HH
#define POROSITYINTERACTIONFORCE_HH

#include "../../contrib/particles/forces/porosityInteractionForce3D.h"

#include <cmath>


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
PorosityInteractionForce3D<T, PARTICLETYPE, DESCRIPTOR>::PorosityInteractionForce3D(
    LBconverter<T>& conv) :
  Force3D<T, PARTICLETYPE>(), _conv(conv)
{
}

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
void PorosityInteractionForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
  typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
  ParticleSystem3D<T, PARTICLETYPE>& pSys)
{
  T force[3] = {T(), T(), T()};
  computeForce(p, pInt, pSys, force);

  p->getStoreForce()[0] += force[0];
  p->getStoreForce()[1] += force[1];
  p->getStoreForce()[2] += force[2];
}

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
void PorosityInteractionForce3D<T, PARTICLETYPE, DESCRIPTOR>::computeForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt, // pInt is particle system
    ParticleSystem3D<T, PARTICLETYPE>& pSys, T force[3])
{
  std::vector<std::pair<size_t, T>> ret_matches;
  // kind of contactDetection has to be chosen in application
  pSys.getContactDetection()->getMatches(pInt, ret_matches);

  const PARTICLETYPE<T>* p2 = NULL;
  T porosity = T();
  T psi = T();

  T invLL = 1./(2.*_conv.getLatticeL());

  // iterator walks through number of neighbored particles = ret_matches
  typename std::vector<std::pair<size_t, T> >::iterator it =
    ret_matches.begin();

  T r = sqrt(it->second)*invLL;
  // iterator walks through number of neighbored particles = ret_matches
  for (; it != ret_matches.end(); it++) {
    p2 = &pSys[it->first];

    // iterator->second is pow(distance,2)
    if (0.<= r &&  r < 1.) {
      psi = 15./7.*(2./3. - r*r + pow(r,3./2.)*0.5);
    }
    if (1.<= r &&  r < 2.) {
      psi = 5./14.*pow(2. - r,2);
    }
    psi *= 1./M_PI*invLL*invLL*invLL/8.;
    psi *= 4./3.*M_PI*std::pow(p2->getRad(),3);

    porosity -= psi;
  }
  porosity += 1.;
  std::vector<T> one(3,T(porosity));

  p->setStoreForce(one) ;
}

}

#endif
// POROSITYINTERACTIONFORCE_HH
