/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef RAYLEIGHDRAGFORCE3D_H_
#define RAYLEIGHDRAGFORCE3D_H_

#include <cmath>


namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

enum drag_t {Sphere, HalfSphere, Cone, Cube, AngledCube, LongCylinder, Shortcylinder, StreamlinedBody, StreamlinedHalfBody};

template<typename T, template<typename U> class PARTICLETYPE>
class RayleighDragForce3D : public Force3D<T, PARTICLETYPE> {
public:
  RayleighDragForce3D(AnalyticalF3D<T,T>&, drag_t);
  virtual ~RayleighDragForce3D() {};
  void applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p, int pInt, const ParticleSystem3D<T, PARTICLETYPE>& psSys);

  AnalyticalF3D<T,T>& _getVel;
  T _dragCoeff;
};

template<typename T, template<typename U> class PARTICLETYPE>
RayleighDragForce3D<T, PARTICLETYPE>::RayleighDragForce3D(AnalyticalF3D<T,T>& getvel, drag_t dragCoeff) :  Force3D<T, PARTICLETYPE>(), _getVel(getvel), _dragCoeff(0.) {
  switch (dragCoeff) {
  case 0:
    _dragCoeff = .47;
    break;
  case 1:
    _dragCoeff = .42;
    break;
  case 2:
    _dragCoeff = .50;
    break;
  case 3:
    _dragCoeff = 1.05;
    break;
  case 4:
    _dragCoeff = .8;
    break;
  case 5:
    _dragCoeff = .82;
    break;
  case 6:
    _dragCoeff = 1.15;
    break;
  case 7:
    _dragCoeff = .04;
    break;
  case 8:
    _dragCoeff = .09;
    break;
  }
}

template<typename T, template<typename U> class PARTICLETYPE>
void RayleighDragForce3D<T, PARTICLETYPE>::applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p, int pInt,  const ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  std::vector<T> pos = p->getPos();
  std::vector<T> fluidVel = _getVel(pos);
  std::vector<T> frc(3, T());
  for(int j=0; j<3; ++j) {
    frc[j] += .5* M_PI * std::pow(p->getRad(), 2) * this->_converter.getCharRho() * _dragCoeff * std::pow((fluidVel[j] - p->getVel()[j]), 2);
  }
  p->addForce(frc);
}


}






#endif /* RAYLEIGHDRAGFORCE_H_ */
