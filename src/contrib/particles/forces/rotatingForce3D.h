/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef ROTATINGFORCE3D_H_
#define ROTATINGFORCE3D_H_

#include <cmath>
#include <functional>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
class RotatingForce3D : public Force3D<T, PARTICLETYPE> {
 public:
  RotatingForce3D(AnalyticalF3D<T, T>&, T, int w);
  RotatingForce3D(RotatingForce3D<T, PARTICLETYPE, DESCRIPTOR>&);
  virtual ~RotatingForce3D() {
    this->clout << "Destructing" << std::endl;
  }
  ;
  virtual void applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p,
                          int pInt, ParticleSystem3D<T, PARTICLETYPE>& psSys);

  AnalyticalF3D<T, T>& _getVel;
  T _timestep;
  clock_t time;

  int _w;
};

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
RotatingForce3D<T, PARTICLETYPE, DESCRIPTOR>::RotatingForce3D(
    AnalyticalF3D<T, T>& getvel, T timestep, int w)
    : Force3D<T, PARTICLETYPE>(),
      _getVel(getvel),
      _timestep(timestep),
      _w(w) {
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
RotatingForce3D<T, PARTICLETYPE, DESCRIPTOR>::RotatingForce3D(
    RotatingForce3D<T, PARTICLETYPE, DESCRIPTOR>& f)
    : Force3D<T, PARTICLETYPE>(f),
      _getVel(f._getVel),
      _timestep(_timestep) {
}

template<typename T, template<typename U> class PARTICLETYPE, template<
    typename W> class DESCRIPTOR>
void RotatingForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
    typename std::vector<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& psSys) {
  T fluidVel[3] = { T(), T(), T() };
  T x[3] = { p->getPos()[0], p->getPos()[1], p->getPos()[2]};
  T factor = 0.1; // ??
  std::vector < T > frc(3, T());
  _getVel(fluidVel, x);

  frc = p->getForce();

  frc[0] += 2*_w* (p->getVel()[1] - factor*fluidVel[1])*p->getMass()
      + pow(_w, 2) *x[0]*p->getMass();
  frc[1] += -2*_w* (p->getVel()[0] - factor*fluidVel[0])*p->getMass()
      + pow(_w, 2) *x[1]*p->getMass();

  p->setForce(frc);
  // cout << "fluidvel: " << fluidVel[0] << " "<< fluidVel[1] << " "<< fluidVel[2] << " " << std::endl;
}
}

#endif
