/*
 *  Copyright (C) 2015  Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef SimpleVanDerWaals3D_H_
#define SimpleVanDerWaals3D_H_

#include "force3D.h"
//#include "../particleSystem3D.h"
#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class SimpleVanDerWaals3D : public Force3D<T, PARTICLETYPE> {

 public:
  SimpleVanDerWaals3D();
  virtual ~SimpleVanDerWaals3D() {
  }
  ;
  void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                  ParticleSystem3D<T, PARTICLETYPE> &pSys);

  clock_t time, time2;
};

template<typename T, template<typename U> class PARTICLETYPE>
SimpleVanDerWaals3D<T, PARTICLETYPE>::SimpleVanDerWaals3D()
    : Force3D<T, PARTICLETYPE>(), time(0), time2(0) {
  time = time2 = 0;
}

template<typename T, template<typename U> class PARTICLETYPE>
void SimpleVanDerWaals3D<T, PARTICLETYPE>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& pSys) {

  clock_t delta = clock();
  std::vector<std::pair<size_t, T> > ret_matches;
  pSys.getContactDetection()->getMatches(pInt, ret_matches);

  typename std::vector<std::pair<size_t, T> >::iterator it=ret_matches.begin();
  for (; it!=ret_matches.end(); ++it) {
    if (it->second != 0) {			//distance != 0
      if (!(pSys[it->first].getActive()) && std::pow(pSys[it->first].getRad() + p->getRad(), 2)
          > it->second) {
        p->setActive(false);
        return;
      }
    }
  }
  time2 += clock() - delta;
}

}

#endif
