/*
 *  Copyright (C) 2014 Fabian Klemens, Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef SPRINGDASHPOT3D_H_
#define SPRINGDASHPOT3D_H_

#include "force3D.h"
//#include "../particleSystem3D.h"
//#include "../particleHelper.hh"
#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class SpringDashpot3D : public Force3D<T, PARTICLETYPE> {

 public:
  SpringDashpot3D(T Kn, T Dn, T Ks, T Ds, T fric, T dT);
  virtual ~SpringDashpot3D() {
  }
  void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                  ParticleSystem3D<T, PARTICLETYPE> &pSys);

  clock_t time;


 private:
  T _Kn, _Dn, _Ks, _Ds, _fric;
  T _dT;
  std::vector<T> velR,d, normal, s, tangent;
};

template<typename T, template<typename U> class PARTICLETYPE>
SpringDashpot3D<T, PARTICLETYPE>::SpringDashpot3D(T Kn, T Dn, T Ks, T Ds,
                                                  T fric, T dT)
    : Force3D<T, PARTICLETYPE>(),
      time(0),
      _Kn(Kn),
      _Dn(Dn),
      _Ks(Ks),
      _Ds(Ds),
      _fric(fric),
      _dT(dT),
      velR(3, T()), d(3, T()), normal(3, T()), s(3, T()), tangent(3, T()) {
}

template<typename T, template<typename U> class PARTICLETYPE>
void SpringDashpot3D<T, PARTICLETYPE>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& pSys) {

  clock_t deltaT = clock();

  std::vector<std::pair<size_t, T> > ret_matches;
  pSys.getContactDetection()->getMatches(pInt, ret_matches);

  const PARTICLETYPE<T>* p2 = NULL;
//  for (int i = 0; i < nMatches; i++) {
  typename std::vector<std::pair<size_t, T> >::iterator it=ret_matches.begin();
//  for (int i = 0; i < nMatches; i++) {
  for (; it!=ret_matches.end(); it++) {
    if (it->second != 0) {			//distance != 0
      p2 = &pSys[it->first];

      if ( std::pow(p2->getRad() + p->getRad(),2) > it->second) {
//        std::cout << "Computing spring dashpot" << std::endl;
        T M = p->getMass() * p2->getMass() / (p->getMass() + p2->getMass());  //reduced mass
        velR[0] = p->getVel()[0] - p2->getVel()[0];
        velR[1] = p->getVel()[1] - p2->getVel()[1];
        velR[2] = p->getVel()[2] - p2->getVel()[2];
        d[0] = p->getPos()[0] - p2->getPos()[0];
        d[1] = p->getPos()[1] - p2->getPos()[1];
        d[2] = p->getPos()[2] - p2->getPos()[2];
//        normal = normalize(d);
        T dNorm = std::sqrt(d[0]*d[0] + d[1]*d[1] + d[2]*d[2]);
        normal[0] = d[0] / dNorm;
        normal[1] = d[1] / dNorm;
        normal[2] = d[2] / dNorm;
//        s = crossProduct3D<T>(crossProduct3D<T>(velR, normal), normal);  //tangent vector perpendicular to d
        T velN = normal[0]*velR[0]+  normal[1]*velR[1] +  normal[2]*velR[2];
        s[0] = velN *normal[0] - velR[0];
        s[1] = velN *normal[1] - velR[1];
        s[2] = velN *normal[2] - velR[2];

        if (s[0] != 0 || s[1] != 0 || s[2] != 0) {
          T sNorm = std::sqrt(s[0]*s[0] + s[1]*s[1] + s[2]*s[2]);
//          tangent = normalize(s);
          tangent[0] = s[0] / sNorm;
          tangent[1] = s[1] / sNorm;
          tangent[2] = s[2] / sNorm;
        }

//        T velN = phelper::innerProduct<T>(velR, normal); 	//normal component of relative velocity
//        T velS = phelper::innerProduct<T>(velR, tangent);	//missing term because angular velocity = 0
        T velS = velR[0] * tangent[0] + velR[1] * tangent[1] + velR[2] * tangent[2];

        T uS = velS * _dT;
        T hS = -2. * M * _Ks * uS - 2 * M * _Ds * velS;

        T forceN, forceT;
//        T length = std::sqrt(it->.second);  //distance from particle1 to particle2
        T delta = p2->getRad() + p->getRad() - std::sqrt(it->second);  //contact/overlap
        forceN = 2. * M * _Kn * delta - 2. * M * _Dn * velN;
        if (hS * hS < _fric * _fric * forceN * forceN) {
          forceT = hS;
        } else {
          forceT = _fric * forceN;
        }
        for (int j = 0; j < 3; j++) {
          p->getForce()[j] += forceN * normal[j] + forceT * tangent[j];
//          cout << forceN * normal[j] + forceT * tangent[j] << std::endl;
        }
      }
    }
  }

  time += deltaT -clock();
}

}

#endif
