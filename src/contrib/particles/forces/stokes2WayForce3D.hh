/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef STOKES2WAYFORCE_3D_HH
#define STOKES2WAYFORCE_3D_HH

#include<cmath>

#include "stokes2WayForce3D.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//T c_Acrivos = (32./375. * M_PI);
//T K_Acrivos = 1-1.7601 * pow(c_Acrivos,1./3.) + c_Acrivos - 1.5593 * pow(c_Acrivos,2) + 3.9799 * pow(c_Acrivos, 8./3.) - 3.0734 * pow(c_Acrivos, 10./3.);

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE, template<typename W> class DESCRIPTOR>
Stokes2WayForce3D<T, PARTICLETYPE, DESCRIPTOR>::Stokes2WayForce3D(SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel, SuperLattice3D<T, DESCRIPTOR>& sLattice, LBconverter<T> const& converter, T dT, T mu)
  : Force3D<T, PARTICLETYPE>(),
    _getVel(getVel),
    _sLattice(sLattice),
    _conv(converter),
    _mu(mu)
{
  _C1 = 6. * M_PI * _mu * dT;
  _dTinv = 1. / dT;
}

template<typename T, template<typename U> class PARTICLETYPE, template<typename W> class DESCRIPTOR>
Stokes2WayForce3D<T, PARTICLETYPE, DESCRIPTOR>::Stokes2WayForce3D(SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR>& getVel, SuperLattice3D<T, DESCRIPTOR>& sLattice,LBconverter<T> const& converter)
  : Force3D<T, PARTICLETYPE>(),
    _sLattice(sLattice),
    _conv(converter),
    _getVel(getVel), stokes(0)
{
  _C1 = 6. * M_PI * converter.getDynamicViscosity() * converter.physTime();
  _dTinv = 1. / converter.physTime();
}

/// 6 Pi r mu U_rel
template<typename T, template<typename U> class PARTICLETYPE, template<typename W> class DESCRIPTOR>
void Stokes2WayForce3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
  typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
  ParticleSystem3D<T, PARTICLETYPE>& psSys)
{
  T fluidVel[3] = {0.,0.,0.};

  _getVel(fluidVel, &p->getPos()[0], p->getCuboid());

  T c = _C1 * p->getRad() * p->getInvMass();
  T C2 = 1. / (1. + c);
  T force[3] = {0};
  T latticeForce[3] = {0};

  // p->getVel() is particle velocity of the last time step
  // formulation of new force with particle velocity of the last time step with
  // implicit Euler
  force[0] = p->getMass() * _dTinv * ((c * fluidVel[0] + p->getVel()[0]) * C2 - p->getVel()[0]);
  force[1] = p->getMass() * _dTinv * ((c * fluidVel[1] + p->getVel()[1]) * C2 - p->getVel()[1]);
  force[2] = p->getMass() * _dTinv * ((c * fluidVel[2] + p->getVel()[2]) * C2 - p->getVel()[2]);
  p->getForce()[0] += force[0];
  p->getForce()[1] += force[1];
  p->getForce()[2] += force[2];

  std::vector<T> Force(3, T());
  Force[0] = 6. * M_PI * _mu * p->getRad() * (fluidVel[0] - p->getVel()[0]) * (1-1.7601 * pow((32./375. * M_PI),1./3.) + (32./375. * M_PI) - 1.5593 * pow((32./375. * M_PI),2) + 3.9799 * pow((32./375. * M_PI), 8./3.) - 3.0734 * pow((32./375. * M_PI), 10./3.));
  Force[1] = 6. * M_PI * _mu * p->getRad() * (fluidVel[1] - p->getVel()[1]) * (1-1.7601 * pow((32./375. * M_PI),1./3.) + (32./375. * M_PI) - 1.5593 * pow((32./375. * M_PI),2) + 3.9799 * pow((32./375. * M_PI), 8./3.) - 3.0734 * pow((32./375. * M_PI), 10./3.));
  Force[2] = 6. * M_PI * _mu * p->getRad() * (fluidVel[2] - p->getVel()[2]) * (1-1.7601 * pow((32./375. * M_PI),1./3.) + (32./375. * M_PI) - 1.5593 * pow((32./375. * M_PI),2) + 3.9799 * pow((32./375. * M_PI), 8./3.) - 3.0734 * pow((32./375. * M_PI), 10./3.));


//  latticeForce[0] = -Force[0] / _conv.physMasslessForce() / std::pow(_conv.getLatticeL(), 3) / _conv.getCharRho();
//  latticeForce[1] = -Force[1] / _conv.physMasslessForce() / std::pow(_conv.getLatticeL(), 3) / _conv.getCharRho();
//  latticeForce[2] = -Force[2] / _conv.physMasslessForce() / std::pow(_conv.getLatticeL(), 3) / _conv.getCharRho();

//  latticeForce[0] = -_conv.latticeForce(Force[0]);
//  latticeForce[1] = -_conv.latticeForce(Force[1]);
//  latticeForce[2] = -_conv.latticeForce(Force[2]);

  latticeForce[0] = -Force[0] / _conv.physMasslessForce();
  latticeForce[1] = -Force[1] / _conv.physMasslessForce();
  latticeForce[2] = -Force[2] / _conv.physMasslessForce();

//
//  latticeForce[0] = 0;
//  latticeForce[1] = 10;
//  latticeForce[2] = 0;

  stokes += force[0] * force[0]+force[1] * force[1]+force[2] * force[2];

  int intPos[3] = {0};
  int locIC =  _sLattice.getLoadBalancer().loc(p->getCuboid());
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getLatticeR(&intPos[0], &p->getPos()[0]);
  intPos[0] += _sLattice.getOverlap();
  intPos[1] += _sLattice.getOverlap();
  intPos[2] += _sLattice.getOverlap();
  _sLattice.getExtendedBlockLattice(locIC).get(intPos[0], intPos[1], intPos[2])
  .addExternalField(DESCRIPTOR<T>::ExternalField::forceBeginsAt,DESCRIPTOR<T>::ExternalField::sizeOfForce, latticeForce);
}
}
#endif /* STOKESDRAGFORCE_3D_HH */
