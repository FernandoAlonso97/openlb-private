/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Marie-Luise Maier
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef STOKESDRAGFORCEZHANG_3D_HH
#define STOKESDRAGFORCEZHANG_3D_HH

#include<cmath>
#include "stokesDragForceZhang3D.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//#define forceMJK
#ifndef forceMJK
//  #define forceStokes
#ifndef forceStokes
#define forceZhang
#endif
#endif

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
StokesDragForceZhang3D<T, PARTICLETYPE, DESCRIPTOR>::StokesDragForceZhang3D(
  SuperLatticeInterpDensity3Degree3D<T, DESCRIPTOR>& interpLDensity,
  SuperLattice3D<T, DESCRIPTOR>& sLattice, LBconverter<T> const& converter) :
  Force3D<T, PARTICLETYPE>(), _interpLDensity(interpLDensity),
  _sLattice(sLattice), _conv(converter)
{
}

template<typename T, template<typename U> class PARTICLETYPE, template<
           typename W> class DESCRIPTOR>
void StokesDragForceZhang3D<T, PARTICLETYPE, DESCRIPTOR>::applyForce(
  typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
  ParticleSystem3D<T, PARTICLETYPE>& psSys)
{
  int intPos[3] = { 0 };
  T physIntPos[3] = { T() };
  T _mu = _conv.getDynamicViscosity();
  // always to floor and then use in each direction 4 next neighbor nodes
  // with floor parallelization works better!
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getFloorLatticeR(intPos,
      &p->getPos()[0]);
  _sLattice.getCuboidGeometry().get(p->getCuboid()).getPhysR(physIntPos,
      intPos);

  T fAlphaL[DESCRIPTOR<T>::q] = { T() };
  // Lagrange interpolated polynomial for density on node
  _interpLDensity(fAlphaL, &(p->getPos()[0]), p->getCuboid());

  T physPRad = p->getRad();
  T pRad = _conv.latticeLength(physPRad);

  T uP[3] = { T() };
  uP[0] = _conv.latticeVelocity(p->getVel()[0]);
  uP[1] = _conv.latticeVelocity(p->getVel()[1]);
  uP[2] = _conv.latticeVelocity(p->getVel()[2]);

  T physPU[3] = { T() };
  T physFU[3] = { T() };
  T sp = T();
  T fBetaLNew[DESCRIPTOR<T>::q] = { T() };
  T rhoL = T(1);
  T gF[3] = { T() };   // force density gF

  T latticeL = _conv.getLatticeL();
  T latticeU = _conv.getLatticeU();

  for (int iPop = 0; iPop < _sLattice.getDataSize(); ++iPop) {
    /// MJK
    // Get direction
    const int* ci = DESCRIPTOR<T>::c(iPop);
    // Get f_q of next fluid cell where l = opposite(q)

    T fBeta[DESCRIPTOR<T>::q] = { T() };
    T fBetaPosition[3] = { T() };
    // neighbor Lagrange position of Lagrange particle position in direction i
    fBetaPosition[0] = p->getPos()[0] + latticeL*ci[0];
    fBetaPosition[1] = p->getPos()[1] + latticeL*ci[1];
    fBetaPosition[2] = p->getPos()[2] + latticeL*ci[2];

    _interpLDensity(fBeta, fBetaPosition, p->getCuboid());
    T fi = fBeta[iPop]; // nach collide vor streaming (zeitstep vorher)
    // Get f_l of the boundary cell
    // Add f_q and f_opp
    fi += fAlphaL[util::opposite<DESCRIPTOR<T> >(iPop)];

#ifdef forceZhang
    // Update force
    /*gF[0] += _conv.physVelocity()*ci[0] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2()*
        (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
    gF[1] += _conv.physVelocity()*ci[1] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2()*
        (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
    gF[2] += _conv.physVelocity()*ci[2] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2()*
        (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
        */
    // check this:
    gF[0] += ci[0] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2()*
                      (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
    gF[1] += ci[1] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2()*
                      (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
    gF[2] += ci[2] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2()*
                      (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
#else
    /// physical velocity (factor 0.5 because of 6 in --> 6*M_PI*...
    physFU[0] = _conv.physVelocity()*ci[0]*0.5*fi;
    physFU[1] = _conv.physVelocity()*ci[1]*0.5*fi;
    physFU[2] = _conv.physVelocity()*ci[2]*0.5*fi;

    // point product
    sp = ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2];
    physPU[0] = _conv.physVelocity()*ci[0]*
                rhoL*DESCRIPTOR<T>::invCs2()*DESCRIPTOR<T>::t[iPop]*sp;
    physPU[1] = _conv.physVelocity()*ci[1]*
                rhoL*DESCRIPTOR<T>::invCs2()*DESCRIPTOR<T>::t[iPop]*sp;
    physPU[2] = _conv.physVelocity()*ci[2]*
                rhoL*DESCRIPTOR<T>::invCs2()*DESCRIPTOR<T>::t[iPop]*sp;

    gF[0] += physFU[0]-physPU[0];
    gF[1] += physFU[1]-physPU[1];
    gF[2] += physFU[2]-physPU[2];
#endif
  }

  T force[3] = {T()};

#ifdef forceMJK
  /// MJK
  // für radius 2.5e-5 bewährt: *.000075
  force[0] = _conv.physForce(gF[0])*physPRad*3/latticeL/latticeL*latticeU
             *M_PI*_conv.getCharNu()/_conv.getCharU();
  force[1] = _conv.physForce(gF[1])*physPRad*3/latticeL/latticeL*latticeU
             *M_PI*_conv.getCharNu()/_conv.getCharU();
  force[2] = _conv.physForce(gF[2])*physPRad*3/latticeL/latticeL*latticeU
             *M_PI*_conv.getCharNu()/_conv.getCharU();
#endif
#ifdef forceStokes
  // to compare: F_stokes = 6 M_PI mu r_p (u_f - u_p)
  force[0] = M_PI*physPRad *6*_mu *gF[0];
  force[1] = M_PI*physPRad *6*_mu *gF[1];
  force[2] = M_PI*physPRad *6*_mu *gF[2];
#endif
#ifdef forceZhang
  /*force[0] = M_PI*physPRad *physPRad *gF[0];
  force[1] = M_PI*physPRad *physPRad *gF[1];
  force[2] = M_PI*physPRad *physPRad *gF[2];
  */
//  T urel1 = gF[0]*_conv.getCharRho()*_conv.physVelocity();
//  T urel2 = gF[1]*_conv.getCharRho()*_conv.physVelocity();
//  T urel3 = gF[2]*_conv.getCharRho()*_conv.physVelocity();

//  if (urel1==0.) urel1=1.;
  force[0] = M_PI*physPRad *physPRad
             * gF[0]
             /*change gf to phys units*/
             *_conv.getCharRho()*_conv.physVelocity()
             // this is modified and equals
             *3*_conv.getCharNu()/physPRad
//         *3*2*urel1
//         *_conv.getCharNu()/physPRad/2/urel1 // = 1/Re~c_d
             ;
//  if (urel2==0.) urel2=1.;
  force[1] = M_PI*physPRad *physPRad
             * gF[1]
             /*change gf to phys units*/
             *_conv.getCharRho()*_conv.physVelocity()
             // this is modified and equals
             *3*_conv.getCharNu()/physPRad
//         *3*2*urel2
//         *_conv.getCharNu()/physPRad/2/urel2 // = 1/Re~c_d
             ;
//  if (urel3==0.) urel3=1.;
  force[2] = M_PI*physPRad *physPRad
             * gF[2]
             /*change gf to phys units*/
             *_conv.getCharRho()*_conv.physVelocity()
             // this is modified and equals
             *3*_conv.getCharNu()/physPRad
//         *3*2*urel3
//         *_conv.getCharNu()/physPRad/2/urel3 // = 1/Re~c_d
             ;
#endif
  p->getForce()[0] += force[0];
  p->getForce()[1] += force[1];
  p->getForce()[2] += force[2];

  //std::cout << "[Part]  F2= " << force[2] << std::endl;

}

}
#endif /* STOKESDRAGFORCEZHANG_3D_HH */
