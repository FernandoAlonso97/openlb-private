/*
 *  Copyright (C) 2015  Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef VANDERWAALS3D_H_
#define VANDERWAALS3D_H_

#include "force3D.h"
//#include "../particleSystem3D.h"
//#include "../particleHelper.hh"
#include <cmath>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class VanDerWaals3D : public Force3D<T, PARTICLETYPE> {

 public:
  VanDerWaals3D(T A);
  virtual ~VanDerWaals3D() {
  }
  ;
  void applyForce(typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
                  ParticleSystem3D<T, PARTICLETYPE> &pSys);
  clock_t time2;

 private:
  T _A;
};

template<typename T, template<typename U> class PARTICLETYPE>
VanDerWaals3D<T, PARTICLETYPE>::VanDerWaals3D(T A)
    : Force3D<T, PARTICLETYPE>(),
      time2(0),
      _A(A) {
}

template<typename T, template<typename U> class PARTICLETYPE>
void VanDerWaals3D<T, PARTICLETYPE>::applyForce(
    typename std::deque<PARTICLETYPE<T> >::iterator p, int pInt,
    ParticleSystem3D<T, PARTICLETYPE>& pSys) {

  clock_t delta = clock();
  std::vector<std::pair<size_t, T> > ret_matches;
  pSys.getContactDetection()->getMatches(pInt, ret_matches);

  typename std::vector<std::pair<size_t, T> >::iterator it =
      ret_matches.begin();
  for (; it != ret_matches.end(); ++it) {
    if (it->second != 0) {      //distance != 0
      T centerD = std::sqrt(it->second);
      T c = - _A/6. * (2*p->getRad()*pSys[it->first].getRad()/(it->second-std::pow(p->getRad() + pSys[it->first].getRad(), 2))
          + 2*p->getRad()*pSys[it->first].getRad()/(it->second-std::pow(p->getRad() - pSys[it->first].getRad(), 2)));
//      + std::log((it->second - std::pow(p->getRad() + pSys[it->first].getRad(), 2))/(it->second - std::pow(p->getRad() - pSys[it->first].getRad(), 2))));
      std::vector<T> n(3, T());
//      cout << c << std::endl;
      n[0] = (pSys[it->first].getPos()[0] - p->getPos()[0]) / centerD;
      n[1] = (pSys[it->first].getPos()[1] - p->getPos()[1]) / centerD;
      n[2] = (pSys[it->first].getPos()[2] - p->getPos()[2]) / centerD;
      p->getForce()[0] += c * n[0];
      p->getForce()[1] += c * n[1];
      p->getForce()[2] += c * n[2];

//      T centerD = std::sqrt(it->second);
//      T d = centerD - p->getRad() - pSys[it->first].getRad();
//      std::vector<T> n(3, T());
//      n[0] = (pSys[it->first].getPos()[0] - p->getPos()[0]) / centerD;
//      n[1] = (pSys[it->first].getPos()[1] - p->getPos()[1]) / centerD;
//      n[2] = (pSys[it->first].getPos()[2] - p->getPos()[2]) / centerD;
//      T c = 32. / 3. * _A * std::pow(p->getRad(), 6)
//          / (std::pow(d, 3)
//              * std::pow(4 * std::pow(p->getRad(), 2) - std::pow(d, 2), 2));
//      cout << "C: " << c << " " << n[0] << " " << n[1] << " " << n[2]
//          << std::endl;
//      p->getForce()[0] += c * n[0];
//      p->getForce()[1] += c * n[1];
//      p->getForce()[2] += c * n[2];
    }
  }
  time2 += clock() - delta;
}

}

#endif
