/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef STL_MESH_CPP
#define STL_MESH_CPP

#include <stdio.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

namespace olb {

struct STLPoint {
  STLPoint(std::vector<STLPoint>::iterator it) : r(3, 0.), left(it), right(it) {};

  std::vector<double> r;
  std::vector<std::size_t> triangles;
  std::vector<STLPoint>::iterator left, right;
};

struct STLTriangle {
  size_t points[3];
  float normal[3];
  double uBeta[3], uGamma[3];
  double d, kBeta, kGamma;

};

template <typename T>
class STLMesh {
public:
  //  STLMesh(std::string, SuperStructure3D<T>& sS, bool split=false);
  STLMesh(std::string, SuperStructure3D<T>& sS, int split = 1);

  void print(bool full = false);
  void write();
  void distribute();
  inline int pointSize() const {
    return _points.size();
  }
  inline int triangleSize() const {
    return _triangles.size();
  }
  inline float maxDist() const {
    return _maxDist;
  }
  inline const STLPoint& getPoint(int i) const {
    OLB_ASSERT(i < pointSize(), "STLPoint out of range.");
    return _points[i];
  }
  inline const STLTriangle& getTriangle(int i) const {
    OLB_ASSERT(i < triangleSize(), "STLTriangle out of range.");
    return _triangles[i];
  }

  std::vector<STLPoint> _points;
  std::vector<STLTriangle> _triangles;

private:
  void init();
  bool findPoint(const STLPoint& p, size_t& n);
  size_t addPoint(STLPoint& p);
  double distPoints(size_t p1, size_t p2);
  double distPoints(std::vector<STLPoint>::iterator p1, STLPoint& p2);
  double height_A(const std::vector<double>& A, const std::vector<double>& B, const std::vector<double>& C);
  bool AABBTri(const STLPoint& point0, const STLPoint& point1, const STLPoint& point2,
               const float normal[], const std::vector<T>& c, const std::vector<T>& e);
  void initTri(STLTriangle& tri);


  std::vector<STLPoint>::iterator _tree;
  void nearestPoint(std::vector<STLPoint>::iterator root, STLPoint &nd, int i, STLPoint **best, double &best_dist);
  std::vector<STLPoint>::iterator findMedian(std::vector<STLPoint>::iterator start, std::vector<STLPoint>::iterator end, int idx);
  std::vector<STLPoint>::iterator makeTree(std::vector<STLPoint>::iterator start, size_t len, int i);
  inline void swap(std::vector<STLPoint>::iterator x, std::vector<STLPoint>::iterator y) {
    std::swap(x->r, y->r);
    std::swap(x->triangles, y->triangles);
  }

  SuperStructure3D<T>& _sS;
  std::string _fName;
  double _maxDist;
  bool _distributed;
  mutable OstreamManager clout;
};

//template <typename T>
//STLMesh<T>::STLMesh(std::string fname, SuperStructure3D<T>& sS, bool split): _sS(sS), _fName(fname), _maxDist(0), _distributed(split), clout(std::cout, "STLMesh") {
//  init();
//}
//
//template <typename T>
//void STLMesh<T>::init() {
//  std::ifstream f(_fName.c_str(), std::ios::in) ;
//  if (!f.good())
//    throw runtime_error("STL File not valid.");
//  char buf[6];
//  buf[5] = 0;
//  f.read(buf, 5);
//  const std::string asciiHeader = "solid";
//  if (std::string(buf) == asciiHeader) {
//    f.seekg(0, std::ios::beg);
//    if (f.good()) {
//      char title[80];
//      std::string s0, s1;
//      f.read(title, 80);
//      while (!f.eof()) {
//        f >> s0;                                // facet || endsolid
//        if (s0 == "facet") {
//          STLTriangle tri;
//          f >> s1 >> tri.normal[0] >> tri.normal[1] >> tri.normal[2]; // normal x y z
//          f >> s0 >> s1;   // outer loop
//          STLPoint p0, p1, p2;
//          f >> s0 >> p0.r[0] >> p0.r[1] >> p0.r[2];       // vertex x y z
//          f >> s0 >> p1.r[0] >> p1.r[1] >> p1.r[2];       // vertex x y z
//          f >> s0 >> p2.r[0] >> p2.r[1] >> p2.r[2];       // vertex x y z
//          f >> s0;                            // endloop
//          f >> s0;                            // endfacet
//
//          tri.points[0] = addPoint(p0);
//          tri.points[1] = addPoint(p1);
//          tri.points[2] = addPoint(p2);
//          _triangles.push_back(tri);
//        } else if (s0 == "endsolid") {
//          break;
//        }
//      }
//    }
//  } else {
//    f.close();
//    f.open(_fName.c_str(), std::ios::in | std::ios::binary);
//    char comment[80];
//    f.read(comment, 80);
//    if (!f.good())
//      throw runtime_error("STL File not valid.");
//    comment[79] = 0;
//
//    int32_t nFacets;
//    f.read(reinterpret_cast<char *>(&nFacets), sizeof(int32_t));
//    if (!f.good())
//      throw runtime_error("STL File not valid.");
//
//    float v[12]; // normal=3, vertices=3*3 = 12
//    unsigned short uint16;
//    // Every Face is 50 Bytes: Normal(3*float), Vertices(9*float), 2 Bytes Spacer
//    for (size_t i = 0; i < nFacets; ++i) {
//      for (size_t j = 0; j < 12; ++j) {
//        f.read(reinterpret_cast<char *>(&v[j]), sizeof(float));
//      }
//      f.read(reinterpret_cast<char *>(&uint16), sizeof(unsigned short)); // spacer between successive faces
//      STLTriangle tri;
//      tri.normal[0] = v[0];
//      tri.normal[1] = v[1];
//      tri.normal[2] = v[2];            // normal x y z
//      STLPoint p;
//      p.r[0] = v[3];
//      p.r[1] = v[4];
//      p.r[2] = v[5];         // vertex x y z
//      tri.points[0] = addPoint(p);
//      p.r[0] = v[6];
//      p.r[1] = v[7];
//      p.r[2] = v[8];         // vertex x y z
//      tri.points[1] = addPoint(p);
//      p.r[0] = v[9];
//      p.r[1] = v[10];
//      p.r[2] = v[11];         // vertex x y z
//      tri.points[2] = addPoint(p);
//      _triangles.push_back(tri);
//    }
//  }
//  f.close();
//
//  for (int i = 0; i < _triangles.size(); i++) {
//    _points[_triangles[i].points[0]].triangles.push_back(i);
//    _points[_triangles[i].points[1]].triangles.push_back(i);
//    _points[_triangles[i].points[2]].triangles.push_back(i);
//
//    double minH = std::min(height_A(_points[_triangles[i].points[0]].r, _points[_triangles[i].points[1]].r, _points[_triangles[i].points[2]].r),
//                 height_A(_points[_triangles[i].points[1]].r, _points[_triangles[i].points[0]].r, _points[_triangles[i].points[2]].r));
//    minH = std::min(minH,  height_A(_points[_triangles[i].points[2]].r, _points[_triangles[i].points[1]].r, _points[_triangles[i].points[0]].r));
//    _maxDist = std::max(minH, _maxDist);
//    initTri(_triangles[i]);
//  }
//
//  write();
//}

template <typename T>
bool STLMesh<T>::findPoint(const STLPoint& p, size_t& n) {
  for (int i = 0; i < _points.size(); i++) {
    if (_points[i].r[0] == p.r[0] && _points[i].r[1] == p.r[1]
        && _points[i].r[2] == p.r[2]) {
      n = i;
      return 1;
    }
  }
  return 0;
}

template <typename T>
void STLMesh<T>::nearestPoint(std::vector<STLPoint>::iterator root,
                              STLPoint &nd, int i, STLPoint **best, double &best_dist) {
  double d, dx, dx2;

  if(root == _points.end()) {
    return;
  }
  d = distPoints(root, nd);
  dx = root->r[i] - nd.r[i];
  dx2 = dx*dx;

  if(!*best || d < best_dist) {
    best_dist = d;
    *best = &(*root);
  }

  if (!best_dist) {
    return;
  }
  if(++i >= 3) {
    i = 0;
  }

  nearestPoint(dx > 0 ? root->left : root->right, nd, i, best, best_dist);
  if (dx2 >= best_dist) {
    return;
  }
  nearestPoint(dx > 0 ? root->right : root->left, nd, i, best, best_dist);

}

template <typename T>
size_t STLMesh<T>::addPoint(STLPoint& p) {
  size_t n = 0;
  if(!findPoint(p, n)) {
    _points.push_back(p);
    n = _points.size()-1;
  }
  return n;

  //  STLPoint **best;
  //  T best_dist = 1.;
  //  size_t n = 0;
  //  _tree = makeTree(_points.begin(), _points.size(), 0);
  //  nearestPoint(_tree, p, 0, best, best_dist);
  //  cout << "BEST_DIST: " << best_dist << std::endl;
  //  if(best_dist){
  //    _points.push_back(p);
  //    n = _points.size()-1;
  //  }
  //  return n;

}

template <typename T>
void STLMesh<T>::print(bool full) {
  if (full) {
    int i=0;
    cout << "POINTS: " << std::endl;
    for (auto p : _points) {
      clout <<i++<<": "<<  p.r[0] << " " << p.r[1] << " " << p.r[2] << std::endl;
    }
    i=0;
    cout << "TRIANGLES: " << std::endl;
    for (auto t : _triangles) {
      cout <<i++<<": "<< t.points[0] << " " << t.points[1] << " " << t.points[2]
           << std::endl;
    }
  }
  cout << "#POINTS: " << _points.size() << " #TRIANGLES: "
       << _triangles.size() << " _maxDist: " << _maxDist << std::endl;
}

template <typename T>
void STLMesh<T>::write() {
  if(_distributed || singleton::mpi().getRank()==0) {
    ofstream f;
    f.open ("tmp/"+_fName.substr(0,_fName.length()-4)+"_"+std::to_string(singleton::mpi().getRank())+".stl");
    f << "solid ascii " << _fName.substr(0,_fName.length()-4)<<"_"<<singleton::mpi().getRank() << "\n";
    for (int i=0; i<_triangles.size(); i++) {
      f << "facet normal " << _triangles[i].normal[0] << " " << _triangles[i].normal[1] << " " << _triangles[i].normal[2] << "\n";
      f << "    outer loop\n";
      f << "        vertex " << _points[_triangles[i].points[0]].r[0] << " " << _points[_triangles[i].points[0]].r[1] << " " << _points[_triangles[i].points[0]].r[2] << "\n";
      f << "        vertex " << _points[_triangles[i].points[1]].r[0] << " " << _points[_triangles[i].points[1]].r[1] << " " << _points[_triangles[i].points[1]].r[2] << "\n";
      f << "        vertex " << _points[_triangles[i].points[2]].r[0] << " " << _points[_triangles[i].points[2]].r[1] << " " << _points[_triangles[i].points[2]].r[2] << "\n";
      f << "    endloop\n";
      f << "endfacet\n";
    }
    f.close();
  }
}

template <typename T>
double STLMesh<T>::distPoints(size_t p1, size_t p2) {
  return std::pow(double(_points[p1].r[0] - _points[p2].r[0]), 2)
         + std::pow(double(_points[p1].r[1] - _points[p2].r[1]), 2)
         + std::pow(double(_points[p1].r[2] - _points[p2].r[2]), 2);
}

template <typename T>
double STLMesh<T>::distPoints(std::vector<STLPoint>::iterator p1, STLPoint& p2) {
  return std::pow(double(p1->r[0] - p2.r[0]), 2)
         + std::pow(double(p1->r[1] - p2.r[1]), 2)
         + std::pow(double(p1->r[2] - p2.r[2]), 2);
}


template <typename T>
void STLMesh<T>::initTri(STLTriangle& tri) {
  std::vector<double> A = _points[tri.points[0]].r;
  std::vector<double> B = _points[tri.points[1]].r;
  std::vector<double> C = _points[tri.points[2]].r;
  std::vector<double> b(3, 0.), c(3, 0.);
  double bb = 0., bc = 0., cc = 0.;
  for (int i = 0; i < 3; i++) {
    b[i] = B[i] - A[i];
    c[i] = C[i] - A[i];
    bb += b[i] * b[i];
    bc += b[i] * c[i];
    cc += c[i] * c[i];
  }

  tri.normal[0] = b[1]*c[2] - b[2] * c[1];
  tri.normal[1] = b[2]*c[0] - b[0] * c[2];
  tri.normal[2] = b[0]*c[1] - b[1] * c[0];
  double norm = sqrt(std::pow(tri.normal[0], 2) + std::pow(tri.normal[1], 2) + std::pow(tri.normal[2], 2));
  tri.normal[0] /= norm;
  tri.normal[1] /= norm;
  tri.normal[2] /= norm;

  double D = 1.0 / (cc * bb - bc * bc);
  double bbD = bb * D;
  double bcD = bc * D;
  double ccD = cc * D;
  tri.kBeta = 0.;
  tri.kGamma = 0.;
  tri.d = 0.;
  for (int i = 0; i < 3; i++) {
    tri.uBeta[i] = b[i] * ccD - c[i] * bcD;
    tri.uGamma[i] = c[i] * bbD - b[i] * bcD;
    tri.kBeta -= A[i] * tri.uBeta[i];
    tri.kGamma -= A[i] * tri.uGamma[i];
    tri.d += A[i] * tri.normal[i];
  }
}

template <typename T>
double STLMesh<T>::height_A(const std::vector<double>& A, const std::vector<double>& B, const std::vector<double>& C) {
  std::vector<double> BC(3,0.);
  double d = 0., BCB = 0., BCnorm2 = 0.;
  for (int i=0; i<3; i++) {
    BC[i] = C[i]-B[i];
    d+=BC[i]*A[i];
    BCB+=BC[i]*B[i];
    BCnorm2+=BC[i]*BC[i];
  }
  double alpha = (d-BCB)/BCnorm2;
  std::vector<double> h_A(3,0.);
  for (int i=0; i<3; i++) {
    h_A[i]=A[i]-(B[i]+alpha*BC[i]);
  }
  return (h_A[0]*h_A[0] + h_A[1]*h_A[1] + h_A[2]*h_A[2]);
}

template <typename T>
STLMesh<T>::STLMesh(std::string fname, SuperStructure3D<T>& sS, int split): _sS(sS), _fName(fname), _maxDist(0), clout(std::cout, "STLMesh") {
  clout << " begun reading " << endl;
  std::vector<std::vector<T> > c, e;
  for(int i = 0; i<_sS.getLoadBalancer().size(); i++) {
    std::vector<T> c0(3,0), e0(3,0);
    c0[0] = (2 * _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getOrigin()[0] +
             _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getDeltaR()*(_sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getNx()-1))*.5;
    c0[1] = (2 * _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getOrigin()[1] +
             _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getDeltaR()*(_sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getNy()-1))*.5;
    c0[2] = (2 * _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getOrigin()[2] +
             _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getDeltaR()*(_sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getNz()-1))*.5;
    e0[0] = c0[0] - _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getOrigin()[0] + 2*_sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getDeltaR();
    e0[1] = c0[1] - _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getOrigin()[1] + 2*_sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getDeltaR();
    e0[2] = c0[2] - _sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getOrigin()[2] + 2*_sS.getCuboidGeometry().get(_sS.getLoadBalancer().glob(i)).getDeltaR();
    c.push_back(c0);
    e.push_back(e0);
  }


  std::ifstream f(_fName.c_str(), std::ios::in) ;
  if (!f.good()) {
    throw runtime_error("STL File not valid.");
  }
  char buf[6];
  buf[5] = 0;
  f.read(buf, 5);
  const std::string asciiHeader = "solid";
  if (std::string(buf) == asciiHeader) {
    clout << "ASCII" << std::endl;
    f.seekg(0, std::ios::beg);
    if (f.good()) {
      std::string s0, s1;
      while (!f.eof()) {
        f >> s0;     // facet || endsolid
        if (s0 == "facet") {
          STLTriangle tri;
          f >> s1 >> tri.normal[0] >> tri.normal[1] >> tri.normal[2]; // normal x y z
          f >> s0 >> s1;   // outer loop
          STLPoint point0(_points.end()), point1(_points.end()), point2(_points.end());
          f >> s0 >> point0.r[0] >> point0.r[1] >> point0.r[2];       // vertex x y z
          f >> s0 >> point1.r[0] >> point1.r[1] >> point1.r[2];       // vertex x y z
          f >> s0 >> point2.r[0] >> point2.r[1] >> point2.r[2];       // vertex x y z
          f >> s0;                            // endloop
          f >> s0;                            // endfacet

          /* Test intersection cuboids - triangle*/
          for(int i = 0; i<_sS.getLoadBalancer().size(); i++) {
            if (AABBTri(point0, point1, point2, tri.normal, c[i], e[i])) {
              tri.points[0] = addPoint(point0);
              tri.points[1] = addPoint(point1);
              tri.points[2] = addPoint(point2);
              _triangles.push_back(tri);
            }
          }
        } else if (s0 == "endsolid") {
          break;
        }
      }
    }
  } else {
    clout << "BINARY" << std::endl;
    f.close();
    f.open(_fName.c_str(), std::ios::in | std::ios::binary);
    char comment[80];
    f.read(comment, 80);
    if (!f.good()) {
      throw runtime_error("STL File not valid.");
    }
    comment[79] = 0;

    int32_t nFacets;
    f.read(reinterpret_cast<char *>(&nFacets), sizeof(int32_t));
    if (!f.good()) {
      throw runtime_error("STL File not valid.");
    }

    float v[12]; // normal=3, vertices=3*3 = 12
    unsigned short uint16;
    // Every Face is 50 Bytes: Normal(3*float), Vertices(9*float), 2 Bytes Spacer
    for (size_t i = 0; i < nFacets; ++i) {
      for (size_t j = 0; j < 12; ++j) {
        f.read(reinterpret_cast<char *>(&v[j]), sizeof(float));
      }
      f.read(reinterpret_cast<char *>(&uint16), sizeof(unsigned short)); // spacer between successive faces
      STLTriangle tri;
      tri.normal[0] = v[0];
      tri.normal[1] = v[1];
      tri.normal[2] = v[2];            // normal x y z
      STLPoint point0(_points.end()), point1(_points.end()), point2(_points.end());
      point0.r[0] = v[3];
      point0.r[1] = v[4];
      point0.r[2] = v[5];         // vertex x y z
      point1.r[0] = v[6];
      point1.r[1] = v[7];
      point1.r[2] = v[8];         // vertex x y z
      point2.r[0] = v[9];
      point2.r[1] = v[10];
      point2.r[2] = v[11];         // vertex x y z
      /* Test intersection cuboids - triangle*/
      for(int i = 0; i<_sS.getLoadBalancer().size(); i++) {
        if (AABBTri(point0, point1, point2, tri.normal, c[i], e[i])) {
          tri.points[0] = addPoint(point0);
          tri.points[1] = addPoint(point1);
          tri.points[2] = addPoint(point2);
          _triangles.push_back(tri);
        }
      }
    }
  }
  f.close();

  for (int i = 0; i < _triangles.size(); i++) {
    _points[_triangles[i].points[0]].triangles.push_back(i);
    _points[_triangles[i].points[1]].triangles.push_back(i);
    _points[_triangles[i].points[2]].triangles.push_back(i);

    double minH = std::min(height_A(_points[_triangles[i].points[0]].r, _points[_triangles[i].points[1]].r, _points[_triangles[i].points[2]].r),
                           height_A(_points[_triangles[i].points[1]].r, _points[_triangles[i].points[0]].r, _points[_triangles[i].points[2]].r));
    minH = std::min(minH,  height_A(_points[_triangles[i].points[2]].r, _points[_triangles[i].points[1]].r, _points[_triangles[i].points[0]].r));
    _maxDist = std::max(minH, _maxDist);
    initTri(_triangles[i]);
  }
  _distributed = true;

  clout << " end reading " << endl;
}

template <typename T>
bool STLMesh<T>::AABBTri(const STLPoint& point0, const STLPoint& point1, const STLPoint& point2,
                         const float normal[], const std::vector<T>& c, const std::vector<T>& e) {
  std::vector<T> v0(3,0), v1(3,0), v2(3,0), f0(3,0), f1(3,0), f2(3,0);
  T d = point0.r[0]*normal[0]+point0.r[2]*normal[1]+point0.r[2]*normal[2];
  /* Test intersection cuboids - triangle
   * Intersection test after Christer Ericson - Real time Collision Detection p.*/
  for(int j=0; j<3; j++) {
    v0[j] = point0.r[j]-c[j];
    v1[j] = point1.r[j]-c[j];
    v2[j] = point2.r[j]-c[j];
  }
  for(int j=0; j<3; j++) {
    f0[j] = v1[j] - v0[j];
    f1[j] = v2[j] - v1[j];
    f2[j] = v0[j] - v2[j];
  }

  T p0=0, p1=0, r=0;
  //test a00
  p0 = v0[2]*v1[1] - v0[1]*v1[2];
  p1 = v2[2]*f0[1] - v2[2]*f0[2];
  r = e[1] * fabs(f0[2]) + e[2]*fabs(f0[1]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a01
  p0 = v0[1]*v1[2]-v0[1]*v2[2]-v0[2]*v1[1]+v0[2]*v2[1];
  p1 = -v1[1]*v2[2]+v1[2]*v2[1];
  r = e[1] * fabs(f1[2]) + e[2]*fabs(f1[1]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a02
  p0 = v0[1]*v2[2]-v0[2]*v2[1];
  p1 = v0[1]*v1[2]-v0[2]*v1[1]+v1[1]*v2[2]-v1[2]*v2[1];
  r = e[1]*fabs(f2[2]) + e[2]*fabs(f2[1]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a10
  p0 = v0[0]*v1[2]-v0[2]*v1[0];
  p1 = v0[0]*v2[2]-v0[2]*v2[0]-v1[0]*v2[2]+v1[2]*v2[0];
  r = e[0]*fabs(f0[2]) + e[2]*fabs(f0[0]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a11
  p0 = -v0[0]*v1[2]+v0[0]*v2[2]+v0[2]*v1[0]-v0[2]*v2[0];
  p1 = v1[0]*v2[2]-v1[2]*v2[0];
  r = e[0]*fabs(f1[2])+e[2]*fabs(f1[0]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a12
  p0 = -v0[0]*v2[2]+v0[2]*v2[0];
  p1 = -v0[0]*v1[2]+v0[2]*v1[0]-v1[0]*v2[2]+v1[2]*v2[0];
  r = e[0]*fabs(f2[2])+e[2]*fabs(f2[0]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a20
  p0 = -v0[0]*v1[1]+v0[1]*v1[0];
  p1 = -v0[0]*v2[1]+v0[1]*v2[0]+v1[0]*v2[1]-v1[1]*v2[0];
  r = e[0]*fabs(f0[1])+e[1]*fabs(f0[0]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a21
  p0 = v0[0]*v1[1]-v0[0]*v2[1]-v0[1]*v1[0]+v0[1]*v2[0];
  p1 = -v1[0]*v2[1]+v1[1]*v2[0];
  r = e[0]*fabs(f1[1])+e[1]*fabs(f1[0]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  // test a22
  p0 = v0[0]*v2[1]-v0[1]*v2[0];
  p1 = v0[0]*v1[1]-v0[1]*v1[0]+v1[0]*v2[1]-v1[1]*v2[0];
  r = e[0]*fabs(f2[1])+e[1]*fabs(f2[0]);
  if (max(-max(p0, p1), min(p0, p1)) > r) {
    return false;
  }

  if(max(max(v0[0], v1[0]), v2[0]) < -e[0] || min(min(v0[0], v1[0]), v2[0]) > e[0]) {
    return false;
  }
  if(max(max(v0[1], v1[1]), v2[1]) < -e[1] || min(min(v0[1], v1[1]), v2[1]) > e[1]) {
    return false;
  }
  if(max(max(v0[2], v1[2]), v2[2]) < -e[2] || min(min(v0[2], v1[2]), v2[2]) > e[2]) {
    return false;
  }


  /* Test intersection cuboids - triangle plane*/
  r = e[0]*fabs(normal[0]) + e[1]*fabs(normal[1]) + e[2]*fabs(normal[2]);
  T s =  normal[0]*c[0] + normal[1]*c[1] + normal[2]*c[2] - d;
  return true;

  return (fabs(s) <= fabs(r));
}


template <typename T>
std::vector<STLPoint>::iterator STLMesh<T>::findMedian(std::vector<STLPoint>::iterator start, std::vector<STLPoint>::iterator end, int idx) {
  if (end >=start) {
    return _points.end();
  }
  if (end == start+1) {
    return start;
  }
  std::vector<STLPoint>::iterator p, store, md = start + (end-start) / 2;
  T pivot;
  while(true) {
    pivot = md->r[idx];
    swap(md, end-1);
    for (store = p = start; p<end; p++) {
      if(p->r[idx] < pivot) {
        if (p != store) {
          swap(p, store);
        }
        store++;
      }
    }
    swap(store, end-1);

    if(store->r[idx] == md->r[idx]) {
      return md;
    }
    if(store > md) {
      end = store;
    }
    else {
      start = store;
    }
  }
}

template <typename T>
std::vector<STLPoint>::iterator STLMesh<T>::makeTree(std::vector<STLPoint>::iterator t, size_t len, int i) {
  std::vector<STLPoint>::iterator n = _points.end();
  if(!len) {
    return _points.end();
  }
  if ((n == findMedian(t, t+len, i))) {
    i = (i+1) % 3;
    n->left  = makeTree(t, n-t, i);
    n->right = makeTree(n+1, t+len - (n+1), i);
  }
  return n;
}

}
#endif
