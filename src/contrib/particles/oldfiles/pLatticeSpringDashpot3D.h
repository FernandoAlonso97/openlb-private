/*
*  Copyright (C) 2014 Fabian Klemens, Thomas Henn, Mathias J. Krause
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#ifndef PlatticeSpringDashpot3D_H_
#define PlatticeSpringDashpot3D_H_

#include "force3D.h"
#include "../particleSystem3D.h"
//#include "../particleHelper.hh"
#include <cmath>
#include <unordered_map>

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSystem3D;

template<typename T, template<typename U> class PARTICLETYPE>
class PlatticeSpringDashpot3D : public Force3D<T, PARTICLETYPE> {

public:
  PlatticeSpringDashpot3D(const LBconverter<T>& conv, T Kn, T Dn, T Ks, T Ds, T fric, T sRad);
  virtual ~PlatticeSpringDashpot3D() {};
  void  applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p, const ParticleSystem3D<T, PARTICLETYPE> &pSys);
private:
  T _Kn, _Dn, _Ks, _Ds, _fric, _sRad2;
};

template<typename T, template<typename U> class PARTICLETYPE>
PlatticeSpringDashpot3D<T, PARTICLETYPE>::PlatticeSpringDashpot3D(const LBconverter<T> &conv,
    T Kn, T Dn, T Ks, T Ds, T fric, T sRad)
  :
  Force3D<T, PARTICLETYPE>(conv), _Kn(Kn), _Dn(Dn), _Ks(Ks), _Ds(Ds), _fric(fric) {
  _sRad2 = sRad*sRad;
}

template<typename T, template<typename U> class PARTICLETYPE>
void PlatticeSpringDashpot3D<T, PARTICLETYPE>::applyForce(typename std::vector<PARTICLETYPE<T> >::iterator p, const ParticleSystem3D<T, PARTICLETYPE>& pSys) {

  std::vector<T> pos = p->getPos();
  for (int j=0; j<3; j++) {
    pos[j] -= pSys._physPos[j];
  }
  int iX = floor(pos[0]/pSys._range);
  int iY = floor(pos[1]/pSys._range);
  int iZ = floor(pos[2]/pSys._range);

  std::list<unsigned int> pNums;
  std::list<unsigned int>::const_iterator it;

  for (int i=-1; i<=1; i++) {
    for (int j=-1; j<=1; j++) {
      for (int k=-1; k<=1; k++) {
        if (iX+i > 0 && iY+j> 0 && iZ+k >0 && iX+i < pSys._nExtend[0] && iY+j < pSys._nExtend[1] && iZ+k < pSys._nExtend[2]) {
          try {
            it = pSys._pLattice.at(iX+i).at(iY+j).at(iZ+k).begin();
            for ( it = pSys._pLattice.at(iX+i).at(iY+j).at(iZ+k).begin(); it!= pSys._pLattice.at(iX+i).at(iY+j).at(iZ+k).end(); it++) {
              pNums.push_back(*it);
            }
          } catch (std::exception& e) {
            //            std::cerr << "exception caught: " << e.what() << '\n';
          }
        }
      }
    }
  }

  if(pNums.size()!=0) {
    cout << "pNums.size(): " << pNums.size() << endl;
  }

  it = pNums.begin();
  //  for (int i=0; i<pNums.size(); i++) {
  for (it = pNums.begin(); it!= pNums.end(); it++) {
    const PARTICLETYPE<T> &p2 = pSys[*it];
    T length = std::pow(p->getPos()[0]-p2.getPos()[0],2) +
               std::pow(p->getPos()[1]-p2.getPos()[1],2) +
               std::pow(p->getPos()[2]-p2.getPos()[2],2);
    if (length != 0 && length < (pow(p->getRad()+p2.getRad(), 2))) {      //distance != 0
      length = sqrt(length);
      T delta = p2.getRad() + p->getRad() - length; //contact/overlap
      T M = p->getMass()*p2.getMass() / (p->getMass() + p2.getMass());   //reduced mass
      std::vector<T> pvel=p->getVel(), p2vel=p2.getVel();
      std::vector<T> ppos=p->getPos(), p2pos=p2.getPos();
      std::vector<T> velR(3,0.);
      std::vector<T> d(3,0.), s(3,0.), normal(3,0.), tangent(3,0.);
      for (int i=0; i< 3; i++) {
        velR[i] = pvel[i]-p2vel[i];
        d[i] = ppos[i]-p2pos[i];
      }
      s = crossProduct3D<T>(crossProduct3D<T>(velR, normal), normal);     //tangent vector perpendicular to d

      if (d[0]*d[0]+d[1]*d[1]+d[2]*d[2] >0 ) {
        normal = normalize(d);
      } else {
        print(normal);
      }
      if (s[0]*s[0]+s[1]*s[1]+s[2]*s[2] >0 ) {
        tangent = normalize(s);
      }


      T velN = phelper::innerProduct<T>(velR, normal);    //normal component of relative velocity
      T velS = phelper::innerProduct<T>(velR, tangent);   //missing term because angular velocity = 0

      T uS = velS*this->_converter.getDeltaT();
      T hS = -2. *M* (_Ks*uS+_Ds*velS);

      T forceN, forceT;
      forceN = 2.*M*(_Kn*delta + M*_Dn*velN);
      if(hS*hS < _fric*_fric*forceN*forceN) {
        forceT = hS;
      } else {
        forceT = _fric*forceN;
      }


      std::vector<T> frc(3,0);
      for(int j=0; j<3; j++) {
        frc[j] += forceN*normal[j] + forceT*tangent[j];
      }
      p->addForce(frc);
    }
  }
}



}



#endif
