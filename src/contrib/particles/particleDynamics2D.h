/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2006-2016 Thomas Henn, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PARTICLEDYNAMICS_2D_H
#define PARTICLEDYNAMICS_2D_H

#include "functors/lattice/indicator/indicatorBaseF2D.h"


namespace olb {

template<typename T, template<typename U> class DESCRIPTOR>
class ParticleDynamics2D {
private:
  SuperLattice2D<T, DESCRIPTOR>& _sLattice;
  UnitConverter<T,DESCRIPTOR> const& _converter;
  SuperGeometry2D<T>& _superGeometry;
  std::vector<ParticleIndicatorF2D<T,T>* > _vectorOfIndicator;
  T _g;
  T _lengthX;
  T _lengthY;

public:
  ParticleDynamics2D(SuperLattice2D<T, DESCRIPTOR>& sLattice,
                     UnitConverter<T,DESCRIPTOR> const& converter,
                     SuperGeometry2D<T>& superGeometry, T g, T lengthX, T lengthY)
    : _sLattice(sLattice),
      _converter(converter),
      _superGeometry(superGeometry),
      _g(g),
      _lengthX(lengthX),
      _lengthY(lengthY)
  {

  }

  void addParticle(ParticleIndicatorF2D<T, T>& indicator);

  void computeBoundaryForce(std::vector<ParticleIndicatorF2D<T,T>* >& indicator);

//  void lennardJonesColl(SmoothIndicatorF2D<T, T>& indicator,
//                          const std::vector<T>& pos2, T delta = 0.)

  void addWallColl(ParticleIndicatorF2D<T, T>& indicator, T delta);

//  void addCollisionModel(ParticleIndicatorF2D<T, T>& indicator,
//      ParticleIndicatorF2D<T, T>& indicator2);

//  void eulerIntegration(ParticleIndicatorF2D<T, T>& indicator);

  void verletIntegration(ParticleIndicatorF2D<T, T>& indicator);

  void updateParticleDynamics(std::string name, ParticleIndicatorF2D<T, T>& indicator);

//  void addParticleColl(ParticleIndicatorF2D<T, T>& indicator,
//      ParticleIndicatorF2D<T, T>& indicator2, T delta);

  void addParticleField(ParticleIndicatorF2D<T, T>& indicator);

  void simulateTimestep(std::string name);
};

}

#endif
