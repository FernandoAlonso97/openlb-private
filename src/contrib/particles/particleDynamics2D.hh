/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2006-2016 Thomas Henn, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */



#ifndef PARTICLEDYNAMICS_2D_HH
#define PARTICLEDYNAMICS_2D_HH

#include "particleDynamics2D.h"

namespace olb {

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::addParticle(ParticleIndicatorF2D<T, T>& indicator)
{
  _vectorOfIndicator.push_back(&indicator);
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::computeBoundaryForce(std::vector<ParticleIndicatorF2D<T,T>* >& indicator)
{
  SuperLatticeMomentumExchangeForce2D<T, DESCRIPTOR> force(_sLattice, _superGeometry, indicator,_converter);
  T sumF[force.getTargetDim()];
  for (int i=0; i<force.getTargetDim(); i++) {
    sumF[i]=0.;
  }
  int input[1];
  force(sumF, input);
  for (typename std::vector<ParticleIndicatorF2D<T,T>* >::size_type iInd=0; iInd!=indicator.size(); iInd++) {
    /// get particle acceleration through boundary force and gravity (and buoyancy)
    indicator[iInd]->getAcc2()[0] = sumF[0+4*iInd] / indicator[iInd]->getMass();
    indicator[iInd]->getAcc2()[1] = sumF[1+4*iInd] / indicator[iInd]->getMass() - _g;
    indicator[iInd]->getAlpha2() = sumF[2+4*iInd] / indicator[iInd]->getMofi();
  }
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::addWallColl(ParticleIndicatorF2D<T, T>& indicator, T delta)
{
  std::vector<T> dx(2, T());

  T w1 = 1e-7 / 2.;
  T w = 1e-7 / 2.;

  T rad = indicator.getRadius();
  T massInv = 1. / indicator.getMass();

  dx[0] = _lengthX - indicator.getPos()[0];
  dx[1] = _lengthY - indicator.getPos()[1];

  for (int i = 0; i < 2; i++) {
    if (dx[i] <= rad) {
      indicator.getAcc2()[i] += massInv * -dx[i] * (rad - dx[i]) / w1;
    }
    if (indicator.getPos()[i] <= rad) {
      indicator.getAcc2()[i] += massInv * indicator.getPos()[i] * (rad - indicator.getPos()[i]) / w1;
    }
    if (dx[i] > rad && dx[i] <= rad + delta) {
      indicator.getAcc2()[i] += massInv * -dx[i] * std::pow((rad + delta - dx[i]), 2) / w;
    }
    if (indicator.getPos()[i] > rad && indicator.getPos()[i] <= rad + delta) {
      indicator.getAcc2()[i] += massInv * indicator.getPos()[i] * std::pow((rad + delta - indicator.getPos()[i]), 2) / w;
    }
  }
}

//template<typename T, template<typename U> class DESCRIPTOR>
//void ParticleDynamics2D<T, DESCRIPTOR>::addCollisionModel(ParticleIndicatorF2D<T, T>& indicator,
//    ParticleIndicatorF2D<T, T>& indicator2) {
//this->addParticleColl(indicator, indicator2, _converter.getLatticeL());
//this->addWallColl(indicator, 10. * _converter.getLatticeL());
//}

//template<typename T, template<typename U> class DESCRIPTOR>
//void ParticleDynamics2D<T, DESCRIPTOR>::eulerIntegration(ParticleIndicatorF2D<T, T>& indicator) {
//    T time = _converter.physTime();
//
//    for (int i = 0; i < 2; i++) {
//      _vel[i] += _A[i] * time;
//      _pos[i] += _vel[i] * time;
//
//    }
//    _omega += _A[2] * time;   //angular velocity
//    _theta += _omega * time;  //angle
//  }

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::verletIntegration(ParticleIndicatorF2D<T, T>& indicator)
{
  T time = _converter.physTime();
  for (int i=0; i<2; i++) {
    indicator.getPos()[i] += indicator.getVel()[i] * time + (0.5 * indicator.getAcc()[i] * time * time);
    T avgAcc = (indicator.getAcc()[i] + indicator.getAcc2()[i]) * 0.5;
    indicator.getVel()[i] += avgAcc * time;
    indicator.getAcc()[i] = indicator.getAcc2()[i];
  }
  indicator.getTheta() += indicator.getOmega() * time + (0.5 * indicator.getAlpha() * time * time);
  T avgAlpha = (indicator.getAlpha() + indicator.getAlpha2()) * 0.5;
  indicator.getOmega() += avgAlpha * time;
  indicator.getAlpha() = indicator.getAlpha2();

  indicator.getRotationMat()[0] = std::cos(indicator.getTheta());
  indicator.getRotationMat()[1] = -std::sin(indicator.getTheta());
  indicator.getRotationMat()[2] = std::sin(indicator.getTheta());
  indicator.getRotationMat()[3] = std::cos(indicator.getTheta());
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::updateParticleDynamics(std::string name, ParticleIndicatorF2D<T, T>& indicator)
{
//    if (name == "euler")
//      this->eulerIntegration(indicator);
//    else if (name == "verlet")
  this->verletIntegration(indicator);
//    else
//      std::cout << "ERROR: no valid integration...use 'euler' or 'verlet'"
//                << std::endl;
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::addParticleField(ParticleIndicatorF2D<T, T>& indicator)
{
  /// converting in lattice velocities
  std::vector<T> velL(2, T());
  velL[0] = _converter.getLatticeVelocity(indicator.getVel()[0]);
  velL[1] = _converter.getLatticeVelocity(indicator.getVel()[1]);
  T omegaL = _converter.getLatticeVelocity(indicator.getOmega());

  /// Analytical2D functor for particle motion (trans+rot)
  ParticleU2D<T, T> velocity(indicator, velL, omegaL);
  _sLattice.setExternalParticleField(_superGeometry, velocity, indicator);
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics2D<T, DESCRIPTOR>::simulateTimestep(std::string name)
{
  computeBoundaryForce(_vectorOfIndicator);
  for (int i=0; i<_vectorOfIndicator.size(); i++) {
    updateParticleDynamics(name, *(_vectorOfIndicator[i]));
    addParticleField(*(_vectorOfIndicator[i]));
  }
}

//template<typename T, template<typename U> class DESCRIPTOR>
//void ParticleDynamics2D<T, DESCRIPTOR>::addParticleColl(ParticleIndicatorF2D<T, T>& indicator,
//    ParticleIndicatorF2D<T, T>& indicator2, T delta) {
//  Vector<T, 2> pos2;
//
//  T e1 = 1e-7;
//  T e = 1e-7;
//  T massInv = 1. / indicator.getMass();
//
//  T rad = indicator.getRadius() + delta;
//  pos2 = indicator2.getPos();
//  T dist2 = std::pow(indicator.getPos()[0] - pos2[0], 2) + std::pow(indicator.getPos()[1] - pos2[1], 2);
//
//  if (dist2 <= std::pow(2. * rad, 2)) {
//    indicator.getAcc2()[0] += massInv * (indicator.getPos()[0] - pos2[0]) * (2. * rad - std::sqrt(dist2))
//        / e1;
//    indicator.getAcc2()[1] += massInv * (indicator.getPos()[1] - pos2[1]) * (2. * rad - std::sqrt(dist2))
//        / e1;
//  }
//  else if (std::pow(2 * rad, 2) < dist2
//      && dist2 <= std::pow(2 * rad + delta, 2)) {
//    indicator.getAcc2()[0] += massInv * (indicator.getPos()[0] - pos2[0])
//        * std::pow((2. * rad + delta - std::sqrt(dist2)), 2) / e;
//    indicator.getAcc2()[1] += massInv * (indicator.getPos()[1] - pos2[1])
//        * std::pow((2. * rad + delta - std::sqrt(dist2)), 2) / e;
//  }
//}

}

#endif
