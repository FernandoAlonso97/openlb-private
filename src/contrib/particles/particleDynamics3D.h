/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2006-2016 Thomas Henn, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef PARTICLEDYNAMICS_3D_H
#define PARTICLEDYNAMICS_3D_H

#include "functors/analytical/indicator/smoothIndicatorBaseF3D.h"

namespace olb {


template<typename T, template<typename U> class DESCRIPTOR>
class ParticleDynamics3D {
 private:
  SuperLattice3D<T, DESCRIPTOR>& _sLattice;
  LBconverter<T> const& _converter;
  SuperGeometry3D<T>& _superGeometry;
  T _lengthX;
  T _lengthY;
  T _lengthZ;
  T _g;

 public:
  ParticleDynamics3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, LBconverter<T> const& converter,
                   SuperGeometry3D<T>& superGeometry, T lengthX, T lengthY, T lengthZ, T g)
      : _sLattice(sLattice), _converter(converter),
        _superGeometry(superGeometry), _lengthX(lengthX), _lengthY(lengthY), _lengthZ(lengthZ),
		_g(g) {}

  void computeBoundaryForce(std::vector<ParticleIndicatorF3D<T,T>* >& indicator);

//  void addParticleColl(SmoothIndicatorF3D<T, T>* indicator1,
//                       SmoothIndicatorF3D<T, T>* indicator2, T delta);
//
////  void lennardJonesColl(SmoothIndicatorF2D<T, T>& indicator,
////                          const std::vector<T>& pos2, T delta = 0.)

  void addWallColl(ParticleIndicatorF3D<T, T>& indicator, T delta);

//  void addCollisionModel(SmoothIndicatorF3D<T, T>* indicator,
//                         SmoothIndicatorF3D<T, T>* indicator2);
//
//  void eulerIntegration(SmoothIndicatorF3D<T, T>* indicator);
//
  void verletIntegration(ParticleIndicatorF3D<T, T>& indicator);

  void updateParticleDynamics(ParticleIndicatorF3D<T, T>& indicator, std::string name);

  void addParticleField(ParticleIndicatorF3D<T, T>& indicator);
};

}

#endif
