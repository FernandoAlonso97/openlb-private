/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2006-2016 Thomas Henn, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */



#ifndef PARTICLEDYNAMICS_3D_HH
#define PARTICLEDYNAMICS_3D_HH

#include "particleDynamics3D.h"

namespace olb {

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics3D<T, DESCRIPTOR>::computeBoundaryForce(std::vector<ParticleIndicatorF3D<T,T>* >& indicator)
{
  SuperLatticeMomentumExchangeForce3D<T, DESCRIPTOR> force(_sLattice, _superGeometry, indicator,_converter);
//  SuperLatticePhysBoundaryForceIndicator3D<T, DESCRIPTOR> force(_sLattice, _superGeometry, indicator,  _converter);
//  SuperLatticePhysBoundaryTorqueIndicator3D<T, DESCRIPTOR> torque(_sLattice, _superGeometry, indicator,  _converter);
//  SuperSumIndicator3D<T> sumForce(force, _superGeometry, indicator);
//  SuperSumIndicator3D<T> sumTorque(torque, _superGeometry, indicator);
//
//  T sumF[5] = { T() };
//  T sumT[5] = { T() };
//  int input[1];
//  sumForce(sumF, input);
//  sumTorque(sumT, input);
//
  T sumF[7*indicator.size()] = { T() };
  int input[1];
  force(sumF, input);
  for (typename std::vector<ParticleIndicatorF3D<T,T>* >::size_type iInd=0; iInd!=indicator.size(); iInd++) {
  /// get particle acceleration through boundary force and gravity (and buoyancy)
  indicator[iInd]->getAcc2()[0] = sumF[0+7*iInd] / indicator[iInd]->getMass();
  indicator[iInd]->getAcc2()[1] = sumF[1+7*iInd] / indicator[iInd]->getMass();
  indicator[iInd]->getAcc2()[2] = sumF[2+7*iInd] / indicator[iInd]->getMass() - _g;

  indicator[iInd]->getAlpha2()[0] = sumF[3+7*iInd] / indicator[iInd]->getMofi()[0];
  indicator[iInd]->getAlpha2()[1] = sumF[4+7*iInd] / indicator[iInd]->getMofi()[1];
  indicator[iInd]->getAlpha2()[2] = sumF[5+7*iInd] / indicator[iInd]->getMofi()[2];
  }
}

//  void lennardJonesColl(SmoothIndicatorF3D<T, T>& indicator,
//                        const std::vector<T>& pos2, T delta = 0.) {
//    T dist = std::sqrt(
//        std::pow(_pos[0] - pos2[0], 2) + std::pow(_pos[1] - pos2[1], 2));
//    T a = radius;
//    if (delta == 0.) {
//      delta = _converter.getLatticeL();
//    }
//
//    if (dist <= 2 * radius + delta) {
//      for (int i = 0; i < 2; ++i) {
//        _A[i] /*+*/= 2.4 * std::pow(a, 2)
//            * (2 * std::pow(2 * a / dist, 14) - std::pow(2 * a / dist, 8))
//            * ((_pos[i] - pos2[i]) / std::pow(2 * a, 2));
//        _A[i] /= mass;
//      }
//    }
//  }
//  ;

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics3D<T, DESCRIPTOR>::addWallColl(ParticleIndicatorF3D<T, T>& indicator, T delta)
{
  std::vector<T> dx(3, T());

  T w1 = delta * .5;
  T w = delta * .5;

  T rad = indicator.getRadius();  // + delta;
  T massInv = 1. / indicator.getMass();

  dx[0] = _lengthX - _converter.getLatticeL() - indicator.getPos()[0];
  dx[1] = _lengthY - _converter.getLatticeL() - indicator.getPos()[1];
  dx[2] = indicator.getPos()[2] - _converter.getLatticeL();

  for (int i = 0; i < 3; i++) {
    if (dx[i] <= rad) {
      indicator.getAcc2()[i] += massInv * -dx[i] * (rad - dx[i]) / w1;
    }
    if (indicator.getPos()[i] <= rad) {
      indicator.getAcc2()[i] += massInv * indicator.getPos()[i] * (rad - indicator.getPos()[i]) / w1;
    }
    if (dx[i] > rad && dx[i] <= rad + delta) {
      indicator.getAcc2()[i] += massInv * -dx[i] * std::pow((rad + delta - dx[i]), 2) / w;
    }
    if (indicator.getPos()[i] > rad && indicator.getPos()[i] <= rad + delta) {
      indicator.getAcc2()[i] += massInv * indicator.getPos()[i] * std::pow((rad + delta - indicator.getPos()[i]), 2) / w;
    }
  }
}

//void ParticleDynamics3D<T>::addCollisionModel(SmoothIndicatorF3D<T, T>* indicator,
//                       SmoothIndicatorF3D<T, T>* indicator2) {
//  this->addParticleColl(indicator, indicator2, 2*_converter.getLatticeL());
//  this->addWallColl(indicator, 2. * _converter.getLatticeL());  //10.*converter
//}
//
//template <typename T>
//void ParticleDynamics3D<T>::eulerIntegration(SmoothIndicatorF3D<T, T>* indicator) {
//  T time = _converter.physTime();
//
//  for (int i = 0; i < 3; i++) {
//    indicator->getVel()[i] +=  indicator->getAcc2()[i] * time;
//    indicator->getCenter()[i] += indicator->getVel()[i] * time;
//    indicator->getOmega()[i] +=  indicator->getAcc2()[3+i] * time;   //angular velocity
//    indicator->getTheta()[i] += indicator->getOmega()[i] * time;  //angle
//  }
//}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics3D<T, DESCRIPTOR>::verletIntegration(ParticleIndicatorF3D<T, T>& indicator)
{
  T time = _converter.physTime();

  for (int i=0; i<3; i++) {
    indicator.getPos()[i] += indicator.getVel()[i] * time + (0.5 * indicator.getAcc()[i] * time * time);
    T avgAcc = (indicator.getAcc()[i] +  indicator.getAcc2()[i]) * 0.5;
    indicator.getVel()[i] += avgAcc * time;
    indicator.getAcc()[i] =  indicator.getAcc2()[i];
    indicator.getTheta()[i] += indicator.getOmega()[i] * time + (0.5 * indicator.getAlpha()[i] * time * time);
    T avgAlpha = (indicator.getAlpha()[i] +  indicator.getAlpha2()[i]) * 0.5;
    indicator.getOmega()[i] += avgAlpha * time;
    indicator.getAlpha()[i] =  indicator.getAlpha2()[i];
  }

  indicator.getRotationMat()[0] = std::cos(indicator.getTheta()[1]) * std::cos(indicator.getTheta()[2]);
  indicator.getRotationMat()[1] = std::sin(indicator.getTheta()[0])*std::sin(indicator.getTheta()[1])*std::cos(indicator.getTheta()[2]) - std::cos(indicator.getTheta()[0])*std::sin(indicator.getTheta()[2]);
  indicator.getRotationMat()[2] = std::cos(indicator.getTheta()[0])*std::sin(indicator.getTheta()[1])*std::cos(indicator.getTheta()[2]) + std::sin(indicator.getTheta()[0])*std::sin(indicator.getTheta()[2]);
  indicator.getRotationMat()[3] = std::cos(indicator.getTheta()[1])*std::sin(indicator.getTheta()[2]);
  indicator.getRotationMat()[4] = std::sin(indicator.getTheta()[0])*std::sin(indicator.getTheta()[1])*std::sin(indicator.getTheta()[2]) + std::cos(indicator.getTheta()[0])*std::cos(indicator.getTheta()[2]);
  indicator.getRotationMat()[5] = std::cos(indicator.getTheta()[0])*std::sin(indicator.getTheta()[1])*std::sin(indicator.getTheta()[2]) - std::sin(indicator.getTheta()[0])*std::cos(indicator.getTheta()[2]);
  indicator.getRotationMat()[6] = -std::sin(indicator.getTheta()[1]);
  indicator.getRotationMat()[7] = std::sin(indicator.getTheta()[0])*std::cos(indicator.getTheta()[1]);
  indicator.getRotationMat()[8] = std::cos(indicator.getTheta()[0])*std::cos(indicator.getTheta()[1]);

}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics3D<T, DESCRIPTOR>::updateParticleDynamics(ParticleIndicatorF3D<T, T>& indicator, std::string name)
{
//  if (name == "euler") {
//    this->eulerIntegration(indicator);
//  } else if (name == "verlet") {
  this->verletIntegration(indicator);
//  } else {
//    std::cout << "ERROR: no valid integration...use 'euler' or 'verlet'"
//              << std::endl;
//  }
}

template<typename T, template<typename U> class DESCRIPTOR>
void ParticleDynamics3D<T, DESCRIPTOR>::addParticleField(ParticleIndicatorF3D<T, T>& indicator)
{
  /// converting in lattice velocities
  std::vector<T> velL(3, T());
  velL[0] = _converter.latticeVelocity(indicator.getVel()[0]);
  velL[1] = _converter.latticeVelocity(indicator.getVel()[1]);
  velL[2] = _converter.latticeVelocity(indicator.getVel()[2]);

  std::vector<T> omegaL(3, T());
  omegaL[0] = _converter.latticeVelocity(indicator.getOmega()[0]);
  omegaL[1] = _converter.latticeVelocity(indicator.getOmega()[1]);
  omegaL[2] = _converter.latticeVelocity(indicator.getOmega()[2]);

  /// Analytical3D functor for particle motion (trans+rot)
  ParticleU3D<T, T> velocity(indicator, velL, omegaL);
  _sLattice.setExternalParticleField(_superGeometry, velocity, indicator);
}

}

#endif
