/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Thomas Henn, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef PARTICLEHELPER_HH
#define PARTICLEHELPER_HH

//#include "../utilities/vectorHelpers.cpp"
#include<stdexcept>


namespace olb {
namespace phelper {

template<class c>
void print(const c& foo)
{
  for (auto& bar:foo) {
    std::cout << bar << " ";
  }
  std::cout << std::endl;
}

template<typename T>
std::vector<T> product(const std::vector<T>& u, const std::vector<T>& v, T a=1., T b=1.)
{
  if (u.size() == v.size()) {
    std::vector<T> foo(u.size(), T());
    for (int i=0; i<u.size(); i++) {
      foo[i] = a*u[i] + b*v[i];
    }
    return foo;
  } else {
    throw std::runtime_error("In product: size()s don't match.");
  }
}

template<typename T>
T innerProduct(const std::vector<T>& u,const std::vector<T>& v, T a=1.)
{
  if (u.size() == v.size()) {
    T foo = 0.;
    for (int i=0; i<u.size(); i++) {
      foo += u[i]*v[i];
    }
    return foo*a;
  } else {
    throw std::runtime_error("In innerProduct: size()s don't match.");
  }
}


template<typename T>
std::vector<T> normalize(const std::vector<T>& u)
{
  std::vector<T> foo(u.size(), T());
  T root = std::sqrt(innerProduct(u, u));
  if (root!=0)
    for (int i=0; i<u.size(); i++) {
      foo[i] = u[i]/root;
    }
  return foo;
}


}
} // namespace olb

#endif /* PARTICLEHELPER_HH */
