/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef PARTICLESYSVTUOUT_H
#define PARTICLESYSVTUOUT_H

#include <string>
#include "particleSystem3D.h"
#include "io/base64.h"
#include "io/base64.hh"
#include "core/units.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class ParticleSysVTUout {
public:
  ParticleSysVTUout(ParticleSystem3D<T, PARTICLETYPE>*, LBconverter<T>*, std::string const&, bool binary=false);
  void write(int timestep);

protected:
  void writeHeader();
  void writeParticles();

private:
  ParticleSystem3D<T, PARTICLETYPE>* _psys;
  LBconverter<T>* _conv;
  std::string _filenameBase;
  std::string _filename;
  bool _binary;
};

}  // namespace OLB

#endif /* PARTICLESYSVTUOUT_H */
