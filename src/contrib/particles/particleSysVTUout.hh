/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef PARTICLESYSVTUOUT_HH
#define PARTICLESYSVTUOUT_HH

#include "particleSysVTUout.h"
#include "communication/mpiManager.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
ParticleSysVTUout<T, PARTICLETYPE>::ParticleSysVTUout(ParticleSystem3D<T, PARTICLETYPE>* psys, LBconverter<T>* conv, std::string const& filename, bool binary) : _psys(psys), _conv(conv), _filenameBase(filename), _binary(binary)
{
}

template<typename T, template<typename U> class PARTICLETYPE>
void ParticleSysVTUout<T, PARTICLETYPE>::write(int timestep)
{
  std::stringstream ss;
  ss << _filenameBase << "_" << timestep;
  _filename = ss.str();

#ifdef PARALLEL_MODE_MPI
  if (singleton::mpi().getRank() == 0) {
    writeHeader();
  }
#endif
  writeParticles();
}

template<typename T, template<typename U> class PARTICLETYPE>
void ParticleSysVTUout<T, PARTICLETYPE>::writeHeader()
{

  std::string fullName = singleton::directories().getVtkOutDir() + _filename
                         + ".pvtu";

  std::ofstream fout(fullName.c_str());
  if (!fout) {
    std::cerr << "[Particles3D] could not open file: " << fullName << std::endl;
  }

  fout << "<?xml version=\"1.0\"?>" << std::endl << std::flush;
  fout << "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">" << std::endl;
  fout << "<PUnstructuredGrid GhostLevel=\"0\">" << std::endl;

  fout << "<PPointData>" << std::endl;
  fout << "<PDataArray type=\"Int32\" Name=\"Number\" NumberOfComponents=\"1\" format=\"ascii\" />" << std::endl;
  fout << "<PDataArray type=\"Float32\" Name=\"Radius\" NumberOfComponents=\"1\" format=\"ascii\" />" << std::endl;
  fout << "<PDataArray type=\"Float32\" Name=\"Mass\" NumberOfComponents=\"1\" format=\"ascii\" />" << std::endl;
  fout << "<PDataArray type=\"Float32\" Name=\"PartVelocity\" NumberOfComponents=\"3\" format=\"ascii\" />" << std::endl;

  fout << "</PPointData>" << std::endl;

  fout << "<PCellData />" << std::endl;

  fout << "<PPoints>" << std::endl;
  fout << "<PDataArray type=\"Float32\" Name=\"Position\" NumberOfComponents=\"3\" />" << std::endl;
  fout << "</PPoints>" << std::endl;

  for (int iSize = 0; iSize<singleton::mpi().getSize(); iSize++) {
    std::stringstream out;
    out << iSize;
    fullName = out.str() + "_" + _filename + ".vtu";
    fout << "<Piece Source=\"" << fullName << "\" />" << std::endl;
  }
  fout << "</PUnstructuredGrid>" << std::endl;
  fout << "</VTKFile>" << std::endl;
  fout.close();
}

template<typename T, template<typename U> class PARTICLETYPE>
void ParticleSysVTUout<T, PARTICLETYPE>::writeParticles()
{
  std::stringstream out;
  out << singleton::mpi().getRank();
  std::string fullName = singleton::directories().getVtkOutDir() + out.str()
                         + "_" + _filename + ".vtu";
  std::ofstream fout(fullName.c_str());
  if (!fout) std::cerr << "[Particles3D] could not open file: " << fullName
                         << std::endl;

  fout << "<?xml version=\"1.0\"?>" << std::endl << std::flush;
  fout << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">" << std::endl;
  fout << "<UnstructuredGrid>" << std::endl;
  fout << "<Piece NumberOfPoints=\"" << _psys->numOfParticles() << "\" NumberOfCells=\"" << _psys->numOfParticles() << "\">" << std::endl;
  fout << "<PointData Scalars=\"Number\" Vectors=\"Velocity\">" << std::endl;
  fout << "<DataArray type=\"Int32\" Name=\"Number\" NumberOfComponents=\"1\" format=\"ascii\">" << std::endl;
  for (int i = 0; i < _psys->numOfParticles(); i++) {
    fout << _psys->_particles[i]._number << " ";
  }
  fout << "</DataArray>" << std::endl;
  fout << "<DataArray type=\"Float32\" Name=\"Radius\" NumberOfComponents=\"1\" format=\"ascii\">" << std::endl;

  for (typename std::vector<PARTICLETYPE<T> >::iterator it=_psys->_particles.begin(); it!=_psys->_particles.end(); ++it) {
    fout << it->_rad << " ";
  }
  fout << "</DataArray>" << std::endl;
  fout << "<DataArray type=\"Float32\" Name=\"Mass\" NumberOfComponents=\"1\" format=\"ascii\">" << std::endl;
  for (typename std::vector<PARTICLETYPE<T> >::iterator it=_psys->_particles.begin(); it!=_psys->_particles.end(); ++it) {
    fout << it->_mas << " ";
  }
  fout << "</DataArray>" << std::endl;
  fout << "<DataArray type=\"Float32\" Name=\"PartVelocity\" NumberOfComponents=\"3\" format=\"ascii\">"  << std::endl;
  for (typename std::vector<PARTICLETYPE<T> >::iterator it=_psys->_particles.begin(); it!=_psys->_particles.end(); ++it) {
    fout << it->getVel()[0] << " " << it->getVel()[1] << " " << it->getVel()[2] << " ";
  }

  fout << "</DataArray>" << std::endl;
  fout << "</PointData>" << std::endl;

  fout << "<CellData /> " << std::endl;
  fout << "<Cells>" << std::endl;
  fout << "<DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">"
       << std::endl;
  for (int i = 0; i < _psys->numOfParticles(); i++) {
    fout << i << " ";
  }
  fout << "</DataArray>" << std::endl;
  fout << "<DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">" << std::endl;
  for (int i = 1; i <= _psys->numOfParticles(); i++) {
    fout << i << " ";
  }
  fout << "</DataArray>" << std::endl;
  fout << "<DataArray type=\"UInt8\" Name=\"types\" format=\"ascii\">" << std::endl;
  for (int i = 0; i < _psys->numOfParticles(); i++) {
    fout << 1 << " ";
  }
  fout << "</DataArray>" << std::endl;
  fout << "</Cells>" << std::endl;
  fout << "<Points>" << std::endl;
  fout << "<DataArray type=\"Float32\" Name=\"Position\" NumberOfComponents=\"3\">" << std::endl;


  for (typename std::vector<PARTICLETYPE<T> >::iterator it=_psys->_particles.begin(); it!=_psys->_particles.end(); ++it) {
    fout << it->getPos()[0] << " " << it->getPos()[1] << " " << it->getPos()[2] << " ";
  }

  fout << "</DataArray>" << std::endl;
  fout << "</Points>" << std::endl;

  fout << "</Piece>" << std::endl;
  fout << "</UnstructuredGrid>" << std::endl;
  fout << "</VTKFile>" << std::endl;
  fout.close();
}


}  // namespace OLB



#endif /* PARTICLESYSVTUOUT_HH */
