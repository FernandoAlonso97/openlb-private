/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2015 Fabian Klemens, Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/*
 * @Article{doi:10.1108/02644400410519767,
 * Title                    = {A contact algorithm for partitioning N arbitrary sized objects},
 * Author                   = {John R. Williams and Eric Perkins and Ben Cook},
 * Journal                  = {Engineering Computations},
 * Year                     = {2004},
 * Number                   = {2/3/4},
 * Pages                    = {235-248},
 * Volume                   = {21},
 *
 * Abstract                 = { A new spatial reasoning algorithm that can be used in multi‐body contact detection is presented. The algorithm achieves the partitioning of N bodies of arbitrary shape and size into N lists in order O(N) operations, where each list consists of bodies spatially near to the target object. The algorithm has been tested for objects of arbitrary shape and size, in two and three dimensions. However, we believe that it can be extended to dimensions of four and higher. The algorithm (CGRID) is a binning algorithm that extends traditional binning algorithms so that the arbitrary sizes and shapes can be handled efficiently. The algorithm has applications in discrete element, finite element, molecular dynamics, meshless methods, and lattice‐Boltzmann codes and also in domains such as path planning, target acquisition and general clustering problems. },
 * Doi                      = {10.1108/02644400410519767},
 * Eprint                   = { http://dx.doi.org/10.1108/02644400410519767
 * },
 * Url                      = { http://dx.doi.org/10.1108/02644400410519767
 * }
 * }
 */

#ifndef DCELL_H_
#define DCELL_H_

#include "sortAlgorithms.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class DCell : public ContactDetection<T, PARTICLETYPE> {
 public:
  DCell(ParticleSystem3D<T, PARTICLETYPE>& pSys, T overlap, T spacing);  // : ContactDetection<T, PARTICLETYPE> (pSys) {};

  void sort();
  int getMatches(int pInt, std::vector<std::pair<size_t, T> >& matches);
  //  int getMatches(typename std::vector<PARTICLETYPE<T> >::iterator p, std::vector<std::pair<size_t, T> >& matches) {return 0;};

 private:
  std::vector<T> _physPos, _physExtend;
  std::vector<T> _intExtend;
  T _spacing;
  T _spacingInv;

  std::vector<int> _headZ;
  std::vector<int> _nextZ;
  std::vector<int> _C2;
  std::vector<int> _Z2;

  std::vector<int> _headY;
  std::vector<int> _nextY;
  std::vector<int> _B2;
  std::vector<int> _Y2;

  std::vector<int> _headX;
  std::vector<int> _nextX;
  std::vector<int> _A2;
  std::vector<int> _X2;

  std::vector<int> _tagX, _tagY, _tagZ;

  std::vector<std::vector<std::pair<size_t, T> > > _matches;

};

template<typename T, template<typename U> class PARTICLETYPE>
DCell<T, PARTICLETYPE>::DCell(ParticleSystem3D<T, PARTICLETYPE>& pSys,
                              T overlap, T spacing)
    : ContactDetection<T, PARTICLETYPE>(pSys, "DCell"),
      _physPos(3, T()),
      _physExtend(3, T()),
      _intExtend(3, 0),
      _spacing(spacing) {

  _physPos = this->_pSys._physPos;
  _physExtend = this->_pSys._physExtend;

  _spacingInv = 1. / .1;  //_spacing;

  int intOverlap = std::ceil(overlap * _spacingInv);
  for (int i = 0; i < 3; i++) {
    _physPos[i] -= overlap;
    _intExtend[i] = _physExtend[i] * _spacingInv + 2 * intOverlap;
  }
  _headX.resize(_intExtend[0] + 1, -1);
  _A2.resize(_intExtend[0] + 1, -1);
  _headY.resize(_intExtend[1] + 1, -1);
  _B2.resize(_intExtend[1] + 1, -1);
  _headZ.resize(_intExtend[2] + 1, -1);
  _C2.resize(_intExtend[2] + 1, -1);

}

template<typename T, template<typename U> class PARTICLETYPE>
void DCell<T, PARTICLETYPE>::sort() {

  int laufZ, laufY, laufX, laufIndexX1, laufIndexX2;

  if (_matches.size() < this->_pSys.sizeInclShadow()) {
    _matches.resize(this->_pSys.sizeInclShadow(),
                    std::vector<std::pair<size_t, T> >());
  }
  std::fill(_matches.begin(), _matches.end(),
            std::vector<std::pair<size_t, T> >());

  _nextX.resize(this->_pSys.sizeInclShadow() + 1, -1);
  _X2.resize(this->_pSys.sizeInclShadow() + 1, -1);
  _nextY.resize(this->_pSys.sizeInclShadow() + 1, -1);
  _Y2.resize(this->_pSys.sizeInclShadow() + 1, -1);
  _nextZ.resize(this->_pSys.sizeInclShadow() + 1, -1);
  _Z2.resize(this->_pSys.sizeInclShadow() + 1, -1);

  _tagX.resize(this->_pSys.sizeInclShadow() + 1, 0);
  _tagY.resize(this->_pSys.sizeInclShadow() + 1, 0);
  _tagZ.resize(this->_pSys.sizeInclShadow() + 1, 0);

  int iz = 0, iy = 0, ix = 0, iZ = 0, iY = 0, iX = 0, p = -1, count = 0;

  T dist2 = 0;

  /// build Z list
  for (int i = 0; i < this->_pSys.sizeInclShadow(); ++i) {
    iz = std::floor(
        (this->_pSys[i].getPos()[2] - this->_pSys[i].getRad() - _physPos[2])
            * _spacingInv);
    _nextZ[i] = _headZ[iz];
    _headZ[iz] = i;
  }

  /// loop over slices
//<<<<<<< HEAD
//  for (int z = 0; z < _intExtend[2]; ++z) {
//    for (int i = 0; i < this->_pSys.sizeInclShadow() + 1; ++i) {
//      _tagZ[i] = 0;
//    }
//    /// has the slice an particle ?
//    if (_headZ[z] != -1) {
//      laufZ = _headZ[z];
//      /// check the neighbors of the particles in current slice
//      while (laufZ != -1) {
//        /// have there been big particles in the previous slice(s) ?
//        /// if yes, tag them
//        if (z > 0) {
//          int j = _C2[z - 1];
//          while (j != -1) {
//            _tagZ[j] = 1;
//            j = _Z2[j];
//          }
//        }
//        iy = std::floor(
//            (this->_pSys[laufZ].getPos()[1] - this->_pSys[laufZ].getRad()
//                - _physPos[1]) * _spacingInv);
//        iZ = std::floor(
//            (this->_pSys[laufZ].getPos()[2] + this->_pSys[laufZ].getRad()
//                - _physPos[2]) * _spacingInv);
//        /// build Y list
//        _nextY[laufZ] = _headY[iy];
//        _headY[iy] = laufZ;
//
//        p = laufZ;
//        laufZ = _nextZ[laufZ];
//        /// are there big particles in the slice ?
//        /// if yes, append to next z list
//        if (iZ > z) {
//          _Z2[p] = _C2[z];
//          _C2[z] = p;
//          _nextZ[p] = _headZ[z + 1];
//          _headZ[z + 1] = p;
//        }
//      }
//
//      /// loop over rows in current slice
//      for (int y = 0; y < _intExtend[1]; ++y) {
//        for (int i = 0; i < this->_pSys.sizeInclShadow() + 1; ++i) {
//          _tagY[i] = 0;
//        }
//        // has the row an particle ?
//        if (_headY[y] != -1) {
//          laufY = _headY[y];
//          /// check the neighbors of the particles in current row
//          while (laufY != -1) {
//            /// have there been big particles in the previous row(s) ?
//            /// if yes, tag them
//            if (y > 0) {
//              int j = _B2[y - 1];
//              while (j != -1) {
//                _tagY[j] = 1;
//                j = _Y2[j];
//              }
//            }
//            ix = std::floor(
//                (this->_pSys[laufY].getPos()[0] - this->_pSys[laufY].getRad()
//                    - _physPos[0]) * _spacingInv);
//            iY = std::floor(
//                (this->_pSys[laufY].getPos()[1] + this->_pSys[laufY].getRad()
//                    - _physPos[1]) * _spacingInv);
//            /// build X list
//            _nextX[laufY] = _headX[ix];
//            _headX[ix] = laufY;
//
//            p = laufY;
//            laufY = _nextY[laufY];
//            /// are there big particles in the row ?
//            /// if yes, append to next y list
//            if (iY > y) {
//              _Y2[p] = _B2[y];
//              _B2[y] = p;
//              _nextY[p] = _headY[y + 1];
//              _headY[y + 1] = p;
//            }
//          }
//
//          /// loop over cells in current row
//          for (int x = 0; x < _intExtend[0]; ++x) {
//            for (int i = 0; i < this->_pSys.sizeInclShadow() + 1; ++i) {
//              _tagX[i] = 0;
//            }
//            // has the cell an particle ?
//            if (_headX[x] != -1) {
//              laufX = _headX[x];
//              /// check the neighbors of the particles in current cell
//              while (laufX != -1) {
//                /// have there been big particles in the previous cell(s) ?
//                /// if yes, tag them
//                if (x > 0) {
//                  int j = _A2[x - 1];
//                  while (j != -1) {
//                    _tagX[j] = 1;
//                    j = _X2[j];
//                  }
//                }
//                iX = std::floor(
//                    (this->_pSys[laufX].getPos()[0]
//                        + this->_pSys[laufX].getRad() - _physPos[0])
//                        * _spacingInv);
//                p = laufX;
//                laufX = _nextX[laufX];
//
//                /// are there big particles in the cell ?
//                /// if yes, append to next x list
//                if (iX > x) {
//                  _X2[p] = _A2[x];
//                  _A2[x] = p;
//                  _nextX[p] = _headX[x + 1];
//                  _headX[x + 1] = p;
//                }
//              }
//
//              /// CONTACT CHECK
//              laufIndexX1 = _headX[x];
//              laufIndexX2 = laufIndexX1;
//              while (laufIndexX2 != -1) {
//                if (laufIndexX1 != laufIndexX2) {
//                  if (!(_tagX[laufIndexX1] == 1 && _tagX[laufIndexX2] == 1)
//                      && !(_tagY[laufIndexX1] == 1 && _tagY[laufIndexX2] == 1)
//                      && !(_tagZ[laufIndexX1] == 1 && _tagZ[laufIndexX2] == 1)) {
//                    dist2 = std::pow(
//                        this->_pSys[laufIndexX1].getPos()[0]
//                            - this->_pSys[laufIndexX2].getPos()[0],
//                        2)
//                        + std::pow(
//                            this->_pSys[laufIndexX1].getPos()[1]
//                                - this->_pSys[laufIndexX2].getPos()[1],
//                            2)
//                        + std::pow(
//                            this->_pSys[laufIndexX1].getPos()[2]
//                                - this->_pSys[laufIndexX2].getPos()[2],
//                            2);
//=======
	for ( int z = 0; z < _intExtend[2]; ++z) {
		for (int i=0; i<this->_pSys.sizeInclShadow()+1; ++i) {
			_tagZ[i] = 0;
		}
		/// has the slice an particle ?
		if ( _headZ[z] != -1 ) {
			laufZ = _headZ[z];
			/// check the neighbors of the particles in current slice
			while (laufZ != -1) {
				/// have there been big particles in the previous slice(s) ?
				/// if yes, tag them
				if (z>0){
					int j = _C2[z-1];
					while(j!=-1){
						_tagZ[j] = 1;
						j = _Z2[j];
					}
				}
				iy = std::floor((this->_pSys[laufZ].getPos()[1] - this->_pSys[laufZ].getRad() - _physPos[1]) * _spacingInv);
				iZ = std::floor((this->_pSys[laufZ].getPos()[2] + this->_pSys[laufZ].getRad() - _physPos[2]) * _spacingInv);
				/// build Y list
				_nextY[laufZ] = _headY[iy];
				_headY[iy] = laufZ;

				p = laufZ;
				laufZ = _nextZ[laufZ];
				/// are there big particles in the slice ?
				/// if yes, append to next z list
				if (iZ > z) {
					_Z2[p] = _C2[z];
					_C2[z] = p;
					_nextZ[p] = _headZ[z+1];
					_headZ[z+1] = p;
				}
			}

			/// loop over rows in current slice
			for ( int y = 0; y < _intExtend[1]; ++y) {
				for (int i=0; i<this->_pSys.sizeInclShadow()+1; ++i) {
					_tagY[i] = 0;
				}
				// has the row an particle ?
				if (_headY[y] != -1 ) {
					laufY = _headY[y];
					/// check the neighbors of the particles in current row
					while (laufY != -1) {
						/// have there been big particles in the previous row(s) ?
						/// if yes, tag them
						if (y>0){
							int j = _B2[y-1];
							while(j!=-1){
								_tagY[j] = 1;
								j = _Y2[j];
							}
						}
						ix = std::floor((this->_pSys[laufY].getPos()[0] - this->_pSys[laufY].getRad() - _physPos[0]) * _spacingInv);
						iY = std::floor((this->_pSys[laufY].getPos()[1] + this->_pSys[laufY].getRad() - _physPos[1]) * _spacingInv);
				    /// build X list
						_nextX[laufY] = _headX[ix];
						_headX[ix] = laufY;

						p = laufY;
						laufY = _nextY[laufY];
						/// are there big particles in the row ?
						/// if yes, append to next y list
						if (iY > y) {
							_Y2[p] = _B2[y];
							_B2[y] = p;
							_nextY[p] = _headY[y+1];
							_headY[y+1] = p;
						}
					}

					/// loop over cells in current row
					for ( int x = 0; x < _intExtend[0]; ++x) {
						for (int i=0; i<this->_pSys.sizeInclShadow()+1; ++i) {
							_tagX[i] = 0;
						}
						// has the cell an particle ?
						if (_headX[x] != -1 ) {
							laufX = _headX[x];
							/// check the neighbors of the particles in current cell
							while (laufX != -1) {
								/// have there been big particles in the previous cell(s) ?
								/// if yes, tag them
								if (x>0){
									int j = _A2[x-1];
									while(j!=-1){
										_tagX[j] = 1;
										j = _X2[j];
									}
								}
								iX = std::floor((this->_pSys[laufX].getPos()[0] + this->_pSys[laufX].getRad() - _physPos[0]) * _spacingInv);
								p = laufX;
								laufX = _nextX[laufX];

								/// CONTACT CHECK
								laufIndexX1 = p;
								laufIndexX2 = laufIndexX1;
								while (laufIndexX2 != -1) {
									if (laufIndexX1 != laufIndexX2) {
										if ( !(_tagX[laufIndexX1] == 1 && _tagX[laufIndexX2] == 1)
												&& !(_tagY[laufIndexX1] == 1 && _tagY[laufIndexX2] == 1)
												&& !(_tagZ[laufIndexX1] == 1 && _tagZ[laufIndexX2] == 1) ) {
											dist2 = std::pow(this->_pSys[laufIndexX1].getPos()[0] - this->_pSys[laufIndexX2].getPos()[0], 2)
															+ std::pow(this->_pSys[laufIndexX1].getPos()[1] - this->_pSys[laufIndexX2].getPos()[1], 2)
															+ std::pow(this->_pSys[laufIndexX1].getPos()[2] - this->_pSys[laufIndexX2].getPos()[2], 2);

#ifdef OLB_DEBUG
												if(dist2 < std::pow(this->_pSys[laufIndexX1].getRad() + this->_pSys[laufIndexX2].getRad(),2)) {
													cout << "["<<x<<", "<<y<<", "<<z<<"]: "<<laufIndexX1<<" <-> "<<laufIndexX2<<endl;
													count++;
												}
#endif

											if (this->_pSys[laufIndexX1].getActive()) {
												_matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
											}
											if (this->_pSys[laufIndexX2].getActive()) {
												_matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
											}
										}
									}
									laufIndexX2 = _nextX[laufIndexX2];
								}

								/// are there big particles in the cell ?
								/// if yes, append to next x list
								if (iX > x) {
									_X2[p] = _A2[x];
									_A2[x] = p;
									_nextX[p] = _headX[x+1];
									_headX[x+1] = p;
								}
							}
						}
						_headX[x] = -1;
						if (x > 0) _A2[x-1] = -1;
					}

				}
				_headY[y] = -1;
				if (y > 0) _B2[y-1] = -1;
			}

		}
		_headZ[z] = -1;
		if (z > 0) _C2[z-1] = -1;
	}

#ifndef OLB_DEBUG
/*                    if (dist2
                        < std::pow(
                            this->_pSys[laufIndexX1].getRad()
                                + this->_pSys[laufIndexX2].getRad(),
                            2)) {
//                      cout << "[" << x << ", " << y << ", " << z << "]: "
//                          << laufIndexX1 << " <-> " << laufIndexX2 << endl;
//                      count++;
                    }
//#endif

                    if (this->_pSys[laufIndexX1].getActive()) {
                      _matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
                    }
                    if (this->_pSys[laufIndexX2].getActive()) {
                      _matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
                    }
}
                }
                laufIndexX2 = _nextX[laufIndexX2];
              }
            }
            _headX[x] = -1;
            if (x > 0)
              _A2[x - 1] = -1;
          }

        }
        _headY[y] = -1;
        if (y > 0)
          _B2[y - 1] = -1;
      }

    }
    _headZ[z] = -1;
    if (z > 0)
      _C2[z - 1] = -1;
  }

//#ifdef OLB_DEBUG
  cout << "---------------------------" << endl;
  cout << " " << count << " contact(s) detected!" << endl;
  cout << "---------------------------" << endl;
*/
#endif

}

template<typename T, template<typename U> class PARTICLETYPE>
int DCell<T, PARTICLETYPE>::getMatches(
    int pInt, std::vector<std::pair<size_t, T> >& matches) {
  matches = _matches[pInt];
  return _matches[pInt].size();
//  return 0;

}

}  // namespace olb

#endif
