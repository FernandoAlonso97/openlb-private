/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2015 Thomas Henn
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

/*
 * @article{munjiza1998nbs,
 * title={NBS contact detection algorithm for bodies of similar size},
 * author={Munjiza, A and Andrews, KRF},
 * journal={International Journal for Numerical Methods in Engineering},
 * volume={43},
 * number={1},
 * pages={131--149},
 * year={1998},
 * publisher={Chichester, New York, Wiley [etc.] 1969-}
 * }
 */

#ifndef MUNJIZANBS_H_
#define MUNJIZANBS_H_

#include "sortAlgorithms.h"

namespace olb {

template<typename T, template<typename U> class PARTICLETYPE>
class MunjizaNBS : public ContactDetection<T, PARTICLETYPE> {
 public:
  MunjizaNBS(ParticleSystem3D<T, PARTICLETYPE>& pSys, T overlap, T spacing);

  void sort();
  int getMatches(int pInt, std::vector<std::pair<size_t, T> >& matches);

 private:
  MunjizaNBS<T, PARTICLETYPE>* generate(ParticleSystem3D<T, PARTICLETYPE>& pSys);

  std::vector<T> _physPos, _physExtend;
  std::vector<T> _intExtend;
  T _overlap;
  T _spacing;
  T _spacingInv;

  std::vector<int> _C;
  std::vector<int> _Z;

  std::vector<int> _B1;
  std::vector<int> _Y1;
  std::vector<int> _B2;
  std::vector<int> _Y2;

  std::vector<int> _A1;
  std::vector<int> _X1;
  std::vector<int> _A2;
  std::vector<int> _X2;

  std::vector<bool> _A1IsNew;
  std::vector<bool> _B1IsNew;
  std::vector<bool> _CIsNew;

  std::vector<std::vector<std::pair<size_t, T> > > _matches;

};

template<typename T, template<typename U> class PARTICLETYPE>
MunjizaNBS<T, PARTICLETYPE>* MunjizaNBS<T, PARTICLETYPE>::generate(ParticleSystem3D<T, PARTICLETYPE>& pSys) {
  //std::cout << "calling NanoflannContact.generate()" << std::endl;
  return new MunjizaNBS(pSys,_overlap, _spacing);
}

template<typename T, template<typename U> class PARTICLETYPE>
MunjizaNBS<T, PARTICLETYPE>::MunjizaNBS(ParticleSystem3D<T, PARTICLETYPE>& pSys,
                                        T overlap, T spacing)
    : ContactDetection<T, PARTICLETYPE>(pSys, "MunjizaNBS"),
      _physPos(3, T()),
      _physExtend(3, T()),
      _intExtend(3, 0),
      _overlap(overlap),
      _spacing(spacing) {

  _physPos = this->_pSys.getPhysPos();
  _physExtend = this->_pSys.getPhysExtend();

  _spacingInv = 1. / _spacing;

  int intOverlap = std::ceil(overlap / _spacing);
  for (int i = 0; i < 3; i++) {
    _physPos[i] -= overlap;
    _intExtend[i] = _physExtend[i] / _spacing + 2 * intOverlap;
  }

  _A1.resize(_intExtend[0]+1, -1);
  _A2.resize(_intExtend[0]+1, -1);
  _B1.resize(_intExtend[1]+1, -1);
  _B2.resize(_intExtend[1]+1, -1);
  _C.resize(_intExtend[2]+1, -1);
  _A1IsNew.resize(_intExtend[0]+1, true);
  _B1IsNew.resize(_intExtend[1]+1, true);
  _CIsNew.resize(_intExtend[2]+1, true);
}

template<typename T, template<typename U> class PARTICLETYPE>
void MunjizaNBS<T, PARTICLETYPE>::sort() {

  int laufIndexZ, laufIndexY, laufIndexX1, laufIndexX2;

  if (_matches.size() < this->_pSys.sizeInclShadow()) {
    _matches.resize(this->_pSys.sizeInclShadow(),
                    std::vector<std::pair<size_t, T> >());
  }
  std::fill(_matches.begin(), _matches.end(),
            std::vector<std::pair<size_t, T> >());

  _X1.resize(this->_pSys.sizeInclShadow(), -1);
  _X2.resize(this->_pSys.sizeInclShadow(), -1);
  _Y1.resize(this->_pSys.sizeInclShadow(), -1);
  _Y2.resize(this->_pSys.sizeInclShadow(), -1);
  _Z.resize(this->_pSys.sizeInclShadow(), -1);

  int iZ = 0, iY = 0, iX = 0;
  T dist2 = 0;

  /// 1.
  for (unsigned int i = 0; i < this->_pSys.sizeInclShadow(); ++i) {
    iZ = (int) ((this->_pSys[i].getPos()[2] - _physPos[2]) * _spacingInv);
    _Z[i] = _C[iZ];
    _C[iZ] = i;
  }

  /// 2. Loop over discrete elements k<N
  for (unsigned int i = 0; i < this->_pSys.sizeInclShadow(); ++i) {
    iZ = (int) ((this->_pSys[i].getPos()[2] - _physPos[2]) * _spacingInv);
    if (_CIsNew[iZ]) {
      /// 3. loop over central z list
      laufIndexZ = _C[iZ];
      while (laufIndexZ != -1) {
        iY = (int) ((this->_pSys[laufIndexZ].getPos()[1] - _physPos[1]) * _spacingInv);
        _Y1[laufIndexZ] = _B1[iY];
        _B1[iY] = laufIndexZ;
        laufIndexZ = _Z[laufIndexZ];
      }

      /// 4. loop over neighbor z list
      if (iZ > 0) {
        laufIndexZ = _C[iZ - 1];
        while (laufIndexZ != -1) {
          iY = (int) ((this->_pSys[laufIndexZ].getPos()[1] - _physPos[1])
              * _spacingInv);
          _Y2[laufIndexZ] = _B2[iY];
          _B2[iY] = laufIndexZ;
          laufIndexZ = _Z[laufIndexZ];
        }
      }
      std::fill(_B1IsNew.begin(), _B1IsNew.end(), true);

/// 5. loop over central z list
      laufIndexZ = _C[iZ];
      while (laufIndexZ != -1) {
        iY = (int) ((this->_pSys[laufIndexZ].getPos()[1] - _physPos[1])
            * _spacingInv);
        if (_B1IsNew[iY]) {

          /// 6. loop over yz list
          laufIndexY = _B1[iY];
          while (laufIndexY != -1) {
            iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                * _spacingInv);
            _X1[laufIndexY] = _A1[iX];
            _A1[iX] = laufIndexY;
            laufIndexY = _Y1[laufIndexY];
          }

          /// 7. loop over neighbor yz lists
          /// (iY-1/iZ)
          if (iY > 0) {
            laufIndexY = _B1[iY - 1];
            while (laufIndexY != -1) {
              iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                  * _spacingInv);
              _X2[laufIndexY] = _A2[iX];
              _A2[iX] = laufIndexY;
              laufIndexY = _Y1[laufIndexY];
            }
            /// (iY-1/iZ-1)
            laufIndexY = _B2[iY - 1];
            while (laufIndexY != -1) {
              iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                  * _spacingInv);
              _X2[laufIndexY] = _A2[iX];
              _A2[iX] = laufIndexY;
              laufIndexY = _Y2[laufIndexY];

            }
          }
          /// (iY/iZ-1)
          laufIndexY = _B2[iY];
          while (laufIndexY != -1) {
            iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                * _spacingInv);
            _X2[laufIndexY] = _A2[iX];
            _A2[iX] = laufIndexY;
            laufIndexY = _Y2[laufIndexY];
          }
          /// (iY+1/iZ-1)

          if (iY < _intExtend[1]-1) {
            laufIndexY = _B2[iY + 1];
            while (laufIndexY != -1) {
              iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                  * _spacingInv);
              _X2[laufIndexY] = _A2[iX];
              _A2[iX] = laufIndexY;
              laufIndexY = _Y2[laufIndexY];
            }
          }
          std::fill(_A1IsNew.begin(), _A1IsNew.end(), true);

          /// 8. loop over yz list
          laufIndexY = _B1[iY];
          while (laufIndexY != -1) {
            iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                * _spacingInv);
            if (_A1IsNew[iX]) {
              laufIndexX1 = _A1[iX];
              while (laufIndexX1 != -1) {
                //(iX, iY, iZ)
                laufIndexX2 = laufIndexX1;
                while (laufIndexX2 != -1) {
                  if (laufIndexX1 != laufIndexX2) {
                    dist2 = std::pow(
                        this->_pSys[laufIndexX1].getPos()[0]
                            - this->_pSys[laufIndexX2].getPos()[0],
                        2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[1]
                                - this->_pSys[laufIndexX2].getPos()[1],
                            2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[2]
                                - this->_pSys[laufIndexX2].getPos()[2],
                            2);
                    if (this->_pSys[laufIndexX1].getActive()) {
                      _matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
                    }
                    if (this->_pSys[laufIndexX2].getActive()) {
                      _matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
                    }
                  }
                  laufIndexX2 = _X1[laufIndexX2];
                }
                //(iX-1, iY, iZ-1)
                //(iX,   iY, iZ-1)
                //(iX+1, iY, iZ-1)
                laufIndexX2 = _A2[iX];
                while (laufIndexX2 != -1) {
                  dist2 = std::pow(
                      this->_pSys[laufIndexX1].getPos()[0]
                          - this->_pSys[laufIndexX2].getPos()[0],
                      2)
                      + std::pow(
                          this->_pSys[laufIndexX1].getPos()[1]
                              - this->_pSys[laufIndexX2].getPos()[1],
                          2)
                      + std::pow(
                          this->_pSys[laufIndexX1].getPos()[2]
                              - this->_pSys[laufIndexX2].getPos()[2],
                          2);
                  if (this->_pSys[laufIndexX1].getActive()) {
                    _matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
                  }
                  if (this->_pSys[laufIndexX2].getActive()) {
                    _matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
                  }
                  laufIndexX2 = _X2[laufIndexX2];
                }
                //(iX-1, iY+1, iZ-1)
                //(iX,   iY+1, iZ-1)
                //(iX+1, iY+1, iZ-1)
                if (iX < _intExtend[0]-1) {
                  laufIndexX2 = _A2[iX + 1];
                  while (laufIndexX2 != -1) {
                    dist2 = std::pow(
                        this->_pSys[laufIndexX1].getPos()[0]
                            - this->_pSys[laufIndexX2].getPos()[0],
                        2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[1]
                                - this->_pSys[laufIndexX2].getPos()[1],
                            2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[2]
                                - this->_pSys[laufIndexX2].getPos()[2],
                            2);
                    if (this->_pSys[laufIndexX1].getActive()) {
                      _matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
                    }
                    if (this->_pSys[laufIndexX2].getActive()) {
                      _matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
                    }
                    laufIndexX2 = _X2[laufIndexX2];
                  }
                }
                //(iX-1, iY-1, iZ-1)
                //(iX,   iY-1, iZ-1)
                //(iX+1, iY-1, iZ-1)
                //(iX-1, iY-1, iZ)
                //(iX,   iY-1, iZ)
                //(iX+1, iY-1, iZ)
                if (iX > 0) {
                  laufIndexX2 = _A2[iX - 1];
                  while (laufIndexX2 != -1) {
                    dist2 = std::pow(
                        this->_pSys[laufIndexX1].getPos()[0]
                            - this->_pSys[laufIndexX2].getPos()[0],
                        2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[1]
                                - this->_pSys[laufIndexX2].getPos()[1],
                            2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[2]
                                - this->_pSys[laufIndexX2].getPos()[2],
                            2);
                    if (this->_pSys[laufIndexX1].getActive()) {
                      _matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
                    }
                    if (this->_pSys[laufIndexX2].getActive()) {
                      _matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
                    }
                    laufIndexX2 = _X2[laufIndexX2];
                  }
                  //(iX-1, iY, iZ)
                  laufIndexX2 = _A1[iX - 1];
                  while (laufIndexX2 != -1) {
                    dist2 = std::pow(
                        this->_pSys[laufIndexX1].getPos()[0]
                            - this->_pSys[laufIndexX2].getPos()[0],
                        2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[1]
                                - this->_pSys[laufIndexX2].getPos()[1],
                            2)
                        + std::pow(
                            this->_pSys[laufIndexX1].getPos()[2]
                                - this->_pSys[laufIndexX2].getPos()[2],
                            2);
                    if (this->_pSys[laufIndexX1].getActive()) {
                      _matches[laufIndexX1].emplace_back(laufIndexX2, dist2);
                    }
                    if (this->_pSys[laufIndexX2].getActive()) {
                      _matches[laufIndexX2].emplace_back(laufIndexX1, dist2);
                    }
                    laufIndexX2 = _X1[laufIndexX2];
                  }
                }
                laufIndexX1 = _X1[laufIndexX1];
              }
              _A1IsNew[iX] = false;

            }
            laufIndexY = _Y1[laufIndexY];
          }

          /// 9. loop over yz list
          laufIndexY = _B1[iY];
          while (laufIndexY != -1) {
            iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                * _spacingInv);
            _A1[iX] = -1;
            laufIndexY = _Y1[laufIndexY];
          }

          /// 10. loop over neighbor yz list
          if (iY > 0) {
            laufIndexY = _B1[iY - 1];
            while (laufIndexY != -1) {
              iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                  * _spacingInv);
              _A1[iX] = -1;
              _A2[iX] = -1;
              laufIndexY = _Y1[laufIndexY];
            }

            laufIndexY = _B2[iY - 1];
            while (laufIndexY != -1) {
              iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                  * _spacingInv);
              _A2[iX] = -1;
              laufIndexY = _Y2[laufIndexY];
            }
          }

          if (iY < _intExtend[1]-1) {
            laufIndexY = _B2[iY + 1];
            while (laufIndexY != -1) {
              iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                  * _spacingInv);
              _A2[iX] = -1;
              laufIndexY = _Y2[laufIndexY];
            }
          }

          laufIndexY = _B2[iY];
          while (laufIndexY != -1) {
            iX = (int) ((this->_pSys[laufIndexY].getPos()[0] - _physPos[0])
                * _spacingInv);
            _A2[iX] = -1;
            laufIndexY = _Y2[laufIndexY];
          }
          _B1IsNew[iY] = false;

        }
        laufIndexZ = _Z[laufIndexZ];
      }

      /// 11. loop over z list
      laufIndexZ = _C[iZ];
      while (laufIndexZ != -1) {
        iY = (int) ((this->_pSys[laufIndexZ].getPos()[1] - _physPos[1])
            * _spacingInv);
        _B1[iY] = -1;
        laufIndexZ = _Z[laufIndexZ];
      }

/// 12. loop over neighbor z list
      if (iZ > 0) {
        laufIndexZ = _C[iZ - 1];
        while (laufIndexZ != -1) {
          iY = (int) ((this->_pSys[laufIndexZ].getPos()[1] - _physPos[1])
              * _spacingInv);
          _B2[iY] = -1;
          laufIndexZ = _Z[laufIndexZ];
        }
      }
    }
    _CIsNew[iZ] = false;
  }

  std::fill(_CIsNew.begin(), _CIsNew.end(), true);
  std::fill(_C.begin(), _C.end(), -1);

#ifdef OLB_DEBUG
  for (int i = 0; i < _intExtend[2]; i++) {
    if (_C[i] != -1)
    cout << "_C[" << i << "] == " << _C[i] << std::endl;
  }

  for (int i = 0; i < _intExtend[1]; i++) {
    if (_B1[i] != -1)
    cout << "_B1[" << i << "] == " << _B1[i] << std::endl;
    if (_B2[i] != -1)
    cout << "_B2[" << i << "] == " << _B2[i] << std::endl;
  }

  for (int i = 0; i < _intExtend[0]; i++) {
    if (_A1[i] != -1)
    cout << "_A1[" << i << "] == " << _A1[i] << std::endl;
    if (_A2[i] != -1)
    cout << "_A2[" << i << "] == " << _A2[i] << std::endl;
  }
#endif
}

template<typename T, template<typename U> class PARTICLETYPE>
int MunjizaNBS<T, PARTICLETYPE>::getMatches(
    int pInt, std::vector<std::pair<size_t, T> >& matches) {
  matches = _matches[pInt];
  return _matches[pInt].size();
}

}  // namespace olb

#endif
