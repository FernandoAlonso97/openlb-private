/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef REYNOLDSSTRESS_F_H
#define REYNOLDSSTRESS_F_H


#include<vector>    // for generic i/o
#include<string>     // for lpnorm

#include "functors/lattice/superBaseF3D.h"
#include "functors/lattice/blockBaseF3D.h"
#include "functors/lattice/superLatticeIntegralF3D.h"
#include "functors/lattice/blockLatticeIntegralF3D.h"
#include "core/superLattice3D.h"
#include "geometry/superGeometry3D.h"
#include "geometry/cuboidGeometry3D.h"
#include "dynamics/lbHelpers.h"  // for computation of lattice rho and velocity

#include "core/blockLattice3D.h"

namespace olb {


/// a class used to average a certain Funktor over a cuboid-like structure
template <typename T, template <typename U> class DESCRIPTOR>
class reynoldsStressF3D : public SuperLatticePhysF3D<T,DESCRIPTOR> {
protected:
  SuperLatticePhysVelocity3D<T, DESCRIPTOR>&    _f_velocity;

  CuboidGeometry3D<T>&                          _cuboidGeometry;
  LoadBalancer<T>&                              _loadBalancer;

  T                                             _latticeL;

  std::vector<T>                                _physOrigin;
  std::vector<T>                                _physExtend;

  std::vector<T>                                _physPoint;
  std::vector<int>                              _latticePoint;

  std::vector<T>                                _localVelocity;

  unsigned long                                 _ensemblecount;

  std::vector<std::vector<long double> >        _ensembleAverageVelocitySum;
  std::vector<std::vector<long double> >        _ensembleAverageRST;

  std::vector<std::vector<int> >                _cells_on_cpu;

  int                                           _token;
  int                                           _cell;

  const std::string       _filename;



public:
  reynoldsStressF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperLatticePhysVelocity3D<T, DESCRIPTOR>& f_velocity ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend,const std::string filename);

  void print();

  virtual void addEnsemble() = 0;

  virtual void reInit() = 0;

  bool operator() (T output[], const int input[]);

};
/// REYNOLDSSTRESSFULL
template <typename T, template <typename U> class DESCRIPTOR>
class reynoldsStressFullF3D : public reynoldsStressF3D<T,DESCRIPTOR> {
protected:



public:
  reynoldsStressFullF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperLatticePhysVelocity3D<T, DESCRIPTOR>& f_velocity ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtendi,const std::string filename);

  void addEnsemble();

  void reInit();

};
///REYNOLDSSTRESSPLANE
template <typename T, template <typename U> class DESCRIPTOR>
class reynoldsStressPlaneF3D : public reynoldsStressF3D<T,DESCRIPTOR> {
protected:


  std::vector<bool>                               _plane_normal;

  SuperAverage3D<T>                       _f_velocity_average;

  SuperGeometry3D<T> &                            _superGeometry;

  std::vector<std::vector<T> >                    _velocity_average_plane;

  std::vector<T>                                  _physOrigin_plane;
  std::vector<T>                                  _physExtend_plane;



public:
  reynoldsStressPlaneF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,SuperLatticePhysVelocity3D<T, DESCRIPTOR>& f_velocity ,SuperGeometry3D<T> & superGeometry,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, std::vector<bool> plane_normal,const std::string filename );

  void addEnsemble();

  void reInit();

};

} // end namespace olb

#endif
