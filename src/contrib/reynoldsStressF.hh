/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef REYNOLDSSTRESS_F_HH
#define REYNOLDSSTRESS_F_HH

#include<vector>    // for generic i/o

#include "reynoldsStressF.h"
#include "functors/genericF.h"
#include "functors/lattice/superBaseF3D.h"
#include "functors/lattice/indicator/indicatorF3D.h"
#include "core/superLattice3D.h"
#include "dynamics/lbHelpers.h"  // for computation of lattice rho and velocity
#include <algorithm> // for finding the value in a vector


namespace olb {


///
template <typename T, template <typename U> class DESCRIPTOR>
reynoldsStressF3D<T,DESCRIPTOR>::reynoldsStressF3D(
  SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperLatticePhysVelocity3D<T, DESCRIPTOR>& f_velocity ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, const std::string filename )
  :
  SuperLatticePhysF3D<T,DESCRIPTOR>(sLattice,converter,6),
  _f_velocity(f_velocity),
  _cuboidGeometry(f_velocity.getSuperLattice3D().getCuboidGeometry()),
  _loadBalancer(sLattice.getLoadBalancer()),
  _latticeL(converter.getLatticeL()),
  _physOrigin(physOrigin),
  _physExtend(physExtend),
  _physPoint(std::vector<T> (3,T())),
  _latticePoint(std::vector<int> (4,0)),
  _localVelocity(std::vector<T> (3,0)),

  _ensemblecount(0),
  _token(0),
  _cell(0),

  _filename(filename)


{
  //reInit();

  this->_name = "reynoldsStressF";
}

template <typename T, template <typename U> class DESCRIPTOR>
bool reynoldsStressF3D<T,DESCRIPTOR>::operator() (T output[], const int input[])
{
  std::vector<int> latticePosition;
  latticePosition.push_back(input[0]);
  latticePosition.push_back(input[1]);
  latticePosition.push_back(input[2]);
  latticePosition.push_back(input[3]);
  if ( _loadBalancer.rank(latticePosition[0]) == singleton::mpi().getRank() ) {

    _cell = (std::find(_cells_on_cpu.begin(), _cells_on_cpu.end(), latticePosition)) - _cells_on_cpu.begin();

    if ( _cell < _cells_on_cpu.size() ) {
      for (int i = 0; i<6; i++) {
        output[i]=_ensembleAverageRST[_cell][i];
      }
      return true;
      //return std::vector<T> (_ensembleAverageRST[_cell].begin() , _ensembleAverageRST[_cell].end());
    } else {
      for (int i = 0; i<6; i++) {
        output[i]=0;
      }
      //return std::vector<T>(6,0);
      return true;
    }
  } else {
    for (int i = 0; i<6; i++) {
      output[i]=0;
    }
    //return std::vector<T>();
    return true;
  }
}

template <typename T, template <typename U> class DESCRIPTOR>
void reynoldsStressF3D<T,DESCRIPTOR>::print()
{

  ofstream ofile;
  stringstream fname;
  fname << _filename << ".dat" ;

  _token = 0;
  if (singleton::mpi().isMainProcessor()) {
    ofile.open(fname.str().c_str());
    if ( ofile.is_open() ) {
      ofile << "<?xml version='1.0'?>" << endl;
      ofile << "\n" << endl;
      ofile << "<RST>" << endl;
      ofile << "<header>" << endl;
      ofile << "title='AverageVelocity and RST'" << endl;
      // ofile << "firstTimestep='0' lastTimestep='0'" << endl;
      ofile << "No_usedEnsembles=" << _ensemblecount << endl;
      ofile << "</header>" << endl;
      ofile << "\n" << endl;



      for (int i=0; i<_cells_on_cpu.size(); ++i) {
        // get physical coordinates

        _physPoint = _cuboidGeometry.getPhysR(_cells_on_cpu[i]);

        ofile << "<cell>" << endl;
        ofile << "<position>" << endl;
        ofile << "<localLatticeCoordinates iC=\"" << _cells_on_cpu[i][0] << "\" iX=\"" << _cells_on_cpu[i][1] << "\" iY=\"" << _cells_on_cpu[i][2] << "\" iZ=\"" << _cells_on_cpu[i][3] << "\" />\"" << endl;
        ofile << "<globalphysCoordinates" << " X=\"" << _physPoint[0] << "\" Y=\"" << _physPoint[1] << "\" Z=\"" << _physPoint[2] << "\" />" << endl;
        //ofile << "<localLatticeCoordinates X=" << _cells_on_cpu[i][0] << " Y=" << _cells_on_cpu[i][1] << " Z=" << _cells_on_cpu[i][2] <<  " />" << endl;
        ofile << "</position>" << endl;

        ofile << "<data>" << endl;
        ofile << "<ensembleAveragedVelocity u=\"" << _ensembleAverageVelocitySum[i][0]/_ensemblecount << "\" v=\"" <<  _ensembleAverageVelocitySum[i][1]/_ensemblecount << "\" w=\"" << _ensembleAverageVelocitySum[i][2]/_ensemblecount << "\" />" << endl;
        ofile << "<reynoldsStressTensor uu=\"" << _ensembleAverageRST[i][0] << "\" vv=\"" << _ensembleAverageRST[i][1] << "\" ww=\"" << _ensembleAverageRST[i][2] << "\" uv=\"" << _ensembleAverageRST[i][3] << "\" vw=\"" << _ensembleAverageRST[i][4] << "\" uw=\"" << _ensembleAverageRST[i][5] << "\" />" << endl;
        ofile << "</data>" << endl;
        ofile << "</cell>" << endl;

      }
    } else {
      std::cout << "ERROR OPENING FILE TO WRITE REYNOLDSSTRESSES" << endl;
    }
    ofile.close();
#ifdef PARALLEL_MODE_MPI
    ///increment token to pass it on
    _token = (_token + 1)%singleton::mpi().getSize();

    ///loop to answer the remaining mpi reduces
    for (int i=1; i<singleton::mpi().getSize(); ++i) {
      //std::cout << "on mainproc "<<singleton::mpi().getRank() << ": i " << i << " bis zu max " << singleton::mpi().getSize() << endl;


      singleton::mpi().reduceAndBcast(_token, MPI_MAX);
    }
    if (singleton::mpi().getSize() == 1) {
      ofile.open(fname.str().c_str(), std::ios::app);
      ofile << "</RST>" << endl;
    }


#endif

  }

  else { //non main procs
#ifdef PARALLEL_MODE_MPI
    ///loop to answer the remaining mpi reduces
    for (int i=1; i<singleton::mpi().getSize(); ++i) {

      singleton::mpi().reduceAndBcast(_token, MPI_MAX);

      if (singleton::mpi().getRank()==_token) {
        //std::cout << "proc " << singleton::mpi().getRank() << " writing to file" << endl;
        ofile.open(fname.str().c_str(), std::ios::app);
        if ( ofile.is_open() ) {
          for (int i=0; i<_cells_on_cpu.size(); ++i) {
            // get physical coordinates

            _physPoint = _cuboidGeometry.getPhysR(_cells_on_cpu[i]);

            ofile << "<cell>" << endl;
            ofile << "<position>" << endl;
            ofile << "<localLatticeCoordinates iC=\"" << _cells_on_cpu[i][0] << "\" iX=\"" << _cells_on_cpu[i][1] << "\" iY=\"" << _cells_on_cpu[i][2] << "\" iZ=\"" << _cells_on_cpu[i][3] << "\" />" << endl;
            ofile << "<globalphysCoordinates" <<  " X=\"" << _physPoint[0] << "\" Y=\"" << _physPoint[1] << "\" Z=\"" << _physPoint[2] << "\" />" << endl;
            //ofile << "<globalLatticeCoordinates " << " iX=" << _cells_on_cpu[i][1] << " iY=" << _cells_on_cpu[i][2] << " iZ=" << _cells_on_cpu[i][2] << " \>" << endl;
            //ofile << "<localLatticeCoordinates X=" << _cells_on_cpu[i][0] << " Y=" << _cells_on_cpu[i][1] << " Z=" << _cells_on_cpu[i][2] <<  " />" << endl;
            ofile << "</position>" << endl;

            ofile << "<data>" << endl;
            ofile << "<ensembleAveragedVelocity u=\"" << _ensembleAverageVelocitySum[i][0]/_ensemblecount << "\" v=\"" <<  _ensembleAverageVelocitySum[i][1]/_ensemblecount << "\" w=\"" << _ensembleAverageVelocitySum[i][2]/_ensemblecount << "\" />" << endl;
            ofile << "<reynoldsStressTensor uu=\"" << _ensembleAverageRST[i][0] << "\" vv=\"" << _ensembleAverageRST[i][1] << "\" ww=\"" << _ensembleAverageRST[i][2] << "\" uv=\"" << _ensembleAverageRST[i][3] << "\" vw=\"" << _ensembleAverageRST[i][4] << "\" uw=\"" << _ensembleAverageRST[i][5] << "\" />" << endl;
            ofile << "</data>" << endl;
            ofile << "</cell>" << endl;
            //std::cout << "wrote a dataset" << endl;
          }
          if (singleton::mpi().getRank() == singleton::mpi().getSize()-1 ) {
            ofile << "</RST>" << endl;
          }
          ofile.close();
        }
        ///increment token and pass it on
        _token = (_token + 1)%singleton::mpi().getSize();
        //std::cout << "token: " << _token << " on rank " << singleton::mpi().getRank() << endl;

      }
    }
#endif
  }

}




/// REYNOLDSSTRESSFULLF3D

template <typename T, template <typename U> class DESCRIPTOR>
reynoldsStressFullF3D<T,DESCRIPTOR>::reynoldsStressFullF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,SuperLatticePhysVelocity3D<T, DESCRIPTOR>& f_velocity ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, const std::string filename )
  :
  reynoldsStressF3D<T,DESCRIPTOR>( sLattice, f_velocity , converter, physOrigin, physExtend, filename )
{
  reInit();
  this->_name = "reynoldsStressFullF";
}

template <typename T, template <typename U> class DESCRIPTOR>
void reynoldsStressFullF3D<T,DESCRIPTOR>::reInit()
{
  //std::cout << "inside reInit()" << endl;
  this->_cells_on_cpu.clear();

  for (this->_physPoint[0] = this->_physOrigin[0]; this->_physPoint[0] <= this->_physExtend[0]; this->_physPoint[0] += this->_latticeL ) {

    for (this->_physPoint[1] = this->_physOrigin[1]; this->_physPoint[1] <= this->_physExtend[1]; this->_physPoint[1] += this->_latticeL ) {

      for (this->_physPoint[2] = this->_physOrigin[2]; this->_physPoint[2] <= this->_physExtend[2]; this->_physPoint[2] += this->_latticeL) {


        ///getting latticePoint and respecting latticecuboid
        this->_cuboidGeometry.getLatticeR(this->_physPoint, this->_latticePoint);

        ///testing if the latticecuboid in which the lattice cell is contained is on the acual cpu, if so the return of the functor is added to the sum
        if ( this->_loadBalancer.rank(this->_latticePoint[0]) == singleton::mpi().getRank() ) {

          this->_cells_on_cpu.push_back (this->_latticePoint);


        }
      }
    }
  }

  this->_ensembleAverageVelocitySum = std::vector<std::vector<long double> > (this->_cells_on_cpu.size(),std::vector<long double> (3,0));
  this->_ensembleAverageRST = std::vector<std::vector<long double> > (this->_cells_on_cpu.size(),std::vector<long double> (6,0));
  //std::cout << "values in _cells_on_cpu: " << _cells_on_cpu.size() << endl;
}

template <typename T, template <typename U> class DESCRIPTOR>
void reynoldsStressFullF3D<T,DESCRIPTOR>::addEnsemble()
{


  ++(this->_ensemblecount);

  for (int i=0; i<this->_cells_on_cpu.size(); ++i) {

    this->_localVelocity = this->_f_velocity(this->_cells_on_cpu[i]);
    for (int j=0; j<this->_localVelocity.size(); ++j) {
      this->_ensembleAverageVelocitySum[i][j] = this->_ensembleAverageVelocitySum[i][j] + (long double) this->_localVelocity[j];
    }

    //(ensembleAvglocalStress[iZ][0] * (step-1) + (localvelocity[iZ][0] - ensembleAvgVelocity[iZ][0]) * (localvelocity[iZ][0] - ensembleAvgVelocity[iZ][0])) / (step); //uu
    this->_ensembleAverageRST[i][0]=(this->_ensembleAverageRST[i][0] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) * ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) ) / this->_ensemblecount; //uu
    this->_ensembleAverageRST[i][1]=(this->_ensembleAverageRST[i][1] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) * ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) ) / this->_ensemblecount; //vv
    this->_ensembleAverageRST[i][2]=(this->_ensembleAverageRST[i][2] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) * ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) ) / this->_ensemblecount; //ww
    this->_ensembleAverageRST[i][3]=(this->_ensembleAverageRST[i][3] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) * ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) ) / this->_ensemblecount; //uv
    this->_ensembleAverageRST[i][4]=(this->_ensembleAverageRST[i][4] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) * ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) ) / this->_ensemblecount; //vw
    this->_ensembleAverageRST[i][5]=(this->_ensembleAverageRST[i][5] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) * ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) ) / this->_ensemblecount; //uw




  }


}

/// REYNOLDSSTRESSPLANEF3D

template <typename T, template <typename U> class DESCRIPTOR>
reynoldsStressPlaneF3D<T,DESCRIPTOR>::reynoldsStressPlaneF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,SuperLatticePhysVelocity3D<T, DESCRIPTOR>& f_velocity ,SuperGeometry3D<T> & superGeometry, LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, std::vector<bool> plane_normal, const std::string filename )
  :
  reynoldsStressF3D<T,DESCRIPTOR>( sLattice, f_velocity , converter , physOrigin, physExtend, filename ),
  _plane_normal(plane_normal),
  _f_velocity_average(SuperAverage3D<T> (f_velocity ,converter, this->_loadBalancer , physOrigin, physExtend)),
  _superGeometry(superGeometry),
  _physOrigin_plane(std::vector<T> (3,0)),
  _physExtend_plane(std::vector<T> (3,0))

{
  reInit();
  this->getName() = "reynoldsStressPlaneF";
}

template <typename T, template <typename U> class DESCRIPTOR>
void reynoldsStressPlaneF3D<T,DESCRIPTOR>::reInit()
{
  //std::cout << "inside reInit()" << endl;
  this->_cells_on_cpu.clear();

  for (this->_physPoint[0] = this->_physOrigin[0]; this->_physPoint[0] <= this->_physExtend[0]; this->_physPoint[0] += this->_latticeL ) {

    for (this->_physPoint[1] = this->_physOrigin[1]; this->_physPoint[1] <= this->_physExtend[1]; this->_physPoint[1] += this->_latticeL ) {

      for (this->_physPoint[2] = this->_physOrigin[2]; this->_physPoint[2] <= this->_physExtend[2]; this->_physPoint[2] += this->_latticeL) {

        //std::cout << " phys point 2 " << this->_physPoint[2] << " phys extend 2 " << this->_physExtend[2] << " diff " << this->_physExtend[2] - this->_physPoint[2] << std::endl;
        ///getting latticePoint and respecting latticecuboid
        this->_cuboidGeometry.getLatticeR(this->_physPoint, this->_latticePoint);

        ///testing if the latticecuboid in which the lattice cell is contained is on the acual cpu, if so the return of the functor is added to the sum
        if ( this->_loadBalancer.rank(this->_latticePoint[0]) == singleton::mpi().getRank() ) {

          //std::cout << " _cells_on_cpu 2 " <<  this->_latticePoint[3] <<  " physPoint 2 " << this->_physPoint[2] <<  " diff to extend " << this->_physExtend[2] - this->_physPoint[2] << " diff to origin " << this->_physOrigin[2] - this->_physPoint[2] << std::endl;
          this->_cells_on_cpu.push_back (this->_latticePoint);


        }
      }
    }
  }

  this->_ensembleAverageVelocitySum = std::vector<std::vector<long double> > (this->_cells_on_cpu.size(),std::vector<long double> (3,0));
  this->_ensembleAverageRST = std::vector<std::vector<long double> > (this->_cells_on_cpu.size(),std::vector<long double> (6,0));

  //std::cout << "values in _cells_on_cpu: " << _cells_on_cpu.size() << endl;
  /// plane normal is 0,0,1
  if ((_plane_normal[0]==false) & (_plane_normal[1] == false) & (_plane_normal[2] == true)) {
    //std::cout << " max " << int(floor(this->_physExtend[2]/this->_latticeL)) << " min " << int(floor(this->_physOrigin[2]/this->_latticeL)) << std::endl;
    _velocity_average_plane = std::vector<std::vector<T> > ((int(floor(this->_physExtend[2]/this->_latticeL)) - int(floor(this->_physOrigin[2]/this->_latticeL)))+2, std::vector<T> (3,T()));

    _physOrigin_plane[0] = _superGeometry.getStatistics().getMinPhysR(1)[0];
    _physOrigin_plane[1] = _superGeometry.getStatistics().getMinPhysR(1)[1];

    _physExtend_plane[0] = _superGeometry.getStatistics().getMaxPhysR(1)[0];
    _physExtend_plane[1] = _superGeometry.getStatistics().getMaxPhysR(1)[1];
  }
  /// plane normal is 0,1,0
  else if ((_plane_normal[0]==false) & (_plane_normal[1] == true) & (_plane_normal[2] == false)) {

    _velocity_average_plane = std::vector<std::vector<T> > ((int(floor(this->_physExtend[1]/this->_latticeL)) - int(floor(this->_physOrigin[1]/this->_latticeL)))+2, std::vector<T> (3,T()));

    _physOrigin_plane[0] = _superGeometry.getStatistics().getMinPhysR(1)[0];
    _physOrigin_plane[2] = _superGeometry.getStatistics().getMinPhysR(1)[2];

    _physExtend_plane[0] = _superGeometry.getStatistics().getMaxPhysR(1)[0];
    _physExtend_plane[2] = _superGeometry.getStatistics().getMaxPhysR(1)[2];
  }
  /// plane normal is 1,0,0
  else if ((_plane_normal[0]==false) & (_plane_normal[1] == true) & (_plane_normal[2] == false)) {

    _velocity_average_plane = std::vector<std::vector<T> > ((int(floor(this->_physExtend[0]/this->_latticeL)) - int(floor(this->_physOrigin[0]/this->_latticeL)))+2, std::vector<T> (3,T()));

    _physOrigin_plane[1] = _superGeometry.getStatistics().getMinPhysR(1)[1];
    _physOrigin_plane[2] = _superGeometry.getStatistics().getMinPhysR(1)[2];

    _physExtend_plane[1] = _superGeometry.getStatistics().getMaxPhysR(1)[1];
    _physExtend_plane[2] = _superGeometry.getStatistics().getMaxPhysR(1)[2];
  }

  else {
    std::cout << "Error in constructor of ReynoldsStressPlaneF3D: this plane is not yet definded, cancel the calculation!!!" << std::endl;
  }
}

template <typename T, template <typename U> class DESCRIPTOR>
void reynoldsStressPlaneF3D<T,DESCRIPTOR>::addEnsemble()
{
  //std::cout << "vor plane " << std::endl;
  //std::cout << "size velaverageplane " << _velocity_average_plane.size() << std::endl;
  //std::cout << "size cells_on_cpu " << this->_cells_on_cpu.size() << std::endl;
  /// plane with z=const
  if ((_plane_normal[2] == true)) {
    int ii=0;
    for (double iZ=this->_physOrigin[2]; iZ <= this->_physExtend[2]; iZ += this->_latticeL ) {

      _physOrigin_plane[2] = iZ;
      _physExtend_plane[2] = iZ;
      //std::cout << " vor reinit , iteration " << ii << "on rank " << singleton::mpi().getRank() <<  std::endl;
      _f_velocity_average.reInit(_physOrigin_plane, _physExtend_plane);
      //std::cout << "nach reinit" << std::endl;
      _velocity_average_plane[ii] = _f_velocity_average();
      //std::cout << "iterator ii " << ii << std::endl;
      ++ii;
    }

  }
  /// plane with y=const
  else if ((_plane_normal[1] == true)) {
    int ii=0;
    for (double iY=this->_physOrigin[1]; iY <= this->_physExtend[1]; iY += this->_latticeL ) {

      _physOrigin_plane[1] = iY;
      _physExtend_plane[1] = iY;

      _f_velocity_average.reInit(_physOrigin_plane, _physExtend_plane);

      _velocity_average_plane[ii] = _f_velocity_average();
      ++ii;
    }

  }
  /// plane with x=const
  else if ((_plane_normal[0] == true)) {
    int ii=0;
    for (double iX=this->_physOrigin[0]; iX <= this->_physExtend[0]; iX += this->_latticeL ) {

      _physOrigin_plane[0] = iX;
      _physExtend_plane[0] = iX;

      _f_velocity_average.reInit(_physOrigin_plane, _physExtend_plane);

      _velocity_average_plane[ii] = _f_velocity_average();
      ++ii;
    }

  }
  //std::cout << "nach plane " << std::endl;
  ++(this->_ensemblecount);

  //std::cout << "size " << this->_cells_on_cpu.size() << " on rank " << singleton::mpi().getRank() << std::endl;
  for (int i=0; i<this->_cells_on_cpu.size(); ++i) {

    this->_localVelocity = this->_f_velocity(this->_cells_on_cpu[i]);
    T physPoint[3]{0.,0.,0.};
    int latticePoint[4]{0,0,0,0};
    latticePoint[0]=this->_cells_on_cpu[i][0];
    latticePoint[1]=this->_cells_on_cpu[i][0];
    latticePoint[2]=this->_cells_on_cpu[i][0];
    latticePoint[3]=this->_cells_on_cpu[i][0];
    this->_cuboidGeometry.getPhysR(physPoint,latticePoint);
    int index = (int) round((physPoint[2]-this->_physOrigin[2])/this->_latticeL);

    //std::cout << "resulting index " << index << " on cpu " << singleton::mpi().getRank() << std::endl;

    for (int j=0; j<this->_localVelocity.size(); ++j) {
      this->_ensembleAverageVelocitySum[i][j] = this->_ensembleAverageVelocitySum[i][j] + (long double) this->_velocity_average_plane[index][j];
    }

    this->_ensembleAverageRST[i][0]=(this->_ensembleAverageRST[i][0] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) * ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) ) / this->_ensemblecount; //uu
    this->_ensembleAverageRST[i][1]=(this->_ensembleAverageRST[i][1] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) * ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) ) / this->_ensemblecount; //vv
    this->_ensembleAverageRST[i][2]=(this->_ensembleAverageRST[i][2] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) * ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) ) / this->_ensemblecount; //ww
    this->_ensembleAverageRST[i][3]=(this->_ensembleAverageRST[i][3] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) * ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) ) / this->_ensemblecount; //uv
    this->_ensembleAverageRST[i][4]=(this->_ensembleAverageRST[i][4] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[1] - this->_ensembleAverageVelocitySum[i][1] / this->_ensemblecount) * ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) ) / this->_ensemblecount; //vw
    this->_ensembleAverageRST[i][5]=(this->_ensembleAverageRST[i][5] * (this->_ensemblecount - 1) + ((long double) this->_localVelocity[0] - this->_ensembleAverageVelocitySum[i][0] / this->_ensemblecount) * ((long double) this->_localVelocity[2] - this->_ensembleAverageVelocitySum[i][2] / this->_ensemblecount) ) / this->_ensemblecount; //uw
  }


}


} // end namespace olb

#endif
