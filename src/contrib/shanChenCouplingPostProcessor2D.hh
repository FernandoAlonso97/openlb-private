/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas, Jonas Latt, Andrea Parmigiani
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef SHAN_CHEN_COUPLING_POST_PROCESSOR_2D_HH
#define SHAN_CHEN_COUPLING_POST_PROCESSOR_2D_HH

#include "contrib/shanChenLatticeDescriptors.h"
#include "shanChenCouplingPostProcessor2D.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "core/finiteDifference2D.h"

namespace olb {

////////  ShanChenCouplingPostProcessor2D ///////////////////////////////////

template<typename T, template<typename U> class Lattice>
ShanChenCouplingPostProcessor2D <T,Lattice>::
ShanChenCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_, T G_,
                                std::vector<SpatiallyExtendedObject2D*> partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_), G(G_), partners(partners_)
{
}

template<typename T, template<typename U> class Lattice>
void ShanChenCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef lbHelpers<T,Lattice> lbH;

  int nx = blockLattice.getNx();
  int ny = blockLattice.getNy();

  BlockLattice2D<T,ShanChenD2Q9Descriptor> *partnerLattice =
    dynamic_cast<BlockLattice2D<T,ShanChenD2Q9Descriptor> *>(partners[0]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T blockOmega = blockLattice.getDynamics(iX,iY)->getOmega();
        T partnerOmega = partnerLattice->getDynamics(iX,iY)->getOmega();

        //computation of the total velocity
        T rhoBlock = blockLattice.get(iX,iY).computeRho();
        T rhoPartner = partnerLattice->get(iX,iY).computeRho();
        T jBlock[2] = {T(),T()};
        T jPartner[2] = {T(),T()};

        for (int iPop = 0; iPop < L::q; ++iPop) {
          for (int iD = 0; iD < L::d; ++iD) {
            jBlock[iD] += blockLattice.get(iX,iY)[iPop] * L::c(iPop)[iD];
            jPartner[iD] += partnerLattice->get(iX,iY)[iPop] * L::c(iPop)[iD];
          }
        }

        T rhoTot = rhoBlock*blockOmega+rhoPartner*partnerOmega;
        T uTot[2] = {T(), T()};
        for (int iD = 0; iD < L::d; ++iD) {
          uTot[iD] = (jBlock[iD]*blockOmega +
                      jPartner[iD]*partnerOmega) /
                     rhoTot;
          //                     std::cout << uTot[iD] << ", ";
        }
        //                 std::cout << "\n";

        // force to add for the partnerLattice
        //                 T rhoPartner = partnerLattice->get(iX,iY).computeRho();

        T rhoBlockContribution[2] = {T(), T()};
        T rhoPartnerContribution[2] = {T(), T()};
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
          int nextX = (iX + L::c(iPop)[0] + nx) % nx;
          int nextY = (iY + L::c(iPop)[1] + ny) % ny;

          for (int iD = 0; iD < L::d; ++iD) {
            rhoBlockContribution[iD] +=
              blockLattice.get(nextX,nextY).computeRho() *
              L::c(iPop)[iD];

            rhoPartnerContribution[iD] +=
              partnerLattice->get(nextX,nextY).computeRho() *
              L::c(iPop)[iD];
          }
        }

        T uBlock[2] = {T(),T()};
        T uPartner[2] = {T(),T()};
        for (int iD = 0; iD < L::d; ++iD) {
          uBlock[iD] = uTot[iD] - G/blockOmega * rhoPartnerContribution[iD];
          uPartner[iD] = uTot[iD] - G/partnerOmega* rhoBlockContribution[iD];
        }


        // Note that the G (Green function or range and strength
        // of the force)
        // factor in multiplying the force is missing
        // it will be in the dynamics object.
        blockLattice.get(iX,iY).defineExternalField (
          ShanChenD2Q9Descriptor<T>::ExternalField::velocityBeginsAt,
          ShanChenD2Q9Descriptor<T>::ExternalField::sizeOfVelocity,
          uBlock );

        partnerLattice->get(iX,iY).defineExternalField (
          ShanChenD2Q9Descriptor<T>::ExternalField::velocityBeginsAt,
          ShanChenD2Q9Descriptor<T>::ExternalField::sizeOfVelocity,
          uPartner );
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void ShanChenCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

/// LatticeCouplingGenerator for NS coupling

template<typename T, template<typename U> class Lattice>
ShanChenCouplingGenerator2D<T,Lattice>::ShanChenCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_, T G_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_), G(G_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* ShanChenCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D*> partners) const
{
  return new ShanChenCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1,G, partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* ShanChenCouplingGenerator2D<T,Lattice>::clone() const
{
  return new ShanChenCouplingGenerator2D<T,Lattice>(*this);
}

// =====================================================================//
// ============Fluid wall and ShanChen Interaction======================//
// =====================================================================//


template<typename T, template<typename U> class Lattice>
ShanChenFluidWallCouplingPostProcessor2D<T,Lattice>::
ShanChenFluidWallCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_, T G_,
    T GWett_, T GNonWett_, ScalarField2D<int> &walls_,
    std::vector<SpatiallyExtendedObject2D*> partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     G(G_), GWett(GWett_), GNonWett(GNonWett_),
     walls(walls_), partners(partners_)
{
}

///Coupling Post Processor for fluid-solid Interaction and fluid-fluid Interaction
/// Ciao Orestis ... we should add something which allow us to change the equation of state (EOS)
template<typename T, template<typename U> class Lattice>
void ShanChenFluidWallCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  typedef lbHelpers<T,Lattice> lbH;

  int nx = blockLattice.getNx();
  int ny = blockLattice.getNy();

  BlockLattice2D<T,ShanChenD2Q9Descriptor> *partnerLattice =
    dynamic_cast<BlockLattice2D<T,ShanChenD2Q9Descriptor> *>(partners[0]);

  /// Not clear to me
  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T blockOmega = blockLattice.getDynamics(iX,iY)->getOmega();
        T partnerOmega = partnerLattice->getDynamics(iX,iY)->getOmega();

        //computation of the total velocity
        T rhoBlock, uBlock[2] = {T(), T()};
        T rhoPartner, uPartner[2] = {T(), T()};
        blockLattice.get(iX,iY).computeRhoU(rhoBlock,uBlock);
        partnerLattice->get(iX,iY).computeRhoU(rhoPartner,uPartner);

        T jBlock[2] = {T(),T()};
        T jPartner[2] = {T(),T()};

        for (int iPop = 0; iPop < L::q; ++iPop) {
          rhoBlock += blockLattice.get(iX,iY)[iPop];
          rhoPartner += partnerLattice->get(iX,iY)[iPop];
          for (int iD = 0; iD < L::d; ++iD) {
            jBlock[iD] += blockLattice.get(iX,iY)[iPop] * L::c(iPop)[iD];
            jPartner[iD] += partnerLattice->get(iX,iY)[iPop] * L::c(iPop)[iD];
          }
        }

        /*                T jBlock[2] = {T(),T()};
                        T jPartner[2] = {T(),T()};
                        for (int iD = 0; iD < L::d; ++iD)
                        {
                            jBlock[iD] = rhoBlock * uBlock[iD];
                            jPartner[iD] = rhoPartner * uPartner[iD];
        //                     std::cout << jBlock[iD] << " ";
        //                     std::cout << jPartner[iD] << "\n";
                        }*/

        T rhoTot = (rhoBlock+(T)1)*blockOmega+(rhoPartner+(T)1)*partnerOmega;
        T uTot[2] = {T(), T()};
        for (int iD = 0; iD < L::d; ++iD) {
          uTot[iD] = (jBlock[iD]*blockOmega +
                      jPartner[iD]*partnerOmega) /
                     rhoTot;
          //                     std::cout << uTot[iD] << ", ";
        }
        //                 std::cout << "\n";

        T rhoBlockContribution[2] = {T(), T()};
        T rhoPartnerContribution[2] = {T(), T()};
        T wallContribution[2] = {T(), T()};
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
          int nextX = (iX + L::c(iPop)[0]+nx) % nx;
          int nextY = (iY + L::c(iPop)[1]+ny) % ny;

          for (int iD = 0; iD < L::d; ++iD) {
            rhoBlockContribution[iD] +=
              blockLattice.get(nextX,nextY).computeRho() *
              L::c(iPop)[iD];

            rhoPartnerContribution[iD] +=
              partnerLattice->get(nextX,nextY).computeRho() *
              L::c(iPop)[iD];
          }

          if (walls.get(nextX,nextY)==1) {
            ///Solid Boundary
            for (int iD = 0; iD < L::d; ++iD) {
              wallContribution[iD] += (T)L::c(iPop)[iD];
            }
          }
        }

        for (int iD = 0; iD < L::d; ++iD) {

          uBlock[iD] = uTot[iD] -
                       G/blockOmega * rhoPartnerContribution[iD]/* -
                            GWett/blockOmega *wallContribution[iD]*/ -
                       GNonWett/blockOmega *wallContribution[iD];
          uPartner[iD] = uTot[iD] -
                         G/partnerOmega* rhoBlockContribution[iD] -
                         GWett/partnerOmega *wallContribution[iD]/* -
                            GNonWett/partnerOmega *wallContribution[iD]*/;
          //                     std::cout << uTot[iD] << ", ";
          //                     std::cout << wallContribution[iD] << ", ";
        }
        //                 std::cout << iX << " " << iY << " " << rhoTot << "\n";
        //                 std::cout << "\n";

        // Note that the G (Green function or range and strength
        // of the force)
        // factor in multiplying the force is missing
        // it will be in the dynamics object.
        blockLattice.get(iX,iY).defineExternalField (
          ShanChenD2Q9Descriptor<T>::ExternalField::velocityBeginsAt,
          ShanChenD2Q9Descriptor<T>::ExternalField::sizeOfVelocity,
          uBlock );

        partnerLattice->get(iX,iY).defineExternalField (
          ShanChenD2Q9Descriptor<T>::ExternalField::velocityBeginsAt,
          ShanChenD2Q9Descriptor<T>::ExternalField::sizeOfVelocity,
          uPartner );
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void ShanChenFluidWallCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

/// LatticeCouplingGenerator for NS coupling

template<typename T, template<typename U> class Lattice>
ShanChenFluidWallCouplingGenerator2D<T,Lattice>::ShanChenFluidWallCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_, T G_, T GWett_, T GNonWett_, ScalarField2D<int>& walls_ )
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    G(G_), GWett(GWett_), GNonWett(GNonWett_),  walls(walls_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* ShanChenFluidWallCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D*> partners) const
{
  return new ShanChenFluidWallCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1,G, GWett, GNonWett, walls, partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* ShanChenFluidWallCouplingGenerator2D<T,Lattice>::clone() const
{
  return new ShanChenFluidWallCouplingGenerator2D<T,Lattice>(*this);
}


}  // namespace olb

#endif
