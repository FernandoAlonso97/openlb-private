/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas, Andrea Parmigiani
 *  Address: EPFL, STI-LIN Station 9, 1015 Lausanne, Switzerland
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- generic implementation.
 */
#ifndef SHAN_CHEN_DYNAMICS_HH
#define SHAN_CHEN_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "shanChenLatticeDescriptors.h"
#include "shanChenDynamics.h"

namespace olb {


////////////////////// Class ShanChenDynamics //////////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice>
ShanChenDynamics<T,Lattice>::ShanChenDynamics (
  T omega_, Momenta& momenta_)
  : BasicDynamics<T,Lattice>(momenta_),
    omega(omega_)
{ }

template<typename T, template<typename U> class Lattice>
ShanChenDynamics<T,Lattice>* ShanChenDynamics<T,Lattice>::clone() const
{
  return new ShanChenDynamics<T,Lattice>(*this);
}

template<typename T, template<typename U> class Lattice>
T ShanChenDynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class Lattice>
void ShanChenDynamics<T,Lattice>::collide(CellView<T,Lattice>& cell,
    LatticeStatistics<T>& statistics )
{
  T rho = this->momenta.computeRho(cell);

  T u[Lattice<T>::d];
  cell.computeExternalField(Lattice<T>::ExternalField::velocityBeginsAt, Lattice<T>::d, u);

  T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell,rho,u,omega);

  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice>
T ShanChenDynamics<T,Lattice>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice>
void ShanChenDynamics<T,Lattice>::setOmega(T omega_)
{
  omega = omega_;
}

} // namespace olb

#endif
