/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Andrea Parmigiani, Orestis Malaspinas,
 *  Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- header file
 */
#ifndef SHAN_CHEN_LATTICE_DESCRIPTORS_H
#define SHAN_CHEN_LATTICE_DESCRIPTORS_H

#include <vector>

namespace olb {

/// Descriptors for the 2D and 3D lattices.
/** \warning Attention: The lattice directions must always be ordered in
 * such a way that c[i] = -c[i+(q-1)/2] for i=1..(q-1)/2, and c[0] = 0 must
 * be the rest velocity. Furthermore, the velocities c[i] for i=1..(q-1)/2
 * must verify
 *  - in 2D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *  - in 3D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *                       || (c[i][0]==0 && c[i][1]==0 && c[i][2]<0)
 * Otherwise some of the code will work erroneously, because the
 * aformentioned relations are taken as given to enable a few
 * optimizations.
*/
namespace descriptors {
//===========================================================================//
//=================== AdvectionDiffusion Lattice Descriptors==========================//
//===========================================================================//
struct ExternalVelocity2Ddescriptor {
  static const int numScalars = 2;
  static const int numSpecies = 1;
  static const int velocityBeginsAt = 0;
  static const int sizeOfVelocity   = 2;
};

/// D2Q9 lattice
template <typename T>
struct ShanChenD2Q9Descriptor {
  typedef ShanChenD2Q9Descriptor<T> BaseDescriptor;
  typedef ExternalVelocity2Ddescriptor ExternalField;
  enum { d = 2, q = 9 };      ///< number of dimensions/distr. functions
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
};

}  // namespace descriptors

}  // namespace olb

#endif
