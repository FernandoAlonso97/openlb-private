/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Jonas Latt, Orestis Malaspinas
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- header file
 */
#ifndef THIRTY_NINE_LATTICE_DESCRIPTORS_H
#define THIRTY_NINE_LATTICE_DESCRIPTORS_H

#include <vector>
#include "core/olbDebug.h"

namespace olb {

/// Descriptors for the 2D and 3D lattices.
/** \warning Attention: The lattice directions must always be ordered in
 * such a way that c[i] = -c[i+(q-1)/2] for i=1..(q-1)/2, and c[0] = 0 must
 * be the rest velocity. Furthermore, the velocities c[i] for i=1..(q-1)/2
 * must verify
 *  - in 2D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *  - in 3D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *                       || (c[i][0]==0 && c[i][1]==0 && c[i][2]<0)
 * Otherwise some of the code will work erroneously, because the
 * aformentioned relations are taken as given to enable a few
 * optimizations.
*/
namespace descriptors {
/// D2Q17 lattice
template <typename T> struct D2Q17DescriptorBase {
  typedef D2Q17DescriptorBase<T> BaseDescriptor;
  enum { d = 2, q = 17 };     ///< number of dimensions/distr. functions
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const int vicinity;      ///< size of the vicinity
  static const T length;
  static const T invCs2;
};

/// D3Q39 lattice
template <typename T> struct D3Q39DescriptorBase {
  typedef D3Q39DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 39 };     ///< number of dimensions/distr. functions
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const int vicinity;      ///< size of the vicinity
  static const T length;
  static const T invCs2;
};

template <typename T> struct D2Q17Descriptor
  : public D2Q17DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct ForcedD2Q17Descriptor
  : public D2Q17DescriptorBase<T>, public Force2dDescriptorBase {
};


template <typename T> struct D3Q39Descriptor
  : public D3Q39DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct ForcedD3Q39Descriptor
  : public D3Q39DescriptorBase<T>, public Force3dDescriptorBase {
};
}  // namespace descriptors

}  // namespace olb

#endif
