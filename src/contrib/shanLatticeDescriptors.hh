/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Jonas Latt, Orestis Malaspinas
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- generic code
 */
#ifndef THIRTY_NINE_LATTICE_DESCRIPTORS_HH
#define THIRTY_NINE_LATTICE_DESCRIPTORS_HH

#include "shanLatticeDescriptors.h"

namespace olb {

namespace descriptors {

// D2Q17 ///////////////////////////////////////////////////////////

template<typename T>
const int D2Q17DescriptorBase<T>::c
[D2Q17DescriptorBase<T>::q][D2Q17DescriptorBase<T>::d] = {
  { 0, 0},

  {-1, 0}, { 0,-1},
  {-3, 0}, { 0,-3},

  {-1,-1,}, {-1, 1},

  {-2,-2,}, {-2, 2},

  { 1, 0}, { 0, 1},
  { 3, 0}, { 0, 3},

  { 1, 1}, { 1,-1},

  { 2, 2}, { 2,-2},
};

template<typename T>
const int D2Q17DescriptorBase<T>::opposite[D2Q17DescriptorBase<T>::q] = {
  0, 9, 10, 11, 12, 13, 14, 15, 16, 1, 2, 3, 4, 5, 6, 7, 8
};

template<typename T>
const T D2Q17DescriptorBase<T>::t[D2Q17DescriptorBase<T>::q] = {
  ((T)575+(T)193*sqrt((T)193))/ (T)8100,

  ((T)3355-(T)91*sqrt((T)193))/ (T)18000,((T)3355-(T)91*sqrt((T)193))/ (T)18000,
  ((T)1445-(T)101*sqrt((T)193))/ (T)162000,((T)1445-(T)101*sqrt((T)193))/ (T)162000,

  ((T)655+(T)17*sqrt((T)193))/ (T)27000,((T)655+(T)17*sqrt((T)193))/ (T)27000,

  ((T)685-(T)49*sqrt((T)193))/ (T)54000,((T)685-(T)49*sqrt((T)193))/ (T)54000,

  ((T)3355-(T)91*sqrt((T)193))/ (T)18000,((T)3355-(T)91*sqrt((T)193))/ (T)18000,
  ((T)1445-(T)101*sqrt((T)193))/ (T)162000,((T)1445-(T)101*sqrt((T)193))/ (T)162000,

  ((T)655+(T)17*sqrt((T)193))/ (T)27000,((T)655+(T)17*sqrt((T)193))/ (T)27000,

  ((T)685-(T)49*sqrt((T)193))/ (T)54000,((T)685-(T)49*sqrt((T)193))/ (T)54000
};

template<typename T>
const int D2Q17DescriptorBase<T>::vicinity = 3;

template<typename T>
const T D2Q17DescriptorBase<T>::length = sqrt(((T)125+(T)5*sqrt((T)193)) / (T)72);

template<typename T>
const T D2Q17DescriptorBase<T>::invCs2 = ((T)125+(T)5*sqrt((T)193)) / (T)72;
// D3Q39 ///////////////////////////////////////////////////////////

template<typename T>
const int D3Q39DescriptorBase<T>::c
[D3Q39DescriptorBase<T>::q][D3Q39DescriptorBase<T>::d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-2, 0, 0}, { 0,-2, 0}, { 0, 0,-2},
  {-3, 0, 0}, { 0,-3, 0}, { 0, 0,-3},

  {-2,-2, 0}, {-2, 2, 0}, {-2, 0,-2},
  {-2, 0, 2}, { 0,-2,-2}, { 0,-2, 2},

  {-1,-1,-1}, {-1,-1, 1}, {-1, 1,-1}, {-1, 1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 2, 0, 0}, { 0, 2, 0}, { 0, 0, 2},
  { 3, 0, 0}, { 0, 3, 0}, { 0, 0, 3},

  { 1, 1, 1}, { 1, 1,-1}, { 1,-1, 1}, { 1,-1,-1},

  { 2, 2, 0}, { 2,-2, 0}, { 2, 0, 2},
  { 2, 0,-2}, { 0, 2, 2}, { 0, 2,-2},
};

template<typename T>
const int D3Q39DescriptorBase<T>::opposite[D3Q39DescriptorBase<T>::q] = {
  0, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
};

template<typename T>
const T D3Q39DescriptorBase<T>::t[D3Q39DescriptorBase<T>::q] = {
  (T)1/(T)12,

  (T)1/(T)12,   (T)1/(T)12,   (T)1/(T)12,
  (T)2/(T)135,  (T)2/(T)135,  (T)2/(T)135,
  (T)1/(T)1062, (T)1/(T)1062, (T)1/(T)1062,

  (T)1/(T)432,  (T)1/(T)432,  (T)1/(T)432,
  (T)1/(T)432,  (T)1/(T)432,  (T)1/(T)432,

  (T)1/(T)27,   (T)1/(T)27,   (T)1/(T)27,   (T)1/(T)27,

  (T)1/(T)12,   (T)1/(T)12,   (T)1/(T)12,
  (T)2/(T)135,  (T)2/(T)135,  (T)2/(T)135,
  (T)1/(T)1062, (T)1/(T)1062, (T)1/(T)1062,

  (T)1/(T)432,  (T)1/(T)432,  (T)1/(T)432,
  (T)1/(T)432,  (T)1/(T)432,  (T)1/(T)432,

  (T)1/(T)27,   (T)1/(T)27,   (T)1/(T)27,   (T)1/(T)27,
};

template<typename T>
const int D3Q39DescriptorBase<T>::vicinity = 3;

template<typename T>
const T D3Q39DescriptorBase<T>::length = sqrt((T)3/(T)2);

template<typename T>
const T D3Q39DescriptorBase<T>::invCs2 = (T)3/(T)2;

}  // namespace descriptors

}  // namespace olb

#endif
