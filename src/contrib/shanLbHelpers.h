/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Helper functions for the implementation of LB dynamics. This file is all
 * about efficiency. The generic template code is specialized for commonly
 * used Lattices, so that a maximum performance can be taken out of each
 * case.
 */
#ifndef SHAN_LB_HELPERS_H
#define SHAN_LB_HELPERS_H

#include "shanLatticeDescriptors.h"
#include "core/cell.h"
#include "core/util.h"


namespace olb {


// Forward declarations
template<typename T, class Descriptor> struct shanLbDynamicsHelpers;
template<typename T, class Descriptor> struct shanLbMomentaHelpers;
template<typename T, template<typename U> class Lattice> struct shanLbLatticeHelpers;

/// This structure forwards the calls to the appropriate helper class
template<typename T, template<typename U> class Lattice>
struct shanLbHelpers {

  static T equilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr,
                       const T temperature)
  {
    return shanLbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::equilibrium(iPop, rho, u, uSqr, temperature);
  }

  static T bgkCollision(CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d], T temperature, T omega)
  {
    return shanLbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::bgkCollision(&cell[0], rho, u, temperature, omega);
  }

  static T computeTemperature(CellView<T,Lattice>& cell, T rho, T uSqr)
  {
    return shanLbMomentaHelpers<T,typename Lattice<T>::BaseDescriptor>::computeTemperature(&cell[0], rho, uSqr);
  }
};  // struct shanLbHelpers


/// All helper functions are inside this structure
template<typename T, class Descriptor>
struct shanLbDynamicsHelpers {
  /// Computation of equilibrium distribution
  static T equilibrium(int iPop, T rho, const T u[Descriptor::d],
                       T uSqr, const T temperature)
  {
    T c_u = T();
    T cSqr = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      c_u += Descriptor::c(iPop)[iD]*u[iD];
      cSqr += Descriptor::c(iPop)[iD] * Descriptor::c(iPop)[iD];
    }
    c_u *= Descriptor::invCs2;
    uSqr*= Descriptor::invCs2;
    cSqr *= Descriptor::invCs2;

    return rho * Descriptor::t[iPop] * (
             (T)1 + c_u +
             (T)1/(T)2 * (c_u*c_u - uSqr + (temperature - (T)1)*(cSqr - Descriptor::d)) +
             c_u/(T)6* (c_u*c_u - (T)3*uSqr + (T)3*(temperature - (T)1) * (cSqr - Descriptor::d - (T)2))
           ) - Descriptor::t[iPop];
  }

  /// BGK collision step
  static T bgkCollision(T* cell, T rho, const T u[Descriptor::d], T temperature,T omega)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      cell[iPop] *= (T)1-omega;
      cell[iPop] += omega * shanLbDynamicsHelpers<T,Descriptor>::equilibrium (
                      iPop, rho, u, uSqr, temperature);
    }
    return uSqr;
  }

};  // struct shanLbDynamicsHelpers

/// All helper functions are inside this structure
template<typename T, class Descriptor>
struct shanLbMomentaHelpers {
  /// Computation of the temperature in the bulk
  static T computeTemperature(T* cell, T rho, T uSqr)
  {
    T temp = T();

    for (int iPop = 0; iPop < Descriptor::q; ++iPop) {
      for (int iA = 0; iA < Descriptor::d; ++iA) {
        temp += cell[iPop] * Descriptor::c(iPop)[iA] * Descriptor::c(iPop)[iA];
      }
    }
    temp += (T)Descriptor::d/Descriptor::invCs2;
    temp -= rho*uSqr;
    temp /= rho*(T)Descriptor::d;
    temp *= Descriptor::invCs2;

    return temp;
  }

};  // struct shanLbDynamicsHelpers

}  // namespace olb

// The specialized code is directly included. That is because we never want
// it to be precompiled so that in both the precompiled and the
// "include-everything" version, the compiler can apply all the
// optimizations it wants.
// #include "shanLbHelpers2D.h"
// #include "shanLbHelpers3D.h"

#endif
