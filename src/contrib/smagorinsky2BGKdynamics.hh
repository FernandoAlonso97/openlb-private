/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause, Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * BGK Dynamics with adjusted omega -- generic implementation.
 */
#ifndef SMAGORINSKY_2_BGK_DYNAMICS_HH
#define SMAGORINSKY_2_BGK_DYNAMICS_HH

#include "smagorinsky2BGKdynamics.h"
#include "core/cell.h"
#include "core/util.h"
#include "dynamics/lbHelpers.h"
#include "core/units.h"
#include "core/units.hh"
#include "math.h"

namespace olb {

////////////////////// Class SmagorinskyBGKdynamics //////////////////////////

/** \param vs2_ speed of sound
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */


template<typename T, template<typename U> class Lattice, class Momenta>
Smagorinsky2BGKdynamics<T,Lattice,Momenta>::Smagorinsky2BGKdynamics (
  T omega_, Momenta& momenta_, T smagoConst_, T dx_, T dt_ )
  : BGKdynamics<T,Lattice,Momenta>(omega_,momenta_),
    smagoConst(smagoConst_),
    preFactor(computePreFactor(omega_,smagoConst_, dx_, dt_) )
{ }

template<typename T, template<typename U> class Lattice, class Momenta>
void Smagorinsky2BGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d], pi[util::TensorVal<Lattice<T> >::n];
  this->_momenta.computeAllMomenta(cell, rho, u, pi);
  T newOmega = computeOmega(this->getOmega(), preFactor, rho, pi);
  T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell, rho, u, newOmega);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void Smagorinsky2BGKdynamics<T,Lattice,Momenta>::setOmega(T omega)
{
  this->BGKdynamics<T,Lattice,Momenta>::setOmega(omega);
  preFactor = computePreFactor(omega, smagoConst, dx, dt);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T Smagorinsky2BGKdynamics<T,Lattice,Momenta>::getSmagorinskyOmega(CellView<T,Lattice>& cell )
{
  T rho, uTemp[Lattice<T>::d], pi[util::TensorVal<Lattice<T> >::n];
  this->_momenta.computeAllMomenta(cell, rho, uTemp, pi);
  T newOmega = computeOmega(this->getOmega(), preFactor, rho, pi);
  return newOmega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
T Smagorinsky2BGKdynamics<T,Lattice,Momenta>::computePreFactor(T omega, T smagoConst, T dx, T dt)
{
  //return (T)(smagoConst*smagoConst*dx*dx)*Lattice<T>::invCs2()/dt*4*sqrt(2);
  return (T)0.5 * util::sqr(smagoConst*omega*Lattice<T>::invCs2());
}

template<typename T, template<typename U> class Lattice, class Momenta>
T Smagorinsky2BGKdynamics<T,Lattice,Momenta>::computeOmega(T omega0, T preFactor, T rho, T pi[util::TensorVal<Lattice<T> >::n] )
{


  T PiNeqNormSqr = pi[0]*pi[0] + 2.0*pi[1]*pi[1] + pi[2]*pi[2];
  if (util::TensorVal<Lattice<T> >::n == 6) {
    PiNeqNormSqr += pi[2]*pi[2] + pi[3]*pi[3] + 2*pi[4]*pi[4] +pi[5]*pi[5];
  }
  T PiNeqNorm    = sqrt(PiNeqNormSqr);
  T alpha        = preFactor/rho;
  T linearTerm   = alpha*PiNeqNorm;
  T squareTerm   = (T)2*alpha*alpha*PiNeqNormSqr;
  if ((1-linearTerm+squareTerm)>1) {
    return omega0;
  }
  return omega0*(1-linearTerm+squareTerm);
}


} //end namespace

#endif
