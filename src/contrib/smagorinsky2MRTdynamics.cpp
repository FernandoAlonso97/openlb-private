/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause, Jonas Latt, Patrick Nathen
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. MRT) with which a CellView object
 * can be instantiated -- template instantiation.
 */

#include "smagorinsky2MRTdynamics.h"
#include "smagorinsky2MRTdynamics.hh"
#include "dynamics/mrtDynamics.h"
#include "dynamics/mrtDynamics.hh"
#include "dynamics/mrtLatticeDescriptors.h"
#include "dynamics/mrtLatticeDescriptors.hh"

namespace olb {

template class Smagorinsky2MRTdynamics<double, descriptors::MRTD3Q19Descriptor>;
}
