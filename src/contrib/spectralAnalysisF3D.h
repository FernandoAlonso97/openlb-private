/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef SPECTRAL_ANALYSIS_F_3D_H
#define SPECTRAL_ANALYSIS_F_3D_H

#include<vector>    // for generic i/o
#include<cmath>     // for lpnorm
#include<string>    // for lpnorm

#include "functors/genericF.h"
//#include "functors/superLatticeBaseF3D.h"
//#include "functors/superLatticeLocalF3D.h"
#include "functors/analytical/interpolationF3D.h"
#include "core/superLattice3D.h"
#include "io/ostreamManager.h"
#include <fftw3.h>
//#include "dynamics/lbHelpers.h"  // for computation of lattice rho and velocity
//#include "io/stlReader.h"
//#include "utilities/vectorHelpers.h"

/** Note: Throughout the whole source code directory genericFunctions, the
 *  template parameters for i/o dimensions are:
 *           F: S^m -> T^n  (S=source, T=target)
 */

namespace olb {


template <typename T, template <typename U> class DESCRIPTOR>
class spectralAnalysisF3D : public GenericF<T,int> {
protected:

  CuboidGeometry3D<T>&                          _cuboidGeometry;
  LoadBalancer<T>&                              _loadBalancer;

  T                                             _latticeL;

  std::vector<T>                                _physOrigin;
  std::vector<T>                                _physExtend;

  std::vector<T>                                _physPoint;
  std::vector<int>                              _latticePoint;

  std::vector<std::vector<int> >                _cells_on_cpu;
  std::vector<std::vector<T> >                  _rawdata;

  int                                           _token;
  int                                           _cell;

  int                                           _dim_output;

  bool                                          _parallel;

  fftw_complex***                               _result_complex;
  fftw_plan *                                   _plan_forward;
  T **                                          _input_real;

  int                                           _no_cells;
  std::vector<T>                                _temp_recieved_vec;

  std::vector<std::vector<T> >                  _amplitudes;
  std::vector<std::vector<T> >                  _frequencies;
  std::vector<std::vector<T> >                  _phases;

  bool                                          _firstwrite;

  const std::string       _filename;



public:
  spectralAnalysisF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, bool parallel, int dim_output, const std::string filename);

  ~spectralAnalysisF3D();

  void print(int timestep);

  void reInit();

  void communicate_to_main();

  void transform();

  bool operator() (T output[], const int input[]);
  //std::vector<T> operator() (std::vector<int> latticePosition);

};

template <typename T, template <typename U> class DESCRIPTOR>
class spectralAnalysisOfFunktorF3D : public spectralAnalysisF3D<T,DESCRIPTOR> {
protected:

  SuperLatticePhysF3D<T, DESCRIPTOR>&      _f;


public:

  spectralAnalysisOfFunktorF3D(SuperLatticePhysF3D<T, DESCRIPTOR>& f,SuperLattice3D<T, DESCRIPTOR>& sLattice,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, bool parallel, const std::string filename);


  void getRawData();

};

template <typename T, template <typename U> class DESCRIPTOR>
class spectralAnalysisOfDataF3D : public spectralAnalysisF3D<T,DESCRIPTOR> {
protected:


public:

  spectralAnalysisOfDataF3D( SuperLattice3D<T, DESCRIPTOR>& sLattice ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, bool parallel, int dim_output, const std::string filename);

  void setRawData(std::vector<int>& latticePosition, std::vector<T>& inputValue);

};

} // end namespace olb

#endif
