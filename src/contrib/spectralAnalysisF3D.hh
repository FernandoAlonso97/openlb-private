/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink, Fabian Klemens
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef SPECTRAL_ANALYSIS_F_3D_HH
#define SPECTRAL_ANALYSIS_F_3D_HH

#include "contrib/spectralAnalysisF3D.h"
#include "functors/lattice/superLatticeIntegralF3D.h" // for IdentityF
#include "functors/lattice/blockLatticeIntegralF3D.h"
#include "utilities/vectorHelpers.h"
#include "io/ostreamManager.h"
#include <fftw3.h>
#include <algorithm> // for finding the value in a vector

namespace olb {

template <typename T, template <typename U> class DESCRIPTOR>
spectralAnalysisF3D<T,DESCRIPTOR>::spectralAnalysisF3D(
  SuperLattice3D<T, DESCRIPTOR>& sLattice ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, bool parallel, int dim_output, const std::string filename)
  :
  GenericF<T,int>(dim_output, dim_output),

  _cuboidGeometry(sLattice.getCuboidGeometry()),
  _loadBalancer(sLattice.getLoadBalancer()),
  _latticeL(converter.getLatticeL()),
  _physOrigin(physOrigin),
  _physExtend(physExtend),
  _physPoint(std::vector<T> (3,T())),
  _latticePoint(std::vector<int> (4,0)),

  _token(0),
  _cell(0),

  _dim_output(dim_output),
  _parallel(parallel),
  _temp_recieved_vec(std::vector<T> (_dim_output,T())),
  _firstwrite(true),
  _filename(filename)


{

  /// used to allocate memory
  reInit();

  this->getName() = "spectralAnalysisF";
}

template <typename T, template <typename U> class DESCRIPTOR>
spectralAnalysisF3D<T,DESCRIPTOR>::~spectralAnalysisF3D()
{

  if (_parallel) {

  } else {
    /// main cpu
    if (singleton::mpi().isMainProcessor()) {

      for (int i=0; i<_dim_output; i++) {

        delete [] _input_real[i];
        fftw_destroy_plan (_plan_forward[i]);
        fftw_free(_result_complex[i][0]);

      }
      for (int i=0; i<_dim_output; i++) {

        delete [] _result_complex[i];
        //fftw_free( _result_complex[i] );
      }
      delete [] _input_real;
      delete [] _result_complex;
      //fftw_free (_result_complex);
      delete [] _plan_forward;
      //fftw_destroy_plan(_plan_forward);

    }
    /// non main cpus
    else {

      //delete _input_real;
      //delete _result_complex;
      //delete _plan_forward;

    }
  }



}


template <typename T, template <typename U> class DESCRIPTOR>
bool spectralAnalysisF3D<T,DESCRIPTOR>::operator() (T output[], const int input[])
{
  /*
  //if (((latticePosition[1] > 0 ) && (latticePosition[2] > 0) && (latticePosition[3] > 0)) && ((latticePosition[1] < 33 ) && (latticePosition[2] < 33) && (latticePosition[3] < 32))){
  if ( _loadBalancer.rank(latticePosition[0]) == singleton::mpi().getRank() ) {

    _cell = (std::find(_cells_on_cpu.begin(), _cells_on_cpu.end(), latticePosition)) - _cells_on_cpu.begin();

    if ( _cell < _cells_on_cpu.size() ){
        //std::cout << "cell on cpu" << endl;
        return std::vector<T> (_ensembleAverageRST[_cell].begin() , _ensembleAverageRST[_cell].end());
    }
    else{
        //std::cout << "cell not on cpu" << endl;
    return std::vector<T>(6,0);
    }
  }
  else {
    return std::vector<T>();
  }
  //}
  //else
  */
  return true;
}

template <typename T, template <typename U> class DESCRIPTOR>
void spectralAnalysisF3D<T,DESCRIPTOR>::print(int timestep)
{


  if (singleton::mpi().isMainProcessor()) {
    ofstream ofile;
    stringstream fname;
    fname << _filename << ".dat" ;
    /// writing header and general data at first call
    if (_firstwrite) {

      ofile.open(fname.str().c_str());
      if ( ofile.is_open() ) {

        ofile << "<?xml version='1.0'?>" << endl;
        ofile << "\n" << endl;
        ofile << "<spectralanalysis>" << endl;
        ofile << "<header>" << endl;
        ofile << "title='Spectral amplitudes'" << endl;
        ofile << "parallel='" << _parallel << "'" << endl;
        //ofile << "No_usedEnsembles=" << _ensemblecount << endl;
        ofile << "</header>" << endl;
        ofile << "\n" << endl;

        _firstwrite = false;

        ofile.close();

      } else {
        std::cout << "ERROR OPENING FILE TO WRITE SPECTRAL DATA" << endl;
      }
    }

    /// writing data sequential in each timestep
    ofile.open(fname.str().c_str(), std::ios::app);
    ofile << "<timestep iT='" << timestep << "' >" << endl;
    for (int moment=0; moment<_dim_output; ++moment) {
      // get physical coordinates
      ofile << "<moment number='" << moment << "' >" << endl;

      for (int wavenumber=0; wavenumber<_no_cells/2; ++wavenumber) {
        //std::cout << "in spectral print" << endl;
        ofile << "<wavenumber k='" << wavenumber << "' RealCoefficient='" << _result_complex[moment][0][wavenumber][0] << "' ImaginaryCoefficient='" << _result_complex[moment][0][wavenumber][1] << "' amplitude='" << _amplitudes[moment][wavenumber] << "' />" << endl;
      }
      //std::cout << "nach spectral " << endl;
      ofile << "</moment>" << endl;
    }
    ofile << "</timestep>" << endl;
    ofile << "\n" << endl;

    ofile.close();

  }

}

template <typename T, template <typename U> class DESCRIPTOR>
void spectralAnalysisF3D<T,DESCRIPTOR>::reInit()
{
  //std::cout << "inside reInit()" << endl;
  _cells_on_cpu.clear();

  for (_physPoint[0] = _physOrigin[0]; _physPoint[0] <= _physExtend[0]; _physPoint[0] += _latticeL ) {

    for (_physPoint[1] = _physOrigin[1]; _physPoint[1] <= _physExtend[1]; _physPoint[1] += _latticeL ) {

      for (_physPoint[2] = _physOrigin[2]; _physPoint[2] <= _physExtend[2]; _physPoint[2] += _latticeL ) {

        int latticePoint[4];
        latticePoint[0]=_latticePoint[0];
        latticePoint[1]=_latticePoint[1];
        latticePoint[2]=_latticePoint[2];
        latticePoint[3]=_latticePoint[3];

        T physPoint[3];
        physPoint[0]=_physPoint[0];
        physPoint[1]=_physPoint[1];
        physPoint[2]=_physPoint[2];
        ///getting latticePoint and respecting latticecuboid
        _cuboidGeometry.getLatticeR(latticePoint,physPoint);
        _latticePoint[0]=latticePoint[0];
        _latticePoint[1]=latticePoint[1];
        _latticePoint[2]=latticePoint[2];
        _latticePoint[3]=latticePoint[3];

        ///testing if the latticecuboid in which the lattice cell is contained is on the acual cpu, if so the return of the functor is added to the sum
        if ( _loadBalancer.rank(_latticePoint[0]) == singleton::mpi().getRank() ) {

          _cells_on_cpu.push_back (_latticePoint);
          //std::cout << "in Constructor latticePoint: " << _latticePoint[0] << " " << _latticePoint[1] << " " << _latticePoint[2] << " " << _latticePoint[3] << " on Rank: " << singleton::mpi().getRank() << endl;


        }
      }
    }
  }

  _rawdata = std::vector<std::vector<T> > (_cells_on_cpu.size(),std::vector<T> (_dim_output,0));
  //std::cout << "values in _cells_on_cpu: " << _cells_on_cpu.size() << endl;

  if (_parallel) {

  } else {
    _no_cells = _cells_on_cpu.size();
    //std::cout << " no_cells " << _no_cells << endl;
    singleton::mpi().reduceAndBcast(_no_cells, MPI_SUM);
    //std::cout << " no_cells nach mpi " << _no_cells << endl;
    /// Doing FFT on just the main Processor
    if (singleton::mpi().isMainProcessor()) {
      /// Creating the Input, Result, Plan for all dimensions of the funktor f

      _input_real = new T * [_dim_output];
      //std::cout << "dim " << _f.getTargetDim() << endl;
      for (int k=0; k<_dim_output; ++k) {
        _input_real[k]=new T [_no_cells];
      }

      //_input_real = new T [_no_cells][dim];

      _result_complex = new fftw_complex** [_dim_output];

      for (int i=0; i<_dim_output; ++i) {
        _result_complex[i] = new fftw_complex* [1] ;
      }

      //std::cout << "bevore constr of plan" << endl;
      _plan_forward = new fftw_plan [_dim_output];
      //std::cout << "after constr of plan" << endl;

      for (int i=0; i<_dim_output; ++i) {
        //std::cout << "bevore constr of inner complex" << endl;
        _result_complex[i][0] =  (fftw_complex*) fftw_malloc( sizeof( fftw_complex ) * _no_cells);
        //std::cout << "after constr of inner complex" << endl;
        _plan_forward[i] = fftw_plan_dft_r2c_1d(_no_cells, _input_real[i] , _result_complex[i][0], FFTW_MEASURE );
        //std::cout << "after constr of inner plan" << endl;

      }

      _amplitudes =  std::vector<std::vector<T> > (_dim_output, std::vector<T> (_no_cells/2,T()));
      _frequencies = std::vector<std::vector<T> > (_dim_output, std::vector<T> (_no_cells/2,T()));
      _phases =  std::vector<std::vector<T> > (_dim_output, std::vector<T> (_no_cells/2,T()));
    }


  }

}

template <typename T, template <typename U> class DESCRIPTOR>
void spectralAnalysisF3D<T,DESCRIPTOR>::communicate_to_main()
{
  ///resetting token
  _token = 0;
  int index=0;
  //std::cout << "inside communicate to main" << endl;
  /// Main Processor will shepherd the others to get their values (by sending the token and the latticeCoordinate he needs -> Ordering mechanism)
  if (singleton::mpi().isMainProcessor()) {
    /// looping over needed physical Points and extracting the corresponding latticePoints
    for (_physPoint[0] = _physOrigin[0]; _physPoint[0] <= _physExtend[0]; _physPoint[0] += _latticeL ) {

      for (_physPoint[1] = _physOrigin[1]; _physPoint[1] <= _physExtend[1]; _physPoint[1] += _latticeL ) {

        for (_physPoint[2] = _physOrigin[2]; _physPoint[2] <= _physExtend[2]; _physPoint[2] += _latticeL ) {

          int latticePoint[4];
          latticePoint[0]=_latticePoint[0];
          latticePoint[1]=_latticePoint[1];
          latticePoint[2]=_latticePoint[2];
          latticePoint[3]=_latticePoint[3];

          T physPoint[3];
          physPoint[0]=_physPoint[0];
          physPoint[1]=_physPoint[1];
          physPoint[2]=_physPoint[2];
          ///getting latticePoint and respecting latticecuboid
          _cuboidGeometry.getLatticeR(latticePoint,physPoint);
          _latticePoint[0]=latticePoint[0];
          _latticePoint[1]=latticePoint[1];
          _latticePoint[2]=latticePoint[2];
          _latticePoint[3]=latticePoint[3];

          //std::cout << "main lattice point: " << _latticePoint[0] << " " << _latticePoint[1] << " " << _latticePoint[2] << " " << _latticePoint[3] << endl;
          /// getting rank which owns the lattice point
          _token = _loadBalancer.rank(_latticePoint[0]);//_latticePoint[0];//_loadBalancer.glob(_latticePoint[0]);
          //std::cout << "main token " << _token << endl;

          ///if cell is on this cpu
          if (std::find(_cells_on_cpu.begin(), _cells_on_cpu.end(), _latticePoint) != _cells_on_cpu.end()) {

            _cell = (std::find(_cells_on_cpu.begin(), _cells_on_cpu.end(), _latticePoint)) - _cells_on_cpu.begin();
            for (int i=0; i<_dim_output; ++i) {

              _input_real[i][index] = _rawdata[_cell][i];
              //std::cout << "_input_real[" << i << "][" << index << "]: " << _input_real[i][index] << endl;

            }
            ++index; // i dont like this solution !!!!
          }

          ///if cell is not on main cpu
          else {
            /// bcasting the _token we want to get data from
            singleton::mpi().bCast(& _token , 1 , singleton::mpi().bossId() );
            //std::cout << " not on main cpu" << endl;
            /// sending _latticePoint to the cpu which has it
            //std::cout << "main bevore sendrecieve of lattice point" << endl;
            singleton::mpi().send(& _latticePoint[0], _latticePoint.size() , _token);
            //singleton::mpi().sendRecv(& _latticePoint[0], & _latticePoint[0], _latticePoint.size() , _token, singleton::mpi().bossId() );
            //std::cout << "main after sendrecieve of lattice point" << endl;
            /// recieving value at _latticePoint from the cpu which holds it
            //std::cout << "master recieving from slave: " << singleton::mpi().bossId() << " " << _token << endl;
            ///
            //_cell = (std::find(_cells_on_cpu.begin(), _cells_on_cpu.end(), _latticePoint)) - _cells_on_cpu.begin();
            //std::cout << "slave funktor at point: " << _return_of_f[_cell][0] << " " << _return_of_f[_cell][1] << " " << _return_of_f[_cell][2] << endl;
            /// recieving value at _latticePoint from the cpu which holds it
            singleton::mpi().receive(& _temp_recieved_vec[0], _temp_recieved_vec.size(), _token);
            //singleton::mpi().sendRecv(& _return_of_f[_cell][0], & _temp_recieved_vec[0], _return_of_f[_cell].size() , singleton::mpi().bossId(), _token );
            //std::cout << "main funktor at point: " << _temp_recieved_vec[0] << " " << _temp_recieved_vec[1] << " " << _temp_recieved_vec[2] << endl;

            ///Writing revieved values to input for fft
            for (int i=0; i<_dim_output; ++i) {
              //std::cout << "main in write to input real i,iZ " << i << ","<< iZ <<  endl;
              _input_real[i][index] = _temp_recieved_vec[i]; // this is not the final version: iZ only correct for lines along Z
              //std::cout << "_input_real[" << i << "][" << index << "]: " << _input_real[i][index] << endl;

            }
            ++index;
          }

        }
      }
    }

    /// setting _token to -1 to end the communication

    _token = -1;
    singleton::mpi().bCast(& _token , 1 , singleton::mpi().bossId() , MPI_COMM_WORLD);
  }


  else {


    while (_token != -1) {
      /// recieve token from Root Processor
      singleton::mpi().bCast(& _token , 1 , singleton::mpi().bossId() , MPI_COMM_WORLD);

      if (_token == singleton::mpi().getRank()) {

        /// recieve latticePoint
        singleton::mpi().receive(& _latticePoint[0], _latticePoint.size(), singleton::mpi().bossId());
        //singleton::mpi().sendRecv(& _latticePoint[0], & _latticePoint[0], _latticePoint.size() , _token, singleton::mpi().bossId() );
        //std::cout << "slave lattice point: " << _latticePoint[0] << " " << _latticePoint[1] << " " << _latticePoint[2] << " " << _latticePoint[3] << endl;
        /// getting Index of _latticePoint inside _cells_on_cpu
        _cell = (std::find(_cells_on_cpu.begin(), _cells_on_cpu.end(), _latticePoint)) - _cells_on_cpu.begin();
        //std::cout << "slave cell: " << _cell;

        /// sending result at _latticePoint back
        //std::cout << "slave sending to master: " << singleton::mpi().bossId() << " " << _token << endl;
        //std::cout << "slave funktor at point: " << _rawdata[_cell][0] << " " << _rawdata[_cell][1] << " " << _rawdata[_cell][2] << endl;
        singleton::mpi().send(& _rawdata[_cell][0], _rawdata[_cell].size() , singleton::mpi().bossId());
        //singleton::mpi().sendRecv(& _return_of_f[_cell][0],&  _temp_recieved_vec[0], _return_of_f[_cell].size() , singleton::mpi().bossId(), _token );
      }

    }



  }

  //std::cout << "At end of comm to main " << singleton::mpi().getRank() << endl;
}

template <typename T, template <typename U> class DESCRIPTOR>
void spectralAnalysisF3D<T,DESCRIPTOR>::transform()
{
  if (_parallel) {

    //not yet done

  }
  ///non Parallel fft
  else {

    ///gather all data on main cpu
    //std::cout << "in transform() " << singleton::mpi().getRank() << endl;
    communicate_to_main();
    //std::cout << " nach 2 tem comm to main " << singleton::mpi().getRank() << endl;
    /// let main execute the fft plans
    if (singleton::mpi().isMainProcessor()) {

      for (int i=0; i<_dim_output; ++i) {
        //std::cout << "executing plan i:" << i << endl;
        fftw_execute( _plan_forward[i] );
      }

      /// do something with the transformed values;
      for (int i=0; i<_dim_output; ++i) {
        //T sum=0;
        for (int j=0; j<_no_cells/2; ++j) {

          if (j==0 || j==_no_cells/2 - 1) {

            _amplitudes[i][j] = sqrt(_result_complex[i][0][j][0]*_result_complex[i][0][j][0] + _result_complex[i][0][j][1]*_result_complex[i][0][j][1]) / (_no_cells) ;

          } else {

            _amplitudes[i][j] = 2 * sqrt(_result_complex[i][0][j][0]*_result_complex[i][0][j][0] + _result_complex[i][0][j][1]*_result_complex[i][0][j][1]) / (_no_cells) ;

          }
//          std::cout << "amplitude[" << i << "][" << j << "]: " <<  _amplitudes[i][j] << endl;
//          std::cout << "amplitude[" << i << "][" << j << "]: " <<  _amplitudes[i][j] << " realteil[" << i << "][" << j << "]: " << _result_complex[i][0][j][0] << " imaginärteil[" << i << "][" << j << "]: " << _result_complex[i][0][j][1] << endl;
//          std::cout << "realteil[" << i << "][" << j << "]: " << _result_complex[i][0][j][0] << endl;
//          std::cout << "imaginärteil[" << i << "][" << j << "]: " << _result_complex[i][0][j][1] << endl;
//          T sum = 0.;
//          sum = sum + _amplitudes[i][j];
        }
        //std::cout << "######################################################################" << endl;
//        std::cout << " summe amplitude für i=" << i << " : " << sum << endl;


      }

//      std::cout << "##############################################"<< endl;
//      for (int i=0; i< _no_cells/2; ++i){
//
//          std::cout << "realteil[" << 1 << "][" << i << "]: " << _result_complex[1][0][i][0] << " imaginärteil[" << 1 << "][" << i << "]: " << _result_complex[1][0][i][1] << endl;
//
//      }
//      for (int i=0; i< _no_cells; ++i){
//
//         std::cout << "_input_real[" << 1 << "][" << i << "]: " << _input_real[1][i] << endl;
//
//      }
//      std::cout << "############# vor fft ######################" << endl;
//
//      _plan_forward[1] = fftw_plan_dft_c2r_1d(_no_cells,  _result_complex[1][0], _input_real[1] , FFTW_MEASURE );
//      for (int i=0; i< _no_cells/2; ++i){
//
//          std::cout << "realteil[" << 1 << "][" << i << "]: " << _result_complex[1][0][i][0] << " imaginärteil[" << 1 << "][" << i << "]: " << _result_complex[1][0][i][1] << endl;
//
//      }
//      for (int i=0; i< _no_cells; ++i){
//
//         std::cout << "_input_real[" << 1 << "][" << i << "]: " << _input_real[1][i] << endl;
//
//      }
//      std::cout << "################## nach plan ##################" << endl;
//      fftw_execute( _plan_forward[1] );
//
//      for (int i=0; i< _no_cells; ++i){
//
//         std::cout << "_input_real[" << 1 << "][" << i << "]: " << _input_real[1][i] << endl;
//
//      }
//      for (int i=0; i< _no_cells/2; ++i){
//
//          std::cout << "realteil[" << 1 << "][" << i << "]: " << _result_complex[1][0][i][0] << " imaginärteil[" << 1 << "][" << i << "]: " << _result_complex[1][0][i][1] << endl;
//
//      }
//
//      std::cout << "############# nach fft ######################" << endl;


    }

  }

}


template <typename T, template <typename U> class DESCRIPTOR>
spectralAnalysisOfFunktorF3D<T,DESCRIPTOR>::spectralAnalysisOfFunktorF3D(
  SuperLatticePhysF3D<T, DESCRIPTOR>& f,SuperLattice3D<T, DESCRIPTOR>& sLattice ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, bool parallel, const std::string filename)
  :
  spectralAnalysisF3D<T,DESCRIPTOR>(sLattice, converter, physOrigin, physExtend, parallel, f.getTargetDim(), filename ),
  _f(f)

{
  this->getName() = "spectralAnalysisOfFunktorF";
}

template <typename T, template <typename U> class DESCRIPTOR>
void spectralAnalysisOfFunktorF3D<T,DESCRIPTOR>::getRawData()
{

  for (int i=0; i<this->_cells_on_cpu.size(); ++i) {
     int input[this->_cells_on_cpu[i].size()];
     for (int j=0; j<this->_cells_on_cpu[i].size(); ++j) {
       input[j]=this->_cells_on_cpu[i][j];
     }
     T output[this->_rawdata[i].size()];
     for (int j=0; j<this->_rawdata[i].size(); ++j) {

       output[j]=this->_rawdata[i][j];
     }
     _f(output,input);
     for (int j=0; j<this->_rawdata[i].size(); ++j) {

       this->_rawdata[i][j]=output[j];
     }

    //std::cout << "point " << this->_cells_on_cpu[i][0] << " " << this->_cells_on_cpu[i][1] << " " << this->_cells_on_cpu[i][2] << " " << this->_cells_on_cpu[i][3] << " value of funktor " << this->_rawdata[i][0] << endl;

  }

}

template <typename T, template <typename U> class DESCRIPTOR>
spectralAnalysisOfDataF3D<T,DESCRIPTOR>::spectralAnalysisOfDataF3D(
  SuperLattice3D<T, DESCRIPTOR>& sLattice ,LBconverter<T>& converter, std::vector<T> physOrigin, std::vector<T> physExtend, bool parallel, int dim_output, const std::string filename)
  :
  spectralAnalysisF3D<T,DESCRIPTOR>(sLattice, converter, physOrigin, physExtend, parallel, dim_output, filename)
{
  this->_name = "spectralAnalysisOfDataF";
}

template <typename T, template <typename U> class DESCRIPTOR>
void spectralAnalysisOfDataF3D<T,DESCRIPTOR>::setRawData(std::vector<int>& latticePosition, std::vector<T>& inputValue)
{

  ///check if lattice point is inside evaluation area and on this cpu
  if (std::find(this->_cells_on_cpu.begin(), this->_cells_on_cpu.end(), latticePosition) != this->_cells_on_cpu.end()) {

    //std::cout << "cell on cpu " << singleton::mpi().getRank() << endl;

    this->_rawdata[std::find(this->_cells_on_cpu.begin(), this->_cells_on_cpu.end(), latticePosition) - this->_cells_on_cpu.begin()] = inputValue;
    /*
    for (int i=0; i<this->_dim_output;++i){

        std::cout << "_rawdata at [" << std::find(this->_cells_on_cpu.begin(), this->_cells_on_cpu.end(), latticePosition) - this->_cells_on_cpu.begin() << "] : " << this->_rawdata[std::find(this->_cells_on_cpu.begin(), this->_cells_on_cpu.end(), latticePosition) - this->_cells_on_cpu.begin()][i] << endl;
    }*/

  } /*
    else {

        std::cout << "cell not on cpu" << endl;
    }*/

}



} // end namespace olb

#endif
