/*  Copyright (C) 2015 Sebastian Schmidt, Patrick Nathen
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#ifndef SUPER_WALL_FUNCTION_F_3D_H
#define SUPER_WALL_FUNCTION_F_3D_H

#include <vector>
#include "functors/lattice/indicator/indicatorBaseF3D.h"
#include "core/blockLatticeStructure3D.h"
#include "functors/lattice/superLatticeLocalF3D.h"
#include "blockWallFunctionF3D.hh"

namespace olb {


/// Functor that computes the Populations for boundary cells
/// Wall model for large-eddy simulation based on the lattice Boltzmann method
template <typename T, template <typename U> class DESCRIPTOR>
class SuperWallFunctionF3D : public SuperLatticeF3D<T, DESCRIPTOR> {
private:
  SuperGeometry3D<T>& _sGeometry;
  IndicatorF3D<T>& _wallShape;
  const LBconverter<T>& _converter;
  UWallProfile profile_type;
  fNeqCalcType calc_type;
  T rel_interval_size;
  T abs_interval_size;

public:
  SuperWallFunctionF3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                       SuperGeometry3D<T>& sGeometry,
                       IndicatorF3D<T>& wallShape,
                       LBconverter<T> const& converter,
                       UWallProfile pr_type,
                       fNeqCalcType c_type=fNeqCalcType::GRADU,
                       T rel_search_size=0.8, T abs_search_size=0.25)
    : SuperLatticeF3D<T, DESCRIPTOR>(sLattice, DESCRIPTOR<T>::q),
      _sGeometry(sGeometry),
      _wallShape(wallShape),
      _converter(converter),
      profile_type(pr_type),
      calc_type(c_type),
      rel_interval_size(rel_search_size),
      abs_interval_size(abs_search_size)
  {
    this->getName() = "SuperWallFunctionF3D";
  }

  bool operator() (T output[], const int input[])
  {
    int globIC = input[0];
    if ( this->_sLattice.getLoadBalancer().rank(globIC) == singleton::mpi().getRank() ) {
      int inputLocal[3];
      inputLocal[0] = input[1];
      inputLocal[1] = input[2];
      inputLocal[2] = input[3];

      BlockWallFunctionF3D<T, DESCRIPTOR> blockLatticeF(this->_sLattice,
          this->_sLattice.getBlockLattice(this->_sLattice.getLoadBalancer().loc(globIC)),
          this->_sGeometry.getBlockGeometry(this->_sLattice.getLoadBalancer().loc(globIC)),
          this->_wallShape,
          this->_converter,
          profile_type, calc_type,
          rel_interval_size, abs_interval_size);
      return blockLatticeF(output,inputLocal);
    }
    return false;
  }
};

} // end namespace olb

#endif
