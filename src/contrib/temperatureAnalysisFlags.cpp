/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef TEMPERATURE_ANALYSIS_FLAGS_CPP
#define TEMPERATURE_ANALYSIS_FLAGS_CPP

#include "temperatureAnalysisFlags.h"

namespace olb {

/////// Struct TemperatureAnalysisFlags3D  ///////////////////////

void TemperatureAnalysisFlags3D::reset()
{
  temperatureFieldComputed = false;
  populationFieldComputed = false;
}

/////// Struct AnalysisFlags2D  ///////////////////////

void TemperatureAnalysisFlags2D::reset()
{
  temperatureFieldComputed = false;
  populationFieldComputed = false;
}


}

#endif

