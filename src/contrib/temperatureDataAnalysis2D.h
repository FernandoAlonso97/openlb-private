/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Data analysis (formerly known as BlockStatistics) on 2D BlockLatticeStructures -- header file.
 */

#ifndef TEMPERATURE_DATA_ANALYSIS_2D_H
#define TEMPERATURE_DATA_ANALYSIS_2D_H

#include "temperatureDataAnalysisBase2D.h"
#include "core/blockLattice2D.h"
#include "core/dataFields2D.h"
#include "temperatureAnalysisFlags.h"

namespace olb {

template<typename T, template<typename U> class Lattice>
struct TemperatureAnalysisFieldsImpl2D {
  TemperatureAnalysisFieldsImpl2D(int nx, int ny);
  ScalarField2D<T>   temperatureField;
  TensorField2D<T,Lattice<T>::q > populationField;
};

template<typename T, template<typename U> class Lattice>
struct TemperatureAnalysisFields2D {
  TemperatureAnalysisFields2D (
    ScalarField2D<T>&   temperatureField_,
    TensorField2D<T,Lattice<T>::q >& populationField_);

  TemperatureAnalysisFields2D(TemperatureAnalysisFieldsImpl2D<T,Lattice>& impl);

  ScalarField2D<T>&   temperatureField;
  TensorField2D<T,Lattice<T>::q >& populationField;
};

/// Data analysis on serial block lattices, using serial data fields
template<typename T, template<typename U> class Lattice>
class TemperatureDataAnalysis2D : public TemperatureDataAnalysisBase2D<T,Lattice> {
public:
  TemperatureDataAnalysis2D(BlockLatticeStructure2D<T,Lattice> const& block_);
  TemperatureDataAnalysis2D(BlockLatticeStructure2D<T,Lattice> const& block_, TemperatureAnalysisFields2D<T,Lattice>& fields_ );
  ~TemperatureDataAnalysis2D();
public:
  virtual void reset() const;

  virtual ScalarFieldBase2D<T> const& getTemperature() const;
  virtual TensorFieldBase2D<T,Lattice<T>::q > const& getPopulations() const;

  virtual T computeNusseltNumber(
    ScalarFieldBase2D<T> const &u, T deltaX, T deltaTemp, T kappa) const;

  virtual int getNx() const
  {
    return block.getNx();
  }
  virtual int getNy() const
  {
    return block.getNy();
  }
private:
  void computeTemperatureField() const;
  void computePopulationField() const;

private:
  BlockLatticeStructure2D<T,Lattice> const& block;
  mutable TemperatureAnalysisFieldsImpl2D<T,Lattice> defaultFields;
  mutable TemperatureAnalysisFields2D<T,Lattice>     fields;
  mutable TemperatureAnalysisFlags2D                 flags;
};

}  // namespace olb;

#endif
