/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Data analysis (formerly known as BlockStatistics) on 2D BlockLatticeStructures -- generic implementation.
 */

#ifndef TEMPERATURE_DATA_ANALYSIS_2D_HH
#define TEMPERATURE_DATA_ANALYSIS_2D_HH

#include <cmath>
#include "temperatureDataAnalysis2D.h"
#include "core/cell.h"
#include "core/util.h"

namespace olb {

/////// Struct TemperatureAnalysisFieldsImpl2D  ///////////////////////

template<typename T, template<typename U> class Lattice>
TemperatureAnalysisFieldsImpl2D<T,Lattice>::TemperatureAnalysisFieldsImpl2D(int nx, int ny)
  : temperatureField(nx, ny), populationField(nx,ny)
{ }


/////// Struct TemperatureAnalysisFields2D  ///////////////////////

template<typename T, template<typename U> class Lattice>
TemperatureAnalysisFields2D<T,Lattice>::TemperatureAnalysisFields2D (
  ScalarField2D<T>&   temperatureField_ ,
  TensorField2D<T,Lattice<T>::q >& populationField_)
  :   temperatureField(temperatureField_), populationField(populationField_)
{ }

template<typename T, template<typename U> class Lattice>
TemperatureAnalysisFields2D<T,Lattice>::TemperatureAnalysisFields2D(TemperatureAnalysisFieldsImpl2D<T,Lattice>& impl)
  : temperatureField(impl.temperatureField), populationField(impl.populationField)
{ }

/////// Class TemperatureDataAnalysis2D  /////////////////////////////

template<typename T, template<typename U> class Lattice>
TemperatureDataAnalysis2D<T,Lattice>::TemperatureDataAnalysis2D(BlockLatticeStructure2D<T,Lattice> const& block_)
  : block(block_),
    defaultFields(block.getNx(), block.getNy()),
    fields(defaultFields)
{
  flags.reset();
}

template<typename T, template<typename U> class Lattice>
TemperatureDataAnalysis2D<T,Lattice>::TemperatureDataAnalysis2D(BlockLatticeStructure2D<T,Lattice> const& block_,
    TemperatureAnalysisFields2D<T,Lattice>& fields_ )
  : block(block_),
    defaultFields(block.getNx(), block.getNy()),
    fields(fields_ )
{
  flags.reset();
}

template<typename T, template<typename U> class Lattice>
TemperatureDataAnalysis2D<T,Lattice>::~TemperatureDataAnalysis2D() { }

template<typename T, template<typename U> class Lattice>
void TemperatureDataAnalysis2D<T,Lattice>::reset() const
{
  flags.reset();
}

template<typename T, template<typename U> class Lattice>
ScalarFieldBase2D<T> const&
TemperatureDataAnalysis2D<T,Lattice>::getTemperature() const
{
  computeTemperatureField();
  return fields.temperatureField;
}

template<typename T, template<typename U> class Lattice>
TensorFieldBase2D<T,Lattice<T>::q > const&
TemperatureDataAnalysis2D<T,Lattice>::getPopulations() const
{
  computePopulationField();
  return fields.populationField;
}

template<typename T, template<typename U> class Lattice>
T TemperatureDataAnalysis2D<T,Lattice>::computeNusseltNumber(
  ScalarFieldBase2D<T> const &u, T deltaX, T deltaTemp, T kappa) const
{
  int nx = getNx();
  int ny = getNy();

  computeTemperatureField();
  T u_T = T();
  for (int iX = 0; iX < nx; ++iX) {
    for (int iY = 0; iY < ny; ++iY) {
      u_T += u.get(iX,iY) * fields.temperatureField.get(iX,iY);
    }
  }

  return (T)1 + u_T*deltaX / (kappa*deltaTemp);
}

template<typename T, template<typename U> class Lattice>
void TemperatureDataAnalysis2D<T,Lattice>::computeTemperatureField() const
{
  if (flags.temperatureFieldComputed) {
    return;
  }
  fields.temperatureField.construct();
  for (int iX=0; iX<fields.temperatureField.getNx(); ++iX) {
    for (int iY=0; iY<fields.temperatureField.getNy(); ++iY) {
      fields.temperatureField.get(iX,iY) = block.get(iX,iY).computeRho();
    }
  }
  flags.temperatureFieldComputed = true;
}

template<typename T, template<typename U> class Lattice>
void TemperatureDataAnalysis2D<T,Lattice>::computePopulationField() const
{
  if (flags.populationFieldComputed) {
    return;
  }
  fields.populationField.construct();

  int nx = fields.populationField.getNx();
  int ny = fields.populationField.getNy();
  for (int iX=0; iX<nx; ++iX) {
    for (int iY=0; iY<ny; ++iY) {
      for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
        fields.populationField.get(iX,iY)[iPop] = block.get(iX,iY)[iPop];
      }
    }
  }

  flags.populationFieldComputed = true;
}

}  // namespace olb


#endif
