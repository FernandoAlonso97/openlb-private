/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Data analysis (formerly known as BlockStatistics) on 3D BlockLatticeStructures -- header file.
 */

#ifndef TEMPERATURE_DATA_ANALYSIS_3D_H
#define TEMPERATURE_DATA_ANALYSIS_3D_H

#include "temperatureDataAnalysisBase3D.h"
#include "core/blockLattice3D.h"
#include "core/dataFields3D.h"
#include "temperatureAnalysisFlags.h"
#include "advectionDiffusionLatticeDescriptors.h"

namespace olb {

template<typename T, template<typename U> class Lattice>
struct TemperatureAnalysisFieldsImpl3D {
  TemperatureAnalysisFieldsImpl3D(int nx, int ny, int nz);
  ScalarField3D<T>   temperatureField;
  TensorField3D<T,Lattice<T>::q > populationField;
};

template<typename T, template<typename U> class Lattice>
struct TemperatureAnalysisFields3D {
  TemperatureAnalysisFields3D (
    ScalarField3D<T>&   temperatureField_,
    TensorField3D<T,Lattice<T>::q >& populationField_ );

  TemperatureAnalysisFields3D(TemperatureAnalysisFieldsImpl3D<T,Lattice>& impl);

  ScalarField3D<T>&   temperatureField;
  TensorField3D<T,Lattice<T>::q >& populationField;
};

/// Data analysis on serial block lattices, using serial data fields
template<typename T, template<typename U> class Lattice>
class TemperatureDataAnalysis3D : public TemperatureDataAnalysisBase3D<T,Lattice> {
public:
  TemperatureDataAnalysis3D(BlockLatticeStructure3D<T,Lattice> const& block_);
  TemperatureDataAnalysis3D(BlockLatticeStructure3D<T,Lattice> const& block_, TemperatureAnalysisFields3D<T,Lattice>& fields_ );
  ~TemperatureDataAnalysis3D();
public:
  virtual void reset() const;

  virtual ScalarFieldBase3D<T> const& getTemperature() const;
  virtual TensorFieldBase3D<T,Lattice<T>::q > const& getPopulations() const;

  virtual T computeNusseltNumber(ScalarField3D<T> const &u, T deltaX, T deltaTemp, T kappa) const;

  virtual int getNx() const
  {
    return block.getNx();
  }
  virtual int getNy() const
  {
    return block.getNy();
  }
  virtual int getNz() const
  {
    return block.getNz();
  }
private:
  void computeTemperatureField() const;
  void computePopulations() const;
private:
  BlockLatticeStructure3D<T,Lattice> const& block;
  mutable TemperatureAnalysisFieldsImpl3D<T,Lattice> defaultFields;
  mutable TemperatureAnalysisFields3D<T,Lattice>     fields;
  mutable TemperatureAnalysisFlags3D                 flags;
};


}  // namespace olb;

#endif
