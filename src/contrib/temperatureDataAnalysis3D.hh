/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Data analysis (formerly known as BlockStatistics) on 3D BlockLatticeStructures -- generic implementation.
 */

#ifndef TEMPERATURE_DATA_ANALYSIS_3D_HH
#define TEMPERATURE_DATA_ANALYSIS_3D_HH

#include <cmath>
#include "temperatureDataAnalysis3D.h"
#include "core/cell.h"
#include "core/util.h"

namespace olb {

/////// Struct TemperatureAnalysisFieldsImpl3D  ///////////////////////

template<typename T, template<typename U> class Lattice>
TemperatureAnalysisFieldsImpl3D<T,Lattice>::TemperatureAnalysisFieldsImpl3D(int nx, int ny, int nz)
  : temperatureField(nx, ny, nz),
    populationField(nx, ny, nz)
{ }


/////// Struct TemperatureAnalysisFields3D  ///////////////////////

template<typename T, template<typename U> class Lattice>
TemperatureAnalysisFields3D<T,Lattice>::TemperatureAnalysisFields3D (
  ScalarField3D<T>&   temperatureField_,
  TensorField3D<T,Lattice<T>::q >& populationField_ )
  :
  temperatureField(temperatureField_),
  populationField(populationField_)
{ }

template<typename T, template<typename U> class Lattice>
TemperatureAnalysisFields3D<T,Lattice>::TemperatureAnalysisFields3D(TemperatureAnalysisFieldsImpl3D<T,Lattice>& impl)
  :
  temperatureField(impl.temperatureField),
  populationField(impl.populationField)
{ }


/////// Class TemperatureDataAnalysis3D  /////////////////////////////

template<typename T, template<typename U> class Lattice>
TemperatureDataAnalysis3D<T,Lattice>::TemperatureDataAnalysis3D (
  BlockLatticeStructure3D<T,Lattice> const& block_ )
  : block(block_),
    defaultFields(block.getNx(), block.getNy(), block.getNz()),
    fields(defaultFields)
{
  flags.reset();
}

template<typename T, template<typename U> class Lattice>
TemperatureDataAnalysis3D<T,Lattice>::TemperatureDataAnalysis3D(BlockLatticeStructure3D<T,Lattice> const& block_,
    TemperatureAnalysisFields3D<T,Lattice>& fields_ )
  : block(block_),
    defaultFields(block.getNx(), block.getNy(), block.getNz()),
    fields(fields_ )
{
  flags.reset();
}


template<typename T, template<typename U> class Lattice>
TemperatureDataAnalysis3D<T,Lattice>::~TemperatureDataAnalysis3D() { }

template<typename T, template<typename U> class Lattice>
void TemperatureDataAnalysis3D<T,Lattice>::reset() const
{
  flags.reset();
}

template<typename T, template<typename U> class Lattice>
ScalarFieldBase3D<T> const&
TemperatureDataAnalysis3D<T,Lattice>::getTemperature() const
{
  computeTemperatureField();
  return fields.temperatureField;
}

template<typename T, template<typename U> class Lattice>
TensorFieldBase3D<T, Lattice<T>::q > const&
TemperatureDataAnalysis3D<T,Lattice>::getPopulations() const
{
  computePopulations();
  return fields.populationField;
}


template<typename T, template<typename U> class Lattice>
void TemperatureDataAnalysis3D<T,Lattice>::computeTemperatureField() const
{
  if (flags.temperatureFieldComputed) {
    return;
  }
  fields.temperatureField.construct();
  for (int iX=0; iX<fields.temperatureField.getNx(); ++iX) {
    for (int iY=0; iY<fields.temperatureField.getNy(); ++iY) {
      for (int iZ=0; iZ<fields.temperatureField.getNz(); ++iZ) {
        fields.temperatureField.get(iX,iY,iZ) =
          block.get(iX,iY,iZ).computeRho();
      }
    }
  }
  flags.temperatureFieldComputed = true;
}

template<typename T, template<typename U> class Lattice>
void TemperatureDataAnalysis3D<T,Lattice>::computePopulations() const
{
  if (flags.populationFieldComputed) {
    return;
  }
  fields.populationField.construct();

  int nx = fields.populationField.getNx();
  int ny = fields.populationField.getNy();
  int nz = fields.populationField.getNz();

  for (int iX=0; iX<nx; ++iX) {
    for (int iY=0; iY<ny; ++iY) {
      for (int iZ=0; iZ<nz; ++iZ) {
        for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
          fields.populationField.get(iX,iY,iZ)[iPop] = block.get(iX,iY,iZ)[iPop];
        }
      }
    }
  }

  flags.populationFieldComputed = true;
}

template<typename T, template<typename U> class Lattice>
T TemperatureDataAnalysis3D<T,Lattice>::
computeNusseltNumber(ScalarField3D<T> const &u, T deltaX, T deltaTemp, T kappa) const
{

  T u_T = T();
  computeTemperatureField();
  for (int iX=0; iX<fields.temperatureField.getNx(); ++iX) {
    for (int iY=0; iY<fields.temperatureField.getNy(); ++iY) {
      for (int iZ=0; iZ<fields.temperatureField.getNz(); ++iZ) {
        u_T += u.get(iX,iY,iZ) * fields.temperatureField.get(iX,iY,iZ);
      }
    }
  }
  return (T)1 + u_T*deltaX / (kappa*deltaTemp);
}

}  // namespace olb


#endif
