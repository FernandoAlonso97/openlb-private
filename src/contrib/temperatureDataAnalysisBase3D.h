/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Base class for data analysis on 3D BlockLatticeStructures -- header file.
 */

#ifndef TEMPERATURE_DATA_ANALYSIS_BASE_3D_H
#define TEMPERATURE_DATA_ANALYSIS_BASE_3D_H

#include "core/dataFieldBase3D.h"
#include "advectionDiffusionLatticeDescriptors.h"

namespace olb {

/// Interface for the variants of 3D data analysis classes.
template<typename T, template<typename U> class Lattice>
class TemperatureDataAnalysisBase3D {
public:
  virtual ~TemperatureDataAnalysisBase3D() { }
public:
  virtual void reset() const =0;

  virtual ScalarFieldBase3D<T> const& getTemperature() const =0;
  virtual TensorFieldBase3D<T,Lattice<T>::q > const& getPopulations() const =0;

  virtual T computeNusseltNumber(ScalarField3D<T> const &u, T deltaX, T deltaTemp, T kappa) const =0;

  virtual int getNx() const =0;
  virtual int getNy() const =0;
  virtual int getNz() const =0;
};


}  // namespace olb;

#endif
