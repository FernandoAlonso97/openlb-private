/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Helper functions for the implementation of LB dynamics. This file is all
 * about efficiency. The generic template code is specialized for commonly
 * used Lattices, so that a maximum performance can be taken out of each
 * case.
 */
#ifndef TENSOR_LB_HELPERS_H
#define TENSOR_LB_HELPERS_H

#include "core/latticeDescriptors.h"
#include "core/cell.h"
#include "core/util.h"


namespace olb {


// Forward declarations
template<typename T, template<typename U> class Lattice> struct tensorLbExternalHelpers;

/// This structure forwards the calls to the appropriate helper class
template<typename T, template<typename U> class Lattice>
struct tensorLbHelpers {

  static void addExternalTensor(CellView<T,Lattice>& cell, T omega)
  {
    tensorLbExternalHelpers<T,Lattice>::addExternalTensor(cell, omega);
  }

};  // struct tensorLbHelpers

/// Helper functions for dynamics that access external field
template<typename T, template<typename U> class Lattice>
struct tensorLbExternalHelpers {
  /// Add a force term after BGK collision
  static void addExternalTensor(CellView<T,Lattice>& cell, T omega)
  {
    typedef Lattice<T> L;

    T *tau = cell.getExternal(L::ExternalField::tensorBeginsAt);

    for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
      int iTau = 0;
      T Qtau = T();
      for (int iA = 0; iA < Lattice<T>::d; ++iA) {
        for (int iB = iA; iB < Lattice<T>::d; ++iB) {
          if (iA == iB) {
            Qtau += L::c(iPop)[iA]*L::c(iPop)[iB] * tau[iTau];
            Qtau -= tau[iTau]/L::invCs2;
          } else {
            Qtau += (T)2*L::c(iPop)[iA]*L::c(iPop)[iB] * tau[iTau];
          }

          ++iTau;
        }
      }
      cell[iPop] -= L::t[iPop] * omega * Qtau * L::invCs2 * L::invCs2 /(T)2;
    }
  }
};  // struct externalFieldHelpers


}  // namespace olb

// The specialized code is directly included. That is because we never want
// it to be precompiled so that in both the precompiled and the
// "include-everything" version, the compiler can apply all the
// optimizations it wants.
#include "tensorLbHelpers2D.h"
// #include "tensorLbHelpers3D.h"

#endif
