/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Template specializations for some computationally intensive LB
 * functions of the header file lbHelpers.h, for the D2Q9 grid.
 */

#ifndef TENSOR_LB_HELPERS_2D_H
#define TENSOR_LB_HELPERS_2D_H

namespace olb {

// Efficient specialization for D2Q9 lattice with force
template<typename T>
struct tensorLbExternalHelpers<T, descriptors::TensorD2Q9Descriptor> {

  static void addExternalTensor(
    CellView<T,descriptors::TensorD2Q9Descriptor>& cell, T omega)
  {
    using namespace util::tensorIndices2D;

    static const int tensorBeginsAt
      = descriptors::TensorD2Q9Descriptor<T>::ExternalField::tensorBeginsAt;
    T* tau = cell.getExternal(tensorBeginsAt);

    T trTau = tau[xx] + tau[yy];

    T mu0 = (T)2 * omega / (T)3;
    T mu1 = omega / (T)6;
    T mu2 = omega / (T)24;

    T ext0 = mu0 * (- trTau );
    T ext1 = mu2 * (- trTau + (T)3 * (trTau - (T)2*tau[xy]));
    T ext2 = mu1 * (- trTau + (T)3 * tau[xx]);
    T ext3 = mu2 * (- trTau + (T)3 * (trTau + (T)2*tau[xy]));
    T ext4 = mu1 * (- trTau + (T)3 * tau[yy]);

    cell[0] -= ext0;
    cell[1] -= ext1;
    cell[2] -= ext2;
    cell[3] -= ext3;
    cell[4] -= ext4;
    cell[5] -= ext1;
    cell[6] -= ext2;
    cell[7] -= ext3;
    cell[8] -= ext4;
  }
};

}  // namespace olb

#endif
