/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Template specializations for some computationally intensive LB
 * functions of the header file lbHelpers.h, for some D3Q19 grids.
 */

#ifndef TENSOR_LB_HELPERS_3D_H
#define TENSOR_LB_HELPERS_3D_H

namespace olb {

// Efficient specialization for D3Q19 lattice
template<typename T>
struct tensorLbExternalHelpers<T, descriptors::TensorD3Q19Descriptor> {

  static void addExternalTensor(
    CellView<T,descriptors::TensorD3Q19Descriptor>& cell, T omega)
  {
    using namespace util::tensorIndices3D;

    static const int tensorBeginsAt
      = descriptors::TensorD3Q19Descriptor<T>::ExternalField::tensorBeginsAt;
    T* tau = cell.getExternal(tensorBeginsAt);

    T trTau = tau[xx] + tau[yy] + tau[zz];
    T tau_xx_yy = tau[xx] + tau[yy];
    T tau_xx_zz = tau[xx] + tau[zz];
    T tau_yy_zz = tau[zz] + tau[yy];

    T mu0 = omega / (T)2;
    T mu1 = omega / (T)12;
    T mu2 = omega / (T)24;

    T ext0 = mu0 * (- trTau );

    T ext1 = mu1 * (- trTau + (T)3 * tau[xx]);
    T ext2 = mu1 * (- trTau + (T)3 * tau[yy]);
    T ext3 = mu1 * (- trTau + (T)3 * tau[zz]);

    T ext4 = mu2 * (- trTau + (T)3 * (tau_xx_yy + (T)2*tau[xy]));
    T ext5 = mu2 * (- trTau + (T)3 * (tau_xx_yy - (T)2*tau[xy]));
    T ext6 = mu2 * (- trTau + (T)3 * (tau_xx_zz + (T)2*tau[xz]));
    T ext7 = mu2 * (- trTau + (T)3 * (tau_xx_zz - (T)2*tau[xz]));
    T ext8 = mu2 * (- trTau + (T)3 * (tau_yy_zz + (T)2*tau[yz]));
    T ext9 = mu2 * (- trTau + (T)3 * (tau_yy_zz - (T)2*tau[yz]));

    cell[0] -= ext0;

    cell[1] -= ext1;
    cell[2] -= ext2;
    cell[3] -= ext3;
    cell[4] -= ext4;
    cell[5] -= ext5;
    cell[6] -= ext6;
    cell[7] -= ext7;
    cell[8] -= ext8;
    cell[9] -= ext9;

    cell[10] -= ext1;
    cell[11] -= ext2;
    cell[12] -= ext3;
    cell[13] -= ext4;
    cell[14] -= ext5;
    cell[15] -= ext6;
    cell[16] -= ext7;
    cell[17] -= ext8;
    cell[18] -= ext9;
  }
};


}  // namespace olb

#endif
