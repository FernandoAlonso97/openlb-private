/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Orestis Malaspinas, Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file A helper for initialising 3D boundaries -- header file.  */
#ifndef THERMAL_DYNAMICS_HELPERS_3D_H
#define THERMAL_DYNAMICS_HELPERS_3D_H

#include "blockLatticeStructure3D.h"
#include "boundaries3D.h"
#include<list>

namespace olb {

template<typename T, template<typename U> class Lattice>
class ThermalBoundaryCondition3D {
public:
  ThermalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& lattice_);

  virtual ~ThermalBoundaryCondition3D() { }

  virtual void addTemperatureBoundary0N(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega) =0;
  virtual void addTemperatureBoundary0P(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega) =0;
  virtual void addTemperatureBoundary1N(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega) =0;
  virtual void addTemperatureBoundary1P(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega) =0;
  virtual void addTemperatureBoundary2N(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega) =0;
  virtual void addTemperatureBoundary2P(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega) =0;

  //     virtual void addExternalTemperatureEdge0NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge0NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge0PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge0PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge1NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge1NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge1PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge1PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge2NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge2NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge2PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addExternalTemperatureEdge2PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //
  //     virtual void addInternalTemperatureEdge0NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge0NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge0PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge0PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge1NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge1NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge1PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge1PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge2NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge2NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge2PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //     virtual void addInternalTemperatureEdge2PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega) = 0;
  //
  //     virtual void addExternalTemperatureCornerNNN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerNNP(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerNPN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerNPP(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerPNN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerPNP(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerPPN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addExternalTemperatureCornerPPP(int x, int y, int z,
  //                                               T omega) =0;
  //
  //     virtual void addInternalTemperatureCornerNNN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerNNP(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerNPN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerNPP(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerPNN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerPNP(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerPPN(int x, int y, int z,
  //                                               T omega) =0;
  //     virtual void addInternalTemperatureCornerPPP(int x, int y, int z,
  //                                               T omega) =0;

  BlockLatticeStructure3D<T,Lattice>& getBlock();
  BlockLatticeStructure3D<T,Lattice> const& getBlock() const;
private:
  BlockLatticeStructure3D<T,Lattice>& block;
};


}

#endif
