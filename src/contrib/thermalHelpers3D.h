/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006 Jonas Latt, Orestis Malaspinas
 *  Address: Rue General Dufour 24,  1211 Geneva 4, Switzerland
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file A helper for initialising 2D boundaries -- header file.  */
#ifndef THERMAL_HELPERS_3D_H
#define THERMAL_HELPERS_3D_H

#include "thermalDynamicsHelpers3D.h"
#include <vector>
#include <list>

namespace olb {

/// Factory function for the creation of a LocalThermalBoundaryCondition3D
template<typename T, template<typename U> class Lattice, typename Dynamics>
ThermalBoundaryCondition3D<T,Lattice>*
createLocalThermalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block);

/// Factory function for InterpThermalBoundaryCondition3D, based on RLBdynamics
template<typename T, template<typename U> class Lattice>
ThermalBoundaryCondition3D<T,Lattice>*
createLocalThermalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block)
{
  return createLocalThermalBoundaryCondition3D<T,Lattice,ThermalBGKdynamics<T,Lattice> >(block);
}

}  // namespace olb

#endif
