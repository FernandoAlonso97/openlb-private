/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas, Jonas Latt
 *  Address: Rue General Dufour 24,  1211 Geneva 4, Switzerland
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A helper for initialising 3D boundaries -- generic implementation.
 */
#ifndef THERMAL_HELPERS_3D_HH
#define THERMAL_HELPERS_3D_HH

#include "thermalHelpers3D.h"
#include "cell.h"
#include "blockLattice3D.h"
#include "thermalBoundaries.h"
#include "thermalBoundaries.hh"
#include "thermalDynamicsHelpers3D.h"

namespace olb {

///////// class LocalThermalBoundaryCondition3D (declaration)  //////////////

template<typename T, template<typename U> class Lattice, typename Dynamics>
class LocalThermalBoundaryCondition3D : public ThermalBoundaryCondition3D<T,Lattice> {
public:
  typedef GenericBoundaryMomenta  TempBdMomenta_0N;
  typedef GenericBoundaryMomenta  TempBdMomenta_0P;
  typedef GenericBoundaryMomenta  TempBdMomenta_1N;
  typedef GenericBoundaryMomenta  TempBdMomenta_1P;
  typedef GenericBoundaryMomenta  TempBdMomenta_2N;
  typedef GenericBoundaryMomenta  TempBdMomenta_2P;

  typedef std::vector<TempBdMomenta_0N> TempBdMomentaArray_0N;
  typedef std::vector<TempBdMomenta_0P> TempBdMomentaArray_0P;
  typedef std::vector<TempBdMomenta_1N> TempBdMomentaArray_1N;
  typedef std::vector<TempBdMomenta_1P> TempBdMomentaArray_1P;
  typedef std::vector<TempBdMomenta_2N> TempBdMomentaArray_2N;
  typedef std::vector<TempBdMomenta_2P> TempBdMomentaArray_2P;

  typedef ThermalBoundaries<T, Lattice, Dynamics, 0, 1>  BdDynamics_0P;
  typedef ThermalBoundaries<T, Lattice, Dynamics, 0,-1>  BdDynamics_0N;
  typedef ThermalBoundaries<T, Lattice, Dynamics, 1, 1>  BdDynamics_1P;
  typedef ThermalBoundaries<T, Lattice, Dynamics, 1,-1>  BdDynamics_1N;
  typedef ThermalBoundaries<T, Lattice, Dynamics, 2, 1>  BdDynamics_2P;
  typedef ThermalBoundaries<T, Lattice, Dynamics, 2,-1>  BdDynamics_2N;

  typedef std::vector<BdDynamics_0P*> BdDynamicsArray_0P;
  typedef std::vector<BdDynamics_0N*> BdDynamicsArray_0N;
  typedef std::vector<BdDynamics_1P*> BdDynamicsArray_1P;
  typedef std::vector<BdDynamics_1N*> BdDynamicsArray_1N;
  typedef std::vector<BdDynamics_2P*> BdDynamicsArray_2P;
  typedef std::vector<BdDynamics_2N*> BdDynamicsArray_2N;

public:
  LocalThermalBoundaryCondition3D(BlockLatticeStructure3D<T,Lattice>& block);
  ~LocalThermalBoundaryCondition3D();

  virtual void addTemperatureBoundary0N(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega);
  virtual void addTemperatureBoundary0P(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega);
  virtual void addTemperatureBoundary1N(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega);
  virtual void addTemperatureBoundary1P(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega);
  virtual void addTemperatureBoundary2N(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega);
  virtual void addTemperatureBoundary2P(int x0, int x1, int y0, int y1,
                                        int z0, int z1, T omega);

  //     virtual void addExternalTemperatureEdge0NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge0NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge0PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge0PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge1NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge1NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge1PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge1PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge2NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge2NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge2PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addExternalTemperatureEdge2PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //
  //     virtual void addInternalTemperatureEdge0NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge0NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge0PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge0PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge1NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge1NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge1PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge1PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge2NN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge2NP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge2PN(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //     virtual void addInternalTemperatureEdge2PP(int x0, int x1, int y0, int y1,
  //                                             int z0, int z1, T omega);
  //
  //     virtual void addExternalTemperatureCornerNNN(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerNNP(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerNPN(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerNPP(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerPNN(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerPNP(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerPPN(int x, int y, int z, T omega);
  //     virtual void addExternalTemperatureCornerPPP(int x, int y, int z, T omega);
  //
  //     virtual void addInternalTemperatureCornerNNN(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerNNP(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerNPN(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerNPP(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerPNN(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerPNP(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerPPN(int x, int y, int z, T omega);
  //     virtual void addInternalTemperatureCornerPPP(int x, int y, int z, T omega);

private:
  template<typename ListT>
  static void cleanMomentaList(ListT& dynList);
  template<typename ListT>
  static void cleanDynamicsList(std::list<ListT*>& dynList);
  template<typename MomentaArrayType, typename DynamicsType>
  void addGenericBoundary(
    int x0, int x1, int y0, int y1, int z0, int z1,
    T omega,
    std::list<MomentaArrayType*>& bdMomentaList,
    std::list<std::vector<DynamicsType*>*>& bdDynamicsList);
private:
  std::list<TempBdMomentaArray_0N*> TempBdMomentaList_0N;
  std::list<TempBdMomentaArray_0P*> TempBdMomentaList_0P;
  std::list<TempBdMomentaArray_1N*> TempBdMomentaList_1N;
  std::list<TempBdMomentaArray_1P*> TempBdMomentaList_1P;
  std::list<TempBdMomentaArray_2N*> TempBdMomentaList_2N;
  std::list<TempBdMomentaArray_2P*> TempBdMomentaList_2P;

  std::list<BdDynamicsArray_0N*>  bdDynamicsList_0N;
  std::list<BdDynamicsArray_0P*>  bdDynamicsList_0P;
  std::list<BdDynamicsArray_1N*>  bdDynamicsList_1N;
  std::list<BdDynamicsArray_1P*>  bdDynamicsList_1P;
  std::list<BdDynamicsArray_2N*>  bdDynamicsList_2N;
  std::list<BdDynamicsArray_2P*>  bdDynamicsList_2P;
};

///////// class ThermalBoundaryCondition3D ///////////////////////////////////

template<typename T, template<typename U> class Lattice>
ThermalBoundaryCondition3D<T,Lattice>::ThermalBoundaryCondition3D (
  BlockLatticeStructure3D<T,Lattice>& block_ )
  : block(block_)
{ }

template<typename T, template<typename U> class Lattice>
BlockLatticeStructure3D<T,Lattice>& ThermalBoundaryCondition3D<T,Lattice>::getBlock()
{
  return block;
}

template<typename T, template<typename U> class Lattice>
BlockLatticeStructure3D<T,Lattice> const& ThermalBoundaryCondition3D<T,Lattice>::
getBlock() const
{
  return block;
}


///////// class LocalThermalBoundaryCondition3D ////////////////////////

template<typename T, template<typename U> class Lattice, typename Dynamics>
LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::LocalThermalBoundaryCondition3D (
  BlockLatticeStructure3D<T,Lattice>& block )
  : ThermalBoundaryCondition3D<T,Lattice>(block)
{ }

template<typename T, template<typename U> class Lattice, typename Dynamics>
LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::~LocalThermalBoundaryCondition3D()
{
  cleanDynamicsList(bdDynamicsList_0N);
  cleanDynamicsList(bdDynamicsList_0P);
  cleanDynamicsList(bdDynamicsList_1N);
  cleanDynamicsList(bdDynamicsList_1P);
  cleanDynamicsList(bdDynamicsList_2N);
  cleanDynamicsList(bdDynamicsList_2P);

  cleanMomentaList(TempBdMomentaList_0N);
  cleanMomentaList(TempBdMomentaList_0P);
  cleanMomentaList(TempBdMomentaList_1N);
  cleanMomentaList(TempBdMomentaList_1P);
  cleanMomentaList(TempBdMomentaList_2N);
  cleanMomentaList(TempBdMomentaList_2P);
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
template<typename ListT>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::cleanMomentaList(ListT& list)
{
  typename ListT::iterator iL = list.begin();
  for (; iL != list.end(); ++iL) {
    delete *iL;
  }
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
template<typename ListT>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::cleanDynamicsList (
  std::list<ListT*>& list)
{
  typename std::list<ListT*>::iterator iL = list.begin();
  for (; iL != list.end(); ++iL) {
    ListT& dArray = **iL;
    for (unsigned iA=0; iA<dArray.size(); ++iA) {
      delete dArray[iA];
    }
    delete *iL;
  }

}

template<typename T, template<typename U> class Lattice, typename Dynamics>
template<typename MomentaArrayType, typename DynamicsType>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addGenericBoundary(int x0, int x1, int y0, int y1, int z0, int z1,
                   T omega,
                   std::list<MomentaArrayType*>& bdMomentaList,
                   std::list<std::vector<DynamicsType*>*>& bdDynamicsList)
{
  typedef typename std::vector<DynamicsType*> DynamicsArrayType;
  OLB_PRECONDITION( x0==x1 || y0==y1 || z0==z1 );

  int size = (x1-x0+1) * (y1-y0+1) * (z1-z0+1);
  MomentaArrayType* bdMomenta = new MomentaArrayType;
  DynamicsArrayType* bdDynamics = new DynamicsArrayType;
  bdMomenta -> resize(size);
  bdDynamics -> resize(size);

  int iDyn=0;
  for (int iX=x0; iX<=x1; ++iX) {
    for (int iY=y0; iY<=y1; ++iY) {
      for (int iZ=z0; iZ<=z1; ++iZ) {
        DynamicsType* newDyn = new DynamicsType (
          omega,
          (*bdMomenta)[iDyn],
          this->getBlock().getStatistics()
        );
        this->getBlock().defineDynamics(iX,iX,iY,iY,iZ,iZ, newDyn);
        (*bdDynamics)[iDyn] = newDyn;
        ++iDyn;
      }
    }
  }

  bdMomentaList.push_back(bdMomenta);
  bdDynamicsList.push_back(bdDynamics);
}


template<typename T, template<typename U> class Lattice, typename Dynamics>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addTemperatureBoundary0N(int x0, int x1, int y0, int y1, int z0, int z1,
                         T omega)
{
  addGenericBoundary(x0,x1,y0,y1,z0,z1, omega,
                     TempBdMomentaList_0N, bdDynamicsList_0N );
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addTemperatureBoundary0P(int x0, int x1, int y0, int y1, int z0, int z1,
                         T omega)
{
  addGenericBoundary(x0,x1,y0,y1,z0,z1, omega,
                     TempBdMomentaList_0P, bdDynamicsList_0P);
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addTemperatureBoundary1N(int x0, int x1, int y0, int y1, int z0, int z1,
                         T omega)
{
  addGenericBoundary(x0,x1,y0,y1,z0,z1, omega,
                     TempBdMomentaList_1N, bdDynamicsList_1N);
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addTemperatureBoundary1P(int x0, int x1, int y0, int y1, int z0, int z1,
                         T omega)
{
  addGenericBoundary(x0,x1,y0,y1,z0,z1, omega,
                     TempBdMomentaList_1P, bdDynamicsList_1P);
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addTemperatureBoundary2N(int x0, int x1, int y0, int y1, int z0, int z1,
                         T omega)
{
  addGenericBoundary(x0,x1,y0,y1,z0,z1, omega,
                     TempBdMomentaList_2N, bdDynamicsList_2N);
}

template<typename T, template<typename U> class Lattice, typename Dynamics>
void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
addTemperatureBoundary2P(int x0, int x1, int y0, int y1, int z0, int z1,
                         T omega)
{
  addGenericBoundary(x0,x1,y0,y1,z0,z1, omega,
                     TempBdMomentaList_2P, bdDynamicsList_2P);
}

// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge0NN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge0NP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge0PN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge0PP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge1NN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge1NP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge1PN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge1PP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge2NN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge2NP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge2PN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addExternalTemperatureEdge2PP(x0,x1,y0,y1,z0,z1, omega);
// }
//
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge0NN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge0NN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge0NP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge0NP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge0PN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge0PN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge0PP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge0PP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge1NN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge1NN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge1NP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge1NP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge1PN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge1PN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge1PP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge1PP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge2NN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge2NN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge2NP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge2NP(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge2PN(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge2PN(x0,x1,y0,y1,z0,z1, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureEdge2PP(int x0, int x1, int y0, int y1, int z0, int z1,
//                                T omega)
// {
//     interpBoundary.addInternalTemperatureEdge2PP(x0,x1,y0,y1,z0,z1, omega);
// }
//
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerNNN(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerNNN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerNNP(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerNNP(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerNPN(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerNPN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerNPP(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerNPP(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerPNN(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerPNN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerPNP(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerPNP(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerPPN(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerPPN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addExternalTemperatureCornerPPP(int x, int y, int z, T omega)
// {
//     interpBoundary.addExternalTemperatureCornerPPP(x,y,z, omega);
// }
//
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerNNN(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerNNN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerNNP(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerNNP(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerNPN(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerNPN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerNPP(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerNPP(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerPNN(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerPNN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerPNP(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerPNP(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerPPN(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerPPN(x,y,z, omega);
// }
//
// template<typename T, template<typename U> class Lattice, typename Dynamics>
// void LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>::
//     addInternalTemperatureCornerPPP(int x, int y, int z, T omega)
// {
//     interpBoundary.addInternalTemperatureCornerPPP(x,y,z, omega);
// }


///////// Factory functions ///////////////////////////////////

template<typename T, template<typename U> class Lattice, typename Dynamics>
ThermalBoundaryCondition3D<T,Lattice>* createLocalThermalBoundaryCondition3D (
  BlockLatticeStructure3D<T,Lattice>& block)
{
  return new LocalThermalBoundaryCondition3D<T,Lattice,Dynamics>(block);
}

}  // namespace olb

#endif
