/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas, Jonas Latt
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef THERMAL_INTO_NAVIER_STOKES_COUPLING_POST_PROCESSOR_3D_H
#define THERMAL_INTO_NAVIER_STOKES_COUPLING_POST_PROCESSOR_3D_H

#include "spatiallyExtendedObject3D.h"
#include "core/postProcessing.h"
#include "core/blockLattice3D.h"


namespace olb {

/**
* Multiphysics class for coupling between different lattices.
*/

template<typename T, template<typename U> class Lattice>
class ThermalIntoNavierStokesCouplingPostProcessor3D : public LocalPostProcessor3D<T,Lattice> {
public:
  ThermalIntoNavierStokesCouplingPostProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
      std::vector<SpatiallyExtendedObject3D> spBlock_);
  virtual int extent() const
  {
    return 0;
  }
  virtual int extent(int whichDirection) const
  {
    return 0;
  }
  virtual void process(BlockLattice3D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_, int z0_, int z1_);
private:
  int x0, x1, y0, y1, z0, z1;

  std::vector<SpatiallyExtendedObject3D> spBlock;
};

template<typename T, template<typename U> class Lattice>
class ThermalIntoNavierStokesCouplingGenerator3D
  : public PostProcessorGenerator3D<T,Lattice> {
public:
  ThermalIntoNavierStokesCouplingGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
      std::vector<SpatiallyExtendedObject3D> spBlock_);
  virtual PostProcessor3D<T,Lattice>* generate() const;
  virtual PostProcessorGenerator3D<T,Lattice>*  clone() const;
private:
  std::vector<SpatiallyExtendedObject3D> spBlock;
};


}

#endif
