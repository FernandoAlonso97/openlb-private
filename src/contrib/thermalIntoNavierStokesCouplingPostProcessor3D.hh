/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas, Jonas Latt
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef THERMAL_INTO_NAVIER_STOKES_COUPLING_POST_PROCESSOR_3D_HH
#define THERMAL_INTO_NAVIER_STOKES_COUPLING_POST_PROCESSOR_3D_HH

#include "core/latticeDescriptors.h"
#include "thermalIntoNavierStokesCouplingPostProcessor3D.h"
#include "core/blockLattice3D.h"
#include "core/util.h"
#include "core/finiteDifference3D.h"

using namespace boost;

namespace olb {

////////  ThermalIntoNavierStokesCouplingPostProcessor3D ///////////////////////////////////

template<typename T, template<typename U> class Lattice>
ThermalIntoNavierStokesCouplingPostProcessor3D <T,Lattice>::
ThermalIntoNavierStokesCouplingPostProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
    std::vector<SpatiallyExtendedObject> &spBlock_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_), spBlock(spBlock_)
{
}

template<typename T, template<typename U> class Lattice>
void ThermalIntoNavierStokesCouplingPostProcessor3D<T,Lattice>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  typedef Lattice<T> L;

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1, z0, z1,
         x0_, x1_, y0_, y1_, z0_, z1_,
         newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {

    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          T *temperature = spBlock[0].get(iX,iY,iZ).getExternalField(
                             L::ExternalField::TemperatureBeginsAt);

          temperature[0] = blockLattice.get(iX,iY,iZ).computeRho();
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void ThermalIntoNavierStokesCouplingPostProcessor3D<T,Lattice>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
}

////////  ExtendedFdPlaneBoundaryProcessorGenertor3D ///////////////////////////////

template<typename T, template<typename U> class Lattice>
ThermalIntoNavierStokesCouplingGenerator3D<T,Lattice>::
ThermalIntoNavierStokesCouplingGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
    std::vector<SpatiallyExtendedObject> &spBlock_)
  : PostProcessorGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_), spBlock(spBlock_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor3D<T,Lattice>*
ThermalIntoNavierStokesCouplingGenerator3D<T,Lattice>::generate() const
{
  return new ThermalIntoNavierStokesCouplingPostProcessor3D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1, this->spBlock);
}

template<typename T, template<typename U> class Lattice>
PostProcessorGenerator3D<T,Lattice>*
ThermalIntoNavierStokesCouplingGenerator3D<T,Lattice>::clone() const
{
  return new ThermalIntoNavierStokesCouplingGenerator3D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1, this->spBlock);
}


}  // namespace olb

#endif
