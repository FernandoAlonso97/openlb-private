/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef THI_POST_PROCESSOR_3D_H
#define THI_POST_PROCESSOR_3D_H

#include "core/postProcessing.h"
#include "core/blockLattice3D.h"
#include <cmath>
#include <boost/random.hpp>

using namespace boost;


namespace olb {

/**
* This class computes the finite difference approximation to LB boundary conditions
* on a plane wall in 3D with all the terms of the CE expansion.
*/

template<typename T>
class ForceStruct {
public:
  virtual ~ForceStruct() { }
  virtual T operator()(T n) const = 0;
};

template<typename T, template<typename U> class Lattice>
class THIpostProcessor3D : public LocalPostProcessor3D<T,Lattice> {
public:
  THIpostProcessor3D (int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
                      T P_,
                      ForceStruct<T> const& forceStruct_, unsigned seed=48u);
  virtual int extent() const
  {
    return 0;
  }
  virtual int extent(int whichDirection) const
  {
    return 0;
  }
  virtual void process(BlockLattice3D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_, int z0_, int z1_
                               );
private:
  void seed(unsigned value);

private:
  T pi;

  int x0, x1, y0, y1, z0, z1;

  static const int na = 1;
  static const int nb = 2;

  T P;
  T ampl;
  ForceStruct<T> const& forceStruct;

  mt19937 rng;
  uniform_real<> circle;
  variate_generator<mt19937,uniform_real<> > thetaGen;

};

template<typename T, template<typename U> class Lattice>
class THIpostProcessorGenerator3D
  : public PostProcessorGenerator3D<T,Lattice> {
public:
  THIpostProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
                              T P_,
                              ForceStruct<T> const& forceStruct_, unsigned seed_=48u);
  virtual PostProcessor3D<T,Lattice>* generate() const;
  virtual PostProcessorGenerator3D<T,Lattice>*  clone() const;

private:
  T P;
  ForceStruct<T> const& forceStruct;
  unsigned seed;
};


}

#endif
