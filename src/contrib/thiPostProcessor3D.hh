/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2007 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis.malaspinas@epfl.ch
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef THI_POST_PROCESSOR_3D_HH
#define THI_POST_PROCESSOR_3D_HH

#include "core/latticeDescriptors.h"
#include "thiPostProcessor3D.h"
#include "core/blockLattice3D.h"
#include "core/util.h"

using namespace boost;

namespace olb {

////////  THIpostProcessor3D ///////////////////////////////////

template<typename T, template<typename U> class Lattice>
THIpostProcessor3D <T,Lattice>::
THIpostProcessor3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
                   T P_, ForceStruct<T> const& forceStruct_, unsigned seed)
  :  pi((T)4 * atan((T)1)), x0(x0_), x1(x1_), y0(y0_), y1(y1_), z0(z0_), z1(z1_),
     P(P_), forceStruct(forceStruct_), rng(seed),
     circle(0., 2.*pi), thetaGen(rng, circle)
{
  ampl = sqrt(P /
              (3.*pi* (pow(na,-2./3.) - pow(nb,-2./3.)))
             );
  std::cout << "ampl=" << ampl << std::endl;
  rng.seed(seed);
  thetaGen = variate_generator<mt19937,uniform_real<> >(rng, circle);
}

template<typename T, template<typename U> class Lattice>
void THIpostProcessor3D<T,Lattice>::seed(unsigned value)
{
  rng.seed(value);
  thetaGen = variate_generator<mt19937,uniform_real<> >(rng, circle);
}

template<typename T, template<typename U> class Lattice>
void THIpostProcessor3D<T,Lattice>::
processSubDomain(BlockLattice3D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  typedef Lattice<T> L;
  using namespace olb::util::tensorIndices3D;
  enum {x,y,z};

  int newX0, newX1, newY0, newY1, newZ0, newZ1;
  int Lx = blockLattice.getNx();
  int Ly = blockLattice.getNy();
  int Lz = blockLattice.getNz();

  if ( util::intersect (
         x0, x1, y0, y1, z0, z1,
         x0_, x1_, y0_, y1_, z0_, z1_,
         newX0, newX1, newY0, newY1, newZ0, newZ1 ) ) {

    T forceIni[3] = {T(), T(), T()};
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
          blockLattice.get(iX,iY,iZ).defineExternalField (
            L::ExternalField::forceBeginsAt,
            L::ExternalField::sizeOfForce,
            forceIni);
        }
      }
    }

    T sineVal[4*Lx-3];
    T oneOverSqrt3 = 1. / sqrt(3.);
    for (int nX = 0; nX < 3; ++nX) {
      for (int nY = 0; nY < 3; ++nY) {
        for (int nZ = 0; nZ < 3; ++nZ) {
          T nSqr = (T)(nX*nX) + (T)(nY*nY) + (T)(nZ*nZ);
          T n    = sqrt(nSqr);
          //                     std::cout << nX << " " << nY << " " << nZ << std::endl;
          T A = ampl * forceStruct(n);
          if (na-0.5 < n && n < nb+0.5) {
            T theta = thetaGen();
            for (int iSin = 0; iSin < 4*Lx-3; ++iSin) {
              sineVal[iSin] =
                sin(2.*pi/(T)Lx*(T)iSin+theta);
            }
            for (int iY=newY0; iY<=newY1; ++iY) {
              for (int iZ=newZ0; iZ<=newZ1; ++iZ) {
                T angle = sin(thetaGen());
                for (int iX=newX0; iX<=newX1; ++iX) {
                  T* force =
                    blockLattice.get(iX,iY,iZ).getExternal(
                      L::ExternalField::forceBeginsAt);
                  force[x] +=
                    oneOverSqrt3 * angle*A* sineVal[(iX*nX+iY*nY+iZ*nZ)];
                  //                                     force[x] +=
                  //                                     1. / sqrt(3.) * angle * A *sin(2.*pi/(T)Lx*(T)(iX*nX+iY*nY+iZ*nZ)+theta);
                }
              }
            }
            for (int iX = newX0; iX <= newX1; ++iX) {
              for (int iZ = newZ0; iZ <= newZ1; ++iZ) {
                T angle = sin(thetaGen());
                for (int iY = newY0; iY <= newY1; ++iY) {
                  T* force =
                    blockLattice.get(iX,iY,iZ).getExternal(
                      L::ExternalField::forceBeginsAt);
                  //                                     force[y] +=
                  //                                     1. / sqrt(3.) * angle * A *sin(2.*pi/(T)Lx*(T)(iX*nX+iY*nY+iZ*nZ)+theta);
                  force[y] +=
                    oneOverSqrt3 * angle*A* sineVal[(iX*nX+iY*nY+iZ*nZ)];
                }
              }
            }
            for (int iX = newX0; iX <= newX1; ++iX) {
              for (int iY = newY0; iY <= newY1; ++iY) {
                T angle = sin(thetaGen());
                for (int iZ = newZ0; iZ <= newZ1; ++iZ) {
                  T* force =
                    blockLattice.get(iX,iY,iZ).getExternal(
                      L::ExternalField::forceBeginsAt);
                  //                                     force[z] +=
                  //                                     1. / sqrt(3.) * angle * A *sin(2.*pi/(T)Lx*(T)(iX*nX+iY*nY+iZ*nZ)+theta);
                  force[z] +=
                    oneOverSqrt3 * angle*A* sineVal[(iX*nX+iY*nY+iZ*nZ)];
                }
              }
            }
          }
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void THIpostProcessor3D<T,Lattice>::
process(BlockLattice3D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1, z0, z1);
}

////////  ExtendedFdPlaneBoundaryProcessorGenertor3D ///////////////////////////////

template<typename T, template<typename U> class Lattice>
THIpostProcessorGenerator3D<T,Lattice>::
THIpostProcessorGenerator3D(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
                            T P_, ForceStruct<T> const& forceStruct_, unsigned seed_)
  : PostProcessorGenerator3D<T,Lattice>(x0_, x1_, y0_, y1_, z0_, z1_),
    P(P_), forceStruct(forceStruct_), seed(seed_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor3D<T,Lattice>*
THIpostProcessorGenerator3D<T,Lattice>::generate() const
{
  return new THIpostProcessor3D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1,
          this->P, this->forceStruct, this->seed);
}

template<typename T, template<typename U> class Lattice>
PostProcessorGenerator3D<T,Lattice>*
THIpostProcessorGenerator3D<T,Lattice>::clone() const
{
  return new THIpostProcessorGenerator3D<T,Lattice>
         (this->x0, this->x1, this->y0, this->y1, this->z0, this->z1,
          this->P, this->forceStruct, this->seed);
}


}  // namespace olb

#endif
