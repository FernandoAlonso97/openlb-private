/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef VISCO_FINITE_DIFFERENCE_H
#define VISCO_FINITE_DIFFERENCE_H

namespace olb {

namespace fd {

/// Second-order central gradient (u_p1 = u(x+1))
template<typename T>
T centralGradient(T u_p2, T u_p1, T u_m1, T u_m2)
{
  return ((T)8* (u_p1-u_m1) + u_m2 - u_p2)/(T)12;
}

/// Second-order central gradient (u_p1 = u(x+1))
template<typename T>
T centralGradient(T u_p1, T u_m1, T dx)
{
  return (u_p1-u_m1)/((T)2*dx);
}

/// Second-order asymmetric gradient (u_1 = u(x+1))
template<typename T>
T boundaryGradient(T u_0, T u_1, T u_2, T u_3, T u_4)
{
  return (-(T)25*u_0 + (T)48*u_1 - (T)36*u_2 + (T)16*u_3 - (T)3*u_4) / (T)12;
}
}  // namespace fd

}  // namespace olb


#endif
