/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef VISCO_FINITE_DIFFERENCE_2D_H
#define VISCO_FINITE_DIFFERENCE_2D_H

#include "core/finiteDifference.h"
#include "contrib/viscoFiniteDifference.h"
#include "core/util.h"

namespace olb {

namespace fd {

template<typename T, template<typename U> class Lattice,
         int direction, int orientation,
         bool orthogonal>
struct ViscoDirectedGradients2D {
  static void interpolateVector2nd(T velDeriv[Lattice<T>::d],
                                   BlockLattice2D<T,Lattice> const& blockLattice,
                                   int iX, int iY);
  static void interpolateTensor2nd(T tauDeriv[util::TensorVal<Lattice<T> >::n],
                                   TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n > const& tau,
                                   int iX, int iY);

  static void interpolateVector4th(T velDeriv[Lattice<T>::d],
                                   BlockLattice2D<T,Lattice> const& blockLattice,
                                   int iX, int iY);
  static void interpolateTensor4th(T tauDeriv[util::TensorVal<Lattice<T> >::n],
                                   TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n > const& tau,
                                   int iX, int iY);
};

// Implementation for orthogonal==true; i.e. the derivative is along
// the boundary normal.
template<typename T, template<typename U> class Lattice,
         int direction, int orientation>
struct ViscoDirectedGradients2D<T, Lattice, direction, orientation, true> {
  static void interpolateVector2nd(T velDeriv[Lattice<T>::d],
                                   BlockLattice2D<T,Lattice> const& blockLattice,
                                   int iX, int iY)
  {
    enum {velOffset = Lattice<T>::ExternalField::velocityBeginsAt};
    using namespace fd;

    const T *u0 = blockLattice.get(iX,iY).getExternal(velOffset);
    const T *u1 = blockLattice.get (
                    iX+(direction==0 ? (-orientation):0),
                    iY+(direction==1 ? (-orientation):0) ).getExternal(velOffset);
    const T *u2 = blockLattice.get (
                    iX+(direction==0 ? (-2*orientation):0),
                    iY+(direction==1 ? (-2*orientation):0) ).getExternal(velOffset);

    for (int iD=0; iD<Lattice<T>::d; ++iD) {
      velDeriv[iD] = -orientation * boundaryGradient(u0[iD], u1[iD], u2[iD]);
    }
  }

  static void interpolateTensor2nd(T tauDeriv[util::TensorVal<Lattice<T> >::n],
                                   TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n > const& tau,
                                   int iX, int iY)
  {
    using namespace fd;

    T tau0[util::TensorVal<Lattice<T> >::n];
    T tau1[util::TensorVal<Lattice<T> >::n];
    T tau2[util::TensorVal<Lattice<T> >::n];

    for (int iTau = 0; iTau < util::TensorVal<Lattice<T> >::n; ++iTau) {
      tau0[iTau] = tau.get(iX,iY)[iTau];
      tau1[iTau] = tau.get (
                     iX+(direction==0 ? (-orientation):0),
                     iY+(direction==1 ? (-orientation):0) )[iTau];
      tau2[iTau] = tau.get (
                     iX+(direction==0 ? (-2*orientation):0),
                     iY+(direction==1 ? (-2*orientation):0) )[iTau];

      tauDeriv[iTau] = -orientation * boundaryGradient(tau0[iTau],
                       tau1[iTau], tau2[iTau]);
    }
  }

  static void interpolateVector4th(T velDeriv[Lattice<T>::d],
                                   BlockLattice2D<T,Lattice> const& blockLattice,
                                   int iX, int iY)
  {
    enum {velOffset = Lattice<T>::ExternalField::velocityBeginsAt};
    using namespace fd;

    const T *u0 = blockLattice.get(iX,iY).getExternal(velOffset);
    const T *u1 = blockLattice.get (
                    iX+(direction==0 ? (-orientation):0),
                    iY+(direction==1 ? (-orientation):0) ).getExternal(velOffset);
    const T *u2 = blockLattice.get (
                    iX+(direction==0 ? (-2*orientation):0),
                    iY+(direction==1 ? (-2*orientation):0) ).getExternal(velOffset);

    const T *u3 = blockLattice.get (
                    iX+(direction==0 ? (-3*orientation):0),
                    iY+(direction==1 ? (-3*orientation):0) ).getExternal(velOffset);

    const T *u4 = blockLattice.get (
                    iX+(direction==0 ? (-4*orientation):0),
                    iY+(direction==1 ? (-4*orientation):0) ).getExternal(velOffset);

    for (int iD=0; iD<Lattice<T>::d; ++iD) {
      velDeriv[iD] = -orientation * boundaryGradient(u0[iD], u1[iD], u2[iD], u3[iD], u4[iD]);
    }
  }

  static void interpolateTensor4th(T tauDeriv[util::TensorVal<Lattice<T> >::n],
                                   TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n > const& tau,
                                   int iX, int iY)
  {
    using namespace fd;

    T tau0[util::TensorVal<Lattice<T> >::n];
    T tau1[util::TensorVal<Lattice<T> >::n];
    T tau2[util::TensorVal<Lattice<T> >::n];
    T tau3[util::TensorVal<Lattice<T> >::n];
    T tau4[util::TensorVal<Lattice<T> >::n];

    for (int iTau = 0; iTau < util::TensorVal<Lattice<T> >::n; ++iTau) {
      tau0[iTau] = tau.get(iX,iY)[iTau];
      tau1[iTau] = tau.get (
                     iX+(direction==0 ? (-orientation):0),
                     iY+(direction==1 ? (-orientation):0) )[iTau];
      tau2[iTau] = tau.get (
                     iX+(direction==0 ? (-2*orientation):0),
                     iY+(direction==1 ? (-2*orientation):0) )[iTau];
      tau3[iTau] = tau.get (
                     iX+(direction==0 ? (-3*orientation):0),
                     iY+(direction==1 ? (-3*orientation):0) )[iTau];
      tau4[iTau] = tau.get (
                     iX+(direction==0 ? (-4*orientation):0),
                     iY+(direction==1 ? (-4*orientation):0) )[iTau];

      tauDeriv[iTau] = -orientation * boundaryGradient(tau0[iTau],
                       tau1[iTau], tau2[iTau], tau3[iTau], tau4[iTau]);
    }
  }
};


// Implementation for orthogonal==false; i.e. the derivative is aligned
// with the boundary.
template<typename T, template<typename U> class Lattice,
         int direction, int orientation>
struct ViscoDirectedGradients2D<T, Lattice, direction, orientation, false> {
  static void interpolateVector2nd(T velDeriv[Lattice<T>::d],
                                   BlockLattice2D<T,Lattice> const& blockLattice,
                                   int iX, int iY)
  {
    enum {velOffset = Lattice<T>::ExternalField::velocityBeginsAt};
    using namespace fd;

    int deriveDirection = 1-direction;
    const T *u_p1 = blockLattice.get (
                      iX+(deriveDirection==0 ? 1:0),
                      iY+(deriveDirection==1 ? 1:0) ).getExternal(velOffset);
    const T *u_m1 = blockLattice.get (
                      iX+(deriveDirection==0 ? (-1):0),
                      iY+(deriveDirection==1 ? (-1):0) ).getExternal(velOffset);

    for (int iD=0; iD<Lattice<T>::d; ++iD) {
      velDeriv[iD] = fd::centralGradient(u_p1[iD],u_m1[iD]);
    }
  }

  static void interpolateTensor2nd(T tauDeriv[util::TensorVal<Lattice<T> >::n],
                                   TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n > const& tau,
                                   int iX, int iY)
  {
    using namespace fd;

    T tau_p1[util::TensorVal<Lattice<T> >::n], tau_m1[util::TensorVal<Lattice<T> >::n];

    for (int iTau = 0; iTau < util::TensorVal<Lattice<T> >::n; ++iTau) {
      int deriveDirection = 1-direction;

      tau_p1[iTau] = tau.get(iX+(deriveDirection==0 ? 1:0),
                             iY+(deriveDirection==1 ? 1:0) )[iTau];
      tau_m1[iTau] = tau.get(iX+(deriveDirection==0 ? (-1):0),
                             iY+(deriveDirection==1 ? (-1):0))[iTau];

      tauDeriv[iTau] = fd::centralGradient(tau_p1[iTau],tau_m1[iTau]);
    }
  }

  static void interpolateVector4th(T velDeriv[Lattice<T>::d],
                                   BlockLattice2D<T,Lattice> const& blockLattice,
                                   int iX, int iY)
  {
    enum {velOffset = Lattice<T>::ExternalField::velocityBeginsAt};
    using namespace fd;

    int deriveDirection = 1-direction;
    const T *u_p1 = blockLattice.get (
                      iX+(deriveDirection==0 ? 1:0),
                      iY+(deriveDirection==1 ? 1:0) ).getExternal(velOffset);
    const T *u_m1 = blockLattice.get (
                      iX+(deriveDirection==0 ? (-1):0),
                      iY+(deriveDirection==1 ? (-1):0) ).getExternal(velOffset);

    const T *u_p2 = blockLattice.get (
                      iX+(deriveDirection==0 ? 2:0),
                      iY+(deriveDirection==1 ? 2:0) ).getExternal(velOffset);
    const T *u_m2 = blockLattice.get (
                      iX+(deriveDirection==0 ? (-2):0),
                      iY+(deriveDirection==1 ? (-2):0) ).getExternal(velOffset);

    for (int iD=0; iD<Lattice<T>::d; ++iD) {
      velDeriv[iD] = fd::centralGradient(u_p2[iD], u_p1[iD],u_m1[iD],u_m2[iD]);
    }
  }

  static void interpolateTensor4th(T tauDeriv[util::TensorVal<Lattice<T> >::n],
                                   TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n > const& tau,
                                   int iX, int iY)
  {
    using namespace fd;

    T tau_p1[util::TensorVal<Lattice<T> >::n], tau_m1[util::TensorVal<Lattice<T> >::n];
    T tau_p2[util::TensorVal<Lattice<T> >::n], tau_m2[util::TensorVal<Lattice<T> >::n];

    for (int iTau = 0; iTau < util::TensorVal<Lattice<T> >::n; ++iTau) {
      int deriveDirection = 1-direction;

      tau_p1[iTau] = tau.get(iX+(deriveDirection==0 ? 1:0),
                             iY+(deriveDirection==1 ? 1:0) )[iTau];
      tau_m1[iTau] = tau.get(iX+(deriveDirection==0 ? (-1):0),
                             iY+(deriveDirection==1 ? (-1):0))[iTau];
      tau_p2[iTau] = tau.get(iX+(deriveDirection==0 ? 2:0),
                             iY+(deriveDirection==1 ? 2:0) )[iTau];
      tau_m2[iTau] = tau.get(iX+(deriveDirection==0 ? (-2):0),
                             iY+(deriveDirection==1 ? (-2):0))[iTau];

      tauDeriv[iTau] = fd::centralGradient(tau_p2[iTau],tau_p1[iTau],tau_m1[iTau],tau_m2[iTau]);
    }
  }
};

}  // namespace fd

}  // namespace olb


#endif
