/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef VISCO_HELPERS_H
#define VISCO_HELPERS_H

#include "core/util.h"

namespace olb {

template<typename T>
struct viscoHelpers {

  /// Second-order extrapolation
  static void computeSource(T source[3], T tau[3], T A[3], T gradU[2][2], T muP)
  {
    using namespace util::tensorIndices2D;
    enum {x, y};

    source[xx] = -tau[xx] / muP
                 + (T)2 * (A[xx] * gradU[x][x] + A[xy]*gradU[y][x]);

    source[xy] = -tau[xy] / muP
                 + A[xx] * gradU[x][y] + A[xy]*(gradU[y][y]+ gradU[x][x])
                 + gradU[y][x] * A[yy];

    source[yy] = -tau[yy] / muP
                 + (T)2 * (A[xy] * gradU[x][y] + A[yy]*gradU[y][y]);
  }

  /// Second-order extrapolation
  static void computeSource3D(T source[6], T tau[6], T A[6], T gradU[3][3], T muP)
  {
    using namespace util::tensorIndices3D;
    enum {x, y, z};

    //      source[ab] = -tau[ab] / muP
    //          + (A[ax]*gradU[x][b]+A[ay]*gradU[y][b]+A[az]*gradU[z][b])
    //          + (gradU[x][a]*A[xb]+gradU[y][a]*A[yb]+gradU[z][a]*A[zb]);

    source[xx] = -tau[xx] / muP
                 +(T)2*(A[xx]*gradU[x][x]+A[xy]*gradU[y][x]+A[xz]*gradU[z][x]);

    source[xy] = -tau[xy] / muP
                 + (A[xx]*gradU[x][y]+A[xy]*gradU[y][y]+A[xz]*gradU[z][y])
                 + (gradU[x][x]*A[xy]+gradU[y][x]*A[yy]+gradU[z][x]*A[yz]);

    source[xz] = -tau[xz] / muP
                 + (A[xx]*gradU[x][z]+A[xy]*gradU[y][z]+A[xz]*gradU[z][z])
                 + (gradU[x][x]*A[xz]+gradU[y][x]*A[yz]+gradU[z][x]*A[zz]);

    source[yy] = -tau[yy] / muP
                 + (T)2*(A[xy]*gradU[x][y]+A[yy]*gradU[y][y]+A[yz]*gradU[z][y]);

    source[yz] = -tau[yz] / muP
                 + (A[xy]*gradU[x][z]+A[yy]*gradU[y][z]+A[yz]*gradU[z][z])
                 + (gradU[x][y]*A[xz]+gradU[y][y]*A[yz]+gradU[z][y]*A[zz]);

    source[zz] = -tau[zz] / muP
                 + (T)2*(A[xz]*gradU[x][z]+A[yz]*gradU[y][z]+A[zz]*gradU[z][z]);

  }

  //for fenep (le2 < infinity)

  static void fromAtoTau(T A[3], T tau[3], T muP, T lambda, T le2)
  {
    using namespace util::tensorIndices2D;
    enum {x, y};

    T trA = A[xx] + A[yy];
    T factor = muP / lambda;
    T factor2 = (T)1 / ((T)1 - trA/le2);
    T a = (T)1 / ((T)1 - (T)3/le2); // the 3 could be a 2
    tau[xx] =  factor * (A[xx] * factor2 - a);
    tau[xy] =  factor *  A[xy] * factor2;
    tau[yy] =  factor * (A[yy] * factor2 - a);
  }

  static void fromTauToA(T tau[3], T A[3], T muP, T lambda, T le2)
  {
    using namespace util::tensorIndices2D;

    const T lambdaOverMu = lambda/muP;

    T trTau = tau[xx] + tau[yy];
    T a = (T)1 / ((T)1-(T)3/le2); // the 3 could be a 2
    T fact = (T)1 + ((T)2*a + lambdaOverMu*trTau) / le2;
    A[xx] = (tau[xx]*lambdaOverMu + a)/fact;

    A[xy] = (tau[xy]*lambdaOverMu)/fact;

    A[yy] = (tau[yy]*lambdaOverMu + a)/fact;
  }

  //    for oldroydb (le2 infinity)

  static void fromAtoTau(T A[3], T tau[3], T muP, T lambda)
  {
    using namespace util::tensorIndices2D;
    enum {x, y};

    T factor = muP / lambda;
    tau[xx] =  factor * (A[xx] - (T)1);
    tau[xy] =  factor *  A[xy];
    tau[yy] =  factor * (A[yy] - (T)1);
  }

  static void fromAtoTau3D(T A[6], T tau[6], T muP, T lambda)
  {
    using namespace util::tensorIndices3D;

    T factor = muP / lambda;
    tau[xx] =  factor * (A[xx] - (T)1);
    tau[xy] =  factor *  A[xy];
    tau[xz] =  factor *  A[xz];
    tau[yy] =  factor * (A[yy] - (T)1);
    tau[yz] =  factor *  A[yz];
    tau[zz] =  factor * (A[zz] - (T)1);
  }

  static void fromTauToA(T tau[3], T A[3], T muP, T lambda)
  {
    using namespace util::tensorIndices2D;

    const T lambdaOverMu = lambda/muP;

    A[xx] = tau[xx]*lambdaOverMu + (T)1;

    A[xy] = tau[xy]*lambdaOverMu;

    A[yy] = tau[yy]*lambdaOverMu + (T)1;
  }

  static void fromTauToA3D(T tau[6], T A[6], T muP, T lambda)
  {
    using namespace util::tensorIndices3D;

    const T lambdaOverMu = lambda/muP;

    A[xx] = tau[xx]*lambdaOverMu + (T)1;

    A[xy] = tau[xy]*lambdaOverMu;

    A[xz] = tau[xz]*lambdaOverMu;

    A[yy] = tau[yy]*lambdaOverMu + (T)1;

    A[yz] = tau[yz]*lambdaOverMu;

    A[zz] = tau[zz]*lambdaOverMu + (T)1;
  }
}; //struct viscoHelpers

}  // namespace olb


#endif
