/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- header file
 */
#ifndef VISCOELASTIC_LATTICE_DESCRIPTORS_H
#define VISCOELASTIC_LATTICE_DESCRIPTORS_H

#include "core/latticeDescriptors.h"

#include <vector>
#include "core/olbDebug.h"

namespace olb {

/// Descriptors for the 2D and 3D lattices.
/** \warning Attention: The lattice directions must always be ordered in
 * such a way that c[i] = -c[i+(q-1)/2] for i=1..(q-1)/2, and c[0] = 0 must
 * be the rest velocity. Furthermore, the velocities c[i] for i=1..(q-1)/2
 * must verify
 *  - in 2D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *  - in 3D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *                       || (c[i][0]==0 && c[i][1]==0 && c[i][2]<0)
 * Otherwise some of the code will work erroneously, because the
 * aformentioned relations are taken as given to enable a few
 * optimizations.
*/
namespace descriptors {

struct TensorExternalField2d {
  static const int numScalars = 3;
  static const int numSpecies = 1;
  static const int tensorBeginsAt = 0;
  static const int sizeOfTensor   = 3;
};

struct TensorExternalField2dBase {
  typedef TensorExternalField2d ExternalField;
};

struct TensorExternalField3d {
  static const int numScalars = 6;
  static const int numSpecies = 1;
  static const int tensorBeginsAt = 0;
  static const int sizeOfTensor   = 6;
};

struct TensorExternalField3dBase {
  typedef TensorExternalField3d ExternalField;
};

struct Visco2dDescriptor {
  static const int numScalars = 4;
  static const int numSpecies = 2;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 2;
  static const int velocityBeginsAt = 2;
  static const int sizeOfVel   = 2;
};

struct Visco2dDescriptorBase {
  typedef Visco2dDescriptor ExternalField;
};

template <typename T> struct ViscoD2Q9Descriptor
  : public D2Q9DescriptorBase<T>, public Visco2dDescriptorBase {
};

template <typename T> struct TensorD2Q9Descriptor
  : public D2Q9DescriptorBase<T>, public TensorExternalField2dBase {
};

template <typename T> struct TensorD3Q19Descriptor
  : public D3Q19DescriptorBase<T>, public TensorExternalField3dBase {
};

}  // namespace descriptors

}  // namespace olb

#endif
