/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef OLDROYD_B_PROCESSOR_2D_H
#define OLDROYD_B_PROCESSOR_2D_H

#include "core/spatiallyExtendedObject2D.h"
#include "core/postProcessing.h"
#include "core/blockLattice2D.h"
#include <cmath>


namespace olb {

/**
* Class for the coupling between a Navier-Stokes (NS) lattice and an
* Advection-Diffusion (AD) lattice.
*/

//======================================================================
// First order explicit time scheme and second order centered spatial
// finite difference scheme for the oldroydb constitutive equation for
// the bulk.
//======================================================================
template<typename T, template<typename U> class Lattice>
class ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 1;
  }
  virtual int extent(int whichDirection) const
  {
    return 1;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }
private:
  int x0, x1, y0, y1;
  T lambda, muP;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice>
class ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP;
};

//===================================================================
//=== Force coupling with the solvent LBM in the bulk. F=div tau. ===
//===================================================================

template<typename T, template<typename U> class Lattice>
class ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 1;
  }
  virtual int extent(int whichDirection) const
  {
    return 1;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }
private:
  int x0, x1, y0, y1;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice>
class ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

};

//======================================================================
// First order explicit time scheme and second order centered spatial
// finite difference scheme for the oldroydb constitutive equation for
// flat walls.
//======================================================================
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }

private:
  template<int deriveDirection>
  void interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                            T velDeriv[Lattice<T>::d], int iX, int iY) const;

  template<int deriveDirection>
  void interpolateGradients (
    TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n> const &tau,
    T tauDeriv[util::TensorVal<Lattice<T> >::n], int iX, int iY ) const;
private:
  int x0, x1, y0, y1;
  T lambda, muP;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP;
};

//===================================================================
// Force coupling with the solvent LBM for flat walls. F=div tau.
//===================================================================
template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }

private:
  template<int deriveDirection>
  void interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                            T velDeriv[Lattice<T>::d], int iX, int iY) const;

  template<int deriveDirection>
  void interpolateGradients (
    TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n> const &tau,
    T tauDeriv[util::TensorVal<Lattice<T> >::n], int iX, int iY ) const;
private:
  int x0, x1, y0, y1;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation>
class ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;
};

//======================================================================
// First order explicit time scheme and second order centered spatial
// finite difference scheme for the oldroydb constitutive equation for
// corners.
//======================================================================
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D(int x_, int y_,
      T lambda_, T muP_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }

private:
  int x, y;
  T lambda, muP;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D(int x_, int y_,
      T lambda_, T muP_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

private:
  T lambda, muP;
};


//===================================================================
//=== Force coupling with the solvent LBM for corners. F=div tau. ===
//===================================================================
template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D(int x_, int y_,
      std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 2;
  }
  virtual int extent(int whichDirection) const
  {
    return 2;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }

private:
  int x, y;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
class ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D(int x_, int y_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;
};

//===================================================================
//=== At the end of the solver we must copy tau_t in tau_tm1 ========
//===================================================================

template<typename T, template<typename U> class Lattice>
class TauCopyCouplingPostProcessor2D : public LocalPostProcessor2D<T,Lattice> {
public:
  TauCopyCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
                                 std::vector<SpatiallyExtendedObject2D* > partners_);
  virtual int extent() const
  {
    return 0;
  }
  virtual int extent(int whichDirection) const
  {
    return 0;
  }
  virtual void process(BlockLattice2D<T,Lattice>& blockLattice);
  virtual void processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                                int x0_, int x1_, int y0_, int y1_);
  virtual bool hasReductions() const
  {
    return false;
  }
  virtual void subscribeReductions(BlockLattice2D<T,Lattice>& blockLattice,
                                   Reductor<T>* reductor)
  { }
private:
  int x0, x1, y0, y1;

  std::vector<SpatiallyExtendedObject2D*> partners;
};

template<typename T, template<typename U> class Lattice>
class TauCopyCouplingGenerator2D : public LatticeCouplingGenerator2D<T,Lattice> {
public:
  TauCopyCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_);
  virtual PostProcessor2D<T,Lattice>* generate(std::vector<SpatiallyExtendedObject2D* > partners) const;
  virtual LatticeCouplingGenerator2D<T,Lattice>* clone() const;

};


}

#endif
