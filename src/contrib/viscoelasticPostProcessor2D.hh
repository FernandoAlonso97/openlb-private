/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Orestis Malaspinas
 *  Address: EPFL-STI-LIN Station 9, 1015 Lausanne
 *  E-mail: orestis@lbmethod.org
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef OLDROYD_B_PROCESSOR_2D_HH
#define OLDROYD_B_PROCESSOR_2D_HH

#include "core/latticeDescriptors.h"
#include "core/blockLattice2D.h"
#include "core/util.h"
#include "core/finiteDifference2D.h"
#include "viscoFiniteDifference2D.h"
#include "viscoelasticLatticeDescriptors.h"

using namespace std;

namespace olb {

//=====================================================================================
//==============  ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D ===============
//=====================================================================================

template<typename T, template<typename U> class Lattice>
ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D<T,Lattice>::
ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{   }

template<typename T, template<typename U> class Lattice>
void ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;
  enum {x, y};
  enum {forceOffset = Lattice<T>::ExternalField::forceBeginsAt,
        velOffset = Lattice<T>::ExternalField::velocityBeginsAt
       };
  using namespace fd;

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T dx_tau[TensorVal<Lattice<T> >::n], dy_tau[TensorVal<Lattice<T> >::n];
        for (int iTau = 0; iTau < TensorVal<Lattice<T> >::n; ++iTau) {
          dx_tau[iTau] = centralGradient(
                           tau_tm1->get(iX+1,iY)[iTau],
                           tau_tm1->get(iX-1,iY)[iTau]);
          dy_tau[iTau] = centralGradient(
                           tau_tm1->get(iX,iY+1)[iTau],
                           tau_tm1->get(iX,iY-1)[iTau]);

          //                     dx_tau[iTau] = centralGradient(
          //                             tau_tm1->get(iX+2,iY)[iTau],
          //                             tau_tm1->get(iX+1,iY)[iTau],
          //                             tau_tm1->get(iX-1,iY)[iTau],
          //                             tau_tm1->get(iX-2,iY)[iTau]);
          //                     dy_tau[iTau] = centralGradient(
          //                             tau_tm1->get(iX,iY+2)[iTau],
          //                             tau_tm1->get(iX,iY+1)[iTau],
          //                             tau_tm1->get(iX,iY-1)[iTau],
          //                             tau_tm1->get(iX,iY-2)[iTau]);
        }

        T *u = blockLattice.get(iX,iY).getExternal(velOffset);
        //                 T *u_xp2 = blockLattice.get(iX+2,iY).getExternal(velOffset);
        T *u_xp1 = blockLattice.get(iX+1,iY).getExternal(velOffset);
        T *u_xm1 = blockLattice.get(iX-1,iY).getExternal(velOffset);
        //                 T *u_xm2 = blockLattice.get(iX-2,iY).getExternal(velOffset);

        //                 T *u_yp2 = blockLattice.get(iX,iY+2).getExternal(velOffset);
        T *u_yp1 = blockLattice.get(iX,iY+1).getExternal(velOffset);
        T *u_ym1 = blockLattice.get(iX,iY-1).getExternal(velOffset);
        //                 T *u_ym2 = blockLattice.get(iX,iY-2).getExternal(velOffset);

        T d_u[2][2];
        //                 d_u[x][x] = centralGradient(u_xp2[x],u_xp1[x],u_xm1[x],u_xm2[x]);
        //                 d_u[x][y] = centralGradient(u_xp2[y],u_xp1[y],u_xm1[y],u_xm2[y]);
        //                 d_u[y][x] = centralGradient(u_yp2[x],u_yp1[x],u_ym1[x],u_ym2[x]);
        //                 d_u[y][y] = centralGradient(u_yp2[y],u_yp1[y],u_ym1[y],u_ym2[y]);

        d_u[x][x] = centralGradient(u_xp1[x],u_xm1[x]);
        d_u[x][y] = centralGradient(u_xp1[y],u_xm1[y]);
        d_u[y][x] = centralGradient(u_yp1[x],u_ym1[x]);
        d_u[y][y] = centralGradient(u_yp1[y],u_ym1[y]);

        // Computation of the source term
        T S[TensorVal<Lattice<T> >::n];
        int iPi = 0;
        for (int iA = 0; iA<L::d; ++iA) {
          for (int iB = iA; iB<L::d; ++iB) {
            S[iPi] = 0.5 * (d_u[iA][iB] + d_u[iB][iA]);
            ++iPi;
          }
        }

        T gradUtau[TensorVal<Lattice<T> >::n]; // the (grad u)^T.tau+tau.(grad u)
        gradUtau[xx] = (T)2*(d_u[x][x]*tau_tm1->get(iX,iY)[xx] +
                             d_u[y][x]*tau_tm1->get(iX,iY)[xy]);
        gradUtau[xy] = tau_tm1->get(iX,iY)[xy]*(d_u[x][x] + d_u[y][y])
                       + tau_tm1->get(iX,iY)[xx]*d_u[x][y] + tau_tm1->get(iX,iY)[yy]*d_u[y][x];
        gradUtau[yy] = (T)2*(d_u[y][y]*tau_tm1->get(iX,iY)[yy] +
                             d_u[x][y]*tau_tm1->get(iX,iY)[xy]);

        for (int iTau = 0; iTau < TensorVal<Lattice<T> >::n; ++iTau) {
          T tau_tp1 = ((T)1-(T)1/lambda)*tau_tm1->get(iX,iY)[iTau];

          T uDivTau = u[x]*dx_tau[iTau] +
                      u[y]*dy_tau[iTau];

          //                     std::cout << iX << " " << iY << " " << uDivTau << std::endl;

          tau_tp1 += (
                       -uDivTau + gradUtau[iTau] + (T)2*muP*S[iTau]/lambda
                     );

          tau_t->get(iX,iY)[iTau] = tau_tp1;
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D<T,Lattice>::
ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D<T,Lattice>::clone() const
{
  return new ExplicitEulerBulkOldroydBTauSolverCouplingGenerator2D<T,Lattice>(*this);
}


// ===================================================================================
// ==========================Bulk oldroyd B force updater=============================
// ===================================================================================

template<typename T, template<typename U> class Lattice>
ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice>::
ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_), partners(partners_)
{   }

template<typename T, template<typename U> class Lattice>
void ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;
  enum {x, y};
  enum {forceOffset = Lattice<T>::ExternalField::forceBeginsAt,
        velOffset = Lattice<T>::ExternalField::velocityBeginsAt
       };
  using namespace fd;

  //     TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
  //             dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *force = blockLattice.get(iX,iY).getExternal(forceOffset);

        T divTauX = centralGradient(tau_t->get(iX+1,iY)[xx],tau_t->get(iX-1,iY)[xx])
                    + centralGradient(tau_t->get(iX,iY+1)[xy],tau_t->get(iX,iY-1)[xy]);
        T divTauY = centralGradient(tau_t->get(iX+1,iY)[xy],tau_t->get(iX-1,iY)[xy])
                    + centralGradient(tau_t->get(iX,iY+1)[yy],tau_t->get(iX,iY-1)[yy]);

        force[0] = divTauX;
        force[1] = divTauY;

        T *u = blockLattice.get(iX,iY).getExternal(velOffset);
        blockLattice.get(iX,iY).computeU(u);
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D<T,Lattice>::
ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1, partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D<T,Lattice>::clone() const
{
  return new ExplicitEulerBulkOldroydBForceUpdaterCouplingGenerator2D<T,Lattice>(*this);
}


//=====================================================================================
//==============  ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D for pflat walls ====
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, direction, orientation>::
ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),
     lambda(lambda_), muP(muP_), partners(partners_)
{   }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, direction, orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;
  enum {x, y};
  enum {forceOffset = Lattice<T>::ExternalField::forceBeginsAt,
        velOffset = Lattice<T>::ExternalField::velocityBeginsAt
       };
  using namespace fd;

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {

        T *u = blockLattice.get(iX,iY).getExternal(velOffset); //velocity at time t

        T dx_u[L::d], dy_u[L::d];

        interpolateGradients<0>(blockLattice,dx_u, iX, iY);
        interpolateGradients<1>(blockLattice,dy_u, iX, iY);

        // Computation of the source term
        T S[TensorVal<Lattice<T> >::n];
        S[xx] = dx_u[x];
        S[yy] = dy_u[y];
        S[xy] = 0.5*(dx_u[y]+dy_u[x]);

        T gradUtau[TensorVal<Lattice<T> >::n]; // the (grad u)^T.tau+tau.(grad u)
        gradUtau[xx] = (T)2*(dx_u[x]*tau_tm1->get(iX,iY)[xx] +
                             dy_u[x]*tau_tm1->get(iX,iY)[xy]);
        gradUtau[xy] = tau_tm1->get(iX,iY)[xy]*(dx_u[x] + dy_u[y])
                       + tau_tm1->get(iX,iY)[xx]*dx_u[y] + tau_tm1->get(iX,iY)[yy]*dy_u[x];
        gradUtau[yy] = (T)2*(dy_u[y]*tau_tm1->get(iX,iY)[yy] +
                             dx_u[y]*tau_tm1->get(iX,iY)[xy]);

        T dx_tau[TensorVal<Lattice<T> >::n], dy_tau[TensorVal<Lattice<T> >::n];
        interpolateGradients<0>(*tau_tm1, dx_tau, iX, iY);
        interpolateGradients<1>(*tau_tm1, dy_tau, iX, iY);

        for (int iTau = 0; iTau < TensorVal<Lattice<T> >::n; ++iTau) {
          T tau_tp1 = ((T)1-(T)1/lambda)*tau_tm1->get(iX,iY)[iTau];

          T uDivTau = u[x]*dx_tau[iTau] +
                      u[y]*dy_tau[iTau];

          tau_tp1 += (
                       -uDivTau + gradUtau[iTau] + (T)2*muP*S[iTau]/lambda
                     );

          tau_t->get(iX,iY)[iTau] = tau_tp1;
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, direction, orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::ViscoDirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector2nd(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
template<int deriveDirection>
void ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n> const& tau,
                     T tauDeriv[util::TensorVal<Lattice<T> >::n],
                     int iX, int iY) const
{
  fd::ViscoDirectedGradients2D<T,Lattice,direction,orientation,direction==deriveDirection>::
  interpolateTensor2nd(tauDeriv, tau, iX, iY);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction,int orientation>
ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, direction, orientation>::
ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor2D<T,Lattice>*
ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, direction, orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, direction, orientation>(
           this->x0,this->x1,this->y0,this->y1, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
LatticeCouplingGenerator2D<T,Lattice>*
ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, direction, orientation>::clone() const
{
  return new ExplicitEulerFlatBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, direction, orientation>(*this);
}

//=====================================================================================
//============  ExplicitEulerOldroydBForceUpdateCouplingPostProcessor2D for pflat walls ====
//=====================================================================================

template<typename T, template<typename U> class Lattice, int direction, int orientation>
ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, direction, orientation>::
ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_),partners(partners_)
{   }

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, direction, orientation>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;
  enum {x, y};
  enum {forceOffset = Lattice<T>::ExternalField::forceBeginsAt,
        velOffset = Lattice<T>::ExternalField::velocityBeginsAt
       };
  using namespace fd;

  //     TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
  //             dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        T *force = blockLattice.get(iX,iY).getExternal(forceOffset);

        T dx_tau[TensorVal<Lattice<T> >::n], dy_tau[TensorVal<Lattice<T> >::n];
        interpolateGradients<0>(*tau_t,dx_tau, iX, iY);
        interpolateGradients<1>(*tau_t,dy_tau, iX, iY);


        T divTauX = dx_tau[xx]+dy_tau[xy];
        T divTauY = dx_tau[xy]+dy_tau[yy];

        force[0] = divTauX;
        force[1] = divTauY;

        T *u = blockLattice.get(iX,iY).getExternal(velOffset); //velocity at time t+1
        blockLattice.get(iX,iY).computeU(u);                   // set
      }
    }
  }
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
void ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, direction, orientation>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

template<typename T, template<typename U> class Lattice, int direction, int orientation>
template<int deriveDirection>
void ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(BlockLattice2D<T,Lattice> const& blockLattice,
                     T velDeriv[Lattice<T>::d], int iX, int iY) const
{
  fd::ViscoDirectedGradients2D<T, Lattice, direction, orientation, direction==deriveDirection>::
  interpolateVector2nd(velDeriv, blockLattice, iX, iY);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
template<int deriveDirection>
void ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice,direction,orientation>::
interpolateGradients(TensorFieldBase2D<T,util::TensorVal<Lattice<T> >::n> const& tau,
                     T tauDeriv[util::TensorVal<Lattice<T> >::n],
                     int iX, int iY) const
{
  fd::ViscoDirectedGradients2D<T,Lattice,direction,orientation,direction==deriveDirection>::
  interpolateTensor2nd(tauDeriv, tau, iX, iY);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int direction,int orientation>
ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, direction, orientation>::
ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_)
{ }

template<typename T, template<typename U> class Lattice, int direction,int orientation>
PostProcessor2D<T,Lattice>*
ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, direction, orientation>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, direction, orientation>(
           this->x0,this->x1,this->y0,this->y1,partners);
}

template<typename T, template<typename U> class Lattice, int direction,int orientation>
LatticeCouplingGenerator2D<T,Lattice>*
ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, direction, orientation>::clone() const
{
  return new ExplicitEulerFlatBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, direction, orientation>(*this);
}

//=====================================================================================
//==============  ExplicitEulerBulkOldroydBTauSolverCouplingPostProcessor2D for corners =========================
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>::
ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D(int x_, int y_,
    T lambda_, T muP_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_),
     lambda(lambda_), muP(muP_), partners(partners_)
{   }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;
  enum {xCoord, yCoord};
  enum {forceOffset = Lattice<T>::ExternalField::forceBeginsAt,
        velOffset = Lattice<T>::ExternalField::velocityBeginsAt
       };
  using namespace fd;

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  T dx_tau[TensorVal<Lattice<T> >::n], dy_tau[TensorVal<Lattice<T> >::n];

  ViscoDirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateTensor2nd(dx_tau,*tau_tm1,x,y);
  ViscoDirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateTensor2nd(dy_tau,*tau_tm1,x,y);

  T *u = blockLattice.get(x,y).getExternal(velOffset); // we need the value of u at time t

  T dx_u[L::d], dy_u[L::d];

  fd::ViscoDirectedGradients2D<T, Lattice, 0, xNormal, true>::interpolateVector2nd(dx_u, blockLattice, x,y);
  fd::ViscoDirectedGradients2D<T, Lattice, 1, yNormal, true>::interpolateVector2nd(dy_u, blockLattice, x,y);

  // Computation of the source term
  T S[TensorVal<Lattice<T> >::n];
  S[xx] = dx_u[xCoord];
  S[yy] = dy_u[yCoord];
  S[xy] = 0.5*(dx_u[yCoord]+dy_u[xCoord]);

  T gradUtau[TensorVal<Lattice<T> >::n]; // the (grad u)^T.tau+tau.(grad u)
  gradUtau[xx] = (T)2*(dx_u[xCoord]*tau_tm1->get(x,y)[xx] +
                       dy_u[xCoord]*tau_tm1->get(x,y)[xy]);
  gradUtau[xy] = tau_tm1->get(x,y)[xy]*(dx_u[xCoord] + dy_u[yCoord])
                 + tau_tm1->get(x,y)[xx]*dx_u[yCoord] + tau_tm1->get(x,y)[yy]*dy_u[xCoord];
  gradUtau[yy] = (T)2*(dy_u[yCoord]*tau_tm1->get(x,y)[yy] +
                       dx_u[yCoord]*tau_tm1->get(x,y)[xy]);

  for (int iTau = 0; iTau < TensorVal<Lattice<T> >::n; ++iTau) {
    T tau_tp1 = ((T)1-(T)1/lambda)*tau_tm1->get(x,y)[iTau];

    T uDivTau = u[xCoord]*dx_tau[iTau] +
                u[yCoord]*dy_tau[iTau];

    tau_tp1 += (
                 -uDivTau + gradUtau[iTau] + (T)2*muP*S[iTau]/lambda
               );

    tau_t->get(x,y)[iTau] = tau_tp1;
  }
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, xNormal, yNormal>::
ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D(int x_, int y_,
    T lambda_, T muP_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_),
    lambda(lambda_), muP(muP_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>*
ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, xNormal, yNormal>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>(
           this->x0, this->y0, lambda, muP,partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>*
ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, xNormal, yNormal>::clone() const
{
  return new ExplicitEulerCornerBoundaryOldroydBTauSolverCouplingGenerator2D<T,Lattice, xNormal, yNormal>(*this);
}

//=====================================================================================
//==============  ExplicitEulerBulkOldroydBForceUpdaterCouplingPostProcessor2D for corners =====
//=====================================================================================

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>::
ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D(int x_, int y_,
    std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x(x_), y(y_), partners(partners_)
{   }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  if (util::contained(x, y, x0_, x1_, y0_, y1_)) {
    process(blockLattice);
  }
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
void ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;
  enum {forceOffset = Lattice<T>::ExternalField::forceBeginsAt,
        velOffset = Lattice<T>::ExternalField::velocityBeginsAt
       };
  using namespace fd;

  //     TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
  //             dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  T dx_tau[TensorVal<Lattice<T> >::n], dy_tau[TensorVal<Lattice<T> >::n];

  T *force = blockLattice.get(x,y).getExternal(forceOffset);
  T *u = blockLattice.get(x,y).getExternal(velOffset); // we need the value of u at time t

  ViscoDirectedGradients2D<T, Lattice, 0, xNormal, true>::
  interpolateTensor2nd(dx_tau,*tau_t,x,y);
  ViscoDirectedGradients2D<T, Lattice, 1, yNormal, true>::
  interpolateTensor2nd(dy_tau,*tau_t,x,y);

  T divTauX = dx_tau[xx]+dy_tau[xy];
  T divTauY = dx_tau[xy]+dy_tau[yy];

  force[0] = divTauX;
  force[1] = divTauY;

  blockLattice.get(x,y).computeU(u); // sets the value of u to its value
  // at time t+1
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice, int xNormal,int yNormal>
ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, xNormal, yNormal>::
ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D(int x_, int y_)
  : LatticeCouplingGenerator2D<T,Lattice>(x_, x_, y_, y_)
{ }

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
PostProcessor2D<T,Lattice>*
ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, xNormal, yNormal>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingPostProcessor2D<T,Lattice, xNormal, yNormal>(
           this->x0, this->y0, partners);
}

template<typename T, template<typename U> class Lattice, int xNormal, int yNormal>
LatticeCouplingGenerator2D<T,Lattice>*
ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, xNormal, yNormal>::clone() const
{
  return new ExplicitEulerCornerBoundaryOldroydBForceUpdaterCouplingGenerator2D<T,Lattice, xNormal, yNormal>(*this);
}


// ===================================================================================
// ==========================Copy tau_t in tau_tm1 ===================================
// ===================================================================================

template<typename T, template<typename U> class Lattice>
TauCopyCouplingPostProcessor2D<T,Lattice>::
TauCopyCouplingPostProcessor2D(int x0_, int x1_, int y0_, int y1_,
                               std::vector<SpatiallyExtendedObject2D* > partners_)
  :  x0(x0_), x1(x1_), y0(y0_), y1(y1_), partners(partners_)
{   }

template<typename T, template<typename U> class Lattice>
void TauCopyCouplingPostProcessor2D<T,Lattice>::
processSubDomain(BlockLattice2D<T,Lattice>& blockLattice,
                 int x0_, int x1_, int y0_, int y1_)
{
  typedef Lattice<T> L;
  using namespace util;
  using namespace util::tensorIndices2D;

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_tm1 =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[0]);

  TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *tau_t =
    dynamic_cast<TensorFieldBase2D<T,TensorVal<Lattice<T> >::n> *>(partners[1]);

  int newX0, newX1, newY0, newY1;
  if ( util::intersect (
         x0, x1, y0, y1,
         x0_, x1_, y0_, y1_,
         newX0, newX1, newY0, newY1 ) ) {
    for (int iX=newX0; iX<=newX1; ++iX) {
      for (int iY=newY0; iY<=newY1; ++iY) {
        for (int iTau = 0; iTau < TensorVal<Lattice<T> >::n; ++iTau) {
          tau_tm1->get(iX,iY)[iTau] = tau_t->get(iX,iY)[iTau];
        }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void TauCopyCouplingPostProcessor2D<T,Lattice>::
process(BlockLattice2D<T,Lattice>& blockLattice)
{
  processSubDomain(blockLattice, x0, x1, y0, y1);
}

/// LatticeCouplingGenerator for advectionDiffusion coupling

template<typename T, template<typename U> class Lattice>
TauCopyCouplingGenerator2D<T,Lattice>::
TauCopyCouplingGenerator2D(int x0_, int x1_, int y0_, int y1_)
  : LatticeCouplingGenerator2D<T,Lattice>(x0_, x1_, y0_, y1_)
{ }

template<typename T, template<typename U> class Lattice>
PostProcessor2D<T,Lattice>* TauCopyCouplingGenerator2D<T,Lattice>::generate (
  std::vector<SpatiallyExtendedObject2D* > partners) const
{
  return new TauCopyCouplingPostProcessor2D<T,Lattice>(
           this->x0,this->x1,this->y0,this->y1, partners);
}

template<typename T, template<typename U> class Lattice>
LatticeCouplingGenerator2D<T,Lattice>* TauCopyCouplingGenerator2D<T,Lattice>::clone() const
{
  return new TauCopyCouplingGenerator2D<T,Lattice>(*this);
}

}  // namespace olb

#endif
