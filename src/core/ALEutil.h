//
// Created by Andreas Reiser on 15.05.19.
//

#ifndef OLB_ALEUTIL_H
#define OLB_ALEUTIL_H

#include "core/config.h"
#include "dynamics/lbHelpers.h"
#include <glob.h>
#include "affineTransform.h"
#include "externalFieldALE.h"
#include "contrib/domainDecomposition/subDomainInformation.h"
#include <type_traits>

namespace olb {
namespace aleutil {

#define olb_min(X, Y)  ((X) < (Y) ? (X) : (Y))
#define olb_max(X, Y)  ((X) > (Y) ? (X) : (Y))  
/**
 * 2D methods
 */

/**
 *
 * @param translation : translation for current time step
 * @param angle       : rotation angle for current time step
 * @param anchor      : lattice anchor
 * @return corresponding Affine Transformation
 */

template <typename T>
Affine2<T> movementTransform(const Vec2<T>& translation, const T angle, const Vec2<T>& anchor) {
  const Mat2<T> rotation(angle);
  Affine2<T> transform(rotation);
  transform.applyToRight(-anchor);
  transform.applyToLeft(anchor);
  transform.applyToLeft(translation);
  return transform;
}

/**
 * Transformation to calculate the position of a vertex (in Lattice coordinates)
 * in the external Field:
 */
template <typename T>
Affine2<T> positionInExternalFieldTransform(const Vec2<T>& position, const T orientation, const Vec2<T>& anchor) {
  const Mat2<T> rotation(-orientation);
  Affine2<T> transform(rotation);
  transform.applyToRight(-anchor);
  transform.applyToLeft(position);
  return transform;
}

/**
 * Transformation to rotate velocity to the correct orientation
 * Consists of rotating it to current lattice orientation + rotation with the movement angles
 */

template <typename T>
Affine2<T> velocityRotationTransform(const T angle, const T orientation) {
  const Mat2<T> orientationRotation(-orientation);
  const Mat2<T> movementRotation(-angle);
  Affine2<T> transform(movementRotation);
  transform.applyToRight(orientationRotation);
  return transform;
}


template <typename T>
OPENLB_HOST_DEVICE
Vec2<T> vertexForCellIndex(const size_t cellIndex, const size_t ny) {
  const size_t y = cellIndex % ny;
  const size_t x = (cellIndex - y) / ny;
  return Vec2<T>(x, y);
}

template <typename T>
OPENLB_HOST_DEVICE
void bilinearInterpolate(T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataDst,
                          size_t cellIndex, size_t iPop,
                          T const q11,
                          T const q21,
                          T const q12,
                          T const q22,
                          T const a, T const b,
                          T const a_1, T const b_1) {

  cellDataDst[iPop][cellIndex] = q11 * a_1 * b_1 +
                                 q21 * a   * b_1 +
                                 q12 * a_1 * b   +
                                 q22 * a   * b;
}


template <typename T, template<typename U> class Lattice, class ExternalField>
OPENLB_HOST_DEVICE
void interpolateCellPopulations2D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataSrc,
                                  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataDst,
                                  const AffineTransform<T, Lattice<T>::d>& transform,
                                  const ExternalField externalField,
                                  const size_t cellIndex,
                                  const size_t nx,
                                  const size_t ny) {

  Vec2<T> vertex = vertexForCellIndex<T>(cellIndex, ny);

  transformInPlace(vertex, transform);

  const int x1 = static_cast<int>(floor(vertex(0)));
  const int y1 = static_cast<int>(floor(vertex(1)));
  const int x2 = x1 + 1;
  const int y2 = y1 + 1;


  const bool x1_inside = x1 >= 0 && x1 < nx;
  const bool y1_inside = y1 >= 0 && y1 < ny;
  const bool x2_inside = x2 >= 0 && x2 < nx;
  const bool y2_inside = y2 >= 0 && y2 < ny;

  const T a = vertex(0) - x1;
  const T b = vertex(1) - y1;
  const T a_1 = 1 - a;
  const T b_1 = 1 - b;

  T q11;
  T q21;
  T q12;
  T q22;

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    if (x1_inside && y1_inside)
      q11 = cellDataSrc[iPop][util::getCellIndex2D(x1, y1, ny)];
    else
      q11 = externalField.populationValueFor(iPop, x1, y1);


    if (x2_inside && y1_inside)
      q21 = cellDataSrc[iPop][util::getCellIndex2D(x2, y1, ny)];
    else
      q21 = externalField.populationValueFor(iPop, x2, y1);


    if (x1_inside && y2_inside)
      q12 = cellDataSrc[iPop][util::getCellIndex2D(x1, y2, ny)];
    else
      q12 = externalField.populationValueFor(iPop, x1, y2);


    if (x2_inside && y2_inside)
      q22 = cellDataSrc[iPop][util::getCellIndex2D(x2, y2, ny)];
    else
      q22 = externalField.populationValueFor(iPop, x2, y2);


    bilinearInterpolate<T>(cellDataDst, cellIndex, iPop,
                            q11, q21, q12, q22, a, b, a_1, b_1);
  }
}



/**
 * 3D methods
 */

/**
 *
 * @param translation : translation for current time step
 * @param angles      : rotation angles for current time step
 * @param anchor      : lattice anchor
 * @return corresponding Affine Transformation
 */

template <typename T>
Affine3<T> movementTransform(const Vec3<T>& translation, const Vec3<T>& angles, const Vec3<T>& anchor) {
  const Mat3<T> rotation(angles, Rotation::ToBody());
  Affine3<T> transform(rotation);
  transform.applyToRight(-anchor);
  transform.applyToLeft(anchor);
  transform.applyToLeft(translation);
  return transform;
}

/**
 * Transformation to calculate the position of a vertex (in Lattice coordinates)
 * in the external Field:
 */
template <typename T>
Affine3<T> positionInExternalFieldTransform(const Vec3<T>& position, const Vec3<T>& orientation, const Vec3<T>& anchor) {
  const Mat3<T> rotation(orientation, Rotation::ToSpace());
  Affine3<T> transform(rotation);
  transform.applyToRight(-anchor);
  transform.applyToLeft(position);
  return transform;
}

/**
 * Transformation to rotate velocity to the correct orientation
 * Consists of rotating it to current lattice orientation + rotation with the movement angles
 */

template <typename T>
Affine3<T> velocityRotationTransform(const Vec3<T>& angles, const Vec3<T>& orientation) {
  const Mat3<T> orientationRotation(orientation, Rotation::ToBody());
  const Mat3<T> movementRotation(angles, Rotation::ToBody());
  Affine3<T> transform(movementRotation);
  transform.applyToRight(orientationRotation);
  return transform;
}

template <typename T>
OPENLB_HOST_DEVICE
Vec3<T> vertexForCellIndex(const size_t cellIndex, const size_t ny, const size_t nz) {
  const size_t z = cellIndex % nz;
  const size_t y = ((cellIndex - z) / nz) % ny;
  const size_t x = (cellIndex - z - y * nz) / (nz * ny);
  return Vec3<T>(x, y, z);
}

template <typename T, typename valueType>
OPENLB_HOST_DEVICE
void trilinearInterpolate(valueType* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataDst,
                          size_t cellIndex, size_t iPop,
                          T const q111,
                          T const q211,
                          T const q121,
                          T const q221,
                          T const q112,
                          T const q212,
                          T const q122,
                          T const q222,
                          T const a, T const b, T const c,
                          T const a_1, T const b_1, T const c_1) {

    cellDataDst[iPop][cellIndex] = q111 * a_1 * b_1 * c_1 +
                                 q211 * a   * b_1 * c_1 +
                                 q121 * a_1 * b   * c_1 +
                                 q221 * a   * b   * c_1 +
                                 q112 * a_1 * b_1 * c   +
                                 q212 * a   * b_1 * c   +
                                 q122 * a_1 * b   * c   +
                                 q222 * a   * b   * c;
}

template <typename T>
OPENLB_HOST_DEVICE
void trilinearInterpolate(T* const OPENLB_RESTRICT cellDataDst,
                          size_t cellIndex, size_t iPop,
                          T const q111,
                          T const q211,
                          T const q121,
                          T const q221,
                          T const q112,
                          T const q212,
                          T const q122,
                          T const q222,
                          T const a, T const b, T const c,
                          T const a_1, T const b_1, T const c_1) {

  cellDataDst[iPop] = q111 * a_1 * b_1 * c_1 +
                                 q211 * a   * b_1 * c_1 +
                                 q121 * a_1 * b   * c_1 +
                                 q221 * a   * b   * c_1 +
                                 q112 * a_1 * b_1 * c   +
                                 q212 * a   * b_1 * c   +
                                 q122 * a_1 * b   * c   +
                                 q222 * a   * b   * c;
}

template <typename valueType, typename floatType>
OPENLB_HOST_DEVICE
floatType trilinearInterpolate(   valueType const v111,
                          valueType const v211,
                          valueType const v121,
                          valueType const v221,
                          valueType const v112,
                          valueType const v212,
                          valueType const v122,
                          valueType const v222,
                          floatType const a, floatType const b, floatType const c,
                          floatType const a_1, floatType const b_1, floatType const c_1) {

  return v111 * a_1 * b_1 * c_1 +
                                 v211 * a   * b_1 * c_1 +
                                 v121 * a_1 * b   * c_1 +
                                 v221 * a   * b   * c_1 +
                                 v112 * a_1 * b_1 * c   +
                                 v212 * a   * b_1 * c   +
                                 v122 * a_1 * b   * c   +
                                 v222 * a   * b   * c;
}

template <typename T, template<typename U> class Lattice, class ExternalField,unsigned boundaryDistance>
OPENLB_HOST_DEVICE
void interpolateFluidMask3D(bool * const OPENLB_RESTRICT fluidMask,
                                  bool * const OPENLB_RESTRICT fluidMaskInitial,
                                  const AffineTransform<T, Lattice<T>::d>& transform,
                                  const ExternalField& externalField,
                                  const size_t cellIndex,
                                  const size_t nx,
                                  const size_t ny,
                                  const size_t nz,
                                  const SubDomainInformation<T,Lattice<T>> subDomInfo 
                                  ) {

  Vec3<T> vertex = vertexForCellIndex<T>(cellIndex, ny, nz);

  if( ((vertex(0) <= (boundaryDistance+subDomInfo.ghostLayer[0])) and !(subDomInfo.hasNeighbour[1]))
      or
      ((vertex(0) >= (nx-(boundaryDistance+1)-subDomInfo.ghostLayer[0])) and !(subDomInfo.hasNeighbour[10]))
      or 
      ((vertex(1) <= (boundaryDistance+subDomInfo.ghostLayer[1])) and !(subDomInfo.hasNeighbour[2]))
      or
      ((vertex(1) >= (ny-(boundaryDistance+1)-subDomInfo.ghostLayer[1])) and !(subDomInfo.hasNeighbour[11]))
      or 
      ((vertex(2) <= (boundaryDistance+subDomInfo.ghostLayer[2])) and !(subDomInfo.hasNeighbour[3]))
      or
      ((vertex(2) >= (nz-(boundaryDistance+1)-subDomInfo.ghostLayer[2])) and !(subDomInfo.hasNeighbour[12]))
    ) 
  {
	  fluidMask[cellIndex] = fluidMaskInitial[cellIndex];
	  return;
  }

  transformInPlace(vertex, transform);

  const int x1 = static_cast<int>(floor(vertex(0)));
  const int y1 = static_cast<int>(floor(vertex(1)));
  const int z1 = static_cast<int>(floor(vertex(2)));
  const int x2 = x1 + 1;
  const int y2 = y1 + 1;
  const int z2 = z1 + 1;

  bool x1_inside = x1 >= 0 && x1 < nx;
  bool y1_inside = y1 >= 0 && y1 < ny;
  bool z1_inside = z1 >= 0 && z1 < nz;
  bool x2_inside = x2 >= 0 && x2 < nx;
  bool y2_inside = y2 >= 0 && y2 < ny;
  bool z2_inside = z2 >= 0 && z2 < nz;

  T a = vertex(0) - x1;
  T b = vertex(1) - y1;
  T c = vertex(2) - z1;
  T a_1 = 1 - a;
  T b_1 = 1 - b;
  T c_1 = 1 - c;

  T f111;
  T f211;
  T f121;
  T f221;
  T f112;
  T f212;
  T f122;
  T f222;

  if(x1_inside && y1_inside && z1_inside)
	  f111 = static_cast<T>(externalField.fluidmaskAt(x1, y1, z1) and fluidMaskInitial[util::getCellIndex3D(x1, y1, z1, ny, nz)]);
  else
	  f111 = static_cast<T>(externalField.fluidmaskAt(x1, y1, z1));

  if(x2_inside && y1_inside && z1_inside)
	  f211 = static_cast<T>(externalField.fluidmaskAt(x2, y1, z1) and fluidMaskInitial[util::getCellIndex3D(x2, y1, z1, ny, nz)]);
  else
	  f211 = static_cast<T>(externalField.fluidmaskAt(x2, y1, z1));

  if(x1_inside && y2_inside && z1_inside)
	  f121 = static_cast<T>(externalField.fluidmaskAt(x1, y2, z1) and fluidMaskInitial[util::getCellIndex3D(x1, y2, z1, ny, nz)]);
  else
	  f121 = static_cast<T>(externalField.fluidmaskAt(x1, y2, z1));

  if(x2_inside && y2_inside && z1_inside)
	  f221 = static_cast<T>(externalField.fluidmaskAt(x2, y2, z1) and fluidMaskInitial[util::getCellIndex3D(x2, y2, z1, ny, nz)]);
  else
	  f221 = static_cast<T>(externalField.fluidmaskAt(x2, y2, z1));

  if(x1_inside && y1_inside && z2_inside)
	  f112 = static_cast<T>(externalField.fluidmaskAt(x1, y1, z2) and fluidMaskInitial[util::getCellIndex3D(x1, y1, z2, ny, nz)]);
  else
	  f112 = static_cast<T>(externalField.fluidmaskAt(x1, y1, z2));

  if(x2_inside && y1_inside && z2_inside)
	  f212 = static_cast<T>(externalField.fluidmaskAt(x2, y1, z2) and fluidMaskInitial[util::getCellIndex3D(x2, y1, z2, ny, nz)]);
  else
	  f212 = static_cast<T>(externalField.fluidmaskAt(x2, y1, z2));

  if(x1_inside && y2_inside && z2_inside)
	  f122 = static_cast<T>(externalField.fluidmaskAt(x1, y2, z2) and fluidMaskInitial[util::getCellIndex3D(x1, y2, z2, ny, nz)]);
  else
	  f122 = static_cast<T>(externalField.fluidmaskAt(x1, y2, z2));

  if(x2_inside && y2_inside && z2_inside)
	  f222 = static_cast<T>(externalField.fluidmaskAt(x2, y2, z2) and fluidMaskInitial[util::getCellIndex3D(x2, y2, z2, ny, nz)]);
  else
	  f222 = static_cast<T>(externalField.fluidmaskAt(x2, y2, z2));

  if(trilinearInterpolate<T,T>(f111, f211, f121, f221, f112, f212, f122, f222,
	a, b, c, a_1, b_1, c_1) < 0.5)
	  fluidMask[cellIndex] = false;
  else
	  fluidMask[cellIndex] = fluidMaskInitial[cellIndex];

}
/////////////////////////////////////////////////////////////////////////////////////////////
template <typename T, template<typename U> class Lattice, class ExternalField>
OPENLB_HOST_DEVICE
void interpolateCellPopulations3D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataSrc,
                                  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataDst,
                                  const AffineTransform<T, Lattice<T>::d>& transform,
                                  const ExternalField& externalField,
                                  const size_t cellIndex,
                                  const size_t nx,
                                  const size_t ny,
                                  const size_t nz,
                                  const SubDomainInformation<T,Lattice<T>> subDomInfo 
                                  ) {

  Vec3<T> vertex = vertexForCellIndex<T>(cellIndex, ny, nz);

  transformInPlace(vertex, transform);

  const int x1 = static_cast<int>(floor(vertex(0)));
  const int y1 = static_cast<int>(floor(vertex(1)));
  const int z1 = static_cast<int>(floor(vertex(2)));
  const int x2 = x1 + 1;
  const int y2 = y1 + 1;
  const int z2 = z1 + 1;
  
  Index3D validGridStart = subDomInfo.validGridStart();
  Index3D validGridEnd = subDomInfo.validGridEnd();

  bool x1_inside = x1 >= validGridStart[0] && x1 < validGridEnd[0];
  bool y1_inside = y1 >= validGridStart[1] && y1 < validGridEnd[1];
  bool z1_inside = z1 >= validGridStart[2] && z1 < validGridEnd[2];
  bool x2_inside = x2 >= validGridStart[0] && x2 < validGridEnd[0];
  bool y2_inside = y2 >= validGridStart[1] && y2 < validGridEnd[1];
  bool z2_inside = z2 >= validGridStart[2] && z2 < validGridEnd[2];

  // printf("%d %d %d %d %d %d # %i %i %i # %i %i %i # %i %i %i\n", x1_inside,y1_inside,z1_inside,x2_inside,y2_inside,z2_inside,x1,y1,z1,subDomInfo.validGridStart()[0],validGridStart[1],validGridStart[2],validGridEnd[0],validGridEnd[1],validGridEnd[2]);

  T a = vertex(0) - x1;
  T b = vertex(1) - y1;
  T c = vertex(2) - z1;
  T a_1 = 1 - a;
  T b_1 = 1 - b;
  T c_1 = 1 - c;

  if(transform(0,Lattice<T>::d) > 0)
  {
      if(x2==nx or x1==0)
      {
          x2_inside = false;
          a_1 = olb_min(a_1,0.999);
          a = 1-a_1;
      }
  }
  else if(transform(0,Lattice<T>::d) < 0)
  {
      if(x1==-1 or x2==nx-1)
      {
          x1_inside = false;
          a_1 = olb_max(a_1,0.001);
          a=1-a_1;
      }
  }
  else if(transform(0,Lattice<T>::d) == 0)
  {
      if(x2==nx or x1==0)
      {
          x2_inside = false;
          a_1 = 0.999;
          a = 1-a_1;
      }
  }

  if(transform(1,Lattice<T>::d) > 0)
  {
      if(y2==ny or y1==0)
      {
          y2_inside = false;
          b_1 = olb_min(b_1,0.999);
          b = 1-b_1;
      }
  }
  else if(transform(1,Lattice<T>::d) < 0)
  {
      if(y1==-1 or y2==ny-1)
      {
          y1_inside = false;
          b = olb_min(b,0.999);
          b_1=1-b;
      }
  }
  else if(transform(1,Lattice<T>::d) == 0)
  {
      if(y2==ny or y1==0)
      {
          y2_inside = false;
          b_1 = 0.999;
          b = 1-b_1;
      }
  }

  T q111;
  T q211;
  T q121;
  T q221;
  T q112;
  T q212;
  T q122;
  T q222;

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    if (x1_inside && y1_inside && z1_inside)
      q111 = cellDataSrc[iPop][util::getCellIndex3D(x1, y1, z1, ny, nz)];
    else
    {
      q111 = externalField.populationValueFor(iPop, x1, y1, z1);
    }

    if (x2_inside && y1_inside && z1_inside)
      q211 = cellDataSrc[iPop][util::getCellIndex3D(x2, y1, z1, ny, nz)];
    else
    {
      q211 = externalField.populationValueFor(iPop, x2, y1, z1);
    }


    if (x1_inside && y2_inside && z1_inside)
      q121 = cellDataSrc[iPop][util::getCellIndex3D(x1, y2, z1, ny, nz)];
    else
    {
      q121 = externalField.populationValueFor(iPop, x1, y2, z1);
    }


    if (x2_inside && y2_inside && z1_inside)
      q221 = cellDataSrc[iPop][util::getCellIndex3D(x2, y2, z1, ny, nz)];
    else
    {
      q221 = externalField.populationValueFor(iPop, x2, y2, z1);
    }



    if (x1_inside && y1_inside && z2_inside)
      q112 = cellDataSrc[iPop][util::getCellIndex3D(x1, y1, z2, ny, nz)];
    else
    {
      q112 = externalField.populationValueFor(iPop, x1, y1, z2);
    }


    if (x2_inside && y1_inside && z2_inside)
      q212 = cellDataSrc[iPop][util::getCellIndex3D(x2, y1, z2, ny, nz)];
    else
    {
      q212 = externalField.populationValueFor(iPop, x2, y1, z2);
    }


    if (x1_inside && y2_inside && z2_inside)
      q122 = cellDataSrc[iPop][util::getCellIndex3D(x1, y2, z2, ny, nz)];
    else
    {
      q122 = externalField.populationValueFor(iPop, x1, y2, z2);
    }


    if (x2_inside && y2_inside && z2_inside)
      q222 = cellDataSrc[iPop][util::getCellIndex3D(x2, y2, z2, ny, nz)];
    else
    {
      q222 = externalField.populationValueFor(iPop, x2, y2, z2);
    }

    trilinearInterpolate<T>(cellDataDst, cellIndex, iPop,
        q111, q211, q121, q221, q112, q212, q122, q222,
        a, b, c, a_1, b_1, c_1);
  }

  T rho;
  T u[Lattice<T>::d];
  lbHelpers<T,Lattice>::computeRhoU(cellDataDst,cellIndex,rho,u);
  cellDataDst[Lattice<T>::rhoIndex][cellIndex] = rho;
  for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
    cellDataDst[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];
}

template <typename T, template<typename U> class Lattice, class ExternalField>
OPENLB_HOST_DEVICE
void interpolateCellPopulations3D(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataSrc,
                                  T * const OPENLB_RESTRICT cellDataDst,
                                  const AffineTransform<T, Lattice<T>::d>& transform,
                                  const ExternalField externalField,
                                  const size_t cellIndex,
                                  const size_t nx,
                                  const size_t ny,
                                  const size_t nz,
                                  bool* fluidMask,
                                  bool* staticFluidMask) {

  Vec3<T> vertex = vertexForCellIndex<T>(cellIndex, ny, nz);

  transformInPlace(vertex, transform);

  const int x1 = static_cast<int>(floor(vertex(0)));
  const int y1 = static_cast<int>(floor(vertex(1)));
  const int z1 = static_cast<int>(floor(vertex(2)));
  const int x2 = x1 + 1;
  const int y2 = y1 + 1;
  const int z2 = z1 + 1;

  const bool x1_inside = x1 >= 0 && x1 < nx;
  const bool y1_inside = y1 >= 0 && y1 < ny;
  const bool z1_inside = z1 >= 0 && z1 < nz;
  const bool x2_inside = x2 >= 0 && x2 < nx;
  const bool y2_inside = y2 >= 0 && y2 < ny;
  const bool z2_inside = z2 >= 0 && z2 < nz;

  const T a = vertex(0) - x1;
  const T b = vertex(1) - y1;
  const T c = vertex(2) - z1;
  const T a_1 = 1 - a;
  const T b_1 = 1 - b;
  const T c_1 = 1 - c;

  T q111;
  T q211;
  T q121;
  T q221;
  T q112;
  T q212;
  T q122;
  T q222;

  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    if (x1_inside && y1_inside && z1_inside)
      q111 = cellDataSrc[iPop][util::getCellIndex3D(x1, y1, z1, ny, nz)];
    else
      q111 = externalField.populationValueFor(iPop, x1, y1, z1);


    if (x2_inside && y1_inside && z1_inside)
      q211 = cellDataSrc[iPop][util::getCellIndex3D(x2, y1, z1, ny, nz)];
    else
      q211 = externalField.populationValueFor(iPop, x2, y1, z1);


    if (x1_inside && y2_inside && z1_inside)
      q121 = cellDataSrc[iPop][util::getCellIndex3D(x1, y2, z1, ny, nz)];
    else
      q121 = externalField.populationValueFor(iPop, x1, y2, z1);


    if (x2_inside && y2_inside && z1_inside)
      q221 = cellDataSrc[iPop][util::getCellIndex3D(x2, y2, z1, ny, nz)];
    else
      q221 = externalField.populationValueFor(iPop, x2, y2, z1);



    if (x1_inside && y1_inside && z2_inside)
      q112 = cellDataSrc[iPop][util::getCellIndex3D(x1, y1, z2, ny, nz)];
    else
      q112 = externalField.populationValueFor(iPop, x1, y1, z2);


    if (x2_inside && y1_inside && z2_inside)
      q212 = cellDataSrc[iPop][util::getCellIndex3D(x2, y1, z2, ny, nz)];
    else
      q212 = externalField.populationValueFor(iPop, x2, y1, z2);


    if (x1_inside && y2_inside && z2_inside)
      q122 = cellDataSrc[iPop][util::getCellIndex3D(x1, y2, z2, ny, nz)];
    else
      q122 = externalField.populationValueFor(iPop, x1, y2, z2);


    if (x2_inside && y2_inside && z2_inside)
      q222 = cellDataSrc[iPop][util::getCellIndex3D(x2, y2, z2, ny, nz)];
    else
      q222 = externalField.populationValueFor(iPop, x2, y2, z2);


    trilinearInterpolate<T>(cellDataDst, cellIndex, iPop,
        q111, q211, q121, q221, q112, q212, q122, q222,
        a, b, c, a_1, b_1, c_1);
  }
}


}
}

#endif // OLB_ALEUTIL_H
