//
// Created by Andreas Reiser on 14.05.19.
//

#ifndef OLB_AFFINETRANSFORM_H
#define OLB_AFFINETRANSFORM_H

#include <iostream>
#include "core/config.h"
#include <math.h>


namespace olb {

struct Rotation {
  struct ToBody {};  // ZYX
  struct ToSpace {}; // XYZ
};

template <typename Scalar, size_t Rows, size_t Cols> struct Matrix {
public:
  Matrix() = default;

  /**
   * 2D vector init
   * @param x
   * @param y
   */
  OPENLB_HOST_DEVICE

  Matrix(Scalar x, Scalar y) {
    static_assert(Rows == 2, "Expected 2 rows.");
    static_assert(Cols == 1, "Expected 1 col.");
    m_data[index(0, 0)] = x;
    m_data[index(1, 0)] = y;
  }
  /**
   * 3D vector init
   * @param x
   * @param y
   * @param z
   */
  OPENLB_HOST_DEVICE
  Matrix(Scalar x, Scalar y, Scalar z) {
    static_assert(Rows == 3, "Expected 3 rows.");
    static_assert(Cols == 1, "Expected 1 col.");
    m_data[index(0, 0)] = x;
    m_data[index(1, 0)] = y;
    m_data[index(2, 0)] = z;
  }

  /** 
   * construct a 2D or 3D symmetric tensor
   * @param tensor indices array
   */
  OPENLB_HOST_DEVICE
  Matrix(Scalar tensorIndices[]) {
    if (Rows == 2) {
      assert(Rows == 2);
      assert(Cols == 2);
      m_data[index(0,0)] = tensorIndices[0];
      m_data[index(1,0)] = tensorIndices[1];
      m_data[index(0,1)] = tensorIndices[1];
      m_data[index(1,1)] = tensorIndices[2];
    }
    else if (Rows == 3) {
      assert(Rows == 3);
      assert(Cols == 3);
      m_data[index(0,0)] = tensorIndices[0];
      m_data[index(1,0)] = tensorIndices[1];
      m_data[index(0,1)] = tensorIndices[1];
      m_data[index(2,0)] = tensorIndices[2];
      m_data[index(0,2)] = tensorIndices[2];
      m_data[index(1,1)] = tensorIndices[3];
      m_data[index(1,2)] = tensorIndices[4];
      m_data[index(2,1)] = tensorIndices[4];
      m_data[index(2,2)] = tensorIndices[5];
    }
    
  }

  /**
   * constructs a 2D rotation matrix
   * @param angle
   */
  explicit Matrix(const Scalar angle) {
    static_assert(Rows == 2, "Expected 2 rows.");
    static_assert(Cols == 2, "Expected 2 cols.");
    m_data[index(0, 0)] = std::cos(angle);
    m_data[index(0, 1)] = -std::sin(angle);
    m_data[index(1, 0)] = std::sin(angle);
    m_data[index(1, 1)] = std::cos(angle);
  }

  /**
   * Constructs a 3D rotation Matrix with ZYX convention
   * @param phi: yaw
   * @param theta: pitch
   * @param psi: roll
   */
  OPENLB_HOST_DEVICE
  Matrix(const Scalar phi, const Scalar theta, const Scalar psi,
         Rotation::ToBody) {
    static_assert(Rows == 3, "Expected 3 rows.");
    static_assert(Cols == 3, "Expected 3 cols.");
    setRotationValues(phi, theta, psi, Rotation::ToBody{});
  }

  Matrix(const Matrix<Scalar, 3, 1> &angles, Rotation::ToBody) {
    static_assert(Rows == 3, "Expected 3 rows.");
    static_assert(Cols == 3, "Expected 3 cols.");
    setRotationValues(angles(0), angles(1), angles(2), Rotation::ToBody{});
  }

  /**
 * Constructs a 3D rotation Matrix with XYZ convention
 * @param phi: yaw
 * @param theta: pitch
 * @param psi: roll
   */
  OPENLB_HOST_DEVICE
  Matrix(const Scalar phi, const Scalar theta, const Scalar psi,
         Rotation::ToSpace) {
    static_assert(Rows == 3, "Expected 3 row.");
    static_assert(Cols == 3, "Expected 3 cols.");
    setRotationValues(phi, theta, psi, Rotation::ToSpace{});
  }

  Matrix(const Matrix<Scalar, 3, 1> &angles, Rotation::ToSpace) {
    static_assert(Rows == 3, "Expected 3 rows.");
    static_assert(Cols == 3, "Expected 3 cols.");
    setRotationValues(angles(0), angles(1), angles(2), Rotation::ToSpace{});
  }

  OPENLB_HOST_DEVICE
  Scalar &operator()(const size_t row, const size_t col) {
    return m_data[index(row, col)];
  }

  OPENLB_HOST_DEVICE
  Scalar operator()(const size_t row, const size_t col) const {
    return m_data[index(row, col)];
  }

  OPENLB_HOST_DEVICE
  Scalar &operator()(const size_t row) {
    static_assert(Rows != 1, "Expected only 1 row.");
    return m_data[row];
  }

  OPENLB_HOST_DEVICE
  Scalar operator()(const size_t row) const {
    static_assert(Rows != 1, "Expected only 1 row.");
    return m_data[row];
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols> operator-() const {
    Matrix<Scalar, Rows, Cols> ret;
    for (int i = 0; i < Rows * Cols; ++i) {
      ret.m_data[i] = -m_data[i];
    }
    return ret;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols>& operator*=(const Scalar fac) {
    for (int i = 0; i < Rows * Cols; ++i) {
        m_data[i] *= fac;
    }
    return *this;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols>& operator+=(const Matrix<Scalar, Rows, Cols>& rhs) {
    for (int i = 0; i < Rows * Cols; ++i) {
        m_data[i] += rhs.m_data[i];
    }
    return *this;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols> operator+() const {
      Matrix<Scalar, Rows, Cols> ret;
      for (int i = 0; i < Rows * Cols; ++i) {
        ret.m_data[i] = +m_data[i];
      }
      return ret;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols>& operator=(const Matrix<Scalar, Rows, Cols> & other)
  {
      for (int i = 0; i < Rows * Cols; ++i) {
        m_data[i] = other.m_data[i];
      }
      return *this;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols> operator+(const Matrix<Scalar, Rows, Cols>& rhs) {
    Matrix<Scalar, Rows, Cols> ret;
    for (int i = 0; i < Rows * Cols; ++i) {
        ret.m_data[i] += m_data[i]+rhs.m_data[i];
    }
    return ret;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols> operator-(const Matrix<Scalar, Rows, Cols>& rhs) {
    Matrix<Scalar, Rows, Cols> ret;
    for (int i = 0; i < Rows * Cols; ++i) {
        ret.m_data[i] += m_data[i]-rhs.m_data[i];
    }
    return ret;
  }

  OPENLB_HOST_DEVICE
  Matrix<Scalar, Rows, Cols> operator*(const Scalar fac) {
    Matrix<Scalar, Rows, Cols> ret;
    for (int i = 0; i < Rows * Cols; ++i) {
        ret.m_data[i] = m_data[i]*fac;
    }
    return ret;
  }

protected:
  Scalar m_data[Rows * Cols] = {0};

  OPENLB_HOST_DEVICE
  void setRotationValues(const Scalar phi, const Scalar theta, const Scalar psi,
                         Rotation::ToSpace) {
    m_data[index(0, 0)] = std::cos(theta) * std::cos(psi);
    m_data[index(0, 1)] =
        std::sin(phi) * std::sin(theta) * std::cos(psi) - std::cos(phi) * std::sin(psi);
    m_data[index(0, 2)] =
        std::cos(phi) * std::sin(theta) * std::cos(psi) + std::sin(phi) * std::sin(psi);

    m_data[index(1, 0)] = std::cos(theta) * std::sin(psi);
    m_data[index(1, 1)] =
        std::sin(phi) * std::sin(theta) * std::sin(psi) + std::cos(phi) * std::cos(psi);
    m_data[index(1, 2)] =
        std::cos(phi) * std::sin(theta) * std::sin(psi) - std::sin(phi) * std::cos(psi);

    m_data[index(2, 0)] = -std::sin(theta);
    m_data[index(2, 1)] = std::sin(phi) * std::cos(theta);
    m_data[index(2, 2)] = std::cos(phi) * std::cos(theta);
  }

  OPENLB_HOST_DEVICE
  void setRotationValues(const Scalar phi, const Scalar theta, const Scalar psi,
                         Rotation::ToBody) {
    m_data[index(0, 0)] = std::cos(theta) * std::cos(psi);
    m_data[index(0, 1)] = std::cos(theta) * std::sin(psi);
    m_data[index(0, 2)] = -std::sin(theta);

    m_data[index(1, 0)] =
        std::sin(phi) * std::sin(theta) * std::cos(psi) - std::cos(phi) * std::sin(psi);
    m_data[index(1, 1)] =
        std::sin(phi) * std::sin(theta) * std::sin(psi) + std::cos(phi) * std::cos(psi);
    m_data[index(1, 2)] = std::sin(phi) * std::cos(theta);

    m_data[index(2, 0)] =
        std::cos(phi) * std::sin(theta) * std::cos(psi) + std::sin(phi) * std::sin(psi);
    m_data[index(2, 1)] =
        std::cos(phi) * std::sin(theta) * std::sin(psi) - std::sin(phi) * std::cos(psi);
    m_data[index(2, 2)] = std::cos(phi) * std::cos(theta);
  }

  OPENLB_HOST_DEVICE
  static constexpr size_t index(size_t row, size_t col) {
    return Rows * col + row;
  }
};


// template <typename Scalar, size_t Rows, size_t Cols> 
// std::ostream& operator << (std::ostream& os, Matrix<Scalar,Rows,Cols>& matrix)
// {
  // for (int row=0;row<Rows;++row)
  // {
    // for (int col=0;col<Cols;++col)
    // {
      // os << matrix(row,col) << " ";
    // }
    // os << "\n";
  // }
  // os << std::endl;

  // return os;
// }


template <typename Scalar, size_t Dim> struct AffineTransform;

template <typename Scalar> struct AffineTransform<Scalar, 2> {
  Matrix<Scalar, 2, 3> m;

  AffineTransform() {
    m(0, 0) = 1;
    m(1, 1) = 1;
  }

  /**
   * Init Transform with 2x2 matrix
   * M M 0
   * M M 0
   * @param matrix
   */
  explicit AffineTransform(const Matrix<Scalar, 2, 2> &matrix) {
    m(0, 0) = matrix(0, 0);
    m(0, 1) = matrix(0, 1);
    m(1, 0) = matrix(1, 0);
    m(1, 1) = matrix(1, 1);
  }

  explicit AffineTransform(const Scalar angle) {
    m(0, 0) = std::cos(angle);
    m(0, 1) = -std::sin(angle);
    m(1, 0) = std::sin(angle);
    m(1, 1) = std::cos(angle);
  }
  OPENLB_HOST_DEVICE
  Scalar &operator()(const size_t row, const size_t col) { return m(row, col); }
  OPENLB_HOST_DEVICE
  Scalar operator()(const size_t row, const size_t col) const {
    return m(row, col);
  }

  /**
   * Apply 2D translation on the left, the homogeneous coordinate of t is omitted
   * @param t
   */
  void applyToLeft(const Matrix<Scalar, 2, 1> &t) {
    m(0, 2) += t(0);
    m(1, 2) += t(1);
  }

  /**
   * Apply 2D translation on the right, the homogeneous coordinate of t is omitted
   * @param t
   */
  void applyToRight(const Matrix<Scalar, 2, 1> &t) {
    m(0, 2) += m(0, 0) * t(0) + m(0, 1) * t(1);
    m(1, 2) += m(1, 0) * t(0) + m(1, 1) * t(1);
  }

  void applyToLeft(const Matrix<Scalar, 2, 2>& rot) {
    const Scalar a = rot(0,0) * m(0,0) + rot(0,1) * m(1,0);
    const Scalar b = rot(0,0) * m(0,1) + rot(0,1) * m(1,1);
    const Scalar c = rot(0,0) * m(0,2) + rot(0,1) * m(1,2);

    const Scalar d = rot(1,0) * m(0,0) + rot(1,1) * m(1,0);
    const Scalar e = rot(1,0) * m(0,1) + rot(1,1) * m(1,1);
    const Scalar f = rot(1,0) * m(0,2) + rot(1,1) * m(1,2);

    m(0,0) = a;
    m(0,1) = b;
    m(0,2) = c;
    m(1,0) = d;
    m(1,1) = e;
    m(1,2) = f;
  }

  void applyToRight(const Matrix<Scalar, 2, 2>& rot) {
    const Scalar a = m(0,0) * rot(0,0) + m(0,1) * rot(1,0);
    const Scalar b = m(0,0) * rot(0,1) + m(0,1) * rot(1,1);

    const Scalar c = m(1,0) * rot(0,0) + m(1,1) * rot(1,0);
    const Scalar d = m(1,0) * rot(0,1) + m(1,1) * rot(1,1);

    m(0,0) = a;
    m(0,1) = b;
    m(1,0) = c;
    m(1,1) = d;
  }
};

template <typename Scalar> struct AffineTransform<Scalar, 3> {
  Matrix<Scalar, 3, 4> m;

  OPENLB_HOST_DEVICE
  AffineTransform() {
    m(0, 0) = 1;
    m(1, 1) = 1;
    m(2, 2) = 1;
  }

  /**
 * Init Transform with 3x3 matrix
 * M M M 0
 * M M M 0
 * M M M 0
 * @param matrix
   */
  OPENLB_HOST_DEVICE
  explicit AffineTransform(const Matrix<Scalar, 3, 3> &matrix) {
    m(0, 0) = matrix(0, 0);
    m(0, 1) = matrix(0, 1);
    m(0, 2) = matrix(0, 2);

    m(1, 0) = matrix(1, 0);
    m(1, 1) = matrix(1, 1);
    m(1, 2) = matrix(1, 2);

    m(2, 0) = matrix(2, 0);
    m(2, 1) = matrix(2, 1);
    m(2, 2) = matrix(2, 2);
  }

  OPENLB_HOST_DEVICE
  Scalar &operator()(const size_t row, const size_t col) { return m(row, col); }

  OPENLB_HOST_DEVICE
  Scalar operator()(const size_t row, const size_t col) const { return m(row, col); }

  OPENLB_HOST_DEVICE
  void applyToLeft(const Matrix<Scalar, 3, 1> &t) {
    m(0, 3) += t(0);
    m(1, 3) += t(1);
    m(2, 3) += t(2);
  }

  OPENLB_HOST_DEVICE
  void applyToRight(const Matrix<Scalar, 3, 1> &t) {
    m(0, 3) += m(0, 0) * t(0) + m(0, 1) * t(1) + m(0, 2) * t(2);
    m(1, 3) += m(1, 0) * t(0) + m(1, 1) * t(1) + m(1, 2) * t(2);
    m(2, 3) += m(2, 0) * t(0) + m(2, 1) * t(1) + m(2, 2) * t(2);
  }

  void applyToLeft(const Matrix<Scalar, 3, 3>& rot) {
    const Scalar a = rot(0,0) * m(0,0) + rot(0,1) * m(1,0) + rot(0,2) * m(2,0);
    const Scalar b = rot(0,0) * m(0,1) + rot(0,1) * m(1,1) + rot(0,2) * m(2,1);
    const Scalar c = rot(0,0) * m(0,2) + rot(0,1) * m(1,2) + rot(0,2) * m(2,2);
    const Scalar d = rot(0,0) * m(0,3) + rot(0,1) * m(1,3) + rot(0,2) * m(2,3);

    const Scalar e = rot(1,0) * m(0,0) + rot(1,1) * m(1,0) + rot(1,2) * m(2,0);
    const Scalar f = rot(1,0) * m(0,1) + rot(1,1) * m(1,1) + rot(1,2) * m(2,1);
    const Scalar g = rot(1,0) * m(0,2) + rot(1,1) * m(1,2) + rot(1,2) * m(2,2);
    const Scalar h = rot(1,0) * m(0,3) + rot(1,1) * m(1,3) + rot(1,2) * m(2,3);

    const Scalar i = rot(2,0) * m(0,0) + rot(2,1) * m(1,0) + rot(2,2) * m(2,0);
    const Scalar j = rot(2,0) * m(0,1) + rot(2,1) * m(1,1) + rot(2,2) * m(2,1);
    const Scalar k = rot(2,0) * m(0,2) + rot(2,1) * m(1,2) + rot(2,2) * m(2,2);
    const Scalar l = rot(2,0) * m(0,3) + rot(2,1) * m(1,3) + rot(2,2) * m(2,3);

    m(0,0) = a; m(1,0) = e; m(2,0) = i;
    m(0,1) = b; m(1,1) = f; m(2,1) = j;
    m(0,2) = c; m(1,2) = g; m(2,2) = k;
    m(0,3) = d; m(1,3) = h; m(2,3) = l;
  }

  void applyToRight(const Matrix<Scalar, 3, 3>& rot) {
    const Scalar a = m(0,0) * rot(0,0) + m(0,1) * rot(1,0) + m(0,2) * rot(2,0);
    const Scalar b = m(0,0) * rot(0,1) + m(0,1) * rot(1,1) + m(0,2) * rot(2,1);
    const Scalar c = m(0,0) * rot(0,2) + m(0,1) * rot(1,2) + m(0,2) * rot(2,2);

    const Scalar d = m(1,0) * rot(0,0) + m(1,1) * rot(1,0) + m(1,2) * rot(2,0);
    const Scalar e = m(1,0) * rot(0,1) + m(1,1) * rot(1,1) + m(1,2) * rot(2,1);
    const Scalar f = m(1,0) * rot(0,2) + m(1,1) * rot(1,2) + m(1,2) * rot(2,2);

    const Scalar g = m(2,0) * rot(0,0) + m(2,1) * rot(1,0) + m(2,2) * rot(2,0);
    const Scalar h = m(2,0) * rot(0,1) + m(2,1) * rot(1,1) + m(2,2) * rot(2,1);
    const Scalar i = m(2,0) * rot(0,2) + m(2,1) * rot(1,2) + m(2,2) * rot(2,2);

    m(0,0) = a; m(1,0) = d; m(2,0) = g;
    m(0,1) = b; m(1,1) = e; m(2,1) = h;
    m(0,2) = c; m(1,2) = f; m(2,2) = i;

  }
};

template <typename Scalar>
OPENLB_HOST_DEVICE
void transformInPlace(Matrix<Scalar, 2, 1> &v,
                      const AffineTransform<Scalar, 2> &t) {
  const Scalar tmpX = t(0, 0) * v(0) + t(0, 1) * v(1) + t(0, 2); // v(2) is implicitly 1
  const Scalar tmpY = t(1, 0) * v(0) + t(1, 1) * v(1) + t(1, 2);
  v(0) = tmpX;
  v(1) = tmpY;
}

template <typename Scalar>
OPENLB_HOST_DEVICE
void transformInPlace(Matrix<Scalar, 2, 2> &v,
                      const AffineTransform<Scalar, 2> &t) {
  //This function implements a tensor transformation - T_transpose*M*T
  const Scalar tmp00 = t(0, 0) * (v(0,0)*t(0,0)+v(1,0)*t(1,0)) + t(1, 0) * (v(0,1)*t(0,0)+v(1,1)*t(1,0));
  const Scalar tmp01 = t(1, 2) * (v(0,0)*t(0,0)+v(1,0)*t(1,0)) + t(1, 1) * (v(0,1)*t(0,0)+v(1,1)*t(1,0));
  const Scalar tmp10 = t(1, 1) * (v(0,0)*t(0,1)+v(1,0)*t(1,1)) + t(1, 0) * (v(0,1)*t(0,1)+v(1,1)*t(1,1));
  const Scalar tmp11 = t(1, 2) * (v(0,0)*t(0,1)+v(1,0)*t(1,1)) + t(1, 1) * (v(0,1)*t(0,1)+v(1,1)*t(1,1));
  v(0,0) = tmp00;
  v(0,1) = tmp01;
  v(1,0) = tmp10;
  v(1,1) = tmp11;
}

template <typename Scalar>
OPENLB_HOST_DEVICE
void transformInPlace(Matrix<Scalar, 3, 1> &v,
                      const AffineTransform<Scalar, 3> &t) {
  const Scalar tmpX = t(0, 0) * v(0) + t(0, 1) * v(1) + t(0, 2) * v(2) + t(0, 3); // v(3) is implicitly 1
  const Scalar tmpY = t(1, 0) * v(0) + t(1, 1) * v(1) + t(1, 2) * v(2) + t(1, 3);
  const Scalar tmpZ = t(2, 0) * v(0) + t(2, 1) * v(1) + t(2, 2) * v(2) + t(2, 3);
  v(0) = tmpX;
  v(1) = tmpY;
  v(2) = tmpZ;
}

template <typename Scalar>
OPENLB_HOST_DEVICE
void transformInPlace(Matrix<Scalar, 3, 3> &v,
                      const AffineTransform<Scalar, 3> &t) {
  //This function implements a tensor transformation - T_transpose*M*T
  const Scalar tmp00 = t(0,0)*(t(0,0)*v(0,0)+t(1,0)*v(1,0)+t(2,0)*v(2,0)) + t(1,0)*(t(0,0)*v(0,1)+t(1,0)*v(1,1)+t(2,0)*v(2,1)) + t(2,0)*(t(0,0)*v(0,2)+t(1,0)*v(1,2)+t(2,0)*v(2,2));
  const Scalar tmp01 = t(0,1)*(t(0,0)*v(0,0)+t(1,0)*v(1,0)+t(2,0)*v(2,0)) + t(1,1)*(t(0,0)*v(0,1)+t(1,0)*v(1,1)+t(2,0)*v(2,1)) + t(2,1)*(t(0,0)*v(0,2)+t(1,0)*v(1,2)+t(2,0)*v(2,2));
  const Scalar tmp02 = t(0,2)*(t(0,0)*v(0,0)+t(1,0)*v(1,0)+t(2,0)*v(2,0)) + t(1,2)*(t(0,0)*v(0,1)+t(1,0)*v(1,1)+t(2,0)*v(2,1)) + t(2,2)*(t(0,0)*v(0,2)+t(1,0)*v(1,2)+t(2,0)*v(2,2));
  const Scalar tmp10 = t(0,0)*(t(0,1)*v(0,0)+t(1,1)*v(1,0)+t(2,1)*v(2,0)) + t(1,0)*(t(0,1)*v(0,1)+t(1,1)*v(1,1)+t(2,1)*v(2,1)) + t(2,0)*(t(0,1)*v(0,2)+t(1,1)*v(1,2)+t(2,1)*v(2,2));
  const Scalar tmp11 = t(0,1)*(t(0,1)*v(0,0)+t(1,1)*v(1,0)+t(2,1)*v(2,0)) + t(1,1)*(t(0,1)*v(0,1)+t(1,1)*v(1,1)+t(2,1)*v(2,1)) + t(2,1)*(t(0,1)*v(0,2)+t(1,1)*v(1,2)+t(2,1)*v(2,2));
  const Scalar tmp12 = t(0,2)*(t(0,1)*v(0,0)+t(1,1)*v(1,0)+t(2,1)*v(2,0)) + t(1,2)*(t(0,1)*v(0,1)+t(1,1)*v(1,1)+t(2,1)*v(2,1)) + t(2,2)*(t(0,1)*v(0,2)+t(1,1)*v(1,2)+t(2,1)*v(2,2));
  const Scalar tmp20 = t(0,0)*(t(0,2)*v(0,0)+t(1,2)*v(1,0)+t(2,2)*v(2,0)) + t(1,0)*(t(0,2)*v(0,1)+t(1,2)*v(1,1)+t(2,2)*v(2,1)) + t(2,0)*(t(0,2)*v(0,2)+t(1,2)*v(1,2)+t(2,2)*v(2,2));
  const Scalar tmp21 = t(0,1)*(t(0,2)*v(0,0)+t(1,2)*v(1,0)+t(2,2)*v(2,0)) + t(1,1)*(t(0,2)*v(0,1)+t(1,2)*v(1,1)+t(2,2)*v(2,1)) + t(2,1)*(t(0,2)*v(0,2)+t(1,2)*v(1,2)+t(2,2)*v(2,2));
  const Scalar tmp22 = t(0,2)*(t(0,2)*v(0,0)+t(1,2)*v(1,0)+t(2,2)*v(2,0)) + t(1,2)*(t(0,2)*v(0,1)+t(1,2)*v(1,1)+t(2,2)*v(2,1)) + t(2,2)*(t(0,2)*v(0,2)+t(1,2)*v(1,2)+t(2,2)*v(2,2));
  v(0,0) = tmp00;
  v(0,1) = tmp01;
  v(0,2) = tmp02;
  v(1,0) = tmp10;
  v(1,1) = tmp11;
  v(1,2) = tmp12;
  v(2,0) = tmp20;
  v(2,1) = tmp21;
  v(2,2) = tmp22;
}

template<typename Scalar>
OPENLB_HOST_DEVICE
Matrix<Scalar, 2, 1> transform(const Matrix<Scalar, 2, 1>& v, const AffineTransform<Scalar, 2>& t) {
  const Scalar tmpX = t(0, 0) * v(0) + t(0, 1) * v(1) + t(0, 2);
  const Scalar tmpY = t(1, 0) * v(0) + t(1, 1) * v(1) + t(1, 2);
  return Matrix<Scalar, 2, 1>(tmpX, tmpY);
}

template<typename Scalar>
OPENLB_HOST_DEVICE
Matrix<Scalar, 3, 1> transform(const Matrix<Scalar, 3, 1>& v, const AffineTransform<Scalar, 3>& t) {
  const Scalar tmpX = t(0,0) * v(0) + t(0,1) * v(1) + t(0,2) * v(2) + t(0,3); // v(3) is implicitly 1
  const Scalar tmpY = t(1,0) * v(0) + t(1,1) * v(1) + t(1,2) * v(2) + t(1,3);
  const Scalar tmpZ = t(2,0) * v(0) + t(2,1) * v(1) + t(2,2) * v(2) + t(2,3);
  return Matrix<Scalar, 3, 1>(tmpX, tmpY, tmpZ);
}

template <typename Scalar, size_t Dim> using Vec = Matrix<Scalar, Dim, 1>;

template <typename Scalar> using Vec2 = Matrix<Scalar, 2, 1>;
template <typename Scalar> using Vec3 = Matrix<Scalar, 3, 1>;

template <typename Scalar> using Vertex2 = Matrix<Scalar, 2, 1>;
template <typename Scalar> using Vertex3 = Matrix<Scalar, 3, 1>;

template <typename Scalar> using Mat2 = Matrix<Scalar, 2, 2>;
template <typename Scalar> using Mat3 = Matrix<Scalar, 3, 3>;

template <typename Scalar> using Affine2 = AffineTransform<Scalar, 2>;
template <typename Scalar> using Affine3 = AffineTransform<Scalar, 3>;



template<typename Scalar>
OPENLB_HOST_DEVICE
Vec<int, 2> vecToInt(const Vec<Scalar, 2>& vec) {
  int tmpX = static_cast<int>(round(vec(0)));
  int tmpY = static_cast<int>(round(vec(1)));
  return Vec<int, 2>(tmpX, tmpY);
}

template<typename Scalar>
OPENLB_HOST_DEVICE
Vec<int, 3> vecToInt(const Vec<Scalar, 3>& vec) {
  int tmpX = static_cast<int>(round(vec(0)));
  int tmpY = static_cast<int>(round(vec(1)));
  int tmpZ = static_cast<int>(round(vec(2)));
  return Vec<int, 3>(tmpX, tmpY, tmpZ);
}

template<typename Scalar>
OPENLB_HOST_DEVICE
Scalar dotProduct(Vec<Scalar, 2>& vec1, Vec<Scalar, 2>& vec2) {
  Scalar tmpXProduct = vec1(0)*vec2(0);
  Scalar tmpYProduct = vec1(1)*vec2(1);
  return tmpXProduct+tmpYProduct;
}

template<typename Scalar>
OPENLB_HOST_DEVICE
Scalar dotProduct(Vec<Scalar, 3>& vec1, Vec<Scalar, 3>& vec2) {
  Scalar tmpXProduct = vec1(0)*vec2(0);
  Scalar tmpYProduct = vec1(1)*vec2(1);
  Scalar tmpZProduct = vec1(2)*vec2(2);
  return tmpXProduct+tmpYProduct+tmpZProduct;
}


template <typename Scalar, size_t Rows, size_t Cols>
std::ostream &operator<<(std::ostream &os,
                         const Matrix<Scalar, Rows, Cols> &m) {
  std::cout.precision(5);
  for (int i = 0; i < Rows; ++i) {
    for (int j = 0; j < Cols; ++j) {
      os << std::fixed << m(i, j) << " ";
    }
    os << "\n";
  }
  return os;
}

template <typename Scalar, size_t Dim>
std::ostream &operator<<(std::ostream &os,
                         const AffineTransform<Scalar, Dim> &at) {
  os << at.m;
  return os;
}

}
#endif // OLB_AFFINETRANSFORM_H
