/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2015 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Dynamics for a generic 2D block data -- header file.
 */
#ifndef BLOCK_DATA_2D_HH
#define BLOCK_DATA_2D_HH

#include <algorithm>
#include "olbDebug.h"
#include "blockData2D.h"
#include "geometry/cuboid2D.h"
#include "functors/lattice/blockBaseF2D.h"


namespace olb {


    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::BlockData2D() : BlockStructure2D(0, 0), _size(0), _rawData(nullptr), _field(nullptr) {
        construct();
    }

    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::BlockData2D(Cuboid2D<T> &cuboid, int size)
            : BlockStructure2D(cuboid.getNx(), cuboid.getNy()), _size(size), _rawData(nullptr), _field(nullptr) {
        construct();
    }

    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::BlockData2D(int nx, int ny, int size)
            : BlockStructure2D(nx, ny), _size(size), _rawData(nullptr), _field(nullptr) {
        construct();
    }

    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::~BlockData2D() {
        deConstruct();
    }

    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::BlockData2D(BlockF2D<BaseType> &rhs)
            : BlockStructure2D(rhs.getBlockStructure().getNx(), rhs.getBlockStructure().getNy()),
              _size(rhs.getTargetDim()) {
        construct();
        int i[2];
        for (i[0] = 0; i[0] < this->_nx; ++i[0]) {
            for (i[1] = 0; i[1] < this->_ny; ++i[1]) {
                rhs(_field[i[0]][i[1]], i);
            }
        }
    }

    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::BlockData2D(BlockData2D<T, BaseType> const &rhs)
            : BlockStructure2D(rhs._nx, rhs._ny), _size(rhs._size), _rawData(nullptr), _field(nullptr) {
        if (rhs.isConstructed()) {
            construct();
            std::copy(rhs._rawData, rhs._rawData + getDataSize(), _rawData);
        }
    }

    template<typename T, typename BaseType>
    BlockData2D<T, BaseType> &BlockData2D<T, BaseType>::operator=(BlockData2D<T, BaseType> const &rhs) {
        BlockData2D<T, BaseType> tmp(rhs);
        swap(tmp);
        return *this;
    }

// benefits of move operator: does not allocate memory or copys objects
    template<typename T, typename BaseType>
    BlockData2D<T, BaseType> &BlockData2D<T, BaseType>::operator=(BlockData2D<T, BaseType> &&rhs) {
        if (this == &rhs) {
            return *this;
        }
//  this->releaseMemory();  // free data of object this

        _size = rhs._size;      // swap object data
        _rawData = rhs._rawData;
        _field = rhs._field;
        this->_nx = rhs._nx;
        this->_ny = rhs._ny;

        rhs._rawData = nullptr; // free data of object rhs
        rhs._field = nullptr;
        rhs._nx = 0;
        rhs._ny = 0;

        return *this;
    }

// benefits of move operator: does not allocate memory
    template<typename T, typename BaseType>
    BlockData2D<T, BaseType>::BlockData2D(BlockData2D<T, BaseType> &&rhs)
            : BlockStructure2D(rhs._nx, rhs._ny), _size(0), _rawData(nullptr), _field(nullptr) {
        *this = std::move(rhs); // https://msdn.microsoft.com/de-de/library/dd293665.aspx
    }


    template<typename T, typename BaseType>
    bool BlockData2D<T, BaseType>::isConstructed() const {
        return _rawData;
    }

    template<typename T, typename BaseType>
    void BlockData2D<T, BaseType>::construct() {
        if (!isConstructed()) {
            allocateMemory();
        }
    }

    template<typename T, typename BaseType>
    void BlockData2D<T, BaseType>::deConstruct() {
        if (isConstructed()) {
            releaseMemory();
        }
    }

    template<typename T, typename BaseType>
    void BlockData2D<T, BaseType>::reset() {
        OLB_PRECONDITION(isConstructed());
        for (size_t index = 0; index < getDataSize(); ++index) {
            (*this)[index] = BaseType();
        }
    }

    template<typename T, typename BaseType>
    void BlockData2D<T, BaseType>::swap(BlockData2D<T, BaseType> &rhs) {
        // Block2D
        std::swap(this->_nx, rhs._nx);
        std::swap(this->_ny, rhs._ny);
        // BlockData2D
        std::swap(_size, rhs._size);
        std::swap(_rawData, rhs._rawData);
        std::swap(_field, rhs._field);
    }

    template<typename T, typename BaseType>
    void BlockData2D<T, BaseType>::allocateMemory() {
        // The conversions to size_t ensure 64-bit compatibility. Note that
        //   nx and ny are of type int, which might by 32-bit types, even on
        //   64-bit platforms. Therefore, nx*ny may lead to a type overflow.
        _rawData = new BaseType[getDataSize()];
        _field = new BaseType **[(size_t) (this->_nx)];
        for (int iX = 0; iX < this->_nx; ++iX) {
            _field[iX] = new BaseType *[(size_t) this->_ny];
            for (int iY = 0; iY < this->_ny; ++iY) {
                // connect matrix element to the corresponding array entry of _rawData
                _field[iX][iY] = _rawData + _size * ((size_t) iY + (size_t) (this->_ny) * (size_t) iX);
                for (int iDim = 0; iDim < _size; ++iDim) {
                    // initialize data with zero
                    _field[iX][iY][iDim] = BaseType();
                }
            }
        }
    }

    template<typename T, typename BaseType>
    void BlockData2D<T, BaseType>::releaseMemory() {
        delete[] _rawData;
        _rawData = nullptr;
        for (unsigned int iX = 0; iX < (size_t) (this->_nx); ++iX) {
            delete[] _field[iX];
        }
        delete[] _field;
    }

    template<typename T, typename BaseType>
    BaseType &BlockData2D<T, BaseType>::get(int iX, int iY, int iSize) {
        OLB_PRECONDITION(iX >= 0 && iX < this->_nx);
        OLB_PRECONDITION(iY >= 0 && iY < this->_ny);
        OLB_PRECONDITION(iSize >= 0 && iSize < _size);
        OLB_PRECONDITION(isConstructed());
        return _field[iX][iY][iSize];
    }

    template<typename T, typename BaseType>
    BaseType const &BlockData2D<T, BaseType>::get(int iX, int iY, int iSize) const {
        OLB_PRECONDITION(iX >= 0 && iX < this->_nx);
        OLB_PRECONDITION(iY >= 0 && iY < this->_ny);
        OLB_PRECONDITION(iSize >= 0 && iSize < _size);
        OLB_PRECONDITION(isConstructed());
        return _field[iX][iY][iSize];
    }

    template<typename T, typename BaseType>
    BaseType &BlockData2D<T, BaseType>::operator[](int ind) {
        OLB_PRECONDITION(ind >= 0 && ind < this->_nx * this->_ny * this->_size);
        OLB_PRECONDITION(isConstructed());
        return _rawData[ind];
    }

    template<typename T, typename BaseType>
    BaseType const &BlockData2D<T, BaseType>::operator[](int ind) const {
        OLB_PRECONDITION(ind >= 0 && ind < this->_nx * this->_ny * this->_size);
        OLB_PRECONDITION(isConstructed());
        return _rawData[ind];
    }

    template<typename T, typename BaseType>
    bool *BlockData2D<T, BaseType>::operator()(int iX, int iY, int iData) {
        return (bool *) &_field[iX][iY][iData];
    }

    template<typename T, typename BaseType>
    BaseType BlockData2D<T, BaseType>::getMax() {
        return ***std::max_element(_field, _field + getDataSize());
    }

    template<typename T, typename BaseType>
    BaseType BlockData2D<T, BaseType>::getMin() {
        return ***std::min_element(_field, _field + getDataSize());
    }

    template<typename T, typename BaseType>
    BaseType *BlockData2D<T, BaseType>::getRawData() const {
        return _rawData;
    }

    template<typename T, typename BaseType>
    size_t BlockData2D<T, BaseType>::getDataSize() const {
        return (size_t) (this->_nx) * (size_t) (this->_ny) * (size_t) (_size);
    }

    template<typename T, typename BaseType>
    int BlockData2D<T, BaseType>::getSize() const {
        return _size;
    }

    template<typename T, typename BaseType>
    size_t BlockData2D<T, BaseType>::getSerializableSize() const {
        return 3 * sizeof(int) // _size, _nX/Y
               + getDataSize() * sizeof(BaseType); // _rawData
    };

    template<typename T, typename BaseType>
    bool *BlockData2D<T, BaseType>::getBlock(std::size_t iBlock, std::size_t &sizeBlock, bool loadingMode) {
        std::size_t currentBlock = 0;
        bool *dataPtr = nullptr;

        registerVar(iBlock, sizeBlock, currentBlock, dataPtr, _size);
        registerVar(iBlock, sizeBlock, currentBlock, dataPtr, this->_nx);
        registerVar(iBlock, sizeBlock, currentBlock, dataPtr, this->_ny);
        registerVar(iBlock, sizeBlock, currentBlock, dataPtr, *_rawData, getDataSize());

        return dataPtr;
    }

    template<typename T, template<typename U> class Lattice>
    CellBlockData2D<T, Lattice>::CellBlockData2D(int nx, int ny):
            _nx(static_cast<size_t>(nx)),
            _ny(static_cast<size_t>(ny)),
            fluidData_(new T *[Lattice<T>::dataSize]),
            fluidDataEven(new T *[Lattice<T>::dataSize]),
            fluidDataOdd(new T *[Lattice<T>::dataSize]),
            fluidDataCurrent(nullptr),
            fluidMask_(new bool[_nx * _ny])
#ifdef ENABLE_CUDA
            ,
            gpuFluidData_(nullptr),
            gpuFluidDataEven(nullptr),
            gpuFluidDataOdd(nullptr),
            gpuFluidDataCurrent(nullptr),
            gpuFluidMask_(nullptr)
#endif
            {

        fluidDataPostProcessOdd = new T *[Lattice<T>::dataSize];
        fluidDataPostProcessEven = new T *[Lattice<T>::dataSize];

        for (unsigned i = 0; i < _nx * _ny; ++i)
        {
            fluidMask_[i] = true;
        }

        //allocation
        for (int noField = 0; noField < Lattice<T>::dataSize; ++noField) {
            int nOffset = util::getGridOffset2D<Lattice<T>>(noField, _ny);
            fluidData_[noField] = new T[_nx * _ny + std::abs(nOffset)];

            for (size_t i = 0; i < _nx * _ny + std::abs(nOffset); ++i) {
                fluidData_[noField][i] = T();
            }

            if (noField >= Lattice<T>::q) {
                fluidDataEven[noField] = fluidData_[noField];
                fluidDataOdd[noField] = fluidData_[noField];
                fluidDataPostProcessEven[noField] = fluidData_[noField];
                fluidDataPostProcessOdd[noField] = fluidData_[noField];
            }
        }

        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            int nOffset = util::getGridOffset2D<Lattice<T>>(iPop, _ny);

            fluidDataEven[iPop] = fluidData_[iPop];
            int opposite = Lattice<T>::opposite(iPop);
            fluidDataOdd[iPop] = fluidData_[opposite];

            if (nOffset > 0) {
                fluidDataEven[iPop] += nOffset;
                fluidDataOdd[iPop] += nOffset;
            }
            fluidDataPostProcessEven[opposite] = fluidDataEven[iPop];
            fluidDataPostProcessOdd[opposite] = fluidDataOdd[iPop];
        }

        fluidDataCurrent = fluidDataEven;
        fluidDataPostProcessCurrent = fluidDataPostProcessEven;

#ifdef ENABLE_CUDA
        auto memory_size = (Lattice<T>::dataSize) * sizeof(T*);
        cudaError_t error = cudaMallocManaged(&gpuFluidData_, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&gpuFluidDataEven, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&gpuFluidDataOdd, memory_size);
        HANDLE_ERROR(error);

        for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i) {
            int offset = util::getGridOffset2D<Lattice<T>>(i, _ny);

            error = cudaMalloc(&gpuFluidData_[i], sizeof(T) * (_nx * _ny + std::abs(offset)));
            HANDLE_ERROR(error);

            if (i >= Lattice<T>::q) {
                gpuFluidDataEven[i] = gpuFluidData_[i];
                gpuFluidDataOdd[i] = gpuFluidData_[i];
            }
        }

        for (unsigned int i = 0; i < Lattice<T>::q; ++i) {
            int offset = util::getGridOffset2D<Lattice<T>>(i, _ny);
            gpuFluidDataEven[i] = gpuFluidData_[i];
            gpuFluidDataOdd[i] = gpuFluidData_[Lattice<T>::opposite(i)];

            if (offset > 0) {
                gpuFluidDataEven[i] += offset;
                gpuFluidDataOdd[i] += offset;
            }

            HANDLE_ERROR(cudaMemset(gpuFluidData_[i], 0, sizeof(T) * (_nx * _ny + std::abs(offset))));
        }

        error = cudaMalloc(&gpuFluidMask_, sizeof(bool) * _nx * _ny);
        HANDLE_ERROR(error);
        HANDLE_ERROR(cudaMemset(gpuFluidMask_, 0, sizeof(bool) * _nx * _ny));
        
        gpuFluidDataCurrent = gpuFluidDataEven;
#endif
    }

    template<typename T, template<typename U> class Lattice>
    CellBlockData2D<T, Lattice>::~CellBlockData2D<T, Lattice>() {
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            delete[] fluidData_[iPop];
        }
        delete[] fluidData_;
        delete[] fluidDataEven;
        delete[] fluidDataOdd;

        delete[] fluidMask_;

        /**
         * TODO: need to free GPU memory
         */
    }

    template <typename T, template <typename> class Lattice>
    bool* CellBlockData2D<T, Lattice>::getFluidMask() {
        return fluidMask_;
    }

    template <typename T, template <typename> class Lattice>
    bool CellBlockData2D<T, Lattice>::getFluidMask(int iX, int iY) {
        return fluidMask_[calc_index(iX, iY)];
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData2D<T, Lattice>::setFluidMaskForCell(int iX, int iY, bool flag) {
        fluidMask_[calc_index(iX, iY)] = flag;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData2D<T, Lattice>::getData(int iX, int iY) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][calc_index(iX, iY)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData2D<T, Lattice>::getData(int iX, int iY) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][calc_index(iX, iY)];
        }

        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData2D<T, Lattice>::getData(size_t i) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData2D<T, Lattice>::getData(size_t i) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][i];
        }

        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellPopulationArray<T, Lattice> CellBlockData2D<T, Lattice>::getPopulations(int iX, int iY) const {
        CellPopulationArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][this->calc_index(iX, iY)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    constexpr size_t CellBlockData2D<T, Lattice>::calc_index(size_t iX, size_t iY) const {
        return util::getCellIndex2D(iX, iY, _ny);
    }

    template<typename T, template<typename U> class Lattice>
    T *CellBlockData2D<T, Lattice>::getCellData(int iX, int iY, int iData) const {
        return &fluidDataCurrent[iData][calc_index(iX, iY)];
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockData2D<T, Lattice>::getCellData() const {
        return fluidDataCurrent;
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockData2D<T, Lattice>::getPostProcessData() const {
        return fluidDataPostProcessCurrent;
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockData2D<T, Lattice>::stream() {

        if (even) {
            fluidDataCurrent = fluidDataOdd;
            fluidDataPostProcessCurrent = fluidDataPostProcessOdd;
        } else {
            fluidDataCurrent = fluidDataEven;
            fluidDataPostProcessCurrent = fluidDataPostProcessEven;
        }
        even = !even;
    }

#ifdef ENABLE_CUDA
    template <typename T, template <typename> class Lattice>
    void CellBlockData2D<T, Lattice>::gpuStream() {
        if (gpuEven) {
            gpuFluidDataCurrent = gpuFluidDataOdd;
        } else {
            gpuFluidDataCurrent = gpuFluidDataEven;
        }
        gpuEven = !gpuEven;
    }

    template <typename T, template <typename> class Lattice>
    T** CellBlockData2D<T, Lattice>::gpuGetFluidData() {
        return gpuFluidDataCurrent;
    }

    template <typename T, template <typename> class Lattice>
    bool* CellBlockData2D<T, Lattice>::gpuGetFluidMask() {
        return gpuFluidMask_;
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData2D<T, Lattice>::transferCellDataToGPU() {
        size_t gridSize = _nx * _ny;
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaMemcpy(gpuFluidDataCurrent[i], fluidDataCurrent[i], sizeof(T) * gridSize, cudaMemcpyHostToDevice);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(gpuFluidMask_, fluidMask_, sizeof(bool) * gridSize, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData2D<T, Lattice>::transferCellDataToCPU() {
        size_t gridSize = _nx * _ny;
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaMemcpy(fluidDataCurrent[i], gpuFluidDataCurrent[i], sizeof(T) * gridSize, cudaMemcpyDeviceToHost);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(fluidMask_, gpuFluidMask_, sizeof(bool) * gridSize, cudaMemcpyDeviceToHost);
        HANDLE_ERROR(error);
    }

#endif



    /**
     * ALE CellBlockData
     */


    template<typename T, template<typename U> class Lattice>
    CellBlockDataALE2D<T, Lattice>::CellBlockDataALE2D(int nx, int ny) : CellBlockData2D<T, Lattice>(nx, ny),
            fluidDataALE_(nullptr),
            fluidDataEvenALE(nullptr),
            fluidDataOddALE(nullptr),
            fluidDataCurrentALE(nullptr) {
        fluidDataALE_ = new T *[Lattice<T>::q];
        fluidDataOddALE = new T *[Lattice<T>::dataSize];
        fluidDataEvenALE = new T *[Lattice<T>::dataSize];

        /**
         * We only need to duplicate the populations
         */
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            int nOffset = util::getGridOffset2D<Lattice<T>>(iPop, this->_ny);
            fluidDataALE_[iPop] = new T[this->_nx * this->_ny + std::abs(nOffset)];

            for (size_t i = 0; i < this->_nx * this->_ny + std::abs(nOffset); ++i) {
                fluidDataALE_[iPop][i] = T();
            }
        }

        for (int noField = Lattice<T>::q; noField < Lattice<T>::dataSize; ++noField) {
            fluidDataEvenALE[noField] = this->fluidData_[noField];
            fluidDataOddALE[noField] = this->fluidData_[noField];
        }

        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            int nOffset = util::getGridOffset2D<Lattice<T>>(iPop, this->_ny);

            fluidDataEvenALE[iPop] = fluidDataALE_[iPop];
            int opposite = Lattice<T>::opposite(iPop);
            fluidDataOddALE[iPop] = fluidDataALE_[opposite];

            if (nOffset > 0) {
                fluidDataEvenALE[iPop] += nOffset;
                fluidDataOddALE[iPop] += nOffset;
            }
            fluidDataCurrentALE = fluidDataEvenALE;
        }
    }

    template<typename T, template<typename U> class Lattice>
    CellBlockDataALE2D<T, Lattice>::~CellBlockDataALE2D<T, Lattice>() {
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            delete[] fluidDataALE_[iPop];
        }
        delete[] fluidDataALE_;

        delete[] fluidDataEvenALE;
        delete[] fluidDataOddALE;
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockDataALE2D<T, Lattice>::stream() {
        if (this->even) {
            if (!isALE) {
                fluidDataCurrentALE = fluidDataOddALE;
                this->fluidDataCurrent = this->fluidDataOdd;
            } else {
                fluidDataCurrentALE = this->fluidDataOdd;
                this->fluidDataCurrent = fluidDataOddALE;
            }
        } else {
            if (!isALE) {
                fluidDataCurrentALE = fluidDataEvenALE;
                this->fluidDataCurrent = this->fluidDataEven;
            } else {
                fluidDataCurrentALE = this->fluidDataEven;
                this->fluidDataCurrent = fluidDataEvenALE;
            }
        }
        this->even = !this->even;
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockDataALE2D<T, Lattice>::moveMesh() {
        std::swap(this->fluidDataCurrent, fluidDataCurrentALE);
        isALE = !isALE;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE2D<T, Lattice>::getDataALE(int iX, int iY) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][this->calc_index(iX, iY)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE2D<T, Lattice>::getDataALE(int iX, int iY) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][this->calc_index(iX, iY)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellPopulationArray<T, Lattice> CellBlockDataALE2D<T, Lattice>::getPopulations(int iX, int iY) const {
        CellPopulationArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][this->calc_index(iX, iY)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE2D<T, Lattice>::getDataALE(size_t i) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE2D<T, Lattice>::getDataALE(size_t i) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    T *CellBlockDataALE2D<T, Lattice>::getCellDataALE(int iX, int iY, int iData) const {
        return &fluidDataCurrentALE[iData][this->calc_index(iX, iY)];
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockDataALE2D<T, Lattice>::getCellDataALE() const {
        return fluidDataCurrentALE;
    }


}  // namespace olb

#endif
