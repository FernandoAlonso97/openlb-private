/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006-2008 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The dynamics of a 2D block lattice -- header file.
 */
#ifndef BLOCK_LATTICE_2D_H
#define BLOCK_LATTICE_2D_H

#include <vector>
#include "olbDebug.h"
#include "postProcessing.h"
#include "blockLatticeStructure2D.h"
#include "multiPhysics.h"
#include "core/cell.h"
#include "latticeStatistics.h"
#include "serializer.h"
#include "dynamicsContainer2D.h"
#include "cellBlockData.h"
#include "contrib/domainDecomposition/domainDecomposition.h"



/// All OpenLB code is contained in this namespace.
namespace olb {

    template<typename T>
    class BlockGeometryStructure2D;

    template<typename T, template<typename U> class Lattice>
    struct Dynamics;

    template<typename T>
    class BlockIndicatorF2D;


/** A regular lattice for highly efficient 2D LB dynamics.
 * A block lattice contains a regular array of CellView objects and
 * some useful methods to execute the LB dynamics on the lattice.
 *
 * This class is not intended to be derived from.
 */
    template<typename T, template<typename U> class Lattice>
    class BlockLattice2D : public BlockLatticeStructure2D<T, Lattice>, public Serializable {
    public:
        typedef std::vector<PostProcessor2D<T, Lattice> *> PostProcVector;
    private:
        /// Actual data array
//  CellBlockData2D<T,Lattice> cellData;
//  DynamicsContainer2D<T,Lattice> dynamicsContainer;
        PostProcVector postProcessors, latticeCouplings;
#ifdef PARALLEL_MODE_OMP
        LatticeStatistics<T> **statistics;
#else
        LatticeStatistics<T> *statistics;
#endif
    protected:
      unsigned ghostLayer[2] = {0,0};

    public:
        CellBlockData<T, Lattice>* cellData;
        DynamicsContainer2D<T, Lattice> dynamicsContainer;

        /// Construction of an nx_ by ny_ lattice
        BlockLattice2D(int nx, int ny, Dynamics<T, Lattice> *dynamics);

        /// Construction of an nx_ by ny_ lattice with ALE functionality
        BlockLattice2D(int nx, int ny, Dynamics<T,Lattice>* fluidDynamics, CellBlockDataALE<T,Lattice>* cellDataALE);

        /// Construction from SubDomainInfo
        BlockLattice2D(const SubDomainInformation<T,Lattice<T>>& subDomainInfo,Dynamics<T,Lattice>* dynamics);

        /// Destruction of the lattice
        virtual ~BlockLattice2D() override;

        /// Copy construction
        BlockLattice2D(BlockLattice2D<T, Lattice> const &rhs);

        /// Copy assignment
        BlockLattice2D &operator=(BlockLattice2D<T, Lattice> const &rhs);

        /// Swap the content of two BlockLattices
        void swap(BlockLattice2D &rhs);

    public:
        /// Read access to lattice cells
        CellView<T, Lattice> get(int iX, int iY) override {
            OLB_PRECONDITION(iX < this->_nx);
            OLB_PRECONDITION(iY < this->_ny);
            return CellView<T, Lattice>(getDynamics(iX, iY), cellData->getData(iX, iY));
        }

        /// Read only access to lattice cells
        CellView<T, Lattice> get(int iX, int iY) const override {
            OLB_PRECONDITION(iX < this->_nx);
            OLB_PRECONDITION(iY < this->_ny);
            return CellView<T, Lattice>(const_cast<Dynamics<T, Lattice> *>(dynamicsContainer.getDynamics(iX, iY)),
                                        cellData->getData(iX, iY));
        }

        T *getData(int iX, int iY, int iData) const;
        T** getData() {return cellData->getCellData(); }
        T** getDataGPU() { return cellData->gpuGetFluidData(); }

        bool getMaskEntry(int iX, int iY) const override
        {
            OLB_PRECONDITION(iX<this->_nx);
            OLB_PRECONDITION(iY<this->_ny);
            return cellData->getFluidMask(iX,iY);
        }

        /// Initialize the data after geometry setup
        void initDataArrays();

        /// Initialize the lattice cells to become ready for simulation
        void initialize() override;

        void defineDynamics(int x0, int x1, int y0, int y1,
                            Dynamics<T, Lattice> *dynamics) override;

        void defineDynamics(BlockGeometryStructure2D<T> &blockGeometry, int material,
                            Dynamics<T, Lattice> *dynamics) override;

        /// Get the dynamics for the specific point
        Dynamics<T, Lattice> *getDynamics(int iX, int iY) override;

        /// Define the dynamics on a lattice site
        void defineDynamics(int iX, int iY, Dynamics<T, Lattice> *dynamics) override;

        /**
         * Define the dynamics by indicator
         *
         * \param indicator Block indicator describing the target domain
         * \param overlap   Overlap of the underlying block geometry
         * \param dynamics  Dynamics to be defined
         **/
        // virtual void defineDynamics(BlockIndicatorF2D<T> &indicator, int overlap, Dynamics<T, Lattice> *dynamics);

        // Define rho and u on a rectangular domain
        virtual void defineRhoU(int const x0_, int const x1_, int const y0_, int const y1_,
                                T const rho, T const u[Lattice<T>::d]) override;

        void iniEquilibrium(int const x0_, int const x1_, int const y0_, int const y1_,
                            T const rho, T const u[Lattice<T>::d]);

        void defineForce(int const x0_, int const x1_, int const y0_, int const y1_, T const force[Lattice<T>::d]);

        using BlockLatticeStructure2D<T, Lattice>::iniEquilibrium;
        using BlockLatticeStructure2D<T, Lattice>::defineRhoU;

        /// Apply collision step to the whole domain
        template<typename FluidDynamics>
        void collide();

        /// Field collision step
        template<typename FluidDynamics>
        void fieldCollision();

        /// Boundary collision step
        void boundaryCollision();

        /// Apply first collision, then streaming step to the whole domain
        template<typename FluidDynamics>
        void collideAndStream();

#ifdef ENABLE_CUDA
        template<typename FluidDynamics>
        void fieldCollisionGPU();

        void boundaryCollisionGPU();

        void streamGPU();

        void postProcessGPU();

        void syncStreamsGPU();

#ifdef __CUDACC__
        void holdInputStream(cudaStream_t& inputStream);
        void waitOnInputStream(cudaStream_t& inputStream, cudaEvent_t& inputEvent);
#endif
        /// Apply first collision, then streaming step to the whole domain
        template<typename FluidDynamics>
        void collideAndStreamGPU();

        //collide and stream without cudaDeviceSynchronize; user should call cudaDeviceSynchronize after this call sometime
        template<typename FluidDynamics>
        void collideAndStreamAsyncGPU();
#endif

        /// Copy CPU data to GPU
        void copyDataToGPU();

        /// Copy GPU data to CPU
        void copyDataToCPU();

        /// Apply the streaming.
        void stream();
        
        /// Compute the average density within a rectangular domain
        T computeAverageDensity(int x0, int x1, int y0, int y1) const override;

        /// Compute the average density within the whole domain
        T computeAverageDensity() const override;

        /// Compute components of the stress tensor on the cell.
        void computeStress(int iX, int iY, T pi[util::TensorVal<Lattice<T> >::n]) override;

        /// Subtract a constant offset from the density within the whole domain
        void stripeOffDensityOffset(
                int x0, int x1, int y0, int y1, T offset) override;

        /// Subtract a constant offset from the density within a rect. domain
        void stripeOffDensityOffset(T offset) override;

        /// Apply an operation to all cells of a sub-domain
        void forAll(int x0_, int x1_, int y0_, int y1_,
                    WriteCellFunctional<T, Lattice> const &application) override;

        /// Apply an operation to all cells
        void forAll(WriteCellFunctional<T, Lattice> const &application) override;

        /// Add a non-local post-processing step
        void addPostProcessor(PostProcessorGenerator2D<T, Lattice> const &ppGen) override;

        /// Clean up all non-local post-processing steps
        void resetPostProcessors() override;

        /// Execute post-processing on a sub-lattice
        void postProcess(int x0_, int x1_, int y0_, int y1_) override;

        /// Execute post-processing steps
        void postProcess() override;

        /// Add a non-local post-processing step which couples together lattices
        void addLatticeCoupling(LatticeCouplingGenerator2D<T, Lattice> const &lcGen,
                                std::vector<SpatiallyExtendedObject2D *> partners) override;

        /// Execute couplings on a sub-lattice
        void executeCoupling(int x0_, int x1_, int y0_, int y1_) override;

        /// Execute couplings
        void executeCoupling() override;

        /// Subscribe postProcessors for reduction operations
        void subscribeReductions(Reductor<T> &reductor) override;

        /// Return a handle to the LatticeStatistics object
        LatticeStatistics<T> &getStatistics() override;

        /// Return a constant handle to the LatticeStatistics object
        LatticeStatistics<T> const &getStatistics() const override;

        SpatiallyExtendedObject2D *getComponent(int iBlock) override;

        SpatiallyExtendedObject2D const *getComponent(int iBlock) const override;

        multiPhysics::MultiPhysicsId getMultiPhysicsId() const override;

    public:
        /// Number of data blocks for the serializable interface
        std::size_t getNblock() const override;

        /// Binary size for the serializer
        std::size_t getSerializableSize() const override;

        /// Return a pointer to the memory of the current block and its size for the serializable interface
        bool *getBlock(std::size_t iBlock, std::size_t &sizeBlock, bool loadingMode) override;

        DynamicsDataHandler<T> *getDataHandler(Dynamics<T, Lattice> *dynamics);

        size_t getCellIndex(int iX, int iY);

        template<template<typename V,template <typename W> class> class Functor>
        void writeBlockLatticeByFunctor(const typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells);

        template<template<typename V,template <typename W> class> class Functor>
        void readBlockLatticeByFunctor(typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells);

    private:
        /// Release memory for post processors
        void clearPostProcessors();

        /// Release memory for lattice couplings
        void clearLatticeCouplings();

        void periodicEdge(int x0, int x1, int y0, int y1);

        void makePeriodic();

};


}  // namespace olb

#endif
