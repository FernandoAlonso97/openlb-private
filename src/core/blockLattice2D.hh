/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006-2008 Jonas Latt
 *  OMP parallel code by Mathias Krause, Copyright (C) 2007
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The dynamics of a 2D block lattice -- generic implementation.
 */
#ifndef BLOCK_LATTICE_2D_HH
#define BLOCK_LATTICE_2D_HH

#include <algorithm>
#include "blockLattice2D.h"
#include "dynamics/dynamics.h"
#include "dynamics/lbHelpers.h"
#include "util.h"
#include "communication/loadBalancer.h"
#include "communication/blockLoadBalancer.h"
#include "functors/lattice/indicator/blockIndicatorF2D.h"
#include "config.h"
#include "gpuhandler.h"
#include "cellBlockData.h"

#include <iostream>

namespace olb {

////////////////////// Class BlockLattice2D /////////////////////////

/** \param nx lattice width (first index)
 *  \param ny lattice height (second index)
 */
    template<typename T, template<typename U> class Lattice>
    BlockLattice2D<T, Lattice>::BlockLattice2D(int nx, int ny, Dynamics<T, Lattice> *fluidDynamics)
            : BlockLatticeStructure2D<T, Lattice>(nx, ny),
              cellData(new CellBlockData<T, Lattice>(nx, ny)),
              dynamicsContainer(nx, ny, cellData, fluidDynamics) // TODO
    {
//  cellData = new CellBlockData2D<T,Lattice>(nx, ny);
        resetPostProcessors();

#ifdef PARALLEL_MODE_OMP
        statistics = new LatticeStatistics<T>* [3*omp.get_size()];
#pragma omp parallel
        {
          statistics[omp.get_rank() + omp.get_size()]
            = new LatticeStatistics<T>;
          statistics[omp.get_rank()] = new LatticeStatistics<T>;
          statistics[omp.get_rank() + 2*omp.get_size()]
            = new LatticeStatistics<T>;
        }
#else
        statistics = new LatticeStatistics<T>;
#endif
}

template<typename T, template<typename U> class Lattice>
BlockLattice2D<T,Lattice>::BlockLattice2D(int nx, int ny, Dynamics<T,Lattice>* fluidDynamics, CellBlockDataALE<T,Lattice>* cellDataALE)
  : BlockLatticeStructure2D<T,Lattice>(nx,ny),
    cellData(cellDataALE),
    dynamicsContainer(nx, ny, cellData, fluidDynamics) // TODO
{
  cellData = cellDataALE;
  resetPostProcessors();
#ifdef PARALLEL_MODE_OMP
        statistics = new LatticeStatistics<T>* [3*omp.get_size()];
#pragma omp parallel
        {
          statistics[omp.get_rank() + omp.get_size()]
            = new LatticeStatistics<T>;
          statistics[omp.get_rank()] = new LatticeStatistics<T>;
          statistics[omp.get_rank() + 2*omp.get_size()]
            = new LatticeStatistics<T>;
        }
#else
        statistics = new LatticeStatistics<T>;
#endif
    }

template<typename T, template<typename U> class Lattice>
BlockLattice2D<T,Lattice>::BlockLattice2D(const SubDomainInformation<T,Lattice<T>>& subDomainInfo,Dynamics<T,Lattice>* fluidDynamics)
  : BlockLatticeStructure2D<T,Lattice>( subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1]),
    cellData(new CellBlockData<T,Lattice>(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1])),
    dynamicsContainer(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1], cellData, fluidDynamics)

{
  ghostLayer[0]=(subDomainInfo.ghostLayer[0]);
  ghostLayer[1]=(subDomainInfo.ghostLayer[1]);
  resetPostProcessors();
#ifdef PARALLEL_MODE_OMP
        statistics = new LatticeStatistics<T>* [3*omp.get_size()];
#pragma omp parallel
        {
          statistics[omp.get_rank() + omp.get_size()]
            = new LatticeStatistics<T>;
          statistics[omp.get_rank()] = new LatticeStatistics<T>;
          statistics[omp.get_rank() + 2*omp.get_size()]
            = new LatticeStatistics<T>;
        }
#else
        statistics = new LatticeStatistics<T>;
#endif
    }

/** During destruction, the memory for the lattice and the contained
 * cells is released. However, the dynamics objects pointed to by
 * the cells must be deleted manually by the user.
 */
    template<typename T, template<typename U> class Lattice>
    BlockLattice2D<T, Lattice>::~BlockLattice2D() {
        clearPostProcessors();
        clearLatticeCouplings();
#ifdef PARALLEL_MODE_OMP
#pragma omp parallel
        {
          delete statistics[omp.get_rank()];
        }
        delete statistics;
#else
        delete statistics;
#endif
        delete cellData;
    }

/** The whole data of the lattice is duplicated. This includes
 * both particle distribution function and external fields.
 * \warning The dynamics objects and postProcessors are not copied
 * \param rhs the lattice to be duplicated
 */
    template<typename T, template<typename U> class Lattice>
    BlockLattice2D<T, Lattice>::BlockLattice2D(BlockLattice2D<T, Lattice> const &rhs)
            : BlockLatticeStructure2D<T, Lattice>(rhs._nx, rhs._ny),
              cellData(rhs.cellData),
              dynamicsContainer(rhs.dynamicsContainer) {
        resetPostProcessors();
        cellData = rhs.cellData;
#ifdef PARALLEL_MODE_OMP
        statistics = new LatticeStatistics<T>* [3*omp.get_size()];
#pragma omp parallel
        {
          statistics[omp.get_rank() + omp.get_size()]
            = new LatticeStatistics<T>;
          statistics[omp.get_rank()] = new LatticeStatistics<T> (**rhs.statistics);
          statistics[omp.get_rank() + 2*omp.get_size()]
            = new LatticeStatistics<T>;
        }
#else
        statistics = new LatticeStatistics<T>(*rhs.statistics);
#endif
    }

/** The current lattice is deallocated, then the lattice from the rhs
 * is duplicated. This includes both particle distribution function
 * and external fields.
 * \warning The dynamics objects and postProcessors are not copied
 * \param rhs the lattice to be duplicated
 */
    template<typename T, template<typename U> class Lattice>
    BlockLattice2D<T, Lattice> &BlockLattice2D<T, Lattice>::operator=(
            BlockLattice2D<T, Lattice> const &rhs) {
        BlockLattice2D<T, Lattice> tmp(rhs);
        swap(tmp);
        return *this;
    }

/** The swap is efficient, in the sense that only pointers to the
 * lattice are copied, and not the lattice itself.
 */
    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::swap(BlockLattice2D &rhs) {
        std::swap(this->_nx, rhs._nx);
        std::swap(this->_ny, rhs._ny);
        std::swap(cellData, rhs.cellData);
        postProcessors.swap(rhs.postProcessors);
        std::swap(statistics, rhs.statistics);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::initDataArrays() {
        dynamicsContainer.init();
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::initialize() {
        postProcess();
    }

    template<typename T, template<typename U> class Lattice>
    T *BlockLattice2D<T, Lattice>::getData(int iX, int iY, int iData) const {
        return cellData->getCellData(iData, iX, iY);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::defineDynamics(
            int x0, int x1, int y0, int y1, Dynamics<T, Lattice> *dynamics) {
        OLB_PRECONDITION(x0 >= 0 && x1 < this->_nx);
        OLB_PRECONDITION(x1 >= x0);
        OLB_PRECONDITION(y0 >= 0 && y1 < this->_ny);
        OLB_PRECONDITION(y1 >= y0);

        dynamicsContainer.defineDynamics(x0, x1, y0, y1, dynamics);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::defineDynamics(
            BlockGeometryStructure2D<T> &blockGeometry, int material, Dynamics<T, Lattice> *dynamics) {
        dynamicsContainer.defineDynamics(blockGeometry, material, dynamics);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::defineDynamics(
            int iX, int iY, Dynamics<T, Lattice> *dynamics) {
        OLB_PRECONDITION(iX >= 0 && iX < this->_nx);
        OLB_PRECONDITION(iY >= 0 && iY < this->_ny);

        dynamicsContainer.defineDynamics(iX, iY, dynamics);
    }

    // template<typename T, template<typename U> class Lattice>
    // void BlockLattice2D<T, Lattice>::defineDynamics(
    //         BlockIndicatorF2D<T> &indicator, int overlap, Dynamics<T, Lattice> *dynamics) {
    //     dynamicsContainer.defineDynamics(indicator, overlap, dynamics);
    // }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::defineRhoU(int const x0, int const x1, int const y0, int const y1,
                                                T const rho, T const u[Lattice<T>::d]) {
        OLB_PRECONDITION(x0 >= 0 && x1 < this->_nx);
        OLB_PRECONDITION(x1 >= x0);
        OLB_PRECONDITION(y0 >= 0 && y1 < this->_ny);
        OLB_PRECONDITION(y1 >= y0);

        for (int iX = x0; iX <= x1; ++iX) {
            for (int iY = y0; iY <= y1; ++iY) {
                Dynamics<T, Lattice> *dynamics = getDynamics(iX, iY);
                dynamics->defineRhoU(dynamicsContainer.getDataHandler(dynamics), cellData->getCellData(),
                                     util::getCellIndex2D(iX, iY, this->_ny), rho, u);
            }
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::iniEquilibrium(int const x0, int const x1, int const y0, int const y1,
                                                    T const rho, T const u[Lattice<T>::d]) {
        OLB_PRECONDITION(x0 >= 0 && x1 < this->_nx);
        OLB_PRECONDITION(x1 >= x0);
        OLB_PRECONDITION(y0 >= 0 && y1 < this->_ny);
        OLB_PRECONDITION(y1 >= y0);

        for (int iX = x0; iX <= x1; ++iX) {
            for (int iY = y0; iY <= y1; ++iY) {
                getDynamics(iX, iY)->iniEquilibrium(cellData->getCellData(), util::getCellIndex2D(iX, iY, this->_ny),
                                                    rho, u);
            }
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::defineForce(int const x0, int const x1, int const y0, int const y1,
                                                 T const force[Lattice<T>::d]) {
        OLB_PRECONDITION(x0 >= 0 && x1 < this->_nx);
        OLB_PRECONDITION(x1 >= x0);
        OLB_PRECONDITION(y0 >= 0 && y1 < this->_ny);
        OLB_PRECONDITION(y1 >= y0);

        for (int iX = x0; iX <= x1; ++iX) {
            for (int iY = y0; iY <= y1; ++iY) {
                get(iX, iY).defineExternalField(Lattice<T>::forceIndex, Lattice<T>::d, force);
            }
        }
    }

    template<typename T, template<typename U> class Lattice>
    Dynamics<T, Lattice> *BlockLattice2D<T, Lattice>::getDynamics(int iX, int iY) {
        return dynamicsContainer.getDynamics(iX, iY);
    }

    template<typename T, template<typename U> class Lattice>
    template<typename FluidDynamics>
    void BlockLattice2D<T, Lattice>::collide() {
        fieldCollision<FluidDynamics>();

        boundaryCollision();
    }

    template<typename T, template<typename U> class Lattice>
    template<typename FluidDynamics>
    void BlockLattice2D<T, Lattice>::fieldCollision() {
        T **fieldData = cellData->getCellData();
        DynamicsDataHandler<T> *dataHandler = dynamicsContainer.getDataHandler(dynamicsContainer.getFluidDynamics());
        T **momentaData = nullptr;
        if (dataHandler)
            momentaData = dataHandler->getDynamicsData();
        T *collisionData = dynamicsContainer.getFluidDynamics()->getCollisionData();

        size_t size = this->_nx * this->_ny;

        bool *mask = cellData->getFluidMask();
        for (size_t cellId = 0; cellId < size; ++cellId) {
            if (mask[cellId])
                FluidDynamics::collision(fieldData, cellId, momentaData, 0, collisionData);
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::boundaryCollision() {
        T **fieldData = cellData->getCellData();

        for (auto &itr : dynamicsContainer.getBoundaryDynamicsSet()) {
            itr->dispatchCollisionCPU(dynamicsContainer.getDataHandler(itr), fieldData);
        }
    }

/** At the end of this method, the post-processing steps are automatically
 * invoked. **/
    template<typename T, template<typename U> class Lattice>
    template<typename FluidDynamics>
    void BlockLattice2D<T, Lattice>::collideAndStream() {
        collide<FluidDynamics>();

        stream();

        postProcess();

        getStatistics().incrementTime();
    }

#ifdef ENABLE_CUDA

    template<typename T, template<typename U> class Lattice>
    template<typename FluidDynamics>
    void BlockLattice2D<T, Lattice>::fieldCollisionGPU() {
        dynamicsContainer.getGPUHandler().template executeFluidKernel<FluidDynamics>(this->_nx * this->_ny,
                                                                                     dynamicsContainer.getFluidDynamics());
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::boundaryCollisionGPU() {
        size_t position = 1;

        GPUHandler<T, Lattice> &gpuHandler = dynamicsContainer.getGPUHandler();
        for (auto &itr : dynamicsContainer.getBoundaryDynamicsSet()) {
            size_t size = dynamicsContainer.getDataHandler(itr)->getCellIDs().size();
            itr->dispatchCollisionGPU(gpuHandler, position, size);
            ++position;
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::streamGPU() {
        cellData->gpuStream();
    }

/** At the end of this method, the post-processing steps are automatically
 * invoked. */
    template<typename T, template<typename U> class Lattice>
    template<typename FluidDynamics>
    void BlockLattice2D<T, Lattice>::collideAndStreamGPU() {
        fieldCollisionGPU<FluidDynamics>();

        boundaryCollisionGPU();

        cudaDeviceSynchronize();

        streamGPU();

        cudaDeviceSynchronize();

        postProcessGPU();

        cudaDeviceSynchronize();
    }

    template<typename T, template<typename U> class Lattice>
    template<typename FluidDynamics>
    void BlockLattice2D<T, Lattice>::collideAndStreamAsyncGPU() {
        fieldCollisionGPU<FluidDynamics>();
        boundaryCollisionGPU();
        streamGPU();

        syncStreamsGPU();

        postProcessGPU();

        syncStreamsGPU();
    }

#endif

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::copyDataToGPU() {
#ifdef ENABLE_CUDA
        cellData->copyCellDataToGPU();
        for (auto &itr : dynamicsContainer.getBoundaryDynamicsSet()) {
            dynamicsContainer.getGPUHandler().transferToGPU(itr, dynamicsContainer.getDataHandler(itr));
        }
#endif
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::copyDataToCPU() {
#ifdef ENABLE_CUDA
        cellData->copyCellDataToCPU();
        for (auto &itr : dynamicsContainer.getBoundaryDynamicsSet()) {
            dynamicsContainer.getGPUHandler().transferToCPU(itr, dynamicsContainer.getDataHandler(itr));
        }
#endif
    }


    template<typename T, template<typename U> class Lattice>
    T BlockLattice2D<T, Lattice>::computeAverageDensity(int x0, int x1, int y0, int y1) const {
        T sumRho = T();
        for (int iX = x0; iX <= x1; ++iX) {
            for (int iY = y0; iY <= y1; ++iY) {
                T rho, u[Lattice<T>::d];
                get(iX, iY).computeRhoU(rho, u);
                sumRho += rho;
            }
        }
        return sumRho / (T) (x1 - x0 + 1) / (T) (y1 - y0 + 1);
    }

    template<typename T, template<typename U> class Lattice>
    T BlockLattice2D<T, Lattice>::computeAverageDensity() const {
        return computeAverageDensity(0, this->_nx - 1, 0, this->_ny - 1);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::computeStress(int iX, int iY, T pi[util::TensorVal<Lattice<T>>::n]) {
        // TODO
        //grid[iX][iY].computeStress(pi);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::stripeOffDensityOffset(int x0, int x1, int y0, int y1, T offset) {
        for (int iX = x0; iX <= x1; ++iX) {
            for (int iY = y0; iY <= y1; ++iY) {
                for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
                    get(iX, iY)[iPop] -= Lattice<T>::t(iPop) * offset;
                }
            }
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::stripeOffDensityOffset(T offset) {
        stripeOffDensityOffset(0, this->_nx - 1, 0, this->_ny - 1, offset);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::forAll(
            int x0, int x1, int y0, int y1, WriteCellFunctional<T, Lattice> const &application) {
        for (int iX = x0; iX <= x1; ++iX) {
            for (int iY = y0; iY <= y1; ++iY) {
                int pos[] = {iX, iY};
                application.apply(get(iX, iY), pos);
            }
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::forAll(WriteCellFunctional<T, Lattice> const &application) {
        forAll(0, this->_nx - 1, 0, this->_ny - 1, application);
    }


    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::addPostProcessor(
            PostProcessorGenerator2D<T, Lattice> const &ppGen) {
        postProcessors.push_back(ppGen.generate());
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::resetPostProcessors() {
        clearPostProcessors();
        StatPPGenerator2D<T, Lattice> statPPGenerator;
        addPostProcessor(statPPGenerator);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::clearPostProcessors() {
        typename PostProcVector::iterator ppIt = postProcessors.begin();
        for (; ppIt != postProcessors.end(); ++ppIt) {
            delete *ppIt;
        }
        postProcessors.clear();
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::postProcess() {
//  for (unsigned iPr=0; iPr<postProcessors.size(); ++iPr) {
//    postProcessors[iPr] -> process(*this);
//  }

        T **fieldData = cellData->getCellData();

        for (auto &itr : dynamicsContainer.getBoundaryDynamicsSet()) {
            itr->dispatchPostProcCPU(dynamicsContainer.getDataHandler(itr), fieldData, this->_nx, this->_ny);
        }

    }

#ifdef ENABLE_CUDA

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::postProcessGPU() {

        int position = 0;

        GPUHandler<T, Lattice> &gpuHandler = dynamicsContainer.getGPUHandler();
        for (auto &itr : dynamicsContainer.getBoundaryDynamicsSet()) {
            size_t
            size = dynamicsContainer.getDataHandler(itr)->getCellIDs().size();
            itr->dispatchPostProcGPU(gpuHandler, position, size);
            ++position;
        }

    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::syncStreamsGPU() {
        dynamicsContainer.getGPUHandler().syncStreams();
    }

#ifdef __CUDACC__
    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::holdInputStream(cudaStream_t& inputStream) {
        dynamicsContainer.getGPUHandler().holdInputStream(inputStream);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::waitOnInputStream(cudaStream_t& inputStream, cudaEvent_t& inputEvent) {
        dynamicsContainer.getGPUHandler().waitOnInputStream(inputStream, inputEvent);
    }
#endif

#endif

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::postProcess(int x0_, int x1_, int y0_, int y1_) {
        for (unsigned iPr = 0; iPr < postProcessors.size(); ++iPr) {
            postProcessors[iPr]->processSubDomain(*this, x0_, x1_, y0_, y1_);
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::addLatticeCoupling(
            LatticeCouplingGenerator2D<T, Lattice> const &lcGen,
            std::vector<SpatiallyExtendedObject2D *> partners) {
        latticeCouplings.push_back(lcGen.generate(partners));
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::executeCoupling() {
        for (unsigned iPr = 0; iPr < latticeCouplings.size(); ++iPr) {
            latticeCouplings[iPr]->process(*this);
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::executeCoupling(int x0_, int x1_, int y0_, int y1_) {
        for (unsigned iPr = 0; iPr < latticeCouplings.size(); ++iPr) {
            latticeCouplings[iPr]->processSubDomain(*this, x0_, x1_, y0_, y1_);
        }
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::clearLatticeCouplings() {
        typename PostProcVector::iterator ppIt = latticeCouplings.begin();
        for (; ppIt != latticeCouplings.end(); ++ppIt) {
            delete *ppIt;
        }
        latticeCouplings.clear();
    }


    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::subscribeReductions(Reductor<T> &reductor) {
        for (unsigned iPr = 0; iPr < postProcessors.size(); ++iPr) {
            postProcessors[iPr]->subscribeReductions(*this, &reductor);
        }
    }

    template<typename T, template<typename U> class Lattice>
    LatticeStatistics<T> &BlockLattice2D<T, Lattice>::getStatistics() {
#ifdef PARALLEL_MODE_OMP
        return *statistics[omp.get_rank()];
#else
        return *statistics;
#endif
    }

    template<typename T, template<typename U> class Lattice>
    LatticeStatistics<T> const &BlockLattice2D<T, Lattice>::getStatistics() const {
#ifdef PARALLEL_MODE_OMP
        return *statistics[omp.get_rank()];
#else
        return *statistics;
#endif
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::stream() {
        cellData->stream();
    }

    template<typename T, template<typename U> class Lattice>
    std::size_t BlockLattice2D<T, Lattice>::getNblock() const {
        // TODO
        return 0;// return 2 + rawData[0].getNblock() * this->_nx * this->_ny;
    }


    template<typename T, template<typename U> class Lattice>
    std::size_t BlockLattice2D<T, Lattice>::getSerializableSize() const {
        return 0; // TODO
        // return 2 * sizeof(int) + rawData[0].getSerializableSize() * this->_nx * this->_ny;
    }


    template<typename T, template<typename U> class Lattice>
    bool *BlockLattice2D<T, Lattice>::getBlock(std::size_t iBlock, std::size_t &sizeBlock, bool loadingMode) {
        std::size_t currentBlock = 0;
        bool *dataPtr = nullptr;

        registerVar(iBlock, sizeBlock, currentBlock, dataPtr, this->_nx);
        registerVar(iBlock, sizeBlock, currentBlock, dataPtr, this->_ny);
        // TODO
        return dataPtr;
    }

    //Added by Shreyas
    template<typename T,template<typename U> class Lattice>
    size_t BlockLattice2D<T,Lattice>::getCellIndex(int iX, int iY)
    {
    return util::getCellIndex2D(iX,iY,this->_ny);
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::periodicEdge(int x0, int x1, int y0, int y1) {
        // TODO
    }

    template<typename T, template<typename U> class Lattice>
    void BlockLattice2D<T, Lattice>::makePeriodic() {
        static const int vicinity = Lattice<T>::vicinity;
        int maxX = this->_nx - 1;
        int maxY = this->_ny - 1;
        periodicEdge(0, vicinity - 1, 0, maxY);
        periodicEdge(maxX - vicinity + 1, maxX, 0, maxY);
        periodicEdge(vicinity, maxX - vicinity, 0, vicinity - 1);
        periodicEdge(vicinity, maxX - vicinity, maxY - vicinity + 1, maxY);
    }

    template<typename T, template<typename U> class Lattice>
    SpatiallyExtendedObject2D *BlockLattice2D<T, Lattice>::getComponent(int iBlock) {
        OLB_PRECONDITION(iBlock == 0);
        return this;
    }

    template<typename T, template<typename U> class Lattice>
    SpatiallyExtendedObject2D const *BlockLattice2D<T, Lattice>::getComponent(int iBlock) const {
        OLB_PRECONDITION(iBlock == 0);
        return this;
    }

    template<typename T, template<typename U> class Lattice>
    multiPhysics::MultiPhysicsId BlockLattice2D<T, Lattice>::getMultiPhysicsId() const {
        return multiPhysics::getMultiPhysicsBlockId<T, Lattice>();
    }

    template<typename T, template<typename U> class Lattice>
    template<template<typename V,template <typename W> class> class Functor>
    void BlockLattice2D<T,Lattice>::writeBlockLatticeByFunctor(const typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices, const size_t numberOfCells)
    {
        #ifdef ENABLE_CUDA
        GPUHandler<T, Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
        gpuHandler.template writeBlockLatticeByFunctor<Functor> (data,cellIndices,numberOfCells);
        #endif
    }

    template<typename T, template<typename U> class Lattice>
    template<template<typename V,template <typename W> class> class Functor>
    void BlockLattice2D<T,Lattice>::readBlockLatticeByFunctor(typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices, const size_t numberOfCells)
    {
        #ifdef ENABLE_CUDA
        GPUHandler<T, Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
        gpuHandler.template readBlockLatticeByFunctor<Functor>(data,cellIndices,numberOfCells);
        #endif
    }

}  // namespace olb

#endif
