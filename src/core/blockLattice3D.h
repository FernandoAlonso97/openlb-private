/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006-2008 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The dynamics of a 3D block lattice -- header file.
 */
#ifndef BLOCK_LATTICE_3D_H
#define BLOCK_LATTICE_3D_H

// #include <vector>
#include "olbDebug.h"
#include "core/cell.h"
#include "postProcessing.h"
#include "blockLatticeStructure3D.h"
#include "multiPhysics.h"
#include "latticeStatistics.h"
#include "serializer.h"
#include "dynamicsContainer3D.h"
#include "cellBlockData.h"
#include "contrib/domainDecomposition/domainDecomposition.h"


/// All OpenLB code is contained in this namespace.
namespace olb {

template<typename T> class BlockGeometryStructure3D;
template<typename T> class BlockIndicatorF3D;
template<typename T, template<typename U> class Lattice> struct Dynamics;


/** BlockLattice3D is a regular lattice for highly efficient 3D LB dynamics.
 * A block lattice contains an array of CellView objects and
 * some useful methods to execute the LB dynamics on the lattice.
 *
 * This class is not intended to be derived from.
 */
template<typename T, template<typename U> class Lattice>
class BlockLattice3D : public BlockLatticeStructure3D<T,Lattice>, public Serializable {
public:
  typedef std::vector<PostProcessor3D<T,Lattice>*> PostProcVector;

    CellBlockData<T,Lattice>* cellData;
    DynamicsContainer3D<T,Lattice> dynamicsContainer;
private:
  /// Actual data array
//  CellBlockData3D<T,Lattice> cellData;
//  DynamicsContainer3D<T,Lattice> dynamicsContainer;
  PostProcVector       postProcessors, latticeCouplings;
#ifdef PARALLEL_MODE_OMP
  LatticeStatistics<T> **statistics;
#else
  LatticeStatistics<T> *statistics;
#endif
protected:
  unsigned ghostLayer[3] = {0,0,0};

public:
  /// Construction of an nx_ by ny_ by nz_ lattice
  BlockLattice3D(int nx_, int ny_, int nz_, Dynamics<T,Lattice>* dynamics);
  /// Construction of an nx_ by ny_ by nz_ lattice with ALE
  BlockLattice3D(int nx_, int ny_, int nz_, Dynamics<T,Lattice>* dynamics, CellBlockDataALE<T,Lattice>* cellDataALE);
  /// Construction from SubDomainInfo
  BlockLattice3D(const SubDomainInformation<T,Lattice<T>>& subDomainInfo,Dynamics<T,Lattice>* dynamics);
  BlockLattice3D(const SubDomainInformation<T,Lattice<T>>& subDomainInfo,Dynamics<T,Lattice>* dynamics,CellBlockDataALE<T,Lattice>* cellDataALE);
  /// Destruction of the lattice
  virtual ~BlockLattice3D() override;
  /// Copy construction
  BlockLattice3D(BlockLattice3D<T,Lattice> const& rhs);
  /// Copy assignment
  BlockLattice3D& operator=(BlockLattice3D<T,Lattice> const& rhs);
  /// Swap the content of two BlockLattices
  void swap(BlockLattice3D& rhs);

  /// Read access to lattice cells
  CellView<T,Lattice> get(int iX, int iY, int iZ) override
  {
    OLB_PRECONDITION(iX<this->_nx);
    OLB_PRECONDITION(iY<this->_ny);
    OLB_PRECONDITION(iZ<this->_nz);
    return CellView<T,Lattice>(getDynamics(iX, iY, iZ), cellData->getData(iX, iY, iZ));
  }
  /// Read only access to lattice cells
  CellView<T,Lattice> get(int iX, int iY, int iZ) const override
  {
    OLB_PRECONDITION(iX<this->_nx);
    OLB_PRECONDITION(iY<this->_ny);
    OLB_PRECONDITION(iZ<this->_nz);
    return CellView<T,Lattice>(const_cast<Dynamics<T,Lattice>*>(dynamicsContainer.getDynamics(iX, iY, iZ)), cellData->getData(iX, iY, iZ));
  }

  bool getMaskEntry(int iX, int iY, int iZ) const override
  {
      OLB_PRECONDITION(iX<this->_nx);
      OLB_PRECONDITION(iY<this->_ny);
      OLB_PRECONDITION(iZ<this->_nz);
      return cellData->getFluidMask(iX,iY,iZ);
  }

  T* getData(int iX, int iY, int iZ, int iData) const;
  T* getDataPrevious(int iX, int iY, int iZ,int iData) const;
  CellPopulationArray<T,Lattice> getPopulations(int iX, int iY, int iZ) override {
      OLB_PRECONDITION(iX<this->_nx);
      OLB_PRECONDITION(iY<this->_ny);
      OLB_PRECONDITION(iZ<this->_nz);
      return cellData->getPopulations(iX,iY,iZ);
  }
  T** getData() {return cellData->getCellData(); }
  T** getDataGPU() { return cellData->gpuGetFluidData(); }
  bool* getFluidMask()
  {
      return cellData->getFluidMask();
  }
  bool* getFluidMaskGPU()
  {
      return cellData->gpuGetFluidMask();
  }
  /// Initialize the data after geometry setup
  void initDataArrays();
  /// Initialize the lattice cells to become ready for simulation
  void initialize() override;
  /// Define the dynamics on a 3D sub-box
  void defineDynamics (
    int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
    Dynamics<T,Lattice>* dynamics ) override;
  /// Define the dynamics on a lattice site
  void defineDynamics(int iX, int iY, int iZ, Dynamics<T,Lattice>* dynamics) override;
  /// Define the dynamics by material
  void defineDynamics(BlockGeometryStructure3D<T>& blockGeometry, int material, Dynamics<T,Lattice>* dynamics) override;
  /// Get the dynamics on a lattice site
  Dynamics<T,Lattice>* getDynamics(int iX, int iY, int iZ) override;

  /**
   * Define the dynamics by indicator
   *
   * \param indicator Block indicator describing the target domain
   * \param overlap   Overlap of the underlying block geometry
   * \param dynamics  Dynamics to be defined
   **/
  // virtual void defineDynamics(BlockIndicatorF3D<T>& indicator, int overlap, Dynamics<T,Lattice>* dynamics);
  // Define rho and u on a rectangular domain
  void defineRhoU ( int const x0_, int const x1_, int const y0_, int const y1_,
      int const z0_, int const z1_, T const rho, T const u[Lattice<T>::d]) override;
  void iniEquilibrium ( int const x0_, int const x1_, int const y0_, int const y1_,
      int const z0_, int const z1_, T const rho, T const u[Lattice<T>::d]) override;
  void defineForce( int const x0_, int const x1_, int const y0_, int const y1_,
        int const z0_, int const z1_, T const force[Lattice<T>::d]);
  using BlockLatticeStructure3D<T, Lattice>::iniEquilibrium;
  using BlockLatticeStructure3D<T, Lattice>::defineRhoU;

  /// Apply collision step to the whole domain
  template<typename FluidDynamics>
  void collide();
  /// Field collision step
  // template<template FluidDynamics, template<typename> class Memory>
  // void collide(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler)
  /// Field collision step using read and write from communication Buffers
  template<typename FluidDynamics>
  void fieldCollision();

  // template<typename FluidDynamics,template<typename> class Memory>
  // void fieldCollision(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler)
  /// Boundary collision step
  void boundaryCollision();

  // template<template<typename> class Memory>
  // void boundaryCollision(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler)
  /// Apply first collision, then streaming step to the whole domain
  template<typename FluidDynamics>
  void collideAndStream();

  // template<typename FluidDynamics,template<typename> class Memory, typename Callable>
  // void collideAndStream(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler,Callable& call);
  /// Apply the streaming.
  void stream();

  template<template<typename> class Memory>
  void readFromCommBuffer(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler);

  template<template<typename> class Memory>
  void writeToCommBuffer(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler);

#ifdef ENABLE_CUDA
  template<typename FluidDynamics>
  void fieldCollisionGPU();

  template<typename FluidDynamics>
  void fieldCollisionBBGPU();

  void boundaryCollisionGPU();

  void streamGPU();

  void postProcessGPU();

  void syncStreamsGPU();

#ifdef __CUDACC__
  void holdInputStream(cudaStream_t& inputStream);
  void waitOnInputStream(cudaStream_t& inputStream, cudaEvent_t& inputEvent);
#endif
  /// Apply first collision, then streaming step to the whole domain
  template<typename FluidDynamics>
  void collideAndStreamGPU();

  template<typename FluidDynamics>
  void collideAndStreamCachedGPU();

  //collide and stream without cudaDeviceSynchronize; user should call cudaDeviceSynchronize after this call sometime
  template<typename FluidDynamics>
  void collideAndStreamAsyncGPU();

  void executeTestFieldKernel(int indicator);
#endif
  /// Copy CPU data to GPU
  void copyDataToGPU();
  /// Copy GPU data to CPU
  void copyDataToCPU();
  /// Copy GPU viz data only to CPU
  void copyVizDataToCPU(bool copyRho, bool copyU, bool copyForce, bool copyFluidMask);
  // Copy fluidMask to GPU
  void copyFluidMaskToGPU();

  /// Compute the average density within a rectangular domain
  T computeAverageDensity(int x0_, int x1_, int y0_, int y1_,
                                  int z0_, int z1_ ) const override;
  /// Compute the average density within the whole domain
  T computeAverageDensity() const override;
  /// Compute components of the stress tensor on the cell.
  void computeStress(int iX, int iY, int iZ, T pi[util::TensorVal<Lattice<T> >::n]) override;
  /// Subtract a constant offset from the density within the whole domain
  void stripeOffDensityOffset (
    int x0_, int x1_, int y0_, int y1_, int z0_, int z1_, T offset ) override;
  /// Subtract a constant offset from the density within a rect. domain
  void stripeOffDensityOffset(T offset) override;
  /// Apply an operation to all cells of a sub-domain
  void forAll(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_,
                      WriteCellFunctional<T,Lattice> const& application) override;
  /// Apply an operation to all cells
  void forAll(WriteCellFunctional<T,Lattice> const& application) override;
  /// Add a non-local post-processing step
  void addPostProcessor (
    PostProcessorGenerator3D<T,Lattice> const& ppGen ) override;
  /// Clean up all non-local post-processing steps
  void resetPostProcessors() override;
  /// Execute post-processing on a sub-lattice
//  void postProcess(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_) override;
  /// Execute post-processing steps
  void postProcess() override;
  /// Add a non-local post-processing step
  void addLatticeCoupling (
    LatticeCouplingGenerator3D<T,Lattice> const& lcGen,
    std::vector<SpatiallyExtendedObject3D*> partners ) override;
  /// Execute couplings on a sub-lattice
  void executeCoupling(int x0_, int x1_, int y0_, int y1_, int z0_, int z1_) override;
  /// Execute couplings steps
  void executeCoupling() override;
  /// Subscribe postProcessors for reduction operations
  void subscribeReductions(Reductor<T>& reductor) override;
  /// Return a handle to the LatticeStatistics object
  LatticeStatistics<T>& getStatistics() override;
  /// Return a constant handle to the LatticeStatistics object
  LatticeStatistics<T> const& getStatistics() const override;

  SpatiallyExtendedObject3D* getComponent(int iBlock) override;
  SpatiallyExtendedObject3D const* getComponent(int iBlock) const override;
  multiPhysics::MultiPhysicsId getMultiPhysicsId() const override;

  /// Number of data blocks for the serializable interface
  std::size_t getNblock() const override;
  /// Binary size for the serializer
  std::size_t getSerializableSize() const override;
  /// Return a pointer to the memory of the current block and its size for the serializable interface
  bool* getBlock(std::size_t iBlock, std::size_t& sizeBlock, bool loadingMode) override;

  DynamicsDataHandler<T>* getDataHandler(Dynamics<T, Lattice>* dynamics) {
      return dynamicsContainer.getDataHandler(dynamics);
  }

  long long getCellIndex(int iX, int iY, int iZ) const;

  template<template<typename V,template <typename W> class> class Functor>
  void writeBlockLatticeByFunctor(const typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells);

  template<template<typename V,template <typename W> class> class Functor>
  void readBlockLatticeByFunctor(typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells);

  template<typename Callable, typename ... ARGS>
  void apply (Callable call, size_t loopLength, ARGS ... args)
  {
#ifdef ENABLE_CUDA
  GPUHandler<T,Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
  gpuHandler.apply(call,loopLength,args...);
#else
  for (size_t index=0;index<loopLength;++index)
     call(getData(),index,args...);
#endif
  }

private:
  /// Release memory for post processors
  void clearPostProcessors();
  /// Release memory for post processors
  void clearLatticeCouplings();
  /// Make the lattice periodic in all directions
  void makePeriodic();

  void periodicSurface(int x0, int x1, int y0, int y1, int z0, int z1);

};

}  // namespace olb

#endif
