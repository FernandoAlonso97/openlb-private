/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006-2008 Jonas Latt
 *  OMP parallel code by Mathias Krause, Copyright (C) 2007
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The dynamics of a 3D block lattice -- generic implementation.
 */
#ifndef BLOCK_LATTICE_3D_HH
#define BLOCK_LATTICE_3D_HH

#include <algorithm>

#include "util.h"
#include "blockLattice3D.h"
#include "functors/lattice/indicator/blockIndicatorF3D.h"
#include "dynamics/dynamics.h"
#include "dynamics/lbHelpers.h"
#include "communication/loadBalancer.h"
#include "communication/blockLoadBalancer.h"


#include "core/cell.hh"
#include "postProcessing.hh"
#include "blockLatticeStructure3D.hh"
#include "latticeStatistics.hh"
#include "serializer.hh"
#include "dynamicsContainer3D.hh"
#include "cellBlockData.hh"

namespace olb {

////////////////////// Class BlockLattice3D /////////////////////////

/** \param nx_ lattice width (first index)
 *  \param ny_ lattice height (second index)
 *  \param nz_ lattice depth (third index)
 */
template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>::BlockLattice3D(int nx, int ny, int nz, Dynamics<T,Lattice>* fluidDynamics)
  : BlockLatticeStructure3D<T,Lattice>(nx,ny,nz),
    cellData(new CellBlockData<T,Lattice>(nx, ny, nz)),
    dynamicsContainer(nx, ny, nz, cellData, fluidDynamics)
{
  resetPostProcessors();
#ifdef PARALLEL_MODE_OMP
  statistics = new LatticeStatistics<T>* [3*omp.get_size()];
  #pragma omp parallel
  {
    statistics[omp.get_rank() + omp.get_size()]
      = new LatticeStatistics<T>;
    statistics[omp.get_rank()] = new LatticeStatistics<T>;
    statistics[omp.get_rank() + 2*omp.get_size()]
      = new LatticeStatistics<T>;
  }
#else
  statistics = new LatticeStatistics<T>;
  statistics->initialize();
#endif
}

/** \param nx_ lattice width (first index)
 *  \param ny_ lattice height (second index)
 *  \param nz_ lattice depth (third index)
 */
template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>::BlockLattice3D(int nx, int ny, int nz, Dynamics<T,Lattice>* fluidDynamics, CellBlockDataALE<T,Lattice>* cellDataALE)
  : BlockLatticeStructure3D<T,Lattice>(nx,ny,nz),
    cellData(cellDataALE),
    dynamicsContainer(nx, ny, nz, cellData, fluidDynamics)

{
  cellDataALE = nullptr;
  resetPostProcessors();
#ifdef PARALLEL_MODE_OMP
  statistics = new LatticeStatistics<T>* [3*omp.get_size()];
  #pragma omp parallel
  {
    statistics[omp.get_rank() + omp.get_size()]
      = new LatticeStatistics<T>;
    statistics[omp.get_rank()] = new LatticeStatistics<T>;
    statistics[omp.get_rank() + 2*omp.get_size()]
      = new LatticeStatistics<T>;
  }
#else
  statistics = new LatticeStatistics<T>;
  statistics->initialize();
#endif
}

template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>::BlockLattice3D(const SubDomainInformation<T,Lattice<T>>& subDomainInfo, Dynamics<T,Lattice>* fluidDynamics, CellBlockDataALE<T,Lattice>* cellDataALE)
  : BlockLatticeStructure3D<T,Lattice>(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1],subDomainInfo.localGridSize()[2]),
    cellData(cellDataALE),
    dynamicsContainer(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1],subDomainInfo.localGridSize()[2], cellData, fluidDynamics)

{
  ghostLayer[0]=(subDomainInfo.ghostLayer[0]);
  ghostLayer[1]=(subDomainInfo.ghostLayer[1]);
  ghostLayer[2]=(subDomainInfo.ghostLayer[2]);
  cellDataALE = nullptr;
  resetPostProcessors();
#ifdef PARALLEL_MODE_OMP
  statistics = new LatticeStatistics<T>* [3*omp.get_size()];
  #pragma omp parallel
  {
    statistics[omp.get_rank() + omp.get_size()]
      = new LatticeStatistics<T>;
    statistics[omp.get_rank()] = new LatticeStatistics<T>;
    statistics[omp.get_rank() + 2*omp.get_size()]
      = new LatticeStatistics<T>;
  }
#else
  statistics = new LatticeStatistics<T>;
  statistics->initialize();
#endif
}

template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>::BlockLattice3D(const SubDomainInformation<T,Lattice<T>>& subDomainInfo,Dynamics<T,Lattice>* fluidDynamics)
  : BlockLatticeStructure3D<T,Lattice>(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1],subDomainInfo.localGridSize()[2]),
    cellData(new CellBlockData<T,Lattice>(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1],subDomainInfo.localGridSize()[2])),
    dynamicsContainer(subDomainInfo.localGridSize()[0],subDomainInfo.localGridSize()[1],subDomainInfo.localGridSize()[2], cellData, fluidDynamics)
{

    ghostLayer[0]=subDomainInfo.ghostLayer[0];
    ghostLayer[1]=subDomainInfo.ghostLayer[1];
    ghostLayer[2]=subDomainInfo.ghostLayer[2];

  resetPostProcessors();
#ifdef PARALLEL_MODE_OMP
  statistics = new LatticeStatistics<T>* [3*omp.get_size()];
  #pragma omp parallel
  {
    statistics[omp.get_rank() + omp.get_size()]
      = new LatticeStatistics<T>;
    statistics[omp.get_rank()] = new LatticeStatistics<T>;
    statistics[omp.get_rank() + 2*omp.get_size()]
      = new LatticeStatistics<T>;
  }
#else
  statistics = new LatticeStatistics<T>;
  statistics->initialize();
#endif
}

/** During destruction, the memory for the lattice and the contained
 * cells is released. However, the dynamics objects pointed to by
 * the cells must be deleted manually by the user.
 */
template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>::~BlockLattice3D()
{
  clearPostProcessors();
  clearLatticeCouplings();
#ifdef PARALLEL_MODE_OMP
  #pragma omp parallel
  {
    delete statistics[omp.get_rank()];
  }
  delete statistics;
#else
  delete statistics;
#endif
  delete cellData;
}

/** The whole data of the lattice is duplicated. This includes
 * both particle distribution function and external fields.
 * \warning The dynamics objects and postProcessors are not copied
 * \param rhs the lattice to be duplicated
 */
template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>::BlockLattice3D(BlockLattice3D<T,Lattice> const& rhs)
  :  BlockLatticeStructure3D<T,Lattice>(rhs._nx,rhs._ny,rhs._nz),
     cellData(rhs.cellData),
     dynamicsContainer(rhs.dynamicsContainer)
{
  resetPostProcessors();
  cellData = rhs.cellData;
  #ifdef PARALLEL_MODE_OMP
    statistics = new LatticeStatistics<T>* [3*omp.get_size()];
    #pragma omp parallel
    {
      statistics[omp.get_rank() + omp.get_size()]
        = new LatticeStatistics<T>;
      statistics[omp.get_rank()] = new LatticeStatistics<T> (**rhs.statistics);
      statistics[omp.get_rank() + 2*omp.get_size()]
        = new LatticeStatistics<T>;
    }
  #else
    statistics = new LatticeStatistics<T> (*rhs.statistics);
  #endif
}

/** The current lattice is deallocated, then the lattice from the rhs
 * is duplicated. This includes both particle distribution function
 * and external fields.
 * \warning The dynamics objects and postProcessors are not copied
 * \param rhs the lattice to be duplicated
 */
template<typename T, template<typename U> class Lattice>
BlockLattice3D<T,Lattice>& BlockLattice3D<T,Lattice>::operator= (
  BlockLattice3D<T,Lattice> const& rhs )
{
  BlockLattice3D<T,Lattice> tmp(rhs);
  swap(tmp);
  return *this;
}

/** The swap is efficient, in the sense that only pointers to the
 * lattice are copied, and not the lattice itself.
 */
template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::swap(BlockLattice3D& rhs)
{
  std::swap(this->_nx, rhs._nx);
  std::swap(this->_ny, rhs._ny);
  std::swap(this->_nz, rhs._nz);
  std::swap(cellData, rhs.cellData);
  postProcessors.swap(rhs.postProcessors);
  std::swap(statistics, rhs.statistics);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::initDataArrays()
{
  dynamicsContainer.init();

}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::initialize()
{
  postProcess();
}

template<typename T, template<typename U> class Lattice>
T* BlockLattice3D<T, Lattice>::getData(int iX, int iY, int iZ, int iData) const
{
  // std::cout << "getData at index " << getCellIndex(iX,iY,iZ) << " for ipop " << iData << " " << iX << " " << iY << " " << iZ << std::endl;
  return cellData->getCellData(iData, iX, iY, iZ);
}

template<typename T, template<typename U> class Lattice>
T* BlockLattice3D<T, Lattice>::getDataPrevious(int iX, int iY, int iZ, int iData) const
{
  return cellData->getCellDataPrevious(iData, iX, iY, iZ);
}
/** The dynamics object is not duplicated: all cells of the rectangular
 * domain point to the same dynamics.
 *
 * The dynamics object is not owned by the BlockLattice3D object, its
 * memory management must be taken care of by the user.
 */
template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::defineDynamics (
  int x0, int x1, int y0, int y1, int z0, int z1,
  Dynamics<T,Lattice>* dynamics )
{
  OLB_PRECONDITION(x0>=0 && x1<this->_nx);
  OLB_PRECONDITION(x1>=x0);
  OLB_PRECONDITION(y0>=0 && y1<this->_ny);
  OLB_PRECONDITION(y1>=y0);
  OLB_PRECONDITION(z0>=0 && z1<this->_nz);
  OLB_PRECONDITION(z1>=z0);

  dynamicsContainer.defineDynamics(x0, x1, y0, y1, z0, z1, dynamics);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::defineDynamics (
  int iX, int iY, int iZ, Dynamics<T,Lattice>* dynamics)
{
  OLB_PRECONDITION(iX>=0 && iX<this->_nx);
  OLB_PRECONDITION(iY>=0 && iY<this->_ny);
  OLB_PRECONDITION(iZ>=0 && iZ<this->_nz);

  dynamicsContainer.defineDynamics(iX, iY, iZ, dynamics);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::defineDynamics (
  BlockGeometryStructure3D<T>& blockGeometry, int material, Dynamics<T,Lattice>* dynamics)
{
    dynamicsContainer.defineDynamics(blockGeometry, material, dynamics);
}

// template<typename T, template<typename U> class Lattice>
// void BlockLattice3D<T,Lattice>::defineDynamics (
  // BlockIndicatorF3D<T>& indicator, int overlap, Dynamics<T,Lattice>* dynamics)
// {
    // dynamicsContainer.defineDynamics(indicator, overlap, dynamics);
// }

template<typename T, template<typename U> class Lattice>
Dynamics<T,Lattice>* BlockLattice3D<T,Lattice>::getDynamics (
        int iX, int iY, int iZ)
{
    return dynamicsContainer.getDynamics(iX,iY,iZ);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::defineRhoU ( int const x0, int const x1, int const y0, int const y1,
    int const z0, int const z1, T const rho, T const u[Lattice<T>::d])
{
  OLB_PRECONDITION(x0>=0 && x1<this->_nx);
  OLB_PRECONDITION(x1>=x0);
  OLB_PRECONDITION(y0>=0 && y1<this->_ny);
  OLB_PRECONDITION(y1>=y0);
  OLB_PRECONDITION(z0>=0 && z1<this->_nz);
  OLB_PRECONDITION(z1>=z0);

  for ( int iX=x0; iX<=x1; ++iX ) {
    for ( int iY=y0; iY<=y1; ++iY ) {
        for( int iZ=z0; iZ<=z1; ++iZ) {
            Dynamics<T,Lattice>* dynamics = getDynamics(iX,iY,iZ);
            dynamics->defineRhoU(dynamicsContainer.getDataHandler(dynamics), cellData->getCellData(), util::getCellIndex3D(iX, iY, iZ,this->_ny, this->_nz), rho, u );
        }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::iniEquilibrium( int const x0, int const x1, int const y0, int const y1,
    int const z0, const int z1, T const rho, T const u[Lattice<T>::d])
{
    OLB_PRECONDITION(x0>=0 && x1<this->_nx);
    OLB_PRECONDITION(x1>=x0);
    OLB_PRECONDITION(y0>=0 && y1<this->_ny);
    OLB_PRECONDITION(y1>=y0);
    OLB_PRECONDITION(z0>=0 && z1<this->_nz);
    OLB_PRECONDITION(z1>=z0);

    for ( int iX=x0; iX<=x1; ++iX ) {
      for ( int iY=y0; iY<=y1; ++iY ) {
        for( int iZ=z0; iZ<=z1; ++iZ) {
            getDynamics(iX,iY,iZ)->iniEquilibrium(cellData->getCellData(), util::getCellIndex3D(iX, iY, iZ,this->_ny, this->_nz), rho, u );
        }
      }
    }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::defineForce ( int const x0, int const x1, int const y0, int const y1,
    int const z0, int const z1, T const force[Lattice<T>::d])
{
  OLB_PRECONDITION(x0>=0 && x1<this->_nx);
  OLB_PRECONDITION(x1>=x0);
  OLB_PRECONDITION(y0>=0 && y1<this->_ny);
  OLB_PRECONDITION(y1>=y0);
  OLB_PRECONDITION(z0>=0 && z1<this->_nz);
  OLB_PRECONDITION(z1>=z0);

  for ( int iX=x0; iX<=x1; ++iX ) {
    for ( int iY=y0; iY<=y1; ++iY ) {
        for( int iZ=z0; iZ<=z1; ++iZ) {
            get(iX,iY,iZ).defineExternalField(Lattice<T>::forceIndex,Lattice<T>::d, force);
        }
    }
  }
}


template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::collide()
{
  fieldCollision<FluidDynamics>();

  boundaryCollision();
}


template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::fieldCollision()
{
  T** fieldData = cellData->getCellData();
  DynamicsDataHandler<T>* dataHandler = dynamicsContainer.getDataHandler(dynamicsContainer.getFluidDynamics());
  T** momentaData = nullptr;
  if (dataHandler)
    momentaData = dataHandler->getDynamicsData();
  T* collisionData = dynamicsContainer.getFluidDynamics()->getCollisionData();

  size_t size = this->_nx*this->_ny*this->_nz;

  bool* mask = cellData->getFluidMask();
  for(size_t cellId = 0; cellId < size; ++cellId)
  {
    if (mask[cellId]) {
      FluidDynamics::collision(fieldData, cellId, momentaData, 0, collisionData);
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::boundaryCollision()
{
  T** fieldData = cellData->getCellData();

  for (auto& itr : dynamicsContainer.getBoundaryDynamicsSet())
  {
    // std::cout << "######################################################" <<std::endl;
    // std::cout << "running dynamics " << itr << " for a number of points " << dynamicsContainer.getDataHandler(itr)->getCellIDs().size() << std::endl;
    itr->dispatchCollisionCPU(dynamicsContainer.getDataHandler(itr), fieldData);
  }
}

/** At the end of this method, the post-processing steps are automatically
 * invoked. **/
template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::collideAndStream()
{
  collide<FluidDynamics>();

  stream();

  postProcess();

  getStatistics().incrementTime();
}

// template<typename T, template<typename U> class Lattice>
// template<typename FluidDynamics,template<typename> class Memory, typename Callable>
// void BlockLattice3D<T,Lattice>::collideAndStream(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler,Callable& call)
// {

  // collide<FluidDynamics>();

  // writeToCommBuffer(commDataHandler);

  // call();

  // readFromCommBuffer(commDataHandler);

  // stream();

  // postProcess();

  // getStatistics().incrementTime();
// }

template<typename T, template<typename U> class Lattice>
template<template<typename> class Memory>
void BlockLattice3D<T,Lattice>::readFromCommBuffer(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler)
{

  for (unsigned population=0;population < Lattice<T>::q; ++population)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[Lattice<T>::opposite(population)])
    {
    
      int direction[3] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      for(unsigned dim=0;dim<Lattice<T>::d;++dim)
      {
        direction[dim] =  Lattice<T>::c(Lattice<T>::opposite(population),dim);
      }
   
     int iXS = std::numeric_limits<int>::max();
     int iYS = std::numeric_limits<int>::max();
     int iZS = std::numeric_limits<int>::max();
     int iXE = std::numeric_limits<int>::max();
     int iYE = std::numeric_limits<int>::max();
     int iZE = std::numeric_limits<int>::max();

     switch (direction[0]) 
     {
      case 1:
        iXS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case 0:
        iXS = 0; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case -1:
        iXS = 0;
        iXE = 0;
        break;
     }
     switch (direction[1]) 
     {
      case 1:
        iYS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case 0:
        iYS = 0; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case -1:
        iYS = 0;
        iYE = 0;
        break;
     }
     switch (direction[2]) 
     {
      case 1:
        iZS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        break;

      case 0:
        iZS = 0; 
        iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        break;

      case -1:
        iZS = 0;
        iZE = 0;
        break;
     }




      for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      {
       // std::cout << "iteration " << iPop << std::endl;
       if (commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS; int iZSS=iZS;
         int iXEE=iXE; int iYEE=iYE; int iZEE=iZE;
         int latticeOffset[3] = {direction[0] + Lattice<T>::c(iPop,0),direction[1] + Lattice<T>::c(iPop,1),direction[2] + Lattice<T>::c(iPop,2)};
         latticeOffset[0] <0 ? iXSS-=latticeOffset[0] : iXEE-=latticeOffset[0];
         latticeOffset[1] <0 ? iYSS-=latticeOffset[1] : iYEE-=latticeOffset[1];
         latticeOffset[2] <0 ? iZSS-=latticeOffset[2] : iZEE-=latticeOffset[2];

         // std::cout << "latticeOffset in read " << latticeOffset[0]  << " " << latticeOffset[1] << " " << latticeOffset[2] << " for pop " << iPop << " s-e " << iXSS << " " << iYSS << " " << iZSS << " to " << iXEE << " " << iYEE << " " << iZEE << std::endl;
         unsigned iterator = 0;
         for (int iX = iXSS; iX<=iXEE;++iX)
          for (int iY = iYSS; iY<=iYEE;++iY)
            for (int iZ = iZSS; iZ<=iZEE;++iZ)
            {
            
              // if (iterator < abs(util::getGridOffset<Lattice<T>>(iPop,this->_ny, this->_nz)))
              {
              // *getDataPrevious(iX+Lattice<T>::c(iPop,0),iY+Lattice<T>::c(iPop,1),iZ+Lattice<T>::c(iPop,2),iPop) = commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData[iterator];
             *getData(iX-Lattice<T>::c(population,0),iY-Lattice<T>::c(population,1),iZ-Lattice<T>::c(population,2),Lattice<T>::opposite(iPop)) =  commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData[iterator];
              // std::cout << "id " << iX << " " << iY << " " << iZ << " values " <<  commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData[iterator] << std::endl;
              ++iterator;
              }
              // if (iterator-1 ==33)
      // std::cout << "read for domain " << commDataHandler.domainInfo.localSubDomain << " for neighbour " << population <<" population " << iPop << " values(IN)" <<  commDataHandler.getCommunicationDataIn()[population].popArrays[iPop].popData[iterator-1] << " value post read in field " << *getData(iX,iY,iZ,Lattice<T>::opposite(iPop))  << " in loop with start " << iXS << " " << iYS << " " << iZS << " and end " << iXE << " " << iYE << " " << iZE << " at index " << iX << " " << iY << " " << iZ <<  " cellIndex " << getCellIndex(iX,iY,iZ) << std::endl;
            }
       }
      }
    }
  }

}

template<typename T, template<typename U> class Lattice>
template<template<typename> class Memory>
void BlockLattice3D<T,Lattice>::writeToCommBuffer(CommunicationDataHandler<T,Lattice<T>,Memory>& commDataHandler)
{

  for (unsigned neighbour=0;neighbour < Lattice<T>::q; ++neighbour)
  {
    if (commDataHandler.domainInfo.getLocalInfo().hasNeighbour[neighbour])
    {
    
      int direction[3] = {std::numeric_limits<int>::max(),std::numeric_limits<int>::max(),std::numeric_limits<int>::max()};
      for(unsigned dim=0;dim<Lattice<T>::d;++dim)
      {
        direction[dim] =  Lattice<T>::c(neighbour,dim);
      }
   
     int iXS = std::numeric_limits<int>::max();
     int iYS = std::numeric_limits<int>::max();
     int iZS = std::numeric_limits<int>::max();
     int iXE = std::numeric_limits<int>::max();
     int iYE = std::numeric_limits<int>::max();
     int iZE = std::numeric_limits<int>::max();

     switch (direction[0]) 
     {
      case 1:
        iXS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case 0:
        iXS = 0; 
        iXE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[0]-1; 
        break;

      case -1:
        iXS = 0;
        iXE = 0;
        break;
     }
     switch (direction[1]) 
     {
      case 1:
        iYS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case 0:
        iYS = 0; 
        iYE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[1]-1; 
        break;

      case -1:
        iYS = 0;
        iYE = 0;
        break;
     }
     switch (direction[2]) 
     {
      case 1:
        iZS = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        break;

      case 0:
        iZS = 0; 
        iZE = commDataHandler.domainInfo.getLocalInfo().localGridSize().index[2]-1; 
        break;

      case -1:
        iZS = 0;
        iZE = 0;
        break;
     }


      for (unsigned iPop=0;iPop<Lattice<T>::q;++iPop)
      {
       if (commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData)
       {
         int iXSS=iXS; int iYSS=iYS; int iZSS=iZS;
         int iXEE=iXE; int iYEE=iYE; int iZEE=iZE;
         int latticeOffset[3] = {-direction[0] + Lattice<T>::c(iPop,0),-direction[1] + Lattice<T>::c(iPop,1),-direction[2] + Lattice<T>::c(iPop,2)};
         latticeOffset[0] <0 ? iXSS-=latticeOffset[0] : iXEE-=latticeOffset[0];
         latticeOffset[1] <0 ? iYSS-=latticeOffset[1] : iYEE-=latticeOffset[1];
         latticeOffset[2] <0 ? iZSS-=latticeOffset[2] : iZEE-=latticeOffset[2];

         // std::cout << "latticeOffset in write " << latticeOffset[0]  << " " << latticeOffset[1] << " " << latticeOffset[2] << " for pop " << iPop << " s-e " << iXSS << " " << iYSS << " " << iZSS << " to " << iXEE << " " << iYEE << " " << iZEE << std::endl;
         unsigned iterator = 0;
         for (unsigned iX = iXSS; iX<=iXEE;++iX)
          for (unsigned iY = iYSS; iY<=iYEE;++iY)
            for (unsigned iZ = iZSS; iZ<=iZEE;++iZ)
            {
                // unsigned iXN = iX-iXS; 
                // unsigned iYN = iY-iYS; 
                // unsigned iZN = iZ-iZS; 
               commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData[iterator]
               = *getData(iX,iY,iZ,Lattice<T>::opposite(iPop)); // opposite bc of revert after collision
               ++iterator;
               // std::cout << typeid(*getData(iX,iY,iZ,iPop)).name() << " " << typeid(
               // commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData[util::getCellIndex3D(iX, iY, iZ,
               // commDataHandler.getCommunicationDataOut()[neighbour].size[1],commDataHandler.getCommunicationDataOut()[neighbour].size[2])]).name() << std::endl;

               // if (iterator-1 == 33)
      // std::cout << "write for domain " << commDataHandler.domainInfo.localSubDomain << " for neighbour " << neighbour <<" population " << iPop << " values(OUT) " <<  commDataHandler.getCommunicationDataOut()[neighbour].popArrays[iPop].popData[iterator-1] << " in loop with start " << iXS << " " << iYS << " " << iZS << " and end " << iXE << " " << iYE << " " << iZE << " with fluid mask " << getMaskEntry(iX,iY,iZ) << " " << iX << " " << iY << " " << iZ <<  std::endl;

            }
       }
      }
    }
  }


}

#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::fieldCollisionGPU()
{
  dynamicsContainer.getGPUHandler().template executeFluidKernel<FluidDynamics>(this->_nx*this->_ny*this->_nz,
      dynamicsContainer.getFluidDynamics());
}

template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::fieldCollisionBBGPU()
{
  dynamicsContainer.getGPUHandler().template executeFluidKernelBounceBack<FluidDynamics>(this->_nx*this->_ny*this->_nz,
      dynamicsContainer.getFluidDynamics());
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::boundaryCollisionGPU()
{
  size_t position = 1;

  GPUHandler<T, Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
  for (auto& itr : dynamicsContainer.getBoundaryDynamicsSet())
  {
    size_t size = dynamicsContainer.getDataHandler(itr)->getCellIDs().size();
    // std::cout << "dispatch size" << size << std::endl;
    itr->dispatchCollisionGPU(gpuHandler, position, size);
    ++position;
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::streamGPU()
{
  cellData->gpuStream();
}

/** At the end of this method, the post-processing steps are automatically
 * invoked. */
template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::collideAndStreamGPU()
{

  fieldCollisionGPU<FluidDynamics>();

  boundaryCollisionGPU();

  cudaDeviceSynchronize();
#ifdef OLB_DEBUG 
  HANDLE_ERROR(cudaPeekAtLastError());
#endif

  streamGPU();

  cudaDeviceSynchronize();
#ifdef OLB_DEBUG 
  HANDLE_ERROR(cudaPeekAtLastError());
#endif
  postProcessGPU();

  cudaDeviceSynchronize();
#ifdef OLB_DEBUG 
  HANDLE_ERROR(cudaPeekAtLastError());
#endif
}

template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::collideAndStreamCachedGPU()
{

    fieldCollisionBBGPU<FluidDynamics>();

  boundaryCollisionGPU();

  cudaDeviceSynchronize();

  streamGPU();

  cudaDeviceSynchronize();

  postProcessGPU();

  cudaDeviceSynchronize();
}

template<typename T, template<typename U> class Lattice>
template<typename FluidDynamics>
void BlockLattice3D<T,Lattice>::collideAndStreamAsyncGPU()
{

  fieldCollisionGPU<FluidDynamics>();
  boundaryCollisionGPU();
  streamGPU();

  syncStreamsGPU();

  postProcessGPU();

  syncStreamsGPU();
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::executeTestFieldKernel(int indicator)
{
    dynamicsContainer.getGPUHandler().executeTestFieldKernel(this->_nx*this->_ny*this->_nz,indicator);
}
#endif

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::copyDataToGPU()
{
#ifdef ENABLE_CUDA
  cellData->copyCellDataToGPU();
  for (auto& itr : dynamicsContainer.getBoundaryDynamicsSet())
  {
    dynamicsContainer.getGPUHandler().transferToGPU(itr, dynamicsContainer.getDataHandler(itr));
  }
#endif
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::copyDataToCPU()
{
#ifdef ENABLE_CUDA
  cellData->copyCellDataToCPU();
  for (auto& itr : dynamicsContainer.getBoundaryDynamicsSet())
  {
    dynamicsContainer.getGPUHandler().transferToCPU(itr, dynamicsContainer.getDataHandler(itr));
  }
#endif
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::copyVizDataToCPU(bool copyRho, bool copyU, bool copyForce, bool copyFluidMask)
{
#ifdef ENABLE_CUDA
  cellData->copyVizDataToCPU(copyRho, copyU, copyForce, copyFluidMask);
#endif
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::copyFluidMaskToGPU()
{
#ifdef ENABLE_CUDA
  cellData->copyFluidMaskToGPU();
  cudaDeviceSynchronize();
#endif

}


template<typename T, template<typename U> class Lattice>
T BlockLattice3D<T,Lattice>::computeAverageDensity (
  int x0, int x1, int y0, int y1, int z0, int z1) const
{
  T sumRho = T();
  for (int iX=x0; iX<=x1; ++iX) {
    for (int iY=y0; iY<=y1; ++iY) {
      for (int iZ=z0; iZ<=z1; ++iZ) {
        T rho, u[Lattice<T>::d];
        get(iX,iY,iZ).computeRhoU(rho, u);
        sumRho += rho;
      }
    }
  }
  return sumRho / (T)(x1-x0+1) / (T)(y1-y0+1) / (T)(z1-z0+1);
}

template<typename T, template<typename U> class Lattice>
T BlockLattice3D<T,Lattice>::computeAverageDensity() const
{
  return computeAverageDensity(0, this->_nx-1, 0, this->_ny-1, 0, this->_nz-1);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::computeStress(int iX, int iY, int iZ,
T pi[util::TensorVal<Lattice<T> >::n])
{
    // TODO
//    grid[iX][iY][iZ].computeStress(pi);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::stripeOffDensityOffset (
  int x0, int x1, int y0, int y1, int z0, int z1, T offset )
{
  for (int iX=x0; iX<=x1; ++iX) {
    for (int iY=y0; iY<=y1; ++iY) {
      for (int iZ=z0; iZ<=z1; ++iZ) {
        //if (offset<-42000.) {
        //T rho = get(iX,iY,iZ).computeRho();
        // if (rho<0) {
        //for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
        //  if (get(iX,iY,iZ)[iPop] + Lattice<T>::t[iPop] < T() ) {
        //    get(iX,iY,iZ)[iPop] = -Lattice<T>::t[iPop]+0.0000001;
        //  }
        //  else if(rho>1.)
        // get(iX,iY,iZ)[iPop] -= Lattice<T>::t[iPop] * (rho-1.);
        //}
        //}
        //}
        //else {
        // only stripe off if rho stays positive
        //if (get(iX,iY,iZ).computeRho()>offset) {
        for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
          get(iX,iY,iZ)[iPop] -= Lattice<T>::t(iPop) * offset;
        }
        // }
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::stripeOffDensityOffset(T offset)
{
  stripeOffDensityOffset(0, this->_nx-1, 0, this->_ny-1, 0, this->_nz-1, offset);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::forAll (
  int x0, int x1, int y0, int y1, int z0, int z1,
  WriteCellFunctional<T,Lattice> const& application )
{
  for (int iX=x0; iX<=x1; ++iX) {
    for (int iY=y0; iY<=y1; ++iY) {
      for (int iZ=z0; iZ<=z1; ++iZ) {
        int pos[] = {iX, iY, iZ};
        application.apply( get(iX,iY,iZ), pos );
      }
    }
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::forAll(WriteCellFunctional<T,Lattice> const& application)
{
  forAll(0, this->_nx-1, 0, this->_ny-1, 0, this->_nz-1, application);
}


template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::addPostProcessor (
  PostProcessorGenerator3D<T,Lattice> const& ppGen )
{
  postProcessors.push_back(ppGen.generate());
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::resetPostProcessors()
{
  clearPostProcessors();
  StatPPGenerator3D<T,Lattice> statPPGenerator;
  addPostProcessor(statPPGenerator);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::clearPostProcessors()
{
  typename std::vector<PostProcessor3D<T,Lattice>*>::iterator ppIt = postProcessors.begin();
  for (; ppIt != postProcessors.end(); ++ppIt) {
    delete *ppIt;
  }
  postProcessors.clear();
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::postProcess()
{

  T** fieldData = cellData->getCellData();

  for (auto& itr : dynamicsContainer.getBoundaryDynamicsSet())
  {
      itr->dispatchPostProcCPU(dynamicsContainer.getDataHandler(itr), fieldData, this->_nx, this->_ny, this->_nz);
  }

}

#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::postProcessGPU()
{

  int position = 0;

  GPUHandler<T, Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
  for (auto& itr : dynamicsContainer.getBoundaryDynamicsSet())
  {
    size_t size = dynamicsContainer.getDataHandler(itr)->getCellIDs().size();
    itr->dispatchPostProcGPU(gpuHandler, position, size);
    ++position;
  }

}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T, Lattice>::syncStreamsGPU() {
  dynamicsContainer.getGPUHandler().syncStreams();
}

#ifdef __CUDACC__
template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T, Lattice>::holdInputStream(cudaStream_t& inputStream) {
    dynamicsContainer.getGPUHandler().holdInputStream(inputStream);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T, Lattice>::waitOnInputStream(cudaStream_t& inputStream, cudaEvent_t& inputEvent) {
    dynamicsContainer.getGPUHandler().waitOnInputStream(inputStream, inputEvent);
}
#endif

#endif

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::addLatticeCoupling (
  LatticeCouplingGenerator3D<T,Lattice> const& lcGen,
  std::vector<SpatiallyExtendedObject3D*> partners )
{
  latticeCouplings.push_back(lcGen.generate(partners));
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::executeCoupling()
{
  for (unsigned iPr=0; iPr<latticeCouplings.size(); ++iPr) {
    latticeCouplings[iPr] -> process(*this);
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::executeCoupling (
  int x0_, int x1_, int y0_, int y1_, int z0_, int z1_)
{
  for (unsigned iPr=0; iPr<latticeCouplings.size(); ++iPr) {
    latticeCouplings[iPr] -> processSubDomain(*this, x0_, x1_, y0_, y1_, z0_, z1_);
  }
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::clearLatticeCouplings()
{
  typename std::vector<PostProcessor3D<T,Lattice>*>::iterator ppIt = latticeCouplings.begin();
  for (; ppIt != latticeCouplings.end(); ++ppIt) {
    delete *ppIt;
  }
  latticeCouplings.clear();
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::subscribeReductions(Reductor<T>& reductor)
{
  for (unsigned iPr=0; iPr<postProcessors.size(); ++iPr) {
    postProcessors[iPr] -> subscribeReductions(*this, &reductor);
  }
}

template<typename T, template<typename U> class Lattice>
LatticeStatistics<T>& BlockLattice3D<T,Lattice>::getStatistics()
{
#ifdef PARALLEL_MODE_OMP
  return *statistics[omp.get_rank()];
#else
  return *statistics;
#endif
}

template<typename T, template<typename U> class Lattice>
LatticeStatistics<T> const&
BlockLattice3D<T,Lattice>::getStatistics() const
{
#ifdef PARALLEL_MODE_OMP
  return *statistics[omp.get_rank()];
#else
  return *statistics;
#endif
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::stream() {
  cellData->stream();
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::makePeriodic()
{
  static const int vicinity = Lattice<T>::vicinity;
  int maxX = this->getNx()-1;
  int maxY = this->getNy()-1;
  int maxZ = this->getNz()-1;
  periodicSurface(0,             vicinity-1,    0,    maxY,              0,       maxZ);
  periodicSurface(maxX-vicinity+1,     maxX,    0,    maxY,              0,       maxZ);
  periodicSurface(vicinity,      maxX-vicinity, 0,    vicinity-1,        0,       maxZ);
  periodicSurface(vicinity,      maxX-vicinity, maxY-vicinity+1, maxY,   0,       maxZ);
  periodicSurface(vicinity,      maxX-vicinity, vicinity, maxY-vicinity, 0, vicinity-1);
  periodicSurface(vicinity,      maxX-vicinity, vicinity, maxY-vicinity, maxZ-vicinity+1, maxZ);
}

template<typename T, template<typename U> class Lattice>
void BlockLattice3D<T,Lattice>::periodicSurface (
  int x0, int x1, int y0, int y1, int z0, int z1)
{
    // TODO
}

template<typename T, template<typename U> class Lattice>
SpatiallyExtendedObject3D* BlockLattice3D<T,Lattice>::getComponent(int iBlock)
{
  OLB_PRECONDITION( iBlock==0 );
  return this;
}

template<typename T, template<typename U> class Lattice>
SpatiallyExtendedObject3D const* BlockLattice3D<T,Lattice>::getComponent(int iBlock) const
{
  OLB_PRECONDITION( iBlock==0 );
  return this;
}

template<typename T, template<typename U> class Lattice>
multiPhysics::MultiPhysicsId BlockLattice3D<T,Lattice>::getMultiPhysicsId() const
{
  return multiPhysics::getMultiPhysicsBlockId<T,Lattice>();
}


template<typename T, template<typename U> class Lattice>
std::size_t BlockLattice3D<T,Lattice>::getNblock() const
{
    return 0; // TODO
//  return 3 + rawData[0].getNblock() * this->_nx * this->_ny * this->_nz;
}


template<typename T, template<typename U> class Lattice>
std::size_t BlockLattice3D<T,Lattice>::getSerializableSize() const
{
    return 0; // TODO
//  return 3 * sizeof(int) + rawData[0].getSerializableSize() * this->_nx * this->_ny * this->_nz;
}


template<typename T, template<typename U> class Lattice>
bool* BlockLattice3D<T,Lattice>::getBlock(std::size_t iBlock, std::size_t& sizeBlock, bool loadingMode)
{
  std::size_t currentBlock = 0;
  bool* dataPtr = nullptr;

  registerVar                      (iBlock, sizeBlock, currentBlock, dataPtr, this->_nx);
  registerVar                      (iBlock, sizeBlock, currentBlock, dataPtr, this->_ny);
  registerVar                      (iBlock, sizeBlock, currentBlock, dataPtr, this->_nz);
  // TODO
  return dataPtr;
}

template<typename T,template<typename U> class Lattice>
long long BlockLattice3D<T,Lattice>::getCellIndex(int iX, int iY, int iZ) const
{
  return util::getCellIndex3D(iX,iY,iZ,this->_ny,this->_nz);
}

template<typename T, template<typename U> class Lattice>
template<template<typename V,template <typename W> class> class Functor>
void BlockLattice3D<T,Lattice>::writeBlockLatticeByFunctor(const typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices, const size_t numberOfCells)
{
#ifdef ENABLE_CUDA
  GPUHandler<T, Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
  gpuHandler.template writeBlockLatticeByFunctor<Functor> (data,cellIndices,numberOfCells);
#endif
}
template<typename T, template<typename U> class Lattice>
template<template<typename V,template <typename W> class> class Functor>
void BlockLattice3D<T,Lattice>::readBlockLatticeByFunctor(typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices, const size_t numberOfCells)
{
#ifdef ENABLE_CUDA
  GPUHandler<T, Lattice>& gpuHandler = dynamicsContainer.getGPUHandler();
  gpuHandler.template readBlockLatticeByFunctor<Functor>(data,cellIndices,numberOfCells);
#endif
}

}  // namespace olb

#endif
