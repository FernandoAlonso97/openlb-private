//
// Created by Andreas Reiser on 28.10.18.
//

#include "blockLatticeALE2D.h"
#include "blockLatticeALE2D.hh"
#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"

namespace olb {
    template class BlockLatticeALE2D<double, descriptors::D2Q9Descriptor, ExternalFieldALE<double, descriptors::D2Q9Descriptor, ExternalFieldProviderF>>;
}