//
// Created by Andreas Reiser on 28.10.18.
//

#ifndef OPENLB_BLOCKLATTICEALE2D_H
#define OPENLB_BLOCKLATTICEALE2D_H

#include <vector>
#include <dynamics/lbHelpers.h>
#include "olbDebug.h"
#include "postProcessing.h"
#include "blockLatticeStructure2D.h"
#include "multiPhysics.h"
#include "cell.h"
#include "latticeStatistics.h"
#include "serializer.h"
#include "dynamicsContainer2D.h"
#include "blockLattice2D.h"
#include "externalFieldALE.h"


namespace olb {
    template<typename T, template<typename U> class Lattice, class ExternalField>
    class BlockLatticeALE2D : public BlockLattice2D<T, Lattice> {

    public:
      BlockLatticeALE2D(int nx, int ny, const Vec2<T>& anchor, Dynamics<T, Lattice> *dynamics, ExternalField& externalField);

      /**
       * Interpolates the populations at the new transformed mesh positions into the current interpolated field
       * Swaps the pointers to current collide/interpolated field at the end
       */
      void moveMesh(const Vec2<T>& translation, const T angle,
          const Vec2<T>& position, const T orientation);

#ifdef ENABLE_CUDA
      void moveMeshGPU(const Vec2<T>& translation, const T angle,
          const Vec2<T>& position, const T orientation);
#endif


    private:
      ExternalField& externalField_;
      Vec2<T> anchor_;
    };
}


#endif //OPENLB_BLOCKLATTICEALE2D_H
