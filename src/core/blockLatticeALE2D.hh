//
// Created by Andreas Reiser on 28.10.18.
//


#ifndef OPENLB_BLOCKLATTICEALE2D_HH
#define OPENLB_BLOCKLATTICEALE2D_HH

#include <algorithm>
#include "blockLattice2D.h"
#include "dynamics/dynamics.h"
#include "dynamics/lbHelpers.h"
#include "util.h"
#include "communication/loadBalancer.h"
#include "communication/blockLoadBalancer.h"
#include "functors/lattice/indicator/blockIndicatorF2D.h"
#include "config.h"
#include "gpuhandler.h"
#include "blockLatticeALE2D.h"
#include "externalFieldALE.h"

#include <iostream>
#include <cmath>

namespace olb {

template<typename T, template<typename U> class Lattice, class ExternalField>
BlockLatticeALE2D<T, Lattice, ExternalField>::BlockLatticeALE2D(int nx, int ny, const Vec2<T>& anchor, Dynamics<T, Lattice> *dynamics, ExternalField& externalField)
        : BlockLattice2D<T, Lattice>(nx, ny, dynamics, new CellBlockDataALE<T,Lattice>(nx,ny)),
                externalField_(externalField),
                anchor_(anchor)
                {}


template<typename T, template<typename U> class Lattice, class ExternalField>
void BlockLatticeALE2D<T, Lattice, ExternalField>::moveMesh(const Vec2<T>& translation, const T angle,
                                                            const Vec2<T>& position, const T orientation) {
  auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(this->cellData);

  const Affine2<T> movement = aleutil::movementTransform<T>(translation, angle, anchor_);
  const Affine2<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);
  const Affine2<T> velocityRotation = aleutil::velocityRotationTransform<T>(angle, orientation);


  externalField_.preTimeStepCalculations(velocityRotation, positionInExternalField);

  for (size_t cellIndex = 0; cellIndex < this->_nx * this->_ny; ++cellIndex) {
    aleutil::interpolateCellPopulations2D<T, Lattice, ExternalField>(
        cellDataALE->getCellData(),
            cellDataALE->getCellDataALE(),
            movement,
            externalField_,
            cellIndex,
            this->_nx,
            this->_ny);
  }

  cellDataALE->moveMesh();
}
    

#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice, class ExternalField>
void BlockLatticeALE2D<T, Lattice, ExternalField>::moveMeshGPU(const Vec2<T>& translation, const T angle,
                                                            const Vec2<T>& position, const T orientation) {

  const Affine2<T> movement = aleutil::movementTransform<T>(translation, angle, anchor_);
  const Affine2<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);
  const Affine2<T> velocityRotation = aleutil::velocityRotationTransform<T>(angle, orientation);

  externalField_.preTimeStepCalculations(velocityRotation, positionInExternalField);

  this->dynamicsContainer.getGPUHandler().template executeMoveMeshKernel2D<ExternalField>(movement, externalField_);
  cudaDeviceSynchronize();
  static_cast<CellBlockDataALE<T, Lattice> *>(this->cellData)->gpuMoveMesh();
}
#endif
}


#endif
