//
// Created by Andreas Reiser on 2018-11-28.
//

#include "blockLatticeALE3D.h"
#include "blockLattice3D.h"
#include "blockLattice3D.hh"
#include "dynamics/latticeDescriptors.h"
#include "dynamics/latticeDescriptors.hh"

namespace olb {

    template class BlockLatticeALE3D<double, descriptors::D3Q19Descriptor, ExternalFieldALE<double, descriptors::D3Q19Descriptor, ExternalFieldProviderF>>;

}