//
// Created by Andreas Reiser on 2018-11-28.
//

#ifndef OPENLB_BLOCKLATTICEALE3D_H
#define OPENLB_BLOCKLATTICEALE3D_H

#include "affineTransform.h"
#include "ALEutil.h"
//#include "blockLatticeStructure3D.h"
#include "blockLattice3D.h"
#include "core/cell.h"
#include "dynamicsContainer3D.h"
#include "latticeStatistics.h"
#include "multiPhysics.h"
#include "olbDebug.h"
#include "postProcessing.h"
#include "serializer.h"
#include <dynamics/lbHelpers.h>
// #include <vector>

namespace olb {

//    template<typename T, template<typename U> class Lattice>
//    struct Dynamics;

    template<typename T, template<typename U> class Lattice, class ExternalField>
    class BlockLatticeALE3D : public BlockLattice3D<T, Lattice> {
    
	private:
        ExternalField externalField_;
        Vec3<T> anchor_;

    public:
        BlockLatticeALE3D(int nx, int ny, int nz, const Vec3<T>& anchor, Dynamics<T, Lattice> *dynamics, ExternalField& externalField);
        BlockLatticeALE3D(const SubDomainInformation<T,Lattice<T>>& subDomInfo,const Vec3<T>& anchor, Dynamics<T, Lattice> *dynamics,ExternalField& externalField);

        /**
          * Interpolates the populations at the new transformed mesh positions into the current interpolated field
          * Swaps the pointers to current collide/interpolated field at the end
          */

        void moveMesh(const Vec3<T>& translation, const Vec3<T>& angles,
                      const Vec3<T>& position, const Vec3<T>& orientation,
                      SubDomainInformation<T,Lattice<T>> subDomInfo
                      );

        template<typename ExternalFluidMask,unsigned boundaryDistance = 2>
        void updateFluidMask(const Vec3<T>& translation, const Vec3<T>& angles,
                const Vec3<T>& position, const Vec3<T>& orientation,
                ExternalFluidMask& externalFluidMask,
                SubDomainInformation<T,Lattice<T>> subDomInfo);

        void updateAnchorPoint(Vec3<T> const & newAnchorPoint);

        template<class FluidDynamics>
        void collideAndStreamALE(const Vec3<T>& translation, const Vec3<T>& angles,
                const Vec3<T>& position, const Vec3<T>& orientation);

        template<class FluidDynamics>
        void collideALE(const Vec3<T>& translation, const Vec3<T>& angles,
                const Vec3<T>& position, const Vec3<T>& orientation);

        template<class FluidDynamics>
        void fieldCollisionALE(const Vec3<T>& translation, const Vec3<T>& angles,
                const Vec3<T>& position, const Vec3<T>& orientation);

        void setCurrentFluidMaskAsStationary();
        void saveInitialFluidMask();
		    void resetTimer(){ externalField_.resetTimer();}

#ifdef ENABLE_CUDA
        void moveMeshGPU(const Vec3<T>& translation, const Vec3<T>& angles,
                         const Vec3<T>& position, const Vec3<T>& orientation,
                         SubDomainInformation<T,Lattice<T>> subDomInfo
                         );
        template<typename ExternalFluidMask, unsigned boundaryDistance = 2>
        void updateFluidMaskGPU(const Vec3<T>& translation, const Vec3<T>& angles,
                const Vec3<T>& position, const Vec3<T>& orientation,
                ExternalFluidMask& externalFluidMask,
                SubDomainInformation<T,Lattice<T>> subDomInfo);
#endif

        CellBlockDataALE<T,Lattice>& getCellBlockData ()
        {
          return *(static_cast<CellBlockDataALE<T,Lattice>*>(this->cellData));
        }
    };

}


#endif //OPENLB_BLOCKLATTICEALE3D_H
