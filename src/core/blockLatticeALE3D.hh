//
// Created by Andreas Reiser on 2018-11-28.
//

#ifndef OPENLB_BLOCKLATTICEALE3D_HH
#define OPENLB_BLOCKLATTICEALE3D_HH

#include "blockLattice3D.h"
#include "blockLattice3D.hh"
#include "blockLatticeALE3D.h"
#include "communication/blockLoadBalancer.h"
#include "communication/loadBalancer.h"
#include "dynamics/dynamics.h"
#include "dynamics/lbHelpers.h"
#include "functors/lattice/indicator/blockIndicatorF3D.h"
#include "util.h"
#include <algorithm>
#include <vector>
#include <chrono>

namespace olb {

template<typename T, template<typename U> class Lattice, class ExternalField>
BlockLatticeALE3D<T, Lattice, ExternalField>::BlockLatticeALE3D(int nx, int ny, int nz,
        const Vec3<T>& anchor,
        Dynamics<T, Lattice> *dynamics,
        ExternalField& externalField)

        : BlockLattice3D<T, Lattice>(nx, ny, nz, dynamics, new CellBlockDataALE<T,Lattice>(nx,ny,nz) ),
          externalField_(externalField),
          anchor_(anchor)
          {}

template<typename T, template<typename U> class Lattice, class ExternalField>
BlockLatticeALE3D<T, Lattice, ExternalField>::BlockLatticeALE3D(const SubDomainInformation<T,Lattice<T>>& subDomInfo, const Vec3<T>& anchor,
        Dynamics<T, Lattice> *dynamics, ExternalField& externalField)

        : BlockLattice3D<T, Lattice>(subDomInfo,dynamics, new CellBlockDataALE<T,Lattice>(subDomInfo.localGridSize()[0],subDomInfo.localGridSize()[1],subDomInfo.localGridSize()[2]) ),
          externalField_{externalField},
          anchor_(anchor)
          {}

template<typename T, template<typename U> class Lattice, class ExternalField>
void BlockLatticeALE3D<T, Lattice, ExternalField>::updateAnchorPoint(Vec3<T> const & newAnchorPoint)
{
    anchor_ = newAnchorPoint;
}

template<typename T, template<typename U> class Lattice, class ExternalField>
template<class FluidDynamics>
void BlockLatticeALE3D<T, Lattice, ExternalField>::collideAndStreamALE(const Vec3<T>& translation, const Vec3<T>& angles,
        const Vec3<T>& position, const Vec3<T>& orientation)
{
    collideALE<FluidDynamics>(translation, angles, position, orientation);

    this->stream();

    this->postProcess();

    this->getStatistics().incrementTime();
}

template<typename T, template<typename U> class Lattice, class ExternalField>
template<class FluidDynamics>
void BlockLatticeALE3D<T, Lattice, ExternalField>::collideALE(const Vec3<T>& translation, const Vec3<T>& angles,
        const Vec3<T>& position, const Vec3<T>& orientation)
{
    fieldCollisionALE<FluidDynamics>(translation, angles, position, orientation);

    this->boundaryCollision();
}

template<typename T, template<typename U> class Lattice, class ExternalField>
template<class FluidDynamics>
void BlockLatticeALE3D<T, Lattice, ExternalField>::fieldCollisionALE(const Vec3<T>& translation, const Vec3<T>& angles,
        const Vec3<T>& position, const Vec3<T>& orientation)
{
    auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(this->cellData);

    T** fieldData = cellDataALE->getCellData();

    const Affine3<T> movement = aleutil::movementTransform<T>(translation, angles, anchor_);
    const Affine3<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);
    const Affine3<T> velocityRotation = aleutil::velocityRotationTransform<T>(angles, orientation);

    externalField_.preTimeStepCalculations(velocityRotation, positionInExternalField);

    DynamicsDataHandler<T>* dataHandler = this->dynamicsContainer.getDataHandler(this->dynamicsContainer.getFluidDynamics());
    T** momentaData = nullptr;
    if (dataHandler)
      momentaData = dataHandler->getDynamicsData();
    T* collisionData = this->dynamicsContainer.getFluidDynamics()->getCollisionData();

    size_t size = this->_nx*this->_ny*this->_nz;

    bool* mask = this->cellData->getFluidMask();
    for(size_t cellId = 0; cellId < size; ++cellId)
    {
      if (mask[cellId])
          ;
//        FluidDynamics::collisionALE(fieldData, cellId, momentaData, 0, collisionData);
    }

}

template<typename T, template<typename U> class Lattice, class ExternalField>
void BlockLatticeALE3D<T, Lattice, ExternalField>::saveInitialFluidMask()
{
	setCurrentFluidMaskAsStationary();
}


template<typename T, template<typename U> class Lattice, class ExternalField>
void BlockLatticeALE3D<T, Lattice, ExternalField>::setCurrentFluidMaskAsStationary()
{
  static_cast<CellBlockDataALE<T,Lattice>*>(this->cellData)->setCurrentFluidMaskAsStationary();
}

template<typename T, template<typename U> class Lattice, class ExternalField>
void BlockLatticeALE3D<T, Lattice, ExternalField>::moveMesh(const Vec3<T>& translation, const Vec3<T>& angles,
                                                            const Vec3<T>& position, const Vec3<T>& orientation,
                                                            SubDomainInformation<T,Lattice<T>> subDomInfo) {
  auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(this->cellData);

  const Affine3<T> movement = aleutil::movementTransform<T>(translation, angles, anchor_);
  const Affine3<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);
  const Affine3<T> velocityRotation = aleutil::velocityRotationTransform<T>(angles, orientation);


  externalField_.preTimeStepCalculations(velocityRotation, positionInExternalField);

  for (size_t cellIndex = 0; cellIndex < this->_nx * this->_ny * this->_nz; ++cellIndex) {
    aleutil::interpolateCellPopulations3D<T,Lattice>(
        cellDataALE->getCellData(),
        cellDataALE->getCellDataALE(),
        movement,
        externalField_,
        cellIndex,
        this->_nx,
        this->_ny,
        this->_nz,
        subDomInfo);
      // ,
        // this->cellData->getFluidMask(),
        // cellDataALE->getStationaryFluidMask());

  }

  // std::cout << "inside move mesh" << std::endl;
  cellDataALE->moveMesh();
}

template<typename T, template<typename U> class Lattice, class ExternalField>
template<typename ExternalFluidMask, unsigned boundaryDistance>
void BlockLatticeALE3D<T, Lattice, ExternalField>::updateFluidMask(const Vec3<T>& translation, const Vec3<T>& angles,
                                                            const Vec3<T>& position, const Vec3<T>& orientation,
                                                            ExternalFluidMask& externalFluidMask,
                                                            SubDomainInformation<T,Lattice<T>> subDomInfo) {
  auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(this->cellData);

  const Affine3<T> movement = aleutil::movementTransform<T>(translation, angles, anchor_);
  const Affine3<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);


  externalFluidMask.preTimeStepCalculations(positionInExternalField);

  for (size_t cellIndex = 0; cellIndex < this->_nx * this->_ny * this->_nz; ++cellIndex) {
    aleutil::interpolateFluidMask3D<T, Lattice, ExternalFluidMask,boundaryDistance>(
        cellDataALE->getFluidMask(),
        cellDataALE->getFluidMaskALE(),
        movement,
        externalFluidMask,
        cellIndex,
        this->_nx,
        this->_ny,
        this->_nz,
        subDomInfo
        );
  }
}

#ifdef ENABLE_CUDA
  template<typename T, template<typename U> class Lattice, class ExternalField>
  void BlockLatticeALE3D<T, Lattice, ExternalField>::moveMeshGPU(const Vec3<T>& translation, const Vec3<T>& angles,
                                                                    const Vec3<T>& position, const Vec3<T>& orientation,
                                                                    SubDomainInformation<T,Lattice<T>> subDomInfo) {

    const Affine3<T> movement = aleutil::movementTransform<T>(translation, angles, anchor_);
    const Affine3<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);
    const Affine3<T> velocityRotation = aleutil::velocityRotationTransform<T>(angles, orientation);

    // std::cout << movement << std::endl;

    externalField_.preTimeStepCalculations(velocityRotation, positionInExternalField);

    this->dynamicsContainer.getGPUHandler().template executeMoveMeshKernel3D<ExternalField>(movement, externalField_,subDomInfo);

    cudaDeviceSynchronize();
    static_cast<CellBlockDataALE<T, Lattice> *>(this->cellData)->gpuMoveMesh();

  }

  template<typename T, template<typename U> class Lattice, class ExternalField>
  template<typename ExternalFluidMask, unsigned boundaryDistance>
  void BlockLatticeALE3D<T, Lattice, ExternalField>::updateFluidMaskGPU(const Vec3<T>& translation, const Vec3<T>& angles,
          const Vec3<T>& position, const Vec3<T>& orientation,
          ExternalFluidMask& externalFluidMask,
          SubDomainInformation<T,Lattice<T>> subDomInfo) {

    const Affine3<T> movement = aleutil::movementTransform<T>(translation, angles, anchor_);
    const Affine3<T> positionInExternalField = aleutil::positionInExternalFieldTransform<T>(position, orientation, anchor_);

    // std::cout << movement << std::endl;

    externalFluidMask.preTimeStepCalculations(positionInExternalField);

    this->dynamicsContainer.getGPUHandler().template executeUpdateFluidMaskKernel3D<ExternalFluidMask,boundaryDistance>(movement, externalFluidMask,subDomInfo);

    cudaDeviceSynchronize();
  }


#endif


}
#endif //OPENLB_BLOCKLATTICEALE3D_HH
