/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Dynamics for a generic 2D block lattice view -- header file.
 */
#ifndef BLOCK_LATTICE_VIEW_2D_H
#define BLOCK_LATTICE_VIEW_2D_H

#include <vector>
#include "blockLatticeStructure2D.h"
#include "geometry/blockGeometry2D.h"
#include "geometry/blockGeometryStatistics2D.h"

namespace olb {

/// A rectangular extract from a given BlockLatticeStructure.
/** Attention:
 * - This class can only be applied to already existing BlockLattices.
 * - The postProcessors of the original BlockLattice are not called any
 *   more. New, appropriate PostProcessors are generated instead.
 */
template<typename T, template<typename U> class Lattice>
class BlockLatticeView2D : public BlockLatticeStructure2D<T,Lattice> {
public:
  BlockLatticeView2D(BlockLattice2D<T,Lattice>& originalLattice_);
  BlockLatticeView2D(BlockLattice2D<T,Lattice>& originalLattice_,
                     int x0_, int x1_, int y0_, int y1_);
  ~BlockLatticeView2D() override;
  BlockLatticeView2D(BlockLatticeView2D const& rhs);
  BlockLatticeView2D<T,Lattice>& operator=
  (BlockLatticeView2D<T,Lattice> const& rhs);
  void swap(BlockLatticeView2D<T,Lattice>& rhs);

  CellView<T,Lattice> get(int iX, int iY) override;
  CellView<T,Lattice> get(int iX, int iY) const override;

  void defineRhoU(const int x0, const int x1, const int y0, const int y1, const T rho, const T u[2]) override;
  void initialize() override;
  void defineDynamics (
    int x0_, int x1_, int y0_, int y1_,
    Dynamics<T,Lattice>* dynamics ) override;
  void defineDynamics(int iX, int iY, Dynamics<T,Lattice>* dynamics ) override;
  void defineDynamics(BlockGeometryStructure2D<T>& blockGeometry, int material,
      Dynamics<T,Lattice>* dynamics) override;
  Dynamics<T,Lattice>* getDynamics(int iX, int iY) override;
  T computeAverageDensity(int x0_, int x1_, int y0_, int y1_) const override;
  T computeAverageDensity() const override;
  void computeStress(int iX, int iY, T pi[util::TensorVal<Lattice<T> >::n]) override;
  void stripeOffDensityOffset (
    int x0_, int x1_, int y0_, int y1_, T offset ) override;
  void stripeOffDensityOffset(T offset) override;
  void forAll(int x0_, int x1_, int y0_, int y1_,
                      WriteCellFunctional<T,Lattice> const& application) override;
  void forAll(WriteCellFunctional<T,Lattice> const& application) override;
  void addPostProcessor (
    PostProcessorGenerator2D<T,Lattice> const& ppGen) override;
  void resetPostProcessors() override;
  void postProcess(int x0_, int x1_, int y0_, int y1_) override;
  void postProcess() override;
  void addLatticeCoupling (
    LatticeCouplingGenerator2D<T,Lattice> const& lcGen,
    std::vector<SpatiallyExtendedObject2D*> partners ) override;
  void executeCoupling(int x0_, int x1_, int y0_, int y1_) override;
  void executeCoupling() override;
  void subscribeReductions(Reductor<T>& reductor) override;
  LatticeStatistics<T>& getStatistics() override;
  LatticeStatistics<T> const& getStatistics() const override;

  SpatiallyExtendedObject2D* getComponent(int iBlock) override;
  SpatiallyExtendedObject2D const* getComponent(int iBlock) const override;
  multiPhysics::MultiPhysicsId getMultiPhysicsId() const override;
private:
  BlockLattice2D<T,Lattice>  *originalLattice;
  int                          x0, y0;
};

}  // namespace olb

#endif
