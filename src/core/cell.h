/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt, 2015 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Definition of a LB cell -- header file.
 */
#ifndef CELL_H
#define CELL_H

#include "olbDebug.h"
#include "serializer.h"
#include "dynamics/latticeDescriptors.h"
#include "dynamics/dynamics.h"
#include "blockData2D.h"
#include "config.h"

namespace olb {

template<typename T>
class TWrapper {
private:
  T& _p;

public:
  OPENLB_HOST_DEVICE
  TWrapper(T& p):
    _p(p)
  {
  }

  OPENLB_HOST_DEVICE
  T get() const
  {
    return _p;
  }

  OPENLB_HOST_DEVICE
  void set(T t)
  {
    _p = t;
  }

  OPENLB_HOST_DEVICE
  TWrapper& operator=(const T& t)
  {
    _p = t;
    return *this;
  }

  OPENLB_HOST_DEVICE
  TWrapper& operator=(const TWrapper<T>& t)
  {
    _p = t._p;
    return *this;
  }

  OPENLB_HOST_DEVICE
  T operator+(const TWrapper& t) const
  {
    return _p + t._p;
  }

  OPENLB_HOST_DEVICE
  TWrapper& operator+=(const T& t)
  {
    _p += t;
    return *this;
  }

  OPENLB_HOST_DEVICE
  TWrapper& operator*=(const T& t)
  {
    _p *= t;
    return *this;
  }

  OPENLB_HOST_DEVICE
  T operator*(const T& t) const
  {
    return _p * t;
  }

  OPENLB_HOST_DEVICE
  T operator+(const T& t) const
  {
    return _p + t;
  }

  OPENLB_HOST_DEVICE
  T operator-(const TWrapper& t) const
  {
    return _p - t._p;
  }

  OPENLB_HOST_DEVICE
  T operator-(const T& t) const
  {
    return _p - t;
  }

  OPENLB_HOST_DEVICE
  TWrapper& operator-=(const T& t)
  {
    _p -= t;
    return *this;
  }

  OPENLB_HOST_DEVICE
  T operator-() const
  {
    return -_p;
  }

  OPENLB_HOST_DEVICE
  void swap(TWrapper<T> r) const
  {
    std::swap(r._p, _p);
  }
};

template<typename T>
T operator-(const T& l, const TWrapper<T>& r)
{
  return r - l;
}

template<typename T>
T operator+(const T& l, const TWrapper<T>& r)
{
  return r + l;
}

template<typename T>
T operator*(const T& l, const TWrapper<T>& r)
{
  return r * l;
}

template<typename T>
T& operator+=(T& l, const TWrapper<T>& r)
{
   l = r + l;
   return l;
}

template<typename T>
OPENLB_HOST_DEVICE
void swap(TWrapper<T> l, TWrapper<T> r)
{
  T temp = l.get();
  l.set(r.get());
  r.set(temp);
}

/// A LB lattice cell.
/** A cell contains the q values of the distribution functions f on
 * one lattice point, as well as a pointer to the dynamics of the
 * cell. Thanks to this pointer, one can have a space dependend de-
 * finition of the dynamics. This mechanism is useful e.g. for the
 * implementation of boundary conditions, or an inhomogeneous body
 * force.
 *
 * The dynamics object is not owned by the class, it is not
 * destructed in the CellView destructor.
 *
 * This class is not intended to be derived from.
 */
template<typename T, class Descriptor>
class CellBase {

protected:
  T* _data[Descriptor::dataSize];

public:
  /// Read-write access to distribution functions.
  /** \param iPop index of the accessed distribution function */
  OPENLB_HOST_DEVICE
  TWrapper<T> operator[](int const& iPop)
  {
    OLB_PRECONDITION( iPop < Descriptor::dataSize );
    return TWrapper<T>(_data[iPop][0]);
  }
  /// Read-only access to distribution functions.
  /** \param iPop index of the accessed distribution function */
  OPENLB_HOST_DEVICE
  T const& operator[](int const& iPop) const
  {
    OLB_PRECONDITION( iPop < Descriptor::dataSize );
    return _data[iPop][0];
  }
};


template<typename T, template<typename U> class Lattice>
class CellView : public CellBase<T, Lattice<T>> {
public:
  /// Additional per-cell scalars for external fields, e.g. forces
  typedef descriptors::ExternalFieldArray <
  T, typename Lattice<T>::ExternalField > External;
private:
  Dynamics<T,Lattice>* dynamics;  ///< local LB dynamics

public:
  /// Constructor, to be used whenever possible.
  CellView(Dynamics<T,Lattice>* dynamics_, CellDataArray<T,Lattice> data);
public:
  /// Get a pointer to an external field
  T* getExternal(int offset)
  {
    OLB_PRECONDITION( offset < Lattice<T>::ExternalField::numScalars() );
    // return external.get(offset);
    return nullptr; // TODO
  }
  /// Get a const pointer to an external field
  T const* getExternal(int offset) const
  {
    OLB_PRECONDITION( offset < Lattice<T>::ExternalField::numScalars() );
    // return external.get(offset);
    return nullptr; // TODO
  }
  /// Define or re-define dynamics of the cell.
  /** \param dynamics_ a pointer to the dynamics object, whos
    *    memory management falls under the responsibility of the
    *    user */
  void defineDynamics(Dynamics<T,Lattice>* dynamics_);
  /// Get a non-modifiable pointer to the dynamics
  Dynamics<T,Lattice> const* getDynamics() const;
  /// Get a non-modifiable pointer to the dynamics
  Dynamics<T,Lattice>* getDynamics();

  // The following helper functions forward the function call
  // to the Dynamics object
public:
  /// Apply LB collision to the cell according to local dynamics.
  void collide(LatticeStatistics<T>& statistics)
  {
    OLB_PRECONDITION( dynamics );
    dynamics->collide(*this, statistics);
  }

  /// Compute particle density on the cell.
  /** \return particle density
   */
  T computeRho() const
  {
    OLB_PRECONDITION( dynamics );
    return dynamics->computeRho(*this);
  }
  /// Compute fluid velocity on the cell.
  /** \param u fluid velocity
   */
  void computeU(T u[Lattice<T>::d]) const
  {
    OLB_PRECONDITION( dynamics );
    dynamics->computeU(*this, u);
  }
  /// Compute components of the stress tensor on the cell.
  /** \param pi stress tensor */
  void computeStress (
    T pi[util::TensorVal<Lattice<T> >::n]) const
  {
    OLB_PRECONDITION( dynamics );
    T rho, u[Lattice<T>::d];
    dynamics->computeRhoU(*this, rho, u);
    dynamics->computeStress(*this, rho, u, pi);
  }
  /// Compute fluid velocity and particle density on the cell.
  /** \param rho particle density
   *  \param u fluid velocity
   */
  void computeRhoU(T& rho, T u[Lattice<T>::d]) const
  {
    OLB_PRECONDITION( dynamics );
    dynamics->computeRhoU(*this, rho, u);
  }

  void getForce(T force[Lattice<T>::d]) const
  {
      OLB_PRECONDITION( dynamics);
      for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
          force[iDim] = this->_data[Lattice<T>::forceIndex+iDim][0];
  }
  /// Compute all momenta on the celll, up to second order.
  /** \param rho particle density
   *  \param u fluid velocity
   *  \param pi stress tensor
   */
  void computeAllMomenta (
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const
  {
    OLB_PRECONDITION( dynamics );
    dynamics->computeAllMomenta(*this, rho, u, pi);
  }
  /// Access external fields through the dynamics object.
  /** This method is similar to getExternal(): it delivers the
   * value of the external fields. This time, those values
   * are however computed through a virtual call to the dynamics
   * object.
   */
  void computeExternalField(int pos, int size, T* ext) const
  {
    OLB_PRECONDITION(pos+size <= Lattice<T>::ExternalField::numScalars);
    T const* externalData = this->getExternal(pos);
    for (int iExt=0; iExt<size; ++iExt) {
      ext[iExt] = externalData[iExt];
    }
  }
  /// Set particle density on the cell.
  /** \param rho particle density
   */
  void defineRho(T rho)
  {
    OLB_PRECONDITION( dynamics );
    dynamics->defineRho(*this, rho);
  }
  /// Set fluid velocity on the cell.
  /** \param u fluid velocity
   */
  void defineU(const T u[Lattice<T>::d])
  {
    OLB_PRECONDITION( dynamics );
    dynamics->defineU(*this, u);
  }
  /// Define fluid velocity and particle density on the cell.
  /** \param rho particle density
   *  \param u fluid velocity
   */
  void defineRhoU(T rho, const T u[Lattice<T>::d])
  {
    OLB_PRECONDITION( dynamics );
    dynamics->defineRhoU(*this, rho, u);
  }
  /// Define particle populations through the dynamics object.
  /** This method is similar to operator[]: it modifies the
   * value of all the particle populations.
   */
  void definePopulations(const T* f_)
  {
    for (int iPop = 0; iPop < Lattice<T>::q; ++iPop)
    {
      this->operator[](iPop) = f_[iPop];
    }
  }
  /// Define external fields through the dynamics object.
  /** This method is similar to getExternal(): it accesses the
   * value of the external fields.
   */
//  void defineExternalField(int pos, int size, const T* ext)
//  {
//    OLB_PRECONDITION(pos+size <= Lattice<T>::ExternalField::numScalars());
//    T* externalData = this->getExternal(pos);
//    for (int iExt=0; iExt<size; ++iExt) {
//      externalData[iExt] = ext[iExt];
//    }
//  }
  inline void defineExternalField(int pos, int size, const T* ext)
  {
      OLB_PRECONDITION(pos+size <= Lattice<T>::dataSize);
      for(int iExt=0; iExt<size; ++iExt)
          this->_data[pos+iExt][0]= ext[iExt];
  }
  /// Add external fields through the dynamics object.
  /** Similar to defineExternalField(),but instead of replacing existing values
   *  ext is added to existing values.
   */
//  inline void addExternalField(int pos, int size, const T* ext)
//  {
//    OLB_PRECONDITION(pos+size <= Lattice<T>::ExternalField::numScalars());
//    T* externalData = this->getExternal(pos);
//    for (int iExt=0; iExt<size; ++iExt) {
//      externalData[iExt] += ext[iExt];
//    }
//  }
  inline void addExternalField(int pos, int size, const T* ext)
  {
      OLB_PRECONDITION(pos+size <= Lattice<T>::dataSize);
      for(int iExt=0; iExt<size; ++iExt)
          this->_data[pos+iExt][0] += ext[iExt];
  }
  /// Add external fields through the dynamics object.
  /** Similar to defineExternalField(),but instead of replacing existing values
   *  ext is multiplied to existing values.
   */
  inline void multiplyExternalField(int pos, int size, const T* ext)
  {
    OLB_PRECONDITION(pos+size <= Lattice<T>::ExternalField::numScalars());
    T* externalData = this->getExternal(pos);
    for (int iExt=0; iExt<size; ++iExt) {
      externalData[iExt] *= ext[iExt];
    }
  }
  /// Initialize all f values to their local equilibrium
  void iniEquilibrium(T rho, const T u[Lattice<T>::d])
  {
    OLB_PRECONDITION( dynamics );
    dynamics->iniEquilibrium(*this, rho, u);
  }
  /// Revert ("bounce-back") the distribution functions.
  void revert();

private:
};

template<typename T, template<typename U> class Lattice>
struct WriteCellFunctional {
  virtual ~WriteCellFunctional() { };
  virtual void apply(CellView<T,Lattice> cell, int pos[Lattice<T>::d]) const =0;
};

}  // namespace olb

#endif
