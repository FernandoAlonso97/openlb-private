/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Definition of a LB cell -- generic implementation.
 */
#ifndef CELL_HH
#define CELL_HH

#include <algorithm>
#include "cell.h"
#include "util.h"

namespace olb {

////////////////////////// Class CellView /////////////////////////////

/** This constructor initializes the dynamics, but not the values
 * of the distribution functions. Remember that the dynamics is not
 * owned by the CellView object, the user must ensure its proper
 * destruction and a sufficient life time.
 */
template<typename T, template<typename U> class Lattice>
CellView<T,Lattice>::CellView(Dynamics<T,Lattice>* dynamics_, CellDataArray<T,Lattice> data)
  : dynamics(dynamics_)
{
  for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
    this->_data[iPop] = data.data[iPop];
  }
}

template<typename T, template<typename U> class Lattice>
Dynamics<T,Lattice> const* CellView<T,Lattice>::getDynamics() const
{
  OLB_PRECONDITION(dynamics);
  return dynamics;
}

template<typename T, template<typename U> class Lattice>
Dynamics<T,Lattice>* CellView<T,Lattice>::getDynamics()
{
  OLB_PRECONDITION(dynamics);
  return dynamics;
}

template<typename T, template<typename U> class Lattice>
void CellView<T,Lattice>::revert()
{
  for (int iPop=1; iPop<=Lattice<T>::q/2; ++iPop) {
    std::swap(this->f[iPop],this->f[iPop+Lattice<T>::q/2]);
  }
}

}  // namespace olb


#endif
