//
// Created by andi on 19/01/19.
//

#include <dynamics/latticeDescriptors.h>
#include "cellBlockData.hh"
#include "cellBlockData.h"

namespace olb {

    template class CellBlockData<double, descriptors::D2Q9Descriptor>;
    template class CellBlockData<double, descriptors::D3Q19Descriptor>;

}