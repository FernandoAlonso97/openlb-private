//
// Created by andi on 19/01/19.
//

#ifndef OPENLB_CELLBLOCKDATA_H
#define OPENLB_CELLBLOCKDATA_H

#include "util.h"
#include "celldataarray.h"

namespace olb {
    template<typename T, template<typename U> class Lattice>
    class CellBlockData {
    protected:
        size_t nx_;
        size_t nz_;
        size_t ny_;

        size_t gridSize_;

        T **fluidData_;
        T **fluidDataEven;
        T **fluidDataOdd;
        T **fluidDataCurrent;
        T **fluidDataPrevious;
        bool *fluidMask_;
        unsigned int *materialNumber_;

        bool streamEven = true;

#ifdef ENABLE_CUDA
        T **gpuFluidData_;
        T **gpuFluidDataEven;
        T **gpuFluidDataOdd;
        T **gpuFluidDataCurrent;
        T **gpuFluidDataPrevious;
        bool *gpuFluidMask_;

        bool gpuStreamEven = true; // Do I even need this

        cudaStream_t copyStreamToGPU;
        cudaStream_t copyStreamToCPU;
#endif

        constexpr long long indexFor(int iX, int iY, int iZ = 0) const {
            return util::getCellIndex3D(iX, iY, iZ, ny_, nz_);
        }

    public:
        CellBlockData(size_t nx, size_t ny, size_t nz = 1);

        virtual ~CellBlockData();

        virtual void stream();

        void setFluidMaskForCell(bool flag, int iX, int iY, int iZ = 0);

        void setMaterialNumberForCell(unsigned int material, int iX, int iY, int iZ = 0);

#ifdef ENABLE_CUDA
        virtual void gpuStream();

        virtual void copyCellDataToGPU();

        virtual void copyCellDataToCPU();

        virtual void copyVizDataToCPU(bool copyRho, bool copyU, bool copyForce, bool copyFluidMask);

        void copyFluidMaskToGPU();
#endif

        /**
         * Getter
         */

        size_t getNx() { return nx_; };
        size_t getNy() { return ny_; };
        size_t getNz() { return nz_; };
        size_t getGridSize() { return gridSize_; };

        CellDataArray<T, Lattice> getData(int iX, int iY, int iZ = 0);

        CellDataArray<T, Lattice> getData(size_t i);

        CellDataArray<T, Lattice> getData(int iX, int iY, int iZ = 0) const;

        CellDataArray<T, Lattice> getData(size_t i) const;

        CellPopulationArray<T, Lattice> getPopulations(int iX, int iY, int iZ = 0) const;

        T* getCellData(int iPop, int iX, int iY, int iZ = 0) const;

        T* getCellDataPrevious(int iPop, int iX,int iY, int iZ=0) const;

        T** getCellData() const;
        
        T** getCellDataPrevious() const;

        T** getNonOffsetCellData() const;

        bool* getFluidMask();

        bool getFluidMask(int iX, int iY, int iZ = 0);

        unsigned int* getMaterialNumber();

        unsigned int getMaterialNumber(int iX, int iY, int iZ = 0);

#ifdef ENABLE_CUDA
        T** gpuGetFluidData();
        T** gpuGetFluidDataPrevious();
        T** gpuGetNonOffsetFluidData();

        bool* gpuGetFluidMask();
#endif
    };

    template<typename T, template<typename U> class Lattice>
    class CellBlockDataALE: public CellBlockData<T, Lattice> {
    protected:
        T **fluidDataALE_;
        T **fluidDataEvenALE;
        T **fluidDataOddALE;
        T **fluidDataCurrentALE;

        bool* fluidMaskALE_ = nullptr;

        bool aleEven = false;

#ifdef ENABLE_CUDA
        T **gpuFluidDataALE_;
        T **gpuFluidDataEvenALE;
        T **gpuFluidDataOddALE;
        T **gpuFluidDataCurrentALE;

        bool* gpuFluidMaskALE_ = nullptr;

        bool gpuAleEven = false;
#endif

    public:
        CellBlockDataALE(size_t nx, size_t ny, size_t nz = 1);
        ~CellBlockDataALE();

        void stream() override;

        void moveMesh();

#ifdef ENABLE_CUDA
        void gpuStream() override;

        void gpuMoveMesh();

        void copyCellDataToGPU() override;

        void copyCellDataToCPU() override;

#endif

        void setCurrentFluidMaskAsStationary();


        /**
         * Getter
         */

        bool* getStationaryFluidMask();

        CellDataArray<T, Lattice> getDataALE(int iX, int iY, int iZ = 0);

        CellDataArray<T, Lattice> getDataALE(size_t i);

        CellDataArray<T, Lattice> getDataALE(int iX, int iY, int iZ = 0) const;

        CellDataArray<T, Lattice> getDataALE(size_t i) const;

        CellPopulationArray<T, Lattice> getPopulationsALE(int iX, int iY, int iZ = 0) const;

        T* getCellDataALE(int iPop, int iX, int iY, int iZ = 0) const;

        T** getCellDataALE() const;

        T** getFluidDataEven()
        {
          return this->fluidDataEven;
        }

        T** getFluidDataOdd()
        {
          return this->fluidDataOdd;
        }

        
        T** getFluidDataALEEven()
        {
          return this->fluidDataEvenALE;
        }

        T** getFluidDataALEOdd()
        {
          return this->fluidDataOddALE;
        }
        bool* getFluidMaskALE() const;

#ifdef ENABLE_CUDA
        bool* gpuGetStationaryFluidMask();
        T** gpuGetFluidDataALE() const;
        bool* gpuGetFluidMaskALE() const;
#endif
    };
}

#endif //OPENLB_CELLBLOCKDATA_H
