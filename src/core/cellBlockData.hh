//
// Created by andi on 19/01/19.
//

#ifndef OPENLB_CELLBLOCKDATA_HH
#define OPENLB_CELLBLOCKDATA_HH

#include "cellBlockData.h"
#include "cudaErrorHandler.h"

namespace olb {
    template<typename T, template<typename U> class Lattice>
    CellBlockData<T, Lattice>::CellBlockData(size_t nx, size_t ny, size_t nz):
        nx_(nx),
        ny_(ny),
        nz_(nz),
        gridSize_((nx_ * ny_ * nz_)),
        fluidData_(new T *[Lattice<T>::dataSize]),
        fluidDataEven(new T *[Lattice<T>::dataSize]),
        fluidDataOdd(new T *[Lattice<T>::dataSize]),
        fluidDataCurrent(nullptr),
        fluidDataPrevious(nullptr),
        fluidMask_(new bool[gridSize_]),
        materialNumber_(new unsigned int[gridSize_])
#ifdef ENABLE_CUDA
        ,
        gpuFluidData_(nullptr),
        gpuFluidDataEven(nullptr),
        gpuFluidDataOdd(nullptr),
        gpuFluidDataCurrent(nullptr),
        gpuFluidDataPrevious(nullptr),
        gpuFluidMask_(nullptr)
#endif
    {
        for (size_t i = 0; i < gridSize_; ++i)
        {
            fluidMask_[i] = true;
            materialNumber_[i] = 0;
        }

        for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i) {
            int offset = util::getGridOffset<Lattice<T>>(i, ny_, nz_);
            fluidData_[i] = new T[gridSize_ + std::abs(offset)];

            for (int j = 0; j < gridSize_ + std::abs(offset); ++j) {
                fluidData_[i][j] = T();
            }

            if (i >= Lattice<T>::q) {
                fluidDataEven[i] = fluidData_[i];
                fluidDataOdd[i] = fluidData_[i];
            }
        }

        for (int i = 0; i < Lattice<T>::q; ++i) {
            int offset = util::getGridOffset<Lattice<T>>(i, ny_, nz_);
            int opposite = Lattice<T>::opposite(i);

            fluidDataEven[i] = fluidData_[i];
            fluidDataOdd[i] = fluidData_[opposite];

            if (offset > 0) {
                fluidDataEven[i] += offset;
                fluidDataOdd[i] += offset;
            }
        }

        fluidDataCurrent = fluidDataEven;
        fluidDataPrevious = fluidDataOdd;

#ifdef ENABLE_CUDA
        auto memory_size = (Lattice<T>::dataSize) * sizeof(T*);
        cudaError_t error = cudaMallocManaged(&gpuFluidData_, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&gpuFluidDataEven, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&gpuFluidDataOdd, memory_size);
        HANDLE_ERROR(error);

        for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i) {
            int offset = util::getGridOffset<Lattice<T>>(i, ny_, nz_);
            error = cudaMalloc(&gpuFluidData_[i], sizeof(T) * (gridSize_ + std::abs(offset)));
            HANDLE_ERROR(error);

            if (i >= Lattice<T>::q) {
                gpuFluidDataEven[i] = gpuFluidData_[i];
                gpuFluidDataOdd[i] = gpuFluidData_[i];
            }
        }

        for (unsigned int i = 0; i < Lattice<T>::q; ++i) {
            int offset = util::getGridOffset<Lattice<T>>(i, ny_, nz_);
            gpuFluidDataEven[i] = gpuFluidData_[i];
            gpuFluidDataOdd[i] = gpuFluidData_[Lattice<T>::opposite(i)];

            if (offset > 0) {
                gpuFluidDataEven[i] += offset;
                gpuFluidDataOdd[i] += offset;
            }

            cudaMemset(gpuFluidData_[i], 0, sizeof(T) * (gridSize_ + std::abs(offset)));
        }

        error = cudaMalloc(&gpuFluidMask_, sizeof(bool) * gridSize_);
        HANDLE_ERROR(error);
        cudaMemset(gpuFluidMask_, 0, sizeof(bool) * gridSize_);

        gpuFluidDataCurrent = gpuFluidDataEven;
        gpuFluidDataPrevious = gpuFluidDataOdd;

        cudaStreamCreateWithFlags(&copyStreamToGPU,cudaStreamNonBlocking);
        cudaStreamCreateWithFlags(&copyStreamToCPU,cudaStreamNonBlocking);
#endif
    }

    template<typename T, template<typename U> class Lattice>
    CellBlockData<T, Lattice>::~CellBlockData<T, Lattice>() {
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            delete[] fluidData_[i];
        }
        delete[] fluidData_;
        delete[] fluidDataEven;
        delete[] fluidDataOdd;

        delete[] fluidMask_;

        delete[] materialNumber_;

#ifdef ENABLE_CUDA
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaFree(gpuFluidData_[i]);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaFree(gpuFluidData_);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidDataEven);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidDataOdd);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidMask_);
        HANDLE_ERROR(error);
#endif
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockData<T, Lattice>::stream() {
        if (streamEven) {
            fluidDataCurrent = fluidDataOdd;
            fluidDataPrevious = fluidDataEven;
        } else {
            fluidDataCurrent = fluidDataEven;
            fluidDataPrevious = fluidDataOdd;
        }
        streamEven = !streamEven;
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::setFluidMaskForCell(bool flag, int iX, int iY, int iZ) {
        fluidMask_[indexFor(iX, iY, iZ)] = flag;
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::setMaterialNumberForCell(unsigned int material, int iX, int iY, int iZ) {
        materialNumber_[indexFor(iX, iY, iZ)] = material;
    }

#ifdef ENABLE_CUDA
    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::gpuStream() {
        if (gpuStreamEven) {
            gpuFluidDataCurrent = gpuFluidDataOdd;
            gpuFluidDataPrevious = gpuFluidDataEven;
        } else {
            gpuFluidDataCurrent = gpuFluidDataEven;
            gpuFluidDataPrevious = gpuFluidDataOdd;
        }
        gpuStreamEven = !gpuStreamEven;
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::copyCellDataToGPU() {
//        std::cout << "copyCellDataToGPU" << std::endl;
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaMemcpyAsync(gpuFluidDataCurrent[i], fluidDataCurrent[i], sizeof(T) * gridSize_, cudaMemcpyHostToDevice);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(gpuFluidMask_, fluidMask_, sizeof(bool) * gridSize_, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::copyCellDataToCPU() {
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaMemcpy(fluidDataCurrent[i], gpuFluidDataCurrent[i], sizeof(T) * gridSize_, cudaMemcpyDeviceToHost);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(fluidMask_, gpuFluidMask_, sizeof(bool) * gridSize_, cudaMemcpyDeviceToHost);
        HANDLE_ERROR(error);
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::copyVizDataToCPU(bool copyRho, bool copyU, bool copyForce, bool copyFluidMask) {
        if (copyRho) {
            cudaError_t error = cudaMemcpy(fluidDataCurrent[Lattice<T>::rhoIndex], gpuFluidDataCurrent[Lattice<T>::rhoIndex], sizeof(T) * gridSize_, cudaMemcpyDeviceToHost);
            HANDLE_ERROR(error);
        }
        if (copyU) {
            for (int i = Lattice<T>::uIndex; i < Lattice<T>::uIndex+Lattice<T>::d; ++i) {
                cudaError_t error = cudaMemcpy(fluidDataCurrent[i], gpuFluidDataCurrent[i], sizeof(T) * gridSize_, cudaMemcpyDeviceToHost);
                HANDLE_ERROR(error);
            }
        }
        if (copyForce) {
            for (int i = Lattice<T>::forceIndex; i < Lattice<T>::forceIndex+Lattice<T>::d; ++i) {
                cudaError_t error = cudaMemcpy(fluidDataCurrent[i], gpuFluidDataCurrent[i], sizeof(T) * gridSize_, cudaMemcpyDeviceToHost);
                HANDLE_ERROR(error);
            }
        }
        if (copyFluidMask) {
            cudaError_t error = cudaMemcpy(fluidMask_, gpuFluidMask_, sizeof(bool) * gridSize_, cudaMemcpyDeviceToHost);
            HANDLE_ERROR(error);
        }
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockData<T, Lattice>::copyFluidMaskToGPU() {
        cudaError_t error = cudaMemcpy(gpuFluidMask_, fluidMask_, sizeof(bool) * gridSize_, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);
    }
#endif

    /**
     * Getter
     */

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData<T, Lattice>::getData(int iX, int iY, int iZ) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][indexFor(iX, iY, iZ)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData<T, Lattice>::getData(int iX, int iY, int iZ) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][indexFor(iX, iY, iZ)];
        }

        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData<T, Lattice>::getData(size_t i) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockData<T, Lattice>::getData(size_t i) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellPopulationArray<T, Lattice> CellBlockData<T, Lattice>::getPopulations(int iX, int iY, int iZ) const {
        CellPopulationArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][indexFor(iX, iY, iZ)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    T *CellBlockData<T, Lattice>::getCellData(int iData, int iX, int iY, int iZ) const {
        return &fluidDataCurrent[iData][indexFor(iX, iY, iZ)];
    }

    template<typename T, template<typename U> class Lattice>
    T *CellBlockData<T, Lattice>::getCellDataPrevious(int iData, int iX, int iY, int iZ) const {
        return &fluidDataPrevious[iData][indexFor(iX, iY, iZ)];
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockData<T, Lattice>::getCellData() const {
        return fluidDataCurrent;
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockData<T, Lattice>::getCellDataPrevious() const {
        return fluidDataPrevious;
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockData<T, Lattice>::getNonOffsetCellData() const {
        return fluidData_;
    }

    template<typename T, template<typename U> class Lattice>
    bool* CellBlockData<T, Lattice>::getFluidMask() {
        return fluidMask_;
    }

    template<typename T, template<typename U> class Lattice>
    bool CellBlockData<T, Lattice>::getFluidMask(int iX, int iY, int iZ) {
        return fluidMask_[indexFor(iX, iY, iZ)];
    }

    template<typename T, template<typename U> class Lattice>
    unsigned int* CellBlockData<T, Lattice>::getMaterialNumber() {
        return materialNumber_;
    }

    template<typename T, template<typename U> class Lattice>
    unsigned int CellBlockData<T, Lattice>::getMaterialNumber(int iX, int iY, int iZ) {
        return materialNumber_[indexFor(iX, iY, iZ)];
    }

#ifdef ENABLE_CUDA
    template <typename T, template <typename> class Lattice>
    T** CellBlockData<T, Lattice>::gpuGetFluidData() {
        return gpuFluidDataCurrent;
    }

    template <typename T, template <typename> class Lattice>
    T** CellBlockData<T, Lattice>::gpuGetFluidDataPrevious() {
        return gpuFluidDataPrevious;
    }

    template <typename T, template <typename> class Lattice>
    T** CellBlockData<T, Lattice>::gpuGetNonOffsetFluidData() {
        return gpuFluidData_;
    }

    template <typename T, template <typename> class Lattice>
    bool* CellBlockData<T, Lattice>::gpuGetFluidMask() {
        return gpuFluidMask_;
    }
#endif




template<typename T, template<typename U> class Lattice>
CellBlockDataALE<T, Lattice>::CellBlockDataALE(size_t nx, size_t ny, size_t nz):
    CellBlockData<T, Lattice>(nx, ny, nz),
#ifdef ENABLE_CUDA
        gpuFluidDataALE_(nullptr),
        gpuFluidDataEvenALE(nullptr),
        gpuFluidDataOddALE(nullptr),
        gpuFluidDataCurrentALE(nullptr),
        gpuFluidMaskALE_(nullptr),
#endif
        fluidDataALE_(new T *[Lattice<T>::q]),
        fluidDataEvenALE(new T *[Lattice<T>::dataSize]),
        fluidDataOddALE(new T *[Lattice<T>::dataSize]),
        fluidDataCurrentALE(nullptr),
        fluidMaskALE_(new bool [nx*ny*nz])
        {
            for (unsigned int i = 0; i < Lattice<T>::q; ++i) {
                int offset = util::getGridOffset<Lattice<T>>(i, this->ny_, this->nz_);
                fluidDataALE_[i] = new T[this->gridSize_ + std::abs(offset)];

                for (int j = 0; j < this->gridSize_ + std::abs(offset); ++j) {
                    fluidDataALE_[i][j] = T();
                }
            }

            for (int i = Lattice<T>::q; i < Lattice<T>::dataSize; ++i) {
                fluidDataEvenALE[i] = this->fluidData_[i];
                fluidDataOddALE[i] = this->fluidData_[i];
            }

            for (int i = 0; i < Lattice<T>::q; ++i) {
                int offset = util::getGridOffset<Lattice<T>>(i, this->ny_, this->nz_);
                int opposite = Lattice<T>::opposite(i);

                fluidDataEvenALE[i] = fluidDataALE_[i];
                fluidDataOddALE[i] = fluidDataALE_[opposite];

                if (offset > 0) {
                    fluidDataEvenALE[i] += offset;
                    fluidDataOddALE[i] += offset;
                }
            }

            fluidDataCurrentALE = fluidDataEvenALE;
#ifdef ENABLE_CUDA
            cudaError_t error = cudaMallocManaged(&gpuFluidMaskALE_, nx*ny*nz * sizeof(bool));
            HANDLE_ERROR(error);
            error = cudaMallocManaged(&gpuFluidDataALE_, Lattice<T>::q * sizeof(T*));
            HANDLE_ERROR(error);
            error = cudaMallocManaged(&gpuFluidDataEvenALE, Lattice<T>::dataSize * sizeof(T*));
            HANDLE_ERROR(error);
            error = cudaMallocManaged(&gpuFluidDataOddALE, Lattice<T>::dataSize * sizeof(T*));
            HANDLE_ERROR(error);

            for (unsigned int i = 0; i < Lattice<T>::q; ++i) {
                int offset = util::getGridOffset<Lattice<T>>(i, this->ny_, this->nz_);
                error = cudaMallocManaged(&gpuFluidDataALE_[i], sizeof(T) * (this->gridSize_ + std::abs(offset)));
                HANDLE_ERROR(error);
            }

            for (unsigned int i = Lattice<T>::q; i < Lattice<T>::dataSize; ++i) {
                gpuFluidDataEvenALE[i] = this->gpuFluidData_[i];
                gpuFluidDataOddALE[i] = this->gpuFluidData_[i];
            }

            for (unsigned int i = 0; i < Lattice<T>::q; ++i) {
                int offset = util::getGridOffset<Lattice<T>>(i, this->ny_, this->nz_);
                gpuFluidDataEvenALE[i] = gpuFluidDataALE_[i];
                gpuFluidDataOddALE[i] = gpuFluidDataALE_[Lattice<T>::opposite(i)];

                if (offset > 0) {
                    gpuFluidDataEvenALE[i] += offset;
                    gpuFluidDataOddALE[i] += offset;
                }

                cudaMemset(gpuFluidDataALE_[i], 0, sizeof(T) * (this->gridSize_ + std::abs(offset)));
            }
            gpuFluidDataCurrentALE = gpuFluidDataEvenALE;

            error = cudaMalloc(&gpuFluidMaskALE_, sizeof(bool) * this->gridSize_);
            HANDLE_ERROR(error);
            cudaMemset(gpuFluidMaskALE_, 0, sizeof(bool) * this->gridSize_);
#endif
        }

    template<typename T, template<typename U> class Lattice>
    CellBlockDataALE<T, Lattice>::~CellBlockDataALE<T, Lattice>() {
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            delete[] fluidDataALE_[iPop];
        }
        delete[] fluidDataALE_;

        delete[] fluidDataEvenALE;
        delete[] fluidDataOddALE;

        delete[] fluidMaskALE_;

#ifdef ENABLE_CUDA
        for (int i = 0; i < Lattice<T>::q; ++i) {
            cudaError_t error = cudaFree(gpuFluidDataALE_[i]);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaFree(gpuFluidDataALE_);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidDataEvenALE);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidDataOddALE);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidMaskALE_);
        HANDLE_ERROR(error);
#endif
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockDataALE<T, Lattice>::stream() {
        if (this->streamEven) {
            if (!aleEven) {
                fluidDataCurrentALE = fluidDataOddALE;
                this->fluidDataCurrent = this->fluidDataOdd;
            } else {
                fluidDataCurrentALE = this->fluidDataOdd;
                this->fluidDataCurrent = fluidDataOddALE;
            }
        } else {
            if (!aleEven) {
                fluidDataCurrentALE = fluidDataEvenALE;
                this->fluidDataCurrent = this->fluidDataEven;
            } else {
                fluidDataCurrentALE = this->fluidDataEven;
                this->fluidDataCurrent = fluidDataEvenALE;
            }
        }
        this->streamEven = !this->streamEven;
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockDataALE<T, Lattice>::moveMesh() {
        std::swap(this->fluidDataCurrent, fluidDataCurrentALE);
        aleEven = !aleEven;
    }

#ifdef ENABLE_CUDA
    template <typename T, template <typename> class Lattice>
    void CellBlockDataALE<T, Lattice>::gpuStream() {
        if (this->gpuStreamEven) {
            if (!gpuAleEven) {
                gpuFluidDataCurrentALE = gpuFluidDataOddALE;
                this->gpuFluidDataCurrent = this->gpuFluidDataOdd;
            } else {
                gpuFluidDataCurrentALE = this->gpuFluidDataOdd;
                this->gpuFluidDataCurrent = gpuFluidDataOddALE;
            }
        } else {
            if (!gpuAleEven) {
                gpuFluidDataCurrentALE = gpuFluidDataEvenALE;
                this->gpuFluidDataCurrent = this->gpuFluidDataEven;
            } else {
                gpuFluidDataCurrentALE = this->gpuFluidDataEven;
                this->gpuFluidDataCurrent = gpuFluidDataEvenALE;
            }
        }
        this->gpuStreamEven = !this->gpuStreamEven;
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockDataALE<T, Lattice>::gpuMoveMesh() {
        std::swap(this->gpuFluidDataCurrent, gpuFluidDataCurrentALE);
        gpuAleEven = !gpuAleEven;
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockDataALE<T, Lattice>::copyCellDataToGPU() {
        CellBlockData<T, Lattice>::copyCellDataToGPU();
        for (int i = 0; i < Lattice<T>::q; ++i) {
            cudaError_t error = cudaMemcpy(gpuFluidDataCurrentALE[i], fluidDataCurrentALE[i], sizeof(T) * this->gridSize_, cudaMemcpyHostToDevice);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(gpuFluidMaskALE_,fluidMaskALE_,sizeof(bool)*this->gridSize_, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);

        error = cudaMemcpy(this->gpuFluidMask_,this->fluidMask_,sizeof(bool)*this->gridSize_, cudaMemcpyHostToDevice);
		HANDLE_ERROR(error);
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockDataALE<T, Lattice>::copyCellDataToCPU() {
        CellBlockData<T, Lattice>::copyCellDataToCPU();
        for (int i = 0; i < Lattice<T>::q; ++i) {
            cudaError_t error = cudaMemcpy(fluidDataCurrentALE[i], gpuFluidDataCurrentALE[i], sizeof(T) * this->gridSize_, cudaMemcpyDeviceToHost);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(fluidMaskALE_,gpuFluidMaskALE_,sizeof(bool)*this->gridSize_, cudaMemcpyDeviceToHost);
        HANDLE_ERROR(error);

        error = cudaMemcpy(this->fluidMask_,this->gpuFluidMask_,sizeof(bool)*this->gridSize_, cudaMemcpyDeviceToHost);
		HANDLE_ERROR(error);
    }

#endif

    template<typename T,template<typename> class Lattice>
    void CellBlockDataALE<T,Lattice>::setCurrentFluidMaskAsStationary()
    {
      for (size_t index=0;index<this->nx_*this->ny_*this->nz_;++index)
      {
        fluidMaskALE_[index]=this->fluidMask_[index];
      }
#ifdef ENABLE_CUDA
        cudaError_t error = cudaMemcpy(gpuFluidMaskALE_, fluidMaskALE_, sizeof(bool) * this->gridSize_, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);
#endif 
    }

    /**
     * Getter
     */

    template<typename T, template<typename U> class Lattice>
    bool* CellBlockDataALE<T, Lattice>::getStationaryFluidMask() {
        return this->fluidMask_;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE<T, Lattice>::getDataALE(int iX, int iY, int iZ) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][this->indexFor(iX, iY, iZ)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE<T, Lattice>::getDataALE(int iX, int iY, int iZ) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][this->indexFor(iX, iY, iZ)];
        }

        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE<T, Lattice>::getDataALE(size_t i) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataALE<T, Lattice>::getDataALE(size_t i) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellPopulationArray<T, Lattice> CellBlockDataALE<T, Lattice>::getPopulationsALE(int iX, int iY, int iZ) const {
        CellPopulationArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            ret.data[iPop] = &fluidDataCurrentALE[iPop][this->indexFor(iX, iY, iZ)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    T *CellBlockDataALE<T, Lattice>::getCellDataALE(int iData, int iX, int iY, int iZ) const {
        return &fluidDataCurrentALE[iData][this->indexFor(iX, iY, iZ)];
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockDataALE<T, Lattice>::getCellDataALE() const {
        return fluidDataCurrentALE;
    }

    template<typename T, template<typename U> class Lattice>
    bool* CellBlockDataALE<T, Lattice>::getFluidMaskALE() const {
        return fluidMaskALE_;
    }

#ifdef ENABLE_CUDA
    template<typename T, template<typename U> class Lattice>
    T **CellBlockDataALE<T, Lattice>::gpuGetFluidDataALE() const {
        return gpuFluidDataCurrentALE;
    }

    template <typename T, template <typename> class Lattice>
    bool* CellBlockDataALE<T, Lattice>::gpuGetStationaryFluidMask() {
        return gpuGetFluidMaskALE();
    }

    template<typename T, template<typename U> class Lattice>
    bool *CellBlockDataALE<T, Lattice>::gpuGetFluidMaskALE() const {
        return gpuFluidMaskALE_;
    }
#endif
}
#endif //OPENLB_CELLBLOCKDATA_HH
