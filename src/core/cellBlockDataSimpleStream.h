/*
 * cellBlockDataSimpleStream.h
 *
 *  Created on: May 27, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CORE_CELLBLOCKDATASIMPLESTREAM_H_
#define SRC_CORE_CELLBLOCKDATASIMPLESTREAM_H_

#include "util.h"
#include "celldataarray.h"

namespace olb {

    template<typename T, template<typename U> class Lattice>
    class CellBlockDataSimple {
    protected:
        size_t nx_;
        size_t nz_;
        size_t ny_;

        size_t gridSize_;

        T **fluidDataFirst_;
        T **fluidDataSecond_;
        T **fluidDataCurrent;
        T **fluidDataOld;
        bool *fluidMask_;

#ifdef ENABLE_CUDA
        T **fluidDataFirstGPU_;
        T **fluidDataSecondGPU_;
        T **fluidDataCurrentGPU;
        T **fluidDataOldGPU;
        bool *gpuFluidMask_;
#endif

        constexpr size_t indexFor(size_t iX, size_t iY, size_t iZ = 0) const {
            return util::getCellIndex3D(iX, iY, iZ, ny_, nz_);
        }

    public:
        CellBlockDataSimple(size_t nx, size_t ny, size_t nz = 1);

        virtual ~CellBlockDataSimple();

        virtual void stream();

        void setFluidMaskForCell(bool flag, int iX, int iY, int iZ = 0);

#ifdef ENABLE_CUDA
        virtual void gpuStream();

        virtual void copyCellDataToGPU();

        virtual void copyCellDataToCPU();
#endif

        /**
         * Getter
         */

        size_t getNx() { return nx_; };
        size_t getNy() { return ny_; };
        size_t getNz() { return nz_; };
        size_t getGridSize() { return gridSize_; };

        CellDataArray<T, Lattice> getData(int iX, int iY, int iZ = 0);

        CellDataArray<T, Lattice> getData(size_t i);

        CellDataArray<T, Lattice> getData(int iX, int iY, int iZ = 0) const;

        CellDataArray<T, Lattice> getData(size_t i) const;

        CellPopulationArray<T, Lattice> getPopulations(int iX, int iY, int iZ = 0) const;

        T* getCellData(int iPop, int iX, int iY, int iZ = 0) const;

        T** getCellData() const;

        T** getCellDataOld() const;

        bool* getFluidMask();

        bool getFluidMask(int iX, int iY, int iZ = 0);

#ifdef ENABLE_CUDA
        T** gpuGetFluidData();

        T** gpuGetFluidDataOld();

        bool* gpuGetFluidMask();
#endif
    };

}

#endif /* SRC_CORE_CELLBLOCKDATASIMPLESTREAM_H_ */
