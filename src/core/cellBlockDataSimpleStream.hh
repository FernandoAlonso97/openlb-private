/*
 * cellBlockDataSimple.hh
 *
 *  Created on: May 27, 2019
 *      Author: Bastian Horvat
 */

#ifndef SRC_CORE_CELLBLOCKDATASIMPLESTREAM_HH_
#define SRC_CORE_CELLBLOCKDATASIMPLESTREAM_HH_

#include "cellBlockDataSimpleStream.h"
#include "cudaErrorHandler.h"

namespace olb {

template<typename T, template<typename U> class Lattice>
CellBlockDataSimple<T, Lattice>::CellBlockDataSimple(size_t nx, size_t ny, size_t nz):
        nx_(nx),
        ny_(ny),
        nz_(nz),
        gridSize_((nx_ * ny_ * nz_)),
        fluidDataFirst_(new T *[Lattice<T>::dataSize]),
        fluidDataSecond_(new T *[Lattice<T>::dataSize]),
        fluidDataCurrent(nullptr),
        fluidDataOld(nullptr),
        fluidMask_(new bool[gridSize_])
#ifdef ENABLE_CUDA
        ,
        fluidDataFirstGPU_(nullptr),
        fluidDataSecondGPU_(nullptr),
        fluidDataCurrentGPU(nullptr),
        fluidDataOldGPU(nullptr),
        gpuFluidMask_(nullptr)
#endif
    {
        for (size_t i = 0; i < gridSize_; ++i)
        {
            fluidMask_[i] = true;
        }

        for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i) {
            fluidDataFirst_[i] = new T[gridSize_];
            fluidDataSecond_[i] = new T[gridSize_];
        }

        fluidDataCurrent = fluidDataFirst_;
        fluidDataOld = fluidDataSecond_;

#ifdef ENABLE_CUDA
        auto memory_size = (Lattice<T>::dataSize) * sizeof(T*);
        cudaError_t error = cudaMallocManaged(&fluidDataFirstGPU_, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&fluidDataSecondGPU_, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&fluidDataCurrentGPU, memory_size);
        HANDLE_ERROR(error);
        error = cudaMallocManaged(&fluidDataOldGPU, memory_size);
        HANDLE_ERROR(error);

        for (unsigned int i = 0; i < Lattice<T>::dataSize; ++i)
        {
            error = cudaMalloc(&fluidDataFirstGPU_[i], sizeof(T) * gridSize_);
            HANDLE_ERROR(error);
            error = cudaMalloc(&fluidDataSecondGPU_[i], sizeof(T) * gridSize_);
            HANDLE_ERROR(error);
            error = cudaMemset(fluidDataFirstGPU_[i], 0, sizeof(T) * gridSize_);
            HANDLE_ERROR(error);
            error = cudaMemset(fluidDataSecondGPU_[i], 0, sizeof(T) * gridSize_);
            HANDLE_ERROR(error);
        }

        error = cudaMalloc(&gpuFluidMask_, sizeof(bool) * gridSize_);
        HANDLE_ERROR(error);
        error = cudaMemset(gpuFluidMask_, 0, sizeof(bool) * gridSize_);
        HANDLE_ERROR(error);

        fluidDataCurrentGPU = fluidDataFirstGPU_;
        fluidDataOldGPU = fluidDataSecondGPU_;
#endif
    }

    template<typename T, template<typename U> class Lattice>
    CellBlockDataSimple<T, Lattice>::~CellBlockDataSimple<T, Lattice>() {
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            delete[] fluidDataFirst_[i];
            delete[] fluidDataSecond_[i];
        }

        delete [] fluidDataFirst_;
        delete [] fluidDataSecond_;

        delete[] fluidMask_;

#ifdef ENABLE_CUDA

        cudaError_t error;
        for (int i = 0; i < Lattice<T>::dataSize; ++i)
        {
            error = cudaFree(fluidDataFirstGPU_[i]);
            HANDLE_ERROR(error);
            error = cudaFree(fluidDataSecondGPU_[i]);
            HANDLE_ERROR(error);
        }

        error = cudaFree(fluidDataFirstGPU_);
        HANDLE_ERROR(error);
        error = cudaFree(fluidDataSecondGPU_);
        HANDLE_ERROR(error);
        error = cudaFree(gpuFluidMask_);
        HANDLE_ERROR(error);
#endif
    }

    template<typename T, template<typename U> class Lattice>
    void CellBlockDataSimple<T, Lattice>::stream()
    {
        std::swap(fluidDataCurrent,fluidDataOld);
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockDataSimple<T, Lattice>::setFluidMaskForCell(bool flag, int iX, int iY, int iZ) {
        fluidMask_[indexFor(iX, iY, iZ)] = flag;
    }

#ifdef ENABLE_CUDA
    template <typename T, template <typename> class Lattice>
    void CellBlockDataSimple<T, Lattice>::gpuStream()
        {
            std::swap(fluidDataCurrentGPU,fluidDataOldGPU);
        }

    template <typename T, template <typename> class Lattice>
    void CellBlockDataSimple<T, Lattice>::copyCellDataToGPU() {
        cudaDeviceSynchronize();
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaMemcpy(fluidDataCurrentGPU[i], fluidDataCurrent[i], sizeof(T) * gridSize_, cudaMemcpyHostToDevice);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(gpuFluidMask_, fluidMask_, sizeof(bool) * gridSize_, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);
    }

    template <typename T, template <typename> class Lattice>
    void CellBlockDataSimple<T, Lattice>::copyCellDataToCPU() {
        cudaDeviceSynchronize();
        for (int i = 0; i < Lattice<T>::dataSize; ++i) {
            cudaError_t error = cudaMemcpy(fluidDataCurrent[i], fluidDataCurrentGPU[i], sizeof(T) * gridSize_, cudaMemcpyDeviceToHost);
            HANDLE_ERROR(error);
        }
        cudaError_t error = cudaMemcpy(fluidMask_, gpuFluidMask_, sizeof(bool) * gridSize_, cudaMemcpyDeviceToHost);
        HANDLE_ERROR(error);
    }

#endif

    /**
     * Getter
     */

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataSimple<T, Lattice>::getData(int iX, int iY, int iZ) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][indexFor(iX, iY, iZ)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataSimple<T, Lattice>::getData(int iX, int iY, int iZ) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][indexFor(iX, iY, iZ)];
        }

        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataSimple<T, Lattice>::getData(size_t i) {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellDataArray<T, Lattice> CellBlockDataSimple<T, Lattice>::getData(size_t i) const {
        CellDataArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::dataSize; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][i];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    CellPopulationArray<T, Lattice> CellBlockDataSimple<T, Lattice>::getPopulations(int iX, int iY, int iZ) const {
        CellPopulationArray<T, Lattice> ret;
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
            ret.data[iPop] = &fluidDataCurrent[iPop][indexFor(iX, iY, iZ)];
        }
        return ret;
    }

    template<typename T, template<typename U> class Lattice>
    T *CellBlockDataSimple<T, Lattice>::getCellData(int iData, int iX, int iY, int iZ) const {
        return &fluidDataCurrent[iData][indexFor(iX, iY, iZ)];
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockDataSimple<T, Lattice>::getCellData() const {
        return fluidDataCurrent;
    }

    template<typename T, template<typename U> class Lattice>
    T **CellBlockDataSimple<T, Lattice>::getCellDataOld() const {
        return fluidDataOld;
    }

    template<typename T, template<typename U> class Lattice>
    bool* CellBlockDataSimple<T, Lattice>::getFluidMask() {
        return fluidMask_;
    }

    template<typename T, template<typename U> class Lattice>
    bool CellBlockDataSimple<T, Lattice>::getFluidMask(int iX, int iY, int iZ) {
        return fluidMask_[indexFor(iX, iY, iZ)];
    }

#ifdef ENABLE_CUDA
    template <typename T, template <typename> class Lattice>
    T** CellBlockDataSimple<T, Lattice>::gpuGetFluidData() {
        return fluidDataCurrentGPU;
    }

    template <typename T, template <typename> class Lattice>
    T** CellBlockDataSimple<T, Lattice>::gpuGetFluidDataOld() {
        return fluidDataOldGPU;
    }

    template <typename T, template <typename> class Lattice>
    bool* CellBlockDataSimple<T, Lattice>::gpuGetFluidMask() {
        return gpuFluidMask_;
    }
#endif

}

#endif /* SRC_CORE_CELLBLOCKDATASIMPLESTREAM_HH_ */
