#ifndef CORE_CELLDATAARRAY_H
#define CORE_CELLDATAARRAY_H

template<typename T, template<typename U> class Lattice>
struct CellDataArray
{
  T* data[Lattice<T>::dataSize];
};

template<typename T, template<typename U> class Lattice>
struct CellPopulationArray
{
    T* data[Lattice<T>::q];
};

#endif
