#ifndef SRC_CORE_CONFIG_H
#define SRC_CORE_CONFIG_H

constexpr int vectorWidth = 16;

#ifdef __CUDACC__
#define OPENLB_HOST_DEVICE __host__ __device__ __forceinline__
#else
#define OPENLB_HOST_DEVICE
#endif

#ifdef __CUDACC__
#define OPENLB_HOST __host__  __forceinline__
#else
#define OPENLB_HOST
#endif

#ifdef __CUDACC__
#define OPENLB_DEVICE  __device__ __forceinline__
#else
#define OPENLB_DEVICE
#endif

#ifdef __CUDACC__
#define OPENLB_RESTRICT __restrict
#else
#define OPENLB_RESTRICT __restrict__
#endif

#endif
