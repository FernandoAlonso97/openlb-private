#ifndef CUDAERRORHANDLER_H
#define CUDAERRORHANDLER_H

#ifdef ENABLE_CUDA

#include <cuda.h>
#include <stdio.h>

#define HANDLE_ERROR( err ) ( CudaHandleError( err, __FILE__, __LINE__ ) )

inline static void CudaHandleError( cudaError_t err, const char *file, int line )
{
	if (err != cudaSuccess)
	  {
	  		printf( "Code %i: %s in %s at line %d\n", err, cudaGetErrorString( err ),
									file, line );
// #ifndef OLB_DEBUG
	  		exit( EXIT_FAILURE );
// #endif
	  	}
}
#endif
#endif
