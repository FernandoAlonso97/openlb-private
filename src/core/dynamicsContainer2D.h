#ifndef DYNAMICS_CONTAINER_H
#define DYNAMICS_CONTAINER_H

#include <vector>
#include <unordered_map>
#include <set>
#include <map>

#include "dynamics/dynamics.h"
#include "geometry/blockGeometryStructure2D.h"
#include "functors/lattice/indicator/blockIndicatorBaseF2D.h"
#include "gpuhandler.h"
#include "cellBlockData.h"


namespace olb {

typedef uint16_t FluidMaskType;

namespace detail {

struct pairhash {
public:
  template <typename T, typename U>
  std::size_t operator()(const std::pair<T, U> &x) const
  {
    return std::hash<T>()(x.first) ^ std::hash<U>()(x.second);
  }
};

}

template<typename T, template<typename U> class Lattice>
class DynamicsContainer2D
{
private:

  int _nx;
  int _ny;

  CellBlockData<T, Lattice>* cellData;

  std::unordered_map<unsigned int, Dynamics<T,Lattice> *> _boundaryDynamicsByMaterial;
  std::unordered_map<Dynamics<T,Lattice> *, unsigned int> _boundaryDynamicsByDynamics;
  std::set<Dynamics<T,Lattice>*> _boundaryDynamicsSet;

  Dynamics<T,Lattice>* _fluidDynamics;

  std::map<Dynamics<T,Lattice>*, std::unique_ptr<DynamicsDataHandler<T>>> _dataHandlerMap;

#ifdef ENABLE_CUDA
  GPUHandler<T, Lattice> _gpuHandler;
#endif

public:

  DynamicsContainer2D(int nx, int ny, CellBlockData<T, Lattice>* cellData, Dynamics<T,Lattice>* fluidDynamics);
  virtual ~DynamicsContainer2D() = default;

  DynamicsContainer2D(const DynamicsContainer2D<T, Lattice>&);

  virtual void defineDynamics (int iX, int iY,
      Dynamics<T,Lattice>* dynamics );
  virtual void defineDynamics (int x0, int x1, int y0, int y1,
      Dynamics<T,Lattice>* dynamics );
  virtual void defineDynamics(BlockGeometryStructure2D<T>& blockGeometry, int material,
      Dynamics<T,Lattice>* dynamics);
  // virtual void defineDynamics(BlockIndicatorF2D<T>& indicator, int overlap,
  //     Dynamics<T,Lattice>* dynamics);

  Dynamics<T,Lattice>* getDynamics(int iX, int iY);
  const Dynamics<T,Lattice>* getDynamics(int iX, int iY) const;

  Dynamics<T,Lattice>* getFluidDynamics();

  std::unordered_map<unsigned int, Dynamics<T,Lattice> *>& getBoundaryDynamics();
  std::set<Dynamics<T,Lattice>*> getBoundaryDynamicsSet();

  void init();

  DynamicsDataHandler<T>* getDataHandler(Dynamics<T,Lattice>* dynamics);

#ifdef ENABLE_CUDA
  GPUHandler<T, Lattice>& getGPUHandler();
#endif
};

}

#endif
