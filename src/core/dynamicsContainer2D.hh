#ifndef DYNAMICS_CONTAINER_HH
#define DYNAMICS_CONTAINER_HH

#include "dynamicsContainer2D.h"

namespace olb {

template<typename T, template<typename U> class Lattice>
DynamicsContainer2D<T,Lattice>::DynamicsContainer2D(int nx, int ny, CellBlockData<T, Lattice>* cellData, Dynamics<T, Lattice>* fluidDynamics):
    _nx(nx),
    _ny(ny),
    cellData(cellData),
    _fluidDynamics(fluidDynamics)
#if ENABLE_CUDA
    ,_gpuHandler(cellData)
#endif
{}

template<typename T, template<typename U> class Lattice>
DynamicsContainer2D<T, Lattice>::DynamicsContainer2D(const DynamicsContainer2D& rhs):
  _nx(rhs._nx),
  _ny(rhs._ny),
  cellData(rhs.cellData),
  _boundaryDynamicsByMaterial(rhs._boundaryDynamicsByMaterial),
  _boundaryDynamicsByDynamics(rhs._boundaryDynamicsByDynamics),
  _fluidDynamics(rhs._fluidDynamics)
#if ENABLE_CUDA //Added by Shreyas
  ,_gpuHandler(rhs._gpuHandler)
#endif
{
  for (auto& itr : rhs._dataHandlerMap)
  {
    _dataHandlerMap.insert(std::make_pair(itr.first,  std::unique_ptr<DynamicsDataHandler<T>>(new DynamicsDataHandler<T>(itr.second->getNumberOfDataPoints(), itr.second->isFluidDataHandler()))));
  }
}

template<typename T, template<typename U> class Lattice>
void DynamicsContainer2D<T,Lattice>::defineDynamics(int iX, int iY,
    Dynamics<T,Lattice>* dynamics)
{
  bool fluidDynamics = dynamics == _fluidDynamics;
  bool isPeriodic = dynamics->isPostProcDynamics();
  bool flag = fluidDynamics or isPeriodic;

  cellData->setFluidMaskForCell(flag, iX, iY);

  if(!fluidDynamics) {
    _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
    auto pair = _boundaryDynamicsByDynamics.find(dynamics);
    _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
    cellData->setMaterialNumberForCell(pair->second, iX, iY);
  }
}

template<typename T, template<typename U> class Lattice>
void DynamicsContainer2D<T,Lattice>::defineDynamics(int x0, int x1, int y0, int y1,
    Dynamics<T,Lattice>* dynamics)
{
  bool fluidDynamics = dynamics == _fluidDynamics;
  bool isPeriodic = dynamics->isPostProcDynamics();
  bool flag = fluidDynamics or isPeriodic;
  unsigned int material = 0;

  if (!fluidDynamics) {
    _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
    auto pair = _boundaryDynamicsByDynamics.find(dynamics);
    _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
    material = pair->second;
  }

  for (int iY = y0; iY <= y1; ++iY)
  {
    for (int iX = x0; iX <= x1; ++iX)
    {
      cellData->setFluidMaskForCell(flag, iX, iY);
      if (!fluidDynamics)
        cellData->setMaterialNumberForCell(material, iX, iY);
    }
  }
}

template<typename T, template<typename U> class Lattice>
void DynamicsContainer2D<T,Lattice>::defineDynamics(BlockGeometryStructure2D<T>& geometry, int compareMaterial,
    Dynamics<T,Lattice>* dynamics)
{
  bool fluidDynamics = dynamics == _fluidDynamics;
  bool isPeriodic = dynamics->isPostProcDynamics();
  bool flag = fluidDynamics or isPeriodic;
  unsigned int material = 0;

  if (!fluidDynamics) {
    _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
    auto pair = _boundaryDynamicsByDynamics.find(dynamics);
    _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
    material = pair->second;
  }

  for (int iX = 0; iX < _nx; ++iX) {
    for (int iY = 0; iY < _ny; ++iY) {
      if (geometry.getMaterial(iX, iY) == compareMaterial) {
        cellData->setFluidMaskForCell(flag, iX, iY);
        if (!fluidDynamics)
          cellData->setMaterialNumberForCell(material, iX, iY);
      }
    }
  }
}

// template<typename T, template<typename U> class Lattice>
// void DynamicsContainer2D<T,Lattice>::defineDynamics(BlockIndicatorF2D<T>& indicator, int overlap,
//     Dynamics<T,Lattice>* dynamics)
// {
//   bool fluidDynamics = dynamics == _fluidDynamics;
//   bool isPeriodic = dynamics->isPostProcDynamics();
//   bool flag = fluidDynamics or isPeriodic;

//   for (int iX = 0; iX < _nx; ++iX) {
//     for (int iY = 0; iY < _ny; ++iY) {
//       const int blockLocation[2] = { iX-overlap, iY-overlap };
//       if (indicator(blockLocation)) {
//         cellData->setFluidMaskForCell(flag, iX, iY);
//         if (!fluidDynamics)
//           _boundaryDynamics.insert({std::make_pair(iX, iY), dynamics});
//       }
//     }
//   }
// }

template<typename T, template<typename U> class Lattice>
Dynamics<T, Lattice>* DynamicsContainer2D<T,Lattice>::getDynamics(int iX, int iY)
{
  if (cellData->getFluidMask(iX, iY))
    return _fluidDynamics;
  else {
    unsigned int material = cellData->getMaterialNumber(iX, iY);
    auto pair = _boundaryDynamicsByMaterial.find(material);
    return pair->second;
  }
  throw std::runtime_error("getDynamics broken");
  return nullptr;
}

template<typename T, template<typename U> class Lattice>
const Dynamics<T, Lattice>* DynamicsContainer2D<T,Lattice>::getDynamics(int iX, int iY) const
{
  if (cellData->getFluidMask(iX, iY))
    return _fluidDynamics;
  else {
    unsigned int material = cellData->getMaterialNumber(iX, iY);
    auto pair = _boundaryDynamicsByMaterial.find(material);
    return pair->second;
  }
  throw std::runtime_error("getDynamics broken");
  return nullptr;
}

template<typename T, template<typename U> class Lattice>
Dynamics<T,Lattice>* DynamicsContainer2D<T,Lattice>::getFluidDynamics() {
  return _fluidDynamics;
}

template<typename T, template<typename U> class Lattice>
std::unordered_map<unsigned int, Dynamics<T,Lattice> *>&
DynamicsContainer2D<T,Lattice>::getBoundaryDynamics() {
  return _boundaryDynamicsByMaterial;
}

template<typename T, template<typename U> class Lattice>
std::set<Dynamics<T,Lattice>*> DynamicsContainer2D<T,Lattice>::getBoundaryDynamicsSet()
{
  return _boundaryDynamicsSet;
}

template<typename T, template<typename U> class Lattice>
void DynamicsContainer2D<T,Lattice>::init()
{
  _dataHandlerMap.insert(std::make_pair(_fluidDynamics, std::unique_ptr<DynamicsDataHandler<T>> (new DynamicsDataHandler<T>(_fluidDynamics->getMomentaDataSize(), _fluidDynamics->getPostProcessorDataSize(), true)) ) );

  auto& boundaryDynamics = getBoundaryDynamics();
  for (auto& itr : boundaryDynamics)
  {
    _boundaryDynamicsSet.insert(itr.second);

    auto search = _dataHandlerMap.find(itr.second);

    if(search == _dataHandlerMap.end())
    {
        _dataHandlerMap.insert(std::make_pair(itr.second, std::unique_ptr<DynamicsDataHandler<T>> (new DynamicsDataHandler<T>(itr.second->getMomentaDataSize(), itr.second->getPostProcessorDataSize(), false)) ) );
    }
  }

  for (int iX = 0; iX < _nx; iX++) 
    for (int iY = 0; iY < _ny; iY++) {
      unsigned int material = cellData->getMaterialNumber(iX, iY);
      if (material != 0) {
          Dynamics<T,Lattice> * currentBoundaryDynamics = _boundaryDynamicsByMaterial.find(material)->second;
          getDataHandler(currentBoundaryDynamics)->registerCell(
              util::getCellIndex2D(iX, iY, this->_ny));
      }
    }

#if ENABLE_CUDA
  _gpuHandler.addCollisionData(getFluidDynamics());
#endif

  for (auto& itr : _boundaryDynamicsSet)
  {
    getDataHandler(itr)->allocateMemory();
#if ENABLE_CUDA
    _gpuHandler.registerDynamics(itr, getDataHandler(itr));
    _gpuHandler.addCollisionData(itr);
#endif
  }

  getDataHandler(_fluidDynamics)->allocateMemory();

#if ENABLE_CUDA
  _gpuHandler.initCudaStreams();
  _gpuHandler.initCudaEvents();
#endif
}

template<typename T, template<typename U> class Lattice>
DynamicsDataHandler<T>* DynamicsContainer2D<T,Lattice>::getDataHandler(Dynamics<T, Lattice>* dynamics)
{
  return _dataHandlerMap.find(dynamics)->second.get();
}

#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice>
GPUHandler<T, Lattice>& DynamicsContainer2D<T,Lattice>::getGPUHandler()
{
  return _gpuHandler;
}
#endif

}

#endif
