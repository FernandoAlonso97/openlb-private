#ifndef SRC_CORE_DYNAMICSCONTAINER3D_H_
#define SRC_CORE_DYNAMICSCONTAINER3D_H_

#include <vector>
#include <unordered_map>
#include <set>
#include <map>

#include "dynamics/dynamics.h"
#include "geometry/blockGeometryStructure3D.h"
// #include "functors/lattice/indicator/blockIndicatorBaseF3D.h"
#include "gpuhandler.h"


namespace olb {

    typedef uint16_t FluidMaskType;

    namespace detail {

        class Position {
        public:
            Position(int x, int y, int z) :
                    first(x), second(y), third(z) {}

            int first, second, third;

            bool operator==(const Position &rhs) const {
                if (first == rhs.first && second == rhs.second && third == rhs.third)
                    return true;
                else
                    return false;
            }
        };

        struct triplehash {
        public:
            std::size_t operator()(const Position &x) const {
                return (std::hash<int>()(x.first) ^ (std::hash<int>()(x.second) << 1)) ^ std::hash<int>()(x.third);
            }
        };

    }

    template<typename T, template<typename U> class Lattice>
    class DynamicsContainer3D {
    private:

        int _nx;
        int _ny;
        int _nz;

        CellBlockData<T, Lattice> *cellData;

        std::unordered_map<unsigned int, Dynamics<T,Lattice> *> _boundaryDynamicsByMaterial;
        std::unordered_map<Dynamics<T,Lattice> *, unsigned int> _boundaryDynamicsByDynamics;
        std::set<Dynamics<T, Lattice> *> _boundaryDynamicsSet;

        Dynamics<T, Lattice> *_fluidDynamics;        

        std::map<Dynamics<T, Lattice> *, std::unique_ptr<DynamicsDataHandler<T>>> _dataHandlerMap;

#ifdef ENABLE_CUDA
        GPUHandler<T, Lattice> _gpuHandler;
#endif

    public:

        DynamicsContainer3D(int nx, int ny, int nz, CellBlockData<T, Lattice> *cellData,
                            Dynamics<T, Lattice> *fluidDynamics);

        virtual ~DynamicsContainer3D();

        virtual void defineDynamics(int iX, int iY, int iZ,
                                    Dynamics<T, Lattice> *dynamics);

        virtual void defineDynamics(int x0, int x1, int y0, int y1, int z0, int z1,
                                    Dynamics<T, Lattice> *dynamics);

        virtual void defineDynamics(BlockGeometryStructure3D<T> &blockGeometry, int material,
                                    Dynamics<T, Lattice> *dynamics);

        //use if you want to add a dynamics to the maps, but don't actually want to define any cells with that dynamics yet. 
        //implemented for moving boundaries
        void definePlaceholderDynamics(Dynamics<T, Lattice> *dynamics);

        // virtual void defineDynamics(BlockIndicatorF3D<T> &indicator, int overlap,
                                    // Dynamics<T, Lattice> *dynamics);

        Dynamics<T, Lattice> *getDynamics(int iX, int iY, int iZ);

        const Dynamics<T, Lattice> *getDynamics(int iX, int iY, int iZ) const;

        Dynamics<T, Lattice> *getFluidDynamics();

        std::unordered_map<unsigned int, Dynamics<T,Lattice> *>& getBoundaryDynamics();

        std::set<Dynamics<T, Lattice> *> getBoundaryDynamicsSet();

        void init();

        DynamicsDataHandler<T> *getDataHandler(Dynamics<T, Lattice> *dynamics);

#ifdef ENABLE_CUDA

        GPUHandler<T, Lattice> &getGPUHandler();

#endif
    };

}

#endif
