#ifndef SRC_CORE_DYNAMICSCONTAINER3D_HH_
#define SRC_CORE_DYNAMICSCONTAINER3D_HH_

#include "dynamicsContainer3D.h"

namespace olb {

    template<typename T, template<typename U> class Lattice>
    DynamicsContainer3D<T, Lattice>::DynamicsContainer3D(int nx, int ny, int nz, CellBlockData<T, Lattice> *cellData,
                                                         Dynamics<T, Lattice> *fluidDynamics):
            _nx(nx),
            _ny(ny),
            _nz(nz),
            cellData(cellData),
            _fluidDynamics(fluidDynamics)
#if ENABLE_CUDA
            ,_gpuHandler(cellData)
#endif
    {}

    template<typename T, template<typename U> class Lattice>
    DynamicsContainer3D<T, Lattice>::~DynamicsContainer3D() {
    }

template<typename T, template<typename U> class Lattice>
void DynamicsContainer3D<T,Lattice>::defineDynamics(int iX, int iY, int iZ,
    Dynamics<T,Lattice>* dynamics)
{
  bool fluidDynamics = dynamics == _fluidDynamics;
  bool isPeriodic = dynamics->isPostProcDynamics();

  bool flag = fluidDynamics or isPeriodic;

  cellData->setFluidMaskForCell(flag, iX, iY, iZ);
  if (!fluidDynamics)
  {
      _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
      auto pair = _boundaryDynamicsByDynamics.find(dynamics);
      _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
      cellData->setMaterialNumberForCell(pair->second, iX, iY, iZ);
  }
}

template<typename T, template<typename U> class Lattice>
void DynamicsContainer3D<T, Lattice>::defineDynamics(int x0, int x1, int y0, int y1, int z0, int z1,
                                                     Dynamics<T, Lattice> *dynamics) {
    bool fluidDynamics = dynamics == _fluidDynamics;
    bool isPeriodic = dynamics->isPostProcDynamics();
    bool flag = fluidDynamics or isPeriodic;
    unsigned int material = 0;

    if (!fluidDynamics) {
        _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
        auto pair = _boundaryDynamicsByDynamics.find(dynamics);
        _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
        material = pair->second;
    }

    for (int iZ = z0; iZ <= z1; ++iZ) {
        for (int iY = y0; iY <= y1; ++iY) {
            for (int iX = x0; iX <= x1; ++iX) {
                cellData->setFluidMaskForCell(flag, iX, iY, iZ);
                if (!fluidDynamics)
                    cellData->setMaterialNumberForCell(material, iX, iY, iZ);
            }
        }
    }
}
template<typename T, template<typename U> class Lattice>
void DynamicsContainer3D<T,Lattice>::defineDynamics(BlockGeometryStructure3D<T>& geometry, int compareMaterial,
    Dynamics<T,Lattice>* dynamics)
{
  bool fluidDynamics = dynamics == _fluidDynamics;
  bool isPeriodic = dynamics->isPostProcDynamics();
  bool flag = fluidDynamics or isPeriodic;
  unsigned int material = 0;

  if (!fluidDynamics) {
    _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
    auto pair = _boundaryDynamicsByDynamics.find(dynamics);
    _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
    material = pair->second;
  }

  for (int iX = 0; iX < _nx; ++iX)
    for (int iY = 0; iY < _ny; ++iY)
        for (int iZ = 0; iZ < _nz; ++iZ)
          if (geometry.getMaterial(iX, iY,iZ) == compareMaterial) {
              cellData->setFluidMaskForCell(flag, iX, iY, iZ);
              if (!fluidDynamics)
                  cellData->setMaterialNumberForCell(material, iX, iY, iZ);
          }
}

template<typename T, template<typename U> class Lattice>
void DynamicsContainer3D<T,Lattice>::definePlaceholderDynamics(Dynamics<T,Lattice>* dynamics) {
  bool fluidDynamics = dynamics == _fluidDynamics;

  if (!fluidDynamics) {
    _boundaryDynamicsByDynamics.insert({dynamics, _boundaryDynamicsByDynamics.size()+1});
    auto pair = _boundaryDynamicsByDynamics.find(dynamics);
    _boundaryDynamicsByMaterial.insert({pair->second, dynamics});
  }


  //need to pre-make a data handler for placeholder dynamics
  _dataHandlerMap.insert(std::make_pair(dynamics, std::unique_ptr<DynamicsDataHandler<T>>(
                new DynamicsDataHandler<T>(dynamics->getMomentaDataSize(), dynamics->getPostProcessorDataSize(), false))));
}

// template<typename T, template<typename U> class Lattice>
// void DynamicsContainer3D<T, Lattice>::defineDynamics(BlockIndicatorF3D<T> &indicator, int overlap,
                                                     // Dynamics<T, Lattice> *dynamics) {
    // bool fluidDynamics = dynamics == _fluidDynamics;
    // bool isPeriodic = dynamics->isPostProcDynamics();
    // bool flag = fluidDynamics or isPeriodic;

    // for (int iX = 0; iX < _nx; ++iX) {
        // for (int iY = 0; iY < _ny; ++iY) {
            // for (int iZ = 0; iZ < _nz; ++iZ) {
                // const int blockLocation[2] = {iX - overlap, iY - overlap};
                // if (indicator(blockLocation)) {
                    // cellData->setFluidMaskForCell(flag, iX, iY, iZ);
                    // if (!fluidDynamics)
                        // _boundaryDynamics.insert({detail::Position(iX, iY, iZ), dynamics});
                // }
            // }
        // }
    // }
// }

    template<typename T, template<typename U> class Lattice>
    Dynamics<T, Lattice> *DynamicsContainer3D<T, Lattice>::getDynamics(int iX, int iY, int iZ) {
        if (cellData->getFluidMask(iX, iY, iZ))
            return _fluidDynamics;
        else {
            unsigned int material = cellData->getMaterialNumber(iX, iY, iZ);
            auto pair = _boundaryDynamicsByMaterial.find(material);
            return pair->second;
        }
        throw std::runtime_error("getDynamics broken");
        return nullptr;
    }

    template<typename T, template<typename U> class Lattice>
    const Dynamics<T, Lattice> *DynamicsContainer3D<T, Lattice>::getDynamics(int iX, int iY, int iZ) const {
        if (cellData->getFluidMask(iX, iY, iZ))
            return _fluidDynamics;
        else {
            unsigned int material = cellData->getMaterialNumber(iX, iY, iZ);
            auto pair = _boundaryDynamicsByMaterial.find(material);
            return pair->second;
        }
        throw std::runtime_error("getDynamics broken");
        return nullptr;
    }

    template<typename T, template<typename U> class Lattice>
    Dynamics<T, Lattice> *DynamicsContainer3D<T, Lattice>::getFluidDynamics() {
        return _fluidDynamics;
    }

    template<typename T, template<typename U> class Lattice>
    std::unordered_map<unsigned int, Dynamics<T,Lattice> *>&
    DynamicsContainer3D<T, Lattice>::getBoundaryDynamics() {
        return _boundaryDynamicsByMaterial;
    }

    template<typename T, template<typename U> class Lattice>
    std::set<Dynamics<T, Lattice> *> DynamicsContainer3D<T, Lattice>::getBoundaryDynamicsSet() {
        return _boundaryDynamicsSet;
    }

    template<typename T, template<typename U> class Lattice>
    void DynamicsContainer3D<T, Lattice>::init() {
        _dataHandlerMap.insert(std::make_pair(_fluidDynamics, std::unique_ptr<DynamicsDataHandler<T>>(
                new DynamicsDataHandler<T>(_fluidDynamics->getMomentaDataSize(), _fluidDynamics->getPostProcessorDataSize(), true))));

        auto &boundaryDynamics = getBoundaryDynamics();
        for (auto &itr : boundaryDynamics) {
            _boundaryDynamicsSet.insert(itr.second);

            auto search = _dataHandlerMap.find(itr.second);

            if (search == _dataHandlerMap.end()) {
                _dataHandlerMap.insert(std::make_pair(itr.second, std::unique_ptr<DynamicsDataHandler<T>>(
                        new DynamicsDataHandler<T>(itr.second->getMomentaDataSize(), itr.second->getPostProcessorDataSize(), false))));
            }
        }

        for (int iX = 0; iX < _nx; iX++) 
            for (int iY = 0; iY < _ny; iY++)
                for (int iZ = 0; iZ < _nz; iZ++) {
                    unsigned int material = cellData->getMaterialNumber(iX, iY, iZ);
                    if (material != 0) {
                        Dynamics<T,Lattice> * currentBoundaryDynamics = _boundaryDynamicsByMaterial.find(material)->second;
                        getDataHandler(currentBoundaryDynamics)->registerCell(
                            util::getCellIndex3D(iX, iY, iZ, this->_ny, this->_nz));
                    }
                }
        

#if ENABLE_CUDA
        _gpuHandler.addCollisionData(getFluidDynamics());
#endif

        for (auto &itr : _boundaryDynamicsSet) {
            getDataHandler(itr)->allocateMemory();
            getDataHandler(itr)->sort();
#if ENABLE_CUDA
            _gpuHandler.registerDynamics(itr, getDataHandler(itr));
            _gpuHandler.addCollisionData(itr);
#endif
        }

        getDataHandler(_fluidDynamics)->allocateMemory();

#if ENABLE_CUDA
        _gpuHandler.initCudaStreams();
        _gpuHandler.initCudaEvents();
#endif
    }

    template<typename T, template<typename U> class Lattice>
    DynamicsDataHandler<T> *DynamicsContainer3D<T, Lattice>::getDataHandler(Dynamics<T, Lattice> *dynamics) {
    	auto search = _dataHandlerMap.find(dynamics);
		if(search != _dataHandlerMap.end())
			return search->second.get();
		else
			return nullptr;
    }

#ifdef ENABLE_CUDA

    template<typename T, template<typename U> class Lattice>
    GPUHandler<T, Lattice> &DynamicsContainer3D<T, Lattice>::getGPUHandler() {
        return _gpuHandler;
    }

#endif

}

#endif
