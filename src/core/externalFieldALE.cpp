//
// Created by andi on 30/01/19.
//

#include "externalFieldALE.h"
#include "externalFieldALE.hh"
#include <dynamics/latticeDescriptors.h>

namespace olb {
    template class ExternalField<double, descriptors::D2Q9Descriptor>;
    template class ExternalField<double, descriptors::D3Q19Descriptor>;
}