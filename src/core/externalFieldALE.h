//
// Created by andi on 30/01/19.
//

#ifndef OPENLB_EXTERNALFIELDPROVIDER_H
#define OPENLB_EXTERNALFIELDPROVIDER_H

#include "affineTransform.h"

namespace olb {


template<typename T, template<typename U> class Lattice>
class ConstExternalField {
public:
  explicit ConstExternalField(const Vec<T, Lattice<T>::d> velocity);
  ~ConstExternalField() = default;

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation, const AffineTransform<T, Lattice<T>::d> ef_location);

  OPENLB_HOST_DEVICE
  T populationValueFor(size_t iPop, long long int x, long long int y, long long int z = 1) const;

  OPENLB_HOST_DEVICE
  Vec<T, Lattice<T>::d> getBaseVelocity();

protected:
  const Vec<T, Lattice<T>::d> base_velocity_;
  Vec<T, Lattice<T>::q> current_populations_;    // base_velocity_ is rotated
};

template<typename T, template<typename U> class Lattice>
class ZeroExternalField {
public:
  ZeroExternalField() = default;
  ~ZeroExternalField() = default;

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
      const AffineTransform<T, Lattice<T>::d> ef_location);

  OPENLB_HOST_DEVICE
  T populationValueFor(const size_t iPop, const size_t x, const size_t y,
                       const size_t z = 1) const;

};

/**
 * Vortex Fields
 */

template<typename T, template<typename U> class Lattice>
class VortexHorizontalExternalField {
public:
  VortexHorizontalExternalField(T posX, T posZ, T coreRadius, T vorticity);
  ~VortexHorizontalExternalField() = default;

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation, const AffineTransform<T, Lattice<T>::d> ef_location);

  OPENLB_HOST_DEVICE
  T populationValueFor(size_t iPop, size_t x, size_t y, size_t z) const;

private:
  OPENLB_HOST_DEVICE
  Vec<T, Lattice<T>::d> velocityAt(const Vec<T, Lattice<T>::d>& point) const;

protected:
  AffineTransform<T, Lattice<T>::d> vel_rotation_; // transform for the current timestep.
  AffineTransform<T, Lattice<T>::d> ef_location_transform_; // transform for finding location in external field

  const T posX_;
  const T posZ_;
  const T coreRadius2_; // square of core radius
  const T vorticity_;
};


template<typename T, template<typename U> class Lattice>
class VortexVerticalExternalField {
public:
  VortexVerticalExternalField(T posX, T posY, T coreRadius, T vorticity);
  ~VortexVerticalExternalField() = default;

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation, const AffineTransform<T, Lattice<T>::d> ef_location);

  OPENLB_HOST_DEVICE
  T populationValueFor(size_t iPop, size_t x, size_t y, size_t z) const;

private:
  OPENLB_HOST_DEVICE
  Vec<T, Lattice<T>::d> velocityAt(const Vec<T, Lattice<T>::d>& point) const;

protected:
  AffineTransform<T, Lattice<T>::d> vel_rotation_; // transform for the current timestep.
  AffineTransform<T, Lattice<T>::d> ef_location_transform_; // transform for finding location in external field

  const T posX_;
  const T posY_;
  const T coreRadius2_; // square of core radius
  const T vorticity_;
};


/**
 * Non-trivial ExternalField
 */

template<typename T, template<typename U> class Lattice>
class PredefinedExternalField {
public:
  PredefinedExternalField(T** fieldData, Vec<T,Lattice<T>::d> externalVelocity, size_t sizeX, size_t sizeY,
          size_t sizeZ = 1, T positionScaling = 1.);
  ~PredefinedExternalField();

  OPENLB_HOST_DEVICE
  PredefinedExternalField(const PredefinedExternalField<T, Lattice>& other);

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation, const AffineTransform<T, Lattice<T>::d> ef_location);

  OPENLB_HOST_DEVICE
  T populationValueFor(size_t iPop, const long long int x, const long long int y, const long long int z = 1) const;

  void setNextExternalField(T** fieldData);

  void dispatchFieldCopy();

public:
  OPENLB_HOST_DEVICE
  Vec<T, Lattice<T>::d> velocityAt(Vec<T,Lattice<T>::d> vertex) const;

  OPENLB_HOST_DEVICE
  bool isInside(const Vec<int, Lattice<T>::d>& vertex) const;

protected:
  const size_t sizeX_;
  const size_t sizeY_;
  const size_t sizeZ_;
  const size_t gridSize_;
  const T      positionScaling_;
  const Vec<T,Lattice<T>::d> externalVelocity_;

  AffineTransform<T, Lattice<T>::d> vel_rotation_; // transform for the current timestep.
  AffineTransform<T, Lattice<T>::d> ef_location_transform_; // transform for finding location in external field

  T** fieldData_ = nullptr;
#ifdef ENABLE_CUDA
  T** gpu_fieldData_ = nullptr;
#endif
};

template<typename T, template<typename U> class Lattice>
class PredefinedExternalFluidMask {
public:
  PredefinedExternalFluidMask(bool* fieldData, const size_t sizeX, const size_t sizeY, const size_t sizeZ,bool defaults[6],T scaleFactor = 1.0);
  ~PredefinedExternalFluidMask();

  PredefinedExternalFluidMask(const PredefinedExternalFluidMask<T, Lattice>& other) = default;

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> ef_location);

  OPENLB_HOST_DEVICE
  bool fluidmaskAt(long long int x, long long int y, long long int z) const;
private:

  OPENLB_HOST_DEVICE
  bool isInside(const Vec<int, Lattice<T>::d>& vertex) const;

protected:
  const size_t sizeX_;
  const size_t sizeY_;
  const size_t sizeZ_;
  const size_t gridSize_;
  const T      scaleFactor_=1.0;
  bool         defaults_[6];

  AffineTransform<T, Lattice<T>::d> ef_location_transform_; // transform for finding location in external field

  bool* fieldData_ = nullptr;
#ifdef ENABLE_CUDA
  bool* gpu_fieldData_ = nullptr;
#endif
};

template<typename T, template<typename> class Lattice>
class ZGreaterFalseMask {

  public:
  explicit ZGreaterFalseMask(long long int z):z_(z) {}

  ZGreaterFalseMask() = delete;

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> ef_location)
  {
    ef_location_transform_ = ef_location;
  }

  OPENLB_HOST_DEVICE
  bool fluidmaskAt(long long int x, long long int y, long long int z) const
  {
    Vec<T, Lattice<T>::d> vertex(x, y, z);

    transformInPlace(vertex, ef_location_transform_);

    auto roundedVertex = vecToInt(vertex);
    
    if (roundedVertex(2)>z_)
      return false;
    else 
      return true;
  }

  private:
    long long int z_=0;
    AffineTransform<T, Lattice<T>::d> ef_location_transform_; // transform for finding location in external field
};


template<typename T, template<typename U> class Lattice>
class PredefinedTimeVariantExternalField {
  public:

public:
  const size_t sizeX_;
  const size_t sizeY_;
  const size_t sizeZ_;
  const size_t gridSize_;
  const T      positionScaling_;
  const Vec<T,Lattice<T>::d> outsideVelocity_;
  const size_t stepWidth_ = 0;
  size_t timeStep_ = 0;
  size_t initialDelaySteps_ = 0;

  AffineTransform<T, Lattice<T>::d> vel_rotation_; // transform for the current timestep.
  AffineTransform<T, Lattice<T>::d> ef_location_transform_; // transform for finding location in external field

  T** fieldDataCurrent_ = nullptr;
  unsigned int CPUIncrement_ = 0;


#ifdef ENABLE_CUDA
  T** gpu_fieldDataCurrent_ = nullptr;
  unsigned int GPUIncrement_ = 0;
  unsigned int CPUReadIncrement_;
  cudaStream_t copyStream_;
#ifndef __CUDA_ARCH__
  std::vector<T**> gpu_fieldData_;
#endif
#endif
#ifndef __CUDA_ARCH__
  std::vector<T**> fieldData_;
#endif


public:
  PredefinedTimeVariantExternalField(std::vector<T**> fieldData, const Vec<T,Lattice<T>::d> outsideVelocity,
          const size_t stepWidth, const size_t initialDelaySteps, const size_t sizeX, const size_t sizeY, const size_t sizeZ = 1,
          const float gpuMemorySize = 5, const T positionScaling = 1);

  OPENLB_HOST_DEVICE
  PredefinedTimeVariantExternalField(const PredefinedTimeVariantExternalField<T, Lattice>& other);

  void preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation, const AffineTransform<T, Lattice<T>::d> ef_location);
  void resetTimer();

  OPENLB_HOST_DEVICE
  T populationValueFor(size_t iPop, const long long int x, const long long int y, const long long int z = 1) const;

  constexpr bool indexShift(unsigned int const increment, unsigned int currentIndex, unsigned int const stepSize, unsigned int const size) const
  {
      return (increment/stepSize)%size != currentIndex;
  }

  constexpr unsigned int getIncrease(unsigned int const increment, unsigned int const size) const
  {
      return (increment+1)%size;
  }


protected:
  OPENLB_HOST_DEVICE
  Vec<T, Lattice<T>::d> velocityAt(const long long int x, const long long int y,
          const long long int z) const;
private:

  OPENLB_HOST_DEVICE
  bool isInside(const Vec<int, Lattice<T>::d>& vertex) const;
};



struct WithFluidMaskTag {};


template<typename T, template<typename U> class Lattice>
class PredefinedTimeVariantExternalFieldWithFluidMask:public PredefinedTimeVariantExternalField<T,Lattice>,WithFluidMaskTag {

  public:
    bool * fluidMask_ = nullptr;

#ifdef ENABLE_CUDA
    bool* gpu_fluidMask_ = nullptr;
#endif
  

  public:
    PredefinedTimeVariantExternalFieldWithFluidMask(std::vector<T**> fieldData,bool* fluidMask, const Vec<T,Lattice<T>::d> outsideVelocity,
          const size_t stepWidth, const size_t sizeX, const size_t sizeY, const size_t sizeZ = 1,
          const float gpuMemorySize = 5);

  OPENLB_HOST_DEVICE
  PredefinedTimeVariantExternalFieldWithFluidMask(const PredefinedTimeVariantExternalFieldWithFluidMask<T, Lattice>& other);

  OPENLB_HOST_DEVICE
  bool fluidMaskValueFor (size_t x,size_t y, size_t z = 1) const;

};




} // end of namespace

#endif //OPENLB_EXTERNALFIELDPROVIDER_H

