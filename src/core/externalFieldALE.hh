//
// Created by andi on 30/01/19.
//

#ifndef OPENLB_EXTERNALFIELDPROVIDER_HH
#define OPENLB_EXTERNALFIELDPROVIDER_HH

#include "externalFieldALE.h"
#include "core/config.h"
#include "dynamics/lbHelpers.h"
#include "ALEutil.h"
#include <cmath>

namespace olb {

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T fi(const Vec<T, Lattice<T>::d>& vel, const size_t iPop) {
  T rho = 1.;
  T uSqr = 0;
  T u[Lattice<T>::d];
  for (int d = 0; d < Lattice<T>::d; ++d) {
    uSqr += vel(d) * vel(d);
    u[d] = vel(d);
  }
  return lbHelpers<T, Lattice>::equilibrium(iPop, rho, u, uSqr);
}
/**
 * ConstExternalField
 */

template<typename T, template<typename U> class Lattice>
ConstExternalField<T, Lattice>::ConstExternalField(const Vec<T, Lattice<T>::d> velocity) :
      base_velocity_(velocity)
      {}

template<typename T, template<typename U> class Lattice>
void ConstExternalField<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
    const AffineTransform<T, Lattice<T>::d> ef_location) {

  auto rotated_velocity = transform(base_velocity_, vel_rotation);
  for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
    current_populations_(iPop) = fi<T,Lattice>(rotated_velocity, iPop);
  }
}

template<typename T, template<typename U> class Lattice>
T ConstExternalField<T, Lattice>::populationValueFor(const size_t iPop, const long long int x,
                                                 const long long int y, const long long int z) const {
  return current_populations_(iPop);
}

template<typename T, template<typename U> class Lattice>
Vec<T,Lattice<T>::d> ConstExternalField<T, Lattice>::getBaseVelocity()
{
	return base_velocity_;
}


/**
 * ZeroExternalField
 */
template<typename T, template<typename U> class Lattice>
void ZeroExternalField<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
    const AffineTransform<T, Lattice<T>::d> ef_location) {
  return;
}

template<typename T, template<typename U> class Lattice>
T ZeroExternalField<T, Lattice>::populationValueFor(const size_t iPop, const size_t x,
                                                 const size_t y, const size_t z) const {
  return 0;
}

/**
 * Vortex Fields
 */

template<typename T, template<typename U> class Lattice>
VortexHorizontalExternalField<T, Lattice>::VortexHorizontalExternalField(
    const T posX,
    const T posZ,
    const T coreRadius,
    const T vorticity) :
      posX_(posX),
      posZ_(posZ),
      coreRadius2_(coreRadius*coreRadius),
      vorticity_(vorticity) { static_assert(Lattice<T>::d == 3, "vortex field is only 3D."); }

template<typename T, template<typename U> class Lattice>
void VortexHorizontalExternalField<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
    const AffineTransform<T, Lattice<T>::d> ef_location) {
  vel_rotation_ = vel_rotation;
  ef_location_transform_ = ef_location;
}

template<typename T, template<typename U> class Lattice>
T VortexHorizontalExternalField<T, Lattice>::populationValueFor(const size_t iPop, const size_t x,
                                                            const size_t y, const size_t z) const {
  auto vel = velocityAt(Vec<T, Lattice<T>::d>(x, y, z));
  return fi<T,Lattice>(vel, iPop);
}

template<typename T, template<typename U> class Lattice>
Vec<T, Lattice<T>::d> VortexHorizontalExternalField<T, Lattice>::velocityAt(const Vec<T, Lattice<T>::d>& point) const {
  Vec<T, Lattice<T>::d> velocity;
  Vec<T, Lattice<T>::d> point2 = point;
  transformInPlace(point2, ef_location_transform_);
  const T xRel = point2(0) - posX_;
  const T zRel = point2(2) - posZ_;

  const T radius = sqrt(xRel*xRel+zRel*zRel);
  if (radius == 0)
    return velocity;

  const T velTang = 2 * (vorticity_ / (2 * M_PI)) * radius / (radius * radius + coreRadius2_);

  velocity(0) = -zRel / radius * velTang;
  velocity(1) = 0;
  velocity(2) = xRel / radius * velTang;

  return transform(velocity, vel_rotation_); // rotate velocity to current orientation
}



template<typename T, template<typename U> class Lattice>
VortexVerticalExternalField<T, Lattice>::VortexVerticalExternalField(
    const T posX,
    const T posY,
    const T coreRadius,
    const T vorticity) :
      posX_(posX),
      posY_(posY),
      coreRadius2_(coreRadius*coreRadius),
      vorticity_(vorticity) { static_assert(Lattice<T>::d == 3, "vortex field is only 3D."); }

template<typename T, template<typename U> class Lattice>
void VortexVerticalExternalField<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
    const AffineTransform<T, Lattice<T>::d> ef_location) {
  vel_rotation_ = vel_rotation;
  ef_location_transform_ = ef_location;
}

template<typename T, template<typename U> class Lattice>
T VortexVerticalExternalField<T, Lattice>::populationValueFor(const size_t iPop, const size_t x,
                                                          const size_t y, const size_t z) const {
  auto vel = velocityAt(Vec<T, Lattice<T>::d>(x, y, z));
  return fi<T,Lattice>(vel, iPop);
}

template<typename T, template<typename U> class Lattice>
Vec<T, Lattice<T>::d> VortexVerticalExternalField<T, Lattice>::velocityAt(const Vec<T, Lattice<T>::d>& point) const {
  Vec<T, Lattice<T>::d> velocity;
  Vec<T, Lattice<T>::d> point2 = point;
  transformInPlace(point2, ef_location_transform_);
  const T xRel = point2(0) - posX_;
  const T yRel = point2(1) - posY_;

  // std::cout << point2(0) << "," << point2(1) << std::endl;

  const T radius = sqrt(xRel*xRel+yRel*yRel);
  if (radius == 0)
    return velocity;

  const T velTang = 2 * (vorticity_ / (2 * M_PI)) * radius / (radius * radius + coreRadius2_);

  velocity(0) = -yRel / radius * velTang;
  velocity(1) = xRel / radius * velTang;
  velocity(2) = 0;

  return transform(velocity, vel_rotation_); // rotate velocity to current orientation
}


/**
 * Non-trivial ExternalField
 */

template<typename T, template<typename U> class Lattice>
PredefinedExternalField<T, Lattice>::PredefinedExternalField(T** fieldData,
														 Vec<T,Lattice<T>::d> externalVelocity,
                                                         const size_t sizeX,
                                                         const size_t sizeY,
                                                         const size_t sizeZ,
                                                         const T positionScaling) :
      fieldData_(fieldData),
      externalVelocity_(externalVelocity),
      sizeX_(sizeX),
      sizeY_(sizeY),
      sizeZ_(sizeZ),
      positionScaling_(positionScaling),
      gridSize_(sizeX_*sizeY_*sizeZ_)
{
    static_assert(Lattice<T>::d == 3, "PredefinedExternalField only works for 3D.");
#ifdef ENABLE_CUDA
        cudaError_t error = cudaMallocManaged(&gpu_fieldData_, Lattice<T>::d * sizeof(T *));
        HANDLE_ERROR(error);
        for (int d = 0; d < Lattice<T>::d; ++d) {
          error = cudaMalloc(&gpu_fieldData_[d], gridSize_ * sizeof(T));
          HANDLE_ERROR(error);
          error = cudaMemcpy(gpu_fieldData_[d], fieldData_[d], gridSize_ * sizeof(T),
                                         cudaMemcpyHostToDevice);
          HANDLE_ERROR(error);
        }
#endif
      }

template<typename T, template<typename U> class Lattice>
PredefinedExternalField<T, Lattice>::PredefinedExternalField(const PredefinedExternalField<T, Lattice>& other) :
    fieldData_(other.fieldData_),
    externalVelocity_(other.externalVelocity_),
    sizeX_(other.sizeX_),
    sizeY_(other.sizeY_),
    sizeZ_(other.sizeZ_),
    gridSize_(other.gridSize_),
    positionScaling_(other.positionScaling_),
    vel_rotation_(other.vel_rotation_),
    ef_location_transform_(other.ef_location_transform_)
#ifdef ENABLE_CUDA
    ,gpu_fieldData_(other.gpu_fieldData_)
#endif 
    {}

template<typename T, template<typename U> class Lattice>
PredefinedExternalField<T, Lattice>::~PredefinedExternalField()
{
#ifdef ENABLE_CUDA
#ifdef __CUDACC__
#else
	for(int d = 0; d < Lattice<T>::d; ++d)
		cudaFree(gpu_fieldData_[d]);
	cudaFree(gpu_fieldData_);
#endif
#endif
}

template<typename T, template<typename U> class Lattice>
void PredefinedExternalField<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
    const AffineTransform<T, Lattice<T>::d> ef_location) {
  vel_rotation_ = vel_rotation;
  ef_location_transform_ = ef_location;
}

template<typename T, template<typename U> class Lattice>
T PredefinedExternalField<T, Lattice>::populationValueFor(const size_t iPop, const long long int x,
                                                      const long long int y, const long long int z) const {
  auto vel = velocityAt(Vec<T,Lattice<T>::d>(x, y, z));

  return fi<T,Lattice>(vel, iPop);
}


template<typename T, template<typename U> class Lattice>
void PredefinedExternalField<T, Lattice>::setNextExternalField(T ** fieldData)
{
  fieldData_ = fieldData;
}


template<typename T, template<typename U> class Lattice>
void PredefinedExternalField<T, Lattice>::dispatchFieldCopy()
{
#ifdef ENABLE_CUDA
        cudaError_t error = cudaMallocManaged(&gpu_fieldData_, Lattice<T>::d * sizeof(T *));
        HANDLE_ERROR(error);
        for (int d = 0; d < Lattice<T>::d; ++d) {
          error = cudaMalloc(&gpu_fieldData_[d], gridSize_ * sizeof(T));
          HANDLE_ERROR(error);
          error = cudaMemcpy(gpu_fieldData_[d], fieldData_[d], gridSize_ * sizeof(T),
                                         cudaMemcpyHostToDevice);
          HANDLE_ERROR(error);
        }
#else
        // static_assert(false,"PedefinedExternalField only works with cuda");
#endif
  
}

template<typename T, template<typename U> class Lattice>
Vec<T, Lattice<T>::d> PredefinedExternalField<T, Lattice>::velocityAt(Vec<T,Lattice<T>::d> vertex) const {
  Vec<T, Lattice<T>::d> velocity;

  transformInPlace(vertex, ef_location_transform_);

  vertex *= positionScaling_;

   Vec<int,Lattice<T>::d> lowerBounds;
   Vec<int,Lattice<T>::d> upperBounds;

   const int dim = Lattice<T>::d;

   Matrix<T,Lattice<T>::d,8> velocities;

  for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
  {
	  lowerBounds(iDim) = static_cast<int>(floor(vertex(iDim)));
	  upperBounds(iDim) = lowerBounds(iDim) + 1;
  }
  
  T a = vertex(0) - lowerBounds(0);
  T b = vertex(1) - lowerBounds(1);
  T c = vertex(2) - lowerBounds(2);
  T a_1 = 1 - a;
  T b_1 = 1 - b;
  T c_1 = 1 - c;


  Vec<int, Lattice<T>::d> vertexRead;

  for (unsigned iPerm = 0; iPerm < pow(2, dim); ++iPerm) {
	for (unsigned iDim = 0; iDim < Lattice<T>::d; ++iDim) {
	  if (iPerm & (1 << iDim)) {
		  vertexRead(iDim) = upperBounds(iDim);
	  }
	  else {
		  vertexRead(iDim) = lowerBounds(iDim);
	  }
	}

	  bool inside = isInside(vertexRead);
	  if (inside) {
		size_t index = util::getCellIndex3D(vertexRead(0), vertexRead(1), vertexRead(2), sizeY_, sizeZ_);
	#ifdef __CUDACC__
		velocities(0,iPerm) = gpu_fieldData_[0][index];
		velocities(1,iPerm) = gpu_fieldData_[1][index];
		velocities(2,iPerm) = gpu_fieldData_[2][index];
	#else
		velocities(0,iPerm) = fieldData_[0][index];
		velocities(1,iPerm) = fieldData_[1][index];
		velocities(2,iPerm) = fieldData_[2][index];
	#endif
	  }
	  else {
		  velocities(0,iPerm) = externalVelocity_(0);
		  velocities(1,iPerm) = externalVelocity_(1);
		  velocities(2,iPerm) = externalVelocity_(2);
	  }


  }
  for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
	if(Lattice<T>::d == 3)
		aleutil::trilinearInterpolate(&velocity(iDim),0,0
			,velocities(iDim,0),velocities(iDim,1),velocities(iDim,2),
			velocities(iDim,3),velocities(iDim,4),velocities(iDim,5),
			velocities(iDim,6),velocities(iDim,7),
			a,b,c,a_1,b_1,c_1);

  return transform(velocity, vel_rotation_); // rotate velocity to current orientation
}

template<typename T, template<typename U> class Lattice>
bool PredefinedExternalField<T, Lattice>::isInside(const Vec<int, Lattice<T>::d>& vertex) const {
  if (Lattice<T>::d == 2) {
    auto x = vertex(0) >= 0 && vertex(0) < sizeX_;
    auto y = vertex(1) >= 0 && vertex(1) < sizeY_;
    return x && y;
  }
  if (Lattice<T>::d == 3) {
    auto x = vertex(0) >= 0 && vertex(0) < sizeX_;
    auto y = vertex(1) >= 0 && vertex(1) < sizeY_;
    auto z = vertex(2) >= 0 && vertex(2) < sizeZ_;
    return x && y && z;
  }
}

template<typename T, template<typename U> class Lattice>
PredefinedExternalFluidMask<T, Lattice>::PredefinedExternalFluidMask(bool* fieldData,
                                                         const size_t sizeX,
                                                         const size_t sizeY,
                                                         const size_t sizeZ,
                                                         bool defaults[],
                                                         T scaleFactor
                                                         
                                                         ) :
      fieldData_(fieldData),
      sizeX_(sizeX),
      sizeY_(sizeY),
      sizeZ_(sizeZ),
      scaleFactor_(scaleFactor),
      gridSize_(sizeX_*sizeY_*sizeZ_)
    {
        for (unsigned i=0;i<6;++i)
        {
          defaults_[i] = defaults[i];
        }
    static_assert(Lattice<T>::d == 3, "PredefinedExternalFluidMask only works for 3D.");
#ifdef ENABLE_CUDA
    	cudaError_t error = cudaMalloc(&gpu_fieldData_, sizeof(bool) * gridSize_);
    	HANDLE_ERROR(error);
    	error = cudaMemcpy(gpu_fieldData_, fieldData_, gridSize_ * sizeof(bool),
    	                                         cudaMemcpyHostToDevice);
	    HANDLE_ERROR(error);
#endif
      }

// template<typename T, template<typename U> class Lattice>
// PredefinedExternalFluidMask<T, Lattice>::PredefinedExternalFluidMask(const PredefinedExternalFluidMask<T, Lattice>& other) :
    // fieldData_(other.fieldData_),
    // sizeX_(other.sizeX_),
    // sizeY_(other.sizeY_),
    // sizeZ_(other.sizeZ_),
    // gridSize_(other.gridSize_),
    // defaults_(other.defaults_),
    // ef_location_transform_(other.ef_location_transform_)
    // {
// #ifdef ENABLE_CUDA
      // bool* tmp = other.gpu_fieldData_;
      // other.gpu_fieldData_=gpu_fieldData_;
      // gpu_fieldData_ = tmp;
// #endif
    // }


template<typename T, template<typename U> class Lattice>
PredefinedExternalFluidMask<T, Lattice>::~PredefinedExternalFluidMask()
{
#ifdef ENABLE_CUDA
#ifdef __CUDACC__
#else
	cudaFree(gpu_fieldData_);
#endif
#endif
}

template<typename T, template<typename U> class Lattice>
void PredefinedExternalFluidMask<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> ef_location)
{
  ef_location_transform_ = ef_location;
}

template<typename T, template<typename U> class Lattice>
bool PredefinedExternalFluidMask<T, Lattice>::fluidmaskAt(const long long int x, const long long int y,
                                                        const long long int z) const {
  Vec<T, Lattice<T>::d> vertex(x, y, z);

  transformInPlace(vertex, ef_location_transform_);
  vertex *= scaleFactor_;

  auto roundedVertex = vecToInt(vertex);
  bool inside = isInside(roundedVertex);
  if (inside)
  {
    size_t index = util::getCellIndex3D(roundedVertex(0), roundedVertex(1), roundedVertex(2), sizeY_, sizeZ_);
#ifdef __CUDACC__
    return gpu_fieldData_[index];
#else
    return fieldData_[index];
#endif
  }
  else
  {
    if (roundedVertex(0)<0)
      return defaults_[0];
    else if (roundedVertex(0)>=sizeX_)
      return defaults_[1];

    else if (roundedVertex(1)<0)
      return defaults_[2];
    else if (roundedVertex(1)>=sizeY_)
      return defaults_[3];

    else if (roundedVertex(2)<0)
      return defaults_[4];
    else if (roundedVertex(2)>=sizeZ_)
      return defaults_[5];

    else
	  return true;
  }
}

template<typename T, template<typename U> class Lattice>
bool PredefinedExternalFluidMask<T, Lattice>::isInside(const Vec<int, Lattice<T>::d>& vertex) const {
  if (Lattice<T>::d == 2) {
    auto x = vertex(0) >= 0 && vertex(0) < sizeX_;
    auto y = vertex(1) >= 0 && vertex(1) < sizeY_;
    return x && y;
  }
  if (Lattice<T>::d == 3) {
    auto x = vertex(0) >= 0 && vertex(0) < sizeX_;
    auto y = vertex(1) >= 0 && vertex(1) < sizeY_;
    auto z = vertex(2) >= 0 && vertex(2) < sizeZ_;
    return x && y && z;
  }
}

template<typename T, template<typename U> class Lattice>
PredefinedTimeVariantExternalField<T, Lattice>::PredefinedTimeVariantExternalField(std::vector<T**> fieldData,
                                                         const Vec<T,Lattice<T>::d> outsideVelocity,
                                                         const size_t stepWidth,
                                                         const size_t initialDelaySteps,
                                                         const size_t sizeX,
                                                         const size_t sizeY,
                                                         const size_t sizeZ,
                                                         const float gpuMemorySize,
                                                         const T positionScaling) :
#ifndef __CUDA_ARCH__
      fieldData_(fieldData),
#endif
      fieldDataCurrent_(fieldData[1]),
      sizeX_(sizeX),
      sizeY_(sizeY),
      sizeZ_(sizeZ),
      gridSize_(sizeX_*sizeY_*sizeZ_),
      positionScaling_(positionScaling),
      outsideVelocity_(outsideVelocity),
      stepWidth_(stepWidth),
      initialDelaySteps_(initialDelaySteps)
      {
    static_assert(Lattice<T>::d == 3, "PredefinedExternalField only works for 3D.");
#ifdef ENABLE_CUDA
#ifndef __CUDA_ARCH__
    unsigned int const numFields = std::min(static_cast<typename std::vector<T**>::size_type>((gpuMemorySize*1024*1024*1024)/(gridSize_*sizeof(T)*3))
            ,fieldData.size());
    gpu_fieldData_.resize(numFields);
    CPUReadIncrement_ = gpu_fieldData_.size()%fieldData.size();

    for(unsigned int iGPUData = 0; iGPUData < gpu_fieldData_.size(); ++iGPUData)
    {
        cudaError_t error = cudaMallocManaged(&gpu_fieldData_[iGPUData], Lattice<T>::d * sizeof(T *));
        HANDLE_ERROR(error);
        for (int d = 0; d < Lattice<T>::d; ++d) {
          error = cudaMalloc(&gpu_fieldData_[iGPUData][d], gridSize_ * sizeof(T));
          HANDLE_ERROR(error);
          error = cudaMemcpy(gpu_fieldData_[iGPUData][d], fieldData[iGPUData][d], gridSize_ * sizeof(T),
                                         cudaMemcpyHostToDevice);
          HANDLE_ERROR(error);
        }
    }

        gpu_fieldDataCurrent_ = gpu_fieldData_[0];

        HANDLE_ERROR(cudaStreamCreateWithFlags(&copyStream_,cudaStreamNonBlocking));

#endif
#endif
      }

template<typename T, template<typename U> class Lattice>
PredefinedTimeVariantExternalField<T, Lattice>::PredefinedTimeVariantExternalField(const PredefinedTimeVariantExternalField<T, Lattice>& other) :
    fieldDataCurrent_(other.fieldDataCurrent_),
    sizeX_(other.sizeX_),
    sizeY_(other.sizeY_),
    sizeZ_(other.sizeZ_),
    gridSize_(other.gridSize_),
    positionScaling_(other.positionScaling_),
    outsideVelocity_(other.outsideVelocity_),
    stepWidth_(other.stepWidth_),
    timeStep_(other.timeStep_),
    initialDelaySteps_(other.initialDelaySteps_),
    vel_rotation_(other.vel_rotation_),
    ef_location_transform_(other.ef_location_transform_),
    CPUIncrement_(other.CPUIncrement_)
#ifdef ENABLE_CUDA
    ,gpu_fieldDataCurrent_(other.gpu_fieldDataCurrent_)
    ,GPUIncrement_(other.GPUIncrement_)
    ,CPUReadIncrement_(other.CPUReadIncrement_)
#ifndef __CUDA_ARCH__
    ,gpu_fieldData_(other.gpu_fieldData_)
    ,copyStream_(other.copyStream_)
#endif
#endif
#ifndef __CUDA_ARCH__
    ,fieldData_(other.fieldData_)
#endif
    {
    }

template<typename T, template<typename U> class Lattice>
void PredefinedTimeVariantExternalField<T, Lattice>::preTimeStepCalculations(const AffineTransform<T, Lattice<T>::d> vel_rotation,
    const AffineTransform<T, Lattice<T>::d> ef_location) {

#ifndef __CUDA_ARCH__

	size_t timeStep = timeStep_;
	if(timeStep < initialDelaySteps_)
		timeStep = 0;
	else
		timeStep -= initialDelaySteps_;

    if(indexShift(timeStep,CPUIncrement_,stepWidth_,fieldData_.size()))
    {
#ifdef ENABLE_CUDA
        unsigned int GPUWriteIncrement = GPUIncrement_;
        cudaMemcpyAsync(gpu_fieldData_[GPUWriteIncrement][0],fieldData_[CPUReadIncrement_][0],gridSize_*sizeof(T)
            ,cudaMemcpyHostToDevice,copyStream_);
        cudaMemcpyAsync(gpu_fieldData_[GPUWriteIncrement][1],fieldData_[CPUReadIncrement_][1],gridSize_*sizeof(T)
            ,cudaMemcpyHostToDevice,copyStream_);
        cudaMemcpyAsync(gpu_fieldData_[GPUWriteIncrement][2],fieldData_[CPUReadIncrement_][2],gridSize_*sizeof(T)
            ,cudaMemcpyHostToDevice,copyStream_);
        std::cout << "Wrote GPU data" << std::endl;

        CPUReadIncrement_ = getIncrease(CPUReadIncrement_,fieldData_.size());
        GPUIncrement_ = getIncrease(GPUIncrement_,gpu_fieldData_.size());
        gpu_fieldDataCurrent_ = gpu_fieldData_[GPUIncrement_];
#endif
        CPUIncrement_ = getIncrease(CPUIncrement_,fieldData_.size());
        fieldDataCurrent_ = fieldData_[CPUIncrement_];
    }
    ++timeStep_;
    vel_rotation_ = vel_rotation;
    ef_location_transform_ = ef_location;
#endif
}

template<typename T, template<typename U> class Lattice>
void PredefinedTimeVariantExternalField<T, Lattice>::resetTimer()
{
  timeStep_=0;
}

template<typename T, template<typename U> class Lattice>
T PredefinedTimeVariantExternalField<T, Lattice>::populationValueFor(const size_t iPop, const long long int x,
                                                      const long long int y, const long long int z) const {
   auto vel = velocityAt(x, y, z);

  return fi<T,Lattice>(vel, iPop);
}

template<typename T, template<typename U> class Lattice>
Vec<T, Lattice<T>::d> PredefinedTimeVariantExternalField<T, Lattice>::velocityAt(const long long int x, const long long int y,
                                                        const long long int z) const {
  Vec<T, Lattice<T>::d> velocity;
  Vec<T, Lattice<T>::d> vertex(x, y, z);

  transformInPlace(vertex, ef_location_transform_);

  vertex *= positionScaling_;

  Vec<int,Lattice<T>::d> lowerBounds;
  Vec<int,Lattice<T>::d> upperBounds;

  const int dim = Lattice<T>::d;

  Matrix<T,Lattice<T>::d,8> velocities;

  for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
  {
      lowerBounds(iDim) = static_cast<int>(floor(vertex(iDim)));
      upperBounds(iDim) = lowerBounds(iDim) + 1;
  }

  T a = vertex(0) - lowerBounds(0);
  T b = vertex(1) - lowerBounds(1);
  T c = vertex(2) - lowerBounds(2);
  T a_1 = 1 - a;
  T b_1 = 1 - b;
  T c_1 = 1 - c;

  Vec<int, Lattice<T>::d> vertexRead;

  for (unsigned iPerm = 0; iPerm < pow(2, dim); ++iPerm) {

    for (unsigned iDim = 0; iDim < Lattice<T>::d; ++iDim)
      if (iPerm & (1 << iDim))
          vertexRead(iDim) = upperBounds(iDim);
      else
          vertexRead(iDim) = lowerBounds(iDim);

    bool inside = isInside(vertexRead);
    if (inside) {
      size_t index = util::getCellIndex3D(vertexRead(0), vertexRead(1), vertexRead(2), sizeY_, sizeZ_);
#ifdef __CUDACC__
      velocities(0,iPerm) = gpu_fieldDataCurrent_[0][index];
      velocities(1,iPerm) = gpu_fieldDataCurrent_[1][index];
      velocities(2,iPerm) = gpu_fieldDataCurrent_[2][index];
#else
      velocities(0,iPerm) = fieldDataCurrent_[0][index];
      velocities(1,iPerm) = fieldDataCurrent_[1][index];
      velocities(2,iPerm) = fieldDataCurrent_[2][index];
#endif
    }
	else {
		velocities(0,iPerm) = outsideVelocity_(0);
		velocities(1,iPerm) = outsideVelocity_(1);
		velocities(2,iPerm) = outsideVelocity_(2);
	}
  }
    for(unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim)
      if(Lattice<T>::d == 3)
          aleutil::trilinearInterpolate(&velocity(iDim),0,0
              ,velocities(iDim,0),velocities(iDim,1),velocities(iDim,2),
              velocities(iDim,3),velocities(iDim,4),velocities(iDim,5),
              velocities(iDim,6),velocities(iDim,7),
              a,b,c,a_1,b_1,c_1);

    return transform(velocity, vel_rotation_); // rotate velocity to current orientation
}

template<typename T, template<typename U> class Lattice>
bool PredefinedTimeVariantExternalField<T, Lattice>::isInside(const Vec<int, Lattice<T>::d>& vertex) const {
  if (Lattice<T>::d == 2) {
    auto x = vertex(0) >= 0 && vertex(0) < sizeX_;
    auto y = vertex(1) >= 0 && vertex(1) < sizeY_;
    return x && y;
  }
  if (Lattice<T>::d == 3) {
    auto x = vertex(0) >= 0 && vertex(0) < sizeX_;
    auto y = vertex(1) >= 0 && vertex(1) < sizeY_;
    auto z = vertex(2) >= 0 && vertex(2) < sizeZ_;
    return x && y && z;
  }
}

// ###########################################
//
//
//

template<typename T, template<typename U> class Lattice>
PredefinedTimeVariantExternalFieldWithFluidMask<T, Lattice>::PredefinedTimeVariantExternalFieldWithFluidMask(std::vector<T**> fieldData,bool* fluidMask,
                                                         const Vec<T,Lattice<T>::d> outsideVelocity,
                                                         const size_t stepWidth,
                                                         const size_t sizeX,
                                                         const size_t sizeY,
                                                         const size_t sizeZ,
                                                         const float gpuMemorySize) :
  PredefinedTimeVariantExternalField<T,Lattice>(fieldData,outsideVelocity,stepWidth,sizeX,sizeY,sizeZ,gpuMemorySize)
  ,fluidMask_(fluidMask)
{

#ifdef ENABLE_CUDA
        cudaError_t error = cudaMallocManaged(&gpu_fluidMask_, sizeof(bool)*(this->gridSize_));
        HANDLE_ERROR(error);
        error = cudaMemcpy(gpu_fluidMask_,fluidMask_,sizeof(bool)*(this->gridSize_),cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);
#endif
}

template<typename T, template<typename U> class Lattice>
PredefinedTimeVariantExternalFieldWithFluidMask<T, Lattice>::PredefinedTimeVariantExternalFieldWithFluidMask(const PredefinedTimeVariantExternalFieldWithFluidMask<T,Lattice>& other) :
  PredefinedTimeVariantExternalField<T,Lattice>(other),
  fluidMask_(other.fluidMask_)
#ifdef ENABLE_CUDA
  ,
  gpu_fluidMask_(other.gpu_fluidMask_)
#endif
{}


template<typename T, template<typename U> class Lattice>
bool PredefinedTimeVariantExternalFieldWithFluidMask<T,Lattice>::fluidMaskValueFor(size_t x,size_t y, size_t z) const
{

  Vec<T, Lattice<T>::d> vertex(x, y, z);

  this->transformInPlace(vertex, this->ef_location_transform_);
  auto roundedVertex = vecToInt(vertex);
  bool inside = this->isInside(roundedVertex);
  if (inside)
  {
#ifdef __CUDA_ARCH 
    return gpu_fluidMask_[util::getCellIndex3D(roundedVertex(0), roundedVertex(1), roundedVertex(2), this->sizeY_, this->sizeZ_)];
#else
    return fluidMask_[util::getCellIndex3D(roundedVertex(0), roundedVertex(1), roundedVertex(2), this->sizeY_, this->sizeZ_)];
#endif
  }
  else 
  return true;
}


} // end of namespace


#endif //OPENLB_EXTERNALFIELDPROVIDER_HH
