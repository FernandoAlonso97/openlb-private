/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Markus Mohrhard
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef GPUHANDLER_H
#define GPUHANDLER_H

// #include <map>
#include <dynamics/dynamics.h>
#include "cellBlockData.h"
#include "cellBlockData.hh"
#include "externalFieldALE.h"
#include "affineTransform.h"
#include "contrib/domainDecomposition/subDomainInformation.h"

namespace olb {

#ifdef ENABLE_CUDA
template <typename T, template <typename V> class Lattice>
class GPUHandler
{
public:

  GPUHandler(CellBlockData<T, Lattice>* cellData);
  ~GPUHandler();

  void registerDynamics(Dynamics<T, Lattice>* dynamics, DynamicsDataHandler<T>* dataHandler);
  void addCollisionData(Dynamics<T,Lattice>* dynamics);


  template <typename DynamicsType>
  void executeBoundaryKernel(Dynamics<T,Lattice>* dynamics, size_t position, size_t size);
  template <typename DynamicsType>
  void executeFluidKernel(size_t gridSize, Dynamics<T,Lattice>* dynamics);
  template <typename DynamicsType>
  void executePostProcessorKernel(Dynamics<T,Lattice>* dynamics, size_t position, size_t size);

  void executeTestFieldKernel(size_t gridSize, int indicator);


  template<class ExternalField>
  void executeMoveMeshKernel2D(const AffineTransform<T, Lattice<T>::d>& transform, const ExternalField externalField);

  template<class ExternalField>
  void executeMoveMeshKernel3D(const AffineTransform<T, Lattice<T>::d>& transform, const ExternalField& externalField, SubDomainInformation<T,Lattice<T>>);

  template<class ExternalField,unsigned boundaryDistance>
  void executeUpdateFluidMaskKernel3D(const AffineTransform<T, Lattice<T>::d>& transform, const ExternalField& externalField,SubDomainInformation<T,Lattice<T>> subDomInfo);

  void transferToGPU(Dynamics<T,Lattice>* dynamics, DynamicsDataHandler<T>* dataHandler);
  void transferToCPU(Dynamics<T,Lattice>* dynamics, DynamicsDataHandler<T>* dataHandler);

  static GPUHandler<T, Lattice>& get();

  void initCudaStreams();
  void initCudaEvents();

  void syncStreams();
  void holdInputStream(cudaStream_t& inputStream);
  void waitOnInputStream(cudaStream_t& inputStream, cudaEvent_t& inputEvent);

  template<template<typename V,template <typename W> class> class Functor>
  void writeBlockLatticeByFunctor(const typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells);

  template<template<typename V,template <typename W> class> class Functor>
  void readBlockLatticeByFunctor(typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT data,const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells);

  template<typename Callable, typename ... ARGS>
  void apply (Callable call,size_t loopLength, ARGS ... args);

#ifdef OLB_DEBUG
  std::map<Dynamics<T,Lattice>*, size_t*> getDynamicsCellIDsMap()
  {
  	return dynamicsCellIDsMap;
  }
#endif

  size_t* getDynamicsCellIDsGPU(Dynamics<T,Lattice>* dynamics) {
    return dynamicsCellIDsMap.find(dynamics)->second;
  }

  T** getDynamicsDataGPU(Dynamics<T,Lattice>* dynamics) {
    return dynamicsDataMap.find(dynamics)->second;
  }

  T** getPostProcDataGPU(Dynamics<T,Lattice>* dynamics) {
    return postProcDataMap.find(dynamics)->second;
  }

private:
  CellBlockData<T, Lattice>* cellData;

private:

  std::map<Dynamics<T,Lattice>*, T**> dynamicsDataMap;
  std::map<Dynamics<T,Lattice>*, T**> postProcDataMap;
  std::map<Dynamics<T,Lattice>*, T* > collisionDataMap;
  std::map<Dynamics<T,Lattice>*, size_t*> dynamicsCellIDsMap;

  size_t gridDim_;

  void deleteMemory(T** gpuMemory, unsigned int numberOfDataPointers);
  void deleteMemory(size_t* gpuMemory);
  size_t numberDynamics_;

  cudaStream_t* streams_;
  cudaEvent_t* events_;
  bool isBulkDynamics_ = true;


};
#endif

}

#include "gpuhandler.hh"

#endif
