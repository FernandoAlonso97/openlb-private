/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Markus Mohrhard
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef GPUHANDLER_HH
#define GPUHANDLER_HH

#include "gpuhandler.h"
#include "dynamics/dynamicsKernels.h"
#include "cudaErrorHandler.h"
#include "externalFieldALE.h"

#define THREADNUM 256

namespace olb {

#ifdef __CUDACC__

    template<typename T, template<typename> class Lattice>
    GPUHandler<T, Lattice>::GPUHandler(CellBlockData<T, Lattice> *cellData):
            cellData(cellData),
            gridDim_((cellData->getNx() * cellData->getNy() * cellData->getNz() ) / THREADNUM) {
    }

    template<typename T, template<typename> class Lattice>
    GPUHandler<T, Lattice>::~GPUHandler() {
        cudaDeviceSynchronize();

//  for (auto& itr : dynamicsDataMap)
//  {
//    unsigned int numberOfDataPointers = itr.first->getDataHandler()->getNumberOfDataPoints();
//    deleteMemory(itr.second, numberOfDataPointers);
//  }
//
//  for (auto& itr : dynamicsCellIDsMap)
//  {
//    deleteMemory(itr.second);
//  }

//  cudaFree(gpuCollsionData_);

        //TODO: Free gpuFluidData_. So far it results in error code 29
        // cudaErrorCudartUnloading; Apparently cuda API is unloaded
        // before cudaFree is called. Not so much for other GPU data...

    }
#endif

#ifdef __CUDACC__
    void handle_error(cudaError_t error) {
        HANDLE_ERROR(error);
    }

#endif

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::registerDynamics(Dynamics<T, Lattice> *dynamics, DynamicsDataHandler<T> *dataHandler) {
#ifdef __CUDACC__
        // allocate the GPU memory for the dynamics
        size_t
        dataSize = dataHandler->getAllocationSize();
        unsigned int numberOfDataPointers = dataHandler->getNumberOfDataPoints();
        unsigned int numberOfPostProcDataPointers = dataHandler->getNumberOfPostProcDataPoints();

        T **gpuData = nullptr;
        T **gpuPostProcData = nullptr;

        cudaError_t error;

        // This needs to be done in order to avoid allocation errors
        if (numberOfDataPointers) {
            error = cudaMallocManaged(&gpuData, numberOfDataPointers * sizeof(T *));
            HANDLE_ERROR(error);
            for (unsigned int i = 0; i < numberOfDataPointers; ++i) {
                error = cudaMalloc(&gpuData[i], sizeof(T) * dataSize);
                HANDLE_ERROR(error);

                HANDLE_ERROR(cudaMemset(gpuData[i], 0, sizeof(T) * dataSize));
            }
        }

        if (numberOfPostProcDataPointers) {
            error = cudaMallocManaged(&gpuPostProcData, numberOfPostProcDataPointers * sizeof(T *));
        HANDLE_ERROR(error);
            for (unsigned int i = 0; i < numberOfPostProcDataPointers; ++i) {
                error = cudaMalloc(&gpuPostProcData[i], sizeof(T) * dataSize);
                HANDLE_ERROR(error);

                HANDLE_ERROR(cudaMemset(gpuPostProcData[i], 0, sizeof(T) * dataSize));
            }
        }

        dynamicsDataMap.insert(std::pair<Dynamics<T, Lattice> *, T **>(dynamics, gpuData));
        postProcDataMap.insert(std::pair<Dynamics<T, Lattice> *, T **>(dynamics, gpuPostProcData));

        size_t * gpuCellIDs;

        error = cudaMalloc(&gpuCellIDs, sizeof(size_t) * dataSize);
        HANDLE_ERROR(error);

        HANDLE_ERROR(cudaMemset(gpuCellIDs, 0, sizeof(size_t) * dataSize));
        dynamicsCellIDsMap.insert(std::pair<Dynamics<T, Lattice> *, size_t *>(dynamics, gpuCellIDs));

#endif
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::addCollisionData(Dynamics<T, Lattice> *dynamics) {
#ifdef __CUDACC__

        T *gpuCollisionData;
        int collisionDataSize = dynamics->getCollisionDataSize();

        cudaError_t error = cudaMalloc(&gpuCollisionData, sizeof(T) * collisionDataSize);
        HANDLE_ERROR(error);

        collisionDataMap.insert(std::pair<Dynamics<T, Lattice> *, T *>(dynamics, gpuCollisionData));

        HANDLE_ERROR(cudaMemcpy(gpuCollisionData, dynamics->getCollisionData(), sizeof(T) * collisionDataSize,
                   cudaMemcpyHostToDevice));

#endif
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::initCudaStreams() {
        numberDynamics_ = dynamicsDataMap.size() + 1;
        streams_ = new cudaStream_t[numberDynamics_];
        for (size_t i = 0; i < numberDynamics_; ++i)
            HANDLE_ERROR(cudaStreamCreate(&streams_[i]));
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::initCudaEvents() {
        numberDynamics_ = dynamicsDataMap.size() + 1;
        events_ = new cudaEvent_t[numberDynamics_];
        for (size_t i = 0; i < numberDynamics_; ++i)
            HANDLE_ERROR(cudaEventCreate(&events_[i]));
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::syncStreams() {
        numberDynamics_ = dynamicsDataMap.size() + 1;
        for (int currentStream = 0; currentStream < numberDynamics_; currentStream++) {
            cudaEventRecord(events_[currentStream], streams_[currentStream]);
        }
        for (int currentStream = 0; currentStream < numberDynamics_; currentStream++) {
            for (int currentEvent = 0; currentEvent < numberDynamics_; currentEvent++) {
                cudaStreamWaitEvent(streams_[currentStream], events_[currentEvent]);
            }
        }
    }

#ifdef __CUDACC__
    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::holdInputStream(cudaStream_t &inputStream) {
        numberDynamics_ = dynamicsDataMap.size() + 1;
        for (int currentStream = 0; currentStream < numberDynamics_; currentStream++) {
            cudaEventRecord(events_[currentStream], streams_[currentStream]);
        }
        for (int currentEvent = 0; currentEvent < numberDynamics_; currentEvent++) {
            cudaStreamWaitEvent(inputStream, events_[currentEvent]);
        }
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::waitOnInputStream(cudaStream_t &inputStream, cudaEvent_t &inputEvent) {
        cudaEventRecord(inputEvent, inputStream);
        numberDynamics_ = dynamicsDataMap.size() + 1;
        for (int currentStream = 0; currentStream < numberDynamics_; currentStream++) {
            cudaStreamWaitEvent(streams_[currentStream], inputEvent);
        }
    }
#endif

    template<typename T, template<typename> class Lattice>
    GPUHandler<T, Lattice> &GPUHandler<T, Lattice>::get() {
        static GPUHandler<T, Lattice> instance;
        return instance;
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::transferToGPU(Dynamics<T, Lattice> *dynamics, DynamicsDataHandler<T> *dataHandler) {
#ifdef __CUDACC__
        size_t
        dataSize = dataHandler->getCellIDs().size();
        unsigned int numberOfDataPointers = dataHandler->getNumberOfDataPoints();
        unsigned int numberOfPostProcDataPointers = dataHandler->getNumberOfPostProcDataPoints();
        T **cpuData = dataHandler->getDynamicsData();
        T **cpuPostProcData = dataHandler->getPostProcData();
        T **gpuData = dynamicsDataMap.find(dynamics)->second;
        T **gpuPostProcData = postProcDataMap.find(dynamics)->second;

        if (numberOfDataPointers) {
            for (unsigned int i = 0; i < numberOfDataPointers; ++i) {
                cudaError_t error = cudaMemcpy(gpuData[i], cpuData[i], sizeof(T) * dataSize, cudaMemcpyHostToDevice);
                HANDLE_ERROR(error);
            }
        }

        if (numberOfPostProcDataPointers) {
            for (unsigned int i = 0; i < numberOfPostProcDataPointers; ++i) {
                cudaError_t error = cudaMemcpy(gpuPostProcData[i], cpuPostProcData[i], sizeof(T) * dataSize, cudaMemcpyHostToDevice);
                HANDLE_ERROR(error);
            }
        }

        size_t * gpuIDData = dynamicsCellIDsMap.find(dynamics)->second;
        size_t const *cpuIDData = dataHandler->getCellIDs().data();
        cudaError_t error = cudaMemcpy(gpuIDData, cpuIDData, sizeof(size_t) * dataSize, cudaMemcpyHostToDevice);
        HANDLE_ERROR(error);

#endif
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::transferToCPU(Dynamics<T, Lattice> *dynamics, DynamicsDataHandler<T> *dataHandler) {
#ifdef __CUDACC__
        size_t
        dataSize = dataHandler->getCellIDs().size();
        unsigned int numberOfDataPointers = dataHandler->getNumberOfDataPoints();
        unsigned int numberOfPostProcDataPointers = dataHandler->getNumberOfPostProcDataPoints();
        T **cpuData = dataHandler->getDynamicsData();
        T **cpuPostProcData = dataHandler->getPostProcData();
        T **gpuData = dynamicsDataMap.find(dynamics)->second;
        T **gpuPostProcData = postProcDataMap.find(dynamics)->second;

        if (numberOfDataPointers) {
            for (unsigned int i = 0; i < numberOfDataPointers; ++i) {
                cudaError_t error = cudaMemcpy(cpuData[i], gpuData[i], sizeof(T) * dataSize, cudaMemcpyDeviceToHost);
                HANDLE_ERROR(error);
            }
        }

        if (numberOfPostProcDataPointers) {
            for (unsigned int i = 0; i < numberOfPostProcDataPointers; ++i) {
                cudaError_t error = cudaMemcpy(cpuPostProcData[i], gpuPostProcData[i], sizeof(T) * dataSize, cudaMemcpyDeviceToHost);
                HANDLE_ERROR(error);
            }
        }
#endif
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::deleteMemory(T **gpuData, unsigned int numberOfDataPointers) {
#ifdef __CUDACC__
        for (unsigned int i = 0; i < numberOfDataPointers; ++i) {
            cudaError_t error = cudaFree(gpuData[i]);
            HANDLE_ERROR(error);
        }

        cudaError_t error = cudaFree(gpuData);
        HANDLE_ERROR(error);
#endif
    }

    template<typename T, template<typename> class Lattice>
    void GPUHandler<T, Lattice>::deleteMemory(size_t * gpuMemory) {
#ifdef __CUDACC__
        cudaError_t error = cudaFree(gpuMemory);
        HANDLE_ERROR(error);
#endif
    }

    template<typename T, template<typename> class Lattice>
    template<typename DynamicsType>
    void GPUHandler<T, Lattice>::executeBoundaryKernel(Dynamics<T, Lattice> *dynamics, size_t position, size_t size) {
#ifdef __CUDACC__
        size_t
        dataSize = size;
        T **gpuMomentaData = dynamicsDataMap.find(dynamics)->second;
        size_t * cellIDs = dynamicsCellIDsMap.find(dynamics)->second;
        T *gpuCollisionData = collisionDataMap.find(dynamics)->second;

        dim3 threadsPerBlock(THREADNUM, 1, 1);
        dim3 numBlocks(dataSize / THREADNUM + 1, 1, 1);
        boundaryKernel<T, DynamicsType> <<< numBlocks, threadsPerBlock, 0, streams_[position]>>>
            (cellData->gpuGetFluidData(), dataSize, gpuMomentaData, cellIDs, gpuCollisionData);
#endif
    }

    template<typename T, template<typename> class Lattice>
    template<typename DynamicsType>
    void GPUHandler<T, Lattice>::executeFluidKernel(size_t gridSize, Dynamics<T, Lattice> *dynamics) {
    // TODO: gpuMomentaData is not initialized
    T **gpuMomentaData = nullptr;
    T *gpuCollisionData = collisionDataMap.find(dynamics)->second;

    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(gridDim_ + 1, 1, 1);
    fluidKernel<T, DynamicsType> <<<numBlocks, threadsPerBlock, 0, streams_[0]>>>
            (cellData->gpuGetFluidData(), gridSize, gpuMomentaData, cellData->gpuGetFluidMask(), gpuCollisionData);
    }

template<typename T, template<typename> class Lattice>
template<typename DynamicsType>
void GPUHandler<T, Lattice>::executePostProcessorKernel(Dynamics <T, Lattice> *dynamics, size_t position, size_t size) {

    size_t dataSize = size;
    T **gpuMomentaData = dynamicsDataMap.find(dynamics)->second;
    T **gpuProstProcData = postProcDataMap.find(dynamics)->second;
    size_t *cellIDs = dynamicsCellIDsMap.find(dynamics)->second;
    T *gpuCollisionData = collisionDataMap.find(dynamics)->second;

    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(dataSize / THREADNUM + 1, 1, 1);
    if (Lattice<T>::d == 2) {
        postProcKernel2D<T, DynamicsType> <<<numBlocks, threadsPerBlock, 0, streams_[position]>>>
            (cellData->gpuGetFluidData(), gpuMomentaData, gpuCollisionData, gpuProstProcData, dataSize, cellIDs, cellData->getNx(), cellData->getNy());
    }
    else if (Lattice<T>::d == 3) {
        postProcKernel3D<T, DynamicsType> <<<numBlocks, threadsPerBlock, 0, streams_[position]>>>
            (cellData->gpuGetFluidData(), gpuMomentaData, gpuCollisionData, gpuProstProcData, dataSize, cellIDs, cellData->getNx(), cellData->getNy(), cellData->getNz());
    }
}

template<typename T, template<typename> class Lattice>
template<class ExternalField>
void GPUHandler<T, Lattice>::executeMoveMeshKernel2D(const AffineTransform<T, Lattice<T>::d>& transform, const ExternalField externalField) {
  dim3 threadsPerBlock(THREADNUM, 1, 1);
  dim3 numBlocks(gridDim_ + 1, 1, 1);

  auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(cellData);

  moveMeshKernel2D<T, Lattice, ExternalField> <<<numBlocks, threadsPerBlock, 0, streams_[0]>>>(
      cellDataALE->gpuGetFluidData(),
          cellDataALE->gpuGetFluidDataALE(),
          transform,
          externalField,
          cellDataALE->getGridSize(),
          cellDataALE->getNx(),
          cellDataALE->getNy()
  );
}

template<typename T, template<typename> class Lattice>
template<class ExternalField>
void GPUHandler<T, Lattice>::executeMoveMeshKernel3D(const AffineTransform<T, Lattice<T>::d>& transform, const ExternalField & externalField, SubDomainInformation<T,Lattice<T>> subDomInfo) {
    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(gridDim_ + 1, 1, 1);

    auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(cellData);

    moveMeshKernel3D<T, Lattice, ExternalField> <<<numBlocks, threadsPerBlock, 0, streams_[0]>>>(
        cellDataALE->gpuGetFluidData(),
        cellDataALE->gpuGetFluidDataALE(),
        transform,
        externalField,
        cellDataALE->getGridSize(),
        cellDataALE->getNx(),
        cellDataALE->getNy(),
        cellDataALE->getNz(),
        cellDataALE->gpuGetFluidMask(),
        cellDataALE->gpuGetStationaryFluidMask(),
        subDomInfo
  );
#ifdef OLB_DEBUG 
    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaGetLastError());
#endif

}

template<typename T, template<typename> class Lattice>
template<class ExternalField,unsigned boundaryDistance>
void GPUHandler<T, Lattice>::executeUpdateFluidMaskKernel3D(const AffineTransform<T, Lattice<T>::d>& transform, const ExternalField & externalFluidMask, SubDomainInformation<T,Lattice<T>> subDomInfo) {
    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(gridDim_ + 1, 1, 1);

    auto cellDataALE = static_cast<CellBlockDataALE<T,Lattice>*>(cellData);

    updateFluidMask3D<T, Lattice, ExternalField,boundaryDistance> <<<numBlocks, threadsPerBlock, 0, streams_[0]>>>(
            cellDataALE->gpuGetFluidMask(),
            cellDataALE->gpuGetFluidMaskALE(),
            transform,
            externalFluidMask,
            cellDataALE->getGridSize(),
            cellDataALE->getNx(),
            cellDataALE->getNy(),
            cellDataALE->getNz(),
            subDomInfo
  );

#ifdef OLB_DEBUG 
    HANDLE_ERROR(cudaGetLastError());
#endif

}

template<typename T, template<typename> class Lattice>
void GPUHandler<T, Lattice>::executeTestFieldKernel(size_t gridSize, int indicator) {

    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(gridDim_ + 1, 1, 1);
    testFieldKernel<T, Lattice> <<<numBlocks, threadsPerBlock, 0, streams_[0]>>>
            (cellData->gpuGetFluidData(), gridSize, indicator, cellData->getNx(), cellData->getNy(), cellData->getNz());

    cudaDeviceSynchronize();
}

template<typename T, template<typename> class Lattice>
template<template<typename V, template<typename W> class> class Functor>
void GPUHandler<T, Lattice>::writeBlockLatticeByFunctor(
        const typename Functor<T, Lattice>::returnType *const OPENLB_RESTRICT data,
        const size_t *const OPENLB_RESTRICT cellIndices, const size_t numberOfCells) {
    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(numberOfCells / THREADNUM + 1, 1, 1);
    writeBlockLatticeByFunctorKernel<T, Lattice, Functor> <<<numBlocks, threadsPerBlock, 0, 0>>>
            (cellData->gpuGetFluidData(), data, cellIndices, numberOfCells);
}

template<typename T, template<typename> class Lattice>
template<template<typename V, template<typename W> class> class Functor>
void
GPUHandler<T, Lattice>::readBlockLatticeByFunctor(typename Functor<T, Lattice>::returnType *const OPENLB_RESTRICT data,
                                                  const size_t *const OPENLB_RESTRICT cellIndices,
                                                  const size_t numberOfCells) {
    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(numberOfCells / THREADNUM + 1, 1, 1);
    readBlockLatticeByFunctorKernel<T, Lattice, Functor> <<<numBlocks, threadsPerBlock, 0, 0>>>
            (cellData->gpuGetFluidData(), data, cellIndices, numberOfCells);
}

template<typename T, template<typename> class Lattice>
template<typename Callable, typename ... ARGS>
void 
GPUHandler<T, Lattice>::apply(Callable call, size_t loopLength, ARGS ... args) 
{
    dim3 threadsPerBlock(THREADNUM, 1, 1);
    dim3 numBlocks(loopLength/THREADNUM + 1, 1, 1);
    applyFunctorKernel<T,Lattice> <<<numBlocks, threadsPerBlock, 0, 0>>>
            (call,cellData->gpuGetFluidData(),loopLength,args...);
}

} // end of Namespace
#endif
