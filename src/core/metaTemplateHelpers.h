/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau, Bastian Horvat, Markus Mohrhart
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Helper functions for the implementation of LB dynamics. This file is all
 * about efficiency. The generic template code is specialized for commonly
 * used Lattices, so that a maximum performance can be taken out of each
 * case.
 */
#ifndef META_TEMPLATE_HELPERS_H
#define META_TEMPLATE_HELPERS_H

#include <type_traits>

namespace olb{

template<typename T, template<typename U> class Lattice> class CellView;


// Helper to be able to compare values with std::is_same<>
template<int number>
struct ValueToType{};

// Helper functions to add index to array
template<int VALUE, int POSITION, bool CONDITION>
typename std::enable_if<CONDITION,void>::type addIndex (int * indices)
{
	indices[POSITION] = VALUE;
	return;
}
template<int VALUE, int POSITION, bool CONDITION>
typename std::enable_if<!CONDITION,void>::type addIndex (int * indices)
{
	return;
}
 ///////// CalcRhoOnWall //////////////
template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX==Lattice<T>::q,T>::type CalcRhoWall(CellView<T,Lattice> const& cell)
{
  return 0;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX=0>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX<Lattice<T>::q,T>::type CalcRhoWall(CellView<T,Lattice> const& cell)
{
  return std::is_same<ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>,ValueToType<orientation>>::value*cell[INDEX]
           +CalcRhoWall<T,Lattice,direction,orientation,INDEX+1>(cell);
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX==Lattice<T>::q,T>::type CalcRhoWall(CellDataArray<T,Lattice> const& data)
{
  return 0;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX=0>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX<Lattice<T>::q,T>::type CalcRhoWall(CellDataArray<T,Lattice> const& data)
{
  return std::is_same<ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>,ValueToType<orientation>>::value*data.data[INDEX][0]
           +CalcRhoWall<T,Lattice,direction,orientation,INDEX+1>(data);
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX==Lattice<T>::q,T>::type CalcRhoWall(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
{
  return 0;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX=0>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX<Lattice<T>::q,T>::type CalcRhoWall(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
{
  return std::is_same<ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>,ValueToType<orientation>>::value*cellData[INDEX][cellIndex]
           +CalcRhoWall<T,Lattice,direction,orientation,INDEX+1>(cellData, cellIndex);
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX==Lattice<T>::q,T>::type CalcRhoWall(const T * const OPENLB_RESTRICT cellData)
{
  return 0;
};

template<typename T, template<typename U> class Lattice, int direction, int orientation,int INDEX=0>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX<Lattice<T>::q,T>::type CalcRhoWall(const T * const OPENLB_RESTRICT cellData)
{
  return std::is_same<ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>,ValueToType<orientation>>::value*cellData[INDEX]
           +CalcRhoWall<T,Lattice,direction,orientation,INDEX+1>(cellData);
};

///////////////// getSubIndices ///////////////////
// Meta Template Recusions in order to get rid of subIndices calculations which are not available in GPU
// use:
// int size = getNumberOfSubIndices<T,Lattice,1,0>();
// int indices[size];
// getSubIndices<T,Lattice,1,0>(indices);

// recursion end 
template<typename T, template<typename U> class Lattice, int direction, int orientation, int INDEX>
typename std::enable_if<INDEX==Lattice<T>::q,int>::type getNumberOfSubIndices ()
{
  return 0;
}
// recursion start and body
template<typename T, template<typename U> class Lattice, int direction, int orientation, int INDEX=0>
typename std::enable_if<INDEX<Lattice<T>::q,int>::type getNumberOfSubIndices ()
{
  return std::is_same< ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>, ValueToType<orientation> >::value*1
         + getNumberOfSubIndices<T,Lattice,direction,orientation,INDEX+1>();
}
// recursion end
template<typename T, template<typename U> class Lattice, int direction, int orientation, int INDEX, int POSITION>
typename std::enable_if<INDEX==Lattice<T>::q,void>::type getSubIndices (int * indices)
{
	return;
}
// recursion start and body
template<typename T, template<typename U> class Lattice, int direction, int orientation, int INDEX=0, int POSITION=0>
typename std::enable_if<INDEX<Lattice<T>::q,void>::type getSubIndices (int * indices)
{
	addIndex<INDEX,POSITION,
	std::is_same< ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>, ValueToType<orientation> >::value
	> (indices);	
	
	getSubIndices<T,Lattice,direction,orientation,INDEX+1,
    POSITION+1*std::is_same< ValueToType<Lattice<T>::CPUBaseDescriptor::c[INDEX][direction]>, ValueToType<orientation> >::value
	> (indices);
	return;
}


} // end of namespace


namespace impedanceBoundaryHelpers{
//                      0     1     2    3    4     5     6     7     8     9    10    11   12   13   14   15   16    17    18
std::string f[19] = {"f19", "f2", "f4", "f6","f12","f11","f14","f13","f18","f17","f1","f3","f5","f7","f8","f9","f10","f15","f16"};
int f_i[20] = {999, 10, 1, 11, 2, 12, 3, 13, 14, 15, 16, 5, 4, 7, 6, 17, 18, 9, 8, 0};

// Helper struct to compare vectors at compile time

template<int X, int Y, int Z=0>
struct vectorCompare{
public:
  static constexpr int x = X;
  static constexpr int y = Y;
  static constexpr int z = Z;
};

// Define edge in 3D

template<int Plane, int direction, int orientation>
struct defineEdge3D{
    typedef vectorCompare<1-bool(Plane),Plane % 2,(Plane-1)*bool(Plane)> Axis;
    typedef vectorCompare<((Plane % 2) || ((Plane-1)*bool(Plane)))*direction,(1-bool(Plane))*direction,0> Normal1;
    typedef vectorCompare<0,((Plane-1)*bool(Plane))*orientation,((1-bool(Plane)) || (Plane % 2))*orientation> Normal2;

    OPENLB_HOST_DEVICE
    static void print()
    {
        std::cout << "Axis: " << Axis::x << ", " << Axis::y << ", " << Axis::z << std::endl;
        std::cout << "Normal1: " << Normal1::x << ", " << Normal1::y << ", " << Normal1::z << std::endl;
        std::cout << "Normal2: " << Normal2::x << ", " << Normal2::y << ", " << Normal2::z << std::endl;
    }

};

// Define corner in 3D

template<int direction1, int direction2, int direction3>
struct defineCorner3D{
  typedef vectorCompare<direction1,0,0> normal1;
  typedef vectorCompare<0,direction2,0> normal2;
  typedef vectorCompare<0,0,direction3> normal3;

  OPENLB_HOST_DEVICE
  static void print()
  {
        std::cout << "Normal1: " << normal1::x << ", " << normal1::y << ", " << normal1::z << std::endl;
        std::cout << "Normal2: " << normal2::x << ", " << normal2::y << ", " << normal2::z << std::endl;
        std::cout << "Normal3: " << normal3::x << ", " << normal3::y << ", " << normal3::z << std::endl;
  }

};

// Check if index is valid for distributions parallel to edge normal (Type=1)

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K == Lattice<T>::q and Type == 1, bool>::type isValid()
{
  return false;
}

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K < Lattice<T>::q and Type == 1, bool>::type isValid()
{
  return (std::is_same<vectorCompare<-(defineEdge3D<Plane,direction,orientation>::Normal1::x+defineEdge3D<Plane,direction,orientation>::Normal2::x),-(defineEdge3D<Plane,direction,orientation>::Normal1::y+defineEdge3D<Plane,direction,orientation>::Normal2::y),-(defineEdge3D<Plane,direction,orientation>::Normal1::z+defineEdge3D<Plane,direction,orientation>::Normal2::z)>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<-(defineEdge3D<Plane,direction,orientation>::Normal1::x+defineEdge3D<Plane,direction,orientation>::Normal2::x),0,0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<0,-(defineEdge3D<Plane,direction,orientation>::Normal1::y+defineEdge3D<Plane,direction,orientation>::Normal2::y),0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<0,0,-(defineEdge3D<Plane,direction,orientation>::Normal1::z+defineEdge3D<Plane,direction,orientation>::Normal2::z)>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value );
}

// Check if index is valid for distributions perpendicular to edge normal (Type=2)

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K == Lattice<T>::q and Type == 2, bool>::type isValid()
{
  return false;
}

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K < Lattice<T>::q and Type == 2, bool>::type isValid()
{
  return (std::is_same<vectorCompare<defineEdge3D<Plane,direction,orientation>::Axis::x-defineEdge3D<Plane,direction,orientation>::Normal1::x,defineEdge3D<Plane,direction,orientation>::Axis::y-defineEdge3D<Plane,direction,orientation>::Normal1::y,defineEdge3D<Plane,direction,orientation>::Axis::z-defineEdge3D<Plane,direction,orientation>::Normal1::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<defineEdge3D<Plane,direction,orientation>::Axis::x-defineEdge3D<Plane,direction,orientation>::Normal2::x,defineEdge3D<Plane,direction,orientation>::Axis::y-defineEdge3D<Plane,direction,orientation>::Normal2::y,defineEdge3D<Plane,direction,orientation>::Axis::z-defineEdge3D<Plane,direction,orientation>::Normal2::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<-defineEdge3D<Plane,direction,orientation>::Axis::x-defineEdge3D<Plane,direction,orientation>::Normal1::x,-defineEdge3D<Plane,direction,orientation>::Axis::y-defineEdge3D<Plane,direction,orientation>::Normal1::y,-defineEdge3D<Plane,direction,orientation>::Axis::z-defineEdge3D<Plane,direction,orientation>::Normal1::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<-defineEdge3D<Plane,direction,orientation>::Axis::x-defineEdge3D<Plane,direction,orientation>::Normal2::x,-defineEdge3D<Plane,direction,orientation>::Axis::y-defineEdge3D<Plane,direction,orientation>::Normal2::y,-defineEdge3D<Plane,direction,orientation>::Axis::z-defineEdge3D<Plane,direction,orientation>::Normal2::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value );
}

// Check if index is valid for distributions tangential on edge (Type=3)

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K == Lattice<T>::q and Type == 3, bool>::type isValid()
{
  return false;
}

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K < Lattice<T>::q and Type == 3, bool>::type isValid()
{
  return (std::is_same<vectorCompare<defineEdge3D<Plane,direction,orientation>::Normal1::x-defineEdge3D<Plane,direction,orientation>::Normal2::x,defineEdge3D<Plane,direction,orientation>::Normal1::y-defineEdge3D<Plane,direction,orientation>::Normal2::y,defineEdge3D<Plane,direction,orientation>::Normal1::z-defineEdge3D<Plane,direction,orientation>::Normal2::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<-defineEdge3D<Plane,direction,orientation>::Normal1::x+defineEdge3D<Plane,direction,orientation>::Normal2::x,-defineEdge3D<Plane,direction,orientation>::Normal1::y+defineEdge3D<Plane,direction,orientation>::Normal2::y,-defineEdge3D<Plane,direction,orientation>::Normal1::z+defineEdge3D<Plane,direction,orientation>::Normal2::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value );
}

// Check if index is valid for distributions along edge axis (Type = 4)

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K == Lattice<T>::q and Type == 4, bool>::type isValid()
{
  return false;
}

template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K < Lattice<T>::q and Type == 4, bool>::type isValid()
{
  return (std::is_same<vectorCompare<defineEdge3D<Plane,direction,orientation>::Axis::x,defineEdge3D<Plane,direction,orientation>::Axis::y,defineEdge3D<Plane,direction,orientation>::Axis::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value ||
      std::is_same<vectorCompare<-defineEdge3D<Plane,direction,orientation>::Axis::x,-defineEdge3D<Plane,direction,orientation>::Axis::y,-defineEdge3D<Plane,direction,orientation>::Axis::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value );
}

// Check if index is valid for corner (Type=5)

template<typename T, template <typename> class Lattice, int Type, int direction1, int direction2, int direction3, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K == Lattice<T>::q and Type == 5, bool>::type isValid()
{
  return false;
}

template<typename T, template <typename> class Lattice, int Type, int direction1, int direction2, int direction3, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K < Lattice<T>::q and Type == 5, bool>::type isValid()
{
  return (std::is_same<vectorCompare<-defineCorner3D<direction1,direction2,direction3>::normal1::x,0,0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
                || std::is_same<vectorCompare<0,-defineCorner3D<direction1,direction2,direction3>::normal2::y,0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
                || std::is_same<vectorCompare<0,0,-defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
                || std::is_same<vectorCompare<-defineCorner3D<direction1,direction2,direction3>::normal1::x,-defineCorner3D<direction1,direction2,direction3>::normal2::y,0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
                || std::is_same<vectorCompare<-defineCorner3D<direction1,direction2,direction3>::normal1::x,0,-defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
                || std::is_same<vectorCompare<0,-defineCorner3D<direction1,direction2,direction3>::normal2::y,-defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
        );
}

// Check if index is valid for tangential distributions at corner (Type=6)

template<typename T, template <typename> class Lattice, int Type, int direction1, int direction2, int direction3, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K == Lattice<T>::q and Type == 6, bool>::type isValid()
{
  return false;
}

template<typename T, template <typename> class Lattice, int Type, int direction1, int direction2, int direction3, int K>
OPENLB_HOST_DEVICE
constexpr typename std::enable_if<K < Lattice<T>::q and Type == 6, bool>::type isValid()
{
  return (std::is_same<vectorCompare<-defineCorner3D<direction1,direction2,direction3>::normal1::x,defineCorner3D<direction1,direction2,direction3>::normal2::y,0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
       || std::is_same<vectorCompare<defineCorner3D<direction1,direction2,direction3>::normal1::x,-defineCorner3D<direction1,direction2,direction3>::normal2::y,0>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
       || std::is_same<vectorCompare<-defineCorner3D<direction1,direction2,direction3>::normal1::x,0,defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
       || std::is_same<vectorCompare<defineCorner3D<direction1,direction2,direction3>::normal1::x,0,-defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
       || std::is_same<vectorCompare<0,-defineCorner3D<direction1,direction2,direction3>::normal2::y,defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
       || std::is_same<vectorCompare<0,defineCorner3D<direction1,direction2,direction3>::normal2::y,-defineCorner3D<direction1,direction2,direction3>::normal3::z>,vectorCompare<Lattice<T>::CPUBaseDescriptor::c[K][0],Lattice<T>::CPUBaseDescriptor::c[K][1],Lattice<T>::CPUBaseDescriptor::c[K][2]>>::value
        );
}

// Helper functions to add index to array
template<int VALUE, int POSITION, bool CONDITION>
OPENLB_HOST_DEVICE
typename std::enable_if<CONDITION,void>::type addIndex (int * indices)
{
  indices[POSITION] = VALUE;
  return;
}
template<int VALUE, int POSITION, bool CONDITION>
OPENLB_HOST_DEVICE
typename std::enable_if<!CONDITION,void>::type addIndex (int * indices)
{
  return;
}

// recursion end
template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int INDEX, int POSITION>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX==Lattice<T>::q,void>::type getSubIndices (int * indices)
{
  return;
}
// recursion start and body
template<typename T, template <typename> class Lattice, int Type, int Plane, int direction, int orientation, int INDEX=1, int POSITION=0>
OPENLB_HOST_DEVICE
typename std::enable_if<INDEX<Lattice<T>::q,void>::type getSubIndices (int * indices)
{
  addIndex<INDEX,POSITION,isValid<T, Lattice, Type, Plane, direction, orientation, INDEX>()
  > (indices);

  getSubIndices<T, Lattice, Type, Plane, direction, orientation,INDEX+1,
    POSITION+1*isValid<T, Lattice, Type, Plane, direction, orientation, INDEX>()
  > (indices);
  return;
}

}


#endif
