/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- header file.
 */
#ifndef LB_DYNAMICS_H
#define LB_DYNAMICS_H

#include "latticeDescriptors.h"
#include "core/util.h"
#include "core/postProcessing.h"
#include "core/latticeStatistics.h"
#include "core/blockData2D.h"
#include "core/config.h"
#include "dynamicsDataHandler.h"
#ifdef ENABLE_CUDA
#include "core/gpuhandler.h"
#endif

namespace olb {

template<typename T, template<typename U> class Lattice> class CellView;

/// Interface for the dynamics classes
//
// 1.Definition of public interface to:
// define Rho, U, Stress etc. on cells
// provide read access for Momenta on cells via the depricated compute interface, which should be called get and not compute
//
// The Non-Virtual Interface Idiom is used in order to separate interface definition and implementation
//
// 2.Provide a method for polymorphic dispatch of funcions that compute the collision step
//
template<typename T, template<typename U> class Lattice>
class Dynamics {
public:
  Dynamics() {}
  /// Destructor: virtual to enable inheritance
  virtual ~Dynamics() { }
  /// Clone the object on its dynamic type.
  virtual Dynamics<T,Lattice>* clone() const =0;


  /// 1. definition of interfaces
  /// define Rho,U,Stress etc. interface

  void 	defineRho (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho);
  void 	defineU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u);
  void 	defineRhoU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u);
  void 	defineAllMomenta (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi);
  void 	iniEquilibrium (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T const rho, T const * const OPENLB_RESTRICT u);


  /// define get interface
  T 	computeRho(DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex);
  void 	computeU (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT u) ;
  void 	computeJ (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j) ;
  void 	computeStress (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi ) ;
  void 	computeRhoU (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u) ;
  void 	computeAllMomenta (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi) ;
  T		computeEquilibrium (int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr) ;
  

 /// 2. definition of polymorphic dispatch 
 
  void dispatchCollisionCPU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData);
  void dispatchPostProcCPU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz = 0);

#ifdef ENABLE_CUDA
  void dispatchPostProcGPU (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size);
  void dispatchCollisionGPU (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size);
#endif

 /// OLD IMPLEMENTATION 
  /// Implementation of the collision step
  OPENLB_HOST_DEVICE
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) =0;
  virtual void collide(CellDataArray<T,Lattice> data,
                       LatticeStatistics<T>& statistics_) =0;
  virtual void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor){};
  /// Compute equilibrium distribution function
  OPENLB_HOST_DEVICE
  //virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Initialize cell at equilibrium distribution
  void iniEquilibrium(CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d]);
//  void iniEquilibrium (T** cellData, size_t cellIndex, T const rho, T const u[Lattice<T>::d]);
  /// Compute particle density on the cell.
  /** \return particle density
   */
  OPENLB_HOST_DEVICE
  virtual T computeRho(CellView<T,Lattice> const& cell) const =0;
  /// Compute fluid velocity on the cell.
  /** \param u fluid velocity
   */
  OPENLB_HOST_DEVICE
  virtual void computeU( CellView<T,Lattice> const& cell,
                         T u[Lattice<T>::d] ) const =0;
  /// Compute fluid momentum (j=rho*u) on the cell.
  /** \param j fluid momentum
   */
  virtual void computeJ( CellView<T,Lattice> const& cell,
                         T j[Lattice<T>::d] ) const =0;
  /// Compute components of the stress tensor on the cell.
  /** \param pi stress tensor */
  OPENLB_HOST_DEVICE
  virtual void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const =0;
  /// Compute fluid velocity and particle density on the cell.
  /** \param rho particle density
   *  \param u fluid velocity
   */
  OPENLB_HOST_DEVICE
  virtual void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const =0;
  /// Compute all momenta on the cell, up to second order.
  /** \param rho particle density
   *  \param u fluid velocity
   *  \param pi stress tensor
   */
  OPENLB_HOST_DEVICE
  virtual void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const =0;
  /// Set particle density on the cell.
  /** \param rho particle density
   */
  virtual void defineRho(CellView<T,Lattice>& cell, T rho) =0;
  /// Set fluid velocity on the cell.
  /** \param u fluid velocity
   */
  virtual void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) =0;
  /// Functions for offLattice Velocity boundary conditions
  virtual void setBoundaryIntersection(int iPop, T distance);
  virtual bool getBoundaryIntersection(int iPop, T point[Lattice<T>::d]);

  virtual T    getVelocityCoefficient(int iPop);

  /// Define fluid velocity and particle density on the cell.
  /** \param rho particle density
   *  \param u fluid velocity
   */
  virtual void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) =0;
  // virtual void defineRhoU (
      // T** cellData, size_t cellIndex, T const rho, T const u[Lattice<T>::d]) =0;
  /// Define all momenta on the cell, up to second order.
  /** \param rho particle density
   *  \param u fluid velocity
   *  \param pi stress tensor
   */
  virtual void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) =0;
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const =0;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega) =0;

  virtual T* getCollisionData() {assert(false);return 0;};
  virtual int getCollisionDataSize() {assert(false);return 0;};
  virtual int getMomentaDataSize() {assert(false); return 0;};
  virtual int getPostProcessorDataSize() {assert(false); return 0;};

  bool isPostProcDynamics();

  OPENLB_HOST_DEVICE
  static T computeEquilibriumStatic (int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr) ;
protected:
  /// TODO: do not use assert but define functions as pure! at this state this would only increase the struggle to implement all the functions in all classes

  /// private pure virtual functions in order to overload the define interface in derived classes:
  virtual void _defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho){ assert(false); } ;
  virtual void _defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u) { assert(false); };
  virtual void _defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u) { assert(false); };
  virtual void _defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi){ assert(false); };
  
  // private pure virtual functions in order to overload the compute interface in derived classes:
  virtual T 	_computeRho_polym(DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)  { assert(false);return 0; };
  virtual void 	_computeU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT u)  { assert(false); };
  virtual void 	_computeJ_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j)  { assert(false); };
  virtual void 	_computeStress_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )  {assert(false); };
  virtual void 	_computeRhoU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u) { assert(false); };
  virtual void 	_computeAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi) { assert(false); };
  virtual T		_computeEquilibirum_poly (int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr) ; // non pure

  /// private prure virtual dispatch functions
  virtual void _dispatchCollisionCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData) { assert(false); };
  virtual void _dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz = 0) { assert(false); };

#ifdef ENABLE_CUDA
  virtual void _dispatchCollisionGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) { assert(false); };
  virtual void _dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) { assert(false); };
#endif

  virtual bool _isPostProcDynamics_poly(){assert(false); return false;};

};

/// Interface for classes that compute velocity momenta
/** This class is useful for example to distinguish between bulk and
 * boundary nodes, given that on the boundaries, a particular strategy
 * must be applied to compute the velocity momenta.
 */
template<typename T, template<typename U> class Lattice>
struct Momenta {
  Momenta() {}
  /// Destructor: virtual to enable inheritance
  virtual ~Momenta() { }
  /// Compute particle density on the cell.
  OPENLB_HOST_DEVICE
  virtual T computeRho(CellView<T,Lattice> const& cell) const =0;
  OPENLB_HOST_DEVICE
  virtual T computeRho(CellDataArray<T,Lattice> const& data) const =0;
  /// Compute fluid velocity on the cell.
  OPENLB_HOST_DEVICE
  virtual void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const =0;
  OPENLB_HOST_DEVICE
  virtual void computeU (
    CellDataArray<T,Lattice> const& data,
    T u[Lattice<T>::d] ) const =0;
  /// Compute fluid momentum on the cell.
  virtual void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const =0;
  /// Compute components of the stress tensor on the cell.
  OPENLB_HOST_DEVICE
  virtual void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const =0;
  OPENLB_HOST_DEVICE
  virtual void computeStress (
    CellDataArray<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const =0;
  /// Compute fluid velocity and particle density on the cell.
  OPENLB_HOST_DEVICE
  virtual void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  virtual void computeRhoU (
    CellDataArray<T,Lattice> const& data,
    T& rho, T u[Lattice<T>::d]) const;
  /// Compute all momenta on the cell, up to second order.
  OPENLB_HOST_DEVICE
  virtual void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  virtual void computeAllMomenta (
    CellDataArray<T,Lattice> const& data,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  /// Set particle density on the cell.
  virtual void defineRho(CellView<T,Lattice>& cell, T rho) =0;
  /// Set fluid velocity on the cell.
  virtual void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) =0;
  /// Define fluid velocity and particle density on the cell.
  virtual void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]);
  /// Define all momenta on the cell, up to second order.
  virtual void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) =0;

};

/// Abstract base for dynamics classes
/** In this version of the Dynamics classes, the computation of the
 * velocity momenta is taken care of by an object of type Momenta.
 */
template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor = NoPostProcessor<T,Lattice>>
class BasicDynamics : public Dynamics<T,Lattice> {
public:
  typedef Lattice<T> LatticeType;
  typedef Momenta MomentaType;
  typedef PostProcessor PostProcessorType;
  /// Must be contructed with an object of type Momenta
  BasicDynamics(Momenta momenta);
  /// Must not be constructed with an object of type Momenta
  BasicDynamics();
  /// Implemented via the Momenta object
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData);
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex);

  /// Implemented via the Momenta object
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  OPENLB_HOST_DEVICE
  static void computeU (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void computeU (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT u);

  /// Implemented via the Momenta object
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const override;
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T * const OPENLB_RESTRICT j );
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T * const OPENLB_RESTRICT j );

  /// Implemented via the Momenta object
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  OPENLB_HOST_DEVICE
  static void computeStress (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );
  OPENLB_HOST_DEVICE
  static void computeStress (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );

  /// Implemented via the Momenta object
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  OPENLB_HOST_DEVICE
  static void computeRhoU (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T& rho, T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void computeRhoU (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u);

  /// Implemented via the Momenta object
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );
  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );

  /// Implemented via the Momenta object
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  OPENLB_HOST_DEVICE
  static void defineRho( T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho);
  OPENLB_HOST_DEVICE
  static void defineRho( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex,  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho);

  /// Implemented via the Momenta object
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) override;
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
                       const T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void defineU(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex,
                       const T * const OPENLB_RESTRICT u);

  /// Implemented via the Momenta object
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) override;
  OPENLB_HOST_DEVICE
  static void defineRhoU (
    T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void defineRhoU (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u);

  /// Implemented via the Momenta object
  // void defineRhoU (
    // T** cellData, size_t cellIndex, T const rho, T const u[Lattice<T>::d]) override;
  /// Implemented via the Momenta object
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) override;
  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT cellData,T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi );

  int getMomentaDataSize() override {return Momenta::numberDataEntries;}
  int getPostProcessorDataSize() override {return PostProcessor::numberDataEntries;}

protected:
  Momenta _momenta;  ///< computation of velocity momenta

	private:
  /// TODO: do not use assert but define functions as pure! at this state this would only increase the struggle to implement all the functions in all classes

  /// private pure virtual functions in order to overload the define interface in derived classes:
  virtual void _defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho) override;
  virtual void _defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u) override;
  virtual void _defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u) override;
  virtual void _defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi) override;
  
  // private pure virtual functions in order to overload the compute interface in derived classes:
  virtual T 	_computeRho_polym(DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex) override;
  virtual void 	_computeU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT u) override;
  virtual void 	_computeJ_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j) override; 
  virtual void 	_computeStress_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi ) override;
  virtual void 	_computeRhoU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u) override;
  virtual void 	_computeAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi) override;

  void _dispatchCollisionCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData) override ;
  void _dispatchPostProcCPU_poly (DynamicsDataHandler<T>*, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT CellDataArray, size_t nx, size_t ny, size_t nz=0) override;

#ifdef ENABLE_CUDA
  void _dispatchCollisionGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) override ;
  void _dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) override;
#endif

  bool _isPostProcDynamics_poly() override;
};

/// Implementation of a generic dynamics to realize a pressure drop at a periodic boundary
template<typename T, template<typename U> class Lattice, typename BaseDynamics>
class PeriodicPressureDynamics : public BaseDynamics {

public:
  /// Constructor
  PeriodicPressureDynamics(Dynamics<T,Lattice>& dynamics, T densityOffset, int nx, int ny, int nz=0) : BaseDynamics(), _densityOffset(densityOffset), _nx(nx), _ny(ny), _nz(nz) {};

  /// Implementation of the collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override{
  BaseDynamics::collide(cell,statistics_);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    if ( ((_nx==1 || _nx==-1) && Lattice<T>::c(iPop)[0]==_nx) || ((_ny==1 || _ny==-1) && Lattice<T>::c(iPop)[1]==_ny) || (Lattice<T>::d==3 && !_nz && Lattice<T>::c(iPop)[2]==_nz) ) {
      cell[iPop] += (cell[iPop] + Lattice<T>::t[iPop])*_densityOffset;
    }
  }
};

private:
  T _densityOffset;
  int _nx, _ny, _nz;
};

/// Implementation of the BGK collision step
template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor = NoPostProcessor<T,Lattice>>
class BGKdynamics : public BasicDynamics<T,Lattice,Momenta,BGKdynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor> {
public:
  /// Constructor
  BGKdynamics(T omega, Momenta& momenta);
  /// Constructor
  BGKdynamics(T omega);
  /// Clone the object on its dynamic type.
  BGKdynamics<T,Lattice, Momenta, PostProcessor>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override;
  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;

  T* getCollisionData() override
          {return _collisionData;};
  int getCollisionDataSize() override
          {return 1;};

  enum{omegaIndex = 0};
private:
  T _omega;  ///< relaxation parameter
  T _collisionData[1];
};

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor = NoPostProcessor<T,Lattice>>
class EntropicMRTdynamics : public BasicDynamics<T,Lattice,Momenta,EntropicMRTdynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor> {
public:
  /// Constructor
  EntropicMRTdynamics(T omega, Momenta& momenta);
  /// Constructor
  EntropicMRTdynamics(T omega);
  /// Clone the object on its dynamic type.
  EntropicMRTdynamics<T,Lattice, Momenta, PostProcessor>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override {assert(false);}
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override {assert(false);}
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override {assert(false);};
  OPENLB_HOST_DEVICE
  static T productEquilibrium(int iPop, T rho, T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static T productEquilibriumQuant(int iD, T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static T productEquilibriumFromQuant(int iPop, T rho, T u[Lattice<T>::d], T uQuant[Lattice<T>::d]) ;

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;

  T* getCollisionData() override
          {return _collisionData;};
  int getCollisionDataSize() override
          {return 2+19*19+19*19;};

  enum{omegaIndex = 0, betaIndex = 1, momentIndex = 2, invMomentIndex = 2+19*19};
private:
  T _omega;  ///< relaxation parameter
  T _beta;
  T _collisionData[2+19*19+19*19];

  static const T momentMatrix[19*19];
  static const T invMomentMatrix[19*19];
};

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor = NoPostProcessor<T,Lattice>>
class EntropicSmagorinskyMRTdynamics : public BasicDynamics<T,Lattice,Momenta,EntropicSmagorinskyMRTdynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor> {
public:
  /// Constructor
  EntropicSmagorinskyMRTdynamics(T omega, Momenta& momenta, T smagoConst);
  /// Constructor
  EntropicSmagorinskyMRTdynamics(T omega, T smagoConst);
  /// Clone the object on its dynamic type.
  EntropicSmagorinskyMRTdynamics<T,Lattice, Momenta, PostProcessor>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override {assert(false);}
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override {assert(false);}
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override {assert(false);};

  OPENLB_HOST_DEVICE
  static T productEquilibrium(int iPop, T rho, T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static T productEquilibriumQuant(int iD, T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static T productEquilibriumFromQuant(int iPop, T rho, T u[Lattice<T>::d], T uQuant[Lattice<T>::d]) ;

  OPENLB_HOST_DEVICE
  static T computeEffectiveOmega(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
        T const omega, T const preFactor, T const rho, T const u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;

  T* getCollisionData() override
          {return _collisionData;};
  int getCollisionDataSize() override
          {return 4+19*19+19*19;};

  enum{omegaIndex = 0, betaIndex = 1, smagoIndex = 2, preFactorIndex = 3, momentIndex = 4, invMomentIndex = 4+19*19};
private:
  T _omega;  ///< relaxation parameter
  T _beta;
  T _collisionData[4+19*19+19*19];

  static const T momentMatrix[19*19];
  static const T invMomentMatrix[19*19];
};

/// Implementation of the pressure-corrected BGK collision step
template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor = NoPostProcessor<T,Lattice>>
class ConstRhoBGKdynamics final : public BasicDynamics<T,Lattice,Momenta,ConstRhoBGKdynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor> {
public:
  /// Constructor
  ConstRhoBGKdynamics(T omega, Momenta& momenta);
  /// Constructor
  ConstRhoBGKdynamics(T omega);
  /// Clone the object on its dynamic type.
  ConstRhoBGKdynamics<T,Lattice, Momenta, PostProcessor>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override;

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		  size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		  size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData
		  ,const T * const OPENLB_RESTRICT collisionData);

  OPENLB_HOST_DEVICE
  static T computeEquilibriumStatic(int iPop, T rho, const T u[Lattice<T>::d], T uSqr);
  
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;

  T* getCollisionData() override
		  {return _collisionData;};
  int getCollisionDataSize() override
		  {return 1;};

  enum{omegaIndex = 0};
private:
  T _omega;  ///< relaxation parameter
  T _collisionData[1];

protected:
  /// private prure virtual dispatch functions
  
};

/// Implementation of the so-called incompressible collision step
template<typename T, template<typename U> class Lattice, class Momenta>
class IncBGKdynamics : public BasicDynamics<T,Lattice,Momenta, IncBGKdynamics<T,Lattice,Momenta>> {
public:
  /// Constructor
  IncBGKdynamics(T omega, Momenta& momenta);
  /// Clone the object on its dynamic type.
  IncBGKdynamics<T,Lattice, Momenta>* clone() const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;
private:
  T _omega;  ///< relaxation parameter
};



/// Implementation of the Regularized BGK collision step
/** This model is substantially more stable than plain BGK, and has roughly
 * the same efficiency. However, it cuts out the modes at higher Knudsen
 * numbers and can not be used in the regime of rarefied gases.
 */
template<typename T, template<typename U> class Lattice, class Momenta>
class RLBdynamics : public BasicDynamics<T,Lattice,Momenta, RLBdynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  RLBdynamics(T omega, Momenta& momenta);
  /// Clone the object on its dynamic type.
  RLBdynamics<T,Lattice, Momenta>* clone() const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;
private:
  T _omega;  ///< relaxation parameter
};

/// Implementation of Regularized BGK collision, followed by any Dynamics
template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
class CombinedRLBdynamics : public BasicDynamics<T,Lattice,Momenta, CombinedRLBdynamics<T, Lattice, Dynamics, Momenta>> {
public:
  /// Constructor
  CombinedRLBdynamics(T omega, Momenta& momenta);
  /// Clone the object on its dynamic type.
  CombinedRLBdynamics<T, Lattice, Dynamics, Momenta>* clone() const override;
  /// Compute equilibrium distribution function
  OPENLB_HOST_DEVICE
  T computeEquilibrium(int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr) ;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override;

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		  size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		  size_t momentaIndex, T* const OPENLB_RESTRICT collisionData) { assert(false); }

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData
		  ,const T * const OPENLB_RESTRICT collisionData) { assert(false); }
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;
private:
  Dynamics _boundaryDynamics;
};

/// Implementation of the BGK collision step with external force
template<typename T, template<typename U> class Lattice, class Momenta>
class ForcedBGKdynamics : public BasicDynamics<T,Lattice,Momenta, ForcedBGKdynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  ForcedBGKdynamics(T omega, Momenta& momenta);
  /// Clone the object on its dynamic type.
  ForcedBGKdynamics<T,Lattice, Momenta>* clone() const override;
  ///  Compute fluid velocity on the cell.
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Compute fluid velocity and particle density on the cell.
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override;
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;
protected:
  T _omega;  ///< relaxation parameter
  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;
};

/// Implementation of the BGK collision step with external force
template<typename T, template<typename U> class Lattice, class Momenta>
class ResettingForcedBGKdynamics : public ForcedBGKdynamics<T,Lattice, Momenta> {
public:
  ResettingForcedBGKdynamics(T omega, Momenta& momenta);
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  inline void setForce(T force[3])
  {
//    _frc[0] = force[0];
//    _frc[1] = force[1];
//    _frc[2] = force[2];
    _frc[0] = 0.0;
    _frc[1] = 0.0;
    _frc[2] = 0.0;
  }
private:
  T _frc[3];
};

/// Other Implementation of the BGK collision step with external force
template<typename T, template<typename U> class Lattice, class Momenta>
class ForcedShanChenBGKdynamics : public ForcedBGKdynamics<T,Lattice, Momenta> {
public:
  /// Constructor
  ForcedShanChenBGKdynamics(T omega, Momenta& momenta);
  ///  Compute fluid velocity on the cell.
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Compute fluid velocity and particle density on the cell.
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
};

/// Implementation of the 3D D3Q13 dynamics
/** This is (so far) the minimal existing 3D model, with only 13
 * directions. Three different relaxation times are used to achieve
 * asymptotic hydrodynamics, isotropy and galilean invariance.
 */
template<typename T, template<typename U> class Lattice, class Momenta>
class D3Q13dynamics : public BasicDynamics<T,Lattice,Momenta, D3Q13dynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  D3Q13dynamics(T omega, Momenta& momenta);
  /// Clone the object on its dynamic type.
  D3Q13dynamics<T,Lattice, Momenta>* clone() const override;
  /// Compute equilibrium distribution function
  OPENLB_HOST_DEVICE
  T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega) override;
private:
  T lambda_nu;        ///< first relaxation parameter
  T lambda_nu_prime;  ///< second relaxation parameter
};

/// Standard computation of velocity momenta in the bulk
template<typename T, template<typename U> class Lattice>
struct BulkMomenta {
  static constexpr int numberDataEntries = 0;
  static constexpr int dataOffset_ = 0;
  /// Compute particle density on the cell.
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const;
  OPENLB_HOST_DEVICE
  T computeRho(CellDataArray<T,Lattice> const& data) const;
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData,size_t momentaIndex);
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex ,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex);


  /// Compute fluid velocity on the cell.
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  void computeU (
    CellDataArray<T,Lattice> const& data,
    T u[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeU (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T * const OPENLB_RESTRICT u );

  OPENLB_HOST_DEVICE
  static void computeU (const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex ,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT u);

  /// Compute fluid momentum on the cell.
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const;
  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT cellData, const T* const OPENLB_RESTRICT momentaData,
    T * const OPENLB_RESTRICT j);

  OPENLB_HOST_DEVICE
  static void computeJ (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
size_t momentaIndex,  T * const OPENLB_RESTRICT j);

  /// Compute components of the stress tensor on the cell.
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeStress (
    CellDataArray<T,Lattice> const& data,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeStress (
   const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );

  OPENLB_HOST_DEVICE
  static void computeStress (
   const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,  T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi );

  /// Compute fluid velocity and particle density on the cell.
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const;
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellDataArray<T,Lattice> const& data,
    T& rho, T u[Lattice<T>::d]) const;
  template<int size>
  void computeRhoU (
    CellDataArray<T,Lattice> const& cell,
    T* rho, T u[Lattice<T>::d][size]) const;
  OPENLB_HOST_DEVICE
  static void computeRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T& rho, T u[Lattice<T>::d]);

  OPENLB_HOST_DEVICE
  static void computeRhoU (
   const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, size_t const momentaIndex,
  T& rho, T * const OPENLB_RESTRICT u);

  /// Compute all momenta on the cell, up to second order.
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellDataArray<T,Lattice> const& data,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const;
  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
    const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi);

  OPENLB_HOST_DEVICE
  static void computeAllMomenta (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T& rho, T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi);

  /// Set particle density on the cell.
  void defineRho(CellView<T,Lattice>& cell, T rho);
  OPENLB_HOST_DEVICE
  static void defineRho( T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho);

  OPENLB_HOST_DEVICE
  static void defineRho( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho);

  /// Set fluid velocity on the cell.
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineU( T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  const T * const OPENLB_RESTRICT u);
  OPENLB_HOST_DEVICE
  static void defineU( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  const T * const OPENLB_RESTRICT u);

  /// Define fluid velocity and particle density on the cell.
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]);
  OPENLB_HOST_DEVICE
  static void defineRhoU (
    T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u);

  OPENLB_HOST_DEVICE
  static void defineRhoU (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT  momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u);

  /// Define all momenta on the cell, up to second order.
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] );
  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi);

  OPENLB_HOST_DEVICE
  static void defineAllMomenta (
    T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T rho, const T * const OPENLB_RESTRICT u,
    const T * const OPENLB_RESTRICT pi);


};

template<typename T, template<typename U> class Lattice, class Momenta = BulkMomenta<T,Lattice>, class PostProcessor = NoPostProcessor<T,Lattice>>
class IniEquilibriumDynamics : public BasicDynamics<T,Lattice,Momenta,IniEquilibriumDynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor>
{
public:
  IniEquilibriumDynamics();

  IniEquilibriumDynamics<T,Lattice, Momenta, PostProcessor>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override {assert(false);}
  void collide(CellDataArray<T,Lattice> data,
                       LatticeStatistics<T>& statistics_) override {assert(false);}
  void collide(CellDataArray<T,Lattice> data,
                       LatticeStatistics<T>& statistics_, Lattice<T> const & latticeDescriptor) override {assert(false);}

  /// Yields NaN
  T getOmega() const override {return std::numeric_limits<T>::quiet_NaN();}
  /// Does nothing
  void setOmega(T omega) override {};

  T* getCollisionData() override {return nullptr;};
  int getCollisionDataSize() override {return 0;};
  int getMomentaDataSize() override {return 0;};

///especially change collision
  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

};

/// Velocity is stored in external scalar (and computed e.g. in a PostProcessor)
template<typename T, template<typename U> class Lattice>
struct ExternalVelocityMomenta : public Momenta<T,Lattice> {
  /// Compute particle density on the cell.
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  /// Compute fluid velocity on the cell.
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Compute fluid momentum on the cell.
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const override;
  /// Compute components of the stress tensor on the cell.
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Compute fluid velocity and particle density on the cell.
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  /// Compute all momenta on the cell, up to second order.
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Set particle density on the cell.
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  /// Set fluid velocity on the cell.
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) override;
  /// Define fluid velocity and particle density on the cell.
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) override;
  /// Define all momenta on the cell, up to second order.
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) override;


};

/// Implementation of "bounce-back" dynamics
/** This is a very popular way to implement no-slip boundary conditions,
 * because the dynamics are independent of the orientation of the boundary.
 * It is a special case, because it implements no usual LB dynamics.
 * For that reason, it derives directly from the class Dynamics.
 *
 * The code works for both 2D and 3D lattices.
 */
template<typename T, template<typename U> class Lattice>
class BounceBack : public Dynamics<T,Lattice> {
public:
  /// A fictitious density value on bounce-back in not fixed on nodes via this constructor.
  BounceBack();
  /// You may fix a fictitious density value on bounce-back nodes via this constructor.
  BounceBack(T rho);
  /// Clone the object on its dynamic type.
  BounceBack<T,Lattice>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override;
  /// Yields 1;
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  /// Yields 0;
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Yields 0;
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const override;
  /// Yields NaN
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Does nothing
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  /// Does nothing
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) override;
  /// Does nothing
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) override;
  /// Does nothing
  // void defineRhoU (
        // T** cellData, size_t cellIndex, T const rho, T const u[Lattice<T>::d]) override;
  /// Does nothing
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) override;
  /// Yields NaN
  T getOmega() const override;
  /// Does nothing
  void setOmega(T omega) override;

  T* getCollisionData() override {static T inst; return &inst;};
  int getCollisionDataSize() override {return 0;};
  int getMomentaDataSize() override {return 0;};
  int getPostProcessorDataSize() override {return 0;}

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);
  
protected:
  void _dispatchCollisionCPU_poly (DynamicsDataHandler<T>* dataHandler, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData) override;
  void _dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT CellDataArray, size_t nx, size_t ny, size_t nz=0) override;
  void _defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho) override;
  void _defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u) override;
  void _defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u) override;
  void _defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi) override;

#ifdef ENABLE_CUDA
  void _dispatchCollisionGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) override ;
  void _dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) override;
#endif

  virtual bool _isPostProcDynamics_poly(){return false;};
private:
  T _rho;
  bool _rhoFixed;
};

/// Implementation of "bounce-back velocity" dynamics
/** This is a very popular way to implement no-slip boundary conditions,
 * because the dynamics are independent of the orientation of the boundary.
 * It is a special case, because it implements no usual LB dynamics.
 * For that reason, it derives directly from the class Dynamics. It
 * fixes the velociy to a given velocity _u.
 *
 * The code works for both 2D and 3D lattices.
 */
template<typename T, template<typename U> class Lattice>
class BounceBackVelocity : public Dynamics<T,Lattice> {
public:
  /// A fictitious density value on bounce-back in not fixed on nodes via this constructor.
  BounceBackVelocity(const T u[Lattice<T>::d]);
  /// You may fix a fictitious density value on bounce-back nodes via this constructor.
  BounceBackVelocity(const T rho, const T u[Lattice<T>::d]);
  /// Clone the object on its dynamic type.
  BounceBackVelocity<T,Lattice>* clone() const override;
  /// Collision step, bounce back with a fixed velocity _u
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Retuns rho (if defined else zero)
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  /// Retuns _u
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Retuns rho (if defined else zero) times _u
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const override;
  /// Yields NaN
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Retuns rho (if defined else zero) and _u
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Devines the velocity rho
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  /// Devines the velocity _u
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) override;
  /// Devines rho and _u
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) override;
  /// Does nothing
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) override;
  /// Yields NaN
  T getOmega() const override;
  /// Does nothing
  void setOmega(T omega) override;
private:
  T _rho;
  bool _rhoFixed;
  T _u[Lattice<T>::d];
};

/// Implementation of "bounce-back anti" dynamics
/** This is a way to implement a Dirichlet rho/pressure boundary conditions,
 * because the dynamics are independent of the orientation of the boundary.
 * It is a special case, because it implements no usual LB dynamics.
 * For that reason, it derives directly from the class Dynamics. It
 * fixes the rho to a given _rho.
 *
 * The code works for both 2D and 3D lattices.
 */
template<typename T, template<typename U> class Lattice>
class BounceBackAnti : public Dynamics<T,Lattice> {
public:
  /// A fictitious density value on bounce-back in not fixed on nodes via this constructor.
  BounceBackAnti();
  /// You may fix a fictitious density value on bounce-back nodes via this constructor.
  BounceBackAnti(T rho);
  /// Clone the object on its dynamic type.
  BounceBackAnti<T,Lattice>* clone() const override;
  /// Collision step, bounce back with a fixed velocity _u
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Retuns rho (if defined else zero)
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  /// Retuns _u
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Retuns rho (if defined else zero) times _u
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const override;
  /// Yields NaN
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Retuns rho (if defined else zero) and _u
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Devines the velocity rho
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  /// Devines the velocity _u
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) override;
  /// Devines rho and _u
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) override;
  /// Does nothing
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) override;
  /// Yields NaN
  T getOmega() const override;
  /// Does nothing
  void setOmega(T omega) override;
private:
  T _rho;
  bool _rhoFixed;
  T _u[Lattice<T>::d];
};


/** Corresponds to macro Robin boundary, micro Fresnel surface
 *  Motivated by Hiorth et al. 2008; doi 10.1002/fld.1822
 */
template<typename T, template<typename U> class Lattice>
class PartialBounceBack final: public BounceBack<T,Lattice> {
public:
  PartialBounceBack(T rf);
  OPENLB_HOST_DEVICE
  T computeEquilibrium( int iPop, T rho, const T u[Lattice<T>::d], T uSqr ) const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics_) override;
private:
  T _rf;
};

template<typename T, template<typename U> class Lattice>
class NoDataDynamics : public Dynamics<T, Lattice>
{
public:
  virtual T* getCollisionData() override {static T inst; return &inst;};
  virtual int getCollisionDataSize() override {return 0;};
  virtual int getMomentaDataSize() override {return 0;};
  virtual int getPostProcessorDataSize() override {return 0;}

  virtual void _defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho){ } ;
  virtual void _defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u) { };
  virtual void _defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u) { };
  virtual void _defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi){ };


  /// private prure virtual dispatch functions
  virtual void _dispatchCollisionCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData) {};
  virtual void _dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz = 0) {};

#ifdef ENABLE_CUDA
  void _dispatchCollisionGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) { };
  void _dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) { };
#endif
};

/// Implementation of a "dead cell" that does nothing
template<typename T, template<typename U> class Lattice>
class NoDynamics : public NoDataDynamics<T, Lattice> {
public:
  /// You may fix a fictitious density value on no dynamics node via this constructor.
  NoDynamics(T rho = T(1) );
  /// Clone the object on its dynamic type.
  NoDynamics<T,Lattice>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override;

  template <int width>
  void collision(CellDataArray<T,Lattice> cell, const bool* fluidMask,
                       LatticeStatistics<T>& statistics_) {}

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		  size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		  size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData
		  ,const T * const OPENLB_RESTRICT collisionData);
  /// Yields 1;
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  /// Yields 0;
  OPENLB_HOST_DEVICE
  void computeU (
    CellView<T,Lattice> const& cell,
    T u[Lattice<T>::d] ) const override;
  /// Yields 0;
  void computeJ (
    CellView<T,Lattice> const& cell,
    T j[Lattice<T>::d] ) const override;
  /// Yields NaN
  OPENLB_HOST_DEVICE
  void computeStress (
    CellView<T,Lattice> const& cell,
    T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  OPENLB_HOST_DEVICE
  void computeRhoU (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d]) const override;
  OPENLB_HOST_DEVICE
  void computeAllMomenta (
    CellView<T,Lattice> const& cell,
    T& rho, T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  /// Does nothing
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  /// Does nothing
  void defineU(CellView<T,Lattice>& cell,
                       const T u[Lattice<T>::d]) override;
  /// Does nothing
  void defineRhoU (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d]) override;
  /// Does nothing
  void defineAllMomenta (
    CellView<T,Lattice>& cell,
    T rho, const T u[Lattice<T>::d],
    const T pi[util::TensorVal<Lattice<T> >::n] ) override;
  /// Yields NaN
  T getOmega() const override;
  /// Does nothing
  void setOmega(T omega) override;

  virtual T* getCollisionData() override { return nullptr;};
  virtual int getCollisionDataSize() { return 0;};

private:
  virtual bool _isPostProcDynamics_poly() override {return false;};
  /// Default rho=1
  T _rho;
};
/// Implementation of a "dead cell" that does nothing
template<typename T, template<typename U> class Lattice>
class GhostDynamics : public BasicDynamics<T, Lattice,BulkMomenta<T,Lattice>, GhostDynamics<T,Lattice>> {
public:
  /// You may fix a fictitious density value on no dynamics node via this constructor.
  GhostDynamics(T rho = T(1) );
  /// Clone the object on its dynamic type.
  GhostDynamics<T,Lattice>* clone() const override;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override;

  template <int width>
  void collision(CellDataArray<T,Lattice> cell, const bool* fluidMask,
                       LatticeStatistics<T>& statistics_) {}

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
		  size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		  size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData
		  ,const T * const OPENLB_RESTRICT collisionData);

  /// Yields 1;
  // OPENLB_HOST_DEVICE
  // T computeRho(CellView<T,Lattice> const& cell) const override;
  // /// Yields 0;
  // OPENLB_HOST_DEVICE
  // void computeU (
    // CellView<T,Lattice> const& cell,
    // T u[Lattice<T>::d] ) const override;
  // /// Yields 0;
  // void computeJ (
    // CellView<T,Lattice> const& cell,
    // T j[Lattice<T>::d] ) const override;
  // /// Yields NaN
  // OPENLB_HOST_DEVICE
  // void computeStress (
    // CellView<T,Lattice> const& cell,
    // T rho, const T u[Lattice<T>::d],
    // T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  // OPENLB_HOST_DEVICE
  // void computeRhoU (
    // CellView<T,Lattice> const& cell,
    // T& rho, T u[Lattice<T>::d]) const override;
  // OPENLB_HOST_DEVICE
  // void computeAllMomenta (
    // CellView<T,Lattice> const& cell,
    // T& rho, T u[Lattice<T>::d],
    // T pi[util::TensorVal<Lattice<T> >::n] ) const override;
  // /// Does nothing
  // void defineRho(CellView<T,Lattice>& cell, T rho) override;
  // /// Does nothing
  // void defineU(CellView<T,Lattice>& cell,
                       // const T u[Lattice<T>::d]) override;
  // /// Does nothing
  // void defineRhoU (
    // CellView<T,Lattice>& cell,
    // T rho, const T u[Lattice<T>::d]) override;
  // /// Does nothing
  // void defineAllMomenta (
    // CellView<T,Lattice>& cell,
    // T rho, const T u[Lattice<T>::d],
    // const T pi[util::TensorVal<Lattice<T> >::n] ) override;
  /// Yields NaN
  T getOmega() const override;
  /// Does nothing
  void setOmega(T omega) override;

  virtual T* getCollisionData() override { return nullptr;};
  virtual int getCollisionDataSize() { return 0;};

private:
  virtual bool _isPostProcDynamics_poly() override {return false;};
  /// Default rho=1
  T _rho;
};


// Implementation of PostProcessor only (e.g. periodicity). NO method of this class should be called EXCEPT postprocessing!
template<typename T, template<typename U> class Lattice, class PostProcessor>
class PostProcessingDynamics : public Dynamics<T, Lattice> {
public:
  typedef Lattice<T> LatticeType;
  typedef BulkMomenta<T,Lattice> MomentaType;
  typedef PostProcessor PostProcessorType;
  /// You may fix a fictitious density value on no dynamics node via this constructor.
  PostProcessingDynamics(){};
  /// Clone the object on its dynamic type.
  PostProcessingDynamics<T,Lattice,PostProcessor>* clone() const override {assert(false); return nullptr;};
    /// Collision step
    OPENLB_HOST_DEVICE
    void collide(CellView<T,Lattice>& cell,
                         LatticeStatistics<T>& statistics_) override {assert(false);};
    void collide(CellDataArray<T,Lattice> cell,
                         LatticeStatistics<T>& statistics_) override {assert(false);};
    void collide(CellDataArray<T,Lattice> data,
                         LatticeStatistics<T>& statistics_,Lattice<T> const & latticeDescriptor) override {assert(false);};

    template <int width>
    void collision(CellDataArray<T,Lattice> cell, const bool* fluidMask,
                         LatticeStatistics<T>& statistics_) {assert(false);}

    OPENLB_HOST_DEVICE
    static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
            size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
            size_t momentaIndex, T* const OPENLB_RESTRICT collisionData) {assert(false);};

    OPENLB_HOST_DEVICE
    static void collision(T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData
            ,const T * const OPENLB_RESTRICT collisionData) {assert(false);};
    /// Yields 1;
    OPENLB_HOST_DEVICE
    T computeRho(CellView<T,Lattice> const& cell) const override {assert(false); return 0;};
    /// Yields 0;
    OPENLB_HOST_DEVICE
    void computeU (
      CellView<T,Lattice> const& cell,
      T u[Lattice<T>::d] ) const override {assert(false);};
    /// Yields 0;
    void computeJ (
      CellView<T,Lattice> const& cell,
      T j[Lattice<T>::d] ) const override {assert(false);};
    /// Yields NaN
    OPENLB_HOST_DEVICE
    void computeStress (
      CellView<T,Lattice> const& cell,
      T rho, const T u[Lattice<T>::d],
      T pi[util::TensorVal<Lattice<T> >::n] ) const override {assert(false);};
    OPENLB_HOST_DEVICE
    void computeRhoU (
      CellView<T,Lattice> const& cell,
      T& rho, T u[Lattice<T>::d]) const override {assert(false);};
    OPENLB_HOST_DEVICE
    void computeAllMomenta (
      CellView<T,Lattice> const& cell,
      T& rho, T u[Lattice<T>::d],
      T pi[util::TensorVal<Lattice<T> >::n] ) const override {assert(false);};
    /// Does nothing
    void defineRho(CellView<T,Lattice>& cell, T rho) override {assert(false);};
    /// Does nothing
    void defineU(CellView<T,Lattice>& cell,
                         const T u[Lattice<T>::d]) override {assert(false);};
    /// Does nothing
    void defineRhoU (
      CellView<T,Lattice>& cell,
      T rho, const T u[Lattice<T>::d]) override {assert(false);};
    /// Does nothing
    void defineAllMomenta (
      CellView<T,Lattice>& cell,
      T rho, const T u[Lattice<T>::d],
      const T pi[util::TensorVal<Lattice<T> >::n] ) override {assert(false);};
    /// Yields NaN
    T getOmega() const override {assert(false); return 0;};
    /// Does nothing
    void setOmega(T omega) override {assert(false);};

    T* getCollisionData() override {return nullptr;};
    int getCollisionDataSize() {return 0;};

    virtual int getMomentaDataSize() override {return 0;};
    virtual int getPostProcessorDataSize() override {return PostProcessor::numberDataEntries;}

  private:
    bool _isPostProcDynamics_poly() override {return true;};

    void _defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho){ assert(false); } ;
    void _defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u) { assert(false); };
    void _defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u) { assert(false); };
    void _defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi){ assert(false); };

    // private pure virtual functions in order to overload the compute interface in derived classes:
    T     _computeRho_polym(DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)  { assert(false);return 0; };
    void  _computeU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT u)  { assert(false); };
    void  _computeJ_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j)  { assert(false); };
    void  _computeStress_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )  {assert(false); };
    void  _computeRhoU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u) { assert(false); };
    void  _computeAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi) { assert(false); };

    /// private prure virtual dispatch functions
    void _dispatchCollisionCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData) {};
    void _dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz = 0);

  #ifdef ENABLE_CUDA
    void _dispatchCollisionGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size) {};
    void _dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size);
  #endif
};

/// Dynamics for offLattice boundary conditions
/// OffDynamics are basically NoDynamics with the additional functionality
/// to store given velocities exactly at boundary links.
template<typename T, template<typename U> class Lattice>
class OffDynamics : public NoDynamics<T,Lattice> {
public:
  /// Constructor
  OffDynamics(const T _location[Lattice<T>::d]);
  /// Constructor
  OffDynamics(const T _location[Lattice<T>::d], T _distances[Lattice<T>::q]);
  /// Returns local stored rho which is updated if the bc is used as velocity!=0 condition
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
  /// Returns an average of the locally stored u
  OPENLB_HOST_DEVICE
  void computeU(CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const override;
  /// Set Intersection of the link and the boundary
  void setBoundaryIntersection(int iPop, T distance) override;
  /// Get Intersection of the link and the boundary
  bool getBoundaryIntersection(int iPop, T intersection[Lattice<T>::d]) override;
  /// Set particle density on the cell.
  void defineRho(CellView<T,Lattice>& cell, T rho) override;
  /// Set single velocity
  void defineRho(int iPop, T rho) override;
  /// Set fluid velocity on the cell.
  void defineU(CellView<T,Lattice>& cell, const T u[Lattice<T>::d]) override;
  /// Set constant velocity
  void defineU(const T u[Lattice<T>::d]) override;
  /// Set single velocity
  void defineU(int iPop, const T u[Lattice<T>::d]) override;
  /// Get VelocitySummand for Bouzidi-Boundary Condition
  T getVelocityCoefficient(int iPop) override;

private:
  T _rho;
  T _u[Lattice<T>::q][Lattice<T>::d];
  T location[Lattice<T>::d];
  T distances[Lattice<T>::q];
  T boundaryIntersection[Lattice<T>::q][Lattice<T>::d];
  T velocityCoefficient[Lattice<T>::q];
};

/// Implementation of density sink by setting a zero distribution on the cell
template<typename T, template<typename U> class Lattice>
class ZeroDistributionDynamics : public NoDynamics<T,Lattice> {
public:
  /// Constructor.
  ZeroDistributionDynamics();
  /// Clone the object on its dynamic type.
  ZeroDistributionDynamics<T,Lattice>* clone() const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Yields 1
  OPENLB_HOST_DEVICE
  T computeRho(CellView<T,Lattice> const& cell) const override;
};


namespace instances {

template<typename T, template<typename U> class Lattice>
BulkMomenta<T,Lattice>& getBulkMomenta();

template<typename T, template<typename U> class Lattice>
ExternalVelocityMomenta<T,Lattice>& getExternalVelocityMomenta();

template<typename T, template<typename U> class Lattice>
BounceBack<T,Lattice>& getBounceBack();

template <typename T, template<typename U> class Lattice>
GhostDynamics<T,Lattice>& getGhostDynamics();

template<typename T, template<typename U> class Lattice>
PartialBounceBack<T,Lattice>& getPartialBounceBack(const double rf);

template<typename T, template<typename U> class Lattice>
BounceBackVelocity<T,Lattice>& getBounceBackVelocity(const double rho, const double u[Lattice<T>::d]);

template<typename T, template<typename U> class Lattice>
BounceBackAnti<T,Lattice>& getBounceBackAnti(const double rho);

template<typename T, template<typename U> class Lattice>
NoDynamics<T,Lattice>& getNoDynamics(T rho = T(1) );

template<typename T, template<typename U> class Lattice>
ZeroDistributionDynamics<T,Lattice>& getZeroDistributionDynamics();

}

}

#endif
