/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006-2015 Jonas Latt, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- generic implementation.
 */
#ifndef LB_DYNAMICS_HH
#define LB_DYNAMICS_HH

#include <algorithm>
#include <limits>
#include "dynamics.h"
#include "core/cell.h"
#include "lbHelpers.h"
#include "firstOrderLbHelpers.h"
#include "d3q13Helpers.h"
#include "core/gpuhandler.h"
#include "boundary/momentaOnBoundaries.h"

#include <iostream>

namespace olb {



////////////////////// Class Dynamics ////////////////////////

  /// 1. definition of interfaces
  /// define Rho,U,Stress etc. interface

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::defineRho (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho)
  {
  	_defineRho_polym (dataHandler, cellData,cellIndex,rho);
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::defineU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u)
  {
	_defineU_polym (dataHandler, cellData,cellIndex,u);	
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::defineRhoU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u)
  {
  	_defineRhoU_polym (dataHandler, cellData,cellIndex,rho,u);
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::defineAllMomenta (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi)
  {
 	_defineAllMomenta_polym (dataHandler, cellData,cellIndex,rho,u,pi);
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::iniEquilibrium (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T const rho, T const * const OPENLB_RESTRICT u)
  {
  	T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  	for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
    	cellData[iPop][cellIndex] = computeEquilibrium(iPop, rho, u, uSqr);
  	}

  	cellData[Lattice<T>::rhoIndex][cellIndex] = rho;
  	for(int iDim =0; iDim<Lattice<T>::d; ++iDim)
  	    cellData[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];
  }

  /// define get interface
template<typename T, template<typename U> class Lattice>
  T 	 Dynamics<T,Lattice>::computeRho(DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
  {
  	_computeRho_polym (dataHandler, cellData,cellIndex);
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::computeU (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT u)
  {
  	_computeU_polym (dataHandler, cellData, cellIndex,u);
  }

template<typename T, template<typename U> class Lattice>
  void 	Dynamics<T,Lattice>::computeJ (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j)
  {
  	_computeJ_polym (dataHandler, cellData,cellIndex,j);
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::computeStress (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi ) 
  {
  	_computeStress_polym (dataHandler, cellData,cellIndex,rho,u,pi );
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::computeRhoU (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u) 
  {
    _computeRhoU_polym (dataHandler, cellData,cellIndex,rho,u);
  }

template<typename T, template<typename U> class Lattice>
  void 	 Dynamics<T,Lattice>::computeAllMomenta (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi) 
  {
  	_computeAllMomenta_polym (dataHandler, cellData,cellIndex,rho,u,pi);
  }

template<typename T, template<typename U> class Lattice>
  T		 Dynamics<T,Lattice>::computeEquilibrium (int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr) 
  {
    return _computeEquilibirum_poly (iPop,rho,u,uSqr);
  }


template<typename T, template<typename U> class Lattice>
  T  Dynamics<T,Lattice>::_computeEquilibirum_poly (int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr) 
  {
  	return computeEquilibriumStatic(iPop, rho, u, uSqr);
  }

template<typename T, template<typename U> class Lattice>
  T  Dynamics<T,Lattice>::computeEquilibriumStatic (int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr)
  {
    return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
  }

template<typename T, template<typename U> class Lattice>
  void Dynamics<T,Lattice>::dispatchCollisionCPU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData)
  {
  	_dispatchCollisionCPU_poly (dataHandler, cellData);
  }

template<typename T, template<typename U> class Lattice>
void  Dynamics<T,Lattice>::dispatchPostProcCPU (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz)
{
  _dispatchPostProcCPU_poly (dataHandler, cellData,nx,ny,nz);
}

#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice>
  void Dynamics<T,Lattice>::dispatchCollisionGPU (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
  {
  	_dispatchCollisionGPU_poly (gpuHandler, position, size);
  }

template<typename T, template<typename U> class Lattice>
  void  Dynamics<T,Lattice>::dispatchPostProcGPU (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
  {
  	_dispatchPostProcGPU_poly (gpuHandler, position, size);
  }
#endif

/// OLD IMPLEMENTATION
//template<typename T, template<typename U> class Lattice>
//T Dynamics<T,Lattice>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
//{
//  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
//}

template<typename T, template<typename U> class Lattice>
void Dynamics<T,Lattice>::iniEquilibrium(CellView<T,Lattice>& cell, T rho, const T u[Lattice<T>::d])
{
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
    cell[iPop] = computeEquilibrium(iPop, rho, u, uSqr);
  }
}

//template<typename T, template<typename U> class Lattice>
//void Dynamics<T,Lattice>::iniEquilibrium(T** cellData, size_t cellIndex, T const rho, T const u[Lattice<T>::d])
//{
//  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
//  for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
//    cellData[iPop][cellIndex] = computeEquilibrium(iPop, rho, u, uSqr);
//  }
//
//  cellData[Lattice<T>::rhoIndex][cellIndex] = rho;
//  for(int iDim =0; iDim<Lattice<T>::d; ++iDim)
//      cellData[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];
//}

template<typename T, template<typename U> class Lattice>
void Dynamics<T,Lattice>::setBoundaryIntersection(int iPop, T distance)
{ }

template<typename T, template<typename U> class Lattice>
bool Dynamics<T,Lattice>::getBoundaryIntersection(int iPop, T point[Lattice<T>::d])
{
  return 0;
}

template<typename T, template<typename U> class Lattice>
T Dynamics<T,Lattice>::getVelocityCoefficient(int iPop)
{
  return 0;
}

template<typename T, template<typename U> class Lattice>
bool Dynamics<T,Lattice>::isPostProcDynamics()
{
  return _isPostProcDynamics_poly();
}



////////////////////// Class BasicDynamics ////////////////////////
//
template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::BasicDynamics(Momenta momenta)
  : _momenta(momenta)
{ }


template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::BasicDynamics()
{ }



/// OLD IMPLEMENTATION
template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
T BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeRho(CellView<T,Lattice> const& cell) const
{
    return cell[Lattice<T>::rhoIndex];
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
T BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData)
{
    return cellData[Lattice<T>::rhoIndex];
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
T BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,size_t momentaIndex)
{
    return cellData[Lattice<T>::rhoIndex][cellIndex];
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeU (
  CellView<T,Lattice> const& cell,
  T u[Lattice<T>::d]) const
{
  for (int iD= 0; iD < Lattice<T>::d; ++iD)
  {
    u[iD] = cell[Lattice<T>::uIndex + iD];
  }
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeU (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T * const OPENLB_RESTRICT u) 
{
  for (int iD= 0; iD < Lattice<T>::d; ++iD)
  {
    u[iD] = cellData[Lattice<T>::uIndex + iD];
  }
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T * const OPENLB_RESTRICT u) 
{
  for (int iD= 0; iD < Lattice<T>::d; ++iD)
  {
    u[iD] = cellData[Lattice<T>::uIndex + iD][cellIndex];
  }
}


template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeJ (
  CellView<T,Lattice> const& cell,
  T j[Lattice<T>::d]) const
{
  _momenta.computeJ(cell, j);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeJ (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T * const OPENLB_RESTRICT j)
{
  MomentaType::computeJ(cellData, momentaData, j);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeJ (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T * const OPENLB_RESTRICT j)
{
  MomentaType::computeJ(cellData, cellIndex, momentaData,momentaIndex, j);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  _momenta.computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeStress (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  MomentaType::computeStress(cellData,momentaData, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeStress (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  MomentaType::computeStress(cellData,cellIndex, momentaData, momentaIndex, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = cell[Lattice<T>::rhoIndex];
  for (int iD= 0; iD < Lattice<T>::d; ++iD)
  {
    u[iD] = cell[Lattice<T>::uIndex + iD];
  }
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeRhoU (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T& rho, T * const OPENLB_RESTRICT u)
{
  rho = cellData[Lattice<T>::rhoIndex];
  for (int iD= 0; iD < Lattice<T>::d; ++iD)
  {
    u[iD] = cellData[Lattice<T>::uIndex + iD];
  }
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T& rho, T * const OPENLB_RESTRICT u)
{
  rho = cellData[Lattice<T>::rhoIndex][cellIndex];
  for (int iD= 0; iD < Lattice<T>::d; ++iD)
  {
    u[iD] = cellData[Lattice<T>::uIndex + iD][cellIndex];
  }
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  this->computeRhoU(cell, rho, u);
  this->computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeAllMomenta (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T& rho, T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  MomentaType::computeRhoU(cellData,momentaData, rho, u);
  MomentaType::computeStress(cellData,momentaData, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::computeAllMomenta (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T& rho, T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  MomentaType::computeRhoU(cellData,cellIndex, momentaData,momentaIndex, rho, u);
  MomentaType::computeStress(cellData, cellIndex,momentaData,momentaIndex, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  _momenta.defineRho(cell, rho);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineRho(T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData, T rho)
{
  MomentaType::defineRho(cellData,momentaData, rho);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineRho(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T rho)
{
  MomentaType::defineRho(cellData, cellIndex, momentaData, momentaIndex, rho);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{
  _momenta.defineU(cell, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineU (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  const T * const OPENLB_RESTRICT u)
{
  MomentaType::defineU(cellData, momentaData, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  const T * const OPENLB_RESTRICT u)
{
  MomentaType::defineU(cellData,cellIndex, momentaData,momentaIndex, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  _momenta.defineRhoU(cell, rho, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineRhoU (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u)
{
  MomentaType::defineRhoU(cellData,momentaData, rho, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineRhoU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u)
{
  MomentaType::defineRhoU(cellData,cellIndex, momentaData,momentaIndex, rho, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{
  _momenta.defineAllMomenta(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineAllMomenta (
  T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi )
{
	MomentaType::defineAllMomenta(cellData, momentaData, rho, u, pi);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::defineAllMomenta (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi )
{
	MomentaType::defineAllMomenta(cellData,cellIndex,  momentaData,momentaIndex, rho, u, pi);
}

/// implementation of poly define and compute

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho)
{
	defineRho (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex), rho);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u) 
{
	defineU (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex), u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u)
{
	defineRhoU(cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex),rho,u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi)
{
	defineAllMomenta (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex), rho, u, pi);
}
  
  // private pure virtual functions in order to overload the compute interface in derived classes:
template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  T 	BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_computeRho_polym(DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
{
	return computeRho (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex));
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void 	BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_computeU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT u) 
{
	computeU (cellData, cellIndex,  dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex), u);
} 

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void 	BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_computeJ_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j)
{
	computeJ (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex),j);
} 

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
 void 	BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_computeStress_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
{
	computeStress (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex),rho, u, pi );
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
  void 	BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_computeRhoU_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u)
{
	computeRhoU (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex), rho, u);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
 void 	BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_computeAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi)
{
	computeAllMomenta (cellData, cellIndex, dataHandler->getDynamicsData(), dataHandler->getMomentaIndex(cellIndex), rho, u, pi);
}

/// Implementation of dispatch functions
template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void  BasicDynamics<T,Lattice,Momenta,DynamicsType, PostProcessor>::_dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz)
{
  size_t const * const cellIDs = dataHandler->getCellIDs().data();
  unsigned int const numCells = dataHandler->getCellIDs().size();
  unsigned int const sizePostProcData = dataHandler->getNumberOfPostProcDataPoints();
  T * const collisionData = static_cast<DynamicsType*>(this)->getCollisionData();
  T * const * const momentaData = dataHandler->getDynamicsData();
  T * const * const postProcData = dataHandler->getPostProcData();

  for(size_t position=0; position < numCells; ++position)
  {
    size_t indexCalcHelper[Lattice<T>::d];
    indexCalcHelper[0] = nx;
    indexCalcHelper[1] = ny;

    if(Lattice<T>::d == 3)
      indexCalcHelper[2] = nz;

    PostProcessor::template process<DynamicsType>(cellData, momentaData, collisionData, postProcData,
                cellIDs[position], position, indexCalcHelper);

  }

}

#if ENABLE_CUDA
template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
{
  gpuHandler.template executePostProcessorKernel<DynamicsType>(this, position, size);
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_dispatchCollisionGPU_poly(GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
{
  gpuHandler.template executeBoundaryKernel<DynamicsType> (this, position, size);
}
#endif

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
void BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_dispatchCollisionCPU_poly(DynamicsDataHandler<T>* dataHandler,
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData)
{
  // std::cout << "dispatching polymorphic dynamics via static cast " << std::endl;
  size_t const * const cellIDs = dataHandler->getCellIDs().data();
  T * const collisionData = static_cast<DynamicsType*>(this)->getCollisionData();
  T * const * const momentaData = dataHandler->getDynamicsData();

  for(size_t position=0; position < dataHandler->getCellIDs().size(); ++position)
  {
    DynamicsType::collision(cellData, cellIDs[position],
        momentaData, position, collisionData);
    // std::cout << "running collision for ptr "<< this << " for cell " << cellIDs[position] << std::endl; 
  }
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType, class PostProcessor>
bool BasicDynamics<T,Lattice,Momenta,DynamicsType,PostProcessor>::_isPostProcDynamics_poly()
{
    return false;
}

////////////////////// Class BGKdynamics //////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
BGKdynamics<T,Lattice,Momenta,PostProcessor>::BGKdynamics (T omega, Momenta& momenta)
  : BasicDynamics<T,Lattice,Momenta, BGKdynamics<T, Lattice, Momenta,PostProcessor> ,PostProcessor>(momenta),
    _omega(omega)
{
    _collisionData[omegaIndex] = omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
BGKdynamics<T,Lattice,Momenta,PostProcessor>::BGKdynamics (T omega)
  : BasicDynamics<T,Lattice,Momenta, BGKdynamics<T, Lattice, Momenta,PostProcessor> ,PostProcessor>(Momenta{}),
    _omega(omega)
{
    _collisionData[omegaIndex] = omega;
}


template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
BGKdynamics<T,Lattice,Momenta,PostProcessor>* BGKdynamics<T,Lattice,Momenta,PostProcessor>::clone() const
{
  return new BGKdynamics<T,Lattice,Momenta,PostProcessor>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void BGKdynamics<T,Lattice,Momenta,PostProcessor>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell, rho, u, _omega);
  // statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void BGKdynamics<T,Lattice,Momenta,PostProcessor>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics )
{}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void BGKdynamics<T,Lattice,Momenta,PostProcessor>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics, Lattice<T> const & latticeDescriptor )
{}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void BGKdynamics<T,Lattice,Momenta,PostProcessor>::collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        size_t momentaIndex, T* const OPENLB_RESTRICT collisionData)
{
    T rho, u[Lattice<T>::d];
  Momenta::computeRhoU(cellData, cellIndex, momentaData, momentaIndex, rho, u);
  cellData[Lattice<T>::rhoIndex][cellIndex] = rho;
  for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
    cellData[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];

    
    T omega = collisionData[omegaIndex];
    lbHelpers<T, Lattice>::bgkCollision(cellData, cellIndex, rho, u, omega);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T BGKdynamics<T,Lattice,Momenta,PostProcessor>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void BGKdynamics<T,Lattice,Momenta,PostProcessor>::setOmega(T omega)
{
  _omega = omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
IniEquilibriumDynamics<T,Lattice,Momenta,PostProcessor>::IniEquilibriumDynamics()
{
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void IniEquilibriumDynamics<T,Lattice,Momenta,PostProcessor>::collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        size_t momentaIndex, T* const OPENLB_RESTRICT collisionData)
{
  T rho, u[Lattice<T>::d];
  rho = cellData[Lattice<T>::rhoIndex][cellIndex];
  for (unsigned int iDim = 0; iDim < Lattice<T>::d; ++iDim) {
    u[iDim] = cellData[Lattice<T>::uIndex+iDim][cellIndex];
  }
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  
  for(int iPop=0; iPop<Lattice<T>::q; iPop++) {
    cellData[iPop][cellIndex] = lbHelpers<T,Lattice>::equilibrium(Lattice<T>::opposite(iPop),rho,u,uSqr);
  }
}

template<typename T, template<typename U> class Lattice, class Momenta,class PostProcessor>
IniEquilibriumDynamics<T,Lattice,Momenta,PostProcessor>* IniEquilibriumDynamics<T,Lattice,Momenta,PostProcessor>::clone() const
{
  return new IniEquilibriumDynamics<T,Lattice,Momenta,PostProcessor>(*this);
}

////////////////////// Class EntropicMRTdynamics //////////////////////////

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::EntropicMRTdynamics (T omega, Momenta& momenta)
  : BasicDynamics<T,Lattice,Momenta, EntropicMRTdynamics<T, Lattice, Momenta,PostProcessor> ,PostProcessor>(momenta),
    _omega(omega), _beta(omega/2.0)
{
    _collisionData[omegaIndex] = omega;
    _collisionData[betaIndex] = omega/2.0;
    std::copy(momentMatrix, momentMatrix+19*19, &_collisionData[momentIndex]);
    std::copy(invMomentMatrix, invMomentMatrix+19*19, &_collisionData[invMomentIndex]);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::EntropicMRTdynamics (T omega)
  : BasicDynamics<T,Lattice,Momenta, EntropicMRTdynamics<T, Lattice, Momenta,PostProcessor> ,PostProcessor>(Momenta{}),
    _omega(omega), _beta(omega/2.0)
{
    _collisionData[omegaIndex] = omega;
    _collisionData[betaIndex] = omega/2.0;
    std::copy(momentMatrix, momentMatrix+19*19, &_collisionData[momentIndex]);
    std::copy(invMomentMatrix, invMomentMatrix+19*19, &_collisionData[invMomentIndex]);
}


template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>* EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::clone() const
{
  return new EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::setOmega(T omega)
{
  _omega = omega;
  _beta = omega/2.0;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::productEquilibrium(int iPop, T rho, T u[Lattice<T>::d]) 
{
  T fEq = (T)1.0;
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    T quant = sqrt(1+3*u[iD]*u[iD]);
    fEq *= (2-quant)*pow((2*u[iD]+quant)/(1-u[iD]), Lattice<T>::c(iPop, iD));
  }

  fEq *= rho*Lattice<T>::t(iPop);
  fEq -= Lattice<T>::t(iPop);

  return fEq;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::productEquilibriumQuant(int iD, T u[3]) 
{
  return sqrt(1+3*u[iD]*u[iD]);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::productEquilibriumFromQuant(int iPop, T rho, T u[Lattice<T>::d], T uQuant[Lattice<T>::d]) 
{
  T fEq = (T)1.0;
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    fEq *= (2-uQuant[iD])*pow((2*u[iD]+uQuant[iD])/(1-u[iD]), Lattice<T>::c(iPop, iD));
  }

  fEq *= rho*Lattice<T>::t(iPop);
  fEq -= Lattice<T>::t(iPop);

  return fEq;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        size_t momentaIndex, T* const OPENLB_RESTRICT collisionData)
{

  T rho, u[Lattice<T>::d];
  Momenta::computeRhoU(cellData, cellIndex, momentaData, momentaIndex, rho, u);

  cellData[Lattice<T>::rhoIndex][cellIndex] = rho;
  for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
      cellData[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];
      
  T uQuant[Lattice<T>::d];
  for(unsigned int iD=0; iD <Lattice<T>::d; ++iD) {
      uQuant[iD] = productEquilibriumQuant(iD, u);
  }

  T fEq[Lattice<T>::q];
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    fEq[iPop] = productEquilibriumFromQuant(iPop, rho, u, uQuant);
    if (isnan(fEq[iPop]))
      printf("equilibrium is nan\n");
  }
  
  T pi[6] = {0.0,0.0,0.0,0.0,0.0,0.0};
  T piEq[6] = {0.0,0.0,0.0,0.0,0.0,0.0};
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    for (int mom = 0; mom < 6; mom++) {
      pi[mom] += cellData[iPop][cellIndex] * collisionData[momentIndex + Lattice<T>::q*(mom+4)+iPop];
      piEq[mom] += fEq[iPop] * collisionData[momentIndex + Lattice<T>::q*(mom+4)+iPop];
    }
  }

  T piTrace = pi[0] + pi[1] + pi[2];
  T piEqTrace = piEq[0] + piEq[1] + piEq[2];

  for (int i = 0; i < 3; i++) {
    pi[i] -= piTrace/3.0;
    piEq[i] -= piEqTrace/3.0;
  }  

  T deltaSi[Lattice<T>::q];

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {

    T si = 0.0;
    T siEq = 0.0;
    for (int mom = 0; mom < 6; mom++) {
      si += pi[mom] * collisionData[invMomentIndex + Lattice<T>::q*(iPop) + mom + 4];
      siEq += piEq[mom] * collisionData[invMomentIndex + Lattice<T>::q*(iPop) + mom + 4];
    }
    deltaSi[iPop] = si - siEq;
    if (isnan(deltaSi[iPop]))
      printf("deltasi nan\n");

  }

  //calculate gamma higher order relaxation
  T innerProdDelSDelH = 0.0;
  T innerProdDelHDelH = 0.0;
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    T delHi = cellData[iPop][cellIndex] - fEq[iPop] - deltaSi[iPop];

    innerProdDelSDelH += deltaSi[iPop]*delHi/(fEq[iPop]+Lattice<T>::t(iPop));
    innerProdDelHDelH += delHi*delHi/(fEq[iPop]+Lattice<T>::t(iPop));
  }

  T beta = collisionData[betaIndex];
  // printf("beta: %f\n", beta);
  if (util::nearZero(innerProdDelHDelH, (T)1.0E-7))
    innerProdDelHDelH = (T)1.0E-7;

  T gamma = (T)1.0/beta - (2-(T)1.0/beta)*innerProdDelSDelH/innerProdDelHDelH;

  //do the actual collision
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {

    T delHi = cellData[iPop][cellIndex] - fEq[iPop] - deltaSi[iPop];
    cellData[iPop][cellIndex] -= beta*(2*deltaSi[iPop] + gamma*(delHi));
  }

  //swap pops to opposite
  T tmp[Lattice<T>::q/2+1];

  for (int i=1; i<=Lattice<T>::q/2; ++i)
  tmp[i] = cellData[i][cellIndex];

  for (int i=1; i<=Lattice<T>::q/2; ++i)
  cellData[i][cellIndex] = cellData[Lattice<T>::opposite(i)][cellIndex];

  for (int i=1; i<=Lattice<T>::q/2; ++i)
  cellData[Lattice<T>::opposite(i)][cellIndex] = tmp[i];
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
const T EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::momentMatrix[] = 
{
1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,0.0000000000,-1.0000000000,0.0000000000,0.0000000000,-1.0000000000,-1.0000000000,-1.0000000000,-1.0000000000,0.0000000000,0.0000000000,1.0000000000,0.0000000000,0.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-1.0000000000,0.0000000000,-1.0000000000,1.0000000000,0.0000000000,0.0000000000,-1.0000000000,-1.0000000000,0.0000000000,1.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,0.0000000000,1.0000000000,1.0000000000,0.0000000000,0.0000000000,0.0000000000,-1.0000000000,0.0000000000,0.0000000000,-1.0000000000,1.0000000000,-1.0000000000,1.0000000000,0.0000000000,0.0000000000,1.0000000000,0.0000000000,0.0000000000,1.0000000000,-1.0000000000,1.0000000000,-1.0000000000,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,0.6666666667,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,0.0000000000,-0.0000000000,-0.0000000000,0.0000000000,1.0000000000,-1.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,0.0000000000,0.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,0.0000000000,-0.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.0000000000,0.0000000000,-0.0000000000,1.0000000000,-1.0000000000,0.0000000000,-0.0000000000,0.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,1.0000000000,-1.0000000000,-0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,-0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,-0.6666666667,-0.6666666667,0.3333333333,0.3333333333,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,0.3333333333,0.3333333333,-0.6666666667,-0.6666666667,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,-0.6666666667,0.6666666667,0.0000000000,0.0000000000,0.3333333333,0.3333333333,0.0000000000,-0.3333333333,0.0000000000,0.6666666667,-0.6666666667,0.0000000000,0.0000000000,-0.3333333333,-0.3333333333,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.3333333333,-0.3333333333,0.0000000000,0.0000000000,-0.6666666667,-0.6666666667,0.0000000000,-0.3333333333,0.0000000000,-0.3333333333,0.3333333333,0.0000000000,0.0000000000,0.6666666667,0.6666666667,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,-0.6666666667,0.6666666667,0.3333333333,-0.3333333333,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,0.6666666667,-0.6666666667,-0.3333333333,0.3333333333,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,0.3333333333,-0.3333333333,-0.6666666667,0.6666666667,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,-0.3333333333,0.3333333333,0.6666666667,-0.6666666667,0.1111111111,-0.2222222222,-0.2222222222,0.1111111111,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.1111111111,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.1111111111,-0.2222222222,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,-0.2222222222,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,0.1111111111,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444
};

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
const T EntropicMRTdynamics<T,Lattice,Momenta,PostProcessor>::invMomentMatrix[] = 
{
0.3333333333,0.0000000000,0.0000000000,0.0000000000,-0.3333333333,-0.3333333333,-0.3333333333,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,1.0000000000,1.0000000000,1.0000000000,0.0555555556,-0.1666666667,-0.0000000000,-0.0000000000,0.1666666667,-0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.5000000000,0.5000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,-0.0000000000,0.0555555556,0.0000000000,-0.1666666667,0.0000000000,-0.1666666667,0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.5000000000,0.5000000000,0.0000000000,0.0000000000,-0.5000000000,0.0000000000,-0.5000000000,0.0555555556,0.0000000000,0.0000000000,-0.1666666667,-0.1666666667,-0.1666666667,0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.5000000000,0.5000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0277777778,-0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.2500000000,-0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,-0.0833333333,0.0000000000,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,-0.2500000000,0.0000000000,-0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,0.0000000000,-0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0277777778,0.0000000000,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.2500000000,0.0555555556,0.1666666667,-0.0000000000,-0.0000000000,0.1666666667,-0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,-0.0000000000,0.0555555556,0.0000000000,0.1666666667,0.0000000000,-0.1666666667,0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0000000000,0.0000000000,-0.5000000000,0.0000000000,-0.5000000000,0.0555555556,0.0000000000,0.0000000000,0.1666666667,-0.1666666667,-0.1666666667,0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0277777778,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,0.0833333333,0.0000000000,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0277777778,0.0000000000,0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000
};

////////////////////// Class EntropicSmagorinskyMRTdynamics //////////////////////////

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::EntropicSmagorinskyMRTdynamics (T omega, Momenta& momenta, T smagoConst)
  : BasicDynamics<T,Lattice,Momenta, EntropicSmagorinskyMRTdynamics<T, Lattice, Momenta,PostProcessor> ,PostProcessor>(momenta),
    _omega(omega), _beta(omega/2.0)
{
    _collisionData[omegaIndex] = omega;
    _collisionData[betaIndex] = omega/2.0;
    _collisionData[smagoIndex] = smagoConst;
    _collisionData[preFactorIndex] = 18*smagoConst*smagoConst;
    std::copy(momentMatrix, momentMatrix+19*19, &_collisionData[momentIndex]);
    std::copy(invMomentMatrix, invMomentMatrix+19*19, &_collisionData[invMomentIndex]);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::EntropicSmagorinskyMRTdynamics (T omega, T smagoConst)
  : BasicDynamics<T,Lattice,Momenta, EntropicSmagorinskyMRTdynamics<T, Lattice, Momenta,PostProcessor> ,PostProcessor>(Momenta{}),
    _omega(omega), _beta(omega/2.0)
{
    _collisionData[omegaIndex] = omega;
    _collisionData[betaIndex] = omega/2.0;
    _collisionData[smagoIndex] = smagoConst;
    _collisionData[preFactorIndex] = 18*smagoConst*smagoConst;
    std::copy(momentMatrix, momentMatrix+19*19, &_collisionData[momentIndex]);
    std::copy(invMomentMatrix, invMomentMatrix+19*19, &_collisionData[invMomentIndex]);
}


template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>* EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::clone() const
{
  return new EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::setOmega(T omega)
{
  _omega = omega;
  _beta = omega/2.0;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::computeEffectiveOmega(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
        T const omega, T const preFactor, T const rho, T const u[Lattice<T>::d])
{
  T pi[3][3] = {0};

    T uSqr = 0;

#pragma unroll
    for(int i=0; i<Lattice<T>::d; ++i)
      uSqr += u[i]*u[i];

    for(int iPop=0; iPop<Lattice<T>::q; ++iPop)
    {
      T fNeq = cellData[iPop][cellIndex] - lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
      for(unsigned int j=0; j<Lattice<T>::d; ++j)
          for(unsigned int k=0; k<Lattice<T>::d; ++k)
              pi[j][k] += Lattice<T>::c(iPop,k)*Lattice<T>::c(iPop,j)*fNeq;
    }

    T piSqr = 0;

    for(unsigned int i=0; i<Lattice<T>::d; ++i)
      for(unsigned int j=0; j<Lattice<T>::d; ++j)
          piSqr += pi[i][j]*pi[i][j];

    /// Molecular realaxation time
    T tau_mol = 1. /omega;

    T nuFluid = (tau_mol-0.5)/3;

    /// Turbulent realaxation time
    T tau_turb = 0.5*(sqrt(nuFluid*nuFluid + preFactor*sqrt(piSqr)) - nuFluid);

    /// Effective realaxation time
    T tau_eff = tau_mol+tau_turb;

    T omega_new= 1./tau_eff;
    return omega_new;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::productEquilibrium(int iPop, T rho, T u[Lattice<T>::d]) 
{
  T fEq = (T)1.0;
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    T quant = sqrt(1+3*u[iD]*u[iD]);
    fEq *= (2-quant)*pow((2*u[iD]+quant)/(1-u[iD]), Lattice<T>::c(iPop, iD));
  }

  fEq *= rho*Lattice<T>::t(iPop);
  fEq -= Lattice<T>::t(iPop);

  return fEq;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::productEquilibriumQuant(int iD, T u[3]) 
{
  return sqrt(1+3*u[iD]*u[iD]);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::productEquilibriumFromQuant(int iPop, T rho, T u[Lattice<T>::d], T uQuant[Lattice<T>::d]) 
{
  T fEq = (T)1.0;
  for (int iD = 0; iD < Lattice<T>::d; iD++) {
    fEq *= (2-uQuant[iD])*pow((2*u[iD]+uQuant[iD])/(1-u[iD]), Lattice<T>::c(iPop, iD));
  }

  fEq *= rho*Lattice<T>::t(iPop);
  fEq -= Lattice<T>::t(iPop);

  return fEq;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        size_t momentaIndex, T* const OPENLB_RESTRICT collisionData)
{

  T rho, u[Lattice<T>::d];
  Momenta::computeRhoU(cellData, cellIndex, momentaData, momentaIndex, rho, u);

  cellData[Lattice<T>::rhoIndex][cellIndex] = rho;
  for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
      cellData[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];

  T newOmega = computeEffectiveOmega(cellData, cellIndex, momentaData, momentaIndex, collisionData[omegaIndex], collisionData[preFactorIndex], rho, u);
      
  T uQuant[Lattice<T>::d];
  for(unsigned int iD=0; iD <Lattice<T>::d; ++iD) {
      uQuant[iD] = productEquilibriumQuant(iD, u);
  }

  T fEq[Lattice<T>::q];
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    fEq[iPop] = productEquilibriumFromQuant(iPop, rho, u, uQuant);
    if (isnan(fEq[iPop]))
      printf("equilibrium is nan\n");
  }
  
  T pi[6] = {0.0,0.0,0.0,0.0,0.0,0.0};
  T piEq[6] = {0.0,0.0,0.0,0.0,0.0,0.0};
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    for (int mom = 0; mom < 6; mom++) {
      pi[mom] += cellData[iPop][cellIndex] * collisionData[momentIndex + Lattice<T>::q*(mom+4)+iPop];
      piEq[mom] += fEq[iPop] * collisionData[momentIndex + Lattice<T>::q*(mom+4)+iPop];
    }
  }

  T piTrace = pi[0] + pi[1] + pi[2];
  T piEqTrace = piEq[0] + piEq[1] + piEq[2];

  for (int i = 0; i < 3; i++) {
    pi[i] -= piTrace/3.0;
    piEq[i] -= piEqTrace/3.0;
  }  

  T deltaSi[Lattice<T>::q];

  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {

    T si = 0.0;
    T siEq = 0.0;
    for (int mom = 0; mom < 6; mom++) {
      si += pi[mom] * collisionData[invMomentIndex + Lattice<T>::q*(iPop) + mom + 4];
      siEq += piEq[mom] * collisionData[invMomentIndex + Lattice<T>::q*(iPop) + mom + 4];
    }
    deltaSi[iPop] = si - siEq;
    if (isnan(deltaSi[iPop]))
      printf("deltasi nan\n");

  }

  //calculate gamma higher order relaxation
  T innerProdDelSDelH = 0.0;
  T innerProdDelHDelH = 0.0;
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
    T delHi = cellData[iPop][cellIndex] - fEq[iPop] - deltaSi[iPop];

    innerProdDelSDelH += deltaSi[iPop]*delHi/(fEq[iPop]+Lattice<T>::t(iPop));
    innerProdDelHDelH += delHi*delHi/(fEq[iPop]+Lattice<T>::t(iPop));
  }

  // T beta = collisionData[betaIndex];
  T beta = newOmega/2.0;
  // printf("beta: %f\n", beta);
  if (util::nearZero(innerProdDelHDelH, (T)1.0E-7))
    innerProdDelHDelH = (T)1.0E-7;

  T gamma = (T)1.0/beta - (2-(T)1.0/beta)*innerProdDelSDelH/innerProdDelHDelH;

  //do the actual collision
  for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {

    T delHi = cellData[iPop][cellIndex] - fEq[iPop] - deltaSi[iPop];
    cellData[iPop][cellIndex] -= beta*(2*deltaSi[iPop] + gamma*(delHi));
  }

  //swap pops to opposite
  T tmp[Lattice<T>::q/2+1];

  for (int i=1; i<=Lattice<T>::q/2; ++i)
  tmp[i] = cellData[i][cellIndex];

  for (int i=1; i<=Lattice<T>::q/2; ++i)
  cellData[i][cellIndex] = cellData[Lattice<T>::opposite(i)][cellIndex];

  for (int i=1; i<=Lattice<T>::q/2; ++i)
  cellData[Lattice<T>::opposite(i)][cellIndex] = tmp[i];
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
const T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::momentMatrix[] = 
{
1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,0.0000000000,-1.0000000000,0.0000000000,0.0000000000,-1.0000000000,-1.0000000000,-1.0000000000,-1.0000000000,0.0000000000,0.0000000000,1.0000000000,0.0000000000,0.0000000000,1.0000000000,1.0000000000,1.0000000000,1.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-1.0000000000,0.0000000000,-1.0000000000,1.0000000000,0.0000000000,0.0000000000,-1.0000000000,-1.0000000000,0.0000000000,1.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,0.0000000000,1.0000000000,1.0000000000,0.0000000000,0.0000000000,0.0000000000,-1.0000000000,0.0000000000,0.0000000000,-1.0000000000,1.0000000000,-1.0000000000,1.0000000000,0.0000000000,0.0000000000,1.0000000000,0.0000000000,0.0000000000,1.0000000000,-1.0000000000,1.0000000000,-1.0000000000,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,0.6666666667,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.6666666667,0.6666666667,0.0000000000,-0.0000000000,-0.0000000000,0.0000000000,1.0000000000,-1.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,0.0000000000,0.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,0.0000000000,-0.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.0000000000,0.0000000000,-0.0000000000,1.0000000000,-1.0000000000,0.0000000000,-0.0000000000,0.0000000000,-0.0000000000,-0.0000000000,-0.0000000000,1.0000000000,-1.0000000000,-0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,1.0000000000,-1.0000000000,0.0000000000,-0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,-0.6666666667,-0.6666666667,0.3333333333,0.3333333333,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,0.6666666667,0.6666666667,-0.3333333333,-0.3333333333,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,0.3333333333,0.3333333333,-0.6666666667,-0.6666666667,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,-0.3333333333,-0.3333333333,0.6666666667,0.6666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,-0.6666666667,0.6666666667,0.0000000000,0.0000000000,0.3333333333,0.3333333333,0.0000000000,-0.3333333333,0.0000000000,0.6666666667,-0.6666666667,0.0000000000,0.0000000000,-0.3333333333,-0.3333333333,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.3333333333,-0.3333333333,0.0000000000,0.0000000000,-0.6666666667,-0.6666666667,0.0000000000,-0.3333333333,0.0000000000,-0.3333333333,0.3333333333,0.0000000000,0.0000000000,0.6666666667,0.6666666667,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,-0.6666666667,0.6666666667,0.3333333333,-0.3333333333,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,0.6666666667,-0.6666666667,-0.3333333333,0.3333333333,0.0000000000,0.0000000000,0.0000000000,0.3333333333,0.0000000000,0.0000000000,0.3333333333,-0.3333333333,-0.6666666667,0.6666666667,0.0000000000,0.0000000000,-0.3333333333,0.0000000000,0.0000000000,-0.3333333333,0.3333333333,0.6666666667,-0.6666666667,0.1111111111,-0.2222222222,-0.2222222222,0.1111111111,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.1111111111,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.1111111111,-0.2222222222,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,-0.2222222222,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444,-0.2222222222,-0.2222222222,0.1111111111,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444,0.1111111111,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,-0.2222222222,0.4444444444,0.4444444444
};

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
const T EntropicSmagorinskyMRTdynamics<T,Lattice,Momenta,PostProcessor>::invMomentMatrix[] = 
{
0.3333333333,0.0000000000,0.0000000000,0.0000000000,-0.3333333333,-0.3333333333,-0.3333333333,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,1.0000000000,1.0000000000,1.0000000000,0.0555555556,-0.1666666667,-0.0000000000,-0.0000000000,0.1666666667,-0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.5000000000,0.5000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,-0.0000000000,0.0555555556,0.0000000000,-0.1666666667,0.0000000000,-0.1666666667,0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.5000000000,0.5000000000,0.0000000000,0.0000000000,-0.5000000000,0.0000000000,-0.5000000000,0.0555555556,0.0000000000,0.0000000000,-0.1666666667,-0.1666666667,-0.1666666667,0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.5000000000,0.5000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0277777778,-0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.2500000000,-0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,-0.0833333333,0.0000000000,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,-0.2500000000,0.0000000000,-0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,0.0000000000,-0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0277777778,0.0000000000,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.2500000000,0.0555555556,0.1666666667,-0.0000000000,-0.0000000000,0.1666666667,-0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,-0.0000000000,0.0555555556,0.0000000000,0.1666666667,0.0000000000,-0.1666666667,0.1666666667,-0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0000000000,0.0000000000,-0.5000000000,0.0000000000,-0.5000000000,0.0555555556,0.0000000000,0.0000000000,0.1666666667,-0.1666666667,-0.1666666667,0.1666666667,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0000000000,-0.5000000000,-0.5000000000,0.0277777778,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.0000000000,0.0277777778,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,0.0833333333,0.0000000000,-0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000,-0.0000000000,0.0277777778,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,0.2500000000,0.0000000000,0.0000000000,0.2500000000,0.0277777778,0.0000000000,0.0833333333,-0.0833333333,0.0000000000,0.0833333333,0.0833333333,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.0000000000,0.0000000000,0.2500000000,0.0000000000,-0.2500000000,0.0000000000,0.0000000000,0.2500000000
};

////////////////////// Class ConstRhoBGKdynamics //////////////////////////
template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::ConstRhoBGKdynamics (
  T omega, Momenta& momenta )
  : BasicDynamics<T,Lattice,Momenta, ConstRhoBGKdynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor>(momenta),
    _omega(omega)
{
       _collisionData[omegaIndex] = omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::ConstRhoBGKdynamics (
  T omega )
  : BasicDynamics<T,Lattice,Momenta, ConstRhoBGKdynamics<T, Lattice, Momenta, PostProcessor>, PostProcessor>(Momenta{}),
    _omega(omega)
{
	_collisionData[omegaIndex] = omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>* ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::clone()
const
{
  return new ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>(*this);
}




/// OLD IMPLEMNTATION
template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);

  T deltaRho = (T)1 - (statistics).getAverageRho();
  T ratioRho = (T)1 + deltaRho/rho;

  T uSqr = lbHelpers<T,Lattice>::constRhoBgkCollision (
      cell, rho, u, ratioRho, _omega );
  statistics.incrementStats(rho+deltaRho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics )
{
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics,
  Lattice<T> const & latticeDescriptor )
{
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::collision (
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
    T* const OPENLB_RESTRICT collisionData )
{
  // TODO this is a simple BGK collision step !!! needs to be changed
  T rho, u[Lattice<T>::d];
  Momenta::computeRhoU(cellData, cellIndex, momentaData, momentaIndex, rho, u);
  cellData[Lattice<T>::rhoIndex][cellIndex] = rho;
  for(unsigned int iDim=0; iDim<Lattice<T>::d; ++iDim)
      cellData[Lattice<T>::uIndex+iDim][cellIndex] = u[iDim];
  // T uSqr = lbHelpers<T, Lattice>::bgkCollision(cellData, cellIndex, rho, u, _omega);
  T omega = collisionData[omegaIndex];
  lbHelpers<T, Lattice>::bgkCollision(cellData, cellIndex, rho, u, omega);
  //T uSqr = lbHelpers<T, Lattice>::bgkCollision(cellData, cellIndex, rho, u, momenta);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::collision (
    T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT collisionData)
{
  // TODO this is a simple BGK collision step !!! needs to be changed
  Momenta::computeRhoU(cellData, momentaData, cellData[Lattice<T>::rhoIndex], &cellData[Lattice<T>::uIndex]);
  // T uSqr = lbHelpers<T, Lattice>::bgkCollision(cellData, cellIndex, rho, u, _omega);
  T uSqr = lbHelpers<T, Lattice>::bgkCollision(cellData, cellData[Lattice<T>::rhoIndex], &cellData[Lattice<T>::uIndex], collisionData[0]);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::computeEquilibriumStatic(int iPop, T rho, const T u[Lattice<T>::d], T uSqr)
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
T ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor>
void ConstRhoBGKdynamics<T,Lattice,Momenta,PostProcessor>::setOmega(T omega)
{
  _omega = omega;
}

////////////////////// Class IncBGKdynamics //////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
IncBGKdynamics<T,Lattice,Momenta>::IncBGKdynamics (
  T omega, Momenta& momenta )
  : BasicDynamics<T,Lattice,Momenta, IncBGKdynamics>(momenta), _omega(omega)
{ }

template<typename T, template<typename U> class Lattice, class Momenta>
IncBGKdynamics<T,Lattice,Momenta>* IncBGKdynamics<T,Lattice,Momenta>::clone() const
{
  return new IncBGKdynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void IncBGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho = this->_momenta.computeRho(cell);
  T p = rho / Lattice<T>::invCs2();
  T j[Lattice<T>::d];
  this->_momenta.computeJ(cell, j);
  T uSqr = lbHelpers<T,Lattice>::incBgkCollision(cell, p, j, _omega);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T IncBGKdynamics<T,Lattice,Momenta>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void IncBGKdynamics<T,Lattice,Momenta>::setOmega(T omega)
{
  _omega = omega;
}



////////////////////// Class RLBdynamics /////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
RLBdynamics<T,Lattice,Momenta>::RLBdynamics (
  T omega, Momenta& momenta )
  : BasicDynamics<T,Lattice,Momenta, RLBdynamics<T,Lattice,Momenta>>(momenta),
    _omega(omega)
{ }

template<typename T, template<typename U> class Lattice, class Momenta>
RLBdynamics<T,Lattice,Momenta>* RLBdynamics<T,Lattice,Momenta>::clone() const
{
  return new RLBdynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void RLBdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d], pi[util::TensorVal<Lattice<T> >::n];
  this->_momenta.computeAllMomenta(cell, rho, u, pi);
  T uSqr = rlbHelpers<T,Lattice>::rlbCollision(cell, rho, u, pi, _omega);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RLBdynamics<T,Lattice,Momenta>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void RLBdynamics<T,Lattice,Momenta>::setOmega(T omega)
{
  _omega = omega;
}

////////////////////// Class CombinedRLBdynamics /////////////////////////

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::CombinedRLBdynamics (
  T omega, Momenta& momenta)
  : BasicDynamics<T,Lattice,Momenta, CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>>(momenta),
    _boundaryDynamics(omega)
{ }

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>*
CombinedRLBdynamics<T,Lattice, Dynamics,Momenta>::clone() const
{
  return new CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
T CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::
computeEquilibrium(int iPop, T rho, const T * OPENLB_RESTRICT u, T uSqr) 
{
  return _boundaryDynamics.computeEquilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
void CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;

  T rho, u[L::d], pi[util::TensorVal<Lattice<T> >::n];
  this->_momenta.computeAllMomenta(cell,rho,u,pi);

  T uSqr = util::normSqr<T,L::d>(u);

  for (int iPop = 0; iPop < L::q; ++iPop) {
    cell[iPop] = computeEquilibrium(iPop,rho,u,uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }

  _boundaryDynamics.collide(cell, statistics);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
void CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics )
{
  typedef Lattice<T> L;

  T rho, u[L::d], pi[util::TensorVal<Lattice<T> >::n];
  assert(false);
  // this->_momenta.computeAllMomenta(data,rho,u,pi);

  T uSqr = util::normSqr<T,L::d>(u);

  for (int iPop = 0; iPop < L::q; ++iPop) {
    data.data[iPop][0] = computeEquilibrium(iPop,rho,u,uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }

  _boundaryDynamics.collide(data, statistics);
}

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
void CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics, Lattice<T> const & latticeDescriptor )
{
  //not implemented
}

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
T CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::getOmega() const
{
  return _boundaryDynamics.getOmega();
}

template<typename T, template<typename U> class Lattice, typename Dynamics, class Momenta>
void CombinedRLBdynamics<T,Lattice,Dynamics,Momenta>::setOmega(T omega)
{
  _boundaryDynamics.setOmega(omega);
}


////////////////////// Class ForcedBGKdynamics /////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
ForcedBGKdynamics<T,Lattice,Momenta>::ForcedBGKdynamics (
  T omega, Momenta& momenta )
  : BasicDynamics<T,Lattice,Momenta,ForcedBGKdynamics>(momenta), _omega(omega)
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size
  OLB_PRECONDITION( Lattice<T>::d == Lattice<T>::ExternalField::sizeOfForce );
}

template<typename T, template<typename U> class Lattice, class Momenta>
ForcedBGKdynamics<T,Lattice,Momenta>* ForcedBGKdynamics<T,Lattice,Momenta>::clone() const
{
  return new ForcedBGKdynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedBGKdynamics<T,Lattice,Momenta>::computeU (CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const
{
  T rho;
  this->_momenta.computeRhoU(cell, rho, u);
  for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
    u[iVel] += cell.getExternal(forceBeginsAt)[iVel] / (T)2.;
  }
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedBGKdynamics<T,Lattice,Momenta>::computeRhoU (CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d] ) const
{
  this->_momenta.computeRhoU(cell, rho, u);
  for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
    u[iVel] += cell.getExternal(forceBeginsAt)[iVel] / (T)2.;
  }
}


template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedBGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* force = cell.getExternal(forceBeginsAt);
  if ( !util::nearZero(force[0]) || !util::nearZero(force[1]) || !util::nearZero(force[2]) ) // TODO: unnecessary??
    for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
      u[iVel] += force[iVel] / (T)2.;
    }
//  if (force[2] != 0)
//  std::cout << force[0] << " " << force[1] << " " << force[2] << std::endl;
  T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell, rho, u, _omega);
  lbHelpers<T,Lattice>::addExternalForce(cell, u, _omega, rho);
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedBGKdynamics<T,Lattice,Momenta>::collide (
  CellDataArray<T,Lattice> cell,
  LatticeStatistics<T>& statistics )
{
 //not implemented
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedBGKdynamics<T,Lattice,Momenta>::collide (
  CellDataArray<T,Lattice> cell,
  LatticeStatistics<T>& statistics, Lattice<T> const & latticeDescriptor )
{
 //not implemented
}

template<typename T, template<typename U> class Lattice, class Momenta>
T ForcedBGKdynamics<T,Lattice,Momenta>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedBGKdynamics<T,Lattice,Momenta>::setOmega(T omega)
{
  _omega = omega;
}

////////////////////// Class ResettingForcedBGKdynamics /////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
ResettingForcedBGKdynamics<T,Lattice,Momenta>::ResettingForcedBGKdynamics (
  T omega, Momenta& momenta )
  : ForcedBGKdynamics<T,Lattice,Momenta>(omega, momenta)
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size
  OLB_PRECONDITION( Lattice<T>::d == Lattice<T>::ExternalField::sizeOfForce );
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ResettingForcedBGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* force = cell.getExternal(this->forceBeginsAt);
  if ( !util::nearZero(force[0]) || !util::nearZero(force[1]) || !util::nearZero(force[2]) ) // TODO: unnecessary??
    for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
      u[iVel] += force[iVel] / (T)2.;
    }
//  if (force[2] != 0)
//  std::cout << force[0] << " " << force[1] << " " << force[2] << std::endl;
  T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell, rho, u, this->_omega);
  lbHelpers<T,Lattice>::addExternalForce(cell, u, this->_omega, rho);
  statistics.incrementStats(rho, uSqr);

  force[0] = _frc[0];
  force[1] = _frc[1];
  force[2] = _frc[2];
//  force[0] = 0.;
//  force[1] = 0.;
//  force[2] = 0.;
}

////////////////////// Class ForcedShanChenBGKdynamics /////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
ForcedShanChenBGKdynamics<T,Lattice,Momenta>::ForcedShanChenBGKdynamics (
  T omega, Momenta& momenta )
  : ForcedBGKdynamics<T,Lattice,Momenta>(omega, momenta )
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size
  OLB_PRECONDITION( Lattice<T>::d == Lattice<T>::ExternalField::sizeOfForce );
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedShanChenBGKdynamics<T,Lattice,Momenta>::computeU (CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const
{
  T rho;
  this->_momenta.computeRhoU(cell, rho, u);
  for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
    u[iVel] += cell.getExternal(this->forceBeginsAt)[iVel] / (T)2.;
  }
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedShanChenBGKdynamics<T,Lattice,Momenta>::computeRhoU (CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d] ) const
{
  this->_momenta.computeRhoU(cell, rho, u);
  for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
    u[iVel] += cell.getExternal(this->forceBeginsAt)[iVel] / (T)2.;
  }
}

template<typename T, template<typename U> class Lattice, class Momenta>
void ForcedShanChenBGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T* force = cell.getExternal(this->forceBeginsAt);
  for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
    u[iVel] += force[iVel] /  this->getOmega();
  }
  T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell, rho, u, this->getOmega() );
  uSqr=0.;
  for (int iVel=0; iVel<Lattice<T>::d; ++iVel) {
    u[iVel] += force[iVel] / (T)2.;
    u[iVel] -= force[iVel] /  this->getOmega();
    uSqr    += pow(u[iVel],Lattice<T>::d);
  }
  statistics.incrementStats(rho, uSqr);
}

////////////////////// Class D3Q13dynamics /////////////////////////

/** \param omega relaxation parameter, related to the dynamic viscosity
 *  \param momenta a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
D3Q13dynamics<T,Lattice,Momenta>::D3Q13dynamics (
  T omega, Momenta& momenta )
  : BasicDynamics<T,Lattice,Momenta,D3Q13dynamics<T, Lattice, Momenta>>(momenta)
{
  setOmega(omega);
}

template<typename T, template<typename U> class Lattice, class Momenta>
D3Q13dynamics<T,Lattice,Momenta>* D3Q13dynamics<T,Lattice,Momenta>::clone() const
{
  return new D3Q13dynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T D3Q13dynamics<T,Lattice,Momenta>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  // To get at the equilibrium, execute collision with relaxation parameters 1
  CellView<T,Lattice> tmp;
  for (int pop=0; pop<Lattice<T>::q; ++pop) {
    tmp[pop] = Lattice<T>::t[pop];
  }
  d3q13Helpers<T>::collision(tmp, rho, u, (T)1, (T)1);
  return tmp[iPop];
}

template<typename T, template<typename U> class Lattice, class Momenta>
void D3Q13dynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  T rho, u[Lattice<T>::d];
  this->_momenta.computeRhoU(cell, rho, u);
  T uSqr = d3q13Helpers<T>::collision (
             cell, rho, u, lambda_nu, lambda_nu_prime );
  statistics.incrementStats(rho, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T D3Q13dynamics<T,Lattice,Momenta>::getOmega() const
{
  return (T)4 / ( (T)3/lambda_nu + (T)1/(T)2 );
}

template<typename T, template<typename U> class Lattice, class Momenta>
void D3Q13dynamics<T,Lattice,Momenta>::setOmega(T omega)
{
  lambda_nu = (T)3 / ( (T)4/omega - (T)1/(T)2 );
  lambda_nu_prime = (T)3 / ( (T)2/omega + (T)1/(T)2 );
}

////////////////////// Class Momenta //////////////////////////////
template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void Momenta<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = this->computeRho(cell);
  this->computeU(cell, u);

}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void Momenta<T,Lattice>::computeRhoU (
  CellDataArray<T,Lattice> const& data,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = this->computeRho(data);
  this->computeU(data, u);

}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void Momenta<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  this->computeRhoU(cell, rho, u);
  this->computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void Momenta<T,Lattice>::computeAllMomenta (
  CellDataArray<T,Lattice> const& data,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  this->computeRhoU(data, rho, u);
  this->computeStress(data, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
void Momenta<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  this->defineRho(cell, rho);
  this->defineU(cell, u);

}

////////////////////// Class BulkMomenta //////////////////////////

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BulkMomenta<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{
  return lbHelpers<T,Lattice>::computeRho(cell);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BulkMomenta<T,Lattice>::computeRho(CellDataArray<T,Lattice> const& data) const
{
  return lbHelpers<T,Lattice>::computeRho(data);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BulkMomenta<T,Lattice>::computeRho(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, size_t momentaIndex)
{
  return lbHelpers<T,Lattice>::computeRho(cellData);
}
template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BulkMomenta<T,Lattice>::computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex )
{
  return lbHelpers<T,Lattice>::computeRho(cellData,cellIndex);
}



template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeU(CellView<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  T dummyRho;
  lbHelpers<T,Lattice>::computeRhoU(cell, dummyRho, u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeU(CellDataArray<T,Lattice> const& data, T u[Lattice<T>::d]) const
{
  T dummyRho;
  lbHelpers<T,Lattice>::computeRhoU(data, dummyRho, u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeU(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT u) 
{
  T dummyRho;
  lbHelpers<T,Lattice>::computeRhoU(cellData, dummyRho, u);
}
template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,  T * const OPENLB_RESTRICT u) 
{
  T dummyRho;
  lbHelpers<T,Lattice>::computeRhoU(cellData, cellIndex, dummyRho, u);
}



template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::computeJ(CellView<T,Lattice> const& cell, T j[Lattice<T>::d]) const
{
  lbHelpers<T,Lattice>::computeJ(cell, j);
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::computeJ(const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T * const OPENLB_RESTRICT j) 
{
  lbHelpers<T,Lattice>::computeJ(cellData, j);
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::computeJ(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex, T * const OPENLB_RESTRICT j) 
{
  lbHelpers<T,Lattice>::computeJ(cellData,cellIndex, j);
}




template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  lbHelpers<T,Lattice>::computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeStress (
  CellDataArray<T,Lattice> const& data,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const 
{
  lbHelpers<T,Lattice>::computeStress(data, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeStress (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi)
{
  lbHelpers<T,Lattice>::computeStress(cellData, rho, u, pi);
}
template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeStress (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi)
{
  lbHelpers<T,Lattice>::computeStress(cellData,cellIndex, rho, u, pi);
}




template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d] ) const
{
  lbHelpers<T,Lattice>::computeRhoU(cell, rho,u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeRhoU (
  CellDataArray<T,Lattice> const& data,
  T& rho, T u[Lattice<T>::d] ) const
{
  lbHelpers<T,Lattice>::computeRhoU(data, rho,u);
}

template<typename T, template<typename U> class Lattice>
template<int width>
void BulkMomenta<T,Lattice>::computeRhoU (
  CellDataArray<T,Lattice> const& data,
  T* rho, T u[Lattice<T>::d][width]) const
{
  lbHelpers<T,Lattice>::template computeRhoU<width>(data, rho, u);
}

template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::computeRhoU (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T& rho, T u[Lattice<T>::d])
{
  lbHelpers<T,Lattice>::computeRhoU(cellData,cellIndex,rho,u);
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::computeRhoU (
  const T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, size_t const momentaIndex,
  T& rho, T * const OPENLB_RESTRICT u)
{
  lbHelpers<T,Lattice>::computeRhoU(cellData,rho,u);
}


template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  this->computeRhoU(cell, rho, u);
  this->computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeAllMomenta (
  CellDataArray<T,Lattice> const& data,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  this->computeRhoU(data, rho, u);
  this->computeStress(data, rho, u, pi);
}
template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeAllMomenta (
  const T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T& rho, T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
{
  computeRhoU(cellData, rho, u);
  computeStress(cellData, rho, u, pi);
}
template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BulkMomenta<T,Lattice>::computeAllMomenta (
  const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
  size_t momentaIndex, T& rho, T u[Lattice<T>::d],
  T * const OPENLB_RESTRICT pi )
{
  computeRhoU(cellData,cellIndex,momentaData,momentaIndex, rho, u);
  computeStress(cellData,cellIndex,momentaData, momentaIndex, rho, u, pi);
}



template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  T oldRho, u[Lattice<T>::d];
  computeRhoU(cell, oldRho, u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cell, fNeq, oldRho, u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineRho( T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData, T rho)
{
  T oldRho, u[Lattice<T>::d];
  computeRhoU(cellData, momentaData, oldRho, u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cellData, fNeq, oldRho, u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineRho( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,T rho)
{
  T oldRho, u[Lattice<T>::d];
  computeRhoU(cellData,cellIndex, momentaData,momentaIndex, oldRho, u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cellData,cellIndex, fNeq, oldRho, u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop][cellIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}




template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{
  T rho, oldU[Lattice<T>::d];
  computeRhoU(cell, rho, oldU);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cell, fNeq, rho, oldU);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }

}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineU (
  T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  const T * const OPENLB_RESTRICT u)
{
  T rho, oldU[Lattice<T>::d];
  computeRhoU(cellData, rho, oldU);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cellData, fNeq, rho, oldU);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }

}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  const T * const OPENLB_RESTRICT u)
{
  T rho, oldU[Lattice<T>::d];
  computeRhoU(cellData,cellIndex,momentaData, momentaIndex, rho, oldU);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cellData,cellIndex, fNeq, rho, oldU);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop][cellIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }

}




template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  T oldRho, oldU[Lattice<T>::d];
  computeRhoU(cell, oldRho, oldU);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cell, fNeq, oldRho, oldU);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineRhoU (
  T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u)
{
  T oldRho, oldU[Lattice<T>::d];
  computeRhoU(cellData, momentaData, oldRho, oldU);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cellData, fNeq, oldRho, oldU);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineRhoU (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u)
{
  T oldRho, oldU[Lattice<T>::d];
  computeRhoU(cellData,cellIndex, momentaData, momentaIndex, oldRho, oldU);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cellData,cellIndex, fNeq, oldRho, oldU);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop][cellIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}



template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineAllMomenta (
  T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT momentaData,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi)
{
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}
template<typename T, template<typename U> class Lattice>
void BulkMomenta<T,Lattice>::defineAllMomenta (
  T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T rho, const T * const OPENLB_RESTRICT u,
  const T * const OPENLB_RESTRICT pi)
{
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cellData[iPop][cellIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}

////////////////////// Class ExternalVelocityMomenta //////////////////////////

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T ExternalVelocityMomenta<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{
  return lbHelpers<T,Lattice>::computeRho(cell);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void ExternalVelocityMomenta<T,Lattice>::computeU(CellView<T,Lattice> const& cell, T u[Lattice<T>::d]) const
{
  T const* uExt = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = uExt[iD];
  }
}

template<typename T, template<typename U> class Lattice>
void ExternalVelocityMomenta<T,Lattice>::computeJ(CellView<T,Lattice> const& cell, T j[Lattice<T>::d]) const
{
  T rho = computeRho(cell);
  T const* uExt = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = uExt[iD]*rho;
  }
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void ExternalVelocityMomenta<T,Lattice>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  lbHelpers<T,Lattice>::computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void ExternalVelocityMomenta<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d] ) const
{
  rho = computeRho(cell);
  computeU(cell,u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void ExternalVelocityMomenta<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(cell, rho,u);
  computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
void ExternalVelocityMomenta<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  T oldRho, u[Lattice<T>::d];
  computeRhoU(cell, oldRho, u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  T fNeq[Lattice<T>::q];
  lbHelpers<T,Lattice>::computeFneq(cell, fNeq, oldRho, u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 fNeq[iPop];
  }
}

template<typename T, template<typename U> class Lattice>
void ExternalVelocityMomenta<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{
  T* uExt = cell.getExternal(Lattice<T>::ExternalField::velocityBeginsAt);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    uExt[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
void ExternalVelocityMomenta<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cell, rho);
  defineU(cell, u);
}

template<typename T, template<typename U> class Lattice>
void ExternalVelocityMomenta<T,Lattice>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{
  defineU(cell, u);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr) +
                 firstOrderLbHelpers<T,Lattice>::fromPiToFneq(iPop, pi);
  }
}
////////////////////// Class BounceBack ///////////////////////////

template<typename T, template<typename U> class Lattice>
BounceBack<T,Lattice>::BounceBack()
{
  _rhoFixed=false;
}

template<typename T, template<typename U> class Lattice>
BounceBack<T,Lattice>::BounceBack(T rho)
  :_rho(rho)
{
  _rhoFixed=true;
}

template<typename T, template<typename U> class Lattice>
BounceBack<T,Lattice>* BounceBack<T,Lattice>::clone() const
{
  return new BounceBack<T,Lattice>();
}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  for (int iPop=1; iPop <= Lattice<T>::q/2; ++iPop) {
    swap(cell[iPop], cell[iPop+Lattice<T>::q/2]);
  }
}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics )
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::collide (
  CellDataArray<T,Lattice> data,
  LatticeStatistics<T>& statistics, Lattice<T> const & latticeDescriptor )
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
        size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
        size_t momentaIndex, T* const OPENLB_RESTRICT collisionData)
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_defineRhoU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u)
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_defineRho_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho)
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_defineU_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT u)
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_defineAllMomenta_polym (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, const T * const OPENLB_RESTRICT pi)
{}


template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_dispatchCollisionCPU_poly(DynamicsDataHandler<T>* dataHandler,
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData)
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz)
{}

#ifdef ENABLE_CUDA
template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_dispatchCollisionGPU_poly(GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
{}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::_dispatchPostProcGPU_poly(GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
{}
#endif


template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BounceBack<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{

  if (_rhoFixed) {
    return _rho;
  }
  return lbHelpers<T,Lattice>::computeRho(cell);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBack<T,Lattice>::computeU (
  CellView<T,Lattice> const& cell,
  T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::computeJ (
  CellView<T,Lattice> const& cell,
  T j[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BounceBack<T,Lattice>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  for (int iPi=0; iPi<util::TensorVal<Lattice<T> >::n; ++iPi) {
    pi[iPi] = T();//std::numeric_limits<T>::signaling_NaN();
  }
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBack<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = computeRho(cell);
  computeU(cell, u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBack<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(cell, rho, u);
  computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{ }

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{ }

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{ }

// template<typename T, template<typename U> class Lattice>
// void BounceBack<T,Lattice>::defineRhoU (
    // T** cellData, size_t cellIndex, T const rho, T const u[Lattice<T>::d])
// { }

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{ }

template<typename T, template<typename U> class Lattice>
T BounceBack<T,Lattice>::getOmega() const
{
  return T();//std::numeric_limits<T>::signaling_NaN();
}

template<typename T, template<typename U> class Lattice>
void BounceBack<T,Lattice>::setOmega(T omega)
{ }


////////////////////// Class BounceBackVelocity ///////////////////////////

template<typename T, template<typename U> class Lattice>
BounceBackVelocity<T,Lattice>::BounceBackVelocity(const T u[Lattice<T>::d])
{
  _rhoFixed=false;
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
BounceBackVelocity<T,Lattice>::BounceBackVelocity(const T rho, const T u[Lattice<T>::d])
  :_rho(rho)
{
  _rhoFixed=true;
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
BounceBackVelocity<T,Lattice>* BounceBackVelocity<T,Lattice>::clone() const
{
  return new BounceBackVelocity<T,Lattice>(_u);
}

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  for (int iPop=1; iPop <= Lattice<T>::q/2; ++iPop) {
    swap(cell[iPop], cell[iPop+Lattice<T>::q/2]);
  }
  for (int iPop=1; iPop < Lattice<T>::q; ++iPop) {
    for (int iD=0; iD<Lattice<T>::d; ++iD) {
      cell[iPop] += computeRho(cell)*_u[iD]*Lattice<T>::c(iPop)[iD]*Lattice<T>::t[iPop]*2*Lattice<T>::invCs2();
    }
  }
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BounceBackVelocity<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{
  if (_rhoFixed) {
    return _rho;
  }
  return lbHelpers<T,Lattice>::computeRho(cell);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBackVelocity<T,Lattice>::computeU (
  CellView<T,Lattice> const& cell,
  T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = _u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::computeJ (
  CellView<T,Lattice> const& cell,
  T j[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = computeRho(cell)*_u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BounceBackVelocity<T,Lattice>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  for (int iPi=0; iPi<util::TensorVal<Lattice<T> >::n; ++iPi) {
    pi[iPi] = T();//std::numeric_limits<T>::signaling_NaN();
  }
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBackVelocity<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = computeRho(cell);
  computeU(cell, u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBackVelocity<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(cell, rho, u);
  computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{ }

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    _u[iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cell,rho);
  defineU(cell,u);
}

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{ }

template<typename T, template<typename U> class Lattice>
T BounceBackVelocity<T,Lattice>::getOmega() const
{
  return T();//std::numeric_limits<T>::signaling_NaN();
}

template<typename T, template<typename U> class Lattice>
void BounceBackVelocity<T,Lattice>::setOmega(T omega)
{ }

////////////////////// Class BounceBackAnti ///////////////////////////

template<typename T, template<typename U> class Lattice>
BounceBackAnti<T,Lattice>::BounceBackAnti()
{
  _rhoFixed = false;
  _rho = T(1);
}

template<typename T, template<typename U> class Lattice>
BounceBackAnti<T,Lattice>::BounceBackAnti(const T rho)
  :_rho(rho)
{
  _rhoFixed = true;
}

template<typename T, template<typename U> class Lattice>
BounceBackAnti<T,Lattice>* BounceBackAnti<T,Lattice>::clone() const
{
  return new BounceBackAnti<T,Lattice>(_rho);
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  /*
    for (int iPop=1; iPop <= Lattice<T>::q/2; ++iPop) {
      swap(cell[iPop], cell[iPop+Lattice<T>::q/2]);
    }
    for (int iPop=1; iPop < Lattice<T>::q; ++iPop) {
      if (Lattice<T>::c(iPop)[0] == -1)
        cell[iPop] = -cell[Lattice<T>::opposite(iPop]] + (computeRho(cell) - T(1))*Lattice<T>::t[iPop)*2;
    }
  */
  //T rho, u[Lattice<T>::d];
  //computeRhoU(cell, rho, u);
  //T uSqr = lbHelpers<T,Lattice>::bgkCollision(cell, rho, u, 1.78571);
  //statistics.incrementStats(rho, uSqr);

}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T BounceBackAnti<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{

  if (_rhoFixed) {
    return _rho;
  }
  return lbHelpers<T,Lattice>::computeRho(cell);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBackAnti<T,Lattice>::computeU (
  CellView<T,Lattice> const& cell,
  T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::computeJ (
  CellView<T,Lattice> const& cell,
  T j[Lattice<T>::d]) const
{
  computeU(cell, j);
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD]*=computeRho(cell);
  }
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void BounceBackAnti<T,Lattice>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  for (int iPi=0; iPi<util::TensorVal<Lattice<T> >::n; ++iPi) {
    pi[iPi] = T();//std::numeric_limits<T>::signaling_NaN();
  }
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBackAnti<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = computeRho(cell);
  computeU(cell, u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void BounceBackAnti<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(cell, rho, u);
  computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  _rho = rho;
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{
  defineRho(cell,rho);
  defineU(cell,u);
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{ }

template<typename T, template<typename U> class Lattice>
T BounceBackAnti<T,Lattice>::getOmega() const
{
  return T();//std::numeric_limits<T>::signaling_NaN();
}

template<typename T, template<typename U> class Lattice>
void BounceBackAnti<T,Lattice>::setOmega(T omega)
{ }


////////////////////// Class partialBounceBack ///////////////////////////

template<typename T, template<typename U> class Lattice>
PartialBounceBack<T,Lattice>::PartialBounceBack(T rf) : _rf(rf)
{
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T PartialBounceBack<T, Lattice>::computeEquilibrium ( int iPop, T rho, const T u[Lattice<T>::d], T uSqr ) const
{
  return lbHelpers<T, Lattice>::equilibriumFirstOrder( iPop, rho, u );
}

template<typename T, template<typename U> class Lattice>
void PartialBounceBack<T,Lattice>::collide( CellView<T,Lattice>& cell, LatticeStatistics<T>& statistics )
{
  for (int iPop=1; iPop <= Lattice<T>::q/2; ++iPop) {
    swap(cell[iPop], cell[iPop+Lattice<T>::q/2]);
  }
  for (int iPop=1; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = (_rf -1) * (cell[iPop] + Lattice<T>::t[iPop]) - Lattice<T>::t[iPop];
  }
}



////////////////////// Class NoDynamics ///////////////////////////

template<typename T, template<typename U> class Lattice>
NoDynamics<T,Lattice>::NoDynamics(T rho) :_rho(rho)
{
}

template<typename T, template<typename U> class Lattice>
NoDynamics<T,Lattice>* NoDynamics<T,Lattice>::clone() const
{
  return new NoDynamics<T,Lattice>(this->_rho);
}

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{ }

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::collide (
  CellDataArray<T,Lattice> cell,
  LatticeStatistics<T>& statistics )
{ }

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::collision (
  T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
  T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T* const OPENLB_RESTRICT collisionData )
{
  size_t indices[3];
  util::getCellIndices3D(cellIndex,6,6,indices);
  std::cout << "NoDynamics Collide for index " << cellIndex << " " << indices[0] << " " << indices[1] << " " << indices[2] << std::endl;
  for (size_t iPop = 1; iPop <= Lattice<T>::q / 2; ++iPop)
  {
    size_t opposite = Lattice<T>::opposite(iPop);
    T tmp = cellData[opposite][cellIndex];
    cellData[opposite][cellIndex] = cellData[iPop][cellIndex];
    cellData[iPop][cellIndex] = tmp;
  }
}

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::collision (
  T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT collisionData)
{
  for (size_t iPop = 1; iPop < Lattice<T>::q; ++iPop)
  {
    size_t opposite = Lattice<T>::opposite(iPop);
    T tmp = cellData[opposite];
    cellData[opposite] = cellData[iPop];
    cellData[iPop] = tmp;
  }
}

template<typename T, template<typename U> class Lattice>
T NoDynamics<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{
  return _rho;
  // return 7.0;
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void NoDynamics<T,Lattice>::computeU (
  CellView<T,Lattice> const& cell,
  T u[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    u[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::computeJ (
  CellView<T,Lattice> const& cell,
  T j[Lattice<T>::d]) const
{
  for (int iD=0; iD<Lattice<T>::d; ++iD) {
    j[iD] = T();
  }
}

template<typename T, template<typename U> class Lattice>
  OPENLB_HOST_DEVICE
void NoDynamics<T,Lattice>::computeStress (
  CellView<T,Lattice> const& cell,
  T rho, const T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  for (int iPi=0; iPi<util::TensorVal<Lattice<T> >::n; ++iPi) {
    pi[iPi] = T();//std::numeric_limits<T>::signaling_NaN();
  }
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void NoDynamics<T,Lattice>::computeRhoU (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d]) const
{
  rho = computeRho(cell);
  computeU(cell, u);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void NoDynamics<T,Lattice>::computeAllMomenta (
  CellView<T,Lattice> const& cell,
  T& rho, T u[Lattice<T>::d],
  T pi[util::TensorVal<Lattice<T> >::n] ) const
{
  computeRhoU(cell, rho, u);
  computeStress(cell, rho, u, pi);
}

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{ }

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{ }

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::defineRhoU (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d])
{ }

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::defineAllMomenta (
  CellView<T,Lattice>& cell,
  T rho, const T u[Lattice<T>::d],
  const T pi[util::TensorVal<Lattice<T> >::n] )
{ }

template<typename T, template<typename U> class Lattice>
T NoDynamics<T,Lattice>::getOmega() const
{
  return T();//std::numeric_limits<T>::signaling_NaN();
}

template<typename T, template<typename U> class Lattice>
void NoDynamics<T,Lattice>::setOmega(T omega)
{ }

////////////////////// Class GhostDynamics ///////////////////////////

template<typename T, template<typename U> class Lattice>
GhostDynamics<T,Lattice>::GhostDynamics(T rho) :_rho(rho)
{
}

template<typename T, template<typename U> class Lattice>
GhostDynamics<T,Lattice>* GhostDynamics<T,Lattice>::clone() const
{
  return new GhostDynamics<T,Lattice>(this->_rho);
}

template<typename T, template<typename U> class Lattice>
void GhostDynamics<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{ }

template<typename T, template<typename U> class Lattice>
void GhostDynamics<T,Lattice>::collide (
  CellDataArray<T,Lattice> cell,
  LatticeStatistics<T>& statistics )
{ }

template<typename T, template<typename U> class Lattice>
void GhostDynamics<T,Lattice>::collide (
  CellDataArray<T,Lattice> cell,
  LatticeStatistics<T>& statistics, Lattice<T> const & latticeDescriptor )
{ }

template<typename T, template<typename U> class Lattice>
void GhostDynamics<T,Lattice>::collision (
  T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
  T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData, size_t momentaIndex,
  T* const OPENLB_RESTRICT collisionData )
{
  // for (size_t iPop = 1; iPop <= Lattice<T>::q / 2; ++iPop)
  // {
  //   size_t opposite = Lattice<T>::opposite(iPop);
  //   T tmp = cellData[opposite][cellIndex];
  //   cellData[opposite][cellIndex] = cellData[iPop][cellIndex];
  //   cellData[iPop][cellIndex] = tmp;
  // }
}

template<typename T, template<typename U> class Lattice>
void GhostDynamics<T,Lattice>::collision (
  T * const OPENLB_RESTRICT cellData,const T * const OPENLB_RESTRICT momentaData, const T * const OPENLB_RESTRICT collisionData)
{
}

// template<typename T, template<typename U> class Lattice>
// T GhostDynamics<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
// {
  // return _rho;
// }

// template<typename T, template<typename U> class Lattice>
// OPENLB_HOST_DEVICE
// void GhostDynamics<T,Lattice>::computeU (
  // CellView<T,Lattice> const& cell,
  // T u[Lattice<T>::d]) const
// {
  // for (int iD=0; iD<Lattice<T>::d; ++iD) {
    // u[iD] = T();
  // }
// }

// template<typename T, template<typename U> class Lattice>
// void GhostDynamics<T,Lattice>::computeJ (
  // CellView<T,Lattice> const& cell,
  // T j[Lattice<T>::d]) const
// {
  // for (int iD=0; iD<Lattice<T>::d; ++iD) {
    // j[iD] = T();
  // }
// }

// template<typename T, template<typename U> class Lattice>
  // OPENLB_HOST_DEVICE
// void GhostDynamics<T,Lattice>::computeStress (
  // CellView<T,Lattice> const& cell,
  // T rho, const T u[Lattice<T>::d],
  // T pi[util::TensorVal<Lattice<T> >::n] ) const
// {
  // for (int iPi=0; iPi<util::TensorVal<Lattice<T> >::n; ++iPi) {
    // pi[iPi] = T();//std::numeric_limits<T>::signaling_NaN();
  // }
// }

// template<typename T, template<typename U> class Lattice>
// OPENLB_HOST_DEVICE
// void GhostDynamics<T,Lattice>::computeRhoU (
  // CellView<T,Lattice> const& cell,
  // T& rho, T u[Lattice<T>::d]) const
// {
  // rho = computeRho(cell);
  // computeU(cell, u);
// }

// template<typename T, template<typename U> class Lattice>
// OPENLB_HOST_DEVICE
// void GhostDynamics<T,Lattice>::computeAllMomenta (
  // CellView<T,Lattice> const& cell,
  // T& rho, T u[Lattice<T>::d],
  // T pi[util::TensorVal<Lattice<T> >::n] ) const
// {
  // computeRhoU(cell, rho, u);
  // computeStress(cell, rho, u, pi);
// }

// template<typename T, template<typename U> class Lattice>
// void GhostDynamics<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
// { }

// template<typename T, template<typename U> class Lattice>
// void GhostDynamics<T,Lattice>::defineU (
  // CellView<T,Lattice>& cell,
  // const T u[Lattice<T>::d])
// { }

// template<typename T, template<typename U> class Lattice>
// void GhostDynamics<T,Lattice>::defineRhoU (
  // CellView<T,Lattice>& cell,
  // T rho, const T u[Lattice<T>::d])
// { }

// template<typename T, template<typename U> class Lattice>
// void GhostDynamics<T,Lattice>::defineAllMomenta (
  // CellView<T,Lattice>& cell,
  // T rho, const T u[Lattice<T>::d],
  // const T pi[util::TensorVal<Lattice<T> >::n] )
// { }

template<typename T, template<typename U> class Lattice>
T GhostDynamics<T,Lattice>::getOmega() const
{
  return T();//std::numeric_limits<T>::signaling_NaN();
}

template<typename T, template<typename U> class Lattice>
void GhostDynamics<T,Lattice>::setOmega(T omega)
{ }

////////////////////// Class PeriodicDynamics ///////////////////////////

/// Implementation of dispatch functions
template<typename T, template<typename U> class Lattice,class PostProcessor>
void PostProcessingDynamics<T,Lattice,PostProcessor>::_dispatchPostProcCPU_poly (DynamicsDataHandler<T>* dataHandler, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t nx, size_t ny, size_t nz)
{
  size_t const * const cellIDs = dataHandler->getCellIDs().data();
  unsigned int const numCells = dataHandler->getCellIDs().size();
  unsigned int const sizePostProcData = dataHandler->getNumberOfPostProcDataPoints();
  T * const collisionData = static_cast<PostProcessingDynamics<T,Lattice,PostProcessor>*>(this)->getCollisionData();
  T * const * const momentaData = dataHandler->getDynamicsData();
  T * const * const postProcData = dataHandler->getPostProcData();

  for(size_t position=0; position < numCells; ++position)
  {
    size_t indexCalcHelper[Lattice<T>::d];
    indexCalcHelper[0] = nx;
    indexCalcHelper[1] = ny;

    if(Lattice<T>::d == 3)
      indexCalcHelper[2] = nz;

    PostProcessor::template process<PostProcessingDynamics<T,Lattice,PostProcessor>>(cellData, momentaData, collisionData,
            postProcData, cellIDs[position], position, indexCalcHelper);
  }

}

#if ENABLE_CUDA
template<typename T, template<typename U> class Lattice,class PostProcessor>
void PostProcessingDynamics<T,Lattice,PostProcessor>::_dispatchPostProcGPU_poly (GPUHandler<T, Lattice>& gpuHandler, size_t position, size_t size)
{
  gpuHandler.template executePostProcessorKernel<PostProcessingDynamics<T,Lattice,PostProcessor>>(this, position, size);
}
#endif

////////////////////// Class offDynamics ///////////////////////////

template<typename T, template<typename U> class Lattice>
OffDynamics<T,Lattice>::OffDynamics(const T _location[Lattice<T>::d])
{
  typedef Lattice<T> L;
  for (int iD = 0; iD < L::d; iD++) {
    location[iD] = _location[iD];
  }
  for (int iPop = 0; iPop < L::q; iPop++) {
    distances[iPop] = -1;
    velocityCoefficient[iPop] = 0;
    for (int iD = 0; iD < L::d; iD++) {
      boundaryIntersection[iPop][iD] = _location[iD];
      _u[iPop][iD] = T();
    }
  }
  _rho=T(1);
}

template<typename T, template<typename U> class Lattice>
OffDynamics<T,Lattice>::OffDynamics(const T _location[Lattice<T>::d], T _distances[Lattice<T>::q])
{
  typedef Lattice<T> L;
  for (int iD = 0; iD < L::d; iD++) {
    location[iD] = _location[iD];
  }
  for (int iPop = 0; iPop < L::q; iPop++) {
    distances[iPop] = _distances[iPop];
    velocityCoefficient[iPop] = 0;
    const int* c = L::c(iPop);
    for (int iD = 0; iD < L::d; iD++) {
      boundaryIntersection[iPop][iD] = _location[iD] - _distances[iPop]*c[iD];
      _u[iPop][iD] = T();
    }
  }
  _rho=T(1);
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T OffDynamics<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{
  /*typedef Lattice<T> L;
  T rhoTmp = T();
  T counter = T();
  int counter2 = int();
  for (int iPop = 0; iPop < L::q; iPop++) {
    if (distances[iPop] != -1) {
      rhoTmp += (cell[iPop] + L::t[iPop])*L::t[iPop];
      counter += L::t[iPop];
      counter2++;
    }
  }
  //if (rhoTmp/counter + 1<0.1999) std::cout << rhoTmp/counter2 + 1 <<std::endl;
  //if (rhoTmp/counter + 1>1.001) std::cout << rhoTmp/counter2 + 1 <<std::endl;
  return rhoTmp/counter/counter;*/
  return _rho;
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
void OffDynamics<T,Lattice>::computeU(CellView<T,Lattice> const& cell, T u[Lattice<T>::d] ) const
{
  typedef Lattice<T> L;
  for (int iD = 0; iD < L::d; iD++) {
    u[iD] = T();
  }
  int counter = 0;
  for (int iPop = 0; iPop < L::q; iPop++) {
    if ( !util::nearZero(distances[iPop]+1) ) {
      for (int iD = 0; iD < L::d; iD++) {
        u[iD] += _u[iPop][iD];
      }
      counter++;
    }
  }
  if (counter!=0) {
    for (int iD = 0; iD < L::d; iD++) {
      u[iD] /= counter;
    }
  }
  return;
}

template<typename T, template<typename U> class Lattice>
void OffDynamics<T,Lattice>::setBoundaryIntersection(int iPop, T distance)
{
  /// direction points from the fluid node into the solid domain
  /// distance is the distance from the fluid node to the solid wall
  typedef Lattice<T> L;
  distances[iPop] = distance;
  const int* c = L::c(iPop);
  for (int iD = 0; iD < L::d; iD++) {
    boundaryIntersection[iPop][iD] = location[iD] - distance*c[iD];
  }
}

template<typename T, template<typename U> class Lattice>
bool OffDynamics<T,Lattice>::getBoundaryIntersection(int iPop, T intersection[Lattice<T>::d])
{
  typedef Lattice<T> L;
  if ( !util::nearZero(distances[iPop]+1) ) {
    for (int iD = 0; iD < L::d; iD++) {
      intersection[iD] = boundaryIntersection[iPop][iD];
    }
    return true;
  }
  return false;
}

template<typename T, template<typename U> class Lattice>
void OffDynamics<T,Lattice>::defineRho(CellView<T,Lattice>& cell, T rho)
{
  _rho=rho;
}

template<typename T, template<typename U> class Lattice>
void OffDynamics<T,Lattice>::defineRho(int iPop, T rho)
{
  _rho=rho;
}

template<typename T, template<typename U> class Lattice>
void OffDynamics<T,Lattice>::defineU (
  CellView<T,Lattice>& cell,
  const T u[Lattice<T>::d])
{
  defineU(u);
}

template<typename T, template<typename U> class Lattice>
void OffDynamics<T,Lattice>::defineU(const T u[Lattice<T>::d])
{
  typedef Lattice<T> L;
  for (int iPop = 0; iPop < L::q; iPop++) {
    if ( !util::nearZero(distances[iPop]+1) ) {
      defineU(iPop, u);
    }
  }
}

/// Bouzidi velocity boundary condition formulas for the Coefficients:
/** 2*     invCs2*weight*(c,u)  for dist < 1/2
 *  1/dist*invCs2*weight*(c,u)  for dist >= 1/2
 */

template<typename T, template<typename U> class Lattice>
void OffDynamics<T,Lattice>::defineU(
  int iPop, const T u[Lattice<T>::d])
{
  OLB_PRECONDITION(distances[iPop] != -1)
  typedef Lattice<T> L;
  const int* c = L::c(iPop);
  velocityCoefficient[iPop] = 0;
  // scalar product of c(iPop) and u
  for (int sum = 0; sum < L::d; sum++) { // +/- problem because of first stream than postprocess
    velocityCoefficient[iPop] -= c[sum]*u[sum];
  }
  // compute summand for boundary condition
  velocityCoefficient[iPop] *= 2*L::invCs2() * L::t[iPop];

  for (int iD = 0; iD < L::d; iD++) {
    _u[iPop][iD] = u[iD];
  }
}

template<typename T, template<typename U> class Lattice>
T OffDynamics<T,Lattice>::getVelocityCoefficient(int iPop)
{
  return velocityCoefficient[iPop];
}

////////////////////// Class ZeroDistributionDynamics ///////////////////////////

template<typename T, template<typename U> class Lattice>
ZeroDistributionDynamics<T,Lattice>::ZeroDistributionDynamics()
{
}

template<typename T, template<typename U> class Lattice>
ZeroDistributionDynamics<T,Lattice>* ZeroDistributionDynamics<T,Lattice>::clone() const
{
  return new ZeroDistributionDynamics<T,Lattice>();
}

template<typename T, template<typename U> class Lattice>
void ZeroDistributionDynamics<T,Lattice>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = -Lattice<T>::t[iPop];
  }
}

template<typename T, template<typename U> class Lattice>
OPENLB_HOST_DEVICE
T ZeroDistributionDynamics<T,Lattice>::computeRho(CellView<T,Lattice> const& cell) const
{
  return lbHelpers<T,Lattice>::computeRho(cell);
}

/////////////// Singletons //////////////////////////////////

namespace instances {

template<typename T, template<typename U> class Lattice>
BulkMomenta<T,Lattice>& getBulkMomenta()
{
  static BulkMomenta<T,Lattice> bulkMomentaSingleton;
  return bulkMomentaSingleton;
}

template<typename T, template<typename U> class Lattice>
ExternalVelocityMomenta<T,Lattice>& getExternalVelocityMomenta()
{
  static ExternalVelocityMomenta<T,Lattice> externalVelocityMomentaSingleton;
  return externalVelocityMomentaSingleton;
}

template<typename T, template<typename U> class Lattice>
BounceBack<T,Lattice>& getBounceBack()
{
  static BounceBack<T,Lattice> bounceBackSingleton;
  return bounceBackSingleton;
}

template<typename T, template<typename U> class Lattice>
PartialBounceBack<T,Lattice>& getPartialBounceBack(const double rf)
{
  static PartialBounceBack<T,Lattice> partialBounceBackSingleton(rf);
  return partialBounceBackSingleton;
}


template<typename T, template<typename U> class Lattice>
BounceBackVelocity<T,Lattice>& getBounceBackVelocity(const double rho, const double u[Lattice<T>::d])
{
  static BounceBackVelocity<T,Lattice> bounceBackSingleton(rho,u);
  return bounceBackSingleton;
}

template<typename T, template<typename U> class Lattice>
BounceBackAnti<T,Lattice>& getBounceBackAnti(const double rho)
{
  static BounceBackAnti<T,Lattice> bounceBackSingleton(rho);
  return bounceBackSingleton;
}

template <typename T, template<typename U> class Lattice>
GhostDynamics<T,Lattice>& getGhostDynamics() {
  static GhostDynamics<T,Lattice> ghostDynamicsSingleton((T)1.0);
  return ghostDynamicsSingleton;
}

template<typename T, template<typename U> class Lattice>
NoDynamics<T,Lattice>& getNoDynamics(T rho)
{
  static NoDynamics<T,Lattice> noDynamicsSingleton(rho);
  return noDynamicsSingleton;
}

template<typename T, template<typename U> class Lattice>
ZeroDistributionDynamics<T,Lattice>& getZeroDistributionDynamics()
{
  static ZeroDistributionDynamics<T,Lattice> zeroSingleton;
  return zeroSingleton;
}

}

}

#endif
