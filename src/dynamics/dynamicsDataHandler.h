
/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Jakob Bludau, Bastian Horvat, Markus Mohrhardt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Groups all the generic 2D template files in the boundaryConditions directory.
 */

#ifndef DYNAMICS_DATA_HANDLER_H
#define DYNAMICS_DATA_HANDLER_H

#include <vector>
#include <algorithm>
#include <core/olbDebug.h>

namespace olb {

template <typename T>
class DynamicsDataHandler {

private:
  std::vector<size_t>           _cellID;
  T**                           _data;
  T**                           _postProcData;
  size_t                        _numberOfDataPointers;
  size_t                        _numberOfPostProcDataPointers;
  bool                          _isAllocated;
  bool                          _fluidDynamics;
  size_t                        _extraAllocationCells;

public:

  DynamicsDataHandler(const unsigned int numberOfDataPointers, const unsigned int numberOfPostProcDataPointers, bool fluidDynamics) :
    _numberOfDataPointers(numberOfDataPointers),
    _numberOfPostProcDataPointers(numberOfPostProcDataPointers),
    _isAllocated(false),
    _fluidDynamics(fluidDynamics),
    _extraAllocationCells(0)
  {
    _data = new T*[_numberOfDataPointers];
    for (unsigned int i=0; i<_numberOfDataPointers ; ++i)
    {
      _data[i] = nullptr;
    }

    _postProcData = new T*[_numberOfPostProcDataPointers];
    for (unsigned int i=0; i<_numberOfPostProcDataPointers ; ++i)
    {
        _postProcData[i] = nullptr;
    }
  }

  ~DynamicsDataHandler()
  {
    for (unsigned int i=0; i<_numberOfDataPointers;++i)
    {
      delete[] _data[i];
    }
    delete[] _data;

    for (unsigned int i=0; i<_numberOfPostProcDataPointers;++i)
    {
      delete[] _postProcData[i];
    }
    delete[] _postProcData;

  }

  size_t * getCellIDsData()
  {
    OLB_ASSERT(_isAllocated, "Access of unallocated data array (T**)");
    return &_cellID[0];
  }

  T** getDynamicsData ()
  {
    OLB_ASSERT(_isAllocated, "Access of unallocated data array (T**)");
    return _data;
  }

  T** getPostProcData ()
  {
    OLB_ASSERT(_isAllocated, "Access of unallocated data array (T**)");
    return _postProcData;
  }

  void setExtraAllocationSpace(size_t extraCells) {
    _extraAllocationCells = extraCells;
  }

  size_t getExtraAllocationSpace() {
    return _extraAllocationCells;
  }

  size_t getAllocationSize() {
    return _cellID.size() + _extraAllocationCells;
  }

  std::vector<size_t> const & getCellIDs() const { return _cellID; }

  void overwriteCellIDs(std::vector<size_t> newCellIDs) {
    _cellID = newCellIDs;
  }

  void sort()
  {
      std::sort(_cellID.begin(), _cellID.end());
  }

  size_t getMomentaIndex(size_t cellIndex)
  {
    if (_fluidDynamics)
      return 0;
    return std::distance(_cellID.begin(), std::find(_cellID.begin(), _cellID.end(), cellIndex));
  }

  void registerCell (unsigned int cellIndex)
  {
    _cellID.push_back(cellIndex);
  }

  void allocateMemory ()
  {
    OLB_ASSERT(!_isAllocated, "Double allocation of memory (T**)");
    for (unsigned int i=0; i<_numberOfDataPointers ; ++i)
    {
      _data[i] = new T [getAllocationSize()];
      std::memset(_data[i],0,getAllocationSize());
    }
    for (unsigned int i=0; i<_numberOfPostProcDataPointers ; ++i)
    {
      _postProcData[i] = new T [getAllocationSize()];
      std::memset(_postProcData[i],0,getAllocationSize());
    }

    _isAllocated = true;
  }

  unsigned int getNumberOfDataPoints() const
  {
    return _numberOfDataPointers;
  }

  unsigned int getNumberOfPostProcDataPoints() const
  {
    return _numberOfPostProcDataPointers;
  }

  bool isFluidDataHandler() const
  {
    return _fluidDynamics;
  }

};

} // end of namespace
#endif
