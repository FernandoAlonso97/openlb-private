/*
 * dynamicsDataHandlerNew.h
 *
 *  Created on: May 28, 2019
 *      Author: ga69kiq
 */

#ifndef SRC_DYNAMICS_DYNAMICSDATAHANDLERNEW_H_
#define SRC_DYNAMICS_DYNAMICSDATAHANDLERNEW_H_

#include <vector>
#include <algorithm>
#include <core/olbDebug.h>
#ifdef ENABLE_CUDA
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include "core/cudaErrorHandler.h"
#endif

namespace olb {

template <typename T>
class DynamicsDataHandlerNew {

private:
  T**                           _data;
  size_t                        _numberOfDataPointers;
  bool                          _isAllocated;
  bool                          _fluidDynamics;
#ifdef ENABLE_CUDA
  thrust::host_vector<size_t> _cellID;
  thrust::device_vector<size_t> _cellIDGPU;
  T** _dataGPU;
#else
  std::vector<size_t>           _cellID;
#endif

public:

  DynamicsDataHandlerNew(const unsigned int numberOfDataPointers, bool fluidDynamics) :
    _numberOfDataPointers(numberOfDataPointers),
    _isAllocated(false),
    _fluidDynamics(fluidDynamics)
  {
    _data = new T*[_numberOfDataPointers];
    for (unsigned int i=0; i<_numberOfDataPointers ; ++i)
    {
      _data[i] = nullptr;
    }
#ifdef ENABLE_CUDA
    if(_numberOfDataPointers)
    {
        cudaError_t error = cudaMallocManaged(&_dataGPU, _numberOfDataPointers*sizeof(T));
        HANDLE_ERROR(error);
    }
    else
        _dataGPU = nullptr;
#endif
  }

  ~DynamicsDataHandlerNew()
  {
    for (unsigned int i=0; i<_numberOfDataPointers;++i)
    {
      delete[] _data[i];
    }

    delete[] _data;
#ifdef ENABLE_CUDA

    for (unsigned int i=0; i<_numberOfDataPointers;++i)
    {
      cudaFree(_dataGPU[i]);
    }

    delete[] _dataGPU;
#endif
  }

  T** getDynamicsData ()
  {
    OLB_ASSERT(_isAllocated, "Access of unallocated data array (T**)");
    return _data;
  }

#ifdef ENABLE_CUDA
  thrust::host_vector<size_t> const & getCellIDsCPU() const { return _cellID; }
  thrust::device_vector<size_t> const & getCellIDsGPU() const { return _cellIDGPU; }
#else
  std::vector<size_t> const & getCellIDs() const { return _cellID; }
#endif

  void sort()
  {
      std::sort(_cellID.begin(), _cellID.end());
  }

  size_t getMomentaIndex(size_t cellIndex)
  {
    if (_fluidDynamics)
      return 0;
    return std::distance(_cellID.begin(), std::find(_cellID.begin(), _cellID.end(), cellIndex));
  }

  void registerCell (unsigned int cellIndex)
  {
    _cellID.push_back(cellIndex);
  }

  void allocateMemory ()
  {
    OLB_ASSERT(!_isAllocated, "Double allocation of memory (T**)");


    if( (not _cellID.empty()) or (_numberOfDataPointers == 0) )
    {
        for (unsigned int i=0; i<_numberOfDataPointers ; ++i)
            _data[i] = new T [_cellID.size()];

#ifdef ENABLE_CUDA
        for (unsigned int i=0; i<_numberOfDataPointers ; ++i)
        {
            cudaError_t error = cudaMalloc(&_dataGPU[i],_cellID.size()*sizeof(T));
            HANDLE_ERROR(error);
        }
#endif

        _isAllocated = true;
    }
  }

  void copyDataToGPU()
  {
#ifdef ENABLE_CUDA
      cudaDeviceSynchronize();
      _cellIDGPU = _cellID;
      for (int i = 0; i < _numberOfDataPointers; ++i) {
          cudaError_t error = cudaMemcpy(_dataGPU[i], _data[i], sizeof(T) * _cellID.size(), cudaMemcpyHostToDevice);
          HANDLE_ERROR(error);
      }
#endif
  }

  unsigned int getNumberOfDataPoints() const
  {
    return _numberOfDataPointers;
  }

  bool isFluidDataHandler() const
  {
    return _fluidDynamics;
  }

};

} // end of namespace



#endif /* SRC_DYNAMICS_DYNAMICSDATAHANDLERNEW_H_ */
