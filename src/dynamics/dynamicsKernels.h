/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2018 Bludau Jakob
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of dynamics classes (e.g. BGK) with which a CellView object
 * can be instantiated -- header file.
 */
#ifndef DYNAMICS_KERNELS_H
#define DYNAMICS_KERNELS_H

#include "../core/ALEutil.h"
#include "latticeDescriptors.h"
#include "../core/util.h"
#include "../dynamics/lbHelpers.h"

namespace olb {

template<class Momenta>
OPENLB_HOST_DEVICE
// No zero arrays allowed on GPU -> dummy size for momenta without data needed
constexpr int dummySize()
{
    return Momenta::numberDataEntries == 0 ? 1 : Momenta::numberDataEntries;
}
 
#ifdef __CUDACC__
template<typename T>
__global__
void sampleKernel (T*const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, unsigned index,T*const OPENLB_RESTRICT sampleData,size_t writeBufferStep)
{
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= 1)
    return;
  
  sampleData[writeBufferStep] = cellData[index][cellIndex];
}

template<typename T, template<typename U> class Lattice, class ExternalField>
__global__ void moveMeshKernel3D(
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataSrc,
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataDst,
    const AffineTransform<T, Lattice<T>::d> transform,
    const ExternalField externalField,
    const size_t gridSize,
    const size_t nx,
    const size_t ny,
    const size_t nz,
    bool* fluidMask,
    bool* staticFluidMask,
    SubDomainInformation<T,Lattice<T>> subDomInfo) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= gridSize)
    return;

  aleutil::interpolateCellPopulations3D<T, Lattice, ExternalField>(
      cellDataSrc,
      cellDataDst,
      transform,
      externalField,
      threadIndex,
      nx,
      ny,
      nz,
      subDomInfo
      // fluidMask,
      // staticFluidMask
  );
}

template<typename T, template<typename U> class Lattice, class ExternalField,unsigned boundaryDistance>
__global__ void updateFluidMask3D(
    bool* const OPENLB_RESTRICT cellDataSrc,
    bool* const OPENLB_RESTRICT cellDataDst,
    const AffineTransform<T, Lattice<T>::d> transform,
    const ExternalField externalField,
    const size_t gridSize,
    const size_t nx,
    const size_t ny,
    const size_t nz,
    const SubDomainInformation<T,Lattice<T>> subDomInfo) {
  const size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  const size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                       + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= gridSize)
    return;

  aleutil::interpolateFluidMask3D<T, Lattice, ExternalField,boundaryDistance>(
      cellDataSrc,
      cellDataDst,
      transform,
      externalField,
      threadIndex,
      nx,
      ny,
      nz,
      subDomInfo
  );
}

template<typename T, template<typename U> class Lattice, class ExternalField>
__global__ void moveMeshKernel2D(
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataSrc,
    T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellDataDst,
    const AffineTransform<T, Lattice<T>::d> transform,
    const ExternalField externalField,
    const size_t gridSize,
    const size_t nx,
    const size_t ny) {

    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                         + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= gridSize)
        return;

  aleutil::interpolateCellPopulations2D<T, Lattice, ExternalField>(
      cellDataSrc,
      cellDataDst,
      transform,
      externalField,
      threadIndex,
      nx,
      ny
  );
}

template<typename T,class Dynamics>
__global__ void fluidKernel (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray,
		size_t length, T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		bool * const OPENLB_RESTRICT fluidMask, T * const OPENLB_RESTRICT collisionData )
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  if (!fluidMask[threadIndex])
    return;

  Dynamics::collision(cellArray,threadIndex,momentaData,threadIndex,collisionData);
}

template<typename T,class Dynamics>
__global__ void boundaryKernel (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray,
		size_t length,T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		size_t  * const OPENLB_RESTRICT cellIds, T * const OPENLB_RESTRICT collisionData)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;


  Dynamics::collision(cellArray, cellIds[threadIndex], momentaData, threadIndex, collisionData);
}

template<typename T, class Dynamics>
__global__ void postProcKernel2D (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray,
		T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData
		, T const * const OPENLB_RESTRICT collisionData,
		T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData, size_t length,
		size_t * const OPENLB_RESTRICT cellIds, size_t const nx, size_t const ny)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  size_t indexCalcHelper[2] = {nx, ny};
  Dynamics::PostProcessorType::template process<Dynamics>(cellArray, momentaData, collisionData, postProcData,
          cellIds[threadIndex], threadIndex, indexCalcHelper);
}

template<typename T, class Dynamics>
__global__ void postProcKernel3D (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData
        , T const * const OPENLB_RESTRICT collisionData,
        T * const OPENLB_RESTRICT * const OPENLB_RESTRICT postProcData, size_t length,
        size_t * const OPENLB_RESTRICT cellIds, size_t const nx, size_t const ny, size_t const nz)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                      + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  size_t indexCalcHelper[3] = {nx, ny, nz};
  Dynamics::PostProcessorType::template process<Dynamics>(cellArray, momentaData, collisionData, postProcData,
          cellIds[threadIndex], threadIndex, indexCalcHelper);
}

template<typename T, template <typename> class Lattice>
__global__ void testFieldKernel(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray,
        size_t length, int indicator, size_t const nx, size_t const ny, size_t const nz)
{
    size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
    size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                        + blockIndex * blockDim.x * blockDim.y * blockDim.z;

    if (threadIndex >= length)
      return;

    size_t indices[3];
    util::getCellIndices3D(threadIndex,ny,nz,indices);

    for(unsigned int iPop = 0; iPop<Lattice<T>::dataSize; ++iPop)
        if(isnan(cellArray[iPop][threadIndex]) or isinf(cellArray[iPop][threadIndex]))
        {
            printf("Nan detected %i: %lu, %lu, %lu, %lu\n", indicator, threadIndex, indices[0], indices[1], indices[2]);
            return;
        }

}


__global__ void collision_D2Q9_BGKFluid_Omega_1_23885(float* __restrict__ * __restrict__ vals, bool* __restrict__ mask, int size) {
int blockId = blockIdx.x + blockIdx.y * gridDim.x;
int idx = blockId * (blockDim.x * blockDim.y) + (threadIdx.y * blockDim.x) + threadIdx.x;
if (idx >= size) return;
if (!mask[idx]) return;
float x0 = -vals[4][idx] + vals[8][idx];
float x1 = -vals[3][idx];
float x2 = -vals[5][idx];
float x3 = x0 + x1 + x2 + vals[1][idx] + vals[7][idx];
float tem0 = x3;
float x4 = tem0*tem0;
float x5 = vals[0][idx] + vals[1][idx] + vals[2][idx] + vals[3][idx] + vals[4][idx] + vals[5][idx] + vals[6][idx] + vals[7][idx] + vals[8][idx] + 1.0;
float tem1 = x5;
float x6 = 1/(tem1*tem1);
float x7 = 1.50000000000000f*x6;
float x8 = x4*x7;
float x9 = -vals[2][idx];
float x10 = x9 + vals[6][idx];
float tem2 = (x1 + x10 - vals[1][idx] + vals[5][idx] + vals[7][idx]);
float x11 = x7*tem2*tem2 - 1;
float x12 = x11 + x8;
float x13 = 0.0344125000000000f*vals[0][idx] + 0.0344125000000000f*vals[1][idx] + 0.0344125000000000f*vals[2][idx] + 0.0344125000000000f*vals[3][idx] + 0.0344125000000000f*vals[4][idx] + 0.0344125000000000f*vals[5][idx] + 0.0344125000000000f*vals[6][idx] + 0.0344125000000000f*vals[7][idx] + 0.0344125000000000f*vals[8][idx] + 0.0344125000000000f;
float x14 = 2*vals[1][idx];
float x15 = 2*vals[5][idx];
float x16 = 4.50000000000000f*x6;
float x17 = 3.0*vals[1][idx];
float x18 = 3.0*vals[5][idx];
float x19 = x17 - x18;
float x20 = 3.0*vals[2][idx];
float x21 = 3.0*vals[3][idx];
float x22 = 3.0*vals[6][idx];
float x23 = 3.0*vals[7][idx];
float x24 = 1.0/x5;
float x25 = x24*(x19 + x20 + x21 - x22 - x23);
float x26 = -vals[6][idx];
float x27 = x2 + x26 + vals[1][idx] + vals[2][idx] + vals[3][idx] - vals[7][idx];
float tem3 = x27;
float x28 = tem3*tem3;
float x29 = -x28*x7;
float x30 = -x8;
float x31 = -x21;
float x32 = x24*(x19 + x23 + x31 - 3.0*vals[4][idx] + 3.0*vals[8][idx]);
float x33 = x29 + x30 + x32 + 1;
float x34 = 0.137650000000000f*vals[0][idx] + 0.137650000000000f*vals[1][idx] + 0.137650000000000f*vals[2][idx] + 0.137650000000000f*vals[3][idx] + 0.137650000000000f*vals[4][idx] + 0.137650000000000f*vals[5][idx] + 0.137650000000000f*vals[6][idx] + 0.137650000000000f*vals[7][idx] + 0.137650000000000f*vals[8][idx] + 0.137650000000000f;
float x35 = 3.0*x6;
float x36 = x28*x35 + x30 + 1;
float tem4 = (x0 + x9 - 2*vals[3][idx] + vals[6][idx] + 2*vals[7][idx]);
float x37 = x16*tem4*tem4;
float x38 = x35*x4;
float x39 = -x25;
float temp0 = -0.137650000000000f*x12*(4*vals[0][idx] + 4*vals[1][idx] + 4*vals[2][idx] + 4*vals[3][idx] + 4*vals[4][idx] + 4*vals[5][idx] + 4*vals[6][idx] + 4*vals[7][idx] + 4*vals[8][idx] + 4.0) - 0.238850000000000f*vals[0][idx] - 0.550600000000000f;
float tem5 = (x10 - x14 + x15 + vals[4][idx] - vals[8][idx]);
float temp1 = x13*(x16*tem5*tem5 + x25 + x33) - 0.238850000000000f*vals[1][idx] - 0.0344125000000000f;
float temp2 = x34*(x25 + x36) - 0.238850000000000f*vals[2][idx] - 0.137650000000000f;
float temp3 = -x13*(x12 + x24*(-x17 + x18 - x20 + x22 + x23 + x31) + x32 - x37) - 0.238850000000000f*vals[3][idx] - 0.0344125000000000f;
float temp4 = -x34*(x11 + x32 - x38) - 0.238850000000000f*vals[4][idx] - 0.137650000000000f;
float tem6 = (x0 + x14 - x15 + x26 + vals[2][idx]);
float temp5 = x13*(x16*tem6*tem6 + x29 + x30 - x32 + x39 + 1) - 0.238850000000000f*vals[5][idx] - 0.0344125000000000f;
float temp6 = x34*(x36 + x39) - 0.238850000000000f*vals[6][idx] - 0.137650000000000f;
float temp7 = x13*(x33 + x37 + x39) - 0.238850000000000f*vals[7][idx] - 0.0344125000000000f;
float temp8 = x34*(x29 + x32 + x38 + 1) - 0.238850000000000f*vals[8][idx] - 0.137650000000000f;
float temp9 = x5;
float temp10 = -x24*x27;
float temp11 = x24*x3;
vals[0][idx] = temp0;
vals[1][idx] = temp5;
vals[2][idx] = temp6;
vals[3][idx] = temp7;
vals[4][idx] = temp8;
vals[5][idx] = temp1;
vals[6][idx] = temp2;
vals[7][idx] = temp3;
vals[8][idx] = temp4;
vals[9][idx] = temp9;
vals[10][idx] = temp10;
vals[11][idx] = temp11;
}

template<typename T,class Dynamics>
__global__ void fluidKernel_cached (T** cellArray, size_t length,T** momentaData,
		const bool* fluidMask, T const * const OPENLB_RESTRICT gpuCollsionData )
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  if (fluidMask[threadIndex] == false)
    return;

  T cellCache[Dynamics::LatticeType::q];
  // the following should be made constant if the same for all cells
  T momentaCache[dummySize<Dynamics::MomentaType>()];

// read into registers or cache
#pragma unroll
 for (int q=0;q<Dynamics::LatticeType::q;++q)
  cellCache[q] = cellArray[q][threadIndex];

#pragma unroll
 for (int mIndex=0;mIndex<Dynamics::MomentaType::numberDataEntries;++mIndex)
  momentaCache[mIndex] = momentaData[mIndex][threadIndex]; // TODO no offset added -> assumed that bulk momenta have index 0

// do the collision work
  Dynamics::collision(cellCache,momentaCache,gpuCollsionData);

 // write from registers or cache
#pragma unroll
 for (int q=0;q<Dynamics::LatticeType::q;++q)
   cellArray[q][threadIndex] = cellCache[q];
}

template<typename T,class Dynamics>
__global__ void boundaryKernel_cached (T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellArray, size_t length,const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
		size_t * cellIds, T const * const OPENLB_RESTRICT gpuCollsionData)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= length)
    return;

  T cellCache[Dynamics::LatticeType::q];
  // the following should be made constant if the same for all cells
  T momentaCache[dummySize<Dynamics::MomentaType>()];

// read into registers or cache
#pragma unroll
 for (int q=0;q<Dynamics::LatticeType::q;++q)
  cellCache[q] = cellArray[q][cellIds[threadIndex]];

#pragma unroll
 for (int mIndex=0;mIndex<Dynamics::MomentaType::numberDataEntries;++mIndex)
  momentaCache[mIndex] = momentaData[mIndex+Dynamics::MomentaType::dataOffset_][threadIndex];

// do the collision work
  Dynamics::collision(cellCache,momentaCache,gpuCollsionData);

 // write from registers or cache
#pragma unroll
 for (int q=0;q<Dynamics::LatticeType::q;++q)
   cellArray[q][cellIds[threadIndex]] = cellCache[q];
}

template<typename T,template<typename U> class Lattice, template < typename V,template<typename W> class > class Functor>
__global__ void writeBlockLatticeByFunctorKernel(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,const typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT functorData, const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= numberOfCells)
    return;

 Functor<T,Lattice>::writeToField(cellData,cellIndices[threadIndex],functorData[threadIndex]); 

}

template<typename T,template<typename U> class Lattice, template < typename V,template<typename W> class > class Functor>
__global__ void readBlockLatticeByFunctorKernel(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,typename Functor<T,Lattice>::returnType* const OPENLB_RESTRICT functorData, const size_t * const OPENLB_RESTRICT cellIndices,const size_t numberOfCells)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= numberOfCells)
    return;

 functorData[threadIndex] = Functor<T,Lattice>::readFromField(cellData,cellIndices[threadIndex]); 

}

template<typename T,template<typename U> class Lattice,typename Callable, typename ... ARGS>
__global__ void applyFunctorKernel (Callable call,T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,const size_t loopLength, ARGS ... args)
{
  size_t blockIndex = blockIdx.x + blockIdx.y * gridDim.x + blockIdx.z * gridDim.x * gridDim.y;
  size_t threadIndex = threadIdx.x + threadIdx.y * blockDim.x + threadIdx.z * blockDim.x * blockDim.y
                    + blockIndex * blockDim.x * blockDim.y * blockDim.z;

  if (threadIndex >= loopLength)
	return;

  call(cellData,threadIndex,args...);

}





#endif 
} // end of namespace

#endif
