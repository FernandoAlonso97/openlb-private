/*
 * list.h
 *
 *  Created on: Jan 16, 2018
 *      Author: Bastian Horvat
 */

#include "core/config.h"

#ifndef SRC_DYNAMICS_DYNAMICSLIST_H_
#define SRC_DYNAMICS_DYNAMICSLIST_H_
namespace dynamicsList {
struct LISTEND {
  typedef LISTEND Head;
  typedef LISTEND Tail;
};

template <typename H, typename T=LISTEND> struct LIST {

  typedef H Head;
  typedef T Tail;
  T tail{};
  H data{};
};

template <typename LIST> struct LENGTH {
  static const unsigned int result = 1 + LENGTH <typename LIST::Tail>::result;
};

template <> struct LENGTH<LISTEND> {
  static const unsigned int result = 0;
};

template<typename LIST, int N>
struct getType {
  typedef typename getType<typename LIST::Tail,N-1>::type type;
};

template<typename LIST>
struct getType<LIST,0> {
  typedef typename LIST::Head type;
};


template<typename LIST, int N>
OPENLB_HOST_DEVICE
inline typename std::enable_if< 0<N ,typename getType<LIST, N>::type& >::type
get(LIST& list)
{
  return get<typename LIST::Tail, N-1>(list.tail);
}

template<typename LIST, int N>
OPENLB_HOST_DEVICE
inline typename std::enable_if<N == 0 ,typename getType<LIST, 0>::type& >::type
get(LIST& list)
{
  return list.data;
}

template<typename...>
struct is_one_of {
  static constexpr bool value = false;
};

template <typename F, typename S, typename... T>
struct is_one_of<F, S, T...> {
    static constexpr bool value =
        std::is_same<F, S>::value || is_one_of<F, T...>::value || is_one_of<S, T...>::value;
};

template<typename A, typename...Values>
struct getCreationType{
  typedef LIST<A,typename getCreationType<Values...>::type> type;
};

template<typename A>
struct getCreationType<A>{
  typedef LIST<A> type;
};

template<typename A, typename... Values>
typename getCreationType<A,Values...>::type createList()
{
  static_assert(!is_one_of<A,Values...>::value, "Duplicate types in list are not allowed");
  typename getCreationType<A,Values...>::type list{};
  return list;
}

template<typename Type, typename List>
typename std::enable_if<std::is_same<LISTEND,typename List::Head>::value,size_t>::type
getPosition()
{
  static_assert(!std::is_same<LISTEND, typename List::Head>::value,"Type is not within list");
}

template<typename Type, typename List>
constexpr typename std::enable_if<std::is_same<Type, typename List::Head>::value,size_t>::type
getPosition()
{
  return 0;
}

template<typename Type, typename List>
constexpr typename std::enable_if<!(std::is_same<Type,typename List::Head>::value) and !(std::is_same<LISTEND, typename List::Head>::value),size_t>::type
getPosition()
{
  return 1+getPosition<Type,typename List::Tail>();
}

} // End namespace dynamicslist
#endif /* SRC_DYNAMICS_DYNAMICSLIST_H_ */
