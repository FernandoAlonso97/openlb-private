/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007, 2017 Orestis Malaspinas, Jonas Latt, Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of entropic dynamics classes (e.g. EntropicEq,
 * ForcedEntropicEq, Entropic, ForcedEntropic) with which a CellView object
 * can be instantiated -- header file.
 *
 * Entropic Modell:
 * Ansumali, Santosh, Iliya V. Karlin, and Hans Christian Öttinger
 * Minimal entropic kinetic models for hydrodynamics
 * EPL (Europhysics Letters) 63.6 (2003): 798
 */

#ifndef ENTROPIC_LB_DYNAMICS_H
#define ENTROPIC_LB_DYNAMICS_H

#include "dynamics/dynamics.h"

namespace olb {

template<typename T, template<typename U> class Lattice> class CellView;


/// Implementation of the entropic collision step
template<typename T, template<typename U> class Lattice, class Momenta>
class EntropicEqDynamics : public BasicDynamics<T,Lattice, Momenta, EntropicEqDynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  EntropicEqDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  EntropicEqDynamics<T,Lattice, Momenta>* clone() const override;
  /// Compute equilibrium distribution function
  T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const override;
  /// Collision step
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega_) override;
private:
  T omega;  ///< relaxation parameter
};

/// Implementation of the forced entropic collision step
template<typename T, template<typename U> class Lattice, class Momenta>
class ForcedEntropicEqDynamics : public BasicDynamics<T,Lattice,Momenta,ForcedEntropicEqDynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  ForcedEntropicEqDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual ForcedEntropicEqDynamics<T,Lattice,Momenta>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  T omega;  ///< relaxation parameter

  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;
};

/// Implementation of the entropic collision step

template<typename T, template<typename U> class Lattice, class Momenta, class PostProcessor = NoPostProcessor<T,Lattice>>
class EntropicDynamics : public BasicDynamics<T,Lattice, Momenta, EntropicDynamics<T, Lattice, Momenta, PostProcessor>> {
public:
  /// Constructor
  EntropicDynamics(T omega_);
  EntropicDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  EntropicDynamics<T,Lattice,Momenta,PostProcessor>* clone() const override;
  /// Compute equilibrium distribution function
  T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  OPENLB_HOST_DEVICE
  void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_) override;
  void collide(CellDataArray<T,Lattice> cell,
                       LatticeStatistics<T>& statistics_) override {} 

  OPENLB_HOST_DEVICE
  static void collision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,
          size_t cellIndex, T* const OPENLB_RESTRICT * const OPENLB_RESTRICT momentaData,
          size_t momentaIndex, T* const OPENLB_RESTRICT collisionData);

  /// Get local relaxation parameter of the dynamics
  T getOmega() const override;
  /// Set local relaxation parameter of the dynamics
  void setOmega(T omega_) override;

  T* getCollisionData() override
          {return _collisionData;};
  int getCollisionDataSize() override
          {return 1;};

  enum{omegaIndex = 0};
private:
  /// computes the entropy function H(f)=sum_i f_i*ln(f_i/t_i)
  OPENLB_HOST_DEVICE
  static T computeEntropy(const T f[]);
  /// computes the entropy growth H(f)-H(f-alpha*fNeq)
  OPENLB_HOST_DEVICE
  static T computeEntropyGrowth(const T f[], const T fNeq[], const T &alpha);
  /// computes the entropy growth derivative
  /// dH/dalpha=-sum_i fNeq_i*ln((f_i-alpha*fNeq_i)/t_i)
  OPENLB_HOST_DEVICE
  static T computeEntropyGrowthDerivative(const T f[], const T fNeq[], const T &alpha);
  /// Get the alpha parameter
  OPENLB_HOST_DEVICE
  static bool getAlpha(T &alpha, const T f[], const T fNeq[]);

  T omega;  ///< relaxation parameter
  T _collisionData[1];
};

/// Implementation of the forced entropic collision step
template<typename T, template<typename U> class Lattice, class Momenta>
class ForcedEntropicDynamics : public BasicDynamics<T,Lattice,Momenta, ForcedEntropicDynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  ForcedEntropicDynamics(T omega_, Momenta& momenta_);
  /// Clone the object on its dynamic type.
  virtual ForcedEntropicDynamics<T,Lattice,Momenta>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
private:
  /// computes the entropy function H(f)=sum_i f_i*ln(f_i/t_i)
  T computeEntropy(const T f[]);
  /// computes the entropy growth H(f)-H(f-alpha*fNeq)
  T computeEntropyGrowth(const T f[], const T fNeq[], const T &alpha);
  /// computes the entropy growth derivative
  /// dH/dalpha=-sum_i fNeq_i*ln((f_i-alpha*fNeq_i)/t_i)
  T computeEntropyGrowthDerivative(const T f[], const T fNeq[], const T &alpha);
  /// Get the alpha parameter
  bool getAlpha(T &alpha, const T f[], const T fNeq[]);

  T omega;  ///< relaxation parameter

  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;
};
}

#endif
