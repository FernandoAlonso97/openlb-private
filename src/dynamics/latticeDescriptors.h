/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Descriptor for all types of 2D and 3D lattices. In principle, thanks
 * to the fact that the OpenLB code is generic, it is sufficient to
 * write a new descriptor when a new type of lattice is to be used.
 *  -- header file
 */
#ifndef LATTICE_DESCRIPTORS_H
#define LATTICE_DESCRIPTORS_H

#include <vector>
#include "core/olbDebug.h"
#include "core/config.h"

namespace olb {

/// Descriptors for the 2D and 3D lattices.
/** \warning Attention: The lattice directions must always be ordered in
 * such a way that c[i] = -c[i+(q-1)/2] for i=1..(q-1)/2, and c[0] = 0 must
 * be the rest velocity. Furthermore, the velocities c[i] for i=1..(q-1)/2
 * must verify
 *  - in 2D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *  - in 3D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *                       || (c[i][0]==0 && c[i][1]==0 && c[i][2]<0)
 * Otherwise some of the code will work erroneously, because the
 * aformentioned relations are taken as given to enable a few
 * optimizations.
*/
namespace descriptors {

/// descriptor base
/*template <unsigned Size> class DescriptorBase
{
  enum { size = Size };       ///< number of dimensions
};

template <typename T, unsigned Size> class Vector
    : public DescriptorBase<Size>
{
    T data[Size];  ///< number of dimensions
};

template <typename T> class Scalar
    : public Vector<T,1>
{
};

template <typename T> class Vector2D
    : public Vector<T,2>
{
};

template <typename T> class Vector3D
    : public Vector<T,1>
{
};*/


struct NoExternalField {
  static const int numScalars = 0;
  static const int numSpecies = 0;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 0;
  static const int velocityBeginsAt = 0;
  static const int sizeOfVelocity   = 0;
};

struct NoExternalFieldGPU {
  const int numScalars{0};
  const int numSpecies{0};
  const int forceBeginsAt{0};
  const int sizeOfForce{0};
  const int velocityBeginsAt{0};
  const int sizeOfVelocity{0};
};

struct NoExternalFieldBase {
  typedef NoExternalField ExternalField;
};

struct NoExternalFieldBaseGPU {
  NoExternalFieldGPU ExternalFieldGPU{};
};

struct Force2dDescriptor {
  static const int numScalars = 2;
  static const int numSpecies = 1;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 2;
};

struct Force2dDescriptorGPU {
    const int numScalars = 2;
    const int numSpecies = 1;
    const int forceBeginsAt = 0;
    const int sizeOfForce   = 2;
};

struct Force2dDescriptorBase {
  typedef Force2dDescriptor ExternalField;
};

struct Force2dDescriptorBaseGPU {
    Force2dDescriptorGPU ExternalFieldGPU{};
};

struct Force3dDescriptor {
  static const int numScalars = 3;
  static const int numSpecies = 1;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 3;
};

struct Force3dDescriptorGPU {
    const int numScalars = 3;
    const int numSpecies = 1;
    const int forceBeginsAt = 0;
    const int sizeOfForce   = 3;
};

struct Force3dDescriptorBase {
  typedef Force3dDescriptor ExternalField;
};

struct Force3dDescriptorBaseGPU {
    Force3dDescriptorGPU ExternalFieldGPU{};
};

struct V6Force2dDescriptor {
  static const int numScalars = 8;
  static const int numSpecies = 2;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 2;
  static const int vBeginsAt = 2;
  static const int sizeOfV   = 6;
};

struct V6Force2dDescriptorBase {
  typedef V6Force2dDescriptor ExternalField;
};

struct V12Force3dDescriptor {
  static const int numScalars = 15;
  static const int numSpecies = 2;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 3;
  static const int vBeginsAt = 3;
  static const int sizeOfV   = 12;
};

struct V12Force3dDescriptorBase {
  typedef V12Force3dDescriptor ExternalField;
};

template<typename T, typename ExternalField>
class ExternalFieldArray {
public:
  T* get(int index)
  {
    OLB_PRECONDITION( index < ExternalField::numScalars );
    return data+index;
  }
  T const* get(int index) const
  {
    OLB_PRECONDITION( index < ExternalField::numScalars );
    return data+index;
  }
private:
  T data[ExternalField::numScalars];
};

template<typename T>
class ExternalFieldArray<T,descriptors::NoExternalField> {
public:
  T* get(unsigned index)
  {
    OLB_PRECONDITION( false );
    static T data = T();
    return &data;
  }
  T const* get(unsigned index) const
  {
    OLB_PRECONDITION( false );
    static T data = T();
    return &data;
  }
};

struct Velocity2dDescriptor {
  static const int numScalars = 2;
  static const int numSpecies = 1;
  static const int velocityBeginsAt = 0;
  static const int sizeOfVelocity   = 2;
};

struct Velocity2dBase {
  typedef Velocity2dDescriptor ExternalField;
};

struct Velocity3dDescriptor {
  static const int numScalars = 3;
  static const int numSpecies = 1;
  static const int velocityBeginsAt = 0;
  static const int sizeOfVelocity   = 3;
};

struct Velocity3dBase {
  typedef Velocity3dDescriptor ExternalField;
};

struct ParticleAdvectionDiffusion3dDescriptor {
  static const int numScalars = 6;
  static const int numSpecies = 2;
  static const int velocityBeginsAt = 0;
  static const int sizeOfVelocity   = 3;
  static const int velocity2BeginsAt = 3;
  static const int sizeOfVelocity2   = 3;
};

struct ParticleAdvectionDiffusion3dDescriptorBase {
  typedef ParticleAdvectionDiffusion3dDescriptor ExternalField;
};

/// D2Q5 lattice
template <typename T> struct D2Q5DescriptorBase {
  typedef D2Q5DescriptorBase<T> BaseDescriptor;
  enum { d = 2, q = 5 };      ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
};

/// D2Q5 lattice lattice for advection-diffusion problems (MRT)
template <typename T>
struct AdvectionDiffusionMRTD2Q5DescriptorBase {
  typedef AdvectionDiffusionMRTD2Q5DescriptorBase<T> BaseDescriptor;
  enum { d = 2, q = 5 };          // number of dimensions/distr. functions
  static const int vicinity;      // size of neighborhood
  static const int c[q][d];       // lattice directions
  static const T t[q];            // lattice weights
  static const int opposite[q];   // opposite entry
  static const T M[q][q];         // Matrix of base change between f and moments : moments=M.f
  static const T invM[q][q];      // inverse of base change matrix : f=invM.moments
  static const T S[q];            // relaxation times
  enum { shearIndexes = 2 };
  static const int shearViscIndexes[shearIndexes]; // relevant indexes of r. t. for shear viscosity
  static const int bulkViscIndex = 2; // relevant index of r. t. for bulk viscosity
  static const T invCs2;              // inverse square of speed of sound
};

template <typename T> struct AdvectionDiffusionMRTD2Q5Descriptor
    : public AdvectionDiffusionMRTD2Q5DescriptorBase<T>, public Velocity2dBase {
};


/// D2Q9 lattice
template <typename T> struct D2Q9DescriptorBaseGPU {
  typedef D2Q9DescriptorBaseGPU<T> BaseDescriptorGPU;
  enum { d = 2, q = 9, rhoIndex = q, uIndex = q+1, dataSize = q+d+1 };      ///< number of dimensions/distr. functions
  int vicinity{0};  ///< size of neighborhood
  int c[q][d]{
    { 0, 0},
    {-1, 1}, {-1, 0}, {-1,-1}, { 0,-1},
    { 1,-1}, { 1, 0}, { 1, 1}, { 0, 1}
  };   ///< lattice directions
  int opposite[q]{
    0, 5, 6, 7, 8, 1, 2, 3, 4
  }; ///< opposite entry
  T t[q]{
    (T)4/(T)9,
    (T)1/(T)36, (T)1/(T)9, (T)1/(T)36, (T)1/(T)9,
    (T)1/(T)36, (T)1/(T)9, (T)1/(T)36, (T)1/(T)9
  };        ///< lattice weights
  T invCs2{3};      ///< inverse square of speed of sound
};

template <typename T> struct D2Q9DescriptorBase {
  typedef D2Q9DescriptorBase<T> BaseDescriptor;
  enum { d = 2, q = 9, rhoIndex = q, uIndex = q+1, dataSize = q+d+1 };      ///< number of dimensions/distr. functions
  static constexpr int vicinity = 1;  ///< size of neighborhood
  static constexpr int c[q][d] = {
      { 0, 0},
      {-1, 1}, {-1, 0}, {-1,-1}, { 0,-1},
      { 1,-1}, { 1, 0}, { 1, 1}, { 0, 1}
    };   ///< lattice directions
  static constexpr int opposite[q] = {
      0, 5, 6, 7, 8, 1, 2, 3, 4
    }; ///< opposite entry
  static const T t[q];
  static constexpr T invCs2 = (T)3;      ///< inverse square of speed of sound
};


/// D3Q7 lattice
template <typename T> struct D3Q7DescriptorBase {
  typedef D3Q7DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 7, rhoIndex = q, uIndex = q + 1, dataSize = q+d+1 };     ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
};

/// D3Q7 lattice for advection-diffusion problems (MRT)
template <typename T>
struct AdvectionDiffusionMRTD3Q7DescriptorBase {
  typedef AdvectionDiffusionMRTD3Q7DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 7, rhoIndex = q, uIndex = q + 1, dataSize = q+d+1  };          // number of dimensions/distr. functions
  static const int vicinity;      // size of neighborhood
  static const int c[q][d];       // lattice directions
  static const T t[q];            // lattice weights
  static const int opposite[q];   // opposite entry
  static const T M[q][q];         // Matrix of base change between f and moments : moments=M.f
  static const T invM[q][q];      // inverse of base change matrix : f=invM.moments
  static const T S[q];            // relaxation times
  static const T S_2[q];          // relaxation times
  enum { shearIndexes = 3 };
  static const int shearViscIndexes[shearIndexes]; // relevant indexes of r. t. for shear viscosity
  static const int bulkViscIndex = 1; // relevant index of r. t. for bulk viscosity
  static const T invCs2;              // inverse square of speed of sound
};

template <typename T> struct AdvectionDiffusionMRTD3Q7Descriptor
    : public AdvectionDiffusionMRTD3Q7DescriptorBase<T>, public Velocity3dBase {
};

template <typename T> struct ParticleAdvectionDiffusionMRTD3Q7Descriptor
    : public D3Q7DescriptorBase<T>, public ParticleAdvectionDiffusion3dDescriptorBase {
};

/// D3Q13 lattice
template <typename T> struct D3Q13DescriptorBase {
  typedef D3Q13DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 13, rhoIndex = q, uIndex = q + 1, dataSize = q+d+1  };     ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
  static const T lambda_e;    ///< relaxation parameter for the bulk stress
  static const T lambda_h;    ///< additional relaxation parameter
};

/// D3Q15 lattice
template <typename T> struct D3Q15DescriptorBase {
  typedef D3Q15DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 15, rhoIndex = q, uIndex = q + 1, dataSize = q+d+1  };     ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
};

/// D3Q19 lattice
template <typename T> struct D3Q19DescriptorBase {
  typedef D3Q19DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 19, rhoIndex = q, uIndex = q+1, dataSize = q+d+1 };     ///< number of dimensions/distr. functions
  static constexpr int vicinity = 1;  ///< size of neighborhood
  static constexpr int c[q][d] = {
          { 0, 0, 0},

          {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
          {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
          {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},

          { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
          { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
          { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1}
        };   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static constexpr T invCs2 = (T)3;      ///< inverse square of speed of sound
};

template <typename T> struct D3Q19DescriptorBaseGPU {
  typedef D3Q19DescriptorBaseGPU<T> BaseDescriptorGPU;
  enum { d = 3, q = 19, rhoIndex = q, uIndex = q+1 , dataSize = q+d+1 };      ///< number of dimensions/distr. functions
  int vicinity = 1;  ///< size of neighborhood
  int c[q][d] = {
          { 0, 0, 0},

          {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
          {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
          {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},

          { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
          { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
          { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1}
        };   ///< lattice directions
  int opposite[q] = {
          0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 1, 2, 3, 4, 5, 6, 7, 8, 9
        }; ///< opposite entry
  T t[q] = {
          (T)1/(T)3,

          (T)1/(T)18, (T)1/(T)18, (T)1/(T)18,
          (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,
          (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,

          (T)1/(T)18, (T)1/(T)18, (T)1/(T)18,
          (T)1/(T)36, (T)1/(T)36, (T)1/(T)36,
          (T)1/(T)36, (T)1/(T)36, (T)1/(T)36
        };        ///< lattice weights
  T invCs2 = (T)3;      ///< inverse square of speed of sound
};

/// D3Q27 lattice
template <typename T> struct D3Q27DescriptorBase {
  typedef D3Q27DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 27, rhoIndex = q, uIndex = q+1, dataSize = q+d+1 };     ///< number of dimensions/distr. functions
  static constexpr int vicinity = 1;  ///< size of neighborhood
  static constexpr int c[q][d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
  {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},
  {-1,-1,-1}, {-1,-1, 1}, {-1, 1,-1}, {-1, 1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
  { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1},
  { 1, 1, 1}, { 1, 1,-1}, { 1,-1, 1}, { 1,-1,-1}

        };   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static constexpr T invCs2 = (T)3;      ///< inverse square of speed of sound

};
template <typename T> struct D3Q27DescriptorBaseGPU {
  typedef D3Q27DescriptorBaseGPU<T> BaseDescriptorGPU;
  enum { d = 3, q = 27, rhoIndex = q, uIndex = q+1 , dataSize = q+d+1 };      ///< number of dimensions/distr. functions
  int vicinity = 1;  ///< size of neighborhood
  int c[q][d] = {
  { 0, 0, 0},

  {-1, 0, 0}, { 0,-1, 0}, { 0, 0,-1},
  {-1,-1, 0}, {-1, 1, 0}, {-1, 0,-1},
  {-1, 0, 1}, { 0,-1,-1}, { 0,-1, 1},
  {-1,-1,-1}, {-1,-1, 1}, {-1, 1,-1}, {-1, 1, 1},

  { 1, 0, 0}, { 0, 1, 0}, { 0, 0, 1},
  { 1, 1, 0}, { 1,-1, 0}, { 1, 0, 1},
  { 1, 0,-1}, { 0, 1, 1}, { 0, 1,-1},
  { 1, 1, 1}, { 1, 1,-1}, { 1,-1, 1}, { 1,-1,-1}
        };   ///< lattice directions
  int opposite[q] = {
         0, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13
        }; ///< opposite entry
  T t[q] = {
  (T)8/(T)27,

  (T)2/(T)27, (T)2/(T)27, (T)2/(T)27,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)216, (T)1/(T)216, (T)1/(T)216, (T)1/(T)216,

  (T)2/(T)27, (T)2/(T)27, (T)2/(T)27,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)54, (T)1/(T)54, (T)1/(T)54,
  (T)1/(T)216, (T)1/(T)216, (T)1/(T)216, (T)1/(T)216
        };        ///< lattice weights
  T invCs2 = (T)3;      ///< inverse square of speed of sound
};


template <typename T>struct AdvectionDiffusionD2Q5Descriptor
  : public D2Q5DescriptorBase<T>, public Velocity2dBase {
};

template <typename T> struct D2Q9DescriptorCPU
  : public D2Q9DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct D2Q9DescriptorGPU
  : public D2Q9DescriptorBaseGPU<T>, public NoExternalFieldBaseGPU{
};

template <typename T> struct ForcedD2Q9DescriptorCPU
  : public D2Q9DescriptorBase<T>, public Force2dDescriptorBase {
};

template <typename T> struct ForcedD2Q9DescriptorGPU
  : public D2Q9DescriptorBaseGPU<T>, public Force2dDescriptorBaseGPU{
};

#ifdef D2Q9LATTICE
  #ifdef __CUDACC__
    __constant__ D2Q9DescriptorGPU<T> DescriptorGPU{};
  #endif
#endif

#ifdef FORCEDD2Q9LATTICE
  #ifdef __CUDACC__
    __constant__ ForcedD2Q9DescriptorGPU<T> DescriptorGPU{};
  #endif
#endif

template<typename T> struct D2Q9Descriptor{
  enum{d = 2, q = 9, rhoIndex = q, uIndex = q + 1, dataSize = q+d+1 };
  typedef D2Q9Descriptor<T> BaseDescriptor;
  typedef D2Q9DescriptorCPU<T> CPUBaseDescriptor;

  struct ExternalFieldInternal {
    OPENLB_HOST_DEVICE
    static int numScalars()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.numScalars;
      #else
        return D2Q9DescriptorCPU<T>::ExternalField::numScalars;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int numSpecies()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.numSpecies;
      #else
        return D2Q9DescriptorCPU<T>::ExternalField::numSpecies;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int forceBeginsAt()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.forceBeginsAt;
      #else
        return D2Q9DescriptorCPU<T>::ExternalField::forceBeginsAt;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int sizeOfForce()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.sizeOfForce;
      #else
        return D2Q9DescriptorCPU<T>::ExternalField::sizeOfForce;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int velocityBeginsAt()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.velocityBeginsAt;
      #else
        return D2Q9DescriptorCPU<T>::ExternalField::velocityBeginsAt;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int sizeOfVelocity()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.sizeOfVelocity;
      #else
        return D2Q9DescriptorCPU<T>::ExternalField::sizeOfVelocity;
      #endif
    }
  };

  typedef ExternalFieldInternal ExternalField;

  OPENLB_HOST_DEVICE
  static int vicinity()
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.vicinity;
    #else
      return D2Q9DescriptorCPU<T>::vicinity;
    #endif
  }

  OPENLB_HOST_DEVICE
  static int c(const int q, const int d)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.c[q][d];
    #else
      return D2Q9DescriptorCPU<T>::c[q][d];
    #endif
  }

  OPENLB_HOST_DEVICE
  static int opposite(const int q)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.opposite[q];
    #else
   static constexpr int opposite[9] = {
      0, 5, 6, 7, 8, 1, 2, 3, 4
    }; ///< opposite entry
      return opposite[q];
    #endif
  }

  OPENLB_HOST_DEVICE
  static T t(const int q)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.t[q];
    #else
      return D2Q9DescriptorCPU<T>::t[q];
    #endif
  }

  OPENLB_HOST_DEVICE
  static T invCs2()
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.invCs2;
    #else
      return D2Q9DescriptorCPU<T>::invCs2;
    #endif
  }

};

template <typename T > struct ForcedD2Q9Descriptor{
    enum{d = 2, q = 9, rhoIndex = q, uIndex = q+1, forceIndex = uIndex+d, dataSize = q+d+1+d};
    typedef ForcedD2Q9Descriptor<T> BaseDescriptor;
    typedef ForcedD2Q9DescriptorCPU<T> CPUBaseDescriptor;

    OPENLB_HOST_DEVICE
    static int vicinity()
    { return D2Q9Descriptor<T>::vicinity(); }

    OPENLB_HOST_DEVICE
    static int c(const int q, const int d)
    { return D2Q9Descriptor<T>::c(q,d); }

    OPENLB_HOST_DEVICE
    static int opposite(const int q)
    { return D2Q9Descriptor<T>::opposite(q); }

    OPENLB_HOST_DEVICE
    static T t(const int q)
    { return D2Q9Descriptor<T>::t(q); }

    OPENLB_HOST_DEVICE
    static T invCs2()
    { return D2Q9Descriptor<T>::invCs2(); }

    struct ExternalFieldInternal
    {
        OPENLB_HOST_DEVICE
        static int forceBeginsAt()
        {
          #if __CUDA_ARCH__
            return forceIndex;
          #else
            return forceIndex;
          #endif
        }

        OPENLB_HOST_DEVICE
        static int sizeOfForce()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.sizeOfForce;
          #else
            return ForcedD2Q9DescriptorCPU<T>::ExternalField::sizeOfForce;
          #endif
        }
        OPENLB_HOST_DEVICE
        static int numScalars()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.numScalars;
          #else
            return ForcedD2Q9DescriptorCPU<T>::ExternalField::numScalars;
          #endif
        }

        OPENLB_HOST_DEVICE
        static int numSpecies()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.numSpecies;
          #else
            return ForcedD2Q9DescriptorCPU<T>::ExternalField::numSpecies;
          #endif
        }
    };

    typedef ExternalFieldInternal ExternalField;
};

template <typename T> struct V6ForcedD2Q9Descriptor
  : public D2Q9DescriptorBase<T>, public V6Force2dDescriptorBase {
};

template <typename T> struct AdvectionDiffusionD3Q7Descriptor
  : public D3Q7DescriptorBase<T>, public Velocity3dBase {
};

template <typename T> struct ParticleAdvectionDiffusionD3Q7Descriptor
  : public D3Q7DescriptorBase<T>, public ParticleAdvectionDiffusion3dDescriptorBase {
};

template <typename T> struct D3Q13Descriptor
  : public D3Q13DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct ForcedD3Q13Descriptor
  : public D3Q13DescriptorBase<T>, public Force3dDescriptorBase {
};

template <typename T> struct D3Q15Descriptor
  : public D3Q15DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct ForcedD3Q15Descriptor
  : public D3Q15DescriptorBase<T>, public Force3dDescriptorBase {
};

/// D3Q19

template <typename T> struct D3Q19DescriptorCPU
  : public D3Q19DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct D3Q19DescriptorGPU
  : public D3Q19DescriptorBaseGPU<T>, public NoExternalFieldBaseGPU{
};

template <typename T> struct ForcedD3Q19DescriptorCPU
  : public D3Q19DescriptorBase<T>, public Force3dDescriptorBase {
};

template <typename T> struct ForcedD3Q19DescriptorGPU
  : public D3Q19DescriptorBaseGPU<T>, public Force3dDescriptorBaseGPU{
};

#ifdef D3Q19LATTICE
  #ifdef __CUDACC__
    __constant__ D3Q19DescriptorGPU<T> DescriptorGPU{};
  #endif
#endif

#ifdef FORCEDD3Q19LATTICE
    #ifdef __CUDACC__
        __constant__ ForcedD3Q19DescriptorGPU<T> DescriptorGPU{};
    #endif
#endif

template<typename T> struct D3Q19Descriptor{
  enum{d = 3, q = 19, rhoIndex = q, uIndex = q+1, dataSize = q+d+1};
  typedef D3Q19Descriptor<T> BaseDescriptor;
  typedef D3Q19DescriptorCPU<T> CPUBaseDescriptor;

  struct ExternalFieldInternal {
    OPENLB_HOST_DEVICE
    static int numScalars()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.numScalars;
      #else
        return D3Q19DescriptorCPU<T>::ExternalField::numScalars;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int numSpecies()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.numSpecies;
      #else
        return D3Q19DescriptorCPU<T>::ExternalField::numSpecies;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int forceBeginsAt()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.forceBeginsAt;
      #else
        return D3Q19DescriptorCPU<T>::ExternalField::forceBeginsAt;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int sizeOfForce()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.sizeOfForce;
      #else
        return D3Q19DescriptorCPU<T>::ExternalField::sizeOfForce;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int velocityBeginsAt()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.velocityBeginsAt;
      #else
        return D3Q19DescriptorCPU<T>::ExternalField::velocityBeginsAt;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int sizeOfVelocity()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.sizeOfVelocity;
      #else
        return D3Q19DescriptorCPU<T>::ExternalField::sizeOfVelocity;
      #endif
    }
  };

  typedef ExternalFieldInternal ExternalField;

  OPENLB_HOST_DEVICE
  static int vicinity()
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.vicinity;
    #else
      return D3Q19DescriptorCPU<T>::vicinity;
    #endif
  }

  OPENLB_HOST_DEVICE
  static int c(const int q, const int d)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.c[q][d];
    #else
      return D3Q19DescriptorCPU<T>::c[q][d];
    #endif
  }

  OPENLB_HOST_DEVICE
  static int opposite(const int q)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.opposite[q];
    #else
      return D3Q19DescriptorCPU<T>::opposite[q];
    #endif
  }

  OPENLB_HOST_DEVICE
  static T t(const int q)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.t[q];
    #else
      return D3Q19DescriptorCPU<T>::t[q];
    #endif
  }

  OPENLB_HOST_DEVICE
  static T invCs2()
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.invCs2;
    #else
      return D3Q19DescriptorCPU<T>::invCs2;
    #endif
  }

};

template <typename T > struct ForcedD3Q19Descriptor{
    enum{d = 3, q = 19, rhoIndex = q, uIndex = q+1, forceIndex = uIndex+d, dataSize = q+d+1+d};
    typedef ForcedD3Q19Descriptor<T> BaseDescriptor;
    typedef ForcedD3Q19DescriptorCPU<T> CPUBaseDescriptor;

    OPENLB_HOST_DEVICE
    static int vicinity()
    { return D3Q19Descriptor<T>::vicinity(); }

    OPENLB_HOST_DEVICE
    static int c(const int q, const int d)
    { return D3Q19Descriptor<T>::c(q,d); }

    OPENLB_HOST_DEVICE
    static int opposite(const int q)
    { return D3Q19Descriptor<T>::opposite(q); }

    OPENLB_HOST_DEVICE
    static T t(const int q)
    { return D3Q19Descriptor<T>::t(q); }

    OPENLB_HOST_DEVICE
    static T invCs2()
    { return D3Q19Descriptor<T>::invCs2(); }

    struct ExternalFieldInternal
    {
        OPENLB_HOST_DEVICE
        static int forceBeginsAt()
        {
          #if __CUDA_ARCH__
            return forceIndex;
          #else
            return forceIndex;
          #endif
        }

        OPENLB_HOST_DEVICE
        static int sizeOfForce()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.sizeOfForce;
          #else
            return ForcedD3Q19DescriptorCPU<T>::ExternalField::sizeOfForce;
          #endif
        }
        OPENLB_HOST_DEVICE
        static int numScalars()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.numScalars;
          #else
            return ForcedD3Q19DescriptorCPU<T>::ExternalField::numScalars;
          #endif
        }

        OPENLB_HOST_DEVICE
        static int numSpecies()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.numSpecies;
          #else
            return ForcedD3Q19DescriptorCPU<T>::ExternalField::numSpecies;
          #endif
        }
    };

    typedef ExternalFieldInternal ExternalField;
};

template <typename T> struct V12ForcedD3Q19Descriptor
  : public D3Q19DescriptorBase<T>, public V12Force3dDescriptorBase {
};

template <typename T> struct ParticleAdvectionDiffusionD3Q19Descriptor
  : public D3Q19DescriptorBase<T>, public ParticleAdvectionDiffusion3dDescriptorBase {
};

/// D3Q27
template <typename T> struct D3Q27DescriptorCPU
  : public D3Q27DescriptorBase<T>, public NoExternalFieldBase {
};

template <typename T> struct D3Q27DescriptorGPU
  : public D3Q27DescriptorBaseGPU<T>, public NoExternalFieldBaseGPU{
};

template <typename T> struct ForcedD3Q27DescriptorCPU
  : public D3Q27DescriptorBase<T>, public Force3dDescriptorBase {
};

template <typename T> struct ForcedD3Q27DescriptorGPU
  : public D3Q27DescriptorBaseGPU<T>, public Force3dDescriptorBaseGPU{
};

#ifdef D3Q27LATTICE
  #ifdef __CUDACC__
    __constant__ D3Q27DescriptorGPU<T> DescriptorGPU{};
  #endif
#endif

#ifdef FORCEDD3Q27LATTICE
    #ifdef __CUDACC__
        __constant__ ForcedD3Q27DescriptorGPU<T> DescriptorGPU{};
    #endif
#endif

template<typename T> struct D3Q27Descriptor{
  enum{d = 3, q = 27, rhoIndex = q, uIndex = q+1, dataSize = q+d+1};
  typedef D3Q27Descriptor<T> BaseDescriptor;
  typedef D3Q27DescriptorCPU<T> CPUBaseDescriptor;

  struct ExternalFieldInternal {
    OPENLB_HOST_DEVICE
    static int numScalars()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.numScalars;
      #else
        return D3Q27DescriptorCPU<T>::ExternalField::numScalars;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int numSpecies()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.numSpecies;
      #else
        return D3Q27DescriptorCPU<T>::ExternalField::numSpecies;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int forceBeginsAt()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.forceBeginsAt;
      #else
        return D3Q27DescriptorCPU<T>::ExternalField::forceBeginsAt;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int sizeOfForce()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.sizeOfForce;
      #else
        return D3Q27DescriptorCPU<T>::ExternalField::sizeOfForce;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int velocityBeginsAt()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.velocityBeginsAt;
      #else
        return D3Q27DescriptorCPU<T>::ExternalField::velocityBeginsAt;
      #endif
    }

    OPENLB_HOST_DEVICE
    static int sizeOfVelocity()
    {
      #if __CUDA_ARCH__
        return DescriptorGPU.ExternalFieldGPU.sizeOfVelocity;
      #else
        return D3Q27DescriptorCPU<T>::ExternalField::sizeOfVelocity;
      #endif
    }
  };

  typedef ExternalFieldInternal ExternalField;

  OPENLB_HOST_DEVICE
  static int vicinity()
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.vicinity;
    #else
      return D3Q27DescriptorCPU<T>::vicinity;
    #endif
  }

  OPENLB_HOST_DEVICE
  static int c(const int q, const int d)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.c[q][d];
    #else
      return D3Q27DescriptorCPU<T>::c[q][d];
    #endif
  }

  OPENLB_HOST_DEVICE
  static int opposite(const int q)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.opposite[q];
    #else
      return D3Q27DescriptorCPU<T>::opposite[q];
    #endif
  }

  OPENLB_HOST_DEVICE
  static T t(const int q)
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.t[q];
    #else
      return D3Q27DescriptorCPU<T>::t[q];
    #endif
  }

  OPENLB_HOST_DEVICE
  static T invCs2()
  {
    #if __CUDA_ARCH__
      return DescriptorGPU.invCs2;
    #else
      return D3Q27DescriptorCPU<T>::invCs2;
    #endif
  }

};

template <typename T > struct ForcedD3Q27Descriptor{
    enum{d = 3, q = 27, rhoIndex = q, uIndex = q+1, forceIndex = uIndex+d, dataSize = q+d+1+d};
    typedef ForcedD3Q27Descriptor<T> BaseDescriptor;
    typedef ForcedD3Q27DescriptorCPU<T> CPUBaseDescriptor;

    OPENLB_HOST_DEVICE
    static int vicinity()
    { return D3Q27Descriptor<T>::vicinity(); }

    OPENLB_HOST_DEVICE
    static int c(const int q, const int d)
    { return D3Q27Descriptor<T>::c(q,d); }

    OPENLB_HOST_DEVICE
    static int opposite(const int q)
    { return D3Q27Descriptor<T>::opposite(q); }

    OPENLB_HOST_DEVICE
    static T t(const int q)
    { return D3Q27Descriptor<T>::t(q); }

    OPENLB_HOST_DEVICE
    static T invCs2()
    { return D3Q27Descriptor<T>::invCs2(); }

    struct ExternalFieldInternal
    {
        OPENLB_HOST_DEVICE
        static int forceBeginsAt()
        {
          #if __CUDA_ARCH__
            return forceIndex;
          #else
            return forceIndex;
          #endif
        }

        OPENLB_HOST_DEVICE
        static int sizeOfForce()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.sizeOfForce;
          #else
            return ForcedD3Q27DescriptorCPU<T>::ExternalField::sizeOfForce;
          #endif
        }
        OPENLB_HOST_DEVICE
        static int numScalars()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.numScalars;
          #else
            return ForcedD3Q27DescriptorCPU<T>::ExternalField::numScalars;
          #endif
        }

        OPENLB_HOST_DEVICE
        static int numSpecies()
        {
          #if __CUDA_ARCH__
            return DescriptorGPU.ExternalFieldGPU.numSpecies;
          #else
            return ForcedD3Q27DescriptorCPU<T>::ExternalField::numSpecies;
          #endif
        }
    };

    typedef ExternalFieldInternal ExternalField;
};

}  // namespace descriptors

}  // namespace olb

#endif
