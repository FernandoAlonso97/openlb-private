/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Helper functions for the implementation of LB dynamics. This file is all
 * about efficiency. The generic template code is specialized for commonly
 * used Lattices, so that a maximum performance can be taken out of each
 * case.
 */
#ifndef LB_HELPERS_H
#define LB_HELPERS_H

#include "latticeDescriptors.h"
#include "core/cell.h"
#include "core/util.h"
#include "core/config.h"
#include "core/metaTemplateHelpers.h"
#include "dynamics/firstOrderLbHelpers.h"


namespace olb {

// Forward declarations
template<typename T, class Descriptor> struct lbDynamicsHelpers;
template<typename T, template<typename U> class Lattice> struct lbExternalHelpers;
template<typename T, template<typename U> class Lattice> struct lbLatticeHelpers;
template<typename T, class Descriptor> class CellBase;

//Forward declarations by Shreyas
template<typename T, class Descriptor> class CellBase;
template<typename T, template<typename U> class Lattice> class CellView;

/// This structure forwards the calls to the appropriate helper class
template<typename T, template<typename U> class Lattice>
struct lbHelpers {

  OPENLB_HOST_DEVICE
  static T equilibrium(int iPop, T const rho, const T u[Lattice<T>::d], const T uSqr)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::equilibrium(iPop, rho, u, uSqr);
  }

  OPENLB_HOST_DEVICE
  static T equilibriumFirstOrder(int iPop, T rho, const T u[Lattice<T>::d])
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::equilibriumFirstOrder(iPop, rho, u);
  }

  OPENLB_HOST_DEVICE
  static T incEquilibrium(int iPop, const T j[Lattice<T>::d], const T jSqr, const T pressure)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::incEquilibrium(iPop, j, jSqr, pressure);
  }

  OPENLB_HOST_DEVICE
  static void computeFneq ( CellView<T,Lattice> const& cell,
                            T fNeq[Lattice<T>::q], T rho, const T u[Lattice<T>::d] )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>::computeFneq(cell, fNeq, rho, u);
  }

  OPENLB_HOST_DEVICE
  static void computeFneq ( const T * const OPENLB_RESTRICT cellData,
                            T * const OPENLB_RESTRICT fNeq, T rho, const T * const OPENLB_RESTRICT u )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>::computeFneq(cellData, fNeq, rho, u);
  }
  OPENLB_HOST_DEVICE
  static void computeFneq ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT fNeq, T rho, const T * const OPENLB_RESTRICT u )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>::computeFneq(cellData,cellIndex, fNeq, rho, u);
  }

  OPENLB_HOST_DEVICE
  static T bgkCollision(CellView<T,Lattice>& cell, T const& rho, const T u[Lattice<T>::d], T const& omega)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::bgkCollision(cell, rho, u, omega);
  }

  OPENLB_HOST_DEVICE
  static T bgkCollision(CellView<T,Lattice>& cell, T const& rho, const T u[Lattice<T>::d], const T omega[Lattice<T>::q])
  {
    const T uSqr = util::normSqr<T,Lattice<T>::d>(u);
    for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
      cell[iPop] *= (T)1-omega[iPop];
      cell[iPop] += omega[iPop] * lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>::equilibrium (iPop, rho, u, uSqr );
    }
    return uSqr;
  }

  OPENLB_HOST_DEVICE
  static T bgkCollision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T const& rho, const T * const OPENLB_RESTRICT u, T const omega)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::bgkCollision(cellData, cellIndex, rho, u, omega);
  }

  OPENLB_HOST_DEVICE
  static T bgkCollision(T * const OPENLB_RESTRICT cellData, T const rho, const T * const OPENLB_RESTRICT u, T const omega)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::bgkCollision(cellData, rho, u, omega);
  }

  //====================================
  /*
   * Helpers included from the adLbDynamicsHelpers
   */
  OPENLB_HOST_DEVICE
  static T rlbCollision( CellView<T, Lattice>& cell, T rho, const T u[Lattice<T>::d], T omega )
  {
    return lbDynamicsHelpers<T, typename Lattice<T>::BaseDescriptor>
           ::rlbCollision( cell, rho, u, omega );
  }

  OPENLB_HOST_DEVICE
  static T sinkCollision( CellView<T, Lattice>& cell, T intensity, T omega, T sink )
  {
    return lbDynamicsHelpers<T, typename Lattice<T>::BaseDescriptor>
           ::sinkCollision( cell, intensity, omega, sink );
  }

  OPENLB_HOST_DEVICE
  static T anisoCollision( CellView<T, Lattice>& cell, T rho, T absorption, T scattering, T scaling )
  {
    return lbDynamicsHelpers<T, typename Lattice<T>::BaseDescriptor>
           ::anisoCollision( cell, rho, absorption, scattering, scaling );
  }

  //====================================
  /*
   * Helpers included from the advectionDiffusionMRTlbHelpers
   */
  OPENLB_HOST_DEVICE
  static T mrtCollision( CellView<T, Lattice>& cell, T const& rho, const T u[Lattice<T>::d], const T invM_S[Lattice<T>::q][Lattice<T>::q] ) {

    return lbDynamicsHelpers<T, typename Lattice<T>::BaseDescriptor>
           ::mrtCollision(cell, rho, u, invM_S );
  }
  //====================================

  OPENLB_HOST_DEVICE
  static T incBgkCollision(CellView<T,Lattice>& cell, T pressure, const T j[Lattice<T>::d], T omega)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::incBgkCollision(cell, pressure, j, omega);
  }

  OPENLB_HOST_DEVICE
  static T constRhoBgkCollision(CellView<T,Lattice>& cell,
                                T rho, const T u[Lattice<T>::d], T ratioRho, T omega)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::constRhoBgkCollision(cell, rho, u, ratioRho, omega);
  }

  template<int size>
  static void constRhoBgkCollision(CellDataArray<T,Lattice>& data, const bool* valid,
                                T rho[size], const T u[Lattice<T>::d][size], T ratioRho[size], T omega, T uSqr[size])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>::template constRhoBgkCollision<size>(data.data, valid, rho, u, ratioRho, omega, uSqr);
  }


  OPENLB_HOST_DEVICE
  static T constRhoBgkCollision(CellDataArray<T,Lattice> data, int i,
                                T rho, const T u[Lattice<T>::d], T ratioRho, T omega, Lattice<T> const & latticeDescriptor)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::constRhoBgkCollision(data.data, i, rho, u, ratioRho, omega, latticeDescriptor);
  }

  OPENLB_HOST_DEVICE
  static T computeRho(CellView<T,Lattice> const& cell)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::computeRho(cell);
  }

  OPENLB_HOST_DEVICE
  static T computeRho(CellDataArray<T,Lattice> const& data)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::computeRho(data.data);
  }
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::computeRho(cellData);
  }
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
  {
    return lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
           ::computeRho(cellData, cellIndex);
  }

  OPENLB_HOST_DEVICE
  static void computeJ(CellView<T,Lattice> const& cell, T j[Lattice<T>::d] )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeJ(cell, j);
  }
  OPENLB_HOST_DEVICE
  static void computeJ(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT j )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeJ(cellData, j);
  }
  OPENLB_HOST_DEVICE
  static void computeJ(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,  T * const OPENLB_RESTRICT j )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeJ(cellData,cellIndex, j);
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeRhoU(cell, rho, u);
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(CellDataArray<T,Lattice> const& data, T& rho, T u[Lattice<T>::d])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeRhoU(data.data, rho, u);
  }

  template<int width>
  static void computeRhoU(CellDataArray<T,Lattice> const& data, T* rho, T u[Lattice<T>::d][width])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::template computeRhoU<width>(data.data, rho, u);
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(T** cellData, size_t cellIndex, T& rho, T u[Lattice<T>::d])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeRhoU(cellData, cellIndex, rho, u);
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT cellData, T& rho, T * const OPENLB_RESTRICT u)
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeRhoU(cellData, rho, u);
  }
  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u)
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeRhoU(cellData,cellIndex, rho, u);
  }

  OPENLB_HOST_DEVICE
  static void computeRhoJ(CellView<T,Lattice> const& cell, T& rho, T j[Lattice<T>::d])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeRhoJ(cell, rho, j);
  }

  OPENLB_HOST_DEVICE
  static void computeStress(CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
                            T pi[util::TensorVal<Lattice<T> >::n] )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeStress(cell, rho, u, pi);
  }

  OPENLB_HOST_DEVICE
  static void computeStress(CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d],
                            T pi[util::TensorVal<Lattice<T> >::n] )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeStress(data.data, rho, u, pi);
  }
  OPENLB_HOST_DEVICE
  static void computeStress(T** cellData, size_t cellIndex, T rho, const T u[Lattice<T>::d],
                            T pi[util::TensorVal<Lattice<T> >::n] )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeStress(cellData, cellIndex, rho, u, pi);
  }

  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT cellData, T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi)
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeStress(cellData, rho, u, pi);
  }
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi)
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeStress(cellData,cellIndex, rho, u, pi);
  }

  OPENLB_HOST_DEVICE
  static void computeAllMomenta(CellView<T,Lattice> const& cell, T& rho, T u[Lattice<T>::d],
                                T pi[util::TensorVal<Lattice<T> >::n] )
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::computeAllMomenta(cell, rho, u, pi);
  }

  OPENLB_HOST_DEVICE
  static void modifyVelocity(CellView<T,Lattice>& cell, const T newU[Lattice<T>::d])
  {
    lbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>
    ::modifyVelocity(cell, newU);
  }

  OPENLB_HOST_DEVICE
  static void addExternalForce(CellView<T,Lattice>& cell, const T u[Lattice<T>::d], T omega, T amplitude=(T)1)
  {
    lbExternalHelpers<T,Lattice>::addExternalForce(cell, u, omega, amplitude);
  }

  OPENLB_HOST_DEVICE
  static void addExternalForce(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
          const T * const OPENLB_RESTRICT u, T omega, T amplitude = 1)
  {
    lbExternalHelpers<T,Lattice>::addExternalForce(cellData, cellIndex, u, omega, amplitude);
  }

  OPENLB_HOST_DEVICE
  static void addExternalForce(T * const OPENLB_RESTRICT cellData,
          const T * const OPENLB_RESTRICT u, T omega, T amplitude = 1)
  {
    lbExternalHelpers<T,Lattice>::addExternalForce(cellData, u, omega, amplitude);
  }

  OPENLB_HOST_DEVICE
  static void swapAndStream2D(CellView<T,Lattice> **grid, int iX, int iY)
  {
    lbLatticeHelpers<T,Lattice>::swapAndStream2D(grid, iX, iY);
  }

  OPENLB_HOST_DEVICE
  static void swapAndStream3D(CellView<T,Lattice> ***grid, int iX, int iY, int iZ)
  {
    lbLatticeHelpers<T,Lattice>::swapAndStream3D(grid, iX, iY, iZ);
  }

};  // struct lbHelpers


/// All helper functions are inside this structure
template<typename T, class Descriptor>
struct lbDynamicsHelpers {
  /// Computation of equilibrium distribution
  OPENLB_HOST_DEVICE
  static T equilibrium(int iPop, T const rho, const T u[Descriptor::d], const T uSqr)
  {
    T c_u = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      c_u += Descriptor::c(iPop,iD)*u[iD];
    }
    return rho * Descriptor::t(iPop) * ( (T)1 + Descriptor::invCs2() * c_u
                                              + Descriptor::invCs2() * Descriptor::invCs2() * (T)0.5 * c_u *c_u
                                              - Descriptor::invCs2() * (T)0.5 * uSqr )
                                              - Descriptor::t(iPop);
  }

  /// First order computation of equilibrium distribution
  // TODO AP: check if this equilibrium function is the needed one
  // -> 12.09.2017: the implementation is taken from advectionDiffusionLbHelpers.h
  OPENLB_HOST_DEVICE
  static T equilibriumFirstOrder(int iPop, T rho, const T u[Descriptor::d])
  {
    return rho * Descriptor::t[iPop] - Descriptor::t[iPop];
  }

  OPENLB_HOST_DEVICE
  static T incEquilibrium( int iPop, const T j[Descriptor::d],
                           const T jSqr, const T pressure )
  {
    T c_j = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      c_j += Descriptor::c(iPop)[iD]*j[iD];
    }
    T rho = (T)1 + pressure*Descriptor::invCs2;

    return Descriptor::t[iPop] * ( Descriptor::invCs2 * pressure +
                                   Descriptor::invCs2 * c_j +
                                   Descriptor::invCs2 * Descriptor::invCs2/(T)2 * c_j*c_j -
                                   Descriptor::invCs2/(T)2 * jSqr
                                 ) - Descriptor::t[iPop];
  }

  OPENLB_HOST_DEVICE
  static void computeFneq(CellBase<T,Descriptor> const& cell, T fNeq[Descriptor::q], T rho, const T u[Descriptor::d])
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      fNeq[iPop] = cell[iPop] - equilibrium(iPop, rho, u, uSqr);
    }
  }

  OPENLB_HOST_DEVICE
  static void computeFneq(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT fNeq, T rho, const T * const OPENLB_RESTRICT u)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      fNeq[iPop] = cellData[iPop] - equilibrium(iPop, rho, u, uSqr);
    }
  }
  OPENLB_HOST_DEVICE
  static void computeFneq(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT fNeq, T rho, const T * const OPENLB_RESTRICT u)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      fNeq[iPop] = cellData[iPop][cellIndex] - equilibrium(iPop, rho, u, uSqr);
    }
  }

  /// BGK collision step
  OPENLB_HOST_DEVICE
  static T bgkCollision(CellBase<T,Descriptor>& cell, T const& rho, const T u[Descriptor::d], T const& omega)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      cell[iPop] *= (T)1-omega;
      cell[iPop] += omega * lbDynamicsHelpers<T,Descriptor>::equilibrium(iPop, rho, u, uSqr );
    }
    return uSqr;
  }

  /// BGK collision step with T**
  OPENLB_HOST_DEVICE
  static T bgkCollision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t cellIndex, T const& rho, const T * const OPENLB_RESTRICT u, T const omega)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop)
    {
      cellData[iPop][cellIndex] *=  (T)1-omega;
      cellData[iPop][cellIndex] += omega * lbDynamicsHelpers<T,Descriptor>::equilibrium(iPop, rho, u, uSqr );
    }

    T tmp[Descriptor::q/2+1];

    for (int i=1; i<=Descriptor::q/2; ++i)
    tmp[i] = cellData[i][cellIndex];

    for (int i=1; i<=Descriptor::q/2; ++i)
    cellData[i][cellIndex] = cellData[Descriptor::opposite(i)][cellIndex];

    for (int i=1; i<=Descriptor::q/2; ++i)
    cellData[Descriptor::opposite(i)][cellIndex] = tmp[i];

    return uSqr;
  }

  /// BGK collision step with T OPENLB_RESTRICT
  OPENLB_HOST_DEVICE
  static T bgkCollision(T * const OPENLB_RESTRICT cellData, T const rho, const T OPENLB_RESTRICT u[Descriptor::d], T const omega)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      cellData[iPop] *=  (T)1-omega;
      cellData[iPop] += omega * lbDynamicsHelpers<T,Descriptor>::equilibrium(iPop, rho, u, uSqr );
    }

    T tmp[Descriptor::q/2+1];

    for (int i=1; i<=Descriptor::q/2; ++i)
    tmp[i] = cellData[i];

    for (int i=1; i<=Descriptor::q/2; ++i)
    cellData[i] = cellData[Descriptor::opposite(i)];

    for (int i=1; i<=Descriptor::q/2; ++i)
    cellData[Descriptor::opposite(i)] = tmp[i];
    
    return uSqr;
  }

  /// Incompressible BGK collision step
  OPENLB_HOST_DEVICE
  static T incBgkCollision(CellBase<T,Descriptor>& cell, T pressure, const T j[Descriptor::d], T omega)
  {
    const T jSqr = util::normSqr<T,Descriptor::d>(j);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      cell[iPop] *= (T)1-omega;
      cell[iPop] += omega * lbDynamicsHelpers<T,Descriptor>::incEquilibrium (
                      iPop, j, jSqr, pressure );
    }
    return jSqr;
  }

  /// BGK collision step with density correction
  OPENLB_HOST_DEVICE
  static T constRhoBgkCollision(CellBase<T,Descriptor>& cell, T rho, const T u[Descriptor::d], T ratioRho, T omega)
  {
    const T uSqr = util::normSqr<T,Descriptor::d>(u);
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      T feq = lbDynamicsHelpers<T,Descriptor>::equilibrium(iPop, rho, u, uSqr );
      cell[iPop] =
        ratioRho*(feq+Descriptor::t(iPop))-Descriptor::t(iPop) +
        ((T)1-omega)*(cell[iPop]-feq);
    }
    return uSqr;
  }

  //========================================

  /// RLB advection diffusion collision step
  OPENLB_HOST_DEVICE
  static T rlbCollision(CellBase<T, Descriptor>& cell, T rho, const T u[Descriptor::d], T omega )
  {
    const T uSqr = util::normSqr<T, Descriptor::d>( u );
    // First-order moment for the regularization
    T j1[Descriptor::d];
    for ( int iD = 0; iD < Descriptor::d; ++iD ) {
      j1[iD] = T();
    }

    T fEq[Descriptor::q];
    for ( int iPop = 0; iPop < Descriptor::q; ++iPop ) {
      fEq[iPop] = lbDynamicsHelpers<T, Descriptor>::equilibriumFirstOrder( iPop, rho, u );
      for ( int iD = 0; iD < Descriptor::d; ++iD ) {
        j1[iD] += Descriptor::c(iPop)[iD] * ( cell[iPop] - fEq[iPop] );
      }
    }

    // Collision step
    for ( int iPop = 0; iPop < Descriptor::q; ++iPop ) {
      T fNeq = T();
      for ( int iD = 0; iD < Descriptor::d; ++iD ) {
        fNeq += Descriptor::c(iPop)[iD] * j1[iD];
      }
      fNeq *= Descriptor::t[iPop] * Descriptor::invCs2;
      cell[iPop] = fEq[iPop] + ( (T)1 - omega ) * fNeq;
    }
    return uSqr;
  }

  /// Paper: Mink et al. 2016 DOI: 10.1016/j.jocs.2016.03.014
  OPENLB_HOST_DEVICE
  static T sinkCollision(CellBase<T, Descriptor>& cell, T intensity, T omega, T sink){
    const T uSqr =  0;
    // place holder
    // see implementation at advectionDiffusionLbHelpers3D.hh
    // -> 12.09.2017 AP: all functionalities of advectionDiffusionLbHelpers3D.hh are shifted to lbHelpersD3Q7.hh
    // collision step only valid for D3Q7 lattices, so we use spezialisation
    std::cout << "Attention: collision is disabled" << std::endl;
    return uSqr;
  }

  OPENLB_HOST_DEVICE
  static T anisoCollision(CellBase<T, Descriptor>& cell, T rhoBGK, T absorption, T scattering, T scaling)
  {
    const T uSqr =  0;
    //    T rho[Descriptor::q];
    //    for (int iPop = 0; iPop < Descriptor::q; ++iPop) {
    //      rho[iPop] = T(0);
    //    }
    //
    //    for (int iPop = 0; iPop < Descriptor::q; iPop++) {
    //      for ( int jPop = 0; jPop < Descriptor::q; ++jPop ) {
    //        rho[iPop] += (cell[jPop] + Descriptor::t[jPop]); // * Descriptor::henyeyPhaseFunction[jPop][iPop];
    //      }
    //    }

    //    double feq = adLbDynamicsHelpers<T, Descriptor>::equilibriumFirstOrder(iPop, rhoBGK, 0.0);
    //    for (int iPop = 0; iPop < Descriptor::q; ++iPop) {
    //      cell[iPop] = (cell[iPop] + Descriptor::t[iPop])
    //                   - ( cell[iPop] + Descriptor::t[iPop] - rhoBGK * Descriptor::t[iPop] )
    //                   - absorption/scattering * scaling * ( cell[iPop] + Descriptor::t[iPop] )
    //                   - Descriptor::t[iPop];
    //    }

    for (int iPop = 0; iPop < Descriptor::q; ++iPop) {
    cell[iPop] = (cell[iPop] + Descriptor::t[iPop])
        - scattering * scaling * ( cell[iPop] + Descriptor::t[iPop] - rhoBGK * Descriptor::t[iPop] )
        - absorption * scaling * ( cell[iPop] + Descriptor::t[iPop] )
        - Descriptor::t[iPop];
    }

    // algo MINK
    //    for ( int iPop = 0; iPop < Descriptor::q; ++iPop ) {
    //      cell[iPop] = rhoBGK * Descriptor::t[iPop] - Descriptor::t[iPop];
    //    }
    return uSqr;
  }

  //====================================
  /*
   * Helpers included from the advectionDiffusionMRTlbHelpers
   */
  OPENLB_HOST_DEVICE
  static T mrtCollision( CellBase<T,Descriptor>& cell, const T rho, const T u[Descriptor::d], const T invM_S[Descriptor::q][Descriptor::q] )
  {
    std::cout<< "Empty function" << std::endl;
    const T uSqr =  0;
//// Implemented in advectionDiffusionMRTlbHelpers2D.h and advectionDiffusionMRTlbHelpers3D.h
//    T uSqr = util::normSqr<T, Descriptor::d>(u);
//    T momenta[Descriptor::q];
//    T momentaEq[Descriptor::q];
//
//    computeMomenta(momenta, cell);
//    computeEquilibrium(momentaEq, rho, u, uSqr);
//
//    for (int iPop = 0; iPop < Descriptor::q; ++iPop) {
//      T collisionTerm = T();
//      for (int jPop = 0; jPop < Descriptor::q; ++jPop) {
//        collisionTerm += invM_S[iPop][jPop] * (momenta[jPop] - momentaEq[jPop]);
//      }
//      cell[iPop] -= collisionTerm;
//    }
    return uSqr;
  }
  //========================================

  /// Computation of density
  OPENLB_HOST_DEVICE
  static T computeRho(CellBase<T,Descriptor> const& cell)
  {
    T rho = T();
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cell[iPop];
    }
    rho += (T)1;
    return rho;
  }

  OPENLB_HOST_DEVICE
  static T computeRho(const T** cell)
  {
    T rho = T();
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cell[iPop][0];
    }
    rho += (T)1;
    return rho;
  }
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT cellData)
  {
    T rho = T();
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop];
    }
    rho += (T)1;
    return rho;
  }
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex)
  {
    T rho = T();
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop][cellIndex];
    }
    rho += (T)1;
    return rho;
  }

  /// Computation of momentum
  OPENLB_HOST_DEVICE
  static void computeJ(CellBase<T,Descriptor> const& cell, T j[Descriptor::d])
  {
    for (int iD=0; iD < Descriptor::d; ++iD) {
      j[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      for (int iD=0; iD < Descriptor::d; ++iD) {
        j[iD] += cell[iPop]*Descriptor::c(iPop,iD);
      }
    }
  }
  /// Computation of momentum
  OPENLB_HOST_DEVICE
  static void computeJ(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT j)
  {
    for (int iD=0; iD < Descriptor::d; ++iD) {
      j[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      for (int iD=0; iD < Descriptor::d; ++iD) {
        j[iD] += cellData[iPop]*Descriptor::c(iPop)[iD];
      }
    }
  }
  OPENLB_HOST_DEVICE
  static void computeJ(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j)
  {
    for (int iD=0; iD < Descriptor::d; ++iD) {
      j[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      for (int iD=0; iD < Descriptor::d; ++iD) {
        j[iD] += cellData[iPop][cellIndex]*Descriptor::c(iPop,iD);
      }
    }
  }

  /// Computation of hydrodynamic variables
  OPENLB_HOST_DEVICE
  static void computeRhoU(CellBase<T,Descriptor> const& cell, T& rho, T u[Descriptor::d])
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cell[iPop];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        u[iD] += cell[iPop]*Descriptor::c(iPop,iD);
      }
    }
    rho += (T)1;
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] /= rho;
    }
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(T** cellData, size_t cellIndex, T& rho, T u[Descriptor::d])
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop][cellIndex];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        u[iD] += cellData[iPop][cellIndex]*Descriptor::c(iPop,iD);
      }
    }
    rho += (T)1;
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] /= rho;
    }
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT cellData, T& rho, T * const OPENLB_RESTRICT u)
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        u[iD] += cellData[iPop]*Descriptor::c(iPop,iD);
      }
    }
    rho += (T)1;
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] /= rho;
    }
  }
  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u)
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop][cellIndex];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        u[iD] += cellData[iPop][cellIndex]*Descriptor::c(iPop,iD);
      }
    }
    rho += (T)1;
    for (int iD=0; iD < Descriptor::d; ++iD) {
      u[iD] /= rho;
    }
  }

  /// Computation of hydrodynamic variables
  OPENLB_HOST_DEVICE
  static void computeRhoJ(CellBase<T,Descriptor> const& cell, T& rho, T j[Descriptor::d])
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      j[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cell[iPop];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        j[iD] += cell[iPop]*Descriptor::c(iPop)[iD];
      }
    }
    rho += (T)1;
  }
  /// Computation of hydrodynamic variables
  OPENLB_HOST_DEVICE
  static void computeRhoJ(const T * const OPENLB_RESTRICT cellData, T& rho, T * const OPENLB_RESTRICT j)
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      j[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        j[iD] += cellData[iPop]*Descriptor::c(iPop)[iD];
      }
    }
    rho += (T)1;
  }
  OPENLB_HOST_DEVICE
  static void computeRhoJ(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT j)
  {
    rho = T();
    for (int iD=0; iD < Descriptor::d; ++iD) {
      j[iD] = T();
    }
    for (int iPop=0; iPop < Descriptor::q; ++iPop) {
      rho += cellData[iPop][cellIndex];
      for (int iD=0; iD < Descriptor::d; ++iD) {
        j[iD] += cellData[iPop][cellIndex]*Descriptor::c(iPop)[iD];
      }
    }
    rho += (T)1;
  }

  /// Computation of stress tensor
  OPENLB_HOST_DEVICE
  static void computeStress(CellBase<T,Descriptor> const& cell, T rho, const T u[Descriptor::d],
                            T pi[util::TensorVal<Descriptor>::n] )
  {
    int iPi = 0;
    for (int iAlpha=0; iAlpha < Descriptor::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta < Descriptor::d; ++iBeta) {
        pi[iPi] = T();
        for (int iPop=0; iPop < Descriptor::q; ++iPop) {
          pi[iPi] += Descriptor::c(iPop,iAlpha)*
                     Descriptor::c(iPop,iBeta) * cell[iPop];
        }
        // stripe off equilibrium contribution
        pi[iPi] -= rho*u[iAlpha]*u[iBeta];
        if (iAlpha==iBeta) {
          pi[iPi] -= 1./Descriptor::invCs2()*(rho-(T)1);
        }
        ++iPi;
      }
    }
  }

  /// Computation of stress tensor
  OPENLB_HOST_DEVICE
  static void computeStress(T** data, T rho, const T u[Descriptor::d],
                            T pi[util::TensorVal<Descriptor>::n] )
  {
    int iPi = 0;
    for (int iAlpha=0; iAlpha < Descriptor::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta < Descriptor::d; ++iBeta) {
        pi[iPi] = T();
        for (int iPop=0; iPop < Descriptor::q; ++iPop) {
          pi[iPi] += Descriptor::c(iPop)[iAlpha]*
                     Descriptor::c(iPop)[iBeta] * data[iPop][0];
        }
        // stripe off equilibrium contribution
        pi[iPi] -= rho*u[iAlpha]*u[iBeta];
        if (iAlpha==iBeta) {
          pi[iPi] -= 1./Descriptor::invCs2*(rho-(T)1);
        }
        ++iPi;
      }
    }
  }
  /// Computation of stress tensor
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T u[Descriptor::d],
                            T pi[util::TensorVal<Descriptor>::n] )
  {
    int iPi = 0;
    for (int iAlpha=0; iAlpha < Descriptor::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta < Descriptor::d; ++iBeta) {
        pi[iPi] = T();
        for (int iPop=0; iPop < Descriptor::q; ++iPop) {
          pi[iPi] += Descriptor::c(iPop,iAlpha)*
                     Descriptor::c(iPop,iBeta) * cellData[iPop][cellIndex];
        }
        // stripe off equilibrium contribution
        pi[iPi] -= rho*u[iAlpha]*u[iBeta];
        if (iAlpha==iBeta) {
          pi[iPi] -= 1./Descriptor::invCs2()*(rho-(T)1);
        }
        ++iPi;
      }
    }
  }
  /// Computation of stress tensor
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT cellData, T rho, const T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
  {
    int iPi = 0;
    for (int iAlpha=0; iAlpha < Descriptor::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta < Descriptor::d; ++iBeta) {
        pi[iPi] = T();
        for (int iPop=0; iPop < Descriptor::q; ++iPop) {
          pi[iPi] += Descriptor::c(iPop,iAlpha)*
                     Descriptor::c(iPop,iBeta) * cellData[iPop];
        }
        // stripe off equilibrium contribution
        pi[iPi] -= rho*u[iAlpha]*u[iBeta];
        if (iAlpha==iBeta) {
          pi[iPi] -= 1./Descriptor::invCs2()*(rho-(T)1);
        }
        ++iPi;
      }
    }
  }

  /// Computation of all hydrodynamic variables
  OPENLB_HOST_DEVICE
  static void computeAllMomenta(CellBase<T,Descriptor> const& cell, T& rho, T u[Descriptor::d],
                                T pi[util::TensorVal<Descriptor>::n] )
  {
    computeRhoU(cell, rho, u);
    computeStress(cell, rho, u, pi);
  }
  /// Computation of all hydrodynamic variables
  OPENLB_HOST_DEVICE
  static void computeAllMomenta(const T * const OPENLB_RESTRICT cellData, T& rho, T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
  {
    computeRhoU(cellData, rho, u);
    computeStress(cellData, rho, u, pi);
  }

  static void modifyVelocity(CellBase<T,Descriptor>& cell, const T newU[Descriptor::d])
  {
    T rho, oldU[Descriptor::d];
    computeRhoU(cell, rho, oldU);
    const T oldUSqr = util::normSqr<T,Descriptor::d>(oldU);
    const T newUSqr = util::normSqr<T,Descriptor::d>(newU);
    for (int iPop=0; iPop<Descriptor::q; ++iPop) {
      cell[iPop] = cell[iPop]
                   - equilibrium(iPop, rho, oldU, oldUSqr)
                   + equilibrium(iPop, rho, newU, newUSqr);
    }
  }
  OPENLB_HOST_DEVICE
  static void computeAllMomenta(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u,
  T * const OPENLB_RESTRICT pi )
  {
    computeRhoU(cellData,cellIndex, rho, u);
    computeStress(cellData,cellIndex, rho, u, pi);
  }


};  // struct lbDynamicsHelpers

/// Helper functions for dynamics that access external field
template<typename T, template<typename U> class Lattice>
struct lbExternalHelpers {
  /// Add a force term after BGK collision
  OPENLB_HOST_DEVICE
  static void addExternalForce(CellView<T,Lattice>& cell, const T u[Lattice<T>::d], T omega, T amplitude)
  {
    static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt();
    T* force = cell.getExternal(forceBeginsAt);
    for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
      T c_u = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        c_u += Lattice<T>::c(iPop,iD)*u[iD];
      }
      c_u *= Lattice<T>::invCs2()*Lattice<T>::invCs2();
      T forceTerm = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        forceTerm +=
          (   ((T)Lattice<T>::c(iPop,iD)-u[iD]) * Lattice<T>::invCs2()
              + c_u * Lattice<T>::c(iPop,iD)
          )
          * force[iD];
      }
      forceTerm *= Lattice<T>::t(iPop);
      forceTerm *= T(1) - omega/T(2);
      forceTerm *= amplitude;
      cell[iPop] += forceTerm;
    }
  }

  OPENLB_HOST_DEVICE
  static void addExternalForce(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex,
          const T * const OPENLB_RESTRICT u, T omega, T amplitude)
  {
    for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
      T c_u = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        c_u += Lattice<T>::c(iPop,iD)*u[iD];
      }
      c_u *= Lattice<T>::invCs2()*Lattice<T>::invCs2();
      T forceTerm = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        forceTerm +=
          (   ((T)Lattice<T>::c(iPop,iD)-u[iD]) * Lattice<T>::invCs2()
              + c_u * Lattice<T>::c(iPop,iD)
          )
          * cellData[Lattice<T>::forceIndex+iD][cellIndex];
      }
      forceTerm *= Lattice<T>::t(iPop);
      forceTerm *= T(1) - omega/T(2);
      forceTerm *= amplitude;
      // cellData is already reverted -> we have to write to opposite
      cellData[Lattice<T>::opposite(iPop)][cellIndex] += forceTerm;
    }
  }

  OPENLB_HOST_DEVICE
  static void addExternalForce(T * const OPENLB_RESTRICT cellData,
          const T * const OPENLB_RESTRICT u, T omega, T amplitude)
  {
    for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
      T c_u = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        c_u += Lattice<T>::c(iPop,iD)*u[iD];
      }
      c_u *= Lattice<T>::invCs2()*Lattice<T>::invCs2();
      T forceTerm = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        forceTerm +=
          (   ((T)Lattice<T>::c(iPop,iD)-u[iD]) * Lattice<T>::invCs2()
              + c_u * Lattice<T>::c(iPop,iD)
          )
          * cellData[Lattice<T>::forceIndex+iD];
      }
      forceTerm *= Lattice<T>::t(iPop);
      forceTerm *= T(1) - omega/T(2);
      forceTerm *= amplitude;
      // cellData is already reverted -> we have to write to opposite
      cellData[iPop] += forceTerm;
    }
  }

};  // struct externalFieldHelpers

/// Helper functions with full-lattice access
template<typename T, template<typename U> class Lattice>
struct lbLatticeHelpers {
  /// Swap ("bounce-back") values of a cell (2D), and apply streaming step
  OPENLB_HOST_DEVICE
  static void swapAndStream2D(CellView<T,Lattice> **grid, int iX, int iY)
  {
    const int half = Lattice<T>::q/2;
    for (int iPop=1; iPop<=half; ++iPop) {
      int nextX = iX + Lattice<T>::c(iPop)[0];
      int nextY = iY + Lattice<T>::c(iPop)[1];
      T fTmp                   = grid[iX][iY][iPop];
      grid[iX][iY][iPop]       = grid[iX][iY][iPop+half];
      grid[iX][iY][iPop+half]  = grid[nextX][nextY][iPop];
      grid[nextX][nextY][iPop] = fTmp;
    }
  }

  /// Swap ("bounce-back") values of a cell (3D), and apply streaming step
  OPENLB_HOST_DEVICE
  static void swapAndStream3D(CellView<T,Lattice> ***grid,
                              int iX, int iY, int iZ)
  {
    const int half = Lattice<T>::q/2;
    for (int iPop=1; iPop<=half; ++iPop) {
      int nextX = iX + Lattice<T>::c(iPop)[0];
      int nextY = iY + Lattice<T>::c(iPop)[1];
      int nextZ = iZ + Lattice<T>::c(iPop)[2];
      T fTmp                          = grid[iX][iY][iZ][iPop];
      grid[iX][iY][iZ][iPop]          = grid[iX][iY][iZ][iPop+half];
      grid[iX][iY][iZ][iPop+half]     = grid[nextX][nextY][nextZ][iPop];
      grid[nextX][nextY][nextZ][iPop] = fTmp;
    }
  }
};

/// All boundary helper functions are inside this structure
template<typename T, template<typename U> class Lattice, int direction, int orientation>
struct BoundaryHelpers {
  
  static void computeStress (
    CellView<T,Lattice> const& cell, T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] )
  {
    typedef Lattice<T> L;
    const T uSqr = util::normSqr<T,L::d>(u);

    std::vector<int> const& onWallIndices = util::subIndex<L, direction, 0>();
    std::vector<int> const& normalIndices = util::subIndex<L, direction, orientation>();

    T fNeq[Lattice<T>::q];
    for (unsigned fIndex=0; fIndex<onWallIndices.size(); ++fIndex) {
      int iPop = onWallIndices[fIndex];
      fNeq[iPop] =
        cell[iPop] -
        lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
    }
    for (unsigned fIndex=0; fIndex<normalIndices.size(); ++fIndex) {
      int iPop = normalIndices[fIndex];
      if (iPop == 0) {
        fNeq[iPop] = T();  // fNeq[0] will not be used anyway
      } else {
        fNeq[iPop] =
          cell[iPop] -
          lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
      }
    }

    int iPi = 0;
    for (int iAlpha=0; iAlpha<L::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta<L::d; ++iBeta) {
        pi[iPi] = T();
        for (unsigned fIndex=0; fIndex<onWallIndices.size(); ++fIndex) {
          const int iPop = onWallIndices[fIndex];
          pi[iPi] +=
              L::c(iPop,iAlpha)*L::c(iPop,iBeta)*fNeq[iPop];
        }
        for (unsigned fIndex=0; fIndex<normalIndices.size(); ++fIndex) {
          const int iPop = normalIndices[fIndex];
          pi[iPi] += (T)2 * L::c(iPop,iAlpha)*L::c(iPop,iBeta)*
                     fNeq[iPop];
        }
        ++iPi;
      }
    }
  }

  static void computeStress (
    CellDataArray<T,Lattice> const& data, T rho, const T u[Lattice<T>::d],
    T pi[util::TensorVal<Lattice<T> >::n] )
  {
    typedef Lattice<T> L;
    const T uSqr = util::normSqr<T,L::d>(u);

    std::vector<int> const& onWallIndices = util::subIndex<L, direction, 0>();
    std::vector<int> const& normalIndices = util::subIndex<L, direction, orientation>();

    T fNeq[Lattice<T>::q];
    for (unsigned fIndex=0; fIndex<onWallIndices.size(); ++fIndex) {
      int iPop = onWallIndices[fIndex];
      fNeq[iPop] =
        data.data[iPop][0] -
        lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
    }
    for (unsigned fIndex=0; fIndex<normalIndices.size(); ++fIndex) {
      int iPop = normalIndices[fIndex];
      if (iPop == 0) {
        fNeq[iPop] = T();  // fNeq[0] will not be used anyway
      } else {
        fNeq[iPop] =
          data.data[iPop][0] -
          lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
      }
    }

    int iPi = 0;
    for (int iAlpha=0; iAlpha<L::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta<L::d; ++iBeta) {
        pi[iPi] = T();
        for (unsigned fIndex=0; fIndex<onWallIndices.size(); ++fIndex) {
          const int iPop = onWallIndices[fIndex];
          pi[iPi] +=
              L::c(iPop,iAlpha)*L::c(iPop,iBeta)*fNeq[iPop];
        }
        for (unsigned fIndex=0; fIndex<normalIndices.size(); ++fIndex) {
          const int iPop = normalIndices[fIndex];
          pi[iPi] += (T)2 * L::c(iPop,iAlpha)*L::c(iPop,iBeta)*
                     fNeq[iPop];
        }
        ++iPi;
      }
    }
  }

  static void computeStress (
    const T * const OPENLB_RESTRICT cellData, T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi )
  {
    typedef Lattice<T> L;
    const T uSqr = util::normSqr<T,L::d>(u);

    int onWallIndicesSize = getNumberOfSubIndices<T,L,direction,0>();
    int onWallIndices[onWallIndicesSize];
    getSubIndices<T,Lattice,direction,0>(onWallIndices);

    int normalIndicesSize = getNumberOfSubIndices<T,L,direction,orientation>();
    int normalIndices[normalIndicesSize];
    getSubIndices<T,Lattice,direction,orientation>(normalIndices);

    T fNeq[Lattice<T>::q];
    for (unsigned fIndex=0; fIndex<onWallIndicesSize; ++fIndex) {
      int iPop = onWallIndices[fIndex];
      fNeq[iPop] =
        cellData[iPop] -
        lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
    }

    for (unsigned fIndex=0; fIndex<normalIndicesSize; ++fIndex) {
      int iPop = normalIndices[fIndex];
      if (iPop == 0) {
        fNeq[iPop] = T();  // fNeq[0] will not be used anyway
      } else {
        fNeq[iPop] =
          cellData[iPop] -
          lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
      }
    }

    int iPi = 0;
    for (int iAlpha=0; iAlpha<L::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta<L::d; ++iBeta) {
        pi[iPi] = T();
        for (unsigned fIndex=0; fIndex<onWallIndicesSize; ++fIndex) {
          const int iPop = onWallIndices[fIndex];
          pi[iPi] +=
              L::c(iPop,iAlpha)*L::c(iPop,iBeta)*fNeq[iPop];
        }
        for (unsigned fIndex=0; fIndex<normalIndicesSize; ++fIndex) {
          const int iPop = normalIndices[fIndex];
          pi[iPi] += (T)2 * L::c(iPop,iAlpha)*L::c(iPop,iBeta)*
                     fNeq[iPop];
        }
        ++iPi;
      }
    }
  }

  OPENLB_HOST_DEVICE
  static void computeStress (
    const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u,
    T * const OPENLB_RESTRICT pi )
  {
    typedef Lattice<T> L;
    const T uSqr = util::normSqr<T,L::d>(u);

    //TODO: refactor below to occur at compile time, Shreyas just brute force modified to get it to work at all.
	  int onWallIndicesSize = 0;
    int onWallIndices[Lattice<T>::q];

	  int normalIndicesSize = 0;
    int normalIndices[Lattice<T>::q];

    for (int iPop = 0; iPop < Lattice<T>::q; iPop++) {
      if (Lattice<T>::c(iPop, direction) == 0)
        onWallIndices[onWallIndicesSize++] = iPop;
      if (Lattice<T>::c(iPop, direction) == orientation)
        normalIndices[normalIndicesSize++] = iPop;
    }

    T fNeq[Lattice<T>::q];
    for (unsigned fIndex=0; fIndex<onWallIndicesSize; ++fIndex) {
      int iPop = onWallIndices[fIndex];
      fNeq[iPop] =
        cellData[iPop][cellIndex] -
        lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
    }

    for (unsigned fIndex=0; fIndex<normalIndicesSize; ++fIndex) {
      int iPop = normalIndices[fIndex];
      if (iPop == 0) {
        fNeq[iPop] = T();  // fNeq[0] will not be used anyway
      } else {
        fNeq[iPop] =
          cellData[iPop][cellIndex] -
          lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
      }
    }

    int iPi = 0;
    for (int iAlpha=0; iAlpha<L::d; ++iAlpha) {
      for (int iBeta=iAlpha; iBeta<L::d; ++iBeta) {
        pi[iPi] = T();
        for (unsigned fIndex=0; fIndex<onWallIndicesSize; ++fIndex) {
          const int iPop = onWallIndices[fIndex];
          pi[iPi] +=
            L::c(iPop,iAlpha)*L::c(iPop,iBeta)*fNeq[iPop];
        }
        for (unsigned fIndex=0; fIndex<normalIndicesSize; ++fIndex) {
          const int iPop = normalIndices[fIndex];
          pi[iPi] += (T)2 * L::c(iPop,iAlpha)*L::c(iPop,iBeta)*
            fNeq[iPop];
        }
        ++iPi;
      }
    }
  }


};  // struct boundaryHelpers

}  // namespace olb

// The specialized code is directly included. That is because we never want
// it to be precompiled so that in both the precompiled and the
// "include-everything" version, the compiler can apply all the
// optimizations it wants.
#include "lbHelpersD2Q5.h"
#include "lbHelpersD2Q9.h"
#include "lbHelpersD3Q7.h"
#include "lbHelpersD3Q15.h"
#include "lbHelpersD3Q19.h"
#include "lbHelpersD3Q27.h"

#endif
