/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2006, 2007 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Template specializations for some computationally intensive LB
 * functions of the header file lbHelpers.h, for the D2Q9 grid.
 */

#ifndef LB_HELPERS_D2Q9_H
#define LB_HELPERS_D2Q9_H

namespace olb {

// Efficient specialization for D2Q9 base lattice
template<typename T>
struct lbDynamicsHelpers<T, descriptors::D2Q9Descriptor<T> > {

  OPENLB_HOST_DEVICE
  static T equilibrium( int iPop, T rho, const T * const OPENLB_RESTRICT u, T uSqr )
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    T c_u = L::c(iPop,0)*u[0] + L::c(iPop,1)*u[1];
    return rho * L::t(iPop) * (
             1. + 3.*c_u + 4.5*c_u*c_u - 1.5*uSqr )
           - L::t(iPop);
  }

  template<int width>
  static void equilibrium( int iPop, T rho[width], const T u[2][width], const T uSqr[width], T feq[width] )
  {
    typedef descriptors::D2Q9Descriptor<T> L;
#if defined(__INTEL_COMPILER)
	#pragma ivdep
#endif
    for (int i = 0; i < width; ++i)
    {
      T c_u = L::c(iPop,0)*u[0][i] + L::c(iPop,1)*u[1][i];
      feq[i] = rho[i] * L::t(iPop) * (
             1. + 3.*c_u + 4.5*c_u*c_u - 1.5*uSqr[i] )
           - L::t(iPop);
    }
  }

  OPENLB_HOST_DEVICE
  static T equilibriumFirstOrder( int iPop, T rho, const T u[2] )
   {
     typedef descriptors::D2Q9Descriptor<T> L;
     T c_u = L::c(iPop,0) * u[0] + L::c(iPop,1) * u[1];

     return rho * L::t(iPop) * ( ( T )1 + c_u * L::invCs2() ) - L::t(iPop);
   }

  OPENLB_HOST_DEVICE
  static T incEquilibrium( int iPop, const T j[2], const T jSqr, const T pressure )
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    T c_j = L::c(iPop,0)*j[0] + L::c(iPop,1)*j[1];
    return L::t(iPop) * (
             3.*pressure + 3.*c_j + 4.5*c_j*c_j - 1.5*jSqr )
           - L::t(iPop);
  }

  OPENLB_HOST_DEVICE
  static void computeFneq ( CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, T fNeq[9], T rho, const T u[2] )
  {
    const T uSqr = u[0]*u[0] + u[1]*u[1];
    for (int iPop=0; iPop < 9; ++iPop) {
      fNeq[iPop] = cell[iPop] - equilibrium(iPop, rho, u, uSqr);
    }
  }

  OPENLB_HOST_DEVICE
  static void computeFneq ( const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT fNeq, T rho, const T * const OPENLB_RESTRICT u )
  {
    const T uSqr = u[0]*u[0] + u[1]*u[1];
    for (int iPop=0; iPop < 9; ++iPop) {
      fNeq[iPop] = cellData[iPop] - equilibrium(iPop, rho, u, uSqr);
    }
  }
  OPENLB_HOST_DEVICE
  static void computeFneq ( const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT fNeq, T rho, const T * const OPENLB_RESTRICT u )
  {
    const T uSqr = u[0]*u[0] + u[1]*u[1];
    for (int iPop=0; iPop < 9; ++iPop) {
      fNeq[iPop] = cellData[iPop][cellIndex] - equilibrium(iPop, rho, u, uSqr);
    }
  }

  OPENLB_HOST_DEVICE
  static T bgkCollision ( CellBase<T,descriptors::D2Q9Descriptor<T> >& cell, T const& rho, const T u[2], T const& omega)
  {
    T uxSqr = u[0]*u[0];
    T uySqr = u[1]*u[1];

    T ux_ = (T)3 * u[0];
    T uy_ = (T)3 * u[1];

    T uxSqr_ = (T)3 * uxSqr;
    T uySqr_ = (T)3 * uySqr;
    T uxSqr__ = (T)3/(T)2 * uxSqr;
    T uySqr__ = (T)3/(T)2 * uySqr;
    T uSqr_ = uxSqr__ + uySqr__;

    T uxPySqr_ = (T)9/(T)2 * (u[0]+u[1])*(u[0]+u[1]);
    T uxMySqr_ = (T)9/(T)2 * (u[0]-u[1])*(u[0]-u[1]);

    T rho_ = (T)4/(T)9 * rho;
    T cf_  = (T)4/(T)9 * (rho-(T)1);

    cell[0] *= (T)1-omega;
    cell[0] += omega*(cf_ + rho_*(- uxSqr__ - uySqr__));

    rho_ = (T)1/(T)9 * rho;
    cf_  = (T)1/(T)9 * (rho-(T)1);

    cell[6] *= (T)1-omega;
    cell[6] += omega*(cf_ + rho_*(ux_ + uxSqr_ - uySqr__));
    cell[8] *= (T)1-omega;
    cell[8] += omega*(cf_ + rho_*(uy_ + uySqr_ - uxSqr__));
    cell[2] *= (T)1-omega;
    cell[2] += omega*(cf_ + rho_*(-ux_ + uxSqr_ - uySqr__));
    cell[4] *= (T)1-omega;
    cell[4] += omega*(cf_ + rho_*(-uy_ + uySqr_ - uxSqr__));

    rho_ = (T)1/(T)36 * rho;
    cf_  = (T)1/(T)36 * (rho-(T)1);

    cell[7] *= (T)1-omega;
    cell[7] += omega*(cf_ + rho_*(ux_ + uy_ + uxPySqr_ - uSqr_));
    cell[1] *= (T)1-omega;
    cell[1] += omega*(cf_ + rho_*(-ux_ + uy_ + uxMySqr_ - uSqr_));
    cell[3] *= (T)1-omega;
    cell[3] += omega*(cf_ + rho_*(-ux_ - uy_ + uxPySqr_ - uSqr_));
    cell[5] *= (T)1-omega;
    cell[5] += omega*(cf_ + rho_*(ux_ - uy_ + uxMySqr_ - uSqr_));

    return uxSqr + uySqr;
  }


  OPENLB_HOST_DEVICE
  static T bgkCollision ( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex , T const& rho, const T u[2], T const omega)
  {
    T uxSqr = u[0]*u[0];
    T uySqr = u[1]*u[1];

    T ux_ = (T)3 * u[0];
    T uy_ = (T)3 * u[1];

    T uxSqr_ = (T)3 * uxSqr;
    T uySqr_ = (T)3 * uySqr;
    T uxSqr__ = (T)3/(T)2 * uxSqr;
    T uySqr__ = (T)3/(T)2 * uySqr;
    T uSqr_ = uxSqr__ + uySqr__;

    T uxPySqr_ = (T)9/(T)2 * (u[0]+u[1])*(u[0]+u[1]);
    T uxMySqr_ = (T)9/(T)2 * (u[0]-u[1])*(u[0]-u[1]);

    T rho_ = (T)4/(T)9 * rho;
    T cf_  = (T)4/(T)9 * (rho-(T)1);

    cellData[0][cellIndex] *= (T)1-omega;
    cellData[0][cellIndex] += omega*(cf_ + rho_*(- uxSqr__ - uySqr__));

    rho_ = (T)1/(T)9 * rho;
    cf_  = (T)1/(T)9 * (rho-(T)1);

    cellData[6][cellIndex] *= (T)1-omega;
    cellData[6][cellIndex] += omega*(cf_ + rho_*(ux_ + uxSqr_ - uySqr__));
    cellData[8][cellIndex] *= (T)1-omega;
    cellData[8][cellIndex] += omega*(cf_ + rho_*(uy_ + uySqr_ - uxSqr__));
    cellData[2][cellIndex] *= (T)1-omega;
    cellData[2][cellIndex] += omega*(cf_ + rho_*(-ux_ + uxSqr_ - uySqr__));
    cellData[4][cellIndex] *= (T)1-omega;
    cellData[4][cellIndex] += omega*(cf_ + rho_*(-uy_ + uySqr_ - uxSqr__));

    rho_ = (T)1/(T)36 * rho;
    cf_  = (T)1/(T)36 * (rho-(T)1);

    cellData[7][cellIndex] *= (T)1-omega;
    cellData[7][cellIndex] += omega*(cf_ + rho_*(ux_ + uy_ + uxPySqr_ - uSqr_));
    cellData[1][cellIndex] *= (T)1-omega;
    cellData[1][cellIndex] += omega*(cf_ + rho_*(-ux_ + uy_ + uxMySqr_ - uSqr_));
    cellData[3][cellIndex] *= (T)1-omega;
    cellData[3][cellIndex] += omega*(cf_ + rho_*(-ux_ - uy_ + uxPySqr_ - uSqr_));
    cellData[5][cellIndex] *= (T)1-omega;
    cellData[5][cellIndex] += omega*(cf_ + rho_*(ux_ - uy_ + uxMySqr_ - uSqr_));

    //TODO: This is not right. We need to make this nice and in a general way!
    T tmp[4];
    tmp[0] = cellData[1][cellIndex];
    tmp[1] = cellData[2][cellIndex];
    tmp[2] = cellData[3][cellIndex];
    tmp[3] = cellData[4][cellIndex];

    cellData[1][cellIndex] = cellData[descriptors::D2Q9Descriptor<T>::opposite(1)][cellIndex];
    cellData[2][cellIndex] = cellData[descriptors::D2Q9Descriptor<T>::opposite(2)][cellIndex];
    cellData[3][cellIndex] = cellData[descriptors::D2Q9Descriptor<T>::opposite(3)][cellIndex];
    cellData[4][cellIndex] = cellData[descriptors::D2Q9Descriptor<T>::opposite(4)][cellIndex];

    cellData[descriptors::D2Q9Descriptor<T>::opposite(1)][cellIndex] = tmp[0];
    cellData[descriptors::D2Q9Descriptor<T>::opposite(2)][cellIndex] = tmp[1];
    cellData[descriptors::D2Q9Descriptor<T>::opposite(3)][cellIndex] = tmp[2];
    cellData[descriptors::D2Q9Descriptor<T>::opposite(4)][cellIndex] = tmp[3];

    return uxSqr + uySqr;
  }

  OPENLB_HOST_DEVICE
  static T bgkCollision ( T * OPENLB_RESTRICT cellData,T const rho, const T OPENLB_RESTRICT u[2], T const omega)
  {
    T uxSqr = u[0]*u[0];
    T uySqr = u[1]*u[1];

    T ux_ = (T)3 * u[0];
    T uy_ = (T)3 * u[1];

    T uxSqr_ = (T)3 * uxSqr;
    T uySqr_ = (T)3 * uySqr;
    T uxSqr__ = (T)3/(T)2 * uxSqr;
    T uySqr__ = (T)3/(T)2 * uySqr;
    T uSqr_ = uxSqr__ + uySqr__;

    T uxPySqr_ = (T)9/(T)2 * (u[0]+u[1])*(u[0]+u[1]);
    T uxMySqr_ = (T)9/(T)2 * (u[0]-u[1])*(u[0]-u[1]);

    T rho_ = (T)4/(T)9 * rho;
    T cf_  = (T)4/(T)9 * (rho-(T)1);

    cellData[0] *= (T)1-omega;
    cellData[0] += omega*(cf_ + rho_*(- uxSqr__ - uySqr__));

    rho_ = (T)1/(T)9 * rho;
    cf_  = (T)1/(T)9 * (rho-(T)1);

    cellData[6] *= (T)1-omega;
    cellData[6] += omega*(cf_ + rho_*(ux_ + uxSqr_ - uySqr__));
    cellData[8] *= (T)1-omega;
    cellData[8] += omega*(cf_ + rho_*(uy_ + uySqr_ - uxSqr__));
    cellData[2] *= (T)1-omega;
    cellData[2] += omega*(cf_ + rho_*(-ux_ + uxSqr_ - uySqr__));
    cellData[4] *= (T)1-omega;
    cellData[4] += omega*(cf_ + rho_*(-uy_ + uySqr_ - uxSqr__));

    rho_ = (T)1/(T)36 * rho;
    cf_  = (T)1/(T)36 * (rho-(T)1);

    cellData[7] *= (T)1-omega;
    cellData[7] += omega*(cf_ + rho_*(ux_ + uy_ + uxPySqr_ - uSqr_));
    cellData[1] *= (T)1-omega;
    cellData[1] += omega*(cf_ + rho_*(-ux_ + uy_ + uxMySqr_ - uSqr_));
    cellData[3] *= (T)1-omega;
    cellData[3] += omega*(cf_ + rho_*(-ux_ - uy_ + uxPySqr_ - uSqr_));
    cellData[5] *= (T)1-omega;
    cellData[5] += omega*(cf_ + rho_*(ux_ - uy_ + uxMySqr_ - uSqr_));

    return uxSqr + uySqr;
  }


  OPENLB_HOST_DEVICE
  static T incBgkCollision (CellBase<T,descriptors::D2Q9Descriptor<T> >& cell, T pressure, const T j[2], T omega)
  {
    const T jSqr = util::normSqr<T,descriptors::D2Q9Descriptor<T>::d>(j);
    for (int iPop=0; iPop < descriptors::D2Q9Descriptor<T>::q; ++iPop) {
      cell[iPop] *= (T)1-omega;
      cell[iPop] += omega * lbHelpers<T,descriptors::D2Q9Descriptor>::incEquilibrium (
                      iPop, j, jSqr, pressure );
    }
    return jSqr;
  }

  OPENLB_HOST_DEVICE
  static T constRhoBgkCollision(CellBase<T,descriptors::D2Q9Descriptor<T> >& cell, T rho, const T u[2], T ratioRho, T omega)
  {
    const T uSqr = util::normSqr<T,descriptors::D2Q9Descriptor<T>::d>(u);
    for (int iPop=0; iPop < descriptors::D2Q9Descriptor<T>::q; ++iPop) {
      T feq = lbHelpers<T,descriptors::D2Q9Descriptor>::equilibrium(iPop, rho, u, uSqr );
      cell[iPop] = ratioRho*(feq+descriptors::D2Q9Descriptor<T>::t(iPop))
                   -descriptors::D2Q9Descriptor<T>::t(iPop) +
                   ((T)1-omega)*(cell[iPop]-feq);
    }
    return uSqr;
  }

  template<int width>
  static void constRhoBgkCollision(T * const OPENLB_RESTRICT * const OPENLB_RESTRICT data, const bool* valid, T rho[width], const T u[2][width], T ratioRho[width], T omega, T uSqr[width])
  {
#if defined(__INTEL_COMPILER)
#pragma ivdep
#endif
    for (int i = 0; i < width; ++i) {
      uSqr[i] = u[0][i]*u[0][i] + u[1][i]*u[1][i];
    }
#if defined(__INTEL_COMPILER)
#pragma ivdep
#endif
    for (int iPop=0; iPop < descriptors::D2Q9DescriptorBase<T>::q; ++iPop) {
      T* __restrict__ val = data[iPop];
      T feq[width];
      lbDynamicsHelpers<T, descriptors::D2Q9Descriptor<T>>::equilibrium<width>(iPop, rho, u, uSqr, feq );
#if defined(__INTEL_COMPILER)
#pragma ivdep
#endif
      for (int i = 0; i < width; ++i) {
        val[i] = val[i] * !valid[i] + valid[i] * (ratioRho[i]*(feq[i]+descriptors::D2Q9DescriptorBase<T>::t[iPop])
            -descriptors::D2Q9DescriptorBase<T>::t[iPop] +
            ((T)1-omega)*(val[i]-feq[i]));
      }
    }
  }


  OPENLB_HOST_DEVICE
  static void partial_rho ( CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell,
                            T& lineX_P1, T& lineX_0, T& lineX_M1, T& lineY_P1, T& lineY_M1 )
  {
    lineX_P1  = cell[5] + cell[6] + cell[7];
    lineX_0   = cell[0] + cell[4] + cell[8];
    lineX_M1  = cell[1] + cell[2] + cell[3];

    lineY_P1  = cell[7] + cell[8] + cell[1];
    lineY_M1  = cell[3] + cell[4] + cell[5];
  }

  OPENLB_HOST_DEVICE
  static void partial_rho (const T* const* data,
                            T& lineX_P1, T& lineX_0, T& lineX_M1, T& lineY_P1, T& lineY_M1 )
  {
    lineX_P1  = data[5][0] + data[6][0] + data[7][0];
    lineX_0   = data[0][0] + data[4][0] + data[8][0];
    lineX_M1  = data[1][0] + data[2][0] + data[3][0];

    lineY_P1  = data[7][0] + data[8][0] + data[1][0];
    lineY_M1  = data[3][0] + data[4][0] + data[5][0];
  }

  static void partial_rho (const T* const* data,
                            T& lineX_P1, T& lineX_0, T& lineX_M1, T& lineY_P1, T& lineY_M1, unsigned int pos )
  {
    lineX_P1  = data[5][pos] + data[6][pos] + data[7][pos];
    lineX_0   = data[0][pos] + data[4][pos] + data[8][pos];
    lineX_M1  = data[1][pos] + data[2][pos] + data[3][pos];

    lineY_P1  = data[7][pos] + data[8][pos] + data[1][pos];
    lineY_M1  = data[3][pos] + data[4][pos] + data[5][pos];
  }

  // static void partial_rho (T** cellData, size_t cellIndex,
                            // T& lineX_P1, T& lineX_0, T& lineX_M1, T& lineY_P1, T& lineY_M1)
  // {
    // lineX_P1  = cellData[5][cellIndex] + cellData[6][cellIndex] + cellData[7][cellIndex];
    // lineX_0   = cellData[0][cellIndex] + cellData[4][cellIndex] + cellData[8][cellIndex];
    // lineX_M1  = cellData[1][cellIndex] + cellData[2][cellIndex] + cellData[3][cellIndex];

    // lineY_P1  = cellData[7][cellIndex] + cellData[8][cellIndex] + cellData[1][cellIndex];
    // lineY_M1  = cellData[3][cellIndex] + cellData[4][cellIndex] + cellData[5][cellIndex];
  // }
OPENLB_HOST_DEVICE
  static void partial_rho (const T * const OPENLB_RESTRICT cellData,
                            T& lineX_P1, T& lineX_0, T& lineX_M1, T& lineY_P1, T& lineY_M1)
  {
    lineX_P1  = cellData[5] + cellData[6] + cellData[7];
    lineX_0   = cellData[0] + cellData[4] + cellData[8];
    lineX_M1  = cellData[1] + cellData[2] + cellData[3];

    lineY_P1  = cellData[7] + cellData[8] + cellData[1];
    lineY_M1  = cellData[3] + cellData[4] + cellData[5];
  }
  OPENLB_HOST_DEVICE
  static void partial_rho (const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& lineX_P1, T& lineX_0, T& lineX_M1, T& lineY_P1, T& lineY_M1)
  {
    lineX_P1  = cellData[5][cellIndex] + cellData[6][cellIndex] + cellData[7][cellIndex];
    lineX_0   = cellData[0][cellIndex] + cellData[4][cellIndex] + cellData[8][cellIndex];
    lineX_M1  = cellData[1][cellIndex] + cellData[2][cellIndex] + cellData[3][cellIndex];

    lineY_P1  = cellData[7][cellIndex] + cellData[8][cellIndex] + cellData[1][cellIndex];
    lineY_M1  = cellData[3][cellIndex] + cellData[4][cellIndex] + cellData[5][cellIndex];
  }

  OPENLB_HOST_DEVICE
  static T computeRho(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell)
  {
    T rho = cell[0] + cell[1] + cell[2] + cell[3] + cell[4]
            + cell[5] + cell[6] + cell[7] + cell[8] + (T)1;
    return rho;
  }

  OPENLB_HOST_DEVICE
  static T computeRho(const T* const* data)
  {
    T rho = data[0][0] + data[1][0] + data[2][0] + data[3][0] + data[4][0]
            + data[5][0] + data[6][0] + data[7][0] + data[8][0] + (T)1;
    return rho;
  }

  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT data)
  {
    T rho = data[0] + data[1] + data[2] + data[3] + data[4]
            + data[5] + data[6] + data[7] + data[8] + (T)1;
    return rho;
  }
  OPENLB_HOST_DEVICE
  static T computeRho(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT data, size_t cellIndex)
  {
    T rho = data[0][cellIndex] + data[1][cellIndex] + data[2][cellIndex] + data[3][cellIndex] + data[4][cellIndex]
            + data[5][cellIndex] + data[6][cellIndex] + data[7][cellIndex] + data[8][cellIndex] + (T)1;
    return rho;
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, T& rho, T u[2])
  {
    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cell, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
    T invRho= 1./rho;
    u[0]  = (lineX_P1 - lineX_M1)*invRho;
    u[1]  = (lineY_P1 - lineY_M1)*invRho;
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(const T* const* data, T& rho, T u[2])
  {
    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(data, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
    T invRho= 1./rho;
    u[0]  = (lineX_P1 - lineX_M1)*invRho;
    u[1]  = (lineY_P1 - lineY_M1)*invRho;
  }

  template<int width>
  static void computeRhoU(const T* const* __restrict data, T* __restrict rho, T u[2][width])
  {
#if defined(__INTEL_COMPILER)
    #pragma ivdep
#endif
    for (unsigned int i = 0; i < width; ++i) {
      T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
      partial_rho(data, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1, i);

      rho[i] = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
      T invRho= 1./rho[i];
      u[0][i]  = (lineX_P1 - lineX_M1)*invRho;
      u[1][i]  = (lineY_P1 - lineY_M1)*invRho;
    }
  }

  OPENLB_HOST_DEVICE
  static void computeRhoU(T** cellData, size_t cellIndex, T& rho, T u[2])
  {
    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData, cellIndex, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
    T invRho= 1./rho;
    u[0]  = (lineX_P1 - lineX_M1)*invRho;
    u[1]  = (lineY_P1 - lineY_M1)*invRho;
  }
  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT cellData, T& rho, T * const OPENLB_RESTRICT u)
  {
    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
    T invRho= 1./rho;
    u[0]  = (lineX_P1 - lineX_M1)*invRho;
    u[1]  = (lineY_P1 - lineY_M1)*invRho;
  }
  OPENLB_HOST_DEVICE
  static void computeRhoU(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u)
  {
    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData,cellIndex, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
    T invRho= 1./rho;
    u[0]  = (lineX_P1 - lineX_M1)*invRho;
    u[1]  = (lineY_P1 - lineY_M1)*invRho;
  }
  OPENLB_HOST_DEVICE
  static void computeRhoJ(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, T& rho, T j[2])
  {
    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cell, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;
    j[0]  = (lineX_P1 - lineX_M1);
    j[1]  = (lineY_P1 - lineY_M1);
  }

  OPENLB_HOST_DEVICE
  static void computeJ(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, T j[2] )
  {
    T lineX_P1, lineX_M1, lineY_P1, lineY_M1;

    lineX_P1  = cell[5] + cell[6] + cell[7];
    lineX_M1  = cell[1] + cell[2] + cell[3];
    lineY_P1  = cell[7] + cell[8] + cell[1];
    lineY_M1  = cell[3] + cell[4] + cell[5];

    j[0]  = (lineX_P1 - lineX_M1);
    j[1]  = (lineY_P1 - lineY_M1);
  }
  OPENLB_HOST_DEVICE
  static void computeJ(const T * const OPENLB_RESTRICT cellData, T * const OPENLB_RESTRICT j)
  {
    T lineX_P1, lineX_M1, lineY_P1, lineY_M1;

    lineX_P1  = cellData[5] + cellData[6] + cellData[7];
    lineX_M1  = cellData[1] + cellData[2] + cellData[3];
    lineY_P1  = cellData[7] + cellData[8] + cellData[1];
    lineY_M1  = cellData[3] + cellData[4] + cellData[5];

    j[0]  = (lineX_P1 - lineX_M1);
    j[1]  = (lineY_P1 - lineY_M1);
  }
  OPENLB_HOST_DEVICE
  static void computeJ(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T * const OPENLB_RESTRICT j)
  {
    T lineX_P1, lineX_M1, lineY_P1, lineY_M1;

    lineX_P1  = cellData[5][cellIndex] + cellData[6][cellIndex] + cellData[7][cellIndex];
    lineX_M1  = cellData[1][cellIndex] + cellData[2][cellIndex] + cellData[3][cellIndex];
    lineY_P1  = cellData[7][cellIndex] + cellData[8][cellIndex] + cellData[1][cellIndex];
    lineY_M1  = cellData[3][cellIndex] + cellData[4][cellIndex] + cellData[5][cellIndex];

    j[0]  = (lineX_P1 - lineX_M1);
    j[1]  = (lineY_P1 - lineY_M1);
  }

  OPENLB_HOST_DEVICE
  static void computeStress(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, T rho, const T u[2], T pi[3])
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cell, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    pi[xx] = lineX_P1+lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[0]*u[0];
    pi[yy] = lineY_P1+lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[1]*u[1];
    pi[xy] = -cell[1] + cell[3] - cell[5] + cell[7]   - rho*u[0]*u[1];
  }

  OPENLB_HOST_DEVICE
  static void computeStress(const T* const* data, T rho, const T u[2], T pi[3])
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(data, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    pi[xx] = lineX_P1+lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[0]*u[0];
    pi[yy] = lineY_P1+lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[1]*u[1];
    pi[xy] = -data[1][0] + data[3][0] - data[5][0] + data[7][0]   - rho*u[0]*u[1];
  }
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT cellData, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi)
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    pi[xx] = lineX_P1+lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[0]*u[0];
    pi[yy] = lineY_P1+lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[1]*u[1];
    pi[xy] = -cellData[1] + cellData[3] - cellData[5] + cellData[7]   - rho*u[0]*u[1];
  }
  OPENLB_HOST_DEVICE
  static void computeStress(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T rho, const T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi)
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData,cellIndex, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    pi[xx] = lineX_P1+lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[0]*u[0];
    pi[yy] = lineY_P1+lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rho*u[1]*u[1];
    pi[xy] = -cellData[1][cellIndex] + cellData[3][cellIndex] - cellData[5][cellIndex] + cellData[7][cellIndex]   - rho*u[0]*u[1];
  }

  OPENLB_HOST_DEVICE
  static void computeAllMomenta(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, T& rho, T u[2], T pi[3] )
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cell, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;

    T rhoU0 = (lineX_P1 - lineX_M1);
    T rhoU1 = (lineY_P1 - lineY_M1);
    u[0]  = rhoU0/rho;
    u[1]  = rhoU1/rho;

    pi[xx] = lineX_P1 + lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rhoU0*u[0];
    pi[yy] = lineY_P1 + lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rhoU1*u[1];
    pi[xy] = -cell[1] + cell[3] - cell[5] + cell[7]        - rhoU0*u[1];
  }
  OPENLB_HOST_DEVICE
  static void computeAllMomenta(const T * const OPENLB_RESTRICT cellData, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;

    T rhoU0 = (lineX_P1 - lineX_M1);
    T rhoU1 = (lineY_P1 - lineY_M1);
    u[0]  = rhoU0/rho;
    u[1]  = rhoU1/rho;

    pi[xx] = lineX_P1 + lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rhoU0*u[0];
    pi[yy] = lineY_P1 + lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rhoU1*u[1];
    pi[xy] = -cellData[1] + cellData[3] - cellData[5] + cellData[7]        - rhoU0*u[1];
  }
  OPENLB_HOST_DEVICE
  static void computeAllMomenta(const T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, T& rho, T * const OPENLB_RESTRICT u, T * const OPENLB_RESTRICT pi )
  {
    typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(cellData,cellIndex, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;

    T rhoU0 = (lineX_P1 - lineX_M1);
    T rhoU1 = (lineY_P1 - lineY_M1);
    u[0]  = rhoU0/rho;
    u[1]  = rhoU1/rho;

    pi[xx] = lineX_P1 + lineX_M1 - 1./L::invCs2()*(rho-(T)1) - rhoU0*u[0];
    pi[yy] = lineY_P1 + lineY_M1 - 1./L::invCs2()*(rho-(T)1) - rhoU1*u[1];
    pi[xy] = -cellData[1][cellIndex] + cellData[3][cellIndex] - cellData[5][cellIndex] + cellData[7][cellIndex]        - rhoU0*u[1];
  }

  OPENLB_HOST_DEVICE
  static void computeAllMomenta(const T* const* data, T& rho, T u[2], T pi[3] )
  {
    // typedef descriptors::D2Q9Descriptor<T> L;
    // Workaround for Intel(r) compiler 9.1;
    // "using namespace util::tensorIndices2D" is not sufficient
    using util::tensorIndices2D::xx;
    using util::tensorIndices2D::yy;
    using util::tensorIndices2D::xy;

    T lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1;
    partial_rho(data, lineX_P1, lineX_0, lineX_M1, lineY_P1, lineY_M1);

    rho = lineX_P1 + lineX_0 + lineX_M1 + (T)1;

    T rhoU0 = (lineX_P1 - lineX_M1);
    T rhoU1 = (lineY_P1 - lineY_M1);
    u[0]  = rhoU0/rho;
    u[1]  = rhoU1/rho;

    pi[xx] = lineX_P1 + lineX_M1 - 1./descriptors::D2Q9Descriptor<T>::invCs2()*(rho-(T)1) - rhoU0*u[0];
    pi[yy] = lineY_P1 + lineY_M1 - 1./descriptors::D2Q9Descriptor<T>::invCs2()*(rho-(T)1) - rhoU1*u[1];
    pi[xy] = -data[1][0] + data[3][0] - data[5][0] + data[7][0] - rhoU0*u[1];
  }

  OPENLB_HOST_DEVICE
  static void modifyVelocity(CellBase<T,descriptors::D2Q9Descriptor<T> > const& cell, const T newU[2])
  {
    T rho, oldU[2];
    computeRhoU(cell, rho, oldU);
    const T oldUSqr = util::normSqr<T,2>(oldU);
    const T newUSqr = util::normSqr<T,2>(newU);
    for (int iPop=0; iPop<9; ++iPop) {
      cell[iPop] = cell[iPop]
                   - equilibrium(iPop, rho, oldU, oldUSqr)
                   + equilibrium(iPop, rho, newU, newUSqr);
    }
  }
  OPENLB_HOST_DEVICE
  static void modifyVelocity( T * const OPENLB_RESTRICT cellData, const T * const OPENLB_RESTRICT newU)
  {
    T rho, oldU[2];
    computeRhoU(cellData, rho, oldU);
    const T oldUSqr = util::normSqr<T,2>(oldU);
    const T newUSqr = util::normSqr<T,2>(newU);
    for (int iPop=0; iPop<9; ++iPop) {
      cellData[iPop] = cellData[iPop]
                   - equilibrium(iPop, rho, oldU, oldUSqr)
                   + equilibrium(iPop, rho, newU, newUSqr);
    }
  }
  OPENLB_HOST_DEVICE
  static void modifyVelocity( T * const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData, size_t cellIndex, const T * const OPENLB_RESTRICT newU)
  {
    T rho, oldU[2];
    computeRhoU(cellData, cellIndex,rho, oldU);
    const T oldUSqr = util::normSqr<T,2>(oldU);
    const T newUSqr = util::normSqr<T,2>(newU);
    for (int iPop=0; iPop<9; ++iPop) {
      cellData[iPop][cellIndex] = cellData[iPop][cellIndex]
                   - equilibrium(iPop, rho, oldU, oldUSqr)
                   + equilibrium(iPop, rho, newU, newUSqr);
    
	}
  }

};  //struct lbHelpers<D2Q9Descriptor>

// Efficient specialization for D2Q9 lattice with force
//template<typename T>
//struct lbExternalHelpers<T, descriptors::ForcedD2Q9Descriptor> {
//
//  OPENLB_HOST_DEVICE
//  static void addExternalForce(
//    CellView<T,descriptors::ForcedD2Q9Descriptor>& cell,
//    const T u[descriptors::ForcedD2Q9Descriptor<T>::d], T omega, T amplitude)
//  {
//    static const int forceBeginsAt
//      = descriptors::ForcedD2Q9Descriptor<T>::ExternalField::forceBeginsAt;
//    T* force = cell.getExternal(forceBeginsAt);
//    T mu = amplitude*((T)1-omega/(T)2);
//
//    cell[0] += mu *(T)4/(T)3  *( force[0] * (-  u[0]             ) +
//                                 force[1] * (        -   u[1]    )   );
//    cell[1] += mu *(T)1/(T)12 *( force[0] * ( 2*u[0] - 3*u[1] - 1) +
//                                 force[1] * (-3*u[0] + 2*u[1] + 1)   );
//    cell[2] += mu *(T)1/(T)3  *( force[0] * ( 2*u[0]          - 1) +
//                                 force[1] * (        -   u[1]    )   );
//    cell[3] += mu *(T)1/(T)12 *( force[0] * ( 2*u[0] + 3*u[1] - 1) +
//                                 force[1] * ( 3*u[0] + 2*u[1] - 1)   );
//    cell[4] += mu *(T)1/(T)3  *( force[0] * (-  u[0]             ) +
//                                 force[1] * (        + 2*u[1] - 1)   );
//    cell[5] += mu *(T)1/(T)12 *( force[0] * ( 2*u[0] - 3*u[1] + 1) +
//                                 force[1] * (-3*u[0] + 2*u[1] - 1)   );
//    cell[6] += mu *(T)1/(T)3  *( force[0] * ( 2*u[0]          + 1) +
//                                 force[1] * (        -   u[1]    )   );
//    cell[7] += mu *(T)1/(T)12 *( force[0] * ( 2*u[0] + 3*u[1] + 1) +
//                                 force[1] * ( 3*u[0] + 2*u[1] + 1)   );
//    cell[8] += mu *(T)1/(T)3  *( force[0] * (-  u[0]             ) +
//                                 force[1] * (        + 2*u[1] + 1)   );
//  }
//};

// Efficient specialization for D2Q9 lattice and for forced D2Q9 lattice
//   (operations applying to the whole lattice)

template<typename T>
struct lbLatticeHelpers<T, descriptors::D2Q9Descriptor> {

  OPENLB_HOST_DEVICE
  static void swapAndStreamCell (
    CellView<T,descriptors::D2Q9Descriptor> **grid,
    int iX, int iY, int nX, int nY, int iPop)
  {
    T fTmp               = grid[iX][iY][iPop].get();
    grid[iX][iY][iPop]   = grid[iX][iY][iPop+4];
    grid[iX][iY][iPop+4] = grid[nX][nY][iPop];
    grid[nX][nY][iPop]   = fTmp;
  }

  OPENLB_HOST_DEVICE
  static void swapAndStream2D (
    CellView<T,descriptors::D2Q9Descriptor> **grid, int iX, int iY )
  {
    swapAndStreamCell(grid, iX, iY, iX-1, iY+1, 1);
    swapAndStreamCell(grid, iX, iY, iX-1, iY,   2);
    swapAndStreamCell(grid, iX, iY, iX-1, iY-1, 3);
    swapAndStreamCell(grid, iX, iY, iX,   iY-1, 4);
  }
};

template<typename T>
struct lbLatticeHelpers<T, descriptors::ForcedD2Q9Descriptor> {

  OPENLB_HOST_DEVICE
  static void swapAndStreamCell (
    CellView<T,descriptors::ForcedD2Q9Descriptor> **grid,
    int iX, int iY, int nX, int nY, int iPop)
  {
    T fTmp               = grid[iX][iY][iPop];
    grid[iX][iY][iPop]   = grid[iX][iY][iPop+4];
    grid[iX][iY][iPop+4] = grid[nX][nY][iPop];
    grid[nX][nY][iPop]   = fTmp;
  }

  OPENLB_HOST_DEVICE
  static void swapAndStream2D (
    CellView<T,descriptors::ForcedD2Q9Descriptor> **grid, int iX, int iY )
  {
    T fTmp;
    swapAndStreamCell(grid, iX, iY, iX-1, iY+1, 1);
    swapAndStreamCell(grid, iX, iY, iX-1, iY,   2);
    swapAndStreamCell(grid, iX, iY, iX-1, iY-1, 3);
    swapAndStreamCell(grid, iX, iY, iX,   iY-1, 4);
  }

};

}  // namespace olb

#endif
