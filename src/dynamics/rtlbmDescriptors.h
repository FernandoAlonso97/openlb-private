/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2017 Albert Mink
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
*/

/** \file
 *  -- header file
 */
#ifndef RTLBM_DESCRIPTORS_H
#define RTLBM_DESCRIPTORS_H

#include "latticeDescriptors.h"

namespace olb {

/// Descriptors for the 2D and 3D lattices.
/** \warning Attention: The lattice directions must always be ordered in
 * such a way that c[i] = -c[i+(q-1)/2] for i=1..(q-1)/2, and c[0] = 0 must
 * be the rest velocity. Furthermore, the velocities c[i] for i=1..(q-1)/2
 * must verify
 *  - in 2D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *  - in 3D: (c[i][0]<0) || (c[i][0]==0 && c[i][1]<0)
 *                       || (c[i][0]==0 && c[i][1]==0 && c[i][2]<0)
 * Otherwise some of the code will work erroneously, because the
 * aformentioned relations are taken as given to enable a few
 * optimizations.
*/
namespace descriptors {

/// D3Q7 lattice for radiative transport problems @2016 A. Mink et al.
template <typename T>
struct D3Q7DescriptorBaseRTLB {
  typedef D3Q7DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 7 };     ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
  static const double henyeyPhaseFunction[q][q]; ///<anisotropic discrete scattering coefficient
};

template <typename T>
struct D3Q7DescriptorRTLB : public D3Q7DescriptorBaseRTLB<T>, public NoExternalFieldBase { };


/// D3Q19 lattice TODO: AM
template <typename T>
struct D3Q19DescriptorBaseRTLB {
  typedef D3Q19DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 19 };     ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
  static const double henyeyPhaseFunction[q][q]; ///<anisotropic discrete scattering coefficient
};

template <typename T>
struct D3Q19DescriptorRTLB : public D3Q19DescriptorBaseRTLB<T>, public NoExternalFieldBase {};



/** D3Q27 lattice.
 *  zero direction only need for correct stream process. Contains dummy values.
 */
template <typename T>
struct D3Q27DescriptorBaseRTLB {
  typedef D3Q27DescriptorBase<T> BaseDescriptor;
  enum { d = 3, q = 27 };     ///< number of dimensions/distr. functions
  static const int vicinity;  ///< size of neighborhood
  static const int c[q][d];   ///< lattice directions
  static const int opposite[q]; ///< opposite entry
  static const T t[q];        ///< lattice weights
  static const T invCs2;      ///< inverse square of speed of sound
  static const double henyeyPhaseFunction[q][q]; ///<anisotropic discrete scattering coefficient
};

template <typename T>
struct D3Q27DescriptorRTLB : public D3Q27DescriptorBaseRTLB<T>, public NoExternalFieldBase {};


}  // namespace descriptors

}  // namespace olb

#endif
