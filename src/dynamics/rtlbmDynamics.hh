/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2017 Albert Mink, Christopher McHardy
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A collection of radiative transport dynamics classes -- generic implementation.
 */

#ifndef RTLBM_DYNAMICS_HH
#define RTLBM_DYNAMICS_HH

#include "rtlbmDynamics.h"
#include "lbHelpers.h"

using namespace olb::descriptors;

namespace olb {



//==================================================================//
//============= BGK Model for Advection diffusion plus sink term ===//
//==================================================================//

template<typename T, template<typename U> class Lattice, class Momenta>
RTLBMdynamicsMink<T, Lattice, Momenta>::RTLBMdynamicsMink
( T omega, Momenta& momenta, T latticeAbsorption, T latticeScattering )
  : BasicDynamics<T, Lattice, Momenta, RTLBMdynamicsMink<T, Lattice, Momenta>>( momenta ), _omega(omega), _sink( 3.0*latticeAbsorption*(latticeAbsorption+latticeScattering) / 8.0)
{
  static_assert( std::is_base_of<D3Q7DescriptorBaseRTLB<T>, Lattice<T> >::value, "Descriptor not derived from D3Q7DescriptorBase.");
}

template<typename T, template<typename U> class Lattice, class Momenta>
RTLBMdynamicsMink<T, Lattice, Momenta>* RTLBMdynamicsMink<T, Lattice, Momenta>::clone() const
{
  return new RTLBMdynamicsMink<T, Lattice, Momenta>( *this );
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMdynamicsMink<T, Lattice, Momenta>::computeEquilibrium
( int iPop, T rho, const T u[Lattice<T>::d], T uSqr ) const
{
  return lbHelpers<T, Lattice>::equilibriumFirstOrder( iPop, rho, u );
}


template<typename T, template<typename U> class Lattice, class Momenta>
void RTLBMdynamicsMink<T, Lattice, Momenta>::collide
( CellView<T, Lattice>& cell, LatticeStatistics<T>& statistics )
{
  T intensity = this->_momenta.computeRho( cell );
  T uSqr = lbHelpers<T,Lattice>::sinkCollision( cell, intensity, _omega, _sink );
  statistics.incrementStats( intensity, uSqr );
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMdynamicsMink<T, Lattice, Momenta>::getOmega() const
{
  return _omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void RTLBMdynamicsMink<T, Lattice, Momenta>::setOmega( T omega )
{
  _omega = omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMdynamicsMink<T, Lattice, Momenta>::getSink() const
{
  return _sink;
}


template<typename T, template<typename U> class Lattice, class Momenta>
RTLBMconstDynamicsMink<T, Lattice, Momenta>::RTLBMconstDynamicsMink
( Momenta& momenta, T latticeAbsorption, T latticeScattering )
  : BasicDynamics<T, Lattice, Momenta, RTLBMconstDynamicsMink<T, Lattice, Momenta>>( momenta ), _sink( 3.0*latticeAbsorption*(latticeAbsorption+latticeScattering) / 8.0)
{
  bool const is_d3q7_descriptor = std::is_base_of<D3Q7DescriptorBaseRTLB<T>, Lattice<T> >::value
                               || std::is_base_of<D3Q7DescriptorBase<T>, Lattice<T> >::value;

  static_assert( is_d3q7_descriptor, "Descriptor not derived from D3Q7DescriptorBase.");
}

template<typename T, template<typename U> class Lattice, class Momenta>
RTLBMconstDynamicsMink<T, Lattice, Momenta>* RTLBMconstDynamicsMink<T, Lattice, Momenta>::clone() const
{
  return new RTLBMconstDynamicsMink<T, Lattice, Momenta>( *this );
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMconstDynamicsMink<T, Lattice, Momenta>::computeEquilibrium
( int iPop, T rho, const T u[Lattice<T>::d], T uSqr ) const
{
  return lbHelpers<T, Lattice>::equilibriumFirstOrder( iPop, rho, u );
}


template<typename T, template<typename U> class Lattice, class Momenta>
void RTLBMconstDynamicsMink<T, Lattice, Momenta>::collide
( CellView<T, Lattice>& cell, LatticeStatistics<T>& statistics )
{
  T intensity = this->_momenta.computeRho( cell );
  T uSqr = lbHelpers<T,Lattice>::sinkCollision( cell, intensity, 1, _sink );
  statistics.incrementStats( intensity, uSqr );
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMconstDynamicsMink<T, Lattice, Momenta>::getOmega() const
{
  return 1;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void RTLBMconstDynamicsMink<T, Lattice, Momenta>::setOmega( T omega )
{}

//==================================================================//
//============= BGK Model for Advection diffusion anisotropic ===//
//==================================================================//

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType>
RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>::RTLBMdynamicsMcHardy
(Momenta& momenta, T latticeAbsorption, T latticeScattering)
  : BasicDynamics<T, Lattice, Momenta, RTLBMdynamicsMcHardy<T,Lattice,Momenta,DynamicsType>>(momenta), _absorption(latticeAbsorption), _scattering(latticeScattering)
{ }

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType>
RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>* RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>::clone() const
{
  return new RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>( *this );
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType>
T RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>::computeEquilibrium( int iPop, T rho, const T u[Lattice<T>::d], T uSqr ) const
{
  return lbHelpers<T,Lattice>::equilibriumFirstOrder( iPop, rho, u );
}


template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType>
void RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>::collide( CellView<T, Lattice>& cell, LatticeStatistics<T>& statistics )
{
  T temperature = this->_momenta.computeRho(cell );
//  T uSqr = advectionDiffusionLbHelpers<T,Lattice>::sinkCollision( cell, temperature, omega, 0. );
  T uSqr = lbHelpers<T, Lattice>::
           anisoCollision( cell, temperature, _absorption, _scattering );
  statistics.incrementStats( temperature, uSqr );
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType>
T RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>::getOmega() const
{
  return -1;
}

template<typename T, template<typename U> class Lattice, class Momenta, class DynamicsType>
void RTLBMdynamicsMcHardy<T, Lattice, Momenta, DynamicsType>::setOmega( T omega )
{
}

//==================================================================================//
template<typename T, template<typename U> class Lattice, class Momenta>
RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>::RTLBMdynamicsMcHardyWH
(Momenta& momenta, T latticeAbsorption, T latticeScattering)
  : RTLBMdynamicsMcHardy<T, Lattice, Momenta,RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>>(momenta, latticeAbsorption, latticeScattering)
{ }

template<typename T, template<typename U> class Lattice, class Momenta>
RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>* RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>::clone() const
{
  return new RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>( *this );
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>::computeEquilibrium( int iPop, T rho, const T u[Lattice<T>::d], T uSqr ) const
{
  return lbHelpers<T,Lattice>::equilibriumFirstOrder( iPop, rho, u );
}

template<typename T, template<typename U> class Lattice, class Momenta>
void RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>::collide( CellView<T, Lattice>& cell, LatticeStatistics<T>& statistics )
{
  T temperature = this->_momenta.computeRho(cell );
//  T uSqr = advectionDiffusionLbHelpers<T,Lattice>::sinkCollision( cell, temperature, omega, 0. );
  T uSqr = lbHelpers<T, Lattice>::
           anisoCollisionWH( cell, temperature, this->_absorption, this->_scattering );
  statistics.incrementStats( temperature, uSqr );
}

template<typename T, template<typename U> class Lattice, class Momenta>
T RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>::getOmega() const
{
  return -1;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void RTLBMdynamicsMcHardyWH<T, Lattice, Momenta>::setOmega( T omega )
{
}


} // namespace olb


#endif
