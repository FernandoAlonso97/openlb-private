/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2017 Lukas Baron, Mathias J. Krause,
 *                          Albert Mink, Adrian Kummerländer
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef BLOCK_CALC_F_3D_HH
#define BLOCK_CALC_F_3D_HH

#include "blockCalcF3D.h"

namespace olb {

template <typename T, template<typename> class F>
BlockCalc3D<T,F>::BlockCalc3D (BlockF3D<T>& f, BlockF3D<T>& g)
  : BlockF3D<T>(f.getBlockStructure(), f.getTargetDim()),
    _f(f), _g(g), _glob{}, _fIsBlock(true), _gIsBlock(true)
{
  this->getName() = "(" + f.getName() + F<T>::symbol + g.getName() + ")";
  std::swap(f._ptrCalcC, this->_ptrCalcC);
}

template <typename T, template<typename> class F>
BlockCalc3D<T,F>::BlockCalc3D (BlockF3D<T>& f, GenericF<T,int>& g, int glob)
  : BlockF3D<T>(f.getBlockStructure(), f.getTargetDim()),
    _f(f), _g(g), _glob(glob), _fIsBlock(true), _gIsBlock(false)
{
  this->getName() = "(" + f.getName() + F<T>::symbol + g.getName() + ")";
  std::swap(f._ptrCalcC, this->_ptrCalcC);
}

template <typename T, template<typename> class F>
BlockCalc3D<T,F>::BlockCalc3D (GenericF<T,int>& f, int glob, BlockF3D<T>& g)
  : BlockF3D<T>(g.getBlockStructure(), g.getTargetDim()),
    _f(f), _g(g), _glob(glob), _fIsBlock(false), _gIsBlock(true)
{
  this->getName() = "(" + f.getName() + F<T>::symbol + g.getName() + ")";
  std::swap(f._ptrCalcC, this->_ptrCalcC);
}

template <typename T, template<typename> class F>
bool BlockCalc3D<T,F>::operator()(T output[], const int input[])
{
  T tmp[this->_f.getTargetDim()];

  if ( this->_fIsBlock && this->_gIsBlock ) {
    this->_f(output,input);
    this->_g(tmp, input);
  }
  else {
    const int superInput[4] = {
      this->_glob,
      input[0], input[1], input[2]
    };

    if ( this->_fIsBlock ) {
      this->_f(output, input);
      this->_g(tmp, superInput);
    }
    else {
      this->_f(output, superInput);
      this->_g(tmp, input);
    }
  }

  for (int i = 0; i < this->_f.getTargetDim(); i++) {
    output[i] = F<T>()(output[i], tmp[i]);
  }

  return true;
}


/////////////////////////////////operator()///////////////////////////////////

template <typename T>
BlockF3D<T>& BlockF3D<T>::operator+(BlockF3D<T>& rhs)
{
  auto tmp = std::make_shared< BlockPlus3D<T> >(*this,rhs);
  this->_ptrCalcC = tmp;
  return *tmp;
}

template <typename T>
BlockF3D<T>& BlockF3D<T>::operator-(BlockF3D<T>& rhs)
{
  auto tmp = std::make_shared< BlockMinus3D<T> >(*this,rhs);
  this->_ptrCalcC = tmp;
  return *tmp;
}

template <typename T>
BlockF3D<T>& BlockF3D<T>::operator*(BlockF3D<T>& rhs)
{
  auto tmp = std::make_shared< BlockMultiplication3D<T> >(*this,rhs);
  this->_ptrCalcC = tmp;
  return *tmp;
}

template <typename T>
BlockF3D<T>& BlockF3D<T>::operator/(BlockF3D<T>& rhs)
{
  auto tmp = std::make_shared< BlockDivision3D<T> >(*this,rhs);
  this->_ptrCalcC = tmp;
  return *tmp;
}


} // end namespace olb

#endif
