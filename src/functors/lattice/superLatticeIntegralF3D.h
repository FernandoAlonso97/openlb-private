/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2017 Lukas Baron, Tim Dornieden, Mathias J. Krause,
 *  Albert Mink, Benjamin Förster, Adrian Kummerländer
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef SUPER_LATTICE_INTEGRAL_F_3D_H
#define SUPER_LATTICE_INTEGRAL_F_3D_H

#include <vector>

#include "functors/genericF.h"
#include "blockLatticeIntegralF3D.h"
#include "superBaseF3D.h"
#include "indicator/superIndicatorBaseF3D.h"
#include "indicator/indicatorBaseF3D.h"
#include "indicator/superIndicatorF3D.h"
#include "superLatticeLocalF3D.h"
#include "functors/analytical/interpolationF3D.h"
#include "functors/lattice/reductionF3D.h"
#include "core/superLattice3D.h"
#include "core/vector.h"
#include "io/ostreamManager.h"
#include "geometry/superGeometry3D.h"



/** Note: Throughout the whole source code directory genericFunctions, the
 *  template parameters for i/o dimensions are:
 *           F: S^m -> T^n  (S=source, T=target)
 */

namespace olb {

////////////////////////////////////////////////////////////////////////////////
//////if globIC is not on the local processor, the returned vector is empty/////
////////////////////////////////////////////////////////////////////////////////


/// functor that returns the max in each component of all points of a certain material
template <typename T, typename W = T>
class SuperMin3D final : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>& _f;
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
public:
  SuperMin3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& superGeometry, const int material);
  bool operator() (W output[], const int input[]) override;
};

/// functor that returns the max in each component of all points of a certain material
template <typename T, typename W = T>
class SuperMax3D final : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>& _f;
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
public:
  SuperMax3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& superGeometry, const int material);
  bool operator() (W output[], const int input[]) override;
};


/// sums over all cells of a certain material number
template <typename T, typename W = T>
class SuperSum3D final : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>& _f;
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
public:
  SuperSum3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& superGeometry, const int material);
  bool operator() (W output[], const int input[]) override;
};

/// sums over all cells of a SmoothIndicatorSphere
template <typename T, typename W = T>
class SuperSumIndicator3D final : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>& _f;
  SuperGeometry3D<T>& _superGeometry;
  ParticleIndicatorF3D<T,T>& _indicator;
public:
  SuperSumIndicator3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& superGeometry, ParticleIndicatorF3D<T,T>& indicator);
  bool operator() (T output[], const int input[]) override;
};


/// average over all cells of a certain material number
template <typename T, typename W = T>
class SuperAverage3D final : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>& _f;
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
public:
  SuperAverage3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& superGeometry, const int material);
  bool operator() (W output[], const int input[]) override;
};

template <typename T, typename W = T>
class SuperIntegral3D final : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>& _f;
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
public:
  SuperIntegral3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& superGeometry, const int material);
  bool operator() (W output[], const int input[]) override;
};


/// Functor that returns the Lp norm over omega of the the euklid norm of the input functor
/**
 * Maintains block level BlockLpNorm3D functors as required.
 *
 * P == 0: inf norm
 * P >= 1: p norm
 */
template <typename T, typename W, int P>
class SuperLpNorm3D : public SuperF3D<T,W> {
private:
  SuperF3D<T,W>&                        _f;
  std::unique_ptr<SuperIndicatorF3D<T>> _ownIndicatorF;
  SuperIndicatorF3D<T>* const           _indicatorF;

  /// old _block agnostic_ operator logic
  /**
   * To be removed as soon as all functors are blockified.
   **/
  bool _block_agnostic_operator(W output[], const int input[]);

  /**
   * \param f             data functor
   * \param ownIndicatorF optional unique ownership of the indicator functor passed to _indicatorF
   * \param indicatorF    pointer to the indicator functor describing the subset to be integrated
   **/
  SuperLpNorm3D(SuperF3D<T,W>& f, std::unique_ptr<SuperIndicatorF3D<T>>&& ownIndicatorF, SuperIndicatorF3D<T>* indicatorF);
public:
  /**
   * \param f             data functor
   * \param ownIndicatorF unique ownership of the indicator functor describing the subset to be integrated
   **/
  SuperLpNorm3D(SuperF3D<T,W>& f, std::unique_ptr<SuperIndicatorF3D<T>>&& indicatorF);

  /**
   * \param f          data functor
   * \param indicatorF indicator functor describing the subset to be integrated
   **/
  SuperLpNorm3D(SuperF3D<T,W>& f, SuperIndicatorF3D<T>& indicatorF);

  /**
   * Legacy constructor accepting super geometry reference.
   *
   * \param f          data functor
   * \param indicatorF indicator functor describing the subset to be integrated
   **/
  SuperLpNorm3D(SuperF3D<T,W>& f, SuperGeometry3D<T>&, SuperIndicatorF3D<T>& indicatorF);

  /**
   * \param f          data functor
   * \param geometry   super geometry required to construct SuperIndicatorMaterial3D using materials
   * \param materials  vector of material numbers to be included in the Lp norm
   **/
  SuperLpNorm3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& geometry, std::vector<int> materials);

  /**
   * \param f          data functor
   * \param geometry   super geometry required to construct SuperIndicatorMaterial3D using material
   * \param material   single material number to be included in the Lp norm
   **/
  SuperLpNorm3D(SuperF3D<T,W>& f, SuperGeometry3D<T>& geometry, const int material);

  bool operator() (W output[], const int input[]) override;
};


/// Functor that returns the L1 norm over omega of the the euklid norm of the input functor
template <typename T, typename W = T>
using SuperL1Norm3D = SuperLpNorm3D<T,W,1>;

/// Functor that returns the L2 norm over omega of the the euklid norm of the input functor
template <typename T, typename W = T>
using SuperL2Norm3D = SuperLpNorm3D<T,W,2>;

/// Functor that returns the Linf norm over omega of the the euklid norm of the input functor
template <typename T, typename W = T>
using SuperLinfNorm3D = SuperLpNorm3D<T,W,0>;


/// functor counts to get the discrete surface for a material no. in direction (1,0,0), (0,1,0), (0,0,1), (-1,0,0), (0,-1,0), (0,0,-1) and total surface, then it converts it into phys units
template <typename T>
class SuperGeometryFaces3D final : public GenericF<T,int> {
private:
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
  T _latticeL;
  std::vector<BlockGeometryFaces3D<T>* > _blockGeometryFaces;
public:
  template<template<typename U> class DESCRIPTOR>
  SuperGeometryFaces3D(SuperGeometry3D<T>& superGeometry, const int material, const UnitConverter<T,DESCRIPTOR>& converter);
  SuperGeometryFaces3D(SuperGeometry3D<T>& superGeometry, const int material, T _latticeL);
  bool operator() (T output[], const int input[]) override;
  std::vector<T> operator() (std::vector<int> input)
  {
    std::vector<T> output(this->getTargetDim(), T());
    operator()(&output[0],&input[0]);
    return output;
  };
};


/// functor to get pointwise phys force acting on a boundary with a given material on local lattice
template <typename T, template <typename U> class DESCRIPTOR>
class SuperLatticePhysDrag3D final : public SuperLatticePhysF3D<T,DESCRIPTOR> {
private:
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
  SuperGeometryFaces3D<T> _faces;
  SuperLatticePhysBoundaryForce3D<T,DESCRIPTOR> _pBoundForce;
  SuperSum3D<T,T> _sumF;
  T _factor;
public:
  SuperLatticePhysDrag3D(SuperLattice3D<T,DESCRIPTOR>& sLattice,
                         SuperGeometry3D<T>& superGeometry, const int material,
                         const UnitConverter<T,DESCRIPTOR>& converter);
  bool operator() (T output[], const int input[]) override;
};

/// functor to get pointwise phys force acting on a boundary with a given indicator on local lattice
template <typename T, template <typename U> class DESCRIPTOR>
class SuperLatticePhysDragIndicator3D final : public SuperLatticePhysF3D<T,DESCRIPTOR> {
private:
  SuperGeometry3D<T>& _superGeometry;
  ParticleIndicatorSphere3D<T,T>& _indicator;
public:
  SuperLatticePhysDragIndicator3D(SuperLattice3D<T,DESCRIPTOR>& sLattice,
                                  SuperGeometry3D<T>& superGeometry,
                                  ParticleIndicatorSphere3D<T,T>& indicator,
                                  const UnitConverter<T,DESCRIPTOR>& converter);
  bool operator() (T output[], const int input[]) override;
};


/**
 *  functor to get pointwise phys force acting on a boundary with a given material on local lattice
 *  see: Caiazzo, Junk: Boundary Forces in lattice Boltzmann: Analysis of MEA
 */
template <typename T, template <typename U> class DESCRIPTOR>
class SuperLatticePhysCorrDrag3D final : public SuperLatticePhysF3D<T,DESCRIPTOR> {
private:
  SuperGeometry3D<T>& _superGeometry;
  const int _material;
public:
  SuperLatticePhysCorrDrag3D(SuperLattice3D<T,DESCRIPTOR>& sLattice,
                             SuperGeometry3D<T>& superGeometry, const int material,
                             const UnitConverter<T,DESCRIPTOR>& converter);
  bool operator() (T output[], const int input[]) override;
};


/**
 *  functor to get the surface integral of a vector field where the vector field
 *  is represented by a SuperLatticeF functor and the surface is a plane (or area of a circle)
 */
template<typename T, template<typename U> class DESCRIPTOR>
class SuperLatticeFlux3D final : public SuperLatticeF3D<T, DESCRIPTOR> {
private:
  SuperGeometry3D<T>& _sg;
  /// define the plane by a starting point _origin and either a _normal, or two vectors _u and _v
  Vector<T,3> _u;
  Vector<T,3> _v;
  Vector<T,3> _origin;
  Vector<T,3> _normal;
  /// radius of the plane (optional)
  T _rad;
  /// grid length (default=lattice length)
  T _h;
  /// number of points of the (discretized) plane
  int _vox;
  /// Indicator describing relevant subplane
  std::unique_ptr<SuperIndicatorF3D<T>> _ownIndicatorF;
  SuperIndicatorF3D<T>* const           _indicatorF;
  /// functor for interpolation
  AnalyticalFfromSuperF3D<T> _analyticalF;

  /// initializes the member variables ie. defines all variables concerning the plane
  void init(SuperLatticeF3D<T, DESCRIPTOR>& f);
  /// checks if point physR (and its direct neighbours) have the material numbers of _mat (default: _mat=1) ie. are inside the domain
  bool checkInside(std::vector<T> physR, int iC);
  /// interpolates the quantity at all points of the area and sums it up
  void calculate(Vector<T,3>& flow, int xDir, int yDir, int xStart=0, int yStart=0);

public:
  /// define plane by
  /// two vectors
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticeFlux3D(SuperLatticeF3D<T, DESCRIPTOR>& f, SuperGeometry3D<T>& sg,
                     std::vector<T>& u, std::vector<T>& v, std::vector<T> A,
                     T radius = T(), T h = T());
  /// define plane by
  /// two vectors and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticeFlux3D(SuperLatticeF3D<T, DESCRIPTOR>& f, SuperGeometry3D<T>& sg,
                     std::vector<T>& u, std::vector<T>& v, std::vector<T> A,
                     std::list<int> materials, T radius = T(), T h = T());
  /// define plane by
  /// normal
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticeFlux3D(SuperLatticeF3D<T, DESCRIPTOR>& f, SuperGeometry3D<T>& sg,
                     std::vector<T>& n, std::vector<T> A, T radius = T(), T h = T() );
  /// define plane by
  /// normal and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticeFlux3D(SuperLatticeF3D<T, DESCRIPTOR>& f, SuperGeometry3D<T>& sg,
                     std::vector<T>& n, std::vector<T> A, std::list<int> materials,
                     T radius = T(), T h = T());
  /// define plane by
  /// circle
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticeFlux3D(SuperLatticeF3D<T, DESCRIPTOR>& f, SuperGeometry3D<T>& sg,
                     IndicatorCircle3D<T>& circle, T h = T());
  /// define plane by
  /// circle and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticeFlux3D(SuperLatticeF3D<T, DESCRIPTOR>& f, SuperGeometry3D<T>& sg,
                     IndicatorCircle3D<T>& circle, std::list<int> materials, T h = T());

  /// returns vector with
  /// output[0]=flux, output[1]=size of the area and output[2..4]=flow vector (ie. vector of summed up quantity)
  /// if quantity has dimension one: output[0] (=flux) is replaced by the force
  bool operator() (T output[], const int input[]) override;

  //  std::string name() {
  //    return "SuperLatticeFlux3D";
  //  }
  void print(std::string regionName = "", std::string fluxSiScaleName = "", std::string meanSiScaleName = "");
};


template<typename T, template<typename U> class DESCRIPTOR>
class SuperLatticePhysPressureFlux3D final : public SuperLatticeF3D<T, DESCRIPTOR> {
private:
  SuperLatticePhysPressure3D<T, DESCRIPTOR> _p;
  SuperLatticeFlux3D<T, DESCRIPTOR> _fluxF;
  mutable OstreamManager clout;

public:
  /// define plane by
  /// two vectors
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysPressureFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& u, std::vector<T>& v, std::vector<T> A,
                                 T radius = T(), T h = T());
  /// define plane by
  /// two vectors and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysPressureFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& u, std::vector<T>& v, std::vector<T> A,
                                 std::list<int> materials, T radius = T(), T h = T());
  /// define plane by
  /// normal
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysPressureFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& n, std::vector<T> A, T radius = T(),
                                 T h = T());
  ///define plane by
  ///normal and material list
  ///a radius (default=0 -> radius will be initialized as diameter of the geometry)
  ///and grid length h (default=latticeL)
  SuperLatticePhysPressureFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& n, std::vector<T> A,
                                 std::list<int> materials, T radius = T(), T h = T());
  /// define plane by
  /// circle
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysPressureFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 IndicatorCircle3D<T>& circle, T h = T());
  /// define plane by
  /// circle and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysPressureFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 IndicatorCircle3D<T>& circle, std::list<int> materials,
                                 T h = T());

  /// returns vector with
  /// output[0]=flux, output[1]=size of the area and output[2..4]=flow vector (ie. vector of summed up quantity)
  /// if quantity has dimension one: output[0] (=flux) is replaced by the force
  bool operator() (T output[], const int input[]) override;
  //  std::string name() {
  //    return "SuperLatticePhysPresssureFlux3D";
  //  }

  void print(std::string regionName = "", std::string fluxSiScaleName = "N", std::string meanSiScaleName = "Pa");
};

template<typename T, template<typename U> class DESCRIPTOR>
class SuperLatticePhysVelocityFlux3D final : public SuperLatticeF3D<T, DESCRIPTOR> {
private:
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> _vel;
  SuperLatticeFlux3D<T, DESCRIPTOR> _fluxF;
  mutable OstreamManager clout;

public:
  /// define plane by
  /// two vectors u, v
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  /// vector A is origin
  SuperLatticePhysVelocityFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& u, std::vector<T>& v, std::vector<T> A,
                                 T radius = T(), T h = T());
  /// define plane by
  /// two vectors u, v and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  /// vector A is origin
  SuperLatticePhysVelocityFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& u, std::vector<T>& v, std::vector<T> A,
                                 std::list<int> materials, T radius = T(), T h = T());
  /// define plane by
  /// normal vector n
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  /// vector A is origin
  SuperLatticePhysVelocityFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& n, std::vector<T> A, T radius = T(),
                                 T h = T());
  /// define plane by
  /// normal vector n and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  /// vector A is origin
  SuperLatticePhysVelocityFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 std::vector<T>& n, std::vector<T> A, std::list<int> materials,
                                 T radius = T(), T h = T());
  /// define plane by
  /// circle
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysVelocityFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 IndicatorCircle3D<T>& circle, T h = T());
  /// define plane by
  /// circle and material list
  /// a radius (default=0 -> radius will be initialized as diameter of the geometry)
  /// and grid length h (default=latticeL)
  SuperLatticePhysVelocityFlux3D(SuperLattice3D<T, DESCRIPTOR>& sLattice,
                                 const UnitConverter<T,DESCRIPTOR>& converter, SuperGeometry3D<T>& sg,
                                 IndicatorCircle3D<T>& circle,
                                 std::list<int> materials, T h = T());

  /// returns vector with
  /// output[0]=flux, output[1]=size of the area and output[2..4]=flow vector (ie. vector of summed up quantity)
  /// if quantity has dimension one: output[0] (=flux) is replaced by the force
  bool operator() (T output[], const int input[]) override;
  //  std::string name() {
  //    return "SuperLatticePhysVelocityFlux3D";
  //  }

  void print(std::string regionName = "", std::string fluxSiScaleName = "m^3/s", std::string meanSiScaleName = "m/s");
};

template<typename T>
class SuperLatticeMassFlux3D final : public SuperF3D<T>
{
public:
  // define plane by
  // normal and material list
  // a radius (default=0 -> radius will be initialized as diameter of the geometry)
  // and grid length h (default=latticeL)
  SuperLatticeMassFlux3D(SuperF3D<T>& latticeVel, SuperF3D<T>& latticeDensity,
      SuperGeometry3D<T>& sg, T conversationFactorMass, T conversationFactorTime,
      Vector<T, 3>& normal, Vector<T, 3>& origin, std::list<int>& materials, T extent);
  bool operator()(T output[], const int input[]) override;
  void print(std::string regionName, std::string massFluxSiScaleName);
private:
  // functors for interpolation
  AnalyticalFfromSuperF3D<T> _analyticalLatticeVel;
  AnalyticalFfromSuperF3D<T> _analyticalLatticeDensity;

  SuperGeometry3D<T>& _sg;
  // factors convert lattice units into physical ones
  T _conversationFactorMass, _conversationFactorTime;

  // define the plane by a starting point _origin and a _normal
  Vector<T, 3> _normal;
  Vector<T, 3> _origin;
  // list of materials to check on the plane (optional; default=1)
  std::list<int> _materials;
  // maximal extension of the plane
  T _extent;
  // grid length
  T _gridLength;
  // two vectors _spanA and _spanB that lie in the plan
  Vector<T, 3> _spanA;
  Vector<T, 3> _spanB;

  // output stream
  mutable OstreamManager clout;

  // number of points of the (discretized) plane
  int _vox;

  // initializes the member variables ie. defines all variables concerning the plane
  void init( );
  // checks if point physR (and its direct neighbours) have the material numbers of _mat (default: _mat=1) ie. are inside the domain
  bool checkInside(std::vector<T> physR, int iC);
  // interpolates the quantity at all points of the area and sums it up
  void calculate(Vector<T, 3>& flow, int xDir, int yDir, int xStart = 0,
      int yStart = 0);
};



} // end namespace olb

#endif
