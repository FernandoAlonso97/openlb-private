/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2017 Marc Haussmann
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef GNUPLOT_HEATMAP_WRITER_H
#define GNUPLOT_HEATMAP_WRITER_H

#include <iomanip>
#include <iostream>
#include <list>
#include "functors/lattice/blockLatticeReduction2D.hh"
#include "io/gnuplotWriter.h"

namespace olb {

/** This class is derived from gnuplot.
 * It plots heat maps.
 * Heat map matrix data can be stored (keepData).
 * Heat maps can have contours (contour) with number of levels (contourlevel).
 * Available colour schemes are "grey", "pm3d", "blackbody" and "rainbow"
 */
template <typename T, template <typename U> class DESCRIPTOR >
class GnuplotHeatMap : public Gnuplot<T> {
public:
  /// Constructor liveplot is not implemented with HeatMap at the moment
  GnuplotHeatMap(std::string name = "emptyName", bool liveplot = false);
  /// writes PNG resolution is adapted to BlockLatticeReduction resolution
  void writeHeatMapPNG(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                       bool keepData = false, bool contour = false, int contourlevel = 10, std::string colour = "rainbow");
  /// writes PDF
  void writeHeatMapPDF(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                       bool keepData = false, bool contour = false, int contourlevel = 10, std::string colour = "rainbow");
  /// writes JPEG Full HD resolution to ensure best picture quality
  void writeHeatMapJPEG(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                        bool keepData = false, bool contour = false, int contourlevel = 10, std::string colour = "rainbow");
  /// writes CSV data with format: x y value
  void writeHeatMapCSVdata(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                           bool keepData = true);

private:
  std::string _datapath;
  std::string _plotpath;
  std::string _dir;
  bool _keepData;
  bool _contour;
  int _contourlevel;
  std::string _colour;
  int _iT;
  Vector<T,3> _discretenormal;
  Vector<T,3> _origin;
  T _spacing;
  int _nx;
  int _ny;
  std::string _quantityname;

  void writeHeatMapPlotFile(std::string type);

  void writeHeatMapDataFile(BlockLatticeReduction3D<T, DESCRIPTOR>& f, bool csv);


};

}  // namespace olb

#endif

