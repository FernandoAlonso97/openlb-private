/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2017 Marc Haussmann
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef GNUPLOT_HEATMAP_WRITER_HH
#define GNUPLOT_HEATMAP_WRITER_HH

#include <fstream>
#include <iostream>
#include <unistd.h>

#include "core/singleton.h"
#include "io/fileName.h"
#include "functors/lattice/blockLatticeReduction2D.hh"
#include "gnuplotHeatMapWriter.h"
#include "utilities/vectorHelpers.h"

namespace olb {
template <typename T, template <typename U> class DESCRIPTOR >
GnuplotHeatMap<T,DESCRIPTOR>::GnuplotHeatMap(std::string name, bool liveplot)
  : Gnuplot<T>(name, liveplot), _datapath(), _plotpath(),_dir(singleton::directories().getGnuplotOutDir()),
    _keepData(), _contour(), _contourlevel(), _colour(), _iT(), _discretenormal(), _origin(), _spacing(), _nx(), _ny(), _quantityname()
{ }

template <typename T, template <typename U> class DESCRIPTOR >
void GnuplotHeatMap<T,DESCRIPTOR>::writeHeatMapPNG(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                                                  bool keepData, bool contour, int contourlevel, std::string colour)
{
    if ( f.getTargetDim() != 1 ) {
      std::cout << "HeatMap Error: Functor targetDim is not 1. " << std::endl;
      exit(-1);
    } else {
      if ( singleton::mpi().getRank() == 0 ) {
        _plotpath = name;
        _keepData = keepData;
        _contour = contour;
        _contourlevel = contourlevel;
        _colour = colour;
        _iT = iT;
        _nx = f.getBlockStructure().getNx();
        _ny = f.getBlockStructure().getNy();
        _discretenormal = crossProduct3D(f.getVectorU(),f.getVectorV());
        _discretenormal.normalize(1.);
        _origin = f.getPhysOrigin();
        _spacing = f.getPhysSpacing();
        _quantityname = f.getName();
        _quantityname.erase (_quantityname.begin(), _quantityname.begin()+15);
        _quantityname.pop_back();
        writeHeatMapDataFile(f, false);
        writeHeatMapPlotFile("png");
        this->startGnuplot("heatMapPNG");
      }
    }
  return;
}

template <typename T, template <typename U> class DESCRIPTOR >
void GnuplotHeatMap<T,DESCRIPTOR>::writeHeatMapPDF(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                                                   bool keepData, bool contour, int contourlevel, std::string colour)
{
    if ( f.getTargetDim() != 1 ) {
      std::cout << "HeatMap Error: Functor targetDim is not 1. " << std::endl;
      exit(-1);
    } else {
      if ( singleton::mpi().getRank() == 0 ) {
        _plotpath = name;
        _keepData = keepData;
        _contour = contour;
        _contourlevel = contourlevel;
        _colour = colour;
        _iT = iT;
        _nx = f.getBlockStructure().getNx();
        _ny = f.getBlockStructure().getNy();
        _discretenormal = crossProduct3D(f.getVectorU(),f.getVectorV());
        _discretenormal.normalize(1.);
        _origin = f.getPhysOrigin();
        _spacing = f.getPhysSpacing();
        _quantityname = f.getName();
        _quantityname.erase (_quantityname.begin(), _quantityname.begin()+15);
        _quantityname.pop_back();
        writeHeatMapDataFile(f, false);
        writeHeatMapPlotFile("pdf");
        this->startGnuplot("heatMapPDF");
      }
    }
  return;
}

template <typename T, template <typename U> class DESCRIPTOR >
void GnuplotHeatMap<T,DESCRIPTOR>::writeHeatMapJPEG(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                                                    bool keepData, bool contour, int contourlevel, std::string colour)
{
    if ( f.getTargetDim() != 1 ) {
      std::cout << "HeatMap Error: Functor targetDim is not 1. " << std::endl;
      exit(-1);
    } else {
      if ( singleton::mpi().getRank() == 0 ) {
        _plotpath = name;
        _keepData = keepData;
        _contour = contour;
        _contourlevel = contourlevel;
        _colour = colour;
        _iT = iT;
        _nx = f.getBlockStructure().getNx();
        _ny = f.getBlockStructure().getNy();
        _discretenormal = crossProduct3D(f.getVectorU(),f.getVectorV());
        _discretenormal.normalize(1.);
        _origin = f.getPhysOrigin();
        _spacing = f.getPhysSpacing();
        _quantityname = f.getName();
        _quantityname.erase (_quantityname.begin(), _quantityname.begin()+15);
        _quantityname.pop_back();
        writeHeatMapDataFile(f, false);
        writeHeatMapPlotFile("jpeg");
        this->startGnuplot("heatMapJPEG");
      }
    }
  return;
}

template <typename T, template <typename U> class DESCRIPTOR >
void GnuplotHeatMap<T,DESCRIPTOR>::writeHeatMapCSVdata(BlockLatticeReduction3D<T, DESCRIPTOR>& f, int iT, std::string const& name,
                                                       bool keepData)
{
    if ( f.getTargetDim() != 1 ) {
      std::cout << "HeatMap Error: Functor targetDim is not 1. " << std::endl;
      exit(-1);
    } else {
      if ( singleton::mpi().getRank() == 0 ) {
        _plotpath = name;
        _keepData = keepData;
        _iT = iT;
        _nx = f.getBlockStructure().getNx();
        _ny = f.getBlockStructure().getNy();
        _discretenormal = crossProduct3D(f.getVectorU(),f.getVectorV());
        _discretenormal.normalize(1.);
        _origin = f.getPhysOrigin();
        _spacing = f.getPhysSpacing();
        writeHeatMapDataFile(f, true);
      }
    }
  return;
}

template <typename T, template <typename U> class DESCRIPTOR >
void GnuplotHeatMap<T,DESCRIPTOR>::writeHeatMapPlotFile(std::string type)
{
  std::ofstream fout;

  std::string plotFile;
  if (type == "pdf") {
    plotFile = singleton::directories().getGnuplotOutDir()+"data/heatMapPDF.p";
  } else if (type == "png") {
    plotFile = singleton::directories().getGnuplotOutDir()+"data/heatMapPNG.p";
  } else if (type == "jpeg") {
    plotFile = singleton::directories().getGnuplotOutDir()+"data/heatMapJPEG.p";
  } else {
    std::cout << "WARNING: invalid Gnuplot type={'pdf', 'png'}" << std::endl;
    exit(-1);
  }

  fout.open(plotFile.c_str(), std::ios::trunc);

  if (type=="pdf") {
    fout << "set terminal pdf enhanced" << "\n"
         << "set output '"<< _plotpath <<".pdf'" << "\n";
  }
  if (type=="png") {
    fout << "set terminal png " << "size " << _nx +100  << "," << _ny + 100 << "\n"
         << "set output '"<< _plotpath <<".png'" << "\n";
  }
  if (type=="jpeg") {
    fout << "set terminal jpeg " << "size " << 1920  << "," << 1080 << "font \"Helvetica,25\"" << "\n"
         << "set output '"<< _plotpath <<".jpeg'" << "\n";
  }

  fout << "set pm3d map" << "\n";
  fout << "unset key" << "\n";
  fout << "set size ratio -1" << "\n";
  fout << "set xtics out" << "\n";
  fout << "set ytics out" << "\n";
  fout << "set xtics nomirror" << "\n";
  fout << "set ytics nomirror" << "\n";

  if ( util::nearZero(_discretenormal[0]) && util::nearZero(_discretenormal[1]) ) {
    fout << "set xlabel \"x in m \"" << "\n"
         << "set ylabel \"y in m \"" << "\n";
  } else if ( util::nearZero(_discretenormal[0]) && util::nearZero(_discretenormal[2]) ) {
    fout << "set xlabel \"x in m \"" << "\n"
         << "set ylabel \"z in m \"" << "\n";
    _origin[1] = _origin[2];
  } else if ( util::nearZero(_discretenormal[1]) && util::nearZero(_discretenormal[2]) ) {
    fout << "set xlabel \"y in m \"" << "\n"
         << "set ylabel \"z in m \"" << "\n";
    _origin[0] = _origin[1];
    _origin[1] = _origin[2];
  }

  if (_contour) {
    fout << "set contour base" << "\n";
    fout << "set cntrparam levels " << _contourlevel << "\n";
    fout << "set cntrparam bspline" << "\n";
    fout << "do for [i=1:"<< _contourlevel <<"] {" << "\n";
    fout << "set linetype i lc rgb \"black\""<< "\n";
    fout << "}" << "\n";
  }

  fout << "set cblabel offset 0.5 \"" <<_quantityname;
  if (_quantityname == "EuklidNorm(physVelocity)") {
    fout << " in m/s\"" << "\n";
  } else if (_quantityname == "physPressure") {
    fout << " in Pa\"" << "\n";
  } else {
    fout << "\"\n";
  }

  fout << "set autoscale fix" << "\n";
  if (_colour == "grey") {
    fout << "set palette grey" << "\n";
  } else if (_colour == "pm3d") {

  } else if (_colour == "blackbody") {
    fout << "set palette defined ( 0 \"black\", 1 \"red\", 2 \"yellow\")" << "\n";
  }
  else {
    fout << "set palette defined ( 0 \"blue\", 1 \"green\", 2 \"yellow\", 3 \"orange\", 4 \"red\" )" << "\n";
  }
  fout << "splot '" << _datapath << "' u (($1+" << _origin[0] << ")*" << _spacing << "):"
                    << "(($2+" << _origin[1] << ")*" << _spacing <<"):3 matrix with pm3d" << "\n";
  fout.close();
  return;
}

template <typename T, template <typename U> class DESCRIPTOR >
void GnuplotHeatMap<T,DESCRIPTOR>::writeHeatMapDataFile(BlockLatticeReduction3D<T, DESCRIPTOR>& f, bool csv)
{
  std::string suffix;
  if (csv){
   suffix = ".csv";
  } else {
   suffix = ".matrix";
  }

  if(_keepData) {
    _datapath = createFileName( _dir + "data/", _plotpath, _iT) + suffix;
  } else {
    _datapath = _dir + "data/" + _plotpath  + suffix;
  }
  _plotpath = createFileName( _dir, _plotpath, _iT);
  std::ofstream fout( _datapath.c_str() );
  int i[2] = {0,0};
  if (!csv){
    for (i[1] = 0; i[1] < _ny; i[1]++) {
      for (i[0] = 0; i[0] < _nx; i[0]++) {
        T evaluated[1];
        f(evaluated,i);
        fout << evaluated[0] << " ";
        if (i[0] == _nx - 1) {
          fout  << "\n";
        }
      }
    }
  } else {
    if ( util::nearZero(_discretenormal[0]) && util::nearZero(_discretenormal[2]) ) {
      _origin[1] = _origin[2];
    } else if ( util::nearZero(_discretenormal[1]) && util::nearZero(_discretenormal[2]) ) {
      _origin[0] = _origin[1];
      _origin[1] = _origin[2];
    }
    for (i[1] = 0; i[1] < _ny; i[1]++) {
      for (i[0] = 0; i[0] < _nx; i[0]++) {
      T evaluated[1];
      f(evaluated,i);
      fout << _origin[0] + _spacing * i[0] << " " << _origin[1] + _spacing * i[1] << " " << evaluated[0] << "\n";
      }
    }
  }
  fout.close();
  return;
}




}  // namespace olb

#endif

