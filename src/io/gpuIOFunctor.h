#ifndef GPU_IO_FUNCTORS_H
#define GPU_IO_FUNCTORS_H

#include <fstream>
#include <iostream>
#include <exception>
#include "mpi.h"

namespace olb {

class inputFileToShortExecption: public std::exception {

	virtual const char* what() const throw()
        {
		return "input File does not have enough lines";
	}

} inputFileToShort;

class inputFileNonExistentExecption: public std::exception {

	virtual const char* what() const throw()
        {
		return "input File does not exist";
	}

} inputFileNonExistent;

template<typename T,typename Lattice>
void writeFluidMasktoFile (std::string filename,bool * fluidMask,const SubDomainInformation<T,Lattice>& subDomInfo,size_t iXS, size_t iXE, size_t iYS, size_t iYE, size_t iZS, size_t iZE)
{
    std::ios_base::sync_with_stdio(false);
    auto myfile = std::fstream(filename, std::ios::out | std::ios::binary);
    for(size_t iX=iXS;iX<iXE;++iX)
      for(size_t iY=iYS;iY<iYE;++iY)
        for (size_t iZ=iZS;iZ<iZE;++iZ)
        {
          
          Index3D localIndex;
          if (subDomInfo.isLocal(iX,iY,iZ,localIndex))
          {
            auto index = util::getCellIndex3D(localIndex[0],localIndex[1],localIndex[2],subDomInfo.localGridSize()[1],subDomInfo.localGridSize()[2]);
            int value = static_cast<int>(fluidMask[index]);
            if (iZ == subDomInfo.localGridSize()[2]-2)
              value = static_cast<int>(false);
            myfile.write((char*)&value, sizeof(int));
          }
        
        }

    myfile.close();
}


template<typename T,typename Lattice>
void writeFluidMasktoFileMultiLattice (std::string filename,bool * fluidMask,const DomainInformation<T,Lattice>& domInfo,size_t iXS, size_t iXE, size_t iYS, size_t iYE, size_t iZS, size_t iZE)
{

  MPI_Win window;
  MPI_Win_create(fluidMask,sizeof(bool)*domInfo.getLocalInfo().localGridSize()[0]*domInfo.getLocalInfo().localGridSize()[1]*domInfo.getLocalInfo().localGridSize()[2],sizeof(bool),MPI_INFO_NULL,MPI_COMM_WORLD,&window);

  std::cout << "inside writeFluidMASK" << std::endl;
  if (domInfo.localSubDomain == 0)
  {
    std::ios_base::sync_with_stdio(false);
    auto myfile = std::fstream(filename, std::ios::out | std::ios::binary);
    for(size_t iX=iXS;iX<iXE;++iX)
      for(size_t iY=iYS;iY<iYE;++iY)
        for (size_t iZ=iZS;iZ<iZE;++iZ)
        {
          
          Index3D localIndex;
          bool value;
          for (unsigned domain=0;domain<domInfo.noSubDomains;++domain)
          {
            if (domInfo.subDomains[domain].isLocal(iX,iY,iZ,localIndex))
            {
              auto localIdx = util::getCellIndex3D(localIndex[0],localIndex[1],localIndex[2],domInfo.subDomains[domain].localGridSize()[1],domInfo.subDomains[domain].localGridSize()[2]);
            
              MPI_Win_lock(MPI_LOCK_SHARED, domain, 0, window);
              MPI_Get(&value,1,MPI_CXX_BOOL,domain,localIdx,1,MPI_CXX_BOOL,window);
              MPI_Win_unlock(domain, window);
              // int MPI_Get(void *origin_addr, int origin_count, MPI_Datatype
                              // origin_datatype, int target_rank, MPI_Aint target_disp,
                                          // int target_count, MPI_Datatype target_datatype, MPI_Win
                                                      // win)
              int valueInt = static_cast<int>(value);
              if (iZ == domInfo.subDomains[domain].localGridSize()[2]-2)
                valueInt = static_cast<int>(false);
              myfile.write((char*)&valueInt, sizeof(int));
            }
          }
        }

    myfile.close();
  }

  MPI_Barrier (MPI_COMM_WORLD);

  MPI_Win_free(&window);
  std::cout << "endOf writeFluidMASK" << std::endl;
}


template<typename T,typename Lattice>
void readFluidMaskfromFile (std::string filename,bool * fluidMask,const SubDomainInformation<T,Lattice>& subDomInfo, size_t iXS, size_t iXE, size_t iYS, size_t iYE, size_t iZS, size_t iZE)
{
    auto myfile = std::fstream(filename, std::ios::in | std::ios::binary);
    if (myfile.is_open())
    {
    for(size_t iX=iXS;iX<iXE;++iX)
      for(size_t iY=iYS;iY<iYE;++iY)
        for (size_t iZ=iZS;iZ<iZE;++iZ)
        {
          Index3D localIndex;
          if (subDomInfo.isLocal(iX,iY,iZ,localIndex))
          {
            auto index = util::getCellIndex3D(localIndex[0],localIndex[1],localIndex[2],subDomInfo.localGridSize()[1],subDomInfo.localGridSize()[2]);
            int value;
            myfile.read((char*)&value,sizeof(int));
            fluidMask[index]=static_cast<bool>(value);
          }
        }

    myfile.close();
    }
    else
       throw inputFileNonExistent;
}

namespace gpuFunctor {

template<typename T,template <typename U> class Lattice>
class WriteRhoUFunctor{

	public:
	WriteRhoUFunctor() = delete;

	WriteRhoUFunctor(DomainInformation<T,Lattice<T>> domInfo,size_t iX0, size_t iX1, size_t iY0, size_t iY1, size_t iZ0, size_t iZ1):
      domInfo_{domInfo},
			startIndices{iX0,iY0,iZ0},
			endIndices{iX1,iY1,iZ1},
			size_((iX1-iX0)*(iY1-iY0)*(iZ1-iZ0))
	{
		for (int i =0;i<Lattice<T>::d+1;++i)
			HANDLE_ERROR(cudaMallocManaged(&data_[i],sizeof(T)*size_));

	}

    	OPENLB_HOST_DEVICE
    	void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
	{
	
		size_t indices[3];
		util::getCellIndices3D(threadIndex,domInfo_.getLocalInfo().localGridSize()[1],domInfo_.getLocalInfo().localGridSize()[2],indices);

    Index3D globalIndices = domInfo_.getLocalInfo().getGlobalIndex(Index3D{indices[0],indices[1],indices[2],domInfo_.getLocalInfo().localSubDomain});
	  
		if ((globalIndices[0]>=startIndices[0] && globalIndices[0]<endIndices[0]) && (globalIndices[1]>=startIndices[1] && globalIndices[1]<endIndices[1]) && (globalIndices[2]>=startIndices[2] && globalIndices[2]<endIndices[2]))
			{

		 		size_t localIndex = util::getCellIndex3D(indices[0]-startIndices[0],
									 indices[1]-startIndices[1],
									 indices[2]-startIndices[2],
									 endIndices[1]-startIndices[1],
									 endIndices[2]-startIndices[2]);
				
				data_[0][localIndex] = cellData[Lattice<T>::rhoIndex][threadIndex];		
				for (int i=0;i<Lattice<T>::d;++i)
					data_[1+i][localIndex] = cellData[Lattice<T>::uIndex+i][threadIndex];
			
			}
		else 
			return;

	}

  void writeToSingleBinaryFile(std::string filename)
  {

    MPI_Win window[Lattice<T>::d+1];
    for (unsigned i=0;i<Lattice<T>::d+1;++i)
    MPI_Win_create(data_[i],sizeof(T)*size_,sizeof(T),MPI_INFO_NULL,MPI_COMM_WORLD,&window[i]);

    if (domInfo_.getLocalInfo().localSubDomain == 0)
    {
      std::ofstream outfile;
      outfile.open (filename,std::ios::binary);
		  if (outfile.is_open())
      {
      for (unsigned iX = startIndices[0];iX<endIndices[0];++iX)
        for (unsigned iY = startIndices[1];iY<endIndices[1];++iY)
          for (unsigned iZ = startIndices[2];iZ<endIndices[2];++iZ)
          {
            Index3D localIndex;
            T value;
            for (unsigned domain=0;domain<domInfo_.noSubDomains;++domain)
            {
              if (domInfo_.subDomains[domain].isLocal(iX,iY,iZ,localIndex))
              {
		 		        size_t localIdx = util::getCellIndex3D(iX-startIndices[0],
									 iY-startIndices[1],
									 iZ-startIndices[2],
									 endIndices[1]-startIndices[1],
									 endIndices[2]-startIndices[2]);
				
            
                for(int i=0;i<Lattice<T>::d+1;++i) // +1 as rho und u is written (see size of data_)
                {
                  MPI_Win_lock(MPI_LOCK_SHARED,domain, 0, window[i]);
                  MPI_Get(&value,1,MPI_DOUBLE,domain,localIdx,1,MPI_DOUBLE,window[i]);
                  MPI_Win_unlock(domain, window[i]);
                  outfile.write(reinterpret_cast<const char*>(&value),sizeof(T));
                }
              
              }
            }
          }
        outfile.close();
      }
      else 
        throw inputFileNonExistent;
    }

    MPI_Barrier (MPI_COMM_WORLD);
    for (unsigned i=0;i<Lattice<T>::d+1;++i)
      MPI_Win_free (&window[i]);
  
  }

	void writeToFile (std::string filename)
	{
    std::ofstream outfile;
		outfile.open (filename);

		for(int i=0;i<size_;++i)
		{
			outfile << i << " ";
			for (int thingIdx=0;thingIdx<Lattice<T>::d+1;++thingIdx)
			    outfile << data_[thingIdx][i] << " ";
			outfile << "\n";

		}
		outfile.close();
	}

	void writeToBinaryFile (std::string filename)
	{
    std::ofstream outfile;
    outfile.open (filename,std::ios::binary);
		if (outfile.is_open())
    {
      for(int i=0;i<Lattice<T>::d+1;++i) // +1 as rho und u is written (see size of data_)
      outfile.write(reinterpret_cast<const char*>(data_[i]),size_*sizeof(T));

		  outfile.close();
    }
    else 
      throw inputFileNonExistent;

	}

	//private:
	public:
  DomainInformation<T,Lattice<T>> domInfo_;
	size_t startIndices[Lattice<T>::d];
	size_t endIndices[Lattice<T>::d];
	size_t size_ = 0;

	T* data_[Lattice<T>::d+1];


 //friend T** copyToAllocatedPointer (WriteRhoUFunctor<T,Lattice>& functor,T velocityConversion);
};


template<typename T,template <typename> class Lattice>
T** copyToAllocatedPointer (WriteRhoUFunctor<T,Lattice>& functor, T velocityConversion)
{

    T invVelocityConversion = 1.0/velocityConversion;
		T** velocityField;
		velocityField = new T*[3];
		for(size_t iDim = 0; iDim < 3; ++iDim)
			velocityField[iDim] = new T[functor.size_];

    for(int dim=0;dim<3;++dim)
      for (int index=0;index<functor.size_;++index)
        velocityField[dim][index] = invVelocityConversion*functor.data_[dim+1][index];


    return velocityField;
}

template<typename T,template <typename U> class Lattice>
class ReadRhoUFunctor{

	public:
	ReadRhoUFunctor() = delete;

	ReadRhoUFunctor(DomainInformation<T,Lattice<T>> domInfo,size_t iX0, size_t iX1, size_t iY0, size_t iY1, size_t iZ0, size_t iZ1):
      domInfo_{domInfo},
			startIndices{iX0,iY0,iZ0},
			endIndices{iX1,iY1,iZ1},
			size_((iX1-iX0)*(iY1-iY0)*(iZ1-iZ0))
	{
		for (int i =0;i<Lattice<T>::d+1;++i)
			HANDLE_ERROR(cudaMallocManaged(&data_[i],sizeof(T)*size_));

	}

    	OPENLB_HOST_DEVICE
    	void operator() (T* const OPENLB_RESTRICT * const OPENLB_RESTRICT cellData,size_t threadIndex) const
	{
	
		size_t indices[3];
		util::getCellIndices3D(threadIndex,domInfo_.getLocalInfo().localGridSize()[1],domInfo_.getLocalInfo().localGridSize()[2],indices);

    Index3D globalIndices = domInfo_.getLocalInfo().getGlobalIndex(Index3D{indices[0],indices[1],indices[2],domInfo_.getLocalInfo().localSubDomain});
	  
		if ((globalIndices[0]>=startIndices[0] && globalIndices[0]<endIndices[0]) && (globalIndices[1]>=startIndices[1] && globalIndices[1]<endIndices[1]) && (globalIndices[2]>=startIndices[2] && globalIndices[2]<endIndices[2]))
			{

		 		size_t localIndex = util::getCellIndex3D(indices[0]-startIndices[0],
									 indices[1]-startIndices[1],
									 indices[2]-startIndices[2],
									 endIndices[1]-startIndices[1],
									 endIndices[2]-startIndices[2]);

        T u[3] = {0.0,0.0,0.0};
        T rho = 0.0;

				cellData[Lattice<T>::rhoIndex][threadIndex] = data_[0][localIndex];		
				rho = data_[0][localIndex];		

				for (int i=0;i<Lattice<T>::d;++i)
        {
					cellData[Lattice<T>::uIndex+i][threadIndex] = data_[1+i][localIndex];
          u[i]=data_[1+i][localIndex];
        }

        T uSqr = util::normSqr<T,Lattice<T>::d>(u);
        for (int iPop=0; iPop<Lattice<T>::q; ++iPop) {
           cellData[iPop][threadIndex] = lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
        }
			
			}
		else 
			return;

	}

	void readFromFile (std::string filename)
	{
    std::ifstream infile;
		infile.open (filename);
		std::string line;
  		 if (infile.is_open())
  		 {
		  size_t count = 0;
    		 while ( std::getline (infile,line) )
    		 {
           std::istringstream ss(line);
			int index=0;
			ss >> index;

			if (index<size_)
			for (int i=0;i<Lattice<T>::d+1;++i)
			    ss >> data_[i][index];
			++count;
			
    		 }
    		 infile.close();
                 if (count != size_)
			throw inputFileToShort;
  		}
		else 
		   throw inputFileNonExistent;
	}
	void readFromBinaryFile (std::string filename)
	{
    std::ifstream infile;
    infile.open (filename, std::ios::binary);
		if (infile.is_open())
    {
      for(int i=0;i<Lattice<T>::d+1;++i) // +1 as rho und u is written (see size of data_)
      infile.read(reinterpret_cast<char*>(data_[i]),size_*sizeof(T));

		  infile.close();
    }
    else 
      throw inputFileNonExistent;

	}

	// private:
	public:
  DomainInformation<T,Lattice<T>> domInfo_;
	size_t startIndices[Lattice<T>::d];
	size_t endIndices[Lattice<T>::d];
	size_t size_ = 0;

	T* data_[Lattice<T>::d+1];

  //friend T** copyToAllocatedPointer (ReadRhoUFunctor<T,Lattice>& functor,T velocityConversion);

};

template<typename T,template <typename> class Lattice>
T** copyToAllocatedPointer (ReadRhoUFunctor<T,Lattice>& functor, T velocityConversion)
{

    T invVelocityConversion = 1.0/velocityConversion;
		T** velocityField;
		velocityField = new T*[3];
		for(size_t iDim = 0; iDim < 3; ++iDim)
			velocityField[iDim] = new T[functor.size_];

    for(int dim=0;dim<3;++dim)
      for (int index=0;index<functor.size_;++index)
        velocityField[dim][index] = invVelocityConversion*functor.data_[dim+1][index];


    return velocityField;
}
} // end of namespace gpuFunctor
} // end of namespace
#endif
