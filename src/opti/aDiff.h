/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of a algoritmic differentiation data type
 * using the forward method -- header file.
 */

#ifndef A_DIFF_H
#define A_DIFF_H

#ifdef ADT

#include <math.h>
#include <cstdlib>
#include <iostream>


// Use BaseType double as the default BaseType.
#ifndef BaseType
#define BaseType S
#endif

/// All OpenLB code is contained in this namespace.
//namespace olb {

//namespace opti {

/// Definition of a description of a algoritmic differentiation
/// data type using the forward method
/** An ADf is a data type which enables the calculation of
 * derivatives by means of algorithmic differentiation.
 *
 * This class is not intended to be derived from.
 */


template <class T, unsigned DIM>
class ADf {
  // private:
public:
  /// value
  T _v;
  /// derivatives
  T _d[DIM];

  // class functions
  ADf();

  //explicit ADf(const T& v);
  ADf(const T& v);
  ADf(const ADf& a);
  ADf(const ADf& a, bool b);

  T& v();
  T& d(unsigned i);

  ADf& operator = (const ADf& a);
  ADf& operator = (const T& v);

  ADf& operator += (const ADf& a);
  ADf& operator += (const T& v);

  ADf& operator -= (const ADf& a);
  ADf& operator -= (const T& v);

  ADf& operator *= (const ADf& a);
  ADf& operator *= (const T& v);

  ADf& operator /= (const ADf& a);
  ADf& operator /= (const T& v);

  void setDiffVariable(unsigned iD);

  bool hasZeroDerivative() const;
  operator BaseType();
};



// class functions
template <class T, unsigned DIM>
inline ADf<T,DIM>::ADf():_v(T())
{
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]=T();
  }
}

template <class T, unsigned DIM>
inline ADf<T,DIM>::ADf(const T& v):_v(v)
{
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]=T();
  }
}

template <class T, unsigned DIM>
inline ADf<T,DIM>::ADf(const ADf& a):_v(a._v)
{
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]=a._d[i];
  }
}

template <class T, unsigned DIM>
inline ADf<T,DIM>::ADf(const ADf& a, bool b):_v(b?T(1)/a._v:-a._v)
{
  if (b) {
    for (unsigned i=0; i<DIM; ++i) {
      _d[i]=-a._d[i];
      _d[i]*=_v;
      _d[i]*=_v;
    }
  } else {
    for (unsigned i=0; i<DIM; ++i) {
      _d[i]=-a._d[i];
    }
  }
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator= (const ADf& a)
{
  _v=a._v;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]=a._d[i];
  }
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator= (const T& v)
{
  _v=v;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]=T();
  }
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator += (const ADf<T,DIM>& a)
{
  _v+=a._v;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]+=a._d[i];
  }
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator += (const T& v)
{
  _v+=v;
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator -= (const ADf<T,DIM>& a)
{
  _v-=a._v;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]-=a._d[i];
  }
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator -= (const T& v)
{
  _v-=v;
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator *= (const ADf<T,DIM>& a)
{
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]*=a._v;
    _d[i]+=_v*a._d[i];
  }
  _v*=a._v;
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator *= (const T& v)
{
  _v*=v;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]*=v;
  }
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator /= (const ADf<T,DIM>& a)
{
  T tmp(T(1)/a._v);
  _v*=tmp;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]-=_v*a._d[i];
    _d[i]*=tmp;
  }
  return *this;
}

template <class T, unsigned DIM>
inline ADf<T,DIM>& ADf<T,DIM>::operator /= (const T& v)
{
  T tmp(T(1)/v);
  _v*=tmp;
  for (unsigned i=0; i<DIM; ++i) {
    _d[i]*=tmp;
  }
  return *this;
}

template <class T, unsigned DIM>
inline T& ADf<T,DIM>::v()
{
  return _v;
}

template <class T, unsigned DIM>
inline T& ADf<T,DIM>::d(unsigned i)
{
  return _d[i];
}

template <class T, unsigned DIM>
inline void ADf<T,DIM>::setDiffVariable(unsigned iD)
{
  unsigned i;
  for (i=0; i<iD; ++i) {
    _d[i]=T();
  }
  _d[iD]=T(1);
  for (i=iD+1; i<DIM; ++i) {
    _d[i]=T();
  }
}

template <class T, unsigned DIM>
inline std::ostream& operator << (std::ostream& os, const ADf<T,DIM>& o)
{
  os << o._v;
  if (DIM!=0) {
    os << " [ ";
    for (unsigned i=0; i<DIM; ++i) {
      os << o._d[i] << " ";
    }
    os << "]";
  }
  return os;
}










// indirect helper functions, providing general arithmetics
// overloading of operators for interaction with BaseType and itself
// addition, subtraction, multiplication and division
template <class T, unsigned DIM>
inline ADf<T,DIM> operator+ (const BaseType& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(b)+=a;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator+ (const ADf<T,DIM>& a,const BaseType& b)
{
  return ADf<T,DIM>(a)+=b;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator+ (const ADf<T,DIM>& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(a)+=b;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator- (const BaseType& a, const ADf<T,DIM>& b)
{
  //TODO was plus now minus!
  return ADf<T,DIM>(b,0)-=a;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator- (const ADf<T,DIM>& a,const BaseType& b)
{
  return ADf<T,DIM>(a)-=b;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator- (const ADf<T,DIM>& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(a)-=b;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator* (const BaseType& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(b)*=a;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator* (const ADf<T,DIM>& a,const BaseType& b)
{
  return ADf<T,DIM>(a)*=b;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator* (const ADf<T,DIM>& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(a)*=b;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator/ (const BaseType& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(b,1)*=a;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator/ (const ADf<T,DIM>& a,const BaseType& b)
{
  return ADf<T,DIM>(a)/=b;
}


template <class T, unsigned DIM>
inline ADf<T,DIM> operator/ (const ADf<T,DIM>& a, const ADf<T,DIM>& b)
{
  return ADf<T,DIM>(a)/=b;
}

/* Unary operators */
template <class T, unsigned DIM>
inline ADf<T,DIM> operator+ (const ADf<T,DIM>& a)
{
  return ADf<T,DIM>(a);
}

template <class T, unsigned DIM>
inline ADf<T,DIM> operator- (const ADf<T,DIM>& a)
{
  return ADf<T,DIM>(a,0);
}










// General arithmetics for interaction with objects of the first template
// parameter. This enables computations of second derivatives by pyramiding
// ADf.
// addition, subtraction, multiplication and division
template <class T, unsigned DIM>
inline ADf<T,DIM> add1(const T& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(a+b._v);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=b._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> add2(const ADf<T,DIM>& a,const T& b)
{
  ADf<T,DIM> c(a._v+b);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> sub1(const T& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(a-b._v);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=-b._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> sub2(const ADf<T,DIM>& a,const T& b)
{
  ADf<T,DIM> c(a._v-b);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> mul1(const T& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(a*b._v);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=b._d[i]*a;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> mul2(const ADf<T,DIM>& a,const T& b)
{
  ADf<T,DIM> c(a._v*b);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*b;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> di_v1(const T& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(a/b._v);
  T tmp(-c._v/b._v);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=tmp*b._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> di_v2(const ADf<T,DIM>& a,const T& b)
{
  ADf<T,DIM> c(a._v/b);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=(a._d[i])/b;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> pow1(const T& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(pow(a,b._v));
  T tmp(b._v*pow(a,b._v-T(1))),tmp1(c._v*lo_d(a));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=tmp1*b._d[i];
  }
  return c;
}
template <class T, unsigned DIM>
inline ADf<T,DIM> pow2(const ADf<T,DIM>& a, const T& b)
{
  ADf<T,DIM> c(pow(a._v,b));
  T tmp(b*pow(a._v,b-T(1))),tmp1(c._v*lo_d(a._v));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=tmp*a._d[i];
  }
  return c;
}










// extended math functions:
// pow, sqr, exp, lo_d, sqrt, sin, cos, tan, asin, acos, atan
template <class T, unsigned DIM>
inline ADf<T,DIM> pow (const BaseType& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(pow(a,b._v));
  T tmp(c._v*lo_d(ADf<T,DIM>(a)));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=tmp*b._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> pow (const ADf<T,DIM>& a,const BaseType& b)
{
  ADf<T,DIM> c(pow(a._v,b));
  T tmp(b*pow(a._v,b-T(1)));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=tmp*a._d[i];
  }
  return c;
}


template <class T, unsigned DIM>
inline ADf<T,DIM> pow (const ADf<T,DIM>& a, const ADf<T,DIM>& b)
{
  ADf<T,DIM> c(pow(a._v,b._v));
  T tmp(b._v*pow(a._v,b._v-T(1))),tmp1(c._v*/*lo_d*/(a._v)); //TODO
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=tmp*a._d[i]+tmp1*b._d[i];
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> pow (const ADf<T,DIM>& a,int b)
{
  ADf<T,DIM> c(pow(a._v,b));
  T tmp(b*pow(a._v,b-1));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> sqr (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c((a._v)*(a._v));
  T tmp(T(2)*a._v);
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> exp (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(exp(a._v));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*c._v;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> lo_d (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(lo_d(a._v));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]/a._v;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> sqrt (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(sqrt(a._v));
  T tmp(c._v*T(2));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]/tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> sin (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(sin(a._v));
  T tmp(cos(a._v));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> cos (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(cos(a._v));
  T tmp(-sin(a._v));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> tan (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(tan(a._v));
  T tmp(T(1)+(c._v)*(c._v));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> asin (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(asin(a._v));
  T tmp(T(1)/sqrt(T(1)-(a._v)*(a._v)));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> acos (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(acos(a._v));
  T tmp(-T(1)/sqrt(T(1)-(a._v)*(a._v)));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}

template <class T, unsigned DIM>
inline ADf<T,DIM> atan (const ADf<T,DIM>& a)
{
  ADf<T,DIM> c(atan(a._v));
  T tmp(T(1)/(T(1)+(a._v)*(a._v)));
  for (unsigned i=0; i<DIM; ++i) {
    c._d[i]=a._d[i]*tmp;
  }
  return c;
}










/// tests if ADf has only zero derivatives
template <class T, unsigned DIM>
inline bool ADf<T,DIM>::hasZeroDerivative() const
{
  // Achtung, sehr gefaehlich! x=42.000
  //return true;

  bool retValue = false;
  T sum = 0;
  for (unsigned i=0; i<DIM; ++i) {
    sum+=fabs(_d[i]);
  }
  if (sum==0) {
    retValue=true;
  }
  return retValue;
}

// mj ACHTUNG
// l: ohne Ruecksicht auf die Ableitungen
/*
template <class T, unsigned DIM>
inline ADf<T,DIM> std::fabs (const ADf<T,DIM>& a)
{
    if (a.hasZeroDerivative() != true) {
        std::cout << "Fehler in fabs" << std::endl;
        exit(1);
    }
  ADf<T,DIM> c(fabs(a._v));
  return c;
}
*/

template <class T, unsigned DIM>
inline ADf<T,DIM>& std::max (const ADf<T,DIM>& a, const ADf<T,DIM>& b)
{
  if ((a.hasZeroDerivative() != true)&&(b.hasZeroDerivative() != true)) {
    std::cout << "Fehler in max" << std::endl;
    exit(1);
  }
  if (a>=b) {
    return a;
  }
  return b;
}









// boolean comparison operators: ==, !=, >, >=, <, <=
template <class T, unsigned DIM>
inline bool operator == (const ADf<T,DIM> &a, const ADf<T,DIM> &b)
{
  return a._v==b._v;
}

template <class T, unsigned DIM>
inline bool operator == (const ADf<T,DIM> &a, const BaseType &b)
{
  return a._v==b;
}
template <class T, unsigned DIM>
inline bool operator == (const BaseType &a, const ADf<T,DIM> &b)
{
  return a==b._v;
}

template <class T, unsigned DIM>
inline bool operator != (const ADf<T,DIM> &a, const ADf<T,DIM> &b)
{
  return a._v!=b._v;
}

template <class T, unsigned DIM>
inline bool operator != (const ADf<T,DIM> &a, const BaseType &b)
{
  return a._v!=b;
}
template <class T, unsigned DIM>
inline bool operator != (const BaseType &a, const ADf<T,DIM> &b)
{
  return a!=b._v;
}


template <class T, unsigned DIM>
inline bool operator > (const ADf<T,DIM> &a, const ADf<T,DIM> &b)
{
  return a._v>b._v;
}

template <class T, unsigned DIM>
inline bool operator > (const ADf<T,DIM> &a, const BaseType &b)
{
  return a._v>b;
}
template <class T, unsigned DIM>
inline bool operator > (const BaseType &a, const ADf<T,DIM> &b)
{
  return a>b._v;
}

template <class T, unsigned DIM>
inline bool operator >= (const ADf<T,DIM> &a, const ADf<T,DIM> &b)
{
  return a._v>=b._v;
}

template <class T, unsigned DIM>
inline bool operator >= (const ADf<T,DIM> &a, const BaseType &b)
{
  return a._v>=b;
}
template <class T, unsigned DIM>
inline bool operator >= (const BaseType &a, const ADf<T,DIM> &b)
{
  return a>=b._v;
}


template <class T, unsigned DIM>
inline bool operator < (const ADf<T,DIM> &a, const ADf<T,DIM> &b)
{
  return a._v<b._v;
}

template <class T, unsigned DIM>
inline bool operator < (const ADf<T,DIM> &a, const BaseType &b)
{
  return a._v<b;
}

template <class T, unsigned DIM>
inline bool operator < (const BaseType &a, const ADf<T,DIM> &b)
{
  return a<b._v;
}

template <class T, unsigned DIM>
inline bool operator <= (const ADf<T,DIM> &a, const ADf<T,DIM> &b)
{
  return a._v<=b._v;
}

template <class T, unsigned DIM>
inline bool operator <= (const ADf<T,DIM> &a, const BaseType &b)
{
  return a._v<=b;
}
template <class T, unsigned DIM>
inline bool operator <= (const BaseType &a, const ADf<T,DIM> &b)
{
  return a<=b._v;
}









// typecast functions
/// typecast from ADf --> BaseType
template <class T, unsigned DIM>
inline ADf<T,DIM>::operator BaseType()
{
  if (this->hasZeroDerivative() != true) {
    std::cout << "error in typecast ADf -> Basetype" << std::endl;
    std::cout << *this << std::endl;
    exit(1);
  }
  //Alle Ableitungen 0 => Gut
  return BaseType(_v);
}




//} // namespace opti

//} // namespace olb
#endif
#endif
