/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef BACKUP_H
#define BACKUP_H

#include <string>

#include "core/units.h"
#include "io/xmlReader.h"


using namespace std;

namespace olb {

namespace opti {

// TODO: Is this class still in use?

template<typename S, typename T>
class Backup {

private:
  XMLreader*                                  _xml;
  LBconverter<S>*                             _converter;
  string                                      _saveName;
  int                                         _nStep;
  bool                                        _saveEnd;

public:
  Backup()
  {
    _xml = NULL;
    _converter = NULL;
  };
  Backup(LBconverter<S>* converter, XMLreader* xml)
  {
    _xml = NULL;
    _converter = NULL;
    reInit(converter, xml);
  };
  void reInit(LBconverter<S>* converter, XMLreader* xml)
  {
    _xml = xml;
    _converter = converter;
    reInit();
  };
  void reInit()
  {
    S saveTime = 0;
    (*_xml)["Output"]["BackupSolution"]["FileName"].read(_saveName);
    (*_xml)["Output"]["BackupSolution"]["SaveEnd"].read(_saveEnd);
    (*_xml)["Output"]["BackupSolution"]["SaveTime"].read(saveTime);
    _nStep = _converter->numTimeSteps(saveTime);
  };
  /*
    void checkAndSave(Serializerable<T>* serializer, int iT = 0) {
      if (iT==-1 && _saveEnd) {
        save(serializer);
      }
      else if (iT%_nStep==0 && iT!=0) {
        save(serializer, iT);
      }
    };

    void save(Serializable<T>* serializer, int iT = 0) {
      string saveName = _saveName;
      if (iT!=0) {
        saveName = olb::graphics::createFileName(_saveName, iT, 6);
      }
      cout << "Saving backup file ..." << endl;
      saveData(*serializer,saveName);
    };

    void load(Serializable<T>* serializer, int iT = 0) {
      string saveName = _saveName;
      if (iT!=0) {
        saveName = olb::graphics::createFileName(_saveName, iT, 6);
      }
      cout << "Loading backup file ..." << endl;
      loadData(*serializer,saveName);
    };*/
};

}

}

#endif