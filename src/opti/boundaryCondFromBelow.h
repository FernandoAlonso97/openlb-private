/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


//#ifndef _BOUNDARYCONDFROMBELOW_H_
//#define _BOUNDARYCONDFROMBELOW_H_

#include <vector>
#include <map>
#include <cmath>
#include <assert.h>

/// This class holds the necessary data to calculate a velocity
/// profile for the boundary conditions at the outlets of the lung at
/// a specified generation N. For details see the paper "Lattice
/// Boltzmann Simulation of the Human Lungs with boundary conditions
/// from below".
/// mailto: thomas.gengenbach@kit.edu

/*
class VelocityProfile
{
public:
  VelocityProfile() {
    std::vector<double> help(3,0); //x, y, z Koordinate normiert

    // Normalen
    help[0]= 0.0;
    help[1] = 0.0;
    help[2] = 1.0;
    _material2normal[3] = help;

    help[0]= 0.499889;
    help[1] = -0.000005;
    help[2] = -0.866089;
    normalize(help);
    _material2normal[202] = help;

    help[0]= -0.000144;
    help[1] =  -0.000184;
    help[2] = -1.000000;
    normalize(help);
    _material2normal[302] = help;

    help[0]= -1.000000;
    help[1] = -0.000732;
    help[2] = -0.000363;
    normalize(help);
    _material2normal[401] = help;

    help[0]= -0.500055;
    help[1] = -0.000048;
    help[2] = -0.865993;
    normalize(help);
    _material2normal[402] = help;


    // Mittelpunkte
    help[0] = -0.001235;
    help[1] = -0.001280;
    help[2] = 7.910070;
    _material2centerpoint[3] = help;
    help[0] = 3.057734;
    help[1] = -0.001325;
    help[2] = -3.358649;
    _material2centerpoint[202] = help;
    help[0] = -2.455880;
    help[1] = 0.000459;
    help[2] = -7.108922;
    _material2centerpoint[302] = help;
    help[0] = -9.599052;
    help[1] = -0.000165;
    help[2] = -4.258337;
    _material2centerpoint[401] = help;
    help[0] = -7.687082;
    help[1] = -0.000711;
    help[2] = -7.495898;
    _material2centerpoint[402] = help;

    // Radien
    _material2radius[3]   = 0.997116;
    _material2radius[202] = 0.777119;
    _material2radius[302] = 0.615373;
    _material2radius[401] = 0.480470;
    _material2radius[402] = 0.480334;
  };

  std::vector<double> getNormal(int material) {
    std::vector<double> temp;
    temp.push_back(_material2normal[material][0]);
    temp.push_back(_material2normal[material][1]);
    temp.push_back(_material2normal[material][2]);
    return temp;
  };

  std::vector<double> getCenterpoint(int material) {
    std::vector<double> temp;
    temp.push_back(_material2centerpoint[material][0]);
    temp.push_back(_material2centerpoint[material][1]);
    temp.push_back(_material2centerpoint[material][2]);
    return temp;
  };

  double getPoiseuilleFlow(int material, std::vector<double> physCoords) {
    double t = (physCoords[0]*_material2normal[material][0]
                +physCoords[1]*_material2normal[material][1]
                +physCoords[2]*_material2normal[material][2])
               - (_material2centerpoint[material][0]*_material2normal[material][0]
                  +_material2centerpoint[material][1]*_material2normal[material][1]
                  +_material2centerpoint[material][2]*_material2normal[material][2]);

    double ip1 = _material2centerpoint[material][0] + t*_material2normal[material][0],
           ip2 = _material2centerpoint[material][1] + t*_material2normal[material][1],
           ip3 = _material2centerpoint[material][2] + t*_material2normal[material][2];

    double r = std::pow(ip1-physCoords[0], 2.) + std::pow(ip2-physCoords[1], 2.) + std::pow(ip3-physCoords[2], 2.);
    if (std::sqrt(r) < _material2radius[material]) {
      return -(-1./std::pow(_material2radius[material], 2.) * r + 1.);
    }
    return 0;
  };

  double getRadius(int material) {
    return _material2radius[material];
  };

private:
  // Map from Material Number of the Outlet to the normal vector in the
  // format [x,y,z,normalizer], hence to get the normalized vector use
  // [x/normalizer,y/nomralizer,z/normalizer].
  // Material numbers are in the format [gen,num].
  std::map<int, std::vector<double> > _material2normal;

  std::map<int, std::vector<double> > _material2centerpoint;

  std::map<int, double > _material2radius;

  void normalize(std::vector<double> &vec) {
    double norm = std::sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
    vec[0] = vec[0]/norm;
    vec[1] = vec[1]/norm;
    vec[2] = vec[2]/norm;
  }
};*/


//#endif /* _BOUNDARYCONDFROMBELOW_H_ */
