/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012, 2013, 2016 Mathias J. Krause, Lukas Baron, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


// this file contains the controlled functions that are used to set up the
// fluid flow problem in dependence of a control variable, e.g. the boundary
// velocities, the  pressure and the porosity

#ifndef CONTROLLED_FUNCTIONS_3D_H
#define CONTROLLED_FUNCTIONS_3D_H

#include <cmath>
#include <vector>
#include <map>

#include "controller.h"
#include "dualFunctors3D.h"
#include "dualFunctors3D.hh"

#include "boundaryCondFromBelow.h"
#include "functors/functors3D.h"

using namespace olb;

/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {


/// controlled analytical functor
template <typename T, template<typename U> class DESCRIPTOR>
class ControlledAnalyticalF3D : public AnalyticalF3D<T,T> {
protected:
  Controller<T>* _controller;
  UnitConverter<T,DESCRIPTOR>* _converter;
  SuperLattice3D<T,DESCRIPTOR>* _lattice;
  SuperGeometry3D<T>* _geometry;


public:
  ControlledAnalyticalF3D(SuperLattice3D<T,DESCRIPTOR>* lattice, SuperGeometry3D<T>* geometry,
                          Controller<T>* controller, UnitConverter<T,DESCRIPTOR>* converter, int targetDim)
    : AnalyticalF3D<T,T>(targetDim), _controller(controller), _converter(converter) ,
      _lattice(lattice), _geometry(geometry)
  { }

  virtual std::string name() = 0;

};

/*
/// controlled force function, (return the value that is found in variable control[i] times analytical solution) (deprecated)
template <typename T, template<typename U> class DESCRIPTOR>
class ControlledAnalyticalF3D : public AnalyticalF3D<T,T> {
protected:
  Controller<T>* _controller;
  LBconverter<T>* _converter;
  SuperLattice3D<T,DESCRIPTOR>* _lattice;
  SuperGeometry3D<T>* _geometry;


public:
  ControlledAnalyticalF3D(SuperLattice3D<T,DESCRIPTOR>* lattice, SuperGeometry3D<T>* geometry, Controller<T>* controller, LBconverter<T>* converter, int targetDim)
    : AnalyticalF3D<T,T>(targetDim), _lattice(lattice), _geometry(geometry), _controller(controller), _converter(converter) { }

  virtual std::string name() = 0;

};
*/


/// controlled analytical Velocity function
template <typename T, template<typename U> class DESCRIPTOR>
class ControlledAnalyticalVelocity3D : public ControlledAnalyticalF3D<T,DESCRIPTOR> {
private:

public:
  ControlledAnalyticalVelocity3D(SuperLattice3D<T,DESCRIPTOR>* lattice, SuperGeometry3D<T>* geometry, Controller<T>* controller, UnitConverter<T,DESCRIPTOR>* converter)
    : ControlledAnalyticalF3D<T,DESCRIPTOR>(lattice, geometry, controller, converter,3) { }

  bool operator()(T output[], const T input[]);
  std::string name()
  {
    return "startVelocity";
  }

};

/// controlled analytical pressure function
template <typename T, template<typename U> class DESCRIPTOR>
class ControlledAnalyticalPressure3D : public ControlledAnalyticalF3D<T,DESCRIPTOR> {
private:

public:
  ControlledAnalyticalPressure3D(SuperLattice3D<T,DESCRIPTOR>* lattice, SuperGeometry3D<T>* geometry, Controller<T>* controller, UnitConverter<T,DESCRIPTOR>* converter)
    : ControlledAnalyticalF3D<T,DESCRIPTOR>(lattice,geometry, controller, converter,1) { }

  bool operator()(T output[], const T input[]);
  std::string name()
  {
    return "startPressure";
  }

};


/// controlled analytical ExternalField
template <typename T, template<typename U> class DESCRIPTOR>
class ControlledAnalyticalExternalField3D : public ControlledAnalyticalF3D<T,DESCRIPTOR> {
private:

public:
  ControlledAnalyticalExternalField3D(SuperLattice3D<T,DESCRIPTOR>* lattice, SuperGeometry3D<T>* geometry, Controller<T>* controller, UnitConverter<T,DESCRIPTOR>* converter, int targetDim = 3)
    : ControlledAnalyticalF3D<T,DESCRIPTOR>(lattice,geometry, controller, converter, targetDim) { }

  bool operator()(T output[], const T input[]);
  std::string name()
  {
    return "startExternalField";
  }
};

/// controlled super lattice functor
template <typename T, template<typename U> class DESCRIPTOR>
class ControlledSuperLatticeF3D : public SuperLatticeF3D<T,DESCRIPTOR> {
protected:
  Controller<T>& _controller;
  UnitConverter<T,DESCRIPTOR>& _converter;
  SuperGeometry3D<T>& _geometry;

public:
  ControlledSuperLatticeF3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry,
                            Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : SuperLatticeF3D<T,DESCRIPTOR>(lattice,targetDim),
      _controller(controller), _converter(converter), _geometry(geometry)
  { };
};

template <typename T, template<typename U> class DESCRIPTOR>
class Objective3D : public ControlledSuperLatticeF3D<T,DESCRIPTOR> {
protected:

public:
  Objective3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry,
              Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter)
    : ControlledSuperLatticeF3D<T,DESCRIPTOR>(lattice, geometry, controller, converter, 1)
  {
    this->getName() = "objective";
  };
  bool operator()(T output[], const int input[]);
};

template <typename T, template<typename U> class DESCRIPTOR>
class DObjectiveDf3D : public ControlledSuperLatticeF3D<T,DESCRIPTOR> {
protected:
  T _globalValue;
  SuperF3D<T,T>* _dObjectiveDf;
public:
  DObjectiveDf3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, Controller<T>& controller,
                 UnitConverter<T,DESCRIPTOR>& converter);
  bool operator()(T output[], const int input[]);
};

template <typename T, template<typename U> class DESCRIPTOR>
class DObjectiveDControl3D : public ControlledSuperLatticeF3D<T,DESCRIPTOR> {
protected:

public:
  DObjectiveDControl3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : ControlledSuperLatticeF3D<T,DESCRIPTOR>(lattice, geometry, controller, converter, targetDim)
  {
    this->getName() = "DObjectiveDControl";
  };
  bool operator()(T output[], const int input[]);
};


// ---------------------------
// Define Projections for the control variables
// ---------------------------

/// Projection Base Class
/**
 * A Projection maps an alpha \in (-\infty, \infty) on a control \in [0,1].
 */
template <typename T, template<typename U> class DESCRIPTOR>
class Projection3D : public ControlledSuperLatticeF3D<T,DESCRIPTOR> {

protected:
  int _material;
  int _nVoxel;
  std::vector<int> _nVoxelCuboid;
  int _nCuboids;
  int _nDim;
  T _scale;

public:
  Projection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
               Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim, T scale=T(1))
    : ControlledSuperLatticeF3D<T,DESCRIPTOR>(lattice, geometry, controller, converter, targetDim),
      _material(material), _scale(scale)
  {
    this->getName() = "alpha";
    _nVoxel = geometry.getStatistics().getNvoxel(material);
    _nCuboids = geometry.getCuboidGeometry().getNc();
    _nVoxelCuboid.resize(_nCuboids);
    _nVoxelCuboid[0] = int();
    for (int iC=1; iC<_nCuboids; iC++) {
      _nVoxelCuboid[iC] = _nVoxelCuboid[iC-1] + geometry.getCuboidGeometry().get(iC).getLatticeVolume();
    }
    _nDim = targetDim;
  };

  /// Copy Constructor
  Projection3D(Projection3D<T,DESCRIPTOR>& rhs)
    : Projection3D (rhs._sLattice, rhs._geometry, rhs._material, rhs._controller, rhs._converter, rhs._nDim, rhs._scale)
  { };

  int getIndex(const int input[], int iDim)
  {
    int nY = this->_geometry.getCuboidGeometry().get(input[0]).getNy();
    int nZ = this->_geometry.getCuboidGeometry().get(input[0]).getNz();
    return _nVoxelCuboid[input[0]]*_nDim + (input[1]*nY*nZ + input[2]*nZ + input[3])*_nDim + iDim;
  }

  T& getControl(const int input[], int iDim)
  {
    return this->_controller.getControl(getIndex(input, iDim) );
  };

  T gridTerm()
  {
//    std::cout << "controlledFunctions nDim: " << this->_nDim << std::endl; exit(-1);
    return pow( this->_converter.getPhysDeltaX(), 2 /*WARNING: Dim-1*/ ) * this->_converter.getLatticeViscosity() * this->_converter.getLatticeRelaxationTime();
  };

  bool operator()(T output[], const int input[])
  {
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      output[iDim] = getControl(input, iDim) * _scale;
    }
    return true;
  };

};


/// Lukas Baron Projection
/**
 * Projection Mapping: d(a) = (0.5 + 0.5 * sin ( ( a - 0.5 ) * PI ) ** 2
 */
template <typename T, template<typename U> class DESCRIPTOR>
class BaronProjection3D : public Projection3D<T,DESCRIPTOR> {
public:
  BaronProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                    Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };

  bool operator()(T output[], const int input[])
  {
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      output[iDim] = pow( .5 + .5 * sin(
                            ( this->_controller.getControl( this->getIndex(input, iDim) ) - .5 ) * 3.14 ), 2.);
    }
    return true;
  };
};


/// Derivative of BaronProjection3D with respect to alpha
template <typename T, template<typename U> class DESCRIPTOR>
class DBaronProjectionDAlpha3D : public Projection3D<T,DESCRIPTOR> {

public:
  DBaronProjectionDAlpha3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                           Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };
  bool operator()(T output[], const int input[])
  {
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      output[iDim] = ( .5 + .5 * sin(
                         ( this->_controller.getControl( this->getIndex(input, iDim) ) - .5) * 3.14 ) )
                     * cos( ( this->_controller.getControl( this->getIndex(input, iDim) ) - .5 ) * 3.14 )
                     * 3.14;
    }
    return true;
  };
};


/// Mathias Krause Projection
/**
 * Projection Mapping: d(a) = exp(a**2)
 */
template <typename T, template<typename U> class DESCRIPTOR>
class KrauseProjection3D : public Projection3D<T,DESCRIPTOR> {
public:
  KrauseProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                     Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };

  bool operator()(T output[], const int input[])
  {
    T ctrl;
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
      output[iDim] = 1 - ( 1. / pow( 2.7182, pow( ctrl, 2. ) ) );
    }
    return true;
  };
};


/// Derivative of KrauseProjection3D with respect to alpha
template <typename T, template<typename U> class DESCRIPTOR>
class DKrauseProjectionDAlpha3D : public Projection3D<T,DESCRIPTOR> {

public:
  DKrauseProjectionDAlpha3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                            Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };

  bool operator()(T output[], const int input[])
  {

    T ctrl;
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
      output[iDim] = ( 2 * ctrl / pow( 2.7182, pow( ctrl, 2. ) ) );
    }
    return true;
  };
};


/********* DEPRECATED (see new GIProjections) ************/
//template <typename T, template<typename U> class DESCRIPTOR>
//class FoersterProjection3D : public Projection3D<T,DESCRIPTOR> {
//public:
//  FoersterProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
//                 Controller<T>& controller, LBconverter<T>& converter, int targetDim)
//      : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
//  { };
//
//  bool operator()(T output[], const int input[]) {
//
//    T ctrl; // alpha
//    //T K; // permeability
//    for (int iDim = 0; iDim < this->_nDim; iDim++) {
//      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
//      //K = pow( 2.71828182, ctrl) + tau * nuLattice * pow( physLength, T( dim-1 ) ) ;
//      output[iDim] = exp(ctrl) / ( exp(ctrl) + this->gridTerm() );
//    }
//    return true;
//  };
//};
//
//template <typename T, template<typename U> class DESCRIPTOR>
//class DFoersterProjectionDControl3D : public Projection3D<T,DESCRIPTOR> {
//
//public:
//  DFoersterProjectionDControl3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
//                          Controller<T>& controller, LBconverter<T>& converter, int targetDim)
//      : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
//  { };
//
//  bool operator()(T output[], const int input[]) {
//
//    T ctrl;
//
//    for (int iDim = 0; iDim < this->_nDim; iDim++) {
//      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
//      output[iDim] = exp(ctrl) * this->gridTerm() / pow( exp(ctrl) + this->gridTerm(), 2. );
//    }
//    return true;
//  };
//};
//
//
//
//template <typename T, template<typename U> class DESCRIPTOR>
//class Foerster2Projection3D : public Projection3D<T,DESCRIPTOR> {
//public:
//  Foerster2Projection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
//                       Controller<T>& controller, LBconverter<T>& converter, int targetDim)
//      : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
//  { };
//
//  bool operator()(T output[], const int input[]) {
//
//    T ctrl; // alpha
//    //T K; // permeability
//    for (int iDim = 0; iDim < this->_nDim; iDim++) {
//      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
//      //K = pow( 2.71828182, ctrl) + tau * nuLattice * pow( physLength, T( dim-1 ) ) ;
//      output[iDim] = (exp(pow(ctrl, 2.0)) - 1.0) / ( exp(pow(ctrl, 2.0)) - 1.0 + this->gridTerm() );
//    }
//    return true;
//  };
//};
//
//template <typename T, template<typename U> class DESCRIPTOR>
//class DFoerster2ProjectionDControl3D : public Projection3D<T,DESCRIPTOR> {
//
//public:
//  DFoerster2ProjectionDControl3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
//                                Controller<T>& controller, LBconverter<T>& converter, int targetDim)
//      : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
//  { };
//
//  bool operator()(T output[], const int input[]) {
//
//    T ctrl;
//
//    for (int iDim = 0; iDim < this->_nDim; iDim++) {
//      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
//      output[iDim] = 2* ctrl * exp(pow(ctrl, 2.0)) * this->gridTerm() / pow( exp(pow(ctrl, 2.0)) - 1.0 +
//                                                                                                this->gridTerm(), 2. );
//    }
//    return true;
//  };
//};



/// Simon Stasius Projection (DEPRECATED! See new GIProjections)
template <typename T, template<typename U> class DESCRIPTOR>
class StasiusProjection3D : public Projection3D<T,DESCRIPTOR> {
public:
  StasiusProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                      Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };

  bool operator()(T output[], const int input[])
  {
    T nuLattice = this->_converter.getLatticeNu();
    T tau =  this->_converter.getTau();
    T physLength = this->_converter.physLength();
    int dim = this->_nDim; //this->_converter.getDim();

    T ctrl;
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
      output[iDim] = pow( ctrl, 2. ) / ( pow( ctrl, 2. ) +  tau * nuLattice * pow( physLength, T( dim-1 ) ) );
    }
    return true;
  };
};


/// Simon Stasius Projection (DEPRECATED! See new GIProjections)
template <typename T, template<typename U> class DESCRIPTOR>
class DStasiusProjectionDAlpha3D : public Projection3D<T,DESCRIPTOR> {

public:
  DStasiusProjectionDAlpha3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                             Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };

  bool operator()(T output[], const int input[])
  {
    T nuLattice = this->_converter.getLatticeNu();
    T tau =  this->_converter.getTau();
    T physLength = this->_converter.physLength();
    int dim = 3; //this->_converter.getDim();

    T const_term = tau * nuLattice * pow( physLength, T( dim-1 ) );

    T ctrl;
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
      output[iDim] = 2. * ctrl * const_term / pow( ( pow( ctrl, 2. ) + const_term ), 2.);
    }
    return true;
  };
};



// ------------------------------------------ //
//       grid-independent projections         //
// ------------------------------------------ //


/// Grid-independent Projection Base Class
/**
 * Ba = 1 - GridTerm() / K(a)
 * with K(a) = subprojection(a) + GridTerm()
 */
template <typename T, template<typename U> class DESCRIPTOR>
class GiProjection3D : public Projection3D<T,DESCRIPTOR> {
public:
  GiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                 Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : Projection3D<T,DESCRIPTOR>(lattice, geometry, material, controller, converter, targetDim, 1)
  { };

  virtual T subprojection(T ctrl) = 0;
  virtual T dSubprojection(T ctrl) = 0;

  virtual bool operator()(T output[], const int input[])
  {
    T ctrl; // alpha
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
      //K = pow( 2.71828182, ctrl) + tau * nuLattice * pow( physLength, T( dim-1 ) ) ;
      output[iDim] = subprojection(ctrl) / ( subprojection(ctrl) + this->gridTerm() );
    }
    return true;
  };
};


/// Derivative of GiProjection3D with respect to alpha
template <typename T, template<typename U> class DESCRIPTOR>
class DGiProjectionDAlpha3D : public Projection3D<T,DESCRIPTOR> {
protected:
  GiProjection3D<T,DESCRIPTOR>& _giProjection;
public:
  DGiProjectionDAlpha3D(GiProjection3D<T,DESCRIPTOR>& proj)
    : Projection3D<T,DESCRIPTOR>(proj),
      _giProjection(proj)
  { };

  virtual bool operator()(T output[], const int input[])
  {
    T ctrl;
    for (int iDim = 0; iDim < this->_nDim; iDim++) {
      ctrl = this->_controller.getControl( this->getIndex(input, iDim) );
      output[iDim] = _giProjection.dSubprojection(ctrl) * this->gridTerm() /
                     pow( _giProjection.subprojection(ctrl) + this->gridTerm(), 2. );
    }
    return true;
  };
};



template <typename T, template<typename U> class DESCRIPTOR>
class FoersterGiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  FoersterGiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                         Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return exp(ctrl);
  }

  T dSubprojection(T ctrl)
  {
    return exp(ctrl);
  }
};


/// FoersterProjection for arbitrary n
/**
 * subproj(a) = exp(a^(2n)) - 1
 */
template <typename T, template<typename U> class DESCRIPTOR>
class FoersterNGiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
protected:
  int _n; /// n in exp(a^(2n)) - 1
public:
  FoersterNGiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                          Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int n, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim),
      _n(n)
  {
    if (n < 1) {
      std::cout << "Error! n must be at least 1. Exiting..." << std::endl;
      exit(-1);
    }
  };
  T subprojection(T ctrl)
  {
    return exp( pow(ctrl, 2.0 * _n) ) - 1.0;
  }

  T dSubprojection(T ctrl)
  {
    return 2.0*_n * pow(ctrl, (2.0*_n - 1.0)) * exp( pow(ctrl, 2.0 * _n) );
  }
};


/// StasiusProjection for arbitrary n
/**
 * subproj(a) = a^(2n)
 */
template <typename T, template<typename U> class DESCRIPTOR>
class StasiusNGiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
protected:
  int _n; /// n in exp(a^(2n))
public:
  StasiusNGiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                         Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int n, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim),
      _n(n)
  {
    if (n < 1) {
      std::cout << "Error! n must be at least 1. Exiting..." << std::endl;
      exit(-1);
    }
  };
  T subprojection(T ctrl)
  {
    return pow(ctrl, 2.0 * _n);
  }

  T dSubprojection(T ctrl)
  {
    return 2.0*_n * pow(ctrl, (2.0*_n - 1.0));
  }
};


template <typename T, template<typename U> class DESCRIPTOR>
class Foerster2GiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  Foerster2GiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                          Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return exp( pow(ctrl, 2.0) ) - 1.0;
  }

  T dSubprojection(T ctrl)
  {
    return 2.0 * ctrl * exp( pow(ctrl, 2.0) );
  }
};


template <typename T, template<typename U> class DESCRIPTOR>
class Foerster3GiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  Foerster3GiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                          Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return exp( pow(ctrl, 4.0) ) - 1.0;
  }

  T dSubprojection(T ctrl)
  {
    return 4.0 * pow(ctrl, 3.0) * exp( pow(ctrl, 4.0) );
  }
};


template <typename T, template<typename U> class DESCRIPTOR>
class Foerster4GiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  Foerster4GiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                          Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return exp( pow(ctrl, 10.0) ) - 1.0;
  }

  T dSubprojection(T ctrl)
  {
    return 10.0 * pow(ctrl, 9.0) * exp( pow(ctrl, 10.0) );
  }
};


template <typename T, template<typename U> class DESCRIPTOR>
class StasiusGiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  StasiusGiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                        Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return pow(ctrl, 2.0);
  }

  T dSubprojection(T ctrl)
  {
    return 2.0 * ctrl;
  }
};



template <typename T, template<typename U> class DESCRIPTOR>
class Stasius2GiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  Stasius2GiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                         Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return pow(ctrl, 4.0);
  }

  T dSubprojection(T ctrl)
  {
    return 4.0 * pow(ctrl, 3.0);
  }
};


template <typename T, template<typename U> class DESCRIPTOR>
class Stasius3GiProjection3D : public GiProjection3D<T,DESCRIPTOR> {
public:
  Stasius3GiProjection3D(SuperLattice3D<T,DESCRIPTOR>& lattice, SuperGeometry3D<T>& geometry, int material,
                         Controller<T>& controller, UnitConverter<T,DESCRIPTOR>& converter, int targetDim)
    : GiProjection3D<T,DESCRIPTOR> (lattice, geometry,  material, controller, converter, targetDim)
  { };
  T subprojection(T ctrl)
  {
    return pow(ctrl, 10.0);
  }

  T dSubprojection(T ctrl)
  {
    return 10.0 * pow(ctrl, 9.0);
  }
};




/*
////////////////////////////////////////////////////////////////////////////////
/// functor to compute the objective functional
template<typename T, template <typename U> class DESCRIPTOR>
class ComputeVolumeFlowPenalty : public LatticeF3D<T,DESCRIPTOR> {
private:
  SuperLattice3D<T, DESCRIPTOR>* sLattice;
  SuperGeometry3D<T>* bg;
  Dynamics<T, DESCRIPTOR>* bulkDynamics;
  LBconverter<T>* converter;
  T u0;
  T alpha;

public:
  // Constructor
  ComputeVolumeFlowPenalty(SuperLattice3D<T, DESCRIPTOR>* _sLattice,
                      SuperGeometry3D<T>* _bg,
                      Dynamics<T, DESCRIPTOR>* _bulkDynamics,
                      LBconverter<T>* _converter,
                      T _u0,
                      T _alpha
                      )
    : LatticeF3D<T,DESCRIPTOR> (NULL,NULL),
      sLattice(_sLattice),
      bg(_bg),
      bulkDynamics(_bulkDynamics),
      converter(_converter),
      u0(_u0), alpha(_alpha)
  { }

  std::vector<T> operator()(std::vector<T> input);

  // obligatory access operator


};*/

} // namespace opti

} // namespace olb
#endif // CONTROLLED_FUNCTIONS_H

