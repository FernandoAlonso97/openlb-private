/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of optimization algorthims -- header file.
 */
/** \file
 * A collection of dynamics classes for dual LB methods
 * (e.g. dual BGK) with which a CellView object can be
 * instantiated -- header file.
 */

#ifndef DUAL_DYNAMICS_H
#define DUAL_DYNAMICS_H

//#include "optiStructure.h"
//#include "solver3D.h"
#include "dualLbHelpers.h"
#include "utilities/vectorHelpers.h"

/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {


template<typename T> class Controller;
template<typename T, template<typename U> class Lattice> class DualController;




/// Implementation of the dual BGK collision step with external force
template<typename T, template<typename U> class Lattice, class Momenta>
class DualForcedBGKdynamics : public BasicDynamics<T,Lattice,Momenta, DualForcedBGKdynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  DualForcedBGKdynamics(T omega_, Momenta& momenta_, Controller<T>* controller);
  /// Clone the object on its dynamic type.
  virtual DualForcedBGKdynamics<T,Lattice,Momenta>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
  /// Does nothing
  virtual void defineRho(CellView<T,Lattice>& cell, T rho);
private:
  T omega;  ///< relaxation parameter
  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;

  Controller<T>* _controller;
};

////////////////////// Class ForcedBGKdynamics /////////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
DualForcedBGKdynamics<T,Lattice,Momenta>::DualForcedBGKdynamics (
  T omega_, Momenta& momenta_, Controller<T>* controller)
  : BasicDynamics<T,Lattice,Momenta, DualForcedBGKdynamics<T, Lattice, Momenta>>(momenta_),
    omega(omega_)
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size
  OLB_PRECONDITION( Lattice<T>::d == Lattice<T>::ExternalField::sizeOfForce );
  _controller = controller;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void DualForcedBGKdynamics<T,Lattice,Momenta>::defineRho(CellView<T,Lattice>& cell, T rho)
{

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop]=rho;
  }
}

template<typename T, template<typename U> class Lattice, class Momenta>
DualForcedBGKdynamics<T,Lattice,Momenta>* DualForcedBGKdynamics<T,Lattice,Momenta>::clone() const
{
  return new DualForcedBGKdynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T DualForcedBGKdynamics<T,Lattice,Momenta>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void DualForcedBGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{

#ifndef ADT

  cell.revert();

  // Preparation
  T* dJdF = cell.getExternal(Lattice<T>::ExternalField::dJdFBeginsAt);
  T* f = cell.getExternal(Lattice<T>::ExternalField::fBeginsAt);
  T* force = cell.getExternal(forceBeginsAt);
  T rho_f;
  T u_f[3];

  dualLbDynamicsHelpers<T,typename Lattice<T>::BaseDescriptor>::computeRhoU(f, rho_f, u_f);
  T uSqr_f = util::normSqr<T,Lattice<T>::d>(u_f);
  T eq_phi[Lattice<T>::q];
  T force_phi[Lattice<T>::q];

  // Force
  T F1_phi[Lattice<T>::q][Lattice<T>::q];
  T F2_phi[Lattice<T>::q][Lattice<T>::q];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    T f_c = T();
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      f_c += force[iD]*Lattice<T>::c(iPop)[iD];
    }
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      T sum = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        sum += (f_c*Lattice<T>::c(iPop)[iD]-force[iD]/(T)Lattice<T>::invCs2)*Lattice<T>::c[jPop][iD];
      }
      F1_phi[iPop][jPop] = Lattice<T>::t[iPop]*Lattice<T>::invCs2*f_c;
      F2_phi[iPop][jPop] = Lattice<T>::t[iPop]*Lattice<T>::invCs2*Lattice<T>::invCs2*sum;
    }
  }

  // Collision preperation
  for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
    eq_phi[jPop] = T();
    force_phi[jPop] = T();
    for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
      eq_phi[jPop] += cell[iPop]*dualLbHelpers<T,Lattice>::equilibrium(iPop, jPop, rho_f, u_f, uSqr_f);
      force_phi[jPop] += cell[iPop]*(F1_phi[iPop][jPop] + (1.-.5*omega)*F2_phi[iPop][jPop]);
    }
  }

  // Collision
  for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
    cell[jPop] = cell[jPop] - omega*(cell[jPop]-eq_phi[jPop]) + force_phi[jPop] + dJdF[jPop];
  }

  // Incrementing statistic values for convergence
  T phi2 = T();
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
      phi2 += cell[iPop]*cell[iPop];
  }
  statistics.incrementStats(1.0 + cell[0], phi2);
  cell.revert();

#endif
}

template<typename T, template<typename U> class Lattice, class Momenta>
T DualForcedBGKdynamics<T,Lattice,Momenta>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void DualForcedBGKdynamics<T,Lattice,Momenta>::setOmega(T omega_)
{
  omega = omega_;
}














///////////// Porous //////////////////////////


/// Implementation of the dual BGK collision step with external force
template<typename T, template<typename U> class Lattice, class Momenta>
class DualPorousBGKdynamics : public BasicDynamics<T,Lattice,Momenta, DualPorousBGKdynamics<T, Lattice, Momenta>> {
public:
  /// Constructor
  DualPorousBGKdynamics(T omega_, Momenta& momenta_, Controller<T>* controller);
  /// Clone the object on its dynamic type.
  virtual DualPorousBGKdynamics<T,Lattice,Momenta>* clone() const;
  /// Compute equilibrium distribution function
  virtual T computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const;
  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);
  /// Get local relaxation parameter of the dynamics
  virtual T getOmega() const;
  /// Set local relaxation parameter of the dynamics
  virtual void setOmega(T omega_);
  /// Does nothing
  virtual void defineRho(CellView<T,Lattice>& cell, T rho);
private:
  T omega;  ///< relaxation parameter
  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;

  Controller<T>* _controller;
};

////////////////////// Class DualPorousBGKdynamics /////////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice, class Momenta>
DualPorousBGKdynamics<T,Lattice,Momenta>::DualPorousBGKdynamics (
  T omega_, Momenta& momenta_, Controller<T>* controller)
  : BasicDynamics<T,Lattice,Momenta, DualPorousBGKdynamics<T, Lattice, Momenta>>(momenta_),
    omega(omega_)
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size
  OLB_PRECONDITION( Lattice<T>::d == Lattice<T>::ExternalField::sizeOfForce );
  _controller = controller;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void DualPorousBGKdynamics<T,Lattice,Momenta>::defineRho(CellView<T,Lattice>& cell, T rho)
{

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop]=rho;
  }
}

template<typename T, template<typename U> class Lattice, class Momenta>
DualPorousBGKdynamics<T,Lattice,Momenta>* DualPorousBGKdynamics<T,Lattice,Momenta>::clone() const
{
  return new DualPorousBGKdynamics<T,Lattice,Momenta>(*this);
}

template<typename T, template<typename U> class Lattice, class Momenta>
T DualPorousBGKdynamics<T,Lattice,Momenta>::computeEquilibrium(int iPop, T rho, const T u[Lattice<T>::d], T uSqr) const
{
  return lbHelpers<T,Lattice>::equilibrium(iPop, rho, u, uSqr);
}

template<typename T, template<typename U> class Lattice, class Momenta>
void DualPorousBGKdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{

  cell.revert();


  T rho_phi_x = T(); // UNÖTIG? FK
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    rho_phi_x += cell[iPop];
  }

#ifndef ADT

  //#ifdef ADT

  // Who am I?
  //T* ct = cell.getExternal(3);
  //int c[3];
  //c[0] = (int)ct[0];
  //c[1] = (int)ct[1];
  //c[2] = (int)ct[2];



  // Preparation
  T* pop_f = cell.getExternal(Lattice<T>::ExternalField::fBeginsAt);
  T* dJdF = cell.getExternal(Lattice<T>::ExternalField::dJdFBeginsAt);
  T d = *(cell.getExternal(Lattice<T>::ExternalField::porosityIsAt));
  //d=1.;

  T rho_f;
  T u_f[3];
  rho_f = T();
  for (int iD=0; iD < Lattice<T>::d; ++iD) {
    u_f[iD] = T();
  }
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    rho_f += pop_f[iPop];
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      u_f[iD] += pop_f[iPop]*Lattice<T>::c(iPop)[iD];
    }
  }
  //rho_f += (T)1;
  for (int iD=0; iD < Lattice<T>::d; ++iD) {
    u_f[iD] /= rho_f;
    u_f[iD] *= d; /*WARNING*/
  }



  //const T* u_wanted;// = _controller->getWantedVelocity().get(c[0],c[1],c[2]);

  //    std::cout <<"-------2a4-----------"<< std::endl;


  T Meq_f[19]; //19 = Lattice<T>::q ÄNDERN FK
  T Meq_phi = T();

  T c_u = T();
  T uSqr = T();
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    c_u = T();
    uSqr = T();
    Meq_f[iPop] = T();
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      c_u += Lattice<T>::c(iPop)[iD]*u_f[iD];
      uSqr += u_f[iD]*u_f[iD];
    }
    Meq_f[iPop] = cell[iPop]*rho_f * Lattice<T>::t[iPop] * (
                    (T)1 + Lattice<T>::invCs2 * c_u +
                    Lattice<T>::invCs2 * Lattice<T>::invCs2/(T)2 * c_u*c_u -
                    Lattice<T>::invCs2/(T)2 * uSqr);
  }

  //           std::cout <<"-------4a-----------"<< std::endl;

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    //dJdF = T();
    Meq_phi = T();
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      //dJdF += (u_wanted[iD]-u_f[iD])*(Lattice<T>::c(iPop)[iD]-u_f[iD]);
    }
    for (int iPop2=0; iPop2 < Lattice<T>::q; ++iPop2) {
      T sum = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        sum += (u_f[iD] - d*/*WARNING*/Lattice<T>::c(iPop)[iD])*(u_f[iD] - Lattice<T>::c[iPop2][iD]);
      }
      Meq_phi += Meq_f[iPop2]*(sum+1./Lattice<T>::invCs2);
    }
    Meq_phi = Meq_phi/(rho_f/Lattice<T>::invCs2);
    //dJdF[iPop] /= rho_f;

    //if (c[0]==4&&c[1]==4&&c[2]==4) std::cout <<cell[iPop]<<"#"<<cell[iPop]-omega*(cell[iPop]-Meq_phi) + dJdF[iPop]<<"#"<<Meq_phi<<"#"<<dJdF[iPop]<<std::endl;

    cell[iPop] = cell[iPop] - omega*(cell[iPop]-Meq_phi) + dJdF[iPop];
  }

  //  if (c[0]==4&&c[1]==4&&c[2]==4) std::cout <<cell[2]<<std::endl;
  // if (c[0]==4&&c[1]==4&&c[2]==4) std::cout <<"#########################"<<std::endl;
  // if (c[0]==4&&c[1]==4&&c[2]==4) std::cout <<"#########################"<<std::endl;

  T rho_phi, u_rho_phi[Lattice<T>::d];
  rho_phi = T();
  for (int iD=0; iD < Lattice<T>::d; ++iD) {
    u_rho_phi[iD] = T();
  }
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    rho_phi += cell[iPop];
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      u_rho_phi[iD] += cell[iPop]*Lattice<T>::c(iPop)[iD]*Lattice<T>::t[iPop];
    }
  }
  T uSqr_phi = T();
  for (int iD=0; iD < Lattice<T>::d; ++iD) {
    uSqr_phi += u_rho_phi[iD]*u_rho_phi[iD];
  }
  uSqr_phi/=(rho_phi*rho_phi);


  //original Name: statistics.gatherStats(1.0 + cell[0], uSqr_phi);
  //sollte es nicht besser: incrementStats() heißen? gatherStats gibt's doch gar nicht
  statistics.incrementStats(1.0 + cell[0], uSqr_phi);
  cell.revert();

#endif
}

template<typename T, template<typename U> class Lattice, class Momenta>
T DualPorousBGKdynamics<T,Lattice,Momenta>::getOmega() const
{
  return omega;
}

template<typename T, template<typename U> class Lattice, class Momenta>
void DualPorousBGKdynamics<T,Lattice,Momenta>::setOmega(T omega_)
{
  omega = omega_;
}




} // namespace opti

} // namespace olb

#endif
