/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef DUAL_FUNCTORS_3D_H
#define DUAL_FUNCTORS_3D_H

#include <cmath>
#include <vector>
#include <map>

#include "functors/functors3D.h"

using namespace olb;


/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {

/// functor to get the pointwise dual dissipation density on local lattices, if globIC is not on
/// the local processor, the returned vector is empty
template <typename T, template <typename U> class Lattice>
class BlockLatticeDphysDissipationDf3D : public BlockLatticeF3D<T,Lattice> {
protected:
  const UnitConverter<T,Lattice>& _converter;
public:
  BlockLatticeDphysDissipationDf3D(BlockLatticeStructure3D<T,Lattice>& blockLattice,
                                   const UnitConverter<T,Lattice>& converter);
  bool operator()(T output[], const int input[]);
};

/// functor to get pointwise dual dissipation density on local lattices, if globIC is not on
/// the local processor, the returned vector is empty
template <typename T, template <typename U> class Lattice>
class SuperLatticeDphysDissipationDf3D : public SuperLatticePhysF3D<T,Lattice> {

public:
  SuperLatticeDphysDissipationDf3D(SuperLattice3D<T,Lattice>& sLattice,
                                   const UnitConverter<T,Lattice>& converter);
  bool operator()(T output[], const int input[]);
};

/// functor to get pointwise dual velocity density on local lattices, if globIC is not on
/// the local processor, the returned vector is empty

template <typename T, template <typename U> class Lattice>
class BlockLatticeDphysVelocityDf3D : public BlockLatticeF3D<T,Lattice> {
 private:
   int _nDim;
   int _extractDim;
protected:
  const UnitConverter<T,Lattice>& _converter;
public:
  BlockLatticeDphysVelocityDf3D(BlockLatticeStructure3D<T,Lattice>& blockLattice,
                                const UnitConverter<T,Lattice>& converter, int nDim, int extractDim);
  bool operator()(T output[], const int input[]);
};



/// functor to get pointwise dual velocity density on local lattices, if globIC is not on
/// the local processor, the returned vector is empty
template <typename T, template <typename U> class Lattice>
class SuperLatticeDphysVelocityDf3D : public SuperLatticePhysF3D<T,Lattice> {
private:
  int _nDim;
  int _extractDim;
public:
  SuperLatticeDphysVelocityDf3D(SuperLattice3D<T,Lattice>& sLattice,
                                const UnitConverter<T,Lattice>& converter);
  SuperLatticeDphysVelocityDf3D(SuperLattice3D<T,Lattice>& sLattice,
                                const UnitConverter<T,Lattice>& converter,
                                SuperExtractComponentF3D<T,T>& sExtract);
  bool operator()(T output[], const int input[]);
};

/// functor to compute 1/2(f-f_wanted)^2 on a lattice
template <typename T, template <typename U> class Lattice>
class DifferenceObjective3D : public SuperLatticeF3D<T,Lattice> {
private:
  SuperLatticePhysF3D<T,Lattice>& _f;
  AnalyticalF3D<T,T>& _wantedF;
  SuperGeometry3D<T>& _geometry;
  int _material;
public:
  DifferenceObjective3D(SuperLatticePhysF3D<T,Lattice>& f, AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry, int material);
  bool operator()(T output[], const int input[]);
};

/// functor to compute 0.5(f-f_wanted)^2 on a lattice
template <typename T, template <typename U> class Lattice>
class DdifferenceObjectiveDf3D : public SuperLatticeF3D<T,Lattice> {
private:
  SuperLatticePhysF3D<T,Lattice>& _f;
  SuperLatticePhysF3D<T,Lattice>& _dFdF;
  AnalyticalF3D<T,T>& _wantedF;
  SuperGeometry3D<T>& _geometry;
  int _material;
  T _globalValue;
public:
  DdifferenceObjectiveDf3D(SuperLatticePhysF3D<T,Lattice>& f, SuperLatticePhysF3D<T,Lattice>& dFdF, AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry, int material);
  bool operator()(T output[], const int input[]);
};


/// functor to compute 0.5(f-f_wanted)^2/f_wanted^2 on a lattice
//template <typename T, template <typename U> class Lattice>
template<typename T, template<typename U> class DESCRIPTOR>
class RelativeDifferenceObjective3D : public SuperF3D<T,T> {
private:
  SuperLattice3D<T, DESCRIPTOR>& _sLattice;
  SuperF3D<T,T>& _f;
  AnalyticalF3D<T,T>& _wantedF;
  SuperGeometry3D<T>& _geometry;
  SuperIndicatorF3D<T>& _indicatorF;
public:
  RelativeDifferenceObjective3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperF3D<T,T>& f, AnalyticalF3D<T,T>& wantedF,
                                SuperGeometry3D<T>& geometry, SuperIndicatorF3D<T>& indicatorF);
  RelativeDifferenceObjective3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperF3D<T,T>& f, AnalyticalF3D<T,T>& wantedF,
                                SuperGeometry3D<T>& geometry, std::vector<int> materials);
  RelativeDifferenceObjective3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperF3D<T,T>& f, AnalyticalF3D<T,T>& wantedF,
                                SuperGeometry3D<T>& geometry, int material);
  bool operator()(T output[], const int input[]);
};


/// functor to compute 0.5(f-f_wanted)^2/f_wanted^2 on a lattice
template <typename T, template <typename U> class Lattice>
class DrelativeDifferenceObjectiveDf3D : public SuperF3D<T,T> {
private:
  SuperF3D<T,T>& _f;
  SuperF3D<T,T>& _dFdF;
  AnalyticalF3D<T,T>& _wantedF;
  SuperGeometry3D<T>& _geometry;
  SuperIndicatorF3D<T>& _indicatorF;
  T _globalValue;
  int _extractDim;
public:
  DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperF3D<T,T>& f, SuperF3D<T,T>& dFdF,
                                   AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry,
                                   SuperIndicatorF3D<T>& indicatorF);
  DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperExtractComponentF3D<T,T>& f, SuperF3D<T,T>& dFdF,
                                   AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry,
                                   SuperIndicatorF3D<T>& indicatorF);
  DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperF3D<T,T>& f, SuperF3D<T,T>& dFdF,
                                   AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry,
                                   std::vector<int> materials);
  DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperF3D<T,T>& f, SuperF3D<T,T>& dFdF,
                                   AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry, int material);
  bool operator()(T output[], const int input[]);
};

} // namespace opti

} // namespace olb

#endif
