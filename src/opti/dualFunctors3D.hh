/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2014 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef DUAL_FUNCTORS_3D_HH
#define DUAL_FUNCTORS_3D_HH

#include<cmath>
#include<vector>
#include<map>

#include "opti/dualFunctors3D.h"
#include "opti/dualLbHelpers.h"
#include "functors/functors3D.h"

using namespace olb::opti;
using namespace olb;


/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {


template <typename T, template <typename U> class Lattice>
BlockLatticeDphysDissipationDf3D<T,Lattice>::BlockLatticeDphysDissipationDf3D
(BlockLatticeStructure3D<T,Lattice>& blockLattice, const UnitConverter<T,Lattice>& converter)
  : BlockLatticeF3D<T,Lattice>(blockLattice,Lattice<T>::q), _converter(converter)
{
  this->getName() = "dPhysDissipationDf";
}

template <typename T, template <typename U> class Lattice>
bool BlockLatticeDphysDissipationDf3D<T,Lattice>::operator()(T dDdf[], const int latticeR[])
{
  T nuLattice = _converter.getLatticeNu();
  T omega = _converter.getLatticeRelaxationFrequency();
  T dt = _converter.physTime();

  T rho;
  T u[3];
  T pi[6];
  this->_blockLattice.get(latticeR[0],latticeR[1],latticeR[2]).computeAllMomenta(rho, u, pi);
  T uSqr = util::normSqr<T,Lattice<T>::d>(u);

  for (int i=0; i<Lattice<T>::q; i++) { //output default 0
    dDdf[i] = T();
  }

  T pi2[Lattice<T>::d][Lattice<T>::d];

  pi2[0][0]=pi[0];
  pi2[0][1]=pi[1];
  if (util::TensorVal<Lattice<T> >::n == 6) {
    pi2[0][2]=pi[2];
  } else {
    pi2[1][1]=pi[2];
  }
  pi2[1][0]=pi[1];
  if (util::TensorVal<Lattice<T> >::n == 6) {
    pi2[1][1]=pi[3];
    pi2[1][2]=pi[4];
    pi2[2][0]=pi[2];
    pi2[2][1]=pi[4];
    pi2[2][2]=pi[5];
  }

  for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
    for (int iDim=0; iDim < Lattice<T>::d; ++iDim) {
      for (int jDim=0; jDim < Lattice<T>::d; ++jDim) {
        T dPidf2ndTerm = T();
        for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
          dPidf2ndTerm += Lattice<T>::c(iPop)[iDim] * Lattice<T>::c(iPop)[jDim]
                          * dualLbHelpers<T,Lattice>::equilibrium(iPop, jPop, rho, u, uSqr);
        }
        T dpidf = Lattice<T>::c[jPop][iDim]*Lattice<T>::c[jPop][jDim] - dPidf2ndTerm;
        // rho2 instead of rho vs simon!
        dDdf[jPop] += 2.*(pi2[iDim][jDim]*(dpidf - pi2[iDim][jDim]/2./rho*2.));
      }
    }
    dDdf[jPop] *= pow(omega*Lattice<T>::invCs2()/rho,2)/2.*_converter.getCharNu()/_converter.getLatticeNu()/dt/dt;
  }
  return true;
}

template <typename T, template <typename U> class Lattice>
SuperLatticeDphysDissipationDf3D<T,Lattice>::SuperLatticeDphysDissipationDf3D
(SuperLattice3D<T,Lattice>& sLattice, const UnitConverter<T,Lattice>& converter)
  : SuperLatticePhysF3D<T,Lattice>(sLattice,converter,Lattice<T>::q*Lattice<T>::d)
{
  this->getName() = "dPhysDissipationDf";
}

template <typename T, template <typename U> class Lattice>
bool SuperLatticeDphysDissipationDf3D<T,Lattice>::operator()(T output[], const int input[])
{
  int globIC = input[0];
  if ( this->_sLattice.getLoadBalancer().rank(globIC) == singleton::mpi().getRank() ) {
    int inputLocal[3];
    int overlap = this->_sLattice.getOverlap();
    inputLocal[0] = input[1] + overlap;
    inputLocal[1] = input[2] + overlap;
    inputLocal[2] = input[3] + overlap;

    BlockLatticeDphysDissipationDf3D<T,Lattice> blockLatticeF(
      this->_sLattice.getExtendedBlockLattice(this->_sLattice.getLoadBalancer().loc(globIC)),
      this->_converter);
    blockLatticeF(output,inputLocal);
    return true;
  } else {
    return false;
  }
}

template <typename T, template <typename U> class Lattice>
BlockLatticeDphysVelocityDf3D<T,Lattice>::BlockLatticeDphysVelocityDf3D
(BlockLatticeStructure3D<T,Lattice>& blockLattice, const UnitConverter<T,Lattice>& converter, int nDim, int extractDim)
  : BlockLatticeF3D<T,Lattice>(blockLattice,Lattice<T>::q*Lattice<T>::d), _converter(converter),
    _nDim(nDim), _extractDim(extractDim)
{
  this->getName() = "dPhysVelocityDf";
}

template <typename T, template <typename U> class Lattice>
bool BlockLatticeDphysVelocityDf3D<T,Lattice>::operator()(T dVelocityDf[], const int latticeR[])
{
  for (int i=0; i<Lattice<T>::d*Lattice<T>::q; i++) {
    dVelocityDf[i] = T();
  }
  T rho;
  T u[3];
  this->_blockLattice.get(latticeR[0],latticeR[1],latticeR[2]).computeRhoU(rho, u);

  for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
    for (int iDim=0; iDim < Lattice<T>::d; ++iDim) {
      if (iDim != _extractDim && _extractDim != -1) {
        dVelocityDf[jPop*Lattice<T>::d + iDim] = T(0);
      } else {
        dVelocityDf[jPop*Lattice<T>::d + iDim] = (Lattice<T>::c[jPop][iDim]-u[iDim])/rho;
      }
    }
  }
  return true;
}

template <typename T, template <typename U> class Lattice>
SuperLatticeDphysVelocityDf3D<T,Lattice>::SuperLatticeDphysVelocityDf3D
(SuperLattice3D<T,Lattice>& sLattice, const UnitConverter<T,Lattice>& converter)
  : SuperLatticePhysF3D<T,Lattice>(sLattice,converter,Lattice<T>::q), _nDim(-1), _extractDim(-1)
{
  this->getName() = "dPhysVelocityDf";
}

template <typename T, template <typename U> class Lattice>
SuperLatticeDphysVelocityDf3D<T,Lattice>::SuperLatticeDphysVelocityDf3D
(SuperLattice3D<T,Lattice>& sLattice, const UnitConverter<T,Lattice>& converter, SuperExtractComponentF3D<T,T>& sExtract)
  : SuperLatticePhysF3D<T,Lattice>(sLattice,converter,Lattice<T>::q),_nDim(1), _extractDim(sExtract.getExtractDim())
{
  this->getName() = "dPhysVelocityDf";
}

template <typename T, template <typename U> class Lattice>
bool SuperLatticeDphysVelocityDf3D<T,Lattice>::operator()(T output[], const int input[])
{
  int globIC = input[0];
  if ( this->_sLattice.getLoadBalancer().rank(globIC) == singleton::mpi().getRank() ) {
    int inputLocal[3];
    int overlap = this->_sLattice.getOverlap();
    inputLocal[0] = input[1] + overlap;
    inputLocal[1] = input[2] + overlap;
    inputLocal[2] = input[3] + overlap;

    BlockLatticeDphysVelocityDf3D<T,Lattice> blockLatticeF(
        this->_sLattice.getExtendedBlockLattice(this->_sLattice.getLoadBalancer().loc(globIC)),
        this->_converter,_nDim, _extractDim);
    blockLatticeF(output,inputLocal);
    return true;
  } else {
    return false;
  }
}

template <typename T, template <typename U> class Lattice>
DifferenceObjective3D<T,Lattice>::DifferenceObjective3D
(SuperLatticePhysF3D<T,Lattice>& f, AnalyticalF3D<T,T>& wantedF, SuperGeometry3D<T>& geometry, int material)
  : SuperLatticeF3D<T,Lattice>(f.getSuperLattice(),f.getTargetDim()), _f(f), _wantedF(wantedF), _geometry(geometry),
    _material(material)
{
  this->getName() = "differenceObjective";
}

template <typename T, template <typename U> class Lattice>
bool DifferenceObjective3D<T,Lattice>::operator()(T output[], const int input[])
{
  SuperLatticeFfromAnalyticalF3D<T,Lattice> fOnLattice(this->_wantedF,this->_sLattice);
  SuperL2Norm3D<T> uLpNorm(this->_f-fOnLattice,this->_geometry,_material);

  uLpNorm(output,input);

  pow(output[0],2);
  output[0] /= (T) 2;

  return true;
}

template <typename T, template <typename U> class Lattice>
DdifferenceObjectiveDf3D<T,Lattice>::DdifferenceObjectiveDf3D
(SuperLatticePhysF3D<T,Lattice>& f, SuperLatticePhysF3D<T,Lattice>& dFdF, AnalyticalF3D<T,T>& wantedF,
 SuperGeometry3D<T>& geometry, int material)
  : SuperLatticeF3D<T,Lattice>(f.getSuperLattice(),f.getTargetDim()), _f(f), _dFdF(dFdF), _wantedF(wantedF),
    _geometry(geometry), _material(material)
{
  this->getName() = "dDifferenceObjectiveDf";
}

template <typename T, template <typename U> class Lattice>
bool DdifferenceObjectiveDf3D<T,Lattice>::operator()(T dJdF[], const int latticeR[])
{
  T weight = pow(this->_f.getConverter().getLatticeL(),3);

  T physR[3] = {T(),T(),T()};
  this->_geometry.getPhysR(physR,latticeR);

  for (int i=0; i<Lattice<T>::q; i++) {
    dJdF[i] = T();
  }

  int nDim = this->_f.getTargetDim();
  if (this->_geometry.getLoadBalancer().rank(latticeR[0]) == singleton::mpi().getRank() ) {
    std::vector<T> dJdD(nDim,T());
    T f[nDim];
    this->_f(f,latticeR);
    T wantedF[nDim];
    this->_wantedF(wantedF,physR);
    T dFdF[nDim*Lattice<T>::q];
    this->_dFdF(dFdF,latticeR);
    for (int iDim=0; iDim<nDim; iDim++) {
      dJdD[iDim] = (f[iDim] - wantedF[iDim])*weight*weight;
      for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
        dJdF[jPop] += dJdD[iDim]*dFdF[jPop*nDim + iDim];
      }
    }
  }
  return true;
}


//template <typename T, template <typename U> class Lattice>
template<typename T, template<typename U> class DESCRIPTOR>
RelativeDifferenceObjective3D<T,DESCRIPTOR>::RelativeDifferenceObjective3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperF3D<T,T>& f,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    SuperIndicatorF3D<T>& indicatorF)
  : SuperF3D<T,T>(sLattice,1),
    _sLattice(sLattice),
    _f(f),
    _wantedF(wantedF),
    _geometry(geometry),
    _indicatorF(indicatorF)
{
  this->getName() = "relativeDifferenceObjective";
}


//template <typename T, template <typename U> class Lattice>
template<typename T, template<typename U> class DESCRIPTOR>
RelativeDifferenceObjective3D<T,DESCRIPTOR>::RelativeDifferenceObjective3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperF3D<T,T>& f,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    std::vector<int> materials)
  : RelativeDifferenceObjective3D(sLattice, f, wantedF, geometry, *(new SuperIndicatorMaterial3D<T> (geometry, materials)))
{};


//template <typename T, template <typename U> class Lattice>
template<typename T, template<typename U> class DESCRIPTOR>
RelativeDifferenceObjective3D<T,DESCRIPTOR>::RelativeDifferenceObjective3D(SuperLattice3D<T, DESCRIPTOR>& sLattice, SuperF3D<T,T>& f,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    int material)
  : RelativeDifferenceObjective3D(sLattice, f, wantedF, geometry, std::vector<int> (1, material))
{};


//template <typename T, template <typename U> class Lattice>
template<typename T, template<typename U> class DESCRIPTOR>
bool RelativeDifferenceObjective3D<T,DESCRIPTOR>::operator()(T output[], const int input[])
{
  SuperLatticeFfromAnalyticalF3D<T,DESCRIPTOR> fOnLattice(this->_wantedF, _sLattice);
  SuperL2Norm3D<T> uLpNorm(this->_f - fOnLattice,this->_geometry, this->_indicatorF);
  SuperL2Norm3D<T> uSolLpNorm(fOnLattice, this->_geometry, this->_indicatorF);
  T result[1];
  T result1[1];
  uLpNorm(result,input);
  uSolLpNorm(result1,input);
  output[0] = pow(result[0] / result1[0], 2) / (T) 2;
  return true;
}


template <typename T, template <typename U> class Lattice>
DrelativeDifferenceObjectiveDf3D<T,Lattice>::DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperF3D<T,T>& f,
    SuperF3D<T,T>& dFdF,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    SuperIndicatorF3D<T>& indicatorF)
  :  SuperF3D<T,T>(sLattice,f.getTargetDim()),
    _f(f), _dFdF(dFdF), _wantedF(wantedF),
    _geometry(geometry), _indicatorF(indicatorF), _extractDim(-1)
{
  this->getName() = "dRelativeDifferenceObjectiveDf";
  SuperLatticeFfromAnalyticalF3D<T,Lattice> fOnLattice(this->_wantedF, sLattice );
  SuperL2Norm3D<T> wantedFlp(fOnLattice, this->_geometry, _indicatorF);
  T output[1];
  wantedFlp(output, nullptr);
  this->_globalValue = pow(output[0], 2);
}


template <typename T, template <typename U> class Lattice>
DrelativeDifferenceObjectiveDf3D<T,Lattice>::DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperExtractComponentF3D<T,T>& f,
    SuperF3D<T,T>& dFdF,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    SuperIndicatorF3D<T>& indicatorF)
  :  SuperF3D<T,T>(sLattice,f.getTargetDim()),
    _f(f), _dFdF(dFdF), _wantedF(wantedF),
    _geometry(geometry), _indicatorF(indicatorF), _extractDim(f.getExtractDim())
{
  this->getName() = "dRelativeDifferenceObjectiveDf";
  SuperLatticeFfromAnalyticalF3D<T,Lattice> fOnLattice(this->_wantedF, sLattice );
  SuperL2Norm3D<T> wantedFlp(fOnLattice, this->_geometry, _indicatorF);
  T output[1];
  wantedFlp(output, nullptr);
  this->_globalValue = pow(output[0], 2);
}


template <typename T, template <typename U> class Lattice>
DrelativeDifferenceObjectiveDf3D<T,Lattice>::DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperF3D<T,T>& f,
    SuperF3D<T,T>& dFdF,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    std::vector<int> materials)
  : DrelativeDifferenceObjectiveDf3D(f, dFdF, wantedF, geometry,
                                     *(new SuperIndicatorMaterial3D<T> (geometry, materials)))
{};


template <typename T, template <typename U> class Lattice>
DrelativeDifferenceObjectiveDf3D<T,Lattice>::DrelativeDifferenceObjectiveDf3D(SuperLattice3D<T, Lattice>& sLattice, SuperF3D<T,T>& f,
    SuperF3D<T,T>& dFdF,
    AnalyticalF3D<T,T>& wantedF,
    SuperGeometry3D<T>& geometry,
    int material)
  : DrelativeDifferenceObjectiveDf3D(f, dFdF, wantedF, geometry, std::vector<int> (1, material))
{};



//ACHTUNG: BRAUCHT CONVERTER!
template <typename T, template <typename U> class Lattice>
bool DrelativeDifferenceObjectiveDf3D<T,Lattice>::operator()(T dJdF[], const int latticeR[])
{
  T weight = pow(this->_f.getSuperStructure().getCuboidGeometry().getMinDeltaR(),3);
  T wantedSum = this->_globalValue;
  T physR[3] = {T(),T(),T()};
  this->_geometry.getPhysR(physR,latticeR);
  for (int i=0; i<Lattice<T>::q; i++) {
    dJdF[i] = T();
  }
  int nDim = Lattice<T>::d;//this->_f.getTargetDim();
  bool indicate;
  _indicatorF(&indicate, latticeR);
  if (this->_geometry.getLoadBalancer().rank(latticeR[0]) == singleton::mpi().getRank() && indicate) {
    std::vector<T> dJdD(nDim,T());
    T f[nDim];
    this->_f(f,latticeR);
    T wantedF[nDim];
    this->_wantedF(wantedF,physR);
    T dFdF[nDim*Lattice<T>::q];
    this->_dFdF(dFdF,latticeR);
    for (int iDim=0; iDim<nDim; iDim++) {
      if (_extractDim != -1) {
        if (iDim == _extractDim) {
          dJdD[iDim] = (f[0] - wantedF[0])*weight*weight;
        } else {
          dJdD[iDim] = T(0);
        }
      } else {
        dJdD[iDim] = (f[iDim] - wantedF[iDim])*weight*weight;
      }
      for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
        dJdF[jPop] += dJdD[iDim]*dFdF[jPop*nDim + iDim];
      }
    }
  }
  return true;
}


} // namespace opti

} // namespace olb

#endif
