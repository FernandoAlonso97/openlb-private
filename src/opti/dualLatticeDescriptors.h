/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#ifndef DUAL_LATTICE_DESCRIPTORS_H
#define DUAL_LATTICE_DESCRIPTORS_H

#include "dynamics/latticeDescriptors.h"


namespace olb {

namespace descriptors {

////////////////////////////////////////////////////////////////////////////////
// descriptor for dual lattice - 3D

struct DualForcedD3Q19DescriptorBase {
  static const int numScalars = 47;
  static const int numSpecies = 5;
  static const int forceBeginsAt = 0;
  static const int sizeOfForce   = 3;
  static const int coordinateBeginsAt = 3;
  static const int sizeOfCoordinate   = 3;
  static const int fBeginsAt = 6;
  static const int sizeOfF   = 19;
  static const int dJdFBeginsAt = 25;
  static const int sizeOfdJdF   = 19;
  static const int dJdAlphaBeginsAt = 44;
  static const int sizeOfdJdAlpha   = 3;
};

template <typename T> struct DualForcedD3Q19Descriptor
  : public D3Q19DescriptorBase<T> {
  typedef DualForcedD3Q19DescriptorBase ExternalField;
};

template <typename T> struct DualForcedMRTD3Q19Descriptor
  : public MRTD3Q19DescriptorBase<T>, DualForcedD3Q19DescriptorBase {
  typedef DualForcedD3Q19DescriptorBase ExternalField;
};

////////////////////////////////////////////////////////////////////////////////
// descriptor for dual lattice - 3D porous

struct DualPorousD3Q19DescriptorBase {
  static const int numScalars = 48;
  static const int numSpecies = 6;
  static const int porosityIsAt = 0;
  static const int sizeOfPorosity   = 1;
  static const int forceBeginsAt = 1;
  static const int sizeOfForce   = 3;
  static const int coordinateBeginsAt = 4;
  static const int sizeOfCoordinate   = 3;
  static const int fBeginsAt = 7;
  static const int sizeOfF   = 19;
  static const int dJdFBeginsAt = 26;
  static const int sizeOfdJdF   = 19;
  static const int dJdAlphaBeginsAt = 45;
  static const int sizeOfdJdAlpha   = 3;
};


template <typename T> struct DualPorousD3Q19Descriptor
  : public D3Q19DescriptorBase<T> {
  typedef DualPorousD3Q19DescriptorBase ExternalField;
};

} // namespace descriptors

} // namespace olb

#endif
