/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2013 Mathias J. Krause, Geng Liu
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of optimization algorthims -- header file.
 */
/** \file
 * A collection of dynamics classes for dual LB methods
 * (e.g. dual MRT) with which a CellView object can be
 * instantiated -- header file.
 */

#ifndef DUAL_MRT_DYNAMICS_H
#define DUAL_MRT_DYNAMICS_H

//#include "optiStructure.h"
//#include "solver3D.h"



/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {


template<typename T> class Controller;
template<typename T, template<typename U> class Lattice> class DualController;




/// Implementation of the dual MRT collision step with external force
template<typename T, template<typename U> class Lattice, class Momenta>
class DualForcedMRTdynamics : public MRTdynamics<T,Lattice,Momenta> {
public:
  /// Constructor
  DualForcedMRTdynamics(T omega_, Momenta& momenta_, Controller<T>* controller);

  /// Collision step
  virtual void collide(CellView<T,Lattice>& cell,
                       LatticeStatistics<T>& statistics_);

  virtual void defineRho(CellView<T,Lattice>& cell, T rho);
private:

  T Mt_S_M[Lattice<T>::q][Lattice<T>::q];
  T invM_invMt[Lattice<T>::q][Lattice<T>::q];
  T Mt_S_invMt[Lattice<T>::q][Lattice<T>::q];

  static const int forceBeginsAt = Lattice<T>::ExternalField::forceBeginsAt;
  static const int sizeOfForce   = Lattice<T>::ExternalField::sizeOfForce;

  Controller<T>* _controller;
};

////////////////////// Class ForcedMRTdynamics /////////////////////////

/** \param omega_ relaxation parameter, related to the dynamic viscosity
 *  \param momenta_ a Momenta object to know how to compute velocity momenta
 */
template<typename T, template<typename U> class Lattice,class Momenta>
DualForcedMRTdynamics<T,Lattice,Momenta>::DualForcedMRTdynamics (
  T omega_, Momenta& momenta_, Controller<T>* controller)
  : MRTdynamics<T,Lattice,Momenta>(omega_, momenta_)
{
  // This ensures both that the constant sizeOfForce is defined in
  // ExternalField and that it has the proper size

  T rt[Lattice<T>::q]; // relaxation times vector.
  for (int iPop  = 0; iPop < Lattice<T>::q; ++iPop) {
    rt[iPop] = Lattice<T>::S[iPop];
  }
  for (int iPop  = 0; iPop < Lattice<T>::shearIndexes; ++iPop) {
    rt[Lattice<T>::shearViscIndexes[iPop]] = omega_;
  }

  T tmp[Lattice<T>::q][Lattice<T>::q];

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      Mt_S_M[iPop][jPop] = T();
      Mt_S_invMt[iPop][jPop] = T();
      invM_invMt[iPop][jPop] = T();
      tmp[iPop][jPop] = T();
    }
  }

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      for (int kPop=0; kPop < Lattice<T>::q; ++kPop) {
        if (jPop==kPop) {
          tmp[iPop][jPop] += Lattice<T>::M[kPop][iPop]*rt[kPop];
        }
      }
    }
  }
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      for (int kPop=0; kPop < Lattice<T>::q; ++kPop) {
        Mt_S_M[iPop][jPop] += tmp[iPop][kPop]*Lattice<T>::M[kPop][jPop];
        Mt_S_invMt[iPop][jPop] += tmp[iPop][kPop]*Lattice<T>::invM[jPop][kPop];
        invM_invMt[iPop][jPop] += Lattice<T>::invM[iPop][kPop]*Lattice<T>::invM[jPop][kPop];
      }
    }
  }

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      //std::cout << Mt_S_M[iPop][jPop];
      //  std::cout <<Mt_S_invMt[iPop][jPop];
      //    std::cout <<invM_invMt[iPop][jPop];
      //if (iPop == jPop) {
      // Mt_S_M[iPop][jPop] = omega_;
      // Mt_S_invMt[iPop][jPop] = omega_;
      // invM_invMt[iPop][jPop] = 1.;
      //}
    }
  }

  OLB_PRECONDITION( Lattice<T>::d == Lattice<T>::ExternalField::sizeOfForce );
  _controller = controller;
}

template<typename T, template<typename U> class Lattice,class Momenta>
void DualForcedMRTdynamics<T,Lattice,Momenta>::defineRho(CellView<T,Lattice>& cell, T rho)
{

  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop]=rho;
  }
}



template<typename T, template<typename U> class Lattice,class Momenta>
void DualForcedMRTdynamics<T,Lattice,Momenta>::collide (
  CellView<T,Lattice>& cell,
  LatticeStatistics<T>& statistics )
{

  cell.revert();


  T rho_phi_x = T();
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    rho_phi_x += cell[iPop];
  }

#ifndef ADT

  //#ifdef ADT

  // Who am I?
  T* ct = cell.getExternal(3);
  int c[3];
  c[0] = (int)ct[0];
  c[1] = (int)ct[1];
  c[2] = (int)ct[2];



  // Preparation
  T* pop_f = cell.getExternal(Lattice<T>::ExternalField::fBeginsAt);
  T* dJdF = cell.getExternal(Lattice<T>::ExternalField::dJdFBeginsAt);

  T rho_f;
  T u_f[3];
  rho_f = T();
  for (int iD=0; iD < Lattice<T>::d; ++iD) {
    u_f[iD] = T();
  }
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    rho_f += pop_f[iPop];
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      u_f[iD] += pop_f[iPop]*Lattice<T>::c(iPop)[iD];
    }
  }
  for (int iD=0; iD < Lattice<T>::d; ++iD) {
    u_f[iD] /= rho_f;
  }

  // Computation of dual equillibrium
  T Meq_phi[Lattice<T>::q][Lattice<T>::q];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    T u_c = T();
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      u_c += u_f[iD]*Lattice<T>::c(iPop)[iD];
    }
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      T sum = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        sum += (u_f[iD]*u_f[iD] + 2.*Lattice<T>::c[jPop][iD]*(Lattice<T>::c(iPop)[iD]-u_f[iD]))*Lattice<T>::invCs2*0.5+(u_c*Lattice<T>::c(iPop)[iD]*(2.*Lattice<T>::c[jPop][iD]-u_f[iD]))*Lattice<T>::invCs2*Lattice<T>::invCs2*0.5;
      }
      Meq_phi[iPop][jPop] = Lattice<T>::t[iPop]*(1.+sum);
    }
  }

  // Computation of dual force
  T* force = cell.getExternal(forceBeginsAt);
  T F1_phi[Lattice<T>::q][Lattice<T>::q];
  T F2_phi[Lattice<T>::q][Lattice<T>::q];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    T f_c = T();
    for (int iD=0; iD < Lattice<T>::d; ++iD) {
      f_c += force[iD]*Lattice<T>::c(iPop)[iD];
    }
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      T sum = T();
      for (int iD=0; iD < Lattice<T>::d; ++iD) {
        sum += (f_c*Lattice<T>::c(iPop)[iD]-force[iD]/(T)Lattice<T>::invCs2)*Lattice<T>::c[jPop][iD];
      }
      F1_phi[iPop][jPop] = Lattice<T>::t[iPop]*Lattice<T>::invCs2*f_c;
      F2_phi[iPop][jPop] = Lattice<T>::t[iPop]*Lattice<T>::invCs2*Lattice<T>::invCs2*sum;
    }
  }


  // Computation of dual collision
  T dualMRTcollision[Lattice<T>::q];
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    dualMRTcollision[iPop] = T();
    for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
      T I=T();
      if (iPop==jPop) {
        I=T(1);
      }
      dualMRTcollision[iPop] += (I - (I - Meq_phi[jPop][iPop])*Mt_S_invMt[iPop][iPop] + F1_phi[jPop][iPop] + F2_phi[jPop][iPop]*(1. - 0.5*Mt_S_invMt[iPop][iPop]))*cell[jPop];
    }
  }


  // Adding dirivitive of objective and writing back to cell
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    cell[iPop] = dualMRTcollision[iPop]  + dJdF[iPop];
  }

  // Incrementing statistic values for convergence
  T phi2 = T();
  for (int iPop=0; iPop < Lattice<T>::q; ++iPop) {
    phi2 += cell[iPop]*cell[iPop];
  }

  statistics.incrementStats(1.0 + cell[0], phi2);
  cell.revert();

#endif
}







} // namespace opti

} // namespace olb

#endif
