/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#include <vector>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

template<typename T>
T euklidN2(const T x[], int dim)
{

  T euklid = 0;
  for (int iDim=0; iDim<dim; iDim++) {
    euklid += x[iDim]*x[iDim];
  }
  return euklid;
}

template<typename T>
T euklidN2(const T x[], const T y[], int dim)
{

  T euklid = 0;
  for (int iDim=0; iDim<dim; iDim++) {
    euklid += (x[iDim]-y[iDim])*(x[iDim]-y[iDim]);
  }
  return euklid;
}


template<typename T>
T euklidN(const T x[], int dim)
{

  return sqrt(euklidN2(x,dim));
}

template<typename T>
T euklidN(const T x[], const T y[], int dim)
{

  return sqrt(euklidN2(x,y,dim));
}



