/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef OPTI_CASE_H
#define OPTI_CASE_H

#include <string>

namespace olb {

namespace opti {


/// Abstract Base Class for Optimization Cases
///   Provides interface for OptiSolver which uses an OptiCase together with an Optimizer
template<typename T>
class OptiCase {
public:
  std::string _optiCaseType; // Needed for distinguishing inheriting optiCases
  /// Virtual Destructor for defined behaviour
  virtual ~OptiCase() {};
  virtual T evaluateObjectFunctional(T* _control) = 0;
  virtual void computeDerivatives (T* _control, T* _derivatives) = 0;
  virtual void increaseOptiStep () = 0;
  std::string getType()
  {
    return _optiCaseType;
  }

  /// This method will be overwritten by optiCaseDual only
  virtual T permeabilityToPorosity(T permeability)
  {
    return permeability;
  };
};

} // namespace opti

} // namespace olb

#endif
