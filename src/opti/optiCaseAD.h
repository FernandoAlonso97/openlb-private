/*  This file is part of the OpenLB library
*
*  Copyright (C) 2012 Mathias J. Krause
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

/** \file
* An OptiCase using Algorithmic Differentiation (AD)
*/


#ifndef OPTI_CASE_AD_H
#define OPTI_CASE_AD_H


#include "io/xmlReader.h"

#include "aDiff.h"
#include "optiCase.h"
#include "controller.h"
#include "solver.h"
#include "solver3D.h"


namespace olb {

namespace opti {

/// This class implements the evaluation algorithms of the goal functional and it's derivatives by using algorithmic differentiation (AD)
template<typename S, typename T, template<typename U> class Lattice>
class OptiCaseAD : public OptiCase<S> {

protected:
  XMLreader*                                  _xml;
  std::string                                 _functionalName;
  int                                         _dim;
  Controller<S>*                              _primalController;
  Controller<T>*                              _dualController;
  Solver<S,S,Lattice>*                        _primalSolver;
  Solver<S,T,Lattice>*                        _dualSolver;
  Objective3D<S,Lattice>*                     _primalObjective; //J
  Objective3D<T,Lattice>*                     _dualObjective; //J

public:
  OptiCaseAD()
  {
    this->_optiCaseType = "AD";
    _xml = nullptr;
    _primalController = nullptr;
    _dualController  = nullptr;
    _primalSolver  = nullptr;
    _dualSolver  = nullptr;
  };
  OptiCaseAD(XMLreader* xml)
    : OptiCaseAD()
  {
    reInit(xml);
  };
  ~OptiCaseAD()
  {
    free();
  };

  void reInit(XMLreader* xml)
  {
    _xml = xml;
    reInit();
  };

  void reInit()
  {
    if (_xml!=nullptr) {
      free();
      (*_xml)["Optimization"]["Functional"].read(_functionalName);
      (*_xml)["Optimization"]["Dimension"].read(_dim);

      _primalController = new Controller<S>(_dim);
      _dualController = new Controller<T>(_dim);

      _primalSolver = new Solver3D<S,S,Lattice>(_xml, _primalController);
      _primalSolver->init();

      _dualSolver = new Solver3D<S,T,Lattice>(_xml,_dualController);
      _dualSolver->init();

      _primalObjective = new Objective3D<S,Lattice>( *_primalSolver->getLattice(), *_primalSolver->getGeometry(),
          *_primalController, *_primalSolver->getConverter());

      _dualObjective = new Objective3D<T,Lattice>( *_dualSolver->getLattice(), *_dualSolver->getGeometry(),
          *_dualController, *_dualSolver->getConverter());
    }
  };

  void free()
  {

    delete _primalController;
    _primalController = NULL;
    delete _dualController;
    _dualController = NULL;
    delete _primalSolver;
    _primalSolver = NULL;
    delete _dualSolver;
    _dualSolver = NULL;
  }

  /// evaluates the Objective functional (given through _functionalName)
  S evaluateObjectFunctional(S* _control)
  {

    S value;
    _primalController->setControl(_control,_dim);
    // initialize and solve the fluid flow problem
    //_primalProblem->getSolver().initVelocity();
    //_primalProblem->getSolver().initForce();
    _primalSolver->solve();

    // compute errors and evaluate the goal functional

    int input[3];
    S output[1];
    (*_primalObjective)(output,input);
    value = output[0];
    return value;
  };

  /// evaluates the derivatives of the objective functional
  void computeDerivatives (S* _control, S* _derivatives)
  {
    static int iter(0);
    _primalSolver->writeVTK(iter++);
    //_primalProblem->getSolver().saveBackup("backup");
    T* control = new T [_dim];
    for (int iDim=0; iDim<_dim; iDim++) {
      control[iDim] = (T)_control[iDim];
#ifdef ADT
      control[iDim].setDiffVariable(iDim);
#endif
    }

    _dualController->setControl(control,_dim);
    //_dualSolver->initVelocity();
    //_dualProblem->getSolver().initForce();
    _dualSolver->solve();
    // Evaluate object functional, assign values to "value"

    int input[3];
    T output[1];
    (*_dualObjective)(output,input);
#ifdef ADT
    T value;
    value = output[0];

    for (int iDim=0; iDim<_dim; iDim++) {
      //std::cout << "assign derivatives..." ;
      _derivatives[iDim] = (S) value.d(iDim);
      //std::cout << " ok" << std::endl;
    }
#endif
    delete [] control;
    //std::cout << " ok" << std::endl;
  };

  void increaseOptiStep () {};
};
}
}

#endif
