/*  This file is part of the OpenLB library
*
*  Copyright (C) 2012 Mathias J. Krause
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

/** \file
* An example OptiCase to test the AD Optimizer
*/


#ifndef OPTI_CASE_AD_TEST_H
#define OPTI_CASE_AD_TEST_H

#include <cassert>

#include "io/xmlReader.h"
#include "optiCase.h"


namespace olb {

namespace opti {

/// A class that implements analytically the goalFunctional and the derivatives of the 'Rosenbrock function'
/// to verify the correctness of the optimizer. The Minimum lies at (x,y)=(1,1), where f(x,y)=0.
template<typename S, typename T>
class OptiCaseADTest : public OptiCase<S> {

private:
  XMLreader *_xml;
  int _dim;

public:

  /// Construction of an optiCaseADTest
  OptiCaseADTest()
  {
    this->_optiCaseType = "adTest";
    _xml = nullptr;
  };

  /// Construction of an optiCaseADTest
  OptiCaseADTest(XMLreader *xml)
    : OptiCaseADTest()
  {
    reInit(xml);
  };

  void reInit(XMLreader *xml)
  {
    _xml = xml;
    reInit();
  };

  void reInit()
  {
    if (_xml != NULL) {
      (*_xml)["Optimization"]["Dimension"].read(_dim);
    }
  };

  /// returns the (analytically) evaluated function value of the Rosenbrock function
  S evaluateObjectFunctional(S *control)
  {

    static int counter = 0;
    counter++;
    std::cout << "COUNTER evaluateObjectFunctional called: " << counter << std::endl;

    S value = 0.;
    S alpha = 99.;

    for (int i = 0; i < _dim / 2; i++) {
      value += alpha * (control[2 * i + 1] - control[2 * i] * control[2 * i])
               * (control[2 * i + 1] - control[2 * i] * control[2 * i])
               + (1 - control[2 * i]) * (1 - control[2 * i]);
    }

    return value;
  };

  /// returns the (analytically) evaluated derivative of the Rosenbrock function
  void computeDerivatives(S *_control, S *_derivatives)
  {

    T value = 0.;
    S alpha = 99.;

    T *control = new T[_dim];
    for (int iDim = 0; iDim < _dim; iDim++) {
      control[iDim] = (T) _control[iDim];
#ifdef ADT
      control[iDim].setDiffVariable(iDim);
#endif
    }

    for (int i = 0; i < _dim / 2; i++) {
      value += alpha * (control[2 * i + 1] - control[2 * i] * control[2 * i])
               * (control[2 * i + 1] - control[2 * i] * control[2 * i])
               + (T(1) - control[2 * i]) * (T(1) - control[2 * i]);
    }

#ifdef ADT
    for (int iDim=0; iDim<_dim; iDim++) {
      _derivatives[iDim] = (S) value.d(iDim);
    }
#endif
    delete[] control;
  };
  //T output[];
  //ADFunctor(output, RosenBrockFunctor());


  void increaseOptiStep() { };
};

}
}

#endif
