/*  This file is part of the OpenLB library
*
*  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

/** \file
* An OptiCase using Adjoint-based Differentiation
*/


#ifndef OPTI_CASE_DUAL_H
#define OPTI_CASE_DUAL_H

#include <cassert>

#include "io/xmlReader.h"

#include "optiCase.h"
#include "controller.h"
#include "solver.h"
#include "solver3D.h"

namespace olb {

namespace opti {

/// This class implements the evaluation algorithms of the goal functional and it's derivatives by using Adjoint-based Differentiation
template<typename S, typename T, template<typename U> class Lattice>
class OptiCaseDual : public OptiCase<S> {

protected:
  OstreamManager clout;
  XMLreader* _xml;

  std::string _functionalName;
  std::string _controlType;
  std::string _projectionName;

  int _alphaDim;
  int _controlMaterial;
  int _dim;

  Controller<S>*                  _controller;
  Solver<S, S, Lattice>*          _primalSolver;
  Solver<S, S, Lattice>*          _dualSolver;

  Objective3D<S, Lattice>*        _objective; //J
  DObjectiveDf3D<S, Lattice>*     _dObjectiveDf; //derivative of the objective with respect to the primal variable
  DObjectiveDControl3D<S, Lattice>* _dObjectiveDcontrol; //derivative of the objective with respect to the distributed control variable
  Projection3D<S, Lattice>*       _projection; //maps the control to a lattice functor
  SuperLatticeF3D<S, Lattice>*    _dProjectionDcontrol;

public:
  /// Construction of an OptiCaseAD
  OptiCaseDual();

  /// Construction of an OptiCaseAD
  OptiCaseDual(XMLreader *xml);

  /// Destructor of an OptiCaseAD
  ~OptiCaseDual();

  /// Re-init `OptiCase` from XML
  void reInit(XMLreader *xml);
  /// Re-init `OptiCase` (`_xml` must be initialized before!)
  void reInit();

  /// Delete `_controller`, `_primalSolver` and `_dualSolver` assign `nullptr`
  void free();

  /// Solve primal problem and evaluate objective
  S evaluateObjectFunctional(S *_control);

  /// Compute derivatives for the adjoint problem
  void computeDerivatives(S *_control, S *_derivatives);

  void increaseOptiStep();

  virtual T permeabilityToPorosity(T permeability);
};


}

}

#endif
