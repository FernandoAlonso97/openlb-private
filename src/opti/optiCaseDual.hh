/*  This file is part of the OpenLB library
*
*  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

/** \file
* An OptiCase using Adjoint-based Differentiation
*/


#ifndef OPTI_CASE_DUAL_HH
#define OPTI_CASE_DUAL_HH

#include "optiCaseDual.h"

#include <cassert>
#include <regex>

namespace olb {

namespace opti {

template<typename S, typename T, template<typename U> class Lattice>
OptiCaseDual<S,T,Lattice>::OptiCaseDual()
  : clout(std::cout, "OptiCaseDual")
{
  this->_optiCaseType = "Dual";
  _xml = nullptr;
  _controller = nullptr;
  _primalSolver = nullptr;
  _dualSolver = nullptr;
}

template<typename S, typename T, template<typename U> class Lattice>
OptiCaseDual<S,T,Lattice>::OptiCaseDual(XMLreader *xml)
  : OptiCaseDual()
{
  reInit(xml);
}

template<typename S, typename T, template<typename U> class Lattice>
OptiCaseDual<S,T,Lattice>::~OptiCaseDual()
{
  free();
}

template<typename S, typename T, template<typename U> class Lattice>
void OptiCaseDual<S,T,Lattice>::reInit(XMLreader *xml)
{
  _xml = xml;
  reInit();
}

template<typename S, typename T, template<typename U> class Lattice>
void OptiCaseDual<S,T,Lattice>::reInit()
{
  clout << "### Reinit ... " << std::endl;
  if (_xml != nullptr) {
    // delete all local objects
    free();

    //_functionalName = _db->get<std::string>("Optimization","Functional");
    (*_xml)["Optimization"]["Dimension"].read(_dim);

    // initialize local objects
    _controller = new Controller<S>(_dim);

    _primalSolver = new Solver3D<S, S, Lattice>(_xml, _controller);
    clout << "Init Primal Solver..." << std::endl;
    _primalSolver->init();

    clout << "Create Objectives..." << std::endl;
    _objective = new Objective3D<S, Lattice>(*_primalSolver->getLattice(),
        *_primalSolver->getGeometry(),
        *_controller, *_primalSolver->getConverter());
    clout << "Create DObjectivesDf..." << std::endl;
    _dObjectiveDf = new DObjectiveDf3D<S, Lattice>(*_primalSolver->getLattice(),
        *_primalSolver->getGeometry(),
        *_controller, *_primalSolver->getConverter());
    clout << "Create DObjectivesDctrl..." << std::endl;
    _dObjectiveDcontrol = new DObjectiveDControl3D<S, Lattice>(*_primalSolver->getLattice(),
        *_primalSolver->getGeometry(),
        *_controller, *_primalSolver->getConverter(),
        3);
    clout << "Create Dual Solver..." << std::endl;

    _dualSolver = new Solver3D<S, S, Lattice>(_xml, _controller);
    clout << "Reading Dynamics..." << std::endl;
    std::string dynamicsName;
    (*_xml)["Optimization"]["Dynamics"].read<std::string>(dynamicsName);
    clout << "Reading Dynamics... OK" << std::endl;
    _dualSolver->setBulkDynamics(dynamicsName);
    clout << "Dual Solver Init..." << std::endl;
    _dualSolver->init();

    clout << "Read Opti Data..." << std::endl;
    (*_xml)["Optimization"]["ControlType"].read(_controlType);
    (*_xml)["Optimization"]["ControlMaterial"].read(_controlMaterial);
    (*_xml)["Optimization"]["Projection"].read(_projectionName);
    clout << "Read Opti Data... OK" << std::endl;


    // Force optimisation //
    if (_controlType == "force") {
      std::cout <<"physMasslessForce existiert nicht" << std::endl;
      exit(-1);
      _alphaDim = 3;
      S scale = 1.;// TODO: physMasslessForce existiert nicht / _primalSolver->getConverter()->physMasslessForce();
      _projection = new Projection3D<S, Lattice>(*_primalSolver->getLattice(), *_primalSolver->getGeometry(), _controlMaterial,
          *_controller, *_primalSolver->getConverter(), _alphaDim, scale);
      AnalyticalFfromSuperF3D<S> *_analyticalAlpha = new AnalyticalFfromSuperF3D<S>(*_projection);
      _primalSolver->addExternalField(_analyticalAlpha);
      _dualSolver->addExternalField(_analyticalAlpha);
      static AnalyticalConst3D<S, S> dProjectionDcontrol(scale, scale, scale);
      _dProjectionDcontrol = new SuperLatticeFfromAnalyticalF3D<S, Lattice>(dProjectionDcontrol, *_primalSolver->getLattice());
    }

    // Porosity optimisation //
    if (_controlType == "porosity") {
      _alphaDim = 1;
      std::smatch match;

      // Choose Alpha Class by XML File

      // --- grid-dependant projections --- //
      if (_projectionName == "Baron") {
        _projection = new BaronProjection3D<S, Lattice>(*_primalSolver->getLattice(), *_primalSolver->getGeometry(),
            _controlMaterial, *_controller, *_primalSolver->getConverter(), _alphaDim);
        _dProjectionDcontrol = new DBaronProjectionDAlpha3D<S, Lattice>(*_primalSolver->getLattice(),
            *_primalSolver->getGeometry(), _controlMaterial,
            *_controller, *_primalSolver->getConverter(), _alphaDim);
      } else if (_projectionName == "Krause") {
        _projection = new KrauseProjection3D<S, Lattice>(*_primalSolver->getLattice(), *_primalSolver->getGeometry(),
            _controlMaterial, *_controller, *_primalSolver->getConverter(), _alphaDim);
        _dProjectionDcontrol = new DKrauseProjectionDAlpha3D<S, Lattice>(*_primalSolver->getLattice(),
            *_primalSolver->getGeometry(), _controlMaterial,
            *_controller, *_primalSolver->getConverter(), _alphaDim);
      }

      // --- grid-independant projections --- //
      else if (_projectionName == "Foerster") {
        GiProjection3D<S, Lattice>* proj_ptr = new FoersterGiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
            *_primalSolver->getGeometry(),
            _controlMaterial, *_controller,
            *_primalSolver->getConverter(),
            _alphaDim);
        _projection = proj_ptr;
        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
      }
      // CAREFUL! regex only supported from gcc 4.9+
      else if (std::regex_match(_projectionName, match, std::regex ("(Foerster)([0-9])"))) {
        int n = std::stoi(match[2]);
        clout << "Creating Projection Foerster" << match[2] << ": exp(a^" << 2.0*n << ")" << std::endl;
        GiProjection3D<S, Lattice>* proj_ptr = new FoersterNGiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
            *_primalSolver->getGeometry(),
            _controlMaterial, *_controller,
            *_primalSolver->getConverter(),
            n, _alphaDim);
        _projection = proj_ptr;
        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
      } else if (std::regex_match(_projectionName, match, std::regex ("(Stasius)([0-9])"))) {
        int n = std::stoi(match[2]);
        clout << "Creating Projection Stasius" << match[2] << ": a^" << 2.0*n << "" << std::endl;
        GiProjection3D<S, Lattice>* proj_ptr = new StasiusNGiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
            *_primalSolver->getGeometry(),
            _controlMaterial, *_controller,
            *_primalSolver->getConverter(),
            n, _alphaDim);
        _projection = proj_ptr;
        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
      }
//
//      else if (_projectionName == "Foerster2") {
//        GiProjection3D<S, Lattice>* proj_ptr = new Foerster2GiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
//                                                                                       *_primalSolver->getGeometry(),
//                                                                                       _controlMaterial, *_controller,
//                                                                                       *_primalSolver->getConverter(),
//                                                                                       _alphaDim);
//        _projection = proj_ptr;
//        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
//      }
//      else if (_projectionName == "Foerster3") {
//        GiProjection3D<S, Lattice>* proj_ptr = new Foerster3GiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
//                                                                                       *_primalSolver->getGeometry(),
//                                                                                       _controlMaterial, *_controller,
//                                                                                       *_primalSolver->getConverter(),
//                                                                                       _alphaDim);
//        _projection = proj_ptr;
//        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
//      }
//      else if (_projectionName == "Foerster4") {
//        GiProjection3D<S, Lattice>* proj_ptr = new Foerster4GiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
//                                                                                       *_primalSolver->getGeometry(),
//                                                                                       _controlMaterial, *_controller,
//                                                                                       *_primalSolver->getConverter(),
//                                                                                       _alphaDim);
//        _projection = proj_ptr;
//        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
//      }
//      else if (_projectionName == "Stasius") {
//        GiProjection3D<S, Lattice>* proj_ptr = new StasiusGiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
//                                                                                      *_primalSolver->getGeometry(),
//                                                                                      _controlMaterial, *_controller,
//                                                                                      *_primalSolver->getConverter(),
//                                                                                      _alphaDim);
//        _projection = proj_ptr;
//        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
//      }
//      else if (_projectionName == "Stasius2") {
//        GiProjection3D<S, Lattice>* proj_ptr = new Stasius2GiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
//                                                                                     *_primalSolver->getGeometry(),
//                                                                                     _controlMaterial, *_controller,
//                                                                                     *_primalSolver->getConverter(),
//                                                                                     _alphaDim);
//        _projection = proj_ptr;
//        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
//      }
//      else if (_projectionName == "Stasius3") {
//        GiProjection3D<S, Lattice>* proj_ptr = new Stasius3GiProjection3D<S, Lattice>(*_primalSolver->getLattice(),
//                                                                                      *_primalSolver->getGeometry(),
//                                                                                      _controlMaterial, *_controller,
//                                                                                      *_primalSolver->getConverter(),
//                                                                                      _alphaDim);
//        _projection = proj_ptr;
//        _dProjectionDcontrol = new DGiProjectionDAlpha3D<S, Lattice>(*proj_ptr);
//      }


      AnalyticalFfromSuperF3D<S> *_analyticalAlpha = new AnalyticalFfromSuperF3D<S>(*_projection);
      _primalSolver->addExternalField(_analyticalAlpha);
      _dualSolver->addExternalField(_analyticalAlpha);
    }

  }
  clout << "Reinit ... OK" << std::endl;
}

template<typename S, typename T, template<typename U> class Lattice>
void OptiCaseDual<S,T,Lattice>::free()
{
  delete _controller;
  _controller = nullptr;
  delete _primalSolver;
  _primalSolver = nullptr;
  delete _dualSolver;
  _dualSolver = nullptr;
}

template<typename S, typename T, template<typename U> class Lattice>
S OptiCaseDual<S,T,Lattice>::evaluateObjectFunctional(S *_control)
{
  //project _control to enforce volume constraint
#ifdef mathiasProjection //TODO To be adapted to the new structure

  const int nx = _dualSolver->getGeometry()->getCuboidGeometry().getMotherCuboid().getNx();
  const int ny = _dualSolver->getGeometry()->getCuboidGeometry().getMotherCuboid().getNy();
  const int nz = _dualSolver->getGeometry()->getCuboidGeometry().getMotherCuboid().getNz();

  T pi = 4.0 * atan(1.0);

  T totalD = T();
  T wantedTotalD = T();

  T upperBound, lowerBound, volumeRatio;
  (*_xml)["Optimization"]["UpperBound"].read(upperBound);
  (*_xml)["Optimization"]["LowerBound"].read(lowerBound);
  (*_xml)["Optimization"]["VolumeRatio"].read(volumeRatio);

  for (int iX=1; iX<nx-1; iX++) {
    for (int iY=1; iY<ny-1; iY++) {
      for (int iZ=1; iZ<nz-1; iZ++) {
        if (_dualSolver->getGeometry()->getMaterial(iX,iY,iZ)==6) {

          T control = _control[(iX-1)*(ny-1)*(nz-1)+(iY-1)*(nz-1)+(iZ-1)];
          totalD += 1./(upperBound-lowerBound)*(control-1.);
          wantedTotalD += 1.0;
        }
      }
    }
  }

  for (int iX=1; iX<nx-1; iX++) {
    for (int iY=1; iY<ny-1; iY++) {
      for (int iZ=1; iZ<nz-1; iZ++) {
        if (_dualSolver->getGeometry()->getMaterial(iX,iY,iZ)==6) {
          _control[(iX-1)*(ny-1)*(nz-1)+(iY-1)*(nz-1)+(iZ-1)]  = lowerBound +
              (_control[(iX-1)*(ny-1)*(nz-1)+(iY-1)*(nz-1)+(iZ-1)]
               - lowerBound)
              * wantedTotalD * (1.-volumeRatio) / totalD;
        }
      }
    }
  }
#endif

  _controller->setControl(_control, _dim);
  //_primalProblem->getSolver().loadBackup("backup");
  //_primalProblem->reInit(_controller);
  //_primalSolver->initVelocity();
  //_primalSolver->initForce();
  _primalSolver->solve();
  //_primalProblem->getSolver().saveBackup("backup");

  // TODO: Warum wird input nicht sinnvoll gefüllt?
  int input[3];
  S output[1];
  (*_objective)(output, input);
  S value = output[0];
  util::print(value, "objective", clout);
  return value;
}


template<typename S, typename T, template<typename U> class Lattice>
void OptiCaseDual<S,T,Lattice>::computeDerivatives(S *_control, S *_derivatives)
{
#ifndef ADT
  clout << "### Compute Derivatives ..." << std::endl;

  _controller->setControl(_control, _dim);

  // Init external field for primal solution fPop
  SuperLatticeFpop3D<S, Lattice> fPop(*_primalSolver->getLattice());

  clout << "Define External Fields..." << std::endl;
  _dualSolver->getLattice()->defineExternalField(*_primalSolver->getGeometry(), 1,
      Lattice<S>::ExternalField::fBeginsAt,
      Lattice<S>::ExternalField::sizeOfF, fPop);
  _dualSolver->getLattice()->defineExternalField(*_primalSolver->getGeometry(), _controlMaterial,
      Lattice<S>::ExternalField::fBeginsAt,
      Lattice<S>::ExternalField::sizeOfF, fPop);

  // Init external field for dJdF
  _dualSolver->getLattice()->defineExternalField(*_primalSolver->getGeometry(), 1,
      Lattice<S>::ExternalField::dJdFBeginsAt,
      Lattice<S>::ExternalField::sizeOfdJdF, *_dObjectiveDf);
  _dualSolver->getLattice()->defineExternalField(*_primalSolver->getGeometry(), _controlMaterial,
      Lattice<S>::ExternalField::dJdFBeginsAt,
      Lattice<S>::ExternalField::sizeOfdJdF, *_dObjectiveDf);

  // Init external field for dJdAplpha
  _dualSolver->getLattice()->defineExternalField(*_primalSolver->getGeometry(), _controlMaterial,
      Lattice<S>::ExternalField::dJdAlphaBeginsAt,
      Lattice<S>::ExternalField::sizeOfdJdAlpha,
      *_dObjectiveDcontrol);

  clout << "Define External Fields... OK" << std::endl;

  //_db->modify<std::string>("BoundaryFunction0","Type","DualForcedBGK");

  //_controller->setPopulations(&_primalProblem->getSolver().getAnalysis3D().getPopulations() );
  //_controller->setPressure(&_primalProblem->getSolver().getAnalysis3D().getPressure() );
  //_controller->setVelocity(&_primalProblem->getSolver().getAnalysis3D().getVelocity() );
  //_controller->setWantedVelocity(tensorField );

  clout << "Solve Dual Problem..." << std::endl;
  _dualSolver->solve();
  clout << "Solve Dual Problem... OK" << std::endl;


//#ifdef forceMode
  if (_controlType == "force") {
    S regAlpha;
    (*_xml)["Optimization"]["RegAlpha"].read(regAlpha);

    int nC = _primalSolver->getGeometry()->getCuboidGeometry().getNc();
    int latticeR[4];
    for (int iC=0; iC<nC; iC++) {
      latticeR[0] = iC;
      int nX = _primalSolver->getGeometry()->getCuboidGeometry().get(iC).getNx();
      int nY = _primalSolver->getGeometry()->getCuboidGeometry().get(iC).getNy();
      int nZ = _primalSolver->getGeometry()->getCuboidGeometry().get(iC).getNz();
      for (int iX=0; iX<nX; iX++) {
        latticeR[1] = iX;
        for (int iY=0; iY<nY; iY++) {
          latticeR[2] = iY;
          for (int iZ=0; iZ<nZ; iZ++) {
            latticeR[3] = iZ;
            std::vector<int> latticeRv(latticeR, latticeR + 4);
            if (_primalSolver->getGeometry()->getAndCommunicate(latticeRv) == _controlMaterial) {
              std::vector<S> derivatives(_alphaDim, S());
              if (_primalSolver->getGeometry()->getLoadBalancer().rank(iC)  == singleton::mpi().getRank()) {
                S rho = _primalSolver->getLattice()->get(latticeRv).computeRho();
                T dObjectiveDcontrol[_alphaDim];
                (*_dObjectiveDcontrol)(dObjectiveDcontrol,latticeR);
                T dProjectionDcontrol[_alphaDim];
                (*_dProjectionDcontrol)(dProjectionDcontrol,latticeR);
                for (int iDim=0; iDim<_alphaDim; iDim++) {
                  for (int jPop=0; jPop < Lattice<T>::q; ++jPop) {
//                    std::vector<int> latticeRv(latticeR, latticeR+4);
                    S phi_j = _dualSolver->getLattice()->get(latticeRv)[jPop];
                    derivatives[iDim] -= rho * Lattice<T>::t[jPop] * Lattice<T>::invCs2() * Lattice<T>::c[jPop][iDim] * phi_j;
                  }
                  derivatives[iDim] += regAlpha * _projection->getControl(latticeR, iDim) + dObjectiveDcontrol[iDim] * dProjectionDcontrol[iDim];
                }
              }
#ifdef PARALLEL_MODE_MPI
              singleton::mpi().bCast(&derivatives[0], _alphaDim, _primalSolver->getGeometry()->getLoadBalancer().rank(iC));
#endif
              for (int iDim=0; iDim<_alphaDim; iDim++) {
                _derivatives[_projection->getIndex(latticeR,iDim)] = derivatives[iDim];
              }
            }
          }
        }
      }
    }

  }
//#endif

#ifdef porousMode
  if ( _controlType == "porosity" ) {
    S regAlpha;
    (*_xml)["Optimization"]["RegAlpha"].read(regAlpha);

    T omega = _dualSolver->getConverter()->getLatticeRelaxationFrequency();
    int nC = _primalSolver->getGeometry()->getCuboidGeometry().getNc();

    for (int iC = 0; iC < nC; iC++) {

      Vector<int,3> extend = _primalSolver->getGeometry()->getCuboidGeometry().get(iC).getExtend();

      for (int iX = 0; iX < extend[0]; iX++) {
        for (int iY = 0; iY < extend[1]; iY++) {
          for (int iZ = 0; iZ < extend[2]; iZ++) {
            std::vector<int> latticeR {iC, iX, iY, iZ};

            if (_primalSolver->getGeometry()->getAndCommunicate(latticeR) == _controlMaterial) {
              S derivative = S(0);
              if (_primalSolver->getGeometry()->getLoadBalancer().rank(iC)  == singleton::mpi().getRank()) {
                // rho
                T rho_f = _primalSolver->getLattice()->get(latticeR).computeRho();
                // u_f
                T u_f[3];
                _primalSolver->getLattice()->get(latticeR).computeU(u_f);
                // d_h
                T d = *_dualSolver->getLattice()->get(latticeR).getExternal(Lattice<T>::ExternalField::porosityIsAt);
                // DObjectiveDControl
                T dObjectiveDcontrol;
                (*_dObjectiveDcontrol)(&dObjectiveDcontrol, &latticeR[0]);
                // dProjectionDcontrol
                T dProjectionDcontrol;
                (*_dProjectionDcontrol)(&dProjectionDcontrol, &latticeR[0]);

                for (int iDim = 0; iDim < 3; iDim++) {
                  for (int jPop = 0; jPop < Lattice<T>::q; ++jPop) {
                    S phi_j = _dualSolver->getLattice()->get(latticeR)[jPop];
                    derivative -= u_f[iDim] * rho_f/omega * Lattice<T>::t[jPop] * phi_j * Lattice<T>::invCs2
                                  * ( Lattice<T>::c[jPop][iDim] - d * u_f[iDim] ) * dProjectionDcontrol;
                  }
                }
              }
#ifdef PARALLEL_MODE_MPI
              singleton::mpi().bCast(&derivative, 1, _primalSolver->getGeometry()->getLoadBalancer().rank(iC));
#endif
              _derivatives[_projection->getIndex(&latticeR[0],0)] = derivative;
            }
          }
        }
      }
    }

  }
#endif


//#ifdef porousModeXXX
  if ( _controlType == "porosityExtended" ) {
//    int N;
//    (*_xml)["Application"]["DiscParam"]["resolution"].read(N);
//
//    const int nx = _dualSolver->getGeometry()->getNx();
//    const int ny = _dualSolver->getGeometry()->getNy();
//    const int nz = _dualSolver->getGeometry()->getNz();
//
//    T pi = 4.0 * atan(1.0);
//
//    T totalD = T();
//    T wantedTotalD = T();
//
//    //project _control to enforce volume constraint
//#ifdef mathiasProjection
//
//    T upperBound, lowerBound, volumeRatio;
//    (*_xml)["Optimization"]["UpperBound"].read(upperBound);
//    (*_xml)["Optimization"]["LowerBound"].read(lowerBound);
//    (*_xml)["Optimization"]["VolumeRatio"].read(volumeRatio);
//
//    for (int iX=1; iX<nx-1; iX++) {
//      for (int iY=1; iY<ny-1; iY++) {
//        for (int iZ=1; iZ<nz-1; iZ++) {
//          if (_dualSolver->getGeometry()->getMaterial(iX,iY,iZ)==6) {
//
//            T control = _control[(iX-1)*(ny-1)*(nz-1)+(iY-1)*(nz-1)+(iZ-1)];
//            totalD += 1./(upperBound-lowerBound)*(control-1.);
//            wantedTotalD += 1.0;
//          }
//        }
//      }
//    }
//
//    for (int iX=1; iX<nx-1; iX++) {
//      for (int iY=1; iY<ny-1; iY++) {
//        for (int iZ=1; iZ<nz-1; iZ++) {
//          if (_dualSolver->getGeometry()->getMaterial(iX,iY,iZ)==6) {
//            _control[(iX-1)*(ny-1)*(nz-1)+(iY-1)*(nz-1)+(iZ-1)]  = lowerBound + (_control[(iX-1)*(ny-1)*(nz-1)+(iY-1)*(nz-1)+(iZ-1)] - lowerBound) *wantedTotalD*(1.-volumeRatio)/totalD;
//          }
//        }
//      }
//    }
//#endif
//
//
//    T obst_r_min;
//    (*_dualSolver->getParameter())["Application"]["Obstacle"]["rMax"].read(obst_r_min);
//
//    //clout << "totalD="<< totalD << std::endl;
//
//    for (int iX = 0; iX < nx; iX++) {
//      for (int iY = 0; iY < ny; iY++) {
//        for (int iZ = 0; iZ < nz; iZ++) {
//          if (_dualSolver->getGeometry()->getMaterial(iX, iY, iZ) == 6) {
//
//            T difff[3];
//            difff[0] = T();
//            difff[1] = T();
//            difff[2] = T();
//
//
//            Cell<T, Lattice> cell;
//            _dualSolver->getLattice()->get(iX, iY, iZ, cell);
//
//            //  cell.revert();
//            T nuLattice = _dualSolver->getConverter()->getLatticeNu();
//            T tau = _dualSolver->getConverter()->getTau();
//            T physLength = _dualSolver->getConverter()->physLength();
//            int dim = _dualSolver->getConverter()->getDim();
//            T latticeForce = 1. / _dualSolver->getConverter()->physMasslessForce();
//            T deltaU = _dualSolver->getConverter()->getLatticeU();
//            T deltaX = _dualSolver->getConverter()->getDeltaX();
//            T nu = _dualSolver->getConverter()->getCharNu();
//            T omega = _dualSolver->getConverter()->getLatticeRelaxationFrequency();
//            T d = *(cell.getExternal(Lattice<T>::ExternalField::porosityIsAt));
//            T control = _control[(iX - 1) * (ny - 1) * (nz - 1) + (iY - 1) * (nz - 1) + (iZ - 1)];
//            d = pow(1. / 2. + 1. / 2. * sin((control - 1. / 2.) * pi), 2.);
//            T dDiff = /*1.;*/ (1. / 2. + 1. / 2. * sin((control - 1. / 2.) * pi)) * cos((control - 1. / 2.) * pi) * pi;
//#ifdef simonProjection
//            T ansatzDim = 4.;
//            d = pow( control,ansatzDim)/( pow( control,ansatzDim) + tau * nuLattice * pow(physLength,T(dim-1)) );
//            dDiff = (ansatzDim * tau * nuLattice * pow(physLength,T(dim-1)) * pow( control,(ansatzDim-1.)))/(pow(pow( control,ansatzDim) + tau * nuLattice * pow(physLength,T(dim-1)),2.));
//#endif
//
//#ifdef mathiasProjection
//            d =  1 - 1./pow( 2.71, pow(control, 2.));
//            dDiff = 2.* control*1./pow( 2.71, pow(control, 2.));
//#endif
//
//
//            //cell.defineDynamics(this->_bulkDynamics );
//
//            T *pop_f = cell.getExternal(Lattice<T>::ExternalField::fBeginsAt);
//
//            T rho_f;
//            T u_f[3];
//            rho_f = T();
//            for (int iD = 0; iD < Lattice<T>::d; ++iD) {
//              u_f[iD] = T();
//            }
//            for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
//              rho_f += pop_f[iPop];
//              for (int iD = 0; iD < Lattice<T>::d; ++iD) {
//                u_f[iD] += pop_f[iPop] * Lattice<T>::c(iPop)[iD];
//              }
//            }
//            //  rho_f += (T)1;
//            for (int iD = 0; iD < Lattice<T>::d; ++iD) {
//              u_f[iD] /= rho_f;
//            }
//            //   rho_f += (T)1;
//
//
//
//
//            //       cout <<"rho_f+=pop_f[iPop];: "<< rho_f << endl;
//
//            for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
//              for (int iD = 0; iD < Lattice<T>::d; ++iD) {
//                difff[0] -= u_f[iD] / omega * Lattice<T>::t[iPop] * cell[iPop] * Lattice<T>::invCs2() *
//                            (Lattice<T>::c(iPop)[iD] - d * u_f[iD]) * rho_f * dDiff;
//              }
//              /*
//               difff[0] += rho_f*Lattice<T>::t[iPop]*Lattice<T>::invCs2()*Lattice<T>::c(iPop)[0]*cell[iPop];
//               difff[1] += rho_f*Lattice<T>::t[iPop]*Lattice<T>::invCs2()*Lattice<T>::c(iPop)[1]*cell[iPop];
//               difff[2] += rho_f*Lattice<T>::t[iPop]*Lattice<T>::invCs2()*Lattice<T>::c(iPop)[2]*cell[iPop];
//              */
//            }
//            T *dJdAlpha = cell.getExternal(Lattice<T>::ExternalField::dJdAlphaBeginsAt);
//
//
//            T regAlpha;
//            (*_xml)["Optimization"]["RegAlpha"].read(regAlpha);
//            T penaltyWeight;
//            (*_xml)["Optimization"]["penaltyWeight"].read(penaltyWeight);
//
//
//            T ansatz3 = 2.;
//
//            if (_dualSolver->getGeometry()->getMaterial(iX, iY, iZ) == 6) {
//              //     clout << iX <<"/"<< iY <<"/"<< iZ <<"/"<< control <<"/"<<d<<endl;
//
//              T ansatz5 = 4;
//              T control = _control[(iX - 1) * (ny - 1) * (nz - 1) + (iY - 1) * (nz - 1) + (iZ - 1)];
//
//              _derivatives[(iX - 1) * (ny - 1) * (nz - 1) + (iY - 1) * (nz - 1) + (iZ - 1)]
//                  = /*+regAlpha*control*/
//                  +(nx - 1) * (ny - 1) * (nz - 1) * difff[0]
//                  + (nx - 1) * (ny - 1) * (nz - 1) * dJdAlpha[0];
//              //+ 13*(ny-1)*(nz-1)*penaltyWeight*ansatz3*pow((totalD-wantedTotalD*0.5)/wantedTotalD/0.5,ansatz3-1)/wantedTotalD/0.5*ansatz5*pow(5.*(control-0.05), ansatz5-1 )*5.;
//            }
//            else {
//              _derivatives[(iX - 1) * (ny - 1) * (nz - 1) + (iY - 1) * (nz - 1) + (iZ - 1)] = 0;
//            }
//
//          }
//        }
//      }
//    }
  }

//#endif
  clout << "### Compute Derivatives ... OK" << std::endl;

#endif // no ADT
}

template<typename S, typename T, template<typename U> class Lattice>
void OptiCaseDual<S,T,Lattice>::increaseOptiStep()
{
  _primalSolver->increaseOptiStep();
}


template<typename S, typename T, template<typename U> class Lattice>
T OptiCaseDual<S,T,Lattice>::permeabilityToPorosity(T permeability)
{
  OLB_PRECONDITION(permeability >= pow( _primalSolver->getConverter()->getPhysDeltaX(), this->_dim - 1 ) * _primalSolver->getConverter()->getLatticeViscosity() * _primalSolver->getConverter()->getLatticeRelaxationTime());
  return (1 - pow( _primalSolver->getConverter()->getPhysDeltaX(), 2 /*WARNING: dim-1*/ ) * _primalSolver->getConverter()->getLatticeViscosity() * _primalSolver->getConverter()->getLatticeRelaxationTime() / permeability);
}

} // namespace opti

} // namespace olb

#endif
