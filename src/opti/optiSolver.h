/*  This file is part of the OpenLB library
*
*  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

/** \file
* An Optimization Solver connecting an Optimizer with a specific OptiCase
*/


#ifndef OPTI_SOLVER_H
#define OPTI_SOLVER_H

#include "io/xmlReader.h"

#include "optiCase.h"
#include "optiCaseAD.h"
#include "optiCaseADTest.h"
#include "optiCaseDual.h"

#include "optimizer.h"
#include "optimizerLBFGS.h"
#include "optimizerSteepestDecent.h"


namespace olb {

namespace opti {



/// class to create and hold an instance of Optimizer (to perform optimize())
/// and an instance of a specific OptiCase (to evaluate the functional and it's derivative)
/// depending on what's in xml
template<typename S, typename T, template<typename U> class Lattice>
class OptiSolver {

private:
  XMLreader *_xml;
  OptiCase <S> *_optiCase;
  Optimizer <S> *_optimizer;

  OstreamManager clout;

public:
  /// Construction of an OptiSolver
  OptiSolver() : clout(std::cout, "OptiSolver")
  {
    _xml = nullptr;
    _optimizer = nullptr;
    _optiCase = nullptr;
  };

  /// Construction of an OptiSolver from xml
  OptiSolver(XMLreader *xml) : clout(std::cout, "OptiSolver")
  {
    _xml = nullptr;
    _optimizer = nullptr;
    _optiCase = nullptr;
    reInit(xml);
  };

  /// Destructor of an OptiSolver
  ~OptiSolver()
  {
    freeOptiCase();
    freeOptimizer();
  };

  /// Reinitialization of an OptiSolver from xml
  void reInit(XMLreader *xml)
  {
    _xml = xml;
    reInit();
  };

  /// Reinitialization of OptiSolver's subclasses OptiCase and Optimizer from xml, happens automatically on reInit(xml)
  void reInit()
  {
    if (_xml != nullptr) {
      reInitOptiCase(_xml);
      reInitOptimizer(_xml);
    } else {
      clout << " Error: OptiSolver.reInit(), _xml == nullptr" << std::endl;
    }
  };

  /// deletes optiCase, happens on destruction
  void freeOptiCase()
  {
    delete _optiCase;
    _optiCase = nullptr;
  };

  /// deletes optimizer, happens on destruction
  void freeOptimizer()
  {
    delete _optimizer;
    _optimizer = nullptr;
  };

  /// reinitialized OptiCase from a given xml, should happen automatically
  void reInitOptiCase(XMLreader *xml)
  {
    clout << "### reInitOptiCase started..." << std::endl;
    freeOptiCase();
    std::string optModeName;
    (*xml)["Optimization"]["Mode"].read(optModeName);
    std::string test;
    (*xml)["Opti"]["test"].read(test);
    if (optModeName == "ad") {
      _optiCase = new OptiCaseAD<S, T, Lattice>(_xml);
    } else if (optModeName == "dual") {
      _optiCase = new OptiCaseDual<S, T, Lattice>(_xml);
    } else if (optModeName == "adTest") {
      _optiCase = new OptiCaseADTest<S, T>(_xml);
    } else {
      clout << "ERROR: No valid Optimization Mode specified" << std::endl;
      clout << "       possible values are: ad, dual, adTest" << std::endl;
      clout << "Simulation aborted" << std::endl;
      exit(-1);
    }
    clout << "### reInitOptiCase started... OK" << std::endl;
  };

  /// reinitialized Optimizer from a given xml, should happen automatically
  void reInitOptimizer(XMLreader *xml)
  {
    freeOptimizer();

    xml->setWarningsOn(false);
    std::string optAlgoName = "LBFGS";
    readOptiXMLOrWarn(clout, *xml, "Algorithm", optAlgoName);
    xml->setWarningsOn(true);

    if (optAlgoName == "SteepestDescent") {
      _optimizer = createOptimizerSteepestDescent<S>(*xml);
    }
    if (optAlgoName == "LBFGS") {
      _optimizer = createOptimizerLBFGS<S>(*xml);
    }
    if ( _optiCase->getType() == "Dual") {
      clout << "##################DUAL MODE#####################" << endl;
//      clout << "##################Type: " << _optimizer->_startValueType << "#####################" << endl;
//      // transform startValue if Permeability is given
//      if( _optimizer->_startValueType == "Permeability") {
//        clout << "##################PERM MODE#####################" << endl;
//        clout << "Transforming Permeability startValue into Porosity startValue..." << std::endl;
//        clout << "Old StartValue: " << _optimizer->getStartValue() << std::endl;
//        _optimizer->setStartValue(_optiCase->permeabilityToPorosity(_optimizer->getStartValue()));
//        clout << "New StartValue: " << _optimizer->getStartValue() << std::endl;
//      }
    }
  };

  void solve()
  {
    clout << endl;
    clout << "===========================" << std::endl;
    clout << "OptiCase Solve()..." << std::endl;
    clout << "===========================" << std::endl;
    clout << endl;
    _optimizer->optimize(_optiCase);
    clout << "===========================" << std::endl;
    clout << "OptiCase Solve()... OK" << std::endl;
    clout << "===========================" << std::endl;
  };
};

}

}

#endif
