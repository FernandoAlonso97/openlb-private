/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of optimization algorithms -- header file.
 */


#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include "norm.hh"
#include "optiCase.h"
#include "communication/mpiManager.h"
#include "io/xmlReader.h"
#include "core/singleton.h"


namespace olb {

/// \namespace opti Optimization Code.
namespace opti {


template<typename T> class OptiCase;


/// This wrapper function reads the given parameter from the "Optimization" tag and prints a warning containing
/// the default value. Will exit(1) if exitIfMissing == true.
template<typename ParameterType>
bool readOptiXMLOrWarn(OstreamManager& clout, XMLreader const& params, string parameter,
                       ParameterType& var, bool exitIfMissing=false)
{
  if (!params["Optimization"][parameter].read<ParameterType>(var, false)) {
    clout << "Warning: Cannot read parameter from XML File: " << parameter << endl;
    if ( exitIfMissing ) {
      clout << "Error: This program cannot continue without \"" << parameter << "\". Optimization aborted.";
      exit(1);
    }
    clout << "\t Setting default value: " << var << endl;
    return false;
  }
  return true;
};


/// Interface for the use of various optimization algorithms.
/**
 * An Optimizer provides an interface for the use of various
 * optimization algorithms for an OptiCase.
 *
 * This class is intended to be derived from.
 */

template<typename T>
class Optimizer {

private:
  mutable OstreamManager clout;

protected:
  /// Number of controlled variables
  int                                 _dim;
  /// Constant initial value
  T                                   _startValue;
  /// Vector of controlled variables (size _dim)
  T*                                  _control;
  /// Value of the objective functional evaluated
  /// for controlled variables saved in _control
  T                                   _value;
  /// Vector of derivatives of the object functional
  /// with respect to the controlled variables
  T*                                  _derivative;
  /// Current iteration no.
  int                                 _it;
  /// Maximal number of iteration
  int                                 _maxIt;
  /// Optimizer stops if |_derivatives| < _eps
  T                                   _eps;
  /// Verbose
  bool                                _verboseOn;

  /// Bounded versions
  bool                                _withUpperBound;
  bool                                _withLowerBound;
  T*                                  _boundedControl;
  T                                   _upperBound;
  T                                   _lowerBound;

  /// Provides the Optimizer with methods to evaluate the
  /// value of an object functional and compute derivatives
  OptiCase<T>*                        _optiCase;

public:
  /// Initial value type (permeability has to be transformed into porosity)
  std::string                         _startValueType;

public:
  /// Construction of an Optimizer
  Optimizer(int dim, T eps, int maxIt, T startValue=0.0, bool verboseOn=true, const std::string fname="",
            bool withUpperBound=false, T upperBound=T(), bool withLowerBound=false, T lowerBound=T(),
            std::string startValueType="Porosity")
    : clout(std::cout,"Optimizer"), _dim(dim), _startValue(startValue),
      _it(0), _maxIt(maxIt), _eps(eps), _verboseOn(verboseOn),
      _withUpperBound(withUpperBound), _withLowerBound(withLowerBound),
      _upperBound(upperBound), _lowerBound(lowerBound), _startValueType(startValueType)
  {
    _control = new T [_dim];
    _derivative = new T [_dim];
    if (_withUpperBound||_withLowerBound) {
      _boundedControl = new T [_dim];
    }

    for (int iDim=0; iDim<_dim; iDim++) {
      _control[iDim] = startValue;
      _derivative[iDim] = T();
    }
    this->read(fname);
  };

  /// Virtual Destructor for defined behaviour
  virtual ~Optimizer()
  {

  };

  /// Optimizes a problem given by OptiCase
  virtual void optimizationStep() = 0;

  /// Optimizes a problem given by OptiCase
  virtual void optimize(OptiCase<T>* optiCase)
  {
    _optiCase = optiCase;
    // Evaluate first object functional
    _value = _optiCase->evaluateObjectFunctional(_control);

    // Optimize as long as |derivative| > eps or a maximum of iterations is reached
    do {
      // Optimization step (update of _control, _value, _derivatives and _it)
      this->write();
      _optiCase->increaseOptiStep();
      optimizationStep();
      // Print info
      if (_verboseOn) {
        this->print(_it);
      }
    } while (euklidN(_derivative, _dim) > _eps && _it < _maxIt);
  };

  /// Prints information of the actual optimization step it
  void print(int it)
  {
    clout << "=======================================" << std::endl;
    clout << ">>>>>>>>>> Optimizer: step " << it << " <<<<<<<<<<" << std::endl;
    clout << "   Value objective = " << _value << std::endl;
    clout << "   Norm derivative = " << euklidN(_derivative, _dim) << std::endl;
    //clout << "   control={ ";
    //for (int i=0; i<_dim; i++) {
    //clout << _control[i] << " ";
    //}
    //clout << "}" << std::endl;
    //clout << std::endl;
    clout << "=======================================" << std::endl;
  };

  /// Writes the current control variables linewise into file fname
  void write(const std::string fname="control.dat")
  {
    if (singleton::mpi().isMainProcessor() ) {
      std::ofstream file;
      file.open(singleton::directories().getLogOutDir() + fname.c_str());
      file << _dim << std::endl;
      for (int i=0; i<_dim; i++) {
        file << _control[i] << std::endl;
      }
      file.close();
    }
  };

  /// Reads the latest control variables from file fname
  void read(const std::string fname="control.dat")
  {
    std::ifstream file(fname.c_str());
    if ( file.is_open() ) {
      int dim;
      file >> dim;
      if (dim!=_dim) {
        clout << "Error: dimensions do not match! dim_controller=" << _dim << "; dim_file=" << dim << std::endl;
        assert(false);
      } else {
        for (int i=0; i<_dim; i++) {
          double tmpVal;
          file >> tmpVal;
          _control[i]  = tmpVal;
        }
      }
      file.close();
    }
  };

  /// Sets the start value after initialization
  void setStartValue(T startValue)
  {
    _startValue = startValue;
    // update control variables
    for (int iDim=0; iDim<_dim; iDim++) {
      _control[iDim] = _startValue;
    }
  }

  /// Getter for the start value
  T getStartValue()
  {
    return _startValue;
  }
};

} // namespace opti

} // namespace olb

#endif
