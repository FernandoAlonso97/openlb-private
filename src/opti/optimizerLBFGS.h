/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of the LBFGS optimization algorithm -- header file.
 */


#ifndef OPTIMIZER_LBFGS_H
#define OPTIMIZER_LBFGS_H

#include "optimizerLineSearch.h"


/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {


/// Optimization algorithm: LBFGS.
/** OptimizerSteepestDescent optimizes an optimization problem
 * which is given in OptiCase. OptiCase provides therefore
 * methods to evaluate an object functional and compute derivatives.
 * LBFGS searches along the direction of the derivative to find
 * a lower value of the evaluated object functional.
 *
 * This class is not intended to be derived from.
 */

template<typename T>
class OptimizerLBFGS : public OptimizerLineSearch<T> {

private:
  int _l;

  T _startCoefH;

  T** _sStore;
  T** _yStore;
  T* _rhoStore;
  T* _alpha;

  int _firstStore;
  int _lastStore;

  T* _lastControl;
  T* _lastDerivative;

  mutable OstreamManager clout;

public:
  OptimizerLBFGS(int dim, T eps, int maxIt, T startValue, int l, T startCoefH, bool verboseOn=true,
                 const std::string fname="", bool withUpperBound=false, T upperBound=T(),
                 bool withLowerBound=false, T lowerBound=T(), std::string startValueType="Porosity")
    : OptimizerLineSearch<T>(dim, eps, maxIt, startValue, verboseOn, fname, withUpperBound, upperBound,
                             withLowerBound, lowerBound, startValueType),
      clout(std::cout,"OptimizerLBFGS")
  {
    _l=l;
    _startCoefH = startCoefH;

    _firstStore = 1;
    _lastStore = 1;

    _lastControl = new T [this->_dim];
    _lastDerivative = new T [this->_dim];

    _sStore = new T* [_l];
    _yStore = new T* [_l];
    _rhoStore = new T [_l];

    _alpha = new T [_l];

    for (int i=0; i<_l; i++) {
      _sStore[i] = new T [this->_dim];
      _yStore[i] = new T [this->_dim];
    }
  };

  void setL(int l)
  {
    _l=l;
  };

  void setStartCoefH( T startCoefH)
  {
    _startCoefH = startCoefH;
  };

  void storeHelpers()
  {

    if (this->_it!=0) {
      _rhoStore[(this->_it)%_l] = 0;
      T temp1 = T();
      T temp2 = T();
      for (int iDim=0; iDim<this->_dim; iDim++) {
        _sStore[(this->_it)%_l][iDim] = this->_control[iDim]-_lastControl[iDim];
        _yStore[(this->_it)%_l][iDim] = this->_derivative[iDim]-_lastDerivative[iDim];
        _rhoStore[(this->_it)%_l] += _yStore[(this->_it)%_l][iDim]*_sStore[(this->_it)%_l][iDim];
        temp1 += _sStore[(this->_it)%_l][iDim]*_yStore[(this->_it)%_l][iDim];
        temp2 += _yStore[(this->_it)%_l][iDim]*_yStore[(this->_it)%_l][iDim];
      }
      _startCoefH = temp1/temp2;

      _rhoStore[(this->_it)%_l] = 1./_rhoStore[(this->_it)%_l];
      if (this->_it>_l) {
        _firstStore = (_firstStore+1)%_l;
      }
      _lastStore = (this->_it)%_l;
    }

    for (int iDim=0; iDim<this->_dim; iDim++) {
      _lastControl[iDim] = this->_control[iDim];
      _lastDerivative[iDim] = this->_derivative[iDim];
    }
  }

  virtual void computeDirection()
  {

    // Update _derivative
    this->_optiCase->computeDerivatives(this->_control, this->_derivative);

    if (this->_it==0) {
      T normDerivative = euklidN(this->_derivative, this->_dim);
      for (int iDim=0; iDim<this->_dim; iDim++) {
        this->_direction[iDim] = this->_derivative[iDim] / normDerivative;
      }
      storeHelpers();
    } else {
      // unused variable
      //T normDerivative = euklidN(this->_derivative, this->_dim);
      storeHelpers();

      for (int iDim=0; iDim<this->_dim; iDim++) {
        this->_direction[iDim] = this->_derivative[iDim];
      }

      int iMax = _l;
      if (this->_it<_l) {
        iMax = this->_it;
      }
      int iStore;
      for (int i=0; i<iMax; i++) {
        iStore = (_lastStore + _l - i) % _l;

        _alpha[iStore] = 0;
        for (int iDim=0; iDim<this->_dim; iDim++) {
          _alpha[iStore] += _sStore[iStore][iDim]*this->_direction[iDim];
        }
        _alpha[iStore] *= _rhoStore[iStore];

        for (int iDim=0; iDim<this->_dim; iDim++) {
          this->_direction[iDim] -= _alpha[iStore]*_yStore[iStore][iDim];
        }
      }
      for (int iDim=0; iDim<this->_dim; iDim++) {
        this->_direction[iDim] *= _startCoefH;
      }

      for (int i=0; i<iMax; i++) {
        iStore = (_firstStore + _l + i) % _l;

        T beta = 0;
        for (int iDim=0; iDim<this->_dim; iDim++) {
          beta += _yStore[iStore][iDim]*this->_direction[iDim];
        }
        beta *= _rhoStore[iStore];

        for (int iDim=0; iDim<this->_dim; iDim++) {
          this->_direction[iDim] += _sStore[iStore][iDim]*(_alpha[iStore]-beta);
        }
      }
    }
  };
};



/// Creator Function for LBFGS
template<typename T>
OptimizerLBFGS<T>* createOptimizerLBFGS(XMLreader const& params)
{
  OstreamManager clout(std::cout, "createOptimizerLBFGS");

  // create variables with default values
  int dim;
  int maxIt = 100;
  int l = 20;

  T eps = T(1.e-10);
  T startValue= 0.;
  std::string startValueType = "Porosity";
  T startCoefH = T(1e-4);

  bool verboseOn=true;
  std::string fname;

  bool withUpperBound = false;
  T upperBound = T();
  bool withLowerBound = false;
  T lowerBound = T();

  // deactivate default warnings and show default values instead
  params.setWarningsOn(false);

  // Read Values from XML File
  readOptiXMLOrWarn<int>(clout, params, "Dimension", dim, true);
  readOptiXMLOrWarn<int>(clout, params, "MaxIter", maxIt);
  readOptiXMLOrWarn<int>(clout, params, "L", l);

  readOptiXMLOrWarn<T>(clout,   params, "Tolerance", eps);
  readOptiXMLOrWarn<T>(clout, params, "StartValue", startValue);
//  readOptiXMLOrWarn<std::string>(clout, params, "StartValueType", startValueType);
  readOptiXMLOrWarn<T>(clout, params, "StartCoefH", startCoefH);

  readOptiXMLOrWarn<bool>(clout, params, "Verbose", verboseOn);
  readOptiXMLOrWarn<std::string>(clout, params, "InputFileName", fname);

  if ( readOptiXMLOrWarn<T>(clout, params, "UpperBound", upperBound) ) {
    withUpperBound = true;
  } else {
    clout << "\t-> setting withUpperBound = false" << endl;
  }
  if ( readOptiXMLOrWarn<T>(clout, params, "LowerBound", lowerBound) ) {
    withLowerBound = true;
  } else {
    clout << "\t-> setting withLowerBound = false" << endl;
  }

  // turn default warnings on again
  params.setWarningsOn(true);

  // Create Optimizer Object
  return new OptimizerLBFGS<T>(dim, eps, maxIt, startValue, l, startCoefH, verboseOn, fname, withUpperBound, upperBound, withLowerBound, lowerBound, startValueType);
}


} // namespace opti

} // namespace olb

#endif
