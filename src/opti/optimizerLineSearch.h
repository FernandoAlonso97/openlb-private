/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of line search optimization algorithm -- header file.
 */


#ifndef OPTIMIZER_LINE_SEARCH_H
#define OPTIMIZER_LINE_SEARCH_H

#include "optimizer.h"

/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {

/// Optimization algorithm: LineSearch.
/** OptimizerLineSearch optimizes an optimization problem
 * which is given in OptiCase. OptiCase provides therefore
 * methods to evaluate an object functional and compute derivatives.
 * LineSearch searches along a particular direction to find
 * a lower value of the evaluated object functional.
 *
 * This class is intended to be derived from.
 */

template<typename T>
class OptimizerLineSearch : public Optimizer<T> {

private:
  mutable OstreamManager clout;

protected:
  /// Search direction
  T*                                  _direction;

public:
  /// Construction of an OptimizerLineSearch
  OptimizerLineSearch(int dim, T eps, int maxIt, T startValue=0.0, bool verboseOn=true, const std::string fname="", bool withUpperBound=false, T upperBound=T(), bool withLowerBound=false, T lowerBound=T(), std::string startValueType="Porosity")
    : Optimizer<T>(dim, eps, maxIt, startValue, verboseOn, fname, withUpperBound, upperBound, withLowerBound,
                   lowerBound, startValueType),
      clout(std::cout,"OptimizerLineSearch")
  {
    _direction = new T [this->_dim];
  };

  virtual void computeDirection() = 0;

  /// Optimization step: line search
  virtual void optimizationStep()
  {

    // Compute search direction
    clout << "Computing directions..." << std::endl;
    computeDirection();

    // Search on line to find step length
    T lamda = 1.;
    for (int iDim=0; iDim<this->_dim; iDim++) {
      // L: added
      //clout << "<<<<<<<<<< direction[" << iDim << "]=" << _direction[iDim] << " <<<<<<<<<<" << std::endl;
      this->_control[iDim] -= lamda*_direction[iDim];

      if (this->_withUpperBound) {
        if (this->_control[iDim] > this->_upperBound ) {
          this->_boundedControl[iDim] =this->_upperBound;
        } else {
          this->_boundedControl[iDim] = this->_control[iDim];
        }
      }
      if (this->_withLowerBound) {
        if (this->_control[iDim] < this->_lowerBound ) {
          this->_boundedControl[iDim] =this->_lowerBound;
        } else {
          this->_boundedControl[iDim] = this->_control[iDim];
        }
      }
    }
    // L: added

    clout << "[Step " << this->_it << "] <<<<<<<<<< lambda=" << lamda << " <<<<<<<<<<" << std::endl;

    T tempValue = T();
    if (this->_withUpperBound||this->_withLowerBound) {
      tempValue = this->_optiCase->evaluateObjectFunctional(this->_boundedControl);
    } else {
      tempValue = this->_optiCase->evaluateObjectFunctional(this->_control);
    }

    int refinementStep = 0;
    while (tempValue > this->_value || std::isnan(tempValue)) {
      refinementStep++;

      T newLamda = T();

      if (std::isnan(tempValue) ) {
        newLamda=lamda/4;
      } else {
        for (int iDim=0; iDim<this->_dim; iDim++) {
          newLamda += this->_derivative[iDim]*this->_direction[iDim];
          newLamda = newLamda*lamda*lamda / (2*(tempValue - this->_value + newLamda*lamda));
        }
        if (newLamda<lamda/4 ) {
          newLamda=lamda/4;
        }
      }

      for (int iDim=0; iDim<this->_dim; iDim++) {
        this->_control[iDim] -= (newLamda-lamda)*this->_direction[iDim];

        if (this->_withUpperBound) {
          if (this->_control[iDim] > this->_upperBound ) {
            this->_boundedControl[iDim] = this->_upperBound;
          } else {
            this->_boundedControl[iDim] = this->_control[iDim];
          }
        }
        if (this->_withLowerBound) {
          if (this->_control[iDim] < this->_lowerBound ) {
            this->_boundedControl[iDim] =this->_lowerBound;
          } else {
            this->_boundedControl[iDim] = this->_control[iDim];
          }
        }
      }

      clout << "[Step " << this->_it << "][Ref " << refinementStep << "] <<<<<<<<<< lambda=" << lamda << " <<<<<<<<<<" << std::endl;

      if (this->_withUpperBound||this->_withLowerBound) {
        tempValue = this->_optiCase->evaluateObjectFunctional(this->_boundedControl);
      } else {
        tempValue = this->_optiCase->evaluateObjectFunctional(this->_control);
      }

      lamda = newLamda;
    }

    if (this->_withUpperBound||this->_withLowerBound) {
      for (int iDim=0; iDim<this->_dim; iDim++) {
        this->_control[iDim] = this->_boundedControl[iDim];
      }
    }

    // Update value of the objective functional
    this->_value = tempValue;
    // Update step no.
    this->_it++;
  };
};

} // namespace opti

} // namespace olb

#endif
