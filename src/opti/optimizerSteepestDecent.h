/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of steepest decent optimization algorithm -- header file.
 */


#ifndef OPTIMIZER_STEEPEST_DECENT_H
#define OPTIMIZER_STEEPEST_DECENT_H

#include "optimizerLineSearch.h"



/// All OpenLB code is contained in this namespace.
namespace olb {

/// All optimization code is contained in this namespace.
namespace opti {

/// Optimization algorithm: SteepestDescent
/** OptimizerSteepestDescent optimizes an optimization problem
 * which is given in OptiCase. OptiCase provides therefore
 * methods to evaluate an object functional and compute derivatives.
 * SteepestDescent searches along the direction of the derivative to find
 * a lower value of the evaluated object functional.
 *
 * This class is not intended to be derived from.
 */

template<typename T>
class OptimizerSteepestDescent : public OptimizerLineSearch<T> {

private:
  mutable OstreamManager clout;


public:
  OptimizerSteepestDescent(int dim, T eps, int maxIt, T startValue=0.0, bool verboseOn=true, const std::string fname="",
                           bool withUpperBound=false, T upperBound=T(), bool withLowerBound=false, T lowerBound=T(),
                           std::string startValueType="Porosity")
    : OptimizerLineSearch<T>(dim, eps, maxIt, startValue, verboseOn, fname, withUpperBound, upperBound, withLowerBound,
                             lowerBound, startValueType),
      clout(std::cout,"OptimizerSteepestDescent") {};

  virtual void computeDirection()
  {
    // Update _derivative
    this->_optiCase->computeDerivatives(this->_control, this->_derivative);

    T normDerivative = euklidN(this->_derivative, this->_dim);
    for (int iDim=0; iDim<this->_dim; iDim++) {
      this->_direction[iDim] = this->_derivative[iDim]/normDerivative;
    }
  };
};


/// Creator Function for Steepest Decent
template<typename T>
OptimizerSteepestDescent<T>* createOptimizerSteepestDescent(XMLreader const& params)
{
  OstreamManager clout(std::cout,"createOptimizerSteepestDescent");

  // create variables with default values
  int dim;
  int maxIt = 100;

  T eps = T(1.e-10);
  T startValue= 0.;
  std::string startValueType = "Porosity";

  bool verboseOn=true;
  std::string fname;

  bool withUpperBound = false;
  T upperBound = T();
  bool withLowerBound = false;
  T lowerBound = T();

  // deactivate default warnings and show default values instead
  params.setWarningsOn(false);


  // Read Values from XML File
  readOptiXMLOrWarn<int>(clout, params, "Dimension", dim, true);
  readOptiXMLOrWarn<int>(clout, params, "MaxIter", maxIt);

  readOptiXMLOrWarn<T>(clout,   params, "Tolerance", eps);
  readOptiXMLOrWarn<T>(clout, params, "StartValue", startValue);
//  readOptiXMLOrWarn<std::string>(clout, params, "StartValueType", startValueType);

  readOptiXMLOrWarn<bool>(clout, params, "Verbose", verboseOn);
  readOptiXMLOrWarn<std::string>(clout, params, "InputFileName", fname);

  if ( readOptiXMLOrWarn<T>(clout, params, "UpperBound", upperBound) ) {
    withUpperBound = true;
  } else {
    clout << "\t-> setting withUpperBound = false" << endl;
  }
  if ( readOptiXMLOrWarn<T>(clout, params, "LowerBound", lowerBound) ) {
    withLowerBound = true;
  } else {
    clout << "\t-> setting withLowerBound = false" << endl;
  }

  // turn default warnings on again
  params.setWarningsOn(true);

  // Create Optimizer Object
  return new OptimizerSteepestDescent<T>(dim, eps, maxIt, startValue, verboseOn, fname, withUpperBound, upperBound, withLowerBound, lowerBound, startValueType);
}

} // namespace opti

} // namespace olb

#endif
