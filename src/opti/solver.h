/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef SOLVER_H
#define SOLVER_H


#include "backup.h"
#include "controller.h"
#include "core/superLattice3D.h"
#include "dynamics/dynamics.h"
#include "geometry/superGeometry3D.h"
#include "utilities/timer.h"

#include <iostream>
#include "core/units.h"
#include "io/ostreamManager.h"
#include "io/xmlReader.h"


using namespace std;

namespace olb {

namespace opti {


template<typename S, typename T, template<typename U> class Lattice>
class Solver {

public:
  OstreamManager clout;
  XMLreader *_xml;
  Controller <T> *_controller;
  //UnitConverter<S,Lattice> *_converter;
  // converter with ADf instead of double as template type (might cause some trouble)
  UnitConverter<T,Lattice> *_converter;
  Backup <S, T> *_backup;
  Dynamics <T, Lattice> *_bulkDynamics;

  /// Application variables
  string _name;
  string _olbDir;
  string _outputDir;
  S _startTime;
  S _maxTime;
  S _maxU;
  S _re;
  int _NN;
  int _dim;
  int optiStep;
  // L: added for a rectangular 2d flow domain

  S _logT;
  S _imageSaveT;
  S _vtkSaveT;

  util::Timer <S> *_timer;

  int _noC; // no of cuboids per mpi process

  bool _logConverter;
  bool _pressureFilterOn;
  bool _dualMode;

public:
  Solver();

  Solver(XMLreader *xml, Controller<T> *controller = 0);

  // L: destructor added
  virtual ~Solver();

  void reInit(XMLreader *xml, Controller<T> *controller = 0);

  void reInit();

  //virtual void loadBackup(string fileName) = 0;
  //virtual void saveBackup(string fileName) = 0;

  //virtual DataAnalysisBase3D<T, Lattice> const& getAnalysis3D() const;
  virtual SuperLattice3D <T, Lattice> *getLattice() = 0;

  virtual SuperGeometry3D <T> *getGeometry() = 0;

  XMLreader *getParameter()
  {
    return _xml;
  };


  virtual void init() = 0;

//  virtual void initForce() = 0;
//  //virtual void initPorosity() = 0;
//  virtual void initVelocity() = 0;

  virtual void solve() = 0;

  virtual void writeVTK(int iter = 0) = 0;

  virtual void addExternalField(AnalyticalF3D <T, T> *f) = 0;

  UnitConverter<T,Lattice> *getConverter() {
    return _converter;
  };

  Controller <T> *getController()
  {
    return _controller;
  };

  Dynamics <T, Lattice> *getBulkDynamics()
  {
    return _bulkDynamics;
  };

  void increaseOptiStep()
  {
    optiStep++;
  };

  void setBulkDynamics(XMLreader *xml);

  template<class Momenta>
  void setBulkDynamics(string optModeName);

};

/*

template<typename S, typename T, template<typename U> class Lattice>
Solver<S, T, Lattice> *createSolver(XMLreader */
/*const*//*
&params, Controller<T> *controller = NULL) {
  OstreamManager clout(cout, "createSolver");

  int dim = 3;

  bool verbose = false;
  params.setWarningsOn(false);
  // params[parameter].read(value) sets the value or returns false if the parameter can not be found

  if (!params["Application"]["Dimension"].read<int>(dim, verbose)) {
    clout << "Warning: Cannot read parameter from Xml-file: Dimension. Set default: dim=3"
    << endl;
  }

  params.setWarningsOn(true);

  return new Solver3D<S, T, Lattice>(&params, controller);
}

*/

}

} // namespace olb

#endif
