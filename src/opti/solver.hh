/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef SOLVER_HH
#define SOLVER_HH

#include "solver.h"


using namespace std;
using namespace olb;
using namespace olb::opti;


template<typename S, typename T, template<typename U> class Lattice>
Solver<S,T,Lattice>::Solver()
  : clout(cout, "Solver"), _backup(nullptr), _converter(nullptr)
{
};

template<typename S, typename T, template<typename U> class Lattice>
Solver<S,T,Lattice>::Solver(XMLreader *xml, Controller<T> *controller)
  : clout(cout, "Solver"), _converter(nullptr), _backup(nullptr)
{
  reInit(xml, controller);
};

// L: destructor added
template<typename S, typename T, template<typename U> class Lattice>
Solver<S,T,Lattice>::~Solver()
{
  delete _backup;
  _backup = nullptr;
  delete _converter;
  _converter = nullptr;
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver<S,T,Lattice>::reInit()
{

  (*_xml)["Application"]["Name"].read(_name);
  (*_xml)["Application"]["OlbDir"].read(_olbDir);
  (*_xml)["Output"]["OutputDir"].read(_outputDir);
  (*_xml)["Output"]["LogConverter"].read(_logConverter);
  (*_xml)["Application"]["PhysParameters"]["StartTime"].read(_startTime);
  (*_xml)["Application"]["PhysParameters"]["PhysMaxTime"].read(_maxTime);

  singleton::directories().setOlbDir(_olbDir);
  singleton::directories().setOutputDir(_outputDir);

  (*_xml)["Application"]["dim"].read(_dim);
  (*_xml)["Application"]["PhysParameters"]["CharPhysVelocity"].read(_maxU);
  (*_xml)["Application"]["PhysParameters"]["Re"].read(_re);
  (*_xml)["Application"]["Discretization"]["Resolution"].read(_NN);
  optiStep = 0;

  // L: for 3d only!
  // (*_xml)["Application"]["Mesh"]["lz"].read(lz);

  _converter = createUnitConverter<T,Lattice>((*_xml));
  //_converter->print();

  (*_xml)["Output"]["Log"]["SaveTime"].read(_logT);
  (*_xml)["Output"]["VisualizationImages"]["SaveTime"].read(_imageSaveT);
  (*_xml)["Output"]["VisualizationVTK"]["SaveTime"].read(_vtkSaveT);

  (*_xml)["Solver"]["PressureFilter"].read(_pressureFilterOn);

  if (!(*_xml)["Application"]["Mesh"]["noCuboidsPerProcess"].read(_noC)) {
    _noC = 1;
  }

  setBulkDynamics(_xml);

};

template<typename S, typename T, template<typename U> class Lattice>
void Solver<S,T,Lattice>::reInit(XMLreader *xml, Controller<T> *controller)
{
  _xml = xml;
  _controller = controller;
  reInit();
};


template<typename S, typename T, template<typename U> class Lattice>
void Solver<S,T,Lattice>::setBulkDynamics(XMLreader* xml)
{

  string optModeName;
  if (!(*xml)["BoundaryFunction0"]["Type"].read(optModeName)) {
    (*xml)["Primal"]["Mode"].read(optModeName);
  }
  setBulkDynamics(optModeName);
}


template<typename S, typename T, template<typename U> class Lattice>
template<class Momenta>
void Solver<S,T,Lattice>::setBulkDynamics(string optModeName)
{

  if (optModeName == "ForcedBGK") {
#ifdef forceMode
    _bulkDynamics = new ForcedBGKdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
#endif
  } else if (optModeName == "ForcedShanChenBGK") {
#ifdef forceMode
    _bulkDynamics = new ForcedShanChenBGKdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
#endif
  } else if (optModeName == "ForcedMRT") {
#ifdef forceMode
#ifdef mrtMode
    _bulkDynamics = new ForcedMRTdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
#endif
#endif
  } else if (optModeName == "BGK") {
    _bulkDynamics = new BGKdynamics<T, Lattice, Momenta>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
  } else if (optModeName == "PorousBGK") {
#ifdef porousMode
    _bulkDynamics = new PorousBGKdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
#endif
  } else if (optModeName == "DualForcedBGK") {
#ifdef forceMode
    _bulkDynamics = new DualForcedBGKdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>(), this->_controller );
    _dualMode=true;
#endif
  } else if (optModeName == "DualForcedMRT") {
#ifdef forceMode
#ifdef mrtMode
    _bulkDynamics = new DualForcedMRTdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>(), this->_controller );
    _dualMode=true;
#endif
#endif
  } else if (optModeName == "DualPorousBGK") {
#ifdef porousMode
    _bulkDynamics = new DualPorousBGKdynamics<T, Lattice>(
      this->_converter->getLatticeRelaxationFrequency(),
      instances::getBulkMomenta<T, Lattice>(), this->_controller );
    _dualMode=true;
#endif
  } else {
    clout << "ERROR: " << optModeName << " not found" << endl;
  }
  clout << "Chosen bulkDynamics: " << optModeName << endl;
}

#endif
