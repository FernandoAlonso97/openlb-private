/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef SOLVER_2D_H
#define SOLVER_2D_H

#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <time.h>

#include "controller.h"
#include "controlledFunctions.h" // functors for initialization and objective evaluation
#include "boundaryCondFromBelow.h"
#include "core/blockLattice3D.h"
#include "boundary/superBoundaryCondition3D.h"
#include "boundary/superOffBoundaryCondition3D.h"

//#include "core/blockLattice3D.h"
//#include "core/blockLatticeView3D.h"
//#include "communication/communicator3D.h"
//#include "geometry/cuboidGeometry3D.h"
#include "communication/loadBalancer.h"
#include "communication/heuristicLoadBalancer.h"
//#include "optiObject.h"
//#include "databasket.h"
#include "core/units.h"
#include "core/units.hh"
#include "io/fileName.h"
#include "io/ostreamManager.h"
#include "io/xmlReader.h"
#include "opti/dualDynamics.h"
#include "opti/dualLatticeDescriptors.h"
#include "dynamics/porousLatticeDescriptors.h"
#include "dynamics/porousBGKdynamics.h"





using namespace olb::opti;




/// All OpenLB code is contained in this namespace.
namespace olb {



/// A super lattice combines a number of block lattices that are ordered
/// in a cuboid geometry.
/** The communication between the block lattices is done by two
 * communicators. One (_commStream) is responible to provide the data for
 * the streaming the other (_commBC) for the non-local boundary conditions.
 * To simplify the code structure ghost cells in an overlap of size
 * (_overlap) is indrocuced. It depends on the non-locality of the
 * boundary conditions but is at least one because of the streaming
 *
 * The algorithm is parallelized with mpi. The load balancer (_load)
 * distributes the block lattices to processes.
 *
 * WARNING: For unstructured grids there is an iterpolation needed
 * for the method buffer_outData in coboidNeighbourhood which is not
 * yet implemented! Moreover this class needs to be chanced
 * that the number of times steps for the collision and streaming is
 * is dependent of the refinement level.
 *
 * This class is not intended to be derived from.
 */

//template<typename S, typename T, template<typename U> class Lattice> class SolverObjective;

//template<typename R, typename S, typename T, template<typename U> class Lattice> class Solver3D;
template<typename S, typename T, template<typename U> class Lattice> class Solver;
template<typename S, typename T, template<typename U> class Lattice> class Solver3D;

/// Function: S^dimInput -> T^dimOutput


template<typename S, typename T, template<typename U> class Lattice>
class Solver3DObjective : public GenericF<S,S> {
private:
  Solver3D<S,T,Lattice>& _solver;
public:
  Solver3DObjective(Solver3D<S,T,Lattice>& solver) : _solver(solver) { };

  T operator()();
  std::string name()
  {
    return "solver3DObjective";
  }

};

template<typename S, typename T>
class Backup {

private:
  XMLreader*                                  _xml;
  LBconverter<S>*                             _converter;
  std::string                                 _saveName;
  int                                         _nStep;
  bool                                        _saveEnd;

public:
  Backup()
  {
    _xml = NULL;
    _converter = NULL;
  };
  Backup(LBconverter<S>* converter, XMLreader* xml)
  {
    _xml = NULL;
    _converter = NULL;
    reInit(converter, xml);
  };
  void reInit(LBconverter<S>* converter, XMLreader* xml)
  {
    _xml = xml;
    _converter = converter;
    reInit();
  };
  void reInit()
  {
    S saveTime = 0;
    (*_xml)["Output"]["BackupSolution"]["FileName"].read(_saveName);
    (*_xml)["Output"]["BackupSolution"]["SaveEnd"].read(_saveEnd);
    (*_xml)["Output"]["BackupSolution"]["SaveTime"].read(saveTime);
    _nStep = _converter->numTimeSteps(saveTime);
  };

  void checkAndSave(Serializable<T>* serializer, int iT = 0)
  {
    if (iT==-1 && _saveEnd) {
      save(serializer);
    } else if (iT%_nStep==0 && iT!=0) {
      save(serializer, iT);
    }
  };

  void save(Serializable<T>* serializer, int iT = 0)
  {
    std::string saveName = _saveName;
    if (iT!=0) {
      saveName = olb::createFileName(_saveName, iT, 6);
    }
    std::cout << "Saving backup file ..." << std::endl;
    saveData(*serializer,saveName);
  };

  void load(Serializable<T>* serializer, int iT = 0)
  {
    std::string saveName = _saveName;
    if (iT!=0) {
      saveName = olb::createFileName(_saveName, iT, 6);
    }
    std::cout << "Loading backup file ..." << std::endl;
    loadData(*serializer,saveName);
  };
};







template<typename S, typename T, template<typename U> class Lattice>
class Solver {

protected:
  OstreamManager                              clout;
  XMLreader*                                  _xml;
  opti::Controller<T>*                        _controller;
  //LBconverter<S>*                             _converter;
  // converter with ADf instead of double as template type (might cause some trouble)
  LBconverter<T>*                             _converter;
  Backup<S,T>*                                _backup;
  Dynamics<T, Lattice>*                       _bulkDynamics;



  /// Application variables
  std::string                                 _name;
  std::string                                 _olbDir;
  std::string                                 _outputDir;
  S                                           _startTime;
  S                                           _maxTime;
  S                                           _maxU;
  S                                           _re;
  int                                         _N;
  int                                         _dim;
  // L: added for a rectangular 2d flow domain
  S                                           _lx;
  S                                           _ly;
  S                                           _lz;

  S                                           _logT;
  S                                           _imageSaveT;
  S                                           _vtkSaveT;

  int                                         _noC; // no of cuboids per mpi process

  bool _pressureFilterOn;
  bool _dualMode;

public:
  Solver() : clout(std::cout,"Solver"),_backup(NULL),_converter(NULL) {};

  Solver(XMLreader* xml, opti::Controller<T>* controller = 0) : clout(std::cout,"Solver"),_backup(NULL),_converter(NULL)
  {
    reInit(xml, controller);
  };

  // L: destructor added
  ~Solver()
  {
    delete _backup;
    _backup=NULL;
    delete _converter;
    _converter=NULL;
  }



  void reInit(XMLreader* xml, opti::Controller<T>* controller = 0)
  {
    _xml = xml;
    _controller = controller;
    reInit();
  };

  void reInit()
  {

    (*_xml)["Application"]["Name"].read(_name);
    (*_xml)["Application"]["OlbDir"].read(_olbDir);
    (*_xml)["Output"]["OutputDir"].read(_outputDir);
    (*_xml)["Application"]["PhysParam"]["StartTime"].read(_startTime);
    (*_xml)["Application"]["PhysParam"]["MaxTime"].read(_maxTime);

    singleton::directories().setOlbDir(_olbDir);
    singleton::directories().setOutputDir(_outputDir);

    (*_xml)["Application"]["dim"].read(_dim);
    (*_xml)["Application"]["PhysParam"]["charU"].read(_maxU);
    (*_xml)["Application"]["PhysParam"]["Re"].read(_re);
    (*_xml)["Application"]["DiscParam"]["resolution"].read(_N);
    (*_xml)["Application"]["Mesh"]["lx"].read(_lx);
    (*_xml)["Application"]["Mesh"]["ly"].read(_ly);
    (*_xml)["Application"]["Mesh"]["lz"].read(_lz);

    // L: for 3d only!
    // (*_xml)["Application"]["Mesh"]["lz"].read(lz);

    _converter     = createLBconverter<T>((*_xml));
    //_converter->print();

    (*_xml)["Output"]["Log"]["SaveTime"].read(_logT);
    (*_xml)["Output"]["VisualizationImages"]["SaveTime"].read(_imageSaveT);
    (*_xml)["Output"]["VisualizationVTK"]["SaveTime"].read(_vtkSaveT);

    (*_xml)["Solver"]["PressureFilter"].read(_pressureFilterOn);

    if (!(*_xml)["Application"]["Mesh"]["noCuboidsPerProcess"].read(_noC) ) {
      _noC = 1;
    }

    setBulkDynamics(_xml);






  };


  //virtual void loadBackup(std::string fileName) = 0;
  //virtual void saveBackup(std::string fileName) = 0;

  //virtual DataAnalysisBase3D<T, Lattice> const& getAnalysis3D() const;
  virtual SuperLattice3D<T, Lattice>* getLattice() = 0;
  virtual BlockGeometry3D* getGeometry() = 0;

  XMLreader* getParameter()
  {
    return _xml;
  };


  virtual void init() = 0;
  virtual void init(Solver<S,T,Lattice>& solver) {};
  virtual void initForce() = 0;
  //virtual void initPorosity() = 0;
  virtual void initVelocity() = 0;
  virtual void solve () = 0;
  //virtual bool getValueFunctionG (std::string functionName, T* output = 0) = 0;
  virtual void writeVTK(int iter) = 0;

  virtual T evaluateObjective() = 0;

  LBconverter<T>* getConverter()
  {
    return _converter;
  };

  Dynamics<T, Lattice>* getBulkDynamics()
  {
    return _bulkDynamics;
  };

  void setBulkDynamics(XMLreader* xml);
  void setBulkDynamics(std::string optModeName);

};



template<typename S, typename T, template<typename U> class Lattice>
class Solver3D : public Solver<S,T,Lattice> {

protected:
  S                                           _tolerance;
  util::ValueTracer<S>*                       _converge;
  BlockGeometry3D*                            _bg;
  BlockGeometryStatistics3D*                  _bgStat;
  CuboidGeometry3D<T>*                        _cGeometry;
  SuperLattice3D<T, Lattice>*                 _lattice;

  sOnLatticeBoundaryCondition3D<T, Lattice>*  _onBc;
  sOffLatticeBoundaryCondition3D<T, Lattice>* _offBc;

  // new functor concept
  ControlledSuperLatticeF3D<T,Lattice>*                            _startExternalField;
  ControlledSuperLatticeF3D<T,Lattice>*                            _startPressure;
  ControlledSuperLatticeF3D<T,Lattice>*                            _startVelocity;

  Solver3DObjective<S,T,Lattice>*               _objective;


  OstreamManager                              clout;

public:
  /// Construction of a 3D Solver
  Solver3D()
    : Solver<S,T,Lattice>(), clout(std::cout,"Solver3D")
  { }
  Solver3D(XMLreader* xml, opti::Controller<T>* controller = 0)
    : Solver<S,T,Lattice>(xml, controller), clout(std::cout,"Solver3D")
  {
  };


  void init(Solver<S,T,Lattice>& solver);


  void setBoundaryCondition(XMLreader* xml);
  void setBoundaryCondition(std::string byTypeName);

  void initForce();
  void initVelocity();

  void prepareGeometry();
  void prepareLattice();

  void setBoundaryValues(int iT, int itStart);

  void solve();

  // ### reInit()
  void init()
  {

    prepareGeometry();


#ifdef PARALLEL_MODE_MPI
    _cGeometry = new CuboidGeometry3D<T>(_bg->getPositionX(),
                                         _bg->getPositionY(),
                                         _bg->getPositionZ(),
                                         _bg->getDeltaR(),
                                         _bg->getNx(), _bg->getNy(), _bg->getNz(),
                                         this->_noC*singleton::mpi().getSize());
#else
    _cGeometry = new CuboidGeometry3D<T>(_bg->getPositionX(),
                                         _bg->getPositionY(),
                                         _bg->getPositionZ(),
                                         _bg->getDeltaR(),
                                         _bg->getNx(), _bg->getNy(), _bg->getNz(),
                                         7);
#endif

    // L: too much information for optimization mode!
    _cGeometry->remove(*this->_bg);
    _cGeometry->shrink(*this->_bg);

    HeuristicLoadBalancer<T> *lb = new HeuristicLoadBalancer<T>(*_cGeometry, *_bg);
    _lattice = new SuperLattice3D<T, Lattice>(*_cGeometry, 2, lb);

    SuperLatticeCuboid3D<T, Lattice> cuboid(_lattice);
    SuperLatticeRank3D<T, Lattice> rank(_lattice);
    CuboidVTKout3D<T>::write(cuboid);
    CuboidVTKout3D<T>::write(rank);


    (*this->_xml)["Solver"]["Tolerance"].read(_tolerance);
    _converge = new util::ValueTracer<S>(this->_maxU, this->_N, _tolerance);

    // ### functor initialization /////
    T uIn = 0.2;
    bool b = (*this->_xml)["Application"]["PhysParam"]["charU"].read(uIn);
    // initial force function
    _startExternalField = new ControlledSuperLatticeStartExternalField3D<T,Lattice>(
      this->_lattice,
      this->_bg,
      this->_controller,
      this->_converter);

    // initialize velocity function
    _startVelocity = new ControlledSuperLatticeStartVelocity3D<T,Lattice>(
      this->_lattice,
      this->_bg,
      this->_controller,
      this->_converter);

    // initialize pressure function
    _startPressure = new ControlledSuperLatticeStartPressure3D<T,Lattice>(
      this->_lattice,
      this->_bg,
      this->_controller,
      this->_converter);

    _objective = new Solver3DObjective<S,T,Lattice>(*this);

    // initialize objective function (bulk Dynamics have to be set before)




    setBoundaryCondition(this->_xml);


    prepareLattice();
    setBoundaryValues(0,1);

  };

  SuperLattice3D<T, Lattice>* getLattice()
  {
    return _lattice;
  };
  BlockGeometry3D* getGeometry()
  {
    return _bg;
  };

  void writeVTK(int iter);

  T evaluateObjective()
  {
    return (*_objective)();
  };
};






} // namespace olb

#endif
