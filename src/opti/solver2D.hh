/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * The description of a 3D super lattice -- generic implementation.
 */


#ifndef SOLVER_3D_HH
#define SOLVER_3D_HH
/*
#include "communication/mpiManager.h"
#include "core/blockLattice3D.h"
#include "core/cell.h"
#include "communication/communicator3D.h"
#include "geometry/cuboidGeometry3D.h"
#include "communication/loadBalancer.h"
#include "core/postProcessing.h"
#include "core/superLattice3D.h"
#include "aDiff.h"
*/

#include <string>
#include "solver3D.h"
#include "opti/dualDynamics.h"
#include "utilities/timer.h"
#include "io/ostreamManager.h"
#include "dynamics/porousBGKdynamics.h"
#include "dynamics/porousLatticeDescriptors.h"
#include "opti/dualLatticeDescriptors.h"

using namespace std;
using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::opti;
using namespace olb::util;

/// All OpenLB code is contained in this namespace.
namespace olb {




//template<typename R, typename S, typename T, template<typename U> class DESCRIPTOR> class Solver3D;

////////////////////// Class Solver 3D      /////////////////////////

template<typename S, typename T, template<typename U> class Lattice>
void Solver<S,T,Lattice>::setBulkDynamics(XMLreader* xml)
{

  std::string optModeName;
  (*xml)["BoundaryFunction0"]["Type"].read(optModeName);
  setBulkDynamics(optModeName);
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver<S,T,Lattice>::setBulkDynamics(std::string optModeName)
{

  if (optModeName == "ForcedBGK") {
#ifdef forceMode
    _bulkDynamics = new ForcedBGKdynamics<T, Lattice>(
      this->_converter->getOmega(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
#endif
  } else if (optModeName == "BGK") {
    _bulkDynamics = new BGKdynamics<T, Lattice>(
      this->_converter->getOmega(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
  } else if (optModeName == "PorousBGK") {
#ifdef porousMode
    _bulkDynamics = new PorousBGKdynamics<T, Lattice>(
      this->_converter->getOmega(),
      instances::getBulkMomenta<T, Lattice>() );
    _dualMode=false;
#endif
  } else if (optModeName == "DualForcedBGK") {
#ifdef forceMode
    _bulkDynamics = new DualForcedBGKdynamics<T, Lattice>(
      this->_converter->getOmega(),
      instances::getBulkMomenta<T, Lattice>(), this->_controller );
    _dualMode=true;
#endif
  } else if (optModeName == "DualPorousBGK") {
#ifdef porousMode
    _bulkDynamics = new DualPorousBGKdynamics<T, Lattice>(
      this->_converter->getOmega(),
      instances::getBulkMomenta<T, Lattice>(), this->_controller );
    _dualMode=true;
#endif
  } else {
    clout << "ERROR: " << optModeName << " not found" << std::endl;
  }
  clout << "Chosen bulkDynamics: " << optModeName << std::endl;
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S,T,Lattice>::setBoundaryCondition(XMLreader* xml)
{

  std::string bcType;
  (*xml)["BoundaryFunction0"]["Type"].read(bcType);
  setBoundaryCondition(bcType);
  //(*xml)["BoundaryFunction0"]["Type"].read(bcType);
  //setBoundaryCondition(bcType);
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S,T,Lattice>::setBoundaryCondition(std::string bcType)
{

  if (bcType == "Interp") {
    _onBc = new sOnLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    createInterpBoundaryCondition3D<T,Lattice>(*_onBc);
  } else if (bcType == "Local") {
    _onBc = new sOnLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    createLocalBoundaryCondition3D<T,Lattice>(*_onBc);
  } else if (bcType == "Bouzidi") {
    //_offBc = new sOffLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    //createBouzidiBoundaryCondition3D<T,Lattice>(*_offBc);
  } else {
    clout << "WARNING: " << bcType << " not found" << std::endl;
    _onBc = new sOnLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    createInterpBoundaryCondition3D<T,Lattice>(*_onBc);
    //_offBc = new sOffLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    //createBouzidiBoundaryCondition3D<T,Lattice>(*_offBc);
  }
  clout << "Chosen boundary type: " << bcType << std::endl;
}











template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S,T,Lattice>::initForce()
{
  /*
    clout << "Initializing force ..." << std::endl;
  std::string optModeName;
  (*this->_xml)["BoundaryFunction0"]["Type"].read(optModeName);
  if (optModeName == "ForcedBGK") {
    const int nx = this->_converter->numNodes(this->_lx);
    const int ny = this->_converter->numNodes(this->_ly);
    const int nz = this->_converter->numNodes(this->_lz);

    for (int iX=0; iX<nx; ++iX) {
      for (int iY=0; iY<ny; ++iY) {
        for (int iZ=0; iZ<nz; ++iZ) {

          std::vector<S> r(3,0);
          r[0] = (S)iX/(S)nx;
          r[1] = (S)iY/(S)ny;
          r[2] = (S)iZ/(S)nz;
          std::vector<T> vforce = (*_startForce)(iX,iY,iZ);
          T force[3] = {vforce[0], vforce[1], vforce[2]};
          _lattice->defineExternalField( iX, iX, iY, iY, iZ, iZ,
          Lattice<T>::ExternalField::forceBeginsAt,
          Lattice<T>::ExternalField::sizeOfForce,
          force);
        }
      }
    }
    clout << "Initializing force ...OK" << std::endl;
  }
  */
}





/// initializes all lattice cells with zero velocity
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S,T,Lattice>::initVelocity()
{

  clout << "### Initializing velocity ..." << std::endl;

  const int nx = this->_bg->getNx();
  const int ny = this->_bg->getNy();
  const int nz = this->_bg->getNz();

  T velocity[3];

  for (int iX = 0; iX < nx; iX++) {
    for (int iY = 0; iY < ny; iY++) {
      for (int iZ = 0; iZ < nz; iZ ++) {

        velocity[0] = (*_startVelocity)(iX,iY,iZ)[0];
        velocity[1] = (*_startVelocity)(iX,iY,iZ)[1];
        velocity[2] = (*_startVelocity)(iX,iY,iZ)[2];

        _lattice->defineU(iX, iX, iY, iY, iZ, iZ, velocity);

      }
    }
  }
  clout << "### Initializing velocity ... OK" << std::endl;
}












/// Writing the vtk files - double case
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S,T,Lattice>::writeVTK(int _iter=0)
{

  OstreamManager clout(std::cout,"writeVTK");
  clout << "writeFlowField..." << std::endl;

  SuperLatticePhysVelocity3D<T, Lattice> velocity(_lattice, this->_converter);
  SuperLatticePhysPressure3D<T, Lattice> pressure(_lattice, this->_converter);

  CuboidVTKout3D<T>::write(velocity, _iter);
  CuboidVTKout3D<T>::write(pressure, _iter);

  static int itt = 0;
  CuboidVTKout3D<T>::write(*_startExternalField,itt++);

  clout << "writeFlowField... OK" << std::endl;
}













#ifdef ADT
template<typename S, typename T, template<typename U> class Lattice>
void initConverged(SuperLattice3D<ADf<double,NUMBER_DIRECTIONS>, Lattice>* lattice, util::ValueTracer<S>* converge)
{

  converge->takeValue(lattice->getStatistics().getAverageEnergy().v(),true);
}
#endif
template<typename S, typename T, template<typename U> class Lattice>
void initConverged(SuperLattice3D<S, Lattice>* lattice, util::ValueTracer<S>* converge)
{

  converge->takeValue(lattice->getStatistics().getAverageEnergy(),true);
}

//// Hier steht die wichtige solve()-Funktion //////////////////////////////////
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S,T,Lattice>::solve()
{

  OstreamManager clout(std::cout,"Solver3D.solve");
  clout << "### solve started..." << std::endl;

  int nx = this->_bg->getNx();
  int ny = this->_bg->getNy();
  int nz = this->_bg->getNz();

  T rho = T(1);
  T velocity[3];

  velocity[0] = T();
  velocity[1] = T();
  velocity[2] = T();

  for (int iX = 0; iX < nx; iX++) {
    for (int iY = 0; iY < ny; iY++) {
      for (int iZ = 0; iZ < nz; iZ ++) {
        _lattice->defineRhoU(iX, iX, iY, iY, iZ, iZ, rho, velocity);
        _lattice->iniEquilibrium(iX, iX, iY, iY, iZ, iZ, rho, velocity);
      }
    }
  }


  _converge->resetValues();
  _lattice->getStatistics().initialize();
  _lattice->getStatistics().reset();


  //const int itStart = 10000;
  //const int itStart = 250;
  const int itStart = this->_startTime/this->_converter->physTime();

  writeLogFile(*this->_converter, this->_name);
  clout << "omega = " << this->_converter->getOmega() << endl;


  // create Timer and a suitable LBconverter (with double)
  LBconverter<S>* _converterS = createLBconverter<S>(*(this->_xml));
  //_converterS->print();
  util::Timer<S>* timer = util::createTimer<S>(*this->_xml, *_converterS, nx*ny*nz);
  timer->start();

  // begin of collideAndStream loop
  int iT(0);
  clout << "### 1" << std::endl;
  for (iT=0; this->_converter->physTime(iT-itStart) <= this->_maxTime; ++iT) {


    if (iT%50==0) {
      setBoundaryValues(iT,itStart);
    }

    if (iT%1000==0) {
      //timer->print(iT);
      //computePressureDrop();
      //cout << "### SOLVE iT=" << iT << "; pressureDrop=" << _objectFunctional->operator()(0,0)[0] << std::endl;
    }

    // print control, pressure drop and umax
    if (iT%this->_converter->numTimeSteps(this->_logT) == 0 && iT>0) {
      timer->print(iT);
      _lattice->getStatistics().print(iT, this->_converter->physTime(iT));
      //T deltaP = _objectFunctional->operator()(0,0,0)[0];
      //T maxU = this->_converter->physVelocity(this->_lattice->getStatistics().getMaxU());
      //clout << "iT=" << iT << "; control=" << this->_controller->getControl(0) << "; pressureDrop=" << deltaP << "; umax=" << maxU << std::endl;
    }




    if (iT%this->_converter->numTimeSteps(this->_imageSaveT)==0 && iT>0) {
      //clout << "Saving Gif ..." << std::endl;
      //writeGifs(iT);
    }

    if (iT%this->_converter->numTimeSteps(this->_vtkSaveT)==0 && iT>0) {
      clout << "Saving VTK ..." << std::endl;
      writeVTK(iT);
      clout << "Saving VTK ...OK" << std::endl;
      clout << "iT=" << iT << "; control=" << this->_controller->getControl(0) << "; pressureDrop=" << _objective->operator()() << std::endl;
    }

    //clout << iT << " collideStream...  " << std::endl;


    _lattice->collideAndStream();

    if (this->_pressureFilterOn) {
      if (!(this->_dualMode) ) {
        _lattice->stripeOffDensityOffset(_lattice->getStatistics().getAverageRho()-(T)1 );
      } else {
        _lattice->stripeOffDensityOffset( _lattice->getStatistics().getAverageRho() );
      }
    }


    if (!(this->_dualMode) )
      if (_lattice->getStatistics().getMaxU() > 0.3) {
        clout << "PROBLEM uMax=" << _lattice->getStatistics().getMaxU() << endl;
        break;
      }


    //clout << iT << " collideStream... OK" << std::endl;


    // convergence check
    //initConverged<S,T,Lattice>(_lattice, _converge);
    //if (iT>itStart &&iT%this->_converter->numTimeSteps(this->_logT)==0) {
    //if (iT>itStart &&iT%10==0) {
    //if (_converge->hasConverged()) {
    //  timer->stop();
    //  timer->printShortSummary();
    //  break;
    //}
    //}

  } // end collideAndStream loop

  // print results from last time step
  clout << "iT=" << iT << "; control=" << this->_controller->getControl(0) << "; pressureDrop=" << _objective->operator()() << std::endl;

  // time measurement
  timer->stop();
  //timer->printSummary();
  timer->printShortSummary();

  // //if (sizeof(T)>8) {
  // clout << "Saving VTK ..." << std::endl;
  // singleton::mpi().barrier();
  // //if (singleton::mpi().getRank()==0) {
  //   writeVTK();
  // //}
  // clout << "Saving VTK ...OK" << std::endl;
  // //}

  delete _converterS;
  delete timer;
  clout << "### solve started... OK" << std::endl;
}
//// Ende: solve() /////////////////////////////////////////////////////////////



} // namespace olb

#endif
