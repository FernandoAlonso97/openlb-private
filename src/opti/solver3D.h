/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef SOLVER_3D_H
#define SOLVER_3D_H


#include "backup.h"
#include "controller.h"
#include "controlledFunctions3D.h"

#include "boundary/superBoundaryCondition3D.h"
#include "boundary/superOffBoundaryCondition3D.h"
#include "core/superLattice3D.h"
#include "dynamics/dynamics.h"
#include "geometry/superGeometry3D.h"
#include "utilities/benchmarkUtil.h"
#include "utilities/timer.h"

#include "solver.h"


namespace olb {

namespace opti {


template<typename S, typename T, template<typename U> class Lattice>
class Solver3D : public Solver<S, T, Lattice> {

protected:
  S _tolerance;
  util::ValueTracer<S> *_converge;
  SuperGeometry3D<T> *_bg;
  SuperGeometryStatistics3D<T> *_bgStat;
  CuboidGeometry3D<T> *_cGeometry;
  LoadBalancer<T> *_loadBalancer;
  SuperLattice3D<T, Lattice> *_lattice;

  sOnLatticeBoundaryCondition3D<T, Lattice> *_onBc;
  sOffLatticeBoundaryCondition3D<T, Lattice> *_offBc;

  // new functor concept
  AnalyticalF3D<T, T> *_startExternalField;
  AnalyticalF3D<T, T> *_startPressure;
  AnalyticalF3D<T, T> *_startVelocity;


  OstreamManager clout;

public:
  /// Construction of a 3D Solver
  Solver3D()
    : Solver<S, T, Lattice>(), clout(std::cout, "Solver3D") { }

  Solver3D(XMLreader *xml, opti::Controller<T> *controller = 0)
    : Solver<S, T, Lattice>(xml, controller), clout(std::cout, "Solver3D")
  {
  };


  void addExternalField(AnalyticalF3D<T, T> *f)
  {
    _startExternalField = f;
  };

  void setBoundaryCondition(XMLreader *xml);
  void setBoundaryCondition(std::string byTypeName);

  //void initForce();
  //void initVelocity();
  /// User has to provide this method
  void prepareGeometry();

  /// User has to provide this method
  void prepareLattice();

  /// User has to provide this method
  void setBoundaryValues(int iT);

  /// User has to provide this method
  void getResults(int iT);


  void solve();

  void init();

  SuperLattice3D<T, Lattice> *getLattice()
  {
    return _lattice;
  };

  SuperGeometry3D<T> *getGeometry()
  {
    return _bg;
  };

  LoadBalancer<T>* getLoadBalancer()
  {
    return _loadBalancer;
  }
  CuboidGeometry3D<T>* getCuboidGeometry()
  {
    return _cGeometry;
  }

  void printLog(int iT);
  void writeLogConverter();
  void writeVTK(int iter); // TODO: veraltet?
};


}

} // namespace olb

#endif
