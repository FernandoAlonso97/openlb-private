/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012-2016 Mathias J. Krause, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#ifndef SOLVER_3D_HH
#define SOLVER_3D_HH


#include <string>

#include "dualDynamics.h"
#include "dualMrtDynamics.h"
#include "dualLatticeDescriptors.h"
#include "solver3D.h"

#include "dynamics/porousBGKdynamics.h"
#include "dynamics/porousLatticeDescriptors.h"
#include "geometry/cuboidGeometry3D.h"
#include "io/ostreamManager.h"
#include "io/superVtmWriter3D.h"
#include "utilities/timer.h"

using namespace std;
using namespace olb;
using namespace olb::descriptors;
using namespace olb::util;


namespace olb {

namespace opti {


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::init()
{

  // reInit superGeometry
  prepareGeometry();

  UnitConverter<T,Lattice> *_converterS = createUnitConverter<T,Lattice>(*(this->_xml));
  this->_timer = createTimer<S>(*this->_xml, *_converterS, _bg->getStatistics().getNvoxel());

  _lattice = new SuperLattice3D<T, Lattice>(*_bg);

  std::string convergenceCheck;
  (*this->_xml)["Solver"]["ConvergenceCheck"].read(convergenceCheck);
  (*this->_xml)["Solver"]["Tolerance"].read(_tolerance);

  if (convergenceCheck == "AverageEnergy") {
    _converge = new ValueTracer<S>(1, _tolerance); //NN oder 1? FK
  } else {
    _converge = new ValueTracer<S>(this->_maxU, this->_NN, _tolerance);
  }

  // ### functor initialization /////

  // initialize force function //TODO:MACHT DAS SINN!? in optiCaseDual.hh Z.246/7 wird _startExternalField auf "Projection" gesetzt
  _startExternalField = new ControlledAnalyticalExternalField3D<T, Lattice>(
    this->_lattice, this->_bg, this->_controller, this->_converter);

  // initialize velocity function
  _startVelocity = new ControlledAnalyticalVelocity3D<T, Lattice>(
    this->_lattice, this->_bg, this->_controller, this->_converter);

  // initialize pressure function
  _startPressure = new ControlledAnalyticalPressure3D<T, Lattice>(
    this->_lattice, this->_bg, this->_controller, this->_converter);


  // initialize objective function (bulk Dynamics have to be set before)
  setBoundaryCondition(this->_xml);

  prepareLattice();

  writeLogConverter();
};


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::setBoundaryCondition(XMLreader *xml)
{
  string bcType;
  (*xml)["BoundaryFunction0"]["Type"].read(bcType);
  setBoundaryCondition(bcType);
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::setBoundaryCondition(string bcType)
{

  if (bcType == "Interp") {
    _onBc = new sOnLatticeBoundaryCondition3D<T, Lattice>(*_lattice);
    createInterpBoundaryCondition3D<T, Lattice>(*_onBc);
  } else if (bcType == "Local") {
    _onBc = new sOnLatticeBoundaryCondition3D<T, Lattice>(*_lattice);
    createLocalBoundaryCondition3D<T, Lattice>(*_onBc);
  } else if (bcType == "Bouzidi") {
    //_offBc = new sOffLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    //createBouzidiBoundaryCondition3D<T,Lattice>(*_offBc);
  } else {
    clout << "WARNING: " << bcType << " not found" << endl;
    _onBc = new sOnLatticeBoundaryCondition3D<T, Lattice>(*_lattice);
    createInterpBoundaryCondition3D<T, Lattice>(*_onBc);
    //_offBc = new sOffLatticeBoundaryCondition3D<T,Lattice>(*_lattice);
    //createBouzidiBoundaryCondition3D<T,Lattice>(*_offBc);
  }
  clout << "Chosen boundary type: Interp" << endl;
}


/// Writing the vtk files - double case
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::writeVTK(int iT)
{

  OstreamManager clout(cout, "writeVTK");
  clout << "writeFlowField..." << endl;


  SuperVTMwriter3D <T> vtmWriter("testFlow3d");
  /*
    if (_iter==0) {
      /// Writes the converter log file
      writeLogFile(converter, "testFlow3d");
      /// Writes the geometry, cuboid no. and rank no. as vti file for visualization
      SuperLatticeGeometry3D<T, DESCRIPTOR> geometry(sLattice, superGeometry);
      SuperLatticeCuboid3D<T, DESCRIPTOR> cuboid(sLattice);
      SuperLatticeRank3D<T, DESCRIPTOR> rank(sLattice);
      vtkWriter.write( cuboid );
      vtkWriter.write( geometry );
      vtkWriter.write( rank );
      vtkWriter.createMasterFile();
    }
  */
  /// Writes the vtk files
  //int iTperiodVTK = 10000;//converter.getLatticeTime(0.01);
  //if (iT%iTperiodVTK==0) {
  SuperLatticePhysVelocity3D<T, Lattice> velocity(*_lattice, *(this->_converter));
  SuperLatticePhysPressure3D<T, Lattice> pressure(*_lattice, *(this->_converter));
  SuperLatticeFfromAnalyticalF3D<T, Lattice> tmp(*_startExternalField, *_lattice);

  vtmWriter.addFunctor(velocity);
  vtmWriter.addFunctor(pressure);
  vtmWriter.addFunctor(tmp);
  vtmWriter.write(iT);
  //}

  clout << "writeFlowField... OK" << endl;
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::writeLogConverter()
{
  if (this->_logConverter) {
    clout << "Write log converter..." << endl;
//    writeLogFile(*(this->_converter), this->_name);
    this->_converter->write(this->_name);
    clout << "Write log converter... OK" << endl;
  }
}


template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::printLog(int iT)
{
  if (iT % this->_converter->getLatticeTime(this->_logT) == 0 || iT == 10) {
    this->_timer->print(iT, 2);
    this->_lattice->getStatistics().print(iT, this->_converter->getPhysTime(iT));
  }
}


/// This method implements the important solve algorithm
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::solve()
{

  clout << "Solving..." << endl;

  //_converge->resetValues();
  _lattice->getStatistics().reset();
#ifdef PARALLEL_MODE_MPI
  singleton::mpi().barrier();
#endif
  _lattice->getStatistics().initialize();
#ifdef PARALLEL_MODE_MPI
  singleton::mpi().barrier();
#endif
  // create Timer and a suitable Converter (with double)
  //_converterS->print();
  this->_timer->start();

  // begin of collideAndStream loop
  int iT(0);

  vector<S> re;
//  for (iT = 0; this->_converter->getPhysTime(iT) <= this->_startTime + this->_maxTime; ++iT) { //TODO: start+max !? FK
  for (iT = 0; this->_converter->getPhysTime(iT) <= this->_maxTime; ++iT) {
    if (iT % 50 == 0) {
      setBoundaryValues(iT);
    }

#ifdef PARALLEL_MODE_MPI
    singleton::mpi().barrier();
#endif
    // prints lattice statistics on the screen
    printLog(iT);
#ifdef PARALLEL_MODE_MPI
    singleton::mpi().barrier();
#endif
    if (iT % this->_converter->getLatticeTime(this->_imageSaveT) == 0 && iT > 0) {
      //clout << "Saving Gif ..." << endl;
      //writeGifs(iT);
    }

    if (iT % this->_converter->getLatticeTime(this->_vtkSaveT) == 0) {
      //writeVTK(iT);
      getResults(iT);
    }

    //clout << iT << " collideStream...  " << endl;

#ifdef PARALLEL_MODE_MPI
    singleton::mpi().barrier();
#endif

    _lattice->collideAndStream();

    //clout << iT << " collideStream..2.  " << endl;
#ifdef PARALLEL_MODE_MPI
    singleton::mpi().barrier();
#endif
    if (this->_pressureFilterOn) {
#ifdef ADT
      _lattice->stripeOffDensityOffset(_lattice->getStatistics().getAverageRho() - (T) 1);
#else
      _lattice->stripeOffDensityOffset(_lattice->getStatistics().getAverageRho() - (T) 1);
#endif
    }
#ifdef PARALLEL_MODE_MPI
    //clout << iT << " collideStream..3.  " << endl;
    singleton::mpi().barrier();
#endif
    if (!(this->_dualMode)) if (_lattice->getStatistics().getMaxU() > 0.3) {
      clout << "PROBLEM uMax=" << _lattice->getStatistics().getMaxU() << endl;
      _lattice->getStatistics().print(iT, this->_converter->getPhysTime(iT));
//    break;
      exit(-1);
    }


    //clout << iT << " collideStream... OK" << endl;


    // convergence check
    /*//old convergence check
    initConverged<S,T,Lattice>(_lattice, _converge);
    if (iT>itStart &&iT%this->_converter->getLatticeTime(this->_logT)==0) {
    if (iT>itStart &&iT%10==0) {
    if (_converge->hasConverged()) {
      this->_timer->stop();
      this->_timer->printShortSummary();
      break;
    }
    }*/

//    if ( _converge->hasConverged() ) {
//      clout << "Simulation converged." << endl;
//      getResults(iT);
//      this->_timer->stop();
//      this->_timer->printShortSummary();
////      break;
//      exit(-1);
//    }
//    _converge->takeValue( this->_lattice->getStatistics().getAverageEnergy(), true );

  } // end collideAndStream loop

  // time measurement
  this->_timer->stop();
  //timer->printSummary();
  this->_timer->printShortSummary();

  clout << "Solving... OK" << endl;
}




/*
#ifdef ADT
template<typename S, typename T, template<typename U> class Lattice>
void initConverged(SuperLattice3D<ADf<double,NUMBER_DIRECTIONS>, Lattice>* lattice, ValueTracer<S>* converge) {

  converge->takeValue(lattice->getStatistics().getAverageEnergy().v(),true);
}
#endif

template<typename S, typename T, template<typename U> class Lattice>
void initConverged(SuperLattice3D<S, Lattice> *lattice, ValueTracer<S> *converge) {
  converge->takeValue(lattice->getStatistics().getAverageEnergy(), true);
}
*/




/*
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::initForce() {

}

/// initializes all lattice cells with zero velocity
template<typename S, typename T, template<typename U> class Lattice>
void Solver3D<S, T, Lattice>::initVelocity() {

  //clout << "### Initializing velocity ..." << endl;
  //_lattice->defineU(*(this->_bg), 1, *_startVelocity);
  //clout << "### Initializing velocity ... OK" << endl;
}
 */

}

} // namespace olb

#endif
