/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2012 Mathias J. Krause
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/


#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb3D.hh"   // include full template code
#endif

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

typedef double T;
#define DESCRIPTOR D3Q19Descriptor

void prepareGeometry(UnitConverter<T,DESCRIPTOR> const& converter, IndicatorF3D<T>& indicator, SuperGeometry3D<T>& superGeometry) {
  OstreamManager clout(std::cout,"prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  // Sets material number for fluid and boundary
  superGeometry.rename(0,2,indicator);
  superGeometry.rename(2,1,1,1,1);

  T eps = converter.getPhysLength(1);
  Vector<T,3> origin( -eps, converter.getCharPhysLength() - eps, -eps );
  Vector<T,3> extend( converter.getCharPhysLength() + 2*eps, 2*eps, converter.getCharPhysLength() + 2*eps );
  IndicatorCuboid3D<T> lid(extend,origin);

  superGeometry.rename(2,3,1,lid);

  /// Removes all not needed boundary voxels outside the surface
  superGeometry.clean();
  /// Removes all not needed boundary voxels inside the surface
  superGeometry.innerClean();
  superGeometry.checkForErrors();

//  superGeometry.getCuboidGeometry().printStatistics();
  superGeometry.print();
  clout << "Prepare Geometry ... OK" << std::endl;
  return;
}


void prepareLattice(UnitConverter<T,DESCRIPTOR> const& converter,
                    SuperLattice3D<T,DESCRIPTOR>& lattice,
                    Dynamics<T, DESCRIPTOR>& bulkDynamics,
                    sOnLatticeBoundaryCondition3D<T,DESCRIPTOR>& bc,
                    SuperGeometry3D<T>& superGeometry ) {

  OstreamManager clout(std::cout,"prepareLattice");
  clout << "Prepare Lattice ..." << std::endl;

  const T omega = converter.getLatticeRelaxationFrequency();

  /// Material=0 -->do nothing
  lattice.defineDynamics(superGeometry, 0, &instances::getNoDynamics<T, DESCRIPTOR>());

  /// Material=1 -->bulk dynamics
  lattice.defineDynamics(superGeometry, 1, &bulkDynamics);

  /// Material=2 -->bounce back
  //sLattice.defineDynamics(superGeometry, 2, &instances::getBounceBack<T, DESCRIPTOR>());

  /// Material=2,3 -->bulk dynamics, velocity boundary
  lattice.defineDynamics(superGeometry, 2, &bulkDynamics);
  lattice.defineDynamics(superGeometry, 3, &bulkDynamics);
  bc.addVelocityBoundary(superGeometry, 2, omega);
  bc.addVelocityBoundary(superGeometry, 3, omega);

  clout << "Prepare Lattice ... OK" << std::endl;
  return;
}


int main(int argc, char **argv) {

  /// === 1st Step: Initialization ===

  olbInit(&argc, &argv);
  singleton::directories().setOutputDir("./tmp/");
  OstreamManager clout(std::cout,"main");

  int N = 100;
  int k = 1; //blockLattices per process

  if (argc<2) {
    clout << "Usage: ./cavity (no. of cells in one direction)" << std::endl;
    clout << "Set default: N=100" << std::endl;
    clout << "Set default: k=1" << std::endl;
  }
  if (argc>1) N = atoi(argv[1]);
  if (argc>2) k = atoi(argv[2]);

  // LBconverter<T> converter(
  //   (int) 3,                               // dim
  //   (T)   1./N,                        // latticeL_
  //   (T)   1e-2,                            // latticeU_
  //   (T)   1./1000.,                        // charNu_
  //   (T)   1.,                              // charL_ = 1
  //   (T)   1.                               // charU_ = 1
  // );

  UnitConverterFromResolutionAndLatticeVelocity<T, DESCRIPTOR> const converter(
  int {N}, // resolution: number of voxels per charPhysL
  (T)   0.01,
  (T)   1.0, // charPhysLength: reference length of simulation geometry
  (T)   1.0, // charPhysVelocity: maximal/highest expected velocity during simulation in __m / s__
  (T)   0.001, // physViscosity: physical kinematic viscosity in __m^2 / s__
  (T)   1.0 // physDensity: physical density in __kg / m^3__
  );
  converter.print();

  /// === 2nd Step: Prepare Geometry ===

  /// Instantiation of a unit cube by an indicator

  Vector<T,3> origin;
  Vector<T,3> extend(converter.getCharPhysLength());
  IndicatorCuboid3D<T> cube(extend,origin);

  /// Instantiation of a cuboid geometry with weights
  int noCuboids = k*singleton::mpi().getSize();
  if (noCuboids<7) noCuboids = 7;
  CuboidGeometry3D<T> cuboidGeometry(cube, converter.getPhysLength(1), noCuboids);

  /// Instantiation of a load balancer
  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);

  /// Instantiation of a super geometry
  SuperGeometry3D<T> superGeometry(cuboidGeometry, loadBalancer, 1);

  prepareGeometry(converter, cube, superGeometry);


  /// === 3rd Step: Prepare Lattice ===

  SuperLattice3D<T, DESCRIPTOR> sLattice(superGeometry);
  sLattice.statisticsOff();

  BGKdynamics<T, DESCRIPTOR> bulkDynamics (
    converter.getLatticeRelaxationFrequency(), instances::getBulkMomenta<T,DESCRIPTOR>());

  sOnLatticeBoundaryCondition3D<T,DESCRIPTOR> sBoundaryCondition(sLattice);
  createLocalBoundaryCondition3D<T,DESCRIPTOR>(sBoundaryCondition);

  prepareLattice(converter, sLattice, bulkDynamics, sBoundaryCondition, superGeometry);

  /// === 4th Step: Main Loop with Timer ===

  Timer<T> timer(10, (N+1)*(N+1)*(N+1));
  timer.start();

  int iT;
  for (iT=0; iT<10; ++iT) {
    //sLattice.collide();
    //sLattice.stream();
    sLattice.collideAndStream();
  }
  timer.update(9);
  timer.stop();
  timer.printSummary();

  return 0;
}

