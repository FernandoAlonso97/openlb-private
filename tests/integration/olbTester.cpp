/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008, 2016 Jonas Latt, Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Implementation of testcases in OpenLB
 */

#include "olbTester.h"
#include <iostream>

using namespace std;

namespace olb {

namespace tests {

TestExecution::~TestExecution() {
  for (unsigned iTest = 0; iTest < tests.size(); ++iTest) {
    delete tests[iTest];
  }
}

void TestExecution::addTest(OlbTest *newTest) {
  tests.push_back(newTest);
}

void TestExecution::runTests() {
  vector<string> errorTitles, errorMessages;
  for (unsigned iTest = 0; iTest < tests.size(); ++iTest) {
    clout << endl << "Running test \"" << tests[iTest]->getTitle() << "\" ==> ";
    string errorMessage;
    if (tests[iTest]->isSuccessful(errorMessage, clout)) {
      clout << "SUCCESS" << endl;
    }
    else {
      clout << "FAILURE" << endl;
      errorTitles.push_back(tests[iTest]->getTitle());
      errorMessages.push_back(errorMessage);
    }
  }
  clout << endl << "=============================" << endl;
  if (errorTitles.empty()) {
    clout << "All tests have been successful" << endl;
  }
  else {
    clout << "WARNING: " << errorTitles.size() << " test(s) failed:" << endl;
    for (unsigned iError = 0; iError < errorTitles.size(); ++iError) {
      clout << "Test \"" << errorTitles[iError] << "\" failed with error message" << endl;
      clout << "     \"" << errorMessages[iError] << "\"" << endl;
    }
  }
}

}

}  // namespace olb
