/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Jonas Latt, 2016 Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Common interface for test cases in OpenLB
 */
#ifndef OLB_TESTER_H
#define OLB_TESTER_H

#include <string>
#include <vector>
#include <iostream>

#include "io/ostreamManager.h"

// ----------- MACROS --------- //
/// Macro for returning on false condition. To be used in isSuccessful()
#define CHECK(condition, msg) \
if( !(condition) ) { \
  errorMessage = msg; \
  return false; \
}

/// Macro for comparing method_name() of left and right
#define COMPARE_METHODS(left, right, method_name) \
ss.str(""); \
ss << #method_name << "() does not match: " << left.method_name() << " != " << right.method_name() << std::endl; \
CHECK(left.method_name() == right.method_name(), \
        ss.str())

/// Macro for comparing method_name() of left and right (silent)
#define COMPARE_METHODS_SILENT(left, right, method_name) \
ss.str(""); \
ss << #method_name << "() does not match." << std::endl; \
CHECK(left.method_name() == right.method_name(), \
        ss.str())

/// Macro for comparing two objects through their == operator
#define COMPARE_OBJECTS(left, right) \
ss.str(""); \
ss << "the two objects don't match through their == operator." << std::endl; \
CHECK(left == right, ss.str())
//----------------------------//

using namespace std;

namespace olb {

namespace tests {

class OlbMultiTest;

/// Class for specific test case within an OlbTest
class TestCase {
public:
  /// Pointer to the multiTest
  OlbMultiTest *_multiTest;
  /// Error Message
  std::string errorMessage;

  TestCase(OlbMultiTest *multiTest) : _multiTest(multiTest) { };

  /// Returns title of the test case
  virtual std::string getTitle() const = 0;

  /// Returns bool value of success and fills errorMessage if not successful
  virtual bool isSuccessful(std::string &errorMessage, OstreamManager &clout) = 0;
};

class OlbTest {
public:
  virtual ~OlbTest() { }

  /// Returns title of the test case
  virtual std::string getTitle() const = 0;

  virtual bool isSuccessful(std::string &errorMessage, OstreamManager &clout) = 0;
};

class OlbMultiTest : public OlbTest {
public:
  std::vector<TestCase *> testCases;

  virtual ~OlbMultiTest() { };

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout) {
    clout << endl << "-------------------------" << endl;
    for (TestCase *&testCase : testCases) {
      clout << "\tRunning TestCase \"" << testCase->getTitle() << "\" --> ";

      if (!testCase->isSuccessful(errorMessage, clout)) {
        errorMessage = testCase->getTitle() + ": " + errorMessage;
        return false;
      }
      else {
        clout << "SUCCESS" << endl;
      }
    }
    clout << "--------------------------" << endl;
    errorMessage = "";
    return true;
  }

  void addTestCase(TestCase *testCase) {
    testCases.push_back(testCase);
  }
};

class TestExecution {
public:
  mutable OstreamManager clout;

  TestExecution() : clout("Tester") { };

  ~TestExecution();

  void addTest(OlbTest *newTest);

  void runTests();

private:
  std::vector<OlbTest *> tests;
};

}

}  // namespace olb

#endif
