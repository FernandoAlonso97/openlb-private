/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"
#include "poiseuille2dTest.h"
#include <cmath>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace std;

template<typename T, template<typename U> class Lattice>
Poiseuille2D<T,Lattice>::Poiseuille2D( Poiseuille2D<T,Lattice>::BoundaryType boundaryType )
  : converter(2, 0.2, 0.02, 0.1 ),
#ifndef PARALLEL_MODE_MPI  // sequential program execution
    lattice(converter.numNodes(2), converter.numNodes(1) ),
#else                      // parallel program execution
    lattice (
      createRegularDataDistribution( converter.numNodes(2), converter.numNodes(1) ) ),
#endif
    boundaryCondition( createLocalBoundaryCondition2D(lattice) ),
    bulkDynamics( converter.getOmega(),
                  instances::getBulkMomenta() ),
    iT(0)
{
  iniGeometry(boundaryType);
}

template<typename T, template<typename U> class Lattice>
Poiseuille2D<T,Lattice>::~Poiseuille2D() {
  delete boundaryCondition;
}

template<typename T, template<typename U> class Lattice>
T Poiseuille2D<T,Lattice>::poiseuilleVelocity(int iY) const {
  T y = converter.physLength(iY);
  return 4.*converter.getLatticeU() * (y-y*y);
}

template<typename T, template<typename U> class Lattice>
T Poiseuille2D<T,Lattice>::poiseuillePressure(int iX) const {
  T Lx = lattice.getNx()-1;
  T Ly = lattice.getNy()-1;
  return 8.*converter.getLatticeNu()*converter.getLatticeU() / (Ly*Ly) * (Lx/(T)2-(T)iX);
}


template<typename T, template<typename U> class Lattice>
void Poiseuille2D<T,Lattice>::iniGeometry(Poiseuille2D<T,Lattice>::BoundaryType boundaryType) {
  int nx = lattice.getNx();
  int ny = lattice.getNy();
  T   omega = converter.getOmega();
  lattice.defineDynamics(0,nx-1, 0,ny-1, &bulkDynamics);

  boundaryCondition->addVelocityBoundary1P(1,nx-2,ny-1,ny-1, omega);
  boundaryCondition->addVelocityBoundary1N(1,nx-2,   0,   0, omega);

  if (boundaryType==velocity) {
    boundaryCondition->addVelocityBoundary0N(0,0, 1, ny-2, omega);
    boundaryCondition->addVelocityBoundary0P(nx-1,nx-1, 1, ny-2, omega);
  }
  else {
    boundaryCondition->addPressureBoundary0N(0,0, 1, ny-2, omega);
    boundaryCondition->addPressureBoundary0P(nx-1,nx-1, 1, ny-2, omega);
  }

  boundaryCondition->addExternalVelocityCornerNN(0,0, omega);
  boundaryCondition->addExternalVelocityCornerNP(0,ny-1, omega);
  boundaryCondition->addExternalVelocityCornerPN(nx-1,0, omega);
  boundaryCondition->addExternalVelocityCornerPP(nx-1,ny-1, omega);

  for (int iX=0; iX<nx; ++iX) {
    for (int iY=0; iY<ny; ++iY) {
      T u[2] = {poiseuilleVelocity(iY),0.};
      T rho = (T)1 + poiseuillePressure(iX) * Lattice<T>::invCs2;
      lattice.get(iX,iY).defineRhoU(rho, u);
      lattice.get(iX,iY).iniEquilibrium(rho, u);
    }
  }

  lattice.initialize();
}

template<typename T, template<typename U> class Lattice>
void Poiseuille2D<T,Lattice>::iterate(int numIter) {
  for (int iNewIter=0; iNewIter<numIter; ++iNewIter, ++iT) {
    lattice.collideAndStream();
  }
}

template<typename T, template<typename U> class Lattice>
T Poiseuille2D<T,Lattice>::computeError() const {
  T error = T();
  int nx = lattice.getNx();
  int ny = lattice.getNy();
  for (int iX=0; iX<nx; ++iX) {
    for (int iY=0; iY<ny; ++iY) {
      T uAnalytic[2] = {poiseuilleVelocity(iY),0.};
      T uNumeric[2];
      lattice.get(iX,iY).computeU(uNumeric);
      T diff[2];
      diff[0] = uAnalytic[0]-uNumeric[0];
      diff[1] = uAnalytic[1]-uNumeric[1];
      error += diff[0]*diff[0]+diff[1]*diff[1];
    }
  }
  error /= (T)(nx*ny);
  error = sqrt(error);
  return error;
}

string Poiseuille2dTest::getTitle() const {
  return "2D Poiseuille in a 2x1 geometry, with N=50, u=0.01 and Re=10";
}

bool Poiseuille2dTest::isSuccessful(std::string& errorMessage) {
  Poiseuille2D<double, D2Q9Descriptor> simulation (
    Poiseuille2D<double, D2Q9Descriptor>::velocity );
  simulation.iterate(1000);
  if (simulation.computeError() < 0.0001665) {
    return true;
  }
  else {
    errorMessage = "The computed error is larger than expected from previous benchmarks";
    return false;
  }
}
