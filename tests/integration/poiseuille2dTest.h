/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2008 Jonas Latt
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * A list of all test cases
 */
#ifndef POISEUILLE_TEST_2D_H
#define POISEUILLE_TEST_2D_H

#include "olbTester.h"
#include "olb2D.h"
#include "olb3D.h"

namespace olb {

template<typename T, template<typename U> class Lattice>
class Poiseuille2D {
public:
  typedef enum {velocity, pressure} BoundaryType;
public:
  Poiseuille2D(BoundaryType boundaryType);
  ~Poiseuille2D();
  T poiseuilleVelocity(int iY) const;
  T poiseuillePressure(int iX) const;
  void iniGeometry(BoundaryType boundaryType);
  void iterate(int numIter);
  T computeError() const;
private:
  LBconverter<T> converter;
#ifndef PARALLEL_MODE_MPI // sequential program execution
  BlockLattice2D<T,Lattice> lattice;
#else
  MultiBlockLattice2D<T,Lattice> lattice;
#endif
  OnLatticeBoundaryCondition2D<T,Lattice>* boundaryCondition;
  BGKdynamics<T,Lattice> bulkDynamics;
  int iT;
};

class Poiseuille2dTest : public OlbTest {
  std::string getTitle() const;
  bool isSuccessful(std::string& errorMessage);
};

}

#endif
