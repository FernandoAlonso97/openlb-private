/*  Lattice Boltzmann sample, written in C++, using the OpenLB
 *  library
 *
 *  Copyright (C) 2016 Mathias J. Krause, Marie-Luise Maier
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

// based on sedimentation example in Zhang et al. 2015, PowderTechnology 272
#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used
#include "olb3D.hh"     // Include full template code
#endif

#include <vector>
#include <memory>
#include <cmath>
#include <iostream>
#include <fstream>
#include <cmath>

#include "txtfileOutput.h"

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

#define DESCRIPTOR ForcedD3Q19Descriptor
#define PARTICLE Particle3D

typedef double T;

//#define simulateFluid2Way
//#define zhang // for particle force instead of stokesDragForce regular
//#define functorFields
//#define forceZhangcpp

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

T mu_fluid;
const int overlap = 2;

// by using getFloorLatticeR for particle positions be aware of chosen overlap -> has at least to be +1
//if I use information from number x neighbor points of my particle position
// and I refer particle pos to getFloorLatticeR nodeF, then it can occur that this nodeF lies in neighbor cuboid
// and therefore overlap has to be number x + 1 !!
// no problem if I use getLatticeR(pPos) instead of getFloorLatticeR(pPos)
int iT = 0;
T particleMaxPhysT = 10.;
//const int timeResolution = 1;
const T timeResolution = 1.;
std::vector<T> _fBetaL(19, T());

template<typename T>
class SedimentationVelocity3D {
public:
  SedimentationVelocity3D(T pRadius, T mu, T pDensity, T fDensity,
      std::vector<T> weightDirection, T g = 9.80665) :
      _pRadius(pRadius), _mu(mu), _pDensity(pDensity), _fDensity(fDensity), _weightDirection(
          weightDirection), _g(g) {
    _sediVel.resize(3);
    for (int j = 0; j < 3; ++j) {
      _sediVel[j] = 4. / (18. * _mu) * (_pDensity - _fDensity) * _g
          * _weightDirection[j] * pow(_pRadius, 2);
    }
    _tau = 4. * _pRadius * _pRadius * _pDensity / 18. / _mu;
  }
  std::vector<T> getSediVelo3D() {
    return _sediVel;
  }
  bool operator()(T output[], T input) {
    output[0] = _sediVel[0] * (1 - exp(-input / _tau));
    output[1] = _sediVel[1] * (1 - exp(-input / _tau));
    output[2] = _sediVel[2] * (1 - exp(-input / _tau));
    return true;
  }
  T getTau() {
    return _tau;
  }
private:
  T _pRadius, _mu, _pDensity, _fDensity, _g;
  std::vector<T> _sediVel;
  std::vector<T> _weightDirection;
  T _tau;
};

T getRadFromStokes(LBconverter<T> const& converter, T stokes, T pRho) {
  return std::sqrt(
      stokes * 9. * converter.getDynamicViscosity() * converter.getCharL()
          / (2. * pRho * converter.getCharU()));
}

//template<typename T, template<typename U> class PARTICLE>
void addFluidForce(
    SuperLatticeInterpDensity3Degree3D<T, DESCRIPTOR>& _interpLDensity,
#ifdef functorFields
    SuperLatticeExtrapolatePoly3D<T, DESCRIPTOR>& _interpWeights,
#endif
    SuperParticleSystem3D<T, PARTICLE> spSys, LBconverter<T> const& _conv,
    SuperLattice3D<T, DESCRIPTOR>& _sLattice, SuperGeometry3D<T>& _sGeometry,
    SuperExternal3D<T, DESCRIPTOR> _exField, T &intPosOut, std::vector<T> uSedi,
    T timedependency, T pDensity) {
  int intPos[3] = { 0 };
  T physIntPos[3] = { T() };

  for (int iC = 0; iC < _sLattice.getCuboidGeometry().getNc(); ++iC) {
    // \param _rank  indicates the processing thread of a global cuboid number,
    // getRank() Returns the process ID
    if (_sLattice.getLoadBalancer().rank(iC) == singleton::mpi().getRank()) {
      // \param _loc indicates local cuboid number in actual thread, for given global cuboid number
      // this is to get appropriate particle system
      int locIC = _sLattice.getLoadBalancer().loc(iC);
      // particle collection in this particle system together with particles in overlap region of neighbor
      // cuboid of thread
      T size = spSys.getPSystems()[locIC]->sizeInclShadow();

      for (int pNum = 0; pNum < size; pNum++) {

        T physPRad = spSys.getPSystems()[locIC]->operator[](pNum).getRad();
        // pNum is always, if shadow or not, within the cuboid! At least at its border.

        // by using getFloorLatticeR for particle positions be aware of chosen overlap -> has at least to be +1
        // if not, with getFloorLatticeR rounding it can occur that referred to lattice position is not in cuboid or its overlap
        //        _sLattice.getCuboidGeometry().get(iC).getFloorLatticeR(intPos,
        //            &(spSys.getPSystems()[locIC]->operator[](pNum).getPos()[0]));
        _sLattice.getCuboidGeometry().get(iC).getLatticeR(intPos,
            &(spSys.getPSystems()[locIC]->operator[](pNum).getPos()[0]));
        _sLattice.getCuboidGeometry().get(iC).getPhysR(physIntPos, intPos);

        T fAlphaL[DESCRIPTOR<T>::q] = { T() };
        // Lagrange interpolated polynomial for density on node
        // (?) third order, if three nodes are considered, here: fourth order.
        _interpLDensity(fAlphaL,
            &(spSys.getPSystems()[locIC]->operator[](pNum).getPos()[0]),
            spSys.getPSystems()[locIC]->operator[](pNum).getCuboid());


        T uP[3] = { T() };
        uP[0] = _conv.latticeVelocity(
            spSys.getPSystems()[locIC]->operator[](pNum).getVel()[0]);
        uP[1] = _conv.latticeVelocity(
            spSys.getPSystems()[locIC]->operator[](pNum).getVel()[1]);
        uP[2] = _conv.latticeVelocity(
            spSys.getPSystems()[locIC]->operator[](pNum).getVel()[2]);

        T rhoL = T(1);
        T gF[3] = { T() }; // force density gF

        T latticeL = _conv.getLatticeL();

        T lFU[3] = { T() };
        T sp;
        T lPU[3] = { T() };

        for (unsigned iPop = 0; iPop < DESCRIPTOR<T>::q; ++iPop) {

          // Get direction
          const int* ci = DESCRIPTOR<T>::c[iPop];
          // Get f_q of next fluid cell where l = opposite(q)

          T fBeta[DESCRIPTOR<T>::q] = { T() };
          T fBetaPosition[3] = { T() };
          fBetaPosition[0] =
              spSys.getPSystems()[locIC]->operator[](pNum).getPos()[0]
                  + latticeL * ci[0];
          fBetaPosition[1] =
              spSys.getPSystems()[locIC]->operator[](pNum).getPos()[1]
                  + latticeL * ci[1];
          fBetaPosition[2] =
              spSys.getPSystems()[locIC]->operator[](pNum).getPos()[2]
                  + latticeL * ci[2];

          _interpLDensity(fBeta, fBetaPosition,
              spSys.getPSystems()[locIC]->operator[](pNum).getCuboid());
          T fi = fBeta[iPop];
          // Get f_l of the boundary cell
          // Add f_q and f_opp = f_l
          fi += fAlphaL[util::opposite<DESCRIPTOR<T> >(iPop)];

#ifdef forceZhangcpp
          // Update force
          gF[0] += ci[0]* (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2*
              (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
          gF[1] += ci[1]* (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2*
              (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
          gF[2] += ci[2] * (fi - 2 * DESCRIPTOR<T>::t[iPop] * rhoL * DESCRIPTOR<T>::invCs2*
              (ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2]));
#else

          /// physical velocity (factor 0.5 because of 6 in --> 6*M_PI*...
          lFU[0] = ci[0] * 0.5 * fi;
          lFU[1] = ci[1] * 0.5 * fi;
          lFU[2] = ci[2] * 0.5 * fi;

          // point product
          sp = ci[0] * uP[0] + ci[1] * uP[1] + ci[2] * uP[2];
          lPU[0] = ci[0] * rhoL * DESCRIPTOR<T>::invCs2 * DESCRIPTOR<T>::t[iPop]
              * sp;
          lPU[1] = ci[1] * rhoL * DESCRIPTOR<T>::invCs2 * DESCRIPTOR<T>::t[iPop]
              * sp;
          lPU[2] = ci[2] * rhoL * DESCRIPTOR<T>::invCs2 * DESCRIPTOR<T>::t[iPop]
              * sp;

          gF[0] += lFU[0] - lPU[0];
          gF[1] += lFU[1] - lPU[1];
          gF[2] += lFU[2] - lPU[2];
#endif
        }

#ifdef functorFields
        _interpWeights(
            &(spSys.getPSystems()[locIC]->operator[](pNum).getPos()[0]), gF,
            spSys.getPSystems()[locIC]->operator[](pNum).getRad(), iC);
#else

        // delta weights neighboring nodes, summed up equals 1!
        T delta = T(1);
        T checkDelta = T();
        T a, b, c = T();
        T F[3] = {T()};
        T Fges[3] = {T()};

        T gF0Delta, gF1Delta, gF2Delta;

        for (int i = -1; i <= 2; ++i) {
          for (int j = -1; j <= 2; ++j) {
            for (int k = -1; k <= 2; ++k) {
              delta = T(1);
              // a, b, c in lattice units cause physical ones get cancelled
              a = (physIntPos[0] + i * _conv.getLatticeL()
                  - spSys.getPSystems()[locIC]->operator[](pNum).getPos()[0])
              / _conv.getLatticeL();
              b = (physIntPos[1] + j * _conv.getLatticeL()
                  - spSys.getPSystems()[locIC]->operator[](pNum).getPos()[1])
              / _conv.getLatticeL();
              c = (physIntPos[2] + k * _conv.getLatticeL()
                  - spSys.getPSystems()[locIC]->operator[](pNum).getPos()[2])
              / _conv.getLatticeL();

              // the for loops already define that a, b, c are smaller than 2
              delta *= 1. / 4 * (1 + cos(M_PI * a / 2.));
              delta *= 1. / 4 * (1 + cos(M_PI * b / 2.));
              delta *= 1. / 4 * (1 + cos(M_PI * c / 2.));

#ifdef forceZhangcpp
              // is this correct radius? -  note latL scaling
              T pRad = _conv.latticeLength(
                  spSys.getPSystems()[locIC]->operator[](pNum).getRad());

              // MJK
              F[0] = -gF[0]*delta*M_PI*pRad*pRad;
              F[1] = -gF[1]*delta*M_PI*pRad*pRad;
              F[2] = -gF[2]*delta*M_PI*pRad*pRad;

#else
              // mu_fluid in N*s/m^2 --> change to lattice units
              F[0] = 3*_conv.getCharNu()/physPRad* physPRad *physPRad* gF[0] * delta* M_PI
              *_conv.getCharRho()*_conv.physVelocity()*_conv.latticeForce()
              ;
              F[1] = 3*_conv.getCharNu()/physPRad* physPRad *physPRad* gF[1] * delta* M_PI
              *_conv.getCharRho()*_conv.physVelocity()*_conv.latticeForce()
              ;
              F[2] = 3*_conv.getCharNu()/physPRad* physPRad *physPRad* gF[2] * delta* M_PI
              *_conv.getCharRho()*_conv.physVelocity()*_conv.latticeForce()
              ;
#endif
              Fges[0] += F[0];
              Fges[1] += F[1];
              Fges[2] += F[2];

              if (_sGeometry.getBlockGeometry(locIC).getMaterial(intPos[0] + i,
                      intPos[1] + j, intPos[2] + k) == 1) {
                _sLattice.getBlockLattice(locIC).get(intPos[0] + i,
                    intPos[1] + j, intPos[2] + k).addExternalField(
                    DESCRIPTOR<T>::ExternalField::forceBeginsAt,
                    DESCRIPTOR<T>::ExternalField::sizeOfForce, F);
              }
            }
          }
        }
#endif
      }
    }
  }
}

void prepareGeometry(LBconverter<T> const& converter,
    IndicatorF3D<T>& indicator, T cuboidWidth, IndicatorF3D<T>& extendedDomain,
    SuperGeometry3D<T>& superGeometry) {

  OstreamManager clout(std::cout, "prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0, 2, extendedDomain);
  superGeometry.rename(2, 1, indicator);

  std::vector<T> origin = superGeometry.getStatistics().getMinPhysR(1);
  std::vector<T> extend = superGeometry.getStatistics().getPhysExtend(1);
  std::vector<T> center = superGeometry.getStatistics().getCenterPhysR(1);
  center[2] = origin[2];

  /*
   // inflow
   IndicatorCuboid3D<T> inflow(
   superGeometry.getStatistics().getPhysExtend(1)[0]
   + 2 * converter.getLatticeL(),
   superGeometry.getStatistics().getPhysExtend(1)[1]
   + 2 * converter.getLatticeL(), converter.getLatticeL(), center);
   //  superGeometry.rename(1, 2, inflow);

   // outflow
   center[2] = superGeometry.getStatistics().getMaxPhysR(1)[2];
   IndicatorCuboid3D<T> outflow(
   superGeometry.getStatistics().getPhysExtend(1)[0]
   + 2 * converter.getLatticeL(),
   superGeometry.getStatistics().getPhysExtend(1)[1]
   + 2 * converter.getLatticeL(), converter.getLatticeL(), center);
   //  superGeometry.rename(1, 2, outflow);
   */

  superGeometry.clean();
  superGeometry.innerClean();
  superGeometry.checkForErrors();
  superGeometry.print();

  clout << "Prepare Geometry ... OK" << std::endl;
}

/// Set up the geometry of the simulation
void prepareLattice(SuperLattice3D<T, DESCRIPTOR>& sLattice,
    LBconverter<T> const& converter, SuperGeometry3D<T>& superGeometry,
    Dynamics<T, DESCRIPTOR>& bulkDynamics) {

  OstreamManager clout(std::cout, "prepareLattice");
  clout << "Prepare Lattice ..." << std::endl;

  /// Material=0 -->do nothing
  sLattice.defineDynamics(superGeometry, 0,
      &instances::getNoDynamics<T, DESCRIPTOR>());

  /// Material=1 -->bulk dynamics
  sLattice.defineDynamics(superGeometry, 1, &bulkDynamics);

  // Material=2 -->bulk dynamics if periodic
  sLattice.defineDynamics(superGeometry, 2,
      &instances::getBounceBack<T, DESCRIPTOR>());

  clout << "Prepare Lattice ... OK" << std::endl;
}

/// Generates a slowly increasing inflow for the first iTMaxStart timesteps
void setBoundaryValues(SuperLattice3D<T, DESCRIPTOR>& sLattice,
    SuperGeometry3D<T>& superGeometry) {

  OstreamManager clout(std::cout, "setBoundaryValues");
  if (iT == 0) {
    clout << "Set initial conditions ..." << std::endl;
    /// Initial conditions
    AnalyticalConst3D<T, T> rhoF(1);
    std::vector<T> velocity(3, T());
    AnalyticalConst3D<T, T> uF(velocity);

    // Initialize all values of distribution functions to their local equilibrium
    // Defines an external field (Dichte*Geschwindigkeit) on a domain with a particular material number
    sLattice.defineRhoU(superGeometry, 1, rhoF, uF);
    // Initializes by equilibrium on a domain with a particular material number
    sLattice.iniEquilibrium(superGeometry, 1, rhoF, uF);

    /// Make the lattice ready for simulation
    sLattice.initialize();

    clout << "Set initial conditions ... ok" << std::endl;
  }
}

void getResults(SuperLattice3D<T, DESCRIPTOR>& sLattice,
    LBconverter<T>& converter,
    SuperParticleSystem3D<T, PARTICLE>& spSys,
    SuperParticleSysVtuWriter<T, PARTICLE>& particleOut,
    int iT, int itMax, int itMaxStart, int console, int vtkIter,
    T maxPhysT, SuperGeometry3D<T>& superGeometry,
    Timer<double>& fluidTimer, const int noOfCuboids, const T analyticVel, const T expVel) {

  OstreamManager clout(std::cout, "getResults");
  sLattice.communicate();
  SuperVTKwriter3D<T, T> vtkWriter("sedimentation");
  SuperLatticePhysVelocity3D<T, DESCRIPTOR> velocity(sLattice, converter);
  SuperLatticeExternal3D<T, DESCRIPTOR> external(sLattice,
  DESCRIPTOR<T>::ExternalField::forceBeginsAt,
  DESCRIPTOR<T>::ExternalField::sizeOfForce);
  SuperLatticePhysPressure3D<T, DESCRIPTOR> pressure(sLattice, converter);
  SuperLatticeDensity3D<T, DESCRIPTOR> density(sLattice);

  vtkWriter.addFunctor(velocity);
  vtkWriter.addFunctor(external);
  vtkWriter.addFunctor(pressure);
  vtkWriter.addFunctor(density);

  if (iT == 0) {
    /// Writes the converter log file
    writeLogFile(converter, "sedimentation");
    /// Writes the geometry, cuboid no. and rank no. as vti file for visualization
    SuperLatticeGeometry3D<T, DESCRIPTOR> geometry(sLattice, superGeometry);
    SuperLatticeCuboid3D<T, DESCRIPTOR> cuboid(sLattice);
    SuperLatticeRank3D<T, DESCRIPTOR> rank(sLattice);
    vtkWriter.write(geometry);
    vtkWriter.write(cuboid);
    vtkWriter.write(rank);
    vtkWriter.createMasterFile();

    clout << "itMax=" << itMax << "; itMaxStart=" << itMaxStart << "; console="
        << console << std::endl;
  }
  /// Writes the pvd files
  if ((iT % (vtkIter/10) == 0 && iT < vtkIter) || iT % vtkIter == 0) {
    vtkWriter.write(iT);
    particleOut.write(iT);
  }

  /// Gnuplot constructor (must be static!)
  /// for real-time plotting: gplot("name", true) // experimental!
  static Gnuplot gplot("drag");

  /// write pdf at last time step
  if (iT == converter.numTimeSteps(maxPhysT)-1) {
    /// writes pdf
    gplot.writePDF();
  }

  if (iT % console == 0) {
    /// Timer console output
    //fluidTimer.update(iT);
    //fluidTimer.printStep();
    /// Lattice statistics console output
    sLattice.getStatistics().print(iT, converter.physTime(iT));
    /// set data for gnuplot: input={xValue, yValue(s), names (optional), position of key (optional)}
    gplot.setData(converter.physTime(iT), {expVel, analyticVel}, {"Particle velocity[2] (m/s)", "Analytical velocity[2] (m/s)"}, "bottom right");
    /// writes a png in one file for every timestep, if the file is open it can be used as a "liveplot"
    gplot.writePNG();

    /// every (iT%vtkIter) write an png of the plot
    if (iT%(vtkIter) == 0) {
      /// writes pngs: input={name of the files (optional), x range for the plot (optional)}
      gplot.writePNG(iT, maxPhysT);
    }
  }
}

// overrides typical converter for custom command line parameters not read from xml
template<typename T>
LBconverter<T>* createLBconverter(XMLreader const& params, T latticeU,
    T latticeL) {
  OstreamManager clout(std::cout, "createLBconverter");

  int dim = 0;
  T charNu = T(0);
  T Re = T(0);
  T charL = T(1);
  T charU = T(1);
  T charRho = T(1);
  T pressureLevel = T(0);
  bool verbose = false;
  params.setWarningsOn(false);
  // params[parameter].read(value) sets the value or returns false if the parameter can not be found

  if (!params["Application"]["dim"].read<int>(dim, verbose)) {
    clout << "Error: Cannot read parameter from Xml-file: dim" << std::endl;
    exit(1);
  }
  if (!params["Application"]["PhysParam"]["charL"].read(charL, verbose)) {
    clout << "Parameter charL not found in Xml-file. Set default: charL = 1."
        << std::endl;
  }
  if (!params["Application"]["PhysParam"]["charU"].read(charU, verbose)) {
    clout << "Parameter charU not found in Xml-file. Set default: charU = 1."
        << std::endl;
  }
  if (!params["Application"]["PhysParam"]["charRho"].read(charRho, verbose)) {
    clout
        << "Parameter charRho not found in Xml-file. Set default: charRho = 1."
        << std::endl;
  }
  if (!params["Application"]["PhysParam"]["charPressure"].read(pressureLevel,
      verbose)) {
    clout
        << "Parameter charPressure not found in Xml-file. Set default: charPressure = 0."
        << std::endl;
  }
  if (!params["Application"]["PhysParam"]["charNu"].read(charNu, verbose)) {
    if (!params["Application"]["PhysParam"]["Re"].read(Re, verbose)) {
      clout << "Error: Cannot read neither charNu nor Re from Xml-file."
          << std::endl;
      exit(1);
    }
    charNu = charL * charU / Re;
  }
  params.setWarningsOn(true);

  return new LBconverter<T>(dim, latticeL, latticeU, charNu, charL, charU,
      charRho, pressureLevel);
}

int main(int argc, char* argv[]) {
  /// === 1st Step: Initialization ===

  // in olbInit: initialization of instance of
  // friend MpiManager& mpi() created and mpi().init(argc, argv);
  olbInit(&argc, &argv);

  OstreamManager clout(std::cout, "main");

  string fName("sedimentation.xml");
  XMLreader config(fName);
  LBconverter<T>* converter;

  std::string partOut;
  std::string outputdir;
  T lU, lL;
  T pRadius = 1e-4;  // [m]
  if (argc == 6) { // if input is given through command line args
    clout << "Read args" << std::endl;
    outputdir = std::string(argv[1]);
    partOut = std::string(argv[2]);
    istringstream ss1(argv[3]);
    if (!(ss1 >> lU))
      cerr << "Invalid number " << argv[3] << '\n';
    istringstream ss2(argv[4]);
    if (!(ss2 >> lL))
      cerr << "Invalid number " << argv[4] << '\n';
    lL = lL * 1e-6; // factor for convenience
    istringstream ss3(argv[5]);
    if (!(ss3 >> pRadius))
      cerr << "Invalid number " << argv[5] << '\n';
    pRadius = pRadius * 1e-7; // factor for convenience
    converter = createLBconverter<T>(config, lU, lL);
    clout << "pRadius: " << pRadius << std::endl;
    clout << "latticeU: " << lU << std::endl;
    clout << "latticeL: " << lL << std::endl;
  } else { // else read in from xml only
    converter = createLBconverter<T>(config);
    config["Output"]["PartOutput"].read(partOut);
    config["Application"]["PartParam"]["Radius"].read(pRadius);
    config["Output"]["OutputDir"].read(outputdir);
  }

  // singleton means global, publicly available information
  singleton::directories().setOutputDir("./tmp/");


  bool multiOutput = false;
  config["Output"]["MultiOutput"].read(multiOutput);
  // enable message output for all MPI processes, disabled by default
  clout.setMultiOutput(multiOutput);

  std::string olbdir;
  config["Application"]["OlbDir"].read(olbdir);

  singleton::directories().setOlbDir(olbdir);
  singleton::directories().setOutputDir(outputdir);

  bool comments;
  config["Output"]["OutputComments"].read(comments);
  bool pConsoleOut;
  config["Output"]["ParticleConsole"].read(pConsoleOut);

  T pDensity; // [kg/m^3}
  config["Application"]["PartParam"]["Density"].read(pDensity);

  // set max iteration times
  T maxPhysT;
  config["Application"]["PhysParam"]["MaxTime"].read(maxPhysT);
  T maxPhysStartT;
  config["Application"]["PhysParam"]["StartTime"].read(maxPhysStartT);

  T cuboidLength;
  config["Application"]["Geometry"]["DefaultCuboidLength"].read(cuboidLength);
  T widthRatio;
  config["Application"]["Geometry"]["WidthRatio"].read(widthRatio);
  T cuboidWidth = cuboidLength * widthRatio;

  //TODO can be changed to see particle touching ground
//  cuboidLength /= 2.;

  // settling direction
  std::vector<T> direction(3, T());
  direction[2] = -1.;

  mu_fluid = converter->getDynamicViscosity();

  // settling velocity
  SedimentationVelocity3D<T> sediVel(pRadius, converter->getDynamicViscosity(),
      pDensity, converter->getCharRho(), direction);
  const T tau = sediVel.getTau();
  const std::vector<T> uSedi = sediVel.getSediVelo3D();

  const int itMax = converter->numTimeSteps(maxPhysT);
  const int itMaxStart = converter->numTimeSteps(maxPhysStartT);
  int console = converter->numTimeSteps(maxPhysT / 100.);
  int vtkIter = converter->numTimeSteps(maxPhysT / 1000.);

  converter->print();

/// === 2nd Step: Prepare Geometry ===

  // Cuboid as Channel
  std::vector<T> center(3, T());
  center[0] = cuboidWidth / 2.;
  center[1] = cuboidWidth / 2.;
  center[2] = cuboidLength / 2.;

  std::vector<T> length(3, T());
  length[0] = cuboidWidth;
  length[1] = cuboidWidth;
  length[2] = cuboidLength;

  IndicatorCuboid3D<T> cuboid(length[0], length[1], length[2], center);
  IndicatorIdentity3D<T> frame(cuboid);
  IndicatorLayer3D<T> extendedDomain(frame, converter->getLatticeL());

  // Returns the number of processes
  int noOfCuboids = std::max(1, singleton::mpi().getSize());

  // CuboidGeometry3D constructs a cuboid structure with a uniform spacing of voxelSize
  // which consists of nC=noOfCuboids cuboids, the cuboids not needed are removed
  // and too big ones are shrinked
  // 2nd parameter: cell size
  CuboidGeometry3D<T> cuboidGeometry(extendedDomain, converter->getLatticeL(),
      noOfCuboids);

  /// Instantiation of an empty loadBalancer
  // Constructs a load balancer from a given cuboid geometry using a heurist.:
  // cuboid geometry to base the load balance on
  // blockGeometry used to determine number of full and empty cells if given
  // ratioFullEmpty time it takes for full cells in relation to empty cells
  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);
  /*  Sketch: assume we have 6 cuboids and 2 threads. Thread number 1 owns cuboid 0 and 1.
   *  Thread number 2 owns cuboid 2, 3, 4 and 5.
   *  Then we get the following configuration:
   *
   *  global cuboid number:               0   1   2   3   4   5
   *  local cuboid number of thread 0:    0   1
   *  local cuboid number of thread 1:            0   1   2   3
   *
   *  \param _glob  is a vector from 0,1,...,numberOfThreads-1
   *  \param _loc   indicates local cuboid number in actual thread, for given global cuboid number
   *  \param _rank  indicates the processing thread of a global cuboid number (so 0 or 1)
   *  */

  /// Default instantiation of superGeometry
  // has one latticeL less than extendedDomain
  SuperGeometry3D<T> superGeometry(cuboidGeometry, loadBalancer, overlap);
  // change overlap to avoid problems at cuboid border
  // overlap for superLattice, superGeometry--> superStructure = 2

  prepareGeometry(*converter, frame, cuboidWidth, extendedDomain,
      superGeometry);

  /// === 3rd Step: Prepare Lattice ===

  SuperLattice3D<T, DESCRIPTOR> sLattice(superGeometry);

  ForcedBGKdynamics<T, DESCRIPTOR> bulkDynamics(converter->getOmega(),
      instances::getBulkMomenta<T, DESCRIPTOR>());

  prepareLattice(sLattice, *converter, superGeometry, bulkDynamics);

  /// === 3.1st Step: Prepare Particles ===
  SuperParticleSystem3D<T, PARTICLE> spSys(cuboidGeometry, loadBalancer,
      superGeometry, *converter);
  // spSys overlap is factor multiplied to deltaX and important for the
  // communication of the cuboids, that neighbor knows particles in overlap region on origin cuboid
  // overlap setting works as in superGeometry set overlap!
  spSys.setOverlap(T(sLattice.getOverlap())); // für Schattenpartikel



  std::vector<T> origin = superGeometry.getStatistics().getCenterPhysR(1);
  origin[2] = superGeometry.getStatistics().getMaxPhysR(1)[2]
      - superGeometry.getStatistics().getPhysExtend(1)[2] / 8.;

  origin[1] += .5 * converter->getLatticeL();
  origin[0] += .5  * converter->getLatticeL();

  std::vector<T> iVelo = { T(0), T(0), T(0) };
  PARTICLE<T> par(origin, iVelo, 4 / 3. * M_PI * pow(pRadius, 3) * pDensity,
      pRadius, 0);
  // info: particle generation has to be before of the forces application
  spSys.addParticle(par);

  T givenRad = pRadius;
  T particleRe = 2. * givenRad * uSedi[2] / converter->getCharNu();
  clout << "Particle added, velocity: " << par.getVel()[2]
      << "\n calculated St from givenRad: "
      << spSys.getStokes(*converter, pDensity, givenRad)
      << "\n calculated St with u_particle: "
      << pDensity * std::pow(2. * givenRad, 2) * uSedi[2]
          / (18. * givenRad * (converter->getCharNu() * converter->getCharRho()))
      << "\n calculated St with Re: "
      << 2. / 9. * pDensity / converter->getCharRho() * converter->getRe()
      << "\n density: " << pDensity << "\n getCharRho: "
      << converter->getCharRho() << "\n getRe: " << converter->getRe()
      << "\n particleRe: " << particleRe
      << "\n calculated St with particleRe: "
      << 2. / 9. * pDensity / converter->getCharRho() * particleRe
      << "\n Particle Reynold's number: "
      << 2 * pRadius * sediVel.getSediVelo3D()[2]
          / converter->getDynamicViscosity() * converter->getCharRho()
      << "\n particle radius  : " << pRadius << "\n particle density: "
      << pDensity << "\n particle mass   : "
      << 4. / 3 * M_PI * pow(pRadius, 3) * pDensity << "\n sediVelocity[0] : "
      << uSedi[0] << "\n sediVelocity[1] : " << uSedi[1]
      << "\n sediVelocity[2] : " << uSedi[2] << "\n tau particle    : " << tau
      << "\n cuboid length   : " << cuboidLength << "\n cuboidWidth     : "
      << cuboidWidth << "\n max phys time   : " << maxPhysT
      << "\n phys start time : " << maxPhysStartT << std::endl;

  auto weightForce = make_shared < WeightForce3D<T, PARTICLE> > (direction);
  spSys.addForce(weightForce);

  auto buoyancyForce = make_shared < BuoyancyForce3D<T, PARTICLE, DESCRIPTOR>
      > (*converter, direction);
  spSys.addForce(buoyancyForce);

  SuperLatticeInterpPhysVelocity3D<T, DESCRIPTOR> getVel(sLattice, *converter);
  SuperLatticeInterpPhysVelocity3Degree3D<T, DESCRIPTOR> getVel3Degree(sLattice,
      *converter);
  SuperLatticeInterpDensity3Degree3D<T, DESCRIPTOR> latticeDensity(sLattice,
      *converter);
#ifdef functorFields
  SuperLatticeExtrapolatePoly3D<T, DESCRIPTOR> latticeWeights(sLattice,
      superGeometry, *converter);
#endif

#ifdef zhang
  auto stokesForce = make_shared
  < StokesDragForceZhang3D<T, PARTICLE, DESCRIPTOR>
  > (latticeDensity, sLattice, *converter);
#else
  auto stokesForce = make_shared < StokesDragForce3D<T, PARTICLE, DESCRIPTOR>
      > (getVel, *converter);
#endif
  spSys.addForce(stokesForce);

  std::set<int> boundMaterial = { 2 };

  SuperParticleSysVtuWriter<T, PARTICLE> particleOut(spSys, "particle",
      SuperParticleSysVtuWriter<T, PARTICLE>::particleProperties::velocity
          | SuperParticleSysVtuWriter<T, PARTICLE>::particleProperties::radius);

  T timedependency[3] = { T() };
  T partVel[3] = { T() };

  std::vector<T> timedependingSediVel = { T(), T(), T() };

  std::string filename = outputdir.substr(2, outputdir.size() - 2);
  filename.append(partOut);
  filename.append(".txt");
  clout << filename << std::endl;

  std::ofstream outputFile;
  TxtFileOutput<T, PARTICLE> txtOut(filename.c_str(), outputFile, uSedi, tau,
      timedependingSediVel, *converter, maxPhysT,
      converter->numTimeSteps(maxPhysT), 0,
      TxtFileOutput<T, PARTICLE>::particleProperties::position
          | TxtFileOutput<T, PARTICLE>::particleProperties::velocity
          | TxtFileOutput<T, PARTICLE>::particleProperties::radius, spSys, comments);

/// === 4th Step: Main Loop with Timer ===
  Timer<double> fluidTimer(converter->numTimeSteps(maxPhysT),
      superGeometry.getStatistics().getNvoxel());

  // A super external field is needed to communicate values of the external field
  SuperExternal3D<T, DESCRIPTOR> exField(superGeometry, sLattice, 0, 3,
      sLattice.getOverlap());

  T intPos_test = T();

  AnalyticalConst3D<T, T> zeroAnalytical(0.);
  AnalyticalComposed3D<T, T> zeroField(zeroAnalytical, zeroAnalytical,
      zeroAnalytical);

  for (iT = 0; iT < converter->numTimeSteps(maxPhysT); ++iT) {

    /// === 5th Step: Definition of Initial and Boundary Conditions ===

    setBoundaryValues(sLattice, superGeometry);

#ifdef simulateFluid2Way
    /// === 6th Step: Collide and Stream Execution ===
    // Apply LB collision to the cell according to local dynamics.
    // first collide on bulk
    sLattice.collideAndStream(); // reihenfolge passt laut Niu
    // resets external field
    sLattice.defineExternalField(superGeometry, 1,
    DESCRIPTOR<T>::ExternalField::forceBeginsAt,
    DESCRIPTOR<T>::ExternalField::sizeOfForce, zeroField);

    // checks if mass is conserved
    sLattice.stripeOffDensityOffset(
        sLattice.getStatistics().getAverageRho() - (T) 1);
    //communicates lattice values over cuboid boundaries
    sLattice.communicate();
    //communicates force values over cuboid boundaries,
    // important for periodic boundaries
    exField.communicate();
#endif
    txtOut.writeFile(iT);

    spSys.simulate(converter->physTime());

    sediVel(timedependency, T(iT + 1) * converter->physTime());
    timedependingSediVel = {timedependency[0], timedependency[1],
      timedependency[2]
    };

#ifdef simulateFluid2Way
    addFluidForce(latticeDensity,
#ifdef functorFields
        latticeWeights,
#endif
        spSys, *converter, sLattice, superGeometry, exField, intPos_test, uSedi,
        timedependency[2], pDensity);
#endif


    // only works for multioutput = true
    // (particle system is on one rank)
    for (auto pS : spSys.getParticleSystems()) {
      for (auto p : pS->getParticles()) {
        partVel[0] = p.getVel()[0];
        partVel[1] = p.getVel()[1];
        partVel[2] = p.getVel()[2];
        if (pConsoleOut) {
          if (iT % (console / 10) == 0) {
            clout << "Particle velocity[2]: "
                << p.getVel()[2] << ", Time-dependent analytical sol.[2]: "
                << timedependency[2]<< ", Relative error: ";
            if (timedependency[2] != T()) {
              clout << (p.getVel()[2] - timedependency[2])/timedependency[2] << std::endl;
            } else {
              clout << "undefined" << std::endl;
            }
          }
        }
      }
    }

    getResults(sLattice, *converter, spSys, particleOut, iT, itMax, itMaxStart,
        console, vtkIter, maxPhysT, superGeometry, fluidTimer, noOfCuboids, timedependency[2], partVel[2]);


  }

  fluidTimer.stop();
  fluidTimer.printSummary();

}
