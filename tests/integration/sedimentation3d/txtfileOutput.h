#include "olb3D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used
#include "olb3D.hh"     // Include full template code
#endif

#include <iostream>
#include <fstream>
#include <cmath>

template<typename T, template<typename U> class PARTICLETYPE>
class TxtFileOutput {

public:
  TxtFileOutput(const char *filename, std::ofstream& file,
      std::vector<T> sediVel, T tau,
      std::vector<T>& timedependency,
      LBconverter<T>& converter,
      T maxPhysT, T particleMaxPhysT, int particelStartTime,
      unsigned short particleProperties,
      SuperParticleSystem3D<T, PARTICLETYPE> & spSys, bool comments=true
      ) :
      _filename(filename), _file(file),
      _sediVel(sediVel), _tau(tau), _timedependency(timedependency), _converter(converter), _maxPhysT(maxPhysT),
      _particleMaxPhysT(particleMaxPhysT), _particelStartTime(particelStartTime),
      _properties(particleProperties), _spSys(spSys), _comments(comments) {
  }

  enum particleProperties
    : unsigned short {position = 1, velocity = 2, radius = 4, mass = 8, force = 16};

  void writeFile(int it);
  void writeParams();

private:
  const char* _filename;  // filename of output txt-file
  std::ofstream& _file;   // contact to output file
  std::vector<T> _sediVel;  // sedimentation velocity of particle
  T _tau;                   // tau = relaxation time of particle
  std::vector<T>& _timedependency;
  LBconverter<T>& _converter;
  T _maxPhysT;              // maximum physical time of fluid simulation
  T _particleMaxPhysT;      // maximum physical time of particle simulation
  int _particelStartTime;   // iteration time when particles simulation starts
  unsigned short _properties;
  SuperParticleSystem3D<T, PARTICLETYPE> & _spSys; // doch nicht, weil sonst Parallelisieren!
  bool _comments;
};

template<typename T, template<typename U> class PARTICLETYPE>
void TxtFileOutput<T, PARTICLETYPE>::writeParams() {
  _file << "#sediVelo[0] = " << _sediVel[0] << "; sediVelo[1] = "
      << _sediVel[1] << "; sediVelo[2] = " << _sediVel[2] << "\n";
  _file << "#particleTau = " << _tau << "\n";
  _file << "#timeStep(s) = " << _converter.physTime(1) << "\n";
  _file << "#LB_converter information" << "\n";
  _file << "#characteristical values" << "\n";
  _file << "#Dimension(d):                     dim=" << _converter.getDim()
      << "\n";
  _file << "#Characteristical length(m):       charL="
      << _converter.getCharL() << "\n";
  _file << "#Characteristical speed(m/s):      charU="
      << _converter.getCharU() << "\n";
  _file << "#Characteristical time(s):         charT="
      << _converter.getCharTime() << "\n";
  _file << "#Density factor(kg/m^d):           charRho="
      << _converter.getCharRho() << "\n";
  _file << "#Characterestical mass(kg):        charMass="
      << _converter.getCharMass() << "\n";
  _file << "#Characterestical force(N):        charForce="
      << _converter.getCharForce() << "\n";
  _file << "#Characterestical pressure(Pa):    charPressure="
      << _converter.getCharPressure() << "\n";
  _file << "#Pressure level(Pa):               pressureLevel="
      << _converter.getPressureLevel() << "\n";
  _file << "#Phys. kinematic viscosity(m^2/s): charNu="
      << _converter.getCharNu() << "\n";
  _file << "\n";
  _file << "#lattice values" << "\n";
  _file << "#DeltaX:                           deltaX="
      << _converter.getDeltaX() << "\n";
  _file << "#Lattice velocity:                 latticeU="
      << _converter.getLatticeU() << "\n";
  _file << "#DeltaT:                           deltaT="
      << _converter.getDeltaT() << "\n";
  _file << "#Reynolds number:                  Re=" << _converter.getRe()
      << "\n";
  _file << "#DimlessNu:                        dNu="
      << _converter.getDimlessNu() << "\n";
  _file << "#Viscosity for computation:        latticeNu="
      << _converter.getLatticeNu() << "\n";
  _file << "#Relaxation time:                  tau=" << _converter.getTau()
      << "\n";
  _file << "#Relaxation frequency:             omega="
      << _converter.getOmega() << "\n";
  _file << "\n";
  _file << "#conversion factors" << "\n";
  _file << "#latticeL(m):                      latticeL="
      << _converter.getLatticeL() << "\n";
  _file << "#Time step (s):                    physTime="
      << _converter.physTime() << "\n";
  _file << "#Velocity factor(m/s):             physVelocity="
      << _converter.physVelocity() << "\n";
  _file << "#FlowRate factor(m^d/s):           physFlowRate="
      << _converter.physFlowRate() << "\n";
  _file << "#Mass factor(kg):                  physMass="
      << _converter.physMass() << "\n";
  _file << "#Force factor(N):                  physForce="
      << _converter.physForce() << "\n";
  _file << "#Force factor massless(N/kg):      physMasslessForce="
      << _converter.physMasslessForce() << "\n";
  _file << "#Pressure factor(Pa):              physPressure="
      << _converter.physPressure(4) << "\n";
  _file << "#latticePressure:                  latticeP="
      << _converter.latticePressure() << "\n";
  _file << "\n";
  _file << "# characteristicalTime(s) = " << _converter.getCharTime() << "\n";
  _file << "# deltaT = " << _converter.getDeltaT() << "\n";
  _file << "# fluidMaxPhysT(s) = " << _maxPhysT << "\n";
  _file << "# particleMaxPhysT(s) = " << _particleMaxPhysT << "\n";
  _file << "\r\n";
}

template<typename T, template<typename U> class PARTICLETYPE>
void TxtFileOutput<T, PARTICLETYPE>::writeFile(int it) {
  int rank = 0;
  int size = 1;
#ifdef PARALLEL_MODE_MPI
  rank = singleton::mpi().getRank();
  size = singleton::mpi().getSize();
#endif

  int i=0;
  if (it - _particelStartTime == 0) {
    _file.open(_filename, ios::out | ios::trunc);
    if (_comments) writeParams();
    _file << "# " << i+1 << "Timestep" << " " << i+2 << "physTime" << " " <<
        i+3 << "ID"; i=i+3;
    if (_properties & particleProperties::position) {
      _file << " " << i+1 << "Position(0)" << " " << i+2 << "Position(1)" << " " << i+3 << "Position(2)"; i=i+3;
    }
    if (_properties & particleProperties::radius) {
      _file << " " << i+1 << "Radius"; i=i+1;
    }
    if (_properties & particleProperties::mass) {
      _file << " " << i+1 << "Mass"; i=i+1;
    }
    if (_properties & particleProperties::force) {
      _file << " " << i+1 << "Force(0)" << " " << i+2 << "Force(1)" << " " << i+3 << "Force(2)"; i=i+3;
    }
    if (_properties & particleProperties::velocity) {
      _file << " " << i+1 << "Velocity(0)" << " " << i+2 << "Velocity(1)" << " " << i+3 << "Velocity(2)"; i=i+3;
    }
    _file << " " << i+1 << "absErrorSediVel(2)"; i=i+1;
    _file << " " << i+1 << "analyticalSediVel(2)"; i=i+1;
    _file << " " << i+1 << "relErr(2)"; i=i+1;
    _file << " " << i+1 << "temporalAnalyticalSediVel(2)"; i=i+1;
    _file << "\n";
  }
  if (it - _particelStartTime > 0) _file.open(_filename, ios::out | ios::app);
  for (auto pS : _spSys.getParticleSystems()) {
    for (auto p : pS->getParticles()) {
      _file << it << " " << _converter.physTime(it - _particelStartTime) << " ";
      _file << p.getID() << " ";
      if (_properties & particleProperties::position) {
        _file << p.getPos()[0] << " " << p.getPos()[1] << " " << p.getPos()[2] << " ";
      }
      if (_properties & particleProperties::radius) {
        _file << p.getRad() << " ";
      }
      if (_properties & particleProperties::mass) {
        _file << p.getMass() << " ";
      }
      if (_properties & particleProperties::force) {
        _file << p.getForce()[0] << " " << p.getForce()[1] << " " << p.getForce()[2] << " ";
      }
      if (_properties & particleProperties::velocity) {
        _file << p.getVel()[0] << " " << p.getVel()[1] << " " << p.getVel()[2] << " ";
      }
      _file << p.getVel()[2] - _sediVel[2] << " ";
      _file << _sediVel[2] << " ";
      _file << (p.getVel()[2] - _sediVel[2])/_sediVel[2] << " ";
      _file << _timedependency[2] << " ";
      _file << "\n";
    }
  }
  _file.close();
}
