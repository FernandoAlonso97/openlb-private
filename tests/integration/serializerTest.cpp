/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Test Case for Serializer
 */
#ifndef SERIALIZER_TEST_CPP
#define SERIALIZER_TEST_CPP

#include "serializerTest.h"

namespace olb {

namespace tests {

// --- CUBOID3D --- //
template<typename T>
CuboidSerializerTestCase<T>::CuboidSerializerTestCase(SerializerTest<T> *sTest)
    : TestCase(sTest), _serTest(sTest) { }

template<typename T>
bool CuboidSerializerTestCase<T>::isSuccessful(std::string &errorMessage, OstreamManager &clout) {
  std::stringstream ss;

  Cuboid3D<T> cub(0., 1., .5, .1, 100, 50, 2, 1);

  cub.save("cub");

  Cuboid3D<T> newCub;
  newCub.load("cub");

  COMPARE_OBJECTS(cub, newCub)

  errorMessage = "";
  return true;
}


// --- CuboidGeometry3D --- //
template<typename T>
CuboidGeometry3DSerializerTestCase<T>::CuboidGeometry3DSerializerTestCase(SerializerTest<T> *sTest)
    : TestCase(sTest), _serTest(sTest) { }

template<typename T>
bool CuboidGeometry3DSerializerTestCase<T>::isSuccessful(std::string &errorMessage, OstreamManager &clout) {
  std::stringstream ss;
  _serTest->cGeo->save("CuboidGeometry");

  CuboidGeometry3D<T> newcGeo;
  newcGeo.load("CuboidGeometry");

  COMPARE_OBJECTS(*_serTest->cGeo, newcGeo)

  errorMessage = "";
  return true;
}


// --- LoadBalancer --- //
template<typename T>
LoadBalancerSerializerTestCase<T>::LoadBalancerSerializerTestCase(SerializerTest<T> *sTest)
    : TestCase(sTest), _serTest(sTest) { }

template<typename T>
bool LoadBalancerSerializerTestCase<T>::isSuccessful(std::string &errorMessage, OstreamManager &clout)
{
  std::stringstream ss;
  _serTest->lb->save("LoadBalancer");

  LoadBalancer<T> newLb;
  newLb.load("LoadBalancer");

  COMPARE_OBJECTS(*_serTest->lb, newLb)

  errorMessage = "";
  return true;
}


template<typename T>
SerializerTest<T>::SerializerTest() {
  cGeo = new CuboidGeometry3D<T>(1., 2., .5, .2, 200, 100, 20, 12);
  lb = new HeuristicLoadBalancer<T> (*cGeo);

  addTestCase(new CuboidSerializerTestCase<T>(this));
  addTestCase(new CuboidGeometry3DSerializerTestCase<T>(this));
  addTestCase(new LoadBalancerSerializerTestCase<T>(this));
}

}

}

#endif
