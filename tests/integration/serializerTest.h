/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Test Case for Serializer
 */
#ifndef SERIALIZER_TEST_H
#define SERIALIZER_TEST_H

#include <cassert>
#include <string>
#include <iostream>

#include "olbTester.h"
#include "communication/loadBalancer.h"
#include "core/serializer.h"
#include "geometry/cuboid3D.h"
#include "geometry/cuboidGeometry3D.h"

/// Macro for comparing method_name() cub and newCub
#define COMPARE_CUB(method_name) COMPARE_METHODS(cub, newCub, method_name)
//----------------------------//


namespace olb {

namespace tests {

template<typename T>
class SerializerTest;


// --- CUBOID3D --- //
template<typename T>
class CuboidSerializerTestCase : public TestCase {
public:
  SerializerTest<T> *_serTest;

  CuboidSerializerTestCase(SerializerTest<T> *sTest);

  std::string getTitle() const {
    return "Cuboid3D<T>";
  }

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout);
};



// --- CuboidGeometry3D --- //
template<typename T>
class CuboidGeometry3DSerializerTestCase : public TestCase {
public:
  SerializerTest<T> *_serTest;

  CuboidGeometry3DSerializerTestCase(SerializerTest<T> *sTest);

  std::string getTitle() const {
    return "CuboidGeometry3D<T>";
  }

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout);
};


// --- LoadBalancer --- //
template<typename T>
class LoadBalancerSerializerTestCase : public TestCase {
public:
  SerializerTest<T> *_serTest;

  LoadBalancerSerializerTestCase(SerializerTest<T> *sTest);

  std::string getTitle() const {
    return "LoadBalancer<T>";
  }

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout);
};

template<typename T>
class SerializerTest : public OlbMultiTest {
public:
  Serializer *serSave;
  Serializer *serLoad;

  CuboidGeometry3D<T>* cGeo;
  LoadBalancer<T>* lb;


  SerializerTest();

  std::string getTitle() const {
    return "Serializer: write and read different objects.";
  }
};

}

}

#endif
