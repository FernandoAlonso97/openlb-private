/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Test Case for XMLreader
 */
#ifndef XML_READER_TEST_H
#define XML_READER_TEST_H

#include <cassert>
#include <string>
#include <iostream>

#include "olbTester.h"
#include "io/xmlReader.h"

/// Macro for checking an XmlReader[TAG] against type and value
#define CHECK_AGAINST(tag, type, var_name, var_value) \
CHECK(reader[#tag].read<type>(var_name), "Could not read " #type " value " #var_value) \
error_stream.str(""); \
error_stream.clear(); \
error_stream << #type " " #var_name " value " << var_name << " is not " #var_value; \
CHECK(var_name == var_value, error_stream.str())
//----------------------------//


namespace olb {

namespace tests {

class XmlReaderTest : public OlbTest {

  std::string getTitle() const {
    return "XML Reader: open example file and read different value types";
  }

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout) {
    XMLreader reader("xmlReaderTestExampleFile.xml");

    bool boolValue;
    int intValue;
    double doubleValue;
    std::string stringValue;

    std::stringstream error_stream;

    // Check valid values
    CHECK_AGAINST(BoolValueFalse, bool, boolValue, false)
    CHECK_AGAINST(BoolValueTrue, bool, boolValue, true)
    CHECK_AGAINST(BoolValueZero, bool, boolValue, false)
    CHECK_AGAINST(BoolValueOne, bool, boolValue, true)

    CHECK_AGAINST(IntValue, int, intValue, 128)

    CHECK_AGAINST(DoubleValue, double, doubleValue, 4.88)

    CHECK_AGAINST(StringValue, std::string, stringValue, "TestString with Spaces and New Lines.")

    errorMessage = "";
    return true;
  }
};

}

}

#endif
