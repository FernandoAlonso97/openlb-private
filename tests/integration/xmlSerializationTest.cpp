/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Test Case for XML Serialization
 */
#ifndef XML_SERIALIZATION_TEST_CPP
#define XML_SERIALIZATION_TEST_CPP

#include "xmlSerializationTest.h"

namespace olb {

namespace tests {


// --- CuboidGeometry3D --- //
template<typename T>
CuboidGeometry3DXmlSerializationTest<T>::CuboidGeometry3DXmlSerializationTest(XmlSerializationTest<T> *sTest)
    : TestCase(sTest), _serTest(sTest) { }

template<typename T>
bool CuboidGeometry3DXmlSerializationTest<T>::isSuccessful(std::string &errorMessage, OstreamManager &clout)
{
  std::stringstream ss;
  _serTest->cGeo->writeToFile("CuboidGeometry3D", *_serTest->lb);

  CuboidGeometry3D<T>* newGeo = createCuboidGeometry<T>("CuboidGeometry3D");

  // compare number of cuboids
  COMPARE_METHODS((*_serTest->cGeo), (*newGeo), getNc)
  // compare mother cuboids
  COMPARE_OBJECTS(_serTest->cGeo->getMotherCuboid(), newGeo->getMotherCuboid())

  //clout << endl;

  // Compare individual cuboids against each other
  for( int i = 0; i < newGeo->getNc(); i++ ) {
    //clout << "Comparing Cuboid " << i << "... ";
    COMPARE_OBJECTS(_serTest->cGeo->get(i), newGeo->get(i))
    //clout << "OK." << endl;
  }

  errorMessage = "";
  return true;
}


// --- HeuristicLoadBalancer --- //
template<typename T>
HeuristicLoadBalancerXmlSerializationTest<T>::HeuristicLoadBalancerXmlSerializationTest(XmlSerializationTest<T> *sTest)
    : TestCase(sTest), _serTest(sTest) { }

template<typename T>
bool HeuristicLoadBalancerXmlSerializationTest<T>::isSuccessful(std::string &errorMessage, OstreamManager &clout)
{
  std::stringstream ss;
  _serTest->lb->writeToFile("HeuristicLoadBalancer");

  LoadBalancer<T>* newLb = createLoadBalancer<T>("HeuristicLoadBalancer", _serTest->cGeo);

  COMPARE_OBJECTS(*_serTest->lb, *newLb)

  errorMessage = "";
  return true;
}

template<typename T>
XmlSerializationTest<T>::XmlSerializationTest() {
  cGeo = new CuboidGeometry3D<double>(1., 2., .5, .2, 200, 100, 20, 12);
  lb = new HeuristicLoadBalancer<T> (*cGeo);

  addTestCase(new CuboidGeometry3DXmlSerializationTest<T>(this));
  addTestCase(new HeuristicLoadBalancerXmlSerializationTest<T>(this));
}

}

}

#endif
