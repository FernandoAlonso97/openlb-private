/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2016 Benjamin Förster
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

/** \file
 * Test Case for Serializer
 */
#ifndef XML_SERIALIZATION_TEST_H
#define XML_SERIALIZATION_TEST_H

#include <cassert>
#include <string>
#include <iostream>

#include "olbTester.h"
#include "geometry/cuboidGeometry3D.h"
#include "communication/loadBalancer.h"
#include "communication/heuristicLoadBalancer.h"

using namespace olb;

/// Macro for comparing method_name() cub and newCub
#define COMPARE_CUB(method_name) COMPARE_METHODS(cub, newCub, method_name)
//----------------------------//


namespace olb {

namespace tests {

template<typename T> class XmlSerializationTest;


// --- CuboidGeometry3D<double> --- //
template<typename T>
class CuboidGeometry3DXmlSerializationTest : public TestCase {
public:
  XmlSerializationTest<T> *_serTest;

  CuboidGeometry3DXmlSerializationTest(XmlSerializationTest<T> *sTest);

  std::string getTitle() const {
    return "CuboidGeometry3D<double>";
  }

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout);
};


// --- HeuristicLoadBalancer --- //
template<typename T>
class HeuristicLoadBalancerXmlSerializationTest : public TestCase {
public:
  XmlSerializationTest<T> *_serTest;

  HeuristicLoadBalancerXmlSerializationTest(XmlSerializationTest<T> *sTest);

  std::string getTitle() const {
    return "HeuristicLoadBalancer";
  }

  bool isSuccessful(std::string &errorMessage, OstreamManager &clout);
};

template<typename T>
class XmlSerializationTest : public OlbMultiTest {
public:
  CuboidGeometry3D<T>* cGeo;
  LoadBalancer<T>* lb;

  XmlSerializationTest();

  std::string getTitle() const {
    return "XML Serialization: write and read different objects.";
  }
};

}

}

#endif
