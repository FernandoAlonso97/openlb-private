/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;

/// D2Q5 AdvectionDiffusionBGK Tests
TEST(D2Q5AdvectionDiffusionBGKTest, testGetOmega) {
    AdvectionDiffusionBGKdynamics<double, AdvectionDiffusionD2Q5Descriptor > dynamic(1.0, instances::getBulkMomenta<double, AdvectionDiffusionD2Q5Descriptor >() );
    double omega = dynamic.getOmega();
    ASSERT_EQ(1.0, omega);
}

TEST(D2Q5AdvectionDiffusionBGKTest, testCollideNoVelocity) {
    AdvectionDiffusionBGKdynamics<double, AdvectionDiffusionD2Q5Descriptor> dynamics(1.0, instances::getBulkMomenta<double, AdvectionDiffusionD2Q5Descriptor>());
    CellView<double, AdvectionDiffusionD2Q5Descriptor> cell(&dynamics);
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0}));
}

TEST(D2Q5AdvectionDiffusionBGKTest, testCollide) {
    AdvectionDiffusionBGKdynamics<double, AdvectionDiffusionD2Q5Descriptor> dynamics(1.0, instances::getBulkMomenta<double, AdvectionDiffusionD2Q5Descriptor>());
    CellView<double, AdvectionDiffusionD2Q5Descriptor> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell,
               {1.666666666666667, 0.83333333333333326, 0.83333333333333326, 0.83333333333333326, 0.83333333333333326}));
}

/// D3Q7 AdvectionDiffusionBGK Tests
TEST(D3Q7AdvectionDiffusionBGKTest, testGetOmega) {
    AdvectionDiffusionBGKdynamics<double, AdvectionDiffusionD3Q7Descriptor > dynamic(1.0, instances::getBulkMomenta<double, AdvectionDiffusionD3Q7Descriptor >() );
    double omega = dynamic.getOmega();
    ASSERT_EQ(1.0, omega);
}

TEST(D3Q7AdvectionDiffusionBGKTest, testCollideNoVelocity) {
    AdvectionDiffusionBGKdynamics<double, AdvectionDiffusionD3Q7Descriptor> dynamics(1.0, instances::getBulkMomenta<double, AdvectionDiffusionD3Q7Descriptor>());
    CellView<double, AdvectionDiffusionD3Q7Descriptor> cell(&dynamics);
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}));
}

TEST(D3Q7AdvectionDiffusionBGKTest, testCollide) {
    AdvectionDiffusionBGKdynamics<double, AdvectionDiffusionD3Q7Descriptor> dynamics(1.0, instances::getBulkMomenta<double, AdvectionDiffusionD3Q7Descriptor>());
    CellView<double, AdvectionDiffusionD3Q7Descriptor> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell,
               {1.75, 0.875, 0.875, 0.875, 0.875, 0.875, 0.875}));
}
