/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;

/// D2Q9 BGK Tests
TEST(D2Q9BGKTest, testGetOmega) {
    BGKdynamics<double, D2Q9Descriptor > dynamic(1.0, instances::getBulkMomenta<double, D2Q9Descriptor >() );
    double omega = dynamic.getOmega();
    ASSERT_EQ(1.0, omega);
}

TEST(D2Q9BGKTest, testCollideNoVelocity) {
    BGKdynamics<double, D2Q9Descriptor> dynamics(1.0, instances::getBulkMomenta<double, D2Q9Descriptor>());
    CellView<double, D2Q9Descriptor> cell(&dynamics);
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}));
}

TEST(D2Q9BGKTest, testCollide) {
    BGKdynamics<double, D2Q9Descriptor> dynamics(1.0, instances::getBulkMomenta<double, D2Q9Descriptor>());
    CellView<double, D2Q9Descriptor> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell,
                {4.0, 0.25, 1.0, 0.25, 1.0, 0.25, 1.0, 0.25, 1.0}));
}

/// D3Q19 BGK Tests
TEST(D3Q19BGKTest, testGetOmega) {
    BGKdynamics<double, D3Q19Descriptor > dynamic(1.0, instances::getBulkMomenta<double, D3Q19Descriptor >() );
    double omega = dynamic.getOmega();
    ASSERT_EQ(1.0, omega);
}

TEST(D3Q19BGKTest, testCollideNoVelocity) {
    BGKdynamics<double, D3Q19Descriptor> dynamics(1.0, instances::getBulkMomenta<double, D3Q19Descriptor>());
    CellView<double, D3Q19Descriptor> cell(&dynamics);
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}));
}

TEST(D3Q19BGKTest, testCollide) {
    BGKdynamics<double, D3Q19Descriptor> dynamics(1.0, instances::getBulkMomenta<double, D3Q19Descriptor>());
    CellView<double, D3Q19Descriptor> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell,
                {6.333333333333333, 1.0555555555555556, 1.0555555555555556, 1.0555555555555556,
                0.52777777777777779, 0.52777777777777779, 0.52777777777777779, 0.52777777777777779,
                0.52777777777777779, 0.52777777777777779, 1.0555555555555556, 1.0555555555555556,
                1.0555555555555556, 0.52777777777777779, 0.52777777777777779, 0.52777777777777779,
                0.52777777777777779, 0.52777777777777779, 0.52777777777777779}));
}
