/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;

TEST(D2Q5Boundary, testZeroVelocity) {
  using AdvectionDiffusionBoundaryManager = AdvectionDiffusionBoundaryManager2D<double, AdvectionDiffusionD2Q5Descriptor, AdvectionDiffusionRLBdynamics<double,AdvectionDiffusionD2Q5Descriptor>>;
  Momenta<double, AdvectionDiffusionD2Q5Descriptor>* momenta = AdvectionDiffusionBoundaryManager::getTemperatureBoundaryMomenta<0, 1>();
  Dynamics<double, AdvectionDiffusionD2Q5Descriptor>* dynamics = AdvectionDiffusionBoundaryManager::getTemperatureBoundaryDynamics<0, 1>(0.1, *momenta);
  CellView<double, AdvectionDiffusionD2Q5Descriptor> cell(dynamics);
  test::initCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0});
  LatticeStatistics<double> statistics;
  dynamics->collide(cell, statistics);
  EXPECT_TRUE(test::checkCell(cell,
              {0.0, 0.0, 0.0, 0.0, 0.0}));
  delete dynamics;
  delete momenta;
}

TEST(D2Q5Boundary, testSimpleCollide) {
  using AdvectionDiffusionBoundaryManager = AdvectionDiffusionBoundaryManager2D<double, AdvectionDiffusionD2Q5Descriptor, AdvectionDiffusionRLBdynamics<double,AdvectionDiffusionD2Q5Descriptor>>;
  Momenta<double, AdvectionDiffusionD2Q5Descriptor>* momenta = AdvectionDiffusionBoundaryManager::getTemperatureBoundaryMomenta<0,-1>();
  Dynamics<double, AdvectionDiffusionD2Q5Descriptor>* dynamics = AdvectionDiffusionBoundaryManager::getTemperatureBoundaryDynamics<0,-1>(0.1, *momenta);
  CellView<double, AdvectionDiffusionD2Q5Descriptor> cell(dynamics);
  double dirichletTemperature = 5.;
  // impose temperature boundary
  dynamics->defineRho(cell, dirichletTemperature);
  EXPECT_EQ(dynamics->computeRho(cell), dirichletTemperature);
  LatticeStatistics<double> statistics;
  dynamics->collide(cell, statistics);
  EXPECT_TRUE(test::checkCell(cell,
              {1.3333333333333335, -1.1333333333333333, 0.66666666666666663, 2.4666666666666668, 0.66666666666666663}));
  delete dynamics;
  delete momenta;
}
