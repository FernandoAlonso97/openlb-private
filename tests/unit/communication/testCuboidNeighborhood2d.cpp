/*  This file is part of the OpenLB library
*
*  Copyright (C) 2018 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include <cmath>

#include "olb2D.h"
#include "olb2D.hh"

using namespace olb;

class CuboidNeighbourhood2DTest : public ::testing::Test {
protected:
  CuboidGeometry2D<double>      _cuboidGeometry;
  HeuristicLoadBalancer<double> _loadBalancer;
  SuperGeometry2D<double>       _superGeometry;
  SuperLattice2D<double, descriptors::D2Q9Descriptor> _superLattice;

public:

  CuboidNeighbourhood2DTest():
    _cuboidGeometry(0.0, 0.0, 1.0, 5, 6, 2),
    _loadBalancer(_cuboidGeometry),
    _superGeometry(_cuboidGeometry, _loadBalancer),
    _superLattice(_superGeometry)
  {
  }
};

TEST_F(CuboidNeighbourhood2DTest, testMembers)
{
  Communicator2D<double>& comm = _superLattice.get_commStream();
  std::vector<CuboidNeighbourhood2D<double> >& neigbourhood = comm.get_nh();
  ASSERT_EQ(neigbourhood.size(), 2u);

  EXPECT_EQ(neigbourhood[0].get_inCellsSize(), 5);
  EXPECT_EQ(neigbourhood[0].get_inCsize(), 1);
  EXPECT_EQ(neigbourhood[0].get_outCellsSize(), 5);

  EXPECT_EQ(neigbourhood[1].get_inCellsSize(), 5);
  EXPECT_EQ(neigbourhood[1].get_inCsize(), 1);
  EXPECT_EQ(neigbourhood[1].get_outCellsSize(), 5);
}

TEST_F(CuboidNeighbourhood2DTest, testOutDataZero)
{
  Communicator2D<double>& comm = _superLattice.get_commStream();
  std::vector<CuboidNeighbourhood2D<double> >& neigbourhood = comm.get_nh();
  ASSERT_EQ(neigbourhood.size(), 2u);

  neigbourhood[0].buffer_outData();

  double data[45];
  bool** out_data = neigbourhood[0].get_outData();
  std::memcpy(data, out_data[1], 45 * sizeof(double));
  for (int i = 0; i < 45; ++i)
    EXPECT_EQ(0, data[i]);
}

std::initializer_list<std::initializer_list<double>> value = {
{1,2,3,4,5,6,7,8,9}
, {11,12,13,14,15,16,17,18,19}
, {21,22,23,24,25,26,27,28,29}
, {31,32,33,34,35,36,37,38,39}
, {41,42,43,44,45,46,47,48,49}
, {51,52,53,54,55,56,57,58,59}
, {61,62,63,64,65,66,67,68,69}
, {71,72,73,74,75,76,77,78,79}
, {81,82,83,84,85,86,87,88,89}
, {91,92,93,94,95,96,97,98,99}
, {101,102,103,104,105,106,107,108,109}
, {111,112,113,114,115,116,117,118,119}
, {121,122,123,124,125,126,127,128,129}
, {131,132,133,134,135,136,137,138,139}
, {141,142,143,144,145,146,147,148,149}
};

TEST_F(CuboidNeighbourhood2DTest, testOutData)
{
  BlockLatticeView2D<double, D2Q9Descriptor>& blockLattice0 = _superLattice.getBlockLattice(0);
  BlockLatticeView2D<double, D2Q9Descriptor>& blockLattice1 = _superLattice.getBlockLattice(1);
  test::initBlockLattice2D(blockLattice0, value);
  test::initBlockLattice2D(blockLattice1, value);
  Communicator2D<double>& comm = _superLattice.get_commStream();
  std::vector<CuboidNeighbourhood2D<double> >& neigbourhood = comm.get_nh();
  ASSERT_EQ(neigbourhood.size(), 2u);

  neigbourhood[0].buffer_outData();

  double data[45];
  bool** out_data = neigbourhood[0].get_outData();
  std::memcpy(data, out_data[1], 45 * sizeof(double));
  for (int i = 0; i < 45; ++i)
  {
    double val = 100 + (i/9)*10 + i % 9 + 1;
    EXPECT_EQ(val, data[i]);
  }
}
