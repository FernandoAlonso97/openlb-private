/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;


//constexpr RadiativeUnitConverter( int resolution, T latticeRelaxationTime, T physAbsorption, T physScattering )
TEST(RadiationUnitConverter, testGetPhysAbsorption) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 3, 1 );
  double absorption = converter.getPhysAbsorption();
  ASSERT_EQ(3, absorption);
}

TEST(RadiationUnitConverter, testGetPhysScattering) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 3, 1 );
  double scattering = converter.getPhysScattering();
  ASSERT_EQ(1, scattering);
}

TEST(RadiationUnitConverter, testPhysScatteringAlbedo) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 3, 1 );
  double scatteringAlbedo = converter.getScatteringAlbedo();
  ASSERT_EQ(0.25, scatteringAlbedo);
}

TEST(RadiationUnitConverter, testGetExtinction) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 1, 2 );
  double extinction = converter.getExtinctionCoefficient();
  ASSERT_EQ(3, extinction);
}

TEST(RadiationUnitConverter, testGetPhysDiffusion) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 1, 2 );
  double diffCoefficient = converter.getPhysDiffusionCoefficient();
  ASSERT_EQ( 0.111111111111111111, diffCoefficient);
}

TEST(RadiationUnitConverter, testGetLatticeAbsorption) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 1, 2 );
  double absorption = converter.getLatticeAbsorption();
  ASSERT_EQ( 0.1, absorption );
}

TEST(RadiationUnitConverter, testGetLatticeScattering) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 1, 2 );
  double scattering = converter.getLatticeScattering();
  ASSERT_EQ( 0.2, scattering);
}


TEST(RadiationUnitConverter, testGetLatticeDiffusionCoefficient) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 10, 1.0, 1, 2 );
  double latticeDiffusion = converter.getLatticeDiffusionCoefficient();
  ASSERT_EQ( 1.1111111111111111, latticeDiffusion );
}

TEST(RadiationUnitConverter, testGetRefractionFunction) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 40, 1.0, 0.5, 0.5, 1.33, 1.51 );
  double C_F = getRefractionFunction(converter);
  ASSERT_NEAR( 1.04787, C_F, 10e-6 );
}

TEST(RadiationUnitConverter, testGetPartialBBCoefficient) {
  RadiativeUnitConverter<double,D3Q7DescriptorRTLB> converter( 40, 1.0, 0.5, 0.5, 1.33, 1.51 );
  double r_F = getPartialBBCoefficient(converter);
  ASSERT_NEAR( 1.96484, r_F, 10e-6 );
}
