/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Albert Mink
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;

///AnalyticalConst1D -> ctor
TEST(AnalyticalConst1D, testTargetDim1) {
  AnalyticalConst1D<double,double> analyticalConst(3.145193);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(1, targetDim);
}

TEST(AnalyticalConst1D, testTargetDim5) {
  std::vector<int> functorVal = {0,1,2,3,4};
  AnalyticalConst1D<int,double> analyticalConst(functorVal);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(5, targetDim);
}

TEST(AnalyticalConst1D, testOperator1) {
  AnalyticalConst1D<double,double> analyticalConst(3.145193);
  double source[1], target[1];
  source[0] = 8.1;
  analyticalConst(target,source);
  ASSERT_EQ(3.145193, target[0]);
}

///AnalyticalConst1D -> ::operator()
TEST(AnalyticalConst1D, testOperator5) {
  std::vector<int> functorVal = {0,1,2,3,4};
  AnalyticalConst1D<int,double> analyticalConst(functorVal);
  double source[1];
  int target[5];
  source[0] = -1;
  analyticalConst(target,source);
  ASSERT_EQ(4, target[4]);
}

///AnalyticalConst2D -> ctor
TEST(AnalyticalConst2D, testTargetDim1) {
  AnalyticalConst2D<double,double> analyticalConst(3.145193);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(1, targetDim);
}

TEST(AnalyticalConst2D, testTargetDim2) {
  AnalyticalConst2D<double,double> analyticalConst(1.1,2.2);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(2, targetDim);
}

TEST(AnalyticalConst2D, testTargetDim5) {
  std::vector<int> functorVal = {0,1,2,3,4};
  AnalyticalConst2D<int,double> analyticalConst(functorVal);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(5, targetDim);
}

///AnalyticalConst2D -> ::operator()
TEST(AnalyticalConst2D, testOperator1) {
  AnalyticalConst2D<double,double> analyticalConst(3.145193);
  double source[2], target[1];
  source[0] = -1;
  source[1] = -1;
  analyticalConst(target,source);
  ASSERT_EQ(3.145193, target[0]);
}

TEST(AnalyticalConst2D, testOperator2) {
  AnalyticalConst2D<double,int> analyticalConst(1.1,2.2);
  int source[2] = {-1,-1};
  double target[2];
  analyticalConst(target,source);
  ASSERT_EQ(1.1, target[0]);
  ASSERT_EQ(2.2, target[1]);
}

TEST(AnalyticalConst2D, testOperator5) {
  std::vector<int> functorVal = {0,1,2,3,4};
  AnalyticalConst2D<int,double> analyticalConst(functorVal);
  double source[2] = {-1,-1};
  int target[5];
  analyticalConst(target,source);
  ASSERT_EQ(4, target[4]);
}

///AnalyticalConst3D -> ctor
TEST(AnalyticalConst3D, testTargetDim1) {
  AnalyticalConst3D<double,double> analyticalConst(3.145193);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(1, targetDim);
}

TEST(AnalyticalConst3D, testTargetDim2) {
  AnalyticalConst3D<double,double> analyticalConst(1.1,2.2);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(2, targetDim);
}

TEST(AnalyticalConst3D, testTargetDim3) {
  AnalyticalConst3D<double,double> analyticalConst(1.1,2.2,3.3);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(3, targetDim);
}

TEST(AnalyticalConst3D, testTargetDim5) {
  std::vector<int> functorVal = {0,1,2,3,4};
  AnalyticalConst3D<int,double> analyticalConst(functorVal);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(5, targetDim);
}

TEST(AnalyticalConst3D, testTargetDim6) {
  Vector<int,3> functorVal(0,1,2);
  AnalyticalConst3D<int,double> analyticalConst(functorVal);
  int targetDim = analyticalConst.getTargetDim();
  ASSERT_EQ(3, targetDim);
}

///AnalyticalConst3D -> ::operator()
TEST(AnalyticalConst3D, testOperator1) {
  AnalyticalConst3D<double,bool> analyticalConst(3.145193);
  bool source[3] = {1,1,1};
  double target[1];
  analyticalConst(target,source);
  ASSERT_EQ(3.145193, target[0]);
}

TEST(AnalyticalConst3D, testOperator2) {
  AnalyticalConst3D<double,int> analyticalConst(1.1,2.2);
  int source[3] = {-1,-1,-1};
  double target[2];
  analyticalConst(target,source);
  ASSERT_EQ(1.1, target[0]);
  ASSERT_EQ(2.2, target[1]);
}

TEST(AnalyticalConst3D, testOperator3) {
  AnalyticalConst3D<double,double> analyticalConst(1.1,2.2,3.3);
  double source[3] = {-1,-1,-1};
  double target[3];
  analyticalConst(target,source);
  ASSERT_EQ(1.1, target[0]);
  ASSERT_EQ(2.2, target[1]);
  ASSERT_EQ(3.3, target[2]);
}

TEST(AnalyticalConst3D, testOperator5) {
  std::vector<int> functorVal = {0,1,2,3,4};
  AnalyticalConst3D<int,double> analyticalConst(functorVal);
  double source[3] = {-1,-1,-1};
  int target[5];
  analyticalConst(target,source);
  ASSERT_EQ(0, target[0]);
  ASSERT_EQ(1, target[1]);
  ASSERT_EQ(2, target[2]);
  ASSERT_EQ(3, target[3]);
  ASSERT_EQ(4, target[4]);
}

TEST(AnalyticalConst3D, testOperator6) {
  Vector<int,3> functorVal(0,1,2);
  AnalyticalConst3D<int,double> analyticalConst(functorVal);
  double source[3] = {-1,-1,-1};
  int target[3];
  analyticalConst(target,source);
  ASSERT_EQ(0, target[0]);
  ASSERT_EQ(1, target[1]);
  ASSERT_EQ(2, target[2]);
}

TEST(PLSsolution3D, testOperator3) {
  RadiativeUnitConverter<double,descriptors::D3Q7DescriptorRTLB> const converter( 40, 1.0, 0.5, 1.5 );
  double in[3] = {1,1,1};
  double out[1];
  PLSsolution3D<double,double,descriptors::D3Q7DescriptorRTLB> functor(converter);
  functor(out,in);
  ASSERT_NEAR(0.0137245, out[0],10e-6);
}

TEST(LightSourceConsistency3D, testOperator) {
  RadiativeUnitConverter<double,descriptors::D3Q7DescriptorRTLB> const converter( 40, 1.0, 0.5, 3.5 );
  LightSourceCylindrical3D<double,double,descriptors::D3Q7DescriptorRTLB> functorA(converter, Vector<double,3>(0,0,0));
  PLSsolution3D<double,double,descriptors::D3Q7DescriptorRTLB> functorB(converter);
  double in[3] = {1,1,0};   // on xy-plane functors are equivalent
  double outA[1], outB[1];
  functorA(outA,in);
  functorB(outB,in);
  ASSERT_NEAR(outB[0], outA[0], 10e-6);
}

TEST(LightSourceCylindrical3D, testOperatorSymmetry) {
  RadiativeUnitConverter<double,descriptors::D3Q7DescriptorRTLB> const converter( 40, 1.0, 0.5, 1.5 );
  LightSourceCylindrical3D<double,double,descriptors::D3Q7DescriptorRTLB> functor(converter, Vector<double,3>(1.0,1.0,1.0));
  double in[3] = {1,2,1};
  double out[1];
  functor(out,in);
  ASSERT_NEAR( 0.084473653, out[0],10e-6);
  // check symmetry properties
  in[0] = 2;
  in[1] = 1;
  functor(out,in);
  ASSERT_NEAR( 0.084473653, out[0],10e-6);
  // check alignment to z axis
  in[2] = -1;
  functor(out,in);
  ASSERT_NEAR( 0.084473653, out[0],10e-6);
}
