/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"

using namespace olb;
using namespace olb::descriptors;

TEST(AnalyticPlus3D, testAdd3D)
{
  AnalyticalConst3D<double,double> pi(3.14159, 3.14159, 3.14159);
  AnalyticalConst3D<double,double> val1(1.0, 2.0, 3.0);

  AnalyticPlus3D<double,double> plus(pi, val1);
  double source[3] = {-1,-1,-1};
  double target[3] = {0, 0, 0};
  plus(target, source);
  ASSERT_DOUBLE_EQ(target[0], 4.14159);
  ASSERT_DOUBLE_EQ(target[1], 5.14159);
  ASSERT_DOUBLE_EQ(target[2], 6.14159);
}

TEST(AnalyticMinus3D, testMinus3D)
{
  AnalyticalConst3D<double,double> pi(3.14159, 3.14159, 3.14159);
  AnalyticalConst3D<double,double> val1(1.0, 2.0, 3.0);

  AnalyticMinus3D<double,double> plus(pi, val1);
  double source[3] = {-1,-1,-1};
  double target[3] = {0, 0, 0};
  plus(target, source);
  ASSERT_DOUBLE_EQ(target[0], 2.14159);
  ASSERT_DOUBLE_EQ(target[1], 1.14159);
  ASSERT_DOUBLE_EQ(target[2], 0.14159);
}

TEST(AnalyticMultiplication3D, testMultiply3D)
{
  AnalyticalConst3D<double,double> pi(3.14159, 3.14159, 3.14159);
  AnalyticalConst3D<double,double> val1(1.0, 2.0, 3.0);

  AnalyticMultiplication3D<double,double> multiply(pi, val1);
  double source[3] = {-1,-1,-1};
  double target[3] = {0, 0, 0};
  multiply(target, source);
  ASSERT_DOUBLE_EQ(target[0], 3.14159);
  ASSERT_DOUBLE_EQ(target[1], 6.28318);
  ASSERT_DOUBLE_EQ(target[2], 9.42477);
}

TEST(AnalyticDivision3D, testDivide3D)
{
  AnalyticalConst3D<double,double> pi(3.14159, 3.14159, 3.14159);
  AnalyticalConst3D<double,double> val1(1.0, 2.0, 3.0);

  AnalyticDivision3D<double,double> divide(pi, val1);
  double source[3] = {-1,-1,-1};
  double target[3] = {0, 0, 0};
  divide(target, source);
  ASSERT_DOUBLE_EQ(target[0], 3.14159);
  ASSERT_DOUBLE_EQ(target[1], 1.570795);
  ASSERT_DOUBLE_EQ(target[2], 1.0471966666666666);
}
