/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"

using namespace olb;
using namespace olb::descriptors;
using namespace olb::test;

typedef double T;

// Added just to get access to the BlockData2D protected constructor
// of BlockDataF2D.
template <typename T, typename BaseType>
class TestBlockDataF2D : public BlockDataF2D<T, BaseType>
{
public:

    TestBlockDataF2D(BlockData2D<T, BaseType>& data):
        BlockDataF2D<T, BaseType>(data)
    {
    }
};

TEST(BlockPlus2DTest, testSimpleAdd)
{
    auto aDataLeft = initBlockData2D<double>(1, 1, {{1}});
    auto aDataRight = initBlockData2D<double>(1, 1, {{10}});
    TestBlockDataF2D<T,T> left(aDataLeft);
    TestBlockDataF2D<T,T> right(aDataRight);
    BlockPlus2D<double> plus(left, right);
    double res[1];
    int pos[2] = {0, 0};
    plus(res, pos);
    ASSERT_EQ(11, res[0]);
}

TEST(BlockMinus2DTest, testSimpleSub)
{
    auto aDataLeft = initBlockData2D<double>(1, 1, {{1}});
    auto aDataRight = initBlockData2D<double>(1, 1, {{10}});
    TestBlockDataF2D<T,T> left(aDataLeft);
    TestBlockDataF2D<T,T> right(aDataRight);
    BlockMinus2D<double> plus(left, right);
    double res[1];
    int pos[2] = {0, 0};
    plus(res, pos);
    ASSERT_EQ(-9, res[0]);
}

TEST(BlockMultiplication2DTest, testSimpleMultiplicaction)
{
    auto aDataLeft = initBlockData2D<double>(1, 1, {{3}});
    auto aDataRight = initBlockData2D<double>(1, 1, {{10}});
    TestBlockDataF2D<T,T> left(aDataLeft);
    TestBlockDataF2D<T,T> right(aDataRight);
    BlockMultiplication2D<double> plus(left, right);
    double res[1];
    int pos[2] = {0, 0};
    plus(res, pos);
    ASSERT_EQ(30, res[0]);
}

TEST(BlockDivision2DTest, testSimpleDivision)
{
    auto aDataLeft = initBlockData2D<double>(1, 1, {{10}});
    auto aDataRight = initBlockData2D<double>(1, 1, {{2}});
    TestBlockDataF2D<T,T> left(aDataLeft);
    TestBlockDataF2D<T,T> right(aDataRight);
    BlockDivision2D<double> plus(left, right);
    double res[1];
    int pos[2] = {0, 0};
    plus(res, pos);
    ASSERT_EQ(5, res[0]);
}
