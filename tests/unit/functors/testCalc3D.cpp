/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;
using namespace olb::test;

typedef double T;

// Added just to get access to the BlockData3D protected constructor
// of BlockDataF2D.
template <typename T, typename BaseType>
class TestBlockDataF3D : public BlockDataF3D<T, BaseType>
{
public:

    TestBlockDataF3D(BlockData3D<T, BaseType>& data):
        BlockDataF3D<T, BaseType>(data)
    {
    }
};

TEST(BlockPlus3DTest, testSimpleAdd)
{
    auto aDataLeft = initBlockData3D<double>(1, 1, 1, {{{1}}});
    auto aDataRight = initBlockData3D<double>(1, 1, 1, {{{10}}});
    TestBlockDataF3D<T,T> left(aDataLeft);
    TestBlockDataF3D<T,T> right(aDataRight);
    BlockPlus3D<double> plus(left, right);
    double res[1];
    int pos[3] = {0, 0, 0};
    plus(res, pos);
    ASSERT_EQ(11, res[0]);
}

TEST(BlockMinus3DTest, testSimpleSub)
{
    auto aDataLeft = initBlockData3D<double>(1, 1, 1, {{{1}}});
    auto aDataRight = initBlockData3D<double>(1, 1, 1, {{{10}}});
    TestBlockDataF3D<T,T> left(aDataLeft);
    TestBlockDataF3D<T,T> right(aDataRight);
    BlockMinus3D<double> plus(left, right);
    double res[1];
    int pos[3] = {0, 0, 0};
    plus(res, pos);
    ASSERT_EQ(-9, res[0]);
}

TEST(BlockMultiplication3DTest, testSimpleMultiplicaction)
{
    auto aDataLeft = initBlockData3D<double>(1, 1, 1, {{{3}}});
    auto aDataRight = initBlockData3D<double>(1, 1, 1, {{{10}}});
    TestBlockDataF3D<T,T> left(aDataLeft);
    TestBlockDataF3D<T,T> right(aDataRight);
    BlockMultiplication3D<double> plus(left, right);
    double res[1];
    int pos[3] = {0, 0, 0};
    plus(res, pos);
    ASSERT_EQ(30, res[0]);
}

TEST(BlockDivision2DTest, testSimpleDivision)
{
    auto aDataLeft = initBlockData3D<double>(1, 1, 1, {{{10}}});
    auto aDataRight = initBlockData3D<double>(1, 1, 1, {{{2}}});
    TestBlockDataF3D<T,T> left(aDataLeft);
    TestBlockDataF3D<T,T> right(aDataRight);
    BlockDivision3D<double> plus(left, right);
    double res[1];
    int pos[3] = {0, 0, 0};
    plus(res, pos);
    ASSERT_EQ(5, res[0]);
}
