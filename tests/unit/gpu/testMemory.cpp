/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#ifdef ENABLE_CUDA

#define D2Q9LATTICE 1
typedef double T;
#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"

using namespace olb;
using namespace olb::descriptors;

/*TEST(GPUMemoryTest, testZeroFluidMemory) {
    ConstRhoBGKdynamics<double, D2Q9Descriptor, BulkMomenta<double, D2Q9Descriptor>> dynamics;
    BlockLattice2D<double, D2Q9Descriptor> blockLattice(1, 1, &dynamics);
    blockLattice.defineDynamics(0, 0, &dynamics);
    blockLattice.initialize();
    GPUHandler<double, D2Q9Descriptor>::get().transferCellDataToGPU();
    GPUHandler<double, D2Q9Descriptor>::get().transferCellDataToCPU();
}
*/

TEST(GPUMemoryTest, testZeroFluidMemory) {
  GPUHandler<double, D2Q9Descriptor> gpuHandler;
  gpuHandler.allocateFluidField(1, 1);
  double** cpuData = new double*[12];
  for (int i = 0; i < 12; ++i)
  {
    cpuData[i] = new double[1];
    cpuData[i][0] = i;
  }

  bool* mask = new bool[1];
  mask[0]=true;

  gpuHandler.transferCellDataToGPU(cpuData, mask, 1);
  for (int i = 0; i < 12; ++i)
  {
    cpuData[i][0] = -1;
  }

  gpuHandler.transferCellDataToCPU(cpuData, mask, 1);
  for (int i = 0; i < 12; ++i)
  {
    ASSERT_EQ(i, cpuData[i][0]);
  }
}

#endif
