current_dir := $(dir $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))

include Makefile.inc

GTEST_DIR := $(current_dir)

GTEST_OBJDIR := $(OBJDIR)/tests/unit/gtest

GTEST_CPPFLAGS := $(filter-out -x cu,$(CPPFLAGS)) -isystem $(GTEST_DIR)include -DGTEST_HAS_PTHREAD=0

# Flags passed to the C++ compiler.
GTEST_CXXFLAGS := $(filter-out -x cu,$(CXXFLAGS)) -g -pthread

# All Google Test headers.  Usually you shouldn't change this
# definition.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h

GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

$(GTEST_OBJDIR)/gtest-all.o : $(GTEST_SRCS_)
	mkdir -p $(GTEST_OBJDIR)
	$(CXX) $(GTEST_CPPFLAGS) -I$(GTEST_DIR) $(GTEST_CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc -o $(GTEST_OBJDIR)/gtest-all.o

$(GTEST_OBJDIR)/gtest_main.o : $(GTEST_SRCS_)
	mkdir -p $(GTEST_OBJDIR)
	$(CXX) $(GTEST_CPPFLAGS) -I$(GTEST_DIR) $(GTEST_CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest_main.cc -o $(GTEST_OBJDIR)/gtest_main.o

$(LIBDIR)/gtest_main.a :  $(GTEST_OBJDIR)/gtest-all.o $(GTEST_OBJDIR)/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^

CLEANTARGETS += gtest-clean

gtest-clean :
	@rm $(LIBDIR)/gtest_main.a &> /dev/null || true
	@rm $(GTEST_OBJDIR)/*.o &> /dev/null || true
