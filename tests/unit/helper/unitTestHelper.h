/*  This file is part of the OpenLB library
 *
 *  Copyright (C) 2017 Markus Mohrhard
 *  E-mail contact: info@openlb.net
 *  The most recent release of OpenLB can be downloaded at
 *  <http://www.openlb.net/>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, write to the Free
 *  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
*/

#include <gtest/gtest.h>

#include <initializer_list>

#include "olb2D.h"
#include "olb2D.hh"

#include <iostream>

namespace olb {

namespace util {

template<>
inline bool approxEqual(const int& a, const int& b)
{
    return a == b;
}

}

namespace test {

template<typename T, template<typename U> class Lattice>
bool compareCell(CellView<T, Lattice> cell, const std::initializer_list<T>& compare)
{
    auto itr = compare.begin();
    for (size_t iPop = 0; iPop < Lattice<T>::q; ++itr, ++iPop)
    {
        if (!approxEqual(*itr, cell[iPop].get()))
            return false;
    }

    return true;
}

template<typename T, template<typename U> class Lattice>
testing::AssertionResult checkCell(CellView<T, Lattice> cell, const std::initializer_list<T>& compare)
{
    size_t q = Lattice<T>::q;
    assert(q == compare.size());

    if (compareCell(cell, compare))
    {
        return testing::AssertionSuccess();
    }
    else
    {
        auto result = testing::AssertionFailure();
        result << "Expected: ";

        for (auto& val: compare)
        {
            result << val << ", ";
        }

        result << "\nActual: ";

        for (size_t i = 0; i < q; ++i)
        {
            result << cell[i].get() << ", ";
        }

        return result;
    }
}

template<typename T, template<typename U> class Lattice>
void initCell(CellView<T, Lattice> cell, const std::initializer_list<T>& initData)
{
    size_t q = Lattice<T>::q;
    assert(q == initData.size());

    std::vector<T> data(initData);

    for (size_t i = 0; i < q; ++i)
    {
        cell[i] = data[i];
    }
}

template<typename T, typename BaseType = T>
BlockData2D<T, BaseType> initBlockData2D(int n_x, int n_y, const std::initializer_list<std::initializer_list<T>>& data)
{
    BlockData2D<T, BaseType> res(n_x, n_y);
    int x = 0;
    for(auto& row_data : data)
    {
        int y = 0;
        for(auto& cell : row_data)
        {
            res.get(x, y) = cell;
            ++y;
        }
        ++x;
    }

    return res;
}

template<typename T, typename BaseType = T>
BlockData3D<T, BaseType> initBlockData3D(int n_x, int n_y, int n_z, const std::initializer_list<std::initializer_list<std::initializer_list<T>>>& data)
{
    BlockData3D<T, BaseType> res(n_x, n_y, n_z);
    int x = 0;
    for(auto& x_data : data)
    {
        int y = 0;
        for(auto& y_data : x_data)
        {
            int z = 0;
            for (auto& cell : y_data)
            {
                res.get(x, y, z) = cell;
                ++z;
            }
            ++y;
        }
        ++x;
    }

    return res;
}

template<typename T, template<typename U> class Lattice, template<typename V, template<typename W> class> class BlockLattice>
void initBlockLattice2D(BlockLattice<T, Lattice>& lattice, std::initializer_list<std::initializer_list<double>> values)
{
  int nx = lattice.getNx();
  int ny = lattice.getNy();

  assert (((size_t)nx * (size_t)ny) == values.size());

  auto itr = values.begin();

  for (int iY = 0; iY < ny; ++iY) {
      for (int iX = 0; iX < nx; ++iX) {
          initCell(lattice.get(iX, iY), *itr);
          ++itr;
      }
  }
}

template<typename T, template<typename U> class Lattice>
testing::AssertionResult checkLattice2D(BlockLattice2D<T,Lattice>& lattice, std::initializer_list<std::initializer_list<T>> values)
{
  int nx = lattice.getNx();
  int ny = lattice.getNy();

  assert (((size_t)nx * (size_t)ny) == values.size());

  auto itr = values.begin();
  for (int iY = 0; iY < ny; ++iY) {
      for (int iX = 0; iX < nx; ++iX) {
          testing::AssertionResult res = checkCell(lattice.get(iX, iY), *itr);
          if (!res)
          {
              res << "Position: X: " << iX << ", Y: " << iY;
              return res;
          }

          ++itr;
      }
  }

  return testing::AssertionSuccess();
}

template<typename T, template<typename U> class Lattice>
void dumpLattice(BlockLattice2D<T, Lattice>& lattice)
{
  int nx = lattice.getNx();
  int ny = lattice.getNy();
  for (int iY = 0; iY < ny; ++iY) {
      for (int iX = 0; iX < nx; ++iX) {
        std::cout << ", {";
        CellView<T,Lattice> cell = lattice.get(iX, iY);
        for (int iPop = 0; iPop < Lattice<T>::q; ++iPop) {
          if (iPop != 0) {
            std::cout << ",";
          }
          std::cout << cell[iPop].get();
        }
        std::cout << "}" << std::endl;
      }
  }
}

}

}
