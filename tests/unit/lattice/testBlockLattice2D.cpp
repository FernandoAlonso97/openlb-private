/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"
#include "blockLattice2D_data1.h"

#include "olb2D.h"
#include "olb2D.hh"
using namespace olb;

void resetDirections(BlockLattice2D<double, D2Q9Descriptor>& blockLattice, int base)
{
  for (int iY = 0; iY < 3; ++iY) {
    for (int iX = 0; iX < 3; ++iX) {
      for (int iPop = 0; iPop < D2Q9Descriptor<double>::q; ++iPop) {
        int nextX = D2Q9Descriptor<double>::c(iPop, 0) + iX;
        int nextY = D2Q9Descriptor<double>::c(iPop, 1) + iY;
        if (0 > nextX || 3 <= nextX || 0 > nextY || 3 <= nextY)
        {
          blockLattice.get(iX, iY)[D2Q9Descriptor<double>::opposite(iPop)] = base + iPop + 1;
        }
      }
      base +=10;
    }
  }
}

TEST(BlockLattice2DTest, testStreamSingle)
{
  NoDynamics<double, D2Q9Descriptor> noDynamics;
  BlockLattice2D<double, D2Q9Descriptor> blockLattice(3, 3, &noDynamics);
  // blockLattice.defineDynamics(0, 2, 0, 2, &noDynamics);
  test::initBlockLattice2D(blockLattice, vals);
  EXPECT_TRUE(test::checkLattice2D(blockLattice, vals));

  blockLattice.collideAndStream<NoDynamics<double, D2Q9Descriptor>>();
  ::resetDirections(blockLattice, 100);
  test::dumpLattice(blockLattice);

  EXPECT_TRUE(test::checkLattice2D(blockLattice, valsSingleStepCompare));
}

TEST(BlockLattice2DTest, testStreamTwice)
{
  NoDynamics<double, D2Q9Descriptor> noDynamics;
  BlockLattice2D<double, D2Q9Descriptor> blockLattice(3, 3, &noDynamics);
  // blockLattice.defineDynamics(0, 2, 0, 2, &noDynamics);
  test::initBlockLattice2D(blockLattice, vals);
  EXPECT_TRUE(test::checkLattice2D(blockLattice, vals));

  blockLattice.collideAndStream<NoDynamics<double, D2Q9Descriptor>>();
  ::resetDirections(blockLattice, 100);
  blockLattice.collideAndStream<NoDynamics<double, D2Q9Descriptor>>();
  ::resetDirections(blockLattice, 200);

  test::dumpLattice(blockLattice);
  EXPECT_TRUE(test::checkLattice2D(blockLattice, valsCompare));
}
