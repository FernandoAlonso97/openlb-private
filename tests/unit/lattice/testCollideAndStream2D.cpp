/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"

using namespace olb;
using namespace olb::descriptors;

class TempFunctional : public WriteCellFunctional<double, D2Q9Descriptor>
{
public:

  virtual void apply(CellView<double,D2Q9Descriptor> cell, int pos[2]) const override
  {
    std::cout << pos[0] << ", " << pos[1] << std::endl;
    for (int iPop = 0; iPop < 9; ++iPop)
    {
      std::cout << cell[iPop].get() << ", ";
    }
    std::cout << std::endl;
    std::cout << "rho: " << cell[9].get() << std::endl;
    std::cout << "u: " << cell[10].get() << ", " << cell[11].get() << std::endl;
  }
};

TEST(D2Q9CavityTest, testOneTimeStep)
{
  typedef double T;
  double omega = 0.51;
  ConstRhoBGKdynamics<T, D2Q9Descriptor,BulkMomenta<T,D2Q9Descriptor>> bulkDynamics(omega,
      instances::getBulkMomenta<T, D2Q9Descriptor>());
  BlockLattice2D<T, D2Q9Descriptor> lattice(3, 3, &bulkDynamics);
  //lattice.defineDynamics(1, 1, 1, 1, &bulkDynamics);
  lattice.initDataArrays();

  OnLatticeBoundaryCondition2D<T, D2Q9Descriptor>* boundaryCondition =
    createInterpBoundaryCondition2D<T,D2Q9Descriptor,
    ConstRhoBGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addVelocityBoundary0N(0, 0, 1, 1, omega);
  boundaryCondition->addVelocityBoundary0P(2, 2, 1, 1, omega);
  boundaryCondition->addVelocityBoundary1N(1, 1, 0, 0, omega);
  boundaryCondition->addVelocityBoundary1P(1, 1, 2, 2, omega);
  boundaryCondition->addExternalVelocityCornerNN(0, 0, omega);
  boundaryCondition->addExternalVelocityCornerNP(0, 2, omega);
  boundaryCondition->addExternalVelocityCornerPN(2, 0, omega);
  boundaryCondition->addExternalVelocityCornerPP(2, 2, omega);

  // setBoundaryValues
  for (int iX = 0; iX < 3; ++iX) {
    for (int iY = 0; iY < 3; ++iY) {
      double vel[] = { 0.0, 0.0 };
      lattice.get(iX, iY).defineRhoU(1.0, vel);
      lattice.get(iX,iY).iniEquilibrium(1.0, vel);
    }
  }

  double vel[] = { 1.0, 0.0 };
  lattice.get(1, 2).defineRhoU(1.0, vel);
  lattice.get(1, 2).iniEquilibrium(1, vel);
  lattice.collideAndStream<ConstRhoBGKdynamics<T,D2Q9Descriptor,BulkMomenta<T,D2Q9Descriptor>>>();

  TempFunctional functional;
  lattice.forAll(functional);
}

TEST(D2Q9CavityTest, testTwoTimeSteps)
{
  int iXLeftBorder = 0;
  int iXRightBorder = 3;
  int iYBottomBorder = 0;
  int iYTopBorder = 3;
  typedef double T;
  double omega = 0.51;
  ConstRhoBGKdynamics<T, D2Q9Descriptor, BulkMomenta<T,D2Q9Descriptor>> bulkDynamics(omega,
      instances::getBulkMomenta<T, D2Q9Descriptor>());
  BlockLattice2D<T, D2Q9Descriptor> lattice(iXRightBorder + 1, iYTopBorder + 1, &bulkDynamics);
  // lattice.defineDynamics(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder + 1, iYTopBorder - 1, &bulkDynamics);

  OnLatticeBoundaryCondition2D<T, D2Q9Descriptor>* boundaryCondition =
    createInterpBoundaryCondition2D<T,D2Q9Descriptor,
    ConstRhoBGKdynamics>(lattice);

  // prepareLattice
  boundaryCondition->addVelocityBoundary0N(iXLeftBorder, iXLeftBorder, iYBottomBorder + 1, iYTopBorder - 1, omega);
  boundaryCondition->addVelocityBoundary0P(iXRightBorder, iXRightBorder, iYBottomBorder + 1, iYTopBorder - 1, omega);
  boundaryCondition->addVelocityBoundary1N(iXLeftBorder + 1, iXRightBorder - 1, iYBottomBorder, iYBottomBorder, omega);
  boundaryCondition->addVelocityBoundary1P(iXLeftBorder + 1, iXRightBorder - 1, iYTopBorder, iYTopBorder, omega);
  boundaryCondition->addExternalVelocityCornerNN(iXLeftBorder, iYBottomBorder, omega);
  boundaryCondition->addExternalVelocityCornerNP(iXLeftBorder, iYTopBorder, omega);
  boundaryCondition->addExternalVelocityCornerPN(iXRightBorder, iYBottomBorder, omega);
  boundaryCondition->addExternalVelocityCornerPP(iXRightBorder, iYTopBorder, omega);

  // setBoundaryValues
  for (int iX = 0; iX <= iXRightBorder; ++iX) {
    for (int iY = 0; iY <= iYTopBorder; ++iY) {
      double vel[] = { 0.0, 0.0 };
      lattice.get(iX, iY).defineRhoU(1.0, vel);
      lattice.get(iX,iY).iniEquilibrium(1.0, vel);
    }
  }

  double vel[] = { 1.0, 0.0 };
  for (int iX = iXLeftBorder + 1; iX <= iXRightBorder - 1; ++iX) {
    lattice.get(iX, iYTopBorder).defineRhoU(1.0, vel);
    lattice.get(iX, iYTopBorder).iniEquilibrium(1, vel);
  }

  lattice.initDataArrays();
  TempFunctional functional;
  for (int iT = 0; iT < 1000; ++iT)
  {
    std::cout << "Time: " << iT << std::endl;
    lattice.collideAndStream<ConstRhoBGKdynamics<T,D2Q9Descriptor,BulkMomenta<T,D2Q9Descriptor>>>();
    lattice.forAll(functional);
  }

}
