
#include <iostream>
#include <gtest/gtest.h>
#include "../../../src/core/dynamicsContainer2D.h"
#include "../../../src/core/dynamicsContainer2D.hh"
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"


using namespace olb;
using namespace olb::descriptors;

TEST(DynamicsContainerTest, testConstructor) {
  BGKdynamics<double, D2Q9Descriptor > dynamic(1.0, instances::getBulkMomenta<double, D2Q9Descriptor >());
  DynamicsContainer2D<double, D2Q9Descriptor> container(3, 3, &dynamic);
  for (int x = 0; x < 3; ++x)
  {
    for (int y = 0; y < 3; ++y)
    {
      ASSERT_TRUE(container.isFluidCell(x, y));
    }
  }
}

TEST(DynamicsContainerTest, testDefineDynamicsResetFluidMask) {
  BGKdynamics<double, D2Q9Descriptor > dynamic(1.0, instances::getBulkMomenta<double, D2Q9Descriptor >());
  DynamicsContainer2D<double, D2Q9Descriptor> container(4, 4, &dynamic);
  container.defineDynamics(1, 2, 1, 2, nullptr);
  for (int x = 0; x < 4; ++x)
  {
    for (int y = 0; y < 4; ++y)
    {
      if ((x >= 1 && x <= 2) && (y >= 1 && y <= 2))
        ASSERT_FALSE(container.isFluidCell(x, y)) << "x: " << x << ", y:" << y;
      else
        ASSERT_TRUE(container.isFluidCell(x, y)) << "x: " << x << ", y:" << y;
    }
  }
}

TEST(DynamicsContainerTest, testDefineDynamicsStorage) {
  BGKdynamics<double, D2Q9Descriptor > dynamic(1.0, instances::getBulkMomenta<double, D2Q9Descriptor >());
  DynamicsContainer2D<double, D2Q9Descriptor> container(4, 4, &dynamic);
  container.defineDynamics(1, 2, 1, 2, nullptr);
  for (int x = 0; x < 4; ++x)
  {
    for (int y = 0; y < 4; ++y)
    {
      if ((x >= 1 && x <= 2) && (y >= 1 && y <= 2))
        ASSERT_TRUE(container.getDynamics(x, y) == nullptr) << "x: " << x << ", y:" << y;
      else
        ASSERT_TRUE(container.getDynamics(x, y) == container.getFluidDynamics()) << "x: " << x << ", y:" << y;
    }
  }
}
