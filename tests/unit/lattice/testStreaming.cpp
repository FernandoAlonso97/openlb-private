/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
using namespace olb;

TEST(Streaming2DTest, testOffsetCalculationD2Q9)
{
  int offsetData[] = {0, -2, 1, 4, 3, 2, -1, -4, -3, 0, 0, 0};
  for (int iPop = 0; iPop < 12; ++ iPop)
  {
    int offset = util::getGridOffset<olb::descriptors::D2Q9Descriptor<double>>(iPop, 3);
    ASSERT_EQ(offsetData[iPop], offset);
  }
}
