###########################################################################
## definitions

current_dir := $(dir $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))

include Makefile.inc
include rules.mk

UNIT_TEST_DIR := $(current_dir)

TEST_SOURCES := $(shell find $(current_dir) -name 'test*.cpp')
TEST_OBJECTS := $(patsubst %.cpp, %.o, $(TEST_SOURCES))
TEST_EXECUTABLES := $(patsubst %.cpp, %, $(TEST_SOURCES))
TESTS += $(notdir $(basename $(TEST_EXECUTABLES)))

SHARED_TEST_HEADER := $(current_dir)helper/unitTestHelper.h

include $(addsuffix /gtest/module.mk,$(current_dir))

.PHONY: unittest-clean

# actually generate the targets for the tests

$(foreach test,$(TEST_EXECUTABLES), \
    $(eval $(call linkTest,$(test))))

$(foreach test,$(TEST_EXECUTABLES), \
    $(eval $(call compileTest,$(test))))

$(foreach test,$(TEST_EXECUTABLES), \
    $(eval $(call runTest,$(notdir $(basename $(test))),$(test))))

## define how to remove the generated test files

unittest-clean:
	@rm $(TEST_OBJECTS) &> /dev/null || true
	@rm $(TEST_EXECUTABLES) &> /dev/null || true

CLEANTARGETS += unittest-clean

EXTRA_IDIR += -I./$(UNIT_TEST_DIR) -I./$(UNIT_TEST_DIR)gtest/include
