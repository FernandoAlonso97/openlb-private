/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Markus Mohrhard
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;

// D2Q9 Forced MRT Tests
TEST(ForcedMRTD2Q9Test, testGetOmega) {
    ForcedMRTdynamics<double, ForcedMRTD2Q9Descriptor > dynamic(1.0, instances::getBulkMomenta<double, ForcedMRTD2Q9Descriptor>() );
    double omega = dynamic.getOmega();
    ASSERT_EQ(1.0, omega);
}

TEST(ForcedMRTD2Q9Test, testCollideNoVelocity) {
    ForcedMRTdynamics<double, ForcedMRTD2Q9Descriptor> dynamics(1.0, instances::getBulkMomenta<double, ForcedMRTD2Q9Descriptor>());
    CellView<double, ForcedMRTD2Q9Descriptor> cell(&dynamics);
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}));
}

TEST(ForcedMRTD2Q9Test, testCollide) {
    ForcedMRTdynamics<double, ForcedMRTD2Q9Descriptor> dynamics(1.0, instances::getBulkMomenta<double, ForcedMRTD2Q9Descriptor>());
    CellView<double, ForcedMRTD2Q9Descriptor> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell,{4.3000000000000007, 0.1750000000000006, 0.99999999999999933, 0.1750000000000006,
                0.99999999999999933, 0.1750000000000006, 0.99999999999999933, 0.1750000000000006, 0.99999999999999933}));
}

// D3Q7 Forced MRT Tests

TEST(ForcedMRTD3Q19Test, testGetOmega) {
    ForcedMRTdynamics<double, ForcedMRTD3Q19Descriptor > dynamic(1.0, instances::getBulkMomenta<double, ForcedMRTD3Q19Descriptor>() );
    double omega = dynamic.getOmega();
    ASSERT_EQ(1.0, omega);
}

TEST(ForcedMRTD3Q19Test, testCollideNoVelocity) {
    ForcedMRTdynamics<double, ForcedMRTD3Q19Descriptor> dynamics(1.0, instances::getBulkMomenta<double, ForcedMRTD3Q19Descriptor>());
    CellView<double, ForcedMRTD3Q19Descriptor> cell(&dynamics);
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}));
}

TEST(ForcedMRTD3Q19Test, testCollide) {
    ForcedMRTdynamics<double, ForcedMRTD3Q19Descriptor> dynamics(1.0, instances::getBulkMomenta<double, ForcedMRTD3Q19Descriptor>());
    CellView<double, ForcedMRTD3Q19Descriptor> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell,
                {7.9166666666666643, 0.87611111111110973, 0.87611111111110973, 0.87611111111110962,
                0.48555555555555641, 0.48555555555555641, 0.48555555555555641, 0.48555555555555641,
                0.48555555555555641, 0.48555555555555641, 0.87611111111110951, 0.87611111111110962,
                0.87611111111110962, 0.48555555555555641, 0.48555555555555641, 0.48555555555555641,
                0.48555555555555641, 0.48555555555555641, 0.48555555555555641}));
}
