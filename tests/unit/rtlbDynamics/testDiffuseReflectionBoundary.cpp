/*  This file is part of the OpenLB library
*
*  Copyright (C) 2017 Albert Mink
*  E-mail contact: info@openlb.net
*  The most recent release of OpenLB can be downloaded at
*  <http://www.openlb.net/>
*
*  This program is free software; you can redistribute it and/or
*  modify it under the terms of the GNU General Public License
*  as published by the Free Software Foundation; either version 2
*  of the License, or (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public
*  License along with this program; if not, write to the Free
*  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
*  Boston, MA  02110-1301, USA.
*/

#include <iostream>
#include <gtest/gtest.h>
#include "helper/unitTestHelper.h"

#include "olb2D.h"
#include "olb2D.hh"
#include "olb3D.h"
#include "olb3D.hh"

using namespace olb;
using namespace olb::descriptors;

// Changed datatype from double to int
// Expected to work, but no!
TEST(D3Q7PartialBounceBackTest, testCollideInt) {
    PartialBounceBack<int,D3Q7DescriptorRTLB> dynamics(2);
    CellView<int,D3Q7DescriptorRTLB> cell(&dynamics);
    test::initCell(cell, {1, 2, 3, 4, 5, 6, 7});
    LatticeStatistics<int> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {1, 5, 6, 7, 2, 3, 4}));
}

TEST(D3Q7PartialBounceBackTest, testCollideLimitBB) {
    PartialBounceBack<double,D3Q7DescriptorRTLB> dynamics(2);
    CellView<double,D3Q7DescriptorRTLB> cell(&dynamics);
    test::initCell(cell, {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {1.0, 5.0, 6.0, 7.0, 2.0, 3.0, 4.0}));
}

TEST(D3Q7PartialBounceBackTest, testCollide) {
    PartialBounceBack<double,D3Q7DescriptorRTLB> dynamics(1.9);
    CellView<double,D3Q7DescriptorRTLB> cell(&dynamics);
    test::initCell(cell, {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0});
    LatticeStatistics<double> statistics;
    dynamics.collide(cell, statistics);
    EXPECT_TRUE(test::checkCell(cell, {1.0, 0.88749999999999996, 0.88749999999999996, 0.88749999999999996, 0.88749999999999996, 0.88749999999999996, 0.88749999999999996}));
}
